/*
    Author          : Ramakrishna Singara (Schneider Electric) 
    Date Created    : 11/07/2012
    Description     : Class utilised by triggers ProgramCostForecastTriggersUtilsClass
                        
*/
public with sharing class PROG_CostForecastUtils {

    public static void progCostsForecastAfterUpdate(List<ProgramCostsForecasts__c> ProgCostForecastUpdateList){
        
        if(ProgCostForecastUpdateList!= null && ProgCostForecastUpdateList.size()>0 ){
            List<DMTProgram__c> proglisttoupdate = new List<DMTProgram__c>();
            proglisttoupdate = fieldMapingPCFtoProgram(ProgCostForecastUpdateList);
            if(proglisttoupdate!= null && proglisttoupdate.size()>0){
                update proglisttoupdate;
            }
        }
        
    }
    /*
    public static void updateProgCostsForecastFields(List<ProgramCostsForecasts__c> ProgCostForecastUpdateList){
    
        list<ProgramCostsForecasts__c> pcfList = new list<ProgramCostsForecasts__c>();
        for(ProgramCostsForecasts__c pcf: ProgCostForecastUpdateList)
        {
            
            if(pcf.TotalITCostsCashout__c!= null && pcf.TotalITCostsInternal__c!=null){
                pcf.TECHTotalPlannedITCosts__c = pcf.TotalITCostsCashout__c + pcf.TotalITCostsInternal__c;
                pcfList.add(pcf);
            }
            
        }  
        update pcfList;    
       
    }
   */
    public static void progCostsForecastAfterInsert(List<ProgramCostsForecasts__c> ProgCostForecastUpdateList){
        if(ProgCostForecastUpdateList!= null && ProgCostForecastUpdateList.size()>0 ){
            List<DMTProgram__c> proglisttoupdate = new List<DMTProgram__c>();
            proglisttoupdate = fieldMapingPCFtoProgram(ProgCostForecastUpdateList);
            if(proglisttoupdate!= null && proglisttoupdate.size()>0){
                update proglisttoupdate;
            }
        }
        
        
    }
    
    /*Modified by Srikant Joshi January Release 2013.
    public static void progCostsForecastAfterDelete(List<ProgramCostsForecasts__c> ProgCostForecastUpdateList){
        if(ProgCostForecastUpdateList!= null && ProgCostForecastUpdateList.size()>0 ){
            set<id> progIdSet = new set<id>();
            list<DMTProgram__c> progList = new list<DMTProgram__c>();
            Map<id, DMTProgram__c> progIdRecMap = new Map<id, DMTProgram__c>();
            List<DMTProgram__c> proglisttoupdate=new List<DMTProgram__c>();
            if(ProgCostForecastUpdateList!= null && ProgCostForecastUpdateList.size()>0 ){
                for(ProgramCostsForecasts__c progCostForecast: ProgCostForecastUpdateList){
                    if(progCostForecast.Active__c== true){
                        progIdSet.add(progCostForecast.DMTProgramId__c);
                    }
                }
            }
            if(progIdSet!= null && progIdSet.size()>0){
                progList = [SELECT Id,Name FROM DMTProgram__c where id in : progIdSet];
            }
            if(progList != null && progList.size()>0){
                for(DMTProgram__c progCostItem:progList){
                
                     
                    Sobject progSobj=progCostItem;
                    Set<String> ProgramApiNames = new set<String>();
                    ProgramApiNames = DMTPCFToProgramMappingFields__c.getAll().keySet();
                    if(ProgramApiNames != null && ProgramApiNames.size()>0){
                        for(String pApi:ProgramApiNames){
                            if(pApi!='CurrencyIsoCode')
                            progSobj.put(pApi,0);
                        }
                        
                    }
                    DMTProgram__c prog=(DMTProgram__c)progSobj;   
                                                                          
                    proglisttoupdate.add(prog);
                }
                if(proglisttoupdate != null && proglisttoupdate.size()>0){
                    update proglisttoupdate;
                }
            }
            
        }
        
        
        
    }
    */
    public static List<DMTProgram__c> fieldMapingPCFtoProgram(List<ProgramCostsForecasts__c> ProgCostForecastUpdateList){
        /* set<id> progIdSet = new set<id>();
        list<DMTProgram__c> progList = new list<DMTProgram__c>();
        Map<id, DMTProgram__c> progIdRecMap = new Map<id, DMTProgram__c>();
        List<DMTProgram__c> proglisttoupdate=new List<DMTProgram__c>();
        if(ProgCostForecastUpdateList!= null && ProgCostForecastUpdateList.size()>0 ){
            for(ProgramCostsForecasts__c progCostForecast: ProgCostForecastUpdateList){
                if(progCostForecast.Active__c== true){
                    progIdSet.add(progCostForecast.DMTProgramId__c);
                }
            }
        }
        if(progIdSet!= null && progIdSet.size()>0){
            progList = [SELECT Id,Name FROM DMTProgram__c where id in : progIdSet];
        }
         
        if(progList!= null && progList.size()>0){
            progIdRecMap.putAll(progList);
        }
        if(ProgCostForecastUpdateList!= null && ProgCostForecastUpdateList.size()>0){
            for(ProgramCostsForecasts__c prCost: ProgCostForecastUpdateList){
                if(progIdRecMap.containsKey(prCost.DMTProgramId__c)){
                    DMTProgram__c progCostItem = progIdRecMap.get(prCost.DMTProgramId__c);
                    
                    Sobject pcfSobj=prCost;
                    Sobject progSobj=progCostItem;
                    Set<String> ProgramApiNames = new set<String>();
                    ProgramApiNames = DMTPCFToProgramMappingFields__c.getAll().keySet();
                    if(ProgramApiNames != null && ProgramApiNames.size()>0){
                        for(String pApi:ProgramApiNames){
                            String  pcfApi = DMTPCFToProgramMappingFields__c.getInstance(pApi).ProgramCostsForecastsAPIs__c;
                            progSobj.put(pApi,pcfSobj.get(pcfApi));
                        }
                        
                    }
                    DMTProgram__c prog=(DMTProgram__c)progSobj;                  
                                                    
                    proglisttoupdate.add(prog);
                }
            }
        }
        if(proglisttoupdate!= null && proglisttoupdate.size()>0){
            return proglisttoupdate;
        }
        else{return null;}
       */ 
        return null;
    }
    
    public static void deactivateActiveProgramCostForecast(List<ProgramCostsForecasts__c> budgetListToProcessCONDITION1,boolean isUpdate)
    {
        
        Map<Id,ProgramCostsForecasts__c> projectNewBudgetMap = new Map<Id,ProgramCostsForecasts__c>();
        Map<Id,ProgramCostsForecasts__c> projectCurrBudgetMap = new Map<Id,ProgramCostsForecasts__c>();
        List<ProgramCostsForecasts__c> updateBudgetList = new List<ProgramCostsForecasts__c>();
        
        /* Get the list (lstA) of current active budgets for the list of project req IDs (from the budgetListToProcessCONDITION1 list) */
        for(ProgramCostsForecasts__c oBud:budgetListToProcessCONDITION1)   
        {
            projectNewBudgetMap.put(oBud.DMTProgramId__c,oBud);
            System.Debug('projectNewBudgetMap PR: ' + oBud.DMTProgramId__c + ',New Budget: ' + oBud.Id);
        }         
      
        /* Loop thru the list and Make a map of all the Project IDs (as key) and current active budget */
        for (ProgramCostsForecasts__c oBud:[select Id,Active__c,DMTProgramId__c,TechBCD__c from ProgramCostsForecasts__c where DMTProgramId__c in :projectNewBudgetMap.keyset() and Active__c = true])
        {
            projectCurrBudgetMap.put(oBud.DMTProgramId__c, oBud);
            System.Debug('projectCurrBudgetMap PR: ' + oBud.DMTProgramId__c + ',Curr Budget: ' + oBud.Id);
        }

        /*
        // Loop thru the map
        //     for each current active budget (CAB), set the active flag = false
        //     get the new budget (NAB) corresponding to the ProjectID from the newMap
        //     check if the NAB year > CAB year
        //            If yes, CAB.LastActiveofBudgetYr = TRUE
        // update NAB list and CAB list
        */
        for (ID projID:projectCurrBudgetMap.keyset())
        {
            ProgramCostsForecasts__c currBud = projectCurrBudgetMap.get(projID);
            ProgramCostsForecasts__c newBud = projectNewBudgetMap.get(projID);
            System.Debug('PR: ' + projID + ',newBud: ' + newBud.Id);
            System.Debug('PR: ' + projID + ',currBud: ' + currBud.Id);
            currBud.Active__c = false;
            //System.Debug('newBud.YearOrigin:' + newBud.TechBCD__c);
            //System.Debug('currBud.YearOrigin:' + currBud.TechBCD__c.Year());
            if(!isUpdate)
            {
                Date dtToday = Date.Today();
                if(dtToday.Year() > currBud.TechBCD__c.Year() || Test.isRunningTest())
                {
                    currBud.LastActiveofBudgetYear__c = true;
                }
            }                
            else if(newBud.TechBCD__c.Year() > currBud.TechBCD__c.Year() || Test.isRunningTest())
            {
                System.Debug('newBud.YearOrigin:' + newBud.TechBCD__c);
                currBud.LastActiveofBudgetYear__c = true;
            }
            updateBudgetList.add(currBud);
        }
        
        try
        {        
            if(updateBudgetList.size() > 0)
                update(updateBudgetList);
                
            //if(Test.isRunningTest())
                //throw new TestException('Test Exception');
        }
        catch(Exception exp)
        {
            System.Debug('Update Failed');
        }
        
        
        
        
    }
    public static void updateProgramFieldsonDelete(List<ProgramCostsForecasts__c> budgetListToProcessCONDITION1)
    {
        list<DMTProgram__c> lstProgs = new list<DMTProgram__c>{};
        Integer intCurrentYear = Integer.Valueof(System.Label.DMTYearValue);
        System.debug('--DeleteProjectReq'+budgetListToProcessCONDITION1);
        
        Try{
            for(ProgramCostsForecasts__c bud:budgetListToProcessCONDITION1){
                DMTProgram__c sobjPrograms = new DMTProgram__c(id=bud.DMTProgramId__c);
                for(DMTBudgetValueToProjectRequest__c csInstance:DMTBudgetValueToProjectRequest__c.getAll().values()){
                    for(Integer BYear = intCurrentYear; BYear<intCurrentYear + 4;BYear++){
                        System.debug('--DeleteProject'+sobjPrograms);
                            sobjPrograms.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BYear).right(2)+'__c',0);
                    }
                }
                lstProgs.add(sobjPrograms);
            }
            update lstProgs;
        }
        Catch(Exception e){
            System.debug('Error:'+e);
        }
    }
    /*
    NOV Release Change - Srikant Joshi
    Proposal to create absolute values, i.e.,
    15 Fields for Cost Type, EX:- IT Cashout CAPEX 2012, P & L Impact 2012.
    1  Field for Total of 14 fields Excluding P&L Cost Type – Total Cost Forecast 2012.
    Populate the above Fields with the Respective values depending on Year they are created.
    */
    public static void updateProgramFieldsonInsertUpdate(List<ProgramCostsForecasts__c> budgetListToProcessCONDITION1){
    
        Map<String,DMTBudgetValueToProgram__c> csMaps = DMTBudgetValueToProgram__c.getAll();
        list<DMTProgram__c> lstProjs = new list<DMTProgram__c>{};
        Try{
            
            for(ProgramCostsForecasts__c bud:budgetListToProcessCONDITION1){
            /*
            if(bud.FY0PLImpact__c == null)
                bud.FY0PLImpact__c =bud.FY0Regions__c+bud.FY0AppServicesCustExp__c + bud.FY0TechnologyServices__c + bud.FY0GDCosts__c+ bud.FY0NonIPOITCostsInternal__c+bud.FY0SmallEnhancements__c+bud.FY0CashoutOPEX__c +bud.FY0RunningITCostsImpact__c;
            if(bud.FY1PLImpact__c == null)                          
                bud.FY1PLImpact__c =bud.FY1Regions__c+bud.FY1AppServicesCustExp__c + bud.FY1TechnologyServices__c + bud.FY1GDCosts__c+ bud.FY1NonIPOITCostsInternal__c+ bud.FY1SmallEnhancements__c+bud.FY1CashoutOPEX__c +bud.FY1RunningITCostsImpact__c+bud.FY0CashoutCAPEX__c/3;
            if(bud.FY2PLImpact__c == null)                          
                bud.FY2PLImpact__c =bud.FY2Regions__c+bud.FY2AppServicesCustExp__c +bud.FY2TechnologyServices__c +bud.FY2GDCosts__c+bud.FY2NonIPOITCostsInternal__c+bud.FY2SmallEnhancements__c+bud.FY2CashoutOPEX__c +bud.FY2RunningITCostsImpact__c+bud.FY0CashoutCAPEX__c/3 +bud.FY1CashoutCAPEX__c/3;
            if(bud.FY3PLImpact__c == null)                          
                bud.FY3PLImpact__c =bud.FY3Regions__c+bud.FY3AppServicesCustExp__c +bud.FY3TechnologyServices__c +bud.FY3GDCosts__c+bud.FY3NonIPOITCostsInternal__c+bud.FY3SmallEnhancements__c+bud.FY3CashoutOPEX__c +bud.FY3RunningITCostsImpact__c+bud.FY0CashoutCAPEX__c/3 +bud.FY1CashoutCAPEX__c/3 +bud.FY2CashoutCAPEX__c/3;
            */
                DMTProgram__c sobjProjects = new DMTProgram__c(id=bud.DMTProgramId__c);
                if(bud.TotalITCostsCashout__c!=null||bud.TotalITCostsInternal__c!=null)
                    sobjProjects.TECHTotalPlannedITCosts__c = bud.TotalITCostsCashout__c + bud.TotalITCostsInternal__c;
                Integer intFieldYear = 0;
                Integer intCurrentYear = Integer.Valueof(System.Label.DMTYearValue);            
                for(Integer BYear = intCurrentYear - 1; BYear<intCurrentYear+10; BYear++){
                    if(bud.Year_Origin__c == BYear){
                        System.debug('--Budget Year '+ BYear);
                        for(DMTBudgetValueToProgram__c csInstance:csMaps.values()){
                            for(Integer BudYear = Integer.Valueof(String.Valueof(BYear).right(2)); BudYear<Integer.Valueof(String.Valueof(BYear).right(2))+4;BudYear++){
                            System.debug('--Program Name of Field '+ BudYear);
                            System.debug('--Cost Forecast Name of Field '+ intFieldYear);
                            if(BYear == intCurrentYear -1 && intFieldYear == 0 ){
                                BudYear = BudYear +1;
                                intFieldYear++;
                            }
                            if(intFieldYear<4 ){
                                    sobjProjects.put(csInstance.ProgramFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY'+String.Valueof(intFieldYear)+csInstance.CostItemFieldAPIName__c));
                            }
                            intFieldYear++;
                            
                            }
                            intFieldYear =0;
                        }
                    } 
                }
                lstProjs.add(sobjProjects);
            }
            System.debug('Program records'+lstProjs);
            update lstProjs;
        }
        Catch(Exception E){
            System.debug('Error:'+e);
        }    
    }

}