@isTest
private class AP_PRMCFL_HOME_Controller_TEST {

   @isTest static void testHome() {
	test.startTest();    
   	AP_PRMCFL_HOME_Controller c = new AP_PRMCFL_HOME_Controller();
   	System.debug(c.getContentWorkspace());   	
    System.assertEquals(false,c.getContentWorkspace().size()==1);
	
    test.stopTest();      
   }

   @isTest static void testRedirect() {
	test.startTest();    
   	AP_PRMCFL_HOME_Controller c = new AP_PRMCFL_HOME_Controller();
    PageReference pr = c.redirectPageLogin();
    System.assertEquals(null,pr);
    test.stopTest();      
   }

}