/*
* Test class of IdmsSocialLogin Controller
**/
@IsTest

class IdmsSocialLoginControllerTest{
    // test method 1 
    static testMethod void test1() {
        Test.startTest();
        PageReference pageRef = Page.IdmsSocialLogin;
        pageRef.getParameters().put('app','98765');
        Test.setCurrentPage(pageRef);
        
        IdmsSocialLoginController obj = new IdmsSocialLoginController();
        
        IDMSApplicationMapping__c appMap = new IDMSApplicationMapping__c();
        appMap.name = '98765';
        appMap.context__c = 'Work';
        appMap.AppName__c = 'PRM';
       // appMap.UrlAppSocialRegistrationPage__c= 'www.google.com';
        insert appMap;
        obj.socialRegUrl = 'www.google.com';
        obj.context = Label.CLQ316IDMS301;
        obj.redirect_fb();
        obj.context = '';
        obj.redirect_fb();
        obj.socialRegUrl = null;
        obj.redirect_fb();
        Test.stopTest();
    }
    
    //test method 2
    static testMethod void test2() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                          Email='testidmsuser9@accenture.com', 
                          EmailEncodingKey='UTF-8',
                          FirstName ='Test First',      
                          LastName='Testing',
                          CommunityNickname='CommunityName123',      
                          LanguageLocaleKey='en_US', 
                          country = '',
                          Country__c = '',
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='testidmsuser9@accenture.com' + Label.CLJUN16IDMS71,
                          UserRoleId = r.Id,
                          IDMS_Registration_Source__c = Label.CLJUN16IDMS154,
                          IDMS_User_Context__c = 'Home',
                          isActive = True,   
                          BypassVR__c = True);
        
        System.runAs(u) {
            
            IdmsSocialLoginController obj = new IdmsSocialLoginController();
            obj.checkCountry(u);
            
            Test.startTest();
            PageReference pageRef = Page.IdmsSocialLogin;
            pageRef.getParameters().put('app','98765');
            Test.setCurrentPage(pageRef);
            
            IDMSApplicationMapping__c appMap = new IDMSApplicationMapping__c();
            appMap.name = '98765';
            appMap.context__c = 'Work';
            appMap.AppName__c = 'PRM';
           // appMap.UrlAppSocialRegistrationPage__c= 'www.google.com';
            insert appMap;
            
            obj.socialRegUrl = 'www.google.com';
            obj.context = Label.CLQ316IDMS301;
            obj.redirect_fb();
            obj.context = '';
            obj.redirect_fb();
            obj.socialRegUrl = null;
            obj.redirect_fb();
            obj.context = 'Work';
            obj.redirect_fb();
            Test.stopTest();
        }
    }
    
    //test method 2
    static testMethod void testIdmsSocialLoginController() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                          Email='testidmsuser9@accenture.com', 
                          EmailEncodingKey='UTF-8',
                          FirstName ='Test First',      
                          LastName='Testing',
                          CommunityNickname='CommunityName123',      
                          LanguageLocaleKey='en_US', 
                          country = '',
                          Country__c = '',
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='testidmsuser9@accenture.com' + Label.CLJUN16IDMS71,
                          UserRoleId = r.Id,
                          IDMS_Registration_Source__c = Label.CLJUN16IDMS154,
                          IDMS_User_Context__c = 'Work',
                          isActive = True,   
                          BypassVR__c = True);
        
        System.runAs(u) {
            IDMSApplicationMapping__c appMap = new IDMSApplicationMapping__c();
            appMap.name = '98765';
            appMap.context__c = 'Work';
            appMap.AppName__c = 'HerokuApp';
            //appMap.UrlAppSocialRegistrationPage__c= 'https://www.google.com';
            insert appMap;
            IdmsSocialUser__c socialUser=new IdmsSocialUser__c();
            socialUser.name='IdmsKey';
            socialUser.AppValue__c='IdmsWork1';
            socialUser.oAuthUrl__c='https://www.google.com';
            socialUser.oAuthUrl2__c='https://www.google.com';
            socialUser.oAuthUrl3__c='https://www.google.com';
            insert socialUser;
            System.currentPagereference().getParameters().put('app',appMap.name);
            IdmsSocialLoginController idmsSocialLoginControllercls=new IdmsSocialLoginController();
            idmsSocialLoginControllercls.redirect_fb();
            
        }
    }
    
    
    
}