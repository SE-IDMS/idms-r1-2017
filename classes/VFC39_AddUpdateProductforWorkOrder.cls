public class VFC39_AddUpdateProductforWorkOrder extends VFC_ControllerBase 
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    public VCC08_GMRSearch GMRSearchClass{get;set;} // Controller for the search in PM0 functionality
    //public SVMXC__Service_Order__c CurrentWorkOrder; // Related Work Order
    public WOGMRSearch WOImplementation{get;set;}
    
    public VFC39_AddUpdateProductforWorkOrder(ApexPages.StandardController controller) 
    { 
        GMRSearchClass = new VCC08_GMRSearch();
        WOImplementation = new WOGMRSearch();
    }
 
     /*=======================================
      INNER CLASSES
    =======================================*/
    
    // Implementation of the Utils_GMRSearchMethods interface for the Work Orders
    public class WOGMRSearch implements Utils_GMRSearchMethods
    {

       public Void Init(String ObjID, VCC08_GMRSearch Controller)
       {
           //Pre-populates if there are existing values for Comm Ref & PM0 Heirachy
           SVMXC__Service_Order__c currentWorkOrder = [Select Id, Commercial_Reference__c, TECH_LaunchGMR__c, AdditionalCommercialReferences__c from SVMXC__Service_Order__c where Id=:ObjID limit 1];
           Controller.RelatedObject = (SVMXC__Service_Order__c)currentWorkOrder;
                
           if(Controller.ActionType == 'simple'){                               
             Controller.searchKeyword = currentWorkOrder.Commercial_Reference__c;
           }
                   
       }
       
       public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
       {
           VCC08_GMRSearch GMRController = (VCC08_GMRSearch)ControllerBase;
       
           if(GMRController.RelatedObject !=null)
           {
            
            if(GMRController.ActionType == 'simple')
            {
               AP18_WorkOrderIntegrationMethods.UpdateCommercialReferenceWorkOrder((SVMXC__Service_Order__c)GMRController.RelatedObject ,(DataTemplate__c)obj);
            }
            else if(GMRController.ActionType == 'multiple')
            {
               VCC06_DisplaySearchResults DisplayResultsController = (VCC06_DisplaySearchResults)GMRController.getcomponentControllerMap().get('resultComponent');
               List<DataTemplate__c> SelectedProducts = (List<DataTemplate__c>)DisplayResultsController.getSelectedObjects();
               AP18_WorkOrderIntegrationMethods.UpdateAdditionalCommercialReferencesWorkOrder((SVMXC__Service_Order__c)GMRController.RelatedObject , SelectedProducts);
            }
         }
           PageReference WorkOrderPage = new ApexPages.StandardController(GMRController.RelatedObject).view();
           return WorkOrderPage;  
       }
       
       public PageReference Cancel( VCC08_GMRSearch Controller)
       {
           //Controller.currentWorkOrder.TECH_LaunchGMR__c = false;
           //DataBase.SaveResult UpdateCase = Database.Update(Controller.currentWorkOrder);
           PageReference WorkOrderPage = new ApexPages.StandardController(Controller.RelatedObject ).view();
           return WorkOrderPage;
       }      
    }
      
        
}