/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class ServiceMaxSFMCustomEvents_TEST {

    static testMethod void populateDeinstalledDatesWithDateTest() {
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'test', DecomissioningDate__c = system.today().addDays(-10));
        insert ip;
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord request = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject header = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        header.objName = 'Swap_Asset__c';
        List<SVMXC.SFM_WrapperDef.SFM_StringMap> recordsAsKeyValue = new List<SVMXC.SFM_WrapperDef.SFM_StringMap>();
        SVMXC.SFM_WrapperDef.SFM_Record record = new SVMXC.SFM_WrapperDef.SFM_Record();
        
        recordsAsKeyValue.add(new SVMXC.SFM_WrapperDef.SFM_StringMap('DeinstalledInstalledProduct__c', ip.Id));
        record.setTargetRecordAsKeyValue(recordsAsKeyValue);
        header.setRecords(new List<SVMXC.SFM_WrapperDef.SFM_Record> {record});
        request.headerRecord = header;
        request.detailRecords = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> { header};
        
        ServiceMaxSFMCustomEvents.populateDeinstalledDates(request);
        
    }
    
    static testMethod void populateDeinstalledDatesWithoutDateTest() {
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'test');
        insert ip;
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord request = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject header = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        header.objName = 'Swap_Asset__c';
        List<SVMXC.SFM_WrapperDef.SFM_StringMap> recordsAsKeyValue = new List<SVMXC.SFM_WrapperDef.SFM_StringMap>();
        SVMXC.SFM_WrapperDef.SFM_Record record = new SVMXC.SFM_WrapperDef.SFM_Record();
        
        recordsAsKeyValue.add(new SVMXC.SFM_WrapperDef.SFM_StringMap('DeinstalledInstalledProduct__c', ip.Id));
        record.setTargetRecordAsKeyValue(recordsAsKeyValue);
        header.setRecords(new List<SVMXC.SFM_WrapperDef.SFM_Record> {record});
        request.headerRecord = header;
        request.detailRecords = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> { header};
        
        ServiceMaxSFMCustomEvents.populateDeinstalledDates(request);
        
    }
    
    static testMethod void populateNewinstalledDatesWithDateTest() {
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'test', CommissioningDateInstallDate__c = system.today().addDays(-10));
        insert ip;
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord request = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject header = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        header.objName = 'Swap_Asset__c';
        List<SVMXC.SFM_WrapperDef.SFM_StringMap> recordsAsKeyValue = new List<SVMXC.SFM_WrapperDef.SFM_StringMap>();
        SVMXC.SFM_WrapperDef.SFM_Record record = new SVMXC.SFM_WrapperDef.SFM_Record();
        
        recordsAsKeyValue.add(new SVMXC.SFM_WrapperDef.SFM_StringMap('InstalledProduct__c', ip.Id));
        record.setTargetRecordAsKeyValue(recordsAsKeyValue);
        header.setRecords(new List<SVMXC.SFM_WrapperDef.SFM_Record> {record});
        request.headerRecord = header;
        request.detailRecords = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> { header};
        
        SVMXC.SFM_WrapperDef.SFM_PageData pageData = ServiceMaxSFMCustomEvents.populateNewinstalledDates(request);
        
    }
    
    static testMethod void populateNewinstalledDatesWithoutDateTest() {
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'test');
        insert ip;
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord request = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject header = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        header.objName = 'Swap_Asset__c';
        List<SVMXC.SFM_WrapperDef.SFM_StringMap> recordsAsKeyValue = new List<SVMXC.SFM_WrapperDef.SFM_StringMap>();
        SVMXC.SFM_WrapperDef.SFM_Record record = new SVMXC.SFM_WrapperDef.SFM_Record();
        
        recordsAsKeyValue.add(new SVMXC.SFM_WrapperDef.SFM_StringMap('InstalledProduct__c', ip.Id));
        record.setTargetRecordAsKeyValue(recordsAsKeyValue);
        header.setRecords(new List<SVMXC.SFM_WrapperDef.SFM_Record> {record});
        request.headerRecord = header;
        request.detailRecords = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> { header};
        
        SVMXC.SFM_WrapperDef.SFM_PageData pageData = ServiceMaxSFMCustomEvents.populateNewinstalledDates(request);
        
    }
}