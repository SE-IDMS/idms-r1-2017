/********************************************************************
* Company: Fielo
* Created Date: 03/03/2015
* Description: Main page controller to manage the Fielo CMS
********************************************************************/


global without sharing class FieloPRM_VFC_PageController{

    public List<FieloEE__Section__c> sections {get; set;}
    public Map<Id,List<FieloEE__Section__c>> componentsSection {get; set;}

    public Boolean displayPoints {get;set;}
    public String partnersName {get;set;}
    public String pageName {get; set;}
    public String parentMenu {get; set;}

    //SITE TEMPLATE VARIABLES
    public String browser {get{return getBrowser();} set;}
    public String os {get{return getOS();} set;}
    private String userAgent {get{if(userAgent == null) userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT'); return userAgent;} set;}
    
    public FieloPRM_UTILS_Wrappers.ContactWrapper contactWrapper {get;set;}
    public FieloPRM_UTILS_Wrappers.MemberWrapper memberWrapper {get;set;}
    public String primaryChannelCategoryName {get;set;}
    public FieloEE__ProgramLayout__c layout {get; set;}
    public String primaryChannelCategoryId {get;set;}
    public String fieldOptionalName {get;set;}
    public Boolean showNotification {get;set;}
    public Boolean isAuthenticated {get;set;}
    public FieloEE__Menu__c menu2 {get;set;}
    public String fieldMenuTitle {get;set;}
    public String welcomeMessage {get;set;}
    public String idPrimaryMenu {get;set;}
    public String redirectURL {get;set;}
    public String parameters {get;set;}
    public Boolean isMobile {get;set;}
    public Boolean isTablet {get;set;}
    public String bgAttid {get;set;}
    
    private static String validlang = Label.FieloPRM_CL_65_ValidLanguage;
    private FieloEE__Member__c member;
    private String lang;
    
    public transient Map<String,String> URLParams = ApexPages.currentPage().getParameters();
    global transient String gotoParam = ApexPages.currentPage().getParameters().get('goto');
    global transient String sunQueryStringParam = ApexPages.currentPage().getParameters().get('SunQueryParamsString');
    global transient String currentURL = Apexpages.currentPage().getUrl();
    global transient String referredFromUrl = ApexPages.currentPage().getHeaders().get('Referer');
    global Static CS_PRM_ApexJobSettings__c serveMinify{set;get{
    if(CS_PRM_ApexJobSettings__c.getInstance('SERVE_MINIFIED') != null)
        return CS_PRM_ApexJobSettings__c.getInstance('SERVE_MINIFIED');
    else
        return null;
    }}
    
    private AP_PRMUtils.LanguageCountryCount LanguageCntryCount = new AP_PRMUtils.LanguageCountryCount();
    global transient string loginParameters = '';
    global transient Cookie cLanguage ; 
    global transient Cookie Country ;
    private String languageCode ='';
    private String cookieLang = '';
    private String cntryCode = '';
    public transient Id contactId {get;set;}
    
    
    //CONSTRUCTOR
    public FieloPRM_VFC_PageController(){
        
        //Variables initialization
        primaryChannelCategoryName = '';
        primaryChannelCategoryId = '';
        showNotification = true;
        redirectURL = null;
        idPrimaryMenu = '';        
        //Member query
        if(FieloEE.MemberUtil.getMemberId() != null){
            isAuthenticated = true;
            member = [SELECT Id,FieloEE__User__r.contactId, FieloEE__Points__c, F_PRM_MemberFeatures__c, FieloEE__FirstName__c, FieloEE__LastName__c, F_PRM_PrimaryChannel__c, F_PRM_CountryCode__c, F_PRM_Country__r.CountryCode__c, F_Country__r.CountryCode__c, F_PRM_Channels__c,F_Country__c, 
                      F_CountryName__c, F_PRM_LastLoginDateTime__c,Name,F_PRM_PrimaryContact__c
                      FROM FieloEE__Member__c
                      WHERE Id =: FieloEE.MemberUtil.getMemberId()];
            
            //Reset Menu Cookies and set the Member Cookie
            if((!ApexPages.currentPage().getCookies().containsKey('memberIdc')) || (ApexPages.currentPage().getCookies().containsKey('memberIdc') && ApexPages.currentPage().getCookies().get('memberIdc').getValue() != member.id)){
                FieloPRM_UTILS_MenuMethods.resetRedirectCookie();
                FieloPRM_UTILS_MenuMethods.setACookie('memberIdc', string.valueof(member.id));
            }
            //Set variable "idPrimaryMenu" and "IdPrimaryChannelMenu" Cookie
            if(!ApexPages.currentPage().getCookies().containsKey('IdPrimaryChannelMenu')){
                if(FieloPRM_UTILS_Member.getMemberPrimaryChannel(member) != null){
                    FieloPRM_UTILS_MenuMethods.setACookie('IdPrimaryChannelMenu', FieloPRM_UTILS_Member.getMemberPrimaryChannel(member).F_PRM_Menu__c);
                }
            }
            if(ApexPages.currentPage().getCookies().containsKey('IdPrimaryChannelMenu')){
                idPrimaryMenu = ApexPages.currentPage().getCookies().get('IdPrimaryChannelMenu').getValue();    
            }
            
            contactId = member.FieloEE__User__r.contactId;
        }
        
        //Language variables
        cLanguage = ApexPages.currentPage().getCookies().get('language');
        if(cLanguage != null){
            cookieLang = cLanguage .getValue();
        }
        Country = ApexPages.currentPage().getCookies().get('country');
        
        //Cookie language
        if(!ApexPages.currentPage().getCookies().containsKey('language') || ( ApexPages.currentPage().getCookies().containsKey('language') && ApexPages.currentPage().getCookies().get('language').getValue() == null)){
            FieloPRM_UTILS_MenuMethods.setACookie('language', FieloPRM_UTILS_Member.getBaseLanguage());
        }
        lang = FieloEE.OrganizationUtil.getLanguage();
        if(!String.isBlank(lang) && !validlang.contains(lang)){
            Cookie aux = new Cookie('language', null, null, 0, false);
            ApexPages.currentPage().setCookies(new Cookie[]{aux});
            ApexPages.currentPage().getParameters().remove('language');
        }
        
        //Multilanguage Title field
        fieldMenuTitle = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__Menu__c(), 'FieloEE__Title__c', lang);
        
        //Multilanguage OptionalName field
        fieldOptionalName = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__Component__c(), 'FieloEE__OptionalName__c', lang);
        
        String menuQuerySelectFrom = 'SELECT Id, FieloEE__ExternalName__c, F_PRM_Type__c,FieloEE__Menu__c, FieloEE__Menu__r.FieloEE__ExternalName__c, ' +
                                     'F_PRM_Status__c, FieloEE__visibility__c, F_PRM_AvailableGuestUsers__c, F_PRM_TitleCustomLabel__c, ' +
                                     'FieloEE__AttachmentId__c, F_PRM_ShowSupportStickyBar__c, ' + fieldMenuTitle + ' ' +
                                     'FROM FieloEE__Menu__c ';
        
        //Menu query
        if(Apexpages.currentPage().getParameters().containsKey('idMenu')){
            menu2 = Database.query(menuQuerySelectFrom + 'WHERE Id = \'' + Apexpages.currentPage().getParameters().get('idMenu') + '\'');
        }
        
        //If the Member has not completed the registration process or it is the First Login => redirection to the registration page
       /* if((menu2 == null || (menu2 != null && menu2.FieloEE__ExternalName__c != 'ContinueRegistration' && menu2.FieloEE__ExternalName__c != 'RegistrationCompletion')) && FieloPRM_UTILS_MenuMethods.getRegistrationStringMenu() != null){
            String statusRegistration = FieloPRM_UTILS_MenuMethods.getRegistrationStringMenu();
            if(statusRegistration  != null){
                redirectURL = '/partners/Menu/' + statusRegistration;
                return;
             }
        }*/
        
        if((menu2 == null || 
            (menu2 != null && menu2.FieloEE__ExternalName__c != 'ContinueRegistration' && menu2.FieloEE__ExternalName__c != 'RegistrationCompletion' && menu2.FieloEE__ExternalName__c != 'MainTermsAndConditions' && !menu2.FieloEE__ExternalName__c.containsIgnoreCase('TermsAndConditions'))) && 
            FieloPRM_UTILS_MenuMethods.getRegistrationStringMenu() != null) {

            System.debug('*** Inside Menu2');

            String statusRegistration = FieloPRM_UTILS_MenuMethods.getRegistrationStringMenu();
            if ('MainTermsAndConditions'.equalsIgnoreCase(statusRegistration) && member != null) {
                String tncMenu = 'P_{0}_{1}_TermsAndConditions';
                String tncMenu2 = member.F_PRM_Country__r.Country__r.CountryCode__c == 'US' ? 'TermsAndConditions' : String.format(tncMenu, new String[] { member.F_PRM_Country__r.Country__r.CountryCode__c, member.F_PRM_Language__c });

                redirectURL = '/partners/Menu/' + tncMenu2;
                return;                
            }
            if(statusRegistration  != null){
                redirectURL = '/partners/Menu/' + statusRegistration;
                return;

            }
        }
        
        //MENU RE-WRITE
        String correctMenuExternalName;
        if(menu2 != null){
            if(menu2.FieloEE__ExternalName__c == 'MyPartnership'){
                //MyPartnership Cookie
                correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setMyPartnershipMenuCookie(FieloPRM_UTILS_Member.getMemberPrimaryChannel(member));
            }else if(menu2.FieloEE__ExternalName__c == 'MyPrograms'){
                //MyPrograms Cookie
                correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setMyProgramsMenuCookie(member);
            }else if(menu2.FieloEE__ExternalName__c == 'products'){
                //products Cookie
                correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setCountryMenuByParentCookie(member, 'products', 'productsMenu');
            }else if(menu2.FieloEE__ExternalName__c == 'Support'){
                //Support Cookie
                correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setCountryMenuByParentCookie(member, 'Support', 'supportMenu');
            }
            
            if(member != null & Test.isRunningTest()){
                correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setMyPartnershipMenuCookie(FieloPRM_UTILS_Member.getMemberPrimaryChannel(member));
                correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setHomeMenuCookie(member, FieloPRM_UTILS_Member.getMemberPrimaryChannel(member));
            }
            parentMenu = menu2.FieloEE__Menu__r.FieloEE__ExternalName__c;
        }else if(member != null){
            //HOME MENU: If the Member is logged (not Guest) and the Menu is NULL, set Home as current Menu
            correctMenuExternalName = FieloPRM_UTILS_MenuMethods.setHomeMenuCookie(member, FieloPRM_UTILS_Member.getMemberPrimaryChannel(member));
        }
        
        //Setting the correct Menu
        if(!String.isEmpty(correctMenuExternalName)){
            try{
                menu2 = Database.query(menuQuerySelectFrom + 'WHERE FieloEE__ExternalName__c = \'' + correctMenuExternalName + '\'  LIMIT 1');
                Apexpages.currentPage().getParameters().put('idMenu', menu2.Id);
                pageName = 'PageID_' + menu2.Id;
            }catch(Exception e){system.debug('Exception:' + e.getMessage());}
        }
        
        //PAGE REDIRECTION
        PageReference redirectURLPageReference = redirectPage();
        system.debug('redirectURLPageReference :' + redirectURLPageReference);
        if(redirectURLPageReference != null){
            redirectURL = redirectURLPageReference.getURL();
        }
        
        if(redirectURL == null){
            //Fielo controller (Contains the CMS Sections and Components)
            loadSections();
            layout = FieloEE.ProgramUtil.getFrontEndLayout();
            
            //Variable used for the components "CRUD" and "List View" iframes
            String parameters = '';
            for(String param : Apexpages.currentPage().getParameters().keySet()){
                parameters += '&'+param+'='+Apexpages.currentPage().getParameters().get(param);
            }
            
            //Notification Center
            if(System.Label.CLMAR16PRM24.contains(menu2.FieloEE__ExternalName__c)){
                showNotification = false;
            }
            bgAttid = menu2.FieloEE__AttachmentId__c;
            
            //parentMenu External name
            if(menu2 != null && menu2.FieloEE__Menu__c != null){
                parentMenu = menu2.FieloEE__Menu__r.FieloEE__ExternalName__c;
            }
            
            //Akram
            //Setting "memberWrapper"
            if(member != null){
        
                partnersName = member.FieloEE__FirstName__c + ' ' + member.FieloEE__LastName__c;
                if(partnersName.length() > 20){
                    partnersName = member.FieloEE__FirstName__c;
                    if(partnersName.length() > 20){
                        partnersName = member.FieloEE__FirstName__c.abbreviate(20);
                    }
                }
   
                displayPoints = member.F_PRM_MemberFeatures__c != null && member.F_PRM_MemberFeatures__c.contains(label.CLPRMMAR16009);
                
                memberWrapper = new FieloPRM_UTILS_Wrappers.MemberWrapper(member);
                if(member.F_PRM_PrimaryChannel__c != '' && member.F_PRM_PrimaryChannel__c != null){
                    List<CountryChannels__c> countryChannels = [SELECT Id, F_PRM_Menu__c, F_PRM_Menu__r.FieloEE__ExternalName__c FROM CountryChannels__c WHERE SubChannelCode__c =: member.F_PRM_PrimaryChannel__c AND F_PRM_CountryCode__c =: member.F_Country__r.CountryCode__c LIMIT 1];
                    if(!countryChannels.isEmpty()){
                        List<FieloEE__Category__c> category = [SELECT Id, Name FROM FieloEE__Category__c WHERE F_PRM_Menu__c =: countryChannels[0].F_PRM_Menu__c];
                        if(!category.isEmpty()){
                            primaryChannelCategoryId = category[0].Id;
                            primaryChannelCategoryName = category[0].Name;
                        }
                    }
                }
            }else{
                memberWrapper = new FieloPRM_UTILS_Wrappers.MemberWrapper();
            }

            //Setting "contactWrapper"
            List<Contact> listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c, PRMEmail__c, PRMWorkPhone__c, PRMMobilePhone__c, PRMJobTitle__c, Account.PRMStreet__c, Account.Name FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1];
            if(!listContact.isEmpty()){
                contactWrapper = new FieloPRM_UTILS_Wrappers.ContactWrapper(listContact[0].PRMFirstName__c + ' ' + listContact[0].PRMLastName__c, listContact[0].PRMEmail__c, listContact[0].PRMMobilePhone__c, listContact[0].PRMWorkPhone__c, listContact[0].PRMJobTitle__c, listContact[0].Account.PRMStreet__c, lang, listContact[0].Account.Name);
                //Welcome Message
                welcomeMessage =  label.FieloEE.Welcome + ' ' + contactWrapper.name;
            }
            else{
                contactWrapper = new FieloPRM_UTILS_Wrappers.ContactWrapper();
                cookie language = ApexPages.currentPage().getCookies().get('language');
                if(String.isNotBlank(URLParams.get('language')))
                    contactWrapper.conLanguage = URLParams.get('language').toLowerCase();
                if(language != null) 
                     contactWrapper.conLanguage = language.getvalue().toLowerCase();
            }
            
            //Setting "isMobile" & "isTablet"
            String userAgent = System.currentPageReference().getHeaders().get('User-Agent');
            String device = System.currentPageReference().getParameters().get('device');
            if (!Test.isRunningTest()) {
                isMobile = (userAgent.contains('iPhone') || device == 'mobile');
                isTablet = (userAgent.contains('iPad') || (userAgent.contains('AppleWebKit') && device != null && device != 'mobile'));
            }
        }
    }
    
    //Redirection method
    public PageReference redirectPage(){
        system.debug('menu2:' + menu2);
        system.debug('member:' + member);
        //Parameter "isRedirect" to avoid any recursion
        if(ApexPages.currentPage().getParameters().containsKey('isRedirect') && ApexPages.currentPage().getParameters().get('isRedirect') == 'true'){return null;}
        
        for(String s: URLParams.keyset()){
            if(s == 'language'){
                if(String.isNotBlank(URLParams.get(s))){
                    languageCode = URLParams.get(s) == 'id' ? 'in' : URLParams.get(s);
                    loginParameters += '&Language='+languageCode;
                }
                else{
                    languageCode = cLanguage != null ? (cLanguage.getValue() == 'id' ? 'in' : cLanguage.getValue() ): 'en_US';
                    loginParameters += '&language='+languageCode;
                }
            }
            else if(s == 'country'){
                if(String.isNotBlank(URLParams.get(s))){
                    cntryCode = URLParams.get(s);
                    loginParameters += '&Country='+cntryCode;
                }
                else{
                    cntryCode = Country != null ? Country.getValue(): null;
                    loginParameters += '&country='+cntryCode;
                }
            }
            else{
                loginParameters += '&'+s+'='+URLParams.get(s);
            }
        }
        
        if(String.isNotBlank(cntryCode)){
            LanguageCntryCount  = AP_PRMUtils.updateLanguageAndCountry(languageCode,cntryCode,cookieLang);
            Cookie countryCookie= new Cookie('country', cntryCode,null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{countryCookie});
            Cookie languageCookie= new Cookie('language', LanguageCntryCount.languageCountrymap.get(cntryCode),null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{languageCookie});
        }
        
        
        if(String.isBlank(gotoParam) && String.isBlank(sunQueryStringParam) ){
            system.debug('gotoParam:' + gotoParam);
            system.debug('sunQueryStringParam:' + sunQueryStringParam);
            system.debug('>>>><<<<currentURL:' + currentURL);
            pageReference pageRef;
            if(currentURL.contains('/login')){
                String baseURL;
                if(String.isBlank(site.getBaseCustomUrl()))
                    baseURL = site.getBaseUrl();
                else
                    baseURL = site.getBaseCustomUrl();
                Cookie POMPCookie = ApexPages.currentPage().getCookies().get('app');
                if(POMPCookie != null){
                    Cookie appCookie = new Cookie('app', '', null, 0, false);
                    ApexPages.currentPage().setCookies(new Cookie[]{appCookie});
                }
                System.debug('>>>>loginparameters:'+loginParameters);
                 if(String.isNotBlank(referredFromUrl) && !referredFromUrl.containsIgnoreCase('/menu/registration') && referredFromUrl.startsWith(baseURL) && !referredFromUrl.containsIgnoreCase('/apex/vfp_partnerslogout?newemail') && !referredFromUrl.containsIgnoreCase('/Menu/Logout?type=pr')){
                     if(referredFromUrl.containsIgnoreCase('IDToken1')){
                        pageReference referredURL = new PageReference(referredFromUrl);
                        referredURL.getParameters().remove('IDToken1');
                        referredFromUrl = referredURL.getURL();
                    }
                    pageRef = new PageReference(Label.CLAPR15PRM114 + '&RelayState='+referredFromUrl + loginParameters);
                }
                else
                    pageRef = new PageReference(Label.CLAPR15PRM114 + loginParameters);
                    
                pageRef.setRedirect(true);
                return pageRef;
            }
            else if(currentURL.contains('/pomplogin')){
                Cookie appCookie = new Cookie('app', 'POMP', null, -1, false);
                ApexPages.currentPage().setCookies(new Cookie[]{appCookie});
                System.debug('>>>>starturl:'+URLParams.get('StartURL'));
                if(string.isNotBlank(URLParams.get('StartURL')))
                    pageRef = new PageReference(Label.CLMAR16PRM013 + loginParameters +'&RelayState=/pomp/apex/VFP_NewPompHomePage?PompStartUrl='+URLParams.get('StartURL'));
                else
                    pageRef = new PageReference(Label.CLMAR16PRM013 + loginParameters +'&RelayState=/pomp/apex/VFP_NewPompHomePage');
                pageRef.setRedirect(true);
                return pageRef;
            }
        }
        
        if(member != null){
            if(menu2 != null){
                if(menu2.FieloEE__ExternalName__c != 'ContinueRegistration' && menu2.FieloEE__ExternalName__c != 'RegistrationCompletion'){
                    //MAINTENANCE Redirection: If the Menu is "Template" or "Draft" and it is not a Preview an error is showed
                    if((menu2.F_PRM_Type__c == 'Template' || menu2.F_PRM_Status__c == 'Draft') && ApexPages.currentPage().getParameters().get('isPreview') != '1'){
                        return new PageReference('/VFP_PRMMaintenancePage?isRedirect=true');
                    }
                }
                //HOME Redirection
                if(menu2.FieloEE__visibility__c == 'Public' || (menu2.FieloEE__visibility__c == 'Hidden' && menu2.F_PRM_AvailableGuestUsers__c == true)){
                    return new PageReference('/partners?isRedirect=true');
                }
            }
        }else{
            //LOGIN Redirection
            if((menu2 == null || (menu2.FieloEE__visibility__c != 'Public' && menu2.FieloEE__visibility__c != 'Both' && menu2.F_PRM_AvailableGuestUsers__c != true)) && String.isBlank(gotoParam) && String.isBlank(sunQueryStringParam)){
                system.debug('inside the else block:' + menu2);
                return new PageReference('/partners/Menu/Login');
            }
        }
        return null;
    }
    
    private String getBrowser(){
        if(userAgent.contains('Safari')){
            if(userAgent.contains('Chrome')){
                return 'Chrome';
            }else{
                return 'Safari';
            }
        }else if(userAgent.contains('Firefox')){
            return 'Firefox';
        }else if(userAgent.contains('MSIE')){
            return 'MSIE';
        }
        return 'Other';
    }
    
    private String getOS(){
        if(userAgent.contains('Mac')){
            return 'Mac';
        }else if(userAgent.contains('Windows')){
            return 'Windows';
        }else if(userAgent.contains('Linux')){
            return 'Linux';
        }
        return 'Other';
    }
    
    private void loadSections(){
        componentsSection = new Map<Id,List<FieloEE__Section__c>>();
        FieloEE.PageController.componentsMap = new Map<Id,FieloEE__Component__c>();
        
        //queries for menu's sections list
        sections = [SELECT Id, Name, F_PRM_Asynchronous__c, FieloEE__Type__c, FieloEE__Order__c, FieloEE__Menu__c, FieloEE__CSSClasses__c FROM FieloEE__Section__c WHERE FieloEE__Menu__c =: menu2.Id ORDER BY FieloEE__Order__c ASC NULLS FIRST,CreatedDate ASC];
        
        String fields = 'Id, RecordType.DeveloperName, FieloEE__Menu__r.FieloEE__Menu__c, FieloEE__Account__c, FieloEE__BannerPlacement__c, Parameter_TextArea_1__c, FieloEE__Category__c, FieloCH__Challenge__c, FieloCH__ChallengeType__c, FieloCH__ChallengeViewDisplay__c, F_classesCSS__c, F_PRM_ComponentFilter__c, FieloEE__Content__c, FieloEE__NewsCategory__c, FieloEE__NewsType__c, F_PRM_Country__c, F_PRM_CreateEnrollmentEvent__c, ';
        fields += 'FieloCRUD__CssClassName__c, F_CRUDObjectSettings__c, FieloEE__CCS__c, FieloEE__CSSClasses__c, FieloEE__Delay__c, F_PRM_PublishingDescription__c, FieloEE__Desktop__c, FieloEE__Display__c, FieloEE__Filter__c, Parameter_TextArea_3__c, F_PRM_DocType__c, FieloEE__FieldSet__c, FieloEE__FieldSet2__c, FieloEE__FilterBySegment__c, FieloEE__FilterFormula__c, Form_Url__c, F_PRM_FrontEndStyle__c, ';
        fields += 'FieloEE__Height__c, FieloEE__ContentHTML__c, F_PRM_PublishingInclude__c, FieloEE__Qant__c, F_PRM_JoinButtonLabel__c, F_PRM_JoinButtonLabel_CS__c, F_PRM_JoinButtonLabel_DA__c, F_PRM_JoinButtonLabel_DE__c, F_PRM_JoinButtonLabel_EL__c, F_PRM_JoinButtonLabel_EN__c, F_PRM_JoinButtonLabel_ES__c, F_PRM_JoinButtonLabel_FR__c, F_PRM_JoinButtonLabel_HU__c, F_PRM_JoinButtonLabel_IN__c, F_PRM_JoinButtonLabel_IT__c, ';
        fields += 'F_PRM_JoinButtonLabel_JA__c, F_PRM_JoinButtonLabel_KO__c, F_PRM_JoinButtonLabel_NL__c, F_PRM_JoinButtonLabel_PL__c, F_PRM_JoinButtonLabel_PT__c, F_PRM_JoinButtonLabel_RU__c, F_PRM_JoinButtonLabel_SV__c, F_PRM_JoinButtonLabel_TH__c, F_PRM_JoinButtonLabel_TR__c, F_PRM_JoinButtonLabel_ZH__c, FieloEE__Layout__c, FieloCRUD__ListView__c, FieloEE__Menu__c, FieloEE__MenuRedirect__c, TECH_MigrationUniqueID__c, ';
        fields += 'FieloEE__Mobile__c, FieloEE__Qcolumns__c, FieloEE__Qrows__c, FieloCRUD__ObjectSettings__c, FieloEE__Order__c, FieloEE__OrderBy__c, FieloCRUD__PageLayout__c, FieloEE__PageName__c, FieloEE__Paging__c, F_PRM_PartnerJourneyBehaviour__c, F_PRM_PublishingTimestamp__c, F_PRM_PublishingPublisher__c, F_PRM_PublishingPublisherCountry__c, PublishingCountry__c, PublishingDescription__c, PublishingGoldenId__c, ';
        fields += 'PublishingIsUpdated__c, PublishingOdasevaId__c, F_PRM_PublishingParentStatus__c, PublishingPublisher__c, PublishingSrcUrl__c, PublishingStatus__c, F_PRM_PublishingStatusImage__c, PublishingStatusImage__c, PublishingTgtUrl__c, PublishingTimestamp__c, FieloEE__Section__c, Parameter_Checkbox_1__c, Parameter_2__c, FieloSVY__Survey__c, FieloEE__Tablet__c, FieloEE__Tag__c, FieloEE__Text__c, Text_CS__c, Text_DA__c, ';
        fields += 'Text_DE__c, Text_EL__c, Text_EN__c, Text_ES__c, Text_FR__c, Text_HU__c, Text_IN__c, Text_IT__c, Text_JA__c, Text_KO__c, Text_NL__c, Text_PL__c, Text_PT__c, Text_RU__c, Text_SV__c, Text_TH__c, Text_TR__c, Text_ZH__c, FieloEE__OptionalName__c, Parameter_1__c, OptionalName_CS__c, OptionalName_DA__c, OptionalName_DE__c, OptionalName_EL__c, OptionalName_EN__c, OptionalName_ES__c, OptionalName_FR__c, OptionalName_HU__c, ';
        fields += 'OptionalName_IN__c, OptionalName_IT__c, OptionalName_JA__c, OptionalName_KO__c, OptionalName_NL__c, OptionalName_PL__c, OptionalName_PT__c, OptionalName_RU__c, OptionalName_SV__c, OptionalName_TH__c, OptionalName_TR__c, OptionalName_ZH__c, FieloEE__TransactionType__c, FieloEE__Modal__c, FieloEE__Width__c, FieloEE__Filter2__c, FieloEE__Type__c';
        
        for(FieloEE__Section__c subSection : Database.query('SELECT Id, FieloEE__Type__c, FieloEE__Order__c, Name, FieloEE__Menu__c, FieloEE__Parent__c, FieloEE__CSSClasses__c, (SELECT ' + fields + ' FROM FieloEE__Components__r ORDER BY FieloEE__Order__c asc) FROM FieloEE__Section__c WHERE FieloEE__Parent__c IN : sections ORDER BY FieloEE__Order__c ASC')){
            if(!componentsSection.containsKey(subSection.FieloEE__Parent__c)){
                componentsSection.put(subSection.FieloEE__Parent__c, new List<FieloEE__Section__c>{subSection});
            }else{
                componentsSection.get(subSection.FieloEE__Parent__c).add(subSection);
            }
            FieloEE.PageController.componentsMap.putAll(subSection.FieloEE__Components__r);
        }
    }
    
    //Method to update the Login Date field on the Member
    @RemoteAction
    global static Boolean updateUserLoginDate(Id memberId){
        FieloEE__Member__c member = [SELECT Id, F_PRM_LastLoginDateTime__c FROM FieloEE__Member__c WHERE Id =: memberId];
        User loggedUser = [SELECT Id, LastLoginDate  FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        FieloPRM_MemberTrigger.isAlreadyRun = true;
        member.F_PRM_LastLoginDateTime__c = loggedUser.LastLoginDate;
        try{
            update member;
            return true;
        }catch(exception e){}
        return false;
    }
    
    //Contact Us: Method to send an email
    @RemoteAction
    global static void sendEmailToCase(String description,String currentPage) {
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        mail.setToAddresses(new String[] {'contactus_webinquiries@schneider-electric.com'});
        mail.setSubject('Product Technical Inquiry');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        
        List<Contact> listContact = new List<Contact>();
        if(Test.isRunningTest()){
            listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c, PRMEmail__c, PRMWorkPhone__c, PRMMobilePhone__c, PRMJobTitle__c, Account.PRMStreet__c, Account.Name FROM Contact LIMIT 1];
        }else{
            listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c, PRMEmail__c, PRMWorkPhone__c, PRMMobilePhone__c, PRMJobTitle__c, Account.PRMStreet__c, Account.Name FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1];
        }
        
        if(!listContact.isEmpty()){
            String phone = listContact[0].PRMWorkPhone__c!=null?listContact[0].PRMWorkPhone__c:(listContact[0].PRMMobilePhone__c!=null?listContact[0].PRMMobilePhone__c:'');
            String firstName = listContact[0].PRMFirstName__c!=null?listContact[0].PRMFirstName__c:'';
            String email = listContact[0].PRMEmail__c!=null?listContact[0].PRMEmail__c:'';
            String lastName = listContact[0].PRMLastName__c!=null?listContact[0].PRMLastName__c:'';
            String accountName = listContact[0].Account.Name!=null?listContact[0].Account.Name:'';
            String accountAddress = listContact[0].Account.PRMStreet__c!=null?listContact[0].Account.PRMStreet__c:'';
            mail.setReplyTo(email);
            mail.setHtmlBody('<html><body>First Name = '+firstName+'<br/>Last Name = '+lastName+'<br/>Company Name = '+accountName+'<br/>Company Address = '+accountAddress+'<br/>Email = '+email+'<br/>Phone ='+phone+'<br/>Provide the webpage link ='+currentPage+'<br/>MESSAGE = '+description+'</body></html>');
            if(!Test.isRunningTest()){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    }
    
}