/*
    Author          : Jules Faligot (ACN) 
    Date Created    : 18/08/2011
    Description     : Class used by VFP38_AccountOrderConnector.
*/

public class VFC38_AccOrderController { 

    public AccountOrder AccountImplementation{get;set;}
    public Account Acc = new Account();
    public boolean DisplayComponent{get;set;}
            
    public VFC38_AccOrderController(ApexPages.StandardController controller){
        Acc= (Account)controller.getRecord();         
        AccountImplementation = new AccountOrder ();        
    }   
     
    Public void testRights(){
        DisplayComponent = Utils_TestAccessRights.TestRightsOnAccount(Acc);
    }
    
    // Implementation of the Order implementation for the Account Order Connector
    public class AccountOrder implements Utils_OrderMethods
    {
        public OPP_OrderLink__c Order{get;set;}
        
        public void Init(VCC07_OrderConnectorController PageController)
        { 
            PageController.DisplayPage= true;
            PageController.Sales = true;  
            PageController.Tab2ActionLabel = '';                      
            PageController.columns = getColumns();
            
            List<Account> AccountList = [SELECT ID FROM Account WHERE ID=:PageController.StandardObjectID LIMIT 1];
            if(AccountList.size()==1)
            {
                PageController.RelatedRecord = AccountList[0];
            }
    
            
            PageController.Action2Label=null;
            Account Acc = (Account) PageController.RelatedRecord;
            if(Acc != null)
                PageController.DateCapturer.Account__c = Acc.ID;
            PageController.ActionLabel = Label.CL00493;
            PageController.DisableAllSelectAction = true;
            PageController.AccountReadOnly=true;
            
            PageController.BackOfficeRefresher();  // Populate the back office list    
            
            System.debug('##### BACK OFFICE: ' + PageController.BackOffice );                        
            
            PageController.TabInFocus = 'SearchOrder';
            PageController.SearchOrder();
            if(PageController.fetchedRecords.size()>0)
            {
                PageController.SelectedOrder = (OPP_OrderLink__c)PageController.fetchedRecords[0];
            }
            PageController.existingOrders = new Set<Object>();
                                                                  
        }
          
        public pagereference Cancel(VCC07_OrderConnectorController PageController)
        {
            Account Acc= (Account)PageController.RelatedRecord;            
            PageReference AccountPage =  new ApexPages.StandardController(Acc).cancel();            
            return AccountPage ;
        }
     
        // No Action implemented 
        public PageReference PerformAction(VCC07_OrderConnectorController PageController){ return null;}   
    
        public List<SelectOption> getColumns()
        {
               List<SelectOption> columns = new List<SelectOption>();                                            
                columns.add(new SelectOption('SEOrderReference__c', sObjectType.OPP_OrderLink__c.fields.SEOrderReference__c.getLabel()));
                columns.add(new SelectOption('PONumber__c', sObjectType.OPP_OrderLink__c.fields.PONumber__c.getLabel()));
                columns.add(new SelectOption('OrderCreationDate__c', sObjectType.OPP_OrderLink__c.fields.OrderCreationDate__c.getLabel()));
                columns.add(new SelectOption('Amount__c', sObjectType.OPP_OrderLink__c.fields.Amount__c.getLabel()));
                columns.add(new SelectOption('OrderAccountName__c', sObjectType.OPP_OrderLink__c.fields.OrderAccountName__c.getLabel()));                                                         
                columns.add(new SelectOption('OrderAccountLegacyNumber__c', sObjectType.OPP_OrderLink__c.fields.OrderAccountLegacyNumber__c.getLabel()));        
                columns.add(new SelectOption('OrderPrimaryContact__c', sObjectType.OPP_OrderLink__c.fields.OrderPrimaryContact__c.getLabel()));
                columns.add(new SelectOption('OrderPlacedThrough__c', sObjectType.OPP_OrderLink__c.fields.OrderPlacedThrough__c.getLabel())); 
                columns.add(new SelectOption('OrderEnteredBy__c', sObjectType.OPP_OrderLink__c.fields.OrderEnteredBy__c.getLabel())); 
                columns.add(new SelectOption('Status__c',sObjectType.OPP_OrderLink__c.fields.Status__c.getLabel()));  
                return columns;    
        }
    }                         
}