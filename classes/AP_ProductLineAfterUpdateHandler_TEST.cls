/*
    Author          : Bhuvana S
    Description     : Test class for AP_ProductLineAfterUpdateHandler
*/

@isTest
private class AP_ProductLineAfterUpdateHandler_TEST 
{
    static testMethod void testProdLine()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;
        //Inserts Opportunity Lines
        for(Integer i=0;i<102;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);
        
        //Checks for the Parent Line issue
        OPP_ProductLine__c pl2 = Utils_TestMethods.createProductLine(opp2.Id);
        pl2.Quantity__c = 10;
        pl2.ParentLine__c = lsr[0].getId();
        insert pl2;
        
        OPP_ProductLine__c pl3 = [select Id,ParentLine__c from OPP_ProductLine__c where Id=: lsr[0].getId()];
        pl3.ParentLine__c = lsr[1].getId();
        try
        {
            update pl3;
        }
        catch(Exception e)
        {
            
        }
    }
    
    static testMethod void testProdLine1()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        List<CS_ProductLineBU__c> plBus= new List<CS_ProductLineBU__c>();
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        //Inserts Custom Setting records
        CS_ProductLineBU__c plBU = new CS_ProductLineBU__c();
        plBU.Name =      'BUILDING MANAGEMENT';
        plBU.ProductBU__c = 'BD';
        plBus.add(plBU);

        CS_ProductLineBU__c plBU1 = new CS_ProductLineBU__c();
        plBU1.Name =      'INFOR TECHNO. BUSINESS';
        plBU1.ProductBU__c = 'IT';
        plBus.add(plBU1);

        CS_ProductLineBU__c plBU2 = new CS_ProductLineBU__c();
        plBU2.Name =      'INFRASTRUCTURE';
        plBU2.ProductBU__c = 'EN';
        plBus.add(plBU2);
        insert plBUs;
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;
        //Inserts Opportunity Line
        OPP_ProductLine__c pl2 = Utils_TestMethods.createProductLine(opp2.Id);

        pl2.ProductBU__c = 'BUILDING MANAGEMENT';
        pl2.ProductLine__c = 'BDBMS - BUILDING SYSTEM LEVEL2';
        prodLines.add(pl2);
        
        OPP_ProductLine__c pl3 = Utils_TestMethods.createProductLine(opp2.Id);

        pl3.ProductBU__c = 'INFOR TECHNO. BUSINESS';
        pl3.ProductLine__c = 'ITSL3 - ITB LEVEL 3 SOLUTIONS';
        prodLines.add(pl3);
        
        OPP_ProductLine__c pl5 = Utils_TestMethods.createProductLine(opp2.Id);

        pl5.ProductBU__c = 'INFOR TECHNO. BUSINESS';
        pl5.ProductLine__c = 'ITSL2 - ITB Enterp Syst Level2';
        prodLines.add(pl5);

        OPP_ProductLine__c pl4 = Utils_TestMethods.createProductLine(opp2.Id);
        pl4.ProductBU__c = 'BUILDING MANAGEMENT';
        pl4.ProductLine__c = 'BDSL3 - BUILDING SYSTEM LEVEL3';
        prodLines.add(pl4);
        
        Database.saveResult[] sv = Database.insert(prodLines);
        //Updates Product Line Record
        if(sv!=null && (!(sv.isEmpty())) && sv[3]!=null && sv[3].Id!=null)
        {
            OPP_ProductLine__c pl7 = [select Id,ProductBU__c, ProductLine__c from OPP_ProductLine__c where Id=:sv[3].Id];
            pl7.Quantity__c = 23;
            pl7.ProductBU__c = 'INFRASTRUCTURE';
            pl7.ProductLine__c = 'TEST';
            update pl7;
        }
    }
}