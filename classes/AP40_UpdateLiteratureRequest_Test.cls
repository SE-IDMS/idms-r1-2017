@isTest
private class AP40_UpdateLiteratureRequest_Test {

    static testMethod void UpdateLiteratureRequestTest() {
      
    Account Acc = Utils_TestMethods.createAccount();
    insert Acc;
     
    Contact Con = Utils_TestMethods.createContact(acc.id,'New Test Contact');
    Insert Con;
    
    Case Cse = Utils_TestMethods.createCase(acc.Id,Con.Id,'New');
    Insert Cse;
    
    Cse.CustomerRequest__c='Test Request';
    Database.update(Cse);
    
    LiteratureRequest__c ltr = Utils_TestMethods.createLiteratureRequest(Acc.Id,Con.Id,Cse.Id);
    Insert ltr;
     
    Delete ltr;        
    }
}