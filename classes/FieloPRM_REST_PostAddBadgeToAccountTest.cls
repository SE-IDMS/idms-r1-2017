@isTest

private class FieloPRM_REST_PostAddBadgeToAccountTest{

    static testMethod void unitTest(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            
            FieloPRM_UTILS_BadgeAccount.addBadgeToAccount(null,null);

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            /*FieloPRM_ValidBadgeTypes__c validBadgeAux = new FieloPRM_ValidBadgeTypes__c(FieloPRM_ApiExtId__c = '1', FieloPRM_Type__c = 'Program Level', name = 'test' );
            insert validBadgeAux;*/

            Account acc = new Account();
            acc.Name = 'test acc';
            acc.PRMUIMSId__c = 'acctest';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'PostAddBadgeLastName';
            member.FieloEE__FirstName__c = 'PostAddBadgeFirstName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
          
            insert member;
            /*
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_BadgeAPIName__c = 'asd'; 
            insert badge;*/
            
            FieloPRM_ValidBadgeTypes__c cusSetting = new FieloPRM_ValidBadgeTypes__c(); 
            cusSetting.FieloPRM_Type__c = 'Program Level';
            cusSetting.FieloPRM_ApiExtId__c = 'asd' ;
            cusSetting.name = 'test name';
            insert cusSetting;

            FieloPRM_ValidBadgeTypes__c cusSetting2 = new FieloPRM_ValidBadgeTypes__c(); 
            cusSetting2.FieloPRM_Type__c = 'Program Level';
            cusSetting2.FieloPRM_ApiExtId__c = 'asd2' ;
            cusSetting2.name = 'test name2';
            insert cusSetting2;

            /*
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member__c = member.id;
            badMem.FieloEE__Badge__c = badge.id;
            insert badMem;

            FieloPRM_BadgeAccount__c badgeAcc = new FieloPRM_BadgeAccount__c ();  
            badgeAcc.F_PRM_Badge__c = badge.id;
            badgeAcc.F_PRM_Account__c = acc.id;
            
            insert badgeAcc;*/
    
            
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1; 
                     
            list<String> listIds = new list<String>();
            listIds.add(con1.id);
        
            FieloPRM_REST_PostAddBadgeToAccount rest = new FieloPRM_REST_PostAddBadgeToAccount();
            Map<String ,List<String>> accToBadges = new Map<String ,List<String>>();
            List<String> aux = new List<String>();
            //aux.add(badge.F_PRM_BadgeAPIName__c);
            aux.add('asd');

            //validBadgeAux.add(acc.PRMUIMSId__c );
            //validBadgeAux.add('acctest' );
            
            //accToBadges.put(acc.PRMUIMSId__c , aux);
            accToBadges.put('acctest' , aux);
            accToBadges.put('type' , new List<string>{'1','2'});
            FieloPRM_REST_PostAddBadgeToAccount.postAddBadgeToAccount('Program Level', accToBadges);
            System.debug('TEST line 77');
            FieloPRM_REST_PostAddBadgeToAccount.postAddBadgeToAccount('incorrect type', accToBadges);

        }
    
    }
    
}