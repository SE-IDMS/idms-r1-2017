@RestResource(urlMapping='/prmregistration/*')

global without sharing class AP_PRMRegistrationService{
    
    @HttpGet
    global static AP_PRMRegistrationInformation getRegistrationInfo(){
        RestRequest req = RestContext.request;
        if (!Test.isRunningTest())
            req.addHeader('Content-Type','application/json');

        AP_PRMRegistrationInformation regInfo = new AP_PRMRegistrationInformation();
        return regInfo;
    }
    
    @HttpPost
    global static UserRegResult createRegistration(AP_PRMRegistrationInformation regInfo){
        UserRegResult result = new UserRegResult();
        result.Success = false;

        try {
            System.debug('*** Reg Input Received:' + regInfo);

            result = ValidateData (regInfo);
            system.debug('validateResult:' + result);
            if (result.Success){
                AP_UserRegModel regModel = PrepareData (regInfo);
                System.debug('regModel:' + regModel);
                UIMSCompany__c regTracking = ProcessData (regModel);

                result.UserFederationId = regModel.FederationId;
                result.AccountId = regModel.companyFederatedId;
                result.UserRegistrationTrackingId = regTracking.Id;
                result.Success = true;

                system.debug('processResult:' + result);
            }
        } catch (Exception e) {
            result.ErrorCodes.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_UNKNOWN.name());
            result.ErrorMsg = e.getMessage();
            System.debug('Exception occured:' + e.getMessage());
        }

        system.debug('result:' + result);
        return result;

    }
    
    private static UserRegResult ValidateData (AP_PRMRegistrationInformation regInfo){
        UserRegResult result = new UserRegResult ();
        List<String> errorMsg = new List<String>();
        List<Country__c> cont;
        List<CountryChannels__c> chnls;

        Schema.DescribeFieldResult jobTitleResult = Contact.PRMJobTitle__c.getDescribe();
        Schema.DescribeFieldResult jobFuncResult = Contact.PRMJobFunc__c.getDescribe();
        List<Schema.PicklistEntry> jobTitle= jobTitleResult.getPicklistValues();
        List<Schema.PicklistEntry> jobFunc = jobFuncResult.getPicklistValues();

        Set<String> jobTitleCodes = new Set<String> ();
        Set<String> jobFuncCodes = new Set<String> ();
        Set<String> supportedLanguages = new Set<String> ();

        result.Success = true;

        for (Schema.PicklistEntry f : jobTitle)
            jobTitleCodes.add(f.getValue());
        for (Schema.PicklistEntry f : jobFunc)
            jobFuncCodes.add(f.getValue());
        
        //fedId,firstname,lastname,bt,af,country mandatory
        if(String.isBlank(regInfo.federationId) || String.isBlank(regInfo.BusinessType) || String.isBlank(regInfo.areaOfFocus) || String.isBlank(regInfo.firstName) || String.isBlank(regInfo.lastName) || String.isBlank(regInfo.countryCode) || String.isBlank(regInfo.userLanguage))
            errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_REQFIELDS_BLANK.name());
        
        //email or phone is mandatory
        if(String.isBlank(regInfo.phoneNumber) && String.isBlank(regInfo.email))
            errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_EMAIL_PHONE_BLANK.name());
        
        // Valid Country
        List<PRMCountry__c> prmCluster = new List<PRMCountry__c>([SELECT Id, Country__c,SupportedLanguage1__c,SupportedLanguage2__c,SupportedLanguage3__c,MemberCountry1__c,MemberCountry2__c,MemberCountry3__c,MemberCountry4__c,MemberCountry5__c,
                                (SELECT Id, Name, Channel__c, ChannelCode__c, SubChannel__c, SubChannelCode__c FROM CountryChannels__r WHERE ChannelCode__c = :regInfo.BusinessType AND SubChannelCode__c = :regInfo.areaOfFocus AND Active__c = TRUE)                
                                FROM PRMCountry__c WHERE CountryPortalEnabled__c=True 
                                AND (Country__r.CountryCode__c = :regInfo.countryCode OR MemberCountry1__r.CountryCode__c = :regInfo.countryCode OR MemberCountry2__r.CountryCode__c = :regInfo.countryCode OR MemberCountry3__r.CountryCode__c = :regInfo.countryCode OR MemberCountry4__r.CountryCode__c = :regInfo.countryCode OR MemberCountry5__r.CountryCode__c = :regInfo.countryCode)]);

        if(prmCluster.size() > 0) {
            supportedLanguages.add(prmCluster[0].SupportedLanguage1__c);
            supportedLanguages.add(prmCluster[0].SupportedLanguage2__c);
            supportedLanguages.add(prmCluster[0].SupportedLanguage3__c);
            
            if(supportedLanguages.contains(regInfo.userLanguage))
                regInfo.bFOLanguageLocaleKey = regInfo.userLanguage;
            else if(supportedLanguages.contains(regInfo.userLanguage + '_' + regInfo.countryCode))
                regInfo.bFOLanguageLocaleKey = regInfo.userLanguage + '_' + regInfo.countryCode;
            else
                regInfo.bFOLanguageLocaleKey = (new List<String>(supportedLanguages))[0];

            chnls = prmCluster[0].CountryChannels__r;
        }
        else
            errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_COUNTRY_NOTVALID.name());
        
        // Valid State
        if(String.isNotBlank(regInfo.stateProvinceCode) && !cont.isEmpty()) {
            List<StateProvince__c> state = new List<StateProvince__c>([SELECT Id, CountryCode__c, StateProvinceCode__c, Name FROM StateProvince__c WHERE Country__r.CountryCode__c = :regInfo.countryCode AND StateProvinceCode__c = :regInfo.stateProvinceCode]);
            if(state.size() <= 0)
                errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_STATE_NOTVALID.name());
        }
        
        // Valid Channel
        if (chnls == null || (chnls != null && chnls.size() <= 0))
            errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_CHNL_NOTVALID.name());                
        
        //Valid JobTitle
        if(String.isNotBlank(regInfo.jobTitle) && !jobTitleCodes.contains(regInfo.jobTitle))
            errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_JOBTITLE_NOTVALID.name());
        
        //Valid JobFunction
        if(String.isNotBlank(regInfo.jobFunction) && !jobTitleCodes.contains(regInfo.jobFunction))
            errorMsg.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_JOBFUNC_NOTVALID.name());
            
        //Checking for error messages
        if(errorMsg.size() > 0){
             result.ErrorCodes = errorMsg;
             result.Success = false;
        }
        return result;
    }

    private static AP_UserRegModel PrepareData(AP_PRMRegistrationInformation regInfo){
        
        String regInfoString = JSON.serialize(regInfo);
        System.debug('regInfoString:' + regInfoString);
        AP_UserRegModel regModel = (AP_UserRegModel)JSON.deserialize(regInfoString, AP_UserRegModel.class);
        System.debug('regModel:'  +regModel);
        
        regModel.prmOrigin = 'Self Registration';
        regModel.prmOriginSource = 'SE Extranet';
        regModel.prmOriginSourceInfo = 'IMS';
        regModel.cTaxIdHidden = true;
        regModel.skipStep5 =false;
        regModel.skipStep6 = false;
        regModel.plShowInPartnerLocator = false;
        regModel.isAccountIdSet = false;
        regModel.termsAndConditions = false;

        if (regInfo.includeCompanyInfoInPublicSearch == null)
            regInfo.includeCompanyInfoInPublicSearch = false;
        if (regInfo.IAmSelfEmployed == null)
            regInfo.IAmSelfEmployed = false;
        if (regInfo.companyHeaderQuarters == null)
            regInfo.companyHeaderQuarters = false;

        regModel.emailRequired = regModel.confirmEmailRequired = regModel.lastNameRequired = regModel.firstNameRequired = regModel.businessTypeRequired = regModel.areaOfFocusRequired = true;
        
        
        regModel.doNotHaveCompany  = regInfo.IAmSelfEmployed;
        regModel.companyCountryIsoCode = regInfo.countryCode;
        regModel.companyStateProvinceIsoCode = regInfo.stateProvinceCode;
        
        if(String.isBlank(regInfo.email))
            regModel.email = regInfo.phoneNumber +'@'+ System.label.CLMAR13PRM05;
        else
            regModel.email = regInfo.email;
        
        if(String.isBlank(regInfo.companyFederatedId) && !regInfo.IAmSelfEmployed){
            regModel.skippedCompanyStep = true;
            regModel.isPrimaryContact = false;
        }
        else{
            regModel.skippedCompanyStep = false;
            List<Contact> primaryContact = new List<Contact> ([SELECT Id,PRMUIMSID__c, PRMPrimaryContact__c FROM Contact WHERE Account.PRMUIMSID__c = :regInfo.companyFederatedId AND PRMPrimaryContact__c = TRUE]);
            if(primaryContact.size() > 0)
                regModel.isPrimaryContact = false;
            else
                regModel.isPrimaryContact = true;
        }
       
        regModel.targetApplicationRequested = System.label.CLMAR16PRM011;
        regModel.userLanguage = regInfo.userLanguage;
        regModel.bFOLanguageLocaleKey = regInfo.bFOLanguageLocaleKey;
        System.debug('regModel:' + regModel);
        return regModel;
    }

    private static UIMSCompany__c ProcessData(AP_UserRegModel regModel) {
        UIMSCompany__c regTracking = AP_PRMUtils.CreateUserRegistrationTracking(regModel);
        
        // AP_PRMUtils.InitiateRegistrationProcessing(regModel, regTracking);

        return regTracking;
    }
}