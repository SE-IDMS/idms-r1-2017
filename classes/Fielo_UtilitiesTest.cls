/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date: 22.09.2014                                  *
* Tested Class: MICLASE                                     *
************************************************************/

@isTest
public with sharing class Fielo_UtilitiesTest{
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        menu.Name = 'test';
        menu.F_DeveloperName__c = 'test';
        insert menu;
          
        Fielo_Utilities controller = new Fielo_Utilities();
        Fielo_Utilities.getMenuReference(menu.F_DeveloperName__c);
        menu.FieloEE__CustomURL__c = 'superTest';
        update menu;
        Fielo_Utilities.getMenuReferenceById(menu.id);
        Fielo_Utilities.getMenuReference2(menu.F_DeveloperName__c);
    }
}