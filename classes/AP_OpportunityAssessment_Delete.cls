public class AP_OpportunityAssessment_Delete{

    public static void deleteOpportunityAssessmentResponse(List<OpportunityAssessment__c> oppAssList){
        //*********************************************************************************
        // Method Name : deleteOpportunityAssessmentResponse
        // Purpose : Delete all the OpportunityAssessmentResponse on Deletion of OpportunityAssessment
        // Created by : Hari Krishna Singara- Global Delivery Team
        // Date created : 21th July 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Sep - 12 Release
        ///*********************************************************************************/
         Set<id> oppAssidset=new Set<id>();
          if(oppAssList!= null && oppAssList.size()>0){
            for(OpportunityAssessment__c oass:oppAssList ){
                oppAssidset.add(oass.id);
            }
            if(oppAssidSet != null && oppAssidSet.size()>0){           
                List<OpportunityAssessmentResponse__c> ResList=[select id from OpportunityAssessmentResponse__c where OpportunityAssessment__c  in : oppAssidSet];
                if(ResList != null && ResList.size()>0){
                  
                    delete ResList;
                    
                }
                
            }
        }
    }

}