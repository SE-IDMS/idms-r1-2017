/*
    Author          : Deepak
    Date Created    : 07/01/2016
    Description     : HomePage Component for Application Freeze  class 
*/

global with sharing class VFC11_ApplicationFreezeHomePage {
    
    //public VFC11_ApplicationFreezeHomePage() { }

    @RemoteAction
    global static boolean checkApplicationFreeze() {
        boolean blnReturn=false;
        User objCurrentUser  = [SELECT Id,BypassApplicationFreeze__c,ProfileId FROM User Where id=:UserInfo.getUserId()];
        if(objCurrentUser!=null){
           if(objCurrentUser.BypassApplicationFreeze__c || objCurrentUser.ProfileId==System.Label.CL00110){
               blnReturn=true;
           } 
        }
        return blnReturn;
    }
}