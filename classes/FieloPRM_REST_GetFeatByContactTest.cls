@isTest 

private class FieloPRM_REST_GetFeatByContactTest{

    static testMethod void unitTest(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'ejemplolast';
            member.FieloEE__FirstName__c = 'ejemploFirst';
            member.FieloEE__Street__c = 'callealguna';
            
            insert member;
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level'; 
            insert badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;
            
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.F_PRM_FeatureCode__c = 'testt';
            insert feature;           
           
/*            FieloPRM_BadgeFeature__c ft = new FieloPRM_BadgeFeature__c();  
            ft.F_PRM_Feature__c = feature .id;
            ft.F_PRM_Badge__c = badge.id;
            insert ft;*/
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;
            
           
           /* Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            update con1;*/
            
            system.debug('con1 '+ con1);
            
         
            list<String> listIds = new list<String>();
            listIds.add(con1.PRMUIMSId__c);
        
            FieloPRM_REST_GetFeatByContact rest = new FieloPRM_REST_GetFeatByContact();
            FieloPRM_REST_GetFeatByContact.getFeaturesByContact(listIds );
        }

    }
    
}