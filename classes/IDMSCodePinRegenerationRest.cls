//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This is a global class exposing the Resent Pin code API. It will be called by IFW
//
//*******************************************************************************************

@RestResource(urlMapping='/ResendPinCode/*')
global with sharing class IDMSCodePinRegenerationRest {
    
    //PUT method called by API which will resend a pin code to user
    @HttpPut  
    global static IDMSResponseWrapperCodePIN doPut(String idmsUserId, String FederationId){
    
        IDMSResendPinCode resendPinCode     = new IDMSResendPinCode (); 
        IDMSResponseWrapperCodePIN response = resendPinCode.ResendCodePin(idmsUserId,FederationId);
        //Handle errors with status code 
        RestResponse res                    = RestContext.response;
        
        if(response.Message.startsWith('User ID and Federated Id doesn’t match')){         
            res.statuscode = Integer.valueof(Label.CLDEC16IDMS001);
        }
        if(response.Message.startsWith('Missing')){         
            res.statuscode = Integer.valueof(Label.CLDEC16IDMS001);
        }
        return response;
    }
}