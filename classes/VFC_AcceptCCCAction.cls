public class VFC_AcceptCCCAction{
   
    //Variables
    public String     retUrl;
    public Id         currentId {get;set;}           //Used if Detail View
    public List<CCCAction__c> cccActionList = new List<CCCAction__c>();  //Used if List   View
    public Boolean    errorFlag {get;set;}

   
    //Default Controller for Detail Views
    public VFC_AcceptCCCAction(ApexPages.StandardController controller){
        System.Debug('****VFC_AcceptCCCAction  Standard Controller  Started****');

        this.currentId = ApexPages.currentPage().getParameters().get('Id');
        this.errorFlag = false;

        System.Debug('****VFC_AcceptCCCAction  Standard Controller  Fininshed****');
    }

    //Change Owner to Active User for Detail Views
    public PageReference acceptCurrentCCCAction(){
    
    System.Debug('****VFC_AcceptCCCAction  acceptCurrentCCCAction  Started****');

    PageReference lRetUrl = null;
    Id currentUserId = UserInfo.getUserId();

    //Querying CCCAction record based on current CCCAction Id
    List<CCCAction__c> lcccActionList = [SELECT Id,Name, OwnerId,ActionOwner__c, CCCTeam__c FROM CCCAction__c WHERE Id = :this.currentId];
    if(lcccActionList.size() > 0){       
        lcccActionList.get(0).OwnerId = currentUserId;
        lcccActionList.get(0).ActionOwner__c = currentUserId;
        lcccActionList.get(0).CCCTeam__c = null;
        try{
            Database.update(lcccActionList.get(0));
            lRetUrl = redirectToCurrentCCCAction();
        }
        catch(System.DmlException Ex){
            this.errorFlag = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, Ex.getDmlMessage(0)));
        }
    }
    else{
        this.errorFlag = true;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, System.label.CLSEP12CCC09));
    }

    System.Debug('****VFC_AcceptCCCAction  acceptCurrentCCCAction  Fininshed****');
    return lRetUrl;
    }

    //Redirect to current Case
    public PageReference redirectToCurrentCCCAction(){
        System.Debug('****VFC_AcceptCCCAction  redirectToCurrentCCCAction  Started****');

        PageReference l_RetUrl = new PageReference('/' + this.currentId);

        System.Debug('****VFC_AcceptCCCAction  redirectToCurrentCCCAction  Fininshed****');
        return l_RetUrl;
    }
}