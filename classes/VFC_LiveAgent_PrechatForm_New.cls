global without sharing class VFC_LiveAgent_PrechatForm_New {
    
    private static String DEFAULT_FORM_KEY = 'Default';
    private static String ENDPOINT_KEY = 'endpoint';
    public static String USER_AGENT_KEY = 'USER-AGENT';
    
    //public String customerLanguageCode {get; private set;}
    public Boolean IsValidBrowser {get; private set;}
    public FormSettings settings {get; private set;}
    public String settingsJSON {get; private set;}
    
    public VFC_LiveAgent_PrechatForm_New() {
        
        String userAgent = ApexPages.currentPage().getHeaders().get(USER_AGENT_KEY);
        IsValidBrowser = true;
        
        String[] ieUserAgents = System.Label.CLSEP16CCCLA19.split(',');
        for (String ieUserAgent : ieUserAgents) {
            if (userAgent.containsIgnoreCase(ieUserAgent.trim())) {
                IsValidBrowser = false;
                break;
            }
        }
        
        if (IsValidBrowser)
        {
            IsValidBrowser = true;
            String endpoint = ApexPages.currentPage().getParameters().get(ENDPOINT_KEY);
        
            Pattern p = Pattern.compile('button_id=+(.*)&');
            Matcher pm = p.matcher(endpoint);
            if (pm.find()) {
                String buttonId = pm.group(1);
                settings = new FormSettings(buttonId, getAssignedForm(buttonId));
            } else {
                settings = new FormSettings(null, getDefaultForm());
            }
            
            p = Pattern.compile('https://[0-9a-zA-Z]+\\.(.*?)\\.salesforceliveagent');
            pm = p.matcher(endpoint);
            if (pm.find()) {
                settings.EndpointId = pm.group(1);
            }
            
            p = Pattern.compile('deployment_id=+(.*?)&');
            pm = p.matcher(endpoint);
            if (pm.find()) {
                settings.DeploymentId = pm.group(1);
            }
            
            if(settings.Form != null && settings.Form.IsRoutingMenuVisible__c) {
                retrieveRoutingRules();
            }
            
            System.debug('***** settings: ' + settings);
            settingsJSON = settings.getJSON();
        }
    }
    
    public CS_LiveAgent_PrechatFormSettings__c getAssignedForm(String buttonId) {
        System.debug('************ getAssignedform ************');
        CS_LiveAgent_PrechatFormAssignment__c assignedform = CS_LiveAgent_PrechatFormAssignment__c.getValues(buttonId);
        if(assignedform != null) {
            String assignedformName = assignedform.PrechatFormSettingsName__c;
            System.debug('***** assignedformName: ' + assignedformName);
            CS_LiveAgent_PrechatFormSettings__c form = getSettingsForm(assignedformName);
            if (form != null) {
                return form;
            }
        }
        // if form assignment is not found, return the default form
        return getDefaultForm();
    }
    
    private CS_LiveAgent_PrechatFormSettings__c getSettingsForm(String formName) {
        return CS_LiveAgent_PrechatFormSettings__c.getValues(formName);
    }
    
    private CS_LiveAgent_PrechatFormSettings__c getDefaultForm() {
        return CS_LiveAgent_PrechatFormSettings__c.getValues(DEFAULT_FORM_KEY);
    }
    
    private void retrieveRoutingRules() {
        System.debug('************ retrieveRoutingRules ************');
        CS_LiveAgent_FormRoutingRule__c[] selectedRules = new CS_LiveAgent_FormRoutingRule__c[0];
        CS_LiveAgent_FormRoutingRule__c[] allRoutingRules = CS_LiveAgent_FormRoutingRule__c.getall().values();
        String routingRulePrefix = String.isNotBlank(settings.Form.CustomRoutingRulesPrefix__c) ? settings.Form.CustomRoutingRulesPrefix__c : settings.Form.Name;
        String[] splitFormName = routingRulePrefix.split('-');
        String[] splitRuleName;
        for(CS_LiveAgent_FormRoutingRule__c rule : allRoutingRules) {
            splitRuleName = rule.Name.split('-');
            if(splitRuleName[0] == splitFormName[0]
               && splitRuleName[1] == splitFormName[1]) {
                selectedRules.add(rule);
            }
        }
        selectedRules.sort();
        settings.setRoutingRules(selectedRules);
	}
    
    @remoteAction
    global static boolean isContactOnlyExisting(string sEmail) {
        
        // Check if there is an existing Consumer - Email match
        list<Contact> contacts = [select Id from Contact where Email = :sEmail and IsPersonAccount = false and ToBeDeleted__c = false limit 1];
        
        system.debug('###LA - VFC_LiveAgent_PrechatForm_New - isContactOnlyExisting:' + contacts.size());
        
        return !contacts.isEmpty();
	}

    public class FormSettings {
        public CS_LiveAgent_PrechatFormSettings__c Form {get; set;}
        public Map<String,CS_LiveAgent_FormRoutingRule__c> RoutingRulesMap {get; set;}
        public String[] Categories {get; set;}
        
        public Boolean IsOrderNumberVisible {get; set;}
        public Boolean AreProductFieldsVisible {get; set;}
        
        public String EndpointId {get; set;}
        public String DeploymentId {get; set;}
        public String OrgId {get; set;}
        public String ButtonId {get; set;}
        
        public FormSettings(String buttonId, CS_LiveAgent_PrechatFormSettings__c form) {
            this.ButtonId = buttonId;
            this.Form = form;
            
            this.OrgId = UserInfo.getOrganizationId().left(15);
            
            this.IsOrderNumberVisible = form.IsOrderNumberVisible__c;
            this.AreProductFieldsVisible = form.AreProductFieldsVisible__c;
            
            setCategories();
        }
        
        public void setRoutingRules(CS_LiveAgent_FormRoutingRule__c[] routingRules) {
            System.debug('************ setRoutingRules ************');
            RoutingRulesMap = new Map<String,CS_LiveAgent_FormRoutingRule__c>();
            
            for (CS_LiveAgent_FormRoutingRule__c rule : routingRules) {
                RoutingRulesMap.put(rule.Name, rule);
                
                if (rule.IsOrderNumberVisible__c) {
                    isOrderNumberVisible = rule.IsOrderNumberVisible__c;
                }
                if (rule.AreProductFieldsVisible__c) {
                    areProductFieldsVisible = rule.AreProductFieldsVisible__c;
                }
            }
        }
        
        public void setCategories() {
            System.debug('************ setCategories ************');
            Categories = new String[0];
            if(form != null
               && form.IsCategoryVisible__c
               && form.CategoryValues__c != null
               && form.CategoryValues__c.length() > 0) {
                Categories.addAll(form.CategoryValues__c.split(';', -1));
            }
        }
        
        public String getJSON() {
            return JSON.serialize(this);
        }
        
        public Integer RoutingRulesSize { 
            get {
                if (RoutingRulesMap == null) return 0;
                else return RoutingRulesMap.size(); 
            }
        }
    }
}