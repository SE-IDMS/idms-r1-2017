/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 13/04/2016
* Description: 
*******************************************************************/

public with sharing class FieloPRM_UTILS_MultiLanguage {
    
    public static Boolean existFieldInObject(SObject obj, String fieldApiName){
        //Returns true if the field exists in the fields list of the object
        Boolean exists = false;
        try{
            obj.get(fieldApiName);
            exists = true;
        }catch(Exception e){}
        
        return exists;
    }
    
    public static set<String> existFieldsInObject(SObject obj, Set<String> fieldsApiName){
        //Checks if the fields exist
        set<String> fieldNotFound = new set<String>();
        for(String field: fieldsApiName){
            if(!existFieldInObject(obj, field)){
                fieldNotFound.add(field);
            }
        }
        
        //Returns a list with the fields that have not been found
        return fieldNotFound;
    }
    
    public static String getFieldsetLanguage(SObject obj, set<String> fieldSet, String lang){
        set<String> retFieldSet = new Set<String>();
        
        if(fieldSet != null){
            String defaultLang = 'EN';
            try{defaultLang = (FieloEE.OrganizationUtil.getLanguage() != null ? FieloEE.OrganizationUtil.getLanguage() : 'EN');}catch(Exception e){}
            
            String language = lang != null ? lang : defaultLang;
            language = language.length() > 2 ? language.substring(0,2) : language;
            
            for(String field: fieldSet){
                field = field.trim();
                
                //Save the original field
                String originalField = field;
                
                if(language != null){
                    //Removes the "__c" if any
                    String fieldWithoutC = field.endsWithIgnoreCase('__c') ? field.substring(0, field.lastIndexOfIgnoreCase('__c')) : field;
                    
                    //Adds the language sufix + "__c" (All the ML fields will be custom)
                    String fieldML = fieldWithoutC + '_' + language + '__c';
                    
                    //Verifies if the translated field exists
                    if(!existFieldInObject(obj, fieldML)){
                        //Verifies if the translated field exists without the package prefix
                        String fieldMLWOPrefix = fieldWithoutC.substringAfter('__') + '_' + language + '__c';
                        if(!existFieldInObject(obj, fieldMLWOPrefix)){
                            //If it doesn't, it is replaced by the original
                            field = originalField;
                        }else{
                            field = fieldMLWOPrefix;
                        }
                    }else{
                        field = fieldML;
                    }
                }else{
                    field = originalField;
                }
                
                retFieldSet.add(field);
            }
        }
        List<String> auxList = new List<String>();
        auxList.addAll(retFieldSet);
        return String.join(auxList, ',');
    }
    
    public static String getFieldsetLanguage(SObject obj, String fieldAPIName, String lang){
        return getFieldsetLanguage(obj, new set<String>{fieldAPIName}, lang);
    }
    
}