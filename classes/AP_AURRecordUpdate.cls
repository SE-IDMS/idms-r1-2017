/*
  Author: Pooja Suresh
  Description: Updates account with data from respective AccountUpdateRequest
  Release: May 2013
*/
public class AP_AURRecordUpdate {

  public static void updateAccRecordFromAUR(List<AccountUpdateRequest__c> lstAcc)
  {        
      List<ID> AccIdLst = new list<ID>();
      Map<ID,AccountUpdateRequest__c> mapaur = new Map<ID,AccountUpdateRequest__c>();
      
      for( AccountUpdateRequest__c a :lstAcc) 
      {
         if( a.ApprovalStatus__c == 'Approved' && a.Account__c != NULL )
         {
           AccIdLst.add(Id.valueof(a.Account__c));
           mapaur.put(Id.valueof(a.Account__c),a);
         }
      }

      List<Account> lstAccToUpdate= new List<Account>();
      for(Account accountRecord:[SELECT Id,Name,AdditionalAddress__c,City__c,Country__c,County__c,StateProvince__c,Street__c,VATNumber__c,ZipCode__c,AccType__c,DuplicateWith__c,Fax,LeadingBusiness__c,Phone,ReasonForDeletion__c,ToBeDeleted__c,CorporateHeadquarters__c,Inactive__c,InactiveReason__c,ParentId,UltiParentAcc__c,ClassLevel1__c,ClassLevel2__c,MarketSegment__c,MarketSubSegment__c,PRMAccount__c
                                 FROM Account WHERE ID in :AccIdLst limit :(Limits.getLimitQueryRows()-Limits.getQueryRows()) ])
      {
         AccountUpdateRequest__c accountUpdateRequestRecord=mapaur.get(accountRecord.Id);
         
         if(accountUpdateRequestRecord.RecordTypeId==System.Label.CLMAR16ACC01   ||  Test.isRunningTest() )    //CHECK FOR COUNTRY AUR RECORDTYPE ID
         {
             accountRecord.Name = accountUpdateRequestRecord.AccountName__c;
             accountRecord.Street__c = accountUpdateRequestRecord.Street__c;
             accountRecord.AdditionalAddress__c = accountUpdateRequestRecord.AdditionalInformation__c;
             accountRecord.City__c = accountUpdateRequestRecord.City__c;
             accountRecord.StateProvince__c = accountUpdateRequestRecord.StateProvince__c;
             accountRecord.Country__c = accountUpdateRequestRecord.Country__c;
             accountRecord.County__c = accountUpdateRequestRecord.County__c;
             accountRecord.ZipCode__c = accountUpdateRequestRecord.ZipCode__c;
             accountRecord.VATNumber__c = accountUpdateRequestRecord.VATNumber__c;    
             accountRecord.AccType__c=accountUpdateRequestRecord.AccType__c;
             accountRecord.DuplicateWith__c=accountUpdateRequestRecord.DuplicateWith__c;
             accountRecord.Fax=accountUpdateRequestRecord.Fax__c;
             accountRecord.LeadingBusiness__c=accountUpdateRequestRecord.LeadingBusiness__c;
             accountRecord.Phone=accountUpdateRequestRecord.Phone__c;
             accountRecord.ReasonForDeletion__c=accountUpdateRequestRecord.ReasonForDeletion__c;
             accountRecord.ToBeDeleted__c=accountUpdateRequestRecord.ToBeDeleted__c;
         }
         if(accountUpdateRequestRecord.RecordTypeId==System.Label.CLMAR16ACC02 ||  Test.isRunningTest() )    //CHECK FOR STRATEGIC AUR RECORDTYPE ID
         {
             accountRecord.CorporateHeadquarters__c = accountUpdateRequestRecord.CorporateHeadquarters__c;
             accountRecord.Inactive__c = accountUpdateRequestRecord.Inactive__c;
             accountRecord.InactiveReason__c = accountUpdateRequestRecord.InactiveReason__c;
             accountRecord.ParentId = accountUpdateRequestRecord.ParentAccount__c;
             accountRecord.UltiParentAcc__c = accountUpdateRequestRecord.UltiParentAcc__c;
             //accountRecord.AccountRelationshipStatus__c = accountUpdateRequestRecord.AccountRelationshipStatus__c;
             accountRecord.ClassLevel1__c = accountUpdateRequestRecord.ClassLevel1__c;
             accountRecord.ClassLevel2__c = accountUpdateRequestRecord.ClassLevel2__c;
             accountRecord.MarketSegment__c = accountUpdateRequestRecord.MarketSegment__c;
             accountRecord.MarketSubSegment__c = accountUpdateRequestRecord.MarketSubSegment__c;
             accountRecord.PRMAccount__c = accountUpdateRequestRecord.PRMAccount__c;
         }
         
         lstAccToUpdate.add(accountRecord);
      }
    
      update lstAccToUpdate;
        
  }      
}