// Test Class for VFC_SearchRetailer class

@isTest
public class VFC_SearchRetailer_Test{
// Test Method
static testMethod void testMe(){
Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) {
            VFC_SearchRetailer sr = new VFC_SearchRetailer();
            VFC_SearchRetailer.isOrCondition =True;
            VFC_SearchRetailer.SearchAccountModel sam = new  VFC_SearchRetailer.SearchAccountModel();
            sam.compName='TestCompany';
            sam.Condition1='';
            sam.compHeadQuarters='';
            sam.Condition2='';
            sam.compAddress='';
            sam.Condition3='';
            sam.City='Bangalore';
            sam.Condition4='';
            sam.zipPostalCode='560032';
            sam.Condition5='';
            sam.businessType='FI';
            sam.Condition6='';
            sam.Country='India';
            sam.stateProvince='Karnataka';

            VFC_SearchRetailer.SearchRetailer(sam);
          }
    }

}