/*    Author          : Accenture Team    
      Date Created    : 03/11/2011     
      Description     : Controller extensions for VFP66_DisplayConLocalAttributes. */
                        
public with sharing class VFC66_DisplayConLocalAttributes extends VFC_ControllerBase 
{
    public string conName {get;set;}
    public String interfaceType{get;set;}
    public String pgMsg{get;set;}
    public String severity{get;set;}    
    public string conLocalAttrLkid {get;set;}
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public List<SelectOption> columns{get;set;}
    public Boolean DisplayResults{get;set;} 
    public Boolean displayError{get;set;} 
    public string searchText{get;set;}
    public List<String> Labels{get;set;}
    public String level1Label{set; get{return Labels[0];}}
    public String level1{set; get;}
    public String level2{get; set;}
    public List<SelectOption> items1{set; get;}
    public List<SelectOption> items2{set; get;}
    public String level2Label{set; get{return Labels[1];}}
    private List<Sobject> sobjs;
    private Map<Id, String> countryMap = New Map<Id, String>();
    private CON_LocalAttribute__c currentLocalAttr = new CON_LocalAttribute__c();
    private Boolean flag;
    private Utils_PicklistManager plManager;
    private List<String> keywords;
    private List<String> filters;
    private Utils_DataSource.Result searchResult ;
    private Utils_DataSource dataSource;
    private List<DataTemplate__c> conLocalAttr = New List<DataTemplate__c>(); 
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
 
   //Constructor
   public VFC66_DisplayConLocalAttributes(ApexPages.StandardController controller) 
   {
     System.Debug('****** Initializing the Properties and Variables Begins for VFC66_DisplayConLocalAttributes******');   
     conLocalAttrLkid = system.currentpagereference().getParameters().get(CS007_StaticValues__c.getValues('CS007_4').Values__c); // Reference to the Contact Lookup ID of Contact Local Attribute. 
     currentLocalAttr = (CON_LocalAttribute__c)controller.getRecord();
     conName = System.currentPageReference().getParameters().get(CS007_StaticValues__c.getValues('CS007_3').Values__c); //Reference to the Contact Name of Contact Local Attribute.
     keywords = new List<String>();
     filters = new List<String>();
     columns = new List<SelectOption>();
     Labels = new List<String>();
     interfaceType = 'LA';
     flag = false;
     displayError = false;
     
     system.debug('----filters-----'+filters);
     system.debug('----keywords -----'+keywords );  
     
     dataSource = Utils_Factory.getDataSource(interfaceType);
     columns = new List<SelectOption>();
     columns = dataSource.getColumns();
     plManager = Utils_Factory.getPickListManager(interfaceType);
     
     if( plManager != null)
        {
            Labels = plManager.getPickListLabels();
            items1 = plManager.getPicklistValues(1, null);
            items2 = plManager.getPicklistValues(2, null);
            for(SelectOption option : items1)
             {         
                 if(!option.getValue().equalsIgnoreCase(Label.CL00355))
                 {
                     countryMap.put(option.getValue(), option.getLabel());
                 }
             } 
        }
        fetchedRecords = new List<DataTemplate__c>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC66_DisplayConLocalAttributes******');
  }     

  /* This method will be called on click of "Search" Button.This method calls the factory methods
   * and fetches the search results
   */
    public PageReference search() 
    {     
       System.Debug('****** Searching Begins  for VFC66_DisplayConLocalAttributes******');
        pgMsg = null; 
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();

        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchText!=null)
            keywords.add(searchText.trim());
        if(level1!=null)
            filters.add(level1);
        if(level2!=null)
            filters.add(level2); 
filters.add(Schema.sObjectType.CON_LocalAttribute__c.fields.contact__c.Name+'=TRUE');
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
       if(!keywords.isEmpty() && !filters.isEmpty())
        {
            if(filters[0].equalsIgnoreCase(Label.CL00355) && filters[1].equalsIgnoreCase(Label.CL00355) && keywords[0].length()==0)
            {
               pgMsg = Label.CL00643;
               severity = 'error';

            }             
            else if(filters[0]==NULL || filters[0].equalsIgnoreCase(Label.CL00355))
            { 
                pgMsg = Label.CL00681;
                severity = 'error';
            }
            else if (!filters[1].equalsIgnoreCase(Label.CL00355) || keywords[0].length()>0 || !filters[0].equalsIgnoreCase(Label.CL00355) )
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the Datasource call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                    System.debug(' Result'+searchResult);
                    DisplayResults = true;
                    
                    if(searchResult.NumberOfRecord > 100)
                    {
                        pgMsg = Label.CL00349;
                        severity = 'Info';
                    }
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling Local Attribute Search '+ exc.getMessage() );
                    pgMsg = Label.CL00398;
                    severity = 'error';
                }                
            }            
        }         
        System.Debug('****** Searching Ends  for VFC66_DisplayConLocalAttributes******');        
        return null;          
    }

    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC66_DisplayConLocalAttributes******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchText = null;
        keywords = new List<String>();
        filters = new List<String>();        
        DisplayResults = false;
        level1 = null;
        level2 = null;
        pgMsg = null; 
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC66_DisplayConLocalAttributes******');     
        return null;
    }
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly inserts/updates
     * the Local Attribute.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** Insertion/Updation of Local Attribute Begins******');
        
        VFC66_DisplayConLocalAttributes thisController = (VFC66_DisplayConLocalAttributes)controllerBase;
        pagereference pg;
        if(obj!=NULL)
        {
            conLocalAttr.add((DataTemplate__c)obj);
        }
        for(DataTemplate__c dt : conLocalAttr)
        {
            List<CON_LocalAttribute__c> conLA = New List<CON_LocalAttribute__c>();
            if(dt.field1__c != null && level1!=null)
               conLA = [Select Contact__c, LocalAttribute__c, LocalAttribute__r.Name, Country__c  from CON_LocalAttribute__c where Contact__c=:conLocalAttrLkid and LocalAttribute__r.Name =: dt.field1__c and LocalAttribute__c =: dt.field5__c  and country__c =: level1];  
           if(conLA!=NULL && !conLA.isEmpty())
            {
               pgMsg = Label.CL00642;
               severity = 'error';
            }
             
             /* Modified By ACCENTURE IDC
                Modifed for Defect - DEF-0362
                Modified Date: 07/12/2011
              */
            else
               {
                  CON_LocalAttribute__c coLA = New CON_LocalAttribute__c();
                  coLA.Contact__c = conLocalAttrLkid;
                  coLA.LocalAttribute__c = dt.field5__c;
                  coLA.country__c = level1;
                  Database.SaveResult dsr = Database.Insert(coLA, false);
                  if(!dsr.isSuccess())
                  {
                     Database.Error err = dsr.getErrors()[0];
                     pgMsg = Label.CL00414;
                     severity = 'error';
                  }
                  else
                     //pg = new ApexPages.StandardController(coLA).view();
                     pg= new PageReference('/'+coLA.Contact__c);
               } 
              //End of Modification              
         }
        System.Debug('****** Insertion/Updation of Local Attribute Ends******');
        return pg;        
    }         
}