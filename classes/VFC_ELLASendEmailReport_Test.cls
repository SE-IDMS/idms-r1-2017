/****
TEST:VFC_ELLASendEmailReport
****/

    @isTest
    public class VFC_ELLASendEmailReport_Test {

        static Testmethod void VFC_ELLASendEmailReport_Test() {
        
            PageReference testPage = new pagereference('/apex/VCC_ELLASendEmailReport');
            //testPage.getParameters().put('id', 'newContact1.id');

            Test.setCurrentPage(testPage);
            ApexPages.StandardController controller;
            VFC_ELLASendEmailReport testRet= new VFC_ELLASendEmailReport(); 
            testRet.VFC_ELLASendEmailReport();
            testRet.getOfferReportData();
            testRet.strEmail='test';
        
        
        }
        
        static Testmethod void VFC_batchELLA_TEST() {
        
            LaunchReportStakeholders__c lRS= new LaunchReportStakeholders__c();
           String Time_EXP = '0 0 0 15 3 ? 2022';

            lRS.BUReport__c=true;
            //lRS.CountryReport__c
            lRS.Email__c ='test@schneider-electric.com';
            lRS.IsActive__c=true;
            //lRS.User__c =userinfo.getuserid();
            
            Insert lRS; 
            Batch_ELLAReportEmailAttach erea = new Batch_ELLAReportEmailAttach();
        
            // 
            ID batchprocessid = Database.executeBatch(erea,Integer.valueOf(System.Label.CLAPR15I2P13)); 
             String jobId = System.schedule('ScheduleApexClassTest',Time_EXP,erea);                        
            
        
        
        }

    }