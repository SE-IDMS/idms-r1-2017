/*    
      Author          : Accenture Team    
      Date Created    : 24/10/2011     
      Description     : Controller extensions for VFP68_QuoteLinkClone. This class clones the Quote
                        Link & its related RITE Form.
*/
                        
public class VFC68_QuoteLinkClone 
{
    //Variable Declaration
    public String pgeMsg{get;set;}
    public boolean rndrCnclButtn{get;set;}
    public boolean rndrCnfmButtn{get;set;}    
    public String cnclButtnLbl{get;set;}
    public String sevrity{get;set;}
    public OPP_QuoteLink__c quoteLink = new OPP_QuoteLInk__c();
    public OPP_QuoteLink__c clonedQuteLink = new OPP_QuoteLInk__c();    
    public Map<String, String> insObjKeyPref = new Map<String, String>();        
    public Map<String, String> currObjKeyPref = new Map<String, String>();            
    public Map<Sobject, SObject> srcRecIdClondRec = new Map<Sobject, SObject>();             
    public static Map<Sobject, List<Schema.ChildRelationship>> objChldRel = new Map<Sobject, List<Schema.ChildRelationship>>();
    private List<String> custSett = new List<String>();        
    private List<Sobject> sobjs; 
    private String query;    
    private Map<String, Map<String, String>> currInsRecs = new Map<String, Map<String, String>>();            
    private String oppCurr;
    private boolean isError = false;
    private Id clondQuoteLinkId;
    private String mode;
    //Constructor
    public VFC68_QuoteLinkClone(ApexPages.StandardController controller) 
    {
        quoteLink = (OPP_QuoteLink__c)controller.getRecord();
        mode = ApexPages.currentPage().getParameters().get('mode');
    }
    
    /* This method is called on the load of VFP68_QuoteLinkClone page. Checks if the 
       logged in user has appropriate permissions to clone the Quote Link by calling 
       checkFullAccess() method of Utils_Methods.       
    */
    public pagereference cloneQuoteLink()
    {
        if(quoteLink.id!=null)
        {
            //Gets the Quote Link Query from the corresponding Custom Settings        
            query = CS005_Queries__c.getValues('CS005_1').StaticQuery__c +CS005_Queries__c.getValues('CS005_4').StaticQuery__c+' where Id=\''+quoteLink.id+'\'' ;
            //Queries the corresponding Quote Link
            quoteLink = Database.Query(query);  
    
            //Checks if the logged in user has appropriate permission to clone the Quote Link
            if(Utils_Methods.checkFullAccess(quoteLink.OwnerId))
            {
                if (mode == 'revise') {
                    pgeMsg = Label.CLOCT14SLS07;
                    cnclButtnLbl =  Label.CL00005.toUpperCase();
                    rndrCnfmButtn = true;
                    sevrity = 'confirm';
                   }               
                //Displays a confirmation message, before the logged-in user clones a Quote Link.
                else{
                        pgeMsg = Label.CL00634;
                        cnclButtnLbl =  Label.CL00005.toUpperCase();
                        rndrCnfmButtn = true;
                        sevrity = 'confirm';
                    }
            }
            else
            {
                //Displays an error message, if the logged-in user doesn't have appropriate permissions to clone Quote Link.
                if (mode == 'revise') 
                pgeMsg = Label.CLOCT14SLS64;
                else
                pgeMsg = Label.CL00635;
                
                cnclButtnLbl = Label.CL00267.toUpperCase();
                sevrity = 'error';            
            }
            rndrCnclButtn = true; 
        }
        return null; 
    }
    
    public pagereference continueClone() 
    {
        Savepoint sp = Database.setSavepoint();                 //Create a savepoint before cloning the Quote Link             
        
       //Clones the Quote Link Record       
        clonedQuteLink = quoteLink.clone();            //Clones the Quote Link Record//filter 
        System.debug('-------clonedQuteLink*-------'+clonedQuteLink);  
        List<OPP_QuoteLink__c> LargestRevision = new List<OPP_QuoteLink__c>();   
        List<OPP_QuoteLink__c> LargestRevision1 = new List<OPP_QuoteLink__c>();        
        System.debug('--quoteLink.Id---'+quoteLink.Id);
        System.debug('--quoteLink.Quote_Link---'+quoteLink.OriginalQuoteLink__c );
        if (mode == 'revise')
        {
        // Revision number for BR-5770
       if(quoteLink.OriginalQuoteLink__c==null){ 
            LargestRevision =[SELECT RevisionNumber__c,OriginalQuoteLink__c FROM OPP_QuoteLink__c WHERE OriginalQuoteLink__c=:quoteLink.Id ORDER BY RevisionNumber__c DESC LIMIT 1];      
            clonedQuteLink.OriginalQuoteLink__c = quoteLink.Id;   
                System.debug('---largestrevision.size=---'+LargestRevision.size());
                if(LargestRevision.size()>0)
                {
                    System.debug('---largestrevision.size=---'+LargestRevision.size());
                    clonedQuteLink.RevisionNumber__c=(LargestRevision[0].RevisionNumber__c)+1;
                }
                else
                {
                    clonedQuteLink.RevisionNumber__c = 1;
                }
            } 
           else 
               {
                    LargestRevision1 =[SELECT RevisionNumber__c,OriginalQuoteLink__c FROM OPP_QuoteLink__c WHERE OriginalQuoteLink__c=:quoteLink.OriginalQuoteLink__c ORDER BY RevisionNumber__c DESC LIMIT 1];                  
                    System.debug('---largestrevision1.size=---'+LargestRevision1.size());
                    System.debug('---quoteLink.QuoteLink__c---'+LargestRevision1[0].OriginalQuoteLink__c);            
                    System.debug('---quoteLink.QuoteLink__c---'+LargestRevision1[0].RevisionNumber__c);        
                    clonedQuteLink.OriginalQuoteLink__c = quoteLink.OriginalQuoteLink__c;
                    clonedQuteLink.RevisionNumber__c=(LargestRevision1[0].RevisionNumber__c)+1;    
               }
          } 
          System.debug('---else---'+clonedQuteLink);
          clonedQuteLink.QuoteStatus__c = Label.CL00397;          //Sets the values for Cloned Quote Link
          if(mode == 'revise')
          {
              clonedQuteLink.ActiveQuote__c = true;
          }
          else
          {
              clonedQuteLink.ActiveQuote__c = false;
              clonedQuteLink.OriginalQuoteLink__c = null;       
          }
          clonedQuteLink.CurrencyISOCode = quoteLink.OpportunityName__r.CurrencyISOCode;
        
        /* Modified By ACCENTURE IDC
            Modifed for Defect - DEF-0399
            Modified Date: 06/12/2011
        */
        clonedQuteLink.RecordTypeID = CS007_StaticValues__c.getValues('CS007_21').Values__c;
        
        //End Of Update
        Database.saveResult recIns= Database.Insert(clonedQuteLink, false);
        if(!recIns.isSuccess())
        {
            pgeMsg = recIns.getErrors()[0].getMessage();
            isError = true;
        }
        else{
            clondQuoteLinkId = recIns.getId();
            //Added for BR-9761 - June 2016 release - START
            if(mode == 'revise'){
                clonedQuteLink.opportunityname__r.promiseddeliverydateforquotation__c=null;
                clonedQuteLink.opportunityname__r.quoteissuancedatetocustomer__c=null;
                Database.saveResult recupd= Database.Update(clonedQuteLink.opportunityname__r, false);
                if(!recupd.isSuccess())
                {
                    pgeMsg = recupd.getErrors()[0].getMessage();
                    isError = true;
                }
            }
             //Added for BR-9761 - June 2016 release - END
       }

        if(clondQuoteLinkId!=null && isError==false)
        {
            system.debug('-------clondQuoteLinkId-------'+clondQuoteLinkId);    
            custSett = CS006_FilterFields__c.getValues('CS006_1').ChildRelationFilters__c.split(',');          //Gets the Child objects for Quote Link
            try
            {
                //Calls the cloneRecs method to 1st level RITE Objects the Quote Link record
                currInsRecs = cloneRecs((Sobject)clonedQuteLink,custSett, quoteLink.Id,clondQuoteLinkId);
            }
            catch(Exception e)
            {
                isError = true;
                pgeMsg = e.getMessage();
            }

            if((srcRecIdClondRec!=null && !srcRecIdClondRec.IsEmpty()) && (currInsRecs!=null && !currInsRecs.isEmpty()) && isError==false)
            {
                insObjKeyPref = currInsRecs.get('clonedRecs');          
                currObjKeyPref = currInsRecs.get('currRecs');
                system.debug('-------insObjKeyPref -------'+insObjKeyPref +'-------currObjKeyPref-------'+currObjKeyPref);
                
                //Clones the 2nd Level RITE Objects
                if(objChldRel!=null && !objChldRel.isEmpty())
                {
                    for(Schema.ChildRelationship chld: objChldRel.get((Sobject)clonedQuteLink))
                    {
                        if(chld.getChildSObject()!=null && 
                          (currObjKeyPref.containsKey(String.valueOf(chld.getChildSObject())) && currObjKeyPref.get(String.valueOf(chld.getChildSObject()))!=null) && 
                          (insObjKeyPref.containsKey(String.valueOf(chld.getChildSObject())) && insObjKeyPref.get(String.valueOf(chld.getChildSObject()))!=null))
                        {
                            if(custSett!=null)
                                custSett.clear();
                            system.debug('-----String.valueOf(chld.getChildSObject())------'+String.valueOf(chld.getChildSObject()));
                            
                            //Adds the Child Records which needs to be cloned
                            if(!(String.valueOf(chld.getChildSObject()) == 'RIT_SubContractOGSupp__c'))
                            {
                                //Adds the Notes & Attachments Child Object                                
                                custSett.addall(CS006_FilterFields__c.getValues('CS006_2').ChildRelationFilters__c.split(','));                                
                            }
                            if(String.valueOf(chld.getChildSObject()) == 'RIT_FinancialEnv__c')
                            {
                                //Adds the Terms & Means of Payment, Bank Guarantees Child Object
                                custSett.addall(CS006_FilterFields__c.getValues('CS006_3').ChildRelationFilters__c.split(','));
                            }
                            if(String.valueOf(chld.getChildSObject()) == 'RIT_Budget__c')
                            {
                                //Adds the Terms & Means of Payment, Bank Guarantees Child Object
                                custSett.addall(CS006_FilterFields__c.getValues('CS006_6').ChildRelationFilters__c.split(','));
                            }
                            if(String.valueOf(chld.getChildSObject()) == 'RIT_ContractualEnv__c')
                            {
                                //Adds the Liquidated Damages Child Object
                                custSett.addall(CS006_FilterFields__c.getValues('CS006_4').ChildRelationFilters__c.split(','));                                
                            }
                            if(!(String.valueOf(chld.getChildSObject()) == 'RIT_ProjectInformation__c'))
                            {
                                //Adds the Risk Child Object
                                custSett.addall(CS006_FilterFields__c.getValues('CS006_5').ChildRelationFilters__c.split(','));                                 
                            }
                            currInsRecs.clear();
                            try
                            {
                                //Clones the 2nd level RITE Objects
                                currInsRecs = cloneRecs(chld.getChildSObject().newSObject(),custSett, currObjKeyPref.get(String.valueOf(chld.getChildSObject())),insObjKeyPref.get(String.valueOf(chld.getChildSObject())));
                            }
                            catch(Exception e)
                            {
                                isError = true;
                                pgeMsg = e.getMessage();                                
                            }
                        }
                    }
                }                  
            }            
        }
        if(isError == false) {           
           // return new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+clondQuoteLinkId+'/e?retURL=%2F'+clondQuoteLinkId);      
             return new pagereference(Site.getPathPrefix()+'/'+clondQuoteLinkId+'/e?retURL=%2F'+clondQuoteLinkId);  
        }
        else
        {
            //If there are any failures in the transaction, rollback
            Database.rollback(sp); 
            clondQuoteLinkId =null;
            rndrCnfmButtn = false;
            cnclButtnLbl = Label.CL00267.toUpperCase();
            rndrCnclButtn = true;
            return null;
        }
    } 
    
    public Map<String, Map<String, String>> cloneRecs(Sobject sobj, List<String> chldRelVals, Id srcRecId, Id trgtRecId)
    {               
        List<Schema.ChildRelationship> reqChldRel = new List<Schema.ChildRelationship>();    
        set<String> childRelVals = new Set<String>();
        List<Id> insRecIds = new List<Id>();            
        childRelVals.addall(chldRelVals);
        if(sobj!=null)
        {
            //Queries all the Child Relationships for the corresponding Sobject
            List<Schema.ChildRelationship> allChldRel = sobj.getSobjectType().getDescribe().getChildRelationships();                     
        
            //Collects the Required Child Relationships 
            for(ChildRelationship chldRel: allChldRel)
            {
                String chldSobjName = String.valueOf(chldRel.getChildSObject());  
                if(childRelVals.contains(chldSobjName))
                {
                    reqChldRel.add(chldRel);
                }
            }
            system.debug('-----reqChldRel-----'+reqChldRel);
            if(reqChldRel!=null && !reqChldRel.isEmpty())
                objChldRel.put(sobj, reqChldRel);
        }
        system.debug('-----objChldRel-----'+objChldRel);
        
        //Passes Source Record Id, Target Record Id and List of Child Relationships to cloneChildRecords
        srcRecIdClondRec = Utils_Methods.cloneChildRecords(srcRecId, trgtRecId,reqChldRel);       
        system.debug('----srcRecIdClondRec-----'+srcRecIdClondRec); 
        
        if(srcRecIdClondRec!=null && !srcRecIdClondRec.isEmpty())       
        {
            List<Sobject> clonedRecs = new List<Sobject>();

            for(Sobject s: srcRecIdClondRec.Values())
            {
                //Populates the cloned Quote Link Id 
                if(String.valueOf(s.getSobjectType()) == 'RIT_Risks__c')
                {
                    RIT_Risks__c risk = (RIT_Risks__c)s;
                    risk.QuoteLink__c = clondQuoteLinkId;
                    clonedRecs.add((sobject)risk);
                }
                else 
                    clonedRecs.add(s);
            }
            if(!clonedRecs.isEmpty())
            {
                //Inserts the cloned record to the database
                insRecIds = Utils_Methods.insertRecords((List<sObject>)clonedRecs);        
                system.debug('----insRecIds -----'+insRecIds);     
                           
                //Collects the Sobject Type & Id of the current master records
                for(Sobject sbj: srcRecIdClondRec.keyset())
                {
                    currObjKeyPref.put(String.valueof(sbj.getSObjectType()),sbj.Id);            
                }
                system.debug('----currObjKeyPref-----'+currObjKeyPref);     
                //Collects the Sobject Type & Id of the cloned records
                if(insRecIds!=null && (insRecIds.size() == clonedRecs.size()))
                {
                    for(Integer i=0;i<insRecIds.size();i++)
                    {
                        insObjKeyPref.put(String.valueof(clonedRecs[i].getSobjectType()),String.valueOf(insRecIds[i]));                
                    }
                    system.debug('----insObjKeyPref-----'+currObjKeyPref); 
                    if((insObjKeyPref!=null && !insObjKeyPref.isEmpty()) && (currObjKeyPref!=null && !currObjKeyPref.isEmpty()))
                    {
                        currInsRecs.put('currRecs', currObjKeyPref);
                        currInsRecs.put('clonedRecs', insObjKeyPref);
                    }
                }
                
            }    
        }
        return currInsRecs;    
    } 
}