public class Handler_ContactAfterUpdate {
    public class MyException extends Exception{}
    public static Zuora_Request_Soap_Handler.zuoraHandler zh;
    public static String sessionId;
    
    public static void updateBillingAccountNames(Set<String> contactIds,Map<String,String> mapConNames) {
        Set<String> conAccSet = new Set<String>();
        Map<String, Contact> conMap = new Map<String,Contact>();
        Boolean needToBeUpdated = false;
        List<Zuora__CustomerAccount__c> conNamesToUpdate = new List<Zuora__CustomerAccount__c>();
        List<Contact> conList = [SELECT Id, Name,FirstName, LastName, WorkPhone__c, Email, Phone, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry, AccountId FROM Contact WHERE Id IN:contactIds];
        for(Contact con : conList){
          system.debug('con: ' + con.name);
          conAccSet.add(con.AccountId);
          conMap.put(con.name,con);
        }
        system.debug('mapConNames: ' + mapConNames);
        List<Zuora__CustomerAccount__c> billAcc = [SELECT Id, Zuora__BillToId__c, Zuora__SoldToId__c, Zuora__BillToName__c, Zuora__SoldToName__c, Zuora__Account__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c IN:conAccSet];
        system.debug('billAcc: ' + billAcc);
        for(Zuora__CustomerAccount__c ca :billAcc){
            if (mapConNames.containsKey(ca.Zuora__BillToName__c)){
                system.debug('conName billto before: ' +  ca.Zuora__BillToName__c);
                ca.Zuora__BillToName__c = mapConNames.get(ca.Zuora__BillToName__c);
                system.debug('conName billto after: ' +  ca.Zuora__BillToName__c);
                needToBeUpdated = true;
            }
            if (mapConNames.containsKey(ca.Zuora__SoldToName__c)){
                system.debug('conName soldto before: ' +  ca.Zuora__SoldToName__c);
                ca.Zuora__SoldToName__c = mapConNames.get(ca.Zuora__SoldToName__c);
                system.debug('conName soldto after: ' +  ca.Zuora__SoldToName__c);
                needToBeUpdated = true;
            }
            if (needToBeUpdated == true){
                conNamesToUpdate.add(ca);
                needToBeUpdated = false;
            }
        }
        if (conNamesToUpdate.size()>0)
            update conNamesToUpdate;
    }
    
    @future(callout=true)
    public static void BeginZuoraContactFlow(Set<String> contactIds) {
        if (contactIds.size()>0){
            zh = new Zuora_Request_Soap_Handler.zuoraHandler(null,null,null);
            zh = Zuora_Request_Soap_Handler.loginToZuora();
            try{
            if (zh.id==null)
                throw new MyException(zh.message);
            }catch(MyException ex){
                Helper_SendingErrorEmail.sendNotificationEmail('Contact', ex.getMessage());
            }
            sessionId = zh.id;
            updateContact(contactIds);
        }
            
    }

    public static void updateContact(Set<String> contactIds) {
        Set<String> conAccSet = new Set<String>();
        Map<String, Contact> conMap = new Map<String,Contact>();
       
        List<Contact> conList = [SELECT Id, Name,FirstName, LastName, WorkPhone__c, Email, Phone, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry, AccountId FROM Contact WHERE Id IN:contactIds];
        for(Contact con : conList){
          system.debug('con: ' + con.name);
          conAccSet.add(con.AccountId);
          conMap.put(con.name,con);
        }
        String zuoraContactId; 
        String zuoraContactName; 
        List<Zuora__CustomerAccount__c> billAcc = [SELECT Id, Zuora__BillToId__c, Zuora__SoldToId__c, Zuora__BillToName__c, Zuora__SoldToName__c, Zuora__Account__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c IN:conAccSet];
        
        system.debug('after billAcc: ' + billAcc);
        try{
            for(Zuora__CustomerAccount__c ca :billAcc){ 
                if (billAcc.size()>0){
                    system.debug('billAcc: '+ca.Zuora__BillToId__c);
                    if(conMap.containsKey(ca.Zuora__BillToName__c)){
                        zuoraContactId = ca.Zuora__BillToId__c;
                        zuoraContactName = ca.Zuora__BillToName__c;
                        zh = Zuora_Request_Soap_Handler.updateZuoraContact(sessionId, conMap.get(zuoraContactName), zuoraContactId);
                        if (zh.id==null)
                            throw new MyException(zh.message);
                    }
                    if(conMap.containsKey(ca.Zuora__SoldToName__c)){
                        zuoraContactId = ca.Zuora__SoldToId__c;
                        zuoraContactName = ca.Zuora__SoldToName__c;
                        zh = Zuora_Request_Soap_Handler.updateZuoraContact(sessionId, conMap.get(zuoraContactName), zuoraContactId);
                        if (zh.id==null)
                            throw new MyException(zh.message);
                    }
                    system.debug('zuoraContactId: '+zuoraContactId);
                   
                }
            }
           }catch (MyException ex){
                Helper_SendingErrorEmail.sendNotificationEmail('Contact', ex.getMessage());
           }
     }
}