@isTest
public class AP_ACK_Template_Handler_TEST
{
    public static testMethod void method1()
    {
        Map<Id, AcknowledgmentTemplate__c> MapAckTemplates = new Map<Id, AcknowledgmentTemplate__c> ();
        AcknowledgmentTemplate__c ackTemp = new AcknowledgmentTemplate__c();
        PointOfContact__c poc = new PointOfContact__c();
        Country__c ctry = Utils_TestMethods.createCountry();
        insert ctry;

        BusinessRiskEscalationEntity__c org = Utils_TestMethods.createBRE('testEntity','testSubEntity','testLocation','Plant');
        insert org;
        
        poc.Name = 'testPoc@schneider-electric.com';
        poc.ChannelType__c = 'Email';
        poc.SupportedCountry__c = ctry.Id;
        poc.Organization__c = org.Id;

        insert poc;

        ackTemp.Name = 'testTemplate';
        ackTemp.DefaultTemplate__c = false;
        ackTemp.SupportedCountry__c = ctry.Id; 
        ackTemp.PointOfContact__c = poc.Id;
        insert ackTemp;

        MapAckTemplates.put(ackTemp.Id,ackTemp);
        AP_ACK_Template_Handler.unCheckOtherDefaultTemplates(null,MapAckTemplates,true);
    }
}