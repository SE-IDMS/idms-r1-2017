global class Batch_ProductGMTUpdate implements 
             Database.Batchable<SObject>, Database.Stateful{
    
    /****   Variable declaration            *****/       
    global final  Set<string> setfromObject = new Set<string>();
    global  Set<string> setToObject = new Set<string>();
    global  List<String> TofieldNames = new List<String>();
    global  List<String> fromfieldNames = new List<String>();
    global  Map<string,string> mappingFileds= new Map<string,string>();
    global  String Toquery = 'SELECT ';
    global  String Fromquery = 'SELECT ';
    global  set<string>  setWhereClauseFromField = new set<string>();
    global  set<string>  setWhereClauseToField = new set<string>();
    global  string strwhereClausFrom;
    global  string strwhereClausToField;
   
    global  map<string, List<string>> mapfromObjetField = new map<string, List<string>>();
    global  map<string, List<string>> mapToObjetField = new map<string, List<string>>();
    global  map<string,string> mapWhereClauseFrom = new map<string,string>();
    global  map<string,string> mapWhereClauseTo = new map<string,string>();
    global  map<string,string> mapToFromObject = new map<string,string>();
    boolean isBatchErrorDataExecut; 
    String targetquery; 
    global  string seletToObjet;
    public class batchException extends Exception { /**Custom error message**/}
    global Batch_ProductGMTUpdate(boolean IsErrorDataUpdate,string selectedObeject){
        
       isBatchErrorDataExecut=IsErrorDataUpdate; 
       seletToObjet=selectedObeject;
       if(IsErrorDataUpdate==true) {
        integer countRefdata=[select count() from ReferentialDataError__c where ReRun__c=true ];
        if(countRefdata==0) {
            throw new batchException (label.CLDEC14REF06);
                   
           }
       }
    }
  
    
    global Database.queryLocator start(Database.BatchableContext ctx){
        List<ReferentialDataMapping__c> listrefDataMapping = new List<ReferentialDataMapping__c>();
        string strAndOpr='AND ';//label.CLDEC14REF01
        if(seletToObjet!=null ) {
            listrefDataMapping = [select id, Active__c, FromObject__c, ToObject__c, ToField__c, FromField__c, WhereClauseFromField__c, WhereClauseToField__c from ReferentialDataMapping__c where Active__c=true and ToObject__c=:seletToObjet ];
        }
        if(listrefDataMapping.size() > 0) {
            for(ReferentialDataMapping__c rdm:listrefDataMapping) {
             // mapping sobject
                 mapToFromObject.put(rdm.ToObject__c,rdm.FromObject__c);
                setToObject.add(rdm.ToObject__c);
                setfromObject.add(rdm.FromObject__c);
                mappingFileds.put(rdm.ToField__c,rdm.FromField__c);
                setWhereClauseFromField.add(rdm.WhereClauseFromField__c);
                setWhereClauseToField.add(rdm.WhereClauseToField__c);
                //WhereClauseToandFromObject
                mapWhereClauseFrom.put(rdm.FromObject__c,rdm.WhereClauseFromField__c);
                 //object from and Where class
                mapWhereClauseTo.put(rdm.ToObject__c,rdm.WhereClauseToField__c);
                //  //object To and Where class
                
                
                if(mapToObjetField.containsKey(rdm.ToObject__c)) {
                    mapToObjetField.get(rdm.ToObject__c).add(rdm.ToField__c);
                
                }else {
                    mapToObjetField.put(rdm.ToObject__c,new List<string>{rdm.ToField__c});
                    mapToObjetField.get(rdm.ToObject__c).add(rdm.WhereClauseToField__c);
                }
                if(mapfromObjetField.containsKey(rdm.FromObject__c)) {
                    mapfromObjetField.get(rdm.FromObject__c).add(rdm.FromField__c);
                }else {
                    mapfromObjetField.put(rdm.FromObject__c,new List<string>{rdm.FromField__c});
                    
                    mapfromObjetField.get(rdm.FromObject__c).add(rdm.WhereClauseFromField__c);
                }
            
            }
        }
        system.debug('ListValue'+listrefDataMapping);
       
       
       
       /***** Construct a "To object" string query dynamically *****/
        for(string strObj:mapToFromObject.Keyset()) {
           
            List<string> strList =mapToObjetField.get(strObj); 
            set<string> strSet = new set<string>(strList);  
                
            for(string strField:strSet) {
                Toquery+=strField+ ', ';   
            
            }
            Toquery+='Id from '+strObj+' '; 
           
        }
        
        /***** reRon Batch Job for the Failed records *****/
        if(isBatchErrorDataExecut) {
        
            set<id> idInClauseRefdataError = new set<id>();
            set<string> strInClauseRefdataError = new set<string>();
            
            set<id>  idFailedRecords = new set<id>();
            for(ReferentialDataError__c rdeObj:[select id,ErrorInFields__c,ErrorMessage__c,ReRun__c, IDofToObject__c,StatusCode__c from ReferentialDataError__c where ReRun__c=true ]) {
                if(rdeObj.IDofToObject__c!=null) {
                    idFailedRecords.add(ID.valueof(rdeObj.IDofToObject__c));
                    
                }
            
            }
            
             
            if(idFailedRecords.size() > 0) {
                for(string strObj:mapToFromObject.Keyset()) {
                     for(ID objId : idFailedRecords) {
                        Schema.SObjectType ObjType = objId.getSObjectType();
                        Schema.DescribeSObjectResult dsn = ObjType.getDescribe();
                        if(strObj==string.valueof(dsn.getName()) ) {
                            idInClauseRefdataError.add(objId);
                            strInClauseRefdataError.add(string.valueof(objId));
                        }
                    }    
                    
                    
                
                }
           
        
                /***** construct where Clause Id for failed Record *****/ 
            
                String idInClause = '(\'';
               
                for(string setstr:strInClauseRefdataError) {
                   idInClause+=setstr+'\',\''; 
                
                }       
                idInClause  = idInClause.substring(0,idInClause.length()-2);
                idInClause += ')';
                Toquery+='where Id IN '+idInClause;
            }
           
        }

        //fromField mapfromObjetField
        for(string strKey:mapfromObjetField.keyset()) {
            List<string> strList =mapfromObjetField.get(strKey); 
            
           set<string> strSet = new set<string>(strList);  
            for(string str:strSet) {
                Fromquery+=str+ ', ';
            }
        }
       
        for(string strset:setWhereClauseFromField) {
            strwhereClausFrom=strset;
        
        }
        //
        for(string strset:setWhereClauseToField) {
            strwhereClausToField=strset;
        
        }
        //whereClausToField
      
        if(isBatchErrorDataExecut) {
            Toquery+='AND '+strwhereClausToField+'!=null';
         }   
        else {
            Toquery+='where ';
            Toquery+=strwhereClausToField+'!=null';
            
        }
        system.debug('Toquery------->2'+Toquery);
        return Database.getQueryLocator(Toquery);
    }
    
    global void execute(Database.BatchableContext ctx, List<Sobject>
                        scope){
        set<string> setWhereClauseToField = new set<string>();
        
        string sobjName;
        Map<string, sObject> MapTargetObj = new Map<string, sObject>();
        
        Map<string, sObject> MapUpdateTargetObj = new Map<string, sObject>();
        
        List<Sobject> ListDataObjectUpdate = new List<Sobject>();
        List<Sobject> ListDataObject = (List<Sobject>)scope;
       
        system.debug('TEST--SobjectTarget'+ListDataObject);
        /****
        To get the where field value from "To Object" 
        *****/
        for(Sobject sObj:scope) {
            sobjName=string.valueof(sObj.getSobjectType());
            
            if(sObj.get(mapWhereClauseTo.get(string.valueof(sObj.getSobjectType())))!=null) {
                setWhereClauseToField.add(string.valueof(sObj.get(mapWhereClauseTo.get(string.valueof(sObj.getSobjectType())))));
                system.debug('TEST--SobjectTarget11'+string.valueof(sObj.get(mapWhereClauseTo.get(string.valueof(sObj.getSobjectType())))));
            }

        }
        //Call List of data "from Object"
        if(sobjName!=null) {
            MapTargetObj=toGetFromObjectDataFromToObject(setWhereClauseToField,sobjName) ;
        }
        /***** Execute the "From Object" and "To Object" mapping records for update  *****/
        
        for(Sobject sObj:scope) {
        
            system.debug('TEST--INHANU------>'+string.valueof(sObj.get(strwhereClausToField)));
            sObject frmObj=MapTargetObj.get(string.valueof(sObj.get(strwhereClausToField))) ;
            for(string strMap:mappingFileds.keyset()) {
                system.debug('Look --Value'+strMap);
                system.debug('Look --frmObj'+frmObj);
                if(frmObj!=null) {
                    sObject sObjFrmlookup=getSObject(frmObj,mappingFileds.get(strMap));
                
                    if(sObjFrmlookup==null) {
                        sObject sObjlookup=getSObject(sObj,strMap);
                        system.debug(sObjlookup);
                        if(sObjlookup!=null) {
                            list<string> strfieldNam=strMap.split('\\.');
                            system.debug('Inside I'+strfieldNam[strfieldNam.size()-1]);
                            if(frmObj.get(mappingFileds.get(strMap))!=null) {
                                sObjlookup.put(strfieldNam[strfieldNam.size()-1],frmObj.get(mappingFileds.get(strMap))) ;
                                MapUpdateTargetObj.put(sObjlookup.id,sObjlookup);
                            }
                            
                        }else {   
                                list<string> strfieldNam=mappingFileds.get(strMap).split('\\.');
                                    if(strfieldNam.size() < 2) {
                                       if(frmObj.get(mappingFileds.get(strMap))!=null) {
                                            sObj.put(strMap,frmObj.get(mappingFileds.get(strMap))) ;  
                                            MapUpdateTargetObj.put(sObj.id,sObj);
                                       }  
                                   }
                                   
                        }
                    }   
                    
                    else {
                            if(sObjFrmlookup!=null) {
                                list<string> strfrmfieldNam=mappingFileds.get(strMap).split('\\.');
                                
                                if(sObjFrmlookup.get(strfrmfieldNam[strfrmfieldNam.size()-1])!=null) {
                                    sObj.put(strMap,sObjFrmlookup.get(strfrmfieldNam[strfrmfieldNam.size()-1])) ;
                                    MapUpdateTargetObj.put(sObj.id,sObj);
                                }                           
                                
                            }
                    
                    }
                    system.debug('TESTMAP1'+mappingFileds.get(strMap));
                   // system.debug('TESTMAP'+frmObj.get(mappingFileds.get(strMap)));
                
               }
                                      
            } 
               
        }
        
        system.debug('TEST-------Final List'+MapUpdateTargetObj);
          List<ReferentialDataError__c> toInsertRefrentilErrors=new List<ReferentialDataError__c>();
          List<sObject> tosObjUpdateList = mapUpdateTargetObj.values();
        if(MapUpdateTargetObj.size() >0) {
            List<Database.SaveResult> dsrs = new List<Database.SaveResult> ();
           dsrs = Database.update(tosObjUpdateList, false);
            system.debug('TEST-------Final List');
             /*****To Insert Failed Records from Batch Record *****/        
            for (Integer i=0; i<dsrs.size(); i++){
                Database.SaveResult dsr =dsrs.get(i);
                if(!dsr.isSuccess()){
                    for(Database.Error dataerr :dsr.getErrors()) {
                    
                        ReferentialDataError__c refErrData=new ReferentialDataError__c();
                        refErrData.StatusCode__c=dataerr .getStatusCode().name();
                        refErrData.IDofToObject__c=String.valueOf(tosObjUpdateList.get(i).get('id'));//to get the failed id 
                        refErrData.ErrorMessage__c=dataerr .getMessage();
                        refErrData.ErrorInFields__c=String.valueOf(dataerr .getFields()); 
                        string strid=String.valueOf(tosObjUpdateList.get(i).get('id'));
                        Schema.SObjectType ObjType = id.valueof(strId).getSObjectType();
                        Schema.DescribeSObjectResult dsn = ObjType.getDescribe();
                        refErrData.ObjectName__c =string.valueof(dsn.getName());
                        toInsertRefrentilErrors.add(refErrData);
                    
                    }
                }   
            
            }   
        } 
        if(toInsertRefrentilErrors.size()>0) {
             List<Database.SaveResult> dsrs = Database.insert(toInsertRefrentilErrors, false); 
        }
    }
    
    global void finish(Database.BatchableContext ctx){
    
        
    }
    
    /****   To get Sources records  from sObject        *****/
    
    public Map<string, sObject>  toGetFromObjectDataFromToObject(Set<string> SetCommercialRefId,string toObjectName) {
        
        system.debug('WhwereFields----->'+SetCommercialRefId);
        Set<string> SetCommRefWhereClause =new  Set<string>();
        SetCommRefWhereClause=SetCommercialRefId;
        /***** construct where Clause ID     ******/  
        String idInClause = '(\'';
       
        for(string setstr:SetCommercialRefId) {
           idInClause+=setstr+'\',\''; 
        
        }
        idInClause  = idInClause.substring(0,idInClause.length()-2);
        idInClause += ')';
        Map<string, sObject> mapFrmObj= new Map<string, sObject>();
        
        //Creation of From Object Query String 
        
        string frSQuery=Fromquery; 
        
        frSQuery+='Id from '+mapToFromObject.get(toObjectName)+' where '+strwhereClausFrom+' IN:SetCommRefWhereClause';
        system.debug('WhwereFields----->'+frSQuery);
        //Data Base Query 
        List<SObject> targetFieldResults = Database.Query(frSQuery);
        system.debug('TEST--->Hanu2'+targetFieldResults);
        for(SObject sObj:targetFieldResults ) {
            for(string strSet:SetCommercialRefId) {
               if(sObj.get(strwhereClausFrom)==strSet) {
               
                   mapFrmObj.put(strSet,sObj);
               }
            
            }
        
        }
        system.debug('TEST---Hanu'+mapFrmObj);
        return mapFrmObj;
    }
    /*****  To Retrieving  the Lookup object records    *****/
    public sObject getSObject(sObject sObj,String fieldName) {
                
        sObject sobjLookup=null;
        List<string> fieldNem=fieldName.split('\\.');
        system.debug('HAnu'+fieldNem);
         for (Integer i=0; i<(fieldNem.size()-1); i++){
            
               sobjLookup=sObj.getSObject(fieldNem[i]);
          
            
        }
        
        return sobjLookup;
    }
   
}