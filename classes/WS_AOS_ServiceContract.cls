global class WS_AOS_ServiceContract{     
   
    global class ServiceContract{
        webservice  ID        ContractBfoID;
        webservice  String        AOSContractID;//Its been used in Update of Service Contract
        webservice  ID        InstalledProductBFOID;//Its been used for Update of IP.       
        webservice  String    Name;
        webservice  Date      StartDate;
        webservice  Date      EndDate;
        webservice  String    ContractOwner;
        webservice  String    SoldToName;
        webservice  String    SoldToGoldenID;
        webservice  String    SoldToType;
        webservice  String    SoldToStreet;
        webservice  String    SoldToCity;
        webservice  String    SoldToZipCode;
        webservice  String    SoldToCountry;
        webservice  Decimal    SoldToLatitude;
        webservice  Decimal    SoldToLongitude;
        webservice  List<ServiceLine>   serviceLines; 
    }
        
    global class ServiceLine{

        webservice  ID        ContractLineBFOID;
        webservice  String    Name;
        webservice  ID        ContractHeaderBFOID;
        webservice  Date      StartDate;
        webservice  Date      EndDate;
        webservice Decimal    Price;
        webservice Decimal     Qantity;
        webservice  String    ServicePlanName;
        webservice  List<CoveredProduct>    CoveredProducts; 
    }
    global class CoveredProduct{

        webservice  String   InstalledProductGoldenID;
        webservice  Date     StartDateCoveredProduct;
        webservice  Date      EndDateCoveredProduct;
        webservice  InstalledProduct    InstalledProducts; 
    }  
    global class InstalledProduct{

        webservice  ID         InstalledProductBFOID;
        webservice  String     Name;
        webservice  Date       CommissioningDate;
        webservice  String     SerialLotNumber;
        webservice  String     CommercialReference;
        webservice  String     GoldenID;
        webservice  String     InstalledProductCriticality;
        webservice  Date       AnnouncementofServiceObsolescenseDate;
        webservice  Date       EndOfCommercialisationDate;
        webservice  Date       ServiceObsolecenseDate;
        webservice  String     BrandeName;
        webservice  String     RangeName;
        webservice  String     DeviceTypeName;
        webservice Accountdetails account;
        webservice Locationdetails location;
    }
    global class Accountdetails{
         webservice  String Name;
         webservice  String Street;
         webservice  String City;
         webservice  String ZipCode;
         webservice  String Country;
         webservice  String GoldenID;
         webservice  Decimal Latitude;
         webservice  Decimal Longitude;
    }
    global class Locationdetails{

        webservice  ID  LocationBFOID;
        webservice  String  Name;
        webservice  String  GoldenID;
        webservice  String   Type;
        webservice  Boolean  AirConditionning;
        webservice  Boolean  AirDryerInTheSubstation;
        webservice  Boolean  Heating;
    }
    Public Static String ServiceContractRT = System.Label.CLAPR15SRV01;
    Public Static String ServiceLineRT = System.Label.CLAPR15SRV02;
    Public Static String ServiceContractConnectedRT = System.Label.CLAPR15SRV04;
    Public Static String ServiceLineConnectedRT = System.Label.CLAPR15SRV03;

    webservice static  List<ServiceContract> getAOSServiceContracts(){
        
        Map<id, SVMXC__Service_Contract__c> headerMap = new Map<id,SVMXC__Service_Contract__c>();
        Map<id, SVMXC__Service_Contract__c> headerMapTobeSend = new Map<id,SVMXC__Service_Contract__c>();//This is get the //correct service COntract header to be send which has Service Line and service plan with name te.
        Map<id, SVMXC__Service_Contract__c> sLineMap = new Map<id,SVMXC__Service_Contract__c>();
        List<ServiceContract> results = new List<ServiceContract>();
        /*
        SVMXC__Installed_Product__c ipdetails = new SVMXC__Installed_Product__c();
        Account accdetails = new Account();
        SVMXC__Site__c locdetails = new SVMXC__Site__c();*/
        

        Map<id, SVMXC__Installed_Product__c> IpRecMap = new Map<id,SVMXC__Installed_Product__c>();
        Map<id, Account> AccRecMap = new Map<id,Account>();
        Map<id, SVMXC__Site__c> LocRecMap = new Map<id,SVMXC__Site__c>();
        

        List<SVMXC__Service_Contract__c> scList = new List<SVMXC__Service_Contract__c>();
        Map<id,List<SVMXC__Service_Contract__c>> scidSLListMap = new Map<id,List<SVMXC__Service_Contract__c>>();
        Map<id,List<SVMXC__Service_Contract_Products__c>> slidcpListMap = new Map<id,List<SVMXC__Service_Contract_Products__c>>();
        List<SVMXC__Service_Contract__c> slList = new List<SVMXC__Service_Contract__c>();
        Set<id> slidset = new Set<id>();
        Set<id> IpIdset = new Set<id>();
        Set<id> acidset = new Set<id>();
        Set<id> locidset = new Set<id>();
        
        



         scList  =[select ID,Name,OwnerId,SVMXC__Start_Date__c,SVMXC__End_Date__c, SoldtoAccount__r.SEAccountID__c, SoldtoAccount__r.Name,
                        SoldtoAccount__r.AccType__c,SoldtoAccount__r.Street__c,SoldtoAccount__r.City__c,
                        SoldtoAccount__r.ZipCode__c,SoldtoAccount__r.SVMXC__Latitude__c,SoldtoAccount__r.SVMXC__Longitude__c,SoldtoAccount__r.Country__r.Name
                         from SVMXC__Service_Contract__c where Status__c = 'VAL' and RemoteSystemContractID__c=null and 
                         (RecordTypeId =:ServiceContractRT or RecordTypeId =:ServiceContractConnectedRT)];
                         
            system.debug('scList:'+scList);

        if(scList.size()>0){
            headerMap.putAll(scList);
            system.debug('headerMap:'+headerMap);

            slList  =[select ID,Name,OwnerId,SVMXC__Start_Date__c,SVMXC__End_Date__c,ParentContract__c,SVMXC__Contract_Price2__c,Quantity__c,SVMXC__Service_Plan__c,SVMXC__Service_Plan__r.Name from SVMXC__Service_Contract__c   where   (RecordTypeId =:ServiceLineRT or RecordTypeId =:ServiceLineConnectedRT) AND SVMXC__Service_Plan__c !=null AND SVMXC__Service_Plan__r.Name LIKE '%Ultimate%' AND ParentContract__c in : headerMap.keyset()];
            system.debug('slList:'+slList);
        }
        for(SVMXC__Service_Contract__c sl: slList){
            slidset.add(sl.id);
            if(headerMap.containsKey(sl.ParentContract__c)){
                headerMapTobeSend.put(headerMap.get(sl.ParentContract__c).Id,headerMap.get(sl.ParentContract__c));
            }

            if(scidSLListMap.containskey(sl.ParentContract__c)){
                scidSLListMap.get(sl.ParentContract__c).add(sl);  
            }

            else{
                scidSLListMap.put(sl.ParentContract__c, new List<SVMXC__Service_Contract__c>{sl});
            }
        }
        system.debug('headerMap:'+headerMap);
        system.debug('headerMapTobeSend:'+headerMapTobeSend);

        if(slidset != null && slidset.size()>0){
            for(SVMXC__Service_Contract_Products__c cp:[select ID,SVMXC__Installed_Product__r.GoldenAssetId__c,SVMXC__Service_Contract__c,SVMXC__Start_Date__c,SVMXC__End_Date__c,Golden_Asset_Id__c,SVMXC__Installed_Product__c ,SVMXC__Installed_Product__r.SVMXC__Company__c,SVMXC__Installed_Product__r.SVMXC__Site__c From    SVMXC__Service_Contract_Products__c Where SVMXC__Service_Contract__c in :slidset]){

                if(slidcpListMap.containskey(cp.SVMXC__Service_Contract__c))
                {

                    slidcpListMap.get(cp.SVMXC__Service_Contract__c).add(cp);
                }

                else{
                    slidcpListMap.put(cp.SVMXC__Service_Contract__c, new List<SVMXC__Service_Contract_Products__c>{cp});
                }

                //geting IP
                if(cp.SVMXC__Installed_Product__c !=null){
                    IpIdset.add(cp.SVMXC__Installed_Product__c);
                }

                if(cp.SVMXC__Installed_Product__r.SVMXC__Company__c !=null){
                    acidset.add(cp.SVMXC__Installed_Product__r.SVMXC__Company__c);
                }

                if(cp.SVMXC__Installed_Product__r.SVMXC__Site__c !=null){
                    locidset.add(cp.SVMXC__Installed_Product__r.SVMXC__Site__c);
                }
                
                

                if(IpIdset !=null){
                    List<SVMXC__Installed_Product__c> ipdetails = [Select ID,Name,CommissioningDateInstallDate__c,SVMXC__Serial_Lot_Number__c,SchneiderCommercialReference__c,GoldenAssetId__c,InstalledProductCriticality__c,WithdrawalDate__c,EndOfCommercialisationDate__c,ServiceObsolecenseDate__c,TECH_SDHBRANDID__c,TECH_SDHCategoryId__c,
                    TECH_SDHDEVICETYPEID__c From SVMXC__Installed_Product__c Where Id in :IpIdset];
                    IpRecMap.putAll(ipdetails);
                }

                if(acidset !=null){
                    List<Account> accdetails =[Select Id,Country__r.Name,Street__c,City__c,ZipCode__c,Name,SEAccountID__c,SVMXC__Latitude__c,SVMXC__Longitude__c From Account where Id in :acidset];
                    AccRecMap.putAll(accdetails);
                }

                if(locidset !=null){
                    List<SVMXC__Site__c> locdetails =[Select ID,Name,SVMXC__Location_Type__c,AirConditionning__c,AirDryerInTheSubstation__c,Heating__c, GoldenLocationId__c From SVMXC__Site__c where Id in :locidset];
                    LocRecMap.putAll(locdetails);
                }
                
            }
        }




        if(headerMapTobeSend !=null && headerMapTobeSend.size()>0){
            for(SVMXC__Service_Contract__c sc :headerMapTobeSend.Values()){
                ServiceContract scr = getServiceContractDetail(sc);
                

                if(scidSLListMap.containskey(sc.id) ){
                    List<ServiceLine> slr = new List<ServiceLine>();
                    

                    for(SVMXC__Service_Contract__c sl: scidSLListMap.get(sc.id)){
                        ServiceLine sline = getServiceLineDetails(new List<SVMXC__Service_Contract__c>{sl})[0];
                        if(slidcpListMap.containskey(sl.id))
                        {

                            List<CoveredProduct> cpdlist = new List<CoveredProduct>();
                            for(SVMXC__Service_Contract_Products__c  cp: slidcpListMap.get(sl.id) ){

                                CoveredProduct cpd = getCoveredProductDetail(new List<SVMXC__Service_Contract_Products__c>{cp})[0];
                                
                                


                                if(IpRecMap.containsKey(cp.SVMXC__Installed_Product__c)){
                                    InstalledProduct iprec = getIPDetail(IpRecMap.get(cp.SVMXC__Installed_Product__c));
                                    if(AccRecMap.containsKey(cp.SVMXC__Installed_Product__r.SVMXC__Company__c)){
                                        Accountdetails accrec = getaccount(AccRecMap.get(cp.SVMXC__Installed_Product__r.SVMXC__Company__c));
                                        

                                        iprec.account = accrec;
                                    }

                                    if(LocRecMap.containsKey(cp.SVMXC__Installed_Product__r.SVMXC__Site__c)){
                                        Locationdetails locrec = getlocation(LocRecMap.get(cp.SVMXC__Installed_Product__r.SVMXC__Site__c));
                                        

                                        iprec.location = locrec;
                                    }

                                    cpd.InstalledProducts = iprec;
                                    
                                }

                                cpdlist.add(cpd);
                            }

                            sline.CoveredProducts =cpdlist;
                            //sline.add(cpdlist);
                        }

                        slr.add(sline);
                    }

                    scr.serviceLines = slr;
                }

                results.add(scr);
            }
        }
        system.debug('results:'+results);
        
        return results;
    }
    // Methods
    public static ServiceContract getServiceContractDetail(SVMXC__Service_Contract__c sc){
        
        ServiceContract usc = new ServiceContract();
        usc.ContractBfoID = sc.id;
        usc.Name= sc.Name;
        usc.StartDate= sc.SVMXC__Start_Date__c;
        usc.EndDate= sc.SVMXC__End_Date__c;
        User user =[Select Id,SESAExternalID__c From User Where Id =:sc.OwnerId];
        usc.ContractOwner = user.SESAExternalID__c;
        usc.SoldToName = sc.SoldtoAccount__r.Name;
        usc.SoldToGoldenID = sc.SoldtoAccount__r.SEAccountID__c;
        usc.SoldToType = sc.SoldtoAccount__r.AccType__c;
        usc.SoldToStreet = sc.SoldtoAccount__r.Street__c;
        usc.SoldToCity = sc.SoldtoAccount__r.City__c;
        usc.SoldToZipCode = sc.SoldtoAccount__r.ZipCode__c;
        usc.SoldToCountry = sc.SoldtoAccount__r.country__r.Name;
        usc.SoldToLatitude = sc.SoldtoAccount__r.SVMXC__Latitude__c;
        usc.SoldToLongitude = sc.SoldtoAccount__r.SVMXC__Longitude__c;
        
        usc.serviceLines = new List<ServiceLine>();
        system.debug('usc:'+usc);
        return usc; 
      
    }
    public static List<ServiceLine> getServiceLineDetails(List<SVMXC__Service_Contract__c> slines){
        List<ServiceLine> sldlist = new List<ServiceLine>();
        for(SVMXC__Service_Contract__c sl: slines){
            ServiceLine usl  = new ServiceLine();
            usl.ContractLineBFOID = sl.id;
            usl.Name = sl.name;
            usl.ContractHeaderBFOID    = sl.ParentContract__c;
            usl.StartDate = sl.SVMXC__Start_Date__c; 
            usl.EndDate = sl.SVMXC__End_Date__c;
            usl.Qantity = sl.Quantity__c; 
            usl.Price = sl.SVMXC__Contract_Price2__c;
            usl.ServicePlanName = sl.SVMXC__Service_Plan__r.Name;
            usl.CoveredProducts = new List<CoveredProduct>();
            sldlist.add(usl);
        }

        return sldlist ;
    }
    public static List<CoveredProduct> getCoveredProductDetail(List<SVMXC__Service_Contract_Products__c> cps){ 
            
        List<CoveredProduct> cpdList = new List<CoveredProduct>();
        for(SVMXC__Service_Contract_Products__c cp: cps){
            CoveredProduct cpd = new CoveredProduct();
            

            cpd.InstalledProductGoldenID = cp.SVMXC__Installed_Product__r.GoldenAssetId__c;
            cpd.StartDateCoveredProduct = cp.SVMXC__Start_Date__c;
            cpd.EndDateCoveredProduct = cp.SVMXC__End_Date__c;
            cpd.InstalledProducts = new InstalledProduct();
            cpdList.add(cpd);
        }

        return cpdList ;
    }
    public static InstalledProduct getIPDetail(SVMXC__Installed_Product__c ip){
        InstalledProduct ipd = new InstalledProduct();
        

        ipd.InstalledProductBFOID = ip.Id;
        ipd.Name = ip.Name;
        ipd.CommissioningDate = ip.CommissioningDateInstallDate__c;
        ipd.SerialLotNumber = ip.SVMXC__Serial_Lot_Number__c;
        ipd.CommercialReference = ip.SchneiderCommercialReference__c;
        ipd.GoldenID = ip.GoldenAssetId__c;
        ipd.InstalledProductCriticality = ip.InstalledProductCriticality__c;
        ipd.AnnouncementofServiceObsolescenseDate = ip.WithdrawalDate__c;
        

        ipd.EndOfCommercialisationDate = ip.EndOfCommercialisationDate__c;
        ipd.ServiceObsolecenseDate = ip.ServiceObsolecenseDate__c;
        ipd.BrandeName = ip.TECH_SDHBRANDID__c;
        ipd.RangeName = ip.TECH_SDHCategoryId__c;
        ipd.DeviceTypeName = ip.TECH_SDHDEVICETYPEID__c;
        ipd.account = new Accountdetails() ;
        ipd.location = new Locationdetails();
        

        return ipd;
        

    }
    public static Accountdetails getaccount(Account acc){
        Accountdetails accd = new Accountdetails();
    
        accd.Name = acc.Name;
        accd.GoldenID = acc.SEAccountID__c;
        accd.Street = acc.Street__c;
        accd.City = acc.City__c;
        accd.ZipCode = acc.ZipCode__c;
        accd.Country = acc.country__r.Name;
        accd.Latitude = acc.SVMXC__Latitude__c;
        accd.Longitude = acc.SVMXC__Longitude__c;
        return accd;
    }
    public static Locationdetails getlocation(SVMXC__Site__c loc){
        Locationdetails locd = new Locationdetails();
        locd.Name = loc.Name;
        locd.GoldenID = loc.GoldenLocationId__c;
        locd.Type = loc.SVMXC__Location_Type__c;
        locd.AirDryerInTheSubstation = loc.AirDryerInTheSubstation__c;
        if (loc.AirConditionning__c == NULL || loc.AirConditionning__c == 'No Air Cond') {
            locd.AirConditionning = FALSE;
        } else {
            locd.AirConditionning = TRUE;
        }
        
         if (loc.Heating__c == NULL || loc.Heating__c == 'No Heating') {
            locd.Heating = FALSE;
        } else {
            locd.Heating = TRUE;
        }
        
        return locd;
    }
    global class UpdateResults{
        webservice  ID        RecordId;
        webservice  Boolean    success;
        webservice  String    ErrorMessage;
        webservice  String type;
    }
    public Static Database.SaveResult[] CreateOrUpdateServices(List<SObject> sObjectList,String type)
    {
            Database.SaveResult[] sresults ;
            if(sObjectList != null && sObjectList.size()>0){                
                if(type == 'CREATE' ){
                    sresults = Database.insert(sObjectList, false);
                }
                else if(type == 'UPDATE')
                {
                    sresults = Database.update(sObjectList, false);
                }
            }
        return sresults;
    }

    webservice static  List<UpdateResults> updateAOSServiceContracts(List<ServiceContract> result){
        set<Id> idset = new set<Id>();
        set<Id> idsetIp = new set<Id>();
        set<Id> ScidSet = new set<Id>();
        map<Id,string> scidAOSmap = new map<Id,string>();
        list<SVMXC__Service_Contract__c> sclist = new List<SVMXC__Service_Contract__c>();
        list<SVMXC__Service_Contract__c> sllist = new List<SVMXC__Service_Contract__c>();
        list<SVMXC__Installed_Product__c> iplist = new List<SVMXC__Installed_Product__c>();
        if(result.size()>0){
            for(ServiceContract  sd :result){
                idset.add(sd.ContractBfoID);
                idsetIp.add(sd.InstalledProductBFOID);
                scidAOSmap.put(sd.ContractBfoID,sd.AOSContractID);
                system.debug('***Update****');
                system.debug('idset:'+idset);
                system.debug('idsetIp:'+idsetIp);
            }
        }
        if(idset !=null && idset.size()>0){
            for(SVMXC__Service_Contract__c sl :[Select Id,ParentContract__c,Name,RemoteSystemContractID__c From SVMXC__Service_Contract__c                                    Where Id in :idset AND (RecordTypeId =:ServiceLineRT or RecordTypeId =:ServiceLineConnectedRT)]){
                ScidSet.add(sl.ParentContract__c);
                if(scidAOSmap.containsKey(sl.Id)){
                    sl.RemoteSystemContractID__c = scidAOSmap.get(sl.Id);
                    system.debug('idsetIp:'+sl.RemoteSystemContractID__c);
                }
                    sllist.add(sl);
            }
        }
        if(ScidSet !=null){
            for(SVMXC__Service_Contract__c sc :[Select Id,Name,Remote_System__c From SVMXC__Service_Contract__c                                    Where Id in :ScidSet AND (RecordTypeId =:ServiceContractRT or RecordTypeId =:ServiceContractConnectedRT)]){
                sc.Remote_System__c = 'AOS';
                sclist.add(sc);
            }
        }
        if(sllist.size()>0){
             // Database.SaveResult header_result 
            update sllist;
        }
        if(sclist.size()>0){
            update sclist;
        }
        if(idsetIp !=null && idsetIp.size()>0){
            for(SVMXC__Installed_Product__c ip :[Select Id,Name,Remote_System__c  from SVMXC__Installed_Product__c Where Id                                         in :idsetIp]){
                if(ip.Remote_System__c == null){
                    ip.Remote_System__c = 'AOS';
                }
                else{
                    ip.Remote_System__c =ip.Remote_System__c +';'+'AOS';
                }
                iplist.add(ip);
            }
        }
        if(iplist.size()>0){
            update iplist;
        }
        return null;

    }
    

}