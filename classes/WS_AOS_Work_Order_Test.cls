@isTest
Private class WS_AOS_Work_Order_Test {
        
    static testmethod void Work_OrderTest(){
        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
         //system.runAs(user)
         
         {        
        
        SVMXC__Installed_Product__c iprec= new SVMXC__Installed_Product__c(name='testip',ManageIBinFO__c=true,GoldenAssetId__c ='GIDTest-111');
        insert iprec;
                Contact c = new Contact();
                c.Email = 'Test@test.com';
                c.FirstName = 'First Name Test';
                c.LastName = 'Last Name Test';
                c.phone = '999999999';
                insert c;
                WS_AOS_Work_Order.WorkOrder woone = new WS_AOS_Work_Order.WorkOrder();
                WS_AOS_Work_Order.InstalledProduct ip = new  WS_AOS_Work_Order.InstalledProduct();
                ip.GoldenID = 'GIDTest-111';
                WS_AOS_Work_Order.Contact con = new WS_AOS_Work_Order.Contact();
                con.Email = 'Test@test.com';
                con.FirstName = 'First Name Test';
                con.LastName = 'Last Name Test';
                con.phone = '999999999';
                woone.CustomerRequestDate = system.today();
                woone.Priority =  'High';
                woone.WorkOrderReason = 'Test';
                woone.CommentsToPlanner = 'Test comment to planner';
                woone.RemoteSystemWorkOrderID = 'TEST-000000001';
                woone.InstalledProduct = ip;
                woone.CommentsToPlanner = 'Test comments';
                woone.Status = 'New';
                woone.Category = 'Test';
                woone.Contact = con;
               
                WS_AOS_Work_Order.WorkOrder wotwo = new WS_AOS_Work_Order.WorkOrder();
                WS_AOS_Work_Order.WorkOrder wothree = new WS_AOS_Work_Order.WorkOrder();
                WS_AOS_Work_Order.InstalledProduct ip2 = new  WS_AOS_Work_Order.InstalledProduct();
               
                WS_AOS_Work_Order.Contact con2 = new WS_AOS_Work_Order.Contact();
                con2.Email = 'Test@test.com2';
                con2.FirstName = 'First Name Test2';
                con2.LastName = 'Last Name Test2';
                con2.phone = '999999991';
                wothree.CustomerRequestDate = system.today();
                wothree.Priority =  'High';
                wothree.WorkOrderReason = 'Test2';
                wothree.CommentsToPlanner = 'Test comment to planner2';
                wothree.RemoteSystemWorkOrderID = 'TEST-000000002---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
                wothree.InstalledProduct = ip;
                wothree.CommentsToPlanner = 'Test comments2';
                wothree.Status = 'New';
                wothree.Category = 'Test2';
                wothree.Contact = con2;
                
                List<WS_AOS_Work_Order.WorkOrder> request = new List<WS_AOS_Work_Order.WorkOrder>();
                request.add(woone);
                request.add(wotwo );
                request.add(wothree);
                
                WS_AOS_Work_Order.CreatAOSWorkOrders(request);
                WS_AOS_Work_Order.CreatAOSWorkOrders(null);
                //WS_AOS_Work_Order.prepareServiceOrder(request);
             
             
       }
        
        
    }

}