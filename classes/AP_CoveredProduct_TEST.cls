@isTest//(SeeAllData=true)
private class AP_CoveredProduct_TEST {

    static testMethod void myUnitTest() {
      //  SRV_APR_002
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassVR__c=true,BypassTriggers__c = 'AP_LocationManager',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;


        system.runAs(user){ 
        
        recordtype slrt4 = [select id from recordtype where DeveloperName = 'ServiceLine' limit 1];
        recordtype scrt4 = [select id from recordtype where DeveloperName = 'ServiceContract' limit 1];
        //String ServiceLineRT = System.Label.CLAPR15SRV02;
        String ServiceLineRT = slrt4.id;
       //country 
        //Country__c testCountry = [Select id,Name,Countrycode__c From Country__c where Countrycode__c =: 'FR'];
            //insert testCountry;
             Country__c country = new country__c();
       country.Name = 'TestCountry';
       country.CountryCode__c = 'FR';
       insert country;
       
        Account accountSobj = Utils_TestMethods.createAccount();
        accountSobj.Country__c = country.Id;
        insert accountSobj;
        test.startTest();
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.SVMXC__Company__c = accountSobj.Id;
        sc.recordtypeid=scrt4.id;
        //sc.RecordTypeId = ServiceLineRT;
        try
        {
            insert sc;
        }
        catch(exception e) {}
        
        
        SVMXC__Service_Contract__c sc2 = new SVMXC__Service_Contract__c();
        sc2.SVMXC__Company__c = accountSobj.Id;
        sc2.ParentContract__c=sc.id;
        sc2.RecordTypeId = ServiceLineRT;
        try
        {
            insert sc2 ;
        }
        catch(exception e){}
        set<Id> scid = new set<Id>();
        scid.add(sc2.Id);
        
       
        Product2 product1 = new Product2();
        product1.Name = 'Test Product ';
        product1.ExtProductId__c = 'ExtProductId111';
        insert product1;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = accountSobj.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
         SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = accountSobj.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId123456';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.SVMXC__Product__c = product1.id;
        ip1.RangeToCreate__c = '1233';
        ip1.TECH_CreatedfromSFM__c =  true;
        insert ip1;
        
         SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.SVMXC__Company__c = accountSobj.id;
        ip2.Name = 'Test Intalled Product ';
        ip2.SVMXC__Status__c= 'new';
        ip2.GoldenAssetId__c = 'GoledenAssertId2';
        ip2.BrandToCreate__c ='Test Brand';
        ip2.SVMXC__Site__c = site1.id;
        ip2.SVMXC__Product__c = product1.id;
        ip2.RangeToCreate__c = '1233';
         ip2.TECH_CreatedfromSFM__c =  true;
        insert ip2;
        
        List<SVMXC__Service_Contract_Products__c> covlist = new List<SVMXC__Service_Contract_Products__c>();
        
        SVMXC__Service_Contract_Products__c scp1=  new SVMXC__Service_Contract_Products__c();
        scp1.SVMXC__Service_Contract__c = sc.id;
        scp1.SVMXC__Installed_Product__c = ip1.id;
        insert scp1;
        
        scp1.SVMXC__Installed_Product__c=ip2.id;
        scp1.SVMXC__Start_Date__c=date.today();
        update scp1;
        
        SVMXC__Service_Contract_Products__c scp2=  new SVMXC__Service_Contract_Products__c();
        scp2.SVMXC__Service_Contract__c = sc.id;
        scp2.SVMXC__Installed_Product__c = ip1.id;
        insert scp2;
        
        SVMXC__Service_Contract_Products__c scp3=  new SVMXC__Service_Contract_Products__c();
        scp3.SVMXC__Service_Contract__c = sc2.id;
        scp3.SVMXC__Installed_Product__c = ip2.id;
        try{
        insert scp3;
        }
        catch(exception e){}
         Test.stopTest();
        covlist.add(scp1);
        
    
        
        
        if(covlist.size()>0)
            AP_CoveredProduct.InstalledProdcutUnderContract(covlist);
        
        //if(scid !=null)
       // AP_ServiceContract.SCIBSyncUpdate(scid);
        
        //Delete scp2;
        try{
        Delete scp3;
        }
        catch(exception e){}
        /*
        scp1.SVMXC__Installed_Product__c = ip2.id;
        update scp1;*/
        /* 
        SVMXC__Service_Contract__c sl = new SVMXC__Service_Contract__c();
        sl.SVMXC__Company__c = accountSobj.Id;
        sl.RecordTypeId = ServiceLineRT;
        try{
        insert sl ;
        }
                catch(exception e){}
        
        SVMXC__Service_Contract_Products__c cp=  new SVMXC__Service_Contract_Products__c();
        cp.SVMXC__Service_Contract__c = sl.id;
        cp.SVMXC__Installed_Product__c = ip1.id;
        try{
        insert cp;
        }
        catch(exception e){}
        
        
        try{
        Delete cp;
        }
        catch(exception e){}
        
        //system.assertequal(,ip1.id);
        try{
        undelete cp;
        }
        catch(exception e){}
        */
       } 
        
        
    }
}