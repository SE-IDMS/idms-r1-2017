/*
Modified By          :Nikhil Agarwal
Modified Date       :15-02-2014
Release               :April 2014 Release
BR                      :4532
Description          :Linking Case to Contact Search Page
*/
public class VFC_DK_CreateContact {
   public String CaseId='';
   public String recordType {get; set;}
   public String Workphno{get;set;}
   public String FirstName{get;set;}
   public String LastName{get;set;}
   public String LocalFirstName{get;set;}
   public String LocalLastName{get;set;}
   public String AccountName{get;set;}
   public String LocalAccountName{get;set;}
   public String Street{get;set;}
   public String LocalStreet{get;set;}
   public String City{get;set;}
   public String LocalCity{get;set;}
   public String ZipCode{get;set;}
   public String State{get;set;}
   public String Country{get;set;}
   public String POBox{get;set;}
   public String Email{get;set;}
   public String WorkPhone{get;set;}
   public String WorkPhoneExt{get;set;}
   public String MobilePhone{get;set;}
   public String ClassLevel1{get;set;}
   public String Currencycode{get;set;}
   public String ClassLevel2{get;set;}
   public String MarketSegment{get;set;}
   public String MarketSubSegment{get;set;}
   public String existingAccountId{get;set;}

   public Account a;
   public Contact c ;
   public  Boolean ischecked{get; set;}
   public Account account {get; set;}
   public Contact contact{get; set;}
   public VFC_DK_CreateContact() {
             recordType=Apexpages.currentPage().getParameters().get('recType');
             FirstName=System.currentPageReference().getParameters().get('FirstName');
             LastName=System.currentPageReference().getParameters().get('LastName');
             LocalFirstName=System.currentPageReference().getParameters().get('LocalFirstName');
             LocalLastName=System.currentPageReference().getParameters().get('LocalLastName');
             AccountName=System.currentPageReference().getParameters().get('AccountName');
             LocalAccountName=System.currentPageReference().getParameters().get('LocalAccountName');
             Street=System.currentPageReference().getParameters().get('Street');
             LocalStreet=System.currentPageReference().getParameters().get('StreetLocalLang');
             City=System.currentPageReference().getParameters().get('City');
             LocalCity=System.currentPageReference().getParameters().get('LocalCity');
             ZipCode=System.currentPageReference().getParameters().get('ZipCode');
             State=System.currentPageReference().getParameters().get('State/Province');
             Country=System.currentPageReference().getParameters().get('Country');
             POBox=System.currentPageReference().getParameters().get('POBox');
             Email=System.currentPageReference().getParameters().get('Email');
             WorkPhone=System.currentPageReference().getParameters().get('CallerPhoneNumber');
             CaseId=System.currentPageReference().getParameters().get('Case');
             ClassLevel1=System.currentPageReference().getParameters().get('ClassLevel1');
             Currencycode=System.currentPageReference().getParameters().get('Currency');
            ClassLevel2=System.currentPageReference().getParameters().get('ClassLevel2');
            MarketSegment=System.currentPageReference().getParameters().get('MarketSegment');
            MarketSubSegment=System.currentPageReference().getParameters().get('MarketSubSegment');
            existingAccountId=System.currentPageReference().getParameters().get('existingAccountId');

             if(existingAccountId!=null){
              List<Account> accts=[select Name,Phone,isPersonAccount,AccountLocalName__c,Street__c,StreetLocalLang__c,City__c,LocalCity__c,ZipCode__c,StateProvince__c,Country__c,POBox__c,ClassLevel1__c,CurrencyISOCode,ClassLevel2__c,MarketSegment__c,MarketSubSegment__c from Account where id =:existingAccountId];
              System.debug('accounts information '+accts);
              if(accts.size()==1)
                account= accts[0];
              else
                account=new Account(RecordTypeId =recordType);
             }

             else
             account=new Account(RecordTypeId =recordType);
             System.debug('AccountName '+ AccountName);
             if(AccountName!='null' && AccountName!=null)
             account.Name=AccountName;
             if(LocalAccountName!='null' && LocalAccountName!=null)
             account.AccountLocalName__c=LocalAccountName;
             if(Street!='null' && Street!=null)
             account.Street__c=Street;
              if(LocalStreet!='null' && LocalStreet!=null)
             account.StreetLocalLang__c=LocalStreet;
              if(City!='null' && City!=null)
             account.City__c=City;
              if(LocalCity!='null' && LocalCity!=null)
             account.LocalCity__c=LocalCity;
              if(ZipCode!='null' && ZipCode!=null)
             account.ZipCode__c=ZipCode;
              if(State!='null' && State!=null)
             account.StateProvince__c=State;
              if(Country!='null' && Country!=null)
             account.Country__c=Country;
              if(POBox!='null' && POBox!=null)
             account.POBox__c=POBox;
             contact=new Contact();
             if(FirstName!='null' && FirstName!=null)
             contact.FirstName=FirstName;
             if(LastName!='null' && LastName!=null)
             contact.LastName=LastName;
             if(LocalFirstName!='null' && LocalFirstName!=null)
             contact.LocalFirstName__c=LocalFirstName;
             if(LocalLastName!='null' && LocalLastName!=null)
             contact.LocalLastName__c=LocalLastName;
           if(existingAccountId!='null' && existingAccountId!=null)
            contact.AccountID=existingAccountId;
             //for consumer account address fields prepopulation
              if(recordType==System.Label.CLDEC12ACC01 || existingAccountId!='null' && existingAccountId!=null)
              {
                    if(Street!='null' && Street!=null)
                         contact.Street__c=Street;
                      if(LocalStreet!='null' && LocalStreet!=null)
                         contact.StreetLocalLang__c=LocalStreet;
                      if(City!='null' && City!=null)
                             contact.City__c=City;
                      if(LocalCity!='null' && LocalCity!=null)
                             contact.LocalCity__c=LocalCity;
                      if(ZipCode!='null' && ZipCode!=null)
                             contact.ZipCode__c=ZipCode;
                      if(State!='null' && State!=null)
                             contact.StateProv__c=State;
                      if(Country!='null' && Country!=null)
                         contact.Country__c=Country;
                      if(POBox!='null' && POBox!=null)
                         contact.POBox__c=POBox;
                }

             if(Email!='null' && Email!=null)
             contact.Email=Email;
             if(WorkPhone!='null' && WorkPhone!=null)
                 contact.WorkPhone__c=WorkPhone;
             if(ClassLevel1!='null' && ClassLevel1!=null)
                 account.ClassLevel1__c=ClassLevel1;
             if(Currencycode!='null' && Currencycode!=null)
                account.CurrencyISOCode=Currencycode;
            if(ClassLevel2!='null' && ClassLevel2!=null)
                account.ClassLevel2__c=ClassLevel2;
            if(MarketSegment!='null' && MarketSegment!=null)
                account.MarketSegment__c=MarketSegment;
            if(MarketSubSegment!='null' && MarketSubSegment!=null)
                account.MarketSubSegment__c=MarketSubSegment;
             ischecked=True;
             System.debug('finally in the account'+account);
            }

     public pagereference saveContact() {
        Savepoint sp = Database.setSavepoint();//Create savepoint to rollback if Account is created but not conatct
        String Link;
        try{
            if(contact.AccountId!=null)
            {
               c = new Contact(Salutation=contact.Salutation,FirstName =contact.FirstName,LastName =contact.LastName ,AccountId = contact.AccountId,Email = contact.email,WorkPhone__c=contact.WorkPhone__c,WorkPhoneExt__c=contact.WorkPhoneExt__c,MobilePhone=contact.MobilePhone,UpdateAccountAddress__c=ischecked,Street__c=contact.Street__c,StreetLocalLang__c=contact.StreetLocalLang__c,AdditionalAddress__c=contact.AdditionalAddress__c,LocalAdditionalAddress__c=contact.LocalAdditionalAddress__c,City__c=contact.City__c,LocalCity__c=contact.LocalCity__c,ZipCode__c=contact.ZipCode__c,StateProv__c=contact.StateProv__c,County__c=contact.County__c,LocalCounty__c=contact.LocalCounty__c,Country__c=contact.Country__c,POBox__c=contact.POBox__c,LocalFirstName__c=contact.LocalFirstName__c,LocalLastName__c=contact.LocalLastName__c);
               insert c;
               if(CaseId!='null' && CaseId!='')
                {
                System.debug('inside 1');
                Case UpdateContactonCase=new Case(Id=CaseId);
                UpdateContactonCase.Contactid=c.id;
                UpdateContactonCase.Accountid=c.AccountId;
                update UpdateContactonCase;
                Link='/'+CaseId;      //redirecting back to Case page with new account and contact name
                PageReference pageRef1 = new PageReference(Link);
                pageRef1.setRedirect(true);
                return pageRef1;
                }
                else
                {
                System.debug('inside 2');
                PageReference pageRef = new PageReference('/500/e?');        //creating a new case if not called from Case page
                pageRef.setRedirect(true);
                pageRef.getParameters().put('def_account_id',c.AccountId);
                pageRef.getParameters().put('def_contact_id',c.id);
                return pageRef;
                }
            }

            else if(recordType==System.Label.CLDEC12ACC01)
            {
            //For Consumer Account
                a = new Account(RecordTypeId =System.Label.CLDEC12ACC01,OwnerId=UserInfo.getUserId(),AccountLocalName__c=account.AccountLocalName__c,Salutation=contact.Salutation,FirstName=contact.FirstName,LocalFirstName__pc=contact.LocalFirstName__c,LocalLastName__pc=contact.LocalLastName__c,LastName=contact.LastName,PersonEmail=contact.email,WorkPhone__pc=contact.WorkPhone__c,WorkPhoneExt__pc=contact.WorkPhoneExt__c,PersonMobilePhone=contact.MobilePhone,Street__c=contact.Street__c,StreetLocalLang__c=contact.StreetLocalLang__c,AdditionalAddress__c=contact.AdditionalAddress__c,LocalAdditionalAddress__c=contact.LocalAdditionalAddress__c,City__c=contact.City__c,LocalCity__c=contact.LocalCity__c,ZipCode__c=contact.ZipCode__c,StateProvince__c=contact.StateProv__c,County__c=contact.County__c,LocalCounty__c=contact.LocalCounty__c,Country__c=contact.Country__c,POBox__c=contact.POBox__c,HowHeard__pc=contact.HowHeard__c,OtherreasonHowHeard__pc=contact.OtherreasonHowHeard__c,ClassLevel1__c=ClassLevel1);
                insert a;
                //Modified for BR-4532
                if(CaseId!='null' && CaseId!='')
                {
                Case UpdateContactonCase;
                list<contact> contactid=new list<contact>();
                contactid=[select id from contact where account.id=:a.id];
                UpdateContactonCase=[select id,Contact.id,account.id from Case where id=:CaseId];
                UpdateContactonCase.Contactid=contactid[0].id;
                UpdateContactonCase.Accountid=a.id;
                update UpdateContactonCase;
                Link='/'+CaseId;      //redirecting back to Case page with new account and contact name
                PageReference pageRef1 = new PageReference(Link);
                pageRef1.setRedirect(true);
                return pageRef1;
                }
                else
                {
                PageReference pageRef = new PageReference('/500/e?');        //creating a new case if not called from Case page
                pageRef.setRedirect(true);
                pageRef.getParameters().put('def_account_id',a.id);
                return pageRef;
                }
            }
            else{
            //For Business Account
                a = new Account(Name=account.Name,RecordTypeId =System.Label.CLOCT13ACC08,AccountLocalName__c=account.AccountLocalName__c,OwnerId=UserInfo.getUserId(),Phone=account.Phone,CurrencyIsoCode=account.CurrencyIsoCode,AccType__c=account.AccType__c,LeadingBusiness__c=account.LeadingBusiness__c,ClassLevel1__c=account.ClassLevel1__c,MarketSegment__c=account.MarketSegment__c,ClassLevel2__c=account.ClassLevel2__c,MarketSubSegment__c=account.MarketSubSegment__c,Street__c=account.Street__c,StreetLocalLang__c=account.StreetLocalLang__c,AdditionalAddress__c=account.AdditionalAddress__c,LocalAdditionalAddress__c=account.LocalAdditionalAddress__c,City__c=account.City__c,LocalCity__c=account.LocalCity__c,ZipCode__c=account.ZipCode__c,StateProvince__c=account.StateProvince__c,County__c=account.County__c,LocalCounty__c=account.LocalCounty__c,Country__c=account.Country__c,POBox__c=account.POBox__c);
                insert a;
                if(ischecked==True){
                    c = new Contact(Salutation=contact.Salutation,FirstName =contact.FirstName,LastName =contact.LastName ,AccountId = a.id,Email = contact.email,WorkPhone__c=contact.WorkPhone__c,WorkPhoneExt__c=contact.WorkPhoneExt__c,MobilePhone=contact.MobilePhone,UpdateAccountAddress__c=ischecked,LocalFirstName__c=contact.LocalFirstName__c,LocalLastName__c=contact.LocalLastName__c, HowHeard__c=contact.HowHeard__c,OtherreasonHowHeard__c=contact.OtherreasonHowHeard__c);
                if(Test.isRunningTest())
                throw new noMessageException ('Test Exception');
                }
                else{
                c = new Contact(Salutation=contact.Salutation,FirstName =contact.FirstName,LastName =contact.LastName ,AccountId = a.id,Email = contact.email,WorkPhone__c=contact.WorkPhone__c,WorkPhoneExt__c=contact.WorkPhoneExt__c,MobilePhone=contact.MobilePhone,UpdateAccountAddress__c=ischecked,Street__c=contact.Street__c,StreetLocalLang__c=contact.StreetLocalLang__c,AdditionalAddress__c=contact.AdditionalAddress__c,LocalAdditionalAddress__c=contact.LocalAdditionalAddress__c,City__c=contact.City__c,LocalCity__c=contact.LocalCity__c,ZipCode__c=contact.ZipCode__c,StateProv__c=contact.StateProv__c,County__c=contact.County__c,LocalCounty__c=contact.LocalCounty__c,Country__c=contact.Country__c,POBox__c=contact.POBox__c,LocalFirstName__c=contact.LocalFirstName__c,LocalLastName__c=contact.LocalLastName__c);
                }
                insert c;
                //Modified for BR-4532
                if(CaseId!='null' && CaseId!='')
                {
                Case UpdateContactonCase;
                UpdateContactonCase=[select id,Contact.id,account.id from Case where id=:CaseId];
                UpdateContactonCase.Contactid=c.Id;
                UpdateContactonCase.accountid=a.id;
                update UpdateContactonCase;
                Link='/'+CaseId;            //redirecting back to Case page with new account and contact name
                PageReference pageRef1 = new PageReference(Link);
                pageRef1.setRedirect(true);
                return pageRef1;
                }
                else
                {
                PageReference pageRef = new PageReference('/500/e?');            //creating a new case if not called from Case page
                pageRef.setRedirect(true);
                pageRef.getParameters().put('def_account_id',a.id);
                pageRef.getParameters().put('def_contact_id',c.id);
                return pageRef;
                }
            }
        }
      catch(DmlException dmlexp)
        {
            Database.rollback(sp);//Rollback to the Savepoint
              for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDMLMessage(i)));
        }
        catch(Exception exp)
        {
                    Database.rollback(sp);//Rollback to the Savepoint
                    ApexPages.addMessages(new noMessageException(exp.getMessage()));
        }
        return null;
    }
         public PageReference cancelContact(){
            PageReference pageRef = new PageReference('/apex/VFP_DK_ContactSearch');
            pageRef.setRedirect(True);
            return pageRef;
        }
public class noMessageException extends Exception{}
}