/*Created by: Deepak Kumar
Date:07/08/2013
For the purpose of Updating Installed Product Field Under Contract Field Value From service COntract Active Status Value.
OCT13 Release
*/
Public Class AP_IpUndrContractFldUpdate
{
    public static void IpUnderContrUpdate(Set<Id> scId,Set<Id> ipId)
    {
     system.debug('scId:'+scId);
     system.debug('ipId:'+ipId);
    
    List<SVMXC__Installed_Product__c>  IpList  = new List<SVMXC__Installed_Product__c> ();
    List<SVMXC__Service_Contract__c>  CsList  = new List<SVMXC__Service_Contract__c> ();
    List<SVMXC__Service_Contract_Products__c >  CovList  = new List<SVMXC__Service_Contract_Products__c> ();
     if (!ipId.isempty() && ipId.size() > 0) //Added by VISHNU C for TOO Many SOQL Error B2F SC Interface
    {
        IpList=[Select Id,UnderContract__c,Tech_IpAssociatedToSC__c From SVMXC__Installed_Product__c Where Id=:ipId];
        CovList = [Select Id From SVMXC__Service_Contract_Products__c Where  SVMXC__Installed_Product__c=:ipId limit 1000];
    }
    if (!scId.isempty() && scId.size() > 0) //Added by VISHNU C for TOO Many SOQL Error B2F SC Interface
    {
        CsList=[Select Id,SVMXC__Active__c From SVMXC__Service_Contract__c Where Id=:scId and SVMXC__Active__c=true     limit 1];
    }
    
    system.debug('IpList:'+IpList);
    system.debug('CsList:'+CsList);
    system.debug('CovList:'+CovList);
    
        if(IpList.size()>0)
        {system.debug('111111');
            for(SVMXC__Installed_Product__c ip:IpList)
            {
                if(CsList.size()>0 && CovList.size()>0)
                {system.debug('2222');
                   
                    for(SVMXC__Service_Contract__c cs:CsList)
                    {
                     system.debug('cs.SVMXC__Active__c:'+cs.SVMXC__Active__c);
                        ip.UnderContract__c=cs.SVMXC__Active__c;
                        ip.Tech_IpAssociatedToSC__c = true;
                        system.debug('ip.UnderContract__c:'+ip.UnderContract__c);
                    }
                }
                else if(CovList.size()<=0)
                { 
                    ip.UnderContract__c=false;
                system.debug('ip.UnderContract__c:11'+ip.UnderContract__c);
                }               
            }
        }    
        update IpList;
    }
}