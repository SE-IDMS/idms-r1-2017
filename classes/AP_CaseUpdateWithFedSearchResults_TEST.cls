@IsTest
public class AP_CaseUpdateWithFedSearchResults_TEST {
    
    @IsTest
    static void testAP_CaseUpdateWithFedSearchResults() 
    {
        Account Acc = Utils_TestMethods.createAccount();
        Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
        Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        
        insert Acc;
        insert Ctct;
        insert Cse;
        
        Case Cse1 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Insert Cse1;
        Cse1.AnswertoCustomer__c = 'Answer';
        Update Cse1;
        
        InquiraFAQ__c Inq = new InquiraFAQ__c();
        Inq.Title__c = 'Test1';
        Inq.Excerpt__c = 'Test1';
        Inq.URL__c = 'Test1';
        Inq.Case__c = Cse1.Id;
        Inq.Visibility__c = 'Public';
        Inq.SourceSystem__c = 'Other';
        Inq.SourceSystem__c = 'Other';
        Insert(Inq);
        
        Inq = new InquiraFAQ__c();
        Inq.Title__c = 'Test2';
        Inq.Excerpt__c = 'Test2';
        Inq.URL__c = 'Test2';
        Inq.Case__c = Cse1.Id;
        Inq.Visibility__c = 'Private';
        Inq.SourceSystem__c = 'Other';
        Inq.SourceSystem__c = 'Other';
        Insert(Inq);
        
        Inq = new InquiraFAQ__c();
        Inq.Title__c = 'Test3';
        Inq.Excerpt__c = 'Test3';
        Inq.URL__c = null;
        Inq.Case__c = Cse1.Id;
        Inq.Visibility__c = 'Public';
        Inq.SourceSystem__c = 'Other';
        Inq.SourceSystem__c = 'Other';
        Insert(Inq);
        
        Inq = new InquiraFAQ__c();
        Inq.Title__c = 'Test4';
        Inq.Excerpt__c = 'Test4';
        Inq.URL__c = null;
        Inq.Case__c = Cse1.Id;
        Inq.Visibility__c = 'expert externaL expert';
        Inq.SourceSystem__c = 'Other';
        Inq.SourceSystem__c = 'Other';
        Insert(Inq);
        
        AP_CaseUpdateWithFedSearchResults.executeUpdate(Cse1);
        
        Cse1 = [select Id,AnswertoCustomer__c from Case where Id = :Cse1.Id];
        System.assert(Cse1.AnswertoCustomer__c.contains('Test1'));
        System.assert(!Cse1.AnswertoCustomer__c.contains('Test2'));
        System.assert(!Cse1.AnswertoCustomer__c.contains('Test3'));
        System.assert(!Cse1.AnswertoCustomer__c.contains('Test4'));
        
    }
}