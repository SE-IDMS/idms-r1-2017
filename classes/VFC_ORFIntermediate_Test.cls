@isTest
private class VFC_ORFIntermediate_Test {

    public static testMethod void testORFIntermediateAsPartner() {

        //UserRole uRole = [SELECT Id FROM UserRole WHERE Name='CEO' LIMIT 1];

       // Country__c country = Utils_TestMethods.createCountry();
       Country__c Country = new Country__c (Name='USA', CountryCode__c='US'); 
        insert country;
        //Creation of Partner Program
    
        Recordtype globalProgramRecordType = [Select id from RecordType where developername =  'GlobalProgram' limit 1];
        PartnerProgram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.recordtypeid=globalProgramRecordType.id;
        globalProgram.ProgramStatus__c = 'Active';
        insert globalProgram;
    
        //Program Level
    
        ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.id);
        insert newLevel;
        
        //Creation of Country Program       
        
        Recordtype countryProgramRecordType = [Select id from RecordType where developername =  'CountryPartnerProgram' limit 1];
        PartnerProgram__c countryProgram = new PartnerProgram__c(Name='Test Global Program',
                                                                 globalPartnerProgram__c = globalProgram.id, 
                                                                 country__c = country.id, 
                                                                 recordtypeid =countryProgramRecordType.Id,
                                                                 ProgramStatus__c='Active' );
                                                                 
        insert countryProgram;
        System.debug('*** Country Program:' + countryProgram);
    
        ProgramLevel__c CountryNewLevel = Utils_TestMethods.createCountryProgramLevel(countryProgram.id);
        CountryNewLevel.LevelStatus__c='Active';
        insert CountryNewLevel;
        System.debug('*** New Country Level:' + CountryNewLevel);

        
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=country.Id);
        insert PartnerAcc;
        System.debug('*** Partner Account:' + PartnerAcc);

        partnerAcc.PRMACcount__c=true;
        update partnerAcc;
        System.debug('*** Partner Account Updated: ' + partnerAcc);

        Contact PartnerCon = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  PRMContact__c = true,
                  Country__c=country.Id );
                  
        Insert PartnerCon;
        System.debug('*** Partner Contact:' + PartnerCon);

        List<User> lstUsers = new List<User>();

        Id profilesId = System.label.CLMAR13PRM03; //[select id from profile where name='SE - Channel Partner'].Id;
        User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'patuser1',
                        CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                        Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LanguageLocaleKey = 'en_US' , ProfileID = profilesId, ContactID = PartnerCon.Id  );
        lstUsers.add(u1);
        insert lstUsers;

        System.debug('*** Partner User:' + u1);

        ACC_PartnerProgram__c newAccountProgram = new ACC_PartnerProgram__c(
                                Account__c = partnerAcc.Id,
                                PartnerProgram__c =  countryProgram.Id,
                                ProgramLevel__c = CountryNewLevel.Id,
                                Active__c = true);
        insert newAccountProgram;
        System.debug('*** Account Program:' + newAccountProgram);
        
        FeatureCatalog__c newFC = new FeatureCatalog__c(
            Name='Test Regular ORF',
            Category__c='Opportunity Registration'
          );
        insert newFC;
        System.debug('*** New Feature Catalog:' + newFC);

        ProgramFeature__c newPrgFeature = new ProgramFeature__c(
                    FeatureCatalog__c=newFC.Id,
                    FeatureStatus__c='Active',
                    PartnerProgram__c=countryProgram.Id,
                    ProgramLevel__c = CountryNewLevel.Id
          );
        insert newPrgFeature;
        System.debug('*** New Program Feature:' + newPrgFeature);

        String query = 'SELECT PartnerProgram__r.Name, PartnerProgram__c, Account__c,ProgramLevel__c,ProgramLevel__r.Name  FROM ACC_PartnerProgram__c ' +
                        ' WHERE Active__c = true AND Account__c = \'' + u1.Contact.AccountId + '\' ' + 
                        ' AND (PartnerProgram__r.ProgramStatus__c = \'' + System.Label.CLMAY13PRM09 + '\' OR PartnerProgram__r.ProgramStatus__c = \'' + System.Label.CLAPR14PRM03 + '\')';
        System.debug('*** Query:' + query);

        List<ACC_PartnerProgram__c> lstPrograms = [SELECT PartnerProgram__r.Name, PartnerProgram__c, Account__c,ProgramLevel__c,ProgramLevel__r.Name 
                                                                FROM ACC_PartnerProgram__c 
                                                                WHERE Active__c = true AND Account__c = :u1.Contact.AccountId 
                                                                    AND (PartnerProgram__r.ProgramStatus__c = :System.Label.CLMAY13PRM09 OR PartnerProgram__r.ProgramStatus__c = :System.Label.CLAPR14PRM03)];
         system.runas(u1) {
            OpportunityRegistrationForm__c orf = new OpportunityRegistrationForm__c();
            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(orf);
            PageReference PageRef1 = Page.VFP_ORFIntermediate;
            PageRef1.getParameters().put('userid', u1.Id);
            Test.setCurrentPage(PageRef1); 
            VFC_ORFIntermediate orfIntermediate = new VFC_ORFIntermediate(sdtCon1);
            orfIntermediate.CheckProceed();

            orfIntermediate.doCancel();
         }
    }
    
    public static testMethod void testORFIntermediateAsInternal() {

      User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
      User u1;
      System.runAs ( thisUser ) {
          UserRole uRole = [SELECT Id FROM UserRole WHERE Name='CEO' LIMIT 1];

          List<User> lstUsers = new List<User>();

          Id profilesId = [select id from profile where name='System Administrator'].Id;
          u1= new User(Username = 'testUserTwo@schneider-electric.com', LastName = 'User11', alias = 'patuser1',
                          CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                          Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US' , ProfileID = profilesId, UserRoleId=uRole.Id,
                          ProgramOwner__c=true, ProgramManager__c=true,ProgramApprover__c=true, ProgramAdministrator__c=true,
                          PartnerORFConverter__c=true,BypassVR__c=true );
          lstUsers.add(u1);
          insert lstUsers;

          System.debug('*** Partner User:' + u1);
      }
      
      System.runAs(u1) {
        //Country__c country = Utils_TestMethods.createCountry();
        Country__c Country = new Country__c (Name='USA', CountryCode__c='US'); 
        insert country;
        //Creation of Partner Program
    
        Recordtype globalProgramRecordType = [Select id from RecordType where developername =  'GlobalProgram' limit 1];
        PartnerProgram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.recordtypeid=globalProgramRecordType.id;
        globalProgram.ProgramStatus__c = 'Active';
        insert globalProgram;
    
        //Program Level
    
        ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.id);
        insert newLevel;
        
        //Creation of Country Program       
        
        Recordtype countryProgramRecordType = [Select id from RecordType where developername =  'CountryPartnerProgram' limit 1];
        PartnerProgram__c countryProgram = new PartnerProgram__c(Name='Test Global Program',
                                                                 globalPartnerProgram__c = globalProgram.id, 
                                                                 country__c = country.id, 
                                                                 recordtypeid =countryProgramRecordType.Id,
                                                                 ProgramStatus__c='Active' );
                                                                 
        insert countryProgram;
        System.debug('*** Country Program:' + countryProgram);
    
        ProgramLevel__c CountryNewLevel = Utils_TestMethods.createCountryProgramLevel(countryProgram.id);
        CountryNewLevel.LevelStatus__c='Active';
        insert CountryNewLevel;
        System.debug('*** New Country Level:' + CountryNewLevel);

        
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=country.Id);
        insert PartnerAcc;
        System.debug('*** Partner Account:' + PartnerAcc);

        partnerAcc.isPartner = true;
        partnerAcc.PRMACcount__c=true;
        update partnerAcc;
        System.debug('*** Partner Account Updated: ' + partnerAcc);

        Contact PartnerCon = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  PRMContact__c = true,
                  Country__c=country.Id );
                  
        Insert PartnerCon;
        System.debug('*** Partner Contact:' + PartnerCon);


        ACC_PartnerProgram__c newAccountProgram = new ACC_PartnerProgram__c(
                                Account__c = partnerAcc.Id,
                                PartnerProgram__c =  countryProgram.Id,
                                ProgramLevel__c = CountryNewLevel.Id,
                                Active__c = true);
        insert newAccountProgram;
        System.debug('*** Account Program:' + newAccountProgram);
        
        FeatureCatalog__c newFC = new FeatureCatalog__c(
            Name='Test Regular ORF',
            Category__c='Opportunity Registration'
          );
        insert newFC;
        System.debug('*** New Feature Catalog:' + newFC);

        ProgramFeature__c newPrgFeature = new ProgramFeature__c(
                    FeatureCatalog__c=newFC.Id,
                    FeatureStatus__c='Active',
                    PartnerProgram__c=countryProgram.Id,
                    ProgramLevel__c = CountryNewLevel.Id
          );
        insert newPrgFeature;
        System.debug('*** New Program Feature:' + newPrgFeature);

        List<ACC_PartnerProgram__c> lstPrograms = [SELECT PartnerProgram__r.Name, PartnerProgram__c, Account__c,ProgramLevel__c,ProgramLevel__r.Name 
                                                                FROM ACC_PartnerProgram__c 
                                                                WHERE Active__c = true AND Account__c = :u1.Contact.AccountId 
                                                                    AND (PartnerProgram__r.ProgramStatus__c = :System.Label.CLMAY13PRM09 OR PartnerProgram__r.ProgramStatus__c = :System.Label.CLAPR14PRM03)];

          OpportunityRegistrationForm__c orf = new OpportunityRegistrationForm__c();
          ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(orf);
          PageReference PageRef1 = Page.VFP_ORFIntermediate;
          PageRef1.getParameters().put('userid', u1.Id);
          Test.setCurrentPage(PageRef1); 
          VFC_ORFIntermediate orfIntermediate = new VFC_ORFIntermediate(sdtCon1);
          orfIntermediate.CheckProceed();
      }
    }
}