//Test class for AP_OPP_ProjectTriggers - OCT 2015 Release
@isTest
public class AP_OPP_ProjectTriggers_TEST {

    static testMethod void prjCurrencyChange(){
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        prj.CurrencyIsoCode = 'INR';
        update prj;         
    }
}