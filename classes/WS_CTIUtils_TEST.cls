/*
Author: Ramesh Rajasekaran
Description:This test class is for the webservice class WS_CTIUtils
Created Date: 20-09-2012
*/

@isTest
private class WS_CTIUtils_TEST{

    
    static testMethod void testCaseSearch(){
    
        WS_CTIUtils.CaseInfo CaseInfo = new WS_CTIUtils.CaseInfo();
        
        Country__c ctryObj = new Country__c(name='India',CountryCode__c='IN');
        insert ctryObj;
         
           StateProvince__c stateProvince = Utils_TestMethods.createStateProvince(ctryObj.id);
        insert stateProvince; 
        //Create Preferred Agent
        User agent1 = Utils_TestMethods.createStandardUser('Agent1');
        insert agent1;
        
        //Create Preferred Backup Agent
        User agent2 = Utils_TestMethods.createStandardUser('Agent2');
        insert agent2;
        
        
        
          Account accObj2 = Utils_TestMethods.createAccount();
        accObj2.Name = ' Standard Account';
        accObj2.Phone = '3300000002';
        accObj2.Country__c = ctryObj.id;
        accObj2.StateProvince__c = stateProvince.id;
        accObj2.LeadingBusiness__c = 'Buildings';
        accObj2.ClassLevel1__c = 'Retailer';
        accObj2.Type = 'Prospect';
        accObj2.ServiceContractType__c = '';
        accObj2.City__c = 'bangalore';
        accObj2.ZipCode__c = '560066';
        accObj2.Street__c = 'EPIP';
        accObj2.POBox__c = '12345';
        accObj2.Pagent__c = agent1.Id;
        accObj2.PBAAgent__c = agent2.Id;
        insert accObj2; 
        
        Contact conObj9 = Utils_TestMethods.createContact(accObj2.id, ' Contact9' );
        conObj9.WorkPhone__c = '33000000055';
        conObj9.accountid = accObj2.id;
        conObj9.ServiceContractType__c = 'Contract Support';
        conObj9.Pagent__c = agent1.Id;
        conObj9.PBAAgent__c = agent2.Id;
        insert conObj9;
        
        
        //Create open case for  Contact 1
        Case cseObj = Utils_TestMethods.createCase(accObj2.Id, conObj9.Id, 'Open');
        insert cseObj;     
        system.debug('Id'+cseObj.id +'  Number'+ Cseobj.Casenumber);
        CaseInfo=WS_CTIUtils.getCaseInfo(Null,cseobj.id);
        system.debug(CaseInfo);
        
    }
    
    
   
    static testMethod void testCTIUtils(){
    
        WS_CTIUtils.Result result = new WS_CTIUtils.Result();
        
        Country__c ctryObj = new Country__c(name='India',CountryCode__c='IN');
        insert ctryObj;
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        
        Contact testContact = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact.WorkPhone__c = '626262';
        testContact.Phone = '626262';
        testContact.Country__c=ctryObj.id;
        insert testContact;
        
        Contact testContact2 = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact2.WorkPhone__c = '909090';
        testContact2.Phone = '909090';
        testContact2.Country__c=ctryObj.id;
        insert testContact2;
        
        Contract contract = Utils_TestMethods.createContract(testAccount.Id,testContact.Id);
        contract.StartDate = System.today()-1;  
        contract.ContractTerm = 12;        
        contract.status = 'Draft'; 
        contract.LeadingBusinessBU__c = 'EN';     
        insert contract;        
      
        contract.status = 'Activated';
        Update contract;
        
        CTR_ValueChainPlayers__c ctr = Utils_TestMethods.createCntrctValuChnPlyr(contract.Id);
        ctr.legacypin__c ='88888';
        Insert ctr;
        
        ANI__c ani = new ANI__C(Account__c = testAccount.Id,ANINumber__c = '55555',Contact__c = testContact.Id);
        insert ani;   
        
        ANI__c ani3 = new ANI__C(Account__c = testAccount.Id,ANINumber__c = '11111',Contact__c = testContact2.Id);
        insert ani3; 
        
        Contract contract5 = Utils_TestMethods.createContract(testAccount.Id,testContact2.Id);
        contract5.StartDate = System.today()-1;  
        contract5.ContractTerm = 12;        
        contract5.status = 'Draft'; 
        contract5.LeadingBusinessBU__c = 'EN';     
        insert contract5;        
      
        contract5.status = 'Activated';
        Update contract5;     
        
               
        CTR_ValueChainPlayers__c ctr3 = Utils_TestMethods.createCntrctValuChnPlyr(contract5.Id);
        ctr3.legacypin__c ='121212';
        Insert ctr3; 
             
        PackageTemplate__c  pkgtmp = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp;
        
        Package__c pkg =  Utils_TestMethods.createPackage(contract.Id,pkgtmp.Id);
        Pkg.status__c = 'Active';
        Pkg.Active__c = True;
        Pkg.Support_Level__c = 'Level 1';       
        insert pkg;
        
        Package__c pkg6 =  Utils_TestMethods.createPackage(contract5.Id,pkgtmp.Id);
        Pkg6.status__c = 'Active';
        Pkg6.Active__c = True;
        Pkg6.Support_Level__c = 'Level 1';       
        insert pkg6; 
        
        Package__c pkg1 =  Utils_TestMethods.createPackage(contract.Id,pkgtmp.Id);
        Pkg1.status__c = 'Active';
        Pkg1.Active__c = True;
        Pkg1.Support_Level__c = 'Level 2';       
        insert pkg1;
        
        Package__c pkg5 =  Utils_TestMethods.createPackage(contract.Id,pkgtmp.Id);
        Pkg5.status__c = 'Active';
        Pkg5.Active__c = True;
        Pkg5.Support_Level__c = '';       
        insert pkg5;
        
        Account testAccount1 = Utils_TestMethods.createAccount();
        insert testAccount1;
        
        Contact testContact1 = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact1.WorkPhone__c = '626262';
        testContact1.Country__c=ctryObj.id;
        insert testContact1;
        
        ANI__c ani2 = new ANI__C(Account__c = testAccount1.Id,ANINumber__c = '55555',Contact__c = testContact1.Id);
        insert ani2;
        
        Contract contract1 = Utils_TestMethods.createContract(testAccount1.Id,testContact1.Id);
        contract1.StartDate = System.today()-1;  
        contract1.ContractTerm = 12;        
        contract1.status = 'Draft';      
        insert contract1;
                
        contract1.status = 'Activated';
        contract1.LeadingBusinessBU__c = 'TBD';
        Update contract1;
        
        
        CTR_ValueChainPlayers__c ctr1 = Utils_TestMethods.createCntrctValuChnPlyr(contract1.Id);
        ctr1.legacypin__c ='99999';
        Insert ctr1;
        
             
        ANI__c ani1 = new ANI__C(Account__c = testAccount1.Id,ANINumber__c = '66666',Contact__c = testContact1.Id);
        insert ani1;
        
        PackageTemplate__c  pkgtmp1 = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp1;
        
        Package__c pkg2 =  Utils_TestMethods.createPackage(contract1.Id,pkgtmp1.Id);
        Pkg2.status__c = 'Active';
        Pkg2.Active__c = True;
        Pkg2.Support_Level__c = 'Level 1';       
        insert pkg2; 
        
        Package__c pkg3 =  Utils_TestMethods.createPackage(contract1.Id,pkgtmp1.Id);
        Pkg3.status__c = 'Active';
        Pkg3.Active__c = True;
        Pkg3.Support_Level__c = 'Level 2';       
        insert pkg3;
        
        Package__c pkg4 =  Utils_TestMethods.createPackage(contract1.Id,pkgtmp1.Id);
        Pkg4.status__c = 'Active';
        Pkg4.Active__c = True;
        Pkg4.Support_Level__c = '';       
        insert pkg4;
        
        CS_ContractBUmapping__c cum = new CS_ContractBUmapping__c();
        cum.SupportLevel__c = 'Level 1';
        cum.LeadingBusiness__c = 'EN';
        cum.SkillNumber__c = '123';
        cum.Name = '1';
        cum.BuVDnNumber__C = '12232';
        Insert cum;   
       
        
        CS_ContractBUmapping__c cum1 = new CS_ContractBUmapping__c();
        cum1.SupportLevel__c = 'Level 8';
        cum1.LeadingBusiness__c = 'EN';
        cum1.SkillNumber__c = '123';
        cum1.Name = '1';
        cum1.BuVDnNumber__C = '12232';
        Insert cum1;
        
        CS_ContractBUmapping__c cum3 = new CS_ContractBUmapping__c();
        cum3.SupportLevel__c = 'Level 8';
        cum3.LeadingBusiness__c = 'ID';
        cum3.SkillNumber__c = '123';
        cum3.Name = '1';
        cum3.BuVDnNumber__C = '12234';
        Insert cum3;
        
        CS_CTIErrorCodes__c cti1 = new CS_CTIErrorCodes__c();
        cti1.Errorcode__C = '11223';
        cti1.Errormessage__c = 'Error';
        cti1.wsassociated__C = 'PIN';
        cti1.BuVDnNumber__C = '12232';
        cti1.LeadingBusiness__c = 'EN';
        cti1.Messagecode__c = 1;
        cti1.Name = '1';
        Insert cti1; 
        
        CS_CTIErrorCodes__c cti2 = new CS_CTIErrorCodes__c();
        cti2.Errorcode__C = '11223';
        cti2.Errormessage__c = 'Error';
        cti2.wsassociated__C = 'PIN';
        cti2.BuVDnNumber__C = '12232';
        cti2.LeadingBusiness__c = 'EN';
        cti2.Messagecode__c = 2;
        cti2.Name = '1';
        Insert cti2; 
        
        CS_CTIErrorCodes__c cti3 = new CS_CTIErrorCodes__c();
        cti3.Errorcode__C = '11223';
        cti3.Errormessage__c = 'Error';
        cti3.wsassociated__C = 'ANI';
        cti3.BuVDnNumber__C = '12232';
        cti3.LeadingBusiness__c = 'EN';
        cti3.Messagecode__c = 5;
        cti3.Name = '1';
        Insert cti3;
        
        CS_CTIErrorCodes__c cti4 = new CS_CTIErrorCodes__c();
        cti4.Errorcode__C = '11224';
        cti4.Errormessage__c = 'Error';
        cti4.wsassociated__C = 'ANI';
        cti4.BuVDnNumber__C = '12234';
        cti4.LeadingBusiness__c = 'ID';
        cti4.Messagecode__c = 5;
        cti4.Name = '1';
        Insert cti4;        
               
        test.startTest();
        result = WS_CTIUtils.GetPINSkillNumber('88888',True,12232);
        result = WS_CTIUtils.GetPINSkillNumber('333333',True,12232);         
        result = WS_CTIUtils.GetPINSkillNumber('',True,1223); 
        result = WS_CTIUtils.GetANISkillNumber('66666',12234);      
        
        test.stoptest(); 
         
    }
    static testMethod void testCTIUtilsNew(){
        List<ANI__c> ListANI = new List<ANI__c>();
        WS_CTIUtils.Result result = new WS_CTIUtils.Result();
        
        Country__c ctryObj = new Country__c(name='India',CountryCode__c='IN');
        insert ctryObj;
        
        Account testAccount3 = Utils_TestMethods.createAccount();
        insert testAccount3;
        
        Contact testContact3 = Utils_TestMethods.createContact(testAccount3.id,'Test Account');
        testContact3.WorkPhone__c = '333333';
        testContact3.Phone = '333333';
        testContact3.Country__c=ctryObj.id;
        insert testContact3;
        
        Contract contract3 = Utils_TestMethods.createContract(testAccount3.Id,testContact3.Id);
        contract3.StartDate = System.today()-1;  
        contract3.ContractTerm = 12;        
        contract3.status = 'Draft';      
        insert contract3;
                
        contract3.status = 'Activated';
        contract3.LeadingBusinessBU__c = 'EN';
        contract3.LegacyContractId__c = 'AS';
        Update contract3; 
         
        ANI__c ani1 = new ANI__C(Account__c = testAccount3.Id,ANINumber__c = '66666',Contact__c = testContact3.Id);
        ListANI.add(ani1);
        ANI__c ani2 = new ANI__C(Account__c = testAccount3.Id,ANINumber__c = '66666',Contact__c = testContact3.Id);
        ListANI.add(ani2);
        insert ListANI;
        
        PackageTemplate__c  pkgtmp3 = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp3;
        
        Package__c pkg9 =  Utils_TestMethods.createPackage(contract3.Id,pkgtmp3.Id);
        Pkg9.status__c = 'Active';
        Pkg9.Active__c = True;
        Pkg9.Support_Level__c = 'Level 1';       
        insert pkg9;
        
        CTR_ValueChainPlayers__c ctr5 = Utils_TestMethods.createCntrctValuChnPlyr(contract3.Id);
        ctr5.legacypin__c ='333333';
        Insert ctr5;
        
        result = WS_CTIUtils.GetANISkillNumber('66666',12234);      
        result = WS_CTIUtils.GetANISkillNumber('878787',12232);  
        result = WS_CTIUtils.GetANISkillNumber('11111',12232); 
        result = WS_CTIUtils.GetANISkillNumber('',1223); 
        
    } 
    static testMethod void testCTIUtilsNew1(){
        WS_CTIUtils.ContractInfos result = new WS_CTIUtils.ContractInfos();
        
        Country__c ctryObj = new Country__c(name='India',CountryCode__c='IN');
        insert ctryObj;
        
        Account testAccount3 = Utils_TestMethods.createAccount();
        insert testAccount3;
        
        Contact testContact3 = Utils_TestMethods.createContact(testAccount3.id,'Test Account');
        testContact3.WorkPhone__c = '333333';
        testContact3.Phone = '333333';
        testContact3.Country__c=ctryObj.id;
        insert testContact3;
        
        Contract contract3 = Utils_TestMethods.createContract(testAccount3.Id,testContact3.Id);
        contract3.StartDate = System.today()-1;  
        contract3.ContractTerm = 12;        
        contract3.status = 'Draft';      
        insert contract3;
                
        contract3.status = 'Activated';
        contract3.LeadingBusinessBU__c = 'EN';
        contract3.LegacyContractId__c = 'AS';
        Update contract3; 
         
        
        
        PackageTemplate__c  pkgtmp3 = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp3;
        
        Package__c pkg9 =  Utils_TestMethods.createPackage(contract3.Id,pkgtmp3.Id);
        Pkg9.status__c = 'Active';
        Pkg9.Active__c = True;
        Pkg9.Support_Level__c = 'Level 1';       
        insert pkg9;
        
        CTR_ValueChainPlayers__c ctr5 = Utils_TestMethods.createCntrctValuChnPlyr(contract3.Id);
        ctr5.legacypin__c ='333333';
        Insert ctr5;
        
        
        result = WS_CTIUtils.getContractInfos('333333','A');
        result = WS_CTIUtils.getContractInfos('3333','A');
        result = WS_CTIUtils.getContractInfos('333333','');
        
    } 
    static testMethod void testCTIUtils1(){
    
        WS_CTIUtils.Result result = new WS_CTIUtils.Result();
        
        Country__c ctryObj = new Country__c(name='India',CountryCode__c='IN');
        insert ctryObj;
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        
        Contact testContact = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact.WorkPhone__c = '626262';
        testContact.Phone = '626262';
        testContact.Country__c=ctryObj.id;
        insert testContact;
        
        Contact testContact2 = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact2.WorkPhone__c = '909090';
        testContact2.Phone = '909090';
        testContact2.Country__c=ctryObj.id;
        insert testContact2;
        
        Contract contract = Utils_TestMethods.createContract(testAccount.Id,testContact.Id);
        contract.StartDate = System.today()-1;  
        contract.ContractTerm = 12;        
        contract.status = 'Draft'; 
        contract.LeadingBusinessBU__c = 'EN';     
        insert contract;        
      
        contract.status = 'Activated';
        Update contract;
        
        CTR_ValueChainPlayers__c ctr = Utils_TestMethods.createCntrctValuChnPlyr(contract.Id);
        ctr.legacypin__c ='88888';
        Insert ctr;
        
        ANI__c ani = new ANI__C(Account__c = testAccount.Id,ANINumber__c = '55555',Contact__c = testContact.Id);
        insert ani;   
        
        ANI__c ani3 = new ANI__C(Account__c = testAccount.Id,ANINumber__c = '11111',Contact__c = testContact2.Id);
        insert ani3; 
        
        Contract contract5 = Utils_TestMethods.createContract(testAccount.Id,testContact2.Id);
        contract5.StartDate = System.today()-1;  
        contract5.ContractTerm = 12;        
        contract5.status = 'Draft'; 
        contract5.LeadingBusinessBU__c = 'EN';     
        insert contract5;        
      
        contract5.status = 'Activated';
        Update contract5;     
        
               
        CTR_ValueChainPlayers__c ctr3 = Utils_TestMethods.createCntrctValuChnPlyr(contract5.Id);
        ctr3.legacypin__c ='121212';
        Insert ctr3; 
             
        PackageTemplate__c  pkgtmp = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp;
        
        Package__c pkg =  Utils_TestMethods.createPackage(contract.Id,pkgtmp.Id);
        Pkg.status__c = 'Active';
        Pkg.Active__c = True;
        Pkg.Support_Level__c = 'Level 1';       
        insert pkg;
        
        
        
        Account testAccount1 = Utils_TestMethods.createAccount();
        insert testAccount1;
        
        Contact testContact1 = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact1.WorkPhone__c = '626262';
        testContact1.Country__c=ctryObj.id;
        insert testContact1;
        
        ANI__c ani2 = new ANI__C(Account__c = testAccount1.Id,ANINumber__c = '55555',Contact__c = testContact1.Id);
        insert ani2;
        
        Contract contract1 = Utils_TestMethods.createContract(testAccount1.Id,testContact1.Id);
        contract1.StartDate = System.today()-1;  
        contract1.ContractTerm = 12;        
        contract1.status = 'Draft';      
        insert contract1;
                
        contract1.status = 'Activated';
        contract1.LeadingBusinessBU__c = 'TBD';
        Update contract1;
        
        
        CTR_ValueChainPlayers__c ctr1 = Utils_TestMethods.createCntrctValuChnPlyr(contract1.Id);
        ctr1.legacypin__c ='99999';
        Insert ctr1;
        
             
        ANI__c ani1 = new ANI__C(Account__c = testAccount1.Id,ANINumber__c = '66666',Contact__c = testContact1.Id);
        insert ani1;
        
        PackageTemplate__c  pkgtmp1 = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp1;
        
        Package__c pkg2 =  Utils_TestMethods.createPackage(contract1.Id,pkgtmp1.Id);
        Pkg2.status__c = 'Active';
        Pkg2.Active__c = True;
        Pkg2.Support_Level__c = 'Level 1';       
        insert pkg2; 
        
        
        CS_ContractBUmapping__c cum = new CS_ContractBUmapping__c();
        cum.SupportLevel__c = 'Level 1';
        cum.LeadingBusiness__c = 'EN';
        cum.SkillNumber__c = '123';
        cum.Name = '1';
        cum.BuVDnNumber__C = '12232';
        Insert cum;   
       
        
        CS_ContractBUmapping__c cum1 = new CS_ContractBUmapping__c();
        cum1.SupportLevel__c = 'Level 8';
        cum1.LeadingBusiness__c = 'EN';
        cum1.SkillNumber__c = '123';
        cum1.Name = '1';
        cum1.BuVDnNumber__C = '12232';
        Insert cum1;
        
        CS_ContractBUmapping__c cum3 = new CS_ContractBUmapping__c();
        cum3.SupportLevel__c = 'Level 8';
        cum3.LeadingBusiness__c = 'ID';
        cum3.SkillNumber__c = '123';
        cum3.Name = '1';
        cum3.BuVDnNumber__C = '12234';
        Insert cum3;
        
        CS_CTIErrorCodes__c cti1 = new CS_CTIErrorCodes__c();
        cti1.Errorcode__C = '11223';
        cti1.Errormessage__c = 'Error';
        cti1.wsassociated__C = 'PIN';
        cti1.BuVDnNumber__C = '12232';
        cti1.LeadingBusiness__c = 'EN';
        cti1.Messagecode__c = 1;
        cti1.Name = '1';
        Insert cti1; 
        
        CS_CTIErrorCodes__c cti2 = new CS_CTIErrorCodes__c();
        cti2.Errorcode__C = '11223';
        cti2.Errormessage__c = 'Error';
        cti2.wsassociated__C = 'PIN';
        cti2.BuVDnNumber__C = '12232';
        cti2.LeadingBusiness__c = 'EN';
        cti2.Messagecode__c = 2;
        cti2.Name = '1';
        Insert cti2; 
        
        CS_CTIErrorCodes__c cti3 = new CS_CTIErrorCodes__c();
        cti3.Errorcode__C = '11223';
        cti3.Errormessage__c = 'Error';
        cti3.wsassociated__C = 'ANI';
        cti3.BuVDnNumber__C = '12232';
        cti3.LeadingBusiness__c = 'EN';
        cti3.Messagecode__c = 5;
        cti3.Name = '1';
        Insert cti3;
        
        CS_CTIErrorCodes__c cti4 = new CS_CTIErrorCodes__c();
        cti4.Errorcode__C = '11224';
        cti4.Errormessage__c = 'Error';
        cti4.wsassociated__C = 'ANI';
        cti4.BuVDnNumber__C = '12234';
        cti4.LeadingBusiness__c = 'ID';
        cti4.Messagecode__c = 5;
        cti4.Name = '1';
        Insert cti4;        
               
        test.startTest();
        result = WS_CTIUtils.GetPINSkillNumber('88888',True,12232);
        result = WS_CTIUtils.GetPINSkillNumber('333333',True,12232);         
        result = WS_CTIUtils.GetPINSkillNumber('',True,1223); 
        result = WS_CTIUtils.GetANISkillNumber('66666',12234);      
        
        test.stoptest(); 
        
    }
     static testMethod void testCTIUtils2(){
    
        WS_CTIUtils.Result result = new WS_CTIUtils.Result();
        
        Country__c ctryObj = new Country__c(name='India',CountryCode__c='IN');
        insert ctryObj;
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        
        Contact testContact = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact.WorkPhone__c = '626262';
        testContact.Phone = '626262';
        testContact.Country__c=ctryObj.id;
        insert testContact;
        
        Contact testContact2 = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact2.WorkPhone__c = '909090';
        testContact2.Phone = '909090';
        testContact2.Country__c=ctryObj.id;
        insert testContact2;
        
        Contract contract = Utils_TestMethods.createContract(testAccount.Id,testContact.Id);
        contract.StartDate = System.today()-1;  
        contract.ContractTerm = 12;        
        contract.status = 'Draft'; 
        contract.LeadingBusinessBU__c = 'EN';     
        insert contract;        
      
        contract.status = 'Activated';
        Update contract;
        
        CTR_ValueChainPlayers__c ctr = Utils_TestMethods.createCntrctValuChnPlyr(contract.Id);
        ctr.legacypin__c ='88888';
        Insert ctr;
        
        
        Contract contract5 = Utils_TestMethods.createContract(testAccount.Id,testContact2.Id);
        contract5.StartDate = System.today()-1;  
        contract5.ContractTerm = 12;        
        contract5.status = 'Draft'; 
        contract5.LeadingBusinessBU__c = 'EN';     
        insert contract5;        
      
        contract5.status = 'Activated';
        Update contract5;     
        
               
        CTR_ValueChainPlayers__c ctr3 = Utils_TestMethods.createCntrctValuChnPlyr(contract5.Id);
        ctr3.legacypin__c ='121212';
        Insert ctr3; 
             
        PackageTemplate__c  pkgtmp = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp;
        
        Package__c pkg =  Utils_TestMethods.createPackage(contract.Id,pkgtmp.Id);
        Pkg.status__c = 'Active';
        Pkg.Active__c = True;
        Pkg.Support_Level__c = 'Level 1';       
        insert pkg;
        
        
        
        Account testAccount1 = Utils_TestMethods.createAccount();
        insert testAccount1;
        
        Contact testContact1 = Utils_TestMethods.createContact(testAccount.id,'Test Account');
        testContact1.WorkPhone__c = '626262';
        testContact1.Country__c=ctryObj.id;
        insert testContact1;
        
              
        Contract contract1 = Utils_TestMethods.createContract(testAccount1.Id,testContact1.Id);
        contract1.StartDate = System.today()-1;  
        contract1.ContractTerm = 12;        
        contract1.status = 'Draft';      
        insert contract1;
                
        contract1.status = 'Activated';
        contract1.LeadingBusinessBU__c = 'TBD';
        Update contract1;
        
        
        CTR_ValueChainPlayers__c ctr1 = Utils_TestMethods.createCntrctValuChnPlyr(contract1.Id);
        ctr1.legacypin__c ='99999';
        Insert ctr1;
        
        PackageTemplate__c  pkgtmp1 = Utils_TestMethods.createPkgTemplate();
        insert pkgtmp1;
        
        Package__c pkg2 =  Utils_TestMethods.createPackage(contract1.Id,pkgtmp1.Id);
        Pkg2.status__c = 'Active';
        Pkg2.Active__c = True;
        Pkg2.Support_Level__c = 'Level 1';       
        insert pkg2; 
        
        
        CS_ContractBUmapping__c cum = new CS_ContractBUmapping__c();
        cum.SupportLevel__c = 'Level 1';
        cum.LeadingBusiness__c = 'EN';
        cum.SkillNumber__c = '123';
        cum.Name = '1';
        cum.BuVDnNumber__C = '12232';
        Insert cum;   
       
        
        CS_ContractBUmapping__c cum1 = new CS_ContractBUmapping__c();
        cum1.SupportLevel__c = 'Level 8';
        cum1.LeadingBusiness__c = 'EN';
        cum1.SkillNumber__c = '123';
        cum1.Name = '1';
        cum1.BuVDnNumber__C = '12232';
        Insert cum1;
        
        CS_ContractBUmapping__c cum3 = new CS_ContractBUmapping__c();
        cum3.SupportLevel__c = 'Level 8';
        cum3.LeadingBusiness__c = 'ID';
        cum3.SkillNumber__c = '123';
        cum3.Name = '1';
        cum3.BuVDnNumber__C = '12234';
        Insert cum3;
        
        CS_CTIErrorCodes__c cti1 = new CS_CTIErrorCodes__c();
        cti1.Errorcode__C = '11223';
        cti1.Errormessage__c = 'Error';
        cti1.wsassociated__C = 'PIN';
        cti1.BuVDnNumber__C = '12232';
        cti1.LeadingBusiness__c = 'EN';
        cti1.Messagecode__c = 1;
        cti1.Name = '1';
        Insert cti1; 
        
        CS_CTIErrorCodes__c cti2 = new CS_CTIErrorCodes__c();
        cti2.Errorcode__C = '11223';
        cti2.Errormessage__c = 'Error';
        cti2.wsassociated__C = 'PIN';
        cti2.BuVDnNumber__C = '12232';
        cti2.LeadingBusiness__c = 'EN';
        cti2.Messagecode__c = 2;
        cti2.Name = '1';
        Insert cti2; 
        
        CS_CTIErrorCodes__c cti3 = new CS_CTIErrorCodes__c();
        cti3.Errorcode__C = '11223';
        cti3.Errormessage__c = 'Error';
        cti3.wsassociated__C = 'ANI';
        cti3.BuVDnNumber__C = '12232';
        cti3.LeadingBusiness__c = 'EN';
        cti3.Messagecode__c = 5;
        cti3.Name = '1';
        Insert cti3;
        
        CS_CTIErrorCodes__c cti4 = new CS_CTIErrorCodes__c();
        cti4.Errorcode__C = '11224';
        cti4.Errormessage__c = 'Error';
        cti4.wsassociated__C = 'ANI';
        cti4.BuVDnNumber__C = '12234';
        cti4.LeadingBusiness__c = 'ID';
        cti4.Messagecode__c = 5;
        cti4.Name = '1';
        Insert cti4;        
               
        test.startTest();
        result = WS_CTIUtils.GetPINSkillNumber('88888',True,12232);
        result = WS_CTIUtils.GetPINSkillNumber('333333',True,12232);         
        result = WS_CTIUtils.GetPINSkillNumber('',True,1223); 
        result = WS_CTIUtils.GetANISkillNumber('66666',12234);      
        
        test.stoptest(); 
        
    }
       
 }