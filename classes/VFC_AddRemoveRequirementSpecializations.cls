public with sharing class VFC_AddRemoveRequirementSpecializations {
    
    RequirementCatalog__c reqcat;
    list<RequirementSpecialization__c> lstExistingReqSpe = new list<RequirementSpecialization__c>();
    Set<Id> selOpts1 = new Set<Id>();
    
    public Boolean hasError{get;set;}
    public SelectOption[] selSpec { get; set; }
    public SelectOption[] allSpec{ get; set; }
    
    public VFC_AddRemoveRequirementSpecializations(ApexPages.StandardController controller) {
        selSpec = new List<SelectOption>();  
        allSpec = new List<SelectOption>();
        set<String> setCL1 = new set<String>();
        
        reqcat = (RequirementCatalog__c)controller.getRecord();
        lstExistingReqSpe = [Select Specialization__r.Name, Specialization__c, RequirementCatalog__c 
                from RequirementSpecialization__c where RequirementCatalog__c = : reqcat.Id order by Specialization__r.Name limit 1000];
        
        if(lstExistingReqSpe != null && !lstExistingReqSpe.isEmpty())
        {
            for (RequirementSpecialization__c s : lstExistingReqSpe ) 
            {
                selSpec.add(new SelectOption(s.Specialization__c ,s.Specialization__r.Name));
                selOpts1.add(s.Specialization__c);
                
            }  
        }
        list<Specialization__c> lstallSpecialization = new list<Specialization__c >();
        lstallSpecialization = [SELECT id, name FROM Specialization__c order by name limit 1000];        
        for(Specialization__c Spec: lstallSpecialization ) 
        {
            if(!(selOpts1.contains(Spec.Id)))
                allSpec.add(new SelectOption(Spec.Id, Spec.name));
        }        
        
    }//End of Constructor
    
    Public PageReference updateRequirementSpecialization()
    {
        Savepoint sp =  Database.setSavepoint();
        try
        {
            set<Id> setSelIds = new set<ID>();
            list<RequirementSpecialization__c> lstNewReqSpecialization = new list<RequirementSpecialization__c>();
            
            for(SelectOption so : selSpec) 
            {
                RequirementSpecialization__c speSegment = new RequirementSpecialization__c();
                speSegment.RequirementCatalog__c = reqcat.Id;
                speSegment.Specialization__c= so.getValue();
                lstNewReqSpecialization.add(speSegment);
                setSelIds.add(so.getValue());
            }
            
            Delete [select id from RequirementSpecialization__c where RequirementCatalog__c =:reqcat.id];
            
            insert lstNewReqSpecialization;
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            hasError = true;    
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
                       
            return null;
        }
        
        return new pagereference('/'+reqcat.Id); 
    }//End of method
    
}//End of class