@isTest
global class WS_GMRSearch_TEST{

    global class WS_MOCK_GMRSearch1_TEST implements WebServiceMock 
    {
       global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
           {
               WS_GMRSearch.globalSearchResponse resp = new WS_GMRSearch.globalSearchResponse();               
               Map<String, WS_GMRSearch.globalSearchResponse> response_map_x = new Map<String, WS_GMRSearch.globalSearchResponse>();
               response_map_x.put('response_x', resp);                   
               response.put('response_x', resp);                      
           }
    }
        global class WS_MOCK_GMRSearch2_TEST implements WebServiceMock 
    {
       global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
           {
               WS_GMRSearch.customerCareSearchResponse resp = new WS_GMRSearch.customerCareSearchResponse();               
               Map<String, WS_GMRSearch.customerCareSearchResponse > response_map_x = new Map<String, WS_GMRSearch.customerCareSearchResponse>();
               response_map_x.put('response_x', resp);                   
               response.put('response_x', resp);                      
           }
    }
    static testMethod void testWS_GMRSearch() {
        test.starttest();
            OPP_Product__c objProduct1 = new OPP_Product__c();
            objProduct1.Name = 'TEST BU';
            objProduct1.BusinessUnit__c = 'TEST BU';
            objProduct1.ProductLine__c='TEST_PL';
            objProduct1.ProductFamily__c='TEST_PF';
            objProduct1.Family__c='TEST_FLMY';
            objProduct1.TECH_PM0CodeInGMR__c = '11223344';
            objProduct1.HierarchyType__c = 'PM0-FAMILY';
            objProduct1.IsActive__c = true;
            insert objProduct1;
            Product2 pr=new Product2();
            pr.Name='GIR_AD_LF_CB_08test';
            pr.ProductGDP__c=objProduct1.id;
            pr.ExtProductId__c='123456';
            pr.GDPGMR__c= '1234567890123456';
             insert pr;
        
        WS_GMRSearch.globalSearch specialinstanceglobalSearch=new WS_GMRSearch.globalSearch();
        WS_GMRSearch.globalFilteredSearch specialinstanceglobalFilteredSearch=new WS_GMRSearch.globalFilteredSearch();
        WS_GMRSearch.resultDataBean specialinstanceresultDataBean=new WS_GMRSearch.resultDataBean();
        WS_GMRSearch.globalFilteredSearchResponse specialinstanceglobalFilteredSearchResponse=new WS_GMRSearch.globalFilteredSearchResponse();
        WS_GMRSearch.customerCareSearchResponse specialinstancecustomerCareSearchResponse=new WS_GMRSearch.customerCareSearchResponse();
        WS_GMRSearch.genericBean specialinstancegenericBean=new WS_GMRSearch.genericBean();
        WS_GMRSearch.checkListInDataBean specialinstancecheckListInDataBean=new WS_GMRSearch.checkListInDataBean();
        WS_GMRSearch.globalSearchResponse specialinstanceglobalSearchResponse=new WS_GMRSearch.globalSearchResponse();
        WS_GMRSearch.checkCommercialReferenceList specialinstancecheckCommercialReferenceList=new WS_GMRSearch.checkCommercialReferenceList();
        WS_GMRSearch.returnDataBean specialinstancereturnDataBean=new WS_GMRSearch.returnDataBean();
        WS_GMRSearch.GMRSearchPort specialinstanceGMRSearchPort=new WS_GMRSearch.GMRSearchPort();
        WS_GMRSearch.labelValue specialinstancelabelValue=new WS_GMRSearch.labelValue();
        WS_GMRSearch.customerCareSearch specialinstancecustomerCareSearch=new WS_GMRSearch.customerCareSearch();
        WS_GMRSearch.ServiceExpectedException specialinstanceServiceExpectedException=new WS_GMRSearch.ServiceExpectedException();
        WS_GMRSearch.gmrDataBean specialinstancegmrDataBean=new WS_GMRSearch.gmrDataBean();
        WS_GMRSearch.criteriaInDataBean specialinstancecriteriaInDataBean=new WS_GMRSearch.criteriaInDataBean();
        WS_GMRSearch.checkCommercialReferenceListResponse specialinstancecheckCommercialReferenceListResponse=new WS_GMRSearch.checkCommercialReferenceListResponse();
        WS_GMRSearch.gmrCheckedDataBean specialinstancegmrCheckedDataBean=new WS_GMRSearch.gmrCheckedDataBean();
        WS_GMRSearch.ServiceFatalException specialinstanceServiceFatalException=new WS_GMRSearch.ServiceFatalException();
        WS_GMRSearch.resultFilteredDataBean specialinstanceresultFilteredDataBean=new WS_GMRSearch.resultFilteredDataBean();
        WS_GMRSearch.gmrFilteredDataBean specialinstancegmrFilteredDataBean=new WS_GMRSearch.gmrFilteredDataBean();
        WS_GMRSearch.resultCheckedDataBean specialinstanceresultCheckedDataBean=new WS_GMRSearch.resultCheckedDataBean();
        WS_GMRSearch.paginationInDataBean specialinstancepaginationInDataBean=new WS_GMRSearch.paginationInDataBean();
        WS_GMRSearch.filtersInDataBean specialinstancefiltersInDataBean=new WS_GMRSearch.filtersInDataBean();
        test.stoptest();
        WS_GMRSearch.GMRSearchPort RIconnection=new  WS_GMRSearch.GMRSearchPort();
        RIconnection.ClientCertName_x=System.Label.CL00616;
        RIconnection.timeout_x = 90000;
        WS_GMRSearch.checkListInDataBean searchCRList = new WS_GMRSearch.checkListInDataBean();
        WS_GMRSearch.paginationInDataBean  pagination = new WS_GMRSearch.paginationInDataBean();
        WS_GMRSearch.resultCheckedDataBean Response;
        searchCRList.commercialReferenceList=new  List<String>();
        searchCRList.commercialReferenceList.add('GIR_AD_LF_CB_08test');
        searchCRList.commercialReferenceList.add('blahblah');
        pagination.maxLimitePerPage =100;
        pagination.pageNumber=1;
        WS_GMRSearch.resultCheckedDataBean TestResponse = new WS_GMRSearch.resultCheckedDataBean(); 
        List<WS_GMRSearch.gmrCheckedDataBean> TestRESP = new  List<WS_GMRSearch.gmrCheckedDataBean>();
        WS_GMRSearch.gmrCheckedDataBean TestgmrData  = new WS_GMRSearch.gmrCheckedDataBean();
        WS_GMRSearch.returnDataBean TestRDBean = new  WS_GMRSearch.returnDataBean();
        WS_GMRSearch.labelValue   TestlabValue = new  WS_GMRSearch.labelValue();
        Response = RIconnection.checkCommercialReferenceList(searchCRList, pagination); 
    }
    
    static testMethod void testWS_GMRSearch2() {
      test.starttest();
        OPP_Product__c objProduct1 = new OPP_Product__c();
        objProduct1.Name = 'TEST BU';
        objProduct1.BusinessUnit__c = 'TEST BU';
        objProduct1.ProductLine__c='TEST_PL';
        objProduct1.ProductFamily__c='TEST_PF';
        objProduct1.Family__c='TEST_FLMY';
        objProduct1.TECH_PM0CodeInGMR__c = '11223344';
        objProduct1.HierarchyType__c = 'PM0-FAMILY';
        objProduct1.IsActive__c = true;
        insert objProduct1;
        Product2 pr=new Product2();
        pr.Name='GIR_AD_LF_CB_08test';
        pr.ProductGDP__c=objProduct1.id;
        pr.ExtProductId__c='123456';
        pr.GDPGMR__c= '1234567890123456';
        insert pr;
      test.stoptest();

      String gmrcode='1234567890123456';
      WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
      WS_GMRSearch.criteriaInDataBean filters=new WS_GMRSearch.criteriaInDataBean();        
      WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();
      GMRConnection.endpoint_x=Label.CL00376;
      GMRConnection.timeout_x=40000;
      GMRConnection.ClientCertName_x=Label.CL00616;
 
       filters.businessLine = gmrcode.substring(0,2);            
       filters.productLine = gmrcode.substring(2,4);
       filters.strategicProductFamily = gmrcode.substring(4,6); 
       filters.Family = gmrcode.substring(6,8);
       try{
           Test.setMock(WebServiceMock.class, new WS_GMRSearch_TEST.WS_MOCK_GMRSearch1_TEST ());
           WS_GMRSearch.resultDataBean res=new WS_GMRSearch.resultDataBean ();
           res=GMRConnection.globalSearch(filters,Pagination); 
       }
        Catch(Exception ex){}
    }
    
     static testMethod void testWS_GMRSearch3() {
       try{
           WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();   
           Test.setMock(WebServiceMock.class, new WS_GMRSearch_TEST.WS_MOCK_GMRSearch2_TEST ());
           WS_GMRSearch.resultDataBean res=new WS_GMRSearch.resultDataBean();
           res=GMRConnection.customerCareSearch('blah'); 
       }
        Catch(Exception ex){}
    }
}