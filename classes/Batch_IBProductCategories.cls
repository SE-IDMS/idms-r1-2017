global class Batch_IBProductCategories implements Database.Batchable<sObject>,Schedulable{
    
    global Batch_IBProductCategories(){
    }
   
   
   global void execute(SchedulableContext sc) {
      Batch_IBProductCategories batch = new Batch_IBProductCategories();
      ID batchprocessid = Database.executeBatch(batch);
   }
 
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      String query = 'select brand2__c, devicetype2__c, categoryId__c, IsActive from Product2 where brand2__c <> \'\' and devicetype2__c <> \'\''; 
      
      return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<DeviceTypesPerBrand__c> devicetypesPerBrandToCreate = new list<DeviceTypesPerBrand__c>();
        List<DeviceTypesPerBrand__c> devicetypesPerBrandToDeleteList = new list<DeviceTypesPerBrand__c>();
        Set<DeviceTypesPerBrand__c> devicetypesPerBrandToDeleteSet = new set<DeviceTypesPerBrand__c>();
        List<RangesPerDeviceAndBrand__c> RangesPerDeviceAndBrandToCreate = new list<RangesPerDeviceAndBrand__c>();
        List<RangesPerDeviceAndBrand__c> RangesPerDeviceAndBrandToDeleteList = new list<RangesPerDeviceAndBrand__c>();
        Set<RangesPerDeviceAndBrand__c> RangesPerDeviceAndBrandToDeleteSet = new set<RangesPerDeviceAndBrand__c>();
        List<DeviceTypesPerBrand__c> devicetypesPerBrand = [select id, brand__c, devicetype__c, TECH_id__c from DeviceTypesPerBrand__c];
        List<RangesPerDeviceAndBrand__c> rangesPerDeviceAndBrand = [select id, brand__c, devicetype__c, category__c, TECH_id__c  from RangesPerDeviceAndBrand__c];
        Map<String,DeviceTypesPerBrand__c> devicetypesPerBrandMap = new Map<String,DeviceTypesPerBrand__c>();
        Map<String,RangesPerDeviceAndBrand__c> rangesPerDeviceAndBrandMap = new Map<String,RangesPerDeviceAndBrand__c>();
        
        for(DeviceTypesPerBrand__c D:devicetypesPerBrand){
          devicetypesPerBrandMap.put(D.TECH_id__c, D);
        }
        
        for(RangesPerDeviceAndBrand__c R:rangesPerDeviceAndBrand){
            rangesPerDeviceAndBrandMap.put(R.TECH_id__c, R);
        }
        
        for(Sobject s : scope){
         Product2 P = (Product2)s;
         
         DeviceTypesPerBrand__c devicetypePerBrand = new DeviceTypesPerBrand__c();
         devicetypePerBrand.brand__c = P.brand2__c;
         devicetypePerBrand.devicetype__c = P.devicetype2__c;
         devicetypePerBrand.TECH_id__c = P.brand2__c+'-'+P.devicetype2__c;
         
         RangesPerDeviceAndBrand__c rangePerDeviceAndBrand = new RangesPerDeviceAndBrand__c();
         rangePerDeviceAndBrand.brand__c = P.brand2__c;
         rangePerDeviceAndBrand.devicetype__c = P.devicetype2__c;
         rangePerDeviceAndBrand.category__c = P.categoryId__c;
         rangePerDeviceAndBrand.TECH_id__c = P.brand2__c+'-'+P.devicetype2__c+'-'+P.categoryId__c;
         
         if(P.IsActive == TRUE){   
           if(!devicetypesPerBrandMap.containskey(devicetypePerBrand.TECH_id__c)){
             devicetypesPerBrandToCreate.add(devicetypePerBrand);
             devicetypesPerBrandMap.put(devicetypePerBrand.TECH_id__c, devicetypePerBrand);
           }
       
           if(!rangesPerDeviceAndBrandMap.containskey(rangePerDeviceAndBrand.TECH_id__c)){
             if(P.categoryId__c != null){
               RangesPerDeviceAndBrandToCreate.add(rangePerDeviceAndBrand);
               rangesPerDeviceAndBrandMap.put(rangePerDeviceAndBrand.TECH_id__c, rangePerDeviceAndBrand);
             }
           }
         }else{
           if(devicetypesPerBrandMap.containskey(devicetypePerBrand.TECH_id__c)){
             devicetypesPerBrandToDeleteSet.add(devicetypesPerBrandMap.get(devicetypePerBrand.TECH_id__c));
           }
           
           if(rangesPerDeviceAndBrandMap.containskey(rangePerDeviceAndBrand.TECH_id__c)){
             RangesPerDeviceAndBrandToDeleteSet.add(rangesPerDeviceAndBrandMap.get(rangePerDeviceAndBrand.TECH_id__c));
           }
         }
         
        }
        
        if(devicetypesPerBrandToCreate.size() > 0){
          Database.upsert(devicetypesPerBrandToCreate);
        }
        if(RangesPerDeviceAndBrandToCreate.size() > 0){
          Database.upsert(RangesPerDeviceAndBrandToCreate);
        }
        if(devicetypesPerBrandToDeleteSet.size() > 0){
          for(DeviceTypesPerBrand__c dtpb:devicetypesPerBrandToDeleteSet){
            devicetypesPerBrandToDeleteList.add(dtpb);
          }
          Database.delete(devicetypesPerBrandToDeleteList);
        }
        if(RangesPerDeviceAndBrandToDeleteSet.size() > 0){
          for(RangesPerDeviceAndBrand__c rpdb:RangesPerDeviceAndBrandToDeleteSet){
            RangesPerDeviceAndBrandToDeleteList.add(rpdb);
          }
          Database.delete(RangesPerDeviceAndBrandToDeleteList);
        }

    }
 
    global void finish(Database.BatchableContext BC)
    {
        //Send an email to the User after your batch completes
        /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'yannick.tisserand@accenture.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch_IBProductCategories Execution Report');
        String message = '';
        mail.setPlainTextBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
}