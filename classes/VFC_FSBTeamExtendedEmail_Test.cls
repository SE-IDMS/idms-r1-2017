@isTest
private class VFC_FSBTeamExtendedEmail_Test {

    static testMethod void Method1() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
            
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.Status__c = '1. New';
            FSB.TypeofDocumenttoGenerate__c = 'FSB';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;  
            
            List<FSBTeam__c> lstTeam = new List<FSBTeam__c>();
            
            FSBTeam__c team1 = new FSBTeam__c();
            team1.FieldServiceBulletin__c = FSB.Id;
            team1.User__c = runAsUser.Id;
            team1.Role__c= 'R&D Platform Engineer';
            lstTeam.add(team1);
            
            FSBTeam__c team2 = new FSBTeam__c();
            team2.FieldServiceBulletin__c = FSB.Id;
            team2.Email__c = 'test@se.com';
            team2.Role__c= 'R&D Platform Engineer';
            lstTeam.add(team2);
            
            insert lstTeam;
            
            ApexPages.StandardSetController setController = new ApexPages.StandardSetController(lstTeam);
            PageReference pageRef = Page.VFP_FSBTeamExtendedEmail; 
            ApexPages.currentPage().getParameters().put('id', FSB.id);
            VFC_FSBTeamExtendedEmail emailteam = new VFC_FSBTeamExtendedEmail(setController);
            
            emailteam.sendEmailtoExtendedTeam();
            
        }     
    }
    
    static testMethod void Method2() {
        
        User runAsUser1 = Utils_TestMethods.createStandardUser('test');
        runAsUser1.BypassWF__c = True;
        runAsUser1.BypassVR__c = True;
        insert runAsUser1;
        
        System.runAs(runAsUser1) {
        
        
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.Status__c = '1. New';
            FSB.TypeofDocumenttoGenerate__c = 'FSB';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;  
            
            List<FSBTeam__c> lstTeam = new List<FSBTeam__c>();
            
            FSBTeam__c team1 = new FSBTeam__c();
            team1.FieldServiceBulletin__c = FSB.Id;
            team1.User__c = runAsUser1.Id;
            team1.Role__c= 'R&D Platform Engineer';
            lstTeam.add(team1);
            
            insert lstTeam;
            
            ApexPages.StandardSetController setController = new ApexPages.StandardSetController(lstTeam);
            PageReference pageRef = Page.VFP_FSBTeamExtendedEmail; 
            ApexPages.currentPage().getParameters().put('id', FSB.id);
            VFC_FSBTeamExtendedEmail emailteam = new VFC_FSBTeamExtendedEmail(setController);
            emailteam.sendEmailtoExtendedTeam();
        
        }
    }
}