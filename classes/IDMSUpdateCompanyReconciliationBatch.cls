//Batch Job for Processing the Records
global class IDMSUpdateCompanyReconciliationBatch implements Database.Batchable<SObject>,Database.AllowsCallouts{
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) {
        //Query which will be determine the scope of Records fetching the same
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        Integer lmtRecords = integer.valueof(label.CLNOV16IDMS023);        
        return Database.getQueryLocator([Select Id, AtCreate__c,AtUpdate__c, HttpMessage__c, IdmsUser__c, NbrAttempts__c from IDMSCompanyReconciliationBatchHandler__c where NbrAttempts__c <= :attemptsMaxLmt limit :lmtRecords]);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC, List<SObject> scope) {
        //Unique Batch Id 
        String BatchId = BC.getJobId();        
        List<IDMSCompanyReconciliationBatchHandler__c> recCompaniesToUpdate = new List<IDMSCompanyReconciliationBatchHandler__c>();
        for (SObject userConcScope: scope){ 
            IDMSCompanyReconciliationBatchHandler__c usr = (IDMSCompanyReconciliationBatchHandler__c)userConcScope;
            if(usr.AtUpdate__c == true){
                recCompaniesToUpdate.add(usr);
            }
        }
        if(recCompaniesToUpdate.size() > 0){
            //ToDo: do API call to Update Users in UIMS
            IdmsUimsReconcUpdateCompanyBulk.updateUimsCompanies(recCompaniesToUpdate, BatchId);
            System.debug('Idms users to Update Reconciliate: '+recCompaniesToUpdate);
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        //Below code will fetch the job Id.
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
        // schedule update company batch code starts
        system.debug('*****Inside fourth batch run****** Schedule it after each 30 minutes');
        String CRON_EXP = Label.CLNOV16IDMS060;
        IDMSUserReconcScheduleBatch   userCreateBatch = new IDMSUserReconcScheduleBatch();
        system.schedule('IDMS Users Reconciliation'+System.currentTimeMillis(), CRON_EXP, userCreateBatch);
    }
}