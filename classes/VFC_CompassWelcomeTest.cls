@isTest
public class VFC_CompassWelcomeTest {
    static testMethod void test_CompassWelcome() {
        PageReference compassPageRef = Page.VFP_CompassWelcome;
        Test.setCurrentPage(compassPageRef);
            
        VFC_CompassWelcome objCompassWelcome = new VFC_CompassWelcome(); 
            
        objCompassWelcome.password = 'test';
        objCompassWelcome.username = 'test';

        objCompassWelcome.LoginPanel = true;
        objCompassWelcome.ForgottenPasswordPanel = false;
        objCompassWelcome.HomePanel = true;
            
        Test.starttest(); 
        objCompassWelcome.login();
        objCompassWelcome.forgottenpassword();
        objCompassWelcome.ok();
        objCompassWelcome.cancel();    
        Test.stoptest();
    }
}