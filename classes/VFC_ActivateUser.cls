/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  02/12/2016 
•   Description: class used as controller for vf page activating user. 
**/

public class VFC_ActivateUser{
    public String appidNew;
    string userid       = ApexPages.currentPage().getParameters().get('id');
    public string appid = ApexPages.currentPage().getParameters().get('app');
    
    //constructor used for redirections and fetching values from custom setting
    public VFC_ActivateUser(){
        if(appid != null){
            IDMSApplicationMapping__c appMap;
            appMap = IDMSApplicationMapping__c.getInstance(appid);
            if(appMap!= null){
                appidNew = (String)appMap.get('AppId__c');
            }
        }
    } 
    
    //method used for activation process
    public pagereference activateUserAction(){
        List<User> lstuser   = [select id, federationidentifier, IDMS_Profile_update_source__c, IDMS_Registration_Source__c,isActive from user where id = :userid ];
        IDMSActivateuser activateUser = new IDMSActivateuser(); 
        if(lstuser.size() > 0){
            User UserRecord = lstuser[0];
            IDMSConfirmPinResponse response = activateUser.Activateuser(UserRecord.id,UserRecord.Federationidentifier,UserRecord.IDMS_Registration_Source__c);
        }
        IDMSApplicationMapping__c appMap;
        appMap=IDMSApplicationMapping__c.getInstance(appid);
        if(appMap != null){
            appidNew = (String)appMap.get('AppId__c');
        } 
        string appUrl= (String)appMap.UrlRedirectAfterReg__c;
        if(string.isnotblank(appUrl)){
            PageReference pg = new PageReference(appUrl);
            pg.setredirect(true);
            return pg;
        }else{     
            PageReference pg = new PageReference('/identity/idp/login?app='+appidNew);
            pg.setredirect(true);
            return pg;
        }
    }
}