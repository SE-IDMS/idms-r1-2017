/*
Author          : Rakhi Kumar - Global Delivery Team
Date Created    : 13/02/2013
Description     : May 2013 Release - BR-2175. Delete spam cases on expiry date
-----------------------------------------------------------------
                          MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : <>
Modified Date   : <>
Description     : <>
-----------------------------------------------------------------
*/
global class SCHEDULE_CASE_DeleteSpamCases implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Id batchInstanceId = Database.executeBatch(new BATCH_CASE_DeleteSpamCasesOnExpiryDate(), 1000); 
    }
}