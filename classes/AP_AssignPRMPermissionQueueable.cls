public without sharing class AP_AssignPRMPermissionQueueable implements Queueable {

    List<Id> UserContactBatchLst {get;set;}
    String TargetApplicationRequested {get;set;}
    AP_PRMAppConstants.ProcessPendingQueueItems ProcessType {get;set;}
    
    public class CodeCoverageException extends Exception {
        public String message = 'This exception is thrown to do code coverage';
    }
    public static Boolean throwExceptionInTest = false;
    
    public AP_AssignPRMPermissionQueueable(List<Id> userContactBatchLst, String targetApplicationRequested, AP_PRMAppConstants.ProcessPendingQueueItems processType){
        system.debug('**Constructor Called**');
        this.UserContactBatchLst = userContactBatchLst;
        this.TargetApplicationRequested = targetApplicationRequested;
        this.ProcessType = processType;
    }
    
    public void execute(QueueableContext context) {
        Map<Id,Contact> newRecords;
        System.debug ('** Execute Contactid :: Context**' + userContactBatchLst +' ' + context);  
        System.debug ('** ProcessType: ' + ProcessType);

        try {
            system.debug('**Target Application **'+targetApplicationRequested);
            if (this.ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER || 
                this.ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP) {
                if (this.TargetApplicationRequested == System.Label.CLMAR16PRM011) {
                    System.debug ('*** Initiating Permission assignment');
                    AP_PRMUtils.AssignPortalPermissions(this.UserContactBatchLst);
                    
                    newRecords = new Map<Id,Contact>([SELECT Id, Name, PRMUIMSId__c, PRMApplicationRequested__c, AccountId, FieloEE__Member__c FROM Contact WHERE Id IN :this.UserContactBatchLst AND FieloEE__Member__c != '']);

                    System.debug('*** Contact Member records: ' + newRecords);
                    // TODO: Capture LIMIT Exception and add an entry to PRMRequestQueue__c
                    try{
                        if(throwExceptionInTest == False && Test.isRunningTest()){
                         throw new CodeCoverageException('Exception to do the code coverage');
                        }
                        
                       if (newRecords.size() > 0)
                           ID jobID = System.enqueueJob (new AP_ProcessPRMRegistration(newRecords, AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES));
                    
                    }
                    /*catch (LimitException ltEx){
                           Map<Id,PRMRequestQueue__c> cId = new Map<Id,PRMRequestQueue__c>();
                           List<PRMRequestQueue__c> prmReqQueueLst = new List<PRMRequestQueue__c>([SELECT Id,AccountId__c, ContactId__c, RequestType__c, ProcessingStatus__c,RunCounter__c FROM PRMRequestQueue__c WHERE RequestType__c = 'UPDMEMPROP' AND ContactId__c IN :newRecords.keyset()]);
                           for(PRMRequestQueue__c p : prmReqQueueLst)
                               cId.put(p.ContactId__c,p);
                           for(Contact cc : newRecords.values()){
                               if(cId.containskey(cc.Id)){
                                   cId.get(cc.Id).ProcessingStatus__c = 'Pending';
                                   cId.get(cc.Id).RunCounter__c = cId.get(cc.Id).RunCounter__c + 1;
                                   cId.get(cc.Id).ErrorInfo__c = ltEx.getMessage();
                                   prmReqQueueLst.add(cId.get(cc.Id));
                               }
                               else{
                                   PRMRequestQueue__c prmReqQueue1 = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ErrorInfo__c = ltEx.getMessage(), RunCounter__c = 1, ContactId__c = cc.Id, RequestType__c = 'UPDMEMPROP',ProcessingStatus__c='Pending');
                                   prmReqQueueLst.add(prmReqQueue1);
                               }
                           }                                                     
                           if(prmReqQueueLst != Null && prmReqQueueLst.size()>0)
                               UPSERT prmReqQueueLst;
                    }*/
                    Catch (Exception ex) {
                        System.debug('**Exception Caught AP_AssignPRMPermissionQueueable**'+ex.getmessage()+' '+ex.getStackTraceString());
                        Map<Id,PRMRequestQueue__c> cId = new Map<Id,PRMRequestQueue__c>();
                           List<PRMRequestQueue__c> prmReqQueueLst = new List<PRMRequestQueue__c>([SELECT Id,AccountId__c, ContactId__c, RequestType__c, ProcessingStatus__c,RunCounter__c FROM PRMRequestQueue__c WHERE RequestType__c = 'UPDMEMPROP' AND ContactId__c IN :newRecords.keyset()]);
                           for(PRMRequestQueue__c p : prmReqQueueLst)
                               cId.put(p.ContactId__c,p);
                           for(Contact cc : newRecords.values()){
                               if(cId.containskey(cc.Id)){
                                   cId.get(cc.Id).ProcessingStatus__c = 'Pending';
                                   cId.get(cc.Id).RunCounter__c = cId.get(cc.Id).RunCounter__c + 1;
                                   cId.get(cc.Id).ErrorInfo__c = ex.getMessage();
                                   prmReqQueueLst.add(cId.get(cc.Id));
                               }
                               else{
                                   PRMRequestQueue__c prmReqQueue1 = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ErrorInfo__c = ex.getMessage(), RunCounter__c = 1, ContactId__c = cc.Id, RequestType__c = 'UPDMEMPROP',ProcessingStatus__c='Pending');
                                   prmReqQueueLst.add(prmReqQueue1);
                               }
                           }                                                     
                           if(prmReqQueueLst != Null && prmReqQueueLst.size()>0)
                               UPSERT prmReqQueueLst;
                    }
                } else {
                    System.debug('****');
                    if (UserContactBatchLst != null && UserContactBatchLst.size()>0){
                        System.debug('Hey');
                        AP_PRMUtils.AssignPOMPOnlyPermissions(UserContactBatchLst);
                    }            
                }
            }
        } Catch (Exception ex) {
            System.debug('**Exception Caught AP_AssignPRMPermissionQueueable**'+ex.getmessage()+' '+ex.getStackTraceString());
        }
    }
}