public class VFC_Contract_AddProduct extends VFC_ControllerBase {

    public pkgGMRSearch GMRsearchForPkgTemplate{get;set;}
    public PackageTemplate__c pkgTemplate {get;set;}
    ContractProductInformation__c commRef ; // New Commecial Reference to be inserted.    
    OPP_Product__c existingProd = new OPP_Product__c(); // To get the Product already stored in bFO from OPP_Product__c object.

    /*=======================================
    INNER CLASSES
    =======================================*/ 
    // GMR implementation for Cases
    public class pkgGMRSearch implements Utils_GMRSearchMethods
    {
        VFC_Contract_AddProduct thisController;
        public Void Init(String ObjID, VCC08_GMRSearch Controller)
        {
            thisController = (VFC_Contract_AddProduct)Controller.pageController;

        }

        // Cancel method returns the associated Case detail page
        public PageReference Cancel( VCC08_GMRSearch Controller)
        { 
            String url = '/'+thisController.commRef.PackageTemplate__c;
            PageReference pageRef = new PageReference(url);
            pageRef.setRedirect(true);
            return pageRef;
        }   
        // Action performed when clicking on "Select" on the PM0 search results
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
        {

            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)controllerBase;
            DataTemplate__c Product = (DataTemplate__c)obj;
            
            


            if(GMRController.ActionNumber==1)
            {
                if(Product.field8__c != null)
                    thisController.commRef.CommercialReference__c = Product.field8__c; 
                if(Product.field6__c != null)
                    thisController.commRef.ProductSuccession__c = Product.field6__c;
                if(Product.field9__c != null)
                    thisController.commRef.ProductDescription__c = Product.field9__c; 
                if(Product.field5__c  != null)
                    thisController.commRef.SubFamily__c = Product.field5__c; 
                if(Product.field7__c != null)
                    thisController.commRef.ProductGroup__c = Product.field7__c; 
                if(Product.field11__c != null)
                    thisController.commRef.TECH_PM0CodeInGMR__c = Product.field11__c;//.substring(0,8);
            }
            if(GMRController.ActionNumber==2)
            {
                if(Product.field10__c != null)
                    thisController.commRef.TECH_PM0CodeInGMR__c = Product.field10__c;//.substring(0,8);
            }
           
            if(Product.field4__c != null)
                thisController.commRef.Family__c = Product.field4__c;           
            if(Product.field1__c != null)
                thisController.commRef.ProductBU__c = Product.field1__c;
            if(Product.field2__c != null)
                thisController.commRef.ProductLine__c = Product.field2__c;
            if(Product.field3__c != null)
                thisController.commRef.ProductFamily__c = Product.field3__c;
           
                
            if(thisController.pkgTemplate.Id != null)
                thisController.commRef.PackageTemplate__c =  thisController.pkgTemplate.Id;
            else 
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,Label.CLSEP12CCC57));
                return null;
            }  
            
            try
            {
                insert thisController.commRef;
                String url = '/'+thisController.commRef.PackageTemplate__c;
                PageReference pageRef = new PageReference(url);
                pageRef.setRedirect(true);
                return pageRef;
            } 
            catch(Exception e)
            {   String errorMessage=e.getMessage();
                if (errorMessage.contains(Label.CLSEP12CCC55)){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CLSEP12CCC56));
                }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'An error occured while adding product:'+ e.getMessage()));
                }
                return null;
            }
            return null;
        }
    }
    public VFC_Contract_AddProduct(ApexPages.StandardController controller)
    {
        commRef =  new ContractProductInformation__c();
        commRef = (ContractProductInformation__c)controller.getRecord();
        //pkgTemplateId = commRef.PackageTemplate__c;
        if(commRef.PackageTemplate__c != null)
            pkgTemplate = [select id,name from PackageTemplate__c where id = : commRef.PackageTemplate__c limit 1];
        GMRsearchForPkgTemplate = new pkgGMRSearch();
    }
}