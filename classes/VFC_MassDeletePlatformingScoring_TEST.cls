/* 
    Author          : Pooja Suresh 
    Date Created    : 01/08/2013 - October 2013 release 
    Description     : Test class for the apex class VFC_MassDeletePlatformingScoring for mass deletion of platforming scoring.
*/   

@isTest
private class VFC_MassDeletePlatformingScoring_TEST 
{

  static testmethod void deletePlatformingsTestMethod(){
    
    Profile profile = [select id from profile where name='System Administrator'];
        
    User user = Utils_TestMethods.createStandardUser(profile.id,'TUser1');
    user.BypassVR__c=True;
    user.CAPRelevant__c=True;
    Database.insert(user);    
    
    User user1 = Utils_TestMethods.createStandardUser(profile.id,'RunUser');
    Database.insert(user1);
    
    Account account1 = new Account(Name='TestAccount', AccountLocalName__c='LocalAccountName', Street__c='New Street', City__c='City', POBox__c='XXX', OwnerId = user.id, ZipCode__c='12345', ClassLevel1__c='FI');
    Account account2 = new Account(Name='TestAccount', AccountLocalName__c='LocalAccountName', Street__c='New Street', City__c='City', POBox__c='XXX', OwnerId = user.id, ZipCode__c='12345', ClassLevel1__c='FI');
    Account account3 = new Account(Name='TestAccount', AccountLocalName__c='LocalAccountName', Street__c='New Street', City__c='City', POBox__c='XXX', OwnerId = user.id, ZipCode__c='12345', ClassLevel1__c='FI');
    List<Account> accounts = new Account[]{account1 ,account2, account3};
    Database.insert(accounts); 
    
    
    SFE_IndivCAP__c Cap= Utils_TestMethods.createIndividualActionPlan(user.id);
    cap.ownerId= user.id;
    database.insert(cap);
    
    List<SFE_PlatformingScoring__c> selectedPlatformingScoring = new List<SFE_PlatformingScoring__c>();

    for(integer i=0;i<3;i++)
    {
        selectedPlatformingScoring.add(Utils_TestMethods.createPlatformScoring(cap.id,accounts[i].id,15,'1'));
        
    }
    
    database.insert(selectedPlatformingScoring);
    
    test.startTest();

    system.runAs(user1)
    {
        Pagereference pg = new pagereference('/apex/VFP_MassDeletePlatformingScoring?id='+cap.id); 
        Test.setCurrentPageReference(pg);           
        
        VFC_MassDeletePlatformingScoring deleteScore = new VFC_MassDeletePlatformingScoring(new ApexPages.StandardController(cap));
        deleteScore.ClassificationLvl1 = 'No Filter';
        deleteScore.getAccountClassification();
        //deleteScore.getPlatformingScoring();
        deleteScore.searchRecords(); 
        
        try
        {
            deleteScore.platformingScoringList[0].selected=true;
            deleteScore.massDeletion();                
        }
        catch (DMLException e)
        {
           system.assert(e.getMessage().contains(Label.CL00440));                        
        }
        
        deleteScore.accCity = 'City';
        deleteScore.ClassificationLvl1 = 'EU';
        deleteScore.getAccountClassification();
        deleteScore.searchRecords();
        
        deleteScore.clearSearch();          
        
        VFC_MassDeletePlatformingScoring deleteScore1 = new VFC_MassDeletePlatformingScoring(new ApexPages.StandardController(cap));
        deleteScore1.ClassificationLvl1 = 'No Filter';
        deleteScore1.getAccountClassification();
        //deleteScore1.getPlatformingScoring();
        deleteScore1.searchRecords();
        try
        {
            deleteScore1.massDeletion();                
        }
        catch (DMLException e)
        {
           system.assert(e.getMessage().contains(Label.CL00440));                        
        }
        
    }
    
          system.runAs(user)
    {
        Pagereference pge = new pagereference('/apex/VFP_MassDeletePlatformingScoring?id='+cap.id); 
        Test.setCurrentPageReference(pge);         
            
        VFC_MassDeletePlatformingScoring deleteScoring = new VFC_MassDeletePlatformingScoring(new ApexPages.StandardController(cap));
       
        deleteScoring.ClassificationLvl1 = 'No Filter';
        deleteScoring.getAccountClassification();

        deleteScoring.searchRecords(); 
        
        deleteScoring.platformingScoringList[0].selected=true;
        deleteScoring.massDeletion();
        
            
        SFE_IndivCAP__c indivCap = Utils_TestMethods.createIndividualActionPlan(user.id);   
        indivCap.ownerId= user.id;
        indivCap.status__c=Label.CL00444;       
        database.insert(indivCap);
        
        VFC_MassDeletePlatformingScoring deleteScorings = new VFC_MassDeletePlatformingScoring(new ApexPages.StandardController(indivCap));
        try
        {
            deleteScorings.ClassificationLvl1 = 'No Filter';
            deleteScorings.getAccountClassification();
            deleteScorings.searchRecords(); 
            deleteScorings.platformingScoringList[0].selected=true;
            deleteScorings.massDeletion();            
        }
        catch (DMLException e)
        {
           system.assert(e.getMessage().contains(Label.CL00441));                        
        }                   
    
     }  
      
    test.stopTest();
    }     
    
}