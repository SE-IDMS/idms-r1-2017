/*
    Author          : Abhishek (ACN) 
    Date Created    : 01/08/2011
    Description     : Test class for the apex class VFC48_MassDelete .
*/
@isTest
private class VFC49_cloneCAP_TEST {
    
/* @Method <This method deletePlatformingsTestMethod is used test the VFC49_cloneCAP class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Error>
*/
    static testMethod void cloneCapTestMethod()
    {
   //  Profile profile = [select id from profile where name='System Administrator'];
    Id profileId=null;
    List<String> profilePrefixList=(System.Label.CL00632).split(',');//the valid profile prefix list is populated            
    if(profilePrefixList.size()>0){
    List<Profile> allProfiles=Database.query('select id from Profile where Name Like \''+profilePrefixList[0]+'%\' limit 1');
        if(allProfiles.size()>0)
        {
            profileId=allProfiles[0].id;
        }
    }        
    User user = Utils_TestMethods.createStandardUser('TestUser');
    user.profileId=profileId;
    user.BypassVR__c=True;
    user.CAPRelevant__c=True;
    Database.insert(user);   
            
    Account accounts = Utils_TestMethods.createAccount();
    accounts.ownerId=user.id;
    accounts.PrimaryRelationshipLeader__c=Label.CLDEC14SLS02;
    Account accounts1 = Utils_TestMethods.createAccount();
    accounts1.ownerId=user.id;
    accounts1.PrimaryRelationshipLeader__c=Label.CLDEC14SLS03;
    Account accounts2 = Utils_TestMethods.createAccount();
    accounts2.ownerId=user.id;
    accounts2.PrimaryRelationshipLeader__c=Label.CLDEC14SLS03;
    Account accounts3 = Utils_TestMethods.createAccount();
    accounts3.ownerId=user.id;
    accounts3 .PrimaryRelationshipLeader__c=Label.CLDEC14SLS02;
    
    List<Account> allaccounts=new List<Account>{accounts,accounts1,accounts2,accounts3};
    Database.insert(allaccounts); 
        
    list<SFE_IndivCAP__c> Cap= new list<SFE_IndivCAP__c>();
    
    for(integer i=0;i<200;i++)
    {     
        SFE_IndivCAP__c caps = Utils_TestMethods.createIndividualActionPlan(user.id);   
        caps.assignedTo__c= user.id;
        caps.OwnerId=user.id;
        cap.add(caps);
    }            
    database.insert(cap);
    
    list<SFE_PlatformingScoring__c> scorings = new list<SFE_PlatformingScoring__c>();
    
    for(integer i=0;i<2;i++)
    {
        scorings.add(Utils_TestMethods.createPlatformScoring(cap[i].id,accounts.id,15,'1'));        
    }
    database.insert(scorings);
        test.startTest();
    System.runAs(user){

            ApexPages.StandardController sc1= new ApexPages.StandardController(cap[0]);
            Pagereference p = page.VFP49_cloneCAP;
            Test.setCurrentPageReference(p);             
            VFC49_cloneCAP cloned= new VFC49_cloneCAP(sc1);
            cloned.processhere=4;
            cloned.BatchSize=1;
            cloned.cloneCAP();
            
            scorings.add(Utils_TestMethods.createPlatformScoring(cap[0].id,accounts1.id,13,'1'));
            scorings.add(Utils_TestMethods.createPlatformScoring(cap[0].id,accounts2.id,12,'1'));
            scorings.add(Utils_TestMethods.createPlatformScoring(cap[0].id,accounts3.id,12,'1'));
                        
            database.upsert(scorings);            
            List<Id> templist=new List<Id>{scorings[1].id,scorings[2].id,scorings[3].id};
            cloned.callFutureMethods(templist);

    }
    
    
            ApexPages.StandardController sc2= new ApexPages.StandardController(cap[1]);
            Pagereference pp = page.VFP49_cloneCAP;
            Test.setCurrentPageReference(pp);             
            VFC49_cloneCAP cloned1= new VFC49_cloneCAP(sc2);   
            cloned1.cloneCap();         
test.stopTest();
          
  }
}