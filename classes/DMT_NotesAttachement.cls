public with sharing class DMT_NotesAttachement {

    public PageReference FilterChange() {
        NtsAttach = null;        
        return null;
    }
public DMT_NotesAttachement() {

RFilter = 'A';
Fdate =Date.Today().addMonths(-6);
    Tdate = Date.Today();
}

public list<NotesAttachment> NtsAttach{get;set;}


public Date Fdate{get;set;}
public Date Tdate{get;set;}
Public string RFilter{get;set;}

public pagereference AllNotesAndAttachments() {
    
    NtsAttach = new list<NotesAttachment> (); 
    for(PRJ_ProjectReq__c PR: [select id, Name,NextStep__c,ProjectRequestID__c, (select Parentid, Title from NotesAndAttachments) from PRJ_ProjectReq__c  where NextStep__c not in ('Archive', 'Cancelled', 'Postponed','Rejected') and CreatedDate >= :Fdate and CreatedDate <= :Tdate]){
    
     NotesAttachment  nts = new NotesAttachment();
     NoteAndAttachment natch = new NoteAndAttachment();
     
    if(PR.NotesAndAttachments.size() > 0)
    {
      natch = PR.NotesAndAttachments[0];
      nts.Title = natch.Title;
    }
      
     nts.id = PR.id;
     nts.Prjcode = PR.ProjectRequestID__c;
     nts.Name = PR.Name;
     nts.Status = PR.NextStep__c;
    
   if(RFilter == 'B'){
    if (nts.Title != null)
     NtsAttach.add(nts);
     }
   else 
     NtsAttach.add(nts);
     
    }
  
   return null;     
}

public class NotesAttachment{
    public String id {get; set;}
    public String Prjcode {get; set;}
    public String Name {get; set;}  
    public String Title {get; set;}
    public String Status {get; set;}
    
    public NotesAttachment(){}  

    public NotesAttachment(string id, string name, string p,string t, string s){
        this.id = id;
        this.Prjcode = p;
        this.Name = p;
        this.title = t;
        this.status = s;

    }
   }
   public List<SelectOption> getReportFilter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('A','All Project Requests'));       
       options.add(new SelectOption('B','PRs with Notes & Attachments'));  
      
       return options;
     }
}