public without sharing class FieloPRM_AP_AccountTriggers{

    public static Boolean isActive = true;
    
    public static void addAccountsToTeam(){
        Set<Id> modifiedAccountsIds = new Set<Id>();
    
        for(Id thisId: Trigger.oldMap.keySet()){
            //If team changed and new team is not null, account has been asociated to a new team.
            if(Trigger.oldMap.get(thisId).get('F_PRM_ProgramTeam__c') != Trigger.newMap.get(thisId).get('F_PRM_ProgramTeam__c') && Trigger.newMap.get(thisId).get('F_PRM_ProgramTeam__c') != null){
                modifiedAccountsIds.add(thisId);
            }
        }
        
        List<Account> accountsToProcess = [SELECT Id, F_PRM_ProgramTeam__c, (SELECT Id, F_PRM_ContactRegistrationStatus__c FROM Members__r) FROM Account WHERE Id IN :modifiedAccountsIds];
        
		//Mark members for batch adding to team
		Set<Id> memberIdsToAdd = new Set<Id>();
        for(Account acc: accountsToProcess){
            for(FieloEE__Member__c member: acc.Members__r){
				if (member.F_PRM_ContactRegistrationStatus__c == 'Validated'){
					memberIdsToAdd.add(member.Id);
				}
            }
        }
        
		//Adding members to team
		List<FieloCH__TeamMember__c> newTeamMembers = FieloPRM_Utils_ChTeam.addTeamMember(memberIdsToAdd);
		for (FieloCH__TeamMember__c tm : newTeamMembers){
			tm.F_PRM_Status__c = 'PENDING';
		}
		update newTeamMembers;

		//level accounts
		FieloPRM_Utils_ChTeam.accountsLeveling(accountsToProcess);
    }

}