@isTest
public class IDMSValidateReCaptchaTest implements HttpCalloutMock {
    private HttpResponse resp;
    
    public IDMSValidateReCaptchaTest(String testBody) {
        resp = new HttpResponse();
        resp.setBody(testBody);
        resp.setStatusCode(200);
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
 
    public testmethod static void testWS() {
        String testBody = 'This is a test :-)';
        
        HttpCalloutMock mock = new IDMSValidateReCaptchaTest(testBody);
        Test.setMock(HttpCalloutMock.class, mock);
 
        //IDMSValidateReCaptcha callout = new IDMSValidateReCaptcha();
        Boolean resp = IDMSValidateReCaptcha.validateReCaptcha(testBody);
        
        //System.assertEquals(200, resp.getStatusCode());        
        //System.assertEquals(resp.getBody(), testBody );
    }
}