global class SyncObjectsBatch implements Database.Batchable<sObject>{

	private ObjectSync__c syncObjectsRecord;
	private SyncObjects settings;

	public SyncObjectsBatch(ObjectSync__c syncObjectsRecord){
		this.syncObjectsRecord = syncObjectsRecord;

	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		string query = '';
		if(syncObjectsRecord.Condition__c != null){
			query = 'SELECT Id FROM ' + syncObjectsRecord.FromObject_API_Name__c + ' WHERE ' + syncObjectsRecord.Condition__c;
		}else{
			query ='SELECT Id FROM ' + syncObjectsRecord.FromObject_API_Name__c 	;
		}

		if(test.isrunningtest()){
			query += ' LIMIT 1 ';
		}
		return Database.getQueryLocator(query);
		
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){

		settings = new SyncObjects(syncObjectsRecord);
		//the method "getFromRecords" uses the ids 
		settings.fromSincronize = settings.getFromRecords(scope);
		settings.inBatchContext = true;
		//runs sync 
		settings.syncronize(false);
	} 
  
	global void finish(Database.BatchableContext BC){
	}
}