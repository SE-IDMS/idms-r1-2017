/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date:                                             *
* Tested Class: Fielo_InvoiceUploadAttachmentController     *
************************************************************/

@isTest
public with sharing class Fielo_InvoiceUploadAttachmentCtrTest{
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
            
        ApexPages.currentPage().getParameters().put('attachmentName', 'attch');
        Fielo_InvoiceUploadAttachmentController test = new Fielo_InvoiceUploadAttachmentController();
        test.documentbody = Blob.valueOf('Mi nombre es Ramiro esta es una clase de test');
        test.fileName = 'test.jpg';
        test.doUploadFile();
        test.doDeleteDocument();
        
    }
}