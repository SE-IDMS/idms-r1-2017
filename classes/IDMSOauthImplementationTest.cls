@isTest
public class IDMSOauthImplementationTest {
    static testmethod void testDoGet(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/oauth2/authorize/App/';  
        req.httpMethod = 'GET';
        RestContext.request= req;
        RestContext.response= res;
        IDMSOauthImplementation.doGet();
    }
}