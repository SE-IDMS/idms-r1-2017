/* 
    
    Author: Amitava Dutta
    Created On: 25/01/2011.
    Purpose: Updating / associating the Manager with Record Owner.
    Basically it is related to 'Related To' Field setting.

*/

@isTest
private class VFC18_ChangeManagerInfo_Test
{    
  //Region Defining the Class Variables
    public static VFC18_ChangeManagerInfo objChangeManagerInfo = null;
    public static ApexPages.StandardController objController;
  //End of Defining the Class Variables
    
  //Region Test Methods
    //-------Check the Manager Info Modification Code ---
        static TestMethod void TestChangeManagerInfo()
        {   
          /*
            Test.startTest();   
                List<User> objLstUserForTest = null;
                ObjectivesSettings__c objectivesettings = Utils_TestMethods.createObjectiveSettings();
                insert objectivesettings;
                List<ObjectivesSettings__c> objSettings = new List<ObjectivesSettings__c>();
                objSettings.add(objectivesettings);
                if(objSettings.size() !=0)
                {
                    ApexPages.currentPage().getParameters().put('OpSetID',objSettings[0].ID);
                    //Region Constructor
                        objChangeManagerInfo = new VFC18_ChangeManagerInfo(objController);
                    //End
                    objChangeManagerInfo.ObjectiveSettingsID =  ApexPages.currentPage().getParameters().get('OpSetID');
                   
                    // Testing the Searching Operation without Search Text
                    objChangeManagerInfo.SearchString = '';        
                    objChangeManagerInfo.Search();

                    // Testing Searching Operation with Search Text
                    objChangeManagerInfo.SearchString = 'Ant';        
                    objChangeManagerInfo.Search();
                    
                    // Test Main Method
                    objChangeManagerInfo.ChangeUser();  
                    objChangeManagerInfo.CloseWindow();
                }      
            Test.StopTest();
            */
        }
  //Region End of Test Methods
}