/**

 */
@isTest
private class VFC_OpenERPAccountData_Test {

    static testMethod void myUnitTest() {
        Account Acc = Utils_TestMethods.createAccount();
        insert Acc;
        
        PageReference vfPage = Page.VFP_OpenERPAccountData;
        Test.setCurrentPage(vfPage);
        
        VFC_OpenERPAccountData vfc = new VFC_OpenERPAccountData(new ApexPages.StandardController(acc ));
        vfc.redirect();
        
        Acc.SEAccountID__c = '122345';
        acc.AccountStatus__c = 'CM';
        Update Acc;
        
        VFC_OpenERPAccountData vfc2 = new VFC_OpenERPAccountData(new ApexPages.StandardController(Acc));
        vfc2.redirect();
        
    }
}