public class Ap_CaseServicesFunc
{
    public Static Void  CaseSrvContUpdate(List<Case> CaseList) 
    {
        set<Id> scid = New set<Id>();
        for(Case s: CaseList){
            if(s.SVMXC__Service_Contract__c != null){
                scid.add(s.SVMXC__Service_Contract__c);
            }
        }
        Map<Id,SVMXC__Service_Contract__c> screcMap = New Map<Id,SVMXC__Service_Contract__c>();
        if(scid !=null && scid.size()>0){
            List<SVMXC__Service_Contract__c> sclst =[Select Id,ParentContract__c From SVMXC__Service_Contract__c Where Id in:scid];
            screcMap.putall(sclst);
        }
        for(Case c:CaseList){
        
            if(c.SVMXC__Service_Contract__c != null){
                
                if(screcMap.containskey(c.SVMXC__Service_Contract__c )){
                    if(screcMap.get(c.SVMXC__Service_Contract__c ).ParentContract__c !=null){
                        c.ServiceLine__c = screcMap.get(c.SVMXC__Service_Contract__c ).ParentContract__c;
                    }
                    else{
                        c.ServiceLine__c =c.SVMXC__Service_Contract__c;
                    }
                }
            }
            else{
                c.ServiceLine__c=null;
            
            }
        }   
    }
    public Static Void  CaseIPCommRefUpdate(List<Case> CaseList) 
    {
        Set<Id> IpIdset = new set<Id>();
        List<SVMXC__Installed_Product__c> iplist = new List<SVMXC__Installed_Product__c>();
        for(Case s :CaseList){
            
            if(s.SVMXC__Component__c!=null){
                IpIdset.add(s.SVMXC__Component__c);
            }
        }
        
        Map<Id,SVMXC__Installed_Product__c> iprecMap = New Map<Id,SVMXC__Installed_Product__c>();
        if(IpIdset.size()>0 && IpIdset !=null){
            iplist =[Select Id,SVMXC__Product__c,SVMXC__Product__r.ProductGDP__c From SVMXC__Installed_Product__c Where Id in:IpIdset ];
            iprecMap.putall(iplist);
        }
        for(Case s :CaseList){
            
            if(s.SVMXC__Component__c==null){
                s.CommercialReference_lk__c=null;
            }
            else if(iprecMap.containskey(s.SVMXC__Component__c)){
                if(iprecMap.get(s.SVMXC__Component__c).SVMXC__Product__r.ProductGDP__c !=null){
                    s.CommercialReference_lk__c=iprecMap.get(s.SVMXC__Component__c).SVMXC__Product__c;
                }
            }

        }
    }
}