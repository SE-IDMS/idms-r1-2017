/**
Description: This covers IDMSUpdateUserAIL
Release: Sept 2016
**/

@isTest
class IDMSUpdateUserAILTest{
    //This test method is used to test update AIL record
    static testmethod void testIDMSUpdateUserAILRestService(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/IDMSUpdateUserAIL';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        
        User userObj1 = new User(alias = 'user', email='user' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000',FederationIdentifier ='Pras-Kuld1234',IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111');
        
        insert userObj1;
        System.assert(userObj1!=null);
        
        test.Starttest();
        IDMSUserAIL__c idmsUserAIL=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAclType__c='Application',IDMSAcl__c='MYSE',IDMSFederationIdentifier__c=userObj1.FederationIdentifier,
                                                      IDMSOperation__c='Grant',IDMSUser__c=userObj1.Id,IDMS_Profile_update_source__c='MYSE');
        IDMSResponseWrapperAIL responseWrapper=IDMSUpdateUserAILRest.doPut(idmsUserAIL);
        System.assert(responseWrapper!=null);
        IDMSUserAIL__c idmsUserAIL1=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAcl__c='MYSE',IDMSFederationIdentifier__c=userObj1.FederationIdentifier,
                                                       IDMSOperation__c='Grant',IDMSUser__c=userObj1.Id,IDMS_Profile_update_source__c='MYSE');
        IDMSResponseWrapperAIL responseWrapper1=IDMSUpdateUserAILRest.doPut(idmsUserAIL1);
        System.assert(responseWrapper1!=null);
        IDMSUserAIL__c idmsUserAIL2=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAclType__c='Application',IDMSAcl__c='MYSE',
                                                       IDMSOperation__c='Grant',IDMSUser__c=userObj1.Id,IDMS_Profile_update_source__c='MYSE');
        IDMSResponseWrapperAIL responseWrapper2=IDMSUpdateUserAILRest.doPut(idmsUserAIL2);
        System.assert(responseWrapper2!=null); 
        IDMSUserAIL__c idmsUserAIL3=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAclType__c='Application',IDMSAcl__c='MYSE',
                                                       IDMSOperation__c='Grant',IDMSUser__c=userObj1.Id,IDMS_Profile_update_source__c='MYSE1');
        IDMSResponseWrapperAIL responseWrapper3=IDMSUpdateUserAILRest.doPut(idmsUserAIL3);
        System.assert(responseWrapper3!=null);
        IDMSUserAIL__c idmsUserAIL4=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAclType__c='Application',IDMSAcl__c='MYSE',
                                                       IDMSOperation__c='Grant',IDMSUser__c='007A0304025bQZjIAM',IDMS_Profile_update_source__c='MYSE');
        IDMSResponseWrapperAIL responseWrapper4=IDMSUpdateUserAILRest.doPut(idmsUserAIL4);
        System.assert(responseWrapper4!=null);
        IDMSUserAIL__c idmsUserAIL5=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAclType__c='Application',IDMSAcl__c='MYSE',
                                                       IDMSOperation__c='Grant',IDMS_Profile_update_source__c='MYSE');
        IDMSResponseWrapperAIL responseWrapper5=IDMSUpdateUserAILRest.doPut(idmsUserAIL5);
        System.assert(responseWrapper5!=null);
        IDMSUserAIL__c idmsUserAIL6=new IDMSUserAIL__c(CurrencyIsoCode='EUR',IDMSAclType__c='Application',IDMSAcl__c='MYSE',IDMSFederationIdentifier__c=userObj1.FederationIdentifier,
                                                       IDMSOperation__c='Grant',IDMS_Profile_update_source__c='MYSE');
        IDMSResponseWrapperAIL responseWrapper6=IDMSUpdateUserAILRest.doPut(idmsUserAIL6);
        System.assert(responseWrapper6!=null);
        IDMSResponseWrapperAIL responseWrapper9=new IDMSResponseWrapperAIL();  
        test.Stoptest(); 
    }
    
}