/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 20-oct-2012
    Description     : Test Class for ConnectGlobalKpiTargetTriggers
*/

@isTest
private class ConnectEntityKPIContributorTriggers_Test {
    static testMethod void testConnectEntityKPIContributorTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('conntsZZ');
        User runAsUser2 = Utils_TestMethods_Connect.createStandardConnectUser('conntsZZZ');
        User runAsUser3 = Utils_TestMethods_Connect.createStandardConnectUser('conntsZZZ3');
        System.runAs(runAsUser){
        
         Initiatives__c inti=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransPeople);
         insert inti;
         
         Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',User__c = runasuser.id, Program_Leader_2__c = runasuser2.id,Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp; 
         
         Project_NCP__c cp2=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp2',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id,Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp2;
                
         Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp.id, Transformation__c = Label.ConnectTransPeople, Global_Business__c = 'Industry');
         insert GKPIT;
         
          Global_KPI_Targets__c GKPIT2=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp2.id, Transformation__c = Label.ConnectTransPeople,Global_Business__c = 'Industry');
         insert GKPIT2;
         
         Cascading_KPI_Target__c EKPI = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI;
         
         Entity_KPI_Contributors__c EKPICont = new Entity_KPI_Contributors__c(Cascading_KPI_Target__c = EKPI.id, User__c = runasuser.id,Contributor_Permission__c = 'ReadOnly');
         insert EKPICont;
         EKPICont.User__c = label.Connect_Email;
         update EKPICont;
         
         Entity_KPI_Contributors__c EKPICont1 = new Entity_KPI_Contributors__c(Cascading_KPI_Target__c = EKPI.id, User__c = runasuser.id,Contributor_Permission__c = 'ReadOnly');
         insert EKPICont1;
         delete EKPICont1;
        
          }
         }
        }