/**
* @author Anil Sistla
* @date 05/11/2016
* @description Custom batch class to create registration pools. Processes an iterable of
*               RegistrationPoolInfo. RegistrationPoolIterator returns a list channels
*               for which the pools need to be either created or replenished in the start method
*               Each pool of registration is created by invoking AP_PRMRegistrationPoolingService
*               ProcessChannelRegistrationPools method.
*               It is preferable to run the batch with size of 1 so one channel pool is processed
*               During mass campaigns, ensure to increase the pool threshold by setting multiples of
*               100 with each run limiting to 100 to avoid running into issues
*/
global without sharing class Batch_PRM_ProcessRegistrationPools
                implements Database.Batchable<AP_PRMRegistrationPoolingService.RegistrationPoolInfo>,Schedulable,
                            Database.Stateful {

    private AP_PRMAppConstants.ProcessRegistrationPool typeToProcess;

    public Batch_PRM_ProcessRegistrationPools() {
        ApexLoggingService.Batch_Logs = TRUE;
        System.debug('**Batch_LogsUpd :'+ApexLoggingService.Batch_Logs);
    }

    public Batch_PRM_ProcessRegistrationPools(AP_PRMAppConstants.ProcessRegistrationPool typeToProcess){
        this();
        System.debug('**Batch_LogsUpd123 :'+ApexLoggingService.Batch_Logs);
        this.typeToProcess = typeToProcess;
    }


    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Batchable interface start method returns an instance of custom object
    *               iterator instead of standard Database.queryLocator
    * @return Iterable of type RegistrationPoolInfo
    */
    public Iterable<AP_PRMRegistrationPoolingService.RegistrationPoolInfo> start(Database.batchableContext info){
        ApexLoggingService.Batch_Logs = TRUE;
        System.debug('**Batch_LogsUpdStart :'+ApexLoggingService.Batch_Logs);
        ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','StartInvoked','','ProcessType : '+ this.typeToProcess,'');       
        RegistrationPoolIterable regPoolcls ;
        if(this.typeToProcess != null){
            regPoolcls = new RegistrationPoolIterable(typeToProcess);
        }
        else{
            regPoolcls = new RegistrationPoolIterable();
        }       
        ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','StartEnded','','ProcessType : '+ this.typeToProcess + 'regPoolcls :' + regPoolcls,'');
        ApexLoggingService.Flush();
        return regPoolcls;
    }

    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Batchable interface execute method responsible for creating registration pools
    *               Consumes AP_PRMRegistrationPoolingService
    * @return None
    */
    public void execute(Database.BatchableContext info, List<AP_PRMRegistrationPoolingService.RegistrationPoolInfo> scope) {
        ApexLoggingService.Batch_Logs = TRUE;
        System.debug('**Batch_LogsUpdExecute :'+ApexLoggingService.Batch_Logs);
        ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','ExecuteInvoked','','ProcessType : '+ this.typeToProcess + ' Scope Size : '+ scope.size() + ' Batch Context: ' + info,'');
        System.debug ('*** Batch Context:' + info);
        System.debug ('*** Scope:' + scope);

        AP_PRMAppConstants.ProcessRegistrationPool poolType;

        if (this.typeToProcess != null && this.typeToProcess == AP_PRMAppConstants.ProcessRegistrationPool.ASSIGN_POOLPERMSET) {
            // Kludge to work thtough as base type cannot be typecasted into derived type
            AP_PRMRegistrationPoolingService tSvc = new AP_PRMRegistrationPoolingService ();
            String tPSInfo = JSON.serialize(scope);
            List<AP_PRMRegistrationPoolingService.PermissionSetPoolInfo> tLPSInfo = (List<AP_PRMRegistrationPoolingService.PermissionSetPoolInfo>)JSON.deserialize(tPSInfo, List<AP_PRMRegistrationPoolingService.PermissionSetPoolInfo>.class);
            tSvc.ProcessPermissionSetPools(tLPSInfo);
        }
        else if (scope != null && scope.size() > 0) {
            for (AP_PRMRegistrationPoolingService.RegistrationPoolInfo tPI : scope) {
                AP_PRMRegistrationPoolingService tSvc = new AP_PRMRegistrationPoolingService ();
                tSvc.ProcessRegistrationPools(poolType, tPI);
            }
        }
        ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','ExecuteEnded','','ProcessType : '+ this.typeToProcess + 'Scope Size : '+ scope.size() + ' Batch Context:' + info,'');
        ApexLoggingService.Flush();
    }

    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Batchable interface finsih method responsible for cleanup. To avoid infite batch(es) getting executed
    *                            ensure to launch only when original launch is channel pool creation
    * @return None
    */
    public void finish(Database.BatchableContext info) {
        ApexLoggingService.Batch_Logs = TRUE;
        System.debug('**Batch_LogsUpdFinish :'+ApexLoggingService.Batch_Logs);
        ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','FinishInvoked','','ProcessType : '+ this.typeToProcess + ' Batch Context:' + info,'');
        System.debug('*** Finish Batch Context:' + info);
        if (this.typeToProcess == null) {
            AP_PRMAppConstants.ProcessRegistrationPool psPool = AP_PRMAppConstants.ProcessRegistrationPool.ASSIGN_POOLPERMSET;
            Batch_PRM_ProcessRegistrationPools bProcessPoolsPS = new Batch_PRM_ProcessRegistrationPools (psPool);
            if(!Test.isRunningTest())
                Database.executebatch(bProcessPoolsPS, 2);
        }
        ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','FinishEnded','','ProcessType : '+ this.typeToProcess + ' Batch Context:' + info,'');
        ApexLoggingService.Flush();
    }
    
    //Schedule method
       global void execute(SchedulableContext sc) {
        List<CronTrigger> crnTrigger = new List<CronTrigger>();
        ApexLoggingService.Batch_Logs = TRUE;
        System.debug('**Batch_LogsUpdSchExecute :'+ApexLoggingService.Batch_Logs);
        try{            
            String batchQueryFilter = 'PRM Registration Pooling -%';      
            DateTime thresholdTime = System.now().addhours(-6);  
            List<CronTrigger> crnList = [SELECT Id, State, CronJobDetail.Name, StartTime, NextFireTime FROM CronTrigger WHERE CronJobDetail.Name LIKE :batchQueryFilter AND State = 'Deleted' AND NextFireTime = null AND StartTime <= : thresholdTime LIMIT 100];
            ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','ScheduleExecuteInvoked','','Abort Job List :' + crnList.size(),'');
            for(CronTrigger crn : crnList){
                if(crn != null){
                    System.debug('>>>><<<<crn:'+crn);
                        try{
                            System.debug('>>>><<<<try');
                            system.abortJob(crn.id);
                        }
                        catch(Exception e){
                            ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','ScheduleExecuteCatchBlockInner','','Abort Job List :' + crnList.size() + 'Exception Message :'+e.getMessage(),'Stack Trace :'+e.getStackTraceString());
                            System.debug('Exception occurred : '+e.getMessage() + e.getStackTraceString());
                        }                    
                }
            }
            
                crnTrigger = new List<CronTrigger>([SELECT State, CronJobDetail.Name FROM CronTrigger WHERE CronJobDetail.Name LIKE :batchQueryFilter AND (State = 'Waiting' OR State = 'Acquired') LIMIT 100]);
                if(crnTrigger.isEmpty()){
                    System.debug('**Batch Start Method called**');
                    CS_PRM_ApexJobSettings__c apexJobSettings = CS_PRM_ApexJobSettings__c.getInstance('REG_POOLING_SCHEDULE');
                    Datetime sysTime = System.now().addHours(Integer.valueOf(apexJobSettings.NumberValue__c));
                    String timeStamp = sysTime.format('yyyyMMddHHmmss.sss');
                    String cronExp = sysTime.format('s m H d M \'?\' yyyy');
                    Batch_PRM_ProcessRegistrationPools batch_pprp = new Batch_PRM_ProcessRegistrationPools();
                    System.schedule('PRM Registration Pooling -'+timeStamp, cronExp, batch_pprp);
                    ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Info.name(),'Batch_PRM_ProcessRegistrationPools','ScheduleExecuteEnded','', 'Cron Trigger List Size : '+ crnTrigger.size() + 'SysTime :' + sysTime + 'timeStamp :' + timeStamp + 'cronExp :' + cronExp ,'');
                }
        }   
        catch(Exception e){
            ApexLoggingService.createLog(AP_PRMAppConstants.DebugLogType.Error.name(),'Batch_PRM_ProcessRegistrationPools','ScheduleExecuteCatchBlock','', 'Exception Message :'+e.getMessage(),'Stack Trace :'+e.getStackTraceString());         
        }
        finally
        {
            ApexLoggingService.Flush();
        }
        
      }
 }