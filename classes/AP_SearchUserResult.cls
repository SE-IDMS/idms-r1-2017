global without sharing class AP_SearchUserResult{
    global String UserFederatedId { get; set; }
    global Boolean IsInternal { get; set; }
}