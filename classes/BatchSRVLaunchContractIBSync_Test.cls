@istest(SeeAllData=true)
private class BatchSRVLaunchContractIBSync_Test{
    static TestMethod void InterTest() {
        
        SVMXC__Service_Contract__c sc= new SVMXC__Service_Contract__c(name='test sc',TECH_OutboundEvent__c='IBsync',Tech_NeedIBSync__c=true);
        insert sc;
        sc.TECH_OutboundEvent__c='IB-SYNC';
        update sc;
        
     Test.startTest();
    
      BatchSRVLaunchContractIBSync theBatch = new BatchSRVLaunchContractIBSync();
        theBatch.start( (Database.BatchableContext) null );
        theBatch.execute( (Database.BatchableContext) null, new List<SVMXC__Service_Contract__c>{ sc } );
        theBatch.finish( (Database.BatchableContext) null );

       Test.stopTest(); 
        
    }

}