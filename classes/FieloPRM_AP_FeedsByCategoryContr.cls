/**************************************************************************************
    Author: Fielo Team
    Date: 25/02/2015
    Description: 
    Related Components: 
***************************************************************************************/
public with sharing class FieloPRM_AP_FeedsByCategoryContr{
    
    public Map<FieloEE__Tag__c,List<FieloEE__News__c>> mapTagNews {get;set;}
    public List<FieloEE__Tag__c> listTags {get;set;}
    public String docIds {get;set;}
    public String attachIds {get;set;}
    public string mode {get;set;}
    public Boolean isWidget {get;set;}
    public Map<String,List<FieloEE__News__c>> mapStringNews {get;set;}
    public list<GroupWrapper> listGroupWrapper {get;set;}
    

    private map<string,Schema.SObjectField> newSchemaMaps {get;set;} 
    private map<string,Schema.SObjectField> tagSchemaMaps {get;set;} 
    private Set<String> setAttachIds {get;set;}
    private String languageMember = FieloEE.OrganizationUtil.getLanguage();    

    public String fieldBody {get;set;} 
    public String fieldTitle {get;set;} 
    public String fieldLinkLabel {get;set;} 
    public String fieldExternalLink {get;set;} 
    public String fieldTagName {get;set;} 

    public FieloEE__Component__c componentObj{    
        get{
            return this.componentObj;
        }set{
            if(this.componentObj != value){
                this.componentObj = value;
        
                if (componentObj != null) {
                    init();
                }    
            }
        }    
    }

    public void init(){
        
        newSchemaMaps = Schema.SObjectType.FieloEE__News__c.fields.getMap();
        tagSchemaMaps = Schema.SObjectType.FieloEE__Tag__c.fields.getMap();

        if(fieldBody == null){
            fieldBody = 'FieloEE__Body__c';
            String fieldName = 'Body_' + languageMember + '__c';
            if(newSchemaMaps.containskey(fieldName)){
                fieldBody = fieldName;
            }
        }
        
        if(fieldTitle == null){
            fieldTitle = 'FieloEE__Title__c';
            String fieldName = 'Title_' + languageMember + '__c';
            if(newSchemaMaps.containskey(fieldName)){
                fieldTitle = fieldName;
            }
        }

        if(fieldLinkLabel == null){
            fieldLinkLabel = 'F_PRM_LinkLabel__c';
            String fieldName = 'F_PRM_LinkLabel_' + languageMember + '__c';
            if(newSchemaMaps.containskey(fieldName)){
                fieldLinkLabel = fieldName;
            }
        }
        
        if(fieldExternalLink == null){
            fieldExternalLink = 'FieloEE__ExternalLink__c';
            String fieldName = 'ExternalLink_' + languageMember + '__c';
            if(newSchemaMaps.containskey(fieldName)){
                fieldExternalLink = fieldName;
            }
        }

        if(fieldTagName == null){
            fieldTagName = 'Name';
            String fieldName = 'Name_' + languageMember + '__c';
            if(newSchemaMaps.containskey(fieldName)){
                fieldTagName = fieldName;
            }
        }
        
        
        string idMenu = ApexPages.currentPage().getParameters().get('idMenu');
        
        listTags = new List<FieloEE__Tag__c>();  
        
        Id idCategory = null;
        Id idTag = null;
        isWidget = false;
        mode = 'onmouseover';
        
        
        if(componentObj != null){
            if(componentObj.F_PRM_PartnerJourneyBehaviour__c != null && componentObj.F_PRM_PartnerJourneyBehaviour__c == 'On click'){
                mode = 'onclick';
            }


            if(componentObj.RecordType.DeveloperName == 'FieloPRM_PartnerJourneyWidget'){
                isWidget = true;
                //gets the Tag and Category of the Component record
                idTag = componentObj.FieloEE__Tag__c;
                idCategory = componentObj.FieloEE__Category__c;          
            }
        }
        

                               
        List<FieloEE__Category__c> listCatChannel = new List<FieloEE__Category__c>();
        
        setAttachIds = new Set<String>();

        mapStringNews = new Map<String,List<FieloEE__News__c>>();
        listGroupWrapper = new list<GroupWrapper>();

        Set<String> setQueryFields = new Set<String>{'Id'};
        if(fieldBody.contains('FieloEE')){
            setQueryFields.add('Body__c');         
        }else{
            setQueryFields.add(fieldBody);        
        }
        if(fieldTitle.contains('FieloEE')){
            setQueryFields.add('Title__c');
        }else{
            setQueryFields.add(fieldTitle);
        }
        if(fieldLinkLabel.contains('F_PRM_LinkLabel__c')){
            setQueryFields.add('F_PRM_LinkLabel__c');
        }else{
            setQueryFields.add(fieldLinkLabel);
        }
        if(fieldExternalLink.contains('FieloEE')){
            setQueryFields.add('ExternalLink__c');
        }else{
            setQueryFields.add(fieldExternalLink);
        }     
              
        if(isWidget){
            
            iswidget(idTag,idCategory, setQueryFields);
            
        }else{
        
            isNotwidget(setQueryFields);

        }


        for(GroupWrapper gw: listGroupWrapper ){

            if(gw.tag != null){
                gw.cant = mapStringNews.get(gw.tag.id).size();
            }else{
                gw.cant = mapStringNews.get(gw.cf.id).size();
            }
            
        }
        
        List<Document> listDocs = new List<Document>();
        List<Attachment> listAttach = new List<Attachment>();
        

        docIds = '';
        attachIds = '';
        if(!setAttachIds.isEmpty()){
        
            

            for(String att : setAttachIds){
                if(att.startsWith('015')){
                    docIds += att + ',';
                }else if(att.startsWith('00P')){
                    attachIds += att + ',';
                }

            } 
        }
        
    }

    public class GroupWrapper{
        public FieloEE__Tag__c tag {get;set;}
        public FieloEE__News__c cf {get;set;}
        public Integer cant {get;set;}
    
        public GroupWrapper(){
            this.tag = new FieloEE__Tag__c();
            this.cf = new FieloEE__News__c();
            this.cant = 0;
        }
        public GroupWrapper(FieloEE__Tag__c p_tag, FieloEE__News__c p_cf){
            this.tag = p_tag;
            this.cf = p_cf;
        }
    }


    public void iswidget(Id idTag, Id idCategory, Set<String> setQueryFields){

        //if no Tag was defined in the Component, it searches for the Tag in the URL
        if(idTag == null){       
            idTag = ApexPages.currentPage().getParameters().get('idTag');
        }
        
        //if no Category was defined in the Component, it searches for the Category in the URL
        if(idCategory == null){
            idCategory = ApexPages.currentPage().getParameters().get('idCategory');
        }

        if(idCategory != null && idTag != null){

                //retrieves all the content feed record related to the tag and category that match the members segmentation                
                Map<id,FieloEE__News__c> mapNews = new Map<id,FieloEE__News__c>(FieloEE.NewsService.getNews(setQueryFields,FieloEE.ProgramUtil.getProgramByDomain().Id,FieloEE.MemberUtil.getMemberId(),null,idTag,idCategory,null,null,null));               
                
                set<id> setAuxid = mapNews.keyset();
                String queryTagItem = 'SELECT Id, FieloEE__News__c, FieloEE__Tag__c , FieloEE__Tag__r.FieloEE__AttachmentId__c, FieloEE__Tag__r.' + fieldTagName +
                        ' FROM FieloEE__TagItem__c WHERE FieloEE__News__c IN: setAuxid  '  + 
                        ' AND FieloEE__Tag__c !=: idTag ORDER BY FieloEE__Tag__r.FieloEE__Order__c,FieloEE__News__r.FieloEE__Order__c ';

                
                List<FieloEE__TagItem__c> listTagItems = (List<FieloEE__TagItem__c>) Database.query(queryTagItem);

                
                for(FieloEE__TagItem__c tagItem: listTagItems){

                    FieloEE__Tag__c tag =  tagItem.FieloEE__Tag__r;

                    GroupWrapper groupW = new GroupWrapper(tag,null);

                    if(mapStringNews.containsKey(tagItem.FieloEE__Tag__c)){
                        if(mapNews.containsKey(tagItem.FieloEE__News__c)){  
                            FieloEE__News__c contentFeed = mapNews.get(tagItem.FieloEE__News__c);
                            if(mapStringNews.get(tagItem.FieloEE__Tag__c).size() < 5){
                                mapStringNews.get(tagItem.FieloEE__Tag__c).add(contentFeed);
                            }
                        }
                    }else{
                        if(listGroupWrapper.size() < 5 ){
                            listGroupWrapper.add(groupW);

     
                            List<FieloEE__News__c> auxlist = new List<FieloEE__News__c>();
                            auxlist.add(mapNews.get(tagItem.FieloEE__News__c));
                            mapStringNews.put(tag.Id,auxlist);

                            if(tag.FieloEE__AttachmentId__c != null){
                                setAttachIds.add(tag.FieloEE__AttachmentId__c);
                            }
                        }
                    }
                    
                        
                    

                }

            }
    }
    public void isNotwidget( Set<String> setQueryFields){
        //if the component is not widget it uses content feed parent to grouped the content feed child records.

            setQueryFields.add('F_PRM_Parent__c');
            setQueryFields.add('AttachmentId__c');
            
            system.debug('###setQueryFields: ' + setQueryFields);
            
            //retrieves all the content feed record related to the component that match the members segmentation            
            List<FieloEE__News__c> listCF = FieloEE.NewsService.getNews(setQueryFields,FieloEE.ProgramUtil.getProgramByDomain().Id,FieloEE.MemberUtil.getMemberId(),componentObj.Id,null,null,null,null,'F_PRM_Parent__c NULLS FIRST, FieloEE__Order__c');

            for(FieloEE__News__c cf:  listCF){
                if(cf.F_PRM_Parent__c == null){
                    if(mapStringNews.keyset().size() < 5){
                        GroupWrapper groupW = new GroupWrapper(null,cf);
                        if(!mapStringNews.containsKey(cf.Id)){
                            
                            listGroupWrapper.add(groupW);
                            mapStringNews.put(cf.Id, new List<FieloEE__News__c>{});
                            if(cf.FieloEE__AttachmentId__c != null){
                                setAttachIds.add(cf.FieloEE__AttachmentId__c);
                            }
                            
                        }
                    }
                }else{
                    if(mapStringNews.containsKey(cf.F_PRM_Parent__c)){
                        if(mapStringNews.get(cf.F_PRM_Parent__c).size() < 5){
                            mapStringNews.get(cf.F_PRM_Parent__c).add(cf);
                        }
                    }
                }

            }
    }


}