@isTest(SeeAllData=true)
Public class AP_Event_BORef_TEST
{

 static testMethod void testeventtrigger(){
 Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        
        
 Contact con = Utils_TestMethods.createContact(acc.Id,'con');
        insert con; 
 
 
 SVMXC__Service_Order__c WO =     new            SVMXC__Service_Order__c(                                        
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Open', 
                                                     BackOfficeReference__c='123',  
                                                     SVMXC__Contact__c = Con.Id, 
                                                     WorkOrderName__c = 'test',
                                                     RescheduleReason__c='Weather Related',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     Is_Billable__c = true,
                                                     Customer_Service_Request_Time__c= system.now(),ApplyPredefinedDateTime__c=true);
                                insert WO;
                                
               
        Event OldEvent = New Event(Subject = 'test',Status__c = 'testing',WhatId = WO.Id,
                            StartDateTime = system.now(),
                             EndDateTime = system.now() +1,
                             BackOfficeReference__c=WO.BackOfficeReference__c,
                             WorkOrderName__c = WO.WorkOrderName__c);        
                                             
                               insert OldEvent;
                               
                               
             
                             
                             OldEvent.EndDateTime = system.now() +2;
                             update OldEvent; 
                            
                                                              
                                     }   
         
}