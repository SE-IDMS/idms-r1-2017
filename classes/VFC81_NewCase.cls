public class VFC81_NewCase extends VFC_ControllerBase{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    
    public List<SelectOption> columns{get;set;}
    public Case newCase{get;set;} // Case to be created 
    public List<CaseClassification__c> predefinedCategory{get;set;}
    public Boolean flagPRM{get;set;} // flag indicates if the related Account or Contact is a PRMAccount or a PRMContact
    Public boolean detailsrendered{get;set;}
    Public String ContactId{get;set;}  
    Public String AccountId{get;set;}
   
    /*=======================================
      CONSTRUCTOR
    =======================================*/   
    
    // VFC81_NewCases standard controller extension
    public VFC81_NewCase(ApexPages.StandardController controller){
        ContactId=apexpages.currentpage().getparameters().get('def_contact_id');
        AccountId=apexpages.currentpage().getparameters().get('def_account_id');
        System.debug('VFC81_NewCase.Constructor.INFO - Constructor is called.');
       
        try {
            System.debug('----- flagPRM in beginning : '+flagPRM );
            String contact_id = system.currentpagereference().getParameters().get('def_contact_id');
            String account_id = system.currentpagereference().getParameters().get('def_account_id');
            System.debug('----- contact_id is : '+contact_id);
            System.debug('----- account_id is : '+account_id);
            
            if(contact_id != '' && contact_id != null && flagPRM == null){
                Contact relContact = [SELECT Id, PRMContact__c, Account.RecordTypeId FROM Contact where Id =:contact_id LIMIT 1];
            
                if(relContact.Account.RecordTypeId == System.Label.CLJUL15CCC01){    
                    flagPRM = false;
                }
            }
            if(account_id != '' && account_id  != null && flagPRM == null){
                Account relAccount = [SELECT Id, RecordTypeId FROM Account where Id =:account_id LIMIT 1];
                System.debug('----- relAccount is : '+relAccount );
                if(relAccount.RecordTypeId == System.Label.CLJUL15CCC01)
                    flagPRM = false;
            }
            if(flagPRM == null){
                flagPRM = true;
            }
            System.debug('----- flagPRM is :::: '+flagPRM );
            if(flagPRM == false) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLJUL15CCC04));
            }
            System.debug('----- flagPRM is : '+flagPRM );
            
            newCase = (Case)controller.getRecord(); // Get the case record
            predefinedCategory = new List<CaseClassification__c>();
    
            /*
                Retrieve all active case classifications -order is not null- and sort them by the "order"
                The maximal of pre-defined case is limite to 25.
                Build the list of Setting Wrapper using the custom constructor
            */
            for(CaseClassification__c predefinedCase:[SELECT ID, Name, Order__c, Label__c, Category__c, Reason__c, SubReason__c FROM CaseClassification__c Where Order__c != null ORDER BY Order__c LIMIT 25]){
                if(predefinedCase.Label__c == null) {
                    predefinedCase.Label__c = '--';
                }
                predefinedCategory.add(predefinedCase);
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            System.debug('VFC81_NewCase.Constructor.'+e.getTypeName()+' - '+ e.getMessage());
            System.debug('VFC81_NewCase.Constructor.'+e.getTypeName()+' - Line = '+ e.getLineNumber());
            System.debug('VFC81_NewCase.Constructor.'+e.getTypeName()+' - Stack trace = '+ e.getStackTraceString());
        }
        
        System.debug('VFC81_NewCase.Constructor.INFO - End of constructor.'); 
    }
    public pagereference cancel(){
        System.Debug('VFC81_NewCase.settingWrapper.cancel- Method is called.');
        // Override the Cancel functionality
        PageReference caseCancelPage;
        String ContactId = System.currentPageReference().getParameters().get('def_contact_id');
        String AccountId = System.currentPageReference().getParameters().get('def_account_id');
        if(ContactId !=null && ContactId !='' ){
            caseCancelPage = new PageReference('/'+ ContactId);
        }
        else if(AccountId !=null && AccountId  !=''){
            System.debug('AccountId' + AccountId);
            caseCancelPage = new PageReference('/'+ AccountId);
        }
        else{
            System.debug('SObjectType.Case.getKeyPrefix():' + SObjectType.Case.getKeyPrefix());
            caseCancelPage = new PageReference('/'+SObjectType.Case.getKeyPrefix()+'/o' );
        }
        return caseCancelPage;
    }
}