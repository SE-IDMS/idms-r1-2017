global class POMPUrlRewriter implements Site.UrlRewriter {
    Map<String, CS_PRM_POMPUrlMapping__c> urlMappings = CS_PRM_POMPUrlMapping__c.getAll();
    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        try{
            String FriendlyUrl = myFriendlyUrl.getUrl();
            for(CS_PRM_POMPUrlMapping__c urlMapping:urlMappings.Values()){
                if(FriendlyUrl.startsWithIgnoreCase(urlMapping.Name)){
                    string regExp = '(?i)'+urlMapping.Name;
                    FriendlyUrl = FriendlyUrl.replaceAll(regExp, urlMapping.Page__c);
                    PageReference pg = new PageReference(FriendlyUrl);
                    return pg;
                }
            } 
            return null;
        }
        catch(exception ex){
            System.debug('There is an exception '+ex);
            return null;
        }
    }
    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls) { 
        List<PageReference> myFriendlyUrls = new List<PageReference>();
        for(PageReference mySalesforceUrl : mySalesforceUrls){
            String SalesforceUrl = mySalesforceUrl.getUrl();
            for(CS_PRM_POMPUrlMapping__c urlMapping:urlMappings.Values()){
                if(SalesforceUrl.startsWithIgnoreCase(urlMapping.Page__c)){
                    string regExp = '(?i)'+urlMapping.Page__c;
                    SalesforceUrl = SalesforceUrl.replaceAll(regExp, urlMapping.Name);
                    myFriendlyUrls.add(new PageReference(SalesforceUrl));
                }
            }
        }
        System.debug('*** Friendly Url:' + myFriendlyUrls);
        return myFriendlyUrls; 
    } 
}