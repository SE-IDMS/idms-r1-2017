@isTest
private class VFC47_OpptyNotifSearch_TEST {
    
    static testMethod void searchOpportunityTestMethod()
    { 
    
    //try{
        Country__c country = Utils_TestMethods.createCountry();
        country.name='test countryxyz';
        country.CountryCode__c = 'XYZ';
        Database.insert(country);
        
        Account account = Utils_TestMethods.createAccount();
        //account.Country__c = country.id;
        Database.insert(account);  
        
      /*    Modified By ACCENTURE
            Modifed for December Release Business Requirement BR-963
            Modified Date: 11/11/2011
      */ 
        contact contact = Utils_TestMethods.createContact(account.id,'Test Contact');
        insert contact;
        
        
         Case objCase = Utils_TestMethods.createCase(account.Id,contact.Id,'Open');
        /* BR-4310 Commented By Vimal K (GD India)
        objCase.SupportCategory__c = Label.CL00408;
        objCase.Symptom__c = Label.CL00737;
        objCase.SubSymptom__c = Label.CL00742;
        */
        objCase.SupportCategory__c = Label.CL00737;
        insert objCase;
           
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(account.id);
        workOrder.SVMXC__Contact__c = contact.id;    
        Database.insert(workOrder);
    
        SVMXC__Service_Order__c workOrder1 = new SVMXC__Service_Order__c();
        workOrder1.SVMXC__Contact__c = contact.id;    
        Database.insert(workOrder1);
         //End of Modification 
       
        OpportunityNotification__c opportunityNotif = Utils_TestMethods.createOpportunityNotification(workOrder.id);
        opportunityNotif.Name = 'Test Opp';
        opportunityNotif.case__c=objCase.id;
        opportunityNotif.OpportunityScope__c ='BDSL3';
        opportunityNotif.AccountForConversion__c =account.id;
        opportunityNotif.CountryDestination__c=country.id;
        opportunityNotif.RejectionReason__c='null';
        opportunityNotif.OpportunityOwner__c=null;
        opportunityNotif.Amount__c=null;
        Database.insert(opportunityNotif );
        
        
        OpportunityNotification__c opportunityNotif1 = Utils_TestMethods.createOpportunityNotification(workOrder1.id);
        opportunityNotif1.Name = 'Test Opp1';
        opportunityNotif1.OpportunityScope__c ='BDADS';
        opportunityNotif1.OpportunityOwner__c=userinfo.getuserid();
        opportunityNotif1.AccountForConversion__c =account.id;
        opportunityNotif1.RejectionReason__c='Other';
        Database.insert(opportunityNotif1 ); 

        Team_Members_Opportunity_Notification__c tmon= new Team_Members_Opportunity_Notification__c();
        tmon.Opportunity_Notification__c=opportunityNotif.id;
        insert tmon;
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
         ip.name='test ip';
        insert ip;

        InstalledProductOnOpportunityNotif__c Ipon= new InstalledProductOnOpportunityNotif__c();
        Ipon.InstalledProduct__c=ip.id;
        Ipon.OpportunityNotification__c=opportunityNotif.id;
        
        insert Ipon;
 
        PageReference pageRef = Page.VFP47_OpptyNotifSearch;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stcontoller = new ApexPages.StandardController(opportunityNotif );  
        VFC47_OpptyNotifSearch Controller = new VFC47_OpptyNotifSearch(stcontoller );
       
       /*
           OpportunityTeamMember otm = new OpportunityTeamMember ();
            otm.OpportunityID = opp.id;
            otm.UserID = opp.OpportunityDetector__c;
            otm.TeamMemberRole='FSR';
        insert otm;
        
        Team_Members_Opportunity_Notification__c tmon= new Team_Members_Opportunity_Notification__c();
        tmon.Opportunity_Notification__c=opportunityNotif.id;
        insert tmon;
        
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
         ip.name='test ip';
        insert ip;
            
        
        InstalledProductOnOpportunityNotif__c Ipon= new InstalledProductOnOpportunityNotif__c();
        Ipon.InstalledProduct__c=ip.id;
        Ipon.OpportunityNotification__c=opportunityNotif.id;
        insert Ipon;*/
        
        Controller.doSearchOpportunity(); 
        test.starttest();
        Controller.convert(); 
        test.stoptest();
        Controller.markAsDuplicate();        
        Controller.returnToOppNotification();
        
        //}
        //catch (exception e){
        //    System.debug('Exception '+e.getMessage());
        //    System.debug('Exception '+e.getStackTraceString());
        //}
     
    }
    
    static testMethod void searchOpportunityTestMethodMobilityVersion() { 
    
        //try {
            Country__c country = Utils_TestMethods.createCountry();
            country.name='test countryabc';
            country.CountryCode__c = 'XYZ';
            Database.insert(country );
            
            Account account = Utils_TestMethods.createAccount();
            //account.Country__c = country.id;
            Database.insert(account); 
        
            contact contact = Utils_TestMethods.createContact(account.id,'Test Contact');
            insert contact;
          
            Case objCase = Utils_TestMethods.createCase(account.Id,contact.Id,'Open');
            objCase.SupportCategory__c = Label.CL00737;
            insert objCase;
            
            SVMXC__Site__c loc = new SVMXC__Site__c(name='Pak');
            loc.timeZone__c ='GMT';
            insert loc;
           
            SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(account.id);
            workOrder.SVMXC__Contact__c = contact.id;    
            Database.insert(workOrder);
        
            SVMXC__Service_Order__c workOrder1 = new SVMXC__Service_Order__c();
            workOrder1.SVMXC__Contact__c = contact.id;    
            Database.insert(workOrder1);
            
            RecordType MobileRTId = [SELECT Id FROM RecordType WHERE SObjectType = 'OpportunityNotification__c' AND DeveloperName = 'Mobile' LIMIT 1];
           
            OpportunityNotification__c opportunityNotif = Utils_TestMethods.createOpportunityNotification(workOrder.id);
            opportunityNotif.RecordTypeId = MobileRTId.Id;
            opportunityNotif.From_Mobile__c = true;
            opportunityNotif.Project_Name__c = 'Test Value Longer than 80 Characters for code coverage that will run01234567890123';
            opportunityNotif.Name = 'FROM STANDALONE';
            opportunityNotif.case__c=objCase.id;
            opportunityNotif.OpportunityScope__c ='BDSL3';
            opportunityNotif.AccountForConversion__c =account.id;
            opportunityNotif.CountryDestination__c=country.id;
            opportunityNotif.RejectionReason__c='Duplicate';
            opportunityNotif.OpportunityOwner__c=null;
            opportunityNotif.Location__c=loc.id;
            opportunityNotif.LeadingBusinessBU__c='BD';
            Database.insert(opportunityNotif );
            
            // create interest
            InterestOnOpportunityNotif__c oppNotifInterest = new InterestOnOpportunityNotif__c();
            oppNotifInterest.OpportunityNotification__c = opportunityNotif.Id;
            oppNotifInterest.Leading_Business_BU__c = 'EN';
            oppNotifInterest.Type__c = 'Product (Energy)';
            oppNotifInterest.Interest__c = 'Other HW';
            insert oppNotifInterest;
        
            // create ip reference to opp notification
            //InstalledProductOnOpportunityNotif__c  ipopn = new InstalledProductOnOpportunityNotif__c();
            //ipopn.Tech_CreatedFromIpSFM__c = true;
            //ipopn.OpportunityNotification__c = opportunityNotif.id;
            //insert ipopn;
        
            //User newUser = Utils_TestMethods.createStandardUser('random test alias');
            //insert newUser;
            
            // create team member
            //Team_Members_Opportunity_Notification__c oppNotifTeamMember = new Team_Members_Opportunity_Notification__c();
            //oppNotifTeamMember.Opportunity_Notification__c = opportunityNotif.Id;
            //oppNotifTeamMember.Detector__c = newUser.Id;
            //insert oppNotifTeamMember;

            // create attachment
            Attachment newAttachment1 = new Attachment(Name='OppNotificationTestAttachment.pdf');
            newAttachment1.parentId = opportunityNotif.Id;
            Blob bodyblob = Blob.valueOf('Test Class Attachment Body');
            newAttachment1.body = bodyblob;
            insert newAttachment1;
             
            PageReference pageRef = Page.VFP47_OpptyNotifSearch;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stcontoller = new ApexPages.StandardController(opportunityNotif );  
            VFC47_OpptyNotifSearch Controller = new VFC47_OpptyNotifSearch(stcontoller );
    
            Controller.doSearchOpportunity(); 
            test.starttest();
            Controller.convert(); 
            test.stoptest();       
            Controller.returnToOppNotification();
          //  Controller.markAsDuplicate();
            
        //} catch (exception e){
        //    System.debug('Exception '+e.getMessage());
        //    System.debug('Exception '+e.getStackTraceString());
        //}  
    }
}