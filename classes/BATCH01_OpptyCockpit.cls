/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 25/11/2010
    Description     : Batch Apex class that works on a list of Opportunity Field History records.
              Creates a Weekly News FeedPost on the Opportunity at the top of the tree - where any Opportunities in the tree has had a change
              that is feed tracked.
              
*/
global class BATCH01_OpptyCockpit implements Database.Batchable<SObject>, Schedulable, Database.Stateful{
  private Set<String> allBatchesTreetopIds = new Set<String>();        // stateful variable that contains all Treetop Opportunity Ids processed in all batches
  private Integer nErrorCount = 0;                      // stateful variable that tracks number of errors
  private String query;
  private final Integer BATCH_SIZE = Integer.valueOf(System.Label.CL00087);
  private Integer CHUNK_SIZE = 1000;                      // number of records passed to DML at a time, not final as used in Test Class  
  private String emailTo = System.Label.CL00081;
  private String emailReplyTo = System.Label.CL00086;
  private String emailSubject = System.Label.CL00082;
  private PageReference pr = Page.VFP01_OpptyCockpit;              // used as part of the URL for the Chat post
  
  global void execute(SchedulableContext sc) {
    /*BATCH01_OpptyCockpit b = new BATCH01_OpptyCockpit('Select o.OpportunityId, o.Opportunity.ParentOpportunity__c, o.Opportunity.ParentOpportunity__r.ParentOpportunity__c, o.Opportunity.ParentOpportunity__r.ParentOpportunity__r.ParentOpportunity__c From OpportunityFieldHistory o Where CreatedDate = LAST_N_DAYS:7');*/
    BATCH01_OpptyCockpit b = new BATCH01_OpptyCockpit('Select o.ParentId, o.Parent.ParentOpportunity__c, o.Parent.ParentOpportunity__r.ParentOpportunity__c, o.Parent.ParentOpportunity__r.ParentOpportunity__r.ParentOpportunity__c From OpportunityFeed o Where Type = \'TrackedChange\' and CreatedDate = LAST_N_DAYS:7');
    database.executebatch(b, BATCH_SIZE);
  }
  
  global BATCH01_OpptyCockpit(){
  }
  
  global BATCH01_OpptyCockpit(String q){
    this.query = q;
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(query);
  }
  
  global void execute(Database.BatchableContext BC, List<sObject> scope){
    if(Test.isRunningTest())
      CHUNK_SIZE = 2;                            // needed to achieve test coverage in Test Methods
      
    Database.SaveResult[] lsr = new List<Database.SaveResult>();
    List<FeedItem> posts_to_insert = new List<FeedItem>();          // link posts to be insert in THIS batch
    Set<String> batchTreeTopIds = new Set<String>();            // treetop Opportunity Ids to process in THIS batch
    Set<String>  keys = new Set<String>();                  // accountOwner + treetop concatenated key used to track which AccountOwners have already had FeedPost created    
    
    for(Sobject s : scope){
      OpportunityFeed ofh = (OpportunityFeed)s;
      String treetoptemp = getTreeTopOpportunity(ofh.Parent);  
      if(treetoptemp != null && !this.allBatchesTreetopIds.contains(treetoptemp)){
        batchTreeTopIds.add(treetoptemp);        // only add Treetop Opportunity Ids that have not yet been processed
        this.allBatchesTreetopIds.add(treetoptemp);    // add treetop to stateful set so in next main batch we do not process opps belonging to this tree
      }
    }
    
    for(String id :batchTreeTopIds){
      posts_to_insert.add(new FeedItem(ParentId = id, LinkUrl = pr.getUrl() + '?id=' + id, Title = System.Label.CL00083));
      if(posts_to_insert.size() == CHUNK_SIZE){
        if(!posts_to_insert.isEmpty()){
          lsr = Database.insert(posts_to_insert, false);  
          for(Database.SaveResult sr : lsr){
            if(!sr.isSuccess() || Test.isRunningTest()){
                   nErrorCount++;
              }
          }
        }
        posts_to_insert.clear();
      }
    }
    
    if(!posts_to_insert.isEmpty()){
      lsr = Database.insert(posts_to_insert, false);
      for(Database.SaveResult sr : lsr){
        if(!sr.isSuccess() || Test.isRunningTest()){
              nErrorCount++;
          }
      }
    }
  }

  global void finish(Database.BatchableContext BC){
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems from AsyncApexJob where Id = :BC.getJobId()];
    // send email with any errors 
    if(nErrorCount > 0 || a.NumberOfErrors > 0 || Test.isRunningTest()){
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(emailTo.split(','));
      mail.setReplyTo(emailReplyTo);
      mail.setSubject(emailSubject);
      mail.setPlainTextBody(System.Label.CL00084 + a.TotalJobItems + '/' + a.NumberOfErrors + '. ' + System.Label.CL00085 + nErrorCount);
      if(!Test.isRunningTest())                          // ensure we never actually send an email during a Test Run
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
  }
  
  private String getTreeTopOpportunity(Opportunity o){
    // retrieve the ultimate opportunity that is top of the tree for passed in Opportunity
    String treetop;
    if(o.ParentOpportunity__c == null){
      treetop = o.Id;
    }else if(o.ParentOpportunity__r.ParentOpportunity__c == null){
      treetop = o.ParentOpportunity__r.Id;
    }else if(o.ParentOpportunity__r.ParentOpportunity__r.ParentOpportunity__c == null){
      treetop = o.ParentOpportunity__r.ParentOpportunity__r.Id; 
    }
    return treetop;
  }
}