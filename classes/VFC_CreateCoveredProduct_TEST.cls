/**
    28/Nov/13     Deepak     Test class for the VFC_CreateCoveredProduct  controller
 */
@isTest
private class VFC_CreateCoveredProduct_TEST 
{

    static testMethod void VFC_Test() 
    {   
       // test.StartTest();
        Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.SVMXC__Company__c = accountSobj.Id;
        sc.SVMXC__End_Date__c=date.today();
        sc.SVMXC__Start_Date__c=date.Today();
        insert sc;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = accountSobj.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = accountSobj.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c='test';
        insert ip1;
        
        SVMXC__Service__c ser = New SVMXC__Service__c();
        ser.SVMXC__Active__c=true;
        ser.Name='Test';
        ser.SVMXC__Service_Type__c='test';
        insert ser;
        
        SVMXC__Service_Contract_Services__c is = New SVMXC__Service_Contract_Services__c();
        is.SVMXC__Service_Contract__c=sc.Id;
        is.SVMXC__Service__c=ser.Id;
        insert is;
        
        AssociatedInstalledProduct__c aip = New AssociatedInstalledProduct__c();
        aip.ServiceMaintenanceContract__c=sc.Id;
        aip.InstalledProduct__c=ip1.Id;
        insert aip;
        
        SVMXC__Service_Contract_Products__c scp = New SVMXC__Service_Contract_Products__c();
        scp.IncludedService__c=is.Id;
        scp.SVMXC__Installed_Product__c=ip1.Id;
        scp.SVMXC__Service_Contract__c=sc.Id;
        scp.SVMXC__Start_Date__c =sc.SVMXC__Start_Date__c;
        scp.SVMXC__End_Date__c = sc.SVMXC__End_Date__c;
        insert scp;
        
        
        //ApexPages.StandardController controller = new ApexPages.StandardController(scp);
        Pagereference pg = Page.VFP_CreateCoveredProduct ;
        pg.getParameters().put('Id',sc.Id);
         Test.setCurrentPage(pg);
        
        VFC_CreateCoveredProduct newObj = new VFC_CreateCoveredProduct();
        
        newObj.doProcess();
        newObj.getisSuccess();
        newObj.getcloseWindow();
        newObj.getmessage();
        newObj.getdisplayImage();
        newObj.getisValidated();
        newObj.doclose();
        
        //test.stopTest();      
    }
}