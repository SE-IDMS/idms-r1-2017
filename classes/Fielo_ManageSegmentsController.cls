/*
 * @author      Marco Paulucci
 * @date        05.03.2015
 * @description Visualforce Controller Class
 */
global without sharing class Fielo_ManageSegmentsController {

    // Search value
    public String toSearch { get; set; }
    // Available Segments - Segments with no linked domain (top list)
    public List<Segment> segmentsNot { get; set; }
    // Selected Segments  - Segments with a linked domain (bottom list)
    public List<Segment> segmentsYes { get; set; }
    // sObject available record handler
    public Fielo_GeneralPager generalPager { get; set; }
    // Custom Settings
    public Fielo_SegmentManagerSettings__c settings { get; set; }
    // Error handler
    public Boolean error { get; set; }

    // Segments that have been selected from previous pages but not added yet
    private Map<Id, Segment> segmentsPaginated { get; set; }
    // Segments that have been added and are awaiting the Save call
    private Set<Id> segmentsToAdd { get; set; }
    // Segments to be excluded from the search query
    private Set<Id> segmentsNoSearch { get; set; }
    // Lookup field for queries
    private String lookup { get; set; }
    // Main Object API Name
    private String objectAPI { get; set; }
    // This boolean will warn users if they try to override their search result
    private Boolean warned { get; set; }
    // List of fields used on search
    private List<String> fieldsLike { get; set; }
    // Organize queries
    private enum SOQL {GET_DOMAINS, GET_CURRENT, GET_SEARCH, GET_SEGMENT}
    //Record
    private Id recordId;
    //Current record segment
    private Id segmentId;
    //Multi-segment checkbox name
    private String segmentCheckStr;


    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Constructor
     */
    public Fielo_ManageSegmentsController () {
        
        recordId = ApexPages.currentPage().getParameters().get('Id');
        
        error = !errorHandler();
        
        if (!error) {

            // Initializers
            segmentsNot       = new List<Segment>();
            segmentsYes       = new List<Segment>();
            segmentsToAdd     = new Set<Id>();
            segmentsNoSearch  = new Set<Id>();
            segmentsPaginated = new Map<Id, Segment>();
            toSearch          = '';

            // Load the already selected segments linked to the ObjectType
            for(FieloEE__SegmentDomain__c d : Database.query(getQuery(SOQL.GET_DOMAINS))) {
                segmentsNoSearch.add((Id) d.FieloEE__Segment__c);
            }
            
            for(sObject o : Database.query(getQuery(SOQL.GET_CURRENT))){
                segmentsYes.add(new Segment((FieloEE__RedemptionRule__c) o, true));
            }

            if(lookup.equals('FieloEE__Banner__c') || lookup.equals('FieloEE__News__c') || lookup.equals('FieloEE__Reward__c') || lookup.equals('FieloEE__Menu__c')){
                segmentId = (Id)Database.query('SELECT FieloEE__RedemptionRule__c FROM ' + lookup + ' WHERE Id = \'' + recordId + '\'')[0].get('FieloEE__RedemptionRule__c');
                if(segmentId != null){
                    FieloEE__RedemptionRule__c rr = Database.query(getQuery(SOQL.GET_SEGMENT));
                    segmentsNoSearch.add(rr.Id);
                    segmentsYes.add(new Segment(rr, true));
                }
            }
        }
    }

    /*
     * @author      Marco Paulucci
     * @date        12.03.2015
     * @description Constructor extension - global error checking
     */
    private Boolean errorHandler () {
        return isValidId(recordId)
            && isValidObjectCustomSetting(Fielo_SegmentManager__c.getInstance(objectAPI))
            && isValidGlobalCustomSetting(Fielo_SegmentManagerSettings__c.getInstance());
    }

    /*
     * @author      Marco Paulucci
     * @date        12.03.2015
     * @description Constructor extension - find errors in the URL Id parameter
     */
    private Boolean isValidId (String getId) {

        // Check if the Id exists
        if (getId == null) {
            addMessage(System.Label.Fielo_MS_ErrorNoId, 'E');
            return false;
        }

        // Check if the Id is valid
        try {
            objectAPI = ((Id) getId).getSObjectType().getDescribe().getName();
        } catch (Exception e) {
            addMessage(System.Label.Fielo_MS_ErrorInvalidId, 'E');
            return false;
        }
        return true;
    }

    /*
     * @author      Marco Paulucci
     * @date        12.03.2015
     * @description Constructor extension - find errors in the object Custom Setting
     */
    private Boolean isValidObjectCustomSetting (Fielo_SegmentManager__c setting) {
        
        // Check if an instance exists for the specific object
        if (setting == null) {
            addMessage(System.Label.Fielo_MS_ErrorObjNoCS, 'E');
            return false;
        } 

        // Check that the lookup field exists
        if(!Schema.SObjectType.FieloEE__SegmentDomain__c.fields.getMap().containsKey(setting.F_LookupField__c)) {
            addMessage(System.Label.Fielo_MS_ErrorInvalidLookup, 'E');
            return false;
        }

        lookup = setting.F_LookupField__c;
        segmentCheckStr = lookup.equals('FieloEE__Menu__c') ? 'FieloEE__HasMultiSegments__c' : 'FieloEE__HasSegments__c';
        return true;
    }

    /*
     * @author      Marco Paulucci
     * @date        12.03.2015
     * @description Constructor extension - find errors in the main Custom Setting
     */
    private Boolean isValidGlobalCustomSetting (Fielo_SegmentManagerSettings__c setting) {

        // There is no default custom setting instance
        if (setting == null) {
            addMessage(System.Label.Fielo_MS_ErrorNoGlobCS, 'E');
        } 

        // The fieldset doesn't exist
        else if (!SObjectType.FieloEE__RedemptionRule__c.FieldSets.getMap().containsKey(setting.F_Fieldset__c)) {
            addMessage(System.Label.Fielo_MS_ErrorInvalidFieldset, 'E');
        }

        // The order field doesn't exist
        else if (!SObjectType.FieloEE__RedemptionRule__c.fields.getMap().containsKey(setting.F_OrderBy__c)) {
            addMessage(System.Label.Fielo_MS_ErrorInvalidOrderField, 'E');
        }

        // Invalid PageMessages location
        else if (setting.F_MsgLocation__c != 'both' && setting.F_MsgLocation__c != 'top' && setting.F_MsgLocation__c != 'bottom') {
            addMessage(System.Label.Fielo_MS_ErrorMsgLocation, 'E');
        }

        // Invalid Page Buttons location
        else if (setting.F_BtnLocation__c != 'both' && setting.F_BtnLocation__c != 'top' && setting.F_BtnLocation__c != 'bottom') {
            addMessage(System.Label.Fielo_MS_ErrorBtnLocation, 'E');
        }

        // Invalid pager size
        else if (setting.F_PagerSize__c < 1 || setting.F_PagerSize__c > 1000) {
            addMessage(System.Label.Fielo_MS_ErrorInvalidPagerSize, 'E');
        }

        // Good to go
        else {
            settings = setting;

            // Do a final search-fields check
            Map <String, Schema.SObjectField> fieldMap  = SObjectType.FieloEE__RedemptionRule__c.fields.getMap();
            List<String> fields                         = setting.F_FieldLike__c.replaceAll('[^a-zA-Z\\d\\s,_]', '').split(',', 0);
            List<String> validFields                    = new List<String>();
            String wrongFields                          = '';

            for (String s : fields) {
                s = s.trim().toLowerCase().split('\\.', 0)[0];
                if (fieldMap.containsKey(s)) {
                    validFields.add(s);
                } else {
                    wrongFields += s + ', ';
                }
            }

            if (wrongFields != '') {
                addMessage(System.Label.Fielo_MS_ErrorInvalidSearchField.replace('{%FIELDS}', wrongFields.removeEnd(', ')) + '.', 'W');
            }

            fieldsLike = validFields;
            
            return true;
        }

        return false;
    }


    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////// CONTROLLER METHODS ////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Visualforce search method
     */
    private void doSearch (Boolean silent) {
        // If some segments were selected, ask user to confirm search
        if (!((segmentsPaginated == null || segmentsPaginated.isEmpty()) || warned)) {
            addMessage(System.Label.Fielo_MS_SearchWarning.replace('{%LABEL}', System.Label.Fielo_MS_Go), 'W');
            warned = true;
            return;
        }

        // If no segments are selected or if user confirmed search
        try {
            if(didSetPager(getQuery(SOQL.GET_SEARCH))) {
                setRecords();
                if (!silent) {
                    addMessage(System.Label.Fielo_MS_ReturnedRows.replace('{%COUNT}', String.valueOf(generalPager.getResultSize())), 'C');
                }
            } else {
                if(segmentsNot != null) segmentsNot.clear();
                if(segmentsPaginated != null) segmentsPaginated.clear();
                if (!silent) {
                    addMessage(System.Label.Fielo_MS_EmptyResult, 'W'); 
                }
            }
        } catch (QueryException qe) {
            addMessage(qe.getMessage(), 'E');
        }
        warned   = false;
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Create pager from a query String or a Database.queryLocator
     */
    private Boolean didSetPager (Database.queryLocator soql_query) {
        try {
            generalPager = new Fielo_GeneralPager(soql_query, Integer.valueOf(settings.F_PagerSize__c));
            segmentsPaginated.clear();
            return generalPager.getRecords().size() > 0;
        } catch (Exception e) {
            addMessage(e.getMessage(), 'E');
        }
        return false;
    }
    private Boolean didSetPager (String soql_query) {
        return soql_query != null ? didSetPager(Database.getQueryLocator(soql_query)) : false;
    }

    /*
     * @author      Marco Paulucci
     * @date        09.03.2015
     * @description Store selected segments in memory
     */
    private void storeSelectedSegments () {
        for (Segment s : segmentsNot) {
            if (s.checked) {
                segmentsPaginated.put(s.segment.Id, s);
            } else if (segmentsPaginated.containsKey(s.segment.Id)) {
                // Make sure for each deselected box that the item isn't being stored
                segmentsPaginated.remove(s.segment.Id);
            }
        }
    }

    /*
     * @author      Marco Paulucci
     * @date        09.03.2015
     * @description Refreshes available segment list
     */
    private void setRecords () {
        segmentsNot.clear();
        for (sObject o : generalPager.getRecords()) {
            segmentsNot.add(new Segment((FieloEE__RedemptionRule__c) o, segmentsPaginated.containsKey(o.Id)));
        }
    }    

    /*
     * @author      Marco Paulucci
     * @date        11.03.2015
     * @description Keeping all queries in one place
     */
    private String getQuery (SOQL type) {
        String fieldset     = '';
        String searchFields = '';
        String fieldsetName = settings != null ? settings.F_Fieldset__c : '';
        String orderField   = settings != null ? settings.F_OrderBy__c : '';
        String searchValue  = String.escapeSingleQuotes(toSearch.trim().replace(' ', '%'));

        // Create a string from a fieldset
        if(fieldsetName != ''){
            for(Schema.FieldSetMember f : SObjectType.FieloEE__RedemptionRule__c.FieldSets.getMap().get(fieldsetName).getFields()) {
                fieldset += f.getFieldPath() + ', ';
            }
            fieldset = fieldset.subString(0, fieldset.length() - 2);
        }

        // RETURN: search result
        if (type == SOQL.GET_SEARCH && fieldsLike != null) {
            for (String s : fieldsLike) {
                searchFields += s + ' LIKE \'%' + searchValue + '%\' OR ';
            }

            return 'SELECT ' + fieldset + ' '
                 + 'FROM FieloEE__RedemptionRule__c '
                 + 'WHERE Id NOT IN : segmentsNoSearch '
                 +      'AND ' + '(' + searchFields.removeEnd('OR ') + ')'
                 + 'ORDER BY ' + orderField;
        }

        // RETURN: existing segment domains
        if (type == SOQL.GET_DOMAINS) {
            return 'SELECT FieloEE__Segment__c, ' + this.lookup  + ' '
                 + 'FROM FieloEE__SegmentDomain__c '
                 + 'WHERE ' + this.lookup + ' = \'' + recordId + '\'';
        }

        // RETURN: segments that are being used
        if (type == SOQL.GET_CURRENT) {
            return 'SELECT ' + fieldset + ' ' + 
                 + 'FROM FieloEE__RedemptionRule__c '
                 + 'WHERE Id IN: segmentsNoSearch '
                 + 'ORDER BY ' + orderField;
        }

        // RETURN: segments that are being used
        if (type == SOQL.GET_SEGMENT) {
            return 'SELECT ' + fieldset + ' ' + 
                 + 'FROM FieloEE__RedemptionRule__c '
                 + 'WHERE Id = \'' + segmentId + '\'';
        }



        return null;
    }
    
    /*
     * @author      Marco Paulucci
     * @date        17.06.2015
     * @description Generate the proper redirect url
     */
    webservice static String generateSegmentURL (String request) {
        try { 
            Id recordId = request.subString (request.indexOf('.com/') + 5, request.indexOf ('.com/') + 20);
            return '/apex/Fielo_ManageSegments?id=' + recordId;
        } catch (Exception e) { }
        
        String URL = '';
        try {
            Integer pos = 1 + request.indexOf('?id=') + request.indexOf('&id=');
            Id recordId = request.subString (pos + 4, pos + 19);
            URL = Fielo_SegmentManagerSettings__c.getInstance().F_HomeURL__c + '/apex/Fielo_ManageSegments?id=' + recordId;
        } catch (Exception e) { return e.getMessage(); }
        
        return URL;
    }


    public static PageReference generateSegmentURLacion (String request) {
        try { 
            Id recordId = request.subString (request.indexOf('.com/') + 5, request.indexOf ('.com/') + 20);
            return new PageReference('/apex/Fielo_ManageSegments?id=' + recordId);
        } catch (Exception e) { }
        
        String URL = '';
        try {
            Integer pos = 1 + request.indexOf('?id=') + request.indexOf('&id=');
            Id recordId = request.subString (pos + 4, pos + 19);
            URL = Fielo_SegmentManagerSettings__c.getInstance().F_HomeURL__c + '/apex/Fielo_ManageSegments?id=' + recordId;
        } catch (Exception e) { 
           // return e.getMessage();
           return null;
        }
        
        return new PageReference(URL);
    }

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////// VISUALFORCE METHODS ///////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    /*
     * @author      Marco Paulucci
     * @date        06.03.2015
     * @description Get clean object name
     */
    public String getObjectName () {
        String objectName = '';
        try {
            objectName = recordId.getSObjectType().getDescribe().getLabel();
        } catch (Exception e) {}
        return objectName;
    }

    /*
     * @author      Marco Paulucci
     * @date        09.03.2015
     * @description Create a list of pages to display
     */
    public List<String> getPages () {
        List<String> pages  = new List<String>();
        Integer c           = generalPager.getPageNumber(); // current
        Integer t           = generalPager.getTotalPages(); // total
        Boolean skip        = false;

        if (settings.F_PageIndexes__c) {
            for(Integer i = 1; i <= t; i++) {
                if(i < 3 || i > t - 2 || i == c || i == c - 1 || i == c + 1){
                    pages.add(String.valueOf(i));
                    skip = true;
                } else if (skip) {
                    pages.add('...');
                    skip = false;
                }
            }
        }
        return pages;
    }

    /*
     * @author      Marco Paulucci
     * @date        11.03.2015
     * @description Visualforce search method caller
     */
    public PageReference doSearch () {
        doSearch(false);
        return null;
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Adds selected Segments to a list that will be
     *              looped on Save action
     */
    public PageReference doAdd () {
        Integer i = 0;

        // Look for segments stored in other pages
        for (Id k : segmentsPaginated.keySet()) {
            segmentsYes.add(segmentsPaginated.get(k));
            segmentsToAdd.add(k);
            segmentsNoSearch.add(k);
            i++;
        }

        // Look for segments stored in the current page
        for (Segment s : segmentsNot) {
            if (!segmentsPaginated.containsKey(s.segment.Id) && s.checked) {
                segmentsYes.add(s);
                segmentsToAdd.add(s.segment.Id);
                segmentsNoSearch.add(s.segment.Id);
                i++;
            }
        }

        segmentsPaginated.clear();
        
        // Reload the search result and display success message
        if (i == 0) {
            addMessage(System.Label.Fielo_MS_NothingToAdd, 'W');
        } else {
            doSearch(true);
            addMessage(System.Label.Fielo_MS_AddedToMemory.replace('{%COUNT}', String.valueOf(i)), 'C');
        }
        return null;
    }

    /*
     * @author      Marco Paulucci
     * @date        10.03.2015
     * @description Adds selected Segments to a list that will be
     *              looped on Save action
     */
    public PageReference doAddAll () {
        Integer i = 0;
        for (FieloEE__RedemptionRule__c o : Database.query(getQuery(SOQL.GET_SEARCH))) {
            segmentsToAdd.add(o.Id);
            segmentsNoSearch.add(o.Id);
            segmentsYes.add(new Segment(o, true));
            i++;
        }
        segmentsNot.clear();
        addMessage(System.Label.Fielo_MS_AddedToMemory.replace('{%COUNT}', String.valueOf(i)), 'C');
        return null;
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Return to previous page
     */
    public PageReference doCancel () { 
        return new PageReference('/' + recordId);
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Combination of doQuickSave() and doCancel()
     */
    public PageReference doSave () { 
        doQuickSave();
        return doCancel();
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Creates Segment Domains for each selected Segment
     *              if they don't exist already, and removes domains if
     *              they were deselected
     */
    public PageReference doQuickSave () {
        // Initialize
        List<FieloEE__SegmentDomain__c> domainsToInsert = new List<FieloEE__SegmentDomain__c>();
        Set<Id>                         domainsToDelete = new Set<Id>();

        for (Integer i = segmentsYes.size() - 1; i >= 0; i--) {
            Segment s = segmentsYes[i];
            // Handle insert
            if (s.checked) {
                // We also need to make sure this is not an already added segment
                if (segmentsToAdd.contains(s.segment.Id)) {
                    FieloEE__SegmentDomain__c domain = new FieloEE__SegmentDomain__c(FieloEE__Segment__c = s.segment.Id);
                    domain.put(lookup, recordId);
                    domainsToInsert.add(domain);
                }
            }

            // Handle delete
            else {
                domainsToDelete.add(s.segment.Id);
                segmentsYes.remove(i);
                segmentsNoSearch.remove(s.segment.Id);
            }
        }

        try {
            if (!domainsToInsert.isEmpty()) {
                insert domainsToInsert;
                segmentsToAdd.clear();
                addMessage(System.Label.Fielo_MS_SegmentsAdded.replace('{%COUNT}', String.valueOf(domainsToInsert.size())),'C');
            }
            if (!domainsToDelete.isEmpty()) {
                // Since domainsToDelete can contain segments that were never inserted
                // in the first place, we need to get the real count from a query
                List<FieloEE__SegmentDomain__c> domains = Database.query('SELECT Id FROM FieloEE__SegmentDomain__c WHERE ' + lookup + ' = \'' + recordId + '\' AND FieloEE__Segment__c IN : domainsToDelete');
                
                String delCount = String.valueOf(domains.size());

                delete domains;

                // this will only ever be null if no search was done before
                // we don't want to "refresh" a search that never existed
                if (generalPager != null) {
                    doSearch(true);
                }
                addMessage(System.Label.Fielo_MS_SegmentsDeleted.replace('{%COUNT}', delCount),'C');
            }
            if(domainsToInsert.isEmpty() && domainsToDelete.isEmpty()) {
                addMessage(System.Label.Fielo_MS_NothingChanged, 'W');
            } 
        } catch (DMLException e) {
            addMessage(e.getMessage(), 'E');
        }
        fixRecordSegments();
        return null;
    }

    public void fixRecordSegments(){
        System.debug('RAHL fixRecordSegments');
        System.debug('RAHL lookup ' + lookup);
        if(!lookup.equals('FieloEE__Banner__c') && !lookup.equals('FieloEE__News__c') && !lookup.equals('FieloEE__Reward__c') && !lookup.equals('FieloEE__Menu__c')) return;
        sObject theRecord = Database.query('SELECT Id, FieloEE__RedemptionRule__c, ' + segmentCheckStr + ' FROM ' + lookup + ' WHERE Id = \'' + recordId + '\'');
        List<FieloEE__SegmentDomain__c> domains = Database.query('SELECT Id, FieloEE__Segment__c FROM FieloEE__SegmentDomain__c WHERE ' + lookup + ' = \'' + recordId + '\'');
        Integer count = 0;
        if(theRecord.get('FieloEE__RedemptionRule__c') != null) count++;
        count += domains.size();
        System.debug('RAHL count ' + count);
        if(count == 0 && !(Boolean)theRecord.get(segmentCheckStr)){ 
            return;
        }else{
            if(count == 0){
                theRecord.put(segmentCheckStr, false);
                theRecord.put('FieloEE__RedemptionRule__c', null);
            }else if(count == 1){
                if((Boolean)theRecord.get(segmentCheckStr)){
                    theRecord.put(segmentCheckStr, false);
                    if(!domains.isEmpty()){
                        theRecord.put('FieloEE__RedemptionRule__c', domains.get(0).FieloEE__Segment__c);
                        delete domains;
                    }
                }else if(theRecord.get('FieloEE__RedemptionRule__c') == null){
                    theRecord.put('FieloEE__RedemptionRule__c', domains.get(0).FieloEE__Segment__c);
                }
            }else{
                if(theRecord.get('FieloEE__RedemptionRule__c') != null){
                    FieloEE__SegmentDomain__c domain = new FieloEE__SegmentDomain__c(FieloEE__Segment__c = (Id)theRecord.get('FieloEE__RedemptionRule__c'));
                    domain.put(lookup, recordId);
                    insert domain;
                }
                theRecord.put(segmentCheckStr, true);
                theRecord.put('FieloEE__RedemptionRule__c', null);
            }
            update theRecord;
        }
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Jumps to next page
     */
    public PageReference doNext () {
        storeSelectedSegments();
        generalPager.next();
        setRecords();
        return null;
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Jumps to previous page
     */
    public PageReference doBack () {
        storeSelectedSegments();
        generalPager.previous();
        setRecords();
        return null;
    }

    /*
     * @author      Marco Paulucci
     * @date        09.03.2015
     * @description Create a list of pages to display
     */
    public PageReference doPage () {
        storeSelectedSegments();
        generalPager.setPageNumber(Integer.valueOf(ApexPages.currentPage().getParameters().get('index')));
        setRecords();
        return null;
    }


    ///////////////////////////////////////////////////////////////////////////
    ////////////////////////////////// OTHER //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Message handler
     */
    private void addMessage (String text, String type) {
        if (type == 'E') {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, text));
        } 
        else if (type == 'W' && settings != null && settings.F_Messages__c) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, text));
        } 
        else if (type == 'C' && settings != null && settings.F_Messages__c) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.CONFIRM, text));
        }
    }

    /*
     * @author      Marco Paulucci
     * @date        05.03.2015
     * @description Wrapper subclass to contain boolean values
     */
    private class Segment {
        public FieloEE__RedemptionRule__c segment { get; set; }
        public Boolean checked { get; set; }

        public segment (FieloEE__RedemptionRule__c segment, Boolean checked) {
            this.segment  = segment;
            this.checked  = checked;
        }
    }
}