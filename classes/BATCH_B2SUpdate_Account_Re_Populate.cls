global class BATCH_B2SUpdate_Account_Re_Populate implements Database.Batchable<sObject>
{
    String query;

    global BATCH_B2SUpdate_Account_Re_Populate(String Q)
    {
        query=Q;
    }
      
    global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext BC, List<Account> scope)  // TODO : Update the object name in List<_______>
    {
        //List<Account> accns = new List<Account>();
        for(Account s : scope)
        {
            Account a = (Account)s;
            
            // TODO : Update the fields to be re populated 
            //a.BillingCountryCode=a.TECH_SDH_CountryCode__c; 
            //a.BillingStateCode=a.TECH_SDH_StateProvCode__c;
            a.BillingCity=a.City__c;
            a.BillingStreet=a.Street__c;
            a.BillingPostalCode=a.ZipCode__c;
            //accns.add(a);            
                  
        }
        
   
        
        Database.SaveResult[] srList = Database.update(scope, false);
        for(Database.SaveResult sr : srList) 
        {
            if (sr.isSuccess()) 
            {
                System.debug('Updated Nullified successfully for ' +
                sr.getId());
            }
            else 
            {
                System.debug('Updating returned the following errors.');
                for(Database.Error e : sr.getErrors()) {
                System.debug(e.getMessage());
            }
        }

    }
      
   }
    global void finish(Database.BatchableContext BC)
    {
    }  
    
}