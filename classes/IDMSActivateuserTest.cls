/*
* Test for IDMSActivateUser
* */
@isTest
global class IDMSActivateuserTest{
    
    //Test 1: Creating a user and then activating him only on IDMS
    static testmethod void testdoPut1(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ActivateUser';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        User userput = new User(alias = 'userput', email='userput' + '@accenture.com', 
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='userput' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userput ;
        Test.startTest();
        try{
            User user = new User();
            user.id = userput.id;
            user.FederationIdentifier = userput.FederationIdentifier; 
            user.IDMS_Registration_Source__c = user.IDMS_Registration_Source__c; 
            IDMSActivateuserRest.doPut(user);
        }catch(exception e) {}
        Test.stopTest(); 
    }
    
    //Test 2: Creating a user and then activating him in both IDMS and UIMS
    static testmethod void testdoPut2(){
        Boolean IsActive;
        list <user> ulist= new list <user>();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ActivateUser';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        User UserObj1 = new User(alias = 'user', email='user' + '@accenture.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000',FederationIdentifier ='307-Test123',IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111');
        
        insert UserObj1;
        // UserObj1.IsActive=false;
        //update UserObj1;
        try{
            Test.startTest(); 
            IDMSActivateuser obj = new IDMSActivateuser(); 
            obj.Activateuser(UserObj1.id,UserObj1.FederationIdentifier,'');
            obj.Activateuser(UserObj1.id,UserObj1.FederationIdentifier,'UIMS');
            obj.Activateuser('',UserObj1.FederationIdentifier,'UIMS');
            obj.Activateuser('','','IDMS'); 
            obj.Activateuser(UserObj1.id,UserObj1.FederationIdentifier,'');
            obj.Activateuser(UserObj1.id,'','sample');
            obj.Activateuser('',UserObj1.FederationIdentifier,'');
            obj.Activateuser(UserObj1.id,UserObj1.FederationIdentifier,'UIMS');
            obj.Activateuser(UserObj1.id,UserObj1.FederationIdentifier,'idms');
            Test.stopTest() ; 
        }catch(exception e ){} 
        //obj.Activateuser('','','');
        
    }
    
}