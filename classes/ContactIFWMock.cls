@isTest
global class ContactIFWMock implements HttpCalloutMock{
    global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('fail');
        res.setStatusCode(500);
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{\"Fault\": {\"faultcode\": \"ns0:Server\",\"faultstring\": \"The message could not be sent to the service group IFW_UAT_BPM_ValOn_SG. Check whether the corresponding service container is running.\",\"faultactor\": \"com.eibus.web.soap.Gateway.wcp\",\"detail\": {\"FaultDetails\": {\"LocalizableMessage\": {\"MessageCode\": \"Cordys.WebGateway.Messages.WG_SOAPTransaction_ReceiverDetailsInvalid\",\"Insertion\": \"IFW_UAT_BPM_ValOn_SG\"}},\"FaultRelatedException\": \"fail\"}}}');
        return res;
    }
}