/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | createNewRMA                                                                       |
|                       |                                                                                    |
|     - Object(s)       | RMA__c                                                                             |
|     - Description     |   - Used to set the Record Type on a new RMA                                       |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
public class AP45_CreateNewRMA
{
     /*=======================================
        PUBLIC METHODS
     =======================================*/
     /** This method is used to:
           1) set the Record Type to 'UnvalidatedRMA'
           2) the RMA Shipping Address is defined by the First Return Item (if any) **/ 
     public static void createNewRMA(List<RMA__c> RMAs)
     {
        system.debug('>>>>> INFO: createNewRMA(List<RMA__c> RMAs) method begins');
        system.debug('>>>>> INFO: RMAs before update: ' + RMAs);
        
        //Update Record Type
        for(RMA__c aRMA : RMAs)
        {
           aRMA.RecordTypeId = Label.CL10203;   //Record Type = UnvalidatedRMA
        }

        system.debug('>>>>> INFO: RMAs after update: ' + RMAs);
        system.debug('>>>>> INFO: createNewRMA(List<RMA__c> RMAs) method ends');
     }
}