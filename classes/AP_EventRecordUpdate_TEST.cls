@isTest
private class AP_EventRecordUpdate_TEST 
{
      
    
   static testMethod void testEventLocation()  
    {
        
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;      
        
        Account acc1 = new Account(Name='Parent Acc', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO');       
        insert acc1;
        
        Contact contact1 = Utils_TestMethods.createContact(acc1.Id, 'Test');
        insert contact1;
        
        Contact contact2 = Utils_TestMethods.createContact(acc1.Id, 'Test');
        insert contact2;
        
      Opportunity  opp = new Opportunity(Name='opp test', AccountId=acc1.id ,Amount=1000, OpptyType__c='Standard', OpportunityCategory__c='Master', IncludedInForecast__c='no', StageName='3 - Identify & Qualify', CloseDate=Date.today()+30, LeadingBusiness__c='BD', MarketSegment__c='BDZ', CountryOfDestination__c=ct.id);
        insert opp;
        
       User usr= [SELECT Id, IsActive,ProfileId FROM User WHERE IsActive = true AND ProfileId =:Label.CL00110 limit 1];
        
        Event evt1 = new Event(Subject='Event location Test' ,RecordTypeId=Label.CLMAY13SLS01, Status__c='Planned',StartDateTime =Datetime.Now() ,EndDateTime=Datetime.Now()+5, WhatId=acc1.id );
        insert evt1; 
        
        Event evt2 = new Event(Subject='Event location Test' ,RecordTypeId=Label.CLMAY13SLS01, Status__c='Planned',StartDateTime =Datetime.Now() ,EndDateTime=Datetime.Now()+5, WhatId=opp.id );
        insert evt2;
        
        evt2.WhatId=acc1.id;
        update evt2;
        
        Event evt3 = new Event(Subject='Event location Test' ,RecordTypeId=Label.CLOCT14SLS22, Status__c='Planned',StartDateTime =Datetime.Now() ,EndDateTime=Datetime.Now()+5, WhoId=contact1.id );
        insert evt3; 
        
        evt3.WhoId = contact2.Id;
        evt3.WhatId = null;
        update evt3;
        
        evt3.WhoId = contact1.Id;
        update evt3;
    }
}