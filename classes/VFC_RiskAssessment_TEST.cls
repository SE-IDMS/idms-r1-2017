/*
    Author          : Bhuvana Subramaniyan
    Description     : Test class for VFC_RiskAssessment.
*/


@isTest
private class VFC_RiskAssessment_TEST 
{
    static testMethod void testRiskAssessment()
    {
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        insert cfwr;
        
        test.startTest();
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cfwr);
        VFC_RiskAssessment rskAssment = new VFC_RiskAssessment(sc1);
        rskAssment.completeRiskAssessment();
        rskAssment.edit();
        rskAssment.Cancel();
        rskAssment.comments_section1='test';
        rskAssment.comments_section2_1_1='test';
        rskAssment.comments_section2_1_2='test';
        rskAssment.comments_section2_2='test';
        rskAssment.comments_section3_1='test';
        rskAssment.comments_section4_1_1='test';
        rskAssment.comments_section4_1_2='test';
        rskAssment.comments_section4_1_3='test';
        rskAssment.comments_section4_2_1='test';
        rskAssment.comments_section4_2_2='test';
        rskAssment.comments_section4_3_1='test';
        rskAssment.comments_section4_3_2='test';
        rskAssment.comments_section4_4='test';
        rskAssment.comments_section4_5='test';
        rskAssment.comments_section4_6='test';
        rskAssment.comments_section5_1='test';
        rskAssment.comments_section5_2  ='test';
        rskAssment.comments_section6_1_1='test';
        rskAssment.comments_section6_1_2  ='test';
        rskAssment.comments_section6_2='test';
        rskAssment.comments_section7_1='test';
        rskAssment.comments_section7_2  ='test';
        rskAssment.comments_section7_3='test';
        rskAssment.comments_section7_4  ='test';
        rskAssment.comments_section8_1='test';
        rskAssment.comments_section8_2  ='test';
        rskAssment.comments_section9_1='test';
        rskAssment.comments_section9_2='test';
        rskAssment.urlValue = 'testValue';
        rskAssment.save();
        
        rskAssment.comments_section1='test1';
        rskAssment.comments_section2_1_1='test1';
        rskAssment.comments_section2_1_2='test1';
        rskAssment.comments_section2_2='test1';
        rskAssment.comments_section3_1='test1';
        rskAssment.comments_section4_1_1='test1';
        rskAssment.comments_section4_1_2='test1';
        rskAssment.comments_section4_1_3='test1';
        rskAssment.comments_section4_2_1='test1';
        rskAssment.comments_section4_2_2='test1';
        rskAssment.comments_section4_3_1='test1';
        rskAssment.comments_section4_3_2='test1';
        rskAssment.comments_section4_4='test1';
        rskAssment.comments_section4_5='test1';
        rskAssment.comments_section4_6='test1';
        rskAssment.comments_section5_1='test1';
        rskAssment.comments_section5_2  ='test1';
        rskAssment.comments_section6_1_1='test1';
        rskAssment.comments_section6_1_2  ='test1';
        rskAssment.comments_section6_2='test1';
        rskAssment.comments_section7_1='test1';
        rskAssment.comments_section7_2  ='test1';
        rskAssment.comments_section7_3='test1';
        rskAssment.comments_section7_4  ='test1';
        rskAssment.comments_section8_1='test1';
        rskAssment.comments_section8_2  ='test1';
        rskAssment.comments_section9_1='test1';
        rskAssment.comments_section9_2='test1';
        rskAssment.save();     
        test.stopTest();        
        
    }
}