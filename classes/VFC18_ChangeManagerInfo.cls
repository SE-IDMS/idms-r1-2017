/*
 
  Author: Amitava Dutta
  Created On: 24/12/2010.
  Purpose: Updating / associating the Manager with Record Owner.
  Basically it is related to 'Related To' Field setting.


*/



public with Sharing class VFC18_ChangeManagerInfo {

    //region Private Members
    private string mstrOpSettingID;
    List<User> lstUsers = null;
    List<User> objLstUser = null;
    User objUser = null;
    public string errormessage {get;set;}
    public string currentOwner{get;set;}
   // private string mstrOpSettingName;
    private string mstrSearchString;
   
    //end region
    // region Public Members
    public String ObjectiveSettingsId
    {
        get{return mstrOpSettingID;} set{mstrOpSettingID = value;}
    }
   
    public String SearchString
    {
        get{return mstrSearchString;} set{mstrSearchString = value;}
    }
   
    //end region
    // region Public Class Constructor
    public VFC18_ChangeManagerInfo(ApexPages.StandardController controller) 
    {
        ObjectiveSettingsId = ApexPages.currentPage().getParameters().get('OpSetID');
        ObjectivesSettings__c objective = [SELECT OWNERID FROM OBJECTIVESSETTINGS__c WHERE ID=:ObjectiveSettingsId];
        currentOwner = objective.OwnerId;
        
        objLstUser = [Select u.Id, u.ObjectivesApprover__c, u.ObjectivesApprover__r.FirstName, u.ObjectivesApprover__r.LastName from User u
        WHERE ID IN (SELECT OWNERID FROM OBJECTIVESSETTINGS__c WHERE ID=:ObjectiveSettingsId )] ;
        if(objLstUser.size()!=0)
        {
            //ObjectiveSettingsName = objLstObj[0].NAME;         
            SearchString = objLstUser[0].ObjectivesApprover__r.FirstName;
        }   
        
    }
    // end region
    // region Public Class Methods
       // Search and Generate User List
       public PageReference Search()
       {
           GetAllManager();
           return null;
       } 
       
       // Change / Set Manager
       
       public PageReference ChangeUser()
       {
             PageReference pgref = null;
             System.Debug('**** Update the Manager for the Obejective Owner begins ***** ');
        
             if(ApexPages.currentPage().getParameters().get('UID') != currentOwner )
             {
                List<ObjectivesSettings__c> lstUserInfo = [SELECT NAME,ObjectiveLeader__c,OwnerID FROM ObjectivesSettings__c WHERE ID=:ObjectiveSettingsId];
                if(lstUserInfo.size() !=0)
                {   
                    objUser = [SELECT ObjectivesApprover__c FROM USER WHERE ID =: lstUserInfo [0].OwnerID];
                    objUser.ObjectivesApprover__c =ApexPages.currentPage().getParameters().get('UID');//'005A000000125cW';// System.currentPageReference().getParameters().get('uname');
                    List<User> lstUser = new List<User>();
                    lstUser.add(objUser);
                    if(lstUser.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                    {
                        System.Debug('######## AP04 Error updating : '+ Label.CL00264);
                    }
                    else
                    {
                        Database.SaveResult[] UpdateManager = Database.update(lstUser, false);
                            
                        for(Database.SaveResult ir: UpdateManager )
                        {
                            if(!ir.isSuccess())
                                        Database.Error err = ir.getErrors()[0];
                        } 
                    }   
                }    
                pgref = new PageReference('/' + ObjectiveSettingsId);
            }
            else
            {
                errormessage = Label.CL00270;
                pgref = ApexPages.currentPage();
            
            }
            
           
                System.Debug('**** Ends *****'); 
                return pgref; //new PageReference('/' + ObjectiveSettingsId);
       }
              
       //Populate the User List
       public List<User> GetAllManager()
       {
            System.Debug('***** Searching Operation begins ****');
            string strKeyWord = '';
            strKeyWord += SearchString + '%';
            List<User> lstUserList = null;
            // Populating on the basis of Search / Default
            if(SearchString == null || SearchString == '')
            {
                 lstUserList= [SELECT u.FirstName, u.LastName,ID,u.UserRole.Name from User u 
                 WHERE u.IsActive = true AND LastLoginDate <> null  ORDER BY LastLogInDate DESC Limit 5  ];
            }
            else
            {
                lstUserList = [SELECT u.FirstName, u.LastName,ID,u.UserRole.Name from User u 
                WHERE (FirstName LIKE: strKeyWord + '%' or LastName LIKE: strKeyWord + '%') And u.IsActive = true Limit 120];
               
            }
            System.Debug('***** Ends *****');
            return lstUserList ;
       }
       
       // Redirect the User to Source And Cancelling the Operation
       public PageReference CloseWindow()
       {
           return new PageReference ('/' + ObjectiveSettingsId);
       }
    
    // end region
    

}