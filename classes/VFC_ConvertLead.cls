/*
29-June-2012    Srinivas Nallapati  Mkt Sep Release     Initial creation: Used for enhanced lead conversion
29-Oct-2012     Srinivas Nallapati  Mkt Dec12 Release   When user tries to Convert a Closed Leads, Show error message BR-2272 
01-Mar-2013     Srinivas Nallapati  MKt May13 Release   Use generic user "Convertion Tool" for Lead Convertion - BR-2843
14-Mar-2013     Pooja Gupta Mkt May13 Release           Convert Lead Value Chain Player to Opportunity Value Chain Player
*/ 
public without sharing class VFC_ConvertLead{
    public String opptyCompanyName {get;set;}
    private Lead lead;
    
    public Lead SourceLead {get;set;}
    public Boolean bolCreateOpp {get;set;}
    
    //To store Account and Contacts ids
    public String strAccountId {get;set;}
    public String strContactId {get;set;}
    
    //For lead status and converted status values
    public String strStatus {get;set;}
    public String leadConvertedStatus { get; set; }
    
    //For task remider dateand time
    public Date ReminderDate { get; set; }
    public String ReminderTime { get; set; }
    
    //For required field validations - TODO replace with TECH_ReadyToConvert
    public boolean isRequiredInfoOnLeadNotFilled {get;set;} {isRequiredInfoOnLeadNotFilled = false;}
    
    //For source lead association with account and contact status 
    public boolean hasAccount {get;set;}{hasAccount=false;}
    public boolean hasContact {get;set;}{hasContact=false;}
    
    //To display Account and Contact name on page
    public string AccountName {get;set;}
    public string ContactName {get;set;} 
    Public string LeadOwnerId ;
    //Creating new task   
    public Task NewTask { 
        get ;
        set; 
    }
    public boolean donotcreatetask {get;set;} {donotcreatetask = true;}
    private String first_picklist_option = null; //first value to display in picklist
    
    public Boolean hasError {get;set;} {hasError = false;}
    LeadStatus convertStatus;
    List<LeadValueChainPlayer__c> LstLVCP1 = new list<LeadValueChainPlayer__c>();
    
    
    public List<AccConWrap> DuplicateAccountsWrap {get;set;}
    public List<AccConWrap> DuplicateContactsWrap {get;set;}
    public Database.DMLOptions DML {get;set;}          
    
    
    public VFC_ConvertLead(ApexPages.StandardController main) {
        SourceLead = [SELECT MarketSegment__c, Status, OpportunityDoesNotExist__c, Id, FirstName, LastName, OwnerId,Account__C,Account__r.name,Account__r.ownerId,Account__r.isPersonAccount, Contact__r.name, TECH_Ready2Convert__c, Contact__C, Company, Street, City, State, PostalCOde, Country, Phone, Fax, ZipCode__c, SuspectedValue__c, OpportunityType__c, OpportunityFinder__c, LeadingBusiness__c, ClassificationLevel1__c, City__c,RecordTypeId, QualificationInfo__c FROM Lead WHERE Id = :main.getId()];
        strAccountId = '';
        strContactId = '';
        
        //UAT May13
        //July14 Mkt Revanth Kumar R: commented the below query as it is modified and moved.
        //convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        LstLVCP1 = [Select Account__c,AccountRole__c,Contact__c,ContactRole__c,LeadingPartner__c from LeadValueChainPlayer__c where Lead__c=:SourceLead.ID];
        
        //May13 Mkt   Srinivas Nallapati
        //If logged in user DOES NOT have Edit access on Lead
        if(!( VFC_ConvertLead.hasEditOnLead(Userinfo.getUserId(), SourceLead.Id)) )
        {
            hasError = true;    
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13MKT003));
        }
        //End of May13 Change - Srinivas
        //If Owner is a Queue - Srinivas -Oct13
        LeadOwnerId = SourceLead.OwnerId;
        if(LeadOwnerId.StartsWith('00G'))
        {
            hasError = true;    
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLOCT13MKT002));
        }
        //End of owner is queue check
        //Srinivas- Oct 13  DEF-3307  CLOCT13MKT003
        if(SourceLead.SuspectedValue__c < 0)
        {
            hasError = true;    
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLOCT13MKT003));
        }
        //End of Suspected value check
        // July 14 Mkt Revanth Kumar R
        if(SourceLead.Status != Label.CL00554 && SourceLead.Status != Label.CL00447 && SourceLead.RecordTypeid != System.Label.CLJUL14MKT01) //if Closed
        {
            hasError = true;    
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT07));
        }
        //End of changes
        if( SourceLead.Company == null && SourceLead.Account__c == null && hasError == false) // Only show this warning on convert page
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,System.Label.CLOCT13MKT001));
        
        if( SourceLead.Company == null )
        {
            opptyCompanyName = SourceLead.FirstName + ' ' + SourceLead.LastName;
        }
        else
        {
            opptyCompanyName = SourceLead.company;
        }
        
        //Task Related 
        DateTime dT = System.now();
        NewTask = new Task();
        ReminderDate = Date.newinstance(dT.year(), dT.month(), dT.day());
        ReminderTime = '08:00 AM';        
        NewTask.TECH_ReminderDate__c = System.Today();
        NewTask.Subject = System.Label.CLSEP12MKT15;
        //End of Task related
        
        
        
        
        //Required fields validation 
        if(SourceLead.TECH_Ready2Convert__c == 'false')
            isRequiredInfoOnLeadNotFilled = true;
        
        
        //Conversion related
        //July14 Mkt Revanth Kumar R: To query and assign ConvertStatus according to there 'Record Type'
        if(SourceLead.RecordTypeid == System.Label.CLJUL14MKT01)
            leadConvertedStatus = System.Label.CLJUL14MKT02;
        else
            leadConvertedStatus = System.Label.CL00561;
        convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true and  MasterLabel= :leadConvertedStatus  LIMIT 1];
        System.Debug('*** convertStatus  ***'+convertStatus.MasterLabel);
        // Changes end Revanth Kumar R
        
        bolCreateOpp = (SourceLead.RecordTypeid == System.Label.CLJUL14MKT01) ? SourceLead.OpportunityDoesNotExist__c : false; 
        if(SourceLead.Account__c !=null){
            hasAccount= true;
            AccountName = SourceLead.Account__r.name;
            strAccountId =SourceLead.Account__c ;
            //Set the record owner here
            SourceLead.ownerId = LeadOwnerId = SourceLead.Account__r.ownerId;
        }
        else{
            SourceLead.ownerId = null;
        }
        
        if(SourceLead.Contact__c !=null){
            hasContact= true;
            ContactName = SourceLead.Contact__r.name;
            strContactId =SourceLead.Contact__c; 
        }
        
        //End of Conversion related
        SalesTeamMemberWrapper tmwrap1 = new SalesTeamMemberWrapper(new DataTemplate__c());
        lstSalesTeamWrap.add(tmwrap1);
        SalesTeamMemberWrapper tmwrap2 = new SalesTeamMemberWrapper(new DataTemplate__c());
        lstSalesTeamWrap.add(tmwrap2);
        SalesTeamMemberWrapper tmwrap3 = new SalesTeamMemberWrapper(new DataTemplate__c());
        lstSalesTeamWrap.add(tmwrap3);
        
        DuplicateAccountsWrap = new List<AccConWrap>();
        DuplicateContactsWrap = new list<AccConWrap>();
        LstCompanyInfo    = listOfCompaniesInfo();
        LstContactInfo    = listOfContactsInfo();
        DML = new Database.DMLOptions(); 
        DML.DuplicateRuleHeader.AllowSave = false;
        
        
    }// End of constructor
    
    public String OpportunityName { 
        set;
        get { return SourceLead.FirstName + ' ' + SourceLead.LastName; }
        
    }
    
    //builds a picklist of values based upon the passed information
    public List<selectOption> getPickValues(Sobject object_name, String field_name, String first_val) {
        List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
        if (first_val != null) { //if there is a first value being provided
            options.add(new selectOption(first_val, first_val)); //add the first option
        }
        Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            options.add(new selectOption(a.getLabel(), a.getValue())); //add the value and label to our final list
        }
        return options; //return the List
    }
    
    //return the picklist options for Lead Status
    public List<selectOption> getLeadStatus() {
        return getPickValues(SourceLead, 'Status', first_picklist_option);
    }
    
    public List<SelectOption> getReminderTimes() {
        
        List<SelectOption> lstReminderTimes = new List<SelectOption>();
        for (Integer i = 0; i < 24; i++) {
            String t = (i == 0 ? '12' : (i < 10 ? '0' + String.valueOf(i) : String.valueOf(i))) + ':00' + (i < 12 ? ' AM' : ' PM');
            lstReminderTimes.add(new SelectOption(i+'', t));
        }
        return lstReminderTimes;
    }  
    
    public List<SelectOption> LstCompanyInfo{get;set;}
    
    
    
    
    public List<SelectOption> LstContactInfo {get;set;}
    
    //////////////////////////
    // Action Methods
    //////////////////////////
    
    /*public List<SelectOption> getlstCompanyInfo()
    {
     return lstCompanyInfo;
    }
    
    public List<SelectOption> getLstContactInfo()
    {
      return LstContactInfo;
     }*/
    
    
    //Method checks if a Contact selection need for this conversion. will redirect user to the Contact selection page.
    //Contact selection is not needed. if lead has associated Contact or New Account is being created for the conversion.     
    public PageReference checkContact() {
        if(String.isNotBlank(strAccountId)) //existing if Account is selected
        {
            List<Account> lac = new list<Account>();
            lac = [select name, isPersonAccount from Account where id=:strAccountId];
            if(lac.size()>0)
                AccountName = lac[0].name;
            
            if(String.isNotBlank(strContactId) || lac[0].isPersonAccount == true) //if Lead associated to contact, No need for contact selection
                return convertLead();
            else
                return page.VFP_ConvertLead_SelectAccContact;
        }    
        
        if(String.isBlank(strAccountId)){ // if new Account selected, no need for contact selection
            if(DuplicateAccountsWrap.isEmpty())
                return convertLead();
            else
                return null;
        }
        else
            return page.VFP_ConvertLead;    
    }
    
    public PageReference contactSelected(){
        Savepoint sp = Database.setSavepoint();
        if(String.isBlank(strContactId)){ // if new Account selected, no need for contact selection
            if(DuplicateContactsWrap.isEmpty())
                return convertLead();
            else
                return null;
        }
        else
            return convertLead();
    }
    
    //method Converts lead if Account and Contact selection is completed
    public PageReference convertLead() {
        Savepoint sp = Database.setSavepoint(); //Savepoint to rollback if any exception
        Utils_SDF_Methodology.BypassOnLeadConvertion = true;
        
        System.debug('**** The bypass trigger value is '+Utils_SDF_Methodology.BypassOnLeadConvertion);
        try{    
            
            
            //May13 Change
            //if(Userinfo.getUserId() != SourceLead.OwnerId)
            if(System.Label.CLMAY13MKT002 != SourceLead.OwnerId)
            {
                if(!(SourceLead.OpportunityDoesNotExist__c && String.IsBlank(SourceLead.Account__c) && String.IsBlank(SourceLead.MarketSegment__c) && SourceLead.ClassificationLevel1__c == 'EU')){
                    AP19_Lead.isConvertion = true;
                    Lead ledUpdateTechFromConnvert = new Lead(id=SourceLead.id);
                    ledUpdateTechFromConnvert.TECH_CreatedFromLead__c = false;
                    ledUpdateTechFromConnvert.SubStatus__c = bolCreateOpp ?  System.Label.CLMAR16MKT004 : System.Label.CLMAR16MKT003 ;
                    update ledUpdateTechFromConnvert;
                    AP19_Lead.isConvertion = false;
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLJUN16MKT002));
                    return null;
                }

            }
            // Create LeadConvert object
            Database.LeadConvert lc = new Database.LeadConvert();
            
            lc.setLeadId(SourceLead.Id);
            //lc.setOwnerId(SourceLead.OwnerId);
            //lc.setOwnerId(Userinfo.getUserId());
            //BR-2843 May13 Mkt relese Using "Convertion Tool" user for convertion
            lc.setOwnerId(System.Label.CLMAY13MKT002);
            
            if(String.isNotBlank(strAccountId)) { 
                System.debug('*** strAccountId '+strAccountId);
                lc.setAccountId(strAccountId); 
            }
            
            if(String.isNotBlank(strContactId)){ 
                lc.setContactId(strContactId ); 
                if(DuplicateAccountsWrap.isEmpty()){
                    for(AccConWrap dupCon:DuplicateContactsWrap){
                        System.Debug('*** strContactId == dupCon.Con.Id' +dupCon.Con.Id+' '+strContactId+' '+String.isBlank(strAccountId)+' '+strAccountId);
                        strAccountId = dupCon.Con.Id == strContactId ?  dupCon.Con.AccountId : strAccountId;
                    }
                    lc.setAccountId(strAccountId);
                }
                
            }
            
            
            lc.setDoNotCreateOpportunity(bolCreateOpp); //Create Opportunity during lead conversion
            
            // Set Opportunity Name
            if(bolCreateOpp == false) {lc.setOpportunityName( opptyCompanyName );  }
            
            // Set Lead Converted Status
            //LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            System.Debug('*** New Status ***' + convertStatus.MasterLabel);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            System.Debug('*** convertStatus  ***'+lc.ConvertedStatus);
            
            // Convert!
            AP19_Lead.isConvertion = true;
            System.Debug('*** START Converting Lead ***');
            
            Database.LeadConvertResult lcr;
            if(DML.DuplicateRuleHeader.AllowSave)
                lcr = Database.convertLead(lc,DML);
            else
                lcr = Database.convertLead(lc,false);
            
            System.Debug('*** convertStatus  ***'+lcr);
            System.Debug('*** END Converting Lead ***');                
            AP19_Lead.isConvertion = false;                
            // Redirect...
            PageReference prResult;
            if(lcr.isSuccess()){
                if(donotcreatetask == false)
                {
                    //TODO  Task creation
                    if(NewTask.IsReminderSet){
                        NewTask.ReminderDateTime = DateTime.newInstance(NewTask.TECH_ReminderDate__c, Time.newInstance(Integer.valueOf(ReminderTime), 0, 0, 0));
                    }
                    //NewTask.AccountId = lcr.getAccountId();
                    NewTask.WhatId = lcr.getOpportunityId();
                    NewTask.WhoId = lcr.getContactId();
                    NewTask.OwnerId = !bolCreateOpp ? SourceLead.OwnerId : LeadOwnerId;
                    
                    insert NewTask;
                    
                }   //End of Task creation
                
                // Mop up Opportunity 
                //May13 change
                //if(bolCreateOpp == false && Userinfo.getUserId() != SourceLead.OwnerId) {
                
                if(System.Label.CLMAY13MKT002 != SourceLead.OwnerId){
                    if(bolCreateOpp == false){
                        Opportunity o = new Opportunity(Id=lcr.getOpportunityId());
                        o.OwnerId = SourceLead.OwnerId;
                        o.TECH_CreatedFromLead__c = true;
                        if(String.isNotBlank(SourceLead.QualificationInfo__c))
                            o.Description = AP24_EmailToApex.ConvertHTMLtoPlainText(SourceLead.QualificationInfo__c);
                        
                        //o.RecordTypeId = '012A0000000gJSJ';
                        //o.StageName = '2 - Define Opportunity Portfolio';
                        update o;
                        system.debug('**** UPDATE OF SPLIT BEGINS**** ');
                        //UPDATES THE SPLIT PERCENTAGE OF LEAD CONVERSION USER TO 0,UPDATES THE SPLIT PERCENTAGE OF OPPORTUNITY OWNER TO 100
                        List<OpportunitySplit> splits = new List<OpportunitySplit>();
                        List<OpportunitySplit> leadConvOppSplit = [select Id, SplitTypeId,SplitOwnerId, OpportunityId, SplitPercentage from OpportunitySplit where OpportunityId=:o.Id];                    
                        //IF THERE IS ONE LEAD CONVERSION TOOL RECORD
                        if(leadConvOppSplit!=null && leadConvOppSplit.size()==1)
                        {
                            if(leadConvOppSplit[0].SplitownerId == System.Label.CLMAY13MKT002)
                            {                                 
                                //INSERTS THE OPPORTUNITY OWNER INTO SPLITS
                                OpportunitySplit ownerOppSplit = new OpportunitySplit();
                                ownerOppSplit = leadConvOppSplit[0].clone();
                                ownerOppSplit.SplitOwnerId = sourceLead.OwnerId;
                                ownerOppSplit.SplitPercentage = 0;  
                                system.debug('*********'+ ownerOppSplit);                      
                                Database.saveResult sv = Database.insert(ownerOppSplit,false);
                                if(sv.isSuccess()){
                                    
                                    //UPDATES THE PERCENTAGES
                                    leadConvOppSplit[0].SplitPercentage = 0;
                                    OpportunitySplit ownerOppSplitPer= new OpportunitySplit(Id=ownerOppSplit.Id);
                                    ownerOppSplitPer.SplitPercentage = 100;
                                    splits.add(leadConvOppSplit[0]);                        
                                    splits.add(ownerOppSplitPer);                        
                                    Database.saveResult[] svUpdate = Database.update(splits,false);
                                    for(Database.saveResult sr: svUpdate)
                                    {
                                        system.debug('***********'+ sr.getErrors());
                                    }
                                }
                            }
                        }
                        else if(leadConvOppSplit!=null && leadConvOppSplit.size()==2){
                        
                            //IF OPPORTUNITY OWNER IS THERE IN SPLIT RECORDS
                            OpportunitySplit ownerOppSplitPer,leadConvSplit;
                            for(OpportunitySplit split: leadConvOppSplit){
                                
                                if(split.SplitOwnerId == System.Label.CLMAY13MKT002){
                                    leadConvSplit = split;
                                }
                                else
                                    ownerOppSplitPer= split;
                            }
                            if(leadConvSplit!=null && ownerOppSplitPer!=null){
                                leadConvSplit.SplitPercentage = 0;
                                ownerOppSplitPer.SplitPercentage = 100;
                                splits.add(leadConvSplit);                        
                                splits.add(ownerOppSplitPer);                        
                                Database.saveResult[] sv = Database.update(splits,false);
                                for(Database.saveResult sr: sv){
                                    system.debug('***********'+ sr.getErrors());
                                }
                            }
                        }
                        system.debug('**** UPDATE OF SPLIT ENDS**** ');                 
                    }
                    System.Debug('*** START: Account Id from Lead Convert Result ***' + lcr.getAccountId());
                    //EUS - 014218
                    if(String.isBlank(strAccountId)){
                        Account acc = new Account(Id=lcr.getAccountId());
                        System.Debug('*** OwnerId '+SourceLead.OwnerId);
                        acc.OwnerId = !bolCreateOpp ? SourceLead.OwnerId : LeadOwnerId;
                        update acc;
                    }//end of eus    
                    System.Debug('*** END: Account Id from Lead Convert Result ***');
                    
                    System.Debug('*** START: Contact Id from Lead Convert Result ***' + lcr.getContactId());
                    //EUS - 014218
                    if(String.isBlank(strContactId)){
                        Contact con = new Contact(id=lcr.getContactId());
                        con.OwnerId = !bolCreateOpp ? SourceLead.OwnerId : LeadOwnerId;
                        update con;
                    }//end of eus
                    System.Debug('*** END: Contact Id from Lead Convert Result ***');
                }
                
                //Change for salesteam
                CreatedOpportunityId = lcr.getOpportunityId();
                insertTeamMembers();
                //End of change for salesteam
                
                
                // May 2013 Release - Convert Lead VCP to Opportunity VCP - BR-2842
                //Start BR-2842 - May 2013
                
                //List<LeadValueChainPlayer__c> LstLVCP1 = new list<LeadValueChainPlayer__c>();
                List<OPP_ValueChainPlayers__c> LstOppVCP = new list<OPP_ValueChainPlayers__c>();
                System.debug('************ May Start *****************');
                //LstLVCP1 = [Select Account__c,AccountRole__c,Contact__c,ContactRole__c,LeadingPartner__c from LeadValueChainPlayer__c where Lead__c=:SourceLead.ID];
                
                if(LstLVCP1.size() > 0)
                {    
                    List<CS_LeadToOpportunity__c> csLeadtoOpp = new list<CS_LeadToOpportunity__c>();
                    csLeadtoOpp = CS_LeadToOpportunity__c.getall().values();
                    For(LeadValueChainPlayer__c leadVCP: LstLVCP1)
                    {
                        OPP_ValueChainPlayers__c ocp = new OPP_ValueChainPlayers__c();
                        ocp.OpportunityName__c = lcr.getOpportunityId();
                        For(CS_LeadToOpportunity__c leadtoOppMap: csLeadtoOpp)
                        {
                            ocp.put(leadtoOppMap.destination__c, leadVCP.get(leadtoOppMap.source__c));
                        }
                        
                        LstOppVCP.add(ocp);
                    }
                    insert LstOppVCP;
                    System.debug('************ May End*****************');
                }
                //End BR-2842 - May 2013
                
                prResult = (String.isNotBlank(strAccountId) && lcr.getOpportunityId() != null) ? new PageReference('/' + lcr.getOpportunityId()) : new PageReference('/' + lcr.getAccountId()); 
                
                //Redirect user to Closed Duplicates page
                id ContactId = lcr.getContactId();
                Integer RelatedOpenLeadsCount = [select count() from lead where (Status = :Label.CL00554 OR Status = :Label.CL00447) and Contact__c =:ContactId];
                if(RelatedOpenLeadsCount > 0)
                {
                    PageReference pg = page.VFP92_CloseDuplicateLeads;
                    pg.getParameters().put('id', lcr.getLeadId() );
                    pg.getParameters().put('lcretURL', prResult.getURL() );
                    return pg;
                }
                else
                {   
                    prResult.setRedirect(true);
                    return prResult;  //Redirect to Account page, if no open related leads
                }        
                
            } 
            else {
                Database.Rollback(sp);
                Datacloud.DuplicateResult duplicateResult;
                List<String> recordIds = new List<String>();
                boolean isAccount = false;
                boolean isContact = false;
                for (Database.Error duplicateError : lcr.getErrors()) {
                    String AccDupMessage = 'We recommend you use an existing account instead';
                    String ConDupMessage = 'We recommend you use an existing contact instead';
                    if(duplicateError.getMessage().contains(AccDupMessage) || duplicateError.getMessage().contains(ConDupMessage)){
                        duplicateResult = ((Database.DuplicateError)duplicateError).getDuplicateResult();
                        for (Datacloud.MatchResult duplicateMatchResult : duplicateResult.getMatchResults()) {
                            for(Datacloud.MatchRecord duplicateMatchRecord : duplicateMatchResult.getMatchRecords()){
                                recordIds.add(duplicateMatchRecord.getRecord().Id);
                                String recordId = duplicateMatchRecord.getRecord().Id;
                                if(recordId.startswith('001'))
                                    isAccount = true;
                                else if(recordId.startswith('003'))
                                    isContact = true;
                            }
                        }
                    }
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, duplicateError.getStatusCode()+'-'+duplicateError.getMessage()));
                }
                if(isAccount){
                    DuplicateAccountsWrap.clear();
                    List<Account> duplicateAccounts = [SELECT Name,Owner.FirstName,Owner.LastName,Street__c,StateProvince__c,City__c,Country__c FROM Account WHERE Id IN :recordIds];
                    for(Account Acc:duplicateAccounts){
                        Boolean doNotHaveAccId = true;
                        AccConWrap AccWrap = new AccConWrap(false, Acc, null);
                        duplicateAccountsWrap.add(AccWrap);
                        for(selectOption companyInfo:lstCompanyInfo){
                            system.debug('The value is '+companyInfo.getValue());
                            if(String.isNotBlank(companyInfo.getValue())){
                                if(doNotHaveAccId)
                                    doNotHaveAccId = Acc.Id == companyInfo.getValue() ? false: true;
                            }
                            system.debug('The value is '+doNotHaveAccId);
                        }
                        if(doNotHaveAccId){
                            lstCompanyInfo.add(new selectOption(Acc.Id, Acc.Name ));
                            doNotHaveAccId = false;
                            system.debug('The value is'+doNotHaveAccId);
                        }
                    }
                    system.debug('****** '+duplicateAccountsWrap.Size());
                    
                }
                else If(isContact){
                    DuplicateContactsWrap.clear();
                    list<Contact> duplicateContacts = [SELECT AccountId, Account.Name, Name,FirstName,LastName,Email,Owner.FirstName,Owner.LastName,JobTitle__c FROM Contact WHERE Id IN :recordIds];
                    for(Contact Con:duplicateContacts){
                        Boolean doNotHaveConId = true;
                        AccConWrap ConWrap = new AccConWrap(false, null, Con);
                        duplicateContactsWrap.add(ConWrap);
                        for(selectOption ContactInfo:LstContactInfo){
                            system.debug('The value is '+ContactInfo.getValue());
                            if(String.isNotBlank(ContactInfo.getValue())){
                                if(doNotHaveConId)
                                    doNotHaveConId = Con.Id == ContactInfo.getValue() ? false: true;
                            }
                            system.debug('The value is '+doNotHaveConId);
                        }
                        if(doNotHaveConId){
                            LstContactInfo.add(new selectOption(Con.Id, Con.Name ));
                            doNotHaveConId = false;
                            system.debug('The value is'+doNotHaveConId);
                        }
                    }
                    return page.VFP_ConvertLead_SelectAccContact;
                }
                Utils_SDF_Methodology.BypassOnLeadConvertion = false;
                return null;
            }
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage())); 
            system.debug('###Exception## '+ex.getStackTraceString());
            Database.Rollback(sp); // Rollback all the changes in the event of exception
            Utils_SDF_Methodology.BypassOnLeadConvertion = false;
            return null;
        } 
        
        
    }//End of ConvertLead()
    
    //Change for Salesteam
    public class SalesTeamMemberWrapper{
        public DataTemplate__c  tm {get;set;}
        public Boolean isSelected {get;set;}
        public SalesTeamMemberWrapper(DataTemplate__c  otm)
        {
            tm = otm;
        }
    }    
    public List<SalesTeamMemberWrapper> lstSalesTeamWrap {get;set;} {lstSalesTeamWrap = new List<SalesTeamMemberWrapper>();}
    public Id CreatedOpportunityId;
    
    public void addTeamMember(){
        SalesTeamMemberWrapper tmwrap = new SalesTeamMemberWrapper(new DataTemplate__c());
        lstSalesTeamWrap.add(tmwrap);
    }
    public void removeSelectedTeamMember(){
        List<SalesTeamMemberWrapper> lstNotSelectedSalesTeamWrap = new List<SalesTeamMemberWrapper>();
        for(SalesTeamMemberWrapper tmwrap : lstSalesTeamWrap) 
        {
            if(tmwrap.isSelected == false)
                lstNotSelectedSalesTeamWrap.add(tmwrap);   
        }
        lstSalesTeamWrap = lstNotSelectedSalesTeamWrap;
    } 
    
    public void insertTeamMembers(){ 
        //removeSelectedTeamMember();
        if(lstSalesTeamWrap.size() > 0  && CreatedOpportunityId != null)
        {
            list<OpportunityTeamMember> lstOtm = new list<OpportunityTeamMember>();
            set<id> selectedUsers = new set<id>();
            for(SalesTeamMemberWrapper tmwrap : lstSalesTeamWrap)
            {
                if(tmwrap.tm.User__C != null && !selectedUsers.contains(tmwrap.tm.user__C))
                {
                    selectedUsers.add(tmwrap.tm.User__C);
                    OpportunityTeamMember otm = new OpportunityTeamMember(userid = tmwrap.tm.User__C, OpportunityId = CreatedOpportunityId, TeamMemberRole=System.Label.CLSEP12MKT16);
                    lstOtm.add(otm);
                }
                
            }
            if(lstOtm.size() > 0)
            {
                insert lstOtm;
                
                List<OpportunityShare> shares = [select Id, OpportunityAccessLevel, 
                                                 RowCause from OpportunityShare where OpportunityId = :CreatedOpportunityId and UserOrGroupId in:selectedUsers 
                                                 and RowCause = 'Team'];
                for (OpportunityShare share : shares) 
                    share.OpportunityAccessLevel = System.Label.CLSEP12MKT17;
                update shares;  
                
            }   
        }    
    }
    //End of change for salesteam
    
    //May13 Marketing Srinivas Nallapati
    public static boolean hasEditOnLead(id UserId, id LeadId)
    {
        
        UserRecordAccess ura = [SELECT HasAllAccess,HasEditAccess,MaxAccessLevel,RecordId FROM UserRecordAccess WHERE RecordId = :LeadId AND UserId = :UserId];
        if(ura != null && ura.HasEditAccess == true)
            return true;
        else
            return false;
    }
    //End of May13 Srinivas Nallapti   
    
    //Method creates a select list of Accounts for user selection using fuzzy search using the Lead name    
    public List<SelectOption> listOfCompaniesInfo(){
        List<SelectOption> lstCompanies = new List<SelectOption>();
        if(hasAccount) {
            lstCompanies.add(new SelectOption(strAccountId,AccountName));
        }
        else{
            //DEC 12 AC UAT Srinivas
            
            
            String strSearch = 'SELECT Id, Name, Owner.Name FROM Account';
            List<String> whereConditions = new List<String>();
            String whereClause='';
            whereClause +='Name LIKE \'%'+Utils_Methods.escapeForWhere( opptyCompanyName )+'%\'';
            
            list<String> AvailabelRIds = new list<String>();
            list<String> notAvailabelRIds = new list<String>();
            Schema.DescribeSObjectResult AccDes = Schema.SObjectType.Account;
            Map<Id,Schema.RecordTypeInfo> rtMapById = AccDes.getRecordTypeInfosById();
            for(id i : rtMapById.keyset())
            {
                if(rtMapById.get(i).isAvailable())
                    AvailabelRIds.add(i);
                else
                    notAvailabelRIds.add(i);   
            }
            String recordTypeEquals;
            if(AvailabelRIds.size() > 0)
            {
                recordTypeEquals = '( ';
                for(Integer  i=0; i< AvailabelRIds.size(); i++)
                {
                    recordTypeEquals =recordTypeEquals + ' RecordTypeId =\'' + Utils_Methods.escapeForWhere(AvailabelRIds.get(i)) + '\'';
                    if(i<AvailabelRIds.size() - 1)
                        recordTypeEquals =recordTypeEquals + ' OR ';
                }
                recordTypeEquals =recordTypeEquals + 'OR RecordTypeId = null';
                
                recordTypeEquals += ') ';
            }    
            system.debug(recordTypeEquals);
            if(recordTypeEquals != null)
            {
                whereConditions.add(recordTypeEquals);
            }
            else
            {
                recordTypeEquals = ' RecordTypeId = null';
                whereConditions.add(recordTypeEquals);
            }
            if(whereConditions.size() > 0 || whereClause.length()>0) {
                strSearch += ' WHERE ';
                if(whereClause.length()>0){
                    strSearch +=' ( '+whereClause+' ) '+' AND ';
                }
                for(String condition : whereConditions) {
                    strSearch += condition + ' AND ';   
                }
                // delete last 'AND'
                strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
            }
            strSearch  = strSearch + ' LIMIT 10';
            List<Account> lstAcct = Database.query(strSearch);       
            //String strCompanyWildcard = '%' + SourceLead.Company + '%';
            //List<Account> lstAcct = [SELECT Id, Name, Owner.Name FROM Account WHERE Name LIKE :strCompanyWildcard limit 10];
            
            
            // Add New Account if not found
            lstCompanies.add(new SelectOption('',System.Label.CLSEP12MKT07 + opptyCompanyName));
            
            // Add found Accounts to SelectList
            for(Account a : lstAcct) {
                lstCompanies.add(new SelectOption(a.Id, System.Label.CLSEP12MKT08 + a.Name + ' (' + a.Owner.Name + ')'));
            }
        }
        return lstCompanies;
    }
    
    //Method creates a select list of Contacts  for user selection using fuzzy search using the Lead name
    public List<SelectOption> listOfContactsInfo(){
        List<SelectOption> lstConsinfo = new List<SelectOption>();
        if(hasContact){
            lstConsinfo.add(new SelectOption(strContactId,ContactName));
        }
        else{
            String strFnameWildcard = '%' + SourceLead.firstname+ '%';
            String strLnameWildcard = '%' + SourceLead.Lastname+ '%';
            List<Contact> lstCons = [SELECT Id, Name, Owner.Name,AccountId FROM Contact WHERE Accountid =:strAccountId and (FirstName LIKE :strFnameWildcard OR LastName LIKE :strLnameWildcard) limit 10];
            
            
            // Add New Account if not found
            lstConsinfo.add(new SelectOption('',System.Label.CLSEP12MKT09  + SourceLead.FirstName+' '+ SourceLead.LastName));
            
            // Add found Accounts to SelectList
            for(Contact a : lstCons ) {
                lstConsinfo .add(new SelectOption(a.Id, System.Label.CLSEP12MKT08 + a.Name));
            }
        }
        return lstConsinfo ;
    }
    public PageReference ignoreAndConvert(){
        DML.DuplicateRuleHeader.AllowSave = true;
        return convertLead();
    }
    
    public class AccConWrap{
        public Boolean Selected{ get;set; }
        public Account Acc { get;set; }
        public Contact Con { get;set; }
        public AccConWrap( Boolean Selected, Account Acc, Contact Con){
            this.Selected = Selected;
            this.Acc = Acc;
            this.Con = Con;
        }
    }
    
    
    
}//End of Class