/* 
    Author: Global Delivery (Sch)
    Created date: 19/03/2012
    Description: Test class for AP43_CaseClassificationValidation
     
*/   

@istest
private class AP43_CaseClassificationValidation_TEST
{
    private static testMethod void testCaseClassificationValidation() {         
        
        CaseClassification__c caseClassification1 = new CaseClassification__c(Category__c='1 - Pre-Sales Support');
        insert caseClassification1 ;   
        
        CaseClassification__c caseClassification2 = new CaseClassification__c(Category__c='1 - Pre-Sales Support',
        reason__c = 'Order Booking',
        Subreason__c ='Catalog Selection');
        insert caseClassification2 ;   
        
        CaseClassification__c caseClassification3 = new CaseClassification__c(Category__c='1 - Pre-Sales Support',
        reason__c = 'Order Booking');
        insert caseClassification3 ; 
        
        CaseClassification__c caseClassification4 = new CaseClassification__c();
        insert  caseClassification4;
        
        AP43_CaseClassificationValidation.Validate(caseClassification1);
        AP43_CaseClassificationValidation.Validate(caseClassification2);
        AP43_CaseClassificationValidation.Validate(caseClassification3);
        AP43_CaseClassificationValidation.Validate(caseClassification4);
        AP43_CaseClassificationValidation.validateCagegory(caseClassification1);
        AP43_CaseClassificationValidation.validateReason(caseClassification1);
        AP43_CaseClassificationValidation.validateSubReason(caseClassification1);
               
       
    }
}