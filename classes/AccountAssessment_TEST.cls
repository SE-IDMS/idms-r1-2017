// This class has been modified to verify local branch checkout strategy
// Revert the change
@isTest
private class AccountAssessment_TEST{
        static testMethod void myUnitTest(){
        
            Map<String,ID> profiles = new Map<String,ID>();
            List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
            for(Profile p : ps)
            {
                profiles.put(p.name, p.id);
            }       
            user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];  
            system.runas(admin){    
                    Country__c country = Utils_TestMethods.createCountry();
                    country.countrycode__c ='ASM';
                    insert country;
                
                List<Profile> Adminprofiles = [select id from profile where name='System Administrator'];
                if(Adminprofiles.size()>0){
                    User usr = Utils_TestMethods.createStandardUser(Adminprofiles[0].Id,'tUsVFC92');
                    usr.BypassWF__c = false;
                    usr.ProgramAdministrator__c = true;     
                    usr.country__c = country.countrycode__c;
                
            
                System.runas(usr){
                
                test.startTest();
                PartnerPRogram__c gp1 = new PartnerPRogram__c();
                Account acc = new Account();
                ACC_PartnerProgram__c accprg = new ACC_PartnerProgram__c();
                
                
                
                //creates an account and sets the country code
                acc = Utils_TestMethods.createAccount();
                acc.country__c = country.id;
                insert acc;
                
                //makes the account a partner account
                acc.isPartner = true;
                acc.PRMAccount__c = true;
                update acc;
                
               
                //creates a global program
                gp1 = Utils_TestMethods.createPartnerProgram();
                gp1.TECH_CountriesId__c = country.id;
                gp1.recordtypeid = Label.CLMAY13PRM15;
                gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                insert gp1;
                
                //creates a program level for the global program
                ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                insert prg1;
                prg1.levelstatus__c = 'active';
                update prg1;
                
               
                
                //creates a country partner program
                PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
                insert cp1;
                cp1.ProgramStatus__c = 'active';
                update cp1;
                
                //creates a program level for the country partner program
                ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
                insert prg2;
                prg2.levelstatus__c = 'active';
                update prg2;

                /*
                Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];

                RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
                req.recordtypeid = recCatalogActivityReq.Id;
                insert req;

                RequirementCatalog__c req2 = Utils_TestMethods.createRequirementCatalog();
                req.recordtypeid = recCatalogActivityReq.Id;
                insert req2;
                */

                //creates an account assigned program
                /*Assessment__c newAssessment = Utils_TestMethods.createAssessment();
                newAssessment.PartnerProgram__C = cp1.Id;
                newAssessment.ProgramLevel__C = prg2.Id;
                newAssessment.AutomaticAssignment__c = true;
                insert newAssessment;*/
                
                
                Recordtype prgORFAssessmentRecordType = [Select id from RecordType where developername =  'PRMProgramApplication' limit 1];
            
                Assessment__c newAssessment = new Assessment__c();
                newAssessment = Utils_TestMethods.createAssessment();
                newAssessment.recordTypeId = prgORFAssessmentRecordType.Id ;
                newAssessment.AutomaticAssignment__c = true;
                newAssessment.Active__c = true;
                newAssessment.Name = 'Sample Assessment';
                newAssessment.PartnerProgram__c = gp1.id;
                newAssessment.ProgramLevel__c = prg1.id;
                insert newAssessment;
                
                OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(newAssessment.Id, 'picklist', 4);
                insert opassq;
                
                OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
                insert opassa;
                
                accprg = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
                insert accprg;
                accprg.active__c = true;
                update accprg;
                test.stopTest(); 
                
                SpecializationCatalog__c spct = new SpecializationCatalog__c();
                spct = Utils_TestMethods.createSpecializationCatalog();
                insert spct;
                
                Specialization__c sp = new Specialization__c();
                sp = Utils_TestMEthods.createSpecialization(spct.id);
                insert sp;
                sp.Status__c = 'active';
                update sp;
                
                Specialization__c sp1 = new Specialization__c();
                sp1 = Utils_TestMethods.createCountrySpecialization(sp.id, spct.id);
                insert sp1;
                sp1.Status__c = 'active';
                sp1.TECH_CountriesId__c = country.id;
                update sp1;

                /*
                SpecializationRequirement__c sr1 = new SpecializationRequirement__c();
                sr1.Active__c = false;
                sr1.RequirementCatalog__c = req.id;
                sr1.Specialization__c = sp.id;
                insert sr1;
                
                sr1.Active__c = true;
                update sr1;
                
                SpecializationRequirement__c sr2 = new SpecializationRequirement__c();
                sr2.Active__c = false;
                sr2.RequirementCatalog__c = req2.id;
                sr2.Specialization__c = sp.id;
                insert sr2;
                
                sr2.Active__c = true;
                update sr2;
                */
                
                Assessment__c asm = new Assessment__c();
                asm = Utils_TestMethods.createAssessment();
                asm.recordTypeId = prgORFAssessmentRecordType.Id ;
                
                asm.Active__c = true;
                asm.Name = 'Sample Assessment1';
                asm.AutomaticAssignment__c = true;
                asm.ProgramLevel__C = prg2.Id;
                asm.PartnerProgram__C=cp1.id;
                insert asm;
                asm.Specialization__c = sp1.id;
                update asm;
                
                OpportunityAssessmentQuestion__c opassq1 = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
                insert opassq1;
                
                OpportunityAssessmentAnswer__c opassa1 = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq1.id);
                insert opassa1;
                
                asm.AutomaticAssignment__c = false;
                update asm;
                asm.AutomaticAssignment__c = true;
                update asm;

                
                AccountSpecialization__c accsp = new AccountSpecialization__c();
                accsp = Utils_TestMethods.createAccountSpecialization(acc.id, sp1.id, spct.id);
                insert accsp;
                accsp.Status__c = 'active';
                update accsp;    
                accsp.Status__c = 'Expired';
                update accsp;   
                         

            }
                
            }
        }
    }

    static testMethod void myUnitTest2(){

            CS_RequirementRecordType__C cs1 = new CS_RequirementRecordType__c();
            cs1.name='012A0000000nphNIAQ'; cs1.ASPRRecordTypeID__c = '012A0000000nvnq';
            insert cs1;
        
            Map<String,ID> profiles = new Map<String,ID>();
            List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
            for(Profile p : ps)
            {
                profiles.put(p.name, p.id);
            }       
            user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];  
            system.runas(admin){    
                    Country__c country = Utils_TestMethods.createCountry();
                    country.countrycode__c ='ASM';
                    insert country;
                
                List<Profile> Adminprofiles = [select id from profile where name='System Administrator'];
                if(Adminprofiles.size()>0){
                    User usr = Utils_TestMethods.createStandardUser(Adminprofiles[0].Id,'tUsVFC92');
                    usr.BypassWF__c = false;
                    usr.ProgramAdministrator__c = true;     
                    usr.country__c = country.countrycode__c;
                
                System.runas(usr){
                    
                    PartnerPRogram__c gp1 = new PartnerPRogram__c();
                    Account acc = new Account();
                    ACC_PartnerProgram__c accprg = new ACC_PartnerProgram__c();
                    
                    
                    
                    //creates an account and sets the country code
                    acc = Utils_TestMethods.createAccount();
                    acc.country__c = country.id;
                    insert acc;
                    
                    //makes the account a partner account
                    acc.isPartner = true;
                    acc.PRMAccount__c = true;
                    update acc;
                    
                   test.startTest();
                    //creates a global program
                    gp1 = Utils_TestMethods.createPartnerProgram();
                    gp1.TECH_CountriesId__c = country.id;
                    gp1.recordtypeid = Label.CLMAY13PRM15;
                    gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                    insert gp1;
                    
                    //creates a program level for the global program
                    ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                    insert prg1;
                    prg1.levelstatus__c = 'active';
                    update prg1;
                    
                   
                    
                    //creates a country partner program
                    PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
                    insert cp1;
                    cp1.ProgramStatus__c = 'active';
                    update cp1;
                    
                    //creates a program level for the country partner program
                    ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
                    insert prg2;
                    prg2.levelstatus__c = 'active';
                    update prg2;

                    Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];

                    RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
                    req.recordtypeid = recCatalogActivityReq.Id;
                    insert req;

                    
                    
                    RequirementCatalog__c req2 = Utils_TestMethods.createRequirementCatalog();
                    req.recordtypeid = recCatalogActivityReq.Id;
                    insert req2;
                    
                    accprg = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
                    insert accprg;
                    accprg.active__c = true;
                    update accprg;
                    
                    SpecializationCatalog__c spct = new SpecializationCatalog__c();
                    spct = Utils_TestMethods.createSpecializationCatalog();
                    insert spct;
                    
                    Specialization__c sp = new Specialization__c();
                    sp = Utils_TestMEthods.createSpecialization(spct.id);
                    insert sp;
                    sp.Status__c = 'active';
                    update sp;
                    
                    Specialization__c sp1 = new Specialization__c();
                    sp1 = Utils_TestMethods.createCountrySpecialization(sp.id, spct.id);
                    insert sp1;
                    sp1.Status__c = 'active';
                    sp1.TECH_CountriesId__c = country.id;
                    update sp1;

                    SpecializationRequirement__c sr1 = new SpecializationRequirement__c();
                    sr1.Active__c = false;
                    sr1.RequirementCatalog__c = req.id;
                    sr1.Specialization__c = sp.id;
                    insert sr1;
                    
                    sr1.Active__c = true;
                    update sr1;
                    
                    SpecializationRequirement__c sr2 = new SpecializationRequirement__c();
                    sr2.Active__c = false;
                    sr2.RequirementCatalog__c = req2.id;
                    sr2.Specialization__c = sp1.id;
                    insert sr2;
                    
                    sr2.Active__c = true;
                    update sr2;

                    /*Assessment__c asm = new Assessment__c();
                    asm = Utils_TestMethods.createAssessment();
                    asm.AutomaticAssignment__c = true;
                    asm
                    asm.ProgramLevel__C = prg2.Id;
                    asm.PartnerProgram__C=cp1.id;
                    insert asm;
                    asm.Specialization__c = sp1.id;
                    update asm;*/
                    test.stopTest();
                    Recordtype prgORFAssessmentRecordType = [Select id from RecordType where developername =  'PRMProgramApplication' limit 1];

                    Assessment__c asm = new Assessment__c();
                    asm = Utils_TestMethods.createAssessment();
                    asm.recordTypeId = prgORFAssessmentRecordType.Id ;
                    asm.Active__c = true;
                    asm.Name = 'Sample Assessment2';
                    asm.AutomaticAssignment__c = true;
                    asm.ProgramLevel__C = prg2.Id;
                    asm.PartnerProgram__C=cp1.id;
                    insert asm;
                    asm.Specialization__c = sp1.id;
                    update asm;
                    
                    OpportunityAssessmentQuestion__c opassq1 = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
                    insert opassq1;
                    
                    OpportunityAssessmentAnswer__c opassa1 = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq1.id);
                    insert opassa1;
                    
                    asm.AutomaticAssignment__c = false;
                    update asm;
                    asm.AutomaticAssignment__c = true;
                    update asm;
                    
                    AccountSpecialization__c accsp = new AccountSpecialization__c();
                    accsp = Utils_TestMethods.createAccountSpecialization(acc.id, sp1.id, spct.id);
                    insert accsp;
                    accsp.Status__c = 'active';
                    update accsp; 
                    accsp.Status__c = 'Expired';
                    update accsp;
                    AP_AccountSpecialization.reCreateNewChildAccountSpecializationRequirements(new AccountSpecialization__c[]{accsp});               

                }
                
            }
        }
    }


 }