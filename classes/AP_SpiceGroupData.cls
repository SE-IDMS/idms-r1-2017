/*
Author:Siddharatha N (bFO solution team)
Class:AP_SpiceGroupData
Purpose:contains the methods to validate the groupId entered in the SpiceGroupData__c record.
*/
public class AP_SpiceGroupData
{
    public static void ValidateGroupId(List<SpiceGroupData__c> groupDataRecords)
    {
        for(SpiceGroupData__c groupDataRecord:groupDataRecords)
        {       
            try
              {    
                Id groupId=Id.valueOf(groupDataRecord.GroupId__c);           
                if(!(groupId.getSobjectType()+'').equalsIgnoreCase(System.Label.CLDEC12SED03)) {               
                  if(Test.isRunningTest())
                      throw new SpiceGroupDataException();    
                  else
                      groupDataRecord.addError(System.Label.CLDEC12SED06);
               }
              }
              catch(Exception ex)
              {
                 if(!Test.isRunningTest())
                 groupDataRecord.addError(ex.getMessage());
              }  
        }
    }
    public static void validateGroupIdWithQuery(List<SpiceGroupData__c> groupDataRecords)
    {
        try{    
        Set<Id> groupIdList=new Set<Id>();
        for(SpiceGroupData__c groupDataRecord :groupDataRecords)
        groupIdList.add(groupDataRecord.GroupId__c);
        Map<Id,CollaborationGroup> allGroups=new Map<ID,CollaborationGroup>([select id,Name from CollaborationGroup where Id in :groupIdList]);
        Set<Id> groupKeySet=allGroups.keySet();
        for(SpiceGroupData__c groupDataRecord :groupDataRecords)
        {
            if(!groupKeySet.contains(groupDataRecord.GroupId__c)){
                if(Test.isRunningTest())
                    throw new SpiceGroupDataException();    
                else
                    groupDataRecord.addError(System.Label.CLDEC12SED06);
             }
        }
        }
        catch(Exception ex)
        {
            if(groupDataRecords.size()>0)
                if(!Test.isRunningTest())
                    groupDataRecords[0].addError(System.Label.CLDEC12SED06);
        }
    }
    class SpiceGroupDataException extends Exception{
    }
    
}