@isTest
public class PRM_FeatureCustomPermissionSetTest{
    static testmethod void Test_FeatureTest() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            CS_PRM_ApexJobSettings__c PRMSettings = new CS_PRM_ApexJobSettings__c(
                Name = 'FieloPRM_Batch_MemberFeatProcess',
                InitialSync__c = true
                );
            insert PRMSettings;

            Id manualRecordType = [Select id,DeveloperName,Name,SobjectType FRom RecordType WHERE SobjectType = 'FieloPRM_MemberFeature__c' and developerNAme = 'Manual'].Id;
                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'AKRAM UNIT TEST ';
            member.FieloEE__FirstName__c = 'AKRAM UNIT TEST ';
            member.FieloEE__Street__c = 'AKRAM STREET';
            member.F_Account__c = acc.id;
                 
            insert member;
            //Insert a community User
            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123TEST',
                Email = 'test@test.com',PRMEmail__c='test1@test.com');
            insert c1;

            String str =  string.valueof(System.now());
            str = str.remove('-');
            str = str.remove(' ');
            str = str.remove(':');
            User userCom = new User(FirstName = c1.FirstName,contactId = c1.Id, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = c1.PRMEmail__c, Username = str+c1.PRMEmail__c +'.bfo.com',Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c(name = 'feature test',F_PRM_FeatureCode__c= 'feature api name',F_PRM_CustomLogicClass__c = 'PRM_RegularORFFeature');
            listFeatureinsert.add(feature);

            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c(name = 'feature test1',F_PRM_FeatureCode__c = 'feature api name2',F_PRM_CustomLogicClass__c = 'PRM_ProjectRegistrationFeature');
            listFeatureinsert.add(feature2);

            FieloPRM_Feature__c feature3 = new FieloPRM_Feature__c(name = 'feature test2',F_PRM_FeatureCode__c = 'feature api name3',F_PRM_CustomLogicClass__c = 'PRM_PassedToPartnerOpportunitiesFeature');
            listFeatureinsert.add(feature3);

            FieloPRM_Feature__c feature4 = new FieloPRM_Feature__c(name = 'feature test3',F_PRM_FeatureCode__c = 'feature api name4',F_PRM_CustomLogicClass__c = 'PRM_LeadManagementFeature');
            listFeatureinsert.add(feature4);            


            FieloPRM_Feature__c feature5 = new FieloPRM_Feature__c(name = 'feature test3',F_PRM_FeatureCode__c = 'feature api name5',F_PRM_CustomLogicClass__c = 'PRM_ServiceOpportunityFeature');
            listFeatureinsert.add(feature5);  

            FieloPRM_Feature__c feature6 = new FieloPRM_Feature__c(name = 'feature test3',F_PRM_FeatureCode__c = 'feature api name6',F_PRM_CustomLogicClass__c = 'PRM_CassiniCommunityAccessFeature');
            listFeatureinsert.add(feature6);
                    
            
            //added by cmok / collaboration
            /*
             FieloPRM_Feature__c feature6 = new FieloPRM_Feature__c();
            feature6.name = 'feature test6';
            feature6.F_PRM_FeatureCode__c = 'feature api name6';
            feature6.F_PRM_CustomLogicClass__c = 'PRM_collaborationFeature';
            listFeatureinsert.add(feature6);      */

            insert listFeatureinsert;
            
            Test.startTest(); 

                list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
               
                FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c(F_PRM_Feature__c =  feature.id,F_PRM_Member__c =  member.id,F_PRM_IsActive__c  =  true,PRMUIMSId__c = '123TEST',RecordTypeId=manualRecordType);
                listMemberFeature.add(memberFeature);

                FieloPRM_MemberFeature__c memberfeature1 = new FieloPRM_MemberFeature__c(F_PRM_Feature__c =  feature2.id,F_PRM_Member__c =  member.id,F_PRM_IsActive__c  =  true,PRMUIMSId__c = '123TEST',RecordTypeId=manualRecordType);
                listMemberFeature.add(memberfeature1);

                FieloPRM_MemberFeature__c memberfeature3 = new FieloPRM_MemberFeature__c();
                memberfeature3.F_PRM_Feature__c =  feature3.id;
                memberfeature3.F_PRM_Member__c =  member.id;
                memberFeature3.F_PRM_IsActive__c  =  true;
                memberFeature3.PRMUIMSId__c = '123TEST';
                memberFeature3.RecordTypeId=manualRecordType;
                listMemberFeature.add(memberfeature3);
                
                FieloPRM_MemberFeature__c memberfeature4 = new FieloPRM_MemberFeature__c();
                memberfeature4.F_PRM_Feature__c =  feature4.id;
                memberfeature4.F_PRM_Member__c =  member.id;
                memberFeature4.F_PRM_IsActive__c  =  true;
                memberFeature4.PRMUIMSId__c = '123TEST';
                memberFeature4.RecordTypeId=manualRecordType;
                listMemberFeature.add(memberfeature4);

                FieloPRM_MemberFeature__c memberfeature5 = new FieloPRM_MemberFeature__c();
                memberfeature5.F_PRM_Feature__c =  feature5.id;
                memberfeature5.F_PRM_Member__c =  member.id;
                memberfeature5.F_PRM_IsActive__c  =  true;
                memberfeature5.PRMUIMSId__c = '123TEST';
                memberFeature5.RecordTypeId=manualRecordType;
                listMemberFeature.add(memberfeature5);

                FieloPRM_MemberFeature__c memberfeature6 = new FieloPRM_MemberFeature__c();
                memberfeature6.F_PRM_Feature__c =  feature6.id;
                memberfeature6.F_PRM_Member__c =  member.id;
                memberfeature6.F_PRM_IsActive__c  =  true;
                memberfeature6.PRMUIMSId__c = '123TEST';
                memberFeature6.RecordTypeId=manualRecordType;
                listMemberFeature.add(memberfeature6);
				
                /*
				FieloPRM_MemberFeature__c memberfeature6 = new FieloPRM_MemberFeature__c();
                memberfeature6.F_PRM_Feature__c =  feature6.id;
                memberfeature6.F_PRM_Member__c =  member.id;
                memberfeature6.F_PRM_IsActive__c  =  true;
                memberfeature6.PRMUIMSId__c = '123TEST';
                memberFeature6.RecordTypeId=manualRecordType;
                listMemberFeature.add(memberfeature6);*/

                insert listMemberFeature;
                FieloPRM_Schedule_MemberFeature.scheduleNow(true,listMemberFeature);
            Test.stopTest();

            List<PermissionSetAssignment> psetAssignList = [select Id From PermissionSetAssignment where AssigneeId =:userCom.Id and  (PermissionSet.Name ='PRMRegularORFCommunity' or PermissionSet.Name ='PRMProjectRegistrationCommunity' or PermissionSet.Name ='PRMPassedToPartnerOpportunitiesCommunity' or PermissionSet.Name ='PRMLeadManagementCommunity' or PermissionSet.Name ='PRMServiceOpportunityCommunity' or PermissionSet.Name ='PRM_Collaboration')];
            //System.assertEquals(6, psetAssignList.size());

            
        }
    }
    
    static testmethod void Test_FeatureTestUpdate() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            CS_PRM_ApexJobSettings__c PRMSettings = new CS_PRM_ApexJobSettings__c(
                Name = 'FieloPRM_Batch_MemberFeatProcess',
                InitialSync__c = true
                );
            insert PRMSettings;    
                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'AKRAM UNIT TEST ';
            member.FieloEE__FirstName__c = 'AKRAM UNIT TEST ';
            member.FieloEE__Street__c = 'AKRAM STREET';
            member.F_Account__c = acc.id;
                 
            insert member;
            //Insert a community User
            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123TEST2',
                Email = 'test@test.com',PRMEmail__c='test1@test.com');
            insert c1;

            String str =  string.valueof(System.now());
            str = str.remove('-');
            str = str.remove(' ');
            str = str.remove(':');
            User userCom = new User(FirstName = c1.FirstName, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = str+c1.PRMEmail__c, Username = c1.PRMEmail__c + '002.bfo.com',Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, ContactId = c1.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c= 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'PRM_RegularORFFeature';
            listFeatureinsert.add(feature);

            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c();
            feature2.name = 'feature test1';
            feature2.F_PRM_FeatureCode__c = 'feature api name2';
            feature2.F_PRM_CustomLogicClass__c = 'PRM_ProjectRegistrationFeature';
            listFeatureinsert.add(feature2);

            FieloPRM_Feature__c feature3 = new FieloPRM_Feature__c();
            feature3.name = 'feature test2';
            feature3.F_PRM_FeatureCode__c = 'feature api name3';
            feature3.F_PRM_CustomLogicClass__c = 'PRM_PassedToPartnerOpportunitiesFeature';
            listFeatureinsert.add(feature3);

            FieloPRM_Feature__c feature4 = new FieloPRM_Feature__c();
            feature4.name = 'feature test3';
            feature4.F_PRM_FeatureCode__c = 'feature api name4';
            feature4.F_PRM_CustomLogicClass__c = 'PRM_LeadManagementFeature';
            listFeatureinsert.add(feature4);            

            FieloPRM_Feature__c feature5 = new FieloPRM_Feature__c();
            feature5.name = 'feature test5';
            feature5.F_PRM_FeatureCode__c = 'feature api name5';
            feature5.F_PRM_CustomLogicClass__c = 'PRM_ServiceOpportunityFeature';
            listFeatureinsert.add(feature5); 

            
            FieloPRM_Feature__c feature6 = new FieloPRM_Feature__c();
            feature6.name = 'feature test6';
            feature6.F_PRM_FeatureCode__c = 'feature api name6';
            feature6.F_PRM_CustomLogicClass__c = 'PRM_CassiniCommunityAccessFeature';
            listFeatureinsert.add(feature6);          
            
           
            insert listFeatureinsert;
            
            Test.startTest(); 
                Id manualRecordType = [Select id,DeveloperName,Name,SobjectType FRom RecordType WHERE SobjectType = 'FieloPRM_MemberFeature__c' and developerNAme = 'Manual'].Id;
            
                list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
               
                FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
                memberFeature.F_PRM_Feature__c =  feature.id;
                memberFeature.F_PRM_Member__c =  member.id;
                memberFeature.F_PRM_IsActive__c  =  true;
                memberFeature.PRMUIMSId__c = '123TEST2';
                memberFeature.RecordTypeId=manualRecordType;
                memberFeature.F_PRM_IsSynchronic__c = true;
                listMemberFeature.add(memberFeature);

                FieloPRM_MemberFeature__c memberfeature1 = new FieloPRM_MemberFeature__c();
                memberfeature1.F_PRM_Feature__c =  feature2.id;
                memberfeature1.F_PRM_Member__c =  member.id;
                memberFeature1.F_PRM_IsActive__c  =  true;
                memberFeature1.PRMUIMSId__c = '123TEST2';
                memberFeature1.RecordTypeId=manualRecordType;
                memberFeature.F_PRM_IsSynchronic__c = true;
                listMemberFeature.add(memberfeature1);

                FieloPRM_MemberFeature__c memberfeature3 = new FieloPRM_MemberFeature__c();
                memberfeature3.F_PRM_Feature__c =  feature3.id;
                memberfeature3.F_PRM_Member__c =  member.id;
                memberFeature3.F_PRM_IsActive__c  =  true;
                memberFeature3.PRMUIMSId__c = '123TEST2';
                memberFeature3.RecordTypeId=manualRecordType;
                memberFeature3.F_PRM_IsSynchronic__c = true;
                listMemberFeature.add(memberfeature3);
                
                FieloPRM_MemberFeature__c memberfeature4 = new FieloPRM_MemberFeature__c();
                memberfeature4.F_PRM_Feature__c =  feature4.id;
                memberfeature4.F_PRM_Member__c =  member.id;
                memberFeature4.F_PRM_IsActive__c  =  true;
                memberFeature4.PRMUIMSId__c = '123TEST2';
                memberFeature4.RecordTypeId=manualRecordType;
                memberFeature4.F_PRM_IsSynchronic__c = true;
                listMemberFeature.add(memberfeature4);

                FieloPRM_MemberFeature__c memberfeature5 = new FieloPRM_MemberFeature__c();
                memberfeature5.F_PRM_Feature__c =  feature5.id;
                memberfeature5.F_PRM_Member__c =  member.id;
                memberfeature5.F_PRM_IsActive__c  =  true;
                memberfeature5.PRMUIMSId__c = '123TEST2';
                memberFeature5.RecordTypeId=manualRecordType;
                memberFeature5.F_PRM_IsSynchronic__c = true;
                listMemberFeature.add(memberfeature5);
				
                
				FieloPRM_MemberFeature__c memberfeature6 = new FieloPRM_MemberFeature__c();
                memberfeature6.F_PRM_Feature__c =  feature6.id;
                memberfeature6.F_PRM_Member__c =  member.id;
                memberfeature6.F_PRM_IsActive__c  =  true;
                memberfeature6.PRMUIMSId__c = '123TEST2';
                memberFeature6.RecordTypeId=manualRecordType;
                memberFeature6.F_PRM_IsSynchronic__c = true;
                listMemberFeature.add(memberfeature6);

                insert listMemberFeature;

                listMemberFeature[0].F_PRM_IsActive__c  = false;
                listMemberFeature[1].F_PRM_IsActive__c  = false;
                update listMemberFeature;

                delete listMemberFeature[2];
                //delete listMemberFeature[5];
                FieloPRM_Schedule_MemberFeature.scheduleNow(false,listMemberFeature);
            Test.stopTest();

            List<PermissionSetAssignment> psetAssignList = [select Id From PermissionSetAssignment where AssigneeId =:userCom.Id and  (PermissionSet.Name ='PRMRegularORFCommunity' or PermissionSet.Name ='PRMProjectRegistrationCommunity' or PermissionSet.Name ='PRMPassedToPartnerOpportunitiesCommunity' or PermissionSet.Name ='PRMLeadManagementCommunity' or PermissionSet.Name ='PRMServiceOpportunityCommunity' or PermissionSet.Name ='PRM_Collaboration')];
            
            //System.assertEquals(3, psetAssignList.size());

            
        }
    }
}