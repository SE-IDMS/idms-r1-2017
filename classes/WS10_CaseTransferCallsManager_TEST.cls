/*
Author:Siddharatha Nagavarapu
Description:This test class is for the webservice class WS10_CaseTransferCallsManager
Created Date: 02-02-2012
*/

@isTest
private class WS10_CaseTransferCallsManager_TEST{

    static testMethod void testupdateCaseOwner() {
    //blank case id
    WS10_CaseTransferCallsManager.updateCaseOwner('');
    //invalid case id
    WS10_CaseTransferCallsManager.updateCaseOwner('sampletext');
    //create user
        User testUser=Utils_TestMethods.createStandardUser('test');        
        insert testUser;
        
    //create account    
        Account testAccount=Utils_TestMethods.createAccount(testUser.id);
        insert testAccount;
    //create contact
        Contact testContact=Utils_TestMethods.createContact(testAccount.id,'test Contact');
        insert testContact;
    //create case
        Case testCase=Utils_TestMethods.createCase(testAccount.id,testContact.id,'Open');
        insert testCase;
    //actual unit test
    WS10_CaseTransferCallsManager.additionalCoverage=false;
    WS10_CaseTransferCallsManager.updateCaseOwner(testCase.id+'');
    WS10_CaseTransferCallsManager.additionalCoverage=true;
    WS10_CaseTransferCallsManager.updateCaseOwner(testCase.id+'');
    WS10_CaseTransferCallsManager.groupName='invalid queue name';
    WS10_CaseTransferCallsManager.updateCaseOwner(testCase.id+'');

    
        
    }
}