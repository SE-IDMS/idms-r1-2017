public class PRJ_QuotedGateReportLink
{
string UserId = '';
String url = '';
string businessTechDomains='';

public pageReference assignValuesToReport()
    {    
        UserId= UserInfo.getUserId();
        System.debug('userId : ' +UserId);
        string CIO = System.Label.DMTCIO;
        string CFO = System.Label.DMTCFO;
        Set<DMTAuthorizationMasterData__c> authUser = new Set<DMTAuthorizationMasterData__c> ([SELECT BusinessTechnicalDomain__c from DMTAuthorizationMasterData__c WHERE NextStep__c = 'Quoted' AND AuthorizedUser6__c = :UserId  ]);
        System.debug('>>>>>>>>>>>>>>>>>>' + authUser);
       
        if(UserId.contains(CIO))
                {
                    system.debug('>>>>>>>>>>>>>CIO:' + UserId);
                    url = System.Label.DMTQuotedGateReportLink1; 
                    PageReference pageRef = new PageReference(url);
                    pageRef.setRedirect(true);
                    return pageRef; 
                }
                
       if(UserId.contains(CFO))
                {
                    system.debug('>>>>>>>>>>>>>CIO:' + UserId);
                    url = System.Label.DMTQuotedGateReportLink3; 
                    PageReference pageRef = new PageReference(url);
                    pageRef.setRedirect(true);
                    return pageRef; 
                }
            
        
        
        else if(authUser.size() > 0)
        {
            string param = System.label.DMTReportParam;
            url = System.Label.DMTQuotedGateReportLink;
            for(DMTAuthorizationMasterData__c d :authUser)
            {
                  String b = d.BusinessTechnicalDomain__c;
                  if(businessTechDomains == null)
                      businessTechDomains = b + ',';
                  if(!businessTechDomains.contains(b)&& businessTechDomains != null)
                      businessTechDomains += b + ',';
            }  
            businessTechDomains = businessTechDomains.removeEnd(',');
            System.debug('>>>>>>>>>>>>>>>>>>' + businessTechDomains );
            PageReference pageRef = new PageReference(url);
            pageRef.getParameters().put(param,businessTechDomains);
            pageRef.setRedirect(true);
            return pageRef; 
        }
          
        else
        {
             
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'This Report is only for CIO and CIO[n-1]. Please use the REPORTS tab to access reports');
            ApexPages.addMessage(myMsg);
            return null;
        }    
            
    }
}