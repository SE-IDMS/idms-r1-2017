public class VFC_CopySalesTeam
{
    public Id oppId{get;set;}
    public Integer result{get;set;}
    public String message{get;set;}
    
    public VFC_CopySalesTeam(ApexPages.StandardController controller)
    {     
        oppId = ApexPages.currentPage().getParameters().get('oppid');  
        //system.debug('oppId = '+oppId );
    }
    
    public void copySalesTeam()
    {
       //system.debug('oppId = '+oppId ); 
        
        result = WS01_CopySalesTeamToSalesContributor.CopySalesTeamToSalesContributor(oppId);
        if(result == 1)
           message = Label.CL00260;
        else if(result == 0) 
            message = Label.CL00261; 
        else if(result == -1) 
            message = Label.CL00262;
        else if(result == -100) 
            message = Label.CL00277;
            
        system.debug('the result is : ' + result);
        system.debug('the message is : ' + message);
          
     }   
}