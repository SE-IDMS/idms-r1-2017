@isTest(SeeAllData=true)
public class SVMX_IsSVMXRelatedAccountTest
{
    static testMethod void AccTest()
    {
        Set<Id> AccId = new Set<Id>();
        //Account Acc = [Select Id from Account limit 1];
        Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj; 
        AccId.add(accountSobj.Id);
        SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.SVMXC__Company__c = accountSobj.Id;
        insert sc;
    }
}