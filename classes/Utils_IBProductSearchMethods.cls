// The strategy interface defines the three methods with different behavior according to the current context (Case, Work order ...)
public Interface Utils_IBProductSearchMethods
{
   Void Init(String ObjID, VCC_IBProductSearch Controller); // Initialization of the component
   Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase); // Action performed when commandlink is clicked in the result table
   PageReference Cancel(VCC_IBProductSearch Controller); // Cancel method
}