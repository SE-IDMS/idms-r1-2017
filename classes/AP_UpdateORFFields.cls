/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 27 July 2013
    Description  : 1. updateORFStatus()
                       (a) Update ORF status to 'Pending' once the first Assessment is linked to the ORF
        
***************************************************************************************************************************/
public class AP_UpdateORFFields
{
     public static void updateORFStatus(list<Id> lstORFID)
     {
         list<OpportunityRegistrationForm__c> lstORF = [Select id,Status__c,RecordTypeId from OpportunityRegistrationForm__c where
                                                         id in :lstORFID and (status__c =:Label.CLOCT13PRM16 or status__c =:Label.CLFEB14PRM07) limit 10000];
         for(OpportunityRegistrationForm__c orf : lstORF )
         {
            // orf.status__c = Label.CLOCT13PRM17;
                         
             if(orf.RecordTypeId != null && orf.RecordTypeId == system.label.CLFEB14PRM04)
             {
                if(orf.Status__c == Label.CLOCT13PRM16)
                    oRF.status__c = Label.CLFEB14PRM07;
                else
                    oRF.status__c = Label.CLFEB14PRM02;              
             }
             else
                orf.status__c = Label.CLOCT13PRM17;//pending
                
         }
         
         update lstORF;
     }   
}