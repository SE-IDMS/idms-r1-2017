/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 18/07/2012
    Description     : Test class for the class PROG_ManageProgramCostsForecasts
*/

@isTest
private class PROG_ManageProgramCostsForecasts_TEST
{
    static testMethod void myUnitTest(){
    
        // Creating the DMT Program Object test data
            DMTProgram__c dmtProg = new DMTProgram__c();
            dmtProg.BusinessConnectProgram__c = 'Global Customer Intimacy - Best in class key account management';
            dmtProg.Name = 'B2B eCommerce';
            dmtProg.BusinessOrgFamily__c = 'Business';
            dmtProg.Business_Organization__c = 'Buildings';
            dmtProg.SEOrganizationL1__c = 'India';
            dmtProg.SEOrganizationL2__c = '';
            insert dmtProg;
        
        //Creating Program Cost Forecast Object test data
           ProgramCostsForecasts__c progCostForecast = new ProgramCostsForecasts__c();
           progCostForecast.DMTProgramId__c = dmtProg.id;
           progCostForecast.Active__c = True;
           progCostForecast.LastActiveofBudgetYear__c = False;
           progCostForecast.Description__c = 'Test data';
           insert progCostForecast;
           
           DMTProgramCostForecastCostItemsFields__c pcfCs = new DMTProgramCostForecastCostItemsFields__c();
           pcfCs.Name ='Test 1';
           pcfCs.CostItemFieldLabel__c = 'IT Cashout CAPEXY0';
           pcfCs.CostItemFieldAPIName__c = 'FY0CashoutCAPEX__c';
           pcfCs.Type__c = 'IT Cashout CAPEX';
           pcfCs.TypeOrder__c = 'Type 1';
           pcfCs.BackgroundColour__c = 'background-color:MistyRose';
           insert pcfCs;
           DMTProgramCostForecastCostItemsFields__c pcfCs2 = new DMTProgramCostForecastCostItemsFields__c();
           pcfCs2.Name ='Test 2';
           pcfCs2.CostItemFieldLabel__c = 'IT Cashout CAPEXY1';
           pcfCs2.CostItemFieldAPIName__c = 'FY1CashoutCAPEX__c';
           pcfCs2.Type__c = 'IT Cashout CAPEX';
           pcfCs2.TypeOrder__c = 'Type 1';
           pcfCs2.BackgroundColour__c = 'background-color:MistyRose';
           insert pcfCs2;
                      
           PROG_ManageProgramCostsForecasts pagePCF = new PROG_ManageProgramCostsForecasts(new ApexPages.StandardController(progCostForecast));
                      
           pagePCF.updateFundingEntity();
           pagePCF.backToFundingEntity();
        
    }
}