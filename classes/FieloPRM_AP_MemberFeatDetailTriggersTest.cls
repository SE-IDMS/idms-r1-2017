/********************************************************************
* Company: Fielo
* Created Date: 11/08/2016
* Description: Test class for class FieloPRM_UTILS_Member
********************************************************************/
@isTest
public without sharing class FieloPRM_AP_MemberFeatDetailTriggersTest{
    
    @testSetup
    static void setupTest() {
    }
    
    @isTest
    static void unitTest1(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            Id levelRecordTypeId = Schema.SObjectType.FieloPRM_MemberFeatureDetail__c.getRecordTypeInfosByName().get('Level').getRecordTypeId();
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberFeatProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;
        
            test.starttest();
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;
            
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c = 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'FieloPRM_CustomLogicImplementTest';
            insert feature;
            
            FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
            memberFeature.F_PRM_Feature__c = feature.id;
            memberFeature.F_PRM_Member__c = member.id;
            memberFeature.F_PRM_IsActive__c = true;
            memberFeature.PRMUIMSId__c = '123TEST';
            insert memberFeature;   
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'Badge';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_Segment__c =  segment.id;
            badge.F_PRM_BadgeAPIName__c = 'apinametest';
            insert badge;
            
            FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c(
                F_PRM_MemberFeature__c = memberFeature.Id,
                F_PRM_Badge__c = badge.Id,
                recordTypeId = levelRecordTypeId,
                F_PRM_isActive__c = true
            );
            insert newMemberFeatureDetail;
            
            newMemberFeatureDetail.F_PRM_isActive__c = false;
            update newMemberFeatureDetail;
            
            newMemberFeatureDetail.F_PRM_isActive__c = true;
            update newMemberFeatureDetail;
            
            delete newMemberFeatureDetail;
            
            test.stoptest();
        }
    }
}