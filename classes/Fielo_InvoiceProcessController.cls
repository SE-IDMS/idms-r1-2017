/************************************************************
* Developer: Waldermar Mayo                                 *
* Created Date: 19/08/2014                                  *
************************************************************/
public with sharing class Fielo_InvoiceProcessController {
    
    private Fielo_Invoice__c inv {get;set;}
    public String confirmation {get;set;}
    public String result {get;set;}
    
    public Fielo_InvoiceProcessController(ApexPages.StandardController controller) {
        inv = (Fielo_Invoice__c)controller.getRecord();
        confirmation = 'test';
    }
    
    public void doConfirm(){
        confirmation = Process(inv.Id, true);
    }
    
    public void doProcess(){
        confirmation = null;
        result = Process(inv.Id, false);
                
        if(result == 'OK'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.Fielo_InvoiceProcessControllerSuccess));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.Fielo_InvoiceProcessControllerSuccess));
        }
                
    }
    
    public PageReference doGoToInv(){
        return new PageReference('/'+inv.Id);
    }
    
    private static String Process(String idInvoice, Boolean isConfirm){
        system.debug('Process');
        system.debug('heap limit: ' + Limits.getHeapSize());
        Fielo_Invoice__c inv;
        String retMessage = 'OK';
        try{inv = [SELECT id, F_Member__c, F_Member__r.FieloEE__ReferalMember__c, F_Transaction__c, F_Status__c, F_Transaction__r.FieloEE__Points__c, F_TotalPoints__c, (SELECT Id FROM Invoice_Details_Fielo__r LIMIT 1) FROM Fielo_Invoice__c WHERE id =: idInvoice LIMIT 1];}catch(Exception e){}
        
        if(inv == null){
            return Label.Fielo_InvoiceProcessButtonInvoiceNFound;
        }
        if(inv.F_Status__c == 'Processed'){
            return Label.Fielo_InvoiceProcessButtonProcessed;
        }
        if(inv.Invoice_Details_Fielo__r.IsEmpty()){
            return Label.Fielo_InvoiceProcessButtonAtLeastOne;
        }
        
        //Validaciones de la invoice
        if(isConfirm){
            if(inv.F_Member__c == null || inv.F_Transaction__c == null || inv.F_Status__c == 'Rejected'){
                return Label.Fielo_InvoiceProcessButtonCannotProcess;
            }else{
                return retMessage;
            }
        }
        //07/07/2015
        /*map<Id, Fielo_InvoiceDetails__c> mapInvoiceDetail = new map<Id, Fielo_InvoiceDetails__c>([SELECT Id, F_Invoice__c, F_Points__c, F_Price__c, F_Processed__c,
                                                                        F_Product__c, F_SkipEngine__c, F_Volume__c
                                                                        FROM Fielo_InvoiceDetails__c
                                                                        WHERE F_Invoice__c =: idInvoice AND F_SkipEngine__c = false]);*/

                system.debug('heap limit: ' + Limits.getHeapSize());
        map<Id, Fielo_InvoiceDetails__c> mapInvoiceDetail = new map<Id, Fielo_InvoiceDetails__c>([SELECT Id, F_Invoice__c, F_Points__c, F_Price__c, F_Processed__c,
                                                                        F_Product__c, F_SkipEngine__c, F_Volume__c, F_PointsInstaller__c, F_PointsRetailer__c
                                                                        FROM Fielo_InvoiceDetails__c
                                                                        WHERE F_Invoice__c =: idInvoice AND F_SkipEngine__c = false]);
        system.debug('heap limit: ' + Limits.getHeapSize());                                                                        
        if(!mapInvoiceDetail.IsEmpty()){
        
            List<FieloDRE__ObjectsToProcess__c> listObjectsToProcess = [SELECT Id, FieloDRE__FieldMember__c, FieloDRE__FieldToRuleAppliedMasterLookup__c, FieloDRE__FieldTotalPoints__c, FieloDRE__APINameObject__c, FieloDRE__RuleAppliedMasterLookup__c, FieloDRE__RuleRecordTypeDevName__c,FieloDRE__FieldProcessed__c FROM FieloDRE__ObjectsToProcess__c WHERE FieloDRE__RuleRecordTypeDevName__c = 'Fielo_InvoiceDetailInstaller' OR FieloDRE__RuleRecordTypeDevName__c = 'Fielo_InvoiceDetailRetailer' LIMIT 2];
                    system.debug('heap limit: ' + Limits.getHeapSize()); 
            if(!listObjectsToProcess.isEmpty()){
                for(FieloDRE__ObjectsToProcess__c objectToProcess: listObjectsToProcess){
                    //Proceso los invoice detail en el Rule Engine
                            system.debug('heap limit: ' + Limits.getHeapSize()); 
                    FieloDRE.DynamicRuleEngine.processRecords(mapInvoiceDetail,objectToProcess);                
                            system.debug('heap limit: ' + Limits.getHeapSize()); 
                }
            }
            
            //07/07/2015
            //Proceso los invoice detail en el Rule Engine
            //FieloDRE.DynamicRuleEngine.processRecords(mapInvoiceDetail);
                    system.debug('heap limit: ' + Limits.getHeapSize()); 
            //Actualizo los Invoice detail a "Processed"
            List<Fielo_InvoiceDetails__c> listInvoiceDetailToUpdate = new List<Fielo_InvoiceDetails__c>();
            for(Fielo_InvoiceDetails__c ide: mapInvoiceDetail.values()){
                listInvoiceDetailToUpdate.add(new Fielo_InvoiceDetails__c(
                    Id = ide.Id,
                    F_Processed__c = true
                ));
            }
            
            //07/07/2015
            //inv = [SELECT id, F_Member__c, F_Transaction__c, F_Status__c, F_Transaction__r.FieloEE__Points__c, F_Member__r.FieloEE__ReferalMember__c, F_TotalPoints__c, F_Retailer__c, F_TotalAmount__c, (SELECT Id FROM Invoice_Details_Fielo__r) FROM Fielo_Invoice__c WHERE id =: idInvoice LIMIT 1];
            inv = [SELECT id, F_TotalPointsInstaller__c, F_TotalPointsRetailer__c, F_Member__c, F_Transaction__c, F_Status__c, F_Transaction__r.FieloEE__Points__c, F_Member__r.FieloEE__ReferalMember__c, F_TotalPoints__c, F_Retailer__c, F_TotalAmount__c/*, (SELECT Id FROM Invoice_Details_Fielo__r)*/ FROM Fielo_Invoice__c WHERE id =: idInvoice LIMIT 1];
            
            //Actualizo las Invoice  a "Processed"
            inv.F_Status__c = 'Processed';
            
            FieloEE__Transaction__c tran = [SELECT Id, Name, FieloEE__Value__c, FieloEE__Member__c, F_Invoice__c, FieloEE__Type__c, FieloEE__Approved__c, F_ExternalValue__c FROM FieloEE__Transaction__c WHERE F_Invoice__c =: inv.id Limit 1];
                    system.debug('heap limit: ' + Limits.getHeapSize());
            //07/07/2015
            //tran.F_ExternalValue__c = inv.F_TotalPoints__c;
            tran.F_ExternalValue__c = inv.F_TotalPointsInstaller__c;
            
            
            //Referente
            List<FieloEE__Transaction__c> listMemFromTrans = [SELECT Id FROM FieloEE__Transaction__c WHERE FieloEE__Member__c =: inv.F_Member__c AND FieloEE__Type__c = 'Invoice' AND FieloEE__Approved__c = true LIMIT 1];
                    system.debug('heap limit: ' + Limits.getHeapSize()); 
            SavePoint sp = Database.setSavepoint();
                    system.debug('heap limit: ' + Limits.getHeapSize()); 
            List<FieloEE__Transaction__c> listNewTrans = new List<FieloEE__Transaction__c>();
                    system.debug('heap limit: ' + Limits.getHeapSize());
            try{
                //Retailer Referral
                if((listMemFromTrans.isEmpty()) && inv.F_Member__r.FieloEE__ReferalMember__c != null){
                    FieloEE__Transaction__c tranRet = new FieloEE__Transaction__c(
                        FieloEE__Member__c = inv.F_Member__r.FieloEE__ReferalMember__c,
                        FieloEE__Value__c = 0,
                        //07/07/2015
                        //F_ExternalValue__c = inv.F_TotalPoints__c,
                        F_ExternalValue__c = inv.F_TotalPointsRetailer__c,
                        FieloEE__Type__c = Label.Fielo_InvoiceProcessButtonRefStep
                    );
                    listNewTrans.add(tranRet);
                }
                
                //Retailer income
                if(inv.F_Retailer__c != null){
                    FieloEE__Transaction__c newTran = new FieloEE__Transaction__c (
                        FieloEE__Member__c = inv.F_Retailer__c,
                        FieloEE__Type__c = Label.Fielo_InvoiceProcessButtonRetailerInc,
                        //07/07/2015
                        //FieloEE__Value__c = inv.F_TotalAmount__c,
                        F_ExternalValue__c = inv.F_TotalPointsRetailer__c,
                        F_Invoice__c = inv.Id
                    );
                    listNewTrans.add(newTran);
                }
                        system.debug('heap limit: ' + Limits.getHeapSize());
                //Inoivce and Transaction update 
                update tran;
                        system.debug('heap limit: ' + Limits.getHeapSize()); 
                if(!listNewTrans.isEmpty()){
                    insert listNewTrans;
                }
                        system.debug('heap limit: ' + Limits.getHeapSize()); 
                listNewTrans.add(tran);
                if(!listNewTrans.isEmpty()){
                    for(FieloEE__Transaction__c trans: listNewTrans){
                        trans.FieloEE__Approved__c = true;                    
                    }
                    update listNewTrans;
                }
                        system.debug('heap limit: ' + Limits.getHeapSize()); 
                update listInvoiceDetailToUpdate;
                        system.debug('heap limit: ' + Limits.getHeapSize()); 
                update inv;
                        system.debug('heap limit: ' + Limits.getHeapSize()); 
            }catch(Exception e){
                Database.rollback(sp);
                retMessage = e.getMessage();
            }
            
            return retMessage;
        }else{
            return Label.Fielo_InvoiceProcessButtonNoInvDetail;
        }
    }
}