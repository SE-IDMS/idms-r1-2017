public with sharing class VFC_SpecializationTargetMarket {
    public list<SpecializationMarket__c> lstMarketSegment {get;set;}
    public VFC_SpecializationTargetMarket(ApexPages.StandardController controller) {
        lstMarketSegment = [Select MarketSegmentCatalog__r.MarketSegmentName__c from SpecializationMarket__c where Specialization__c=:controller.getId() and MarketSegmentCatalog__r.parentmarketsegment__c = null order by MarketSegmentCatalog__r.MarketSegmentName__c limit 1000]; 
        
    }

}