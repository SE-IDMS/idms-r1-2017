@isTest
public class Batch_UpdateAccountFromAMP_TEST{
     @testSetup static void datasetup() {
        List<CAPGlobalMasterData__c> capweightlst=new List<CAPGlobalMasterData__c>();
        for(Integer i=1;i<=8;i++){
            CAPGlobalMasterData__c gw=new CAPGlobalMasterData__c();
            gw.AnswerInterpretation1__c='AnswerInterpretation1__c';
            gw.AnswerInterpretation2__c='AnswerInterpretation2__c';
            gw.AnswerInterpretation3__c='AnswerInterpretation3__c';
            gw.AnswerInterpretation4__c='AnswerInterpretation4__c';
            gw.ClassificationLevel1__c='Default';
            gw.QuestionInterpretation__c='QuestionInterpretation__c';
            gw.QuestionSequence__c='Q'+i;
            gw.Weight__c=5;
           capweightlst.add(gw); 
        }
        insert capweightlst;
        Account a1 = Utils_TestMethods.createAccount();
        a1.AccountMasterProfile__c='MQ';
        a1.ClassLevel1__c='SI';
        a1.Tech_CL1Updated__c=TRUE;
        a1.AccountMasterProfileLastUpdated__c =null;
       insert a1;

     AccountMasterProfile__c amp1=new AccountMasterProfile__c();
        amp1.Account__c=a1.id;
        amp1.Q1Rating__c='2';
        amp1.Q2Rating__c='2';
        amp1.Q3Rating__c='2';
        amp1.Q4Rating__c='4';
        amp1.Q5Rating__c='3';
        amp1.Q6Rating__c='2';
        amp1.Q7Rating__c='4';
        amp1.Q8Rating__c='2';
        amp1.DirectSales__c=200;
        amp1.IndirectSales__c=300;
        amp1.totalPam__c=600;
        insert amp1;
        
        a1.AccountMasterProfile__c='MS';
        a1.ClassLevel1__c='SI';
        a1.Tech_CL1Updated__c=TRUE;
        a1.currencyIsocode='EUR';
        a1.AccountMasterProfileLastUpdated__c =null;
        update a1;
     }
    public static testMethod void Testrun1() {  
        Account a2=[Select Id,AccountMasterProfile__c,ClassLevel1__c,Tech_CL1Updated__c,TotalPAM__c,currencyIsocode,AccountMasterProfileLastUpdated__c from Account where AccountMasterProfile__c!=null];   
        if(a2!=null){
            a2.AccountMasterProfile__c='MG';
            a2.ClassLevel1__c='EU';
            a2.Tech_CL1Updated__c=TRUE;
            a2.TotalPAM__c=1;
            a2.currencyIsocode='INR';
            a2.AccountMasterProfileLastUpdated__c =null;
            update a2;
        }
        Batch_UpdateAccountFromAMP ampBatch2 = new Batch_UpdateAccountFromAMP();
        Database.executeBatch(ampBatch2,Integer.ValueOf(Label.CLQ316SLS009));
        Test.startTest();
            string CORN_EXP = '0 0 0 1 4 ?';
            Batch_UpdateAccountFromAMP ipbatch2 = new Batch_UpdateAccountFromAMP ();
            string jobid = system.schedule('my batch job', CORN_EXP, new Batch_UpdateAccountFromAMP());
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        Test.stopTest();
        }   
}