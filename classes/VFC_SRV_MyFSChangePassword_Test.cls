@isTest
public class VFC_SRV_MyFSChangePassword_Test {
    
    public static testMethod void testResetPassword() {

        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());        
        
        SRV_MyFs_SSOHandler ssoHandler = new SRV_MyFs_SSOHandler();
        Id userId = UserInfo.getUserId();
        ssoHandler.updateUser(userId, userId, userId, userId,
                              'federation_Id', new Map<String, String>(), 'SAML_Assertion');
        
        VFC_SRV_MyFSChangePassword.updatePasswordRemoting('oldPassword', 'newPassword');
        
        VFC_SRV_MyFsLoginPage.ResetPasswordResult resetPasswordResult = new VFC_SRV_MyFsLoginPage.ResetPasswordResult();
        
        VFC_SRV_MyFSChangePassword.debugMode = true;
        VFC_SRV_MyFSChangePassword.handleException('Exception Message', resetPasswordResult);
        VFC_SRV_MyFSChangePassword.debugMode = false;
        VFC_SRV_MyFSChangePassword.handleException('Exception Message', resetPasswordResult);
        VFC_SRV_MyFSChangePassword.handleException('Unable to update the user password', resetPasswordResult);
    }

}