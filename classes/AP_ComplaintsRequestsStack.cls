/*****
Hanamanth Stack
*****/


public class AP_ComplaintsRequestsStack {

    public static boolean isExcecute=false; 
    
    public static void updateComplaintsRequestStack(List<Answertostack__c> listSat ) {
        isExcecute=false;
        set<id> compRequsSet= new set<id> ();
        map<string,List<Answertostack__c>>  listCRSk = new map<string,List<Answertostack__c>>();
        
        map<string,Answertostack__c> mapCREH = new map<string,Answertostack__c>();
        List<ComplaintsRequestsEscalationHistory__c>  lisCrCRH = new List<ComplaintsRequestsEscalationHistory__c>();
        map<id,string>  mapCpmReq=new  map<id,string>();
        id AccountableOrg;
        string Depatrtment;
        for(Answertostack__c cEH:listSat) {
                 compRequsSet.add(cEH.ComplaintRequest__c);
        }
        
        //lisCrCRH=[select id,name,ComplaintRequest__c,From_IssueOwner__c,ContactEmailofFromAcctOrg__c,ToDepartment__c,Department__c, ComplaintRequest__r.AccountableOrganization__c,ToOrg__c, FromOrg__c, ComplaintRequest__r.Department__c, EscalationLevel__c, OrgSelectable__c, KeepInvolved__c  from ComplaintsRequestsEscalationHistory__c where ComplaintRequest__c in :compRequsSet and Event_Type__c != :Label.CLAPR15I2P07  order by Event_Date__c ASC];
        listCRSk=addretrivetheOrgToStack(compRequsSet);
        
         List<ComplaintsRequestsEscalationHistory__c>  lisAccOrgCrCRH = new List<ComplaintsRequestsEscalationHistory__c>();
        system.debug('TEST 67---->1'+mapCpmReq);
        map<string,Integer>  mapCrInt = new map<string,Integer>();
        mapCrInt=getLastKeepInvolved(listCRSk);
        if(listCRSk.size() > 0) {
          //  ansStock.add(listCRSk.get(postcurent));

        }
        
        List<ComplaintRequest__c> lis_CR = new List<ComplaintRequest__c>();
        List<ComplaintRequest__c> lis_CR_1 = new List<ComplaintRequest__c>();
        if(listCRSk.size() > 0 && mapCrInt.size() > 0) {
            for(string strId :listCRSk.keyset()) {
            
               
                ComplaintRequest__c compRt= new ComplaintRequest__c(id=strId );
                If(mapCrInt.containsKey(strId)) {
                    compRt.TECH_AnswerSackOrg__c=listCRSk.get(strId)[mapCrInt.get(strId)].Organization__c;//mapCREH.get(strId).FromOrg__c;
                  
                    if (listCRSk.get(strId)[mapCrInt.get(strId)].Department__c!=null)
                    {
                        compRt.TECH_StackDepartment__c=listCRSk.get(strId)[mapCrInt.get(strId)].Department__c;
                    }
                    else
                    {
                        System.debug('Department blank');
                        compRt.TECH_StackDepartment__c='';
                    }
                    lis_CR.add(compRt);
                }
                
            }   
        
        }
        else if(compRequsSet.size() > 0) {
         for(string str : compRequsSet) {
            ComplaintRequest__c compRt= new ComplaintRequest__c(id=str );
            compRt.TECH_AnswerSackOrg__c=null;
            lis_CR_1.add(compRt);
        }   
        
        }
        if(lis_CR_1.size() > 0) {
        
            update lis_CR_1;
        }
        if(lis_CR.size() > 0) {
        
            update lis_CR;
            isExcecute=true;
        }       
        
    }
    
    
    //get the Keep involve position 
     public static map<string,Integer> getLastKeepInvolved(map<string,List<Answertostack__c>> CRStack) {
        integer pos = -1;
        system.debug(pos);
        map<string,Integer>  mapOfCRandPostion= new map<string,Integer>();
        for( string strKey:CRStack.keyset()) {
            
            for (integer i =0 ; i <CRStack.get(strKey).size() && pos < 0; i++) {
                if (CRStack.get(strKey)[i].KeepInvolved__c) {
                   
                    mapOfCRandPostion.put(strKey,i);
                    break;
                }
                
            }
        }
        system.debug('Position --->'+mapOfCRandPostion);
        return mapOfCRandPostion;
    }
    
    public static map<string,List<Answertostack__c>> addretrivetheOrgToStack(set<id> compRequsSet) {
        
        system.debug(compRequsSet);
        map<string,List<Answertostack__c>>  list_CRSk = new map<string,List<Answertostack__c>> ();
        //list<Answertostack__c> updateKpInv= new list<Answertostack__c>();
        for(Answertostack__c astkObj:[select id,name,ComplaintRequest__c,KeepInvolved__c,TECH_StackCounter__c,Organization__c,Department__c  from Answertostack__c where ComplaintRequest__c in :compRequsSet order by TECH_StackCounter__c DESC]) {
        
            if(list_CRSk.containsKey(astkObj.ComplaintRequest__c)) {
                list_CRSk.get(astkObj.ComplaintRequest__c).add(astkObj);
                
            }else {
                list_CRSk.put(astkObj.ComplaintRequest__c,new List<Answertostack__c>{astkObj});
            }
        }
        
        return list_CRSk;
    }
    
   
    
}