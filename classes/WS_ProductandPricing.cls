//Generated by wsdl2apex

public class WS_ProductandPricing {
    public class getLocalPricingGroupInformationResponse {
        public WS_ProductandPricing.localPricingGroupInformationResult return_x;
        private String[] return_x_type_info = new String[]{'return','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class multipleProductAndPricingInformationSearchDataBean {
        public DateTime endDate;
        public String legacyAccountId;
        public String legacyChannelId;
        public WS_ProductandPricing.productAndPricingLineDataBean[] productAndPricingLineList;
        public String sdhLegacyPSName;
        public DateTime startDate;
        private String[] endDate_type_info = new String[]{'endDate','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] legacyAccountId_type_info = new String[]{'legacyAccountId','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] legacyChannelId_type_info = new String[]{'legacyChannelId','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] productAndPricingLineList_type_info = new String[]{'productAndPricingLineList','http://bridgefrontoffice.schneider-electric.com',null,'0','-1','true'};
        private String[] sdhLegacyPSName_type_info = new String[]{'sdhLegacyPSName','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] startDate_type_info = new String[]{'startDate','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'endDate','legacyAccountId','legacyChannelId','productAndPricingLineList','sdhLegacyPSName','startDate'};
    }
    public class localPricingGroupInformationSearchDataBean {
        public String legacyAccountId;
        public String legacyChannelId;
        public String localPricingGroup;
        public Integer quantity;
        public String requestedType;
        public Double requestedValue;
        public String sdhLegacyPSName;
        public DateTime startDate;
        private String[] legacyAccountId_type_info = new String[]{'legacyAccountId','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] legacyChannelId_type_info = new String[]{'legacyChannelId','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] localPricingGroup_type_info = new String[]{'localPricingGroup','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] quantity_type_info = new String[]{'quantity','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] requestedType_type_info = new String[]{'requestedType','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] requestedValue_type_info = new String[]{'requestedValue','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] sdhLegacyPSName_type_info = new String[]{'sdhLegacyPSName','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] startDate_type_info = new String[]{'startDate','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'legacyAccountId','legacyChannelId','localPricingGroup','quantity','requestedType','requestedValue','sdhLegacyPSName','startDate'};
    }
    public class multipleProductAndPricingInformationResult {
        public WS_ProductandPricing.productAndPricingInformationResult[] productAndPricingInfoList;
        public WS_ProductandPricing.returnDataBean returnInfo;
        private String[] productAndPricingInfoList_type_info = new String[]{'productAndPricingInfoList','http://bridgefrontoffice.schneider-electric.com',null,'0','-1','true'};
        private String[] returnInfo_type_info = new String[]{'returnInfo','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'productAndPricingInfoList','returnInfo'};
    }
    public class genericBean {
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class localPricingGroupInformationResult {
        public WS_ProductandPricing.localPricingGroupInformationDataBean localPricingGroupInfo;
        public WS_ProductandPricing.returnDataBean returnInfo;
        private String[] localPricingGroupInfo_type_info = new String[]{'localPricingGroupInfo','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] returnInfo_type_info = new String[]{'returnInfo','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'localPricingGroupInfo','returnInfo'};
    }
    public class returnDataBean {
        public String returnCode;
        public String returnMessage;
        private String[] returnCode_type_info = new String[]{'returnCode','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] returnMessage_type_info = new String[]{'returnMessage','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'returnCode','returnMessage'};
    }
    public class ServiceExpectedException extends Exception{
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class getProductAndPricingInformation {
        public WS_ProductandPricing.productAndPricingInformationSearchDataBean arg0;
        private String[] arg0_type_info = new String[]{'arg0','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'arg0'};
    }
    public class getMultipleProductAndPricingInformationResponse {
        public WS_ProductandPricing.multipleProductAndPricingInformationResult return_x;
        private String[] return_x_type_info = new String[]{'return','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class ServiceFatalException extends Exception  {
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class productAndPricingLineDataBean {
        public String catalogNumber;
        public String localPricingGroup;
        public Integer quantity;
        public String requestedType;
        public Double requestedValue;
        private String[] catalogNumber_type_info = new String[]{'catalogNumber','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] localPricingGroup_type_info = new String[]{'localPricingGroup','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] quantity_type_info = new String[]{'quantity','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] requestedType_type_info = new String[]{'requestedType','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] requestedValue_type_info = new String[]{'requestedValue','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'catalogNumber','localPricingGroup','quantity','requestedType','requestedValue'};
    }
    public class productAndPricingInformationDataBean {
        public String catalogNumber;
        public Double compensation;
        public String currencyCode;
        public String description;
        public Double listPrice;
        public String localPricingGroup;
        public Double standardDiscount;
        public Double standardMulitplier;
        public Double standardPrice;
        private String[] catalogNumber_type_info = new String[]{'catalogNumber','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] compensation_type_info = new String[]{'compensation','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] currencyCode_type_info = new String[]{'currencyCode','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] description_type_info = new String[]{'description','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] listPrice_type_info = new String[]{'listPrice','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] localPricingGroup_type_info = new String[]{'localPricingGroup','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] standardDiscount_type_info = new String[]{'standardDiscount','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] standardMulitplier_type_info = new String[]{'standardMulitplier','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] standardPrice_type_info = new String[]{'standardPrice','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'catalogNumber','compensation','currencyCode','description','listPrice','localPricingGroup','standardDiscount','standardMulitplier','standardPrice'};
    }
    public class getLocalPricingGroupInformation {
        public WS_ProductandPricing.localPricingGroupInformationSearchDataBean arg0;
        private String[] arg0_type_info = new String[]{'arg0','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'arg0'};
    }
    public class productAndPricingInformationResult {
        public WS_ProductandPricing.productAndPricingInformationDataBean productAndPricingInfo;
        public WS_ProductandPricing.returnDataBean returnInfo;
        private String[] productAndPricingInfo_type_info = new String[]{'productAndPricingInfo','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] returnInfo_type_info = new String[]{'returnInfo','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'productAndPricingInfo','returnInfo'};
    }
    public class localPricingGroupInformationDataBean {
        public Double compensation;
        public String description;
        public Double standardDiscount;
        public Double standardMultiplier;
        private String[] compensation_type_info = new String[]{'compensation','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] description_type_info = new String[]{'description','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] standardDiscount_type_info = new String[]{'standardDiscount','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] standardMultiplier_type_info = new String[]{'standardMultiplier','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'compensation','description','standardDiscount','standardMultiplier'};
    }
    public class getProductAndPricingInformationResponse {
        public WS_ProductandPricing.productAndPricingInformationResult return_x;
        private String[] return_x_type_info = new String[]{'return','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class productAndPricingInformationSearchDataBean {
        public String catalogNumber;
        public String legacyAccountId;
        public String legacyChannelId;
        public Integer quantity;
        public String requestedType;
        public Double requestedValue;
        public String sdhLegacyPSName;
        public DateTime startDate;
        private String[] catalogNumber_type_info = new String[]{'catalogNumber','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] legacyAccountId_type_info = new String[]{'legacyAccountId','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] legacyChannelId_type_info = new String[]{'legacyChannelId','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] quantity_type_info = new String[]{'quantity','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] requestedType_type_info = new String[]{'requestedType','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] requestedValue_type_info = new String[]{'requestedValue','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] sdhLegacyPSName_type_info = new String[]{'sdhLegacyPSName','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] startDate_type_info = new String[]{'startDate','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'catalogNumber','legacyAccountId','legacyChannelId','quantity','requestedType','requestedValue','sdhLegacyPSName','startDate'};
    }
    public class getMultipleProductAndPricingInformation {
        public WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean arg0;
        private String[] arg0_type_info = new String[]{'arg0','http://bridgefrontoffice.schneider-electric.com',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'arg0'};
    }
    public class ProductAndPricingInformationPort {
        public String endpoint_x = System.Label.CLSEP12SLS30;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com', 'WS_ProductandPricing'};
        public WS_ProductandPricing.localPricingGroupInformationResult getLocalPricingGroupInformation(WS_ProductandPricing.localPricingGroupInformationSearchDataBean arg0) {
            WS_ProductandPricing.getLocalPricingGroupInformation request_x = new WS_ProductandPricing.getLocalPricingGroupInformation();
            request_x.arg0 = arg0;
            WS_ProductandPricing.getLocalPricingGroupInformationResponse response_x;
            Map<String, WS_ProductandPricing.getLocalPricingGroupInformationResponse> response_map_x = new Map<String, WS_ProductandPricing.getLocalPricingGroupInformationResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://bridgefrontoffice.schneider-electric.com',
              'getLocalPricingGroupInformation',
              'http://bridgefrontoffice.schneider-electric.com',
              'getLocalPricingGroupInformationResponse',
              'WS_ProductandPricing.getLocalPricingGroupInformationResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        public WS_ProductandPricing.multipleProductAndPricingInformationResult getMultipleProductAndPricingInformation(WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean arg0) {
            WS_ProductandPricing.getMultipleProductAndPricingInformation request_x = new WS_ProductandPricing.getMultipleProductAndPricingInformation();
            request_x.arg0 = arg0;
            WS_ProductandPricing.getMultipleProductAndPricingInformationResponse response_x;
            Map<String, WS_ProductandPricing.getMultipleProductAndPricingInformationResponse> response_map_x = new Map<String, WS_ProductandPricing.getMultipleProductAndPricingInformationResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://bridgefrontoffice.schneider-electric.com',
              'getMultipleProductAndPricingInformation',
              'http://bridgefrontoffice.schneider-electric.com',
              'getMultipleProductAndPricingInformationResponse',
              'WS_ProductandPricing.getMultipleProductAndPricingInformationResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        public WS_ProductandPricing.productAndPricingInformationResult getProductAndPricingInformation(WS_ProductandPricing.productAndPricingInformationSearchDataBean arg0) {
            WS_ProductandPricing.getProductAndPricingInformation request_x = new WS_ProductandPricing.getProductAndPricingInformation();
            request_x.arg0 = arg0;
            WS_ProductandPricing.getProductAndPricingInformationResponse response_x;
            Map<String, WS_ProductandPricing.getProductAndPricingInformationResponse> response_map_x = new Map<String, WS_ProductandPricing.getProductAndPricingInformationResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://bridgefrontoffice.schneider-electric.com',
              'getProductAndPricingInformation',
              'http://bridgefrontoffice.schneider-electric.com',
              'getProductAndPricingInformationResponse',
              'WS_ProductandPricing.getProductAndPricingInformationResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}