public class VFC_MassPsAssignment extends VFC_PermissionSetAssignment {

    public List<User> userList {get;set;}
    public String userName {get;set;}
    public List<SelectOption> columnHeaders {get;set;}

    Set<String> originalvalues = new Set<String>();

    public List<SelectOption> availableUserList {get;set;}
    public List<SelectOption> selectedUserList {get;set;}
    
    public List<PermissionSetAssignmentRequests__c> psAssignmentRequests {get;set;}
    
    public Set<String> selectedUserSet = new Set<String>();
    
    public String userSesaList {get;set;}

    public VFC_MassPsAssignment() {
         
        getColumns();  
        userList = new List<User>();
        userList.add(new User(FirstName = 'Name'));
        
        String assignedPs = ApexPages.currentPage().getParameters().get('assignedPs');
        massAssign = true;
        
        availableUserList = new List<SelectOption>();
        selectedUserList = new List<SelectOption>();

    }

    public void getColumns() {
        columnHeaders = new List<SelectOption>();
        columnHeaders.add(new SelectOption('Name','Name'));
    }
    
    public PageReference saveAssignments() {
        
        PageReference resultPage = new PageReference('/apex/VFP_MassPsAssignmentResults?assignedPs=true');
        
        Set<Id> userIdString = new Set<Id>();
        
        for(SelectOption selectedUser:selectedUserList) {
            userIdString.add(selectedUser.getValue());
        }
        
        List<User> relatedUserList = [SELECT Id, IsActive, FirstName, LastName, Name FROM User WHERE Id IN :userIdString];
       
        if(!relatedUserList.isEmpty() && !existingAssignments.isEmpty()) {
          
            psAssignmentRequests = AP_PermissionSetGroupMethods.generateRequest(existingAssignments, relatedUserList, 'DoNotCreateDuplicates');
            insert psAssignmentRequests;
            
            Set<Id> psAssignmentRequestsIdSet = new Set<Id>();
            
            for(PermissionSetAssignmentRequests__c psAssignmentRequest:psAssignmentRequests) {
                psAssignmentRequestsIdSet.add(psAssignmentRequest.Id);
            }
            
            psAssignmentRequests = [SELECT Id, Status__c, Name, AssigneeFormula__c, PermissionSetGroup__c 
                                    FROM PermissionSetAssignmentRequests__c WHERE Id IN :psAssignmentRequestsIdSet];          
        }
        else {
        
            if(relatedUserList.isEmpty()) {
                ApexPages.Message atleastOneUserMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select at least one user.');
                ApexPages.addMessage(atleastOneUserMessage);            
            }
            
            if(existingAssignments.isEmpty()) {
                ApexPages.Message atLeastOnePermissionSetGroupMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select at least one Permission Set Group.');
                ApexPages.addMessage(atLeastOnePermissionSetGroupMessage);             
            }
            
            resultPage = null;             
        }
       
        return resultPage;
    }
    
    public PageReference importUsers() {
        
        PageReference result = new PageReference('/apex/VFP_MassPsAssignment');
        
        for(SelectOption selectedUser:selectedUserList) {
            selectedUserSet.add(selectedUser.getValue());
        }
        
        
        if(userSesaList != null && userSesaList != '') {
        
            Set<String> sesaIdSet = new Set<String>();
            List<String> sesaIdList = new List<String>();
            
            for(String sesaNumber:userSesaList.split('\n')) {
                
                if(sesaNumber != null) {
                    sesaIdSet.add(sesaNumber.trim().toUpperCase());
                    sesaIdList.add(sesaNumber.trim().toUpperCase());       
                                                
                }                
            }
            
            Map<String, SelectOption>  userSesaMap = new Map<String, SelectOption>();
            
            system.debug('sesaIdList = ' +sesaIdList);
            
            for(User user:[SELECT Id, UserName, Name, FirstName, LastName, SESAExternalID__c
                           FROM User WHERE Username IN :sesaIdSet OR SESAExternalID__c IN :sesaIdList 
                           ]) {
                
                userSesaMap.put(user.SESAExternalID__c.toUpperCase(), new SelectOption(user.Id, user.Name));
                userSesaMap.put(user.Username.toUpperCase(), new SelectOption(user.Id, user.Name));
            }
            
            for(Integer i = 0; i < sesaIdList.size(); i++) {
            
                if(userSesaMap.get(sesaIdList.get(i)) == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Line '+(i+1)+': No user found for SESA: '+ sesaIdList.get(i)));
                    result = null;
                }
                else if(!selectedUserSet.contains(sesaIdList.get(i))){
                    selectedUserList.add(userSesaMap.get(sesaIdList.get(i)));                    
                }
            }
        }
        else {
            ApexPages.Message assignedPsMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'You must enter at least one user\'s sesa.');
            ApexPages.addMessage(assignedPsMessage);  
            result = null;
        }
        
        return result;
    }
    
    public PageReference backToMassAssign() {
        return new PageReference('/apex/VFP_MassPsAssignment');
    }
    
    public PageReference returnAddSeveralUserPage() {
        return new PageReference('/apex/VFP_AddSeveralUsers');
    }
    
    public void searchUsers() {
        
        availableUserList.clear();
        
        if(userName != null && userName != '') {
            String searchString = '%' + userName + '%'; 
            for(User user:[SELECT Id, Name FROM User WHERE Name LIKE :searchString LIMIT 100]) {
                availableUserList.add(new SelectOption(user.Id, user.Name));
            }        
        }
    }
}