@isTest
public class Scheduled_OpptyCrFromIP_TEST {
    static testMethod void unitTest()
    {
        Test.StartTest();
        Scheduled_OpptyCrFromIP sh1 = new Scheduled_OpptyCrFromIP();
        DateTime currentDate = System.Now().addHours(3)  ;
         String CRON_EXPRESSION = '' + 59 + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' 
                                  + currentDate.day() + ' ' + currentDate.month() + ' ? ' + currentDate.year();
        
        String jobId = System.schedule('Prodcut create schedule', CRON_EXPRESSION, sh1);
        Test.StopTest();      
    }
}