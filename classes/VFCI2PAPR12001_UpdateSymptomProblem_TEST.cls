//*********************************************************************************
// Class Name       : VFCI2PAPR12001_UpdateSymptomToProblem_TEST
// Purpose          : Test Class for VFCI2PAPR12001_UpdateSymptomToProblem
// Created by       : Global Delivery Team
// Date created     : 28th February 2011
// Modified by      : sukumar salla
// Date Modified    : 4th Feb 2014, 2014 April Release
// Remarks          : For April - 12 Release
///********************************************************************************/

@isTest(SeeAllData=true)
private class VFCI2PAPR12001_UpdateSymptomProblem_TEST {


    static testMethod void myUnitTest() {
       
       // object creation
       BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        Database.insert(breEntity);
       ID BreEntID =  breEntity.id;
       List<User> UserList = new List<User>();
        User PLdr = Utils_TestMethods.createStandardUser('PLdr'); 
        User PLdr1 = Utils_TestMethods.createStandardUser('PLdr1');
        UserList.add(PLdr);
        UserList.add(PLdr1);
        Database.insert(UserList);
        
        List<Problem__c> prblmList = new List<Problem__c>();
        List<Problem__c> prblmUpdateList = new List<Problem__c>();
        Problem__c PrblmNew = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmInProgress = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmInProgressSafety = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmSevMinExecuting = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmSevSafetyExecuting = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmSevMinCompleted = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmSevSafetyCompleted = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmStandBy = Utils_TestMethods.createProblem(BreEntID,1); 
        Problem__c PrblmCanceled = Utils_TestMethods.createProblem(BreEntID,1);
        
        system.debug('@@@@@@'+PrblmNew.Id);  
        insert PrblmNew;
        system.debug('@@@@@@'+PrblmNew.Id);  
         prblmList.add(PrblmNew);
        prblmList.add(PrblmInProgress);
        prblmList.add(PrblmSevMinExecuting);
        prblmList.add(PrblmSevSafetyExecuting);
        prblmList.add(PrblmSevMinCompleted);
        prblmList.add(PrblmSevSafetyCompleted);
        prblmList.add(PrblmStandBy);
        prblmList.add(PrblmInProgressSafety);
        prblmList.add(PrblmCanceled); 
        
          system.debug('Hello');        
        //insert prblmList;
        //Database.SaveResult[] PrblmInsertResult = Database.insert(prblmList, false);
      
      //system.debug('@@@@@@'+PrblmNew.Id);
      
        Problem__c pr=new Problem__c();
        pr.Symptoms__c='TestSymptom';
        pr.TECH_Symptoms__c ='A - a';
        insert pr; 
      
         Problem__c pr1=new Problem__c();
        pr1.Symptoms__c='TestSymptom1';
        pr1.TECH_Symptoms__c ='A - a';
        insert pr1; 
        
        delete pr1;
        
       List<CommercialReference__c> lstctr=new list<CommercialReference__c>();
        lstctr.add(new  CommercialReference__c(Problem__c=pr.id,CommercialReference__c='acd',GMRCode__c='A0012345',ProductGroup__c='sch'));
        lstctr.add(new  CommercialReference__c(Problem__c=pr.id,CommercialReference__c='abd',GMRCode__c='A0012345',ProductGroup__c='Sd'));
        Database.SaveResult[] lstSave = Database.insert(lstctr,false);
          
        OPP_Product__c objProdct=new OPP_Product__c(TECH_PM0CodeInGMR__c='A0012345',ProductFamily__c='',ProductLine__c='test',Family__c='test');
        Database.insert(objProdct);
        list<Symptom__c> lstsymptomMapp=new list<Symptom__c>();
        ApexPages.currentPage().getParameters().put('id', prblmList[0].id);
        ApexPages.currentPage().getParameters().put('ac', '');
        lstsymptomMapp.add(new Symptom__c(Symptom__c='A',Product__c=objProdct.id,SubSymptom__c='a'));
        lstsymptomMapp.add(new Symptom__c(Symptom__c='B',Product__c=objProdct.id,SubSymptom__c='b'));
        
        system.debug('@@@@lstsymptomMapp'+lstsymptomMapp);
        Database.SaveResult[] lstSV2 = Database.insert(lstsymptomMapp,false);
     /*   
        ComplaintRequest__c cr = new ComplaintRequest__c();
        cr.Category__c = 'TestCat';
        cr.ReportingEntity__c='a1HJ00000011rmK';
        //cr.TECH_Symptoms__c='A001234565';   
        cr.TECH_CRReceivedGMRCode__c='A00111123455';
        insert cr;
      */  

        RelatedSymptom__c rs = new RelatedSymptom__c(Problem__c=pr.id, Symptom__c='symptom',SubSymptom__c='Sub symptom',OtherSymptom__c='other symptom');
        insert rs;
        
        ApexPages.currentPage().getParameters().put('crid','');
        ApexPages.currentPage().getParameters().put('pid', pr.Id);
        
        VFCI2PAPR12001_UpdateSymptomToProblem clsupprb =new VFCI2PAPR12001_UpdateSymptomToProblem();
        clsupprb.ParentChlidSymt(lstsymptomMapp);
        clsupprb.setProbSymptom.Add('A - a');
        clsupprb.setProbSymptom.Add('B - b');
        clsupprb.isDisable=true;
        clsupprb.isDisable2=false;
        clsupprb.allChecked=true;
        clsupprb.showMaping(); 
        clsupprb.SaveMaping() ;
        clsupprb.ModyfiMaping();
        clsupprb.ModifyProblem();
        //clsupprb.validateCR();
        
      //  cr.TECH_Symptoms__c='A001234565; A - a';
      //  Update cr;
         clsupprb.showMaping(); 
        clsupprb.SaveMaping() ;
        clsupprb.ModyfiMaping();
        clsupprb.ModifyProblem();
        //clsupprb.validateCR();
        
        
        clsupprb.objProblemUpdSymptom = new Problem__c(Id=PrblmNew.Id);
        clsupprb.isCR=true;
        clsupprb.isPRB=true;
        
        ApexPages.currentPage().getParameters().put('crid',''); 
        ApexPages.currentPage().getParameters().put('pid', pr.id);
        ApexPages.currentPage().getParameters().put(Label.CLI2PAPR120005, Label.CLI2PAPR120006);
        VFCI2PAPR12001_UpdateSymptomToProblem clsupprb1 =new VFCI2PAPR12001_UpdateSymptomToProblem();
        clsupprb1.setProbSymptom.Add('A');
        clsupprb1.setProbSymptom.Add('B');
        clsupprb1.isDisable=false;
        clsupprb1.isDisable2=true;
        clsupprb1.allChecked=true;
        clsupprb1.showMaping();
        clsupprb1.checkall();
        clsupprb1.SaveMaping() ;
        clsupprb1.ModyfiMaping();
        clsupprb1.canceladdsymptom();
        //clsupprb1.validateCR();
        
        clsupprb.objProblemUpdSymptom = new Problem__c(Id=PrblmNew.Id);
        clsupprb.isCR=true;
        clsupprb.isPRB=true;
         
       //system.debug('test----->'+clsupprb1.objComplntRequestSymptom.Related_Symptoms__r);
       
    }
    
    static testMethod void myUnitTest1() {
       
       // object creation
       BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        Database.insert(breEntity);
       ID BreEntID =  breEntity.id;
       List<User> UserList = new List<User>();
        User PLdr = Utils_TestMethods.createStandardUser('PLdr'); 
        User PLdr1 = Utils_TestMethods.createStandardUser('PLdr1');
        UserList.add(PLdr);
        UserList.add(PLdr1);
        Database.insert(UserList);
        
      /*  Country__c CountryObj= Utils_TestMethods.createCountry();
        CountryObj.Name='TestCountryxdrt';
        //CountryObj.CountryCode__c='TCY1';
        Insert CountryObj;
         country__c samplecountry=null;*/
         country__c CountryObj=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            CountryObj=countries[0];
        else{               
            CountryObj=new country__c(Name='TestCountry',CountryCode__c='TCC');
            insert CountryObj;    
        }
        BusinessRiskEscalationEntity__c Org1 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST Repair',
                                            Entity__c='LCR TEST Repair',
                                            SubEntity__c='LCR Sub-Entity1',
                                            Location__c='LCR Location 1',
                                            Location_Type__c= 'LCR',
                                            Street__c = 'LCR Street1',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'LCR City1',
                                            RCZipCode__c = '500225',
                                            ContactEmail__c ='SCH1@schneider.com'
                                            );
        Insert Org1; 

        OPP_Product__c objProdct=new OPP_Product__c(TECH_PM0CodeInGMR__c='A0012345',ProductFamily__c='',ProductLine__c='test',Family__c='test');
        Database.insert(objProdct);
        list<Symptom__c> lstsymptomMapp=new list<Symptom__c>();
      
        ApexPages.currentPage().getParameters().put('ac', '');
        lstsymptomMapp.add(new Symptom__c(Symptom__c='A',Product__c=objProdct.id,SubSymptom__c='a'));
        lstsymptomMapp.add(new Symptom__c(Symptom__c='B',Product__c=objProdct.id,SubSymptom__c='b'));
        
        system.debug('@@@@lstsymptomMapp'+lstsymptomMapp);
        Database.SaveResult[] lstSV2 = Database.insert(lstsymptomMapp,false);
        
         //objComplntRequestSymptom = [select id,TECH_SymptomsAvailable__c,Category__c,TECH_CRReceivedGMRCode__c,(select id, name,Symptom__c,SubSymptom__c,OtherSymptom__c from Related_Symptoms__r),CommercialReferenceReceived__c,FamilyReceived__c,TECH_Symptoms__c from ComplaintRequest__c where Id=:varId];
        
        ComplaintRequest__c cr = new ComplaintRequest__c();
        cr.Category__c = 'Troubleshooting*';
        cr.ReportingEntity__c=Org1.id;
        cr.TECH_Symptoms__c='A - a';   
        cr.TECH_CRReceivedGMRCode__c='A0012345';
        cr.FamilyReceived__c='test';
        insert cr;
        
        RelatedSymptom__c rs = new RelatedSymptom__c(ComplaintRequest__c=cr.id, Symptom__c='symptom',SubSymptom__c='Sub symptom',OtherSymptom__c='other symptom');
        insert rs;

        
        ApexPages.currentPage().getParameters().put('crid',cr.Id);
        ApexPages.currentPage().getParameters().put('pid', '');
        
        VFCI2PAPR12001_UpdateSymptomToProblem clsupprb =new VFCI2PAPR12001_UpdateSymptomToProblem();
        clsupprb.ParentChlidSymt(lstsymptomMapp);
        clsupprb.setProbSymptom.Add('A - a');
        clsupprb.setProbSymptom.Add('B - b');
        clsupprb.isDisable=true;
        clsupprb.isDisable2=false;
        clsupprb.allChecked=true;
        clsupprb.isCR=true;
        clsupprb.isPRB=true;
        clsupprb.checkAll();
        clsupprb.showMaping(); 
        clsupprb.SaveMaping() ;
        clsupprb.ModyfiMaping();
        clsupprb.ModifyProblem();
        
        clsupprb.objComplntRequestSymptom= new ComplaintRequest__c(Id=cr.Id);
        clsupprb.isCR=true;
        clsupprb.isPRB=true;
                  
       
    }
}