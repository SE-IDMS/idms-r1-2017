/*
    Author          : Siddharatha Nagavarapu (Goblal Delivery Team)
    Date Created    : 03/11/2010
    Description     : Test class for batch apex Batch03_CustomerVisitKPI
*/
@isTest
private class Batch03_CustomerVisitKPI_TEST {

    static testMethod void myUnitTest() {
    //create a cap record with the condition
    User runAsUser = Utils_TestMethods.createStandardUser('tst1');
    runAsUser.BypassVR__c=True;
    runAsUser.CAPRelevant__c=True;
    insert runAsUser;
    System.runAs(runAsUser){
    
        List<Event> allEvents=new List<Event>();
        RecordType eventRecordType=[SELECT id,SobjectType FROM RecordType where SobjectType='Event' and Name='Customer Visit'];
        
        Event eventRecord = Utils_TestMethods.createEvent(null,null,'Closed');
        eventRecord.RecordTypeId=eventRecordType.id;
        DateTime dt=System.Now();
        eventRecord.startDateTime=dt.addMonths(-1).addDays(-10);
        eventRecord.ActivityDate=Date.valueOf(dt.addMonths(-1));
        eventRecord.EndDateTime=dt.addMonths(-1);       
        allEvents.add(eventRecord);
        
        Event eventRecord1 = Utils_TestMethods.createEvent(null,null,'Closed');
        eventRecord1.RecordTypeId=eventRecordType.id;
     
        eventRecord1.startDateTime=dt.addMonths(-1).addDays(-10);
        eventRecord1.ActivityDate=Date.valueOf(dt.addMonths(-1));
        eventRecord1.EndDateTime=dt.addMonths(-1);  
         allEvents.add(eventRecord1);
        
        Event eventRecord2 = Utils_TestMethods.createEvent(null,null,'Closed');
        eventRecord2.RecordTypeId=eventRecordType.id;
     
        eventRecord2.startDateTime=dt.addMonths(-1).addDays(-10);
        eventRecord2.ActivityDate=Date.valueOf(dt.addMonths(-1));
        eventRecord2.EndDateTime=dt.addMonths(-1);       
         allEvents.add(eventRecord2);
        
        
        Test.startTest();          
        insert allEvents;   
           
           //testing the schedule apex
           String jobId = System.schedule('Batch03_CustomerVisitKPI','0 0 23 L * ?', new Batch03_CustomerVisitKPI());
           // Get the information from the CronTrigger API object 
           CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
           // Verify the expressions are the same     
        //   System.assertEquals('0 0 23 L * ?',ct.CronExpression);
           // Verify the job has not run     
        //   System.assertEquals(0, ct.TimesTriggered);
         Batch03_CustomerVisitKPI newBatch=new Batch03_CustomerVisitKPI();                        
         newBatch.createBatches();
           
           Test.stopTest();
       }

    }
}