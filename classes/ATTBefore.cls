public class ATTBefore {

    public static void setSitesTokenOnInsert (List<AssignedToolsTechnicians__c> triggerNew, Boolean isInsert) {
    /*
        Method to stamp Sites Access Token with a random 18 digit string value.
    
    */
    
        if (isInsert) {
        
            for (AssignedToolsTechnicians__c att: triggerNew) {
            
                Long randomLong1 = Crypto.getRandomLong();
                Long randomLong2 = Crypto.getRandomLong();
                att.Sites_Access_Token__c = String.valueOf(Math.abs(randomLong1)) + String.valueOf(Math.abs(randomLong2));
            }
        }
    }    
}