@istest
private class AP36_UpdateCaseAfterSentRMA_TEST
{
    static testMethod void TEST_AP36()
    {
        Country__c accountCountry = new Country__c();
        accountCountry.CountryCode__c = 'ZZ';
        insert accountCountry;
        
        StateProvince__c accountStateProvince = new StateProvince__c();
        accountStateProvince.Country__c = accountCountry.ID;
        accountStateProvince.CountryCode__c = 'ZZ';
        accountStateProvince.StateProvinceCode__c = 'WW';
        insert accountStateProvince;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.StateProvince__c = accountStateProvince.ID;
        account1.Country__c = accountCountry.ID;
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
        
        RMA__c newRMA = new RMA__c();
        newRMA.case__c = case1.ID;
        newRMA.AccountName__c = account1.Id;

        //Account Address
        newRMA.AccountStreet__c  = 'Account Street';
        newRMA.AccountCity__c    = 'Account City';
        newRMA.AccountZipCode__c = '12345';
        newRMA.AccountCountry__c = accountCountry.Id;
        newRMA.AccountPOBox__c   = 'Account PO Box';

        //Shipping Address
        newRMA.ShippingStreet__c  = 'Shipping Street';
        newRMA.ShippingCity__c    = 'Shipping City';
        newRMA.ShippingZipCode__c = '67890';
        newRMA.ShippingCountry__c = accountCountry.Id;
        newRMA.ShippingPOBox__c   = 'Shipping PO Box';

        insert newRMA;
        
        Task sentRMA = new Task(WhatID = newRMA.ID, Subject = 'Email: TEST', Description = '**** TEST ****');
        
        insert sentRMA;
    }
}