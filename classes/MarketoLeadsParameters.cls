global class MarketoLeadsParameters {
        global string action;
        global string lookupField;
        global string partitionName;
        global List<MarketoLeadsInput> input;
    }