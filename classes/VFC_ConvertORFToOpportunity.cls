/*
Author          : Schneider Electric, IPO (GD) 
Date Created    : 26/05/2012
Description: This Controller class is called when the user clicks on 'Convert to Opportunity' button on ORF
1. Check if AccountbFO is mapped, if not navigate the user to Account De-Duplication screen
2. Use the selected account and move to step 3 or Create a new account and move to step #3
3. Navigate the user to Opportunity de-duplication screen
4. When clicked on CreateOpportunity in Step #3, control comes back to init
5. Check if ContactbFO is mapped, if not setup a new contact and setup a new opportunity
6. Setup the Value Chain Player list
7. Setup the Notes & Attachments

6-Mar-2013    Srinivas Nallapati    Mar13 PRM    BR-2910
*/

public without sharing class VFC_ConvertORFToOpportunity {
    public static boolean byPassValidation = false;
    public static id ORFOwnerid;
    OpportunityRegistrationForm__c oppRegForm;
    Boolean skipOpportunityCheck = false;
    Boolean skipAccountCheck = false;
      Boolean skipContactCheck = false;
       public Boolean isConInfoReq{ get; set; }
    String oppCountry;
    
    //START: Release APR14, BR-4513 
    
    public String LeadingBU  ; 
    //END: Release APR14
    public ApexPages.StandardController controller;
    
    public VFC_ConvertORFToOpportunity(ApexPages.StandardController controller) {
             isConInfoReq = False;
            this.controller = controller;
            if(!Test.isRunningTest())
            controller.addFields(new List<String> {
                        'PartnerLanguage__c','Name','Name__c' ,'AccountbFO__c', 'AccountName__c', 'AccountCountry__c', 'AccountAddress__c',
                        'AccountStateProvince__c','AccountCity__c','AccountZipcode__c','ProjectAddress__c',
                        'ContactbFO__c', 'ContactFirstName__c', 'ContactLastName__c', 'ContactEmail__c', 'ContactPhone__c',
                        'Status__c', 'CreatedBy', 'DescriptionComment__c', 'OwnerId', 
                        'ConvertedIntoAnOpportunity__c', 'DegreeofSpecification__c', 'Probability__c',  
                        'OpportunityScope__c', 'OpportunityType__c', 'CurrencyIsoCode','Amount__c', 'Amount2__c', 
                        'ExpectedOrderDate__c', 'ProjectCountry__c', 'MarketSegment__c', 'MarketSubSegment__c', 
                        'TECH_PartnerAccountId__c', 'TECH_PartnerContactId__c', 'TechOpportunityExists__c', 
                        'TECH_IsORFValid__c', 'TECH_LeadingBusiness__c','TECH_CountryOfDestination__c','CreatedById','CreatedBy.ContactId',
                        'TECH_Partner_Program__c','TECH_IsContactInfoRequired__c','PartnerProgram__c',
                        'ProgramLevel__c','PartnerKeycode__c','ContactbFO__r','AccountbFO__r','ContactbFO__r.AccountId', 'AccountbFO__r.Id',
                        'TECH_EndUserAccointID__c','TECH_OppAccount__c','TECH_ORFConvertionStatus__c','TECH_ORFConvertionStageName__c','EndUserType__c',
                        'TECH_ChannelPartnerStage__c','TECH_ORFConvertionCountry__c','TECH_ORFConvertionAddress__c','TECH_ORFConvertionZIPCode__c',
                        'TECH_ORFConvertionStateProvince__c','TECH_ORFConvertionCity__c','RecordTypeId','FinalCustSiteAcc__c'
                        });

            this.oppRegForm = ( OpportunityRegistrationForm__c) controller.getRecord();
            
            system.debug ('*** SkipOpportunityCheck **'+ ApexPages.CurrentPage().getparameters().get('skipOppCheck'));
            if (ApexPages.CurrentPage().getparameters().get('skipOppCheck') != null) {
                SkipOpportunityCheck = Boolean.valueOf(ApexPages.CurrentPage().getparameters().get('skipOppCheck')); 
            }
            
            system.debug ('*** SkipAccountCheck **'+ ApexPages.CurrentPage().getparameters().get('skipAccCheck'));
            if (ApexPages.CurrentPage().getparameters().get('skipAccCheck') != null) {
                SkipAccountCheck = Boolean.valueOf(ApexPages.CurrentPage().getparameters().get('skipAccCheck')); 
            }
            
             if (ApexPages.CurrentPage().getparameters().get('skipConCheck') != null) {
                skipContactCheck = Boolean.valueOf(ApexPages.CurrentPage().getparameters().get('skipConCheck')); 
            }
    }
   //---------------------------------------------------------------------------------------------------------------------------------------
    private PageReference convertORFToOpportunity() {
        System.Debug('*** Check Contact Info: ' + oppRegForm.TECH_IsContactInfoRequired__c);
        
        if(this.oppRegForm.EndUserType__c == null || this.oppRegForm.EndUserType__c == 'Business Account') //Regular ORF or Business Account
        {
            if(oppRegForm.TECH_IsContactInfoRequired__c=='false' )  //if Contact details are present 
            {
                System.Debug('*** New Contact ***');
                Sobject newContact = new Contact();
                List<CS_OrfToContact__c> otc = CS_OrfToContact__c.getall().values();
    
                if (this.oppRegForm.ContactbFO__c == null)
                {
                    for(CS_OrfToContact__c csLabel : otc){
                        newContact.put(csLabel.Target__c, this.oppRegForm.get(csLabel.source__c));
                    }
                    System.Debug('*** New Contact ***' + newContact);
                    insert newContact;
                    Contact c = (Contact)newContact;
                    this.oppRegForm.ContactbFO__c = c.Id;
                    update this.oppRegForm;
                }
            }            
        }
        
        Sobject opportunity = new Opportunity(); 
        List<CS_OrfToOpportunity__c> oto= CS_OrfToOpportunity__c.getall().values();
        for(CS_OrfToOpportunity__c custLabel : oto){
         System.Debug('*** Source'+custLabel.Source__c);
            if (custLabel.Source__c != null) {
                opportunity.put(custLabel.Target__c, this.oppRegForm.get(custLabel.source__c));
            }
            else if (custLabel.Defaults_Text__c != null) {
                opportunity.put(custLabel.Target__c, custLabel.Defaults_Text__c);
            }
        }
        system.debug('*****OPT'+ opportunity);
        ORFOwnerid = oppRegForm.ownerid;
        opportunity.put('LeadingBusiness__c',getLeadingBusinessUnit());
        insert opportunity;
        
        //feb14: approving ORF
        if(this.oppRegForm.RecordTypeId == system.label.CLFEB14PRM04 )
            this.oppRegForm.put('Status__c', System.Label.CLJUN13PRM01); //Approved
        else    
            this.oppRegForm.put('Status__c', System.Label.CLFEB13PRM01); // CLFEB13PRM01 - 'In Process'
        
        this.oppRegForm.put('ConvertedIntoAnOpportunity__c', Boolean.valueOf(System.Label.CLSEP12PRM04)); //CLSEP12PRM04 - TRUE
        this.oppRegForm.put('Tech_CheckORFStatusOnSave__c', System.Label.CLSEP12PRM04);
        this.oppRegForm.put('Tech_LinkedOpportunity__c', opportunity.id);   // March 13 PRM chnage for VF email template
        update this.oppRegForm;
        
        
        // Added by Shruti Karn 
        //For October 2013 Release.BR-3555
        list<PartnerAssessment__c> lstORFAssessment =new list<PartnerAssessment__c>();
        list<OpportunityAssessmentResponse__c> lstAssessmentResponse = new list<OpportunityAssessmentResponse__c>();
        list<OpportunityAssessment__c> lstOppAssessment = new list<OpportunityAssessment__c>();
        map<Id,OpportunityAssessment__c> mapOppAssessment = new map<Id,OpportunityAssessment__c>();
        set<OpportunityAssessmentResponse__c> setResponsetoUpdate = new set<OpportunityAssessmentResponse__c>();
        lstORFAssessment = [Select id,assessment__c from PartnerAssessment__c where OpportunityRegistrationForm__c=:this.oppRegForm.Id limit 1000];
        lstAssessmentResponse = [Select id,OpportunityAssessment__c, PartnerAssessment__r.Assessment__c FROM OpportunityAssessmentResponse__c WHERE PartnerAssessment__c in:lstORFAssessment  limit 1000];
        
        /*for(OpportunityAssessmentResponse__c response : lstAssessmentResponse )
        {
            if(!mapOppResponse.containsKey(response.PartnerAssessment__r.Assessment__c))
                mapOppResponse.put(response.PartnerAssessment__r.Assessment__c , new list<OpportunityAssessmentResponse__c> {(response)});
            else
                mapOppResponse.get(response.PartnerAssessment__r.Assessment__c).add(response);
        }*/
        for(PartnerAssessment__c orfAssessment : lstORFAssessment )
        {
            OpportunityAssessment__c oppAssessment = new OpportunityAssessment__c();
            oppAssessment.Opportunity__c = opportunity.ID;
            oppAssessment.Assessment__c = orfAssessment.assessment__c;
            oppAssessment.ORFAssessment__c =  true;
            oppAssessment.PartnerAssessment__c = orfAssessment.Id;
            lstOppAssessment.add(oppAssessment);
        }
        if(!lstOppAssessment.isEmpty())
            insert lstOppAssessment;
        lstAssessmentResponse.clear();
        lstOppAssessment = [Select id,assessment__c from OpportunityAssessment__c where id in:lstOppAssessment limit 1000];
        for(OpportunityAssessment__c oppAssessment : lstOppAssessment)
        {
            mapOppAssessment.put(oppAssessment.assessment__c,oppAssessment);
        }
        for(OpportunityAssessmentResponse__c oppRes : lstAssessmentResponse)
        {
            if(mapOppAssessment.containsKey(oppRes.partnerassessment__r.assessment__c))
            {
                oppRes.OpportunityAssessment__c = mapOppAssessment.get(oppRes.partnerassessment__r.assessment__c).assessment__c;
            }
        }
        
        //lstAssessmentResponse.addAll(setResponsetoUpdate);
        update lstAssessmentResponse;
        
        Note[] notes = [Select id,Body ,Title from Note where ParentId = :this.oppRegForm.Id];
        List<Note> lstNotes = new List<Note>{};
        For(Note existNote: Notes ){
            Note newNote = new Note();
            newNote.Body = existNote.Body;
            newNote.Title = existNote.Title;
            newNote.ParentId = opportunity.Id;
            lstNotes.add(newNote);  
        }
        Insert lstNotes;

        Attachment[] Attachments = [Select id,Body,ContentType,Description,Name from Attachment where ParentId = :this.oppRegForm.Id];
        List<Attachment> lstAttachments = new List<Attachment>{};
        For(Attachment existAttachment: Attachments ){
            Attachment newAttachment = new Attachment();
            newAttachment.Body = existAttachment.Body;
            newAttachment.ContentType = existAttachment.ContentType;
            newAttachment.Description = existAttachment.Description;
            newAttachment.Name = existAttachment.Name;
            newAttachment.ParentId = opportunity.Id;
            lstAttachments.add(newAttachment);  
        }
        Insert lstAttachments;
        // Delete the old notes & Attachments
        // START: APR'14 Build review feedback
        // Delete notes;
        // Delete Attachments;
        // END: APR'14 build review feedback

         // Inserting ValueChainPlayer list
         // 1. ContactbFO added as Primary Customer Contact
         // 2. Partner Account added as Lead Finder and PartnerContact added as Primary Partner Contact
        
        system.debug('***** value chain player:'+this.oppRegForm.RecordTypeId);
        List <OPP_ValueChainPlayers__c> OppVCPlist = new List<OPP_ValueChainPlayers__c>();  
        if(this.oppRegForm.ContactbFO__c != null || this.oppRegForm.RecordTypeId == system.label.CLFEB14PRM04)
        {
            String AccountRole ;
            String ContactRole;
            system.debug('***** inside');
            //if(this.oppRegForm.TECH_ORFType__c == 'Project') //devFeb14
            if(this.oppRegForm.RecordTypeId == system.label.CLFEB14PRM04) //Feb14
                AccountRole = System.Label.CLFEB14PRM03;
                
            if(this.oppRegForm.ContactbFO__c != null) //devFeb14
                ContactRole = System.Label.CLSEP12PRM17;
            
            OPP_ValueChainPlayers__c OppVCP1 = new OPP_ValueChainPlayers__c(
                    OpportunityName__c = opportunity.id,
                    Account__c = this.oppRegForm.AccountbFO__c,
                    AccountRole__c = AccountRole,
                    Contact__c = this.oppRegForm.ContactbFO__c,
                    ContactRole__c = ContactRole
            );
                    
             OppVCPList.add(OppVCP1);
        }
        
      system.debug('?**ORF**?'+this.oppRegForm);
      if(this.oppRegForm.createdby.contactid != null)
      {
            OPP_ValueChainPlayers__c OppVCP2 = new OPP_ValueChainPlayers__c(
                    OpportunityName__c = opportunity.id,
                    Account__c = this.oppRegForm.TECH_PartnerAccountId__c,
                    AccountRole__c = System.Label.CLSEP12PRM15,
                    Contact__c = this.oppRegForm.TECH_PartnerContactId__c,
                    ContactRole__c = System.Label.CLSEP12PRM16,
                    LeadingPartner__c = Boolean.valueOf(System.Label.CLSEP12PRM04)
            ); 
            
            system.debug ('===OppVCP2==='+OppVCP2);
            OppVCPList.add(OppVCP2); 
        }
        if(!OppVCPList.isEmpty())
            insert OppVCPList;
            
        //START: Release APR14 
        //Adding Product Line
        if(!CreateOpportunityProductLine(opportunity.id,this.oppRegForm.CurrencyISOCode))
            return null;            
        //END: Relase APR14         
        
        
         // Added by Shruti Karn 
        //For October 2013 Release to resolve issue for INsufficient access
        opportunity = [Select id,ownerid from Opportunity where id =: opportunity.Id limit 1];
        opportunity.put('ownerid',oppRegForm.ownerid);
        //opportunity.put('LeadingBusiness__c',LeadingBU); //Release APR14;BR-4513
        byPassValidation = true;
        
        update opportunity; 
        
        
        
        
        // route it to newly created Opportunity.
        PageReference pr = new PageReference('/' + opportunity.id);              
        pr.setRedirect(true);         
        return pr;
 
    }
    //End of Opportunity Convertion
    
    //Change for BR-2910    Srinivas Nallapati
    Public User LoggedInUser = [SELECT Id,Name, PartnerORFConverter__c, Contact.AccountId, Contact.FirstName, Contact.LastName, Contact.Email FROM User u WHERE Id = :UserInfo.getUserId()];   
    
    
    public PageReference init(){
          
      try{
        //Srinivas Change 
        User OrfOwner = [select Name, ContactId, Contact.AccountId, Contact.Account.OwnerId from User where id= :this.oppRegForm.ownerId];
        if(OrfOwner.ContactId != null)
        {
          id PartnerAccountId = OrfOwner.Contact.AccountId;
            id PartnerAccountOwner = OrfOwner.Contact.Account.OwnerId;
            
            List<AccountTeamMember> lstAccTeamMember = new List<AccountTeamMember>();
            
            lstAccTeamMember = [Select UserId, AccountId, AccountAccessLevel From AccountTeamMember where AccountId= :PartnerAccountId];
            
            set<id> alloweduserids = new set<id>();
            alloweduserids.add(PartnerAccountOwner);
            //alloweduserids.add(this.oppRegForm.ownerId);
            
            for(AccountTeamMember atm: lstAccTeamMember)
            {
              alloweduserids.add(atm.UserId);
            }
            // Allow PartnerORFConverter users to edit ORFs   BR-2910
            //Apr13 PRM
            List<PermissionSetAssignment>  lsper= [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = :System.Label.CLMAY13PRM36];
            //May13 PRM change -  Allow Adminsto change Oppty owner to Partner User
            Boolean isAdminUser = false;
            String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;
            List<String> profilesToSKIP = (System.Label.CLMAY13PRM33).split(',');//the valid profile prefix list is populated
            for(String pName : profilesToSKIP)//The profile name should start with the prefix else the displayerrormessage flag is set to true
            {
                if(pName.trim() == profileName.trim())
                {
                    isAdminUser = true;             
                }  
            }
            //end Srinivas
            Boolean  PRMORFConverter = isAdminUser || (LoggedInUser.PartnerORFConverter__c  && (lsper != null && lsper.size() > 0) );

            if(!alloweduserids.contains(UserInfo.getUserId()) && PRMORFConverter == false)  //if user not allowed
            {
              ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLSEP12PRM25));
                return null;
            }
           
        }
        //End of srinivas change       
        
        
        // CLSEP12PRM07 - Approved, CLSEP12PRM08 - Rejected
        // Permit only if the Project Registration has submitted for approval
         //if(this.oppRegForm.TECH_ORFType__c = 'Project' && this.oppRegForm.Status__c != system.label.CLFEB14PRM02){
        if(this.oppRegForm.RecordTypeId == system.label.CLFEB14PRM04 && this.oppRegForm.Status__c != system.label.CLFEB14PRM02){ //feb14 release
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, system.label.CLFEB14PRM11));
            return null;
        }       
        
        if(this.oppRegForm.Status__c == system.label.CLSEP12PRM07 || this.oppRegForm.Status__c == system.label.CLSEP12PRM08 || this.oppRegForm.ConvertedIntoAnOpportunity__c == true){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, system.label.CLSEP12PRM03));
            return null;
        } 
           
         // CLSEP12PRM20 - Pending
       /*  if((this.oppRegForm.Status__c== system.label.CLSEP12PRM20 || this.oppRegForm.Status__c== system.label.CLFEB14PRM02)&& this.oppRegForm.TECH_IsORFValid__c=='false' ){
           ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, system.label.CLSEP12PRM02));
           return null;
        } 
        */
        
        if(this.oppRegForm.TECH_IsORFValid__c=='false')
        {
            if(this.oppRegForm.Status__c== system.label.CLSEP12PRM20)
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, system.label.CLSEP12PRM02));
                return null;
            
            }
            else if(this.oppRegForm.RecordTypeId == system.label.CLFEB14PRM04)
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, system.label.CLFEB14PRM16));
                return null;            
            }   
        
        }
        
        
        
        if (oppRegForm.TECH_IsContactInfoRequired__c=='true'&& skipContactCheck == False)
        {
             isConInfoReq=true;
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.WARNING, system.label.CLSEP12PRM27));
             return null;
        }
            
        
       if( this.oppRegForm.AccountbFO__c == null) {
            If(SkipAccountCheck == false){
             String url='';
                //url ='/apex/VFP02_AccountSearch?oppRegFormId='+ this.oppRegForm.id + '&Accname=' + this.oppRegForm.AccountName__c + '&AccCountry=' + this.oppRegForm.AccountCountry__c;
                //PageReference pageRef = new PageReference(url);
                PageReference pageRef = Page.VFP02_AccountSearch;
                pageRef.getParameters().put('oppRegFormId',  this.oppRegForm.id);
                
                //Feb14 Release, Modified for Project Registration form
                //If it is a Consumer Account, the address details should take from Project related fields
                
                String customerName;
                String City;
                                
                if(this.oppRegForm.EndUserType__c != null && this.oppRegForm.EndUserType__c == 'Consumer Account') 
                    customerName = this.oppRegForm.ContactFirstName__c+' '+this.oppRegForm.ContactLastName__c;
                    
                else
                    customerName = this.oppRegForm.AccountName__c;
                    
                
                pageRef.getParameters().put('Accname',  customerName);
               /* pageRef.getParameters().put('AccCountry',  this.oppRegForm.AccountCountry__c);
                
                pageRef.getParameters().put('accCity',  this.oppRegForm.AccountCity__c);
                pageRef.getParameters().put('accStreet', this.oppRegForm.AccountAddress__c);
                pageRef.getParameters().put('accZipCode', this.oppRegForm.AccountZipcode__c);*/
                
                pageRef.getParameters().put('AccCountry', this.oppRegForm.TECH_ORFConvertionCountry__c);              
                pageRef.getParameters().put('accCity',  this.oppRegForm.TECH_ORFConvertionCity__c);
                pageRef.getParameters().put('accStreet', this.oppRegForm.TECH_ORFConvertionAddress__c);
                pageRef.getParameters().put('accZipCode', this.oppRegForm.TECH_ORFConvertionZIPCode__c);
                
                
                pageRef.getParameters().put('fromPRM','true');
                
                pageRef.getParameters().put('saveURL', '/apex/VFP_ConvertORFToOpportunity?id=' + this.oppRegForm.id);

           //     PageRef.getParameters().put('PreviousPageURL','/apex/VFP02_AccountSearch?oppRegFormId='+ this.oppRegForm.id + '&Accname=' + (this.oppRegForm.AccountName__c!=null?EncodingUtil.urlEncode(this.oppRegForm.AccountName__c, 'UTF-8'):'') + '&AccCountry=' +(this.oppRegForm.AccountCountry__c!=null ? EncodingUtil.urlEncode(this.oppRegForm.AccountCountry__c, 'UTF-8'):'') );
                PageRef.getParameters().put('PreviousPageURL','/apex/VFP02_AccountSearch?oppRegFormId='+ this.oppRegForm.id + '&Accname=' + (customerName!=null?EncodingUtil.urlEncode(customerName, 'UTF-8'):'') + '&AccCountry=' +(this.oppRegForm.TECH_ORFConvertionCountry__c!=null ? EncodingUtil.urlEncode(this.oppRegForm.TECH_ORFConvertionCountry__c, 'UTF-8'):'') );
                pageRef.setRedirect(true);
                return pageRef;    
            
            }
            else If (SkipAccountCheck == True){
                system.debug('##skipAccountCheck'+ SkipAccountCheck);
                Sobject newAcc = new Account();                       
                
                if(this.oppRegForm.EndUserType__c != null && this.oppRegForm.EndUserType__c == 'Consumer Account')
                {
                      //  newAcc.put('Name', ''null);
                        newAcc.put('FirstName',this.oppRegForm.ContactFirstName__c);
                        newAcc.put('LastName',this.oppRegForm.ContactLastName__c);
                        newAcc.put('RecordTypeId',System.label.CLDEC12ACC01); //consumer account record type id 
                        newAcc.put('PersonEmail',this.oppRegForm.ContactEmail__c);
                        newAcc.put('WorkPhone__pc',this.oppRegForm.ContactPhone__c);             
                }
                else
                {
                    newAcc.put('Name',this.oppRegForm.AccountName__c);
                                    
                }
                
                    List<CS_OrfToAccount__c> ota= CS_OrfToAccount__c.getall().values();
                
                    for(CS_OrfToAccount__c custLabel : ota){
                        if (custLabel.Source__c != null) {
                            newAcc.put(custLabel.Target__c, this.oppRegForm.get(custLabel.source__c));
                        }
                        else if (custLabel.Defaults_Text__c != null) {
                            newAcc.put(custLabel.Target__c, custLabel.Defaults_Text__c);
                        }
                    }
                
                
              insert newAcc;     
              this.oppRegForm.AccountbFO__c= newAcc.Id;
              this.oppRegForm.ContactbFO__c= null;
              update  this.oppRegForm;
            }      
       }
       
             
        if (this.oppRegForm.AccountbFO__c != null) {
        
            if (this.oppRegForm.ContactbFO__c != null && (this.oppRegForm.ContactbFO__r.AccountId != this.oppRegForm.AccountbFO__r.Id ))
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, system.label.CLSEP12PRM21));
                return null;
            }
                    
            if ((this.oppRegForm.TECHOpportunityExists__c == false || this.oppRegForm.TECHOpportunityExists__c == null) && SkipOpportunityCheck == false) {
                //If (this.oppRegForm.AccountCountry__c!=null)
                    //oppCountry=this.oppRegForm.AccountCountry__c;
                    //else
                    //oppCountry = this.oppRegForm.ProjectCountry__c;
                oppCountry = this.oppRegForm.TECH_CountryOfDestination__c;
                //String url = '/apex/VFP03_OpptySearch?oppRegFormId=' + this.oppRegForm.id + '&oppName=' + this.oppRegForm.Name__c + '&oppCountry=' + oppCountry;
                //PageReference pageRef = new PageReference(url);
                PageReference pageRef = Page.VFP03_OpptySearch;
                pageRef.getParameters().put('saveURL', '/apex/VFP_ConvertORFToOpportunity?id=' + this.oppRegForm.id);
                pageRef.getParameters().put('oppRegFormId', this.oppRegForm.id);
                pageRef.getParameters().put('oppName', this.oppRegForm.Name__c);
                pageRef.getParameters().put('oppCountry', oppCountry);
                pageRef.getParameters().put('saveURL', '/apex/VFP_ConvertORFToOpportunity?id=' + this.oppRegForm.id);
                //system.debug('**PREVURL'+ ApexPages.CurrentPage().getparameters().get('PreviousPageURL') );
                If (ApexPages.CurrentPage().getparameters().get('PreviousPageURL')!=null)
                {
                PageRef.getParameters().put('PreviousPageURL',ApexPages.CurrentPage().getparameters().get('PreviousPageURL'));
                }
                else
                {
                PageRef.getParameters().put('PreviousPageURL', '/'+ this.oppRegForm.id);
                }
                pageRef.setRedirect(true);
                return pageRef ;
            }
            else if ((this.oppRegForm.TECHOpportunityExists__c == false || this.oppRegForm.TECHOpportunityExists__c == null) && SkipOpportunityCheck == true) {
                return convertORFToOpportunity();
            }
            else if (this.oppRegForm.TECHOpportunityExists__c == true) {
                system.debug('**Opp Exists1**');
                return null;
            }
        }
        system.debug('**Opp Exists2**');
        return null;
       }
      
        catch(System.DmlException  e){
           for (Integer i = 0; i < e.getNumDml(); i++) {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),''));
                System.debug(e.getDmlMessage(i)); 
            }
            return null;
        }       
       
    }
  
    public pagereference doGoBack(){
        PageReference pref=new PageReference('/'+this.oppRegForm.id);
        pref.setRedirect(true);
        return pref;
    }   
    
    public pagereference doContinue(){
       skipContactCheck = true;
       return init();
    }     
    
    //SART: Release APR14
    //BR-4513, Converting Opportunity Product Lines
    
    public String getLeadingBusinessUnit()
    {
    
        Map<String, CS_ProductLineBusinessUnit__c> prodLneBUs = CS_ProductLineBusinessUnit__c.getAll(); 
        List<OpportunityRegistrationProducts__c> lstORFProducts = [Select id,ProductName__c from OpportunityRegistrationProducts__c where ORFid__c = :this.oppRegForm.id];
        
        if(lstORFProducts.size() >0)
            LeadingBU = prodLneBUs.get(lstORFProducts[0].ProductName__c).BUCode__c;
            
        return LeadingBU;       
        
    }   
    
    public boolean CreateOpportunityProductLine(String oppID, String OppCurrency)
    {
      
        System.debug('**** Creating Opp Lines ****');
        List<OPP_ProductLine__c> prodLnes = new List<OPP_ProductLine__c>();
        Map<String, CS_ProductLineBusinessUnit__c> prodLneBUs = CS_ProductLineBusinessUnit__c.getAll(); 
        //Map<String, STring> prodLneBU = new Map<String, String>();
        //Map<String, STring> prodLnePL = new Map<String, String>();
        //Map<String ,Id> sysIdRef = new Map<String, Id>();
        
        //get Oppo Registration Product Lines
        
        //List<OpportunityRegistrationProducts__c> lstORFProducts = [Select id,ProductName__c,Quantity__c,Price__c from OpportunityRegistrationProducts__c where ORFid__c = :this.oppRegForm.id];
        List<OpportunityRegistrationProducts__c> lstORFProducts = [Select id,ProductName__c,Quantity__c,Price__c,TECH_ORFProductConvertionStatus__c
                                                                    from OpportunityRegistrationProducts__c where ORFid__c = :this.oppRegForm.id];
        
        System.debug('**** Opp Lines size****'+lstORFProducts.size());
        
        if(lstORFProducts.size() >0)
        {
            //Iterates through the Custom Settings and gets the Product BU and Product Line for the corresponding Opp Scope
            /*for(CS_ProductLineBusinessUnit__c pl: prodLneBUs.values())
            {
                for(OpportunityRegistrationProducts__c aORFLine: lstORFProducts)
                {
                    if(pl.ProductLine__c.startsWith(aORFLine.ProductName__c))        
                    {
                        System.debug('***** Opp Line mapping **** BU:'+pl.ProductBU__c + ' - Prod Line:'+pl.ProductLine__c+' - SysRef:'+pl.SystemIdReference__c);
                        prodLneBU.put(aORFLine.ProductName__c, pl.ProductBU__c);
                        //LeadingBU = pl.ProductBU__c;
                        prodLnePL.put(aORFLine.ProductName__c, pl.ProductLine__c);
                        sysIdRef.put(aORFLine.ProductName__c, pl.SystemIdReference__c);                    
                    }
                }
            } */
            //Creates Opportunity Lines populating Product BU and Product Line   
            
            for(OpportunityRegistrationProducts__c aORFLine: lstORFProducts)
            {
                System.debug('***** Creating Opp Line mapping ****');
                SObject prodLne = new OPP_ProductLine__c();
                
                List<CS_OrfLinesToOpportunityLines__c> oppline= CS_OrfLinesToOpportunityLines__c.getall().values();
                for(CS_OrfLinesToOpportunityLines__c custLabel : oppline)
                {
                    System.Debug('*** Source'+custLabel.Source__c);
                    if (custLabel.Source__c != null)                    
                        prodLne.put(custLabel.Target__c, aORFLine.get(custLabel.source__c));                   
                    else if (custLabel.Defaults_Text__c != null)
                        prodLne.put(custLabel.Target__c, custLabel.Defaults_Text__c);                  
                } 
                
                CS_ProductLineBusinessUnit__c tmpPL = prodLneBUs.get(aORFLine.ProductName__c); 
                prodLne.put('ProductBU__c', tmpPL.ProductBU__c );
                prodLne.put('ProductLine__c', tmpPL.ProductLine__c );
                prodLne.put('Product__c', tmpPL.SystemIdReference__c);
                prodLne.put('Opportunity__c',OppID);
                prodLne.put('CurrencyISOCode',OppCurrency);
                //prodLne.Amount__c = aORFLine.Price__c;       
                //prodLne.put('LineStatus__c',Label.CLSEP12PRM20);
                //prodLne.PartnerProductLine__c =aORFLine.ProductName__c; 
                //prodLne.Quantity__c = aORFLine.Quantity__c;
              
                System.debug('***** Opp Line mapping **** BU:'+tmpPL.ProductBU__c + ' - Prod Line:'+tmpPL.ProductLine__c+' - SysRef:'+tmpPL.SystemIdReference__c);
                prodLnes.add((OPP_ProductLine__c)prodLne);
                            
            }
            
            try
            {   //Inserts the Opportunity Line
                List<Database.SaveResult> prodLinesInsertResult = Database.insert(prodLnes, false);
                System.debug('***** - Product Line insert status ->'+prodLinesInsertResult.size());
                for (Database.SaveResult sr : prodLinesInsertResult)
                {
                    if(!sr.isSuccess())
                    {
                        for(Database.Error err : sr.getErrors())
                        {
                            System.debug('***** - Product Line: The following error has occurred.');                    
                            System.debug('***** - Product Line Error ->'+ err.getStatusCode() + ': ' + err.getMessage());
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error in Inserting Opportunity Line: '+err.getStatusCode()+' '+err.getMessage()));
                        }
                    }
                    else
                        system.debug('**** Product Line Insert Status - Success ****'+sr.getId());
                }
                
            }
            catch(System.DmlException  e)
            {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),''));
                    System.debug(e.getDmlMessage(i)); 
                }
                return false;
            }       
        
        
        } 
        
        
        return true;
    }
    //-----------------------------------------------------------
    //END: Release14
}