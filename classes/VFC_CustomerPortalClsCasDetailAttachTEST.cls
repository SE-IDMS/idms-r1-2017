@isTest
public class VFC_CustomerPortalClsCasDetailAttachTEST {

public static testMethod void VFC_CustomerPortalClsCaseDetailsAttach() {
 
    //Set up all conditions for testing.
    PageReference pageRef = Page.VFP_CustomerPortalClsCaseDetailsAttach;
    Test.setCurrentPage(pageRef);
        
    VFC_CustomerPortalClsCaseDetailsAttach controller = new VFC_CustomerPortalClsCaseDetailsAttach(); 
    PageReference nextPage = controller.getCaseDetailView();
    
    // Verify that page fails without parameters
       System.assertEquals(null, nextPage);         
        
    account a = new account(name = 'name');
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345');
//    Attachment attach

    case case1 = new case(ContactId=c.id, subject='case1', status='New');    
    insert(case1);
    CaseComment comt1 = new CaseComment(ParentId=case1.id, CommentBody='My Comment body1', IsPublished=true);
    CaseComment comt2 = new CaseComment(ParentId=case1.id, CommentBody='My Comment body2', IsPublished=false);
    insert(comt1); insert(comt2);
            
    
    case caseDetails1 = [SELECT CaseNumber, Subject, family__c, Operating_System__c, SoftwareVersion__c, Status, SupportCategory__c, CustomerRequest__c, AnswerToCustomer__c, IsClosed FROM Case WHERE  Id =: Case1.Id]; 

    List<caseComment> caseComments1 = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC];
    List<Attachment> attachments1 = [SELECT Id, CreatedById, Name, CreatedDate, BodyLength, ContentType, Description, IsPrivate, LastModifiedDate FROM Attachment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC];

    casecomment cc = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC][0]; 
    List<caseComment> caseCommentsOK = new List<caseComment>();
    caseCommentsOK.add(cc);

    
    // Testing Begin Here    
    Test.starttest(); 

    controller.attach.name = 'test';
        controller.attach.description = 'description';
    
    // Evaluate when CaseId is not Null and CaseComment is not Null
    controller.setCaseId(case1.id);
//    Controller.setCommentToAdd('My Comment body3'); 
//    controller.saveComment();
//    System.assertEquals(controller.getCommentToAdd(), 'My Comment body3');
//    System.assertEquals(controller.getCaseId(), case1.id);
    caseCommentsOK = controller.getCaseComments();
    controller.getCaseDetail();
    controller.saveAttachment();
    
    // Evaluate when CaseId is Null and CaseComment is Null
    controller.setCaseId(null);
    /*Controller.setCommentToAdd(''); 
    controller.saveComment();
    System.assertEquals(controller.getCommentToAdd(), '');*/
    System.assertEquals(controller.getCaseId(), null);

    
    Test.stoptest(); 
    
}


}