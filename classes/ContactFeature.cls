global class ContactFeature {
    WebService string SCONT_PS { get; set; }
    
    WebService string SCONT_ID { get; set; }
    
    WebService string DML_ACTION { get; set; }

    WebService string FEATURE_CAT_PS { get; set; }
    
    WebService string FEATURE_CAT_ID { get; set; }

    WebService string IS_ACTIVE { get; set; }

    WebService DateTime SDH_VERSION { get; set; }

    public ContactFeature () {
        SCONT_PS = 'BFO';
        FEATURE_CAT_PS = 'BFO';
        DML_ACTION = 'UPSERT';
    }
}