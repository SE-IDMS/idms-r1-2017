public class VFC_MassDeletePlatformingScoring {
    public SFE_PlatformingScoring__c scoring{get; set;}
    public String CapId {get; set;}  
    public String first_picklist_option = 'No Filter';
    public Account sourceAccount {get;set;}
    public string accCity{get;set;}
    public string accMasterProfile{get;set;}
    public string ClassificationLvl1{get;set;}
    public List<wrapPlatformingScoring> platformingScoringList{get;set;}{platformingScoringList = new List<wrapPlatformingScoring>();}
    public List<SFE_PlatformingScoring__c> selectedPlatformingScoring = new List<SFE_PlatformingScoring__c>();
    public Boolean searchButtonClicked{get;set;}
    public Boolean warningMessage{get;set;}
    public Long numberOfRecord{get;set;}
    public SFE_IndivCAP__c cap= new SFE_IndivCAP__c();
     public Boolean allChecked { get; set; }
 
    public VFC_MassDeletePlatformingScoring(ApexPages.StandardController controller) {
      SFE_PlatformingScoring__c scoring = new SFE_PlatformingScoring__c();
      sourceAccount = new Account();
      CapId=ApexPages.currentPage().getParameters().get('Id');
      numberOfRecord=0;
      searchButtonClicked=False;
      warningMessage=False;
      ClassificationLvl1='No Filter';
      searchRecords();
    }    
    public void searchRecords(){
        searchButtonClicked=True; 
         allChecked=False;      
        String strSearch = 'select Id, Name,PlatformedAccount__c,PlatformedAccount__r.Name,AccountLocalName__c,AccountOwner__c,AccountMasterProfile__c,CustomerProfile__c,City__c,Street__c,ClassificationLvl1__c from SFE_PlatformingScoring__c Where IndivCAP__c=:CapId';
        String whereClause='';
        if(accCity!=null && accCity!='')
            whereClause += ' AND City__c LIKE \'%'+Utils_Methods.escapeForWhere(accCity)+'%\'';
         if(accMasterProfile!=null && accMasterProfile!='')
            whereClause += ' AND AccountMasterProfile__c LIKE \'%'+Utils_Methods.escapeForWhere(accMasterProfile)+'%\'';
        if(ClassificationLvl1!='No Filter')
            whereClause += ' AND ClassificationLvl1__c =\''+Utils_Methods.escapeForWhere(ClassificationLvl1)+'\'';
            if(whereClause.length()>0){
                strSearch +=' '+whereClause+' ';
            } 
        strSearch += ' ORDER BY Name';
        strSearch += ' LIMIT 1000';
        try {
        platformingScoringList.clear(); 
        numberOfRecord=0;
        warningMessage=False;
        for(SFE_PlatformingScoring__c a : Database.query(strSearch))
        {
           numberOfRecord++;
           platformingScoringList.add(new wrapPlatformingScoring(a));
        }
           if(numberOfRecord==1000)
                 warningMessage=True;
            if(Test.isRunningTest())
                throw new TestException();
       }
      catch(Exception ex){
            ApexPages.addMessages(ex);
      }
      System.debug('strSearch >>>>'+strSearch);
    }
    
    public PageReference clearSearch(){
        PageReference curPage = new PageReference('/apex/VFP_MassDeletePlatformingScoring');
        curPage.getParameters().put('id',CapId);
        curPage.setRedirect(true);
        return curPage;
    }

    public void massDeletion() {
     try{ 
        selectedPlatformingScoring.clear();
        for(wrapPlatformingScoring pswrapper : platformingScoringList){
        if(pswrapper.selected == true)
            selectedPlatformingScoring.add(pswrapper.rec);
        }

        if(capID!=null)
        {
            cap=[select AssignedTo__c,ownerID,Status__c from SFE_IndivCAP__c where id =:capId];            
        }
        if(selectedPlatformingScoring.size()>0)
        {   
            if(!cap.status__c.equalsIgnoreCase(Label.CL00444))
            {           
                 if((UserInfo.getUserId()!=cap.ownerID)&&(UserInfo.getUserId()!=cap.AssignedTo__c) &&(UserInfo.getProfileId()!=Label.CL00110) &&(UserInfo.getProfileId()!=Label.CLOCT13SLS24)&&(UserInfo.getProfileId()!=Label.CLOCT13SLS25))
                {                
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00440)); 

                } 
                else
                {          
                    if(selectedPlatformingScoring.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                    {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00264)); 
                    }
                    else
                    {                     
                        Database.deleteResult[] MassDelete = Database.delete(selectedPlatformingScoring); 
                        for(Database.deleteResult dr: MassDelete )
                        {

                            if(!dr.isSuccess())
                            {
                                Database.Error err = dr.getErrors()[0];                    
                            }
                    
                        }
                    }
                    platformingScoringList.clear();
                    selectedPlatformingScoring.clear();
                    searchRecords();
                    allChecked=False;                      
                }      
            }  
            else
           {            
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00441)); 
            }     
        }        
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00442));
        }
       }
       catch(Exception ex)
      {
      
      } 
    }
     public List<selectOption> getPickValues(Sobject object_name, String field_name, String first_val) {
      List<selectOption> options = new List<selectOption>(); 
      if (first_val != null) { 
         options.add(new selectOption(first_val, first_val)); //add the first option
      }
      Schema.sObjectType sobject_type = object_name.getSObjectType();
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
      List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); 
      for (Schema.PicklistEntry a : pick_list_values) { 
         options.add(new selectOption(a.getLabel(),a.getLabel()));    
        }
              return options;
   }
   public List<selectOption> getAccountClassification() {
      return getPickValues(sourceAccount,'ClassLevel1__c', first_picklist_option);
   }
    public class wrapPlatformingScoring {
        public SFE_PlatformingScoring__c rec {get; set;}
        public Boolean selected {get; set;}
        public wrapPlatformingScoring(SFE_PlatformingScoring__c ps) {
            rec = ps;
            selected = false;
        }
    }
public class TestException extends Exception {}  
}