@istest
private class VCC14_I2PRR_RIEmailTemplate_TEST
{
    private static testMethod void testVCC14_I2PRR_RIEmailTemplate() { 
    
    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    
    Insert CaseObj;
        
    BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg;  
    
    BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST2',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity2',
                                            Location__c='RC Location2',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street2',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City2',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg1;  
    
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c='xbtg5230',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT;
    RMAShippingToAdmin__c RPDT1 = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',                                
                                 Ruleactive__c = True,
                                 ReturnCenter__c = RCorg1.Id
                                 );
    Insert RPDT1;
    
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC;                                         
    
    RMA__c RR = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street',
                Status__c='open'
                );
    Insert RR;
    
    Test.SetCurrentPageReference(New PageReference('Page.VCC14_I2PRR_RIEmailTemplate'));
    ApexPages.Standardcontroller SC = New ApexPages.StandardController(RR);
    VCC14_I2PRR_RIEmailTemplate CRI = new VCC14_I2PRR_RIEmailTemplate();
    CRI.getRMAProduct();
 }
 
 }