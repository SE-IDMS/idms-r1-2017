public virtual class TriggerBase implements Utils_TriggerManager {
    protected boolean isBefore;
    protected boolean isAfter;
    protected boolean isUpdate;
    protected boolean isInsert;
    protected boolean isDelete;
    
    public TriggerBase(boolean isBefore, boolean isAfter, boolean isUpdate, boolean isInsert, boolean isDelete) {
                            
        this.isBefore = isBefore;
        this.isAfter = isAfter;
        this.isUpdate = isUpdate;
        this.isInsert = isInsert;
        this.isDelete = isDelete;
    }
    
    public TriggerBase() {
                                   
    }
    

    /**
     * execute - responsible for launching execution of business logic.
     *
     */
    public virtual void execute () {
        if (this.isBefore) {
            if (this.isInsert) {
                this.onBeforeInsert();
            } else if (this.isUpdate) {
                this.onBeforeUpdate();
            } else if (this.isDelete) {
                this.onBeforeDelete();
            }
        } else if (this.isAfter) {
            if (this.isInsert) {
                this.onAfterInsert();
            } else if (this.isUpdate) {
                this.onAfterUpdate();
            } else if (this.isDelete) {
                this.onAfterDelete();
            }
        }
    }


    /**
     * onBeforeInsert
     * Has access to newMap.
     */
    public virtual void onBeforeInsert() {}
    
    /**
     * onBeforeUpdate
     * Has access to oldMap and newMap.
     */
    public virtual void onBeforeUpdate() {}

    /**
     * onBeforeDelete
     * Has access to oldMap.
     */
    public virtual void onBeforeDelete() {}

    /**
     * onAfterInsert
     *
     * Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     * Has access to newMap.
     */
    public virtual void onAfterInsert() {}

    /**
     * onAfterUpdate
     * Has access to oldMap and newMap.
     */
    public virtual void onAfterUpdate() {}

    /**
     * onAfterDelete
     * Has access to oldMap.
     */
    public virtual void onAfterDelete() {}

}