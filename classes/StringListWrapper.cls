/*
Author:Siddharth N(GD Solutions)
Purpose:A wrapper with List<List<String>> type to exchange data between components and visualforce pages,used by c_csvParser component.
*/
public with sharing class StringListWrapper {
    public List<List<String>> stringList{get;set;}
    public StringListWrapper() {
        stringList=new List<List<String>>();
    }
}