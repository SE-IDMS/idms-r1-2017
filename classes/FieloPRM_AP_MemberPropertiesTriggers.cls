/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

public without sharing class FieloPRM_AP_MemberPropertiesTriggers {

    public static Boolean isRunning = false;
    
    private static final String PRM_REWARD_FEATURE_CODE = 'PRM_Rewards';
    
    public static void searchMember(){
        List<MemberExternalProperties__c> triggerNew = trigger.new;

        set<String> setUIMSsMember = new set<String>();
        for(MemberExternalProperties__c memberProperties : triggerNew){
            if(memberProperties.Member__c == null){
                setUIMSsMember.add(memberProperties.PRMUIMSId__c);
            }
        }
        
        if(!setUIMSsMember.isEmpty()){
            map<string,FieloEE__Member__c> mapUIMSMember = new map<string,FieloEE__Member__c>();
            for(FieloEE__Member__c member:  [SELECT id, PRMUIMSId__c FROM FieloEE__Member__c WHERE PRMUIMSId__c IN:  setUIMSsMember]){
                mapUIMSMember.put(member.PRMUIMSId__c, member);
            }

            for(MemberExternalProperties__c memberProperties : triggerNew){
                if(memberProperties.Member__c == null && mapUIMSMember.containskey(memberProperties.PRMUIMSId__c)){
                    memberProperties.Member__c = mapUIMSMember.get(memberProperties.PRMUIMSId__c).id;
                }
            }
        }
    }
    
    public static void checkMemberPropertiesStatus(List<MemberExternalProperties__c> triggerNew){
        CS_PRM_ApexJobSettings__c config = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_MemberPropProcess');
        List<MemberExternalProperties__c> toUpdate = new List<MemberExternalProperties__c>();
        for(MemberExternalProperties__c memberProp : triggerNew){
            MemberExternalProperties__c memberProperties = memberProp;
            if(memberProperties.F_PRM_Status__c != 'PROCESSED'){
                if(memberProperties.F_PRM_BadgeOK__c && memberProperties.F_PRM_EventsOK__c && memberProperties.F_PRM_TransactionOK__c){
                    memberProperties.F_PRM_Status__c = 'PROCESSED';
                }else if(memberProperties.F_PRM_TryCount__c >= Integer.valueOf(config.F_PRM_TryCount__c)){
                    memberProperties.F_PRM_Status__c = 'FAILED';
                }else if(memberProperties.DeletionFlag__c){
                    memberProperties.F_PRM_Status__c = 'TODELETE';
                }else if(memberProperties.Member__c != null){
                    memberProperties.F_PRM_Status__c = 'TOPROCESS';
                }else{
                    memberProperties.F_PRM_Status__c = null;
                }
            }else{
                memberProperties.F_PRM_BadgeOK__c = true;
                memberProperties.F_PRM_TransactionOK__c = true;
                memberProperties.F_PRM_EventsOK__c = true;
            }
            toUpdate.add(memberProperties);
        }

        try{
            isRunning = true;
            update toUpdate;
        }catch(Exception e){}
        isRunning = false;
    }
    
    public static void createBadgeMember(List<MemberExternalProperties__c> meps){
        Map<Id, MemberExternalProperties__c> mepById = new Map<Id, MemberExternalProperties__c>();
        List<Id> externalPropertiesIdList = new List<Id>();
        Set<Id> memberIdSet = new Set<Id>();
        for(MemberExternalProperties__c memberProperties : meps){
            if (memberProperties.DeletionFlag__c) continue;
            externalPropertiesIdList.add(memberProperties.externalPropertiesCatalog__c);
            memberIdSet.add(memberProperties.Member__c);
            if(Trigger.isExecuting && Trigger.isBefore){
                mepById.put(memberProperties.Id, memberProperties);
            }else{
                mepById.put(memberProperties.Id, new MemberExternalProperties__c(Id = memberProperties.Id, DeletionFlag__c = memberProperties.DeletionFlag__c, externalPropertiesCatalog__c = memberProperties.externalPropertiesCatalog__c, Member__c = memberProperties.Member__c, F_PRM_BadgeOK__c = memberProperties.F_PRM_BadgeOK__c, F_PRM_TransactionOK__c = memberProperties.F_PRM_TransactionOK__c, F_PRM_EventsOK__c = memberProperties.F_PRM_EventsOK__c, F_PRM_TryCount__c = memberProperties.F_PRM_TryCount__c));
            }
        }

        Set<Id> memberHasRewardsFeatureSet = new Set<Id>();
        for (FieloPRM_MemberFeature__c mf:[SELECT Id, F_PRM_Member__c, F_PRM_Feature__c FROM FieloPRM_MemberFeature__c WHERE F_PRM_IsActive__c=true AND F_PRM_Feature__r.F_PRM_FeatureCode__c=:PRM_REWARD_FEATURE_CODE AND F_PRM_Member__c in :memberIdSet]) {
            memberHasRewardsFeatureSet.add(mf.F_PRM_Member__c);
        }
        Map<Id,ExternalPropertiesCatalog__c> mapExternalPropertiesCatalog = new Map<Id,ExternalPropertiesCatalog__c>([select Id,Type__c,SubType__c,Badge__c,TransactionCatalog__c, TransactionCatalog__r.Type__c, TransactionCatalog__r.Approved__c,TransactionCatalog__r.ExternalValue__c,TransactionCatalog__r.PurchaseValue__c, TransactionCatalog__r.Date__c,(SELECT PRMExternalPropertyEvent__c.Id, PRMExternalPropertyEvent__c.EventCatalog__c, PRMExternalPropertyEvent__c.EventCatalog__r.Id, PRMExternalPropertyEvent__c.EventCatalog__r.Name, PRMExternalPropertyEvent__c.EventCatalog__r.F_PRM_isActive__c, PRMExternalPropertyEvent__c.EventCatalog__r.F_PRM_Type__c, PRMExternalPropertyEvent__c.EventCatalog__r.F_PRM_Value__c, PRMExternalPropertyEvent__c.EventCatalog__r.F_PRM_ValueText__c, PRMExternalPropertyEvent__c.EventCatalog__r.F_PRM_Points__c FROM ExternalPropertiesCatalog__c.External_Propertiy_Events__r WHERE PRMExternalPropertyEvent__c.EventCatalog__r.F_PRM_isActive__c=true) from ExternalPropertiesCatalog__c where Id in: externalPropertiesIdList and Active__c=true]);

        List<FieloEE__Event__c> actualEvents = [SELECT Id, F_PRM_MemberExternalProperties__c, F_PRM_ExternalPropertyEvent__c FROM FieloEE__Event__c WHERE F_PRM_MemberExternalProperties__c IN :mepById.keySet()];

        List<FieloEE__BadgeMember__c> listBadgeMemberToInsert = new List<FieloEE__BadgeMember__c>();
        List<FieloEE__Event__c> listEventsToInsert = new List<FieloEE__Event__c>();
        List<FieloEE__Transaction__c> listTransactionToInsert = new List<FieloEE__Transaction__c>();

        for(MemberExternalProperties__c memberProperties : mepById.values()){
            if(!memberProperties.DeletionFlag__c && memberProperties.Member__c != null && mapExternalPropertiesCatalog.containsKey(memberProperties.externalPropertiesCatalog__c)){
                ExternalPropertiesCatalog__c externalPropertiesCatalog = mapExternalPropertiesCatalog.get(memberProperties.externalPropertiesCatalog__c);
                if(externalPropertiesCatalog.Badge__c!=null && !memberProperties.F_PRM_BadgeOK__c) {
                    FieloEE__BadgeMember__c newBadgeMember = new FieloEE__BadgeMember__c(
                        FieloEE__Member2__c = memberProperties.Member__c, 
                        FieloEE__Badge2__c = externalPropertiesCatalog.Badge__c,
                        F_PRM_MemberExternalProperties__c = memberProperties.Id
                    );
                    listBadgeMemberToInsert.add(newBadgeMember); 
                }else{
                    memberProperties.F_PRM_BadgeOK__c = true;
                }
                if(externalPropertiesCatalog.External_Propertiy_Events__r != null && externalPropertiesCatalog.External_Propertiy_Events__r.size() > 0  && !memberProperties.F_PRM_EventsOK__c) {
                    for (PRMExternalPropertyEvent__c epe : externalPropertiesCatalog.External_Propertiy_Events__r) {
                        boolean found = false;
                        for(FieloEE__Event__c event: actualEvents){
                            if(event.F_PRM_ExternalPropertyEvent__c == epe.Id && event.F_PRM_MemberExternalProperties__c == memberProperties.Id){
                                found = true;
                                break;
                            }
                        }
                        if(!found){
                            FieloEE__Event__c newEvent = new FieloEE__Event__c(
                                FieloEE__Type__c = epe.EventCatalog__r.F_PRM_Type__c,
                                FieloEE__Member__c = memberProperties.Member__c,
                                Name = epe.EventCatalog__r.Name,
                                FieloEE__Value__c = epe.EventCatalog__r.F_PRM_Value__c,
                                F_PRM_ValueText__c = epe.EventCatalog__r.F_PRM_ValueText__c,
                                F_PRM_Points__c = epe.EventCatalog__r.F_PRM_Points__c,
                                F_PRM_MemberExternalProperties__c = memberProperties.Id
                            );
                            listEventsToInsert.add(newEvent); 
                        }
                    }
                     
                }else{
                    memberProperties.F_PRM_EventsOK__c = true;
                }
                if (externalPropertiesCatalog.TransactionCatalog__c != null && memberHasRewardsFeatureSet.contains(memberProperties.Member__c)  && !memberProperties.F_PRM_TransactionOK__c) {
                    FieloEE__Transaction__c newTransaction = new FieloEE__Transaction__c(
                        FieloEE__Member__c = memberProperties.Member__c,
                        FieloEE__Type__c = externalPropertiesCatalog.TransactionCatalog__r.Type__c,
                        FieloEE__Approved__c = externalPropertiesCatalog.TransactionCatalog__r.Approved__c,
                        F_ExternalValue__c = externalPropertiesCatalog.TransactionCatalog__r.ExternalValue__c,
                        FieloEE__Date__c  = Date.today(),
                        FieloEE__Value__c = 0,
                        F_PRM_MemberExternalProperties__c = memberProperties.Id
                    );
                    listTransactionToInsert.add(newTransaction);
                }else{
                    memberProperties.F_PRM_TransactionOK__c = true;
                }
            }
        }
        List<Database.SaveResult> badgeResList = Database.insert(listBadgeMemberToInsert, false);
        List<Database.SaveResult> eventResList = Database.insert(listEventsToInsert, false);
        List<Database.SaveResult> transResList = Database.insert(listTransactionToInsert, false);
        List<FieloEE__ErrorLog__c> logs = new List<FieloEE__ErrorLog__c>();

        if(eventResList.size() > 0){
            for(Integer i = 0; i < eventResList.size(); i++){
                Database.SaveResult sr = eventResList[i];
                MemberExternalProperties__c mep = mepById.get(listEventsToInsert.get(i).F_PRM_MemberExternalProperties__c);
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        logs.add(new FieloEE__ErrorLog__c(FieloEE__Message__c = err.getMessage(),
                                    FieloEE__StackTrace__c = 'Status code:' + err.getStatusCode(), 
                                    FieloEE__Type__c = 'Insert Event error', 
                                    FieloEE__UserId__c = UserInfo.getUserId(),
                                    F_PRM_MemberExternalProperties__c = mep.Id));
                        if(mep.F_PRM_TryCount__c == null){
                            mep.F_PRM_TryCount__c = 1;
                        }else{
                            mep.F_PRM_TryCount__c++;
                        }
                    }
                }
            }
        }
        for(MemberExternalProperties__c mep: mepById.values()){
            boolean found = false;
            for(FieloEE__ErrorLog__c log: logs){
                if(log.F_PRM_MemberExternalProperties__c == mep.Id){
                    found = true;
                    break;
                }
            }
            if(!found){
                mep.F_PRM_EventsOK__c = true;
            }
        }
        if(badgeResList.size() > 0){
            for(Integer i = 0; i < badgeResList.size(); i++){
                Database.SaveResult sr = badgeResList[i];
                MemberExternalProperties__c mep = mepById.get(listBadgeMemberToInsert.get(i).F_PRM_MemberExternalProperties__c);
                System.debug('RAHL mep: ' + mep.Id);
                System.debug('RAHL isSuccess: ' + sr.isSuccess());
                if(sr.isSuccess()){
                    mep.F_PRM_BadgeOK__c = true;
                }else{
                    for(Database.Error err : sr.getErrors()) {
                        logs.add(new FieloEE__ErrorLog__c(FieloEE__Message__c = err.getMessage(),
                                    FieloEE__StackTrace__c = 'Status code:' + err.getStatusCode(), 
                                    FieloEE__Type__c = 'Insert BadgeMember error', 
                                    FieloEE__UserId__c = UserInfo.getUserId(),
                                    F_PRM_MemberExternalProperties__c = mep.Id));
                        if(mep.F_PRM_TryCount__c == null){
                            mep.F_PRM_TryCount__c = 1;
                        }else{
                            mep.F_PRM_TryCount__c++;
                        }
                    }
                }
            }
        }
        if(transResList.size() > 0){
            for(Integer i = 0; i < transResList.size(); i++){
                Database.SaveResult sr = transResList[i];
                MemberExternalProperties__c mep = mepById.get(listTransactionToInsert.get(i).F_PRM_MemberExternalProperties__c);
                if(sr.isSuccess()){
                    mep.F_PRM_TransactionOK__c = true;
                }else{
                    for(Database.Error err : sr.getErrors()) {
                        logs.add(new FieloEE__ErrorLog__c(FieloEE__Message__c = err.getMessage(),
                                    FieloEE__StackTrace__c = 'Status code:' + err.getStatusCode(), 
                                    FieloEE__Type__c = 'Insert Transaction error', 
                                    FieloEE__UserId__c = UserInfo.getUserId(),
                                    F_PRM_MemberExternalProperties__c = mep.Id));
                        if(mep.F_PRM_TryCount__c == null){
                            mep.F_PRM_TryCount__c = 1;
                        }else{
                            mep.F_PRM_TryCount__c++;
                        }
                    }
                }
            }
        }
        System.debug('logs: ' + logs.size());
        insert logs;

        try{
            update mepById.values();
        }catch(Exception e){
            isRunning = true;
            checkMemberPropertiesStatus(mepById.values());
            isRunning = false;
        }
    }

    public static void deleteBadgeMember(List<MemberExternalProperties__c> removeList){
        if(Trigger.isExecuting && Trigger.isInsert){
            return;
        }
        List<MemberExternalProperties__c> triggerOld = removeList;
        List<Id> externalPropertiesIdList = new List<Id>();
        for(MemberExternalProperties__c memberProperties : triggerOld){
            externalPropertiesIdList.add(memberProperties.externalPropertiesCatalog__c);
        }

        Map<Id,ExternalPropertiesCatalog__c> mapExternalPropertiesCatalog = new Map<Id,ExternalPropertiesCatalog__c>([select Id,Type__c,SubType__c,Badge__c from ExternalPropertiesCatalog__c where Id in: externalPropertiesIdList]);
        
        Set<Id> setIdsMember = new Set<Id>();
        Set<Id> setIdsBadges = new Set<Id>();

        for(MemberExternalProperties__c memberProperties : triggerOld){
            if(memberProperties.Member__c != null && mapExternalPropertiesCatalog.containsKey(memberProperties.externalPropertiesCatalog__c)){
                ExternalPropertiesCatalog__c externalPropertiesCatalog = mapExternalPropertiesCatalog.get(memberProperties.externalPropertiesCatalog__c);
                setIdsMember.add(memberProperties.Member__c);
                if(externalPropertiesCatalog.Badge__c!=null) {
                    setIdsBadges.add(externalPropertiesCatalog.Badge__c); 
                }
            }
        }
        List<FieloEE__BadgeMember__c> listBadgeMember = [SELECT id, FieloEE__Badge2__c, FieloEE__Member2__c FROM FieloEE__BadgeMember__c WHERE FieloEE__Badge2__c in : setIdsBadges AND FieloEE__Member2__c in: setIdsMember];
        Map<Id,Map<Id,FieloEE__BadgeMember__c>> mapMemberBadges = new Map<Id,Map<Id,FieloEE__BadgeMember__c>>();
        for(FieloEE__BadgeMember__c badgeMember:  listBadgeMember){
            Map<Id,FieloEE__BadgeMember__c> mapBadgememberAux = new Map<Id,FieloEE__BadgeMember__c>();
            if(mapMemberBadges.containskey(badgeMember.FieloEE__Member2__c)){
                mapBadgememberAux = mapMemberBadges.get(badgeMember.FieloEE__Member2__c);
            }
            mapBadgememberAux.put(badgeMember.FieloEE__Badge2__c,badgeMember);
            mapMemberBadges.put(badgeMember.FieloEE__Member2__c,mapBadgememberAux);
        }
        Set<FieloEE__BadgeMember__c> listBadgeMemberToDelete = new Set<FieloEE__BadgeMember__c>();
        for(MemberExternalProperties__c memberProperties : triggerOld){
            ExternalPropertiesCatalog__c externalPropertiesCatalog = mapExternalPropertiesCatalog.get(memberProperties.externalPropertiesCatalog__c);
            if(mapMemberBadges.containskey(memberProperties.Member__c) && mapMemberBadges.get(memberProperties.Member__c).containsKey(externalPropertiesCatalog.Badge__c)){
                listBadgeMemberToDelete.add(mapMemberBadges.get(memberProperties.Member__c).get(externalPropertiesCatalog.Badge__c));
            }
        }
        if(listBadgeMemberToDelete.size()>0){
            Database.delete(new List<FieloEE__BadgeMember__c>(listBadgeMemberToDelete));
        }

        try{
            for(MemberExternalProperties__c mep: removeList){
                if(mep.F_PRM_Status__c == 'TODELETE'){
                    mep.F_PRM_Status__c = 'PROCESSED';
                    mep.F_PRM_BadgeOK__c = true;
                    mep.F_PRM_TransactionOK__c = true;
                    mep.F_PRM_EventsOK__c = true;
                }
            }
            update removeList;
        }catch(Exception e){/*AFTER DELETE*/}
    }
    
}