// test class for UIMSCompanyAfterUpdateBeforeInsert trigger
@isTest
public class UIMSCompanyAfterUpdateBeforeInsertTest{
    //test method for success case
    static testmethod void testMethodOne(){
        UIMS_Company__c uimsCompany = new UIMS_Company__c(); 
        insert uimsCompany ; 
        
        UIMS_Company__c uimsSelCompany = [select Vnew__c from UIMS_Company__c  where id=: uimsCompany.id];
        Test.startTest();
        uimsSelCompany.Vnew__c = '2';
        update uimsSelCompany ; 
        Test.stopTest();
        
        
    }
}