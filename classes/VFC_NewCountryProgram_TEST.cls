/**
16-April-2013
Shruti Karn  
PRM May13 Release    
Initial Creation: test class for VFC_NewCountryProgram
 */
@isTest
private class VFC_NewCountryProgram_TEST {
  
     @testSetup static void testSetupMethodPRM(){
        
        List<Country__c> lstCountries = new List<Country__c>();
        Country__c country = Utils_TestMethods.createCountry();
        lstCountries.add(country);
        
        Country__c country11 = Utils_TestMethods.createCountry();
        country11.CountryCode__c='US';
        lstCountries.add(country11);
        insert lstCountries;
     
     
     } 
     
     static testMethod void myUnitTest() {
        
        Country__c country   = [SELECT Id,countrycode__c FROM Country__c WHERE Name = 'TestCountry' AND CountryCode__c = 'FR'];
        Country__c country11 = [SELECT Id,countrycode__c FROM Country__c WHERE Name = 'TestCountry' AND CountryCode__c = 'US'];
        
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO'];
        
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        thisUser.BypassVR__c = true;
        thisUser.BypassWF__c = true;
         
        

        User u2;
        /*System.runAs (thisUser) {
            /*u2 = Utils_TestMethods.createStandardUserWithNoCountry('PPADM2');
            u2.BypassVR__c = true;
            u2.BypassWF__c = true;
            u2.ProgramAdministrator__c=true;
            u2.country__c = country11.countrycode__c;
            u2.userroleid = r.Id;
            insert u2;
        }*/
        
        PartnerPRogram__c globalProgram;
        VFC_NewCountryProgram myPageCon1;
        Recordtype levelRecordType = [Select id from RecordType where developername =  'GlobalProgramLevel' limit 1];
        Recordtype featureRecordType = [Select id from RecordType where developername =  'GlobalFeature' limit 1];
        
        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype programActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ProgramRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        
        Update thisUser;
        
        Test.StartTest();
        
        System.RunAs(thisUser){

            ClassificationLevelCatalog__c lvl1 = new ClassificationLevelCatalog__c();
            lvl1.ClassificationLevelName__c = 'Test Class Level1';
            lvl1.Name='TEST1';
            lvl1.Active__c=true;
            insert lvl1;
    
            MarketSegmentCatalog__c sgmt = new MarketSegmentCatalog__c();
            sgmt.MarketSegmentName__c = 'Test Market Segment';
            sgmt.Name='SEG1';
            sgmt.Active__c=true;
            insert sgmt;
    
            DomainsOfExpertiseCatalog__c pdoe = new DomainsOfExpertiseCatalog__c();
            pdoe.Name='PDOE';
            pdoe.DomainsOfExpertiseName__c='Parent DOE';
            pdoe.Active__c=true;
            insert pdoe;
    
            DomainsOfExpertiseCatalog__c cdoe = new DomainsOfExpertiseCatalog__c();
            cdoe.Name='CDOE';
            cdoe.DomainsOfExpertiseName__c='Child DOE';
            cdoe.Active__c=true;
            cdoe.ParentDomainsOfExpertise__c=pdoe.Id;
            insert cdoe;
    
            ProductLineCatalog__c newProd1 = new ProductLineCatalog__c(Name='IT3EST1',ProductName__c='Test Product1',Active__c=true);
            insert newProd1;
    
            globalProgram = Utils_TestMethods.createPartnerProgram();
            globalProgram.TECH_CountriesId__c = country.id;
            globalPRogram.recordtypeid = Label.CLMAY13PRM15;
            insert globalProgram;
            
            // INSERT PartnerProgramCountries
            List<PartnerProgramCountries__c> lstPrgCountries = new List<PartnerProgramCountries__c>();
            PartnerProgramCountries__c ppc1 = new PartnerProgramCountries__c(
                    Country__c = country.Id,
                    PartnerProgram__c = globalProgram.Id
                );
            lstPrgCountries.add(ppc1);
            PartnerProgramCountries__c ppc2 = new PartnerProgramCountries__c(
                    Country__c = country11.Id,
                    PartnerProgram__c = globalProgram.Id
                );
            lstPrgCountries.add(ppc2);
            insert lstPrgCountries;
            
            // INSERT PartnerProgram Clasifications
            PartnerProgramClassification__c ppc = new PartnerProgramClassification__c();
            ppc.PartnerProgram__c=globalProgram.Id;
            ppc.ClassificationLevelCatalog__c=lvl1.Id;
            insert ppc;
    
            // INSERT PartnerProgram Markets
            PartnerProgramMarket__c ppm = new PartnerProgramMarket__c();
            ppm.PartnerProgram__c=globalProgram.Id;
            ppm.MarketSegmentCatalog__c=sgmt.Id;
            insert ppm;
    
            // INSERT PartnerProgram DOE
            ProgramDomainsOfExpertise__c ppdoe = new ProgramDomainsOfExpertise__c();
            ppdoe.PartnerProgram__c=globalProgram.Id;
            ppdoe.DomainsOfExpertiseCatalog__c=cdoe.Id;
            insert ppdoe;
    
            // INSERT Program Products
            ProgramProduct__c pprd = new ProgramProduct__c();
            pprd.PartnerProgram__c=globalProgram.Id;
            pprd.ProductLineCatalog__c=newProd1.Id;
            insert pprd;
    
            
            CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
            newRecTypeMap.name = recCatalogActivityReq.Id;
            newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
            newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
            
            insert newRecTypeMap;
            
            ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
            myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
            
        }
        
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
        u.userroleid = r.Id;
        insert u;
        
        system.runAs(u) {

            myPageCon1.goToCountryPRGEditPage();
            globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
            update globalProgram;
            //myPageCon1.goToCountryPRGEditPage();
            ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel.recordtypeid = levelRecordType.Id;
            insert newLevel;
            system.debug('newlevel:'+newLevel);
            FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
            insert feature;
            
            ProgramFeature__c newFeature = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature.FeatureStatus__c = Label.CLMAY13PRM09;
            newFeature.recordtypeid = featureRecordType.Id;
            insert newFeature;
            
            RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req;
            
            ProgramRequirement__c newRequirement = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel.Id, req.ID);
            newRequirement.Active__c = true;
            newRequirement.recordtypeid = programActivityReq.Id;
            insert newRequirement;
            myPageCon1.goToCountryPRGEditPage();
            //myPageCon1.goToCountryPRGEditPage();

        }
             Test.StopTest();
        
        u2 = Utils_TestMethods.createStandardUserWithNoCountry('PPADM2');
        u2.BypassVR__c = true;
        u2.BypassWF__c = true;
        u2.ProgramAdministrator__c=true;
        u2.country__c = country11.countrycode__c;
        u2.userroleid = r.Id;
        insert u2;
            
        System.runAs(u2) {

            ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
            ApexPages.StandardController sdtCon11 = new ApexPages.StandardController(globalProgram);
            VFC_NewCountryProgram myPageCon11 = new VFC_NewCountryProgram(sdtCon11);
            myPageCon11.goToCountryPRGEditPage();
        

            globalProgram.TECH_CountriesId__c = country.id;
            update globalProgram;

        }
        
        system.runAs(u) {
            myPageCon1.goToCountryPRGEditPage();
        }
        
        u.country__C = '';
        update u;
        Country__c country2;
        system.runAs(u) {

            myPageCon1.goToCountryPRGEditPage();
        
            country2 = Utils_TestMethods.createCountry();
            country2.countrycode__c = 'GB';
            insert country2;

        }
        
        u.country__C =country2.countrycode__c;
        update u;
        
        system.runAs(u) {

            myPageCon1.goToCountryPRGEditPage();
        
        
        list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
        countryPartnerProgram = [Select id,programstatus__c,country__c from PartnerProgram__c where GlobalPartnerProgram__c = :globalProgram.Id limit 1];
        if(!countryPartnerProgram.isEmpty())
        {
            countryPartnerProgram[0].programstatus__c = Label.CLMAY13PRM34;
            update countryPartnerProgram;
        }

        }
     }
}