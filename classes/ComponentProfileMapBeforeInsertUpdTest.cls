@istest
public with sharing class ComponentProfileMapBeforeInsertUpdTest{

static testMethod void testMe(){
 User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'MenuBeforeInsert;';
        //update us;
        
        System.RunAs(us){
         FieloEE__Program__c prgm = new FieloEE__Program__c();
            prgm.Name = 'PRM Is On';
            prgm.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
            prgm.FieloEE__SiteURL__c = System.Label.CLAPR15PRM308;
            prgm.FieloEE__RecentRewardsDays__c = 10.0;
            insert prgm;
   Country__c testCountry = new Country__c();
    testCountry.Name   = 'India';
    testCountry.CountryCode__c = 'IN';
    testCountry.InternationalPhoneCode__c = '91';
    testCountry.Region__c = 'APAC';
    INSERT testCountry;
   Country__c testCountry1 = new Country__c();
            testCountry1.Name   = 'Ireland';
            testCountry1.CountryCode__c = 'IE';
            testCountry1.InternationalPhoneCode__c = '353';
            testCountry1.Region__c = 'EMEAS';
            INSERT testCountry1;
                
    Country__c testCountry2 = new Country__c();
        testCountry2.Name   = 'Spain';
        testCountry2.CountryCode__c = 'ES';
        testCountry2.InternationalPhoneCode__c = '34';
        testCountry2.Region__c = 'EMEAS';
        INSERT testCountry2;

    Country__c testCountry3 = new Country__c();
        testCountry3.Name   = 'Russian Federation';
        testCountry3.CountryCode__c = 'RU';
        testCountry3.InternationalPhoneCode__c = '7';
        testCountry3.Region__c = 'EMEAS';
        INSERT testCountry3;

    Country__c testCountry4 = new Country__c();
        testCountry4.Name   = 'Yemen';
        testCountry4.CountryCode__c = 'YE';
        testCountry4.InternationalPhoneCode__c = '967';
        testCountry4.Region__c = 'EMEAS';
        INSERT testCountry4;

    Country__c testCountry5 = new Country__c();
        testCountry5.Name   = 'Eritrea';
        testCountry5.CountryCode__c = 'ER';
        testCountry5.InternationalPhoneCode__c = '291';
        testCountry5.Region__c = 'EMEAS';
        INSERT testCountry5;
    PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.Country__c = testCountry.Id;
            prmCountry.PLDatapool__c = 'abcd';
            prmCountry.SupportedLanguage1__c = 'en_IN';
            prmCountry.SupportedLanguage2__c = 'en_US';
            prmCountry.SupportedLanguage3__c = 'es';
            prmCountry.MemberCountry1__c = testCountry1.Id;
            prmCountry.MemberCountry2__c = testCountry2.Id;
            prmCountry.MemberCountry3__c = testCountry3.Id;
            prmCountry.MemberCountry4__c = testCountry4.Id;
            prmCountry.MemberCountry5__c = testCountry5.Id;
        INSERT prmcountry;
    FieloEE__Menu__c menu = new FieloEE__Menu__c(Name = 'test',FieloEE__Program__c=prgm.id);
    insert menu;
    FieloEE__Component__c comp = new FieloEE__Component__c(FieloEE__Menu__c = menu.Id);
    insert comp;

    ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
    insert cLC;
    ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
    insert cLChild;
    CountryChannels__c cChannels = new CountryChannels__c();
    cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
    cChannels.Country__c = testCountry.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true; cChannels.PRMCountry__c = prmcountry.Id;
    insert cChannels;
    
    PRMProfileConfig__c ppc = new PRMProfileConfig__c();
    ppc.CountryChannel__c = cChannels.id;
    ppc.Name = 'Test';  
    insert ppc; 
    ComponentProfileMap__c cpm1 = new ComponentProfileMap__c();
    cpm1.Component__c= comp.Id;
    cpm1.PRMProfileConfig__c = ppc.Id;
    cpm1.Active__c = true;  
    insert cpm1;
    
    PRMProfileConfig__c ppc1 = new PRMProfileConfig__c();
    ppc1.CountryChannel__c = cChannels.id;
    ppc1.Name = 'Test123';  
    insert ppc1;
    
    /*ComponentProfileMap__c cpm = new ComponentProfileMap__c();
    cpm.Component__c= comp.Id;
    cpm.PRMProfileConfig__c = ppc.Id;
    cpm.Active__c = true;
    */
    Test.startTest();
    cpm1.PRMProfileConfig__c = ppc1.Id;
    Update cpm1;
    Test.stopTest();
    }
    }
    
static testMethod void testMe1(){
 User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'MenuBeforeInsert;';
        //update us;
        
        System.RunAs(us){
         FieloEE__Program__c prgm = new FieloEE__Program__c();
            prgm.Name = 'PRM Is On';
            prgm.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
            prgm.FieloEE__SiteURL__c = System.Label.CLAPR15PRM308;
            prgm.FieloEE__RecentRewardsDays__c = 10.0;
            insert prgm;
   Country__c testCountry = new Country__c();
    testCountry.Name   = 'India';
    testCountry.CountryCode__c = 'IN';
    testCountry.InternationalPhoneCode__c = '91';
    testCountry.Region__c = 'APAC';
    INSERT testCountry;
   Country__c testCountry1 = new Country__c();
            testCountry1.Name   = 'Ireland';
            testCountry1.CountryCode__c = 'IE';
            testCountry1.InternationalPhoneCode__c = '353';
            testCountry1.Region__c = 'EMEAS';
            INSERT testCountry1;
                
    Country__c testCountry2 = new Country__c();
        testCountry2.Name   = 'Spain';
        testCountry2.CountryCode__c = 'ES';
        testCountry2.InternationalPhoneCode__c = '34';
        testCountry2.Region__c = 'EMEAS';
        INSERT testCountry2;

    Country__c testCountry3 = new Country__c();
        testCountry3.Name   = 'Russian Federation';
        testCountry3.CountryCode__c = 'RU';
        testCountry3.InternationalPhoneCode__c = '7';
        testCountry3.Region__c = 'EMEAS';
        INSERT testCountry3;

    Country__c testCountry4 = new Country__c();
        testCountry4.Name   = 'Yemen';
        testCountry4.CountryCode__c = 'YE';
        testCountry4.InternationalPhoneCode__c = '967';
        testCountry4.Region__c = 'EMEAS';
        INSERT testCountry4;

    Country__c testCountry5 = new Country__c();
        testCountry5.Name   = 'Eritrea';
        testCountry5.CountryCode__c = 'ER';
        testCountry5.InternationalPhoneCode__c = '291';
        testCountry5.Region__c = 'EMEAS';
        INSERT testCountry5;
    PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.Country__c = testCountry.Id;
            prmCountry.PLDatapool__c = 'abcd';
            prmCountry.SupportedLanguage1__c = 'en_IN';
            prmCountry.SupportedLanguage2__c = 'en_US';
            prmCountry.SupportedLanguage3__c = 'es';
            prmCountry.MemberCountry1__c = testCountry1.Id;
            prmCountry.MemberCountry2__c = testCountry2.Id;
            prmCountry.MemberCountry3__c = testCountry3.Id;
            prmCountry.MemberCountry4__c = testCountry4.Id;
            prmCountry.MemberCountry5__c = testCountry5.Id;
        INSERT prmcountry;
    FieloEE__Menu__c menu = new FieloEE__Menu__c(Name = 'test',FieloEE__Program__c=prgm.id);
    insert menu;
    FieloEE__Component__c comp = new FieloEE__Component__c(FieloEE__Menu__c = menu.Id);
    insert comp;

    ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
    insert cLC;
    ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
    insert cLChild;
    CountryChannels__c cChannels = new CountryChannels__c();
    cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
    cChannels.Country__c = testCountry.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true; cChannels.PRMCountry__c = prmcountry.Id;
    insert cChannels;
    
    PRMProfileConfig__c ppc = new PRMProfileConfig__c();
    ppc.CountryChannel__c = cChannels.id;
    ppc.Name = 'Test';  
    insert ppc;
    
     PRMProfileConfig__c ppc2 = new PRMProfileConfig__c();
    ppc2.CountryChannel__c = cChannels.id;
    ppc2.Name = 'Test123';  
    insert ppc2;

    ComponentProfileMap__c cpm = new ComponentProfileMap__c();
    cpm.Component__c= comp.Id;
    cpm.PRMProfileConfig__c = ppc.Id;
    insert cpm;
     Test.startTest();
    cpm.PRMProfileConfig__c = ppc2.Id;
    Update cpm;
    Test.stopTest();
    }
    } 
}