@isTest
private class CaseCommentTriggers_TEST {
    
    static testMethod void ExpertInternalComments_TestMethod(){
        Account accounts = Utils_TestMethods.createAccount();
        insert accounts;
             
        contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
        Database.insert(con);     
    
        Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'In Progress');
        Database.insert(caseobj);
        System.debug('@@@caseobj'+caseobj);
        Test.startTest(); 
        ExpertInternalComment__c objExpInternalComments = new ExpertInternalComment__c();
        objExpInternalComments.case__c = caseobj.id;
        insert objExpInternalComments ;
        
        objExpInternalComments.Comments__c='Test Comment Please ignore';
        update objExpInternalComments ;
        Test.StopTest();
    }
    
    static testMethod void EscalationInternalComments_TestMethod(){
        Account objAcc = Utils_TestMethods.createAccount();
        Database.insert(objAcc);
             
        contact con=Utils_TestMethods.createContact(objAcc.Id,'testContact');
        Database.insert(con);     
    
        Case caseobj=Utils_TestMethods.createCase(objAcc .id,con.id,'In Progress');
        Database.insert(caseobj);
      
        Test.startTest(); 
        CSE_L2L3InternalComments__c objEsclationInternal = new CSE_L2L3InternalComments__c();
        objEsclationInternal.Case__c=caseobj.id;
        database.insert(objEsclationInternal);
        
        objEsclationInternal.Comments__c='Test Comment Please ignore';
        update objEsclationInternal;
        
        
        Test.StopTest();
    }
    
    static testMethod void CaseComments_TestMethod(){
        Account objAcc = Utils_TestMethods.createAccount();
        Database.insert(objAcc);
             
        contact con=Utils_TestMethods.createContact(objAcc.Id,'testContact');
        Database.insert(con);     
    
        Case caseobj=Utils_TestMethods.createCase(objAcc.id,con.id,'In Progress');
        Database.insert(caseobj);
      
        Test.startTest(); 
        CaseComment objCseComment = new CaseComment();
        objCseComment.ParentId=caseobj.id;
        database.insert(objCseComment);
        
        objCseComment.IsPublished=False;
        update objCseComment;
        
        
        Test.StopTest();
    }
    
    static testMethod void Attachment_TestMethod(){
        Account objAcc = Utils_TestMethods.createAccount();
        Database.insert(objAcc);
             
        contact con=Utils_TestMethods.createContact(objAcc.Id,'testContact');
        Database.insert(con);     
    
        Case caseobj=Utils_TestMethods.createCase(objAcc.id,con.id,'In Progress');
        Database.insert(caseobj);
      
        Test.startTest(); 
        Attachment objAttachment = new Attachment(); 
        Blob b = Blob.valueOf('Test Data');   
        objAttachment.ParentId = caseobj.id;  
        objAttachment.Name = 'Test Attachment for Parent';  
        objAttachment.Body = b;  
        insert objAttachment;

        objAttachment.Name='Test Data Please ignore';
        update objAttachment;
        
        
        Test.StopTest();
    } 
     
}