@isTest(SeeAllData=true)
private class ServiceMaxWOAutoAssignment_TEST 
{
     static testMethod void testWO()
     {   Account acc = Utils_TestMethods.createAccount();
         insert acc;
                         
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
                 workOrder.Work_Order_Category__c='On-site';                 
                 workOrder.SVMXC__Order_Type__c='Maintenance';
                 workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                 workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                 workOrder.CustomerRequestedDate__c = Date.today();
                 workOrder.Service_Business_Unit__c = 'Energy';
                 workOrder.SVMXC__Priority__c = 'Normal-Medium';
                 workOrder.CustomerTimeZone__c= 'on leave';
                 workOrder.SendAlertNotification__c = true;
                 workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                 workOrder.BackOfficeReference__c = '111111';
                 workOrder.Comments_to_Planner__c='testing';
                 //workOrder.Work_Order_Notification__c=won.Id;
         insert workOrder;
         set<Id> woid = New Set<Id>();
         woid.add(workOrder.Id);
         Map<Id,Id> Map1 = New Map<Id,Id>();         
         
         Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];        
        
        //User creation
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
       // insert user;
        Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;
        
        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
                       
        RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
        RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          
         for(RecordType rt : rts) 
        {
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
                insert st;
           } 
        }
        for(RecordType rt : rtssg)
        {
           if(rt.DeveloperName == 'Technican')
            {
                sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                           SVMXC__Salesforce_User__c = userinfo.getuserid(),
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '12345'
                                                            );
                insert sgm1;
            }
        } 
        Map1.Put(workOrder.Id,sgm1.Id);
      ServiceMaxWOAutoAssignment.autoAssignTechnician (Map1);
     }
}