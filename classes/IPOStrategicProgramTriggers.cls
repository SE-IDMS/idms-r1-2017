/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  12-Dec-12
    Description         : Class on IPO Strategic Program to Validate Updates to Scope
 */

public class IPOStrategicProgramTriggers
{

// Class to Check IPO Strategic Milestone on IPO Strategic Program Updation

    Public Static void IPOStrategicProgramBeforeUpdate(List<IPO_Strategic_Program__c> Prgnew,List<IPO_Strategic_Program__c> Prgold )
      {
      
          System.Debug('****** IPOStrategicProgramBeforeUpdate Start ****');
          
          List<IPO_Strategic_Milestone__c> Mile = new List<IPO_Strategic_Milestone__c>();
          
          Mile = [Select Name, IPO_Strategic_Program_Name__c,Program__c, Global_Functions__c, Global_Business__c, Operation_Regions__c from IPO_Strategic_Milestone__c];
          
          
          // Check if a Scope with child Milestone is removed in the update action
          
          for(IPO_Strategic_Program__c P : Prgnew){
              for(IPO_Strategic_Milestone__c M: Mile){    
                if(P.Name == M.Program__c) { 
                
                // Check if Entiy in the Program is Null for any Scope (All Entities Removed)
                
                if(((P.Global_Functions__c == null)&& (M.Global_Functions__c !=null)) || ((P.Global_Business__c == null)&& (M.Global_Business__c !=null)) || ((P.Operation__c == null)&& (M.Operation_Regions__c !=null)))
                     P.ValidateScopeonUpdate__c = True;
                
                // Check for Global Function Scope
                         
                    if (P.Global_Functions__c != prgold[0].Global_Functions__c){
                     if((M.Global_Functions__c !=null) && (P.Global_Functions__c != null)){
                      System.Debug('Milestone GF ' + M.Global_Functions__c);
                      System.Debug('Program GF ' + P.Global_Functions__c);
                        for(String GF : M.Global_Functions__c.split(';')){
                            if(!(P.Global_Functions__c.Contains(GF)))
                                P.ValidateScopeonUpdate__c = True;
                            } // GF For Ends
                           } // Milestone GF If Ends
                           } // Program GF If Ends
                           
                           
                  // Check for Global Business Scope
                         
                    if (P.Global_Business__c != prgold[0].Global_Business__c){
                     if((M.Global_Business__c !=null)&& (P.Global_Business__c != null)){
                     System.Debug('Milestone GB ' + M.Global_Business__c);
                      System.Debug('Program GB ' + P.Global_Business__c);
                        for(String GB : M.Global_Business__c.split(';')){
                            if(!(P.Global_Business__c.Contains(GB)))
                                P.ValidateScopeonUpdate__c = True;
                            } // GB For Ends
                           } // GB Milestone If Ends
                          } // GB Program If Ends 
                          
                   // Check for Power Regions
                   
                    if (P.Operation__c != prgold[0].Operation__c){
                     if((M.Operation_Regions__c !=null) && (P.Operation__c != null)){
                        for(String PR : M.Operation_Regions__c.split(';')){
                            if(!(P.Operation__c.Contains(PR)))
                                P.ValidateScopeonUpdate__c = True;
                            } // PR For Ends
                           } // PR Milestone If Ends
                          } // PR Program  If Ends
                           
                         } // IF Ends
                          
                  } // Milestone For Ends   
                 }// Program For Ends      
             
       }
       
       // Class to Check Deployment Network on Program Updation
       
    Public Static void IPOStrategicProgramBeforeUpdate_CheckDeploymentNetwork(List<IPO_Strategic_Program__c> Prgnew,List<IPO_Strategic_Program__c> Prgold )
      {
      
          System.Debug('****** IPOStrategicProgramBeforeUpdate_CheckDeploymentNetwork Start ****');
          
          List<IPO_Strategic_Deployment_Network__c> Team = New List<IPO_Strategic_Deployment_Network__c>();
          
          Team = [Select Name, Scope__c, Entity__c, GSC_Region__c, Partner_Sub_Entity__c, IPO_Strategic_Program_Name__c from IPO_Strategic_Deployment_Network__c];
          
          for(IPO_Strategic_Program__c P : Prgnew){
              for(IPO_Strategic_Deployment_Network__c T: Team){    
                if(P.Name == T.IPO_Strategic_Program_Name__c) { 
                  
                   // Check if Entiy in the Program is Null for any Scope (All Entities Removed)
                
                if(((P.Global_Functions__c == null)&& (T.Scope__c == Label.Connect_Global_Functions)) || ((P.Global_Business__c == null)&& (T.Scope__c == Label.Connect_Global_Business)) || ((P.Operation__c == null)&& (T.Scope__c == Label.Connect_Power_Region)))
                     P.ValidateScopeonUpdate__c = True;
                     
                       // Check for Every Scope
                         
                    
                       if((P.Global_Functions__c != null) && (T.Scope__c == Label.Connect_Global_Functions) && (!(P.Global_Functions__c.Contains(T.Entity__c))))
                          P.ValidateScopeonUpdate__c = True;
                      else if((P.Global_Business__c != null)&& (T.Scope__c == Label.Connect_Global_Business) && (!(P.Global_Business__c.Contains(T.Entity__c))))
                          P.ValidateScopeonUpdate__c = True;
                      else if((P.Operation__c != null) && (T.Scope__c == Label.Connect_Power_Region) && (!(P.Operation__c.Contains(T.Entity__c))))
                          P.ValidateScopeonUpdate__c = True;
                      
                     
                    } // If Ends
                  } // For Ends
                 }// For Ends
                 
                 System.Debug('****** IPOStrategicProgramBeforeUpdate_CheckDeploymentNetwork Stop ****');
                }
                
  Public Static void IPOStrategicProgramNetworkValidationBeforeUpdate(List<IPO_Strategic_Program__c> prgnew)
{

// Validate Deployment Network when the Scope is changed in the Program

System.Debug('****** IPOStrategicProgramNetworkValidationBeforeUpdate Start ****'); 

// Lists

list<IPO_Strategic_Deployment_Network__c> Team = [select Scope__c, Entity__c, IPO_Strategic_Program_Name__c from IPO_Strategic_Deployment_Network__c  ] ;
list <IPO_Strategic_Program__c> Program = [Select Name, IPO_Strategic_Initiative_Name__c, Global_Functions__c, Global_Business__c, Operation__c from IPO_Strategic_Program__c];

String GF_Team;
String GB_Team;
String PR_Team;
String GSC_Team;
String Partner_Team;

Integer GFn = 0;
Integer GBs = 0;
Integer PRs = 0;
Integer GSC_R = 0;
Integer PAReg = 0;

For(IPO_Strategic_Program__c Prg : prgnew){

    For(IPO_Strategic_Deployment_Network__c TMs : Team){

    if(TMs.Scope__c == System.Label.Connect_Global_Functions && TMs.IPO_Strategic_Program_Name__c == Prg.Name){
        GF_Team = GF_Team + Tms.Entity__c + ';';
          }

    // Concatenate the Scope of Selected Team Members for Global Business

    if(TMs.Scope__c == System.Label.Connect_Global_Business && TMs.IPO_Strategic_Program_Name__c == Prg.Name){
        GB_Team = GB_Team + Tms.Entity__c + ';';
         }
         
     // Concatenate the Scope of Selected Team Members for Power Region

    if(TMs.Scope__c == System.Label.Connect_Power_Region && TMs.IPO_Strategic_Program_Name__c == Prg.Name){
        PR_Team = PR_Team + Tms.Entity__c + ';';
         }
         
        
    
    } // Team For Ends
    
    // Check Scope Changes for Global Functions
    if(Prg.Global_Functions__c != null)
     For(String GF : Prg.Global_Functions__c.Split(';')){
       if(GF_Team != null)
        if(!(GF_Team.Contains(GF)))
           GFn = 1;
         }
    
    // Check Scope Changes for Global Business
    if(Prg.Global_Business__c != null)
     For(String GB : Prg.Global_Business__c.Split(';')){
       if(GB_Team != null)
        if(!(GB_Team.Contains(GB)))
           GBs = 1;
         }
    // Check Scope Changes for Power Regions
    if(Prg.Operation__c != null)
     For(String PR : Prg.Operation__c.Split(';')){
     if(PR_Team != null)
        if(!(PR_Team.Contains(PR)))
           PRs = 1;
         }
   
 
     if ((GFn > 0) || (GBs > 0) || (PRs > 0))
        Prg.ProgramNetworkUpdate__c = True;

       } // For Ends
     }
         

}