/*
21-June-2012    Srinivas Nallapati    Mkt June Release    Test Class for Batch_NoOfOpenLeadsUpdate
*/
@isTest (SeeAllData = true)
public class Batch_NoOfOpenLeadsUpdate_TEST{
    static testMethod void testAP19_Lead() 
    {
        Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
        Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();   
            test.startTest();
           
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;            
                        
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            Contact con2 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con2;
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            System.debug(LC1 + 'LC1');   
            System.debug(LC2 + 'LC2');  
            List<Lead> leads = new List<Lead>();
System.Debug('********** Start Leads Insert **********');
            for(Integer i=0;i<20;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                if(math.mod(i,4) == 0)
                {
                   lead1.Contact__C = con2.id;
                   lead1.Account__c = Acc.id;
                   lead1.status = '1 - New';
                   lead1.Category__c = 'ITB';
                }
                else
                {
                    lead1.LeadContact__C = LC1.id;
                    lead1.status = '1 - New';
                    lead1.Category__c = 'IND';
                }
                leads.add(lead1);   
            }
            List<Id> leadsId = new List<Id>();
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
                for(Database.SaveResult sr: leadsInsert)
                {
                    if(!sr.isSuccess())
                    {
                        Database.Error err = sr.getErrors()[0];
                        System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                    } 
                    else
                       leadsId.add(sr.Id);
                }
            System.Debug('********** End Leads insert **********');
            
            leads.clear();
            leads = [select Status, subStatus__c from lead where Id in: leadsId];
                                     
            
            Database.SaveResult[] leadsUpdate;
            integer x =0;
            for(Lead lead2: leads)
            {   
                lead2.Status = Label.CL00447;
                lead2.SubStatus__c = Label.CL00448;
                
                if(math.mod(x,4) == 0)
                {
                   lead2.Contact__C = con1.id; 
                   lead2.Category__c = 'ITB';
                   lead2.OwnerId = UserInfo.getUserID();
                   lead2.TECH_PreviousContactId__c = '008JunkIDtestr5';
                }
                else
                {
                    lead2.LeadContact__C = LC2.id;
                    lead2.Category__c = 'IND';
                    lead2.TECH_PreviousLeadContactId__c = '008JunkIDtestr6';
                }
                x++;    
                
            }
            
            leadsUpdate= Database.update(leads,false);
            for(Database.SaveResult sr: leadsUpdate)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                }    
            }
            
            
            Batch_NoOfOpenLeadsUpdate updateContacts =new Batch_NoOfOpenLeadsUpdate();
            updateContacts.queryString = 'SELECT Contact__c, TECH_PreviousContactId__c, LeadContact__C, TECH_PreviousLeadContactId__c  FROM lead WHERE (Contact__c != null  or leadcontact__C != null) limit 10';
            id batchprocessid = Database.executeBatch(updateContacts);
            system.debug(batchprocessid);
            
            test.stopTest();
 
    } 
}