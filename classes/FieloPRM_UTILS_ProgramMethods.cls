/**************************************************************************************
  Author: Fielo Team (Elena J. Schwarzböck)
  Date: 17/03/2015
  Description: 
  Related Components:
***************************************************************************************/
public class FieloPRM_UTILS_ProgramMethods{
    
    /**
    * [getProgramByContact returns the program record for the contact Id]
    * @method          getProgramByContact
    * @Pre-conditions  {{string}}
    * @Post-conditions {{string}}
    * @param           {[String]}     contactId     [salesforce contact id]
    * @return          {[FieloEE__Program__c]}      []
    */
    public static FieloEE__Program__c getProgramByContact(String contactId){            
            
        List<Contact> listContact = [SELECT Id, FieloEE__Member__r.FieloEE__Program__c FROM Contact WHERE Id =: contactId AND FieloEE__Member__c != null LIMIT 1];

        //if no member found returns null    
        if(listContact.isEmpty() || (!listContact.isEmpty() && listContact[0].FieloEE__Member__r.FieloEE__Program__c == null)){
            return null;
        }
        
        String allFields = '';
        
        for(Schema.SObjectField nameAPIfield : Schema.SObjectType.FieloEE__Program__c.fields.getMap().values()){
            allFields += allFields==''?String.ValueOf(nameAPIfield):', '+String.ValueOf(nameAPIfield);
        }
        
        FieloEE__Program__c prog = new FieloEE__Program__c();
        
        try{
            prog = Database.query('SELECT ' + allFields + ' FROM FieloEE__Program__c WHERE Id = \'' + listContact[0].FieloEE__Member__r.FieloEE__Program__c + '\'');
        }catch(Exception e){
            return null;
        }
        
        return prog;
  
    }        
       

}