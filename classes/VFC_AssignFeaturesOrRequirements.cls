Public class VFC_AssignFeaturesOrRequirements {

    private FeatureCatalog__c fCat = null;
    private RequirementCatalog__c rCat = null;
    private ID returnID;
    Private ID reqRecordType;

     //public List<ProgramFeatureRequirementWrapper> ProgramFeatures {get;set;}
    public List<ProgramFeatureRequirementWrapper> ProgramFeatureRequirements { get; set; }
    public List<ProgramFeatureRequirementWrapper> ProgramFeatureRequirements1 { get; set; }
    public List<ProgramFeatureRequirementWrapper> ProgramRequirements { get; set; }

    public boolean ShowFeatureCatalg = false;
    public boolean ShowRequirementCatalog = false;

    public List<ProgramFeature__c> lstDeletedFeatures = new  List<ProgramFeature__c>();
    public List<ProgramRequirement__c> lstDeletedRequirements = new  List<ProgramRequirement__c>();
    public User currentUser ;
    public String queryUser='';
    public String userType='';
    public list<ProgramFeatureRequirementWrapper> lstError = new list<ProgramFeatureRequirementWrapper>();
    public boolean ErrMsg {set;}

    // Controller
    //----------------------------------
    public VFC_AssignFeaturesOrRequirements (ApexPages.StandardController stdController) {
        SObject record = stdController.getRecord();
        ProgramFeatureRequirements = new List<ProgramFeatureRequirementWrapper>();
        
        //Check current user is approver/owner/Administrator
        currentUser = [Select id,ProgramAdministrator__c,ProgramOwner__c, ProgramApprover__c,ProgramManager__c from User where id =:UserInfo.getUserId() limit 1];
        System.debug('**** User Type->'+currentUser.ProgramAdministrator__c +' - '+currentUser.ProgramApprover__c);
        if(!currentUser.ProgramAdministrator__c && !currentUser.ProgramApprover__c)
        {
            queryUser = ' And PartnerProgram__r.OwnerId ='+'\''+currentUser.id+'\'';
            userType = 'owner';
        }
        
        
        if(record instanceof FeatureCatalog__c ) {
            // This is a Feature Catalog record
            fCat = ((FeatureCatalog__c )record);             
            ProgramFeatureRequirements = getProgramFeatures();
            
            returnID =fCat.id; 
        }

        if(record instanceof RequirementCatalog__c ) {
            // This is a Requirement Catalog record
            rCat = ((RequirementCatalog__c  )record); 
            ProgramFeatureRequirements = getProgramRequirements();
            //ProgramFeatureRequirements1.addAll(ProgramFeatureRequirements);
            
            if(rCat.RecordTypeId == System.label.CLOCT14PRM19)
            {
                reqRecordType = System.label.CLOCT14PRM16;
            }
            else if(rCat.RecordTypeId == System.label.CLOCT14PRM20)
            {
                reqRecordType = System.label.CLOCT14PRM17;
            }
            else
            {
                reqRecordType = System.label.CLOCT14PRM18;
            }
            returnID = rCat.id; 
        }   
    }
    //</ End of Controller 
    //----------------------------------------------------------------
    
    // Action Methods
    public void addNewRow() {

        ProgramFeatureRequirementWrapper tmp = new ProgramFeatureRequirementWrapper(); 

        if(fCat != null)
        {
            tmp.ProgramFeature.FeatureStatus__c = System.label.CLMAY13PRM09;
            tmp.ProgramFeature.FeatureCatalog__c = fCat.Id;
            ProgramFeatureRequirements.add(tmp);
        }
        else
        {
            tmp.ProgramRequirement.RequirementCatalog__c = rCat.id;
            tmp.ProgramRequirement.Active__c = true;
            ProgramFeatureRequirements.add(tmp);        
        }
        
        clearUpdateStatus();

    }
    
    public void clearUpdateStatus() {
        
        integer j=0;
        if(lstError !=null && lstError.size()>0)
        {
            for (ProgramFeatureRequirementWrapper pf : lstError)
            {
                pf.UpdateStatus = '';
            }
        }

        //if(ApexPages.hasMessages())
        //  clearApexMessage();
    
    }
    
    public pageReference clearApexMessage(){
        PageReference pageRef = ApexPages.currentPage();
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    public void deleteRow()
    {
        List<ProgramFeatureRequirementWrapper> lstUndeletedFeatures = new List<ProgramFeatureRequirementWrapper>();
        lstDeletedFeatures.clear();
        lstDeletedRequirements.clear();

        for(ProgramFeatureRequirementWrapper awraperFeature :ProgramFeatureRequirements)
        {
           if(awraperFeature.ToBeDeleted == true) 
            {
               String recId = String.ValueOf(awraperFeature.RecordId);
               if( recId != null && recId != '')
                    if(fCat != null)
                        lstDeletedFeatures.add(awraperFeature.ProgramFeature);
                    else
                        lstDeletedRequirements.add(awraperFeature.ProgramRequirement);
            }
            else
                lstUndeletedFeatures.add(awraperFeature);
        }

        ProgramFeatureRequirements.clear();
        if(lstUndeletedFeatures.size() > 0)
            ProgramFeatureRequirements.addAll(lstUndeletedFeatures);
            
        lstError.clear();//uat
    }

    public PageReference doSave() {  

        System.Debug('*** Saving for New Features :');
        List<ProgramFeature__c> lstNewFeatures = new List<ProgramFeature__c>();
        List<ProgramRequirement__c> lstNewReqirements = new List<ProgramRequirement__c>();
        List<Id> partnerProgramList = new List<id>();
        
        //clearing the error
        ClearUpdateStatus();
        
        for (ProgramFeatureRequirementWrapper pFeatureRequirement : ProgramFeatureRequirements)
        {
            if(fCat != null)
                partnerProgramList.add(pFeatureRequirement.ProgramFeature.PartnerProgram__c);
            else
                partnerProgramList.add(pFeatureRequirement.ProgramRequirement.PartnerProgram__c);
        }
        
        System.debug('partnerProgramList:'+partnerProgramList);
        List<PartnerProgram__c> recordTypeList = new List<PartnerProgram__c>([SELECT Id,OwnerId,RecordTypeId FROM partnerProgram__c WHERE Id in :partnerProgramList AND RecordTypeId = :System.label.CLMAY13PRM15]);

        Map<Id, Id> recordTypeMap = new Map<Id, Id>();
        for(PartnerProgram__c var : recordTypeList)
        {
            //recordTypeMap.put(var.ID, var.RecordTypeId);
            recordTypeMap.put(var.ID, var.OwnerId);
        }
        System.debug('recordTypeMap:'+recordTypeMap);
        boolean fsaveSuccess = true;
        boolean recordTypeCheck = true;
        boolean fsaveDelSuccess = true;
        integer i=0;
        
        
        for (ProgramFeatureRequirementWrapper pf : ProgramFeatureRequirements)
        {
            //Adding features
            String NewRecId = String.ValueOf(pf.RecordId);
            System.Debug('*** New Features :'+NewRecId);
            if(fCat != null && pf.ToBeDeleted != true) //Feature Catalog
            {
                if(pf.ProgramFeature.programLevel__c != null && pf.ProgramFeature.PartnerProgram__c != null )
                {
                    if(NewRecId == null || NewRecId == '')
                    {
                        pf.ProgramFeature.FeatureCatalog__c = fCat.Id;
                        pf.ProgramFeature.RecordTypeId = system.label.CLMAY13PRM06;
                    }
                    pf.ProgramFeature.id = NewRecId;
                    if(recordTypeMap.containsKey(pf.ProgramFeature.PartnerProgram__c)){ //UAT Change
                        lstNewFeatures.add(pf.ProgramFeature);
                        //UAT Change
                        if(userType == 'owner' && (recordTypeMap.get(pf.ProgramFeature.PartnerProgram__c)== null || recordTypeMap.get(pf.ProgramFeature.PartnerProgram__c) != currentUser.id))
                        {
                          pf.UpdateStatus = system.label.CLOCT14PRM73;
                          lstError.add(pf);
                          fsaveSuccess = false;
                        }
                        
                    }   
                    else
                    {
                        recordTypeCheck = false;
                        pf.UpdateStatus = system.label.CLOCT14PRM15;
                        lstError.add(pf);
                        fsaveSuccess = false;                       
                    }   
                }
            }
            else //Requirement Catalog
            {
                if(pf.ProgramRequirement.programLevel__c != null && pf.ProgramRequirement.PartnerProgram__c != null)
                {
                    if(NewRecId == null || NewRecId == '')
                    {
                        pf.ProgramRequirement.RequirementCatalog__c = rCat.Id;
                        //RecordTypeId to Set
                        pf.ProgramRequirement.RecordTypeId = reqRecordType;
                    }
                    pf.ProgramRequirement.id = NewRecId;
                    if(recordTypeMap.containsKey(pf.ProgramRequirement.PartnerProgram__c)){
                        lstNewReqirements.add(pf.ProgramRequirement);
                        //UAT Change
                        if(userType == 'owner' && (recordTypeMap.get(pf.ProgramRequirement.PartnerProgram__c)== null || recordTypeMap.get(pf.ProgramRequirement.PartnerProgram__c) != currentUser.id))
                        {
                          pf.UpdateStatus = system.label.CLOCT14PRM73;
                          lstError.add(pf);
                          fsaveSuccess = false;
                        }
                    }    
                    else {
                        recordTypeCheck = false;
                        pf.UpdateStatus = system.label.CLOCT14PRM15;
                        lstError.add(pf);
                        fsaveSuccess = false;
                    }   
                }
            }
            
        }
        
        //Return if there is an error
        if (!(fsaveSuccess))
        { 
             // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.label.CLOCT14PRM12));
             system.debug('******** List Error ->'+lstError);
             system.debug('******** List Error flag ->'+getErrMsg());
             return null;           
        }
                
        
        i=0;

        try{
            Savepoint sp = Database.setSavepoint();
            //Adding new features to the Object
          //  if(recordTypeCheck)
           // {
                if(lstNewFeatures.size() > 0)
                {
                    fsaveSuccess = true;
                    Database.UpsertResult[] srList = Database.upsert(lstNewFeatures, false);
                    for (Database.UpsertResult sr : srList) {
                        if (sr.isSuccess()) 
                        {
                            system.debug('***** Success ****');
                           // ProgramFeatureRequirements[i].UpdateStatus = 'Success';
                            ProgramFeatureRequirements[i].ProgramFeature.id  = sr.getId();
                            ProgramFeatureRequirements[i].RecordId = sr.getId();
                        }
                        else
                        {
                            System.debug('ComingHere1:Error: '+sr.getErrors()[0].getStatusCode() + ': ' + sr.getErrors()[0].getMessage());
                            ProgramFeatureRequirements[i].UpdateStatus = 'Error: '+sr.getErrors()[0].getMessage();
                            lstError.add(ProgramFeatureRequirements[i]);
                            fsaveSuccess= false;
                        }
    
                        i=i+1;
                    }
                    //if (!fsaveSuccess)
                        //Database.rollback(sp);
                }
                else if(lstNewReqirements.size() > 0)
                {
                    fsaveSuccess = true;
                    Database.UpsertResult[] srList = Database.upsert(lstNewReqirements, false);
                    for (Database.UpsertResult sr : srList) {
                        if (sr.isSuccess()) 
                        {
                            system.debug('***** Success ****');
                            //ProgramFeatureRequirements[i].UpdateStatus = 'Success';
                            ProgramFeatureRequirements[i].ProgramRequirement.id  = sr.getId();
                            ProgramFeatureRequirements[i].RecordId = sr.getId();
                        }
                        else 
                        {
                            System.debug('Error: '+sr.getErrors()[0].getStatusCode() + ': ' + sr.getErrors()[0].getMessage());
                            ProgramFeatureRequirements[i].UpdateStatus = 'Error: '+sr.getErrors()[0].getMessage();
                            lstError.add(ProgramFeatureRequirements[i]);
                            fsaveSuccess = false;
                        }

                        i=i+1;
                    }
                    //if (!fsaveSuccess)
                        //Database.rollback(sp);   
                }
           // }
           /* else
            {
                System.debug('Coming Else1');
                fsaveSuccess = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.label.CLOCT14PRM15));
            }*/

            if(lstDeletedFeatures.size() > 0)
            {
                fsaveDelSuccess = true;
                Database.DeleteResult[] delList = Database.delete(lstDeletedFeatures, false);
                for(Database.DeleteResult dr : delList)
                {
                    if (dr.isSuccess())
                    {
                        //fsaveSuccess = true;
                        //System.debug('Successfully deleted Feature with ID: ' + dr.getId());
                    }
                    else
                    {
                        ProgramFeatureRequirementWrapper tmpVar = new ProgramFeatureRequirementWrapper();
                        //tmpVar.ProgramFeature.FeatureStatus__c = System.label.CLMAY13PRM09;
                        tmpVar.ProgramFeature.Id = dr.getId();
                        tmpVar.RecordId = dr.getId();
                        tmpVar.updateStatus = 'Error: '+dr.getErrors()[0].getMessage();
                        ProgramFeatureRequirements.add(tmpVar);

                        fsaveDelSuccess = false;
                    }
                }
                //if (!fsaveSuccess)
                    //Database.rollback(sp);
            }
            else if(lstDeletedRequirements.size() > 0)
            {
                fsaveDelSuccess = true;
                Database.DeleteResult[] delList = Database.delete(lstDeletedRequirements, false);
                for(Database.DeleteResult dr : delList)
                {
                    if (dr.isSuccess())
                    {
                        //System.debug('Successfully deleted Requirement with ID: ' + dr.getId());
                    }
                    else
                    {
                        ProgramFeatureRequirementWrapper tmpVar = new ProgramFeatureRequirementWrapper();
                        //tmpVar.ProgramFeature.FeatureStatus__c = System.label.CLMAY13PRM09;
                        tmpVar.ProgramRequirement.Id = dr.getId();
                        tmpVar.RecordId = dr.getId();
                        tmpVar.updateStatus = 'Error: '+dr.getErrors()[0].getMessage();
                        ProgramFeatureRequirements.add(tmpVar);

                        fsaveDelSuccess = false;
                    }
                }
                //if (!fsaveSuccess)
                    //Database.rollback(sp);
            }

            if (!(fsaveSuccess && fsaveDelSuccess))
            {System.debug('Else2');
              //  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.label.CLOCT14PRM12));
            }

            /*if(lstDeletedFeatures.size() >0)
                Delete lstDeletedFeatures;
            else if (lstDeletedRequirements.size() > 0)
                Delete lstDeletedRequirements;*/
        }
        catch(Exception e)
        {
           // ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            System.debug('*** Error: '+e.getMessage());
        }

       if(fsaveSuccess && fsaveDelSuccess)
       {
            PageReference SaveOrfPage = new PageReference('/'+returnID);
            return SaveOrfPage ;
       }
       else
         return null ;
    }
    
    /* Getting of Program Features based on the Program Owner
       Approver/Administrator can see the entire list       
    */
    
    public List<ProgramFeatureRequirementWrapper> getProgramFeatures() {
                                            
        String FqueryUser = 'Select Id, PartnerProgram__c, ProgramLevel__c, FeatureStatus__c, PartnerProgram__r.Name, programLevel__r.Name FROM ProgramFeature__c WHERE FeatureCatalog__c =\''+ fCat.Id +'\''+
                    ' AND RecordTypeId =\''+system.label.CLMAY13PRM06+'\''+queryUser+' order by partnerProgram__r.Name, ProgramLevel__r.Name';
                    
        System.debug('**** User Query ->'+FqueryUser);

        List<ProgramFeature__c> lstPF = Database.query(FqueryUser);          
        Map<Id, ProgramFeature__c> pfMap  = new Map<Id, ProgramFeature__c>(lstPF);  
                                                                                            
        List<ProgramFeatureRequirementWrapper> lProgFeat = new List<ProgramFeatureRequirementWrapper>();
        
        if (pfMap != null && !pfMap.isEmpty()) {
            for (String pfId : pfMap.keyset()) {
                ProgramFeature__c pf = new  ProgramFeature__c (Id=pfId,
                                     PartnerProgram__c = pfMap.get(pfId).PartnerProgram__c,
                                     ProgramLevel__c = pfMap.get(pfId).ProgramLevel__c,
                                     FeatureStatus__c = pfMap.get(pfId).FeatureStatus__c
                                     
                                 );
                ProgramFeatureRequirementWrapper tmp = new ProgramFeatureRequirementWrapper();
                tmp.RecordId = pfId;
                tmp.ProgramFeature = pf;
                tmp.SortByFeauture= pfMap.get(pfId).partnerprogram__r.Name + pfMap.get(pfId).programLevel__r.Name;
                lProgFeat.add(tmp);
            }
            lProgFeat.sort();
        }
        if (lProgFeat != null && lProgFeat.size() <= 0){
            ProgramFeatureRequirementWrapper objPrgmFeatrReqWrapper = new ProgramFeatureRequirementWrapper ();
            objPrgmFeatrReqWrapper.ProgramFeature = new ProgramFeature__c();
            objPrgmFeatrReqWrapper.ProgramFeature.FeatureStatus__c = System.label.CLMAY13PRM09;
            lProgFeat.add (objPrgmFeatrReqWrapper);
        }
        return lProgFeat;
    }

    public List<ProgramFeatureRequirementWrapper> getProgramRequirements() {
    
        String ReqQueryUser = 'Select Id, PartnerProgram__c, ProgramLevel__c, Active__c, PartnerProgram__r.Name, programLevel__r.Name FROM ProgramRequirement__c WHERE RequirementCatalog__c =\''+ rCat.Id +'\''+
                    ' AND PartnerProgram__r.RecordTypeId =\''+System.label.CLMAY13PRM15+'\''+queryUser+' order by partnerProgram__r.Name, ProgramLevel__r.Name';
                    
        System.debug('**** User Query ->'+ReqQueryUser);
                    
        List<ProgramRequirement__c> lstPR = Database.query(ReqQueryUser);          
        Map<Id, ProgramRequirement__c> rfMap  = new Map<Id, ProgramRequirement__c>(lstPR);
        
        //Map<Id, ProgramRequirement__c> rfMap  = new Map<Id, ProgramRequirement__c>([SELECT Id, PartnerProgram__c, ProgramLevel__c,Active__c FROM ProgramRequirement__c WHERE RequirementCatalog__c = :rCat.Id AND PartnerProgram__r.RecordTypeId =: System.label.CLMAY13PRM15]);
        List<ProgramFeatureRequirementWrapper> lProgReq = new List<ProgramFeatureRequirementWrapper>();
        
        if (rfMap != null && !rfMap.isEmpty()) {
            for (String prId : rfMap.keyset()) {
                ProgramRequirement__c pr = new  ProgramRequirement__c (Id=prId,
                                     PartnerProgram__c = rfMap.get(prId).PartnerProgram__c,
                                     ProgramLevel__c = rfMap.get(prId).ProgramLevel__c,
                                     Active__c = rfMap.get(prId).Active__c
                                 );
                ProgramFeatureRequirementWrapper tmp = new ProgramFeatureRequirementWrapper();
                tmp.RecordId = prId;
                tmp.ProgramRequirement = pr;
                tmp.SortByRequirement = rfMap.get(prId).partnerprogram__r.Name + rfMap.get(prId).programLevel__r.Name;
                
                lProgReq.add(tmp);
            }
            lProgReq.sort();
        }
        if (lProgReq != null && lProgReq.size() <= 0){
        ProgramFeatureRequirementWrapper objPrgmFeatrReqWrapper = new ProgramFeatureRequirementWrapper ();
        objPrgmFeatrReqWrapper.ProgramRequirement= new ProgramRequirement__c();
        objPrgmFeatrReqWrapper.ProgramRequirement.Active__c = true;
            lProgReq.add (objPrgmFeatrReqWrapper);
        }
        return lProgReq;
    }
    
    

    public pagereference doCancel(){ 
        ProgramFeatureRequirements.clear();
        PageReference orfPage = new PageReference('/'+returnID);
        return orfPage  ;
        
    }
    
    public boolean getErrMsg()
    {
        if(lstError.size() > 0 )
          return true;
        else
          return false; 
    }

 }