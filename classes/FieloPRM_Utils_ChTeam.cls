/********************************************************************
* Company: Fielo
* Developer: Julio Enrique
* Created Date: 13/03/2015
* Description: Gamify teams helper class 
********************************************************************/
public without sharing class FieloPRM_Utils_ChTeam{
    
    public static final String PRM_PROGRAM_NAME = 'PRM Is On';
    public static final String TEAM_NAME_PREFIX = 'P-FT-';

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description create completes teams with all team members, based con account -> list contacts
    *               V2: complete account's lookup to team. Refactor as a consequence
    * @return Void
    */
    public static void createGamifyAccountTeams(Set<Id> accountsIds){

        FieloEE__Program__c prmProgram = [SELECT Id FROM FieloEE__Program__c WHERE Name=:FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME limit 1];
        List<FieloCH__Team__c> newTeams = new List<FieloCH__Team__c>();
        List<FieloCH__TeamMember__c> teamMembers = new List<FieloCH__TeamMember__c>();

        Map<Id,Account> accountsMap = new Map<Id,Account>([SELECT Name,F_PRM_ProgramTeam__c, PRMAreaOfFocus__c, PRMBusinessType__c, PRMCountry__c, (SELECT Id,Name, FieloEE__Member__r.Id, FieloEE__Member__r.Name FROM Contacts WHERE FieloEE__Member__c != NULL AND FieloEE__Member__r.FieloEE__Program__r.Name=:FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME AND FieloEE__Member__r.F_PRM_ContactRegistrationStatus__c = 'Validated') FROM Account WHERE F_PRM_ProgramTeam__c= NULL AND Id IN:accountsIds]);
        List<PRMCountry__c> allCountries = [SELECT Id, Country__c, MemberCountry1__c, MemberCountry2__c, MemberCountry3__c, MemberCountry4__c, MemberCountry5__c FROM PRMCountry__c];
        
        PRMCountry__c myCountry;
        for(Account acc : accountsMap.values()){
            for(PRMCountry__c thisCountry : allCountries){
                if (acc.PRMCountry__c == thisCountry.Country__c || acc.PRMCountry__c == thisCountry.MemberCountry1__c || acc.PRMCountry__c == thisCountry.MemberCountry2__c || acc.PRMCountry__c == thisCountry.MemberCountry3__c || acc.PRMCountry__c == thisCountry.MemberCountry4__c || acc.PRMCountry__c == thisCountry.MemberCountry5__c){
                    myCountry = thisCountry;
                    break;
                }
            }
            newTeams.add(new FieloCH__Team__c(Name = FieloPRM_Utils_ChTeam.TEAM_NAME_PREFIX + acc.Name , FieloCH__Program__c = prmProgram.Id, F_PRM_AccountAreaOfFocus__c = acc.PRMAreaOfFocus__c, F_PRM_AccountBusinessType__c = acc.PRMBusinessType__c, F_PRM_Country__c = (myCountry == null ? null : myCountry.Id) ));
            myCountry = null;
        }

        FieloPRM_AP_AccountTriggers.isActive = false;
        if(!newTeams.isEmpty()){
            Savepoint sp = Database.setSavepoint();
            try{
                insert newTeams; 
            }catch(Exception e){
                Database.rollback(sp);
                return ;
            }

            //create team members and insert. Complete the accounts' team lookup. If fails returns to rollback entire transction and returns
            FieloCH__Team__c teamLoop;
            for(Account acc : accountsMap.values()){
                for(FieloCH__Team__c team: newTeams){
                    if (team.Name == FieloPRM_Utils_ChTeam.TEAM_NAME_PREFIX + acc.Name){
                        teamLoop = team;
                        break;
                    }
                }
                if (teamLoop != null){
                    acc.F_PRM_ProgramTeam__c = teamLoop.Id;
                    for(Contact cont : acc.Contacts){
                        teamMembers.add(new FieloCH__TeamMember__c(FieloCH__Team2__c = teamLoop.Id, FieloCH__Member2__c = cont.FieloEE__Member__r.Id));
                    }
                    teamLoop = null;
                }
            }

            //update accounts with its new team
            try{
                update accountsMap.values();
            }catch(Exception e){
                Database.rollback(sp);
                return ;
            }
            //insert new team members
            try{
                insert teamMembers;
            }catch(Exception e){
                Database.rollback(sp);
                return ;
            }
        }
        FieloPRM_AP_AccountTriggers.isActive = true;
    }

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description add team members to their team based on the account. doesn't add to the challenge
    *               V2: now account has lookup to team. refactor
    * @return Void
    */
    public static List<FieloCH__TeamMember__c> addTeamMember(Set<Id> membersIds){
        List<FieloCH__TeamMember__c> teamMembers = new List<FieloCH__TeamMember__c>();
        //loop members and create team members
        for(FieloEE__Member__c member : [SELECT Id, F_Account__r.F_PRM_ProgramTeam__c, (SELECT Id FROM FieloCH__TeamMembers2__r) FROM FieloEE__Member__c WHERE Id in:membersIds AND F_Account__r.F_PRM_ProgramTeam__c != NULL AND F_PRM_ContactRegistrationStatus__c = 'Validated']){
            //if the member is not subscribed in any team, add it 
            if(member.FieloCH__TeamMembers2__r.isEmpty() && member.F_Account__c != NULL && member.F_Account__r.F_PRM_ProgramTeam__c != NULL){
                teamMembers.add(new FieloCH__TeamMember__c(FieloCH__Member2__c = member.Id, FieloCH__Team2__c = member.F_Account__r.F_PRM_ProgramTeam__c));
            }
        }
        if(!teamMembers.isEmpty()){
            insert teamMembers;
        }
        return teamMembers;
    }
    

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description add a team to a challenge. Challenge must be of type Teams
    * @return Void
    */
    public static void addTeamsChallenge(Id challengeId, Set<Id> accountsIds){
        List<FieloCH__ChallengeMember__c> listChallengeMembers = new List<FieloCH__ChallengeMember__c>();
        //create teams for those account who don't have it
        Set<Id> accountsWithoutTeam = new Set<Id>();
        for(Account acc: [SELECT Id FROM Account WHERE Id IN: accountsIds AND F_PRM_ProgramTeam__c = null]){
            accountsWithoutTeam.add(acc.Id);
        }
        if(!accountsWithoutTeam.isEmpty()){
           FieloPRM_Utils_ChTeam.createGamifyAccountTeams(accountsWithoutTeam);
        }
        
        Set<Id> teamIds = new Set<Id>();
        for(Account thisAcc: [SELECT F_PRM_ProgramTeam__c FROM Account WHERE Id IN: accountsIds]){
            teamIds.add(thisAcc.F_PRM_ProgramTeam__c);
        }

        Set<Id> removeTeamIds = new Set<Id>();

        for(FieloCH__ChallengeMember__c challengeMember: [SELECT FieloCH__Status__c, FieloCH__Team__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Challenge2__c =: challengeId AND FieloCH__Team__c IN: teamIds]){
            //its unsusbscribe. need to update the record
            if(challengeMember.FieloCH__Status__c == 'Unsubscribe'){
                challengeMember.FieloCH__Status__c = null;
                listChallengeMembers.add(challengeMember);
            }
            //if its subscribed, don't create again the record
            removeTeamIds.add(challengeMember.FieloCH__Team__c);
        }

        for(Account acc: [SELECT Id FROM Account WHERE F_PRM_ProgramTeam__c IN: removeTeamIds]){
            accountsIds.remove(acc.Id);
        }

        teamIds = new Set<Id>();
        for(Account acc: [SELECT F_PRM_ProgramTeam__c FROM Account WHERE Id IN: accountsIds]){
            teamIds.add(acc.F_PRM_ProgramTeam__c);
        }

        for(Id teamId: teamIds){
            listChallengeMembers.add(new FieloCH__ChallengeMember__c(FieloCH__Team__c = teamId , FieloCH__Challenge2__c = challengeId));
        }
        
        if(!listChallengeMembers.isEmpty()){
            upsert listChallengeMembers;
        }  
    }

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description Deletes team members if the member's contact it belongs no more to the account. Create new team members if the member's contact is not on the account
    * @return Void
    */
    public static void updateTeams(List<String> accountsIds){
        //create a map of account -> set<MemberId>
        Map<Id,Set<Id>> mapAccountIdMembers = new Map<Id,Set<Id>>();
        Set<Id> membersIds = new Set<Id>();
        Set<Id> selectedTeamIds = new Set<Id>();
        for(Account acc : [SELECT F_PRM_ProgramTeam__c, (SELECT Id, FieloEE__Member__r.Id, FieloEE__Member__r.Name FROM Contacts WHERE FieloEE__Member__c != NULL AND FieloEE__Member__r.FieloEE__Program__r.Name =:FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME) FROM Account WHERE PRMUIMSID__c != null AND PRMAccount__c=true AND Id IN:accountsIds]){
            for(Contact con : acc.Contacts){
                membersIds.add(con.FieloEE__Member__c);
            }
            mapAccountIdMembers.put(acc.Id, membersIds);
            selectedTeamIds.add(acc.F_PRM_ProgramTeam__c);
        }
        List<FieloCH__TeamMember__c> toDelete = new List<FieloCH__TeamMember__c>();
        List<FieloCH__TeamMember__c> toInsert = new List<FieloCH__TeamMember__c>();

        Map<Id, FieloCH__Team__c> teamsById = new Map<Id, FieloCH__Team__c>([SELECT Id, (SELECT FieloCH__Member2__c FROM FieloCH__TeamsMember2__r) FROM FieloCH__Team__c WHERE Id IN :selectedTeamIds]);

        Set<Id> accountMembersIds = new Set<Id>();
        //loop for the teams and check with the map if the member is not anymore on the account, 
        for(Account acc : [SELECT F_PRM_ProgramTeam__c FROM Account WHERE Id IN :accountsIds]){
            accountMembersIds = mapAccountIdMembers.get(acc.Id);
            FieloCH__Team__c team = teamsById.get(acc.F_PRM_ProgramTeam__c);
            if(accountMembersIds != null){
                for(FieloCH__TeamMember__c tMember : team.FieloCH__TeamsMember2__r){
                    //if the member is not part of the account, delete
                    if(!accountMembersIds.contains(tMember.FieloCH__Member2__c)){
                        toDelete.add(tMember);
                    }
                    accountMembersIds.remove(tMember.FieloCH__Member2__c);
                }
                //the remaining membersIds means that it not exists team members, insert
                for(Id memberId : accountMembersIds){
                    toInsert.add(new FieloCH__TeamMember__c(FieloCH__Team2__c = acc.F_PRM_ProgramTeam__c, FieloCH__Member2__c = memberId));
                }
            }
        }
        delete toDelete;
        insert toInsert;
    }


    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description for new members that are validated by PC, give all rewards from the level in which the team is
    */
    public static void membersLeveling(List<FieloCH__TeamMember__c> teamMembers){
        //create map teamId => teamMembers
        Map<Id,List<FieloCH__TeamMember__c>> teamMembersMap = new Map<Id,List<FieloCH__TeamMember__c>>(); 
        Set<Id> memberIds = new Set<Id>();
        for(FieloCH__TeamMember__c tm : teamMembers){
            memberIds.add(tm.FieloCH__Member2__c);
            if(teamMembersMap.containsKey(tm.FieloCH__Team2__c)){
                teamMembersMap.get(tm.FieloCH__Team2__c).add(tm);
            }else{
                teamMembersMap = new Map<Id,List<FieloCH__TeamMember__c>>{tm.FieloCH__Team2__c => new List<FieloCH__TeamMember__c>{tm}};
            }
        }
        
        List<FieloEE__BadgeMember__c> badges = [SELECT FieloEE__Badge2__c, FieloEE__Member2__c FROM FieloEE__BadgeMember__c WHERE FieloEE__Member2__c IN:memberIds];
        List<FieloEE__BadgeMember__c> newBadgeMembers = new List<FieloEE__BadgeMember__c>();
        List<FieloCH__ChallengeMember__c> chMembersUpdate = new List<FieloCH__ChallengeMember__c>();
        //challengeId => list<ChallengeMember>
        for(FieloCH__ChallengeMember__c challengeMember: [SELECT Id, FieloCH__Team__c, FieloCH__Challenge2__c, FieloCH__ChallengeReward__c, FieloCH__ChallengeReward__r.FieloCH__Badge__c, FieloCH__Status__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Team__c IN: teamMembersMap.keySet()]){
            if(challengeMember.FieloCH__Status__c == 'Unsubscribe'){
                challengeMember.FieloCH__Status__c = null;
                chMembersUpdate.add(challengeMember);
            }
            for(FieloCH__TeamMember__c tm : teamMembersMap.get(challengeMember.FieloCH__Team__c)){
                //if the team recieved a reward, give it to the member 
                if(challengeMember.FieloCH__ChallengeReward__c != null){
                    //check that badge does not exist already
                    boolean found = false;
                    for(FieloEE__BadgeMember__c badge: badges){
                        if(badge.FieloEE__Member2__c == tm.FieloCH__Member2__c){
                            if(badge.FieloEE__Badge2__c == challengeMember.FieloCH__ChallengeReward__r.FieloCH__Badge__c){
                                found = true;
                            }
                        }
                    }
                    if(!found){
                        newBadgeMembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c = challengeMember.FieloCH__ChallengeReward__r.FieloCH__Badge__c, FieloEE__Member2__c = tm.FieloCH__Member2__c));
                    }
                }
            }
        }

        Savepoint sp = Database.setSavepoint();
        if(!newBadgeMembers.isEmpty()){
            insert newBadgeMembers;
        }
        
        try{
            if(!chMembersUpdate.isEmpty()) {
                update chMembersUpdate;
            }
        }catch(Exception e){
            //rollback and throw exception
            Database.rollback(sp);
            throw new NewChallengeMemberException();
        }
    }
    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description based on challenge members, it gets the challenge rewards and relate with the teamChallenge records. It also creates badge accounts based on the challenge reward record
    */
    public static void deliveryRewardsChallengeTeam(List<FieloCH__ChallengeMember__c> challengeMembers){
        Set<Id> setChallengeRewardsIds = new Set<Id>();
        Set<Id> setTeamsIds = new Set<Id>();
        
        for(FieloCH__ChallengeMember__c chm: challengeMembers){
            if(chm.FieloCh__ChallengeReward__c != null && chm.FieloCH__Team__c != null){
                setChallengeRewardsIds.add(chm.FieloCh__ChallengeReward__c);
                setTeamsIds.add(chm.FieloCH__Team__c);
            }
        }
        
        if((!setChallengeRewardsIds.isEmpty()) && (!setTeamsIds.isEmpty())){
            //query challenge reward to get badges
            map<Id, FieloCH__ChallengeReward__c> challengeRewardsMap = new map<Id, FieloCH__ChallengeReward__c>([SELECT Id, FieloCH__Badge__c FROM FieloCH__ChallengeReward__c WHERE Id IN: setChallengeRewardsIds AND FieloCH__Badge__c != NULL]);
            
            if(!challengeRewardsMap.isEmpty()){
                map<Id, List<Account>> mapIdTeamListAccounts = new map<Id, List<Account>>();
                for(Account acc: [SELECT Id, F_PRM_ProgramTeam__c FROM Account WHERE F_PRM_ProgramTeam__c IN: setTeamsIds]){
                    if(!mapIdTeamListAccounts.containsKey(acc.F_PRM_ProgramTeam__c)){
                        mapIdTeamListAccounts.put(acc.F_PRM_ProgramTeam__c, new List<Account>{acc});
                    }else{
                        mapIdTeamListAccounts.get(acc.F_PRM_ProgramTeam__c).add(acc);
                    }
                }
                
                List<FieloPRM_BadgeAccount__c> badgeAccountsToInsert = new List<FieloPRM_BadgeAccount__c>();
                for(FieloCH__ChallengeMember__c chm: challengeMembers){
                    if(chm.FieloCh__ChallengeReward__c != null && chm.FieloCH__Team__c != null && challengeRewardsMap.containsKey(chm.FieloCh__ChallengeReward__c) && mapIdTeamListAccounts.containsKey(chm.FieloCH__Team__c)){
                        for(Account acc: mapIdTeamListAccounts.get(chm.FieloCH__Team__c)){
                            FieloPRM_BadgeAccount__c ba = new FieloPRM_BadgeAccount__c(
                                F_PRM_Account__c = acc.Id,
                                F_PRM_Badge__c = challengeRewardsMap.get(chm.FieloCH__ChallengeReward__c).FieloCH__Badge__c
                            );
                            badgeAccountsToInsert.add(ba);
                        }
                    }
                }
                
                if(!badgeAccountsToInsert.isEmpty()){
                    insert badgeAccountsToInsert;
                }
            }
        }
    }

    /**
    * @author Pablo Cassinerio
    * @date 04/05/2016
    * @description create badge accounts for newly added accounts
    */
    public static void accountsLeveling(List<Account> accountsToProcess){
        Map<Id,List<Account>> accountsMap = new Map<Id,List<Account>>(); 
        for(Account acc : accountsToProcess){
            if(accountsMap.containsKey(acc.F_PRM_ProgramTeam__c)){
                accountsMap.get(acc.F_PRM_ProgramTeam__c).add(acc);
            }else{
                accountsMap = new Map<Id,List<Account>>{acc.F_PRM_ProgramTeam__c => new List<Account>{acc}};
            }
        }

        Set<Id> accountIds = new Set<Id>();
        List<FieloPRM_BadgeAccount__c> newBadgeAccounts = new List<FieloPRM_BadgeAccount__c>();
        Set<Id> challengesIds = new Set<Id>();
        for(FieloCH__ChallengeMember__c challengeMember : [SELECT Id, FieloCH__Team__c, FieloCH__Challenge2__c, FieloCH__ChallengeReward__c, FieloCH__ChallengeReward__r.FieloCH__Badge__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Team__c IN: accountsMap.keySet()]){
            challengesIds.add(challengeMember.FieloCH__Challenge2__c);
            for(Account acc : accountsMap.get(challengeMember.FieloCH__Team__c)){
                accountIds.add(acc.Id);

                //if the team recieved a reward, give it to the account 
                if(challengeMember.FieloCH__ChallengeReward__c != null){
                    newBadgeAccounts.add(new FieloPRM_BadgeAccount__c(F_PRM_Badge__c = challengeMember.FieloCH__ChallengeReward__r.FieloCH__Badge__c, F_PRM_Account__c = acc.Id, F_PRM_IsCurrentLevel__c = true));

                }
            }
        }

        if(!newBadgeAccounts.isEmpty()){
            insert newBadgeAccounts;
        }
    }

    public class NewChallengeMemberException extends Exception {}

}