/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 29 June 2012
    Description  : 1. findNewContact()
                       (a) To find the list of Contract for which new Contact is added/updated.
                   2. createValuechainPlayer()
                       (a) To insert/update Contract Value Chain player when a Contract's Contact is inserted/updated.
***************************************************************************************************************************/
public class AP_ContractTriggerUtils
{
/*****************************************************************************************************
    To insert/update Contract Value Chain player when a Contract's Contact is inserted/updated.
*****************************************************************************************************/   
    public static void createValuechainPlayer(list<contract> lstContract)
    {
        list<CTR_ValueChainPlayers__c> lstContractValueChnPlyr = new list<CTR_ValueChainPlayers__c>();
        //Iterate through the list of Contracts
        if(!lstContract.isEmpty())
        {
            for(Contract c : lstContract)
            {
                    CTR_ValueChainPlayers__c contractValueChnPlyr = new CTR_ValueChainPlayers__c();
                    contractValueChnPlyr.Contact__c = c.ContactName__c;
                    //contractValueChnPlyr.ContactRole__c = System.Label.CLSEP12CCC23;//sets the Role as 'Admin'
                    contractValueChnPlyr.Contract__c = c.Id;
                    lstContractValueChnPlyr.add(contractValueChnPlyr);
            }
            
            try
            {
                insert lstContractValueChnPlyr;
            }
            catch(Exception e)
            {
               lstContract[0].addError('Error occurred while creating Contract Value chain Player:'+e.getMessage()); 
            }
        }
           
    }
    
/*****************************************************************************************************
    To find the list of Contracts for which a new Contact is added or the Contact is updated
*****************************************************************************************************/
    public static void findNewContact(map<id,Contract> mapNewContract , map<id,Contract> mapOldContract)
    {
        map<id,contract> mapUpdateContract = new map<id,contract>();
        list<Contract> lstNewContract = new list<Contract>();
        for(Id contractID : mapNewContract.keySet())
        {
            if(mapNewContract.get(contractId).ContactName__c != null && mapNewContract.get(ContractId).ContactName__c != mapOldContract.get(ContractId).ContactName__c &&  mapOldContract.get(ContractId).ContactName__c != null)
                mapUpdateContract.put(contractId,mapNewContract.get(contractId));
            if(mapNewContract.get(contractId).ContactName__c != null &&  mapOldContract.get(ContractId).ContactName__c == null)
                lstNewContract.add(mapNewContract.get(contractId));               
        }
        // Call method to insert new Contract Value Chain Player records
        if(!lstNewContract.isEmpty())
            createValuechainPlayer(lstNewContract);
        //Call method to find the corresponding Value chain player and update it
        if(!mapUpdateContract.isEmpty())
            updateCVCP(mapUpdateContract);
    }
    
/*****************************************************************************************************
    To find the list of existing Contract Value chain player and update them with Contract's new  contact
*****************************************************************************************************/
    public static void updateCVCP(map<id,Contract> mapIdContract)
    {
        list<Id> lstContactId = new list<Id>();
        list<CTR_ValueChainPlayers__c> lstContractValueChnPlyr = new list<CTR_ValueChainPlayers__c>();
        map<Id,CTR_ValueChainPlayers__c> mapContractIdCVCP = new map<Id,CTR_ValueChainPlayers__c>();
        list<contract> lstContract = new list<contract>();
        for(Id contractID : mapIdContract.keySet())
        {
            lstContactId.add(mapIdContract.get(contractID).contactName__c);
        }
        lstContractValueChnPlyr = [Select id, Contact__c, Contract__c from CTR_ValueChainPlayers__c where Contract__c in :mapIdContract.keySet() and Contact__c in :lstContactId limit 10000];
        if(!lstContractValueChnPlyr.isEmpty())
        {
            for(CTR_ValueChainPlayers__c cvcp : lstContractValueChnPlyr)
                mapContractIdCVCP.put(cvcp.Contract__c , cvcp);
        }      
        for(Id contractID : mapIdContract.keySet())
        {
            /*if(mapContractIdCVCP.containsKey(contractID))
                mapContractIdCVCP.get(contractID).Contact__c = mapIdContract.get(contractID).ContactName__c;*/
            if(!mapContractIdCVCP.containsKey(contractID ))
                lstContract.add(mapIdContract.get(contractID ));
        }
      /*  try
        {
            update mapContractIdCVCP.values();
        }
        catch(Exception e)
        {
           lstContractValueChnPlyr[0].addError('Error occurred while creating Contract Value chain Player:'+e.getMessage()); 
        }*/
        //Insert new record in case no matching records are found
        if(!lstContract.isEmpty())
            createValuechainPlayer(lstContract);
        
    }
    
/*****************************************************************************************************
    To find the list of related Cases and update the Contract Type
*****************************************************************************************************/
     public static void updateContractTypeonCase(map<id,Contract> mapOIdContract,map<id,Contract> mapNewContract)
    {

        map<Id,Contract> mapNewContractType = new map<Id,Contract>();
        map<Id,Contract> mapNewContractLevel = new map<Id,Contract>();
        for(Id contractId : mapNewContract.keySet())
        {


            if((mapNewContract.get(contractId).ContractType__c != mapOIdContract.get(contractId).ContractType__c))
            {
               
                mapNewContractType.put(contractId,mapNewContract.get(contractId));
            }
            if((mapNewContract.get(contractId).SupportContractLevel__c != mapOIdContract.get(contractId).SupportContractLevel__c))
                mapNewContractLevel.put(contractId,mapNewContract.get(contractId));
        }

        list<Case> lstCase = [Select id,relatedContract__c, ContractType__c,supportcontractlevel__c from Case where (relatedcontract__c in :mapNewContractType.keySet() or relatedcontract__c in :mapNewContractLevel.keySet()) and status!=: System.Label.CLSEP12CCC01 and status!=:System.Label.CLOCT14CCC20 limit 10000];
        Map<String, CS_SupportContractLevel__c > mapCustomSetting = CS_SupportContractLevel__c.getAll();

        map<String,String> mapSupportLevel = new map<String,String>();
        for(String level : mapCustomSetting.keySet())
            mapSupportLevel.put(mapCustomSetting.get(level).SupportContractLevel__c , level);


        for(Case c : lstCase)
        {



            if(mapNewContractType.containsKey(c.relatedContract__c))
                c.ContractType__c = mapNewContractType.get(c.relatedContract__c).contracttype__c;
            if(mapNewContractLevel.containsKey(c.relatedContract__c))
                if(mapSupportLevel.get(mapNewContractLevel.get(c.relatedContract__c).SupportContractLevel__c) < mapSupportLevel.get(c.SupportContractLevel__c) || c.SupportContractLevel__c == null)
                     c.SupportContractLevel__c = mapNewContractLevel.get(c.relatedContract__c).SupportContractLevel__c;
        }
       update lstCase;        
    }
    /*****************************************************************************************************
    To create Opportunity if Contract is getting expired.
    //Update by Vimal, Moved the Logic of Opportunity Creation to the Class Q2 2016 Release
    *****************************************************************************************************/
    public static void createOpptyForExpiringContract(map<id,Contract> mapOIdContract, map<id,Contract> mapNewContract){
        List<Opportunity> lstOpptyForExpContract = new List<Opportunity>();
        Set<ID> accountIds = new Set<ID>();
        Map<ID, Account> accountMap = new Map<ID, Account>();
  
        for(Contract objContract:mapNewContract.values()){
            if(!accountIds.contains(objContract.AccountId)){
                accountIds.add(objContract.AccountId);
            }
        }
  
        if(accountIds.size() > 0){
            List<Account> accounts = [select id, Name, Country__c from Account where id in :accountIds];
            if(accounts.size() > 0){
                for(Account account:accounts){
                    if(!accountMap.containskey(account.id)){
                        accountMap.put(account.id, account);
                    }
                }
            }
        }

        for(Contract objContract:mapNewContract.values()){
            if(objContract.TECH_CreateOpportunityRenewal__c && objContract.TECH_CreateOpportunityRenewal__c != mapOIdContract.get(objContract.Id).TECH_CreateOpportunityRenewal__c){
                if(accountMap.containsKey(objContract.AccountId)){
                    Opportunity opportunity = AP38_ContractTrigger.createNewContractOpportunity(objContract, accountMap.get(objContract.AccountId));
                    lstOpptyForExpContract.add(opportunity);
                } 
            }
        }
  
        if(!lstOpptyForExpContract.isEmpty()){
            insert lstOpptyForExpContract;
        }
    }
    /*****************************************************************************************************
    To caluclate the OwnerExpirationNoticeDate when Owner fills in the OwnerExpirationNotice Picklist
    //Also calculate the Proactive Case Creation Dates, if Contract need Proactive Case creation (Commented Code,as its decided to use static value than custom label)
    //Added by Vimal K (GD Team), For Q2 2016 Release  (BR-9334)
    *****************************************************************************************************/
    public static void calculateNotificationDates(List<Contract> lstOldContract, List<Contract> lstNewContract, map<id,Contract> mapOIdContract,map<id,Contract> mapNewContract){
        for(Contract objContract: lstNewContract){
            if(mapOIdContract!=null){
              //if((objContract.StartDate != mapOIdContract.get(objContract.Id).StartDate) || (objContract.ContractTerm != mapOIdContract.get(objContract.Id).ContractTerm) || (objContract.OwnerExpirationNotice != mapOIdContract.get(objContract.Id).OwnerExpirationNotice) || (objContract.AutomaticProactiveCase__c != mapOIdContract.get(objContract.Id).AutomaticProactiveCase__c)){
                if((objContract.StartDate != mapOIdContract.get(objContract.Id).StartDate) || (objContract.ContractTerm != mapOIdContract.get(objContract.Id).ContractTerm) || (objContract.OwnerExpirationNotice != mapOIdContract.get(objContract.Id).OwnerExpirationNotice)){
                    Date dtEndDate = objContract.StartDate.addMonths(objContract.ContractTerm);
                    if(objContract.OwnerExpirationNotice == null || objContract.OwnerExpirationNotice == ''){
                        objContract.OwnerExpirationNoticeDate__c = null;
                    }
                    else{
                        objContract.OwnerExpirationNoticeDate__c = dtEndDate.addDays(-(Integer.valueof(objContract.OwnerExpirationNotice)));
                    }
                    /*
                    if((objContract.StartDate != mapOIdContract.get(objContract.Id).StartDate) || (objContract.ContractTerm != mapOIdContract.get(objContract.Id).ContractTerm) || (objContract.AutomaticProactiveCase__c)){
                        objContract.TECH_FirstProactiveCaseCreationDate__c = objContract.StartDate.addDays((Integer.valueof(System.Label.CLJUN16CCC02)));                            
                        objContract.TECH_SecondProactiveCaseCreationDate__c = dtEndDate.addDays(-(Integer.valueof(System.Label.CLJUN16CCC03)));
                    }
                    if(!objContract.AutomaticProactiveCase__c){
                        objContract.TECH_FirstProactiveCaseCreationDate__c  = null;
                        objContract.TECH_SecondProactiveCaseCreationDate__c = null;    
                    }
                    */
                }
            }
            else if(objContract.OwnerExpirationNotice !=null && objContract.OwnerExpirationNotice !=''){
                Date dtEndDate = objContract.StartDate.addMonths(objContract.ContractTerm);
                objContract.OwnerExpirationNoticeDate__c = dtEndDate.addDays(-(Integer.valueof(objContract.OwnerExpirationNotice)));
                /*
                if(objContract.AutomaticProactiveCase__c){
                    objContract.TECH_FirstProactiveCaseCreationDate__c = objContract.StartDate.addDays((Integer.valueof(System.Label.CLJUN16CCC02)));                            
                    objContract.TECH_SecondProactiveCaseCreationDate__c = dtEndDate.addDays(-(Integer.valueof(System.Label.CLJUN16CCC03)));
                }
                */
            }
            /*
            else if(objContract.AutomaticProactiveCase__c){
                Date dtEndDate = objContract.StartDate.addMonths(objContract.ContractTerm);
                objContract.TECH_FirstProactiveCaseCreationDate__c = objContract.StartDate.addDays((Integer.valueof(System.Label.CLJUN16CCC02)));                            
                objContract.TECH_SecondProactiveCaseCreationDate__c = dtEndDate.addDays(-(Integer.valueof(System.Label.CLJUN16CCC03)));
            }
            */
        }
    }
    /*****************************************************************************************************
    Create Proactive Case once date is passed based on TECH_TriggerFirstProactiveCase__c 
    // and TECH_TriggerSecondProactiveCase__c
    //Added by Vimal K (GD Team), For Q2 2016 Release  (BR-9334)
    *****************************************************************************************************/
    public static void createProactiveCaseForContracts(map<id,Contract> mapOIdContract,map<id,Contract> mapNewContract){
        List<Case> lstProactiveCases = new List<Case>();
        for(Contract objContract: mapNewContract.values()){
            if(objContract.TECH_TriggerFirstProactiveCase__c && objContract.TECH_TriggerFirstProactiveCase__c != mapOIdContract.get(objContract.Id).TECH_TriggerFirstProactiveCase__c){
                Case objCase = new Case();
                objCase.AccountId = objContract.AccountId;
                objCase.ContactId = objContract.ContactName__c;
                objCase.Team__c   = objContract.ProactiveCaseSupportTeam__c;
                objCase.Subject   = System.Label.CLJUN16CCC04 + '#1 - Contract#:' + objContract.ContractNumber;
                objCase.ProactiveCase__c = true;
                objCase.OwnerId   = System.Label.CLOCT13CCC15;
                //objCase.RelatedContract__c = objContract.Id; // No Use, Case Creation nullifying the field
                lstProactiveCases.add(objCase);
            }
            if(objContract.TECH_TriggerSecondProactiveCase__c && objContract.TECH_TriggerSecondProactiveCase__c != mapOIdContract.get(objContract.Id).TECH_TriggerSecondProactiveCase__c){
                Case objCase = new Case();
                objCase.AccountId = objContract.AccountId;
                objCase.ContactId = objContract.ContactName__c;
                objCase.Team__c   = objContract.ProactiveCaseSupportTeam__c;
                objCase.Subject   = System.Label.CLJUN16CCC04 + '#2 - Contract#:' + objContract.ContractNumber;
                objCase.OwnerId   = System.Label.CLOCT13CCC15;
                objCase.ProactiveCase__c = true;
                //objCase.RelatedContract__c = objContract.Id; // No Use, Case Creation nullifying the field
                lstProactiveCases.add(objCase);
            }
        }
        if(lstProactiveCases.size()>0){
            insert lstProactiveCases;
        }
    }
    /*****************************************************************************************************
    Create Point Registration Record on Case Closure only for Cassini Contracts
    //Added by Vimal K (GD Team), For Q2 2016 Release 
    *****************************************************************************************************/
    public static void createPointRegistration(Map<Id,Case> newCaseMap){
        Set <Id> setContractId = new Set<Id>();
        Set <Id> setCasesForPointRegistration = new Set<Id>();
        Map<Id, Id> mapContractCase = new Map<Id, Id>();
        List<PointsRegistrationHistory__c> lstPointsRegistrationHistory = new List<PointsRegistrationHistory__c>();

        try{
            for(Case objCase:newCaseMap.values()){
                setContractId.add(objCase.RelatedContract__c); 
                mapContractCase.put(objCase.RelatedContract__c,objCase.Id);
            }

            if(setContractId.size()>0){
                for(Contract objContract: [Select id, WebAccess__c from Contract where Id IN: setContractId AND WebAccess__c=:'Cassini']){
                    if(mapContractCase.containsKey(objContract.Id)){
                        setCasesForPointRegistration.add(mapContractCase.get(objContract.Id));    
                    }
                }
            }
            if(setCasesForPointRegistration.size()>0){
                for(Id CaseId:setCasesForPointRegistration){
                    if(newCaseMap.containsKey(CaseId)){
                        Case objCase = newCaseMap.get(CaseId);
                        PointsRegistrationHistory__c objPointsRegistrationHistory = new PointsRegistrationHistory__c();
                        objPointsRegistrationHistory.Contract__c = objCase.RelatedContract__c;
                        objPointsRegistrationHistory.Points__c = -(objCase.Points__c);
                        objPointsRegistrationHistory.Case__c = objCase.Id;
                        objPointsRegistrationHistory.Name = 'Case # ' +  objCase.CaseNumber;
                        lstPointsRegistrationHistory.add(objPointsRegistrationHistory);
                    }
                }
            }
    
            if(lstPointsRegistrationHistory.size()>0){
                Database.insert(lstPointsRegistrationHistory);
            }
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }
}