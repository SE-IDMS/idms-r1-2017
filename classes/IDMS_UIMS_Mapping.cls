/**
•   Created By: akhilesh bagwari
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is used to do mapping between IDMS user records and UIMS table user records
**/
public class IDMS_UIMS_Mapping {
    
    IDMSCaptureError Errorcls;    
    public User CreateIDMSUserFromUIMS(UIMS_User__c uimsUser, UIMS_Company__c uimsCompany,String context){
    SYStem.debug('context: '+context);
    system.debug('uimsUser: '+uimsUser);
    system.debug('uimsCompany :'+uimsCompany);
        User usr= new User();
        
        Map<String,String> mapJobFunction   = IDMSCheckPicklistValues.getMapJobFunctionValues();
        Map<String,String> mapJobTitle      = IDMSCheckPicklistValues.getMapJobTitleValues();
        Map<String,String> mapLanguageCode  = IDMSCheckPicklistValues.getMapLanguageCode();
        Map<String,String> mapSalutation    = IDMSCheckPicklistValues.getMapSalutation();
        Map<String,String> mapUserState     = IDMSCheckPicklistValues.getMapUserState();
        Map<String,String> mapUserCountry   = IDMSCheckPicklistValues.getMapUserCountryCode();
        
        //Check for Identity Type
        if(String.isnotblank(uimsUser.phoneId__c)){
            usr.IDMSIdentityType__c = 'Mobile';
            usr.Email               = uimsUser.phoneId__c+Label.CLQ316IDMS102;
            usr.Username            = usr.Email;  
        } else {
            usr.IDMSIdentityType__c = 'Email';
            usr.Email               = uimsUser.EMail__c;
            usr.Username            = usr.Email+Label.CLJUN16IDMS71;        
        }        
        
        usr.alias                             = 'Alias';        
        usr.IsIDMSUser__c                     = true;
        usr.IDMS_AdditionalAddress__c         = uimsUser.AddInfoAddress__c;
        usr.Country                           = IDMSCheckPicklistValues.isValidUserCountry(uimsUser.Countrycode__c,mapUserCountry);
        usr.IDMSJobDescription__c             = uimsUser.JobDescription__c;
        usr.Job_Function__c                   = IDMSCheckPicklistValues.isValidJobFunction(uimsUser.JobFunction__c,mapJobFunction);
        usr.Job_Title__c                      = IDMSCheckPicklistValues.isValidJobTitle(uimsUser.JobTitle__c,mapJobTitle);
        usr.City                              = uimsUser.LocalityName__c;
        usr.IDMSMiddleName__c                 = uimsUser.MiddleName__c;
        usr.IDMS_POBox__c                     = uimsUser.PostOfficeBox__c;
        usr.IDMSSalutation__c                 =  IDMSCheckPicklistValues.isValidSalutation(uimsUser.Salutation__c,mapSalutation);

        
       usr.IDMSCompanyFederationIdentifier__c = uimsUser.CompanyId__c;
        usr.IDMSContactGoldenId__c             = uimsUser.GoldenId__c;
        usr.IDMSPrimaryContact__c              = uimsUser.PrimaryContact__c == 'TRUE';
        
        usr.Street                            = uimsUser.Street__c;
        usr.IDMSAddInfoAddressECS__c          = uimsUser.AddInfoAddressECS__c;
        usr.IDMSCountyECS__c                  = uimsUser.CountyECS__c;
        usr.IDMSGivenNameECS__c               = uimsUser.GivenNameECS__c;
        usr.IDMSLocalityNameECS__c            = uimsUser.LocalityNameECS__c;
        usr.IDMSMiddleNameECS__c              = uimsUser.MiddleNameECS__c;
        usr.PostalCode                        = uimsUser.PostalCode__c;
        usr.IDMS_County__c                    = uimsUser.County__c;
        usr.FederationIdentifier              = uimsUser.FederatedID__c;
        
        usr.MobilePhone                       = uimsUser.Cell__c;
        usr.fax                               = uimsUser.fax__c;

        usr.IDMS_PreferredLanguage__c         = IDMSCheckPicklistValues.isValidPreferedLang(uimsUser.LanguageCode__c,mapLanguageCode);
        usr.LastName                          = uimsUser.LastName__c;
        usr.Firstname                         = uimsUser.FirstName__c;
        usr.State                             = IDMSCheckPicklistValues.isValidUserState(uimsUser.State__c,mapUserState);
        usr.Phone                             = uimsUser.Phone__c;
        usr.IDMSSnECS__c                      = uimsUser.SnECS__c;
        usr.IDMSStreetECS__c                  = uimsUser.StreetECS__c;

        // end
        usr.LocaleSidKey                      = 'en_US';
        usr.EmailEncodingKey                  ='UTF-8' ;
        usr.LanguagePreferred__c              = 'English';
        usr.TimeZoneSidKey                    = 'America/Los_Angeles';
        usr.LanguageLocaleKey                 = 'en_US';         

        if(context.equalsIgnoreCase('work')|| context.equalsIgnoreCase('@work')){
            usr.ProfileId = Label.CLQ316IDMS110;
            usr.IDMS_User_Context__c = '@Work';     
        } else{                        
            usr.ProfileId =Label.CLQ316IDMS111;
            usr.IDMS_User_Context__c = '@Home';            
        }
        if(string.isnotblank(uimsUser.Vnew__c)){
            usr.IDMS_VU_NEW__c=uimsUser.Vnew__c;
            usr.IDMS_VU_OLD__c=uimsUser.Vnew__c; 

        }else{
            usr.IDMS_VU_NEW__c= '1';
            usr.IDMS_VU_OLD__c= '1';
        }

        if (uimsCompany != null){
            Map<String,String> mapUserCompCountry = IDMSCheckPicklistValues.getMapUserCompanyCountryCode();
            Map<String,String> mapIDMSClassLevel1 = IDMSCheckPicklistValues.getMapUserClassLevel1();
            Map<String,String> mapIDMSClassLevel2 = IDMSCheckPicklistValues.getMapUserClassLevel2();
            Map<String,String> mapIDMSCompanyNbrEmployees = IDMSCheckPicklistValues.getMapUserCompanyNbrEmployees();
            Map<String,String> mapIDMSCompanyMarketServed = IDMSCheckPicklistValues.getMapUserMarketServed();
            Map<String,String> mapCompanyState = IDMSCheckPicklistValues.getMapUserCompanyState();
            
            usr.IDMSCompanyFederationIdentifier__c              = uimsCompany.FederatedId__c;
            usr.CompanyName                                     = uimsCompany.OrganizationName__c;
            usr.IDMSCompanyGoldenId__c                          = uimsCompany.GoldenId__c;
            usr.Company_Country__c                              =IDMSCheckPicklistValues.isValidPreferedLang(uimsCompany.CountryCode__c,mapUserCompCountry);
            usr.DefaultCurrencyIsoCode                          = uimsCompany.CurrencyCode__c;
            usr.IDMSClassLevel2__c                              = IDMSCheckPicklistValues.isValidPreferedLang(uimsCompany.CustomerClass__c,mapIDMSClassLevel2);
            usr.Company_Postal_Code__c                          = uimsCompany.PostalCode__c;
            usr.IDMSCompanyPoBox__c                             = uimsCompany.PostOfficeBox__c;
            usr.Company_State__c                                = IDMSCheckPicklistValues.isValidPreferedLang(uimsCompany.St__c,mapCompanyState );
            usr.Company_Address1__c                             = uimsCompany.Street__c;
            usr.Phone                                           = uimsCompany.TelephoneNumber__c;
            usr.IDMSTaxIdentificationNumber__c                  = uimsCompany.TaxIdentificationNumber__c;
            usr.IDMSCompanyHeadquarters__c                      = uimsCompany.HeadQuarter__c == 'TRUE';
            usr.IDMSClassLevel1__c                              = IDMSCheckPicklistValues.isValidPreferedLang(uimsCompany.BusinessType__c,mapIDMSClassLevel1);
            usr.Company_Address2__c                             = uimsCompany.AddInfoAddress__c; 
            usr.Company_City__c                                 = uimsCompany.PostalCity__c;
            usr.Company_Website__c                              = uimsCompany.WebSite__c;
            usr.IDMSCompanyMarketServed__c                      = uimsCompany.MarketServed__c;
            usr.IDMSCompanyNbrEmployees__c                      = IDMSCheckPicklistValues.isValidPreferedLang(uimsCompany.EmployeeSize__c,mapIDMSCompanyNbrEmployees );
            usr.IDMSCompanyCounty__c                            = uimsCompany.County__c;
            String marketSegValue                               =  getMarketSegmentValue(uimsCompany.MarketSegment__c);
            if(marketSegValue.equalsIgnoreCase('MarketSegment')){
                usr.IDMSMarketSegment__c                        = uimsCompany.MarketSegment__c;
            }else if (marketSegValue.equalsIgnoreCase('MarketSubSegment')){
                usr.IDMSMarketSubSegment__c                     = uimsCompany.MarketSegment__c;            
            }
            //ECS
            usr.IDMSCompanyNameECS__c                           = uimsCompany.NameECS__c;
            usr.IDMSCompanyStreetECS__c                         = uimsCompany.StreetECS__c;
            usr.IDMSCompanyAddInfoAddressECS__c                 = uimsCompany.AddInfoAddressECS__c;
            usr.IDMSCompanyCityECS__c                           = uimsCompany.CityECS__c;
            usr.IDMSCompanyCountyECS__c                         = uimsCompany.CountyECS__c;
            
            if(string.isnotblank(uimsCompany.Vnew__c)){
                usr.IDMS_VC_NEW__c                              = uimsCompany.Vnew__c;
                usr.IDMS_VC_OLD__c                              = uimsCompany.Vnew__c;
            }else{
                usr.IDMS_VC_NEW__c                              = '1';
                usr.IDMS_VC_OLD__c                              = '1';
            }
            
        } 

        return usr;      
    }
    
    Public void enrichUserByCompany (String context, User UserUims){        
        if(context.equalsIgnoreCase('home') || context.equalsIgnoreCase('@home')){
            Account personAccount = CreatePersonAccount(UserUims);
            system.debug('otmane person account: ' + personAccount);
            insert personAccount;
            Account actRetrieve= [Select Id, PersonContactId From Account Where Id = :personAccount.Id Limit 1][0];
            String ContactId;
            ContactId =  actRetrieve.PersonContactId;
            UserUims.ContactId = ContactId;
        }else if(context.equalsIgnoreCase('work') || context.equalsIgnoreCase('@work')){
            bFoMatchingResult__c bfoMatchingRes = AP_MatchingModule.preMatch(UserUims);
            UserUims.ContactId = bfoMatchingRes.Contact__c;
        }
    }
    
    //Create Account method takes as input userrecord and returns an Account object with updated details    
    Public  Account CreatePersonAccount(User UserRecord){        
        Errorcls = new IDMSCaptureError();   
        try{ 
            Id  PersonAccountRTId = [select id from recordtype where 
                                     isActive=true and developername ='PersonAccount' limit 1].id;
            Account UserAccount= new Account();
            UserAccount.recordtypeid        = PersonAccountRTId ;               
            UserAccount.firstname           = UserRecord.firstname;
            UserAccount.lastname            = UserRecord.lastname; 
            UserAccount.IDMS_Contact_UserExternalId__pc = UserRecord.Username;
            UserAccount.PersonEmail         = UserRecord.email;
            UserAccount.phone               = UserRecord.phone;
            UserAccount.ownerid             = Label.CLJUN16IDMS89;
            //Get list of fields to be validated from Account Fieldset.
            
            Schema.FieldSet personAccField = Schema.SObjectType.Account.fieldSets.getMap().get(Label.CLJUN16IDMS108);          
            List<Schema.FieldSetMember> personAccFieldObj=personAccField.getFields();
            IDMS_field_Mapping__c userfieldname;  
            String userfieldAPIname,accountfieldAPIname;               
            
            //Traverse through the list of fields selected in fieldset. 
            //For each account field, get matching user field from IDMS_field_Mapping custom setting 
            //Update data in Account fields with respective User Fields. 
            
            for(Schema.FieldSetMember personAccFieldObjItr:personAccFieldObj){
                accountfieldAPIname=personAccFieldObjItr.getFieldPath();
                userfieldname= IDMS_field_Mapping__c.getInstance(accountfieldAPIname);
                if(userfieldname != null){ 
                    userfieldAPIname=(String)userfieldname.get(Label.CLJUN16IDMS109);                           
                }
                if(userfieldAPIname == null){                          
                    if(UserRecord.get(userfieldAPIname) != null && (!((string)UserRecord.get(userfieldAPIname)).equalsignorecase('null')) ){   
                        userAccount.put(accountfieldAPIname,UserRecord.get(userfieldAPIname));
                    }else{
                        userAccount.put(accountfieldAPIname,'');
                    }
                } 
                
                accountfieldAPIname = '';
                userfieldAPIname = '';
            }    
            
            
            if((UserRecord.DefaultCurrencyIsoCode == null ||  UserRecord.DefaultCurrencyIsoCode == '' || UserRecord.DefaultCurrencyIsoCode.equalsignorecase('null')) && UserRecord.country != null)
            { 
                IdmsCountryMapping__c cntrmap = IdmsCountryMapping__c.getInstance(UserRecord.country);
                if(cntrmap!=null){
                    userAccount.CurrencyIsoCode = (String)cntrmap.get(Label.CLJUN16IDMS111);
                }                 
            }     
            if(Test.isRunningTest() && userAccount.personemail == 'UserCreationException15@accenture.com' ){
                Integer x =100/0;
            }  
            return  UserAccount;   
        } catch(Exception e) {
            System.debug(e.getMessage());
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            if(UserRecord.IDMS_Registration_Source__c.equalsignorecase(Label.CLJUN16IDMS72)){
                Errorcls.IDMScreateLog(null,Label.CLJUN16IDMS73,Label.CLJUN16IDMS107,UserRecord.username ,ExceptionMessage ,UserRecord.IDMS_Registration_Source__c,Datetime.now(),UserRecord.IDMS_User_Context__c,UserRecord.FederationIdentifier,null,false,Label.CLJUN16IDMS98);
            }else{
                Errorcls.IDMScreateLog(Label.CLJUN16IDMS74,Label.CLJUN16IDMS73,Label.CLJUN16IDMS107,UserRecord.username ,ExceptionMessage ,UserRecord.IDMS_Registration_Source__c,Datetime.now(),UserRecord.IDMS_User_Context__c,UserRecord.FederationIdentifier,null,false,Label.CLJUN16IDMS98);           
            }
        }        
        return null;  
    }
    static Map<String,String> MarketSegmentMap;
    static Map<String,String> MarketSubSegmentMap;
    //Get market segment map    
    public Static Map<String,String> getMarketSegmentMap() {
        if(MarketSegmentMap == null) {
            
            MarketSegmentMap = new Map<String,String>();
            Schema.DescribeFieldResult marketseg = Account.MarketSegment__c.getDescribe();
            List<Schema.PicklistEntry> marketsegPle = marketseg.getPicklistValues();
            for( Schema.PicklistEntry f : marketsegPle){
                MarketSegmentMap.put(f.getValue(),f.getValue());
            }        
        }
        
        return MarketSegmentMap;
    }
    //Get sub market segment map
    public Static Map<String,String> getMarketSubSegmentMap() {
        if(MarketSubSegmentMap== null) {
            
            MarketSubSegmentMap = new Map<String,String>();
            Schema.DescribeFieldResult marketSubseg = Account.MarketSubSegment__c.getDescribe();
            List<Schema.PicklistEntry> MarketSubSegPle = marketSubseg.getPicklistValues();
            for( Schema.PicklistEntry f : MarketSubSegPle ){
                MarketSubSegmentMap .put(f.getValue(),f.getValue());
            }        
        }
        
        return MarketSubSegmentMap ;
    }
    //Get market segment values
    Public Static String getMarketSegmentValue(String MarketSegment){
        String res='';       
        if(IDMS_UIMS_Mapping.getMarketSegmentMap().containsKey(MarketSegment)){
            res = 'MarketSegment';
        }
        
        if( res == null && IDMS_UIMS_Mapping.getMarketSubSegmentMap().containsKey(MarketSegment)){
            res = 'MarketSubSegment';
        }
        return res;
    } 
}