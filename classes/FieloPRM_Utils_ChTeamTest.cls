/********************************************************************
* Company: Fielo
* Created Date: 13/03/2015
* Description: Test class for class FieloPRM_Utils_ChTeam
********************************************************************/
@isTest
public class FieloPRM_Utils_ChTeamTest{
    
    public static testMethod void testUnit1(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;        

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;

            Account acc = new Account(
                Name = 'acc',
                PRMCountry__c = country.id
            );
            insert acc;
            
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});
            
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});

            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'LastName', 
                FieloEE__FirstName__c = 'FirstName' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test', 
                F_Account__c = acc.Id
            );
            insert member;
            
            test.starttest();

            member.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member;
            
            FieloPRM_Utils_ChTeam.addTeamMember(new Set<Id>{member.Id});
            
            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'teams', 
                FieloCH__Mode__c = 'Teams', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Closed', 
                FieloCH__InitialDate__c = Date.Today().addDays(-5), 
                FieloCH__FinalDate__c = Date.today().addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            insert challenge;

            FieloCH__Team__c team = new FieloCH__Team__c(Name = 'Test', F_PRM_Country__c = countrycluster.Id, FieloCH__Program__c = program.Id);
            insert team;
            //[SELECT Id FROM FieloCH__Team__c LIMIT 1];
            
            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Status__c = 'Unsubscribe',
                FieloCH__Team__c = team.Id,
                FieloCH__Challenge2__c = challenge.Id
            );
            insert challengeMember;
        
            FieloPRM_Utils_ChTeam.addTeamsChallenge(challenge.Id,new Set<Id>{acc.Id});
            
            test.stoptest();
                    
        }

    }

    public static testMethod void testUnit2(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;        

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;

            Account acc = new Account(
                Name = 'acc',
                PRMCountry__c = country.id
            );
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'LastName', 
                FieloEE__FirstName__c = 'FirstName' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test', 
                F_Account__c = acc.Id
            );
            insert member;
            
            test.starttest();

            member.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member;
                        
            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'teams', 
                FieloCH__Mode__c = 'Teams', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Closed', 
                FieloCH__InitialDate__c = Date.Today().addDays(-5), 
                FieloCH__FinalDate__c = Date.today().addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            insert challenge;
            
            FieloPRM_Utils_ChTeam.addTeamsChallenge(challenge.Id,new Set<Id>{acc.Id});
            
            test.stoptest();
        
        }

    }

    public static testMethod void testUnit3(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;        

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;

            Account acc1 = new Account(
                Name = 'acc1',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0001',
                PRMAccount__c = true
            );

            Account acc2 = new Account(
                Name = 'acc2',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0002',
                PRMAccount__c = true
            );
            
            insert new List<Account>{acc1,acc2};
            
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc1.Id});

            FieloEE__Member__c member1 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName1', 
                FieloEE__FirstName__c = 'FirstName1' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test1', 
                F_Account__c = acc1.Id
            );

            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName2', 
                FieloEE__FirstName__c = 'FirstName2' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test2', 
                F_Account__c = acc1.Id
            );
            
            insert new List<FieloEE__Member__c>{member1,member2};
            
            test.starttest();

            member1.F_PRM_ContactRegistrationStatus__c = 'Validated';
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            update new List<FieloEE__Member__c>{member1,member2};
            
            FieloPRM_Utils_ChTeam.addTeamMember(new Set<Id>{member1.Id,member2.Id});
            
            FieloEE__Member__c member3 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName2', 
                FieloEE__FirstName__c = 'FirstName2' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test2', 
                F_Account__c = acc1.Id
            );
            insert member3;
            
            member3.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            member2.F_Account__c = acc2.Id;
            
            update new List<FieloEE__Member__c>{member2,member3};
                                   
            FieloPRM_Utils_ChTeam.updateTeams(new List<String>{acc1.Id});
            
            test.stoptest();
                    
        }

    }

    public static testMethod void testUnit4(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;        

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;

            Account acc1 = new Account(
                Name = 'acc1',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0001',
                PRMAccount__c = true
            );

            Account acc2 = new Account(
                Name = 'acc2',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0002',
                PRMAccount__c = true
            );
            
            insert new List<Account>{acc1,acc2};
            
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc1.Id});

            FieloEE__Member__c member1 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName1', 
                FieloEE__FirstName__c = 'FirstName1' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test1', 
                F_Account__c = acc1.Id
            );

            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName2', 
                FieloEE__FirstName__c = 'FirstName2' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test2', 
                F_Account__c = acc1.Id
            );
            
            insert new List<FieloEE__Member__c>{member1,member2};
            
            test.starttest();

            member1.F_PRM_ContactRegistrationStatus__c = 'Validated';
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            update new List<FieloEE__Member__c>{member1,member2};
            
            FieloPRM_Utils_ChTeam.addTeamMember(new Set<Id>{member1.Id,member2.Id});
            
            FieloEE__Member__c member3 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName2', 
                FieloEE__FirstName__c = 'FirstName2' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test2', 
                F_Account__c = acc1.Id
            );
            insert member3;

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'teams', 
                FieloCH__Mode__c = 'Teams', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Closed', 
                FieloCH__InitialDate__c = Date.Today().addDays(-5), 
                FieloCH__FinalDate__c = Date.today().addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            insert challenge;
            
            FieloCH__Team__c team = new FieloCH__Team__c(Name = 'Test', F_PRM_Country__c = countrycluster.Id, FieloCH__Program__c = program.Id);
            insert team;
            
            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Status__c = 'Unsubscribe',
                FieloCH__Team__c = team.Id,
                FieloCH__Challenge2__c = challenge.Id
            );
            insert challengeMember;  
            
            member3.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            member2.F_Account__c = acc2.Id;
            
            update new List<FieloEE__Member__c>{member2,member3};
                                   
            FieloPRM_Utils_ChTeam.updateTeams(new List<String>{acc1.Id});
            
            test.stoptest();
                    
        }

    }

    public static testMethod void testUnit5(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;        

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;

            Account acc1 = new Account(
                Name = 'acc1',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0001',
                PRMAccount__c = true
            );

            Account acc2 = new Account(
                Name = 'acc2',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0002',
                PRMAccount__c = true
            );
            
            insert new List<Account>{acc1,acc2};
            
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc1.Id});

            FieloEE__Member__c member1 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName1', 
                FieloEE__FirstName__c = 'FirstName1' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test1', 
                F_Account__c = acc1.Id
            );

            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName2', 
                FieloEE__FirstName__c = 'FirstName2' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test2', 
                F_Account__c = acc1.Id
            );
            
            insert new List<FieloEE__Member__c>{member1,member2};
            
            test.starttest();

            member1.F_PRM_ContactRegistrationStatus__c = 'Validated';
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            update new List<FieloEE__Member__c>{member1,member2};
            
            FieloPRM_Utils_ChTeam.addTeamMember(new Set<Id>{member1.Id,member2.Id});

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'teams', 
                FieloCH__Mode__c = 'Teams', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Closed', 
                FieloCH__InitialDate__c = Date.Today().addDays(-5), 
                FieloCH__FinalDate__c = Date.today().addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            insert challenge;

            FieloCH__Team__c team = new FieloCH__Team__c(Name = 'Test', F_PRM_Country__c = countrycluster.Id, FieloCH__Program__c = program.Id);
            insert team;
            
             FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Status__c = 'Unsubscribe',
                FieloCH__Team__c = team.Id,
                FieloCH__Challenge2__c = challenge.Id
            );
            insert challengeMember; 
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'Badge',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'apinametest'
            );
            insert badge;

            FieloCH__ChallengeReward__c challengeReward = new FieloCH__ChallengeReward__c(
                FieloCH__Challenge__c = challenge.Id, 
                FieloCH__Badge__c = badge.id
            );  
            insert challengeReward;  
            
            List<FieloCH__ChallengeMember__c> listChallengeMember = new List<FieloCH__ChallengeMember__c>();
            
            FieloCH__ChallengeMember__c challengemember2 = new FieloCH__ChallengeMember__c(
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__Member__c=member1.Id,
                FieloCH__Team__c = team.Id
            );
            
            listChallengeMember.add(challengeMember2);
            
            insert listChallengeMember;
            
            listChallengeMember[0].FieloCh__ChallengeReward__c = challengeReward.Id;
            
            update listChallengeMember;

            acc1.F_PRM_ProgramTeam__c = team.Id;
            
            update acc1;
            
            FieloPRM_Utils_ChTeam.deliveryRewardsChallengeTeam(listChallengeMember);

            FieloEE__Member__c member3 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName3', 
                FieloEE__FirstName__c = 'FirstName3' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test3', 
                F_Account__c = acc1.Id
            );
            
            insert member3;
            
            member3.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            update member3;
            
            test.stoptest();

        }

    }
    public static testMethod void testUnit6(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;        

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;

            Account acc1 = new Account(
                Name = 'acc1',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0001',
                PRMAccount__c = true
            );

            Account acc2 = new Account(
                Name = 'acc2',
                PRMCountry__c = country.id,
                PRMUIMSID__c = 'test0002',
                PRMAccount__c = true
            );
            
            insert new List<Account>{acc1,acc2};
            
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc1.Id});

            FieloEE__Member__c member1 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName1', 
                FieloEE__FirstName__c = 'FirstName1' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test1', 
                F_Account__c = acc1.Id
            );

            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName2', 
                FieloEE__FirstName__c = 'FirstName2' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test2', 
                F_Account__c = acc1.Id
            );
            
            FieloEE__Member__c member3 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'LastName3', 
                FieloEE__FirstName__c = 'FirstName3' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test3', 
                F_Account__c = acc1.Id
            );
            
            insert new List<FieloEE__Member__c>{member1,member2,member3};
            
            test.starttest();

            member1.F_PRM_ContactRegistrationStatus__c = 'Validated';
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            
            update new List<FieloEE__Member__c>{member1,member2};
            
            FieloPRM_Utils_ChTeam.addTeamMember(new Set<Id>{member1.Id,member2.Id});

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'teams', 
                FieloCH__Mode__c = 'Teams', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Closed', 
                FieloCH__InitialDate__c = Date.Today().addDays(-5), 
                FieloCH__FinalDate__c = Date.today().addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            insert challenge;

            FieloCH__Team__c team = new FieloCH__Team__c(Name='Test', F_PRM_Country__c=countrycluster.Id,FieloCH__Program__c=program.Id);
            insert team;
            //FieloCH__Team__c team = [SELECT Id FROM FieloCH__Team__c LIMIT 1];
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'Badge',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'apinametest'
            );
            insert badge;

            FieloCH__ChallengeReward__c challengeReward = new FieloCH__ChallengeReward__c(
                FieloCH__Challenge__c = challenge.Id, 
                FieloCH__Badge__c = badge.id
            );  
            insert challengeReward; 
            
            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
              FieloCH__Status__c = 'Unsubscribe',
                FieloCH__Team__c = team.Id,
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__ChallengeReward__c = challengeReward.Id
            );
            
            FieloCH__ChallengeMember__c challengemember2 = new FieloCH__ChallengeMember__c(
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__Member__c = member1.Id,
                FieloCH__Team__c = team.Id
            );
            
            insert new List<FieloCH__ChallengeMember__c>{challengeMember,challengeMember2};
            
            //[SELECT Id, FieloCH__ChallengeReward__c, FieloCH__Challenge2__c, FieloCH__Team__c FROM FieloCH__ChallengeMember__c LIMIT 1];
            
            challengeMember.FieloCh__ChallengeReward__c = challengeReward.Id;
            challengeMember2.FieloCh__ChallengeReward__c = challengeReward.Id;   
                             
            update new List<FieloCH__ChallengeMember__c>{challengeMember,challengeMember2};
            
            FieloPRM_Utils_ChTeam.deliveryRewardsChallengeTeam(new List<FieloCH__ChallengeMember__c>{challengeMember,challengeMember2});
            
            acc1.F_PRM_ProgramTeam__c = team.Id;
            
            update acc1;

            FieloPRM_Utils_ChTeam.accountsLeveling(new List<Account>{acc1,acc2});
            
            FieloCH__TeamMember__c teamMembers = new FieloCH__TeamMember__c(); 
            teamMembers.FieloCH__Member2__c = member3.Id;
            //teamMembers.F_PRM_Status__c    = 
            teamMembers.FieloCH__Team2__c   = team.Id;
            
            insert new List<FieloCH__TeamMember__c>{teamMembers}; 

            challengeMember.FieloCh__ChallengeReward__c = challengeReward.Id;
            challengeMember2.FieloCh__ChallengeReward__c = challengeReward.Id;              
            challengeMember.FieloCH__Status__c = 'Unsubscribe';
            challengeMember2.FieloCH__Status__c = 'Unsubscribe';   
                             
            update new List<FieloCH__ChallengeMember__c>{challengeMember,challengeMember2};
                        
            FieloPRM_Utils_ChTeam.membersLeveling(new List<FieloCH__TeamMember__c>{teamMembers});
            test.stoptest();

        }

    }
    
}