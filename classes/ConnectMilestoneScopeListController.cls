/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 28-Feb-2012
    Modification Log    : Partner-Region Added on 11-May-2012
    Description         : Controller to display Global Milestone Network 
*/

public with sharing class ConnectMilestoneScopeListController {

    public ConnectMilestoneScopeListController(ApexPages.StandardController controller) {

    }
    
// Declarations 
    public List<Team_Members__c> Scope = New List<Team_Members__c>();
    public string Pageid;
    public string getPageid(){
    return Pageid;
}

 
public List<MilestoneNetwork> MileNetwork = new List<MilestoneNetwork>();

public string program;
public string year;

public string getprogram(){
return program;
}

public List<MilestoneNetwork> getMilestoneNetwork(){

  Pageid = ApexPages.currentPage().getParameters().get('Id');
    List<Connect_Milestones__c> Mile = [Select id, Name, year__c, Program_Number__r.Name, Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c, Partner_Region__c,Smart_Cities_Division__c from Connect_Milestones__c where id = :pageid];
    program = Mile[0].Program_Number__r.Name;
    year = Mile[0].year__c;
    Scope = [SELECT Id, Team_Name__c,Deployment_Leaders__c, Champion_Name__c, Entity__c, GSC_Region__c,Partner_Region__c,Smart_Cities_Division__c from Team_Members__c where Program_Name__c like :program and year__c = :year];
    
    
    // Loop to Check the Scope and the respective Deployment Leaders
    
    // Power Regions Scope
      
    Integer PW;  
    try{  
    if(!(Mile[0].Power_Region__c == null)){  
        for(String PR: Mile[0].Power_Region__c.Split(';')){
           PW = 0;
           for(Team_Members__c Team:Scope){
           system.debug('---PR----'+PR+'---Team.Entity__c----'+Team.Entity__c+'---TTeam_Name__c----'+Team.Team_Name__c+'------DL------'+Team.Deployment_Leaders__c+'---champion------'+Team.Champion_Name__c+'');
            if(PR == Team.Entity__c && Team.Team_Name__c == Label.Connect_Power_Region && !(Team.Deployment_Leaders__c == null)) {
                            MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,Team.GSC_Region__c,Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                PW = 1;
                } // if Ends 
             } // For Loop Ends
                 If(PW == 0){
                     MileNetwork.add(new MilestoneNetwork('',Label.Connect_Power_Region,PR,'','','','',''));
                     } // If Ends
             } // For Loop Ends
            } // If Ends
           } // Try Ends
           catch(Exception e){
              System.Debug('Error:Scope Selection Milestone ' + e);
              }// Catch Ends
           
              // Global Functions Scope with atleast one GSC Region
            integer GFn;
          try{
            if((Mile[0].Global_Functions__c.Contains(Label.Connect_GSC)) && (Mile[0].GSC_Regions__c != null)){
            for(String GF: Mile[0].Global_Functions__c.Split(';')){                      
                           
             for(String GSC: Mile[0].GSC_Regions__c.Split(';')){ 
               GFn = 0; 
                 for(Team_Members__c Team:Scope){
                 if(GF == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leaders__c == null) && (Team.GSC_Region__c == GSC)) {
                   MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,Team.GSC_Region__c,Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GFn = 1;
                  } // If Ends
                 }  // For Loop Ends     
                         
               If(GFn == 0 && GF == Label.Connect_GSC){
                   MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Functions,GF,GSC,'','','',''));
                    } // If Ends
                             
             } // For Loop Ends
             
             if(GFn == 0 && GF != Label.Connect_GSC){
              for(Team_Members__c Team:Scope){
                 if(GF == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leaders__c == null)) {
                    MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,'','',Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GFn = 1;
                  } // If Ends
                 } // For Ends
                if (GFn == 0)
                  MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Functions,GF,'','','','',''));
                 } // If Ends 
              
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
              
           // Global Functions Scope with No GSC Region
           
          try{
            if(!(Mile[0].Global_Functions__c.Contains(Label.Connect_GSC))){          
                
              for(String GF: Mile[0].Global_Functions__c.Split(';')){
              GFn = 0;
                 for(Team_Members__c Team:Scope){
                if(GF == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leaders__c == null)) {
                    MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,Team.GSC_Region__c,Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GFn = 1;
                 } // If Ends
                }  // For Loop Ends
              
                If(GFn == 0 && GF == Label.Connect_GSC){
                    MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Functions,GF,'','','','',''));
                    } // If Ends
                 else if(GFn == 0 && GF != Label.Connect_GSC){
                     MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Functions,GF,'','','','',''));
                     } // If Ends
             
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
            
          
          // Global Business Scope No Partner Region
          
          integer GBs; 
          try{           
          if(!(Mile[0].Global_Business__c == null)&& (Mile[0].Global_Business__c != Label.Connect_Partner) && (Mile[0].Global_Business__c != Label.Connect_Smart_Cities)){  
              for(String GB: Mile[0].Global_Business__c.Split(';')){
               GBs = 0;
               for(Team_Members__c Team:Scope){
                if(GB == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Business && !(Team.Deployment_Leaders__c == null) && (Team.Partner_Region__c == null)&& (Team.Smart_Cities_Division__c == null)) {
                    MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,Mile[0].GSC_Regions__c,Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GBs = 1;
                } // If Ends
               } // For Loop Ends
                 if((GBs == 0) && (GB != Label.Connect_Partner)) {
                     MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Business,GB,Mile[0].GSC_Regions__c,'','','',''));
                     } // If Ends
              } // For Loop Ends
             } // If Ends
            }// Try Ends
             catch(Exception e){
              System.Debug('Error:Scope Selection Milestone ' + e);
              }// Catch Ends
              
        // Global Business with atleast one Partner Region
        
        integer PARn;
        integer SC;
          try{
            if((Mile[0].Global_Business__c.Contains(Label.Connect_Partner)) && (Mile[0].Partner_Region__c != null)){
            for(String GB: Mile[0].Global_Business__c.Split(';')){                      
                           
             for(String PAR: Mile[0].Partner_Region__c.Split(';')){ 
               PARn = 0; 
                 for(Team_Members__c Team:Scope){
                 if(GB == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Business  && !(Team.Deployment_Leaders__c == null) && (Team.Partner_Region__c == PAR)) {
                   MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,'',Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    PARn = 1;
                  } // If Ends
                 }  // For Loop Ends     
                         
               If(PARn  == 0 && GB == Label.Connect_Partner){
                   MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Business,GB,'',PAR,'','',''));
                    } // If Ends
               }  // For Loop Ends  
               } // If Ends
               }// For Ends
                  
               // Smart Cities
               
           if((Mile[0].Global_Business__c.Contains(Label.Connect_Smart_Cities)) && (Mile[0].Smart_Cities_Division__c != null)){
            for(String GB: Mile[0].Global_Business__c.Split(';')){    
                    
              for(String SMC: Mile[0].Smart_Cities_Division__c.Split(';')){ 
               SC = 0; 
                 for(Team_Members__c Team:Scope){
                 if(GB == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Business  && !(Team.Deployment_Leaders__c == null) && (Team.Smart_Cities_Division__c == SMC)) {
                   MileNetwork.add(new MilestoneNetwork(Team.id,Team.Team_Name__c,Team.Entity__c,'',Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    SC = 1;
                  } // If Ends
                 }  // For Loop Ends 
                 
                 If(SC  == 0 && GB == Label.Connect_Smart_Cities){
                   MileNetwork.add(new MilestoneNetwork('',Label.Connect_Global_Business,GB,'','','','',SMC));
                    } // If Ends                         
        
                } //for Loop Ends       
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
           
    return MileNetwork;
  }

public class MilestoneNetwork{
    public String id {get; set;}
    public String Scope {get; set;}
    public String Entity {get; set;}
    public String GSC_Region {get;set;}
    public String Partner_Region {get;set;}
    public String Deployment_Leader {get;set;}
    public String Champions{get;set;}
    public String SmartCity{get;set;}

    public MilestoneNetwork(string id,string s,string e, string gsc, string par, string d, string c,string sc){
        this.id = id;
        this.Scope=s;
        this.Entity=e;
        this.GSC_Region=gsc;
        this.Partner_Region = par;
        this.Deployment_Leader=d;
        this.Champions=c;
        this.SmartCity=sc;

    }
   }
}