//Asynchronous method to update Account fields related AMP
Public class AP_AccountUpdateFromAMP implements Queueable
{
    public List<AccountMasterProfile__c> ampList=new List<AccountMasterProfile__c>();
    public Map<id,account> accMap=new map<id,account>();
    Map<String,Decimal> currencyMap = new Map<String,Decimal>();
        
    public AP_AccountUpdateFromAMP(List<AccountMasterProfile__c> ampListTriggerNew){
        ampList = ampListTriggerNew;
    }
    
    public void execute(QueueableContext context)
    {
        List<account> accUpdateList=new List<account>();        
        // getting all the account ids from the trigger.new
        List<id> accidList=new List<id>();
        for(AccountMasterProfile__c amp:ampList)
            accidList.add(amp.account__c);

        List<account> accList=[select id,AccountMasterProfileLastUpdated__c,TotalPAM__c,AccountMasterProfile__c,CurrencyIsocode,ClassLevel1__c,Country__c,MarketSegment__c from account where id in :accidList];
        for(Account a:acclist)
            accmap.put(a.id,a);

         //Fetch currecy conversion rate
        for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
            currencyMap.put(ct.IsoCode,ct.ConversionRate);        
        String accCurrency;      
        for(AccountMasterProfile__c amp:ampList){
            Account acc=new Account();
            accCurrency=null;      
            if(accmap.containsKey(amp.Account__c)){
                acc=accmap.get(amp.Account__c); 
                if(currencyMap.containsKey(acc.CurrencyIsoCode))
                    accCurrency=acc.CurrencyIsoCode;
                else
                    accCurrency='EUR';// for inactive currency
                acc.AccountMasterProfileLastUpdated__c=amp.LastModifiedDate.Date();
                acc.AccountMasterProfile__c=amp.AccountMasterProfile__c;
                if(amp.TotalPAM__c!=null)
                    acc.TotalPAM__c= (amp.TotalPAM__c * currencyMap.get(accCurrency))/currencyMap.get(amp.CurrencyIsoCode); 
                acc.Tech_CL1Updated__c=False;
                if(acc.CurrencyIsoCode!=accCurrency)
                    acc.CurrencyIsoCode=accCurrency; //for inactive account currency
                accUpdatelist.add(acc);
           }
        }
        try{
            Database.update(accUpdateList,false);
        }
        catch(Exception ex){
            System.debug('**Exception Caught AP_AccountUpdateFromAMP**'+ex.getmessage());
        }
    }
}