/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For PRM May 13 Release:
                 1. To insert all the child records when a new Contact Assigned Program is created.
                 2. To deactivate all the child records when the Parent Program is deactivated.
                 3. To delete all the old child records when the related Program Level chenges and add the new child records.
    
********************************************************************************************************************/
public class AP_CON_ContactProgramUpdate
{
/***************************************************************************************************
    To insert all the child records when a new Contact Assigned Program is created.
***************************************************************************************************/
    public static void createCONChildRecords(list<ContactAssignedProgram__c> lstConProgram)
    {
         //apr 14 prm
        AP_ProcessFeatureAssignment.processContactAssignedFeatures(lstConProgram);

        list<Id> lstLevelID = new list<Id>();
        list<ContactAssignedFeature__c> lstConFeature = new list<ContactAssignedFeature__c>();
        list<ProgramFeature__c> lstPRGFeature = new list<ProgramFeature__c>();
        map<Id,list<ProgramFeature__c>> mapFeatureLevel = new map<Id,list<ProgramFeature__c>>();
        
        list<ProgramRequirement__C> lstPRGReq = new list<ProgramRequirement__c>();
        list<FeatureRequirement__c> lstFTRReq = new list<FeatureRequirement__c>();
        list<ContactAssignedRequirement__c> lstConReq = new list<ContactAssignedRequirement__c>();
        map<Id,list<ProgramFeature__c>> mapLevelFeature = new map<Id,list<ProgramFeature__c>>(); 
        map<Id,list<ProgramRequirement__c>> mapLevelPRGReq = new map<Id,list<ProgramRequirement__c>>();
        map<Id,list<FeatureRequirement__c>> mapLevelFeatureReq = new map<Id,list<FeatureRequirement__c>>();
        map<String,list<ID>> mapProgramConFeature = new map<String,list<ID>>();
        
        set<id> conIds = new set<id>();
        for(ContactAssignedProgram__c conProgram : lstConProgram)
        {
            lstLevelID.add(conProgram.ProgramLevel__c);
            conIds.add(conProgram.contact__c);
            
        }
        //apr 14 prm
        list<ContactAssignedFeature__c> extConFeatures = [Select Contact__c,FeatureCatalog__c, Active__c from ContactAssignedFeature__c where Contact__c in: conIds];
        map<id,set<id>> mpConIdtoFea = new map<id,set<id>>();

        for(ContactAssignedFeature__c tcaf: extConFeatures)
        {
          if(tcaf.Active__c)
          {
            if(mpConIdtoFea.containsKey(tcaf.Contact__c) )
              mpConIdtoFea.get(tcaf.Contact__c).add(tcaf.FeatureCatalog__c);
            else
            {
              set<id> tset = new set<id>(); tset.add(tcaf.FeatureCatalog__c);
              mpConIdtoFea.put(tcaf.Contact__c, tset);
            }  
          }
        }
        /*********************************Contact Assigned Feature********************/
        
        lstPRGFeature = [Select id,FeatureCatalog__c,programlevel__c from ProgramFeature__c where ProgramLevel__c in :lstLevelID and FeatureStatus__c = : Label.CLMAY13PRM09 and (Enabled__c = :Label.CLMAY13PRM11 or  Visibility__c =: Label.CLMAY13PRM11) limit 10000];
        if(!lstPRGFeature.isEmpty())
        {
            for(ProgramFeature__c prgFTR : lstPRGFeature)
            {
                if(!mapLevelFeature.containsKey(prgFTR.ProgramLevel__c))
                    mapLevelFeature.put(prgFTR.ProgramLevel__c, new list<ProgramFeature__c> {(prgFTR)});
                else
                    mapLevelFeature.get(prgFTR.ProgramLevel__c).add(prgFTR);
            }
            
            set<string> cafDuplicateCheck = new set<string>(); // Srinivas fix apr 14
            for(ContactAssignedProgram__c conProgram : lstConProgram)
            {
                
                if(mapLevelFeature.containsKey(conProgram.ProgramLevel__c))
                {
                    for(ProgramFeature__c prgFTR : mapLevelFeature.get(conProgram.ProgramLevel__c))
                    {
                        if(!( mpConIdtoFea.containsKey(conProgram.Contact__c) && mpConIdtoFea.get(conProgram.Contact__c).contains(prgFTR.FeatureCatalog__c)) )
                        {
                          ContactAssignedFeature__c conFeature = new ContactAssignedFeature__c();
                          conFeature.Contact__c = conProgram.Contact__c;
                          //conFeature.PartnerProgram__c = conProgram.PartnerProgram__c;
                          //conFeature.ProgramFeature__c = prgFTR.Id;
                          //conFeature.ProgramLevel__c = conProgram.ProgramLevel__c;//prgFTR.ProgramLevel__c;
                          //conFeature.ContactAssignedProgram__c = conProgram.Id;
                          //Added by Shruti Karn : for October 13 Release
                          conFeature.FeatureCatalog__c = prgFTR.FeatureCatalog__c;
                          conFeature.RecordTypeID = Label.CLOCT13PRM22;
                          string cafKey =   conFeature.Contact__c+'-'+conFeature.FeatureCatalog__c;
                          if(!cafDuplicateCheck.contains(cafKey))//Not contains
                            lstConFeature.add(conFeature);
                          cafDuplicateCheck.add(cafKey);     
                        } 
                    }
                }
                
            }
            
            for(ProgramFeature__c prgFTR : lstPRGFeature)
            {
                if(!mapFeatureLevel.containsKey(prgFTR.FeatureCatalog__c))
                    mapFeatureLevel.put(prgFTR.FeatureCatalog__c,new list<ProgramFeature__c> {(prgFTR)});//.programlevel__c);
                else
                    mapFeatureLevel.get(prgFTR.FeatureCatalog__c).add(prgFTR);//.programlevel__c);
            }
                       
            if(!lstConFeature.isEmpty())
            {
               Database.SaveResult[] SaveResult1 = database.insert(lstConFeature,false);
               for(Integer i=0;i<SaveResult1.size();i++ )
               {
                   Database.SaveResult sr =SaveResult1[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      system.debug('err:'+err);
                      //lstAccProgram[0].addError( err.getMessage()); 
                         
                   }
                }   
                //hotFix modified on 08-Dec-2014, EUS:049494
                lstConFeature = [Select Id, ProgramFeature__c,Contact__c, ProgramLevel__C, ProgramFeature__r.FeatureCatalog__c from ContactAssignedFeature__c where id in:lstConFeature limit 10000];                               
                for(ContactAssignedFeature__c conFeature : lstConFeature)
                {
                    if(!mapProgramConFeature.containsKey(conFeature.ProgramFeature__r.FeatureCatalog__c))
                        mapProgramConFeature.put(conFeature.Contact__c + ':'+conFeature.programlevel__c ,new list<Id> {(conFeature.Id)});//to relate Account Feature and Account Requirement
                    else
                        mapProgramConFeature.get(conFeature.Contact__c + ':'+conFeature.programlevel__c).add(conFeature.Id);//to relate Account Feature and Account Requirement
                }
                   
               
            }
        }
        /*********************************Account Assigned Requirement********************/
        
        lstPRGReq = [Select id,RequirementCatalog__c,RequirementCatalog__r.RecordTypeId, certification__c,programlevel__c from ProgramRequirement__C where PRogramLevel__c in:lstLevelID and active__c = true and RequirementCatalog__r.Applicableto__c =: Label.CLMAY13PRM11 limit 10000];
        lstFTRReq = [Select id, RequirementName__c,RequirementName__r.RecordTypeId, TECH_CertificationID__c,FeatureName__c from FeatureRequirement__C where FeatureName__c in :mapFeatureLevel.keySet() and RequirementName__r.Applicableto__c =: Label.CLMAY13PRM11 limit 10000];
        

        for(ProgramRequirement__c prgReq : lstPRGReq)
        {
            if(!mapLevelPRGReq.containsKey(prgReq.ProgramLevel__c))
                mapLevelPRGReq.put(prgReq.ProgramLevel__c , new list<ProgramRequirement__c> {(prgReq)});
            else
                mapLevelPRGReq.get(prgReq.ProgramLevel__c).add(prgReq);         
        }
        for(FeatureRequirement__C ftrReq : lstFTRReq)
        {
             for(ProgramFeature__c prgFTR : mapFeatureLevel.get(ftrReq.FeatureName__c))
            {
                if(!mapLevelFeatureReq.containsKey(prgFTR.programlevel__c))
                    mapLevelFeatureReq.put(prgFTR.programlevel__c , new list<FeatureRequirement__c> {(ftrReq)});
                else
                    mapLevelFeatureReq.get(prgFTR.programlevel__c).add(ftrReq);
            }
        }
        for(ContactAssignedProgram__c conProgram : lstConProgram)
        {
            
            if(mapLevelPRGReq.containsKey(conPRogram.programlevel__c))
            {
                for(ProgramRequirement__c prgReq : mapLevelPRGReq.get(conPRogram.programlevel__c))
                {
                    ContactAssignedRequirement__c conReq = new ContactAssignedRequirement__c();
                    conReq.Contact__c = conProgram.Contact__c;
                    conReq.PartnerProgram__c = conProgram.PartnerProgram__c;
                    //conReq.RequirementStatus__C = Label.CLMAY13PRM08;
                    conReq.ProgramRequirement__c = prgReq.Id;
                    conReq.Certification__c = prgReq.certification__c;
                    conReq.ContactAssignedProgram__c = conProgram.Id;
                    //conReq.recordtypeid = mapReqRecType.get(prgReq.recordtype.name).getRecordtypeid();
                    conReq.recordtypeid = CS_RequirementRecordType__c.getValues(prgReq.RequirementCatalog__r.RecordTypeId).CARRecordTypeID__c;
                    conReq.RequirementCatalog__c = prgReq.RequirementCatalog__c;
                    lstConReq.add(conReq);
                }
            }
            
           if(mapLevelFeatureReq.containsKey(conProgram.ProgramLevel__c))
            {
                for(FeatureRequirement__C ftrReq : mapLevelFeatureReq.get(conProgram.ProgramLevel__c))
                {
                    if(mapProgramConFeature.containsKey(conProgram.contact__c + ':'+conProgram.ProgramLevel__c))
                    {
                        for(Id contactFeature : mapProgramConFeature.get(conProgram.contact__c + ':'+conProgram.ProgramLevel__c))
                        {
                                                
                            ContactAssignedRequirement__c conReq = new ContactAssignedRequirement__c();
                            conReq.Contact__c = conProgram.Contact__c;
                            conReq.PartnerProgram__c = conProgram.PartnerProgram__c;
                            //conReq.RequirementStatus__C = Label.CLMAY13PRM08;
                            conReq.FeatureRequirement__c = ftrReq.Id;
                            //conReq.Certification__c = ftrReq.certification__c;
                            //conReq.recordtypeid = mapReqRecType.get(prgReq.recordtype.name).getRecordtypeid();
                            conReq.recordtypeid = CS_RequirementRecordType__c.getValues(ftrReq.RequirementName__r.RecordTypeId).CARRecordTypeID__c;
                            conReq.ContactAssignedProgram__c = conProgram.Id;
                            conReq.ContactAssignedFeature__c = contactFeature;
                            conReq.RequirementCatalog__c = ftrReq.RequirementName__c;
                            lstConReq.add(conReq);
                        }
                    }
                    
                }
            }
        }

        
       if(!lstConReq.isEmpty())
       {
           Database.SaveResult[] SaveResult = database.insert(lstConReq,false);
           for(Integer i=0;i<SaveResult.size();i++ )
           {
               Database.SaveResult sr =SaveResult[i];
               if(!sr.isSuccess())
               {
                  Database.Error err = sr.getErrors()[0];
                  system.debug('err:'+err);
                 
               }
           }
       }
        
    
    }
    
/***************************************************************************************************
    To delete all the old child records when the related Program Level chenges and add the new child records.
***************************************************************************************************/

    public static void delChildRecords(map<Id,ContactAssignedProgram__c> mapNewContactProgram , map<ID,ContactAssignedProgram__c> mapOldContactProgram)
    {
        
        list<Id> lstUpdatedContactProgramID = new list<ID>();
        list<ContactAssignedFeature__c> lstConFeature = new list<ContactAssignedFeature__c>();
        list<ContactAssignedRequirement__c> lstContactReq = new list<ContactAssignedRequirement__c>();
        
        for(ID ConPrgID : mapNewContactProgram.keySet())
        {
            if(mapNewContactProgram.get(ConPrgID).ProgramLevel__c != mapOldContactProgram.get(ConPrgID).ProgramLevel__c)
            {
                lstUpdatedContactProgramID.add(ConPrgID);
            }
        }
        if(!lstUpdatedContactProgramID.isEmpty())
        {
            lstConFeature = [Select id from ContactAssignedFeature__c where ContactAssignedProgram__c in :lstUpdatedContactProgramID limit 10000];
            lstContactReq = [Select id from ContactAssignedRequirement__c where ContactAssignedProgram__c in :lstUpdatedContactProgramID limit 10000];
            if(!lstConFeature.isEmpty())
            {
               Database.DeleteResult[] DelResult = database.delete(lstConFeature,false);
               for(Integer i=0;i<DelResult.size();i++ )
               {
                   Database.DeleteResult sr =DelResult[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      system.debug('err:'+err);
                      //lstAccProgram[0].addError( err.getMessage());               
                   }
               }
            }    
            
            if(!lstContactReq.isEmpty())
            {
               Database.DeleteResult[] DelResult2 = database.delete(lstContactReq,false);
               for(Integer i=0;i<DelResult2.size();i++ )
               {
                   Database.DeleteResult sr =DelResult2[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      system.debug('err:'+err);
                      //lstAccProgram[0].addError( err.getMessage());               
                   }
               }
            } 
            createCONChildRecords(mapNewContactProgram.values());   
        }
    }
    
/***************************************************************************************************
    To deactivate all the child records when the Parent Program is deactivated.
***************************************************************************************************/

    public static void deActivateChildRecords(map<Id,ContactAssignedProgram__c> mapNewContactProgram , map<ID,ContactAssignedProgram__c> mapOldContactProgram)
    {
        list<Id> lstUpdatedContactProgramID = new list<ID>();
        list<ContactAssignedFeature__c> lstContactFeature = new list<ContactAssignedFeature__c>();
        list<ContactAssignedRequirement__c> lstContactReq = new list<ContactAssignedRequirement__c>();
        
        for(ID ContactPrgID : mapNewContactProgram.keySet())
        {
            if(mapNewContactProgram.get(ContactPrgID).Active__c != mapOldContactProgram.get(ContactPrgID).Active__c && mapOldContactProgram.get(ContactPrgID).Active__c == true)
            {
                lstUpdatedContactProgramID.add(ContactPrgID);
            }
        }
        if(!lstUpdatedContactProgramID.isEmpty())
        {
            lstContactFeature = [Select id,ContactAssignedProgram__c,Active__c from ContactAssignedFeature__c where ContactAssignedProgram__c in :lstUpdatedContactProgramID limit 10000];
            lstContactReq = [Select id, RequirementStatus__c from ContactAssignedRequirement__c where ContactAssignedProgram__c in :lstUpdatedContactProgramID limit 10000];
            if(!lstContactFeature.isEmpty())
            {
               for(ContactAssignedFeature__c ContactFeature : lstContactFeature)
               {
                   ContactFeature.Active__c = false;
               } 
               Database.SaveResult[] UpdateResult = database.update(lstContactFeature,false);
               for(Integer i=0;i<UpdateResult.size();i++ )
               {
                   Database.SaveResult sr =UpdateResult[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      system.debug('err:'+err);
                   }
               }
            }    
            
            if(!lstContactReq.isEmpty())
            {
               for(ContactAssignedRequirement__c ContactReq : lstContactReq)
               {
                   ContactReq.RequirementStatus__c = Label.CLMAY13PRM08;
               } 
               Database.SaveResult[] UpdateResult = database.update(lstContactReq,false);
               for(Integer i=0;i<UpdateResult.size();i++ )
               {
                   Database.SaveResult sr =UpdateResult[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      system.debug('err:'+err);
                   }
               }
            }    
        }
                
    }
    
}