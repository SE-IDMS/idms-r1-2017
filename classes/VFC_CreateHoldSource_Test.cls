// Created by Uttara PR - Nov 2016 Release - Escape Notes project

@isTest
global class VFC_CreateHoldSource_Test {
    
    static testMethod void method1() {
                
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        insert runAsUser;
        Id userId = UserInfo.getUserId();
        
        System.runAs(runAsUser) {
            
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
            
            BackOfficesystem__c BOS = new BackOfficesystem__c();
            BOS.FrontOfficeOrganization__c = accOrg.Id;
            BOS.BackOfficeSystem__c = 'Oracle ITB';
            BOS.ERPKey__c = 'ITB_OrganizationID';
            BOS.ExternalID__c = '119';
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';        
            prob.RecordTypeId = Label.CLI2PAPR120014;
            insert prob;
            
            List<CommercialReference__c> crlst = new list<CommercialReference__c>();
            CommercialReference__c cr1 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family1234', ProductLine__c='1234ProductLine', CommercialReference__c='AR3100' );
            crlst.add(cr1);
            CommercialReference__c cr2 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family12345', ProductLine__c='1234ProductLine5', CommercialReference__c='AR3300' );
            crlst.add(cr2);
            CommercialReference__c cr3 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family123', ProductLine__c='123ProductLine', CommercialReference__c='' );
            crlst.add(cr3);
            CommercialReference__c cr4 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family12', ProductLine__c='12ProductLine', CommercialReference__c='0J-0N-1439' );
            crlst.add(cr4);
            insert crlst;
            
            List<ShipHoldRequest__c> shrlst = new List<ShipHoldRequest__c>();
            ShipHoldRequest__c shr1 = new ShipHoldRequest__c(Problem__c = prob.id, AffectedProduct__c = cr1.Id, BackOfficeSystem__c = BOS.Id);
            shrlst.add(shr1);
            ShipHoldRequest__c shr2 = new ShipHoldRequest__c(Problem__c = prob.id, AffectedProduct__c = cr2.Id, BackOfficeSystem__c = BOS.Id);
            shrlst.add(shr2);
            ShipHoldRequest__c shr3 = new ShipHoldRequest__c(Problem__c = prob.id, AffectedProduct__c = cr4.Id, BackOfficeSystem__c = BOS.Id);
            shrlst.add(shr3);
            insert shrlst;
            
            ApexPages.StandardSetController SHRController = new ApexPages.StandardSetController(shrlst);
            PageReference pageRef = Page.VFP_CreateHoldSource; 
            ApexPages.currentPage().getParameters().put('Id', prob.id);
            SHRController.setSelected(new ShipHoldRequest__c []{shrlst[0], shrlst[1], shrlst[2]});
            VFC_CreateHoldSource shr = new VFC_CreateHoldSource(SHRController);
            
            shr.Cancel();
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new CreateHoldSourceMock());
            shr.SendShipHold();
            Test.stopTest();
            
            ApexPages.StandardSetController SHRController1 = new ApexPages.StandardSetController(shrlst);
            PageReference pageRef1 = Page.VFP_CreateHoldSource; 
            ApexPages.currentPage().getParameters().put('Id', prob.id);
            List<ShipHoldRequest__c> shrlst1 = new List<ShipHoldRequest__c>();
            SHRController1.setSelected(shrlst1);
            VFC_CreateHoldSource shr11 = new VFC_CreateHoldSource(SHRController1);
            
        }
    }
    
    static testMethod void method2() {
                
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        insert runAsUser;
        Id userId = UserInfo.getUserId();
        
        System.runAs(runAsUser) {
            
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
            
            BackOfficesystem__c BOS = new BackOfficesystem__c();
            BOS.FrontOfficeOrganization__c = accOrg.Id;
            BOS.BackOfficeSystem__c = 'Oracle ITB';
            BOS.ERPKey__c = 'ITB_OrganizationID';
            BOS.ExternalID__c = '119';
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';        
            prob.RecordTypeId = Label.CLI2PAPR120014;
            insert prob;
            
            List<CommercialReference__c> crlst = new list<CommercialReference__c>();
            CommercialReference__c cr1 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family1234', ProductLine__c='1234ProductLine', CommercialReference__c='AR3100' );
            crlst.add(cr1);
            CommercialReference__c cr2 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family12345', ProductLine__c='1234ProductLine5', CommercialReference__c='AR3300' );
            crlst.add(cr2);
            CommercialReference__c cr3 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family123', ProductLine__c='123ProductLine', CommercialReference__c='' );
            crlst.add(cr3);
            CommercialReference__c cr4 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family12', ProductLine__c='12ProductLine', CommercialReference__c='0J-0N-1439' );
            crlst.add(cr4);
            insert crlst;
            
            List<ShipHoldRequest__c> shrlst = new List<ShipHoldRequest__c>();
            ShipHoldRequest__c shr1 = new ShipHoldRequest__c(Problem__c = prob.id, AffectedProduct__c = cr1.Id, BackOfficeSystem__c = BOS.Id);
            shrlst.add(shr1);
            ShipHoldRequest__c shr2 = new ShipHoldRequest__c(Problem__c = prob.id, AffectedProduct__c = cr2.Id, BackOfficeSystem__c = BOS.Id);
            shrlst.add(shr2);
            ShipHoldRequest__c shr3 = new ShipHoldRequest__c(Problem__c = prob.id, AffectedProduct__c = cr4.Id, BackOfficeSystem__c = BOS.Id);
            shrlst.add(shr3);
            insert shrlst;
            
            ApexPages.StandardSetController SHRController = new ApexPages.StandardSetController(shrlst);
            PageReference pageRef = Page.VFP_CreateHoldSource; 
            ApexPages.currentPage().getParameters().put('Id', prob.id);
            SHRController.setSelected(new ShipHoldRequest__c []{shrlst[0], shrlst[1], shrlst[2]});
            VFC_CreateHoldSource shr = new VFC_CreateHoldSource(SHRController);
            
            shr.Cancel();
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new CreateHoldSourceMockFail());
            shr.SendShipHold();
            Test.stopTest();
            
        }
    }
    
    global class CreateHoldSourceMock implements HttpCalloutMock {
        global HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            
            String accessToken = Label.CL112016I2P20; // CL112016I2P20 = 'ad223e6d06938489aa2f29f74d0e4457';
            res.setHeader('Authorization','Bearer '+ accessToken);
            res.setHeader('Content-Type','application/json');
            res.setHeader('Accept',System.Label.CLOCT15FIO018);
            res.setHeader(System.Label.CLOCT15FIO043,System.Label.CLOCT15FIO044);
            res.setHeader(System.Label.CLOCT15FIO045,System.Label.CLOCT15FIO046);
            res.setHeader(System.Label.CLOCT15FIO047,System.Label.CLOCT15FIO048);
            res.setHeader(System.Label.CLOCT15FIO049,System.Label.CLOCT15FIO048);
            res.setStatusCode(200);
            res.setBody('{"id":195174,"status":{"code":"SUCCESS"},"targetERP":"OracleERP"}');
            return res; 
    	}
    }
    
    global class CreateHoldSourceMockFail implements HttpCalloutMock {
        global HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            
            String accessToken = Label.CL112016I2P20; // CL112016I2P20 = 'ad223e6d06938489aa2f29f74d0e4457';
            res.setHeader('Authorization','Bearer '+ accessToken);
            res.setHeader('Content-Type','application/json');
            res.setHeader('Accept',System.Label.CLOCT15FIO018);
            res.setHeader(System.Label.CLOCT15FIO043,System.Label.CLOCT15FIO044);
            res.setHeader(System.Label.CLOCT15FIO045,System.Label.CLOCT15FIO046);
            res.setHeader(System.Label.CLOCT15FIO047,System.Label.CLOCT15FIO048);
            res.setHeader(System.Label.CLOCT15FIO049,System.Label.CLOCT15FIO048);
            res.setStatusCode(200);
            res.setBody('\"Fault\":[\"faultcode"]');
            return res; 
    	}
    }
}