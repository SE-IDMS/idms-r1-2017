public class AP_ProblemComplaintRequestLink {
    
    // Added by Uttara - BR-4674 (Case-Prb link)
    public static void CasePrbLink(List<ProblemComplaintRequestLink__c> newPCRlink, List<String> CRIds) {
        
        List<ProblemCaseLink__c> prbcaselink = new List<ProblemCaseLink__c>();
        Map<String,String> mapCRCase = new Map<String,String>();
        
        System.debug('!!!! newPCRlink' + newPCRlink);
        System.debug('!!!! CRIds' + CRIds);
        
        List<ComplaintRequest__c> CRList = [SELECT Case__c FROM ComplaintRequest__c WHERE Id IN: CRIds];
        
        for(ComplaintRequest__c  cr : CRList) {
            
            mapCRCase.put(cr.Id,cr.Case__c);
        }
        
        System.debug('!!!! mapCRCase' + mapCRCase);
        
        for(ProblemComplaintRequestLink__c pcrlink : newPCRlink)  {
        
            ProblemCaseLink__c pc = new ProblemCaseLink__c();
            
            pc.Case__c = mapCRCase.get(pcrlink.ComplaintRequest__c);
            pc.Problem__c = pcrlink.Problem__c;
            
            prbcaselink.add(pc);    
        }  
        
        System.debug('!!!! prbcaselink' + prbcaselink);
        
        Database.Insert(prbcaselink,false);
    }

}