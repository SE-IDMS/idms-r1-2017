//*********************************************************************************
//Class Name :  Schedule_BusinessRiskEscalationAlertForOpenBRE 
// Purpose : It's a batch class Witch is used to send alert to BRE stake holder if BRE with status open and more than 16 days from Creation date and each week after.
// Created by : Ram Chilukuri - Global Delivery Team
// Date created : 25 June, 2015 and BR-2417.
// Modified by :
// Date Modified :
// Remarks : For October 15 Release  
///********************************************************************************/ 

global class Schedule_BusinessRiskEscalationAlertOpen implements Database.Batchable<sObject>,Schedulable{
    
    List<BusinessRiskEscalations__c> setBREIdsToProcess= new List<BusinessRiskEscalations__c>();
    set<ID> setBREIdsEligibleToProcess = new set<ID>();
    
     global Schedule_BusinessRiskEscalationAlertOpen() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       // if(!Test.isrunningtest()){
            // CLAPR15I2P12 = open

            // CLOCT15I2P40 = 7
            //CLOCT15I2P16 =16
            return Database.getQueryLocator([select id, GSAAffected__c ,AffectedCustomer__c,CountryQualityManager__c,TECH_OriginatingEntity__c,TECH_OriginatingSubEntity__c,TECH_OriginatingLocation__c, ResolutionLeader__c, BusinessRiskSponsor__c from BusinessRiskEscalations__c where Status__c =: System.Label.CLAPR15I2P12 AND ((CreatedDate < : system.now()- integer.valueof(System.Label.CLOCT15I2P16) AND TECH_ReminderOpenDate__c=null) OR (TECH_ReminderOpenDate__c< : system.now()- integer.valueof(System.Label.CLOCT15I2P40) AND TECH_ReminderOpenDate__c!=null) )]);
      //  }
        /* if(Test.isrunningtest()){
            return Database.getQueryLocator([Select id ,GSAAffected__c ,AffectedCustomer__c,CountryQualityManager__c,TECH_OriginatingEntity__c,TECH_OriginatingSubEntity__c,TECH_OriginatingLocation__c, ResolutionLeader__c, BusinessRiskSponsor__c from BusinessRiskEscalations__c where Status__c =: System.Label.CLAPR15I2P12 AND ((CreatedDate < : system.now()- integer.valueof(System.Label.CLOCT15I2P16) AND TECH_ReminderOpenDate__c=null) OR (TECH_ReminderOpenDate__c< : system.now()- integer.valueof(System.Label.CLOCT15I2P40) AND TECH_ReminderOpenDate__c!=null) )]);
        }
        */
        //return NULL;
    }
    
    global void execute(Database.BatchableContext BC, List<BusinessRiskEscalations__c> scope) {
    
        for(BusinessRiskEscalations__c oBRE : scope){
            setBREIdsToProcess.add(oBRE);
        }
        
        AP_BusinessRiskEscalationReminders PCD = new AP_BusinessRiskEscalationReminders();
        PCD.createBusinessRiskStakeholder(setBREIdsToProcess);
        //PCD.createBusinessRiskStakeholder(scope);
        } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    global void execute(SchedulableContext sc) {
        Schedule_BusinessRiskEscalationAlertOpen b1 = new Schedule_BusinessRiskEscalationAlertOpen();
        
        // label : CLAPR15I2P13 = 200
        ID batchprocessid = Database.executeBatch(b1,Integer.valueOf(System.Label.CLAPR15I2P13));           
    }
}