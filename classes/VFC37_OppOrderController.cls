/*
    Author          : Jules Faligot (ACN) 
    Date Created    : 18/08/2011
    Description     : Class used by VFP37_OpportunityOrderConnector.
*/

public class VFC37_OppOrderController {

    public OppOrder OppImplementation{get;set;}
    public OPP_OrderLink__c Order = new OPP_OrderLink__c();
    public boolean AllowCreation{get;set;}
    public boolean DisplayComponent{get;set;}
    
    public void testRights(){   
        DisplayComponent = Utils_TestAccessRights.TestRightsOnOpportunity(Order.Opportunity__c);
    }
      
    public VFC37_OppOrderController(ApexPages.StandardController controller)     {        
         AllowCreation = true;
         OppImplementation = new OppOrder ();    
         Order= (OPP_OrderLink__c)controller.getRecord();
         List<OPP_OrderLink__c> OrderList = [SELECT ID, Name, Opportunity__c, BackOffice__c, PONumber__c, SEOrderReference__c FROM OPP_OrderLink__c WHERE ID=:Order.ID LIMIT 1];
         if(OrderList.size()==1){ 
             Order = OrderList[0];
         }
         OppImplementation.initCreation(Order);
         if(Order.Opportunity__c == null){  
            AllowCreation = false;
            return;  
         }
     }    
    
    public class OppOrder implements Utils_OrderMethods{
    
        public OPP_OrderLink__c Order;
        
        public void initCreation(OPP_OrderLink__c Order){
            if(Order.Opportunity__c == null){  // When coming from the OrderLink tab, the Implementation type is Opportunity but user should not be able to create a new OrderLink    
                String RelatedID = system.currentpagereference().getParameters().get('retURL'); //fetch the related object id from the URL
                if (RelatedID != null)
                    RelatedID = RelatedID.substring(1); // delete the "/"
                list<OPP_OrderLink__c> RelatedOrderList = [SELECT ID, Opportunity__c FROM  OPP_OrderLink__c WHERE ID=:RelatedID];
                if(RelatedOrderList.size()==1){ // if related object is an order, fetch its related opportunity
                    Order.Opportunity__c = RelatedOrderList[0].Opportunity__c;
                } 
                else{
                    List<Opportunity> OppList = [SELECT ID, OwnerID FROM Opportunity WHERE ID=:RelatedID  LIMIT 1];
                    if(OppList.size()==1){    // if related object is an Opportunity assign it directly
                        Order.Opportunity__c = OppList[0].ID;
                    } 
                }                           
            }
        }
        
        public void Init(VCC07_OrderConnectorController PageController){
                                
            Order =  new OPP_OrderLink__c();           
            Order = (OPP_OrderLink__c)PageController.RelatedRecordAttribute;
            PageController.Sales = true;  
            PageController.Tab2ActionLabel = '';        
            
            // If the related Order Link is a database object, SOQL needs to be used to fetch it 
            List<OPP_OrderLink__c> OrderList = [SELECT ID, Name, Opportunity__c, BackOffice__c, PONumber__c, SEOrderReference__c, bFOAccountID__c FROM OPP_OrderLink__c WHERE ID=:Order.ID LIMIT 1];
            if(OrderList.size()==1){ 
                Order = OrderList[0];
                PageController.NewOrder=false;
            }
            
            initCreation(Order);
                                                                           
            PageController.RelatedRecord = Order;
            Opportunity Opty = (Opportunity)[SELECT ID, AccountID FROM Opportunity WHERE ID=:Order.Opportunity__c LIMIT 1];
            PageController.DateCapturer.Account__c = Opty.AccountID;
            PageController.ActionLabel = Label.CL00493;
                     
                
            PageController.KeyForExistingRecords.add('SEOrderReference__c');
            PageController.KeyForExistingRecords.add('BackOffice__c'); 
             
            PageController.existingOrders = new Set<Object>();
            
            For(OPP_OrderLink__c Orderlk:[SELECT Id, BackOffice__c, SEOrderReference__c ,Opportunity__c FROM OPP_OrderLink__c WHERE Opportunity__c=:Order.Opportunity__c]) 
            {
                String NewKey = Orderlk.SEOrderReference__c + Orderlk.BackOffice__c;
                PageController.existingOrders.add((String)NewKey);                            
            }                         
                       
            PageController.columns = getColumns();
           
            if (PageController.NewOrder==True){
                PageController.TabInFocus = 'SearchOrder';}
            else
                {
                PageController.OrderNumber =  Order.SEOrderReference__c;
                PageController.PONumber = Order.PONumber__c; 
                PageController.BackOffice = Order.BackOffice__c;
                PageController.DateCapturer.Account__c = Order.bFOAccountID__c;
                
                PageController.SearchOrder();
                                
                if(PageController.fetchedRecords.size()>0)
                {
                    PageController.SelectedOrder = (OPP_OrderLink__c)PageController.fetchedRecords[0];
                }
                
                try{
                    PageController.DisplayDeliveryDetails();
                }
                catch( exception e){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00398));      
                    PageController.DisplayDeliveryDetails = false;
                }

                PageController.ReadOnly = true;
                PageController.EnableDetails = true;
                PageController.TabInFocus = 'OrderDetails';            
            }
            if(!PageController.readonly)
                PageController.BackOfficeRefresher();  // Populate the back office list                  
        }
        
        public pagereference Cancel(VCC07_OrderConnectorController PageController)
        {
            OPP_OrderLink__c Order = (OPP_OrderLink__c)PageController.RelatedRecord;
            Opportunity Opty = (Opportunity)[SELECT ID, AccountID FROM Opportunity WHERE ID=:Order.Opportunity__c LIMIT 1];
            
            PageReference OpportunityPage =  new ApexPages.StandardController(Opty).cancel();
            
            return OpportunityPage ;
        }
         
        public pagereference PerformAction(VCC07_OrderConnectorController PageController)
        {  
            OPP_OrderLink__c Order = (OPP_OrderLink__c)PageController.RelatedRecord;            
            Opportunity Opty = (Opportunity)[SELECT ID, AccountID FROM Opportunity WHERE ID=:Order.Opportunity__c LIMIT 1];                             
            OPP_OrderLink__c OrderLink= new OPP_OrderLink__c();
            OrderLink.Opportunity__c = Opty.ID;              
            OrderLink.Name  = PageController.SelectedOrder.SEOrderReference__c;
            OrderLink.SEOrderReference__c = PageController.SelectedOrder.SEOrderReference__c;
            OrderLink.PONumber__c = string.valueof(PageController.SelectedOrder.PONumber__c);
            
            OrderLink.OrderCreationDate__c = PageController.SelectedOrder.OrderCreationDate__c;
            OrderLink.Amount__c = PageController.SelectedOrder.Amount__c;
            OrderLink.OrderAccountName__c = PageController.SelectedOrder.OrderAccountName__c;
            
            OrderLink.BackOffice__c = PageController.SelectedOrder.BackOffice__c;
            
            OrderLink.OrderPrimaryContact__c = PageController.SelectedOrder.OrderPrimaryContact__c;
            OrderLink.OrderPlacedThrough__c = PageController.SelectedOrder.OrderPlacedThrough__c;
            OrderLink.OrderEnteredBy__c = PageController.SelectedOrder.OrderEnteredBy__c;
            
            OrderLink.bFOAccountID__c = PageController.DateCapturer.Account__c;  
            // Added by Hari Krishna for EUS-015205
            OrderLink.CurrencyIsoCode = PageController.SelectedOrder.CurrencyIsoCode;
                                              
            DataBase.SaveResult InsertedOrderLink = DataBase.insert(OrderLink);   
            PageReference OpportunityPage =  new ApexPages.StandardController(Opty).view();                              
            
            String NewKey = OrderLink.SEOrderReference__c + OrderLink.BackOffice__c;
            PageController.existingOrders.add((String)NewKey);             
            PageController.DisableSelectAction = true;
            
            OPP_OrderLink__c Ordlink = [SELECT Id, Name FROM OPP_OrderLink__c WHERE ID=:OrderLink.Id];
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00496 +' '+OrderLink.SEOrderReference__c+' '+ Label.CL00500 +' '+ Ordlink.Name)); 
            return null;        
        }   
        
        public List<SelectOption> getColumns()
        {
            List<SelectOption> columns = new List<SelectOption>();                                            
            columns.add(new SelectOption('SEOrderReference__c', sObjectType.OPP_OrderLink__c.fields.SEOrderReference__c.getLabel()));
            columns.add(new SelectOption('PONumber__c', sObjectType.OPP_OrderLink__c.fields.PONumber__c.getLabel()));
            columns.add(new SelectOption('OrderCreationDate__c', sObjectType.OPP_OrderLink__c.fields.OrderCreationDate__c.getLabel()));
            columns.add(new SelectOption('Amount__c', sObjectType.OPP_OrderLink__c.fields.Amount__c.getLabel()));
            columns.add(new SelectOption('OrderAccountName__c', sObjectType.OPP_OrderLink__c.fields.OrderAccountName__c.getLabel()));                                                         
            columns.add(new SelectOption('OrderAccountLegacyNumber__c', sObjectType.OPP_OrderLink__c.fields.OrderAccountLegacyNumber__c.getLabel()));        
            columns.add(new SelectOption('OrderPrimaryContact__c', sObjectType.OPP_OrderLink__c.fields.OrderPrimaryContact__c.getLabel()));
            columns.add(new SelectOption('OrderPlacedThrough__c', sObjectType.OPP_OrderLink__c.fields.OrderPlacedThrough__c.getLabel())); 
            columns.add(new SelectOption('OrderEnteredBy__c', sObjectType.OPP_OrderLink__c.fields.OrderEnteredBy__c.getLabel())); 
            columns.add(new SelectOption('Status__c',sObjectType.OPP_OrderLink__c.fields.Status__c.getLabel()));  
            return columns;                            
        }
    }
}