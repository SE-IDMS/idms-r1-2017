global class VFC_AddProductforContract{
    
    Public List<ContractProductInformation__c> lstCPI {get; set;}
    public PackageTemplate__c templateRecord{get;set;}
    Public ID templateRecordid; 
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public VFC_AddProductforContract(ApexPages.StandardController controller) {
        templateRecord=(PackageTemplate__c)controller.getRecord();
        templateRecordid = ApexPages.currentPage().getParameters().get('CF00NA0000009vacE_lkid');  
        System.debug('templateRecordid :'+templateRecordid ); 
        lstCPI = new List<ContractProductInformation__c>();       
    }
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
        if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
            scriteria=pageParameters.get('commercialReference');
    }
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();
        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;
        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
        System.debug('gmrFilteredDataBeanList:'+results.gmrFilteredDataBeanList);
        return results.gmrFilteredDataBeanList;
    }
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        system.debug('&&& business'+ business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    Public pagereference performCheckbox(){ 
        system.debug('*** checkbox - enter');
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');   
            system.debug('***jsonSelectString'+jsonSelectString);
       
        List<WS_GMRSearch.gmrFilteredDataBean> DBL= (List<WS_GMRSearch.gmrFilteredDataBean> ) JSON.deserialize(jsonSelectString, List<WS_GMRSearch.gmrFilteredDataBean> .class);
        System.debug('DBL'+DBL);
        
        if(DBL != null && DBL != null) {            
            for(WS_GMRSearch.gmrFilteredDataBean DBL1 :DBL){
                ContractProductInformation__c productInfo = new ContractProductInformation__c();
                if(DBL1.ProductIdbFO!=null){    
                    productInfo.CommercialReference_lk__c= DBL1.ProductIdbFO.UnEscapeXML();
                    }
                    productInfo.PackageTemplate__c = templateRecordid;
                    productInfo.ProductBU__c = DBL1.businessLine.label;
                    productInfo.ProductLine__c = DBL1.productLine.label;
                    productInfo.ProductFamily__c = DBL1.strategicProductFamily.label;
                    productInfo.Family__c = DBL1.family.label;
                    if(DBL1.ProductFamilybFO!=null)
                        productInfo.Family_lk__c=DBL1.ProductFamilybFO.UnEscapeXML();
                    productInfo.SubFamily__c = DBL1.subFamily.label;
                    productInfo.ProductDescription__c = DBL1.description;
                    productInfo.ProductGroup__c = DBL1.productGroup.label;
                    productInfo.ProductSuccession__c = DBL1.productSuccession.label;
                    productInfo.CommercialReference__c= DBL1.commercialReference;
                    productInfo.Primary__c=false;
                
                     //for all 7 level's GMR Code
                    if(DBL1.businessLine.value != null){
                        productInfo.TECH_PM0CodeInGMR__c= DBL1.businessLine.value;
                    }
                    if(DBL1.productLine.value != null){
                         productInfo.TECH_PM0CodeInGMR__c+= DBL1.productLine.value;
                    }
                    if(DBL1.strategicProductFamily.label != null){
                        productInfo.TECH_PM0CodeInGMR__c+= DBL1.strategicProductFamily.value;
                    }
                    if(DBL1.family.label != null){
                        productInfo.TECH_PM0CodeInGMR__c+= DBL1.family.value;
                    }
                    if(DBL1.subFamily.label != null){
                        productInfo.TECH_PM0CodeInGMR__c+= DBL1.subFamily.value;
                    }
                    if(DBL1.productSuccession.label != null){
                        productInfo.TECH_PM0CodeInGMR__c+= DBL1.productSuccession.value;
                    }
                    if(DBL1.productGroup.label != null){
                        productInfo.TECH_PM0CodeInGMR__c+= DBL1.productGroup.value;
                    }
                    lstCPI.add(productInfo);
                
                
            }
        }   
        system.debug('*** lstCPI'+lstCPI);
        system.debug('*** templateRecordid'+templateRecordid);
        try{
            insert  lstCPI;
            PageReference packageTemplatepage;            
            packageTemplatepage=new PageReference('/'+ templateRecordid );                        
            return packageTemplatepage;   
            }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    }
    public PageReference pageCancelFunction(){
        PageReference packageTemplatepage;            
        packageTemplatepage=new PageReference('/'+ templateRecordid );                        
        return packageTemplatepage;   
    }
}