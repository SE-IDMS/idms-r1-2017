public class VFC_POReleaseiInBO 
{
    //private final ID PartOrderId;
    private final String action;
    public Static String Url{get;set;}
    public ID PartOrderId {get;set;}
    //private SVMXC__RMA_Shipment_Order__c partOrder;
    //private SVMXC__RMA_Shipment_Line__c partOrderLine;
    public list<SVMXC__RMA_Shipment_Line__c> partOrderLine {get;set;}
    public list<SVMXC__RMA_Shipment_Order__c> partOrder {get;set;}
    //SVMXC__RMA_Shipment_Order__c partOrder =new SVMXC__RMA_Shipment_Order__c();
     
     public VFC_POReleaseiInBO(ApexPages.StandardController controller) 
     {
        PartOrderId = controller.getID();
        //system.debug('The Record context is ' +partOrder);
        //PartOrderId = partOrder.id;
        system.debug('The Part Order Id is' +PartOrderId);
        action = apexPages.currentPage().getParameters().get('Action'); 
        Url = '';
   
     }
     public PageReference redirectToMW()
    {
        string parameters;
        string FinalCordysURL;
        partOrder = [select Id,SVMXC__Service_Order__c,SVMXC__Order_Status__c,WO_Back_Office_System__c,WO_Country_of_BackOffice__c,WO_Back_Office_Ref_Num__c from SVMXC__RMA_Shipment_Order__c where id=:PartOrderId];
        
        
        partOrderLine = [select Id from SVMXC__RMA_Shipment_Line__c where SVMXC__RMA_Shipment_Order__c=:PartOrderId and Return_Reason__c = null];
        if (partOrderLine.size() > 0)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'The Return Reason field is not populated for some Part Order Lines. Please update the field in order to proceed to release. '));          
            return null;
        }
        else
        {
            for(SVMXC__RMA_Shipment_Order__c porder: partOrder)
            {
                porder.RequestForRelease__c = System.now();
                porder.Tech_RequestForRelease__c = System.now();
                porder.SVMXC__Order_Status__c = 'Pending Release in BO';
                update porder;
            }
            if (action ==  'PartOrder.RELEASE')
            {
                parameters = formatURL(partOrder,'PartOrder.RELEASE');
                FinalCordysURL = System.Label.CLAPR15SRV38;
                
            }
            PageReference pg=new PageReference(FinalCordysURL+'?'+parameters);
            pg.setRedirect(true);
            return pg;
            //PageReference pg=new PageReference(FinalCordysURL);
            
        }
        
    }
    public PageReference cancel(){
     return null;
    }
    public static string formatURL(list<SVMXC__RMA_Shipment_Order__c>  PartOrder,string action)
    {
        string parameters = ''; 
        parameters += Label.CLDEC12SRV16+'='+UserInfo.getSessionId();  
        parameters += '&'+'poid' +'='+PartOrder[0].Id;
        parameters += '&'+'wid' +'='+PartOrder[0].SVMXC__Service_Order__c;   
        parameters += '&'+'pid' +'=BFO';
        parameters += '&'+'boid' +'=' +PartOrder[0].WO_Back_Office_System__c;
        parameters += '&'+'COUNTRYOFBACKOFFICE' +'='+PartOrder[0].WO_Country_of_BackOffice__c;
        parameters += '&'+'BACKOFFICEREFERENCE' +'='+PartOrder[0].WO_Back_Office_Ref_Num__c;
        parameters += '&'+Label.CLDEC12SRV17 +'='+action;
        parameters += '&'+Label.CLDEC12SRV35+'='+System.URL.getSalesforceBaseURL().getHost();
        
        return parameters ;
    }
}