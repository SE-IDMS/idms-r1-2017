public without sharing class AP_ProcessPRMRegistration implements Queueable {

    List<Id> RegistrationList {get;set;}
    Map<Id,Contact> ContactList {get;set;}
    AP_PRMAppConstants.ProcessPendingQueueItems ProcessType {get;set;}
    String RequestedAccess{get;set;}
    List<String> queryFields {get;set;}
    private List<SObject> lObjects;
    String FederationId {get;set;}
    Id AccountId{get;set;}
    public static Boolean JITProvisioning = false;
    Map<String,CS_PRM_ApexJobSettings__c> mapApexJobSettings = CS_PRM_ApexJobSettings__c.getall();
    Integer batchSize;

    public static Boolean throwExceptionInTest = false;
    
    public class CodeCoverageException extends Exception {
        public String message = 'This exception is thrown to do code coverage';
    }       
    public AP_ProcessPRMRegistration(List<Id> RegistrationList, AP_PRMAppConstants.ProcessPendingQueueItems ProcessType){
        system.debug('**Constructor Called**'+RegistrationList);
        system.debug('>>>>'+Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c));
        this.RegistrationList = RegistrationList;
        this.ProcessType = ProcessType;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
    }
    public AP_ProcessPRMRegistration(Map<Id,Contact> ContactList, AP_PRMAppConstants.ProcessPendingQueueItems ProcessType){
        system.debug('**Constructor Called**'+RegistrationList);
        system.debug('>>>>'+Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c));
        this.ContactList = ContactList;
        this.ProcessType = ProcessType;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
    }
    public AP_ProcessPRMRegistration(List<Id> ContactIdList, String RequestedAccess, AP_PRMAppConstants.ProcessPendingQueueItems ProcessType){
        system.debug('**Constructor Called**'+RegistrationList);
        this.RegistrationList = ContactIdList;
        this.ProcessType = ProcessType;
        this.RequestedAccess = RequestedAccess;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
    }

    public AP_ProcessPRMRegistration (AP_PRMAppConstants.ProcessPendingQueueItems reqToProcess, List<SObject> lObjToProcess) {
        System.debug('*** Inside List of Objects to Process' + reqToProcess);
        this.ProcessType = reqToProcess;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(reqToProcess)).NumberValue__c);
        if(lObjToProcess != null && lObjToProcess.size() > 0)
            this.lObjects = lObjToProcess;
    }
    

    // Queue-able Execute Method
    public void execute(QueueableContext context) {
        AP_UserRegModel newUser = new AP_UserRegModel();
        List<Contact> cont = new List<Contact>();
        List<UIMSCompany__c> lUcmp;
        String qryFilter = '';
        String key = '';
        List<String> lFlds;
        List<String> contId = new List<String>();
        List<Id> uimsId = new List<Id>();

        system.debug('**UIMS Comp ID :: Context**'+RegistrationList+' '+context);
        try {
            if(ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION){
                qryFilter = 'ID IN :RegistrationList';
                UserRegResult regResult;
                lFlds = AP_PRMUtils.RegistrationTrackingFields();
                lUcmp = Database.query('SELECT Id,'+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE '+qryFilter+' ORDER BY LastModifiedDate DESC');
                key = 'In Progress - '+System.Now();
                if(lUcmp != null && lUcmp.size() > 0) {
                    lUcmp[0].put('RegistrationStatus__c',key);
                    UPDATE lUcmp;
                    System.debug('**UIMS Company**'+lUcmp);
                    newUser = (AP_UserRegModel)JSON.deserialize(lUcmp[0].UserRegistrationInformation__c, AP_UserRegModel.class); 
                    
                    System.debug('**UserRegModel Generated from UIMS Company**'+newUser);
                        
                    //newUser.isPrimaryContact =  (newUser.accountId == null && newUser.skippedCompanyStep == false ? true : false);
                    PRM_FieldChangeListener.enableForAccount = false;
                    PRM_FieldChangeListener.enableForContact = false;

                    if(newUser != Null) {  
                        System.debug('**lUcmp123**'+lUcmp);
                         System.debug('*lUcmp123*'+lUcmp.size());
                        regResult = AP_PRMAccountRegistration.CreatePRMRegistrationbFO(newUser, lUcmp, true); 
                        System.debug('**regResult123**'+regResult);
                       //System.debug('**regResult1234**'+regResult.Id);
                        if(Test.isRunningTest()){
                            regResult.ContactId = lUcmp[0].bFOContact__c; 
                        }
                        if(regResult != null && string.isNotBlank(regResult.ContactId)) {
                            
                            cont = [SELECT Id,Name,PRMUIMSId__c,PRMApplicationRequested__c,AccountId,FieloEE__Member__c FROM Contact WHERE Id = :regResult.ContactId];
                            Map<Id,Contact> newRecords = new Map<Id,Contact>();                    
                            newRecords.put(cont[0].Id, cont[0]);

                            // TODO: Handle LIMIT exception
                            try{
                                if(newRecords.size () > 0) {
                                    AP_PRMAppConstants.ProcessPendingQueueItems permissionType = (String.valueOf(newUser.targetApplicationRequested).CONTAINS('POMP'))?AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP:AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER;
                                    ID jobID = System.enqueueJob(new AP_ProcessPRMRegistration(new List<Id>(newRecords.keySet()),newUser.targetApplicationRequested,permissionType));
                                }
                            }
                            catch (Exception ltEx) {
                                List<PRMRequestQueue__c> prmReqQueueLst;
                                if(newRecords.size() > 0){          
                                    for (Contact cc : newRecords.values()) {
                                            System.debug('**Contact **'+cc);
                                            PRMRequestQueue__c prmReqQueue = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ContactId__c = cc.Id, RequestType__c = (String.valueOf(newUser.targetApplicationRequested).CONTAINS('POMP'))?'POMPPERM':'PRTLPERM',ProcessingStatus__c='Pending');
                                            PRMRequestQueue__c prmReqQueue1 = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ContactId__c = cc.Id, RequestType__c = 'UPDMEMPROP',ProcessingStatus__c='Pending');
                                            prmReqQueueLst = new List<PRMRequestQueue__c>{prmReqQueue,prmReqQueue1};
                                            System.debug('** **'+prmReqQueueLst);
                                    }
                                    if(lUcmp != null && lUcmp.size() > 0){
                                        lUcmp[0].InformationMessage__c = ltEx.getMessage() + ' ' + ltEx.getStackTraceString();
                                        lUcmp[0].IMSRegistrationError__c = true;
                                        UPDATE lUcmp;
                                    }
                                    System.debug('**Request Queue Lst**'+prmReqQueueLst);
                                    if(prmReqQueueLst != Null && prmReqQueueLst.size()>0)
                                        INSERT prmReqQueueLst;
                                }
                            }
                        }
                        if(throwExceptionInTest == False && Test.isRunningTest()){
                         throw new CodeCoverageException('Exception to do the code coverage');
                        }
                        
                    }
                }
            }
            else if(ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER ||
                    ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP) {
                if (this.RequestedAccess == System.Label.CLMAR16PRM011) {
                    System.debug ('*** Initiating Permission assignment');
                    AP_PRMUtils.AssignPortalPermissions(this.RegistrationList);
                    ContactList = new Map<Id,Contact>([SELECT Id, Name, PRMUIMSId__c, PRMApplicationRequested__c, AccountId, FieloEE__Member__c FROM Contact WHERE Id IN :this.RegistrationList AND FieloEE__Member__c != '']);

                    System.debug('*** Contact Member records: ' + ContactList);
                    // TODO: Capture LIMIT Exception and add an entry to PRMRequestQueue__c
                    try{
                        lFlds = AP_PRMUtils.RegistrationTrackingFields();
                        for(Id ids : ContactList.keyset()){
                            contId.add(ids);
                        }

                        lUcmp = Database.query('SELECT Id,'+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE bFOContact__c IN :contId ORDER BY LastModifiedDate DESC');
                        for(UIMSCompany__c uims : lUcmp)
                            uimsId.add(uims.Id);

                        if(mapApexJobSettings.get('REG_IMS_Async').InitialSync__c && String.isBlank(lUcmp[0].UserFederatedId__c)){
                            AP_PRMUtils.IMSRegistrationAsync(uimsId,lFlds);
                        }
                        
                        else{
                            if(AP_PRMUtils.CanAddNewApexJobToQueue() && String.isBlank(lUcmp[0].UserFederatedId__c)) {
                                this.batchSize = Integer.valueOf(mapApexJobSettings.get('REGISTRATION_IMS').NumberValue__c);
                                List<SObject> uimsCompanyLst = [SELECT Id, RegistrationStatus__c, bFOContact__c FROM UIMSCompany__c WHERE bFOContact__c IN :this.RegistrationList];
                                if(uimsCompanyLst != null && uimsCompanyLst.size() > 0)
                                    uimsCompanyLst[0].put('IMSRegistrationQueueSubmittedAt__c',System.Now());
                                AP_PRMAppConstants.ProcessPendingQueueItems regIMS = AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS;
                                System.debug('*** Process Type: ' + regIMS.name());
                                System.debug('*** List to process for ims registration:' + uimsCompanyLst);
                                Batch_PRM_ProcessPendingQueueItems batch_ppsa = new Batch_PRM_ProcessPendingQueueItems(regIMS, uimsCompanyLst, true);
                                if(!Test.isRunningTest())
                                    Database.executebatch(batch_ppsa,this.batchSize); 
                            }
                        }
                        if(throwExceptionInTest == False && Test.isRunningTest()){
                        throw new CodeCoverageException('Exception to do the code coverage');
                        }
                    }
                    Catch (Exception ex) {
                        System.debug('**Exception Caught AP_ProcessPRMRegistration Permission **'+ex.getmessage()+' '+ex.getStackTraceString());
                        Map<Id,PRMRequestQueue__c> cId = new Map<Id,PRMRequestQueue__c>();
                        List<PRMRequestQueue__c> prmReqQueueLst = new List<PRMRequestQueue__c>([SELECT Id,AccountId__c, ContactId__c, RequestType__c, ProcessingStatus__c,RunCounter__c FROM PRMRequestQueue__c WHERE RequestType__c = 'UPDMEMPROP' AND ContactId__c IN :ContactList.keyset()]);
                        for(PRMRequestQueue__c p : prmReqQueueLst)
                            cId.put(p.ContactId__c,p);
                        for(Contact cc : ContactList.values()){
                            if(cId.containskey(cc.Id)){
                                cId.get(cc.Id).ProcessingStatus__c = 'Pending';
                                cId.get(cc.Id).RunCounter__c = cId.get(cc.Id).RunCounter__c + 1;
                                cId.get(cc.Id).ErrorInfo__c = ex.getMessage();
                                prmReqQueueLst.add(cId.get(cc.Id));
                            }
                            else{
                                PRMRequestQueue__c prmReqQueue1 = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ErrorInfo__c = ex.getMessage(), RunCounter__c = 1, ContactId__c = cc.Id, RequestType__c = 'UPDMEMPROP',ProcessingStatus__c='Pending');
                                prmReqQueueLst.add(prmReqQueue1);
                            }
                        }                                                     
                        if(prmReqQueueLst != Null && prmReqQueueLst.size()>0)
                            UPSERT prmReqQueueLst;
                    }
                } 
                else {
                    System.debug('**** Assigning POMP Permissions');
                    if (this.RegistrationList != null && this.RegistrationList.size()>0){
                        System.debug('Assign POMP Permission');
                        AP_PRMUtils.AssignPOMPOnlyPermissions(this.RegistrationList);
                    }            
                }                
            }
            else if(ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES) {
                PRM_FieldChangeListener.enableForAccount=false;
                PRM_FieldChangeListener.enableForContact=true; 
                PRM_FieldChangeListener.checkInternalPropertiesForContacts(new List<Id>(contactList.keySet()));
                PRM_FieldChangeListener.enableForContact=false;                              
            }
            /*else if(ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATE_MASS_REGISTRATION){
                if(newUser != null){
                    AP_PRMUtils.UpdatePRMRegistrationInbFO(newUser); 
                }
            }*/
            else if(ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATE_REQUEST_QUEUE || this.ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATE_REG_HISTORY_TRACKING){
                if(this.lObjects != null && this.lObjects.size() > 0){
                    UPDATE lObjects;
                }
            }
            
        } Catch (Exception ex) {
            System.debug('**Exception Caught AP_PRMRegistrationQueue**'+ex.getmessage()+' '+ex.getStackTraceString());
            if(ProcessType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION){
                lUcmp = Database.query('SELECT Id, RetryRegistration__c, InformationMessage__c FROM UIMSCompany__c WHERE '+qryFilter+' ORDER BY LastModifiedDate DESC');
                if(lUcmp != null && lUcmp.size() > 0){
                    lUcmp[0].put('InformationMessage__c',ex.getmessage()+' '+ex.getStackTraceString());
                    lUcmp[0].put('RetryRegistration__c',true);
                    lUcmp[0].put('RegistrationStatus__c','');
                    UPDATE lUcmp;
                }
            }
            else {
                List<PRMRequestQueue__c> prmReqQueueLst = new List<PRMRequestQueue__c>();
                   if(cont.size() > 0 && !cont.isEmpty()){          
                    for (Contact cc : cont) {
                            System.debug('**Contact **'+cc);
                            PRMRequestQueue__c prmReqQueue = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ContactId__c = cc.Id, RequestType__c = (String.valueOf(cont[0].PRMApplicationRequested__c).CONTAINS('POMP'))?'POMPPERM':'PRTLPERM',ProcessingStatus__c='Pending');
                            PRMRequestQueue__c prmReqQueue1 = new PRMRequestQueue__c(AccountId__c = cc.AccountId, ContactId__c = cc.Id, RequestType__c = 'UPDMEMPROP',ProcessingStatus__c='Pending');
                            prmReqQueueLst = new List<PRMRequestQueue__c>{prmReqQueue,prmReqQueue1};
                            System.debug('** **'+prmReqQueueLst);
                    }
                    System.debug('**Request Queue Lst**'+prmReqQueueLst);
                    if(prmReqQueueLst != Null && prmReqQueueLst.size()>0)
                        INSERT prmReqQueueLst;
               }
            }
        }       
    }
}