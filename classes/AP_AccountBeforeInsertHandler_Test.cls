@isTest
private class AP_AccountBeforeInsertHandler_Test {
    
    @isTest static void testCheckSEAccountId()
    {
        // Implement test code
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;
        Account acc2 = new Account(Name='Acc2', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        Database.SaveResult saveresult=Database.insert(acc2,false);             
        System.assertEquals(saveresult.isSuccess(),false);
    }       
    @isTest static void testChildAccKeyAccountType()  
    {                
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Account parentAcc = new Account(Name='Parent Acc', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO');      
        insert parentAcc;        
        Account childAcc1 = new Account(Name='Parent Acc',ParentId=parentAcc.id ,  ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id);  
        insert childAcc1;    
    }
    @isTest static void testAccountOwnership()
    {
        Country__c ct = Utils_TestMethods.createCountry();
        insert ct;
        User accOwnUser1= Utils_TestMethods.createAdminUser('accOwn1');
        insert accOwnUser1;
        List<AccountOwnershipRule__c> accOwnLst = new List<AccountOwnershipRule__c>();
        accOwnLst.add(new AccountOwnershipRule__c(ClassLevel1__c='SI', LeadingBusiness__c='BD', Country__c=ct.id, AccountOwner__c=accOwnUser1.id));
        accOwnLst.add(new AccountOwnershipRule__c(ClassLevel1__c='SI', Country__c=ct.id, AccountOwner__c=accOwnUser1.id));
        insert accOwnLst;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='SI', LeadingBusiness__c='BD', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234', ReassignOwnership__c=True);      
        insert acc1;
        Account acc2 = new Account(Name='Acc1', ClassLevel1__c='SI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO', ReassignOwnership__c=True);      
        insert acc2;
        String acc1OwnerId = [Select OwnerId from Account Where Id = :acc1.id].OwnerId;
        String acc2OwnerId = [Select OwnerId from Account Where Id = :acc2.id].OwnerId;
          
    }
    @isTest static void testPopulateLeadingBU()
    {
        Country__c ct = Utils_TestMethods.createCountry();
        insert ct;
        User accOwnUser1= Utils_TestMethods.createAdminUser('accOwn1');
        accOwnUser1.UserBusinessUnit__c='BD';
        insert accOwnUser1;
        System.runAs(accOwnUser1)
        {
            Account consumerAccount1  = Utils_TestMethods.createConsumerAccount(ct.Id);
            insert consumerAccount1;
            String leadingBU1 = [Select LeadingBusiness__c from Account where id = :consumerAccount1.id].LeadingBusiness__c;
            System.debug('--->> '+leadingBU1);
            
        }
        
        accOwnUser1.UserBusinessUnit__c='TST';
        update accOwnUser1;
        System.runAs(accOwnUser1)
        {
            Account consumerAccount2  = Utils_TestMethods.createConsumerAccount(ct.Id);
            insert consumerAccount2;
            String leadingBU2 = [Select LeadingBusiness__c from Account where id = :consumerAccount2.id].LeadingBusiness__c;
            System.debug('--->> '+leadingBU2);
            
        }
        list<CS_AccountSimplification__c> asList = new List<CS_AccountSimplification__c>();
        if(CS_AccountSimplification__c.getValues('EU__EN1')== null){
        
            CS_AccountSimplification__c as0= new CS_AccountSimplification__c();
            as0.Name = 'EU__EN1';
            as0.ClassificationLevel1__c ='EU';
            as0.MarketSegment__c ='EN1';
            as0.LeadingBusiness__c = 'EN';
            asList.add(as0);
        }
        if(CS_AccountSimplification__c.getValues('WD__') == null){
            CS_AccountSimplification__c as1= new CS_AccountSimplification__c();
            as1.Name = 'WD__'; 
            as1.ClassificationLevel1__c ='WD';   
            as1.LeadingBusiness__c  ='PW';
            asList.add(as1);
        }
        if(CS_AccountSimplification__c.getValues('WD_WD1_') == null){
            CS_AccountSimplification__c as2= new CS_AccountSimplification__c();
            as2.Name = 'WD_WD1_';
            as2.ClassificationLevel1__c ='WD';
            as2.ClassificationLevel2__c ='WD1';
            as2.LeadingBusiness__c = 'PW';
            asList.add(as2);
        } 
            
        if(asList!= null && asList.size()>0)
        Database.insert(asList); 
    
        List<account> accList = new list<account>();
        Account account1 = Utils_TestMethods.createAccount();    
        account1.ClassLevel1__c = 'EU'; 
        account1.MarketSegment__c = 'EN1';
        
        Account account2 = Utils_TestMethods.createAccount();    
        account2.ClassLevel1__c = 'WD'; 
        
        Account account3 = Utils_TestMethods.createAccount();    
        account3.ClassLevel1__c = 'WD'; 
        account3.ClassLevel2__c = 'WD1';
            
        Account account4 = Utils_TestMethods.createAccount();    
        account4.ClassLevel1__c = 'WD'; 
        account4.ClassLevel2__c = 'WD1';
        account4.MarketSegment__c = 'EN1';
            
        accList.add(account1);
        accList.add(account2);  
        accList.add(account3);
        accList.add(account4);
        
        Database.insert(accList); 
        System.debug('Account Leading BU in the end:'+account1.LeadingBusiness__c);
        System.debug('Account Leading BU in the end:'+account2.LeadingBusiness__c);
        System.debug('Account Leading BU in the end:'+account3.LeadingBusiness__c);
        System.debug('Account Leading BU in the end:'+account4.LeadingBusiness__c);
    }
}