/*
    Copyright © 2012-2016 odaseva.com, inc. All rights reserved.
*/

/*
NOTES
PublishingPublisher__c
PublishingPublisherCountry__c 
PublishingDescription__c
PublishingStatusImage__c
PublishingStatus__c
PublishingTimestamp__c
*/

public WITHOUT SHARING class ODA_PRMPublisher{
    
    //--GET SET
    public List<wrapSObject>myFieloEE_Components{get;set;}
    public List<wrapSObject>myFieloEE_RedemptionRules{get;set;}
    public List<wrapSObject>myFieloEE_Banners{get;set;}
    public List<wrapSObject>myFieloEE_Newss{get;set;}
    public List<wrapSObject>myFieloEE_RedemptionRuleCriterias{get;set;}
    public List<wrapSObject>myFieloEE_Sections{get;set;}
    public List<wrapSObject>myFieloEE_SegmentDomains{get;set;}
    public List<wrapSObject>myFieloEE_Tags{get;set;}
    public List<wrapSObject>myFieloEE_TagItems{get;set;}
    public String myPRMCountry{get;set;}
    
    //--OTHER VARS
    public DateTime myLastDateTime1=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime2=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime3=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime4=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime5=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime6=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime7=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime8=DateTime.newInstance(2500,1,1);
    public DateTime myLastDateTime9=DateTime.newInstance(2500,1,1);
    
    public List<SelectOption>paramPRMCountryList{get;set;}{
        List<SelectOption> options= new List<SelectOption>();
        options.add(new SelectOption('---','---'));
        for(PRMCountry__c myPRMCountry:[SELECT Id,Name FROM PRMCountry__c ORDER BY Name LIMIT 1000]){
            //TODO: si >1000
            //options.add(new SelectOption(myPRMCountry.id,myPRMCountry.Name));
            options.add(new SelectOption(myPRMCountry.id,myPRMCountry.Name,myPRMCountry.Name=='India'?false:true)); 
        }
            
        paramPRMCountryList=options;
    }
    
    //--WRAPPERS
    public class wrapSObject{
        public SObject mySObject {get;set;}
        public Boolean selected {get;set;}
         public wrapSObject(SObject obj) {
            mySObject=obj;
            selected=false;
        }
    }
    
    //--CONSTRUCTOR
    public PageReference ODA_PRMPublisherAction(){
        String goldenId   = apexpages.currentpage().getparameters().get('id');
        String objectName = apexpages.currentpage().getparameters().get('obj');
        String targetId  = null;
        if ((goldenId != null) && (objectName!= null)) {
            if (objectName == 'FieloEE__Banner__c') targetId = ((FieloEE__Banner__c)[SELECT Id FROM FieloEE__Banner__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__Component__c') targetId = ((FieloEE__Component__c)[SELECT Id FROM FieloEE__Component__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__News__c') targetId = ((FieloEE__News__c)[SELECT Id FROM FieloEE__News__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__RedemptionRule__c') targetId = ((FieloEE__RedemptionRule__c)[SELECT Id FROM FieloEE__RedemptionRule__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__RedemptionRuleCriteria__c') targetId = ((FieloEE__RedemptionRuleCriteria__c)[SELECT Id FROM FieloEE__RedemptionRuleCriteria__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__Section__c') targetId = ((FieloEE__Section__c)[SELECT Id FROM FieloEE__Section__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__SegmentDomain__c') targetId = ((FieloEE__SegmentDomain__c)[SELECT Id FROM FieloEE__SegmentDomain__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__Tag__c') targetId = ((FieloEE__Tag__c)[SELECT Id FROM FieloEE__Tag__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            else if (objectName == 'FieloEE__TagItem__c') targetId = ((FieloEE__TagItem__c)[SELECT Id FROM FieloEE__TagItem__c WHERE PublishingGoldenId__c=:goldenId][0]).id;
            PageReference ReturnPage = new PageReference('/' + targetId); 
            ReturnPage.setRedirect(true);
            return ReturnPage;
        }
        else return null;
    }
    
    public ODA_PRMPublisher(){
        system.debug('###CONSTRUCTOR-BEGIN###');
        
        List<FieloEE__Component__c>tempFieloEE_Components=[SELECT PublishingTimestamp__c FROM FieloEE__Component__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_Components.size()>0&&tempFieloEE_Components[0].PublishingTimestamp__c!=null)myLastDateTime1=tempFieloEE_Components[0].PublishingTimestamp__c;
        
        List<FieloEE__RedemptionRule__c>tempFieloEE_RedemptionRules=[SELECT PublishingTimestamp__c FROM FieloEE__RedemptionRule__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_RedemptionRules.size()>0&&tempFieloEE_RedemptionRules[0].PublishingTimestamp__c!=null)myLastDateTime2=tempFieloEE_RedemptionRules[0].PublishingTimestamp__c;
        
        List<FieloEE__Banner__c>tempFieloEE_Banners=[SELECT PublishingTimestamp__c FROM FieloEE__Banner__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_Banners.size()>0&&tempFieloEE_Banners[0].PublishingTimestamp__c!=null)myLastDateTime3=tempFieloEE_Banners[0].PublishingTimestamp__c;
        
        List<FieloEE__News__c>tempFieloEE_Newss=[SELECT PublishingTimestamp__c FROM FieloEE__News__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_Newss.size()>0&&tempFieloEE_Newss[0].PublishingTimestamp__c!=null)myLastDateTime4=tempFieloEE_Newss[0].PublishingTimestamp__c;
        
        List<FieloEE__RedemptionRuleCriteria__c>tempFieloEE_RedemptionRuleCriterias=[SELECT PublishingTimestamp__c FROM FieloEE__RedemptionRuleCriteria__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_RedemptionRuleCriterias.size()>0&&tempFieloEE_RedemptionRuleCriterias[0].PublishingTimestamp__c!=null)myLastDateTime5=tempFieloEE_RedemptionRuleCriterias[0].PublishingTimestamp__c;
        
        List<FieloEE__Section__c>tempFieloEE_Sections=[SELECT PublishingTimestamp__c FROM FieloEE__Section__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_Sections.size()>0&&tempFieloEE_Sections[0].PublishingTimestamp__c!=null)myLastDateTime6=tempFieloEE_Sections[0].PublishingTimestamp__c;
        
        List<FieloEE__SegmentDomain__c>tempFieloEE_SegmentDomains=[SELECT PublishingTimestamp__c FROM FieloEE__SegmentDomain__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_SegmentDomains.size()>0&&tempFieloEE_SegmentDomains[0].PublishingTimestamp__c!=null)myLastDateTime7=tempFieloEE_SegmentDomains[0].PublishingTimestamp__c;
        
        List<FieloEE__Tag__c>tempFieloEE_Tags=[SELECT PublishingTimestamp__c FROM FieloEE__Tag__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_Tags.size()>0&&tempFieloEE_Tags[0].PublishingTimestamp__c!=null)myLastDateTime8=tempFieloEE_Tags[0].PublishingTimestamp__c;
        
        List<FieloEE__TagItem__c>tempFieloEE_TagItems=[SELECT PublishingTimestamp__c FROM FieloEE__TagItem__c ORDER BY PublishingTimestamp__c DESC LIMIT 1];
        if(tempFieloEE_TagItems.size()>0&&tempFieloEE_TagItems[0].PublishingTimestamp__c!=null)myLastDateTime9=tempFieloEE_TagItems[0].PublishingTimestamp__c;
        
        refreshGetters();
        
        system.debug('###CONSTRUCTOR-END###');
    }
    
    //--METHODS
    //refreshGetters
    public void refreshGetters(){
        system.debug('###refreshGetters-BEGIN###');
        system.Debug('myLastDateTime1===>'+myLastDateTime1);
        system.Debug('myLastDateTime2===>'+myLastDateTime2);
        system.Debug('myLastDateTime3===>'+myLastDateTime3);
        system.Debug('myLastDateTime4===>'+myLastDateTime4);
        system.Debug('myLastDateTime5===>'+myLastDateTime5);
        system.Debug('myLastDateTime6===>'+myLastDateTime6);
        system.Debug('myLastDateTime7===>'+myLastDateTime7);
        system.Debug('myLastDateTime8===>'+myLastDateTime8);
        system.Debug('myLastDateTime9===>'+myLastDateTime9);
        
        myFieloEE_Components=new List<wrapSObject>();
        myFieloEE_RedemptionRules=new List<wrapSObject>();
        myFieloEE_Banners=new List<wrapSObject>();
        myFieloEE_Newss=new List<wrapSObject>();
        myFieloEE_RedemptionRuleCriterias=new List<wrapSObject>();
        myFieloEE_Sections=new List<wrapSObject>();
        myFieloEE_SegmentDomains=new List<wrapSObject>();
        myFieloEE_Tags=new List<wrapSObject>();
        myFieloEE_TagItems=new List<wrapSObject>();
        
        for(FieloEE__Component__c myFieloEE_Component:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__MENU__r.Name,FIELOEE__ACCOUNT__r.Name,F_PRM_Country__c FROM FieloEE__Component__c WHERE F_PRM_Country__c=:myPRMCountry AND ((LastModifiedDate>:myLastDateTime1 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication',''))) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_Components.add(new wrapSObject(myFieloEE_Component));
        }
        for(FieloEE__RedemptionRule__c myFieloEE_RedemptionRule:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,F_PRM_TYPE__C,F_PRM_COUNTRY__R.Name,F_PRM_Country__c FROM FieloEE__RedemptionRule__c WHERE F_PRM_Country__c=:myPRMCountry AND ((LastModifiedDate>:myLastDateTime2 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication',''))) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_RedemptionRules.add(new wrapSObject(myFieloEE_RedemptionRule));
        }
        for(FieloEE__Banner__c myFieloEE_Banner:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__COMPONENT__r.Name,FIELOEE__REDEMPTIONRULE__r.Name,F_PRM_Country__c FROM FieloEE__Banner__c WHERE F_PRM_Country__c=:myPRMCountry AND ((LastModifiedDate>:myLastDateTime3 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication',''))) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_Banners.add(new wrapSObject(myFieloEE_Banner));
        }
        for(FieloEE__News__c myFieloEE_News:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__TITLE__C,FIELOEE__CATEGORYITEM__C,F_PRM_Country__c FROM FieloEE__News__c WHERE F_PRM_Country__c=:myPRMCountry AND ((LastModifiedDate>:myLastDateTime4 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication',''))) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_Newss.add(new wrapSObject(myFieloEE_News));
        }
        for(FieloEE__RedemptionRuleCriteria__c myFieloEE_RedemptionRuleCriteria:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__REDEMPTIONRULE__r.Name,FIELOEE__FIELDNAME__C,FIELOEE__REDEMPTIONRULE__r.F_PRM_Country__c FROM FieloEE__RedemptionRuleCriteria__c WHERE FIELOEE__REDEMPTIONRULE__r.F_PRM_Country__c=:myPRMCountry AND ((LastModifiedDate>:myLastDateTime5 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication',''))) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_RedemptionRuleCriterias.add(new wrapSObject(myFieloEE_RedemptionRuleCriteria));
        }
        for(FieloEE__Section__c myFieloEE_Section:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__MENU__r.Name,FIELOEE__TYPE__C FROM FieloEE__Section__c WHERE (LastModifiedDate>:myLastDateTime6 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication','')) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_Sections.add(new wrapSObject(myFieloEE_Section));
        }
        for(FieloEE__SegmentDomain__c myFieloEE_SegmentDomain:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__SEGMENT__r.Name,FIELOEE__NEWS__r.Name,FIELOEE__SEGMENT__r.F_PRM_Country__c FROM FieloEE__SegmentDomain__c WHERE FIELOEE__SEGMENT__r.F_PRM_Country__c=:myPRMCountry AND ((LastModifiedDate>:myLastDateTime7 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication',''))) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_SegmentDomains.add(new wrapSObject(myFieloEE_SegmentDomain));
        }
        for(FieloEE__Tag__c myFieloEE_Tag:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__ORDER__C,F_PRM_TAGFILTER__C FROM FieloEE__Tag__c WHERE (LastModifiedDate>:myLastDateTime8 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication','')) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_Tags.add(new wrapSObject(myFieloEE_Tag));
        }
        for(FieloEE__TagItem__c myFieloEE_TagItem:[SELECT Id,Name,LastModifiedBy.Username,PublishingStatus__c,PublishingPublisher__c,PublishingCountry__c,FIELOEE__TAG__r.Name,FIELOEE__NEWS__r.Name FROM FieloEE__TagItem__c WHERE (LastModifiedDate>:myLastDateTime9 OR PublishingStatus__c IN ('Ready for Publication','Awaiting Publication','')) LIMIT 1000]){
            //TODO: si >1000
            myFieloEE_TagItems.add(new wrapSObject(myFieloEE_TagItem));
        }
        
        system.debug('###refreshGetters-END###');
    }
    
    //publishFieloEE_Component
    public void publishFieloEE_Component(){
        genericPublisher(myFieloEE_Components,true,'F_PRM_Country__c');
        refreshGetters();
    }
    //unpublishFieloEE_Component
    public void unpublishFieloEE_Component(){
        genericPublisher(myFieloEE_Components,false,null);
        refreshGetters();
    }
    //publishFieloEE_RedemptionRule
    public void publishFieloEE_RedemptionRule(){
        genericPublisher(myFieloEE_RedemptionRules,true,'F_PRM_Country__c');
        refreshGetters();
    }
    //unpublishFieloEE_RedemptionRule
    public void unpublishFieloEE_RedemptionRule(){
        genericPublisher(myFieloEE_RedemptionRules,false,null);
        refreshGetters();
    }
    //publishFieloEE_Banner
    public void publishFieloEE_Banner(){
        genericPublisher(myFieloEE_Banners,true,'F_PRM_Country__c');
        refreshGetters();
    }
    //unpublishFieloEE_Banner
    public void unpublishFieloEE_Banner(){
        genericPublisher(myFieloEE_Banners,false,null);
        refreshGetters();
    }
    //publishFieloEE_News
    public void publishFieloEE_News(){
        genericPublisher(myFieloEE_Newss,true,'F_PRM_Country__c');
        refreshGetters();
    }
    //unpublishFieloEE_News
    public void unpublishFieloEE_News(){
        genericPublisher(myFieloEE_Newss,false,null);
        refreshGetters();
    }
    //publishFieloEE_RedemptionRuleCriteria
    public void publishFieloEE_RedemptionRuleCriteria(){
        genericPublisher(myFieloEE_RedemptionRuleCriterias,true,'FIELOEE__REDEMPTIONRULE__r.F_PRM_Country__c');
        refreshGetters();
    }
    //unpublishFieloEE_RedemptionRuleCriteria
    public void unpublishFieloEE_RedemptionRuleCriteria(){
        genericPublisher(myFieloEE_RedemptionRuleCriterias,false,null);
        refreshGetters();
    }
    //publishFieloEE_Section
    public void publishFieloEE_Section(){
        genericPublisher(myFieloEE_Sections,true,null);
        refreshGetters();
    }
    //unpublishFieloEE_Section
    public void unpublishFieloEE_Section(){
        genericPublisher(myFieloEE_Sections,false,null);
        refreshGetters();
    }
    //publishFieloEE_SegmentDomain
    public void publishFieloEE_SegmentDomain(){
        genericPublisher(myFieloEE_SegmentDomains,true,'FIELOEE__SEGMENT__r.F_PRM_Country__c');
        refreshGetters();
    }
    //unpublishFieloEE_SegmentDomain
    public void unpublishFieloEE_SegmentDomain(){
        genericPublisher(myFieloEE_SegmentDomains,false,null);
        refreshGetters();
    }
    //publishFieloEE_Tag
    public void publishFieloEE_Tag(){
        genericPublisher(myFieloEE_Tags,true,null);
        refreshGetters();
    }
    //unpublishFieloEE_Tag
    public void unpublishFieloEE_Tag(){
        genericPublisher(myFieloEE_Tags,false,null);
        refreshGetters();
    }
    //publishFieloEE_TagItem
    public void publishFieloEE_TagItem(){
        genericPublisher(myFieloEE_TagItems,true,null);
        refreshGetters();
    }
    //unpublishFieloEE_TagItem
    public void unpublishFieloEE_TagItem(){
        genericPublisher(myFieloEE_TagItems,false,null);
        refreshGetters();
    }
    //publishAll
    public void publishAll(){
        genericPublisher(myFieloEE_Components,true,'F_PRM_Country__c');
        genericPublisher(myFieloEE_RedemptionRules,true,'F_PRM_Country__c');
        genericPublisher(myFieloEE_Banners,true,'F_PRM_Country__c');
        genericPublisher(myFieloEE_Newss,true,'F_PRM_Country__c');
        genericPublisher(myFieloEE_RedemptionRuleCriterias,true,'FIELOEE__REDEMPTIONRULE__r.F_PRM_Country__c');
        genericPublisher(myFieloEE_Sections,true,null);
        genericPublisher(myFieloEE_SegmentDomains,true,'FIELOEE__SEGMENT__r.F_PRM_Country__c');
        genericPublisher(myFieloEE_Tags,true,null);
        genericPublisher(myFieloEE_TagItems,true,null);
        refreshGetters();
    }
    //unpublishAll
    public void unpublishAll(){
        genericPublisher(myFieloEE_Components,false,null);
        genericPublisher(myFieloEE_RedemptionRules,false,null);
        genericPublisher(myFieloEE_Banners,false,null);
        genericPublisher(myFieloEE_Newss,false,null);
        genericPublisher(myFieloEE_RedemptionRuleCriterias,false,null);
        genericPublisher(myFieloEE_Sections,false,null);
        genericPublisher(myFieloEE_SegmentDomains,false,null);
        genericPublisher(myFieloEE_Tags,false,null);
        genericPublisher(myFieloEE_TagItems,false,null);
        refreshGetters();
    }   
    //genericPublisher
    private void genericPublisher(List<wrapSObject>myWrapSObjects,Boolean publish /*publish=true unpublish=false*/, String fieldPRMCountryName){
        List<SObject>mySObjects=new List<SObject>();
        for(wrapSObject myWrapSObject:myWrapSObjects){
            if(myWrapSObject.selected){
                Id myId=myWrapSObject.mySObject.Id;
                SObject myUpdSObject = myId.getSObjectType().newSObject(myId);
                myUpdSObject.put('PublishingStatus__c',publish?'Ready for Publication':'');
                if(publish)myUpdSObject.put('PublishingGoldenId__c',myId);
                else myUpdSObject.put('PublishingGoldenId__c',null);
                if(publish)myUpdSObject.put('PublishingPublisher__c',UserInfo.getUserId());
                else myUpdSObject.put('PublishingPublisher__c',null);
                if((publish)&&(fieldPRMCountryName!=null)) {
                    if(fieldPRMCountryName.indexOf('.')==-1){
                        myUpdSObject.put('PublishingCountry__c',myWrapSObject.mySObject.get(fieldPRMCountryName));
                    }
                    else {
                        string temp1;
                        SObject temp2=myWrapSObject.mySObject.getSObject(fieldPRMCountryName.split('\\.')[0]);
                        if(temp2!=null)temp1=(String)temp2.get(fieldPRMCountryName.split('\\.')[1]);
                        myUpdSObject.put('PublishingCountry__c',temp1==null?'':temp1);
                    }
                    
                }
                else myUpdSObject.put('PublishingCountry__c',null);
                //myUpdSObject.put('PublishingPublisherCountry__c',[SELECT Country__c FROM User WHERE Id=:UserInfo.getUserId()]);
                mySObjects.add(myUpdSObject);
            }
        }
        update mySObjects;
    }
}