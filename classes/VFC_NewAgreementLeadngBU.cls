/*
    DESCRIPTION: PREPOPULATES LEADING BU AND REDIRECTS TO THE AGREEMENT EDIT PAGE
*/
public with sharing class VFC_NewAgreementLeadngBU 
{
    public OPPAgreement__c agreement{get;set;}
    public PageReference newAgrPage; 
    
    public VFC_NewAgreementLeadngBU(ApexPages.StandardController controller)
    {
        agreement = (OppAgreement__c)controller.getRecord();
    }
    
    //REDIRECTS TO THE AGREEMENT EDIT PAGE, PREPOPULATING LEADING BU
    public pagereference redirectToNewPage()
    {
        String leadingBU='', keyPrefix;
        keyPrefix = OppAgreement__c.SObjectType.getDescribe().getKeyPrefix();
        newAgrPage = new PageReference('/'+keyPrefix+'/e?retURL=%2F'+keyPrefix+'%2Fo'); 

        //QUERIES USER'S LEADING BU
        String userLeadingBU=[Select id,UserBusinessUnit__c from User where id=:UserInfo.getUserId() limit 1].UserBusinessUnit__c ;
        if(userLeadingBU!='' && userLeadingBU!=null)
        {   
            if(CS_OpportunityLeadingBUMapping__c.getValues(userLeadingBU)!=null)
            { 
                leadingBU = CS_OpportunityLeadingBUMapping__c.getValues(userLeadingBU).OpportunityLeadingBU__c;
                newAgrPage.getParameters().put(Label.CLAPR15SLS50,leadingBU);  
            }
        }
        newAgrPage.getParameters().put('nooverride', '1');
        if(system.currentPagereference().getParameters().get(Label.CLAPR15SLS64)!=null)
        {
            newAgrPage.getParameters().put(Label.CLAPR15SLS64, system.currentPagereference().getParameters().get(Label.CLAPR15SLS64));
            newAgrPage.getParameters().put(Label.CLAPR15SLS64+'_lkid', (system.currentPagereference().getParameters().get(Label.CLAPR15SLS64+'_lkid')));
        }     
        return newAgrPage;
    }
}