public with sharing class VFC_PASubscriberCase{ 
    
    // public list<CaseWrap>lstCheckedUser {get;set;}
    
    //Subclass : Wrapper Class 
    @TestVisible public class CaseWrap {
        //Static Variables 
        public boolean blnCheck {get;set;}
        public User objUser {get;set;}
        public String strAccountName{get;set;}
        public string strCaseId;
        public String strSubscribeId;
        public String strAccountId;
        public String strError;
        //Wrapper  Class Controller
        public CaseWrap(Boolean Check,User obj,string strId,string SubId,String straccName,String strAccId) {
            blnCheck =Check;
            objUser = obj;
            strCaseId = strId;
            strSubscribeId= SubId;
            strAccountName= straccName;
            strAccountId=strAccId;
        }
        
    }
       
    //Method to bring the list of Account and Serialize Wrapper Object as JSON
    public  static String getlstCase() {
        Id objectId;
        String strCaseId;
        Set<id>UserId= new set<ID>();
        list<CaseWrap>lstCheckedUser= new list<CaseWrap>();
        list<CaseWrap>lstUnCheckedUser= new list<CaseWrap>();
        Set<Id> UserIdSet = new Set<Id>();
        objectId =ApexPages.currentPage().getParameters().get('id');
        if(objectId!= null){
            Schema.SObjectType token = objectId.getSObjectType();
            Schema.DescribeSObjectResult dr = token.getDescribe();
            if(dr.getName() == 'Case') {
                strCaseId =objectId;
                for(Subscriber__c obj :[select id,Email__c, Name,User__r.FirstName,User__c,User__r.LastName,User__r.Email,User__r.contact.FirstName,User__r.contact.LastName,User__r.contact.email,User__r.contact.Account.Name, User__r.contact.Account.Id from Subscriber__c where Case__c =:objectId ]){
                    User objU = new User(id = obj.User__c, FirstName = obj.User__r.contact.FirstName,LastName=obj.User__r.contact.LastName,Email =obj.User__r.contact.email);
                    CaseWrap  cu = new CaseWrap (true,objU,strCaseId,obj.id, obj.User__r.contact.Account.Name, obj.User__r.contact.Account.Id);
                    lstCheckedUser.add(cu);
                    UserId.add(obj.User__c);
                
                }

                List<Case> lstCase = [Select id,AccountId,ContactID From Case Where Id=:strCaseId];
                /*for(AccountShare u :[SELECT UserOrGroupId FROM AccountShare WHERE AccountId =: lstCase[0].AccountId AND (RowCause = 'Owner' OR RowCause = 'Manual')]){
                    UserIdSet.add(u.UserOrGroupId);
                }*/
                UserIdSet = PA_UtilityClass.getAccountShareUserId(lstCase[0].AccountId);
                system.debug('UserIdSet'+UserIdSet);
                for(User objUser :[select id,
                                   FirstName,
                                   LastName,
                                   Email,
                                   Contact.Email, 
                                   ContactId, 
                                   Contact.FirstName, 
                                   Contact.LastName, 
                                   Contact.Account.Name, 
                                   Contact.Account.Id from User 
                                   where ID NOT IN :UserId  AND 
                                   profileId =: system.label.CLQ316PA037 AND
                                   IsActive =true And 
                                   Id in:UserIdSet ]){
                    User objU = new User(id = objUser.id, FirstName = objUser.Contact.FirstName,LastName=objUser.Contact.LastName,Email =objUser.contact.email);
                    CaseWrap  cu = new CaseWrap (false,objU,strCaseId,null,objUser.Contact.Account.Name, objUser.Contact.Account.Id);
                    lstUnCheckedUser.add(cu);
                 }
            
            }
        }
        system.debug('lstCheckedUser1:'+lstCheckedUser);
        if(lstUnCheckedUser.size()>0){
            lstCheckedUser.addAll(lstUnCheckedUser);

        }
    
        system.debug('lstUnCheckedUser'+lstUnCheckedUser);
        return JSON.serialize(lstCheckedUser);
     }
     @RemoteAction
     public static String getClassInstances(string strJson,String caseid){
       system.debug(strJson);
       List<Subscriber__c>lstsubscriber = new List<Subscriber__c>();
       system.debug('+++++++++strJson '+strJson );
       system.debug('+++++++++strJson '+caseid );
       if(strJson !='[]' && strJson != null) {  
           List<Object> m = (List<Object>)JSON.deserializeUntyped(strJson);
            //system.debug(m.get(strCaseId));
            
            string strCaseId;
                                
            for (Object o : m) {
                Map<String, Object> p = (Map<String, Object>) o;
                
                strCaseId =(String)p.get('strCaseId');
                string strAccountId=(string)p.get('strAccountId');
                Id subscribedId =(String)p.get('strSubscribeId');
                Map<String, Object> oy =(Map<String, Object>)p.get('objUser');
                system.debug('oy===='+oy);
                string strId= (string)oy.get('Id');
                string strEMail= (string)oy.get('Email');
                
                
                Subscriber__c sb = new Subscriber__c();
                sb.Case__c = strCaseId;
                sb.User__c=strId;
                sb.Email__c=strEMail;
                //sb.Account__c = strAccountId;
                if(subscribedId != null){
                   sb.id= subscribedId;
                }
                lstsubscriber.add(sb);
                


            }
            system.debug('lstsubscriber'+lstsubscriber);
            if(!lstsubscriber.IsEmpty()){
                upsert lstsubscriber;
            }
            lstsubscriber = new List<Subscriber__c>([select id from Subscriber__c where Id NOT IN :lstsubscriber and case__c=:strCaseId]);
            if(!lstsubscriber.isEmpty()){
                delete lstsubscriber;
            }
        } else
        {
            lstsubscriber = new List<Subscriber__c>([select id from Subscriber__c where case__c =:caseid]);
            if(!lstsubscriber.isEmpty()){
                delete lstsubscriber;
            }
            
        }
        system.debug('+++++++++caseid '+caseid );
        return caseid;
     }
     @RemoteAction
     public static list<CaseWrap> subscriberResult(String strFilter, String strCaseId,String strJason){
         list<CaseWrap>lstCheckedUser = new list<CaseWrap>();
         Set<Id> ConIdSet = new Set<Id>();
         Set<Id> UserIdSet = new Set<Id>();
         system.debug('strFilter'+strFilter);
         strFilter='%'+strFilter+'%';
         List<Object> m = (List<Object>)JSON.deserializeUntyped(strJason);
         Set<String>setUserId =new Set<String>();
         for (Object o : m) {
            Map<String, Object> p = (Map<String, Object>) o;
            Map<String, Object> oy =(Map<String, Object>)p.get('objUser');
            string strId= (string)oy.get('Id');
            setUserId.add(strId);
         }
        List<Case> lstCase = [Select id,AccountId,ContactID From Case Where Id=:strCaseId];
        
        /*for(AccountShare u :[SELECT UserOrGroupId FROM AccountShare WHERE AccountId =: lstCase[0].AccountId AND (RowCause = 'Owner' OR RowCause = 'Manual')]){
            UserIdSet.add(u.UserOrGroupId);
        }*/
        UserIdSet = PA_UtilityClass.getAccountShareUserId(lstCase[0].AccountId);
        system.debug('UserIdSet'+UserIdSet);
        for(User objUser :[select id,
                           FirstName,
                           LastName,
                           Email,
                           Contact.Email, 
                           ContactId, 
                           Contact.FirstName, 
                           Contact.LastName, 
                           Contact.Account.Name, 
                           Contact.Account.Id from User 
                           where Name Like :strFilter  AND 
                           profileId =: system.label.CLQ316PA037 AND
                           IsActive =true And 
                           Id in:UserIdSet]){
            User objU = new User(id = objUser.id, FirstName = objUser.Contact.FirstName,LastName=objUser.Contact.LastName,Email =objUser.contact.email);
            CaseWrap  cu = new CaseWrap (false,objU,strCaseId,null,objUser.Contact.Account.Name, objUser.Contact.Account.Id);
            lstCheckedUser.add(cu);
         }
          system.debug('UserIdSet'+UserIdSet);
         if(lstCheckedUser.size() >0){
             
             system.debug(lstCheckedUser.size());
             system.debug(strFilter);
             system.debug(lstCheckedUser.size()>=1 && strFilter !='%%');
            if(lstCheckedUser.size()>=1 && strFilter !='%%'){
                if(lstCheckedUser.size()==1 && setUserId.contains(lstCheckedUser[0].objUser.id) ){
                     lstCheckedUser[0].strError='User already subscribe this case'; 
                     return lstCheckedUser;
                 }
                 for (Integer i = (lstCheckedUser.size()-1) ; i>= 0 ; i--){
                     if(setUserId.contains(lstCheckedUser[i].objUser.id)){
                            lstCheckedUser.remove(i);
                     }
                 }
                  system.debug('lstCheckedUser'+lstCheckedUser);
                  if(lstCheckedUser.size()==0){
                    CaseWrap  cu = new CaseWrap (false,null,null,null,null,null); 
                    cu.strError='User already subscribe this case'; 
                     lstCheckedUser.add(cu);

                  }
                 return lstCheckedUser;
             }
             if(lstCheckedUser.size()>=1 && strFilter =='%%'){
                system.debug(strFilter);
                for (Integer i = (lstCheckedUser.size()-1) ; i>= 0 ; i--){
                     if(setUserId.contains(lstCheckedUser[i].objUser.id)){
                            lstCheckedUser.remove(i);
                     }
                 }
                 if(lstCheckedUser.size()==0){
                    CaseWrap  cu = new CaseWrap (false,null,null,null,null,null); 
                    cu.strError='No User'; 
                     lstCheckedUser.add(cu);

                  }
                return lstCheckedUser;
             }   
         }
        CaseWrap  cu = new CaseWrap (false,null,null,null,null,null); 
        cu.strError='No record Found'; 
        lstCheckedUser.add(cu);
        return lstCheckedUser;
     }
     /*public class ObjUser {
        
            public String Id {get;set;} 
            public String FirstName {get;set;} 
            public String LastName {get;set;} 
            public String Email {get;set;}
            public String AccountName{get;set;}
            public String AccountId{get;set;}
            
        }*/
    
}