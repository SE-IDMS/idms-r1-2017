public class SVMXC_WorkDetailsBefore {

    public static void setBillableFlagFromWOAndIsAdminFlag (List<SVMXC__Service_Order_Line__c> triggerNew, Map<Id, SVMXC__Service_Order_Line__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set Billable flag from value on WO (Written March 25, 2015)
            
            Updated : Feb 15, 2016 - Added in Support to set Is Admin WO Flag
            
        */
        /*
        Set<Id> WOIds = new Set<Id>();
        
        for (SVMXC__Service_Order_Line__c sol : triggerNew) {
            if (sol.SVMXC__Service_Order__c != null && !WOIds.contains(sol.SVMXC__Service_Order__c))
                WOIds.add(sol.SVMXC__Service_Order__c);
        }
        
        if (!WOIds.isEmpty()) {
            Map<Id, SVMXC__Service_Order__c> WOMap = new Map<Id, SVMXC__Service_Order__c>
                ([SELECT IsBillable__c, RecordType.Name FROM SVMXC__Service_Order__c WHERE Id IN: WOIds]);
            
            if (!WOMap.isEmpty()) {
                for (SVMXC__Service_Order_Line__c sol2 : triggerNew) {
                    String billableFlagOnWO = WOMap.get(sol2.SVMXC__Service_Order__c).IsBillable__c;
                    if (billableFlagOnWO == 'Yes') {
                        sol2.SVMXC__Is_Billable__c = true;
                        sol2.IsBillable__c = 'Yes';
                    } else if (billableFlagOnWO == 'No') {
                        sol2.SVMXC__Is_Billable__c = false;
                        sol2.IsBillable__c = 'No';
                    }
                    
                    if (WOMap.containsKey(sol2.SVMXC__Service_Order__c) && WOMap.get(sol2.SVMXC__Service_Order__c).RecordType.Name == 'Administration Work Order')
                        sol2.Is_For_Admin_Work_Order__c = true;
                }
            }
        }
        */
    }

    public static void clearSecondsFromStartAndEndTime (List<SVMXC__Service_Order_Line__c> triggerNew, Map<Id, SVMXC__Service_Order_Line__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set clear out the seconds in the start and end time (Written Sept 9, 2014)
            
        */
        /*
        for (SVMXC__Service_Order_Line__c sol : triggerNew) {
            if (sol.SVMXC__Start_Date_and_Time__c != null) {
                DateTime workingTime = sol.SVMXC__Start_Date_and_Time__c;
                sol.SVMXC__Start_Date_and_Time__c = DateTime.newInstance(workingTime.year(), workingTime.month(), workingTime.day(), workingTime.hour(), workingTime.minute(), 0);
            }
            if (sol.SVMXC__End_Date_and_Time__c != null) {
                DateTime workingTime2 = sol.SVMXC__End_Date_and_Time__c;
                sol.SVMXC__End_Date_and_Time__c = DateTime.newInstance(workingTime2.year(), workingTime2.month(), workingTime2.day(), workingTime2.hour(), workingTime2.minute(), 0);
            }
        }
        */
    }
    
    /**
    * This method is a duplicate from AP_WorkDetails.setPrimaryFSR
    *
    */
    public static void setTechWhenNull (List<SVMXC__Service_Order_Line__c> triggerNew, Map<Id, SVMXC__Service_Order_Line__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set Technician/Equipment on insert/update if the value is null.  
            It will be set to the running users related tech record if it exists (Written 12/11/2013)
            
            Updated to only consider Actual lines to set the Technician (August 3, 2015)
            
        */
        
        /*
        // can't use the api call because it uses translated values and that could be different depending on user localization
        //actualsRTId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Actuals').getRecordTypeId();
        Id usageConsumptionRTId = [SELECT Name, Id, SobjectType FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' and Name = 'Usage/Consumption'].Id;
             
        Set<Id> WOLIds = new Set<Id>();
        
        for (SVMXC__Service_Order_Line__c sol : triggerNew) {
            if (sol.SVMXC__Group_Member__c == null && sol.RecordTypeId == usageConsumptionRTId)
                WOLIds.add(sol.Id); 
        }
        
        if (!WOLIds.isEmpty()) {
            
            List<SVMXC__Service_Group_Members__c> techList = [SELECT Id FROM SVMXC__Service_Group_Members__c 
                                WHERE SVMXC__Salesforce_User__c =: UserInfo.getUserId()];
            
            if (!techList.isEmpty()) {
                
                // assumes that only one tech record is found for running user
                Id techId = techList[0].Id;
                
                for (SVMXC__Service_Order_Line__c sol1 : triggerNew) {
                    if (WOLIds.contains(sol1.Id) && sol1.SVMXC__Group_Member__c == null)
                        sol1.SVMXC__Group_Member__c = techId;
                }   
            }
        }
        */
    }

    /**
     * Update the SVMXC__Start_Date_and_Time__c (FSR Start date/time) with the CustomerStartDateandTime__c if SVMXC__Start_Date_and_Time__c is empty.
     * Update the SVMXC__End_Date_and_Time__c (FSR End date/time) with the CustomerEndDateandTime__c if SVMXC__End_Date_and_Time__c is empty.
     * @param aTriggerNew List of Work Details being saved with the new values
     * @param aTriggerOld List of Work Details being saved with the old values
     * @param isInsert Indicates that the trigger is call for an insert operation 
     * @param isUpdate Indicates that the trigger is call for an update operation 
     */
    public static void setFSRTimesFromCustomerTimes(List<SVMXC__Service_Order_Line__c> aTriggerNew, Map<Id, SVMXC__Service_Order_Line__c> aTriggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
        //filter out in order to only deal with Actuals
        Id usageConsumptionRTId = [SELECT Name, Id, SobjectType FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' and Name = 'Usage/Consumption'].Id;
        Set<Id> WOLIds = new Set<Id>();
        
        for (SVMXC__Service_Order_Line__c sol : aTriggerNew) {
            if (sol.SVMXC__Start_Date_and_Time__c == null && sol.RecordTypeId == usageConsumptionRTId) {
                sol.SVMXC__Start_Date_and_Time__c = sol.CustomerStartDateandTime__c; 
            }
            if (sol.SVMXC__End_Date_and_Time__c == null && sol.RecordTypeId == usageConsumptionRTId) {
                sol.SVMXC__End_Date_and_Time__c = sol.CustomerEndDateandTime__c; 
            }
             
        }
        */
    }
    
}