@isTest
private class VFC81_NewCase_TEST
{
    static testMethod void testVFC81_NewCase_TestMethod() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
        
        CaseClassification__c objCaseClassification = Utils_TestMethods.createCaseClassification();
        insert objCaseClassification;
    
        Case objCase = new Case();
        
        PageReference VFC81_PageRef = new PageReference('/apex/VFC81_NewCase?returl=/'+contact1.ID+'&def_contact_id=' +contact1.ID+'&def_account_id='+account1.Id );
        Test.setCurrentPage(VFC81_PageRef); 
        PageReference pageRef = ApexPages.currentPage();
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC81_NewCase NewCaseController = new VFC81_NewCase(CaseStandardController);
        
        //NewCaseController.predefinedCases[0].goToCaseEditPage(); 
        NewCaseController.cancel();
        
        PageReference VFC81_PageRef1 = new PageReference('/apex/VFC81_NewCase?returl=/'+account1.Id+'&def_account_id='+account1.Id );
        Test.setCurrentPage(VFC81_PageRef1); 
        PageReference pageRef1 = ApexPages.currentPage();
        NewCaseController.cancel();
        
        PageReference VFC81_PageRef2 = new PageReference('/apex/VFC81_NewCase');
        Test.setCurrentPage(VFC81_PageRef2); 
        PageReference pageRef2 = ApexPages.currentPage();
        NewCaseController.cancel();
    }
}