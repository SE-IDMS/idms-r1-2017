/*    Author          : Accenture Team    
      Date Created    : 18/08/2011     
      Description     : Controller extensions for VFP59_CreateSupportRequest.This class will check whether the logged in User is System Admin or Delegated Admin or                       
                        or Opportunity Owner or Exist in Opportunity Sales Team and then allow user based on that */
public class VFC59_CreateSupportRequest {

    public OPP_SupportRequest__c crntSupReq = New OPP_SupportRequest__c();
    public PageReference pgRef;
    public Boolean flag;
    public Boolean displayError{get;set;}
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
    public List<opportunityTeamMember> opptyTeam = New List<opportunityTeamMember>();
    
    public VFC59_CreateSupportRequest(ApexPages.StandardController controller) 
    {
        crntSupReq = (OPP_SupportRequest__c)controller.getRecord();
        flag = False;
        displayError = False;
    }
    
    
    public PageReference checkAccess()
    {
        for(Profile prof : [Select Id, Name from Profile where Name Like :System.Label.CL00498])                
        {
            if(userinfo.getProfileId() == prof.Id)
            {
                flag=true;
                displayError = False;
            }
            else
                flag=False;
        }
        
        String oppId;
        if(pageParameters.get(Label.CL00550) != NULL)
        {
            oppId = pageParameters.get(Label.CL00550);
        }
        
        if(!flag && oppId != NULL)
        {
            String Id =[select id from recordtype where name=:Label.Cl00538 and sObjectType='OPP_SupportRequest__c'].id;
            System.debug('********'+Id);
            System.debug('********'+pageParameters.get('RecordType'));
            if(!Id.contains(pageParameters.get('RecordType')))
            {
                opptyTeam = [select OpportunityID,userId from opportunityTeamMember where OpportunityID =: oppId and opportunityAccessLevel =:Label.CL00203];
                
                if(!opptyTeam.isEmpty())
                {    
                    for(opportunityTeamMember team : opptyTeam)
                    {
                        if(userinfo.getuserId() == team.userId)
                            flag = True;
                    }
                }
            }
            
            if(userinfo.getuserId() == [Select OwnerId, Id from Opportunity where Id = :oppId LIMIT 1].OwnerId)
            {
                flag = True;
            }            
        }
        
        if(!flag && oppId != NULL)
        {    
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00530));
            displayError = True;
            pgRef = NULL;
            
        }
        else
        {
            displayError = False;
            flag = true;
            pgRef = new PageReference('/'+SObjectType.OPP_SupportRequest__c.getKeyPrefix()+'/e');
            for(String param : pageParameters.keySet())                
                pgRef.getParameters().put(param, pageParameters.get(param));
            if(pgRef.getParameters().containsKey('save_new'))                
                pgRef.getParameters().remove('save_new');
            pgRef.getParameters().put('nooverride', '1');
        }
        return pgRef;
    }
       
    
    
    public pagereference cancel()
    {
        System.Debug('****** Cancel Method Begins  for  VFC59_CreateSupportRequest******');
        pagereference pg;
        if(pageParameters.get(Label.CL00550)!=null)
        pg = new pagereference('/'+pageParameters.get(Label.CL00550));
        system.debug('------Page reference -----'+pg);
        System.Debug('****** Cancel Method Ends for  VFC59_CreateSupportRequest******');
        if(pg!=null)
            return pg;
        else
            return null;
    } 

}