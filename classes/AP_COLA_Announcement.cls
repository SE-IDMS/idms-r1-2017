/*
Description:Related to Announcemnt Trigger Functionality

Release :Oct 2014

*/
 
 public class AP_COLA_Announcement {
 
    public static void UpdateOfferDescription(List<Announcement__c> ListAnnct) {  
        set<string> setOferId = new set<string>();
        set<string> setAnncetId = new set<string>();
        for(Announcement__c objAnnomnet:ListAnnct) {
            setOferId.add(objAnnomnet.Offer_Lifecycle__c);
            setAnncetId.add(objAnnomnet.id);
        }
        List<Announcement__c> listUpdtAnnounct=new  List<Announcement__c>();
        List<Announcement__c> listAnnounnt=[select id,Offer_Lifecycle__c,OfferDescription__c,SubstitutionStrategy__c from Announcement__c where id=:setAnncetId];
        
        List<Offer_Lifecycle__c> listOfferCycle=[select id,Description__c,Substitution_Strategy__c from Offer_Lifecycle__c where id=:setOferId];
        if(listOfferCycle.size() > 0) {
            for(Announcement__c annObj:listAnnounnt) {
                for(Offer_Lifecycle__c offerObj:listOfferCycle) {
                    if(offerObj.id==annObj.Offer_Lifecycle__c) {
                        annObj.OfferDescription__c=offerObj.Description__c;
                        annObj.SubstitutionStrategy__c = offerObj.Substitution_Strategy__c;
                        listUpdtAnnounct.add(annObj);
                    }
                }
            
            } 
        }   
        
        if(listUpdtAnnounct.size() > 0) {
            update listUpdtAnnounct;
        }
    } //end Of Method 
   
 
 }