/*
    Author          : Shruti Karn
    Date Created    : 28/11/2012
    Description     : Test class for Work Order triggers
*/
@isTest(SeeAllData=true)
private class WorkOrder_Triggers_TEST {

    static testMethod void testWO(){
        Profile profile1 = [select id, name from profile where name = 'System Administrator' ];    
            //User creation
            User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profile1.Id,BypassVR__c = true, BypassWF__c = true,BypassTriggers__c = 'CaseAfterInsert;CaseBeforeInsert;AP54;AP_Contact_PartnerUserUpdate;SVMX21;SVMX05;AP_LocationManager;SVMX06;SVMX07;SVMX05;SRV04;SVMX09',
            timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',UserBusinessUnit__c = 'EN');
            insert user;
            system.runAs(user){

            //test data
            Account testAccount = New Account(Name='TestAccount',
                                                Street__c='New Street',
                                                POBox__c='XXX',
                                               // OwnerId = ownerId, 
                                                ZipCode__c='12345');
            insert testAccount;
            
            Contact testContact = New Contact(FirstName='Test',
                                                LastName='ContactName',
                                                AccountId=testAccount.Id,
                                                JobTitle__c='Z3',
                                                CorrespLang__c='EN',
                                                WorkPhone__c='1234567890',
                          MobilePhone ='111111'
                                               );
            insert testContact;
            
            Case testCase = New Case(       //Status=StatusValue,
                                            AccountId=testAccount.Id,
                                            ContactId=testContact.Id,
                                            SupportCategory__c='6-General',
                                            Priority='Medium',
                                            Subject='Complaint',
                                            origin='Phone');
            insert testCase;
            
            Country__c testCountry = Utils_TestMethods.createCountry();
            insert testCountry;
            
           
            
            SVMXC__Site__c testCustLocation = New SVMXC__Site__c(SVMXC__Account__c=testAccount.Id,
                                                                 Name='test',
                                                                 SVMXC__Street__c='teststreet',
                                                                 LocationCountry__c=testCountry.Id,
                                                                    SVMXC__Zip__c='12312'
            
                                                                                );
            
            
        insert testCustLocation;        
            //WorkOrderNotification__c testWON = Utils_TestMethods.createWorkOrderNotification(testCase.Id, testCustLocation.Id);
            //testWON.OnSiteContact__c =testContact.Id; 
            //insert testWON;
            
            Profile profile = [Select id from Profile where name ='SE - SRV Standard User (technician)' limit 1];
            User ServiceUser =  Utils_TestMethods.createStandardUser('VFCWO');
            ServiceUser.profileId = profile.Id;
            insert ServiceUser;
            
            SVMXC__Service_Group__c ServiceGrp = Utils_TestMethods.createServiceGrp();
            insert ServiceGrp;
            SVMXC__Service_Group_Members__c newTech = Utils_TestMethods.createTech_Equip(ServiceUser.Id,ServiceGrp.Id);
            newTech.PrimaryAddress__c = 'test';
            newTech.SVMXC__Country__c = testCountry.Id;
            newTech.SVMXC__Email__c = 'test@company.com';
            insert newTech;
            
            
            
                            
            SVMXC__Service_Order__c testWorkOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
            testWorkOrder.SVMXC__Site__c = testCustLocation.Id;
            testWorkOrder.SVMXC__Country__c = testCountry.id;
            testWorkOrder.SoldToAccount__c = testAccount.id;
            testWorkOrder.SVMXC__Contact__c = testContact.id;
            insert testWorkOrder;
        ////////////////////////////////////
         SVMXC__Service_Order__c ChildWorkOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
         ChildWorkOrder.Contact_Phone__c =testContact.MobilePhone ;
         ChildWorkOrder.CaseOwner__c =testCase.Owner.Name ;
         ChildWorkOrder.Parent_Work_Order__c = testWorkOrder.Id;
             insert ChildWorkOrder;
            //////////////
            test.startTest();
            AssignedToolsTechnicians__c newTools = New AssignedToolsTechnicians__c();
            newTools.PrimaryUser__c = true;
            newTools.TechnicianEquipment__c=newTech.Id;
            newTools.WorkOrder__c = testWorkOrder.Id;
            insert newTools;
            
            
            
            Event testEvent = new Event();
            testEvent.WhatId = testWorkOrder.Id;
            testEvent.StartDateTime=system.now();
            testEvent.EndDateTime=system.now()+1;
            insert testEvent;
            
            StateProvince__c testState = Utils_TestMethods.createStateProvince(testCountry.Id);
            insert testState;
            
            SVMXC__Territory__c testTerritory  = Utils_TestMethods.createSRVTerritory();
            insert testTerritory;
            //test scenarios
            
            testWorkOrder.customerconfirmed__c = true;
            //testWorkOrder.SVMXC__Group_Member__c = newTech.Id;
            
            //commenting custome confirm - Deepak-oct 13
            testWorkOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
            //testWorkOrder.Work_Order_Notification__c = testWON.Id;
            //testWorkOrder.BackOfficeReference__c = '11122';
            //update testWorkORder;
            testWorkOrder.SVMXC__Order_Status__c='Rescheduled';
            testWorkOrder.SVMXC__Primary_Territory__c = testTerritory.Id;
          // update testWorkOrder;
            /*
            testWorkOrder.SVMXC__Order_Status__c='Service Validated';
           update testWorkOrder;
            testWorkOrder.SendEmailToThirdParty__c = false; 
            testWorkOrder.Work_Order_Category__c = '3rd Party';
            testWorkOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
            testWorkOrder.TechnicianType__c = 'Non-Schneider Employee';
            testWorkOrder.SendEmailToCustomer__c = true;
            //testWorkOrder.CustomerRequestedDate__c = system.today();
            //testWorkOrder.Customer_Service_Request_Time__c = system.now();
            testWorkOrder.Customer_Location__c = testCustLocation.Id;
            testWorkORder.SVMXC__Case__c = testCase.ID;
            testWorkORder.SVMXC__Contact__c = testContact.Id;
            //update testWorkOrder;
            
           // testWorkOrder.Work_Order_Category__c = 'Onsite';*/
            test.stopTest();
        }
    }
}