public with sharing class FieloPRM_C_CockpitComponentController {
    
    public Id currentComponentId{get;set;}
    public Boolean isError{get;set;}
    public String errorMsg{get;set;}
    public String layoutType;
    public FieloPRM_C_CockpitComponentController() {
        
        isError = false;
        System.debug('construtor');
        //initWidget();
    }

    public Widget getComponentConfig(){
        try {
            System.debug('component Id ' + currentComponentId);
            Id memberId = FieloEE.MemberUtil.getmemberId();
            Id categoryId, tagId;
            FieloEE__Member__c member;
            
            for (FieloEE__Member__c m : [SELECT F_Country__c,F_Country__r.CountryCode__c,F_Account__c,F_PRM_Country__c,F_PRM_Country__r.SupportedLanguage1__c,F_PRM_Country__r.SupportedLanguage2__c,F_PRM_Country__r.SupportedLanguage3__c,FieloEE__User__r.LanguageLocaleKey  FROM FieloEE__Member__c WHERE Id =: memberId]){
                member = m;
                break;
            }
            System.debug('member ' + member);
            //Suuported LanguageNumber Logic
            Integer languageNumber=1;
            if((member.F_PRM_Country__r.SupportedLanguage2__c!=null || member.F_PRM_Country__r.SupportedLanguage3__c!=null) && member.FieloEE__User__r.LanguageLocaleKey!=null){
            	if(member.FieloEE__User__r.LanguageLocaleKey.equals(member.F_PRM_Country__r.SupportedLanguage2__c)){
            		languageNumber=2;
            	}else if(member.FieloEE__User__r.LanguageLocaleKey.equals(member.F_PRM_Country__r.SupportedLanguage3__c)){
            		languageNumber=3;
            	}
            }

            Id prmCountryId =member.F_PRM_Country__c;
            System.debug('prmCountryId '  + prmCountryId);
            FieloEE__Component__c theComponent;
            for (FieloEE__Component__c c :[select FieloEE__Category__c,F_PRM_FrontEndStyle__c,FieloEE__Layout__c,FieloEE__Menu__r.F_PRM_Category__c,FieloEE__Tag__c from FieloEE__Component__c where id=:currentComponentId]){
                categoryId = c.FieloEE__Category__c !=null?c.FieloEE__Category__c:(c.FieloEE__Menu__r.F_PRM_Category__c!=null?c.FieloEE__Menu__r.F_PRM_Category__c:getParam('idCategory'));
                tagId = c.FieloEE__Tag__c !=null?c.FieloEE__Tag__c:getParam('idTag');
                theComponent =c;
                break;
            }
            

            System.debug('categoryId ' + categoryId);
            System.debug('tagId ' + tagId);

            List<PRMCockpitComponentConfiguration__c> cmp =  [select Id,ChannelOrProgram__c,CountryCluster__c,Tag__c,Label__c,Layout__c,Type__c,WSURL__c, (Select Id From Attachments),Icon__c,LabelLang2__c,LabelLang3__c,
                                                (SELECT name,ChannelOrProgram__c,Content__c,ContentFromWS__c,Label__c,Tag__c,Link__c,Visible__c,LabelLang2__c,LabelLang3__c FROM PRM_CockpitElementConfigurations__r where Visible__c=true Order by Order__c asc )
                                            from PRMCockpitComponentConfiguration__c 
                                            where (CountryCluster__c=:prmCountryId AND ChannelOrProgram__c=null AND Tag__c=null) OR
                                                  (CountryCluster__c=:prmCountryId AND ChannelOrProgram__c=:categoryId AND Tag__c=null) OR 
                                                  (CountryCluster__c=:prmCountryId AND ChannelOrProgram__c=:categoryId AND Tag__c=:tagId) 
                                            order by Tag__c NULLS LAST,ChannelOrProgram__c NULLS LAST limit 1]; 
            System.debug('cmp ' + cmp);
            if(cmp.size()>0){
                return initWidget(cmp[0],theComponent,languageNumber);
            }else{
                return null;
            }
            
        } catch(Exception ex) {
            isError = true;
           errorMsg = 'The following exception has occurred: getLineNumber: '+ex.getLineNumber()+' getMessage '+ex.getMessage()+' getStackTraceString '+ex.getStackTraceString();
            System.debug(errorMsg);

        }
        return null;
        
    }

    private Widget initWidget(PRMCockpitComponentConfiguration__c cmp,FieloEE__Component__c theComponent,Integer languageNumber) {
        User currUser = [select id, PRMTemporarySamlToken__c, FederationIdentifier,Contact.PRMEmail__c,Contact.PRMCountry__r.CountryCode__c from User where id = :Userinfo.getUserId()]; 
        String token = currUser.PRMTemporarySamlToken__c;
        token = token!=null?token.replaceAll('\r\n', '').replaceAll('\n', '').replaceAll('\r', ''):'';
      
        PRM_BEMMiddlewareUtils.ParameterBean parameterBean = new PRM_BEMMiddlewareUtils.ParameterBean(cmp.Type__c,token,currUser.FederationIdentifier,currUser.Contact.PRMEmail__c,currUser.Contact.PRMCountry__r.CountryCode__c);  
        PRM_BEMMiddlewareUtils.WidgetBean widgetBean = PRM_BEMMiddlewareUtils.getWidget(cmp,parameterBean,null);
        System.debug('widgetBean: ' + widgetBean);
        Map<String,String> elementByName = new Map<String,String>();
        if(widgetBean.returnCode!=PRM_BEMMiddlewareUtils.RETURN_MESSAGE_ERROR){
            if(widgetBean.elements!=null){
                for(PRM_BEMMiddlewareUtils.ElementBean elm:widgetBean.elements){
                    elementByName.put(elm.name,elm.content);
                }
            }
        }else{
            errorMsg = widgetBean.returnMessage;
        }

        Widget myWidget = new Widget();
        myWidget.Id = cmp.Id;
        myWidget.ChannelOrProgram = cmp.ChannelOrProgram__c;
        myWidget.CountryCluster = cmp.CountryCluster__c;
        myWidget.Tag = cmp.Tag__c;
        myWidget.Label = languageNumber<=1?cmp.Label__c:(languageNumber==2?cmp.LabelLang2__c:cmp.LabelLang3__c);
        myWidget.Layout = cmp.Layout__c;
        myWidget.Type = cmp.Type__c;
        myWidget.WSURL = cmp.WSURL__c;
        myWidget.FrontEndStyle = 'bannerRow banner '+(theComponent.F_PRM_FrontEndStyle__c!=null?theComponent.F_PRM_FrontEndStyle__c.replace(';',' '):'banner_lightgreen');
        System.debug(cmp.PRM_CockpitElementConfigurations__r);
        myWidget.elements = new List<Element>();
        for (PRMCockpitElementConfiguration__c e: cmp.PRM_CockpitElementConfigurations__r) {
            System.debug('e ' + e);
            Element elem = new Element();
            elem.ChannelOrProgram = e.ChannelOrProgram__c;
            elem.Content = (e.ContentFromWS__c && elementByName.containsKey(e.name))?elementByName.get(e.name):e.Content__c;
            elem.ContentFromWS = e.ContentFromWS__c;
            elem.Label = languageNumber<=1?e.Label__c:(languageNumber==2?e.LabelLang2__c:e.LabelLang3__c);
            elem.Tag = e.Tag__c;
            elem.Link = e.Link__c;
            elem.Visible = e.Visible__c;
            myWidget.elements.add(elem);
        }
        List<Attachment> att = cmp.Attachments;
        if(att!=null && att.size()>0){
            myWidget.isAttachment=true;
            myWidget.image = att[0].Id;
        }else{
            myWidget.isAttachment=false;
            myWidget.image = cmp.Icon__c;
        }
        System.debug('myWidget' + myWidget);
        return myWidget;
    }

    public static String getParam(String name){
        return ApexPages.currentPage().getParameters().get(name);
    }

    public class Widget {
        public String Id{get;set;}
        public String ChannelOrProgram{get;set;}
        public String CountryCluster{get;set;}
        public String Tag{get;set;}
        public String Label{get;set;}
        public String Layout{get;set;}
        public String Type{get;set;}
        public String WSURL{get;set;}
        public String FrontEndStyle{get;set;}
        public List<Element> elements{get;set;}
        public String image{get;set;}
        public Boolean isAttachment{get;set;}
        public Integer supportedLanguageNumber{get;set;}
        
    }

    public class Element{
        public String ChannelOrProgram{get;set;}
        public String Content{get;set;}
        public Boolean ContentFromWS{get;set;}
        public String Label{get;set;}
        public String Tag{get;set;}
        public String Link{get;set;}
        public Boolean Visible{get;set;}
    } 
}