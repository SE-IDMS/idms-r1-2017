@isTest
private class AP_Case_UpdateCaseSolvingTeam_TEST{
    public static Account    objTestAccountForCase = null;
    public static Contact    CaseContactA = null;
    public static Contact    CaseContactB = null;
    
    public static Country__c    CaseRelatedCountry = null;
    
    public static CustomerCareTeam__c PrimaryCareTeamA  = null;
    public static CustomerCareTeam__c PrimaryCareTeamB  = null;
    
    public static User ByPassWFUser = null;
    
    public static User PrimaryAgent  = null;
    public static User PrimaryAgentManager  = null;
    
    public static User AccPreferredAgent  = null;
    public static User AccPreferredAgentBackup  = null;
    
    public static User ContactAPreferredAgent  = null;
    public static User ContactAPreferredAgentBackup  = null;
    
    public static User ContactBPreferredAgent  = null;
    public static User ContactBPreferredAgentBackup  = null;
    
    
    public static User TeamLeaderA  = null;
    public static User TeamLeaderB  = null;
    
    public static Case testCase = null;
    public static boolean blnUserCreated = false;
    
    /*static testMethod void createUsersAndTeam(){
        if(!blnUserCreated){
            List<User> lstUser = new List<User>();
            
            ByPassWFUser               = Utils_TestMethods.createStandardUser('sysAdmin');
            ByPassWFUser.BypassWF__c=true;
            lstUser.add(ByPassWFUser);
            
            CaseRelatedCountry              = Utils_TestMethods.createCountry();
            Database.insert(CaseRelatedCountry);
            
            PrimaryAgentManager             = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'prmymgr');
            lstUser.add(PrimaryAgentManager);
            
            AccPreferredAgent               = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'acpag');
            lstUser.add(AccPreferredAgent);
            
            AccPreferredAgentBackup         = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'acpagbk');
            lstUser.add(AccPreferredAgentBackup);
            
            ContactAPreferredAgent          = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'capag');
            lstUser.add(ContactAPreferredAgent);
            
            
            ContactAPreferredAgentBackup    = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'capagbk');
            lstUser.add(ContactAPreferredAgentBackup);
            
            
            ContactBPreferredAgent          = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'cbpag');
            lstUser.add(ContactBPreferredAgent);
            
            
            ContactBPreferredAgentBackup    = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'cbpagbk');
            lstUser.add(ContactBPreferredAgentBackup);
            
            TeamLeaderA  = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'teldA');      
            lstUser.add(TeamLeaderA);
            TeamLeaderB  = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'teldB');      
            lstUser.add(TeamLeaderB);
            
            Database.insert(lstUser);
            PrimaryAgent                    = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'prmyagt');
            PrimaryAgent.ManagerId          = PrimaryAgentManager.Id;
            
            Database.insert(PrimaryAgent);
            System.debug(lstUser);
            
            System.RunAs(ByPassWFUser)
            {
                if(PrimaryCareTeamA==null){
                    List<CustomerCareTeam__c> lstCCTeam = new List<CustomerCareTeam__c>();
                    PrimaryCareTeamA = Utils_TestMethods.CustomerCareTeam(CaseRelatedCountry.Id);
                    PrimaryCareTeamA.Name = 'PrimaryCareTeamA';
                    PrimaryCareTeamA.TeamLeader__c = TeamLeaderA.Id;
                    lstCCTeam.add(PrimaryCareTeamA);
                    PrimaryCareTeamB = Utils_TestMethods.CustomerCareTeam(CaseRelatedCountry.Id);
                    PrimaryCareTeamB.Name = 'PrimaryCareTeamB';
                    PrimaryCareTeamB.TeamLeader__c = TeamLeaderB.Id;
                    lstCCTeam.add(PrimaryCareTeamB);
                    Database.insert(lstCCTeam);     
                    System.Debug('CC SOQL Used: ' + Limits.getAggregateQueries());
                    System.Debug('CC SOQL Remaining: ' + Limits.getLimitAggregateQueries());
                }
            }
            blnUserCreated =true;
        }
        
    }
    
    static testMethod void createAccountContact(){
        if(objTestAccountForCase==null){
            if(!blnUserCreated)
                createUsersAndTeam();
            objTestAccountForCase               = Utils_TestMethods.createAccount();
            objTestAccountForCase.Pagent__c     = AccPreferredAgent.Id;
            objTestAccountForCase.PBAAgent__c   = AccPreferredAgentBackup.Id;
                
            Database.insert(objTestAccountForCase); 
            System.Debug('Account SOQL Used: ' + Limits.getAggregateQueries());
            System.Debug('Account SOQL Remaining: ' + Limits.getLimitAggregateQueries());
            
            List<Contact> lstContact = new List<Contact>();
            CaseContactA = Utils_TestMethods.createContact(objTestAccountForCase.Id,'Test ContactA For Cases');
            CaseContactA.Pagent__c = ContactAPreferredAgent.Id;
            CaseContactA.PBAAgent__c = ContactAPreferredAgentBackup.Id;
            lstContact.add(CaseContactA);
            CaseContactB = Utils_TestMethods.createContact(objTestAccountForCase.Id,'Test ContactB For Cases');
            CaseContactB.Pagent__c = ContactBPreferredAgent.Id;
            CaseContactB.PBAAgent__c = ContactBPreferredAgentBackup.Id;
            lstContact.add(CaseContactB);
            Database.insert(lstContact);     
            System.Debug('Contact SOQL Used: ' + Limits.getAggregateQueries());
            System.Debug('Contact SOQL Remaining: ' + Limits.getLimitAggregateQueries());
            
        }
    }
    
    
    static testMethod void createCase(){
        AP_Case_UpdateCaseSolvingTeam.isTest=true;
        if(testCase==null){
            if(CaseContactA==null)
                createAccountContact();
            testCase = Utils_TestMethods.createCase(objTestAccountForCase.Id, CaseContactA.Id, 'New');
            testCase.Team__c = PrimaryCareTeamA.Id;
            Database.insert(testCase);     
            System.Debug('Case Insert SOQL Used: ' + Limits.getAggregateQueries());
            System.Debug('Case Insert SOQL Remaining: ' + Limits.getLimitAggregateQueries());
        }
    }
    
    static testMethod void updateCase(){
        AP_Case_UpdateCaseSolvingTeam.isTest=true;
        if(testCase==null)
            createCase();
        testCase.Team__c = PrimaryCareTeamB.Id;
        testCase.ContactId = CaseContactB.Id;
        Database.update(testCase);     
        
        System.Debug('Case Update SOQL Used: ' + Limits.getAggregateQueries());
        System.Debug('Case Update SOQL Remaining: ' + Limits.getLimitAggregateQueries());
    }*/
    
    static testMethod void updateCaseOwner(){
        AP_Case_UpdateCaseSolvingTeam.isTest=true;
        if(testCase==null){           
            //createCase();
            List<User> lstUser = new List<User>();
            PrimaryAgentManager             = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'prmymgr');
            lstUser.add(PrimaryAgentManager);
            PrimaryAgent                    = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'prmyagt');
            PrimaryAgent.ManagerId          = PrimaryAgentManager.Id;            
            lstUser.add(PrimaryAgent);
            ContactAPreferredAgent          = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'capag');
            lstUser.add(ContactAPreferredAgent); 
            ContactAPreferredAgentBackup    = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'capagbk');
            lstUser.add(ContactAPreferredAgentBackup);
            AccPreferredAgent               = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'acpag');
            lstUser.add(AccPreferredAgent);
            AccPreferredAgentBackup         = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'acpagbk');
            lstUser.add(AccPreferredAgentBackup);
            ByPassWFUser               = Utils_TestMethods.createStandardUser('sysAdmin');
            ByPassWFUser.BypassWF__c=true;
            lstUser.add(ByPassWFUser);
            TeamLeaderA  = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'teldA');      
            lstUser.add(TeamLeaderA);
            TeamLeaderB  = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'teldB');      
            lstUser.add(TeamLeaderB);
            ContactBPreferredAgent          = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'cbpag');
            lstUser.add(ContactBPreferredAgent);
            ContactBPreferredAgentBackup    = Utils_TestMethods.createStandardUser(UserInfo.getProfileId(), 'cbpagbk');
            lstUser.add(ContactBPreferredAgentBackup);
            Database.insert(lstUser);
            CaseRelatedCountry              = Utils_TestMethods.createCountry();
            Database.insert(CaseRelatedCountry);
            System.RunAs(ByPassWFUser)
            {
                List<CustomerCareTeam__c> lstCCTeam = new List<CustomerCareTeam__c>();
                    PrimaryCareTeamA = Utils_TestMethods.CustomerCareTeam(CaseRelatedCountry.Id);
                    PrimaryCareTeamA.Name = 'PrimaryCareTeamA';
                    PrimaryCareTeamA.TeamLeader__c = TeamLeaderA.Id;
                    lstCCTeam.add(PrimaryCareTeamA);
                    PrimaryCareTeamB = Utils_TestMethods.CustomerCareTeam(CaseRelatedCountry.Id);
                    PrimaryCareTeamB.Name = 'PrimaryCareTeamB';
                    PrimaryCareTeamB.TeamLeader__c = TeamLeaderB.Id;
                    lstCCTeam.add(PrimaryCareTeamB);
                    Database.insert(lstCCTeam);   
            }
            objTestAccountForCase = Utils_TestMethods.createAccount();
            objTestAccountForCase.Pagent__c     = AccPreferredAgent.Id;
            objTestAccountForCase.PBAAgent__c   = AccPreferredAgentBackup.Id;
            Database.insert(objTestAccountForCase);
            List<Contact> lstContact = new List<Contact>();
            CaseContactA = Utils_TestMethods.createContact(objTestAccountForCase.Id,'Test ContactA For Cases');
            CaseContactA.Pagent__c = ContactAPreferredAgent.Id;
            CaseContactA.PBAAgent__c = ContactAPreferredAgentBackup.Id;
            lstContact.add(CaseContactA);
            CaseContactB = Utils_TestMethods.createContact(objTestAccountForCase.Id,'Test ContactB For Cases');
            CaseContactB.Pagent__c = ContactBPreferredAgent.Id;
            CaseContactB.PBAAgent__c = ContactBPreferredAgentBackup.Id;
            lstContact.add(CaseContactB);
            Database.insert(lstContact);
            
            test.startTest();
            
            testCase = Utils_TestMethods.createCase(objTestAccountForCase.Id, CaseContactA.Id, 'New');
            testCase.Team__c = PrimaryCareTeamA.Id;
            testCase.OwnerId = UserInfo.getUserId();
            Database.insert(testCase); 
        }
        //System.debug('---->>>> Test Case Owner : '+ testCase.OwnerId );
                
        System.runAs(ByPassWFUser){
            AP_Case_CaseHandler.blnUpdateTestRequired=true;
            testCase.Team__c = PrimaryCareTeamB.Id;
            testCase.OwnerId = PrimaryAgent.Id;
            testCase.ContactId = CaseContactB.Id;
            Database.update(testCase);     
            //System.debug('---->>>> Test Case Owner After Update : '+ testCase.OwnerId );
        }
        
        test.stopTest();
        
        System.Debug('Case Update Owner SOQL Used: ' + Limits.getAggregateQueries());
        System.Debug('Case Update Owner SOQL Remaining: ' + Limits.getLimitAggregateQueries());
    }
}