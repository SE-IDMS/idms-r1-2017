@IsTest
public with sharing class Batch_PartnerActivityTrackerTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';


    static testMethod void testActivity(){
    	FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
	        Test.startTest();
	        Batch_PartnerActivityTracker batchPartnerActivity = new Batch_PartnerActivityTracker(); 
	        Database.executebatch(batchPartnerActivity,100);
	        Test.stopTest();
	    }
    }

    static testMethod void testActivityStep1(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        	Test.startTest();
	        Batch_PartnerActivityTracker batchPartnerActivity = new Batch_PartnerActivityTracker(1); 
	        Database.executebatch(batchPartnerActivity,100);
	        Test.stopTest();
	    }
    }

    static testMethod void testActivityStep2(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        	Test.startTest();
	        Batch_PartnerActivityTracker batchPartnerActivity = new Batch_PartnerActivityTracker(2); 
	        Database.executebatch(batchPartnerActivity,100);
	        Test.stopTest();
	    }
    }

    static testMethod void testSchedulableActivity(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
	        Test.startTest();
	        String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP,new Batch_PartnerActivityTracker());
	        Test.stopTest();
	    }
    }
}