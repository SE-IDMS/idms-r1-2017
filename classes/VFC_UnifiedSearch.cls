global with sharing class VFC_UnifiedSearch {    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public String jsonLevel{get;set;}{jsonLevel='';} 
    
    public String whatSearched{get;set;}
    
    public VFC_UnifiedSearch(){
//        VFC_UnifiedSearch.remoteSearch('XBTG5230', '', true, false, false, false, false);
    
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
    
        //String whatSearched='searchString='+searchString+'&gmrcode='+gmrcode+'&searchincommercialrefs='+searchincommercialrefs+
        //                            '&exactSearch='+exactSearch+'&excludeGDP='+excludeGDP+'&excludeDocs='+excludeDocs+'&excludeSpares='+excludeSpares;
                                    
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;  //Changed Label.CL00616 to OCTCrp_Certificate; //Have to Revert Back //Temp

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
                
            }
           
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    

    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        return Database.query(query);       
    }
    
    
    public PageReference performSelect(){ 
    
        PageReference pageResult;     
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        system.debug('1. ##### jsonLevel' + jsonLevel);    
        if(jsonLevel=='')    
            jsonLevel=pageParameters.get('jsonLevel');
         system.debug('2. ##### jsonLevel' + jsonLevel);
        WS_GMRSearch.gmrDataBean DBL= (WS_GMRSearch.gmrDataBean) JSON.deserialize(jsonSelectString, WS_GMRSearch.gmrDataBean.class);   
        String gmrCode1=this.gmrCode;
        //GMRCode Calculation:
                    String gmrCode='';
                    if(DBL.businessLine.value != null)
                        gmrCode = DBL.businessLine.value;
                    if(DBL.productLine.value != null)
                        gmrCode += DBL.productLine.value;
                    if(DBL.strategicProductFamily.label != null)
                        gmrCode += DBL.strategicProductFamily.value;
                    if(DBL.family.label != null)
                        gmrCode += DBL.family.value;
                    if(DBL.subFamily.label != null)
                        gmrCode += DBL.subFamily.value;
                    if(DBL.productSuccession.label != null)
                        gmrCode += DBL.productSuccession.value;
                    if(DBL.productGroup.label != null)
                        gmrCode += DBL.productGroup.value;

        return new PageReference('/apex/VFP_DisplayResultUnifiedSearch'+'?GMRCode='+gmrCode+'&Mode='+Label.CLOCT14I2P24+'&commRef='+DBL.commercialReference+'&'+whatSearched);
    }
    
    public PageReference performSelectFamily(){
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        WS04_GMR.gmrDataBean DBL= (WS04_GMR.gmrDataBean) JSON.deserialize(jsonSelectString, WS04_GMR.gmrDataBean.class);   

            //GMR Calculation for Select Family:
            String gmrCode='';
            if(DBL.businessLine.value != null)
                gmrCode = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                gmrCode += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                gmrCode += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                gmrCode += DBL.family.value;

        return new PageReference('/apex/VFP_DisplayResultUnifiedSearch?GMRCode='+gmrCode+'&Mode='+Label.CLOCT14I2P25+'&'+whatSearched);
    }
    
    public PageReference pageCancelFunction(){
        return new PageReference('/');
    }

}