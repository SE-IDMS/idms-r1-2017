public class VFC_PsGroupMemberAssignment {
    
    public List<SelectOption> availablePermissionSets {get;set;}
    public List<SelectOption> selectedPermissionSets {get;set;}
    private Set<String> existingPermissions {get;set;}
    
    public PermissionSetGroup__c psGroup {get;set;}
    
    private void retrievePermissionSet() {
        
        for(PermissionSet ps:[SELECT Id, Label FROM PermissionSet 
                              WHERE IsOwnedByProfile = false]) {
            
            if(!existingPermissions.contains(ps.Id)) {
                availablePermissionSets.add(new SelectOption(ps.Id, ps.Label));
            }            
        }
        
        SelectOptionSorter.doSort(availablePermissionSets, SelectOptionSorter.FieldToSort.Label);  
    }
    
    public VFC_PsGroupMemberAssignment(ApexPages.StandardController psGroupController) {
        
        system.debug('Ps Group Id = ' + psGroupController.getId());
        String psGroupId = ApexPages.currentPage().getParameters().get('Id');
        psGroupMember__c psGroupMember = (psGroupMember__c)psGroupController.getRecord();
        
        if(psGroupMember.Id != null) {
            psGroupMember = [SELECT Id, Name, PermissionSetGroup__c 
                             FROM psGroupMember__c WHERE Id = :psGroupMember.Id LIMIT 1];
        }
        
        psGroup = [SELECT Id, Name FROM PermissionSetGroup__c WHERE Id = :psGroupMember.PermissionSetGroup__c];
        
        availablePermissionSets = new List<SelectOption>();
        
        retrieveExistingMembers(); 
        retrievePermissionSet();               
    }
    
    public void retrieveExistingMembers() {

        selectedPermissionSets = new List<SelectOption>();
        existingPermissions = new Set<String>();
        
        for(PsGroupMember__c psMember:[SELECT Id, PermissionSetId__c, PermissionSetGroup__c, PermissionSetGroup__r.Name, Name FROM PsGroupMember__c 
                                       WHERE PermissionSetGroup__c = :psGroup.Id]) {
            
            selectedPermissionSets.add(new SelectOption(psMember.PermissionSetId__c, 
                                                         psMember.Name));
                                                         
            existingPermissions.add(psMember.PermissionSetId__c);
            system.debug('psGroup.Id = ' + psGroup.Id);
        }    
        
        system.debug('');
        
        SelectOptionSorter.doSort(selectedPermissionSets, SelectOptionSorter.FieldToSort.Label);      
    }
    
    public PageReference backToPsGroup() {
        return new ApexPages.StandardController(psGroup).view();
    }
    
    public PageReference savePsGroupMember() {
    
        delete [SELECT Id, PermissionSetId__c, PermissionSetGroup__r.Name, Name FROM PsGroupMember__c 
                                                      WHERE PermissionSetGroup__c = :psGroup.Id];
        
        List<PsGroupMember__c> newPsGroupMember = new List<PsGroupMember__c>();
        
        for(SelectOption selectedPs:selectedPermissionSets) {
            newPsGroupMember.add(new PsGroupMember__c(Name = selectedPs.getLabel(), 
                                                      PermissionSetId__c = selectedPs.getValue(),
                                                      PermissionSetGroup__c = psGroup.Id));
        }
        
        insert newPsGroupMember;
        
        return new ApexPages.StandardController(psGroup).view();
    }
}