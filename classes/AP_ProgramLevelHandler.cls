public class AP_ProgramLevelHandler{
    public static void checkHierarchyLevelCount(List<ProgramLevel__c> lstProgramLevels, Map<Id,ProgramLevel__c> exslstProgramLevels) {
        Set<String> prgNames = new Set<String>();
        Set<String> lvlIds = new Set<String>();
        Map<String, ProgramLevel__c> mapPrgmLevel = new Map<String, ProgramLevel__c>();

        System.debug('New levels:'+lstProgramLevels);
        for(ProgramLevel__c pLevel : lstProgramLevels) {
            lvlIds.add(pLevel.Name);
            if((exslstProgramLevels == null && pLevel.Hierarchy__c == '1' ) || 
                (exslstProgramLevels != null && exslstProgramLevels.get(pLevel.Id).Hierarchy__c != pLevel.Hierarchy__c && pLevel.Hierarchy__c == '1')) {
                prgNames.add(pLevel.PartnerProgram__c);
                mapPrgmLevel.put(pLevel.PartnerProgram__c, pLevel);
            }
        }

        System.debug('New level:'+lvlIds);
        System.debug('Program Name:'+prgNames);
        System.debug('Map'+mapPrgmLevel);

        List<ProgramLevel__c> programLevel = new List<ProgramLevel__c>([
                SELECT Id,Name, PartnerProgram__r.Name, PartnerProgram__c, Hierarchy__c
                FROM ProgramLevel__c
                WHERE PartnerProgram__c = :prgNames AND Hierarchy__c = '1']);

        System.debug('exst level:'+programLevel);

        for(ProgramLevel__c s : programLevel) {
            System.debug('Inside FOR:'+s);
            System.debug('Inside FOR 1:'+mapPrgmLevel.containsKey(s.PartnerProgram__c));
            if(mapPrgmLevel.containskey(s.PartnerProgram__c)){
               System.debug('Inside IF:'+mapPrgmLevel.containsKey(s.PartnerProgram__c));
               mapPrgmLevel.get(s.PartnerProgram__c).addError(Label.CLOCT14PRM76);
            }
        }
    }
}