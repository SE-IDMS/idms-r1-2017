public with sharing class VFC_PRM_PropertiesRetroCalculation {

    public static final List<String> SEPARATOR_LIST  = new List<String>{',','\r\n'};
    
    public String migrationIdList {get;set;}
    public String internalPropertiesList {get;set;}
    public String objectName {get;set;}
    public Integer batchSize {get;set;}
    public Id batchJobId {get;set;}
    public Boolean migrationEnqueued {get{
            List<AsyncApexJob> apexJobList = [select id,ApexClass.Name, Status, ExtendedStatus from AsyncApexJob where ApexClass.Name ='Batch_PRMMigrationFromV1toV2' and status in ('Queued','Preparing','Processing')];    
            return apexJobList==null || apexJobList.size()>0;
    }}
    public Boolean internalPropertiesEnqueued {get{
            List<AsyncApexJob> apexJobList = [select id,ApexClass.Name, Status, ExtendedStatus from AsyncApexJob where ApexClass.Name ='Batch_PRMBadgeBFOProperties' and status in ('Queued','Preparing','Processing')];    
            return apexJobList==null || apexJobList.size()>0;
    }}

    public VFC_PRM_PropertiesRetroCalculation(){
        batchSize=200;
    }
   
    public void retroCalculateMigration() {
        
        List<Id> selectedId = fromStringToListId(migrationIdList);
        System.debug('migrationIdList: '+migrationIdList+'migrationEnqueued: '+migrationEnqueued+' selectedId: '+selectedId);
        if(selectedId!=null && selectedId.size()>0){
            if(!migrationEnqueued){
                Batch_PRMMigrationFromV1toV2 prmMigrationBatch = new Batch_PRMMigrationFromV1toV2(selectedId);
                batchJobId = Database.executebatch(prmMigrationBatch,batchSize);
                System.debug('batchJobId:'+batchJobId);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Job with id '+batchJobId+' is enqueued');
                ApexPages.addMessage(myMsg);
            }
        }
    }

    public void retroCalculateInternalProperties() {
        List<Id> selectedId = fromStringToListId(internalPropertiesList);
        System.debug('internalPropertiesList: '+internalPropertiesList+'internalPropertiesEnqueued: '+internalPropertiesEnqueued+' selectedId: '+selectedId);
        if(selectedId!=null && selectedId.size()>0){
            if(!internalPropertiesEnqueued){
                Batch_PRMBadgeBFOProperties prmMigrationBatch = new Batch_PRMBadgeBFOProperties(objectName,selectedId);
                batchJobId = Database.executebatch(prmMigrationBatch,batchSize);
                System.debug('batchJobId:'+batchJobId +' objectName: '+objectName);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Job with id '+batchJobId+' is enqueued');
                ApexPages.addMessage(myMsg);
            }
        }
    }
    public void refresh() {
    }
    public static List<Id> fromStringToListId(String input){
        List<Id> returnList = new List<Id>();
        if(input!=null && input!=''){
            for(String sep:SEPARATOR_LIST){
                if(input.contains(sep)){
                    for(String elt: input.split(sep)){
                        if(elt!=null && elt!=''){
                            returnList.add((Id)elt);
                        }
                    }
                }
                break;
            }
            if(returnList.size()<1){
               returnList.add((Id)input); 
            }

        }   
        return returnList;
    }
}