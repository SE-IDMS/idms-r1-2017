public class SVMXC_TimeEntryBefore {

    //NZE: 2016-06-08 Singleton to be used in order to retrieve the FSR records information, including the salesforce.com user and related Time Zone
    private static Map <Id, SVMXC__Service_Group_Members__c> techMap = null;
    
    private static Set<Id> techIds = new Set<Id>();

    public static void ensureTechIsOwnerAndSetDayOfWeek (List<SVMXC_Time_Entry__c> triggerNew, Map<Id, SVMXC_Time_Entry__c> triggerOld, Boolean isInsert, Boolean isUpdate) {
        /*
            Method to ensure that the tech is the owner of the record  (Written May 16 2014 / Updated Mar 13 2015)
            
            Updated Nov 12, 2015 - Added support to stamp Day of Week Tech field for Time Entry Start Date, adjusted to
                                   Technicians timezone.
        */
         
        // AMO : 02 august 2016 : DEF-10580 : null pointer exception when building the timezone variable
        techMap = null;
        
        if (isInsert) {
            for (SVMXC_Time_Entry__c te0: triggerNew) {
                if (te0.Technician__c != null && !techIds.contains(te0.Technician__c))
                    techIds.add(te0.Technician__c);
            }
        } else if (isUpdate) {
            for (SVMXC_Time_Entry__c te1: triggerNew) {
                // check for Tech change
                if (te1.Technician__c != null && te1.Technician__c != triggerOld.get(te1.Id).Technician__c && !techIds.contains(te1.Technician__c))
                    techIds.add(te1.Technician__c);
                // check for Start Date Time change
                if (te1.Technician__c != null && te1.Start_Date_Time__c != triggerOld.get(te1.Id).Start_Date_Time__c && !techIds.contains(te1.Technician__c))
                    techIds.add(te1.Technician__c);
            }
            
        }
     
        if (!techIds.isEmpty()) {
                        
            if (getTechMap().size() > 0) {
                
                for (SVMXC_Time_Entry__c te2: triggerNew) { 
                
                    if (te2.Technician__c != null && getTechMap().containsKey(te2.Technician__c) 
                            && te2.OwnerId != getTechMap().get(te2.Technician__c).SVMXC__Salesforce_User__c)
                        te2.OwnerId = getTechMap().get(te2.Technician__c).SVMXC__Salesforce_User__c;
                    
                    if(getTechMap().containsKey(te2.Technician__c)) {
	                    // set day of the Week field based on Technicians timezone
	                    String timezone = String.valueOf(getTechMap().get(te2.Technician__c).SVMXC__Salesforce_User__r.TimeZoneSidKey);
	                    DateTime workingDate = te2.Start_Date_Time__c;
	                    System.debug('##### timezone ' + timezone);
	                    System.debug('##### workingDate ' + workingDate);
	                    String dayofWeek = workingDate.format('EEEE', timezone);
	                    te2.Day_of_the_Week_FSR__c = dayofWeek;
	                    te2.StartDateTimeAtFSRTimeZone__c = workingDate.format('yyyy-MM-dd HH:mm:ss', timezone);
                    }
                }
            }
        }  
    }
    
    private static Map <Id, SVMXC__Service_Group_Members__c> getTechMap() {
        if (techMap == null) {
            // Variable never instanciated, call DB and store result for subsequent calls
            System.debug('##### TechMap BEFORE first instanciation. TechMap: ' + techMap);
            techMap = new Map<Id, SVMXC__Service_Group_Members__c> 
                        ([SELECT Id, SVMXC__Salesforce_User__c, SVMXC__Salesforce_User__r.TimeZoneSidKey 
                        FROM SVMXC__Service_Group_Members__c 
                        WHERE Id IN: techIds AND SVMXC__Salesforce_User__c != null]);
            
            System.debug('##### TechMap AFTER first instanciation. TechMap: ' + techMap);
        }
        return techMap;
    }
}