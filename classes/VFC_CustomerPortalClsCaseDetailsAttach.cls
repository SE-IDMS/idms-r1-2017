public without sharing class VFC_CustomerPortalClsCaseDetailsAttach {
    Id CaseId;
    Id CaseCommentId;
    Case caseDetail;
    List<CaseComment> caseComments = null;
    CaseComment caseCommentDetail;
    List<Attachment> attachments;
    public Attachment attach {get;set;}
    public String AttDescription{get; set;}

    public Id getCaseId(){return CaseId;}
    public void setCaseId(Id idd){
        this.CaseId = idd;
    }
    
    public boolean getIsAuthenticated ()
{
  return (IsAuthenticated());
}

public boolean IsAuthenticated ()
{
  return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
}

    public Attachment getAttach(){return attach;}
    public void setAttach(Attachment a){this.attach = a;}
    public void setCaseDetail(Case case2){this.caseDetail = case2;}
    public List<CaseComment> getCaseComments(){return this.CaseComments;}    
        
//===========INSERT AN ATTACHMENT==================
public Pagereference saveAttachment()
    {
if (isAuthenticated())
{        
        //This shows how to insert an Attachment
        attach.ParentId = CaseId;
        
        /* To make case Description mandatory
        
        if(attach.Description.equalsIgnoreCase('')){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value for File description' );
            ApexPages.addMessage(msg); return null;}
        */
            
        if (    (  !(attach.name != null)  ) || (  !(attach.body != null) )     ){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a file to upload' );
            ApexPages.addMessage(msg); return null;}
        insert attach;
        
    pagereference pageref = new PageReference('/apex/VFP_CustomerPortalClsCaseDetails?Id='+CaseId);
    pageref.setRedirect(true);
    return pageref;   
}
else
{
  return null;
}
}



//==========The constructor=========================    
    public VFC_CustomerPortalClsCaseDetailsAttach(){
    CaseId = ApexPages.currentPage().getParameters().get('Id');
     attach = new Attachment();
     
    if (CaseId != null){
        caseDetail = [SELECT CaseNumber, Subject, Status, SupportCategory__c, CustomerRequest__c, IsClosed, ActionNeededFrom__c FROM Case WHERE  Id =: CaseId];   
        caseComments = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
        attachments = [SELECT Id, CreatedById, Name, CreatedDate, Description, IsPrivate FROM Attachment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
        }
       }   


// Return the parameter to the page
    public Case getCaseDetail() 
    {    
      if (caseDetail != null) 
      {
          if      (caseDetail.status.equalsIgnoreCase('New'))       {caseDetail.status = 'New';}
          else if (caseDetail.status.equalsIgnoreCase('Cancelled')) {caseDetail.status = 'Closed';}
          else if (caseDetail.status.equalsIgnoreCase('Closed'))    {caseDetail.status = 'Closed';}
          else if (caseDetail.ActionNeededFrom__c != null && caseDetail.ActionNeededFrom__c.equalsIgnoreCase('Customer')) {caseDetail.status = 'Waiting for Customer';} 
          else caseDetail.status = 'In Progress';     
      } 
      
      return caseDetail;   
    }

/*=======Return the comments List. ===================*/
  /*  public List<CaseComment> getCaseComments() {    
         List<CaseComment> lcomm = new List<CaseComment>();
        
        if (CaseComments != null){
            for (CaseComment lcc : CaseComments){
                    if (lcc.IsPublished) lcomm.add(lcc);
            }
            return lcomm;
        }
        else return CaseComments; 
 
    } */       
              
    public pageReference getCaseDetailView()
    {
      if (isAuthenticated())
      {
      	getCaseDetail(); 
      }
      
      return null;
    
    }   
    
/*======= Return the Attachments List ===================*/
    public List<Attachment> getAttachments() {    
        List<Attachment> lattach = new List<Attachment>();
        
        if (Attachments != null){
            for (Attachment att : Attachments){
                    if (!(att.IsPrivate)) lattach.add(att);
            }
            return lattach;
        }
        else return Attachments;   
    }        
     
}