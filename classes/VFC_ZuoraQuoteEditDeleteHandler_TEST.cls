@isTest
private class VFC_ZuoraQuoteEditDeleteHandler_TEST{
    public static testMethod void myUnitTest(){
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            usr.BypassWF__c = false;
            usr.ByPassVR__c = true;
            usr.ProgramAdministrator__c = true;
            
            System.runas(usr){
                
                //Code before June 2016 Release 
                //Country__c country = Utils_TestMethods.createCountry();
                //country.countrycode__c ='ASM';
                //insert country;
                //End Code before June 2016 Release
                
                //New Code with June 2016 Release
                Country__c country = null;
                
                List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
                if(countries.size()==1) {
                   country = countries[0];
                }
                else {
                   country = new Country__c(Name='France', CountryCode__c='FR');
                   insert country;
                }
                //End Code with June 2016 Release

                Account ac= Utils_TestMethods.createAccount();
                ac.Country__c=country.id;
                insert ac;
                
                zqu__Quote__c newquote = new zqu__Quote__c();
                newquote.Name = 'Test';
                newquote.zqu__Account__c = ac.Id;
                newquote.zqu__Status__c = 'New';
                newquote.zqu__SubscriptionType__c = 'Cancel Subscription';
                insert newquote;
                
                zqu__Quote__c newquote1 = new zqu__Quote__c();
                newquote1.Name = 'Test';
                newquote1.zqu__Account__c = ac.Id;
                newquote1.zqu__Status__c = 'Sent to Z-Billing';
              //  newquote1.zqu__SubscriptionType__c = 'Cancel Subscription';
                insert newquote1;
                
                PageReference pageRef = Page.VFP_ZuoraQuoteDeleteHandler;
                pageRef.getparameters().put('retURL',ac.id);
                Test.setCurrentPage(pageRef);  
                
                ApexPages.currentPage().getparameters().put('Id',newquote.id);
                ApexPages.StandardController stanPage = new ApexPages.StandardController(newquote); 
                VFC_ZuoraQuoteEditDeleteHandler controller = new VFC_ZuoraQuoteEditDeleteHandler(stanPage);  
            
                controller.checkEditStatus(); 
                controller.checkDeleteStatus();
                controller.back();
                
                
                pageRef = Page.VFP_ZuoraQuoteEditHandler;
                pageRef.getparameters().put('retURL',ac.id);
                Test.setCurrentPage(pageRef);
                
                ApexPages.currentPage().getparameters().put('Id',newquote1.id);
                ApexPages.StandardController stanPage1 = new ApexPages.StandardController(newquote1); 
                VFC_ZuoraQuoteEditDeleteHandler controller1 = new VFC_ZuoraQuoteEditDeleteHandler(stanPage1);
                
                controller1.checkEditStatus(); 
                controller1.checkDeleteStatus();
                controller1.back();
                
                
            }
        }
    }
}