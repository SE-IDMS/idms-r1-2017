@isTest
global class AP_BasicHttpBinding_IOPSImportMock_TEST implements WebServiceMock {
    public string respType;
    public AP_BasicHttpBinding_IOPSImportMock_TEST(String requestType){
        this.respType = requestType;
    } 
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       /*tempuriOrg.getOPSWithdrawalImportResultResponse_element respElement = 
           new tempuriOrg.getOPSWithdrawalImportResultResponse_element();
       respElement.getOPSWithdrawalImportResultResult = new schemasDatacontractOrg200407Busines.JobWS();       
       respElement.getOPSWithdrawalImportResultResult.jobStatus = 'Success';
       schemasMicrosoftCom200310Serializat1.Arrayofstring abc = new schemasMicrosoftCom200310Serializat1.ArrayOfstring();
        abc.string_x = new String[]{'Test','true','Test'};
       respElement.getOPSWithdrawalImportResultResult.detailsList = abc;
       Map<String, tempuriOrg.getOPSWithdrawalImportResultResponse_element> response_map_x = new Map<String, tempuriOrg.getOPSWithdrawalImportResultResponse_element>();
       response_map_x.put('response_x', respElement);        
       //response.put('response_x', respElement);
       system.debug('1111111111111111'+response_map_x); 
       response = new Map<String, Object>();
       response = response_map_x;*/
       if(respType == 'Withdrawal'){
             tempuriOrg.sendWithdrawalObjectResponse_element respElement = new tempuriOrg.sendWithdrawalObjectResponse_element();
           respElement.sendWithdrawalObjectResult = 123;
           response.put('response_x', respElement);
       }else if(respType == 'Substitution'){
             tempuriOrg.sendSubstitutionObjectResponse_element respElement = new tempuriOrg.sendSubstitutionObjectResponse_element();
           respElement.sendSubstitutionObjectResult = 123;
           response.put('response_x', respElement);
       }else if(respType == 'Status'){
           tempuriOrg.getOPSWithdrawalImportResultResponse_element respElement = 
           new tempuriOrg.getOPSWithdrawalImportResultResponse_element();
           respElement.getOPSWithdrawalImportResultResult = new schemasDatacontractOrg200407Busines.JobWS();       
           respElement.getOPSWithdrawalImportResultResult.jobStatus = 'Success';
           schemasMicrosoftCom200310Serializat1.Arrayofstring abc = new schemasMicrosoftCom200310Serializat1.ArrayOfstring();
           abc.string_x = new String[]{'Test','true','Test'};
           respElement.getOPSWithdrawalImportResultResult.detailsList = abc;
           Map<String, tempuriOrg.getOPSWithdrawalImportResultResponse_element> response_map_x = new Map<String, tempuriOrg.getOPSWithdrawalImportResultResponse_element>();
           response_map_x.put('response_x', respElement); 
       }
       
   }
}