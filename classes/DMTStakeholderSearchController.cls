/**
 * Apex Controller for Visualforce Page DMTStakeholderSearch
 * ~ Based on VFC02_AccountSearch created by Adrian MODOLEA / Mohamed EL MOUSSAOUI
 * ~ Modified for DMT by Sreedevi SURENDRAN
 * ~ DMT TO bFO MIGRATION - OCTOBER 7, 2011
**/
public with sharing class DMTStakeholderSearchController
{
    /*============================================================================
        V.A.R.I.A.B.L.E.S
    =============================================================================*/     
    // current DMT Stakeholder used for the input search
    //public Account account{ get; private set; }
    public DMTStakeholder__c stakeholder {get;private set;}
    
    // current page parameters
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
    
    // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL00016);
    
    // collection of DMT Stakeholders matching the search criteria
    public List<DMTStakeholder__c> stakeholders {get; private set;}

    // indicates whether there are more records than the searchMaxSize.
    public Boolean hasNext {
        get {
            return (stakeholders == null || (stakeholders != null && stakeholders.size() > searchMaxSize));
        }
        set;
    }          
    
    /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/    
    public DMTStakeholderSearchController(ApexPages.StandardController controller) {
        stakeholder = (DMTStakeholder__c)controller.getRecord();
    }

    /*============================================================================
        M.E.T.H.O.D.S
    =============================================================================*/         
    // returns the search string to be used in SOSL query
    public String getSearchString() {
        String strSearch = 'FIND {*'+Utils_Methods.escapeForSOSL(stakeholder.LastName__c)+'*}';
        strSearch += ' RETURNING DMTStakeholder__c(Id,FirstName__c,LastName__c,Email__c,ProjectRoles__c';
        List<String> whereConditions = new List<String>();
        //if(stakeholder.LastName__c!=null && stakeholder.LastName__c!='')
        //    whereConditions.add('LastName__c like \''+Utils_Methods.escapeForWhere(stakeholder.LastName__c)+'\'');
        
        if(whereConditions.size() > 0) {
            strSearch += ' WHERE ';
            for(String condition : whereConditions) {
                strSearch += condition + ' AND ';   
            }
            // delete last 'AND'
            strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
        }
        strSearch += ' ORDER BY LastName__c';
        strSearch += ' LIMIT ' + (Integer)(searchMaxSize+1);
        strSearch += ')';
        System.Debug(strSearch);
        return strSearch;   
    }
    
    public String getQueryString() {
        String strSearch = 'SELECT Id,FirstName__c,LastName__c,Email__c,ProjectRoles__c FROM DMTStakeholder__c';
        List<String> whereConditions = new List<String>();
        whereConditions.add('LastName__c LIKE \'%'+Utils_Methods.escapeForWhere(stakeholder.LastName__c)+'%\'');
        if(whereConditions.size() > 0) {
            strSearch += ' WHERE ';
            for(String condition : whereConditions) {
                strSearch += condition + ' AND ';   
            }
            // delete last 'AND'
            strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
        }
        strSearch += ' ORDER BY Name';
        strSearch += ' LIMIT ' + (Integer)(searchMaxSize+1);
        System.Debug(strSearch);
        return strSearch;   
    }
            
    // execute the SOSL query
    public PageReference doSearchDMTStakeholder(){
        Boolean soqlDebugMode = ApexPages.currentPage().getParameters().containsKey('soql');
        // check for the account name length
        if(stakeholder.LastName__c == null || stakeholder.LastName__c.trim().length() < 3)
        {
            stakeholder.LastName__c.addError(System.Label.CL00028);
            return null;
        }
        String s = null;
        if(soqlDebugMode) {
            s = getQueryString();
            Utils_SDF_Methodology.log('START query: ', s);          
        } else {
            s = getSearchString();
            Utils_SDF_Methodology.log('START search: ', s);
        }
        Utils_SDF_Methodology.startTimer();
        try {
            if(soqlDebugMode)
                stakeholders = Database.query(s);
            else
                stakeholders = (List<DMTStakeholder__c>)search.query(s).get(0);
            if(Test.isRunningTest())
                throw new TestException();
        }catch(Exception ex){
            ApexPages.addMessages(ex);
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return null;
    }

    // returns the standard DMT Stakeholder new page with pre-filled information from the input search         
    public PageReference continueCreation(){
        PageReference newDMTStakeholderPage = new PageReference('/a33/e');
        
        /*
        // add parameters from the input search form
        newDMTStakeholderPage.getParameters().put('stk2', stakeholder.LastName__c);
    if(stakeholder.FirstName__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00422, account.AccountLocalName__c);
        if(account.Street__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00036, account.Street__c);    
        if(account.ZipCode__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00038, account.ZipCode__c);   
        if(account.City__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00039, account.City__c);      
        if(account.StateProvince__c!=null) {
            newAccPage.getParameters().put(System.Label.CL00040, [SELECT Name FROM StateProvince__c WHERE Id=:account.StateProvince__c].Name);
            newAccPage.getParameters().put(System.Label.CL00041, account.StateProvince__c);             
        } 
        if(account.Country__c!=null) {
            newAccPage.getParameters().put(System.Label.CL00042, [SELECT Name FROM Country__c WHERE Id=:account.Country__c].Name); 
            newAccPage.getParameters().put(System.Label.CL00043, account.Country__c);           
        }               
        if(account.POBox__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00044, account.POBox__c); 
        if(account.POBoxZip__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00045, account.POBoxZip__c); 
        
        // add parameters from the initial context (except <save_new>)
        for(String param : pageParameters.keySet())
            newAccPage.getParameters().put(param, pageParameters.get(param));
        
        if(newDMTStakeholderPage .getParameters().containsKey('save_new'))
            newDMTStakeholderPage .getParameters().remove('save_new');      
        */
        // do not longer override the creation action   
        newDMTStakeholderPage.getParameters().put('nooverride', '1');
        
        newDMTStakeholderPage.setRedirect(true);
        return newDMTStakeholderPage;
    }  
    
    public class TestException extends Exception {}               
}