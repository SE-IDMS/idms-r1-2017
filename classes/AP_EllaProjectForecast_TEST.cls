@istest
public class AP_EllaProjectForecast_TEST{
    
   /* public static list<User> createUsers(){
        list<User> userList = new list<user>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO'];
        User u1 = new User(
            Alias = 'newUser1', 
            Email='newuser1@testorg.com',
            EmailEncodingKey='UTF-8', 
            LastName='Testing1', 
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', 
            ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', 
            userroleid = r.Id,
            UserName='newuser1@testorg.com'
        );
            
        User u2 = new User(
            Alias = 'newUser2', 
            Email='newuser2@testorg.com',
            EmailEncodingKey='UTF-8', 
            LastName='Testing2', 
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', 
            ProfileId = p.Id,
            userroleid = r.Id,
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='newuser2@testorg.com'
        );
        RecordType rt = [select id, name from RecordType where name = 'Offer Launch - Product' and sObjectType='Offer_Lifecycle__c'];
        userList.add(u1);
        userList.add(u2);
        insert userList;
        return userList;
    }*/
    public static Offer_Lifecycle__c createOffer(Id userId){
        Offer_Lifecycle__c offer = new Offer_Lifecycle__c(
            Name = 'TestOffer', 
            Offer_Name__c = 'Offet Test',
            Leading_Business_BU__c = 'Cloud Services',
            Description__c = 'Offer Description',
            Sales_Date__c = system.today(),
            Process_Type__c = 'PMP',
            Launch_Owner__c = userId,
            Forecast_Unit__c = 'Kilo Euro',
            Marketing_Director__c = userId,
            Offer_Status__c = 'Red',
            Launch_Master_Assets_Status__c = 'Green',
            Offer_Classification__c = 'Major'
            
        );
        //insert offer;
        return offer;
    }
    
    public static Milestone1_Project__c createProject(Id OfferId, Id cId, Id userId){
        Milestone1_Project__c pjct = new Milestone1_Project__c(
            name='Test Project1',
            offer_launch__c = OfferId,
            country__c=cId,
            Country_Launch_Leader__c=userId,
            Country_Announcement_Date__c=system.today(),
            region__c='APAC'
        );
        //insert pjct;
        return pjct;
    }
    private static String monthName(Integer monthNum) {
        return(MONTHS_OF_YEAR[monthNum - 1]);
    }
    private static final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December','Total by Year'};
    private static final List<String> MONTHS_OF_QUARTAR = new String[]{'Q1','Q1','Q1','Q2','Q2','Q2','Q3','Q3','Q3','Q4','Q4','Q4'};
    private String quarterName(Integer monthNum) {
        return(MONTHS_OF_QUARTAR[monthNum - 1]);
    }
    public static list<OfferLifeCycleForecast__c> createForecastRecords( Id ProjId){
        Map<String,OfferLifeCycleForecast__c> mapforecast = new Map<String,OfferLifeCycleForecast__c> ();            
        List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
        OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
        for(integer i=1; i<MONTHS_OF_YEAR.size(); i++ ) {
            offForecast = new OfferLifeCycleForecast__c();
            offForecast.project__c=ProjId;
            offForecast.month__c=monthName(i);
            offForecast.ForcastYear__c='2014';
            offForecast.Actual__c =1000;
            offForecast.BU__c =2000;
            offForecast.Country2__c='France';
            offForecast.Country__c=4000;
            if(i <=3) {
            offForecast.ForecastQuarter__c='Q1';
            }else if( i > 3 && i <=6) {
                offForecast.ForecastQuarter__c='Q2';
            }else if( i > 6 && i <=9) {
                offForecast.ForecastQuarter__c='Q3';
            }
            else if( i > 9 && i <=12) {
                offForecast.ForecastQuarter__c='Q4';
            }
            system.debug('Month Value:'+offForecast);            
            ListoffForecast.add(offForecast);
        }
        OfferLifeCycleForecast__c  offForecast1 = new OfferLifeCycleForecast__c();
        for(integer i=1; i<MONTHS_OF_QUARTAR.size(); i++ ) {
            offForecast1 = new OfferLifeCycleForecast__c();
            offForecast1.project__c=ProjId;
            //offForecast1.month__c='Total by Year';
            offForecast1.ForcastYear__c='2014';
            offForecast1.Actual__c =0;
            offForecast1.BU__c =0;
           // offForecast1.Country2__c='Italy';
            offForecast1.Country__c=0;
            if(i <=3) {
            offForecast1.ForecastQuarter__c='Q1';
            }else if( i > 3 && i <=6) {
                offForecast1.ForecastQuarter__c='Q2';
            }else if( i > 6 && i <=9) {
                offForecast1.ForecastQuarter__c='Q3';
            }
            else if( i > 9 && i <=12) {
                offForecast1.ForecastQuarter__c='Q4';
            }
            system.debug('Quater Value:'+offForecast1);
            ListoffForecast.add(offForecast1);
        }   
        for(integer i=1; i<2; i++ ) {
            offForecast1 = new OfferLifeCycleForecast__c();
            offForecast1.project__c=ProjId;
            offForecast1.month__c='Total by Year';
            offForecast1.ForcastYear__c='2014';
            offForecast1.Actual__c =0;
            offForecast1.BU__c =0;
           // offForecast1.Country2__c='Italy';
            offForecast1.Country__c=0;
            if(i <=3) {
            offForecast1.ForecastQuarter__c='Q1';
            }else if( i > 3 && i <=6) {
                offForecast1.ForecastQuarter__c='Q2';
            }else if( i > 6 && i <=9) {
                offForecast1.ForecastQuarter__c='Q3';
            }
            else if( i > 9 && i <=12) {
                offForecast1.ForecastQuarter__c='Q4';
            }
            system.debug('Quater Value:'+offForecast1);
            ListoffForecast.add(offForecast1);
        }    
            
        insert  ListoffForecast;
        system.debug('Forecast Values:'+ListoffForecast);
        return ListoffForecast;
    }
    
    static testmethod void offerlaunch(){
    
        Test.startTest();
        //list<User> userList = createUsers();
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA12');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);   
        //create Country
        
            list<country__c> countryList = new list<country__c>();
            country__c c = new country__c(name = 'India',countrycode__c = 'IN');
            country__c c1 = new country__c(name = 'France',countrycode__c = 'FR');
            countryList.add(c);
            countryList.add(c1);
            insert countryList;
        
            Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
            insert offerRec;
            Milestone1_Project__c projRec = createProject(offerRec.Id, countryList[0].Id, newUser.Id);
            projRec.Country_End_of_Commercialization_Date__c = System.today();
            insert projRec;
            list<OfferLifeCycleForecast__c> forecastList = createForecastRecords(projRec.Id);
            projRec.Country_Announcement_Date__c = system.today() + 60;
            projRec.Country_End_of_Commercialization_Date__c = system.today() - 10;
            update projRec;
            Test.stopTest();
        
    }
    static testmethod void changingCountry(){
    
        Test.startTest();
            //list<User> userList = createUsers();
            User newUser = Utils_TestMethods.createStandardUser('yuwejUserELLA12');         
            Database.SaveResult UserInsertResult = Database.insert(newUser, false);   
            
        
        //create Country
     
            list<country__c> countryList = new list<country__c>();
            country__c c = new country__c(name = 'India',countrycode__c = 'IN');
            country__c c1 = new country__c(name = 'France',countrycode__c = 'FR');
            countryList.add(c);
            countryList.add(c1);
            insert countryList;
        
            Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
            insert offerRec;
            Milestone1_Project__c projRec = createProject(offerRec.Id, countryList[0].Id, newUser.Id);
            projRec.Country_End_of_Commercialization_Date__c = System.today();
            insert projRec;
            list<OfferLifeCycleForecast__c> forecastList = createForecastRecords(projRec.Id);
            projRec.Country_Announcement_Date__c = system.today() + 60;
            projRec.Country_End_of_Commercialization_Date__c = system.today() - 10;
            projRec.Country__c = countryList[1].Id;
            update projRec;
            Test.stopTest();
       
    }
}