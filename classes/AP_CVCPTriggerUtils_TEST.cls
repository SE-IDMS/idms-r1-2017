/**
12-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for ContractAfterInsert,   ContractAfterUpdate  and AP_ContractTriggerUtils
 */
@isTest
private class AP_CVCPTriggerUtils_TEST {

    static testMethod void myUnitTest() {
        
        Account testAccount = new Account();
        testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        insert testContact;
        
        Contact testContact1 = new Contact();
        testContact1 = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        insert testContact1;
        
        Contract testContract = new Contract();
        testContract = Utils_TestMethods.createContract(testAccount.Id,null);
        insert testContract;
        
        CTR_ValueChainPlayers__c CVCP1 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        CVCP1.Contact__c = testContact.ID;
        CVCP1.ContactRole__c = System.Label.CLSEP12CCC23;
 
        try{
            insert CVCP1;
        }
        catch(Exception e){
            System.assert( e.getMessage().contains('Duplicate Admin Found'), e.getMessage() );
        }
        CVCP1.Contact__c = testContact1.Id;
        try{
            update CVCP1;
        }
        catch(Exception e){
            System.assert( e.getMessage().contains('Duplicate Admin Found'), e.getMessage() );
        }
        
    }    
}