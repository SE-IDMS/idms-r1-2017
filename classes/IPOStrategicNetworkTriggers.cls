/*
    Author           : Gayathri Shivakumar (Schneider Electric)
    Date Created     :  13-Dec-2012
    Description      : Class for IPO Strategic Deployment Network Triggers, Validate Deployment Network
*/

public with sharing class IPOStrategicNetworkTriggers
{


Public Static void IPOStrategicNetworkBeforeInsert(List<IPO_Strategic_Deployment_Network__c> TM)
{

System.Debug('****** IPOStrategicNetworkBeforeInsert Start ****'); 

// Lists

list<IPO_Strategic_Deployment_Network__c> Team = [select Scope__c, Entity__c, IPO_Strategic_Program__c,IPO_Strategic_Program_Name__c from IPO_Strategic_Deployment_Network__c] ;
list <IPO_Strategic_Program__c> Program = [Select Name, Global_Functions__c, Global_Business__c, Operation__c from IPO_Strategic_Program__c ];
List<String> GlobalFunctions = new List<string>();
List<String> GlobalBusiness = new List<string>();
List<String> PowerRegion = new List<string>();
List<String>  GSC_Region = new List<string>();
List<String> Partner_Region = new List<string>();

// Declarations 

String GF_Team;
Integer I;
Boolean GF_Network= false;
Boolean GF_No_Network= false;

String GB_Team;
Boolean GB_Network = false;
Boolean GB_No_Network = false;


String PR_Team;
Boolean PR_Network = false;
Boolean PR_No_Network = false;

String GSC_Team;
Boolean GSC_Network = false;
Boolean GSC_No_Network = false;

String Partner_Team;
Boolean Partner_Network = false;
Boolean Partner_No_Network = false;

GF_Team = '';
GB_Team = '';
PR_Team = '';
GSC_Team = '';
Partner_Team = '';


For(IPO_Strategic_Deployment_Network__c T : TM){



  For(IPO_Strategic_Deployment_Network__c TMs : Team){
  
  // Concatenate the Scope of Selected Team Members for Global Functions
  
    if(TMs.Scope__c == System.Label.Connect_Global_Functions && TMs.IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c){
        GF_Team = GF_Team + Tms.Entity__c + ';';
        System.Debug('GFTeam ' + GF_Team);
         }        
   
    
  // Concatenate the Scope of Selected Team Members for Global Business


    if(TMs.Scope__c == System.Label.Connect_Global_Business && TMs.IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c){
        GB_Team = GB_Team + Tms.Entity__c + ';';
         }
   
       
  // Concatenate the Scope of Selected Team Members for Power Region


    if(TMs.Scope__c == System.Label.Connect_Power_Region && TMs.IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c){
        PR_Team = PR_Team + Tms.Entity__c + ';';
         }
  
       
         
      
    }//Team For Loop End
    
    // Adding The Entity of the Record updated/inserted
    
    if(T.Scope__c == System.Label.Connect_Global_Functions)
     GF_Team = GF_Team + T.Entity__c + ';';
     
     if(T.Scope__c == System.Label.Connect_Global_Business)
       GB_Team = GB_Team + T.Entity__c + ';';
       
     if(T.Scope__c == System.Label.Connect_Power_Region)
       PR_Team = PR_Team + T.Entity__c + ';';
       
         
    System.Debug('GF-Team ' + GF_Team);
    
    // Split the Entities in Program for Global Functions

    For(IPO_Strategic_Program__c Prg : Program){

        if(Prg.Name == T.IPO_Strategic_Program_Name__c && Prg.Global_Functions__c!= null){
            for(String GF: Prg.Global_Functions__c.Split(';')){
                
                 GlobalFunctions.add(GF);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
            
            System.Debug('Updated Teams ' + GF_Team +  ' ' + GSC_Team + ' ' + GB_Team + ' ' + PR_Team);
            
     // Split the Entities in Program for Global Business

    For(IPO_Strategic_Program__c Prg : Program){
       
        System.Debug('Global Business ' + Prg.Global_Business__c);   

        if(Prg.Name == T.IPO_Strategic_Program_Name__c && Prg.Global_Business__c != null){
        
             System.Debug('Global Business ' + Prg.Global_Business__c);   
              for(String GB: Prg.Global_Business__c.Split(';')){
                System.Debug('GB ' + GB);
                 GlobalBusiness.add(GB);
                      } // For String Ends
             } // If Ends
             
       
            } // Program Loop Ends
            
    // Split the Entities in Program for Power Region

    For(IPO_Strategic_Program__c Prg : Program){
    
        
        if(Prg.Name == T.IPO_Strategic_Program_Name__c && Prg.Operation__c != null){
            for(String PR: Prg.Operation__c.Split(';')){
                
                 PowerRegion.add(PR);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
              
            
  // Compare Team Entities for Global Functions 
  
  if((GF_Team == '') && (GlobalFunctions.size() > 0))
      GF_No_Network = True; 
  
  else if(GF_Team != ''){

  for(I=0; I < GlobalFunctions.size(); I++){
    //System.Debug('No Network ');

    if(GF_Team.Contains(GlobalFunctions[I])){
     GF_Network = True; 
     System.Debug('Network GF --- ' + GF_Network);
     }
     else{
     GF_No_Network = True; 
     System.Debug('No Network  GF --' + GF_No_Network);
      }
     }// GF For Loops Ends
    } // GF IF Ends
 
 // Compare Team Entities for Global Business
 
   if((GB_Team == '') && (GlobalBusiness.size() > 0))
      GB_No_Network = True; 
   
   else  if(GB_Team != ''){
 
   for(I=0; I < GlobalBusiness.size(); I++){
    System.Debug( ' GB Team ' + GB_Team  + 'GB ' + GlobalBusiness[I]);
  
    if(GB_Team.Contains(GlobalBusiness[I])){
     GB_Network = True; 
     //System.Debug('No Network ' + GB_Network);
     }
     else{
     GB_No_Network = True; 
     //System.Debug('No Network ' + GB_No_Network);
     }
    }// GB For Loops Ends
   } // GB IF Ends
   
   
   // Compare Team Entities for Power Regions
   
 if((PR_Team == '') && (PowerRegion.size() > 0))
      PR_No_Network = True; 
 else    if(PR_Team != ''){
  for(I=0; I < PowerRegion.size(); I++){
    //System.Debug('No Network ');

    if(PR_Team.Contains(PowerRegion[I])){
     PR_Network = True; 
     //System.Debug('No Network ' + PR_Network);
     }
     else{
     PR_No_Network = True; 
     //System.Debug('No Network ' + PR_No_Network);
     }
    }// PR For Loops Ends
   } // PR IF Ends
   
  
   
System.Debug('No Network ' + 'GF' + GF_No_Network + 'GB' + GB_No_Network + 'PR' + PR_No_Network + 'GSC' + GSC_No_Network);

    if((GF_No_Network == True) || (GB_No_Network == True)|| (PR_No_Network == True))
        T.Program_Network__c = False;
    else 
        T.Program_Network__c = True;
  
  
  //System.Debug( 'Program_Network ' + T.Program_Network__c);  
     } // Record Loop Ends
     
   
   
  System.Debug('****** ConnectNetworkTeamMembersBeforeInsert Stop ****'); 

 } // Static Class Ends
 
 // Class to Validate Deployment Network on Update
 
 Public Static void IPOStrategicNetworkBeforeUpdate(List<IPO_Strategic_Deployment_Network__c> TM, List<IPO_Strategic_Deployment_Network__c> TMold)
{

System.Debug('****** ConnectNetworkTeamMembersBeforeUpdate Start ****'); 

// Lists

list<IPO_Strategic_Deployment_Network__c> Team = [select Scope__c, Entity__c, IPO_Strategic_Program_Name__c from IPO_Strategic_Deployment_Network__c  ] ;
list <IPO_Strategic_Program__c> Program = [Select Name, Global_Functions__c, Global_Business__c, Operation__c from IPO_Strategic_Program__c];
List<String> GlobalFunctions = new List<string>();
List<String> GlobalBusiness = new List<string>();
List<String> PowerRegion = new List<string>();
List<String>  GSC_Region = new List<string>();
List<String> Partner_Region = new List<string>();

// Declarations 

String GF_Team;
Integer I;
Boolean GF_Network;
Boolean GF_No_Network;

String GB_Team;
Boolean GB_Network;
Boolean GB_No_Network;


String PR_Team;
Boolean PR_Network;
Boolean PR_No_Network;

String GSC_Team;
Boolean GSC_Network;
Boolean GSC_No_Network;

String Partner_Team;
Boolean Partner_Network = false;
Boolean Partner_No_Network = false;

GF_Team = '';
GB_Team = '';
PR_Team = '';
GSC_Team = '';
Partner_Team = '';


For(IPO_Strategic_Deployment_Network__c T : TM){


  For(IPO_Strategic_Deployment_Network__c TMs : Team){
  
  // Concatenate the Scope of Selected Team Members for Global Functions

    if(TMs.Scope__c == System.Label.Connect_Global_Functions && TMs.IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c && (TMold[0].Scope__c == System.Label.Connect_Global_Functions && TMold[0].IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c && TMold[0].Entity__c != T.Entity__c)){
        GF_Team = GF_Team + Tms.Entity__c + ';';
         }
              
  // Concatenate the Scope of Selected Team Members for Global Business

     if(TMs.Scope__c == System.Label.Connect_Global_Business && TMs.IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c && (TMold[0].Scope__c == System.Label.Connect_Global_Business && TMold[0].IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c && TMold[0].Entity__c != T.Entity__c )){
        GB_Team = GB_Team + Tms.Entity__c + ';';
         }
   
  // Concatenate the Scope of Selected Team Members for Power Region

    if(TMs.Scope__c == System.Label.Connect_Power_Region && TMs.IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c && (TMold[0].Scope__c == System.Label.Connect_Power_Region && TMold[0].IPO_Strategic_Program_Name__c == T.IPO_Strategic_Program_Name__c && TMold[0].Entity__c != T.Entity__c )){
        PR_Team = PR_Team + Tms.Entity__c + ';';
         }
  
       System.Debug('New Change Operation Region ' + PR_Team);
  
         
     } // Team For Loop Ends
     
      // Adding The Entity of the Record updated/inserted
      
   if(T.Scope__c == System.Label.Connect_Global_Functions )
     GF_Team = GF_Team + T.Entity__c + ';';
     
   if(T.Scope__c == System.Label.Connect_Global_Business)
       GB_Team = GB_Team + T.Entity__c + ';';
       
   if(T.Scope__c == System.Label.Connect_Power_Region)
       PR_Team = PR_Team + T.Entity__c + ';';
      
  
    
    // Split the Entities in Program for Global Functions

    For(IPO_Strategic_Program__c Prg : Program){

        if(Prg.Name == T.IPO_Strategic_Program_Name__c && Prg.Global_Functions__c!= null){
            for(String GF: Prg.Global_Functions__c.Split(';')){
                
                 GlobalFunctions.add(GF);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
            
            System.Debug('Updated Teams ' + GF_Team +  ' ' + GSC_Team + ' ' + GB_Team);
            
     // Split the Entities in Program for Global Business

    For(IPO_Strategic_Program__c Prg : Program){
       
        System.Debug('Global Business ' + Prg.Global_Business__c);   

        if(Prg.Name == T.IPO_Strategic_Program_Name__c && Prg.Global_Business__c != null){
        
             System.Debug('Global Business ' + Prg.Global_Business__c);   
              for(String GB: Prg.Global_Business__c.Split(';')){
                System.Debug('GB ' + GB);
                 GlobalBusiness.add(GB);
                      } // For String Ends
             } // If Ends
             
       
            } // Program Loop Ends
            
    // Split the Entities in Program for Power Region

    For(IPO_Strategic_Program__c Prg : Program){
    
        
        if(Prg.Name == T.IPO_Strategic_Program_Name__c && Prg.Operation__c != null){
            for(String PR: Prg.Operation__c.Split(';')){
                
                 PowerRegion.add(PR);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
     
            
  // Compare Team Entities for Global Functions 
  
  if((GF_Team == '') && (GlobalFunctions.size() > 0))
      GF_No_Network = True; 
  
  else if(GF_Team != ''){

  for(I=0; I < GlobalFunctions.size(); I++){
    //System.Debug('No Network ');
   
    if(GF_Team.Contains(GlobalFunctions[I])){
     GF_Network = True; 
     System.Debug('No Network GF' + GF_Network);
     }
     else{
     GF_No_Network = True; 
     System.Debug('No Network GF ' + GF_No_Network);
      }
     }// GF For Loops Ends
    } // GF IF Ends
 
 // Compare Team Entities for Global Business
 
 if((GB_Team == '') && (GlobalBusiness.size() > 0))
      GB_No_Network = True; 
      
 else if(GB_Team != ''){
    
   for(I=0; I < GlobalBusiness.size(); I++){
    System.Debug( ' GB Team ' + GB_Team  + 'GB ' + GlobalBusiness[I]);
   
    if(GB_Team.Contains(GlobalBusiness[I])){
     GB_Network = True; 
     System.Debug('No Network GB ' + GB_Network);
     }
     else{
     GB_No_Network = True; 
     System.Debug('No Network GB ' + GB_No_Network);
     }
    } // GB For Loops Ends
   }// GB IF Ends
   
   
   // Compare Team Entities for Power Regions
   
 if((PR_Team == '') && (PowerRegion.size() > 0))
     PR_No_Network = True; 
     
 else  if(PR_Team != ''){
 
  for(I=0; I < PowerRegion.size(); I++){
    //System.Debug('No Network ');
  
    if(PR_Team.Contains(PowerRegion[I])){
     PR_Network = True; 
     System.Debug('Network PR' + PR_Network);
     }
     else{
     PR_No_Network = True; 
     System.Debug('No Network PR' + PR_No_Network);
     }
    }// PR For Loops Ends
   } // PR IF Ends
   
  

System.Debug('No Network ' + 'GF' + GF_No_Network + 'GB' + GB_No_Network + 'PR' + PR_No_Network + 'GSC' + GSC_No_Network);

    if((GF_No_Network == True) || (GB_No_Network == True)|| (PR_No_Network == True))
        T.Program_Network__c = False;
      else
        T.Program_Network__c = True;
  
  
  //System.Debug( 'Program_Network ' + T.Program_Network__c);  
     } // Team Loop Ends
     
   
   
  System.Debug('****** ConnectNetworkTeamMembersBeforeUpdate Stop ****'); 

 } // Static Class Ends
 
 

}