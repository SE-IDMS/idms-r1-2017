@istest
private class AP_Case_FailedCaseHandler_TEST{
    static testMethod void doFutureCaseReconciliationProcess_TEST(){
        Test.startTest();
        Case objCase = new Case();
        objCase.Subject = 'This is a Test Subject';
        objCase.Origin = 'Email';
        objCase.Description = 'This is plain text verion of the email body';
        objCase.Priority ='Normal';
        objCase.SuppliedEmail ='vimalkarun@gmail.com';
        
        
        String fromAddress = 'vimalkarun@gmail.com';
        Messaging.InboundEmail          objInboundEmail         =   new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope       objInboundEnvelope      =   new Messaging.InboundEnvelope();
        Messaging.InboundEmail.Header   objInboundEmailHeader   = new Messaging.InboundEmail.Header();
        objInboundEmail.headers=new Messaging.InboundEmail.Header[1];
        objInboundEmailHeader.name='Date';
        objInboundEmailHeader.value='Tue, 28 Apr 2009 14:08:37 -0700';
        objInboundEmail.headers[0]=objInboundEmailHeader;
        
        objInboundEmail.Subject = 'My TestKeyword Testing Email To Case';
        objInboundEmail.fromname = 'Contact USA';
        objInboundEmail.fromAddress = fromAddress;
        objInboundEmail.ToAddresses = new List<String>();
        objInboundEmail.CCAddresses = new List<String>();
        objInboundEmail.plaintextbody =  'This is a test email body please ignore the text';
        
        objInboundEmail.fromAddress = fromAddress;
        objInboundEnvelope.fromAddress = fromAddress;

        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        
        objInboundEmail.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        String strCaseSerialized    = JSON.serialize(objCase);
        String strEmailSerialized    = JSON.serialize(objInboundEmail);
        String strEnvelopeSerialized = JSON.serialize(objInboundEnvelope);
        
        AP_Case_FailedCaseHandler.doFutureCaseReconciliationProcess(strCaseSerialized,strEmailSerialized,strEnvelopeSerialized);         
        Test.stopTest();
    }
}