@isTest(seealldata=true)
//Created by VISHNU C
public class VFC_POReleaseiInBO_TEST
{
    
    public static testMethod void PoRelease() 
    {
        //User user = CreateUser();
       // insert user;
        system.runAs(CreateUser())
        {
         Country__c country = new country__c();
            country.Name = 'TestCountry';
            country.CountryCode__c = 'YY';
            insert country;
            Account acc = new account();
            acc = Utils_TestMethods.createAccount();
            acc.name = 'Test';
            acc.Country__c=country.id;
            acc.City__c = 'Chennai';

            insert acc;
            Contact Cnct=Utils_TestMethods.CreateContact(acc.Id,'TContact');
            insert Cnct;
            //Contact Cnct = new Contact(AccountId=acc.id,email='UTCont@mail.com',phone='1234567890',LastName='UTContact001',FirstName='Fname1');
            //insert Cnct;  //Inserting Contact
            WorkOrderGroup__c WOG = new WorkOrderGroup__c(WOGName__c='Test',Status__c='Open',Account__c=Acc.id,BackOfficeReference__c='123',CountryofBackOffice__c = 'Global', BackOfficeSystem__c = 'IT_ORA');
            insert WOG;
            SVMXC__Site__c Location = new SVMXC__Site__c(Name='Test',SVMXC__Location_Type__c='BUILD',SVMXC__Account__c=acc.id);
            insert Location;
            SVMXC__Service_Order__c wo =      new SVMXC__Service_Order__c(                                       
                                                         SVMXC__Company__c=Acc.Id,
                                                         SVMXC__Order_Status__c = 'New', 
                                                         BackOfficeReference__c='123',  
                                                         TECH_WOBackOfficeRecordID__c='123',
                                                         SVMXC__Order_Type__c = 'Preventive Maintenance',
                                                         WorkOrderSubType__c = 'Battery Maintenance',
                                                         SVMXC__Site__c = Location.id,
                                                         //SVMXC__Product__c = prod.id,
                                                         SVMXC__Contact__c = Cnct.Id, 
                                                         RescheduleReason__c='Weather Related',
                                                         Service_Business_Unit__c='Energy',
                                                         Parent_Work_Order__c = null,
                                                         Is_Billable__c = true,
                                                         Customer_Service_Request_Time__c= system.now(),
                                                         WorkOrderGroup__c = WOG.id,
                                                         CustomerTimeZone__c = 'Pacific/Kiritimati'
                                                         );
                                                   
            insert wo;
            Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c; 
            Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
            Id POId = PartOrderRecordTypeInfo.get('RMA').getRecordTypeId();
            SVMXC__RMA_Shipment_Order__c porder = new SVMXC__RMA_Shipment_Order__c();
            porder.SVMXC__Order_Status__c = 'Open';
            porder.RecordTypeId = POId;
            porder.SVMXC__Service_Order__c = wo.id;//cc
            porder.SVMXC__Company__c = acc.id;
            porder.SVMXC__Contact__c = Cnct.id;
            porder.Service_level__c = 'SDA';
            porder.SVMXC__Shipping_Receiving_Notes__c = 'Test';
            porder.Ship_to__c = acc.id;
            porder.Freight_terms__c = 'COL';
            insert porder;
            
             Schema.DescribeSObjectResult dSobjres1 = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c; 
            Map<String,Schema.RecordTypeInfo> PartOrderLineRecordTypeInfo = dSobjres1.getRecordTypeInfosByName();
            Id RMALineId = PartOrderLineRecordTypeInfo.get('RMA').getRecordTypeId();
            SVMXC__RMA_Shipment_Line__c poLine = new SVMXC__RMA_Shipment_Line__c(
                SVMXC__RMA_Shipment_Order__c = porder.Id,
                //RecordType = RMALineId,
                SVMXC__Expected_Quantity2__c = 5,
                RecordTypeId = RMALineId,
                CommercialReference__c = 'test2',
                RequestedDate__c = Date.Today(),
                Return_Reason__c = 'Not Used',
                SVMXC__Line_Type__c = 'RMA'
            );
            insert poLine;
            PageReference pageRef1 = Page.VFP_POReleaseiInBO ; 
            Test.setCurrentPage(pageRef1);
            system.currentPageReference().getParameters().put('PartOrderId',porder.id);
            system.currentPageReference().getParameters().put('action', 'PartOrder.RELEASE');
            //controller
             VFC_POReleaseiInBO c1 = new VFC_POReleaseiInBO(new ApexPages.StandardController(porder));
            c1.redirectToMW();
        
        }
        
    }
    public Static User CreateUser()
    {
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP30;AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        UserRole uRole = [Select Id,Name,DeveloperName From UserRole where DeveloperName = 'CEO'];
        user.UserRoleId = uRole.Id;
        insert user;
        return user;
    
    }
    
}