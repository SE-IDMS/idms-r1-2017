public with sharing class FieloPRM_AP_MemberSegmentTrigger {
    
    public static void createFeatureMember(){

        list<FieloEE__MemberSegment__c> triggerNew = trigger.new;

        Id segmentRecordTypeId = Schema.SObjectType.FieloPRM_MemberFeatureDetail__c.getRecordTypeInfosByName().get('Segment').getRecordTypeId();
        set<id> setIDSegment = new set<id>();
        set<id> setIDMembers = new set<id>();
        set<String> setMembSegment = new set<String>();
        for(FieloEE__MemberSegment__c memberSegment : triggerNew){
            
                setIDSegment.add(memberSegment.FieloEE__Segment2__c);
                setIDMembers.add(memberSegment.FieloEE__Member2__c);
                String membSegKey =  String.valueof(memberSegment.FieloEE__Segment2__c);
                setMembSegment.add(membSegKey);
            

        }


        map<id,FieloPRM_MemberFeature__c> mapFeatures = new map<id,FieloPRM_MemberFeature__c>( [SELECT ID, F_PRM_Feature__c, F_PRM_IsActive__c, F_PRM_Member__c, 
                (Select id, F_PRM_isActive__c,F_PRM_Segment__c FROM Member_Feature_Details__r) FROM FieloPRM_MemberFeature__c 
                WHERE F_PRM_Member__c in: setIDMembers ]);

        map<id,map<id,FieloPRM_MemberFeature__c>>  mapMemberFeature = new map<id,map<id,FieloPRM_MemberFeature__c>>();
        
        for(FieloPRM_MemberFeature__c memberFeature : mapFeatures.values())
{            map<id,FieloPRM_MemberFeature__c> mapIdMemberFeature = new map<id,FieloPRM_MemberFeature__c>();
            if(mapMemberFeature.containskey(memberFeature.F_PRM_Member__c)){ mapIdMemberFeature = mapMemberFeature.get(memberFeature.F_PRM_Member__c); }
            mapIdMemberFeature.put(memberFeature.F_PRM_Feature__c,memberFeature);

            mapMemberFeature.put(memberFeature.F_PRM_Member__c,mapIdMemberFeature);
        }

        map<id,FieloEE__Member__c> mapIdMember = new map<id,FieloEE__Member__c>([SELECT id, PRMUIMSId__c  FROM FieloEE__Member__c WHERE id in:  setIDMembers]);

        map<id,FieloEE__RedemptionRule__c> mapIdSegment = new map<id,FieloEE__RedemptionRule__c>([SELECT id, F_PRM_Feature__c FROM FieloEE__RedemptionRule__c WHERE F_PRM_Feature__c != null AND id in:  setIDSegment]);
        
        list<FieloPRM_MemberFeature__c> listMemberFeatureToInsert = new list<FieloPRM_MemberFeature__c>();
        list<FieloPRM_MemberFeature__c> listMemberFeatureToUpdate = new list<FieloPRM_MemberFeature__c>();
        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailInsert = new list<FieloPRM_MemberFeatureDetail__c>();
        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailToUpdate = new list<FieloPRM_MemberFeatureDetail__c>();
        map<FieloPRM_MemberFeatureDetail__c, FieloPRM_MemberFeature__c> mapMemberFeatureAux = new map<FieloPRM_MemberFeatureDetail__c, FieloPRM_MemberFeature__c>();
        for(FieloEE__MemberSegment__c memberSegment : triggerNew){
            if(mapIdSegment.containskey(memberSegment.FieloEE__Segment2__c) && mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c != null){
                
                if(!mapMemberFeature.containskey(memberSegment.FieloEE__Member2__c) 
                        || (mapMemberFeature.containskey(memberSegment.FieloEE__Member2__c) 
                        && !mapMemberFeature.get(memberSegment.FieloEE__Member2__c).containskey(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c)) ){

                    system.debug('if1');
                    FieloPRM_MemberFeature__c newFeatureMember = new FieloPRM_MemberFeature__c(F_PRM_Member__c = memberSegment.FieloEE__Member2__c, 
                        F_PRM_Feature__c = mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c, F_PRM_IsActive__c = false,
                        PRMUIMSId__c = mapIdMember.get(memberSegment.FieloEE__Member2__c).PRMUIMSId__c);
                    
                    /*newFeatureMember.F_PRM_Member__c = memberSegment.FieloEE__Member2__c;
                    newFeatureMember.F_PRM_Feature__c = mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c;
                    newFeatureMember.F_PRM_IsActive__c = true;*/

                     FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c();
                
                    newMemberFeatureDetail.F_PRM_isActive__c = true;
                    newMemberFeatureDetail.recordTypeId = segmentRecordTypeId; 
                    newMemberFeatureDetail.F_PRM_Segment__c = memberSegment.FieloEE__Segment2__c;
                    //newMemberFeatureDetail.F_PRM_MemberFeature__c = memberfeature.id;

                    mapMemberFeatureAux.put(newMemberFeatureDetail,newFeatureMember);
                    //listMemberFeatureDetailInser.add(newMemberFeatureDetail);

                    listMemberFeatureToInsert.add(newFeatureMember);    
                }else if(mapMemberFeature.get(memberSegment.FieloEE__Member2__c).containskey(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c) 
                        && mapMemberFeature.get(memberSegment.FieloEE__Member2__c).get(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c).F_PRM_IsActive__c == false ) {

                    system.debug('if2');
                    FieloPRM_MemberFeature__c memeberFeatureAux = mapMemberFeature.get(memberSegment.FieloEE__Member2__c).get(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c);
                    Boolean recordCreated = false;
                    for(FieloPRM_MemberFeatureDetail__c mfd : memeberFeatureAux.Member_Feature_Details__r){
                        String membSegKey = String.valueof(mfd.F_PRM_Segment__c);
                        if(setMembSegment.contains(membSegKey) && mfd.F_PRM_isActive__c == false){

                            mfd.F_PRM_isActive__c = true;
                            recordCreated = true;
                            listMemberFeatureDetailToUpdate.add(mfd);
                        }
                    }
                    if(!recordCreated){
                        FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c();
                        newMemberFeatureDetail.RecordTypeId = segmentRecordTypeId;
                        newMemberFeatureDetail.F_PRM_isActive__c = true;
                        newMemberFeatureDetail.F_PRM_Segment__c = memberSegment.FieloEE__Segment2__c;
                        //newMemberFeatureDetail.F_PRM_Badge__c = ;
                        newMemberFeatureDetail.F_PRM_MemberFeature__c = mapMemberFeature.get(memberSegment.FieloEE__Member2__c).get(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c).id;
                        listMemberFeatureDetailInsert.add(newMemberFeatureDetail);
                    }
                    //memeberFeatureAux.F_PRM_IsActive__c = true;
                    //listMemberFeatureToUpdate.add(memeberFeatureAux);


                }else if(mapMemberFeature.get(memberSegment.FieloEE__Member2__c).containskey(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c) 
                        && mapMemberFeature.get(memberSegment.FieloEE__Member2__c).get(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c).F_PRM_IsActive__c == true){
                    
                    system.debug('if3');
                    FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c();
                    newMemberFeatureDetail.RecordTypeId = segmentRecordTypeId;
                    newMemberFeatureDetail.F_PRM_isActive__c = true;
                    newMemberFeatureDetail.F_PRM_Segment__c = memberSegment.FieloEE__Segment2__c;
                    //newMemberFeatureDetail.F_PRM_Badge__c = ;
                    newMemberFeatureDetail.F_PRM_MemberFeature__c = mapMemberFeature.get(memberSegment.FieloEE__Member2__c).get(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c).id;
                    listMemberFeatureDetailInsert.add(newMemberFeatureDetail);
                }
                
            }

        }
        system.debug('listMemberFeatureToInsert: ' + listMemberFeatureToInsert);
        if(listMemberFeatureToInsert.size() > 0){ 
            insert listMemberFeatureToInsert;
            
           /* for(FieloPRM_MemberFeature__c memberfeature : listMemberFeatureToInsert){
                FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c();
                //newMemberFeatureDetail.F_PRM_User__c = ;
                newMemberFeatureDetail.F_PRM_isActive__c = true;
                newMemberFeatureDetail.RecordTypeId = segmentRecordTypeId;
                //newMemberFeatureDetail.F_PRM_Badge__c = ;
                newMemberFeatureDetail.F_PRM_MemberFeature__c = memberfeature.id;
                
                
                //newMemberFeatureDetail.F_PRM_Segment__c = ;
                listMemberFeatureDetailInsert.add(newMemberFeatureDetail);
            }*/
        }
        for(FieloPRM_MemberFeatureDetail__c mfd : mapMemberFeatureAux.keyset()){

            mfd.F_PRM_MemberFeature__c = mapMemberFeatureAux.get(mfd).id;
            listMemberFeatureDetailInsert.add(mfd);
        }
        system.debug('listMemberFeatureDetailInsert: ' + listMemberFeatureDetailInsert);
        insert listMemberFeatureDetailInsert;
        update listMemberFeatureDetailToUpdate;

        if(listMemberFeatureToUpdate.size() > 0){ update listMemberFeatureToUpdate;}
    }

    public static void disableFeatureMember(){

        list<FieloEE__MemberSegment__c> triggerOld = trigger.old;

        set<id> setIDSegment = new set<id>();
        set<id> setIDMembers = new set<id>();
        set<String> setMemSegKey = new set<String>();
        for(FieloEE__MemberSegment__c memberSegment : triggerOld){
            
                setIDSegment.add(memberSegment.FieloEE__Segment2__c);
                setIDMembers.add(memberSegment.FieloEE__Member2__c);
                string memSegKey = String.valueOf(memberSegment.FieloEE__Member2__c) + String.valueOf(memberSegment.FieloEE__Segment2__c);
                setMemSegKey.add( memSegKey );
            

        }

        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetail = [SELECT id,F_PRM_MemberFeature__r.F_PRM_Member__c,F_PRM_Segment__c, F_PRM_isActive__c   FROM FieloPRM_MemberFeatureDetail__c WHERE F_PRM_MemberFeature__r.F_PRM_Member__c in: setIDMembers AND F_PRM_Segment__c =:  setIDSegment ];
        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailToUpdate = new list<FieloPRM_MemberFeatureDetail__c>();
        for(FieloPRM_MemberFeatureDetail__c mfd: listMemberFeatureDetail){
            string memSegKey = String.valueOf(mfd.F_PRM_MemberFeature__r.F_PRM_Member__c) + String.valueOf(mfd.F_PRM_Segment__c);
            if(setMemSegKey.contains(memSegKey)){
                mfd.F_PRM_isActive__c =  false;
                listMemberFeatureDetailToUpdate.add(mfd);
            }
        }

        update listMemberFeatureDetailToUpdate;
        /*
        map<id,FieloPRM_MemberFeature__c> mapFeatures = new map<id,FieloPRM_MemberFeature__c>( [SELECT ID, F_PRM_Feature__c, F_PRM_IsActive__c, F_PRM_Member__c FROM FieloPRM_MemberFeature__c WHERE F_PRM_Member__c in: setIDMembers AND F_PRM_IsActive__c = true]);
        map<id,map<id,FieloPRM_MemberFeature__c>>  mapMemberFeature = new map<id,map<id,FieloPRM_MemberFeature__c>>();
        
        for(FieloPRM_MemberFeature__c memberFeature : mapFeatures.values()){
            map<id,FieloPRM_MemberFeature__c> mapIdMemberFeature = new map<id,FieloPRM_MemberFeature__c>();
            if(mapMemberFeature.containskey(memberFeature.F_PRM_Member__c)){ mapIdMemberFeature = mapMemberFeature.get(memberFeature.F_PRM_Member__c);}
            mapIdMemberFeature.put(memberFeature.F_PRM_Feature__c,memberFeature);

            mapMemberFeature.put(memberFeature.F_PRM_Member__c,mapIdMemberFeature);
        }


        map<id,FieloEE__RedemptionRule__c> mapIdSegment = new map<id,FieloEE__RedemptionRule__c>([SELECT id, F_PRM_Feature__c FROM FieloEE__RedemptionRule__c WHERE id in:  setIDSegment]);
        
        list<FieloPRM_MemberFeature__c> listMemberFeatureToUpdate = new list<FieloPRM_MemberFeature__c>();

        for(FieloEE__MemberSegment__c memberSegment : triggerOld){
            if(mapIdSegment.containskey(memberSegment.FieloEE__Segment2__c) && mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c != null){
                
                if(mapMemberFeature.containskey(memberSegment.FieloEE__Member2__c) 
                       && mapMemberFeature.get(memberSegment.FieloEE__Member2__c).containskey(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c) ) {

                    FieloPRM_MemberFeature__c memeberFeatureAux = mapMemberFeature.get(memberSegment.FieloEE__Member2__c).get(mapIdSegment.get(memberSegment.FieloEE__Segment2__c).F_PRM_Feature__c);
                    memeberFeatureAux.F_PRM_IsActive__c = false;
                    listMemberFeatureToUpdate.add(memeberFeatureAux);
                }
                
            }

        }
     
        if(listMemberFeatureToUpdate.size() > 0){
            update listMemberFeatureToUpdate;
        }
        */
    }
}