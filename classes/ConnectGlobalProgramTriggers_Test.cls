/*
    Author          : Kiran Kareddy (Schneider Electric)
    Date Created    : 20-Mar-2012
    Description     : Test Class for ConnectGlobalProgramTriggers
*/

@isTest
private class ConnectGlobalProgramTriggers_Test {
    static testMethod void testConnectGlobalProgramTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect1');
        User runAsUser2 = Utils_TestMethods_Connect.createStandardConnectUser('connectT1');
        User runAsUser3 = Utils_TestMethods_Connect.createStandardConnectUser('connectT2');
        System.runAs(runAsUser){
            //Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            Initiatives__c inti=new Initiatives__c(Name='initiative', Transformation__c = 'Connect People');
            insert inti;
            Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',User__c = runasuser.id, Program_Leader_2__c = runasuser2.id,Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power;'+Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Power_Region__c='NA;APAC');
            insert cp;
            Project_NCP__c cp1=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp1',User__c = runasuser.id, Program_Leader_2__c = runasuser2.id,Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power;'+Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Power_Region__c='NA;APAC');
            insert cp1;
            Connect_Milestones__c mile=new Connect_Milestones__c(Program_Number__c=cp.id,Milestone_Name__c='mile1',Global_Functions__c='GM;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;'+Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Power_Region__c='NA');
            insert mile;
            Team_Members__c C=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Functions', Entity__c='GM');
            insert C;
            Team_Members__c C1=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Business', Entity__c='ITB');
            insert C1;
            Team_Members__c C2=new Team_Members__c(Program__c=cp.id,Team_Name__c='Power Region', Entity__c='NA');
            insert C2;
             Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp.id, Transformation__c = Label.ConnectTransPeople);
             insert GKPIT;
             Global_KPI_Targets__c GKPIT1=new Global_KPI_Targets__c(KPI_Name__c='testKPI1',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp1.id, Transformation__c = Label.ConnectTransPeople);
             insert GKPIT1;
            Test.startTest();
           
                     
            cp.Global_Functions__c='GM;S&I;HR;IPO;GSC-Regional';
            cp.Global_Business__c='ITB;power;industry;Partner-Division;'+Label.Connect_Smart_Cities;
            cp.Power_Region__c='NA;APAC;EMEAS';
            cp.GSC_Regions__c='NA';
            cp.Partner_Region__c='test';
            cp.Smart_Cities_Division__c='test';
            cp.User__c = Utils_TestMethods_Connect.createStandardConnectUser('connectUS').id;
            cp.Program_Leader_2__c = label.Connect_Email; 
            cp.Program_Leader_3__c = label.Connect_Email;        
            update cp;
            
            cp.Global_Functions__c='S&I;GSC-Regional';
            cp.GSC_Regions__c='EMEAS';
            cp.Global_Business__c='Industry';
            cp.Power_Region__c='EMEAS';
            

                       
            mile.Global_Functions__c='Finance';
            mile.Global_Business__c='Infrastructure;Partner-Division';
            mile.Power_Region__c='test';
            mile.Partner_Region__c='test';
            try{
            update cp;
            update mile;
            
            }catch(DmlException d)
            {
            d.getMessage();
            }
            cp.Global_Functions__c='';
            cp.Global_Business__c='';
            cp.Power_Region__c='';
            try{
            update cp;
            }catch(DmlException e)
            {
            e.getMessage();
            }
            
            Test.stopTest();
            }
       }
    }