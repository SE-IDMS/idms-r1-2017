/*   Author: Nicolas PALITZYNE (Accenture)  
     Date Of Creation: 05/11/2011   
     Description : Controller for the RMA creation page 
*/

public class VFC60_NewRMA
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/

    public RMA__c NewRMA{get;set;} // Current new RMA
    public Case relatedCase{get;set;} // Master Case
    public Account relatedAccount{get;set;} // Account related to the case
    public Contact relatedContact{get;set;} // Contact related to the case
    public TEX__c relatedTex{get;set;}
    Public Boolean IsCancel{get; set;} // Flag to display Cancel Button
       

    /*=======================================
      CONSTRUCTOR
    =======================================*/ 

    public VFC60_NewRMA(ApexPages.StandardController controller)
    {
        NewRMA = (RMA__c)controller.getRecord();
      
    } 
    
    /*=======================================
      METHODS
    =======================================*/ 
 
    // Method to initialize the RMA with the defaulting values
    public void initializeRMA(RMA__c aNewRMA)
    {
        relatedCase = New Case();
        aNewRMA.CustomerAddress__c = '';
        
        // --- sukumar - oct release ================================================
        Schema.DescribeSObjectResult TEXSchemaResult =  TEX__c.sObjectType.getDescribe();  
        system.debug('## TEX PREFIX - '+ TEXSchemaResult); 
        // --- end sukumar - oct release   ============================================= 
        
        // Fetch the ID of the related case from the URL 
        String RelObjId = system.currentpagereference().getParameters().get(Label.CL00330);
        //String caseID = system.currentpagereference().getParameters().get(Label.CL00330);
        String caseID;
        system.debug('## RelObjId - '+ RelObjId); 
        //system.debug('## return url ##'+ caseID);
        if(RelObjId.contains('?')){
            RelObjId=RelObjId.substring(1,16);
            system.debug('## RelObjId 15- '+ RelObjId); 
        }
        else{  RelObjId=RelObjId.substring(1);}
        system.debug('## RelObjId 1- '+ RelObjId); 
        If (RelObjId.startsWith(TEXSchemaResult.getKeyPrefix()))
        { system.debug('## RelObjId 2- '+ RelObjId); 
            List<TEX__c> TEXList = [SELECT ID, Case__c,name,ForceManualSelection__c, AssessmentType__c from TEX__c WHERE ID=:RelObjId];
            system.debug('## TEXList  ##'+ TEXList[0]);
            system.debug('## TEXList.case__c  ##'+ TEXList[0].case__c);
            caseID = TEXList[0].case__c;
            NewRMA.Tex__c = TEXList[0].ID;
            relatedTex = TEXList[0];
        }
        else
        {
            
            if (RelObjId != null)
            {
                caseID = RelObjId.substring(1); 
            } 
        } 
        aNewRMA.approver__c                      = null;
        aNewRMA.additionalComments__c            = null;
        aNewRMA.ROnumber__c                      = null;
        aNewRMA.OriginalSalesOrder__c            = null;
        aNewRMA.RequiredAction__c                = null;
        aNewRMA.TECH_CheckShippingAddress__c     = false;
        aNewRMA.AccountStreet__c                 = null;
        aNewRMA.AccountAdditionalAddress__c      = null;
        aNewRMA.AccountCity__c                   = null;
        aNewRMA.AccountZipCode__c                = null;
        aNewRMA.AccountStateProvince__c          = null;
        aNewRMA.AccountCounty__c                 = null;
        aNewRMA.AccountCountry__c                = null;
        aNewRMA.AccountPOBox__c                  = null;
        aNewRMA.AccountPOBoxZip__c               = null;
        aNewRMA.AccountStreetLocalLang__c        = null;
        aNewRMA.AccountLocalAdditionalAddress__c = null;
        aNewRMA.AccountLocalCity__c              = null;
        aNewRMA.AccountLocalCounty__c            = null;
        
        // Retrieve related case
        List<Case> caseList = [SELECT ID, CommercialReference__c,Family__c,CaseNumber, AccountID, ContactID  from Case WHERE ID=:caseID];       
        //system.debug('## caseList ##'+ caseList[0]);
        if(caseList.size() == 0)
        {
            caseList = [SELECT ID, CommercialReference__c,Family__c,CaseNumber, AccountID, ContactID from Case WHERE ID=:newRMA.Case__c];
           
        }
        
        // Populate the Customer address with the address of the related Account
        if(caseList.size() == 1)
        {
            relatedCase = caseList[0];

            List<Contact> ContactList = [SELECT ID, Name FROM Contact WHERE ID=:relatedCase.ContactID];
            
            if(ContactList.size() == 1)
            {
                NewRMA.ContactName__c = ContactList[0].ID;
                relatedContact = ContactList[0];
            }
        
            List<Account> accountList = [SELECT Id,
                                                Name,
                                                Street__c,
                                                AdditionalAddress__c,
                                                City__c,
                                                ZipCode__c,
                                                StateProvince__c,
                                                StateProvince__r.Name,
                                                County__c,
                                                Country__c,
                                                Country__r.Name,
                                                POBox__c,   
                                                POBoxZip__c,
                                                StreetLocalLang__c,
                                                LocalAdditionalAddress__c,
                                                LocalCity__c,
                                                LocalCounty__c
                                         FROM Account WHERE ID=:relatedCase.AccountID];

            if(accountList.size()== 1 && accountList[0].Name != null)
            {
               relatedAccount = accountList[0];

               aNewRMA.AccountName__c                   = relatedAccount.Id;
               aNewRMA.AccountStreet__c                 = relatedAccount.Street__c;
               aNewRMA.AccountAdditionalAddress__c      = relatedAccount.AdditionalAddress__c;
               aNewRMA.AccountCity__c                   = relatedAccount.City__c;
               aNewRMA.AccountZipCode__c                = relatedAccount.ZipCode__c;
               aNewRMA.AccountStateProvince__c          = relatedAccount.StateProvince__c;
               aNewRMA.AccountCounty__c                 = relatedAccount.County__c;
               aNewRMA.AccountCountry__c                = relatedAccount.Country__c;
               aNewRMA.AccountPOBox__c                  = relatedAccount.POBox__c;
               aNewRMA.AccountPOBoxZip__c               = relatedAccount.POBoxZip__c;
               aNewRMA.AccountStreetLocalLang__c        = relatedAccount.StreetLocalLang__c;
               aNewRMA.AccountLocalAdditionalAddress__c = relatedAccount.LocalAdditionalAddress__c;
               aNewRMA.AccountLocalCity__c              = relatedAccount.LocalCity__c;
               aNewRMA.AccountLocalCounty__c            = relatedAccount.LocalCounty__c;
            }
            
                  
        
        }
        
         }
    
    // Action to switch automatically to the edit page with pre-defaulted values
    public pagereference goToEditPage()
    {
       
         
     
      // Pre-populate fields of the edit page
        initializeRMA(NewRMA);
        
       
        //************* I2P-RR: DEC 2012 RELEASE - START **********************************************
        // If CommercialReference is null on Case, we should not be able to create Return Request.
        // OctRelease => Adding relatedTex
        if(relatedCase.id==null)
        {
          IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLDEC12RR34));
                return null;
        }
        
        else IF( relatedCase.CommercialReference__c==null && relatedCase.Family__c==null)
            {
                IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLDEC12RR33));
                return null;
            }
        //************* I2P-RR: DEC 2012 RELEASE - END **********************************************
     
     
     //********** sukumar salla oct 2013 release ***************************
     if(NewRMA.TEX__c != null)
     {
       if(relatedCase.Id!=null && relatedTex.Id!=null && relatedTex.AssessmentType__c!='Product Return to Expert Center')
        {
             IsCancel=True;
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT13RR23));
             return null;
        }
       } 
     //*********** sukumar salla end oct 2013 release ****************
        
        // Create a new edit page for the RMA 
        PageReference aNewRMAEditPage = new PageReference('/'+SObjectType.RMA__c.getKeyPrefix()+'/e' );

        // Set the Record Type for New RMA
        aNewRMAEditPage.getParameters().put(Label.CL10201, Label.CL10202);
        
        // Set the return to the parent Case (retURL)
        aNewRMAEditPage.getParameters().put(Label.CL00330, NewRMA.Case__c);   
        
        // Disable override
        aNewRMAEditPage.getParameters().put(Label.CL00690, Label.CL00691); 
        
        // Pre-default the parent case with the ID (_lkid) and the CaseNumber  
        system.debug('## tex ##'+NewRMA.Tex__c);
        aNewRMAEditPage.getParameters().put(Label.CL00688, NewRMA.Case__c);
        aNewRMAEditPage.getParameters().put(Label.CL00689, relatedCase.CaseNumber); 
        // ============ sukumar - oct13 release =========
        if(NewRMA.TEX__c != null)
        {
            aNewRMAEditPage.getParameters().put(Label.CLOCT13RR17,NewRMA.Tex__c); 
            aNewRMAEditPage.getParameters().put(Label.CLOCT13RR16, relatedTex.Name); 
            aNewRMAEditPage.getParameters().put('RecordType',Label.CLOCT13RR27); 
            //aNewRMAEditPage.getParameters().put('RecordType','012K00000000YCG'); 
               
        }
        // ============= end - sukumar ==================
        // Pre-default the Account Name 
        if(NewRMA.AccountName__c != null)
        {
            aNewRMAEditPage.getParameters().put(Label.CL00692, RelatedAccount.Name);
            aNewRMAEditPage.getParameters().put(Label.CL00699, NewRMA.AccountName__c);            
        }

        // Pre-default the Contact Name 
        if(NewRMA.AccountName__c != null)
        {
            aNewRMAEditPage.getParameters().put(Label.CL00701, RelatedContact.Name);
            aNewRMAEditPage.getParameters().put(Label.CL00702, NewRMA.ContactName__c);            
        }

        // Pre-default the Account Address - Street
        if(NewRMA.AccountStreet__c != null && NewRMA.AccountStreet__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10220, NewRMA.AccountStreet__c);
        }

        // Pre-default the Account Address - Additional Information
        if(NewRMA.AccountAdditionalAddress__c != null && NewRMA.AccountAdditionalAddress__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10221, NewRMA.AccountAdditionalAddress__c);
        }

        // Pre-default the Account Address - City
        if(NewRMA.AccountCity__c != null && NewRMA.AccountCity__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10222, NewRMA.AccountCity__c);
        }

        // Pre-default the Account Address - Zip Code
        if(NewRMA.AccountZipCode__c != null && NewRMA.AccountZipCode__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10223, NewRMA.AccountZipCode__c);
        }

        // Pre-default the Account Address - County
        if(NewRMA.AccountCounty__c != null && NewRMA.AccountCounty__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10225, NewRMA.AccountCounty__c);
        }
        // Pre-default the Account Address - State/Province
        // with the ID (_lkid) and the value 
        if(NewRMA.AccountStateProvince__c != null)
        {
            aNewRMAEditPage.getParameters().put(Label.CL10233, NewRMA.AccountStateProvince__c);
            aNewRMAEditPage.getParameters().put(Label.CL10224, relatedAccount.StateProvince__r.Name);
        }

        // Pre-default the Account Address - Country
        // with the ID (_lkid) and the value 
        if(NewRMA.AccountCountry__c != null)
        {
            aNewRMAEditPage.getParameters().put(Label.CL10234, NewRMA.AccountCountry__c);
            aNewRMAEditPage.getParameters().put(Label.CL10226, relatedAccount.Country__r.Name);
        }

        // Pre-default the Account Address - PO Box 
        if(NewRMA.AccountPOBox__c != null && NewRMA.AccountPOBox__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10227, NewRMA.AccountPOBox__c);
        }

        // Pre-default the Account Address - PO Box Zip Code 
        if(NewRMA.AccountPOBoxZip__c != null && NewRMA.AccountPOBoxZip__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10228, NewRMA.AccountPOBoxZip__c);
        }

        // Pre-default the Account Address - Local Street  
        if(NewRMA.AccountStreetLocalLang__c != null && NewRMA.AccountStreetLocalLang__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10229, NewRMA.AccountStreetLocalLang__c);
        }

        // Pre-default the Account Address - Local Additional Info  
        if(NewRMA.AccountLocalAdditionalAddress__c != null && NewRMA.AccountLocalAdditionalAddress__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10230, NewRMA.AccountLocalAdditionalAddress__c);
        }

        // Pre-default the Account Address - Local City  
        if(NewRMA.AccountLocalCity__c != null && NewRMA.AccountLocalCity__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10231, NewRMA.AccountLocalCity__c);
        }

        // Pre-default the Account Address - Local County  
        if(NewRMA.AccountLocalCounty__c != null && NewRMA.AccountLocalCounty__c != '')
        {
            aNewRMAEditPage.getParameters().put(Label.CL10232, NewRMA.AccountLocalCounty__c);
        }
        
       
         
        //*** RELEASE DEC12 - START ****************************************************//
        // pass the saveURL
        // CLDEC12RR02 - saveURL
        // CLDEC12RR01 - '/apex/VFP_CreateReturnItem'
           if(NewRMA.TEX__c != null)
           {
               String SvURL = '/apex/VFP_CreateReturnItem?TEXOverrideFlag='+relatedTex.ForceManualSelection__c;
               aNewRMAEditPage.getParameters().put(Label.CLDEC12RR02, SvURL  );
           }
           else
           {
           aNewRMAEditPage.getParameters().put(Label.CLDEC12RR02, Label.CLDEC12RR01 );
           }
        //*** RELEASE DEC12 - END********************************************************//
        
        return aNewRMAEditPage;        
        
        
                                                        
    }
    
    Public Pagereference doCancel()
    {
         /*   
         string CaseId = relatedCase.Id;
         Pagereference pr = new PageReference('/'+ CaseId);
         pr.setRedirect(true);
         return pr;
        */
         Pagereference pr ;
         string CaseId = relatedCase.Id;
         if(NewRMA.TEX__c!=null)
         {
          String TEXId = NewRMA.TEX__c;
           pr = new PageReference('/'+ TEXId);
            pr.setredirect(true);
             return pr;
         }
         if(relatedCase.Id!=null)
         {
         pr = new PageReference('/'+ CaseId);
         pr.setredirect(true);
         return pr;
         }
         else
         {
         pr = new PageReference(label.CLDEC12RR35);
         //CLDEC12RR35 - /a3T/o
         pr.setredirect(true);
         return pr;
         }
    }
    
    
                 
}