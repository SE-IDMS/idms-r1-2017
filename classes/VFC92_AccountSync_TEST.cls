@isTest
/*
Author:Siddharatha Nagavarapu
Description: Test class for the class VFC92_AccountSync
*/
private class VFC92_AccountSync_TEST {
  static testMethod void VFC92_AccountSync(){
  System.debug('#### START test method for VFC92_AccountSync ####');  
  Account acc=Utils_TestMethods.createAccount(UserInfo.getUserId());
  insert acc;
  VFC92_AccountSync vfCtrl = new VFC92_AccountSync(new ApexPages.StandardController(acc));
  vfCtrl.redirectToMW();
  
  acc.SEAccountID__c='12345';
  acc.AccountStatus__c=System.Label.CL00819;
  update acc;
  
  vfCtrl = new VFC92_AccountSync(new ApexPages.StandardController(acc));
  vfCtrl.redirectToMW();
  
  
  acc.SEAccountID__c=null;
  acc.AccountStatus__c=System.Label.CL00819;
  update acc;
  
  vfCtrl = new VFC92_AccountSync(new ApexPages.StandardController(acc));
  vfCtrl.redirectToMW();
  
  System.debug('#### End test method for VFC92_AccountSync ####');  
  }
}