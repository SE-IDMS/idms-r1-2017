public class VFC_MassUpdateContainmentAction 
{
    private final List<ContainmentAction__c> objs;
    private final ApexPages.StandardSetController cntr;
    private final PageReference fromPage;
    public boolean toDisplay {get;set;}
    public boolean isError{get;set;}
    public boolean Error{get;set;}
    public boolean goback{get;set;}
    public boolean OverdueFlag{get;set;}
    private transient ApexPages.Message currentMsg;
    public String ErrorMsg {get; set;}
     public ContainmentAction__c conAction { get; set; } 
    list<containmentaction__c> descriptionlist=new list<containmentaction__c>();
    list<containmentaction__c> temp=new list<containmentaction__c>();
    list<containmentaction__c> templist=new list<containmentaction__c>();
   
    string convertedstring;
    list<id> idlist=new list<id>();
    
    Public String Videolink {get; set;}
    
    public VFC_MassupdateContainmentAction (ApexPages.StandardSetController controller)
    {   APsPresent = false;
        Prbid = ApexPages.currentPage().getParameters().get('id');
		lstWrapperClass = new List<WrapperClass>();
            System.debug('!!!! Prbid : ' + Prbid);    
            for(CommercialReference__c AP : [SELECT Id, Problem__c, DateCodesSerialNumber__c , ProductLine__c, CommercialReference__c, ProductFamily__c, Family__c, GMRCode__c FROM CommercialReference__c WHERE Problem__c =: Prbid])  {   
                lstWrapperClass.add(new WrapperClass(AP)) ;   
            }    
            System.debug('!!!! lstWrapperClass : ' + lstWrapperClass);
			if(!lstWrapperClass.isEmpty())
				APsPresent = true;
				
				
        cntr = (ApexPages.StandardSetController)controller;
        conAction = (ContainmentAction__c)controller.getRecord(); 
        // OCT 15 RELEASE -- BR-7364 -- Divya M --
    conAction.Shipments__c=Label.CLOCT15I2P25; //CLOCT15I2P25 = 'Not Affected'; 
    conAction.OrderIntake__c=Label.CLOCT15I2P25; 
    conAction.Production__c=Label.CLOCT15I2P25;
    conAction.OtherTemporaryActions__c=Label.CLOCT15I2P26;//CLOCT15I2P26 = 'Not Required'; 
    conAction.StockSortQuarantine__c=Label.CLOCT15I2P26; 
    conAction.StockRework__c=Label.CLOCT15I2P26; 
    conAction.StockScrap__c=Label.CLOCT15I2P26; 
    conAction.StockReturn__c=Label.CLOCT15I2P26; 
    conAction.OtherPermanentActions__c=Label.CLOCT15I2P26;
    conAction.TECH_IsChanged__c = false;
    //Ends OCt 15 Rel
        fromPage = cntr.cancel();
        OverdueFlag = False;
        if (this.objs == null)
        {
            this.objs = (List<ContainmentAction__c>)controller.getSelected();
            system.debug('--->'+objs);
        }
        if (getRecordSize()<1)
        {
            toDisplay=false;
            Error=true;
            goback=true;
        } 
        else 
        {
            String msg = Label.CLMAY13I2P21 + getRecordSize(); // 'Number of records selected for update:'
            currentMsg = new ApexPages.Message(ApexPages.severity.INFO, msg);
            toDisplay=true;
            isError=false;
            goback=false;
            ApexPages.addMessage(currentMsg);
            
        } 
     
       for(containmentaction__c list2:objs)
        {
       idlist.add(list2.id);
        }
      
      descriptionlist=[select description__c, Overdue__c from containmentaction__c where id =:idlist];
      system.debug('descriptionlist contains-->'+descriptionlist);
      
      
      If(descriptionlist.size()>0 || descriptionlist!=null)
      {
          for(containmentaction__c CNA:descriptionlist)
          {
          if(CNA.Overdue__c!=null)
            {
               OverdueFlag = True;
            }
          }
      }
    
   }
   
   public integer getRecordSize()
   {
     if (objs!=null) 
     {
        return objs.size();
     } 
     else 
     {
        return 0;
     }
   }
   
  public String VideoLink(){
       
       String link = Label.CLAPR15I2P04;      
       System.debug('*** link');
       return link;
   }
    
   public String valueToUpdate {
        get;
        set;    
    }
   //this function is used for updating the description field of selected records from containment action
   
   public pagereference updateContainmentAction()
      { 
        OverdueFlag = False;
        
        
        // Added by Uttara - Notes2bFO - Q2 2016 Release
        
        System.debug('!!!! functions : ' + functions);       
        
        countMsg = false;
        for(WrapperClass wc : lstWrapperClass) {
                if(wc.checkbox == true)
                    count++;    
        }
        System.debug('!!!! count : ' + count);
        
        if(functions == 'None' && count > 0) {
            countMsg = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select the Action to be performed.'));  
            return null;  
        }
        else if(functions == 'Add To XA(s)') {
            
            Map<Id,Set<String>> mapXAGMRcode = new Map<Id,Set<String>>();
            Set<String> temp ;
            List<XAAffectedProduct__c> lstXAAP = new List<XAAffectedProduct__c>();  
            XAAffectedProduct__c xaap1 ;
            
            for(XAAffectedProduct__c xaap : [SELECT ContainmentAction__c, GMRCode__c, CommercialReference__c FROM XAAffectedProduct__c WHERE ContainmentAction__c IN: idlist]) {
                temp = new Set<String>();
                String unique = xaap.GMRCode__c + xaap.CommercialReference__c;
                temp.add(unique);
                if(mapXAGMRcode.containsKey(xaap.ContainmentAction__c))
                    mapXAGMRcode.get(xaap.ContainmentAction__c).add(unique);
                else
                    mapXAGMRcode.put(xaap.ContainmentAction__c,temp);
            }
            
            for(WrapperClass wc : lstWrapperClass) {
                if(wc.checkbox == true) {
                    for(Id conId : idlist) {
                        if(!mapXAGMRcode.isEmpty()) {
                            String unique = wc.selectedAffProd.GMRCode__c + wc.selectedAffProd.CommercialReference__c;
                            if(!mapXAGMRcode.get(conId).contains(unique)) {
                                xaap1 = new XAAffectedProduct__c();
                                xaap1.AffectedProduct__c = wc.selectedAffProd.Id;
                                xaap1.ContainmentAction__c = conId;
                                lstXAAP.add(xaap1);
                            }
                        }
                        else {
                            xaap1 = new XAAffectedProduct__c();
                                xaap1.AffectedProduct__c = wc.selectedAffProd.Id;
                                xaap1.ContainmentAction__c = conId;
                                lstXAAP.add(xaap1);                     
                        }
                    }
                }                   
            }
            
            System.debug('!!!! lstXAAP : ' + lstXAAP);
            if(!lstXAAP.isEmpty())
                INSERT lstXAAP;         
        }
        
        else if(functions == 'Delete From XA(s)') { 
        
            Set<String> GMRcodes = new Set<String>();
            List<XAAffectedProduct__c> lstXAAP = new List<XAAffectedProduct__c>();  
            
            for(WrapperClass wc : lstWrapperClass) {
                if(wc.checkbox == true) { 
                    String unique = wc.selectedAffProd.GMRCode__c + wc.selectedAffProd.CommercialReference__c;
                    GMRcodes.add(unique);
                }    
            }
            for(XAAffectedProduct__c xaap : [SELECT ContainmentAction__c, GMRCode__c, CommercialReference__c FROM XAAffectedProduct__c WHERE ContainmentAction__c IN: idlist]) {
                String unique = xaap.GMRCode__c + xaap.CommercialReference__c;
                if(GMRcodes.contains(unique)) 
                    lstXAAP.add(xaap);
            }
            if(!lstXAAP.isEmpty())
                DELETE lstXAAP;
        }
        
        // Ends - Uttara
        
        if(valuetoupdate!='')
        {  
        for(containmentaction__c temp:descriptionlist)
        {
       
        
        system.debug('temp contains:'+temp);
        if(temp.description__c!=null)
        {
        convertedstring=valuetoupdate +'\n'+ label.CLMAY13I2P01 + ' ' +Userinfo.getName() +' on ' + datetime.now() + label.CLMAY13I2P02 +'\n' +'\n' + temp.description__c ;
        }
        else
        {
        convertedstring=valuetoupdate +'\n'+ label.CLMAY13I2P01 + ' ' +Userinfo.getName() +' on ' + datetime.now() + label.CLMAY13I2P02;
        }
        system.debug('converted string contains-->'+convertedstring); 
        temp.description__c=convertedstring;
        system.debug('---->>'+temp.description__c);
        //Oct 15 Release -- BR-7364  -- DIvya M 
        system.debug('Entered inside on click of Update button');
        system.debug('Record Type is ==>'+conAction.RecordType.DeveloperName+'==shipment=='+conAction.Shipments__c+'== orderIntake=='+conAction.OrderIntake__c+'==production=='+conAction.Production__c+'==Other Temp Actions=='+conAction.OtherTemporaryActions__c);
        if(conAction.TECH_IsChanged__c==true && conAction.Shipments__c==Label.CLOCT15I2P25 && conAction.OrderIntake__c==Label.CLOCT15I2P25 && conAction.Production__c == Label.CLOCT15I2P25 && conAction.OtherTemporaryActions__c == Label.CLOCT15I2P26 && conAction.StockSortQuarantine__c ==Label.CLOCT15I2P26 && conAction.StockRework__c==Label.CLOCT15I2P26 && conAction.StockScrap__c==Label.CLOCT15I2P26 && conAction.StockReturn__c==Label.CLOCT15I2P26 && conAction.OtherPermanentActions__c==Label.CLOCT15I2P26){//CLOCT15I2P25 = 'Not Affected' and CLOCT15I2P26 = 'Not Required'; 
            IsError=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLOCt15I2P28));// CLOCt15I2P28='As you checked that there is change in the actions, At least one type of containment action must be requested.'  
            return null;
        }else{
            if(conAction.Shipments__c!=Label.CLOCT15I2P25)
         temp.Shipments__c = conAction.Shipments__c;
         if(conAction.OrderIntake__c!=Label.CLOCT15I2P25)
         temp.OrderIntake__c = conAction.OrderIntake__c;
         if(conAction.Production__c != Label.CLOCT15I2P25)
         temp.Production__c = conAction.Production__c;
         if(conAction.OtherTemporaryActions__c != Label.CLOCT15I2P26)
         temp.OtherTemporaryActions__c = conAction.OtherTemporaryActions__c;
         if(conAction.StockSortQuarantine__c !=Label.CLOCT15I2P26)
         temp.StockSortQuarantine__c = conAction.StockSortQuarantine__c;
         if(conAction.StockRework__c!=Label.CLOCT15I2P26)
         temp.StockRework__c = conAction.StockRework__c;
         if(conAction.StockScrap__c!=Label.CLOCT15I2P26)
         temp.StockScrap__c = conAction.StockScrap__c;
         if(conAction.StockReturn__c!=Label.CLOCT15I2P26)
         temp.StockReturn__c = conAction.StockReturn__c;
         if(conAction.OtherPermanentActions__c!=Label.CLOCT15I2P26)
         temp.OtherPermanentActions__c = conAction.OtherPermanentActions__c;
         }
         //Ends
        templist.add(temp);
        }
        
        update templist;
        }
        else if(valuetoupdate=='')
        {
        IsError=true;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.CLMAY13I2P08));  
        return null;
        }
        
        
        
        return frompage;
      }
       
       //This function is used to cancel the process and return back to Problem Page
       public PageReference cancelContainmentAction()
       {
        return fromPage;
       }
       
        // Added by Uttara - Notes2bFO - Q2 2016 Release
    
        public List<WrapperClass> lstWrapperClass {get; set;}
        public List<XAAffectedProduct__c> lstxaAP {get; set;}
        public XAAffectedProduct__c xaaf {get; set;}
        public String Prbid {get; set;}
        public String functions {get; set;}
        public Boolean countMsg {get; set;}
        public Integer count = 0;
		public Boolean APsPresent {get; set;}
        
        
        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','None'));
            options.add(new SelectOption('Add to XA(s)','Add to XA(s)'));
            options.add(new SelectOption('Delete From XA(s)','Delete From XA(s)'));
            return options;
        }
            
        public List<WrapperClass> getDisplayAffectedProducts() {
            
            return lstWrapperClass;
        }
        
        public class WrapperClass {
            public CommercialReference__c selectedAffProd {get; set;}
            public Boolean checkbox {get; set;}
            
            //constructor
            public WrapperClass(CommercialReference__c singleAP) {
                selectedAffProd = singleAP;
                checkbox = false;
            }
        }
}