@isTest
private class VFC36_CaseOrderController_TEST
{
    static testMethod void VFC36_CaseOrderControllerTestMethod()
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= [Select Id, Name from Country__c Limit 1].Id;
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
        
        ApexPages.StandardController Case1StandardController = new ApexPages.StandardController(case1);
        
        VFC36_CaseOrderController CaseOrderController = new VFC36_CaseOrderController(Case1StandardController);
        
        VCC07_OrderConnectorController OrderComponentController = new VCC07_OrderConnectorController();
        OrderComponentController.OrderMethods = CaseOrderController.CaseImplementation;
        OrderComponentController.StandardObjectID = Case1.Id;
    
        OrderComponentController.getInit();   
        OrderComponentController.SelectedOrder = new OPP_OrderLink__c();     
        OrderComponentController.Action();
        OrderComponentController.cancel();
        
    }
}