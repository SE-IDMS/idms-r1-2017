// Created by Uttara PR - Nov 2016 Release - Escape Notes project

Global with sharing class VFC_GetStockDetails {

    //Variables
    public String PrbId {get; set;}  // current Problem record Id 
    public List<BackOfficesystem__c> lstBOS {get; set;} // Back Office Systems of the Accountable Organization of the current Problem
    public List<CommercialReference__c> lstAP {get; set;} // selected Affected Products
    public Problem__c Prb {get; set;} // current Problem record
    public Boolean showBOselection {get; set;} // boolean used to display the Back Office System picklist
    public Boolean noBOpresent {get; set;} // boolean used to display message when no Back Office present
    public Boolean noAPpresent {get; set;} // boolean used to display message when no Affected Product selected
    public Integer countFamily {get; set;} // Integer to count the number of Family level Affected Products selected
    public Boolean Familyselected {get; set;} // boolean used to display message when a Family level Affected Product is selected
    public Boolean StockDisplayTable {get; set;} // boolean used to display the Stock Display Table
    public List<StockDisplayWrapper> lstSDWrapper {get; set;} // list of wrapper class used for displaying data in table on page
    public Boolean moreAPselected {get; set;} // Boolean to restrict number of Affected Products selected
    public RequestStockDetails requestParameters = new RequestStockDetails(); // It stores all the parameters that need to be sent with the Request
    public ResponseObtained ResponseResult = new ResponseObtained(); // It stores all the response obtained
    public Boolean noResult {get; set;} // Boolean used to display the error message when there is no result from IFW
    public List<String> lstOrgIds = new List<String>();
    public List<BackOfficesystem__c> lstBOS1 = new List<BackOfficesystem__c>();
    public Map<String,BackOfficesystem__c> MapExtIdBOS = new Map<String,BackOfficesystem__c>();
    public Map<String,CommercialReference__c> MapComRefAP = new Map<String,CommercialReference__c>();
    
    public VFC_GetStockDetails(ApexPages.StandardSetController controller) {
        
        PrbId = ApexPages.currentPage().getParameters().get('Id'); // Getting Id of the current Problem record
        System.debug('!!!! PrbId : ' + PrbId);
        
        noResult = false;
        
        if(PrbId != '' && PrbId != null) {
            Prb = [SELECT AccountableOrganization__c FROM Problem__c WHERE Id =: PrbId];   // Getting Accountable Organization of the current Problem     
            System.debug('!!!! Prb : ' + Prb);
        }
        
        if(Prb != null) {
        
            // Getting the Back Office System of the Accountable Organization of the current Problem 
            
            lstBOS = [SELECT BackOfficeSystem__c, ExternalId__c FROM BackOfficesystem__c WHERE FrontOfficeOrganization__c =: Prb.AccountableOrganization__c];
            System.debug('!!!! lstBOS : ' + lstBOS);
            if(lstBOS.isEmpty()) { 
                noBOpresent = true;
                showBOselection = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P01));
            }
            else {
                showBOselection = true;
                noBOpresent = false;
            }
            
            // Getting the selected Affected Products of the current Problem
                        
            if (this.lstAP == null) {
                this.lstAP = (List<CommercialReference__c>)controller.getSelected();
                System.debug('!!!! lstAP : ' + lstAP);
                
                Familyselected = false;             
                countFamily = 0;
                
                //Initialising all the values for the inner classes
                XREF xreff = new XREF();
                xreff.skuSource = Label.CL112016I2P29; //GMR
                xreff.skuTarget = Label.CL112016I2P30; //APC-MGEUS
                
                //Checking if any Family Level Affected Product is selected
                lstAP = [SELECT CommercialReference__c FROM CommercialReference__c WHERE Id IN: lstAP];
                for(CommercialReference__c cr : lstAP) {
                    
                    if(cr.CommercialReference__c == '' || cr.CommercialReference__c == null)
                        countFamily++;  
                    
                    // Request data setup
                    ReqProduct p = new ReqProduct();
                    p.sku = cr.CommercialReference__c;
                    p.xref = xreff;
                    requestParameters.product.add(p);  
                    requestParameters.targetERP = Label.CL112016I2P19; // CL112016I2P19 = 'OracleERP'
                }
                System.debug('!!!! requestParameters : ' + requestParameters);
                
                System.debug('!!!! countFamily : ' + countFamily);
                if(countFamily > 0) {
                    Familyselected = true;  
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P03));      
                }           
            }
            moreAPselected = false;
            if (lstAP.size() < 1) {
                noAPpresent = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P02));
            }
            else if(lstAP.size() > Integer.valueOf(Label.CL112016I2P06)) {
                moreAPselected = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P07));
            }
            else
                noAPpresent = false;
                
            StockDisplayTable = false;
           
        }
    }
    
    public void Confirm() { 
       
        showBOselection = false;
        
        String accessToken = Label.CL112016I2P20; // CL112016I2P20 = 'ad223e6d06938489aa2f29f74d0e4457';
        Http h1 = new Http();
        
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(System.Label.CL112016I2P11);
        req1.setHeader('Authorization','Bearer '+ accessToken);
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Accept',System.Label.CLOCT15FIO018);
        req1.setHeader(System.Label.CLOCT15FIO043,System.Label.CLOCT15FIO044);
        req1.setHeader(System.Label.CLOCT15FIO045,System.Label.CLOCT15FIO046);
        req1.setHeader(System.Label.CLOCT15FIO047,System.Label.CLOCT15FIO048);
        req1.setHeader(System.Label.CLOCT15FIO049,System.Label.CLOCT15FIO048);
        req1.setTimeout(120000);
        req1.setMethod('POST');
        req1.setBody(JSON.serialize(requestParameters));
        
        System.debug('!!!! JSON.serialize(requestParameters) : ' + JSON.serialize(requestParameters));
        System.debug('!!!! req1 : ' + req1);
        
        HttpResponse res1 = new HttpResponse();
        res1 = h1.send(req1);
        String responseReturned = ''; // String which stores the response from the REST API in JSON form
        responseReturned = res1.getBody();
        System.debug('!!!! responseReturned : ' + responseReturned);
        
        //responseReturned = '{    "targetERP": "OracleERP",    "product":  [   {        "sku": "BF500-FR",        "error": {"message": "No stock found in Oracle"}    }]}  ';      
        
        if(!Test.isRunningTest() && (responseReturned != null || responseReturned != '')) {
            
            ResponseResult = (ResponseObtained)JSON.deserialize(responseReturned, ResponseObtained.class);
            
            System.debug('!!!! ResponseResult json deserialized : ' + ResponseResult);
            System.debug('!!!! ResponseResult.product : ' + ResponseResult.product);
            
            lstSDWrapper = new List<StockDisplayWrapper>();
            
            if(ResponseResult.product != null) {
                for(Product p : ResponseResult.product) {
                    lstSDWrapper.add(new StockDisplayWrapper(p));                
                }
                System.debug('!!!! lstSDWrapper.size() : ' + lstSDWrapper.size());
            }
        } 
        else {            
            noResult = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P23));
        }
        
        if(lstSDWrapper != null) {
            for(StockDisplayWrapper sdw : lstSDWrapper) {
                if(sdw.prod.error == null)
					lstOrgIds.add(sdw.prod.organization.id);                
            } 
            
        	lstBOS1 = [SELECT ExternalId__c, FrontOfficeOrganization__r.Name FROM BackOfficesystem__c WHERE ExternalId__c =: lstOrgIds];
            for(BackOfficesystem__c bos : lstBOS1) 
                MapExtIdBOS.put(bos.ExternalId__c, bos);
                
            System.debug('!!!! lstSDWrapper : ' + lstSDWrapper);
            System.debug('!!!! lstBOS1 : ' + lstBOS1);
            
            if(!lstBOS1.isEmpty()) {
                for(StockDisplayWrapper sdw : lstSDWrapper) {
                    if(sdw.prod.error == null) {
                        if(MapExtIdBOS.containsKey(sdw.prod.organization.id))
                            sdw.prod.organization.FOOrg = MapExtIdBOS.get(sdw.prod.organization.id).FrontOfficeOrganization__r.Name;
                    }
                }
            }
            
            System.debug('!!!! lstSDWrapper : ' + lstSDWrapper);
            StockDisplayTable = true;
        }
        else {            
            noResult = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P23));
        }
    }
    
    public PageReference AddImpactedOrganizations() {
        
        List<ShipHoldRequest__c> lstSHR = new List<ShipHoldRequest__c>();
        
        for(CommercialReference__c ap : lstAP)
            MapComRefAP.put(ap.CommercialReference__c, ap);
        
        if(lstSDWrapper != null) {
            for(StockDisplayWrapper sdw : lstSDWrapper) {                
                if(sdw.selected == true) {   
                    ShipHoldRequest__c shr = new ShipHoldRequest__c();
                    shr.Problem__c = PrbId;
                    if(MapComRefAP.containsKey(sdw.prod.sku))
                        shr.AffectedProduct__c = MapComRefAP.get(sdw.prod.sku).Id;
                    if(sdw.prod.error == null) {
                        if(MapExtIdBOS.containsKey(sdw.prod.organization.id))
                    		shr.BackOfficeSystem__c = MapExtIdBOS.get(sdw.prod.organization.id).Id;
                    	shr.BackOfficeCode__c = sdw.prod.organization.code;
                    }    
                    lstSHR.add(shr);
                }            
            }
        }    
        System.debug('!!!! lstSHR : ' + lstSHR);
        Database.insert(lstSHR,false);
        
        PageReference prbpage = new PageReference('/'+PrbId);
        return prbpage;
    }
    
    public PageReference Cancel() {
        
        PageReference cancelpage = new PageReference('/'+PrbId);
        return cancelpage;
    }
    
    public class StockDisplayWrapper {
        
        public Boolean selected {get; set;}
        public Product prod {get; set;}
        
        public StockDisplayWrapper(Product prod1) {
            selected = false;
            prod = prod1;
        }
    }
    
    // Request Schema

    global class ReqProduct {
    
        public String sku;
        public XREF xref;    
    }
    
    global class XREF {
        
        public String skuSource;
        public String skuTarget;
    }
    
    global class RequestStockDetails {
        
        public List<ReqProduct> product = new List<ReqProduct>();
        public String targetERP;        
    }
    
    // Response Schema
    
    global class Error {
        
        public String message {get; set;}
        
        public Error(String msg) {
            message = msg;
        }
    }
    
    global class Organization {
        
        public String code {get; set;}
        public String id {get; set;} 
        public String type {get; set;}
        public String FOOrg {get; set;}
        
        public Organization(String code1, String id1, String type1, String FOOrg1) {
            code = code1;
            id = id1;
            type = type1;
            FOOrg = FOOrg1;
        }
    }
    
    global class Stock {
        
        public String name {get; set;}
        public String value {get; set;}
        
        public Stock(String name1, String value1) {
            name = name1;
            value = value1;
        } 
    }
    
    global class Product {
        
        public String sku {get; set;}
        public String targetSKU {get; set;}
        public Organization organization {get; set;}
        public List<Stock> Stock {get; set;}   
        public Error error {get; set;}
        
        public Product( String sku1, String targetSku1, Organization org1, List<Stock> lstStock1, Error err1) {
            sku = sku1;
            targetSku = targetSku1;
            organization = org1;
            Stock = lstStock1;
            error = err1;
        }
    }
    
    global class ResponseObtained {
        
        public List<Product> product {get; set;}   
        public String targetERP {get; set;}
        
        public ResponseObtained(List<Product> prod1, String targetERP1) {
            product = prod1;
            targetERP = targetERP1;
        }
        
        public ResponseObtained() {}
    }
}