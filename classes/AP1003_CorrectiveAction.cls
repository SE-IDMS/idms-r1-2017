public class AP1003_CorrectiveAction{
    public static void populateCorrectiveActionOwner(List <CorrectiveAction__c> correctiveActionList){
    //*********************************************************************************
        // Method Name      : populateCorrectiveActionOwner
        // Purpose          : To populate Corrective Action Owner from Accountable Organization 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1003_CorrectiveAction  populateCorrectiveActionOwner  Started****');
        
        //Variable Declaration Section
        Map<ID,EntityStakeholder__c[]>  stakeholderMap = new Map<Id,EntityStakeholder__c[]>();
        Map<ID,String>  orgRoleMap = new Map<Id,String>();
        
        
        for(CorrectiveAction__c crtveActn : correctiveActionList){
            if(crtveActn.AccountableOrganization__c != null)
                orgRoleMap.put(crtveActn.AccountableOrganization__c,Label.CL10056); 
        }
        stakeholderMap = Utils_P2PMethods.retrieveOrganizationUserDetails(orgRoleMap);
        
        for(CorrectiveAction__c crtveActn : correctiveActionList){
            if(crtveActn.AccountableOrganization__c != null)
                if(stakeholderMap.containsKey(crtveActn.AccountableOrganization__c))
                crtveActn.Owner__c = stakeholderMap.get(crtveActn.AccountableOrganization__c)[0].User__c;   
        }
        
        System.Debug('****AP1003_CorrectiveAction  populateCorrectiveActionOwner  Finished****');
    }
    
    public static void addCorrectiveOwnerToProblemShare(List <CorrectiveAction__c> correctiveActionList){
        //*********************************************************************************
        // Method Name      : addCorrectiveOwnerToProblemShare
        // Purpose          : To add Corrective Action Owner to Problem Share Object 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        System.Debug('****AP1003_CorrectiveAction addCorrectiveOwnerToProblemShare  Started****');
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        
        for(CorrectiveAction__c crtveActn: correctiveActionList){
            prblmShareList.add(Utils_P2PMethods.createProblemShare(crtveActn.RelatedProblem__c,crtveActn.Owner__c,Label.CL10067));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareList);
        System.Debug('****AP1003_CorrectiveAction addCorrectiveOwnerToProblemShare  Finished****');
    }
    
    public static void deleteCorrectiveOwnerFromProblemShare(Map<Id,CorrectiveAction__c> correctiveActionOldMap){
        //*********************************************************************************
        // Method Name      : deleteCorrectiveOwnerFromProblemShare
        // Purpose          : Delete Sharing for Corrective Action owner when Problem Corrective Action Owner is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1003_CorrectiveAction deleteCorrectiveOwnerFromProblemShare Started****');
        Set<Id> prblmId = new Set<id>();
        Set<Id> crtveActionOwnerId = new Set<id>();
        List<CorrectiveAction__c> oldCorrectiveActionList = correctiveActionOldMap.values();
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        Map <String, Problem__Share> prblmShareMap = new Map<String, Problem__Share>();
        List<Problem__Share> prblmShareListForDelete = new List<Problem__Share>();
        for(CorrectiveAction__c crtveActn: oldCorrectiveActionList){
            prblmId.add(crtveActn.RelatedProblem__c);
            crtveActionOwnerId.add(crtveActn.Owner__c);
        }

        prblmShareList = [Select Id, ParentId, UserOrGroupId, AccessLevel,RowCause from  Problem__Share where ParentId IN:prblmId 
                                                                                                            AND UserOrGroupId IN: crtveActionOwnerId  AND RowCause=:Label.CL10067];
        for (Problem__Share prblmShare:prblmShareList ){
        prblmShareMap.put(String.valueOf(prblmShare.ParentId) + String.valueOf(prblmShare.UserOrGroupId) ,prblmShare);
        }
        
        //Delete the record from Problem Share only, if the Owner is not present in any other Corrective Action
        //The Map "otherCorrectiveActionMap" will get all the other Corrective Action associated with the Problem for this validation. 
		Map<String,CorrectiveAction__c> otherCorrectiveActionMap= new Map<String,CorrectiveAction__c>();
		for (CorrectiveAction__c  otherCorrectiveAction : [Select Id, RelatedProblem__c, Owner__c from  CorrectiveAction__c where RelatedProblem__c IN:prblmId 
                                                                                                        AND Id NOT IN: correctiveActionOldMap.KeySet()]) {
			otherCorrectiveActionMap.put(String.valueOf(otherCorrectiveAction.RelatedProblem__c) + String.valueOf(otherCorrectiveAction.Owner__c),otherCorrectiveAction);
		}
		
        for(CorrectiveAction__c crtveActn: oldCorrectiveActionList){
            if(prblmShareMap.containsKey(String.valueOf(crtveActn.RelatedProblem__c) + String.valueOf(crtveActn.Owner__c))){
                if(!(otherCorrectiveActionMap.containsKey(String.valueOf(crtveActn.RelatedProblem__c) + String.valueOf(crtveActn.Owner__c)))){
                	prblmShareListForDelete.add(prblmShareMap.get(String.valueOf(crtveActn.RelatedProblem__c) + String.valueOf(crtveActn.Owner__c)));
                }
            }
        }
        if(prblmShareListForDelete.size()>0){
            Utils_P2PMethods.deletePrblmShare(prblmShareListForDelete);
        }

        System.Debug('****AP1003_CorrectiveAction deleteCorrectiveOwnerFromProblemShare Finished****');
    }
    public static void updateCorrectiveOwnerToProblemShare(Map<Id,CorrectiveAction__c> correctiveActionOldMap,Map<Id,CorrectiveAction__c> correctiveActionNewMap){
        //*********************************************************************************
        // Method Name      : updateCorrectiveOwnerToProblemShare
        // Purpose          : To Update Sharing for Corrective Action Owner when Corrective Action Record is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1003_CorrectiveAction updateCorrectiveOwnerToProblemShare Started****');
        
        deleteCorrectiveOwnerFromProblemShare(correctiveActionOldMap);
         
        List<Problem__Share> prblmShareListForInsert = new List<Problem__Share>();
        List<CorrectiveAction__c> correctiveActionList =correctiveActionNewMap.values();
        for(CorrectiveAction__c crtveActn: correctiveActionList){
            prblmShareListForInsert.add(Utils_P2PMethods.createProblemShare(crtveActn.RelatedProblem__c,crtveActn.Owner__c,Label.CL10067));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareListForInsert);
        
        System.Debug('****AP1003_CorrectiveAction updateCorrectiveOwnerToProblemShare Finished****');
    }
}