@isTest(SeeAllData=true)

private class VFC_ServiceCertificateTemplate_Test {
    
     private static SVMXC__Service_Contract__c testsc;
    
     static testMethod void myUnitTest() {
   
          PageReference pageRef = Page.VFP_ServiceCertificateTemplate;
         Test.setCurrentPage(pageRef);
 
       Account acc = Utils_TestMethods.createAccount();
              acc.name='test account';
               insert acc;
                 Contact con=new contact();
                 con.lastname='test';
                 con.Firstname='test1';
                 insert con;

        
             testsc = new SVMXC__Service_Contract__c(
                                                         name='test contract',
                                                         SoldtoAccount__c=acc.id,
                                                         SoldToContact__c=con.id,
                                                         Address__c='test address',
                                                         PONumber__c='tp09');
            
         insert testsc;
         
          SVMXC__Installed_Product__c ip1= new SVMXC__Installed_Product__c(SVMXC__Company__c=acc.id);
                  insert ip1;
         
         
         
         
        system.currentPageReference().getParameters().put('RecordId', testsc.id);
         
         SVMXC__Service_Contract__c sline1 = new SVMXC__Service_Contract__c();
         sline1.ParentContract__c =testsc.id;
         sline1.SVMXC__Active__c=true;
         insert sline1;
         SVMXC__PM_Plan__c pm1= new SVMXC__PM_Plan__c(SVMXC__Service_Contract__c=sline1.id,SVMXC__End_Date__c=system.today()+1,SVMXC__Start_Date__c=date.today());
         insert pm1;
         SVMXC__PM_Schedule_Definition__c pmsch1= new SVMXC__PM_Schedule_Definition__c(SVMXC__PM_Plan__c=pm1.id,SVMXC__Frequency__c=12,SVMXC__Frequency_Unit__c='Months');
         insert pmsch1;
         SVMXC__PM_Coverage__c pmcover1=new SVMXC__PM_Coverage__c(SVMXC__PM_Plan__c=pm1.id,SVMXC__Product_Name__c=ip1.id);
         insert pmcover1;
         
 
         ApexPages.StandardController controller = new ApexPages.StandardController(testsc); 
         VFC_ServiceCertificateTemplate  service = new VFC_ServiceCertificateTemplate(controller);
    //service.getkeyString();     
    service.getstrDispTyp();
    
     service.backMethod();
     service.getItems();
     service.setLanguage();
    service.getREPTAG001();
    service.getREPTAG002();
    service.getREPTAG0011();
    service.getREPTAG0012();
    service.getREPTAG0014();
    service.getREPTAG0015();
    service.getREPTAG0016();
    service.getREPTAG0017();
    service.getREPTAG0018();
    service.getREPTAG0019();
    service.getREPTAG0020();
    service.getREPTAG0021();
    service.getREPTAG0022();
    service.getREPTAG0023();
    service.getREPTAG0024();
    service.getREPTAG0025();
    service.getREPTAG0026();
      //   service.addcpRecList();
         
          
        
         }
         
     static testMethod void myUnitTest1() { 
     PageReference pageRef1 = Page.VFP_ServiceCertificateTemplate;
         Test.setCurrentPage(pageRef1);
         
          Account acc1 = Utils_TestMethods.createAccount();
              acc1.name='test account';
               insert acc1;
                 Contact con1=new contact();
                 con1.lastname='test';
                 con1.Firstname='test1';
                 insert con1;

        
              SVMXC__Service_Contract__c sc1= new SVMXC__Service_Contract__c(
                                                         name='test contract',
                                                         SoldtoAccount__c=acc1.id,
                                                         SoldToContact__c=con1.id,
                                                         Address__c='test address',
                                                         PONumber__c='tp09');
            
         insert sc1;
         
         
         
         SVMXC__Service_Contract__c sline = new SVMXC__Service_Contract__c();
         sline.ParentContract__c =sc1.id;
         sline.SVMXC__Active__c=true;
         insert sline;
        
          SVMXC__Installed_Product__c ip= new SVMXC__Installed_Product__c();
         ip.SVMXC__Company__c=acc1.id;
         insert ip;
     
        SVMXC__Service_Contract_Products__c cp= new SVMXC__Service_Contract_Products__c(SVMXC__Installed_Product__c=ip.id,SVMXC__Service_Contract__c=sline.id);
            insert cp;
         SVMXC__PM_Plan__c pm= new SVMXC__PM_Plan__c(SVMXC__Service_Contract__c=sline.id,SVMXC__End_Date__c=system.today()+1,SVMXC__Start_Date__c=date.today());
         insert pm;
         list<SVMXC__PM_Schedule_Definition__c> pmdef1= new list<SVMXC__PM_Schedule_Definition__c>();
         SVMXC__PM_Schedule_Definition__c pmsch= new SVMXC__PM_Schedule_Definition__c(SVMXC__PM_Plan__c=pm.id,SVMXC__Frequency__c=12,SVMXC__Frequency_Unit__c='Months');
         insert pmsch;
         SVMXC__PM_Schedule_Definition__c pmsch1= new SVMXC__PM_Schedule_Definition__c(SVMXC__PM_Plan__c=pm.id,SVMXC__Frequency__c=13,SVMXC__Frequency_Unit__c='Weeks');
         insert pmsch1;
         pmdef1.add(pmsch);
         pmdef1.add(pmsch1);
         SVMXC__PM_Coverage__c pmcover=new SVMXC__PM_Coverage__c(SVMXC__PM_Plan__c=pm.id,SVMXC__Product_Name__c=ip.id);
         insert pmcover;
         
         
         
        /* 
        //integer  pmdef2= VFC_ServiceCertificateTemplate.countPMVisit(pm,pmdef1);
        //
        ApexPages.StandardController controller11 = new ApexPages.StandardController(sc1); 
         VFC_ServiceCertificateTemplate  service22 = new VFC_ServiceCertificateTemplate(controller11);
        
         service22.countPMVisit(pm,pmdef1);
         Integer count = 0;
         Date startDate = system.today();
         Date endDate = pm.SVMXC__End_Date__c;*/
} 
 
}