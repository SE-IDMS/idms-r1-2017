global class Batch_PRMBadgeBFOProperties implements Database.Batchable<sObject>,Database.Stateful {
	
	String query;
    String objectName;
	List<Id> prmBadgeBFOPropertiesIdList;
	List<PRMBadgeBFOProperties__c> badgeListForAccount;
	List<PRMBadgeBFOProperties__c> badgeListForContact;
	List<PRMBadgeBFOProperties__c> badgeListForOpportunity;

	global Batch_PRMBadgeBFOProperties(String objectName,List<Id> prmBadgeBFOPropertiesIdList) {
		this.objectName = objectName;
		this.prmBadgeBFOPropertiesIdList = prmBadgeBFOPropertiesIdList;
		System.debug('prmBadgeBFOPropertiesIdList : ' + prmBadgeBFOPropertiesIdList);
		
        badgeListForAccount = new List<PRMBadgeBFOProperties__c>();
        badgeListForContact = new List<PRMBadgeBFOProperties__c>();
        badgeListForOpportunity = new List<PRMBadgeBFOProperties__c>();
        String fieldsToSelectAccount = 'PRMUIMSId__c';      
        String fieldsToSelectContact = 'PRMUIMSId__c,FieloEE__Member__c';
        String fieldsToSelectOpportunity = 'AccountId, Account.PRMUIMSId__c, Account.PRMCountry__c';
        

        String queryFilter = '';
        //System.debug([select Id,AllowRetroCalculation__c,Operator__c,PRMCountry__c,SubType__c,Value__c,ExternalPropertiesCatalog__r.TableName__c,ExternalPropertiesCatalog__r.FieldName__c,ExternalPropertiesCatalog__r.PRMCountryFieldName__c,ExternalPropertiesCatalog__r.SubType__c from PRMBadgeBFOProperties__c where id in:checkList]);
        for(PRMBadgeBFOProperties__c newPRMBadge:[select Id,Operator__c,PRMCountry__c,SubType__c,Value__c,ExternalPropertiesCatalog__r.TableName__c,ExternalPropertiesCatalog__r.FieldName__c,ExternalPropertiesCatalog__r.PRMCountryFieldName__c,ExternalPropertiesCatalog__r.SubType__c from PRMBadgeBFOProperties__c where id in:prmBadgeBFOPropertiesIdList]){
            if (newPRMBadge.ExternalPropertiesCatalog__r.TableName__c == PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME && objectName == PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME) {
                badgeListForAccount.add(newPRMBadge);
                if (!fieldsToSelectAccount.contains(newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c)){
                    fieldsToSelectAccount += ',' + newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c;
                }
                if (!fieldsToSelectAccount.contains(newPRMBadge.ExternalPropertiesCatalog__r.PRMCountryFieldName__c)){
                    fieldsToSelectAccount+= ',' + newPRMBadge.ExternalPropertiesCatalog__r.PRMCountryFieldName__c;
                }
                queryFilter+= ' OR ('+newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c+' like \'%'+newPRMBadge.Value__c+'%\' AND '+newPRMBadge.ExternalPropertiesCatalog__r.PRMCountryFieldName__c+' = \''+ newPRMBadge.PRMCountry__c+ '\')';

            } else if (newPRMBadge.ExternalPropertiesCatalog__r.TableName__c == PRM_FieldChangeListener.CONTACT_OBJECT_NAME && objectName == PRM_FieldChangeListener.CONTACT_OBJECT_NAME) {
                badgeListForContact.add(newPRMBadge);
                if (!fieldsToSelectContact.contains(newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c)){
                    fieldsToSelectContact += ',' + newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c;
                }
                if (!fieldsToSelectContact.contains(newPRMBadge.ExternalPropertiesCatalog__r.PRMCountryFieldName__c)){
                    fieldsToSelectContact+= ',' + newPRMBadge.ExternalPropertiesCatalog__r.PRMCountryFieldName__c;
                }
                queryFilter+= ' OR ('+newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c+' like \'%'+newPRMBadge.Value__c+'%\' AND '+newPRMBadge.ExternalPropertiesCatalog__r.PRMCountryFieldName__c+' = \''+ newPRMBadge.PRMCountry__c+ '\')';
            } else if (newPRMBadge.ExternalPropertiesCatalog__r.TableName__c == PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME && objectName == PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME) {
                badgeListForOpportunity.add(newPRMBadge);
                if (!fieldsToSelectOpportunity.contains(newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c)){
                    fieldsToSelectOpportunity += ',' + newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c;
                }
                queryFilter+= ' OR ('+newPRMBadge.ExternalPropertiesCatalog__r.FieldName__c+' like \'%'+newPRMBadge.Value__c+'%\' AND Account.PRMCountry__c = \''+ newPRMBadge.PRMCountry__c+ '\')';
            }
        }
        queryFilter+=')';

        if(objectName.equals(PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME)){
        	query = 'select '+fieldsToSelectAccount+ ' from Account where PRMUIMSId__c !=null AND (Id = NULL ' + queryFilter;
        }else if(objectName.equals(PRM_FieldChangeListener.CONTACT_OBJECT_NAME)){
        	query = 'select '+fieldsToSelectContact+ ' from Contact where PRMUIMSId__c !=null AND (Id = NULL ' + queryFilter;
        }else if(objectName.equals(PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME)){
        	query = 'select '+fieldsToSelectOpportunity+ ' from Opportunity where Account.PRMUIMSId__c !=null AND (Id = NULL ' + queryFilter;
        }
        System.debug('query : ' + query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
   		System.debug('#objectName:'+objectName);
   		if(objectName.equals(PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME)){
        	List<Account> accountList = (List<Account>)scope;  
        	for (Account acc : accountList) {
	            for (PRMBadgeBFOProperties__c badgeForAccount : badgeListForAccount) {
	                ExternalPropertiesCatalog__c epc = badgeForAccount.ExternalPropertiesCatalog__r;
	                Set<PRMBadgeBFOProperties__c> repository = new Set<PRMBadgeBFOProperties__c>();
	                //repository.add(badgeForAccount);
	                repository.add(badgeForAccount);
	                PRMBadgeBFOProperties__c accMatch = prmChangeListener.checkPropertyMatch(epc, repository, acc, null);
	                System.debug('accMach account ... : ' + accMatch);
	                if (accMatch!=null) {
	                    prmChangeListener.updateMatchList(true, new Contact(AccountId=acc.Id), badgeForAccount, PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME);
	                }
	            }   
	        }
        }else if(objectName.equals(PRM_FieldChangeListener.CONTACT_OBJECT_NAME)){
        	List<Contact> contactList = (List<Contact>)scope;  
        	for (Contact con : contactList) {
	            for (PRMBadgeBFOProperties__c badgeForContact : badgeListForContact) {
	                ExternalPropertiesCatalog__c epc = badgeForContact.ExternalPropertiesCatalog__r;
	                Set<PRMBadgeBFOProperties__c> repository = new Set<PRMBadgeBFOProperties__c>();
	                //repository.add(badgeForContact);
	                repository.add(badgeForContact);
	                PRMBadgeBFOProperties__c accMatch = prmChangeListener.checkPropertyMatch(epc, repository, con, null);
	                System.debug('accMach contact ... : ' + accMatch);
	                if (accMatch!=null) {
	                    prmChangeListener.updateMatchList(true, con, badgeForContact, PRM_FieldChangeListener.CONTACT_OBJECT_NAME);
	                }
	            }
	        }
        }else if(objectName.equals(PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME)){
        	List<Opportunity> oppList = (List<Opportunity>)scope;
        	for (Opportunity opp : oppList) {
	            for (PRMBadgeBFOProperties__c badgeForOpportunity : badgeListForOpportunity) {
	                ExternalPropertiesCatalog__c epc = badgeForOpportunity.ExternalPropertiesCatalog__r;
	                Set<PRMBadgeBFOProperties__c> repository = new Set<PRMBadgeBFOProperties__c>();
	                //repository.add(badgeForContact);
	                repository.add(badgeForOpportunity);
	                PRMBadgeBFOProperties__c accMatch = prmChangeListener.checkPropertyMatch(epc, repository, opp, opp.Account);
	                System.debug('accMach Opportunity ... : ' + accMatch);
	                if (accMatch!=null) {
	                    prmChangeListener.updateMatchList(true, new Contact(AccountId=opp.AccountId), badgeForOpportunity, PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME);
	                } 
	            }
	        }
        }
   		prmChangeListener.executeAction();
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}