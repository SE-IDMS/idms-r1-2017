/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC10200_Accept_TEST                                                               |
|                       |                                                                                    |
|     - Object(s)       | Case                                                                               |
|     - Description     |   - Test method for changing the Case Owner to the Active User from Detail View    |
|                       |   - Test method for changing the Case Owner to the Active User from List Views     |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | February, 24th 2012                                                                |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
private class VFC10200_Accept_TEST
{
    static testMethod void testVFC10200_Accept_Detail_1()
    {
       System.debug('#### START test method for testVFC10200_Accept_Detail_1 ####');

       Account accounts = Utils_TestMethods.createAccount();
       insert accounts;

       list<Contact> contacts = new list<contact>();

       for(integer i=0;i<200;i++)
       {
          contacts.add(Utils_TestMethods.createContact(accounts.Id, 'TestContact'+''+i));
       }
       database.insert(contacts);

       List<Case> cases1 = new List<Case>();

       for(integer i=0; i<100; i++)
       {
          Case cases = Utils_TestMethods.createCase(accounts.id, contacts[i].id, 'Open');
          cases1.add(cases);
       }
       database.insert(cases1);

       Pagereference p = Page.VFP10200_Accept_Detail;
       Test.setCurrentPageReference(p);

       // Add parameter to page URL
       ApexPages.currentPage().getParameters().put('Id', cases1[0].id);

       VFC10200_Accept caseAccept = new VFC10200_Accept(new ApexPages.StandardController(cases1[0]));
       caseAccept.acceptCurrentCase();

       System.debug('#### END   test method for testVFC10200_Accept_Detail_1 ####');
    }

    static testMethod void testVFC10200_Accept_Detail_2()
    {
       //Test 'error'
       System.debug('#### START test method for testVFC10200_Accept_Detail_2 ####');

       Account accounts = Utils_TestMethods.createAccount();
       insert accounts;

       Pagereference p = Page.VFP10200_Accept_Detail;
       Test.setCurrentPageReference(p);

       // Add parameter to page URL
       ApexPages.currentPage().getParameters().put('Id', accounts.id);

       VFC10200_Accept caseAccept = new VFC10200_Accept(new ApexPages.StandardController(accounts));
       caseAccept.acceptCurrentCase();

       System.debug('#### END   test method for testVFC10200_Accept_Detail_2 ####');
    }

    static testMethod void testVFC10200_Accept_List_1()
    {
       System.debug('#### START test method for VFC10200_Accept_List_1 ####');

       Account accounts = Utils_TestMethods.createAccount();
       insert accounts;

       list<Contact> contacts = new list<contact>();

       for(integer i=0;i<200;i++)
       {
          contacts.add(Utils_TestMethods.createContact(accounts.Id, 'TestContact'+''+i));
       }
       database.insert(contacts);

       List<Case> cases1 = new List<Case>();

       for(integer i=0; i<100; i++)
       {
          Case cases = Utils_TestMethods.createCase(accounts.id, contacts[i].id, 'Open');
          cases1.add(cases);
       }
       database.insert(cases1);

       List<Case> caseId = new List<Case>();

       for(Case aCase : cases1)
       {                   
           caseId.add(aCase);
       }           

       Pagereference p = new PageReference('/apex/VFP10200_Accept?retURL=%2F500%3Ffcf%3D00BK0000000Lo4G');
       Test.setCurrentPageReference(p);

       ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(caseid);
       sc1.setSelected(caseid); 

       VFC10200_Accept caseAccept = new VFC10200_Accept(sc1);
       caseAccept.acceptCases();

       System.debug('#### END   test method for VFC10200_Accept_List_1 ####');
    }

    static testMethod void testVFC10200_Accept_List_2()
    {
       System.debug('#### START test method for VFC10200_Accept_List_2 ####');

       Account accounts = Utils_TestMethods.createAccount();
       insert accounts;

       list<Contact> contacts = new list<contact>();

       for(integer i=0;i<200;i++)
       {
          contacts.add(Utils_TestMethods.createContact(accounts.Id, 'TestContact'+''+i));
       }
       database.insert(contacts);

       List<Case> cases1 = new List<Case>();

       for(integer i=0; i<100; i++)
       {
          Case cases = Utils_TestMethods.createCase(accounts.id, contacts[i].id, 'Open');
          cases1.add(cases);
       }
       database.insert(cases1);

       List<Case> caseId = new List<Case>();

       for(Case aCase : cases1)
       {                   
           caseId.add(aCase);
       }           

       Pagereference p = new PageReference('/apex/VFP10200_Accept?retURL=%2F500%3Ffcf%3D00BK0000000Lo4G');
       Test.setCurrentPageReference(p);

       ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(caseid);

       VFC10200_Accept caseAccept = new VFC10200_Accept(sc1);
       caseAccept.acceptCases();

       System.debug('#### END   test method for VFC10200_Accept_List_2 ####');
    }
}