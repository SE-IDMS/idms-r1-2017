public class AP32_UpdateCoachingCustomerVisitEvent {
    
    public static void updateCoachingCustomerEvent(List<CoachingVisit__c> CVstoUpdate)
    {
        //*********************************************************************************
        // Method Name      : updateCoachingCustomerEvent 
        // Purpose          :This is the public static method that is called from the trigger (CoachingVisitAfterInsertUpdate)
        // Created by       : Ramesh Rajasekaran - Global Delivery Team
        // Date created     : 9th November 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
    ///********************************************************************************/
    
    System.Debug('****AP32_UpdateCoachingCustomerVisitEvent updateCoachingCustomerEvent   Started****')  ;

    Map<Id,Id> mapCVisitEvent = new Map<Id,Id>();
    List<Event> lstCustomerVisits = new List<Event>();
    List<Event> updateEventList = new List<Event>();
    
    for(CoachingVisit__c customerVisitObj:CVstoUpdate){
        if(customerVisitObj.AssociatedCustomerVisit__c != null){    
           // mapCVisitEvent.put(customerVisitObj.AssociatedCustomerVisit__c ,customerVisitObj.Id);
           Id plainId=(customerVisitObj.AssociatedCustomerVisit__c.split('/d')[0]).split('salesforce.com/')[1];
           mapCVisitEvent.put(plainId,customerVisitObj.Id);
        }
    }
    
    system.debug('mapCVisitEvent--->'+mapCVisitEvent);
    
    if(mapCVisitEvent != null && mapCVisitEvent.size()>0){
        lstCustomerVisits = [Select AssociatedCoachingVisitId__c from Event where Id IN: mapCVisitEvent.keySet()];
    }
    system.debug('lstCustomerVisits--->'+lstCustomerVisits);
    
    if(lstCustomerVisits != null && lstCustomerVisits.size()>0)
        for(Event eventObj:lstCustomerVisits){
            if(mapCVisitEvent.containskey(eventObj.Id)) {
            string eventId = eventObj.Id+'';          
            eventObj.AssociatedCoachingVisitId__c = mapCVisitEvent.get(eventId.substring(0, 15));
            updateEventList.add(eventObj);
            }
        }
    
    system.debug('updateEventList--->'+updateEventList);
    
    if(updateEventList != null && updateEventList.size()>0){
        Database.saveResult []svr = database.update(updateEventList);
            for(Database.SaveResult svrt:svr){
                if(!svrt.isSuccess()){
                Database.Error err = svrt.getErrors()[0];
                System.Debug('######## Error Insert: '+err.getStatusCode()+' '+err.getMessage());              
                }
            }
        } 
    
    System.Debug('****AP32_UpdateCoachingCustomerVisitEvent updateCoachingCustomerEvent   Finished****')  ;     
    }
}