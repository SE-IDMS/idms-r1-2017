@isTest
public class VFC_PsGroupMemberAssignment_TEST {

    static testMethod void testPsGroupMemberAssignment() {
    
        User adminUser = [SELECT Id FROM User WHERE ProfileId = '00eA0000000uVHP' AND isActive = true LIMIT 1];
        
        System.runAs (adminUser) {
            Test.startTest();
            
            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController psGroupController     = new ApexPages.StandardController(new psGroupMember__c(PermissionSetGroup__c = permissionSetGroupList.get(0).Id));
            
            VFC_PsGroupMemberAssignment psGroupMemberController = new VFC_PsGroupMemberAssignment(psGroupController);
            
            PermissionSet selectedPs = [SELECT Id, Name, Label FROM PermissionSet LIMIT 1];
            psGroupMemberController.selectedPermissionSets.add(new SelectOption(selectedPs.Id, selectedPs.Label));
            
            psGroupMemberController.savePsGroupMember();
            
            psGroupMemberController.retrieveExistingMembers();
            psGroupMemberController.backToPsGroup();
            
            
        }         
    }

}