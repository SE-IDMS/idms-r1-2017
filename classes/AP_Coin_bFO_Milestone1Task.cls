/************
Task To updated value To Phase Coin
Project_Milestone__c 0. DEFINE
*************/

public class AP_Coin_bFO_Milestone1Task {

    public static boolean isTaskByPass =false;
    public static void updatePhaseStageOncompleteTsk(Map<Id,Milestone1_Task__c> mapMSTsk) {
        set<id> setCoinPhaseID = new set<id>();
        
        map<id,Milestone1_Milestone__c> mapMMStone= new map<id,Milestone1_Milestone__c>();
        
        map<id,List<Milestone1_Task__c>> mapMMSTsk= new map<id,List<Milestone1_Task__c>>();
       
        for(Milestone1_Task__c mTsk:mapMSTsk.values()) {
            setCoinPhaseID.add(mTsk.Project_Milestone__c);
        
        }
        List<Milestone1_Milestone__c> lisMM =[select id ,Stage__c from Milestone1_Milestone__c where id IN:setCoinPhaseID ]; 
        List<Milestone1_Task__c>  listMileStone =[select id,Task_Stage__c,Project_Milestone__r.name from Milestone1_Task__c where Project_Milestone__c IN:setCoinPhaseID  ];
    
        for(Milestone1_Task__c MSTskObj:listMileStone) {
            if(mapMMSTsk.containsKey(MSTskObj.Project_Milestone__c)) {
                mapMMSTsk.get(MSTskObj.Project_Milestone__c).add(MSTskObj);
            
            }else {
                mapMMSTsk.put(MSTskObj.Project_Milestone__c,new list<Milestone1_Task__c>());
                mapMMSTsk.get(MSTskObj.Project_Milestone__c).add(MSTskObj);
            }
       
        
        }
        Map<id,Milestone1_Milestone__c> listUpdaeComptMMP = new Map<id,Milestone1_Milestone__c>();
        Map<id,Milestone1_Milestone__c> listUpdaeNonComptMMP = new Map<id,Milestone1_Milestone__c>();
        for(Milestone1_Milestone__c MSTskObj:lisMM ) {
            
            if(mapMMSTsk.containsKey(MSTskObj.id)) {
                for(Milestone1_Task__c MSTskObj1:mapMMSTsk.get(MSTskObj.id)) {
                    if(MSTskObj.Stage__c!='Completed') {
                        if(MSTskObj1.Task_Stage__c=='Completed') {
                           
                            listUpdaeComptMMP.put(MSTskObj.id,MSTskObj); 
                        }else {
                            listUpdaeNonComptMMP.put(MSTskObj.id,MSTskObj); 
                            break;
                        }
                    }
                
                }
            
            }   
        
        
        }
        list<Milestone1_Milestone__c> listUpdate= new List<Milestone1_Milestone__c>();
        for(Milestone1_Milestone__c MSTskObj1:lisMM) {
            if(!listUpdaeNonComptMMP.containsKey(MSTskObj1.id) && listUpdaeComptMMP.containsKey(MSTskObj1.id)) {
                MSTskObj1.stage__c='Completed';
            }
            listUpdate.add(MSTskObj1);
        }
        system.debug('Test=======>'+listUpdaeNonComptMMP);
        if(listUpdate.size() > 0) {
            isTaskByPass=true;
            update listUpdate;
        }
    }
    public static void updatePhaseStageOnInProgresTsk(Map<Id,Milestone1_Task__c> mapMSTsk) {
        set<id> setCoinPhaseID = new set<id>();
        set<id> setProjectId = new set<id>();
        
        map<id,Milestone1_Milestone__c> mapMMStone= new map<id,Milestone1_Milestone__c>();
        
        map<id,List<Milestone1_Task__c>> mapMMSTsk= new map<id,List<Milestone1_Task__c>>();
        List<Milestone1_Task__c> listMMSTsk= new List<Milestone1_Task__c>();
       
        for(Milestone1_Task__c mTsk:[select id,Task_Stage__c,Project_Milestone__r.name,Project_Milestone__r.Project__c from Milestone1_Task__c where id IN:mapMSTsk.keyset()]) {
            setCoinPhaseID.add(mTsk.Project_Milestone__c);
            setProjectId.add(mTsk.Project_Milestone__r.Project__c);
            listMMSTsk.add(mTsk);
        
        }
        //ForeGet All Phase (MM)
        Map<id,Milestone1_Project__c> MapUpdateStg= new Map<id,Milestone1_Project__c>();
        
        List<Milestone1_Milestone__c> listMM =[select id,Name ,Stage__c,Project__c from Milestone1_Milestone__c where Project__c IN:setProjectId ]; 
        
        for(milestone1_Task__c taskObj:listMMSTsk) {
            for(Milestone1_Milestone__c mmObj:listMM) {
                if(taskObj.Project_Milestone__r.Project__c==mmObj.project__c) {
                    if(taskObj.Project_Milestone__r.name=='1. PREPARE' && taskObj.Task_Stage__c!='' && taskObj.Task_Stage__c!='Not Started') {
                        if(mmObj.name=='0. DEFINE' && mmObj.Stage__c=='Completed' &&  mmObj.Stage__c!=null ) {
                            Milestone1_Project__c instMp = new Milestone1_Project__c(id=mmObj.project__c); 
                            instMp.stage__c=taskObj.Project_Milestone__r.name;
                            MapUpdateStg.put(mmObj.project__c,instMp);
                        }
                    }
                   
                    else if(taskObj.Project_Milestone__r.name=='2. DEPLOY' && taskObj.Task_Stage__c!=''&& taskObj.Task_Stage__c!='Not Started') {
                           if(mmObj.name=='1. PREPARE' && mmObj.Stage__c=='Completed' &&  mmObj.Stage__c!=null) {
                                Milestone1_Project__c instMp = new Milestone1_Project__c(id=mmObj.project__c); 
                                instMp.stage__c=taskObj.Project_Milestone__r.name;
                                MapUpdateStg.put(mmObj.project__c,instMp);
                            }
                    
                    }
                    else if(taskObj.Project_Milestone__r.name=='3. SELL' && taskObj.Task_Stage__c!='' && taskObj.Task_Stage__c!='Not Started') {
                           if(mmObj.name=='2. DEPLOY' && mmObj.Stage__c=='Completed' &&  mmObj.Stage__c!=null) {
                                Milestone1_Project__c instMp = new Milestone1_Project__c(id=mmObj.project__c); 
                                instMp.stage__c=taskObj.Project_Milestone__r.name;
                                MapUpdateStg.put(mmObj.project__c,instMp);
                            }
                    
                    }
                }
            }
        }
        
        if(MapUpdateStg.size() > 0) {
            isTaskByPass=true;
            update MapUpdateStg.values();
            
        }

}

}