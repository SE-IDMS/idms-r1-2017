public class VFC69_ClosedTasks {

//*********************************************************************************
// Controller Name  : VFC69_ClosedTasks
// Purpose          : Controller class for VFP69_ClosedTasks
// Created by       : Global Delivery Team
// Date created     : 02nd Nov 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Dec - 11 Release
///********************************************************************************/
    
    List<Task> tasklist=new List<Task>();
    
    final Integer  RECORD_COUNT=250;
    final Integer  MAXLOOP_COUNT=5;
    
    /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/    
    public VFC69_ClosedTasks(){
        
        Schema.DescribeSObjectResult D = case.sObjectType.getDescribe();
        String caseprefix=D.getKeyPrefix();
        Utils_SDF_Methodology.log('START constructor Execution  ');  
        Utils_SDF_Methodology.startTimer();
        Integer Count=0;
        
        
            for(Task t: [Select t.CallType,t.CreatedDate ,t.CreatedById, t.LastModifiedDate,t.WhoId,t.WhatId , t.OwnerId, t.Owner.Name, t.Subject from Task t where  CreatedById =:UserInfo.getUserId() AND t.OwnerId !=:UserInfo.getUserId() AND Status='Completed'  order by t.LastModifiedDate DESC limit :RECORD_COUNT] ){            
                if(count < MAXLOOP_COUNT){
                    if(t.WhatId!=null){
                         String CaseId=t.WhatId;
                         if(CaseId.startsWith(caseprefix)){
                             tasklist.add(t); 
                             Count++;                
                         } 
                     } 
                }
            } 
       
              
       userName=UserInfo.getFirstName()+' '+UserInfo.getLastName();
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END constructor Execution ');
        Utils_SDF_Methodology.Limits();

    }
    //*********************************************************************************
        // Method Name      : getTasks
        // Purpose          : This will return the closed tasks to the VF 
        // Created by       : Hari Krishna  Global Delivery Team
        // Date created     : 02nd Nov 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
        ///********************************************************************************/
    public List<Task> getTasks(){
    
        return tasklist;
    }
    
    public String userName{get;set;}
    
}