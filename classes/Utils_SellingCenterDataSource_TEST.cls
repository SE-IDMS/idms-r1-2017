@isTest
private class Utils_SellingCenterDataSource_TEST {

//*********************************************************************************
// Controller Name  : Utils_SellingCenterDataSource
// Purpose          : Test Data Source for Search Selling Center
// Created by       : Hari Krishna  Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For Sales April 2012 Release
///********************************************************************************/

    static testMethod void SellingCenterDataSourceTest() {
        
        
        Country__c country1 = Utils_TestMethods.createCountry();
        insert country1;
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
              
        Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
        oppty.OwnerId = UserInfo.getUserId();      
        insert oppty;  
            
        
        
         Opp_SellingCenter__c sellcenter= Utils_TestMethods.createSellingCenter();
        sellcenter.Name = 'Test for VFC80_SearchSellingCenter';
        sellcenter.Business__c ='BD';
        sellcenter.Country__c = country1.id;
        insert  sellcenter;
        
        
        Utils_DataSource SellingCenterDSource = Utils_Factory.getDataSource('SECS');
        List<SelectOption> Columns = SellingCenterDSource.getColumns();
        List<String> dummyList1 = new List<String>();
        List<String> dummyList2 = new List<String>();
        Integer counter = 0;
        
        for(Integer index=1;index<9;index++)
        {
            dummyList1.clear();
            dummyList2.clear();                
            counter = index;
            if(counter == 1)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country1.Id));
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 2)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country1.Id));
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 3)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 4)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 5)
            {
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 6)
            {
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country1.Id));
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 7)
            {
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country1.Id));
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            if(counter == 8)
            {
                dummyList1.add('UNKNOWN');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country1.Id));
                Utils_DataSource.Result Results = SellingCenterDSource.search(dummyList1, dummyList2);
            }
            
        }
        
        
        Utils_DataSource.Result results = SellingCenterDSource.search(dummyList1, dummyList2);
    
        
        
        
        
    
    }
}