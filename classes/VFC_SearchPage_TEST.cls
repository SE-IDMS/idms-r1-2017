/*
    Author          : Bhuvana Subramaniyan
    Description     : Test Class for VFC_SearchPage.
*/
@isTest
private class VFC_SearchPage_TEST
{
    static testmethod void testSearchPage()
    {
        test.startTest();
        CS_CustomLabels__c csL = new CS_CustomLabels__c();
        csL.Name = 'TEStCustomLabel';
        insert csL;
        VFC_SearchPage srchPage = new VFC_SearchPage();
        srchPage.search();
        srchPage.selMetComp = 'Objects';
        srchPage.searchText= 'Name';
        srchPage.sObjectName= 'Asset';
        srchPage.getmetadataComponents();
        srchPage.getObjectName();
        srchPage.search();
        
        srchPage.selMetComp = 'Objects';
        srchPage.searchText= 'Laptop';
        srchPage.sObjectName= 'Asset';
        srchPage.search();
        
        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'CustomLabel';
        srchPage.getcustLabels();
        
        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'BATCHClass';
        srchPage.search();

        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'ApexClass';
        srchPage.searchtext = 'class';
        srchPage.search();
        
        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'Page';
        srchPage.searchtext = 'page';
        srchPage.search();
        
        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'Trigger';
        srchPage.searchtext = 'trigger';
        srchPage.search();
        
        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'Component';
        srchPage.searchtext = 'component';
        srchPage.search();
        
        srchPage = new VFC_SearchPage();
        srchPage.selMetComp = 'EmailTemplate';
        srchPage.searchtext = 'Text';
        srchPage.search();

        Test.stopTest();
    }
    static testMethod void testSearchPage1()
    {
        test.startTest();
        VFC_SearchPage srchPage = new VFC_SearchPage();        
        srchPage.searchText= 'Reason';
        srchPage.selMetComp = 'ValidationRule';        
        srchPage.sObjectName= 'Account';     
        List<String> customLabels = new List<String>();
        customLabels.add('TestCustomLabel');   
        srchPage.selCustLabel = customLabels;
        Test.setMock(WebServiceMock.class, new WS_MOCK_SEARCHPAGE_TEST('ReadMetadata'));
        srchPage.search();
        

    }
    
}