public class SVMXC_WorkOrderAfter {

    public static void createSharingForCreatorOnInsert (List<SVMXC__Service_Order__c> triggerNew, Map<Id, SVMXC__Service_Order__c> triggerOld, Boolean isInsert, Boolean isUpdate) {
        /*
            Method to create Sharing to Creator of WO when in New Order Status - Written : December 9, 2015
            
            NOTE : WO Sharing is also performed in the WorkOrderAfterUpdate trigger, but is being added here to better align with the best practice of a single trigger.
        */
        
        List<SVMXC__Service_Order__Share> woShareLst = new List<SVMXC__Service_Order__Share>();
        
        if (isInsert) {
        
            for (SVMXC__Service_Order__c wo : triggerNew) {
            
                // todo add check for WOs that are follow up?
                if (wo.SVMXC__Order_Status__c == 'New' && wo.Parent_Work_Order__c != null) {
                    woShareLst.add(buildShare(wo.Id, wo.CreatedById));
                }
            }
            
            if (!woShareLst.isEmpty())
                insert woShareLst;
        }
    }

    public static void createSharingForCreator (List<SVMXC__Service_Order__c> triggerNew, Map<Id, SVMXC__Service_Order__c> triggerOld, Boolean isInsert, Boolean isUpdate) {
        /*
            Method to create Sharing to Creator of WO when in New Order Status - Written : December 9, 2015
            
            NOTE : WO Sharing is also performed in the WorkOrderAfterUpdate trigger, but is being added here to better align with the best practice of a single trigger.
        */
        
        List<SVMXC__Service_Order__Share> woShareLst = new List<SVMXC__Service_Order__Share>();
        Map<Id, Id> woCreatorMap = new Map<Id, Id>(); 
        Map<Id,User> CreatedbyUserRecMap = New Map<Id,User>();
        Set<Id> woIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
           
        if (isUpdate) {
            for (SVMXC__Service_Order__c wo : triggerNew) {
            
                if (wo.SVMXC__Order_Status__c == 'New'  && wo.OwnerId != wo.CreatedById) {
                    
                    if (!woIds.contains(wo.Id))
                        woIds.add(wo.Id);
                    if (!userIds.contains(wo.CreatedById))
                        userIds.add(wo.CreatedById); 
                     
                    woCreatorMap.put(wo.Id, wo.CreatedById);
                    
                }
            }
            //Added below logic by VISHNU C for Hot Fix 27/06/2016
            if(userIds.size() > 0 )
            {
                for (user CreatedbyUserRec : [select id,DefaultCurrencyIsoCode,IsActive from user where id in : userIds])
                {
                    CreatedbyUserRecMap.put(CreatedbyUserRec.id,CreatedbyUserRec);
                }
            }
            //Changed by VISHNU C Ends here
            
            if (!woCreatorMap.isEmpty()) {
                // need to look for sharing records
                // Commented for Query limit error on 13-07-2016
                /*List<SVMXC__Service_Order__Share> existingShares = [SELECT ParentId, UserOrGroupId FROM SVMXC__Service_Order__Share 
                                                                    WHERE (ParentId IN : woIds OR UserOrGroupId IN : userIds)
                                                                    AND AccessLevel = 'Edit'];*/
                List<SVMXC__Service_Order__Share> existingShares = [SELECT ParentId, UserOrGroupId FROM SVMXC__Service_Order__Share 
                                                                    WHERE ParentId IN : woIds 
                                                                    AND AccessLevel = 'Edit'];
            
                Map<Id, Set<Id>> existingSharesMap = new Map<Id, Set<Id>>();
                
                if (!existingShares.isEmpty()) {
                    existingSharesMap = buildExistingShareMap(existingShares);
                }
                
                // step back through WO to see what Shares need to be created
                for (SVMXC__Service_Order__c wo2 : triggerNew) {
                    
                    if (woIds.contains(wo2.Id) && CreatedbyUserRecMap.get(wo2.CreatedById).IsActive == true) //Added condition CreatedbyUserRecMap.get(wo.CreatedById).IsActive == 'Y' by VISHNU C
                    //if (woIds.contains(wo2.Id))
                    {
                        system.debug('Entered Inside to check the Sharing Rule');
                        // check for existing Share
                        if (existingSharesMap.containsKey(wo2.Id)) {
                            
                            Set<Id> workingSet = existingSharesMap.get(wo2.Id);
                            
                            if (!workingSet.contains(wo2.CreatedById))
                                woShareLst.add(buildShare(wo2.Id, wo2.CreatedById));
                                
                        } else {
                            // no existing Share
                            woShareLst.add(buildShare(wo2.Id, wo2.CreatedById));
                        }
                    }
                }
            }
            
            if (!woShareLst.isEmpty())
                insert woShareLst;
        }
    }   
    
    private static SVMXC__Service_Order__Share buildShare(Id parentId, Id UserId) {
    
        SVMXC__Service_Order__Share newShare = new SVMXC__Service_Order__Share();
        newShare.ParentId = parentId;
        newShare.UserOrGroupId = UserId;
        newShare.AccessLevel = 'Edit';
        newShare.RowCause = Schema.SVMXC__Service_Order__Share.RowCause.SharingForFollowUpWOOwnership__c;
        
        return newShare;
    }
    
    private static Map<Id, Set<Id>> buildExistingShareMap(List<SVMXC__Service_Order__Share> shareList) {
        
        Map<Id, Set<Id>> returnMap = new Map<Id, Set<Id>>();
        
        for (SVMXC__Service_Order__Share sh : shareList) {
            
            Set<Id> workingSet = new Set<Id>();
            
            if (returnMap.containsKey(sh.ParentId)) {
                
                workingSet = returnMap.get(sh.ParentId);
                
                if (!workingSet.contains(sh.UserOrGroupId)) {
                    workingSet.add(sh.UserOrGroupId);
                    returnMap.put(sh.ParentId, workingSet);
                }
                
            } else {
                workingSet.add(sh.UserOrGroupId);
                returnMap.put(sh.ParentId, workingSet);
            }
        }
    
        return returnMap;
    }

    /*
    public static void syncProductExpertiseReqToWO (List<SVMXC__Service_Order__c> triggerNew, Map<Id, SVMXC__Service_Order__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
       
    
        Set<Id> installedProductIds = new Set<Id>();
        Set<Id> workOrdersToProcessIds = new Set<Id>();
        
        if(triggerNew != null){
            
            for (SVMXC__Service_Order__c wo : triggerNew) {
                if (isInsert) {
                                
                    if (wo.SVMXC__Component__c != null) {
                        
                        workOrdersToProcessIds.add(wo.Id);
                        if (!installedProductIds.contains(wo.SVMXC__Component__c))  
                            installedProductIds.add(wo.SVMXC__Component__c);
                    }
                        
                } else if (isUpdate) {
                    
                    if (wo.SVMXC__Component__c != null && wo.SVMXC__Component__c != triggerOld.get(wo.Id).SVMXC__Component__c) { 
                        
                        workOrdersToProcessIds.add(wo.Id);
                        if (!installedProductIds.contains(wo.SVMXC__Component__c))      
                            installedProductIds.add(wo.SVMXC__Component__c);            
                    }                       
                }
            }
        }
        
        if (!installedProductIds.isEmpty()) {
            
            // prevent this code from executing twice
            setAlreadyCheckedProductReqExpSync();
            
            // at least one Work Order has a Main Installed Product that we need to see if there are any Product Expertise Requirements
            Map<Id, Set<Id>> ipSkillMap = new Map<Id, Set<Id>>();
            
            ipSkillMap = returnSkillsByProduct(installedProductIds);    
            
            if (!ipSkillMap.isEmpty()) {
                //Y. Tisserand (14/04/2015): retrieve existing skills on the WO
                List<Work_Order_Skills__c> oldWorkOrderSkills = [Select Skill__c,Work_Order__c from Work_Order_Skills__c where Work_Order__c IN :trigger.new];
                Set<String> woSkillKey = new Set<String>();
                  
                for(Work_Order_Skills__c wos:oldWorkOrderSkills) {
                    if(!woSkillKey.contains(wos.Work_Order__c+'#'+wos.Skill__c)){
                      woSkillKey.add(wos.Work_Order__c+'#'+wos.Skill__c);
                    }
                }
                  
                 
            
                List<Work_Order_Skills__c> newWorkOrderSkills = new List<Work_Order_Skills__c>();
                
                for (SVMXC__Service_Order__c so2 : triggerNew) {
                    
                    if (workOrdersToProcessIds.contains(so2.Id) && ipSkillMap.containsKey(so2.SVMXC__Component__c)) {
                         if(so2.IsBillable__c=='Yes'&&so2.BillToAccount__c==null){
                           so2.addError('Please, select a Bill To Account for billable Work Orders.'); 
                         }
                         else{
                        
                            // this work order has at least one skill that needs to be added
                            Set<Id> skillIds = ipSkillMap.get(so2.SVMXC__Component__c);
                            
                            for (Id skills : skillIds) {
                                // Only add if not already on the WO
                                if(!woSkillKey.contains(so2.Id+'#'+skills)){
                                  Work_Order_Skills__c wos = new Work_Order_Skills__c();
                                  wos.Skill__c = skills;
                                  wos.Work_Order__c = so2.Id;
                                  newWorkOrderSkills.add(wos);
                                }
                            }
                        }
                    }
                }  
                
                insert newWorkOrderSkills;
                         
            }           
        }
    }
    */
    public static void syncProductExpertiseReqToWO (List<SVMXC__Service_Order__c> triggerNew) {
        /*
            Method to sync Product Expertise Requirements to Work Order based on Component field of Work Order - Written : Feburary 20, 2015
        */
         List<Work_Order_Skills__c> woskilllist= new List<Work_Order_Skills__c>();
        Set<Id> installedProductIds = new Set<Id>();
        Set<Id> workOrdersToProcessIds = new Set<Id>();
        Set<Id> woids = new Set<Id>();
        for (SVMXC__Service_Order__c wo : triggerNew) { 
                woids.add(wo.Id);  
            if (wo.SVMXC__Component__c != null) {
                
                workOrdersToProcessIds.add(wo.Id);
                if (!installedProductIds.contains(wo.SVMXC__Component__c))  
                    installedProductIds.add(wo.SVMXC__Component__c);
            }                   
            
        }
        
        if(woids!=null)
         woskilllist = [Select id,name,Tech_IPchange__c,Skill__c,Work_Order__c from Work_Order_Skills__c where Work_Order__c IN :woids AND Tech_IPchange__c=true AND Record_Type__c='Expertise'];
        
        if(woskilllist.size()>0)
            Delete woskilllist;
        if (!installedProductIds.isEmpty()) {
  
            // at least one Work Order has a Main Installed Product that we need to see if there are any Product Expertise Requirements
            Map<Id, Set<Id>> ipSkillMap = new Map<Id, Set<Id>>();
            
            ipSkillMap = returnSkillsByProduct(installedProductIds);    
            
            if (!ipSkillMap.isEmpty()) {
                //Y. Tisserand (14/04/2015): retrieve existing skills on the WO
                List<Work_Order_Skills__c> oldWorkOrderSkills = [Select Skill__c,Work_Order__c from Work_Order_Skills__c where Work_Order__c IN :trigger.new];
                Set<String> woSkillKey = new Set<String>();
                  
                for(Work_Order_Skills__c wos:oldWorkOrderSkills) {
                    if(!woSkillKey.contains(wos.Work_Order__c+'#'+wos.Skill__c)){
                      woSkillKey.add(wos.Work_Order__c+'#'+wos.Skill__c);
                    }
                }
                  
                 
            
                List<Work_Order_Skills__c> newWorkOrderSkills = new List<Work_Order_Skills__c>();
                
                for (SVMXC__Service_Order__c so2 : triggerNew) {
                    
                    if (workOrdersToProcessIds.contains(so2.Id) && ipSkillMap.containsKey(so2.SVMXC__Component__c)) {
                        //Commented Below Lines by VISHNU C for July Minor 2016 Ticket KSR000001057889 To Remove this validation as it is affecting the WO created from Cases
                        /*if(so2.IsBillable__c=='Yes'&&so2.BillToAccount__c==null)
                        {
                           so2.addError('Please, select a Bill To Account for billable Work Orders.'); 
                        }*/
                         //else{

                            // this work order has at least one skill that needs to be added
                            Set<Id> skillIds = ipSkillMap.get(so2.SVMXC__Component__c);
                                for (Id skills : skillIds) {
                                // Only add if not already on the WO
                                if(!woSkillKey.contains(so2.Id+'#'+skills)){
                                    SVMXC__Skill__c  activeskill= new SVMXC__Skill__c(id=skills);
                                  Work_Order_Skills__c wos = new Work_Order_Skills__c();
                                 //if(activeskill.SVMXC__Active__c==true)
                                         wos.Skill__c = skills;
                                         wos.Work_Order__c = so2.Id;
                                         wos.Tech_IPchange__c=true;
                                        //if(activeskill.SVMXC__Active__c==true)
                                        newWorkOrderSkills.add(wos);
                                }
                            }
                        //}
                    }
                }  
                if(newWorkOrderSkills.size()>0)
                insert newWorkOrderSkills;
                         
            }           
        }
        
    }

    private static Map<Id, Set<Id>> returnSkillsByProduct (Set<Id> installedProductIds) {
    
        Map<Id, Set<Id>> returnMap = new Map<Id, Set<Id>>();
        List<SVMXC__Installed_Product__c> productsList= new List<SVMXC__Installed_Product__c>();
         if(installedProductIds!=null)   
         productsList = [SELECT Id, SVMXC__Product__c,Brand2__c,DeviceType2__c,Category__c, SVMXC__Product__r.Brand2__c, 
                                                            SVMXC__Product__r.DeviceType2__c, SVMXC__Product__r.CategoryId__c
                                                            FROM SVMXC__Installed_Product__c WHERE Id IN : installedProductIds];
                                                            
       
        // next we query to find a match on the Generic Product with the TECH_IsEnrichedequals to true
        
        Set<Id> brandIds = new Set<Id>();
        Map<String, Set<Id>> familySkillMap = new Map<String, Set<Id>>();
        Set<Id> deviceTypeIds = new Set<Id>();
        Set<Id> categoryIds = new Set<Id>();
        Set<Id> ProductIds = new Set<Id>();
        Set<Id> brandIds1 = new Set<Id>();
        Set<Id> deviceTypeIds1 = new Set<Id>();
        Set<Id> categoryIds1 = new Set<Id>();
        Set<Id> ProductIds1 = new Set<Id>();
        List<SVMXC_ApplicableProduct__c> genericProductSkillListQuad = new List<SVMXC_ApplicableProduct__c>();
        List<SVMXC_ApplicableProduct__c> genericProductSkillList = new List<SVMXC_ApplicableProduct__c>();
        Map<Id, String> ipProductMap = new Map<Id, String>(); // organized as Brand/DeviceType/Category
        if(productsList!=null)
        for (SVMXC__Installed_Product__c ipp : productsList) 
        {
        
        String comboString = '';
            if(ipp.SVMXC__Product__c!=null && ipp.Brand2__c!=null && ipp.DeviceType2__c!=null  && ipp.Category__c!=null)
                {
                
                    comboString += ipp.Brand2__c;
                   comboString += ipp.DeviceType2__c;
                   comboString += ipp.Category__c;
                   comboString += ipp.SVMXC__Product__c;
                
                  ProductIds1.add(ipp.SVMXC__Product__c);
                  brandIds1.add(ipp.Brand2__c);
                  deviceTypeIds1.add(ipp.DeviceType2__c);
                   categoryIds1.add(ipp.Category__c);
    
                }
                ipProductMap.put(ipp.Id, comboString); 
                    system.debug('ipProductMapsize' +ipProductMap.size());
                    system.debug('ipProductMapvalues' +ipProductMap);
                
                
            
           /*
            
            if (ip.SVMXC__Product__r.Brand2__c != null) {
                
                if (!brandIds1.contains(ip.Brand2__c))
                    brandIds1.add(ip.Brand2__c);
                    system.debug('brandIds'+brandIds1);
            }
            //Adding Else Condition to Check Brand,Range,DeviceType if Product is Empty as part of BR-8022
            else{
                             
                if (!brandIds1.contains(ip.Brand2__c) )
                    brandIds1.add(ip.Brand2__c);
                    system.debug('brandIds'+brandIds1);
            }
            
            if (ip.SVMXC__Product__r.DeviceType2__c != null) {
                                
                if (!deviceTypeIds1.contains(ip.DeviceType2__c))
                    deviceTypeIds1.add(ip.DeviceType2__c);
                    system.debug('deviceTypeIds'+deviceTypeIds1);
            }
             //Adding Else Condition to Check Brand,Range,DeviceType if Product is Empty as part of BR-8022
            else{
             
                if (!deviceTypeIds1.contains(ip.DeviceType2__c))
                    deviceTypeIds1.add(ip.DeviceType2__c);
                    system.debug('deviceTypeIds'+deviceTypeIds1);
            }
            
            if (ip.SVMXC__Product__r.CategoryId__c != null) {
                
                if (!categoryIds1.contains(ip.Category__c))
                    categoryIds1.add(ip.Category__c);
                    system.debug('categoryIds'+categoryIds1);
            }
             //Adding Else Condition to Check Brand,Range,DeviceType if Product is Empty as part of BR-8022
            else{
                
                
                if (!categoryIds1.contains(ip.Category__c))
                    categoryIds1.add(ip.Category__c);
                    system.debug('categoryIds'+categoryIds);
            }
            //adding Quadriplet Condition
            system.debug('SVMXC__Product__c'+ip.SVMXC__Product__c);
            if (ip.SVMXC__Product__c != null) {
                
                if (!ProductIds1.contains(ip.SVMXC__Product__c))
                    ProductIds1.add(ip.SVMXC__Product__c);
                    system.debug('ProductIds'+ProductIds);
            }
            } */ 
        }
        Set<Id> QuadripletProducts = new Set<Id>(); 
        Boolean QuadriletProdcutMatch;  
        /*
        List<SVMXC_ApplicableProduct__c> genericProductSkillListQuad = [SELECT Id,Skill__c, Product__c, Product__r.Brand2__c,TECH_IsEnriched__c, 
                                                                    Product__r.DeviceType2__c, Product__r.CategoryId__c
                                                                    FROM SVMXC_ApplicableProduct__c 
                                                                    WHERE Product__r.Brand2__c IN: brandIds1 AND 
                                                                    Product__r.DeviceType2__c IN : deviceTypeIds1 AND
                                                                     Product__r.CategoryId__c IN : categoryIds1 AND product__c IN:ProductIds1 ];
        System.debug('genericProductSkillListQuad '+genericProductSkillListQuad );
        
        */
        if(ProductIds1!=null)
        genericProductSkillListQuad = [SELECT Id,Skill__c, Product__c, Product__r.Brand2__c,TECH_IsEnriched__c,Skillactive__c, 
                                                                    Product__r.DeviceType2__c, Product__r.CategoryId__c
                                                                    FROM SVMXC_ApplicableProduct__c 
                                                                    WHERE  product__c IN:ProductIds1  AND Product__r.Brand2__c IN:brandIds1 AND Product__r.DeviceType2__c IN : deviceTypeIds1 AND Product__r.CategoryId__c IN : categoryIds1 AND Skillactive__c=True];
                                            
                                            
            System.debug('genericProductSkillListQuad '+genericProductSkillListQuad );
            System.debug('genericProductSkillListQuad '+genericProductSkillListQuad.size() );           
                                                                    
        if(genericProductSkillListQuad!=null)       
        {
            for (SVMXC_ApplicableProduct__c ap : genericProductSkillListQuad) 
            {
                String familyCombo = '';
                
                //if(ProductIds1.contains(ap.Product__c)){
                        familyCombo += ap.Product__r.Brand2__c;
                        familyCombo += ap.Product__r.DeviceType2__c;
                        familyCombo += ap.Product__r.CategoryId__c;
                        familyCombo += ap.Product__c;
                        //}
                if (familySkillMap.containsKey(familyCombo)) 
                {
                    
                    Set<Id> workingSet1 = familySkillMap.get(familyCombo);
                    workingSet1.add(ap.Skill__c);
                    familySkillMap.put(familyCombo, workingSet1);
                
                } else {
                    
                    Set<Id> workingSet2 = new Set<Id>();
                    workingSet2.add(ap.Skill__c);
                    familySkillMap.put(familyCombo, workingSet2);
                }
                        
                        
                /*
                //Quadrilet Condition for BR-8022
                //if(!ProductIds1.IsEmpty()){
                    if(ProductIds1.contains(ap.Product__c)){
                       QuadripletProducts.add(ap.product__c);
                       QuadriletProdcutMatch = True;
                       System.debug('QuadripletProduct'+QuadripletProducts);
                    }
               // } */
            } 
                for (Id i : installedProductIds) 
                    {
                     
                    if (familySkillMap.containsKey(ipProductMap.get(i)))
                        {
                            returnMap.put(i, familySkillMap.get(ipProductMap.get(i)));
                            System.debug('result return Skills Map for test' + returnMap);
                         }
                           
                    }
        }
            if(productsList!=null)
        for (SVMXC__Installed_Product__c ip : productsList) {
            
            String comboString = '';
            
            if(ip.Brand2__c!=null && ip.DeviceType2__c!=null &&ip.Category__c!=null)
            {
                comboString += ip.Brand2__c ;//+ ip.DeviceType2__c + ip.Category__c;
                comboString += ip.DeviceType2__c;
                comboString += ip.Category__c;
                brandIds.add(ip.Brand2__c);
                deviceTypeIds.add(ip.DeviceType2__c);
                categoryIds.add(ip.Category__c);
                ipProductMap.put(ip.Id, comboString); 
            }
            
            /*
            
            if (ip.SVMXC__Product__r.Brand2__c != null && QuadriletProdcutMatch == True) {
                comboString += ip.SVMXC__Product__r.Brand2__c;
                
                if (!brandIds.contains(ip.SVMXC__Product__r.Brand2__c))
                    brandIds.add(ip.SVMXC__Product__r.Brand2__c);
                    system.debug('brandIds'+brandIds);
            }
            //Adding Else Condition to Check Brand,Range,DeviceType if Product is Empty as part of BR-8022
            else{
              comboString += ip.Brand2__c;
                
                if (!brandIds.contains(ip.Brand2__c))
                    brandIds.add(ip.Brand2__c);
                    system.debug('brandIds'+brandIds);
            }
            
            if (ip.SVMXC__Product__r.DeviceType2__c != null && QuadriletProdcutMatch == True) {
                comboString += ip.SVMXC__Product__r.DeviceType2__c;
                
                if (!deviceTypeIds.contains(ip.SVMXC__Product__r.DeviceType2__c))
                    deviceTypeIds.add(ip.SVMXC__Product__r.DeviceType2__c);
                    system.debug('deviceTypeIds'+deviceTypeIds);
            }
             //Adding Else Condition to Check Brand,Range,DeviceType if Product is Empty as part of BR-8022
            else{
              comboString += ip.DeviceType2__c;
                
                if (!deviceTypeIds.contains(ip.DeviceType2__c))
                    deviceTypeIds.add(ip.DeviceType2__c);
                    system.debug('deviceTypeIds'+deviceTypeIds);
            }
            
            if (ip.SVMXC__Product__r.CategoryId__c != null && QuadriletProdcutMatch == True) {
                comboString += ip.SVMXC__Product__r.CategoryId__c;
                
                if (!categoryIds.contains(ip.SVMXC__Product__r.CategoryId__c))
                    categoryIds.add(ip.SVMXC__Product__r.CategoryId__c);
                    system.debug('categoryIds'+categoryIds);
            }
             //Adding Else Condition to Check Brand,Range,DeviceType if Product is Empty as part of BR-8022
            else{
                comboString += ip.Category__c;
                
                if (!categoryIds.contains(ip.Category__c))
                    categoryIds.add(ip.Category__c);
                    system.debug('categoryIds'+categoryIds);
            }
            //adding Quadriplet Condition
            system.debug('SVMXC__Product__c'+ip.SVMXC__Product__c);
            system.debug('SVMXC__Product__c'+QuadripletProducts);
            if (ip.SVMXC__Product__c != null && QuadripletProducts.contains(ip.SVMXC__Product__c)) {
                comboString += ip.SVMXC__Product__c;
                system.debug('products added');
                if (!ProductIds.contains(ip.SVMXC__Product__c))
                    ProductIds.add(ip.SVMXC__Product__c);
                    system.debug('ProductIds'+ProductIds);
            }
            
            
            
            ipProductMap.put(ip.Id, comboString);   
            System.debug('comboString' + comboString);
            System.debug('###### ipProductMap ' + ipProductMap);
            */
        }
       /*
        List<SVMXC_ApplicableProduct__c> genericProductSkillList = [SELECT Skill__c, Product__c, Product__r.Brand2__c,TECH_IsEnriched__c, 
                                                                    Product__r.DeviceType2__c, Product__r.CategoryId__c
                                                                    FROM SVMXC_ApplicableProduct__c 
                                                                    WHERE (Product__r.Brand2__c IN: brandIds OR 
                                                                    Product__r.DeviceType2__c IN : deviceTypeIds OR
                                                                    Product__r.CategoryId__c IN : categoryIds)];
        
        
        system.debug('genericProductSkillList'+genericProductSkillList );*/
        
        genericProductSkillList = [SELECT Skill__c, Product__c, Product__r.Brand2__c,TECH_IsEnriched__c, 
                                                                    Product__r.DeviceType2__c, Product__r.CategoryId__c,Skillactive__c
                                                                    FROM SVMXC_ApplicableProduct__c 
                                                                    WHERE Product__r.Brand2__c IN: brandIds AND 
                                                                    Product__r.DeviceType2__c IN : deviceTypeIds AND
                                                                    Product__r.CategoryId__c IN : categoryIds AND TECH_IsEnriched__c=True AND Skillactive__c=True];
        
        
        
        system.debug('genericProductSkillList'+genericProductSkillList );
        system.debug('genericProductSkillListsize'+genericProductSkillList.size() );
         //Product__r.TECH_IsEnriched__c = true                                        
        if (genericProductSkillList!=null) {
        
            // Map that defines Brand/DeviceType/Category lookup to Skill Id
           // Map<String, Set<Id>> familySkillMap = new Map<String, Set<Id>>();
            
            for (SVMXC_ApplicableProduct__c ap : genericProductSkillList) 
            {
                String familyCombo = '';
                familyCombo += ap.Product__r.Brand2__c;
                familyCombo += ap.Product__r.DeviceType2__c;
                familyCombo += ap.Product__r.CategoryId__c;
                
               /* 
                String familyCombo = '';
                //Quadrilet Condition for BR-8022
                if(!ProductIds.IsEmpty()){
                    if(ProductIds.contains(ap.Product__c)){
                        familyCombo += ap.Product__r.Brand2__c;
                        familyCombo += ap.Product__r.DeviceType2__c;
                        familyCombo += ap.Product__r.CategoryId__c;
                        familyCombo += ap.Product__c;
                    }
                    else{
                       if(ap.TECH_IsEnriched__c==True){
                            system.debug('entered in Else Quardlet condition');
                            familyCombo += ap.Product__r.Brand2__c;
                            familyCombo += ap.Product__r.DeviceType2__c;
                            familyCombo += ap.Product__r.CategoryId__c;
                             
                        }
                    }
                                      
                }
                //Triplet Condition For BR-8022
                else{
                 if(ap.TECH_IsEnriched__c==True){
                    familyCombo += ap.Product__r.Brand2__c;
                    familyCombo += ap.Product__r.DeviceType2__c;
                    familyCombo += ap.Product__r.CategoryId__c;
                 }
                }*/
                system.debug('familySkillMap'+familySkillMap);
                system.debug('familyCombo'+familyCombo);

                    if (familySkillMap.containsKey(familyCombo)) {
                        
                        Set<Id> workingSet1 = familySkillMap.get(familyCombo);
                        workingSet1.add(ap.Skill__c);
                        familySkillMap.put(familyCombo, workingSet1);
                    
                    } else {
                        
                        Set<Id> workingSet2 = new Set<Id>();
                        workingSet2.add(ap.Skill__c);
                        familySkillMap.put(familyCombo, workingSet2);
                    }
                
            }
        
            
            System.debug('###### familySkillMap ' + familySkillMap);
            
            // now setup Map with Installed Product Ids and necessary skills        
            for (Id i : installedProductIds) 
            {
             
                if (familySkillMap.containsKey(ipProductMap.get(i)))
                    {
                        returnMap.put(i, familySkillMap.get(ipProductMap.get(i)));
                        System.debug('result return Skills Map for test' + returnMap);
                     }
                   
            }
        }
            
        System.debug('##### return Skills Map ' + returnMap);
        return returnMap;   
    }
    /*
    private static Boolean alreadyCheckedProductReqExpSync = false;
    // used to prevent WO Product Expertise Sync code from executing twice in a single trigger invocation

    public static Boolean hasAlreadyCheckedProductReqExpSync() {
        return alreadyCheckedProductReqExpSync;
    }

    public static void setAlreadyCheckedProductReqExpSync() {
        alreadyCheckedProductReqExpSync = true;
    }*/
}