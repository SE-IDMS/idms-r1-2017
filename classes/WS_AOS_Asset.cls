global class WS_AOS_Asset{ 

 global class InstalledProduct{

        webservice ID InstalledProductBFOID;
        webservice String Name;
        webservice Date CommissioningDate;
        webservice String SerialLotNumber;
        webservice String CommercialReference;
        webservice String GoldenID;
        webservice String InstalledProductCriticality;
        webservice Date AnnouncementofServiceObsolescenseDate;
        webservice Date EndOfCommercialisationDate;
        webservice Date ServiceObsolecenseDate;
        webservice String BrandeName;
        webservice String RangeName;
        webservice String DeviceTypeName;
        webservice Account Account;
        webservice Location Location;
     }       
         global class Account{
            webService String AccountName;
            webService String Street;
            webService String City;
            webService String ZipCode;
            webService String Country;
            webService String GoldenID;
            webService String Latitude;
            webService String Longitude;
         }
         
    global class Location{
        webService ID LocationBFOID;
        webService String LocationName;
        webService String GoldenID;
        webService String Type;
        webService Boolean AirConditionning;
        webService  Boolean AirDryerInTheSubstation;
        webService Boolean Heating;
  
 }
    global class InstalledproductResult{

    webservice ID BFOInstalledProductID;
    webservice Boolean ToBeSentForUpdate;
    webservice String GoldenID;

}

    webService static list<InstalledProduct> getAOSAssets(){
        list<SVMXC__Installed_Product__c> iplist= new list<SVMXC__Installed_Product__c>();
        
        List<InstalledProduct> InstalledProductslist = new List<InstalledProduct>();
           List<Location> Loclist = new List<Location>();

            iplist=[select id, name,CommissioningDateInstallDate__c,SVMXC__Serial_Lot_Number__c,SchneiderCommercialReference__c,GoldenAssetId__c,SVMXC__Site__c,SVMXC__Site__r.Name,
                    SVMXC__Site__r.GoldenLocationId__c,SVMXC__Site__r.SVMXC__Location_Type__c,SVMXC__Site__r.AirConditionning__c,SVMXC__Site__r.AirDryerInTheSubstation__c,SVMXC__Site__r.Heating__c,
                    InstalledProductCriticality__c,WithdrawalDate__c,EndOfCommercialisationDate__c,ServiceObsolecenseDate__c,TECH_SDHBRANDID__c,SVMXC__Company__r.ZipCode__c,
                    TECH_SDHCategoryId__c,TECH_SDHDEVICETYPEID__c,ToBeSentForUpdate__c,Remote_System__c,SVMXC__Company__c,SVMXC__Company__r.Name,SVMXC__Company__r.Street__c,
                    SVMXC__Company__r.City__c,SVMXC__Company__r.Country__r.Name,SVMXC__Company__r.SEAccountID__c,SVMXC__Company__r.SVMXC__Latitude__c,SVMXC__Company__r.SVMXC__Longitude__c
                    From SVMXC__Installed_Product__c
                    Where ToBeSentForUpdate__c = true  AND Remote_System__c Includes ('AOS')];

    if(iplist!=null && iplist.size()>0){
    for(SVMXC__Installed_Product__c ip:iplist)
    {
    
                Account acc = new Account();
                  
                    acc.AccountName = ip.SVMXC__Company__r.Name;
                    acc.Street = ip.SVMXC__Company__r.Street__c;
                    acc.City = ip.SVMXC__Company__r.City__c;
                    acc.ZipCode = ip.SVMXC__Company__r.ZipCode__c;
                    acc.Country = ip.SVMXC__Company__r.Country__r.Name;
                    acc.GoldenID = ip.SVMXC__Company__r.SEAccountID__c;
                    acc.Latitude = string.valueof(ip.SVMXC__Company__r.SVMXC__Latitude__c);
                    acc.Longitude = string.valueof(ip.SVMXC__Company__r.SVMXC__Longitude__c);
                    
                    
                                    Location loc = new Location();
                    loc.LocationBFOID =ip.SVMXC__Site__c;
                    loc.LocationName = ip.SVMXC__Site__r.Name;
                   loc.GoldenID = ip.SVMXC__Site__r.GoldenLocationId__c;
                    loc.Type = ip.SVMXC__Site__r.SVMXC__Location_Type__c;
        if(ip.SVMXC__Site__r.AirConditionning__c == null || ip.SVMXC__Site__r.AirConditionning__c == '' ||ip.SVMXC__Site__r.AirConditionning__c == 'CLIM_1')
                {
                    loc.AirConditionning = false;
                }
            else{
                    loc.AirConditionning = true;
                }
                    loc.AirDryerInTheSubstation = ip.SVMXC__Site__r.AirDryerInTheSubstation__c;
        if(ip.SVMXC__Site__r.Heating__c == null || ip.SVMXC__Site__r.Heating__c == '' || ip.SVMXC__Site__r.Heating__c == 'HEAT_1')
                {
                    loc.Heating=false;
                }
            else{
                    loc.Heating= true;
                }
                    
    
        InstalledProduct ipaos = new InstalledProduct();
             ipaos.InstalledProductBFOID = ip.id;
             ipaos.Name = ip.Name;
             ipaos.CommissioningDate = ip.CommissioningDateInstallDate__c;
             ipaos.SerialLotNumber = ip.SVMXC__Serial_Lot_Number__c;
             ipaos.CommercialReference = ip.SchneiderCommercialReference__c;
             ipaos.GoldenID = ip.GoldenAssetId__c;
             ipaos.InstalledProductCriticality = ip.InstalledProductCriticality__c;
             ipaos.AnnouncementofServiceObsolescenseDate = ip.WithdrawalDate__c;
             ipaos.EndOfCommercialisationDate = ip.EndOfCommercialisationDate__c;
             ipaos.ServiceObsolecenseDate = ip.ServiceObsolecenseDate__c;
             ipaos.BrandeName = ip.TECH_SDHBRANDID__c;
             ipaos.RangeName = ip.TECH_SDHCategoryId__c;
             ipaos.DeviceTypeName = ip.TECH_SDHDEVICETYPEID__c;
             ipaos.Account =acc;
            ipaos.Location = loc;
            
    
            InstalledProductslist.add(ipaos);

                  
        
                }
            }
    return InstalledProductslist;
    }   

    webService static list<InstalledproductResult> updateAOSAssets(list<InstalledproductResult> Results){

        list<InstalledproductResult> iplistresult = new list<InstalledproductResult>();
        list<InstalledproductResult> iplistresult0 = new list<InstalledproductResult>();
        list<SVMXC__Installed_Product__c>ipplist1 = new list<SVMXC__Installed_Product__c>();
         list<SVMXC__Installed_Product__c>ipplist11 = new list<SVMXC__Installed_Product__c>();
        list<SVMXC__Installed_Product__c>ipplist2 = new list<SVMXC__Installed_Product__c>();
        map<id,SVMXC__Installed_Product__c> ipmap = new map<id,SVMXC__Installed_Product__c>();
        map<id,InstalledproductResult> resmap= new map<id,InstalledproductResult>();
        set<string> set1 = new set<string>();
    if(Results!=null){

            for(InstalledproductResult ipp:Results){
                if(ipp.GoldenID!=null)
                {
                    set1.add(ipp.GoldenID);
                    iplistresult0.add(ipp);
                }
            }
            if(set1.size()>0)
                        ipplist11=[select id,GoldenAssetId__c,ToBeSentForUpdate__c from SVMXC__Installed_Product__c where GoldenAssetId__c in :set1];
            if(iplistresult0!=null && ipplist11!=null){
            
             
                
                for(InstalledproductResult ipp1:iplistresult0){
                for(SVMXC__Installed_Product__c ip3:ipplist11){
                
                if(ipp1.BFOInstalledProductID==ip3.id){
                    
                
                  
                    ip3.id=ipp1.BFOInstalledProductID;
                    ip3.GoldenAssetId__c=ipp1.GoldenID;
                    ip3.ToBeSentForUpdate__c = false;
                    //ipplist2.add(ip3);
                
                            //ipmap.putall(ipplist2);
                    }
                    }
                 }
                }
                //if(!ipmap.isEmpty()){
                update ipplist11;
                /*Database.SaveResult[] sresult= database.update(ipplist11,false);
                     for(Integer k=0;k<sresult.size();k++ )
                            {
                        Database.SaveResult sr =sresult[k];
                        if(sr.isSuccess())
                        {*/
                        
                        if(ipplist11!=null){
                        for(SVMXC__Installed_Product__c ip4:ipplist11){
                        InstalledproductResult ipres= new InstalledproductResult();
                                ipres.ToBeSentForUpdate= ip4.ToBeSentForUpdate__c;
                                ipres.BFOInstalledProductID=ip4.id;
                                ipres.GoldenID=ip4.GoldenAssetId__c;
                        iplistresult.add(ipres);
                        }
                        }
                        //}
                    }
                    
                   // }
           // }       
        
        
     return iplistresult;    
    }   
}