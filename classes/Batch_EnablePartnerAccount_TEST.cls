//Description: Test Class for Batch_EnablePartnerAccount

@isTest
private class Batch_EnablePartnerAccount_TEST {
    static TestMethod void testEnablePortalAccount(){
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Business Accounts
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        Account acc1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '1');
        insert acc1;
        Account acc2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '2');
        insert acc2;
        Account acc3 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '3');
        insert acc3;
        
        //Partner Accounts
        Account par1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '1', PRMUIMSID__c = '1100');
        insert par1;
        Account par2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '2', PRMUIMSID__c = '1111');
        insert par2;
        
        acc3.SEAccountId__c = '2';
        update acc3;
        
        Batch_EnablePartnerAccount batchAcc = new Batch_EnablePartnerAccount();
        database.executeBatch(batchAcc);
    }
}