@isTest
global class VFC_ContactSearch_TEST {
    static testMethod void testinsertionVFC_ContactSearch() {
        Country__c country = Utils_TestMethods.createCountry();
        country.InternationalPhoneCode__c='1340';
        insert country;        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        PageReference vfPage = Page.VFP_ContactSearch;
        Test.setCurrentPage(vfPage);
        Contact c= new Contact();
        c.FirstName='conFirstName';
        c.LastName='conLastName';
        c.LocalFirstName__c='conLocalFirstName';
        c.LocalLastName__c='conLocalLastName';
        c.Street__c='Street';
        c.StreetLocalLang__c='StreetLocalLang';
        c.Country__c = country.Id;
        c.ZipCode__c='ZipCode';
        c.City__c='City';
        c.LocalCity__c='LocalCity';
        c.StateProv__c=stateProv.Id;
        c.POBox__c='POBox';
        c.email='abc@gmail.com';
        c.WorkPhone__c='+91123456';
        c.WorkPhoneExt__c='123456';
        c.MobilePhone='123456';
        insert c;
        //insert business account
        Account a = new Account();
        a.Name = 'TEST VFC_ContactSearch';
        a.AccountLocalName__c = 'Account Local Name';
        a.Street__c = 'Street';
        a.ZipCode__c = 'zipcode';
        a.City__c = 'city';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
        a.StreetLocalLang__c = 'streetlocal';
        a.ToBeDeleted__c = false;
        a.CorporateHeadquarters__c = true;
        a.LeadingBusiness__c = 'BD';
        a.MarketSegment__c = 'BDZ';        
        insert a;
        //insert person account
        Account a1 = new Account();
        a1.FirstName='conFirstName';
        a1.LastName='conLastName';
        a1.LocalFirstName__pc='conLocalFirstName';
        a1.LocalLastName__pc='conLocalLastName';
        a1.Street__c='Street';
        a1.StreetLocalLang__c='StreetLocalLang';
        a1.Country__c = country.Id;
        a1.ZipCode__c='ZipCode';
        a1.City__c='City';
        a1.LocalCity__c='LocalCity';
        a1.StateProvince__c=stateProv.Id;
        a1.POBox__c='POBox';
        a1.PersonEmail='abc@gmail.com';
        a1.WorkPhone__pc='123456';
        a1.WorkPhoneExt__pc='123456';
        a1.PersonMobilePhone='123456';
        a1.RecordTypeId=System.Label.CLDEC12ACC01;
        insert a1;
        //insert legacy account
        LegacyAccount__c legacc=new LegacyAccount__c();
        legacc.LegacyNumber__c='IN00001201';
        legacc.Account__c=a1.Id;
        insert legacc;
    }
    
    static testMethod void testVFC_ContactSearch() {
    //test without passing values to the fields
        Contact con = new Contact();
        ApexPages.currentPage().getParameters().put('ANI', '13402392773');        
        VFC_ContactSearch vfCtrl = new VFC_ContactSearch(new ApexPages.StandardController(con));        
        vfCtrl.accName='TestAccountName';
        Boolean hasNextVar=vfCtrl.hasNext;
        Boolean hasNextVarAcc=vfCtrl.hasNextAcc;
        Boolean hasNextVarBusAcc=vfCtrl.hasNextBusAcc ;
        vfCtrl.accName='TEST VFC_ContactSearch';
        vfCtrl.acctLocalName='Account Local Name';
        vfCtrl.LegacyNo='IN00001201';
        vfCtrl.doSearch();
        vfCtrl.exactmatch='true';
        vfCtrl.doSearch();
        vfCtrl.businessContactCreate();
        vfCtrl.consumerAccountCreate();
    }
     
    static testMethod void TestWithAcctAndContactVFC_ContactSearch() { 
        Contact con = new Contact();
        Country__c country = Utils_TestMethods.createCountry();
        country.InternationalPhoneCode__c='1340';
        insert country;        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        Account a = new Account();
        a.Name = 'TEST VFC_ContactSearch';
        a.AccountLocalName__c = 'Account Local Name';
        a.Street__c = 'Street';
        a.ZipCode__c = 'zipcode';
        a.City__c = 'city';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
        a.StreetLocalLang__c = 'streetlocal';
        a.ToBeDeleted__c = false;
        a.CorporateHeadquarters__c = true;
        a.LeadingBusiness__c = 'BD';
        a.MarketSegment__c = 'BDZ';        
        insert a;
        con.FirstName='conFirstName';
        con.LastName='conLastName';
        con.LocalFirstName__c='conLocalFirstName';
        con.LocalLastName__c='conLocalLastName';
        con.Street__c='Street';
        con.StreetLocalLang__c='StreetLocalLang';
        con.Country__c = country.Id;
        con.ZipCode__c='ZipCode';
        con.City__c='City';
        con.LocalCity__c='LocalCity';
        con.StateProv__c=stateProv.Id;
        con.POBox__c='POBox';
        con.email='abc@gmail.com';
        con.WorkPhone__c='+91123456';
        con.WorkPhoneExt__c='123456';
        con.MobilePhone='123456';
        con.accountid=a.id;
        insert con;
        //insert person account
        Account a1 = new Account();
        a1.FirstName='conFirstName';
        a1.LastName='conLastName';
        a1.LocalFirstName__pc='conLocalFirstName';
        a1.LocalLastName__pc='conLocalLastName';
        a1.Street__c='Street';
        a1.StreetLocalLang__c='StreetLocalLang';
        a1.Country__c = country.Id;
        a1.ZipCode__c='ZipCode';
        a1.City__c='City';
        a1.LocalCity__c='LocalCity';
        a1.StateProvince__c=stateProv.Id;
        a1.POBox__c='POBox';
        a1.PersonEmail='abc@gmail.com';
        a1.WorkPhone__pc='123456';
        a1.WorkPhoneExt__pc='123456';
        a1.PersonMobilePhone='123456';
        a1.RecordTypeId=System.Label.CLDEC12ACC01;
        insert a1;
        system.debug('-->>'+a1);
        Case CaseObj= Utils_TestMethods.createCase(a.Id,Con.Id,'Open');
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        Insert CaseObj;
        System.currentPageReference().getParameters().put('AccountId',a.id);
        System.currentPageReference().getParameters().put('ConsumerAcctId',a1.id);
        System.currentPageReference().getParameters().put('Case',CaseObj.id);
        System.currentPageReference().getParameters().put('WebCompany','testcompany');
        System.currentPageReference().getParameters().put('WebName','testName');
        System.currentPageReference().getParameters().put('WebCountry','India');
        System.currentPageReference().getParameters().put('WebPhone','123456');
        System.currentPageReference().getparameters().put('Email','abc@gmail.com');
        VFC_ContactSearch vfCtrl = new VFC_ContactSearch(new ApexPages.StandardController(con));
        vfCtrl.SearchBasedOnEmailOnly(con.email);
        vfCtrl.contact.FirstName='conFirstName';
        vfCtrl.doSearch();
        vfCtrl.contact.FirstName='';
        vfCtrl.accName='TEST VFC_ContactSearch';
        vfCtrl.acctLocalName='Account Local Name';
        vfCtrl.LegacyNo='IN00001201';
        vfCtrl.assignContactCountryFromANI(con.Workphone__c);
        vfCtrl.doSearch();
        vfCtrl.exactmatch='true';
        vfCtrl.doSearch();
        vfCtrl.CreateANILink();
        vfCtrl.CreateContactLink();
        vfCtrl.LinktoCase();
    }
    
    static testMethod void TestWithBlankValuesScenarioVFC_ContactSearch() { 
        Contact con = new Contact();
        VFC_ContactSearch vfCtrl = new VFC_ContactSearch(new ApexPages.StandardController(con));
        con.LastName='conSearch';
        vfCtrl.doSearch();
        vfCtrl.LegacyNo='';
        vfCtrl.doSearch();
        vfCtrl.LegacyNo='IN00001201';
        vfCtrl.acctLocalName='';
        vfCtrl.doSearch();
        vfCtrl.accName='';
        vfCtrl.acctLocalName='Account Local Name';
        vfCtrl.doSearch();
        con.city__c='';        
        con.LocalCity__c='';
        vfCtrl.doSearch(); 
        vfCtrl.ExactMatch='true';
        vfCtrl.doSearch();
        vfCtrl.LegacyNo='';
        vfCtrl.doSearch();
        vfCtrl.LegacyNo='IN00001201';
        vfCtrl.acctLocalName='';
        vfCtrl.doSearch();
        vfCtrl.accName='';
        vfCtrl.acctLocalName='Account Local Name';
        vfCtrl.doSearch();
        con.city__c='';        
        con.LocalCity__c='';
        vfCtrl.doSearch(); 
    }
    
    static testMethod void testExactMatchVFC_ContactSearch() {  
        Contact con = new Contact();
        VFC_ContactSearch vfCtrl = new VFC_ContactSearch(new ApexPages.StandardController(con));
        ApexPages.currentPage().getParameters().put('ANI', '13402392773');
        vfCtrl.accName='TEST VFC_ContactSearch';
        vfCtrl.acctLocalName='Account Local Name';
        vfCtrl.LegacyNo='IN00001201';
        vfCtrl.exactmatch='true';
        vfCtrl.doSearch();
    }

    /* 
     *  Test of Contact Search when fields are passed as URL parameters.
     *  01/07/2014  @Cecile Lartaud
     */
    static testMethod void testUrlParametersVFC_ContactSearch() { 
        // Create a country //
        Country__c country = Utils_TestMethods.createCountry();
        country.CountryCode__c = 'Z0';
        insert country; 
        
        // Create a state //
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        stateProv.CountryCode__c = country.CountryCode__c;
        insert stateProv;
        
        // Create a business account //
        Account a = new Account();
        a.Name = 'TEST VFC_ContactSearch';
        a.AccountLocalName__c = 'TEST VFC_ContactSearch';
        a.Street__c = '18 Victoria Square';
        a.ZipCode__c = '21702';
        a.City__c = 'Frederick';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
        a.StreetLocalLang__c = 'streetlocal';
        a.ToBeDeleted__c = false;
        a.CorporateHeadquarters__c = true;
        a.LeadingBusiness__c = 'BD';
        a.MarketSegment__c = 'BDZ';        
        insert a;
        
        // Create a contact //
        Contact c= new Contact();
        c.FirstName='Test';
        c.LastName='vfc_contactSearch';
        c.LocalFirstName__c='Test';
        c.LocalLastName__c='vfc_contactSearch';
        c.AccountId = a.Id;
        c.Street__c='18%20Victoria%20Square';
        c.StreetLocalLang__c='18%20Victoria%20Square';
        c.Country__c = country.Id;
        c.ZipCode__c='21702';
        c.City__c='Frederick';
        c.LocalCity__c='Frederick';
        c.StateProv__c=stateProv.Id;
        c.POBox__c='987';
        c.email='test.vfc@test.fr';
        c.WorkPhone__c='0123456998';
        c.WorkPhoneExt__c='123456';
        c.MobilePhone='123456';
        insert c;
        

        Contact con = new Contact();
        con.Email='test.vfc@test.fr';
        PageReference vfPage2 = Page.VFP_ContactSearch;
        vfPage2.getParameters().put('pcEmail', 'test.vfc@test.fr');
        Test.setCurrentPage(vfPage2);
        VFC_ContactSearch vfCtrl2 = new VFC_ContactSearch(new ApexPages.StandardController(con));
        vfCtrl2.doSearch();
        vfCtrl2.getQueryStringAcc();
        vfCtrl2.getQueryStringAccExactMatch();
        vfCtrl2.getQueryString();
        vfCtrl2.getQueryStringExactMatch();
        
        vfPage2 = Page.VFP_ContactSearch;
        vfPage2.getParameters().put('pcLastName', 'vfc_contactSearch');
        vfPage2.getParameters().put('pcEmail', 'test.vfc@test.fr');
        Test.setCurrentPage(vfPage2);
        vfCtrl2 = new VFC_ContactSearch(new ApexPages.StandardController(con));
        vfCtrl2.doSearch();
        vfCtrl2.getQueryStringAcc();
        vfCtrl2.getQueryStringAccExactMatch();
        vfCtrl2.getQueryString();
        vfCtrl2.getQueryStringExactMatch();
        
        // Call the Visualforce page with parameters //
        PageReference vfPage = Page.VFP_ContactSearch;
        vfPage.getParameters().put('pcEmail', 'test.vfc@test.fr');
        vfPage.getParameters().put('pcCountry', country.CountryCode__c);
        vfPage.getParameters().put('pcFirstName', 'Test');
        vfPage.getParameters().put('pcLastName', 'vfc_contactSearch');
        vfPage.getParameters().put('pcLocFirstName', 'Test');
        vfPage.getParameters().put('pcLocLastName', 'vfc_contactSearch');
        vfPage.getParameters().put('pcPhone', '0123456998');
        vfPage.getParameters().put('pcEmail', 'test.vfc@test.fr');
        vfPage.getParameters().put('paName', 'TEST VFC_ContactSearch');
        vfPage.getParameters().put('paLocName', 'TEST VFC_ContactSearch');
        vfPage.getParameters().put('paLegacyNo', '011145');
        vfPage.getParameters().put('pcStreet', '18%20Victoria%20Square');
        vfPage.getParameters().put('pcStreetLocal', '18%20Victoria%20Square');
        vfPage.getParameters().put('pcZipCode', '21702');
        vfPage.getParameters().put('pcCity', 'Frederick');
        vfPage.getParameters().put('pcLocalCity', 'Frederick');
        vfPage.getParameters().put('pcStateProvince', stateProv.Name);
        vfPage.getParameters().put('pcPOBox', '987');
        Test.setCurrentPage(vfPage);
        VFC_ContactSearch vfCtrl = new VFC_ContactSearch(new ApexPages.StandardController(new Contact()));
        vfCtrl.doSearch();        
    }

}