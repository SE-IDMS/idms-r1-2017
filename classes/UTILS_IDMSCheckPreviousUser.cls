//****************************************************************************************************************** 
// Overall Purpose    : Check the if  new user email is already 
//						in the database the change the username of existing   
//						user.
// Created by         :Mohammad Naved 
// Created Date       : 19/Jan/2016
// Modified by         Name        | Date  "YYYY/MM/DD"   | Version    |  Ticket #  or   BR #  |  Purpose     
// 
// 						Naved			2016/01/19			  V1			BFOID-418						
//******************************************************************************************************************


public class UTILS_IDMSCheckPreviousUser {
    public UTILS_IDMSCheckPreviousUser() {
        
    }
    /*
	**Method Name -checkPreviousUser
	**
	**Parameter -
	**
	**Purpose -
    */
    public Static Boolean checkPreviousUser(String strEmail){
    	list<User> lstUser = new list<User>();

    	for(User u:[select Id,
    					   Email,
    					   UserName,
    					   IsActive 
    					   from User 
    					   where 
    					   Email =: strEmail 
    					   AND IsActive = false ]){
    		
    			u.UserName =u.UserName+String.valueOf(System.today());
    			lstUser.add(u);
    		

    	}
    	if(lstUser.size()>0 && !lstUser.isEmpty()){
    		update lstUser;
    		return true;
    	}
    	
    	return false;	

    	

    	
    }
}