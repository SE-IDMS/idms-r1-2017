public class OpportunityNotificationBeforeInsertClass {

    public static void setProjectNameAndCloseDate (List<OpportunityNotification__c> triggerNew, Map<Id, OpportunityNotification__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set Project Name field on Insert - Written : Sept 30, 2015
            
            This functionality should only run for the Mobile RecordType
            
            Updated Feb 16, 2016 - Added functionality to set the Close Date to TODAY + 30 days if it was not changed from TODAY (for BR-9247)
        */
        
        RecordType MobileRTId = [SELECT Id FROM RecordType WHERE SObjectType = 'OpportunityNotification__c' AND DeveloperName = 'Mobile' LIMIT 1];
        
        for (OpportunityNotification__c oppNotif : triggerNew) {
        
            if (oppNotif.RecordTypeId == MobileRTId.Id) {
            
                if (oppNotif.Project_Name__c != null) {                 
                    
                    String newProjectName = '';
                    
                    if (oppNotif.Project_Name__c != null)
                        newProjectName = oppNotif.Project_Name__c;
                    
                    if (oppNotif.Location_Name__c != null)
                        newProjectName += ' - ' + oppNotif.Location_Name__c;
                    
                    if (oppNotif.Account_Name__c != null)
                        newProjectName += ' - ' + oppNotif.Account_Name__c;
                    
                    if (oppNotif.LeadingBusinessBU__c != null)
                        newProjectName += ' - ' + oppNotif.LeadingBusinessBU__c;
                        
                    if (oppNotif.Name != 'FROM STANDALONE') {
                        if (newProjectName.length() > 255)        
                            oppNotif.Project_Name__c = newProjectName.substring(0,254);
                        else
                            oppNotif.Project_Name__c = newProjectName;   
                    
                        if (newProjectName.length() > 80)
                            oppNotif.Name = newProjectName.substring(0,79);
                        else
                            oppNotif.Name = newProjectName;
                            
                    } else {
                        
                        if (oppNotif.Project_Name__c.length() > 80)
                            oppNotif.Name = oppNotif.Project_Name__c.substring(0,79);
                        else
                            oppNotif.Name = oppNotif.Project_Name__c;
                    } 
                }
                // for BR-9247 
                if ((oppNotif.CloseDate__c == null) || (oppNotif.CloseDate__c != null && oppNotif.CloseDate__c <= Date.today())) {
                    if (oppNotif.CloseDate__c == null)
                        oppNotif.CloseDate__c = Date.today().addDays(30);  
                    else
                        oppNotif.CloseDate__c = oppNotif.CloseDate__c.addDays(30);  
                }
            }
        }       
    }
}