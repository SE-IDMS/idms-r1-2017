public with sharing class AP_PRMCFL_ContentLifecycleTriggerHandler {

/*
public static void onAfterInsert() {
}*/

public static void sendEmailtoComXAdmin(String idRecord, String OtherDeviceType, String OtherProductType) {
  System.debug('*** sendEmailtoComXAdmin idRecord=' + idRecord + ' OtherDeviceType=' + OtherDeviceType + ' OtherProductType=' + OtherProductType);


  Id templateId = Label.CLOCT15CF027;
  EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where Id =: templateId];
  String subject = emailTemplate.Subject;
  String plainBody = emailTemplate.Body;

  if (OtherProductType==null ) {
    plainBody = plainBody.replace('PATTERN_OTHER2', '');  
  }  else {
    plainBody = plainBody.replace('PATTERN_OTHER2', '- new value for Other product brand: ' + OtherProductType);  
  }


  if ( OtherDeviceType==null ) {
    plainBody = plainBody.replace('PATTERN_OTHER1', '');  
  } else {
    plainBody = plainBody.replace('PATTERN_OTHER1', '- new value for Other device type: ' + OtherDeviceType);  
  }

  plainBody = plainBody.replace('PATTERN_LINK', Label.CLOCT15CF061 + idRecord); 


  List<Messaging.SingleEmailMessage> mails =   new List<Messaging.SingleEmailMessage>();

  String listOfEmailToSend = Label.CLOCT15CF030;
  List<String> targetIds = listOfEmailToSend.split(',');
  for(Id targetId: targetIds) {
    Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
    mail.setSenderDisplayName('bFO Platform - Com\'X Library');
    mail.settargetObjectId(targetId);
    mail.setSaveAsActivity(false); 
    mail.setSubject(subject);
    mail.setPlainTextBody(plainBody);
    mails.add(mail);
  } //for
  
  
   
  // Step 6: Send all emails in the master list
  Messaging.sendEmail(mails);
  
}
   
   
}