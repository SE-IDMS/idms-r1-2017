/*
    Author          : Accenture Team
    Date Created    : 03/08/2011
    Description     : Test class for AP22_SupportRequest class.
*/
@isTest
private class AP22_SupportRequest_TEST 
{
    Static Account accounts;
    static user user1;
    Static Opportunity opportunity,opportunity1,opportunity2;
    Static opportunityTeamMember opportunityTeam1,opportunityTeam2;
    Static list<OPP_supportRequest__c> supportrqsts = new list<OPP_supportRequest__c>();
    Static SolutionCenter__c solutionCenter2,solutionCenter1;
    Static OPP_sellingCenter__c sellingCenter;
    Static list<OPP_supportRequest__c> supportreq = new list<OPP_supportRequest__c>();
    static User sysadmin2;
    Static list<OPP_supportRequest__c> supports = new list<OPP_supportRequest__c>();           
    
    static
    {
        System.runAs(new User(Id = Userinfo.getUserId())) {
        UserandPermissionSets user1userandpermissionsetrecord=new UserandPermissionSets('TestUse1','SE - Salesperson');
        user1 = user1userandpermissionsetrecord.userrecord;
        insert user1; 
        }

    }
    
    static testmethod void suppReqTEST()
    {
        accounts = Utils_TestMethods.createAccount();
        insert accounts; 
            OppAgreement__c agreementObj= Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),accounts.Id);         
                agreementObj.PhaseSalesStage__c = '3 - Identify & Qualify';   
                agreementObj.AgreementValidFrom__c = Date.today();         
                agreementObj.FrameOpportunityInterval__c = 'Monthly';
                agreementObj.SignatureDate__c = Date.today();
                agreementObj.Agreement_Duration_in_month__c = '3';
                agreementObj.AgreementType__c = 'Frame Agreement';
                insert agreementObj;
        opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        opportunity.OpptyPriorityLevel__c='standard';
        opportunity.AgreementReference__c = agreementObj.Id;
        insert opportunity; 
        
        opportunityTeam1 = new opportunityTeamMember(OpportunityID=opportunity.id,userId=UserInfo.getUserId());
        insert opportunityTeam1;
        
        sellingCenter = Utils_TestMethods.createSellingCenter();
        insert SellingCenter;
        
        solutionCenter1 = Utils_TestMethods.createSolutionCenter();
        solutionCenter1.IsASolutionCenter__c ='Yes';
        insert solutionCenter1;
        
        for(integer i=0;i<200;i++)
        {
            OPP_supportRequest__c supReq = Utils_TestMethods.createSupportRequest(opportunity.Id);
            supReq.SolutionCenter__c=solutioncenter1.id;
            supReq.PMTender__c= UserInfo.getUserId();
            supReq.AppDevEngineer__c= user1.Id;
            supReq.SolutionArchitect__c= UserInfo.getUserId();
            supReq.SupportUser1__c= UserInfo.getUserId();
            supReq.SupportUser2__c= UserInfo.getUserId();
            supReq.AssignedToUser__c=UserInfo.getUserId();
            supportrqsts.add(supReq);
        }   
        insert supportrqsts;
         supportrqsts[0].AssignedToUser__c = UserInfo.getUserId();
        supportrqsts[0].PMTender__c= UserInfo.getUserId();
        supportrqsts[0].AppDevEngineer__c= UserInfo.getUserId();
        supportrqsts[0].SolutionArchitect__c= UserInfo.getUserId();
        supportrqsts[0].SupportUser1__c= UserInfo.getUserId();
        supportrqsts[0].SupportUser2__c= UserInfo.getUserId();
        supportrqsts[0].SellingCenter__c=sellingCenter.id;
        update supportrqsts[0];
        
        OPP_supportRequest__c supReqsts = Utils_TestMethods.createSupportRequest(opportunity.Id);
        supReqsts.SolutionCenter__c=solutioncenter1.id;
        supReqsts.PMTender__c= UserInfo.getUserId();
        supReqsts.AssignedToUser__c=UserInfo.getUserId();
        supReqsts.recordtypeId='012A0000000nYNR';
        insert supReqsts;
        
        Test.startTest();
            OPP_QuoteLink__c ql = new OPP_QuoteLink__c(OpportunityName__c = opportunity.Id, QuoteStatus__c = 'Approved', ActiveQuote__c = true);
            insert ql; 
            supReqsts.QuoteLink__c=ql.Id;
            supReqsts.TypeOfOffer__c='Bid to Bid';
            update supReqsts;
            
        OPP_supportRequest__c supReqsts2 = Utils_TestMethods.createSupportRequest(opportunity.Id);
        supReqsts2.SellingCenter__c=sellingCenter.id;
        supReqsts2.PMTender__c= UserInfo.getUserId();
        supReqsts2.AssignedToUser__c=UserInfo.getUserId();
        supReqsts2.AppDevEngineer__c=UserInfo.getUserId();
        supReqsts2.SolutionArchitect__c=UserInfo.getUserId();
        supReqsts2.SupportUser1__c=UserInfo.getUserId();
        supReqsts2.SupportUser2__c=UserInfo.getUserId();
        supReqsts2.recordtypeId='012A0000000nYNR';
        insert supReqsts2;
        Test.StopTest();
    }
    
}