@isTest
public with sharing class Utils_TestMethods_Connect
{
    public static User createStandardConnectUser(String alias)
    {
        Profile profile = [select id from profile where name='Standard User'];
        User user = new User(alias = alias, email=alias + '@schneider-electric.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profile.Id,
            //timezonesidkey='Europe/London', username=alias + '@bridge-fo-TEST.com',SEContactID__c = sesa);
            timezonesidkey='Europe/London', username=alias + '@bridge-fo-TEST.com',firstname='User');
        return user;
    }
    public static Initiatives__c createInitiative()
    {
        Initiatives__c inti=new Initiatives__c(Name='initiative');
        return inti;
    }
    
}