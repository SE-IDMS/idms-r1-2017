public with sharing class ConnectEntityTriggers
{


    Public Static void ConnectEntityBeforeInsertUpdate_ValidateEntityRecord(List<Connect_Entity_Progress__c> CEntity){
    
    
    
  list<Connect_Entity_Progress__c> Eprogress=new list<Connect_Entity_Progress__c>();
    
         Eprogress=[select Scope__c,Entity__c,GSC_Region__c,Partner_Region__c,Program_Name__c,Report_Year__c,Smart_Cities_Division__c from Connect_Entity_Progress__c];
            for(Connect_Entity_Progress__c CEn:CEntity){
                CEn.ValidateEntityInsertUpdate__c=false;
                for(Connect_Entity_Progress__c EP:Eprogress){
                    if(EP.Program_Name__c == CEn.Program_Name__c && EP.Report_Year__c == CEn.Report_Year__c){
                        if(CEn.GSC_Region__c == null && CEn.Partner_Region__c == null && CEn.Smart_Cities_Division__c == null && CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c){
                    
                           CEn.ValidateEntityInsertUpdate__c=true;
                        }
                                         
                        if(!(CEn.GSC_Region__c == null)){
                            if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c && CEn.GSC_Region__c == EP.GSC_Region__c)
                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                        }
                        
                         if(!(CEn.Smart_Cities_Division__c == null)){
                         system.debug('----'+EP.Smart_Cities_Division__c+'------'+CEn.Smart_Cities_Division__c);
                            if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c && CEn.Smart_Cities_Division__c == EP.Smart_Cities_Division__c){
                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                            }
                        }
                                
                        if(!(CEn.Partner_Region__c == null)){
                            if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c && CEn.Partner_Region__c == EP.Partner_Region__c)
                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                        }
                   
                
                    }
                }
            }
    }
 
    Public Static void ConnectEntityBeforeInsertUpdate_ValidateScopeEntity(List<Connect_Entity_Progress__c> CEntity)
             {
             
      
                System.Debug('****** ConnectEntityBeforeInsertUpdate Start ****');
              list<Project_NCP__c> Program = new List<Project_NCP__c>(); 
              
              
              
              integer PW;
              integer GB;
              integer GSC;
              integer PAR;
              integer GF;
              integer SCDs;
              
              Program = [select Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c, Name,Partner_Region__c,Year__c,Smart_Cities_Division__c from Project_NCP__c];
              for(Connect_Entity_Progress__c CE:CEntity){
              
              
                     PW = 0; 
                     GB = 0;
                     GSC = 0;
                     GF = 0;
                     PAR = 0;
                     SCDs = 0;
                     CE.ValidateScopeInsertUpdate__c = false;
                     for(Project_NCP__c P:Program){
                     
                     // Check if the Scope in Null in Program or Entity
                     
                      if(P.Name == CE.Program_Name__c && P.Year__c == CE.Report_Year__c ) {
                      if(P.Global_Functions__c != null){
                                  if((CE.Scope__c == Label.Connect_Global_Functions) && !(P.Global_Functions__c.Contains(CE.Entity__c)))
                                  GF = GF + 1;
                                  }
                                  else if((P.Global_Functions__c == null)&& (CE.Scope__c == Label.Connect_Global_Functions))
                            GF = GF + 1;
                          if(P.Global_Business__c != null){
                                  if((CE.Scope__c == Label.Connect_Global_Business) && !(P.Global_Business__c.Contains(CE.Entity__c)))
                                  GB = GB + 1;
                                  }
                                  else if((P.Global_Business__c == null)&& (CE.Scope__c == Label.Connect_Global_Business))
                            GB = GB + 1;
                          if(P.Power_Region__c != null){
                                  if((CE.Scope__c == Label.Connect_Power_Region) && !(P.Power_Region__c.Contains(CE.Entity__c)))
                                  PW = PW + 1;
                                  }
                                  else if((P.Power_Region__c == null)&& (CE.Scope__c == Label.Connect_Power_Region))
                            PW = PW + 1;
                            
                            // GSC Region Validation
                            
                            if((P.GSC_Regions__c !=null)&& (CE.GSC_Region__c !=null)){
                               if((CE.Scope__c == Label.Connect_Global_Functions) && (CE.Entity__c == Label.Connect_GSC) && !(P.GSC_Regions__c.Contains(CE.GSC_Region__c)))
                               GSC = GSC + 1;
                              } 
                               else if((P.GSC_Regions__c == null) && (CE.Entity__c == Label.Connect_GSC) )
                               GSC = GSC + 1;
                               
                                // SCD Region Validation
                            
                            if((P.Smart_Cities_Division__c !=null)&& (CE.Smart_Cities_Division__c !=null)){
                               if((CE.Scope__c == Label.Connect_Global_Business) && (CE.Entity__c == Label.Connect_Smart_Cities) && !(P.Smart_Cities_Division__c.Contains(CE.Smart_Cities_Division__c)))
                               SCDs = SCDs + 1;
                              } 
                               else if((P.Smart_Cities_Division__c == null) && (CE.Entity__c == Label.Connect_Smart_Cities) )
                               SCDs = SCDs + 1;
                               
                                // Partner Region Validation
                             if((P.Partner_Region__c !=null)&& (CE.Partner_Region__c !=null)){
                               if((CE.Scope__c == Label.Connect_Global_Business) && (CE.Entity__c == Label.Connect_Partner) && !(P.Partner_Region__c.Contains(CE.Partner_Region__c)))
                               PAR = PAR + 1;
                              } 
                               else if((P.Partner_Region__c == null) && (CE.Entity__c == Label.Connect_Partner) )
                               PAR = PAR + 1;
                               } // if Ends
                               
                               if((GF > 0) ||  (GB > 0) ||  (PW > 0) || (GSC > 0) || (PAR > 0) || (SCDs > 0))
                                   CE.ValidateScopeInsertUpdate__c = True;
                               
                              } // For Ends
                             } //For Ends

                  }   
            
}