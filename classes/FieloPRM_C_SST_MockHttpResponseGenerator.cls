@isTest
global class FieloPRM_C_SST_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        String body = req.getBody();
        
          String returnedBody = '';  
        if(body.indexOf('<doc:getDocumentList>')!=-1){
            returnedBody = BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentList_Response');
        }else if(body.indexOf('<doc:getDocDetailsByRef>')!=-1){
            returnedBody = BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocDetails_Response');
        }else if(body.indexOf('<doc:getDocDetails>')!=-1){
            returnedBody = BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocDetails_Response');
        }else if(body.indexOf('<doc:getDocumentCount>')!=-1){
            
            if(body.indexOf('countBy="docType"')!=-1){
                returnedBody = BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentCount_docType_Response');    
            }else if(body.indexOf('countBy="docTypeGroup"')!=-1){
                returnedBody = BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentCount_docTypeGroup_Response');    
            }else if(body.indexOf('countBy="range"')!=-1){
                returnedBody = BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentCount_range_Response');    
            }
        }
        
            
            
        // Create a fake response
        // 
        HttpResponse res = new HttpResponse();
        if(returnedBody.equals('')){
            res.setHeader('Content-Type', 'text/plain');
            res.setBody('error can\'t handle :'+body  );
            res.setStatusCode(500);
        }else{
            res.setHeader('Content-Type', 'application/soap+xml; charset=UTF-8');
            res.setBody(returnedBody);
            res.setStatusCode(200);    
        }
        
        
        
        return res;
    }
    

}