@isTest
private class VFC_ServiceCertificateCreationCheck_Test 
{ 
    public static testmethod void test()
    {
       
        
        //create data
         //country1
             country__c coun1 = new country__C();
             coun1.name = 'Test'; 
         //account1
             Account acc1 = new account();
             acc1.Name = 'testaccount'; 
             acc1.Country__c = coun1.id;
            //service contract1
             SVMXC__Service_Contract__c sc1= new SVMXC__Service_Contract__c();
            sc1.name='testContract';
            sc1.SVMXC__Start_Date__c = date.newinstance(2016,4,4);
            sc1.SVMXC__End_Date__c = date.newinstance(2017,4,5);
            sc1.Status__c = 'Pending Commercial Validation' ;
            sc1.LeadingBusinessBU__c = 'Energy';
            sc1.SoldtoAccount__c = acc1.id;
            //insert sc1;
            system.assert(sc1 != null);
            //service line1
             SVMXC__Service_Contract__c scl1= new SVMXC__Service_Contract__c();
             scl1.name = 'testline1' ; 
             scl1.ParentContract__c = sc1.id;
             scl1.SVMXC__Start_Date__c = date.newinstance(2016,4,5);
            scl1.SVMXC__End_Date__c = date.newinstance(2017,3,7);
            scl1.SVMXC__Company__c = acc1.id;
            insert scl1;
            
            
            PageReference pageRef1 = Page.VFP_ServiceCertificateCreationCheck; 
            Test.setCurrentPage(pageRef1);
            
            system.currentPageReference().getParameters().put('SId',sc1.id);
            //controller
             VFC_ServiceCertificateCreationCheck c1 = new VFC_ServiceCertificateCreationCheck(new ApexPages.StandardController(sc1));
            c1.cancel();
            
            //country2
             country__c coun2 = new country__C();
             coun2.name = 'Test2'; 
         //account2
             Account acc2 = new account();
             acc2.Name = 'testaccount2'; 
             acc2.Country__c = coun2.id;
             //product2
             product2 proda = new product2();
             proda.Name = 'testProductA';
             //insert proda;
             //location2
             SVMXC__Site__c loca = new SVMXC__Site__c();
             loca.Name = 'TestLocationA' ;
             loca.SVMXC__Location_Type__c = 'Site';
             loca.SVMXC__Account__c = acc2.id;
             //insert loca; 
            //service contract2
             SVMXC__Service_Contract__c sc2= new SVMXC__Service_Contract__c();
            sc2.name='testContract2';
            sc2.SVMXC__Start_Date__c = date.newinstance(2016,3,2);
            sc2.SVMXC__End_Date__c = date.newinstance(2017,3,4);
            sc2.Status__c = 'Validated' ;
            sc2.LeadingBusinessBU__c = 'IT';
            //insert sc2;
            system.assert(sc2 != null);
            //service line2
             SVMXC__Service_Contract__c scl2= new SVMXC__Service_Contract__c();
             scl2.name = 'testline2' ; 
             scl2.ParentContract__c = sc2.id;
             scl2.SVMXC__Start_Date__c = date.newinstance(2016,1,5);
            scl2.SVMXC__End_Date__c = date.newinstance(2017,3,7);
            scl2.SVMXC__Company__c = acc2.id;
            scl2.SKUComRef__c = proda.id ;
            insert scl2;
            //installed product 
            SVMXC__Installed_Product__c iproda = new SVMXC__Installed_Product__c();
            iproda.name = 'Test Installed Product';
            iproda.SVMXC__Company__c = acc2.id ; 
            iproda.SVMXC__Service_Contract__c = scl2.id; 
            iproda.SVMXC__Site__c = loca.id; 
            insert iproda;
            //covered product 
            SVMXC__Service_Contract_Products__c ciproda = new SVMXC__Service_Contract_Products__c(); 
            ciproda.SVMXC__Service_Contract__c = scl2.id;
            ciproda.SVMXC__Start_Date__c = date.newinstance(2016,3,2);
            ciproda.SVMXC__End_Date__c =  date.newinstance(2016,3,25);
            ciproda.SVMXC__Installed_Product__c = iproda.id;
            ciproda.AssetLocation__c = loca.id;
            insert ciproda ;
            
            //do here 
            
            PageReference pageRef2 = Page.VFP_ServiceCertificateCreationCheck; 
            Test.setCurrentPage(pageRef2);
            
            system.currentPageReference().getParameters().put('SId',sc2.id);
            
                     VFC_ServiceCertificateCreationCheck c2 = new VFC_ServiceCertificateCreationCheck(new ApexPages.StandardController(sc2));
            c2.cancel();
            
            
             Test.setCurrentPage(pageRef2);
            
            system.currentPageReference().getParameters().put('SId',sc2.id);
            
            c2.continue1();
            
            //country3
             country__c coun3 = new country__C();
             coun3.name = 'Test2'; 
         //account3
             Account acc3 = new account();
             acc3.Name = 'testaccount3'; 
             acc3.Country__c = coun3.id;
             //product2
             product2 prodb = new product2();
             prodb.Name = 'testProductA';
             //insert prodb;
             //location3
             SVMXC__Site__c locb = new SVMXC__Site__c();
             loca.Name = 'TestLocationB' ;
             loca.SVMXC__Location_Type__c = 'Site';
             loca.SVMXC__Account__c = acc3.id;
             //insert locb; 
            //service contract3
             SVMXC__Service_Contract__c sc3= new SVMXC__Service_Contract__c();
            sc3.name='testContract3';
            sc3.SVMXC__Start_Date__c = date.newinstance(2016,3,2);
            sc3.SVMXC__End_Date__c = date.newinstance(2018,3,4);
            sc3.Status__c = 'Validated' ;
            sc3.LeadingBusinessBU__c = 'IT';
            //insert sc3;
            system.assert(sc3 != null);
            //service line3
             SVMXC__Service_Contract__c scl3= new SVMXC__Service_Contract__c();
             scl3.name = 'testline3' ; 
             scl3.ParentContract__c = sc3.id;
             scl3.SVMXC__Start_Date__c = date.newinstance(2016,1,5);
            scl3.SVMXC__End_Date__c = date.newinstance(2018,3,7);
            scl3.SVMXC__Company__c = acc2.id;
            scl3.SKUComRef__c = proda.id ;
            insert scl3;
            //installed product 
            SVMXC__Installed_Product__c iprodb = new SVMXC__Installed_Product__c();
            iprodb.name = 'Test Installed ProductB';
            iprodb.SVMXC__Company__c = acc3.id ; 
            iprodb.SVMXC__Service_Contract__c = scl3.id; 
            iprodb.SVMXC__Site__c = locb.id; 
            insert iprodb;
            //covered product 
            SVMXC__Service_Contract_Products__c ciprodb = new SVMXC__Service_Contract_Products__c(); 
            ciprodb.SVMXC__Service_Contract__c = scl3.id;
            ciprodb.SVMXC__Start_Date__c = date.newinstance(2016,3,2);
            ciprodb.SVMXC__End_Date__c =  date.newinstance(2016,3,25);
            ciprodb.SVMXC__Installed_Product__c = iprodb.id;
            ciprodb.AssetLocation__c = locb.id;
            insert ciprodb ;
            
            
            // do here 
            PageReference pageRef3 = Page.VFP_ServiceCertificateCreationCheck; 
            Test.setCurrentPage(pageRef3);
            
            system.currentPageReference().getParameters().put('SId',sc3.id);
            
            VFC_ServiceCertificateCreationCheck c3 = new VFC_ServiceCertificateCreationCheck(new ApexPages.StandardController(sc3));
            c3.init();
            
            
            
        
    }
}