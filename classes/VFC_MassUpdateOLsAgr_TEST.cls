@isTest
private class VFC_MassUpdateOLsAgr_TEST
{     
    static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
    static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
    static User user,newUser,newUser2 ;
    static Country__c newCountry;
    static OppAgreement__c objAgree;
    static Account acc;
    static Opportunity objOpp,opp2,opp3,opp4;
    static OPP_ProductLine__c pl1, pl2, pl3, pl4;
    static OppAgreement__c objAgree2,objAgree3;
    static List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();   
    static List<OppAgreement__c> oa = new List<OppAgreement__c>();
    static OPP_Product__c  prod = new OPP_Product__c();
    static
    {
        VFC61_OpptyClone.isOpptyClone = true;
        acc = Utils_TestMethods.createAccount();
        acc.City__c = 'testCity';
        insert acc;
        
        opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName = '3 - Identify & Qualify';
        insert opp2; 
        objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree2.AgreementValidFrom__c = Date.today();         
        objAgree2.FrameOpportunityInterval__c = 'Monthly';
        objAgree2.SignatureDate__c = Date.today();
        objAgree2.Agreement_Duration_in_month__c = '3';
        insert objAgree2;
        
        prod.ProductLine__c = null;
        prod.BusinessUnit__c = 'EcoBuilding';
        insert prod;
        opp2.AgreementReference__c = objAgree2.Id;
        update opp2;        
        opp3 = Utils_TestMethods.createOpportunity(acc.Id);   
        opp3.StageName = '3 - Identify & Qualify';     
        opp3.AgreementReference__c = objAgree2.Id;        
        insert opp3; 
        opp4 = Utils_TestMethods.createOpportunity(acc.Id);        
        opp4.StageName = '3 - Identify & Qualify';
        opp4.AgreementReference__c = objAgree2.Id;        
        insert opp4;
        //Inserts the Product Lines        
            pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.TECH_IsCreatedFromAgr__c = true;
            pl1.ProductBU__c = 'BU';
            insert pl1;
            
            pl2 = Utils_TestMethods.createProductLine(opp2.Id);
            pl2.Quantity__c = 10;
            pl2.TECH_IsCreatedFromAgr__c = true;
            pl2.TECH_OriginalId__c = pl1.Id;            
            pl1.ProductBU__c = 'BU';
            insert pl2;
            
        
            pl3 = Utils_TestMethods.createProductLine(opp3.Id);
            pl3.Quantity__c = 10;
            insert pl3;
            
            pl4 = Utils_TestMethods.createProductLine(opp3.Id);
            pl4.Quantity__c = 10;
            pl4.TECH_OriginalId__c = pl3.Id;            
            insert pl4;
        
        
        objAgree3 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree3.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree3.AgreementValidFrom__c = Date.today();         
        objAgree3.FrameOpportunityInterval__c = Label.CL00221;
        objAgree3.SignatureDate__c = Date.today();
        objAgree3.Agreement_Duration_in_month__c = '2';
        insert objAgree3;        
    }     

    static testmethod void testMassUpdate5()
    {   
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(oa);
        ApexPages.currentPage().getParameters().put('id',objAgree2.Id);  
        ApexPages.currentPage().getParameters().put('selectedopplne',pl1.Id);
        ApexPages.currentPage().getParameters().put('changeProduct','true');        
        VFC_MassUpdateOLsAgr massupdate = new VFC_MassUpdateOLsAgr(controller);
        VFC_MassUpdateOLsAgr.ProductLine innerObject= new VFC_MassUpdateOLsAgr.ProductLine(pl1);
        massupdate.productLines.add(innerObject);
        massupdate.NextPage();
        
        innerObject= new VFC_MassUpdateOLsAgr.ProductLine(pl1);
        innerObject.selected = true;
        massupdate.productLines.add(innerObject);
        massupdate.NextPage();
        
        //massupdate.productLines.add(new ProductLine(pl1));
        massupdate.pgMsg = 'testMessage';
        massupdate.selStatus = 'test';
        massupdate.editLine = true;
        massupdate.selProdBU = 'BU';
        massupdate.prodLine = null;
        massupdate.search();
        massupdate.getprodBUS();
        massupdate.getLineStatus();
        
        massupdate.clearSearch();
        massupdate.save();
        massupdate.editProductLine();
        massupdate.cancel();
        massupdate.ChangeProduct();
        
    }
    static testmethod void testMassUpdate3()
    {   
        VFC61_OpptyClone.isOpptyClone = true;        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(oa);
        ApexPages.currentPage().getParameters().put('id',objAgree2.Id);  
        ApexPages.currentPage().getParameters().put('selectedopplne',pl1.Id);
        ApexPages.currentPage().getParameters().put('changeProduct','true');        
        VFC_MassUpdateOLsAgr massupdate = new VFC_MassUpdateOLsAgr(controller);
        massupdate.pgMsg = 'testMessage';
        massupdate.selProdBU = 'BU';
        massupdate.prodLine = null;
        massupdate.search();
   }
   
   static testmethod void testMassUpdate4()
    {   
        VFC61_OpptyClone.isOpptyClone = true;        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(oa);
        ApexPages.currentPage().getParameters().put('id',objAgree2.Id);  
        ApexPages.currentPage().getParameters().put('selectedopplne',pl1.Id);
        ApexPages.currentPage().getParameters().put('changeProduct','true');        
        VFC_MassUpdateOLsAgr massupdate = new VFC_MassUpdateOLsAgr(controller);


        massupdate.pgMsg = 'testMessage';
        massupdate.selProdBU = 'BU';
        massupdate.prodLine = 'TEST';
        massupdate.selStatus = 'Pending';
        massupdate.search();
   }
   
   static testmethod void testMassUpdate6()
    {   
        VFC61_OpptyClone.isOpptyClone = true;        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(oa);
        ApexPages.currentPage().getParameters().put('id',objAgree2.Id);  
        ApexPages.currentPage().getParameters().put('addProduct',pl1.Id);
        ApexPages.currentPage().getParameters().put('selectedopp',opp2.Id);        
        VFC_MassUpdateOLsAgr massupdate = new VFC_MassUpdateOLsAgr(controller);
        massupdate.pgMsg = 'testMessage';
        massupdate.addProductLines();
        massupdate.AddProduct();
        
        
        controller = new ApexPages.StandardSetController(oa);
        ApexPages.currentPage().getParameters().put('id',objAgree3.Id);  
        massupdate = new VFC_MassUpdateOLsAgr(controller);
        
   }
    }