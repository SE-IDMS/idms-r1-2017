@isTest
private class AP_OpportunityAssessment_Delete_TEST{
    public static testMethod void OpportunityAssessment_Delete() {
       /*Assessment__c  ass= Utils_TestMethods.createAssessment();
       insert ass;*/
       User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;
       Country__c country = Utils_TestMethods.createCountry();
            country.countrycode__c ='ASM';
            insert country;
            Country__c country1 = Utils_TestMethods.createCountry();
            country1.countrycode__c ='IN';
            insert country1;

            PartnerPRogram__c gp1 = new PartnerPRogram__c();
            Assessment__c asm = new Assessment__c();
            user1.country__c = country.countrycode__c;

            gp1 = Utils_TestMethods.createPartnerProgram();
            gp1.TECH_CountriesId__c = country.id;
            gp1.Country__c = country.id;
            gp1.recordtypeid = Label.CLMAY13PRM15;
            gp1.ProgramStatus__c = Label.CLMAY13PRM47;
            gp1.Name = Label.CLAPR14PRM25;
            insert gp1;

            ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
            prg1.Name = Label.CLAPR14PRM25;
            insert prg1;
            prg1.levelstatus__c = 'active';
            update prg1;

            PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
            cp1.Name = Label.CLAPR14PRM25;
            insert cp1;
            cp1.ProgramStatus__c = 'active';
            update cp1;

            ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
            prg2.Name = Label.CLAPR14PRM25;
            insert prg2;
            prg2.levelstatus__c = 'active';
            update prg2;

            ProgramLevel__c prgLevel = new ProgramLevel__c();
            prgLevel.PartnerProgram__c = gp1.Id;
            prgLevel.GlobalProgramLevel__c = prg1.id;
            Insert prgLevel;
       Assessment__c ass = new Assessment__c();
        ass = Utils_TestMethods.createAssessment();
        //ass.recordTypeId = prgORFAssessmentRecordType.Id ;
        ass.AutomaticAssignment__c = true;
        ass.Active__c = true;
        ass.Name = 'Sample Assessment';
        ass.PartnerProgram__c = gp1.id;
        ass.ProgramLevel__c = prg1.id;
        insert ass;
       Account acc = Utils_TestMethods.createAccount();
       insert acc;
       Opportunity  opp = Utils_TestMethods.createOpenOpportunity(acc.id);
       insert opp;
       OpportunityAssessmentQuestion__c  oppAssQue=Utils_TestMethods.createOpportunityAssessmentQuestion(ass.id,'Picklist',1);
       insert oppAssQue;
       OpportunityAssessmentAnswer__c oppAssAns = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue.id);
       insert oppAssAns ;
       OpportunityAssessment__c oppAss=Utils_TestMethods.createOpportunityAssessment(opp.id,ass.id);
       insert oppAss;
       OpportunityAssessmentResponse__c  oppAssRes = Utils_TestMethods.createOpportunityAssessmentResponse(oppAssQue.id,oppAssAns.id,oppAss.id);
       insert oppAssRes ;
       delete oppAss;
    }
}