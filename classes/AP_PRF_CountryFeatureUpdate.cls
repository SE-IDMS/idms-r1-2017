/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : March 2013
    Description : For PRM May 2013 Release:
                 1. To create Program Feature for Country PArtner Program when a new Global Feature is added.
                 2. To update country program feature when a global feature is updated
    
********************************************************************************************************************/
public class AP_PRF_CountryFeatureUpdate {
    
    public static void creatCountryFeature(map<ID,list<ProgramFeature__c>> mapGlobalLevelFeature , list<ID> lstFtrProgramID , set<Id> setFeatureID)
    {
        
        list<ProgramFeature__c> lstCountryPRGFeature = new list<ProgramFeature__c>();
        list<ProgramFeature__c> lstExistingFeature = new list<ProgramFeature__c>();
       
        
        map<Id,ProgramLevel__c> mapPRGLevel = new map<Id,ProgramLevel__c>([Select p.PartnerProgram__r.GlobalPartnerProgram__c, p.PartnerProgram__c, p.GlobalProgramLevel__c From ProgramLevel__c p 
                                                WHERE p.GlobalProgramLevel__c in :mapGlobalLevelFeature.keySet() and p.PartnerProgram__r.GlobalPartnerProgram__c in :lstFtrProgramID limit 50000]);
                                          
        if(setFeatureID != null)
        {
            lstExistingFeature = [Select id,partnerprogram__c,globalprogramfeature__c,ProgramLevel__c from ProgramFeature__c where recordtypeid =: Label.CLMAY13PRM04 and globalprogramfeature__c in :setFeatureID limit 50000];
            if(!lstExistingFeature.isEmpty())
            {
                for(ProgramFeature__c feature : lstExistingFeature)
                    mapPRGLevel.remove(feature.ProgramLevel__c);
            }

        }

        for(ProgramLevel__c prgLevel : mapPRGLevel.values())
        {
            for(ProgramFeature__c PRGFeature : mapGlobalLevelFeature.get(prgLevel.GlobalProgramLevel__c))
            {
                ProgramFeature__c newPRGFeature = new ProgramFeature__c();
                newPRGFeature.FeatureStatus__c = Label.CLMAY13PRM02;
                newPRGFeature.recordtypeid = Label.CLMAY13PRM04;
                newPRGFeature.FeatureCatalog__c = PRGFeature.FeatureCatalog__c;
                newPRGFeature.PartnerProgram__c = prgLevel.PartnerProgram__c;
                newPRGFeature.ProgramLevel__c = prgLevel.Id;
                newPRGFeature.GlobalProgramFeature__c = PRGFeature.Id;
                lstCountryPRGFeature.add(newPRGFeature);
            }
        }
        
        try
        {
            insert lstCountryPRGFeature;
        }
        catch(Exception e)
        {
            mapGlobalLevelFeature.values().get(0).get(0).addError(Label.CLMAY13PRM44);
        }
    }
    
    //public static void updateCountryFeature(list<ProgramFeature__c> lstNewPRGFeature)
    public static void updateCountryFeature(map<Id,ProgramFeature__c> mapNewGlobalPRGFeature , map<Id,ProgramFeature__c> mapOldGlobalPRGFeature)
    {
        list<ProgramFeature__c> lstChildFeature = new list<ProgramFeature__c>();
        String strFields = '';
        map<Id,ProgramFeature__c> mapGlobalChildFeature = new map<Id,ProgramFeature__c>();
        try
        {        
            Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.ProgramFeature__c.fields.getMap();
                       
            if (mapFields != null)
            {
                for (Schema.SObjectField ft : mapFields.values())// loop through all field tokens (ft)
                { 
                    if (ft.getDescribe().isUpdateable())// field is updateable
                    { 
                        strFields += ft.getDescribe().getName()+',';
                    }
                }
            }
            
            if (strFields.endsWith(','))
            {
                strFields = strFields.substring(0,strFields.lastIndexOf(','));
            }
         
            set<Id> setGlobalFeatureID = new set<ID>();
            list<ProgramFeature__c> lstFeatureToUpdate = new list<PRogramFeature__c>();
            setGlobalFeatureID.addAll(mapNewGlobalPRGFeature.keySet());
            String childFetaureQuery = 'Select '+ strFields + ',PartnerProgram__r.PRogramStatus__c,ProgramLevel__r.LevelStatus__c FROM ProgramFeature__c WHERE GlobalProgramFeature__c in :setGlobalFeatureID  limit 10000';
            lstChildFeature = database.query(childFetaureQuery);
            /*for(ProgramFeatue__C prgFTR : lstChildFeature)
                mapGlobalChildFeature.put(prgFTR.GlobalProgramFeature__c , prgFTR);*/
            
            
            if(!lstChildFeature.isEmpty())
            {
                for(PRogramFeature__c childFeature : lstChildFeature)
                {   
                    for (String str : mapFields.keyset()) 
                    {
                                                
                        if(mapNewGlobalPRGFeature.get(childFeature.globalProgramFeature__C).recordtypeid == Label.CLMAY13PRM06 &&  mapFields.get(str).getDescribe().isUpdateable() && mapNewGlobalPRGFeature.get(childFeature.globalProgramFeature__C).get(str) != mapOldGlobalPRGFeature.get(childFeature.globalProgramFeature__C).get(str))
                        {
                           
                            if(CS_GlobalFeaturetoCountryFeature__c.getValues(str.toLowerCase()) == null)
                            {
                                childFeature.put(str, mapNewGlobalPRGFeature.get(childFeature.globalProgramFeature__C).get(str));
                                lstFeatureToUpdate.add(childFeature);
                            }
                            
                            //For July Release
                            if(str == Label.CLMAY13PRM23 && mapNewGlobalPRGFeature.get(childFeature.globalProgramFeature__C).get(str) != mapOldGlobalPRGFeature.get(childFeature.globalProgramFeature__C).get(str) && mapNewGlobalPRGFeature.get(childFeature.globalProgramFeature__C).get(str) == Label.CLMAY13PRM07)
                            {
                                
                                childFeature.put('TECH_InactiveGlobalFeature__c', true);
                                lstFeatureToUpdate.add(childFeature);
                            }
                        }
                    }
                }
            }  
            if(!lstFeatureToUpdate.isEmpty())
                update lstFeatureToUpdate;
            
        }
        catch(Exception e)
        {
            /*for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                mapNewGlobalPRGFeature.values().get(0).addError(e.getDmlMessage(i)); 
                
                System.debug(e.getDmlMessage(i)); 
            } */
            mapNewGlobalPRGFeature.values().get(0).addError('An error occured while updating child records. Please contact your system administrator');
            System.debug(e.getMessage());
        } 
    }
    

    /*************************To create new features at Account/Contact level when a feature is activated at Country Level*********************/
    /* Commented in Apr 14 PRM - Srinivas Nallapati
    public static void createAccountContactFeature(map<Id,list<ProgramFeature__c>> mapCountryLevelFeature , Boolean isContactFeature)
    {
        list<ACC_PartnerProgram__c> lstAccountProgram = new list<ACC_PartnerProgram__c>();
        list<ContactAssignedProgram__c> lstContactProgram = new list<ContactAssignedProgram__c>();
                
        list<AccountAssignedFeature__c> lstAccountFeature = new list<AccountAssignedFeature__c>();
        list<ContactAssignedFeature__c> lstContactFeature = new list<ContactAssignedFeature__c>();
        map<Id,ContactAssignedProgram__c> mapAccountContactProgram = new map<ID,ContactAssignedProgram__c>();  
        map<Id,set<Id>> mapExistingAcctFeature = new map<Id,set<Id>>();
        map<Id,set<Id>> mapExistingConFeature = new map<Id,set<Id>>();
        list<ProgramFeature__c> lstAllFeature = new list<ProgramFeature__c>();
        for(Id levelId : mapCountryLevelFeature.keySet())
            lstAllFeature.addAll(mapCountryLevelFeature.get(levelId));
        //Update the existing Account and Contact Feature and remove from the new feature list
        list<AccountAssignedFeature__c> lstExistingAccountFeature = new list<AccountAssignedFeature__c>();
        lstExistingAccountFeature = [Select id , active__c,ProgramFeature__c,ProgramLevel__c,accountassignedprogram__c from AccountAssignedFeature__c where ProgramFeature__c IN :lstAllFeature limit 10000];
        for(AccountAssignedFeature__c accountFeature : lstExistingAccountFeature )
        {
            accountFeature.active__c = true;
            if(!mapExistingAcctFeature.containsKey(accountFeature.accountassignedprogram__c))
                mapExistingAcctFeature.put(accountFeature.accountassignedprogram__c , new set<Id> {(accountFeature.ProgramFeature__c)});
            else
                mapExistingAcctFeature.get(accountFeature.accountassignedprogram__c).add(accountFeature.ProgramFeature__c);
        }
        lstAccountFeature.addAll(lstExistingAccountFeature);
        list<ContactAssignedFeature__c> lstExistingContactFeature = new list<ContactAssignedFeature__c>();
        lstExistingContactFeature = [Select id , active__c,ProgramFeature__c,ProgramLevel__c,contactassignedProgram__c from ContactAssignedFeature__c where ProgramFeature__c IN :lstAllFeature limit 10000];
        for(ContactAssignedFeature__c contactFeature : lstExistingContactFeature)
        {
            contactFeature.active__c = true;
            if(!mapExistingConFeature.containsKey(contactFeature.contactassignedProgram__c))
                mapExistingConFeature.put(contactFeature.contactassignedProgram__c , new set<Id> {(contactFeature.ProgramFeature__c)});
            else
                mapExistingConFeature.get(contactFeature.contactassignedProgram__c).add(contactFeature.ProgramFeature__c);
        }
        lstContactFeature.addAll(lstExistingContactFeature);
        lstAccountProgram = [Select id, ProgramLevel__c,account__c,PartnerProgram__c from ACC_PartnerProgram__c where ProgramLevel__c in :mapCountryLevelFeature.keySet() limit 50000];
        if(isContactFeature)
        {
            lstContactProgram = [Select id, AccountAssignedProgram__c,Contact__c,PartnerProgram__c from ContactAssignedProgram__c where ProgramLevel__c in :mapCountryLevelFeature.keySet() limit 50000];
            if(!lstContactProgram.isEmpty())
                for(ContactAssignedProgram__c contactProgram : lstContactProgram)
                {
                    if(!mapAccountContactProgram.containsKey(contactProgram.AccountAssignedProgram__c))
                        mapAccountContactProgram.put(contactProgram.AccountAssignedProgram__c , contactProgram);
                }
        }
        
        for(ACC_PartnerProgram__c accountProgram : lstAccountProgram)
        {
            
            if(mapCountryLevelFeature.containsKey(accountProgram.ProgramLevel__c))
            {
                for(ProgramFeature__c feature : mapCountryLevelFeature.get(accountProgram.ProgramLevel__c))
                {
                    
                    if(!(mapExistingAcctFeature.containsKey(accountProgram.Id) && mapExistingAcctFeature.get(accountProgram.Id).contains(feature.Id)))
                    {
                        
                        AccountAssignedFeature__c newFeature = new AccountAssignedFeature__c();
                        newFeature.Account__c = accountProgram.Account__c;
                        newFeature.AccountAssignedProgram__c = accountProgram.Id;
                        newFeature.ProgramFeature__c = feature.Id;
                        newFeature.ProgramLevel__c = feature.ProgramLevel__c;
                        newFeature.Active__c = true;
                        newFeature.PartnerProgram__c = accountProgram.PartnerProgram__c;
                        lstAccountFeature.add(newFeature);
                    }
                    if(feature.Enabled__c == Label.CLMAY13PRM11 || feature.Visibility__c == Label.CLMAY13PRM11)
                    {
                        if(!mapAccountContactProgram.isEmpty())
                        {
                            if(!(mapExistingConFeature.containsKey(mapAccountContactProgram.get(accountProgram.Id).Id) && mapExistingConFeature.get(mapAccountContactProgram.get(accountProgram.Id).Id).contains(feature.Id)))
                            {
                                ContactAssignedFeature__c contactFeature = new ContactAssignedFeature__c();
                                contactFeature.Contact__c = mapAccountContactProgram.get(accountProgram.Id).Contact__c;
                                contactFeature.ContactAssignedProgram__c = mapAccountContactProgram.get(accountProgram.Id).Id;
                                contactFeature.ProgramFeature__c = feature.Id;
                                contactFeature.ProgramLevel__c = feature.ProgramLevel__c;
                                contactFeature.Active__c = true;
                                contactFeature.PartnerProgram__c = mapAccountContactProgram.get(accountProgram.Id).PartnerProgram__c;
                                lstContactFeature.add(contactFeature);
                            }
                        }
                    }
                }
            }
        }
        
               
        if(!lstAccountFeature.isEmpty())
        {
            try
            {
                upsert lstAccountFeature;
            }
            catch(DMLException e)
            {
                
                for (Integer i = 0; i < e.getNumDml(); i++) 
                { 
                    mapCountryLevelFeature.values().get(0).get(0).addError(e.getDmlMessage(i)); System.debug(e.getDmlMessage(i)); 
                } 
            }
        }
        if(!lstContactFeature.isEmpty())
        {
            try
            {
                upsert lstContactFeature;
            }
            catch(DMLException e)
            {
                for (Integer i = 0; i < e.getNumDml(); i++) 
                { 
                    mapCountryLevelFeature.values().get(0).get(0).addError(e.getDmlMessage(i)); System.debug(e.getDmlMessage(i)); 
                } 
            }
        }
    }
    */
    //APr 14 PRM
    public static void createActivateAccConFeature(map<Id,list<ProgramFeature__c>> mapCountryLevelFeature , Boolean isContactFeature)
    {
        List<ACC_PartnerProgram__c> allRelatedAAP = [SELECT id,Account__c, ProgramLevel__c FROM ACC_PartnerProgram__c WHERE ProgramLevel__c in :mapCountryLevelFeature.keyset() and Active__c=true];
        set<id> accids = new set<id>();
        map<id,set<id>> mpPLtoAccs = new map<id,set<id>>();
        for(ACC_PartnerProgram__c aap : allRelatedAAP)
        {
            accids.add(aap.Account__c);
            if(mpPLtoAccs.containsKey(aap.ProgramLevel__c))
            {
                mpPLtoAccs.get(aap.ProgramLevel__c).add(aap.Account__c);
            }else
            {
                set<id> tset = new set<id>(); tset.add(aap.Account__c);
                mpPLtoAccs.put(aap.ProgramLevel__c,tset);
            }
        }

        List<AccountAssignedFeature__c> lstAllExistingAAF = [Select Account__c, Active__c, FeatureCatalog__c FROM  AccountAssignedFeature__c WHERE Account__c in:accIds];
        map<id,set<id>> mpAccToFC  = new map<id,set<id>>();
        map<id, map<id,AccountAssignedFeature__c>> mpACCtoFCtoAAF = new map<id, map<id,AccountAssignedFeature__c>> ();

        for(AccountAssignedFeature__c aaf : lstAllExistingAAF)
        {
            if(mpAccToFC.containsKey(aaf.account__c))
            {
                mpAccToFC.get(aaf.account__c).add(aaf.FeatureCatalog__c);
            }else
            {
                set<id> tset = new set<id>(); tset.add(aaf.FeatureCatalog__c);
                mpAccToFC.put(aaf.account__c,tset);
            }

            if(mpACCtoFCtoAAF.containsKey(aaf.account__c))
            {
                mpACCtoFCtoAAF.get(aaf.account__c).put(aaf.FeatureCatalog__c, aaf);
            }else
            {
                map<id,AccountAssignedFeature__c> tmp = new map<id,AccountAssignedFeature__c>();
                tmp.put(aaf.FeatureCatalog__c, aaf);
                mpACCtoFCtoAAF.put(aaf.account__c, tmp);
            }

        }

        //Create new aaf
        List<AccountAssignedFeature__c> lstnewAAf = new List<AccountAssignedFeature__c>();
        List<AccountAssignedFeature__c> lstToBeActivatedAAf = new List<AccountAssignedFeature__c>();
        for(Id plid : mapCountryLevelFeature.keyset())
        {
            for(ProgramFeature__c pf : mapCountryLevelFeature.get(plid))
            {
                if(mpPLtoAccs.containsKey(plid))
                    for(id acid : mpPLtoAccs.get(plid))
                    {
                        if(mpAccToFC.containsKey(acid) && mpAccToFC.get(acid).contains(pf.FeatureCatalog__c) )
                        {
                            //feature is already added to account -- Activate existinf 
                            if(mpACCtoFCtoAAF.containsKey(acid))
                            {
                                map<id,AccountAssignedFeature__c> mpFCtoAAf = mpACCtoFCtoAAF.get(acid);
                                IF(mpFCtoAAf.get(pf.FeatureCatalog__c) != null && mpFCtoAAf.get(pf.FeatureCatalog__c).Active__c == false)
                                {
                                    AccountAssignedFeature__c extAaF = mpFCtoAAf.get(pf.FeatureCatalog__c);
                                    extAaF.Active__c = true;
                                    lstToBeActivatedAAf.add(extAaF);
                                }
                                
                            }
                        }
                        else
                        {
                            AccountAssignedFeature__c newFeature = new AccountAssignedFeature__c();
                            newFeature.Account__c = acid;
                            //newFeature.AccountAssignedProgram__c = accountProgram.Id;
                            //newFeature.ProgramFeature__c = feature.Id;
                            //newFeature.ProgramLevel__c = feature.ProgramLevel__c;
                            newFeature.FeatureCatalog__c = pf.FeatureCatalog__c;
                            newFeature.Active__c = true;
                            //newFeature.PartnerProgram__c = accountProgram.PartnerProgram__c;
                            lstnewAAf.add(newFeature);
                        }
                    }
            }
            
        }

        Database.insert(lstnewAAf, false);
        Database.update(lstToBeActivatedAAf, false);

        /// for Contact Assigned features
        List<ContactAssignedProgram__c> allRelatedCAP = [SELECT id,Contact__c, ProgramLevel__c FROM ContactAssignedProgram__c WHERE ProgramLevel__c in :mapCountryLevelFeature.keyset() and Active__c=true];
        set<id> conids = new set<id>();
        map<id,set<id>> mpPLtoCons = new map<id,set<id>>();
        for(ContactAssignedProgram__c cap : allRelatedCAP)
        {
            conids.add(cap.Contact__c);
            if(mpPLtoCons.containsKey(cap.ProgramLevel__c))
            {
                mpPLtoCons.get(cap.ProgramLevel__c).add(cap.Contact__c);
            }else
            {
                set<id> tset = new set<id>(); tset.add(cap.Contact__c);
                mpPLtoCons.put(cap.ProgramLevel__c,tset);
            }
        }
        
        List<ContactAssignedFeature__c> lstAllExistingCAF = [Select Contact__c, Active__c, FeatureCatalog__c FROM  ContactAssignedFeature__c WHERE Contact__c in:conids];
        map<id,set<id>> mpConToFC  = new map<id,set<id>>();
        map<id, map<id,ContactAssignedFeature__c>> mpContoFCtoCAF = new map<id, map<id,ContactAssignedFeature__c>> ();

        for(ContactAssignedFeature__c caf : lstAllExistingCAF)
        {
            if(mpConToFC.containsKey(caf.contact__c))
            {
                mpConToFC.get(caf.contact__c).add(caf.FeatureCatalog__c);
            }else
            {
                set<id> tset = new set<id>(); tset.add(caf.FeatureCatalog__c);
                mpConToFC.put(caf.contact__c,tset);
            }

            if(mpContoFCtoCAF.containsKey(caf.contact__c))
            {
                mpContoFCtoCAF.get(caf.contact__c).put(caf.FeatureCatalog__c, caf);
            }else
            {
                map<id,ContactAssignedFeature__c> tmp = new map<id,ContactAssignedFeature__c>();
                tmp.put(caf.FeatureCatalog__c, caf);
                mpContoFCtoCAF.put(caf.contact__c, tmp);
            }

        }

        //Create new aaf
        List<ContactAssignedFeature__c> lstnewCAf = new List<ContactAssignedFeature__c>();
        List<ContactAssignedFeature__c> lstToBeActivatedCAf = new List<ContactAssignedFeature__c>();
        for(Id plid : mapCountryLevelFeature.keyset())
        {
            for(ProgramFeature__c pf : mapCountryLevelFeature.get(plid))
            {
                if(mpPLtoCons.containsKey(plid))
                    for(id cid : mpPLtoCons.get(plid))
                    {
                        if(mpConToFC.containsKey(cid) && mpConToFC.get(cid).contains(pf.FeatureCatalog__c) )
                        {
                            //feature is already added to account -- Activate existinf 
                            if(mpContoFCtoCAF.containsKey(cid))
                            {
                                map<id,ContactAssignedFeature__c> mpFCtoCAf = mpContoFCtoCAF.get(cid);
                                IF(mpFCtoCAf.get(pf.FeatureCatalog__c) != null && mpFCtoCAf.get(pf.FeatureCatalog__c).Active__c == false)
                                {
                                    ContactAssignedFeature__c extCaF = mpFCtoCAf.get(pf.FeatureCatalog__c);
                                    extCaF.Active__c = true;
                                    lstToBeActivatedCAf.add(extCaF);
                                }
                                
                            }
                        }
                        else
                        {
                            ContactAssignedFeature__c newFeature = new ContactAssignedFeature__c();
                            newFeature.Contact__c = cid;
                            //newFeature.AccountAssignedProgram__c = accountProgram.Id;
                            //newFeature.ProgramFeature__c = feature.Id;
                            //newFeature.ProgramLevel__c = feature.ProgramLevel__c;
                            newFeature.FeatureCatalog__c = pf.FeatureCatalog__c;
                            newFeature.Active__c = true;
                            //newFeature.PartnerProgram__c = accountProgram.PartnerProgram__c;
                            lstnewCAf.add(newFeature);
                        }
                    }
            }
            
        }

        Database.insert( lstnewCAf, false);
        Database.update(lstToBeActivatedCAf, false);        

    }//End of method apr 14 prm
    

    /*
    Srinivas Nallapati      BR-3082  July13
    */
    public static void updateAccountContactFeatures(list<ProgramFeature__c> lstFeature)
    {
        map<id,ProgramFeature__c> mpProgramFeature = new map<id,ProgramFeature__c>(lstFeature);
        List<AccountAssignedFeature__c> lstAccAssignedFeature = [SELECT Account__c,Active__c,FeatureName__c,PartnerProgram__c,ProgramFeature__c,ProgramLevel__c FROM AccountAssignedFeature__c WHERE ProgramFeature__c IN :lstFeature];
        List<ContactAssignedFeature__c> lstConAssignedFeature = [SELECT Active__c,Contact__c,FeatureName__c,PartnerProgram__c,ProgramFeature__c,ProgramLevel__c FROM ContactAssignedFeature__c WHERE ProgramFeature__c IN :lstFeature];

        for(AccountAssignedFeature__c aaf : lstAccAssignedFeature)
        {
            if(mpProgramFeature.get(aaf.ProgramFeature__c).FeatureStatus__c == Label.cljul13prm01)
                aaf.Active__c = false;
        }
        update lstAccAssignedFeature;

        for(ContactAssignedFeature__c caf : lstConAssignedFeature)
        {
            if(mpProgramFeature.get(caf.ProgramFeature__c).FeatureStatus__c == Label.cljul13prm01)
                caf.Active__c = false;
        }    
        update lstConAssignedFeature;

    }

}