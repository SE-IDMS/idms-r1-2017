@isTest
private class Utils_PreventUsageOfChatter_Test{

    static testMethod void testPreventionOnUser()
    {
        FeedItem fi = new FeedItem(ParentId=UserInfo.getUserId(), Body='Test');
        insert fi;
        
        SpiceObjectData__c sod = new SpiceObjectData__c (Name='User', 
                                                        ObjectAPIName__c='User', 
                                                        ErrorMessage__c='Error on user', 
                                                        PostsAllowedinChatter__c=false, 
                                                        CommentsAllowedinChatter__c=false);
        insert sod;
        
        FeedItem fi2 = new FeedItem(ParentId=UserInfo.getUserId(), Body='Test');
        insert fi2;
        
        FeedComment fc = new FeedComment(FeedItemId=fi.Id, CommentBody ='Test');
        insert fc;
    }
    
    static testMethod void testPreventOnGroup()
    {
        CollaborationGroup cgroup = new CollaborationGroup(Name='TestGroup', CollaborationType='Public');
        insert cgroup;
        
        FeedItem fi = new FeedItem(ParentId=cgroup.Id, Body='Test');
        insert fi;
        
        SpiceGroupData__c sgd = new SpiceGroupData__c (Name='TestGroup', GroupId__c=cgroup.Id, 
                                                            PostsAllowedinChatter__c=false, 
                                                            CommentsAllowedinChatter__c=false);
        insert sgd;
        
        FeedItem fi2 = new FeedItem(ParentId=cgroup.Id, Body='Test');
        insert fi2;
        
        FeedComment fc = new FeedComment(FeedItemId=fi.Id, CommentBody ='Test');
        insert fc;
    }
    
    static testMethod void testPreventOnGroup2()
    {
        CollaborationGroup cgroup = new CollaborationGroup(Name='TestGroup', CollaborationType='Public');
        insert cgroup;
        
        FeedItem fi = new FeedItem(ParentId=cgroup.Id, Body='Test');
        insert fi;
        
        SpiceGroupData__c sgd = new SpiceGroupData__c (Name='TestGroup', GroupId__c=cgroup.Id, 
                                                            PostsAllowedinChatter__c=true, 
                                                            CommentsAllowedinChatter__c=true);
        insert sgd;
        
        FeedItem fi2 = new FeedItem(ParentId=cgroup.Id, Body='Test');
        insert fi2;
        
        FeedComment fc = new FeedComment(FeedItemId=fi.Id, CommentBody ='Test');
        insert fc;
    }
}