public class AP_RI_FieldUpdate{

  Public static void updateCaseOwner(List<RMA_Product__c> RIs)
  {
  
     Map<id, List<RMA_Product__c>> mapCaseIdRI = new map<id, List<RMA_Product__c>>();
     for(RMA_Product__c RtnItem: RIs)
     {
         ID value = RtnItem.TECH_CaseId__c;
         If(mapCaseIdRI.Containskey(RtnItem.TECH_CaseId__c))
         {
           mapCaseIdRI.get(RtnItem.TECH_CaseId__c).add(RtnItem);
         }
         else
         {
           mapCaseIdRI.put(RtnItem.TECH_CaseId__c, new List<RMA_Product__c>());
           mapCaseIdRI.get(RtnItem.TECH_CaseId__c).add(RtnItem);
         }
     }
     
     
     List<Case> lstCases = [Select id, Owner.id from Case where id in: mapCaseIdRI.keyset()];
     map<id, id> mapCaseIdOwner = new Map<id,id>();
     for(Case tmpCase: lstCases )
     {
       if(!Test.isRunningTest())
       { 
            if(tmpCase.Owner.id.getsobjecttype()==user.sobjecttype)
            {
                mapCaseIdOwner.put(tmpCase.id, tmpCase.Owner.id);
            }
       }
       else
        {
            mapCaseIdOwner.put(tmpCase.id, tmpCase.Owner.id);
        }       
     }
     
     List<RMA_Product__c> lstNewRIs = new List<RMA_Product__c>();
     Id CaseOwnerId;
     for(id caseId: mapCaseIdRI.keyset())
     {
         
         if(mapCaseIdOwner.containskey(caseId))
         {
            CaseOwnerId = mapCaseIdOwner.get(caseId);
         }
         for(RMA_Product__c tmpRI: mapCaseIdRI.get(caseId))
         {
            tmpRI.CaseOwner__c = CaseOwnerId;
            //lstNewRIs.add(tmpRI);
         }
         
     }
     
     
     
  }
}