public class VFC84_RedirectToSolOrSelSearch
{
    private OPP_SupportRequest__c objOppSupReq;
    String strTemp='';
    public VFC84_RedirectToSolOrSelSearch(ApexPages.StandardController controller)
    {
        Utils_SDF_Methodology.startTimer();
        objOppSupReq = (OPP_SupportRequest__c )controller.getRecord();
        
        if(objOppSupReq.RecordTypeID==Label.CL10092 || objOppSupReq.RecordTypeID==Label.CL10094) 
            strTemp='sel';
        else if(objOppSupReq.RecordTypeID==Label.CL10093)
            strTemp='sol';
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.Limits();    
           
    }
     public PageReference doRedirect()
     {
         System.debug('....'+strTemp);
         
         PageReference pg;
         if(strTemp=='sol')
         {
             pg=new PageReference('/apex/'+Label.CL10095); 
             setParametersForPageReference(pg);           
         }
         else if(strTemp=='sel')
         {
             
             System.debug('..in pg...');
             pg=new PageReference('/apex/'+Label.CL10096); 
             setParametersForPageReference(pg);
         }
         return pg;
         
     }
     public void setParametersForPageReference(PageReference pg)
     {
         pg.getParameters().put(Label.CL10217,ApexPages.currentPage().getParameters().get(Label.CL10217));
         pg.getParameters().put(Label.CL10219,ApexPages.currentPage().getParameters().get(Label.CL10219));
         pg.getParameters().put('retURL',ApexPages.currentPage().getParameters().get('retURL'));
         pg.getParameters().put('RecordType',ApexPages.currentPage().getParameters().get('RecordType'));
         pg.getParameters().put('ent',ApexPages.currentPage().getParameters().get('ent'));
         pg.getParameters().put('sfdc.override','1');
         pg.getParameters().put('save_new','1'); 
         
     }  
     
}