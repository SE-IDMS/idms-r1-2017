global class UserRegResult {
    global String AccountId;
    global String ContactId;
    global String UserId;
    global String UserFederationId;
    global String bFOAccountId;
    global String bFOMemberId;
    
    global boolean Success;
    global String ErrorMsg;
    global List<String> ErrorCodes;
    global String StatusCode;

    global String UserRegistrationTrackingId;
    
    global Datetime UserFedIdCreatedDate;
    global Datetime CompanyFedIdCreatedDate;  

    global UserRegResult() {
        this (false);
    }

    global UserRegResult (boolean initStatus) {
        AccountId = '';
        ContactId = '';
        UserId = '';
        UserFederationId = '';
        Success = initStatus;
        ErrorMsg = '';
        ErrorCodes = new List<String> ();
        StatusCode = '';
        bFOAccountId = bFOMemberId = '';
    }
}