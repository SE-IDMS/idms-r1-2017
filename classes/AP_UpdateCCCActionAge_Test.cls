@isTest
private class AP_UpdateCCCActionAge_Test{
    public static testMethod void testupdateCCCActionAge(){
        System.debug('#### AP_UpdateCCCActionAge_Test.testupdateCCCActionAge Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Country__c objCountry = Utils_TestMethods.createCountry();
        insert objCountry;
        
        Test.startTest();
        
        System.debug('Limit: '+Limits.getQueries());
        System.debug('Case Before Insert Limit Available: '+Limits.getLimitQueries());
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        objCase.CCCountry__c = objCountry.Id;
        insert objCase;
        System.debug('Case Insert Limit Used: '+Limits.getQueries());
        System.debug('Case Insert Limit Available: '+Limits.getLimitQueries());
        
        
        CCCAction__c objAction = Utils_TestMethods.createCCCAction(objCase.Id);
        insert objAction;
        System.debug('CCCAction Insert Limit Used: '+Limits.getQueries());
        System.debug('CCCAction Insert Limit Available: '+Limits.getLimitQueries());
         
        objAction.Status__c =Label.CLSEP12CCC06; // InProgress
        update objAction;
        System.debug('CCCAction Update Limit Used: '+Limits.getQueries());
        System.debug('CCCAction Update Limit Available: '+Limits.getLimitQueries());
        
        objAction.Status__c =Label.CLSEP12CCC07; // Closed
        update objAction;
        
        objAction.Status__c =Label.CLSEP12CCC08; // Open (Re)
        update objAction;
        
        objAction.Status__c =Label.CLSEP12CCC06; // InProgress (Re)
        update objAction;
        
        objAction.Status__c =Label.CLSEP12CCC07; // Closed (Re)
        update objAction;
        
        objAction.Status__c =Label.CLSEP12CCC08; // Open (Re)
        update objAction;
        Test.stopTest(); 
        
        System.debug('#### AP_UpdateCCCActionAge_Test.testupdateCCCActionAge Ends ####');
    }
}