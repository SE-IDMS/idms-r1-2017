/*    Copyright (c) 2014 Zuora, Inc.
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy of 
 *   this software and associated documentation files (the "Software"), to use copy, 
 *   modify, merge, publish the Software and to distribute, and sublicense copies of 
 *   the Software, provided no fee is charged for the Software.  In addition the
 *   rights specified above are conditioned upon the following:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
 *   or promote products derived from this Software without specific prior written
 *   permission from Zuora, Inc.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 *   ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  

 *   IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED

 *   BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.
 */
@isTest(SeeAllData=True)
private class ZuoraQuoteRulesEnginePluginTest{

    static testmethod void ZuoraQuoteRulesEnginePluginTest() {
      zqu__ZProduct__c testNullProd = makeProduct('Null');
      zqu__ZProduct__c testWelcomeProd = makeProduct('Welcome');
      zqu__ZProduct__c testAddProd = makeProduct('AddOn');

      zqu__ProductRatePlan__c testNullPRP = makeProductRatePlan(testWelcomeProd.Id, 'testNullRatePlan', 'Null');    
      zqu__ProductRatePlan__c testWelcomePRP = makeProductRatePlan(testNullProd.Id, 'testWelcomeRatePlan', 'Welcome');  
      zqu__ProductRatePlan__c testAddPRP = makeProductRatePlan(testAddProd.Id, 'testAddOnRatePlan', 'AddOn');    

      zqu__ProductRatePlanCharge__c testNullPC = makeProductRatePlanCharge(testNullPRP.Id, 'recurring', 'Flat-Fee', 'Null');
      zqu__ProductRatePlanCharge__c testWelcomePC = makeProductRatePlanCharge(testWelcomePRP.Id, 'recurring', 'Flat-Fee', 'Welcome');
      zqu__ProductRatePlanCharge__c testAddPC = makeProductRatePlanCharge(testAddPRP.Id, 'recurring', 'Flat-Fee', 'AddOn');

      Account acc = makeAccount();    
      Contact con = makeContact(acc);    
      zqu__Quote__c newQuote = makeQuote(acc, con);
      zqu__QuoteAmendment__c newQuoteAmendment = makeQuoteAmendment(newQuote);
      zqu__QuoteRatePlan__c newNullQRP = makeQuoteRatePlan(newQuote, testNullPRP, newQuoteAmendment);
      zqu__QuoteRatePlan__c newWelcomeQRP = makeQuoteRatePlan(newQuote, testWelcomePRP, newQuoteAmendment);
      zqu__QuoteRatePlan__c newAddOnQRP = makeQuoteRatePlan(newQuote, testAddPRP, newQuoteAmendment);
      //zqu__QuoteRatePlanCharge__c newQC = makeQuoteRatePlanCharge(newQuote,testPC, newQRP);

      update newQuote;

      List<zqu.ZQuoteRulesEngine.ChangeLog> logs = new List<zqu.ZQuoteRulesEngine.ChangeLog>(); 
      zqu.ZQuoteRulesEngine.ChangeLog cL = new zqu.ZQuoteRulesEngine.ChangeLog();


      cL.description = 'test123';
      logs.add(cL);
      
      List<String> ratePlanIds = new List<String>{testNullPRP.Id, testWelcomePRP.Id, testAddPRP.Id};
      List<zqu.ZChargeGroup> zcgs = new List<zqu.ZChargeGroup>();
      
      zcgs.add(zqu.zQuoteUtil.getChargeGroup(newQuote.Id, testNullPRP.Id));
      zcgs.add(zqu.zQuoteUtil.getChargeGroup(newQuote.Id, testWelcomePRP.Id));
      zcgs.add(zqu.zQuoteUtil.getChargeGroup(newQuote.Id, testAddPRP.Id));

      for(Integer i=0; i<zcgs.size(); i++) {
        zqu.zChargeGroup zcg = zcgs.get(i);


          for(zqu.zCharge zc : zcg.zCharges) {
              zc.chargeobject = new zqu__QuoteRatePlanCharge__c();  
               System.Debug('zc' + zc);
               System.Debug('zc.chargeobject: ' + zc.chargeobject);  
              if (i == 0){
                zc.chargeobject.put('NbLicenses__c', 10);
              }

              if (i == 1){
                zc.chargeobject.put('NbLicenses__c', null);
              }

              if (i == 2){
                zc.chargeobject.put('NbLicenses__c', 1);
              }
          }
      } 

      System.Debug('Charges: ' + zcgs);
      System.Debug('Logs:' + logs);
      
      ZuoraQuoteRulesEnginePlugin.runpricerules(zcgs,logs);   
      ZuoraQuoteRulesEnginePlugin.runValidationRules(newQuote,zcgs,logs);
      ZuoraQuoteRulesEnginePlugin.runProductRules(newQuote,ratePlanIds,logs);

      ZuoraUpdateQuoteRatePlanCharge.updateZChargeGroups(newQuote, zcgs);
    
    }
    
    
    /**
     * Insert a test account in the db
     */
    public static Account makeAccount() {

      Account testAcc = new Account();

      List<Country__c> countryList = [SELECT Id from Country__c WHERE Name = 'France'];

      Id countryId = countryList.get(0).Id;
      

      testAcc.Name = 'my test account';
      testAcc.Street__c = 'Test Strret';
      TestAcc.ClassLevel1__c = 'SI';
      TestAcc.City__c = 'TestCity';
      TestAcc.ZipCode__c = '90003';
      TestAcc.Country__c = countryId;

      insert testAcc;
      return testAcc;
    }
    
    /**
     * Insert a test contact associated with an account in the db
     */
    public static Contact makeContact(Account testAcc) {

      Contact testContact = new Contact();

      testContact.FirstName = 'firstname';
      testContact.LastName = 'lastname';
      testContact.Email = 'email@mail.me';

      testContact.AccountId = testAcc.Id;

      insert testContact;
      return testContact;
    }
    
    /**
     * Insert a test quote based on an opportunity in the db
     */
    public static zqu__Quote__c makeQuote(Account testAcc, Contact testCon) {

      zqu__Quote__c testQuote = new zqu__Quote__c();

      testQuote.Name = 'my test quote';

      testQuote.zqu__Account__c = testAcc.Id;
      testQuote.QuoteContactName__c = testCon.Id;
      testQuote.Entity__c = 'SOL';
      testQuote.zqu__Currency__c = 'USD';
      testQuote.zqu__Hidden_Subscription_Name__c = 'Boo';
      testQuote.zqu__Status__c = 'New';
      testQuote.zqu__SubscriptionTermStartDate__c = Date.today();
      testQuote.zqu__InitialTerm__c = 12;
      testQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
      
      testQuote.Nb_MW__c = 12;

      insert testQuote;
      return testQuote;
    }
    
    /**
     * Insert a amendment test quote based on an account in the db
     */
    public static zqu__Quote__c makeAmendmentQuote(Account testAcc) {

      zqu__Quote__c testAmendmentQuote = new zqu__Quote__c();

      testAmendmentQuote.Name = 'my test quote';

      testAmendmentQuote.zqu__Account__c = testAcc.Id;
      testAmendmentQuote.zqu__Currency__c = 'USD';
      testAmendmentQuote.zqu__Hidden_Subscription_Name__c = 'Boo';
      testAmendmentQuote.zqu__Status__c = 'New';
      testAmendmentQuote.zqu__SubscriptionType__c = 'Amend Subscription';
      testAmendmentQuote.zqu__SubscriptionTermStartDate__c = Date.today();
      testAmendmentQuote.zqu__InitialTerm__c = 12;
      testAmendmentQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);

      insert testAmendmentQuote;
      return testAmendmentQuote;
    }
    
    
    /**
     * Insert a test quote amendment based on an account in the db
     */
    public static zqu__QuoteAmendment__c makeQuoteAmendment(zqu__Quote__c testQuote) {

      zqu__QuoteAmendment__c testQuoteAmendment = new zqu__QuoteAmendment__c();

      testQuoteAmendment.Name = 'my test quote amendment';
      testQuoteAmendment.zqu__Quote__c = testQuote.Id;
      insert testQuoteAmendment;
      return testQuoteAmendment;
    }
    
    /**
     * Insert a test quote rate plan based on an account in the db
     */
    public static zqu__QuoteRatePlan__c makeQuoteRatePlan(zqu__Quote__c testQuote, zqu__ProductRatePlan__c testPRP, zqu__QuoteAmendment__c testQuoteAmendment) {

      zqu__QuoteRatePlan__c testQuoteRatePlan = new zqu__QuoteRatePlan__c();

      testQuoteRatePlan.Name = 'my test quote rate plan';
      testQuoteRatePlan.zqu__Quote__c = testQuote.Id;
      testQuoteRatePlan.zqu__QuoteAmendment__c = testQuoteAmendment.Id;
     

      insert testQuoteRatePlan;
      return testQuoteRatePlan;
    }
      
    public static zqu__ZProduct__c makeProduct(String type) {
      zqu__ZProduct__c testNullProduct = new zqu__ZProduct__c();
      testNullProduct.Name = 'testing';
      if ('Welcome'.equals(type)) {
        testNullProduct.zqu__SKU__c = 'CA2SKU';
      } else {
        testNullProduct.zqu__SKU__c = 'testSKU';
      }
      testNullProduct.zqu__ZuoraId__c = '12'+type;
      testNullProduct.zqu__EffectiveStartDate__c = date.newinstance(2000, 1, 1);
      testNullProduct.zqu__EffectiveEndDate__c = date.newinstance(2050, 1, 1);
      testNullProduct.zqu__Deleted__c = false;

      
      insert testNullProduct;
      return testNullProduct;
    }

    private static integer ratePlanId = 154657;
      
    public static zqu__ProductRatePlan__c makeProductRatePlan(String zproduct, String name, String type) {
      zqu__ProductRatePlan__c testrp = new zqu__ProductRatePlan__c();
      testrp.name = 'test rp';
      testrp.zqu__ZProduct__c = zproduct;
      testrp.zqu__ZuoraId__c = String.valueOf(ratePlanId);
      ratePlanId++;
      testrp.zqu__Deleted__c = false;
      //set custom fields 

      //Needed for pricing queries
      testrp.zqu__EffectiveStartDate__c = Date.newInstance(2010, 1, 1);
      testrp.zqu__EffectiveEndDate__c = Date.newInstance(2020, 1, 1);
     
      insert testrp;
      return testrp;
    }
      
    private static integer ratePlanChargeId = 125481;
      
    public static zqu__ProductRatePlanCharge__c makeProductRatePlanCharge(String prp, String type, String model, String customType) {
      zqu__ProductRatePlanCharge__c testprpc = new zqu__ProductRatePlanCharge__c();
      testprpc.zqu__ProductRatePlan__c = prp;
      testprpc.name=type+model+ratePlanChargeId;
      testprpc.zqu__ZuoraId__c = String.valueOf(ratePlanChargeId);
      ratePlanChargeId++;
      if ('Welcome'.equals(customType)) {
        testprpc.zqu__UOM__c = 'MW';
      } else if ('AddOn'.equals(customType)) {
        testprpc.zqu__UOM__c = 'User x MW';
      } else {
        testprpc.zqu__UOM__c = 'test UOM';
      }
      testprpc.zqu__Type__c = type;
      testprpc.zqu__RecurringPeriod__c = 'Month';
      testprpc.zqu__PrepaymentPeriods__c = 1;
      testprpc.zqu__Model__c = model;
      testprpc.zqu__MinQuantity__c = 0;
      testprpc.zqu__MaxQuantity__c = 1000;
      testprpc.zqu__ListPrice__c = 5;
      testprpc.zqu__Description__c = 'Test charge';
      testprpc.zqu__DefaultQuantity__c = 5;
      testprpc.zqu__Discount_Apply_Type__c = 0;
      testprpc.zqu__Discount_Level__c = 'RatePlan';
      testprpc.zqu__Upto_How_Many_Periods__c = 0;
      testprpc.zqu__Deleted__c = false;

      insert testprpc;
      return testprpc;
    }
    
    
}