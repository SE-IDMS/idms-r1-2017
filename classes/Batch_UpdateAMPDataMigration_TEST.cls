@isTest
Public class Batch_UpdateAMPDataMigration_TEST
{
    public static testMethod void AMPDataMigTest()
    {
    Account a1 = Utils_TestMethods.createAccount();
        a1.AccountMasterProfile__c='';
        a1.ClassLevel1__c='RT';
        a1.MarketSegment__c='BD1';
        insert a1;
    System.debug('a1 Country__c :'+a1.Country__c);
     AccountMasterProfile__c amp1=new AccountMasterProfile__c();
        amp1.Account__c=a1.id;
        amp1.Q1Rating__c='2';
        amp1.Q2Rating__c='2';
        amp1.Q3Rating__c='2';
        amp1.Q4Rating__c='4';
        amp1.Q5Rating__c='3';
        amp1.Q6Rating__c='2';
        amp1.Q7Rating__c='4';
        amp1.Q8Rating__c='2';
        amp1.DirectSales__c=200;
        amp1.IndirectSales__c=300;
        amp1.totalPam__c=600;
        insert amp1;
        String q='Select Id from AccountMasterProfile__c where Account__r.ClassLevel1__c =\'RT\' and Account__r.MarketSegment__c=\'BD1\' and Account__r.Country__c='+a1.Country__c;
        Batch_UpdateAMPDataMigration dataMigBatch = new Batch_UpdateAMPDataMigration(q);
        Database.executeBatch(dataMigBatch,Integer.ValueOf(Label.CLQ316SLS009));
    }
}