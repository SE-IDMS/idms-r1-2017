/***
Q2 2016 release 
****/

@istest
public class Batch_ELLARepUserAddRemove_TEST {
    //public static User newUser{get;set;}
   // public static User newUser1{get;set;}
    static Testmethod  void Test_Batch_ELLARepUserAddRemove () { 
   
         Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
    
        List<LaunchReportStakeholders__c> newLReportSholder= new List<LaunchReportStakeholders__c>();
        User newUser = Utils_TestMethods.createStandardUser('ELLAMQ2');  
        
        newUser.BypassVR__c = TRUE;
         newUser.IsActive = TRUE;
          newUser.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger'; 
        User newUser1 = Utils_TestMethods.createStandardUser('Q2Share');  
        newUser1.BypassVR__c = TRUE;
        
        newUser1.UserRoleID = rol_ID ;
         newUser1.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger';  
        newUser1.IsActive = TRUE;     

        
        
        
      //  Database.SaveResult UserInsertResult = Database.insert(newUser, false);

        //Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
       insert newUser;
       insert newUser1; 
         
        system.runas(newUser1) {
            
      
        LaunchReportStakeholders__c LaunchReport = new LaunchReportStakeholders__c();
        LaunchReport.BUReport__c=true;
        LaunchReport.User__c=newUser1.id;
        LaunchReport.IsActive__c=true;
        newLReportSholder.add(LaunchReport);
        
        //insert newLReportSholder; 
        
        
        
        system.debug('TEST-->User'+newUser);
         system.debug('TEST-->'+newUser1);
         List<UserLogin> usersLoglist = [SELECT IsFrozen,UserId FROM UserLogin WHERE UserId =:newUser1.id and IsFrozen=false];  
        system.debug('TEST-->'+usersLoglist);
            
        Test.startTest();
        Set<string> Setid = new Set<string> {newUser1.id,newUser.id} ;
         Batch_ELLARepUserAddRemove erear = new Batch_ELLARepUserAddRemove(Setid);
         ID batchprocessid = Database.executeBatch(erear,Integer.valueOf(System.Label.CLAPR15I2P13)); 
         Test.stopTest();
        // String jobId = System.schedule('ScheduleApexClassTest',Time_EXP,erea);         
          }    
    }
    
     static Testmethod  void Test_Batch_ELLARepUserAddRemove1 () { 
   
         Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
    
        List<LaunchReportStakeholders__c> newLReportSholder= new List<LaunchReportStakeholders__c>();
        User newUser = Utils_TestMethods.createStandardUser('ELLAMQ2');  
        
        newUser.BypassVR__c = TRUE;
         newUser.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger'; 
        // newUser.IsActive = TRUE;
        User newUser1 = Utils_TestMethods.createStandardUser('Q2Share');  
        newUser1.BypassVR__c = TRUE;

        newUser1.UserRoleID = rol_ID ;
        newUser1.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger';  
        newUser1.IsActive = TRUE;  



        User newUser3 = Utils_TestMethods.createStandardUser('Q2ViAce');  
        newUser3.BypassVR__c = TRUE;

        newUser3.UserRoleID = rol_ID ;
        newUser3.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger';  
        newUser3.IsActive = TRUE;           

        

        
      //  Database.SaveResult UserInsertResult = Database.insert(newUser, false);

        //Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
       insert newUser;
       insert newUser1; 
       insert newUser3; 
        system.runas(newUser1) {
            
            
            
        PermissionSetAssignment psaObj = new PermissionSetAssignment();
        psaObj.AssigneeId = newUser1.Id;
        psaObj.PermissionSetId = label.CLJUN16ELLA05;
        insert psaObj;  
        
        PermissionSetAssignment psaObj1 = new PermissionSetAssignment();
        psaObj1.AssigneeId = newUser3.Id;
        psaObj1.PermissionSetId = label.CLJUN16ELLA07;
        //insert psaObj;  
        insert psaObj1; 
        LaunchReportStakeholders__c LaunchReport = new LaunchReportStakeholders__c();
        LaunchReport.BUReport__c=true;
        LaunchReport.User__c=newUser1.id;
        //LaunchReport.IsActive__c=true;
        newLReportSholder.add(LaunchReport);
        
        insert newLReportSholder; 
        
        system.debug('TEST-->'+newUser1);
        test.startTest();
        Set<string> Setid = new Set<string> {newUser1.id,newUser.id,newUser3.id} ;
         Batch_ELLARepUserAddRemove erear = new Batch_ELLARepUserAddRemove(Setid );
         ID batchprocessid = Database.executeBatch(erear,Integer.valueOf(System.Label.CLAPR15I2P13)); 
         test.stopTest();
        // String jobId = System.schedule('ScheduleApexClassTest',Time_EXP,erea);         
          }    
    }
    
     static Testmethod  void Test_Batch_ELLARepUserAddRemove2 () { 
   
         Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
    
        List<LaunchReportStakeholders__c> newLReportSholder= new List<LaunchReportStakeholders__c>();
        User newUser = Utils_TestMethods.createStandardUser('ELLAMQ2');  
        
        newUser.BypassVR__c = TRUE;
        // newUser.IsActive = TRUE;
         newUser.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger'; 
        User newUser1 = Utils_TestMethods.createStandardUser('Q2Share');  
        newUser1.BypassVR__c = TRUE;
        
        newUser1.UserRoleID = rol_ID ;
        newUser1.BypassTriggers__c='AP_Offer1;AP_Offer3,AP_UserActions,UserBeforeTrigger';  
        newUser1.IsActive = TRUE;       
        
        

        
      //  Database.SaveResult UserInsertResult = Database.insert(newUser, false);

        //Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
        insert newUser;
        insert newUser1; 
        system.runas(newUser1) {
            
            
        
   
        PermissionSetAssignment psaObj = new PermissionSetAssignment();
        psaObj.AssigneeId = newUser1.Id;
        psaObj.PermissionSetId = label.CLJUN16ELLA06;
        insert psaObj;  
        
        
             
        PermissionSetAssignment psaObj1 = new PermissionSetAssignment();
        psaObj1.AssigneeId = newUser.Id;
        psaObj1.PermissionSetId = label.CLJUN16ELLA06;
        insert psaObj1;  
        LaunchReportStakeholders__c LaunchReport = new LaunchReportStakeholders__c();
        LaunchReport.BUReport__c=true;
        LaunchReport.User__c=newUser1.id;
        //LaunchReport.IsActive__c=true;
        newLReportSholder.add(LaunchReport);
        
        insert newLReportSholder; 
        
        system.debug('TEST-->'+newUser1);
     
        test.startTest();
          String Time_EXP = '0 0 0 15 3 ? 2022';

        Set<string> Setid = new Set<string> {newUser1.id,newUser.id} ;
        Batch_ELLARepUserAddRemove erear = new Batch_ELLARepUserAddRemove(Setid);
        ID batchprocessid = Database.executeBatch(erear,Integer.valueOf(System.Label.CLAPR15I2P13)); 
        String jobId = System.schedule('ScheduleApexClassTest',Time_EXP,erear);
        test.stopTest();
                 
          }    
    }

   
}