public class BatchCreateTimesheetsHelper {

    public static Map<Id, Map<String, String>> returnSVMXProfileSettingMap (Set<Id> svmxProfileIds) {
    /*
        Method returns the Timesheet ServiceMax settings for the profiles specified
    */
        Set<String> settingsToQuery = new Set<String>{'TIMESHEET002', 'TIMESHEET003', 'PRORIGINATIONDATE'};
        
        List<SVMXC__ServiceMax_Config_Data__c> settingsList = [SELECT SVMXC__Internal_Value__c, SVMXC__Display_Value__c, 
                                                              SVMXC__Config_Profile_Name__c, SVMXC__Setting_Configuration_Profile__r.Id,
                                                              SVMXC__Setting_ID__r.SVMXC__SettingId__c,
                                                              SVMXC__Setting_ID__r.SVMXC__Default_Value__c
                                                              FROM SVMXC__ServiceMax_Config_Data__c WHERE 
                                                              SVMXC__Setting_Configuration_Profile__r.Id IN : svmxProfileIds
                                                              AND SVMXC__Setting_ID__r.SVMXC__SettingId__c IN : settingsToQuery];
        
        Map<Id, Map<String, String>> returnMap = new Map<Id, Map<String, String>>();
        
        for (SVMXC__ServiceMax_Config_Data__c cd : settingsList) {
            
            Map<String, String> workingMap = new Map<String, String>();
            
            String valueToUse = cd.SVMXC__Internal_Value__c;
            
            if (cd.SVMXC__Internal_Value__c == 'Default Value')
                valueToUse = cd.SVMXC__Setting_ID__r.SVMXC__Default_Value__c;
                    
            if (returnMap.containsKey(cd.SVMXC__Setting_Configuration_Profile__r.Id)) {
                
                workingMap = returnMap.get(cd.SVMXC__Setting_Configuration_Profile__r.Id);
                workingMap.put(cd.SVMXC__Setting_ID__r.SVMXC__SettingId__c, valueToUse);
                returnMap.put(cd.SVMXC__Setting_Configuration_Profile__r.Id, workingMap);
                
            } else {
                workingMap.put(cd.SVMXC__Setting_ID__r.SVMXC__SettingId__c, valueToUse);
                returnMap.put(cd.SVMXC__Setting_Configuration_Profile__r.Id, workingMap);    
            }
        }
        
        return returnMap;
    }        
    public static Map<Id, Id> returnSFDCToSVMXMapping(Set<Id> profileIds) {
    /*
        Method returns a Mapping between SFDC Profiles to SVMX Profiles
    */
        List<SVMXC__ServiceMax_Config_Data__c> profileList = [SELECT SVMXC__Profile__c, SVMXC__Access_Configuration_Profile__c
                                                             FROM SVMXC__ServiceMax_Config_Data__c 
                                                             WHERE SVMXC__RecordType_Name__c = 'Configuration Access'
                                                             AND SVMXC__Access_Configuration_Profile__c IN : profileIds];       
        
        Map<Id, Id> returnMap = new Map<Id, Id>();
        
        for (SVMXC__ServiceMax_Config_Data__c cd : profileList) {
            returnMap.put(cd.SVMXC__Profile__c, cd.SVMXC__Access_Configuration_Profile__c);
        }

        return returnMap;
    }
    
    public static Id returnOrgWideProfileId (Map<Id, SVMXC__ServiceMax_Config_Data__c> svmxProfiles) {
    /*
        Method returns the Org-Wide ServiceMax Profile Id 
    */    
        Id returnId = null;
        
        for (Id p : svmxProfiles.keySet()) {
            
            SVMXC__ServiceMax_Config_Data__c workingRecord = svmxProfiles.get(p);
            if (workingRecord.SVMXC__Active__c == true && workingRecord.SVMXC__Configuration_Type__c == 'Global') {
                returnId = workingRecord.Id;
                break;
            }
        }
        
        return returnId;
    }
    
    public static Map<Id, SVMXC__ServiceMax_Config_Data__c> returnSVMXProfiles () {
    /*
        Method returns all the SVMX Profiles
    */
        
        // query to get all the SVMXC Profile Ids
        Map<Id, SVMXC__ServiceMax_Config_Data__c> svmxProfileMap = 
            new Map<Id, SVMXC__ServiceMax_Config_Data__c>([Select Id, SVMXC__Active__c,
                                                          SVMXC__Configuration_Type__c 
                                                          FROM SVMXC__ServiceMax_Config_Data__c
                                                          WHERE SVMXC__RecordType_Name__c = 'Configuration Profile']);
        
        return svmxProfileMap;
    }
    
    public static Set<Id> techsNeedingTimesheet (Set<Id> techIds, Date dateToCheck) {
    /*
        Method to take incoming Set of Tech Ids and return Tech Ids that do not have a Timesheet 
        within the Date Period.
    */
        List<SVMXC_Timesheet__c> timesheetsList = [SELECT Technician__c FROM SVMXC_Timesheet__c 
                                                  WHERE Start_Date__c <= :dateToCheck AND End_Date__c >=:dateToCheck
                                                  AND Technician__c IN : techIds];
        
        if (!timesheetsList.isEmpty()) {
            
            // pull out Techs that already have a Timesheet created in the period being checked
            for (SVMXC_Timesheet__c ts : timesheetsList) {
                if (techIds.contains(ts.Technician__c))
                    techIds.remove(ts.Technician__c);       
            }      
        }
        
        return techIds;
    }
    
    public static SVMXC_Timesheet__c buildTimesheet(Map<String, String> timesheetSettings, Id technicianId, Date timesheetDate) {
    /*
        Method creates an individual TimeSheet against the Technician specified using the appropriate Timesheet Creation Rules 
    */
        
        SVMXC_Timesheet__c returnTimeSheet = new SVMXC_Timesheet__c();
        returnTimeSheet.Technician__c = technicianId;
        
        // duplication of functionality in ServiceMaxTimesheetUtils 
        if (timesheetSettings.get('TIMESHEET003').equalsIgnoreCase('true')) {
 
            returnTimeSheet.Start_Date__c = timesheetDate.toStartOfMonth();
            returnTimeSheet.End_Date__c = returnTimeSheet.Start_Date__c.addMonths(1).toStartOfMonth().addDays(-1);
        
        } else {
            
            Date prdate = Date.valueOf(timesheetSettings.get('PROriginationDate'));
            Integer cycleDays = Integer.valueOf(timesheetSettings.get('TIMESHEET002')) * 7;
            Integer diff = prdate.daysBetween(timesheetDate);
            Integer paycycleweeks = 0;
            
            if(diff >= 0 && diff < cycleDays) 
                
                returnTimeSheet.Start_Date__c = prdate;
            
            else if(diff >= cycleDays) {
                
                paycycleweeks =diff/cycleDays;     
                returnTimeSheet.Start_Date__c = prdate.addDays(  paycycleweeks * cycleDays); 
            
            } else if(diff < 0) {                 
                    System.debug('Invalid time entry start, so Timesheet will not be generated');
            }
                
            if (returnTimeSheet.Start_Date__c != null)
                returnTimeSheet.End_Date__c = returnTimeSheet.Start_Date__c.addDays(cycleDays).addDays(-1);
        } 
        return returnTimeSheet;
    }
}