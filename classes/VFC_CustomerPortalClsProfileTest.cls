@isTest
public class VFC_CustomerPortalClsProfileTest 
{

  public static testMethod void ShowContactDetails () 
  {
    Id RoleId;
    Id ProfileId;
    
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    User u;
    
    System.runAs (thisUser) 
    {
    
    // Create Account Owner User
    
    RoleId    = [Select Id From UserRole Where PortalType = 'None' Limit 1][0].Id;
    ProfileId = [Select Id from Profile  where name       = 'System Administrator'][0].Id;

    User owner = new User(
      alias             = 'aown', 
      email             = 'stephen.norbury@non.schneider-electric.com', 
      userroleid        = RoleId,
      emailencodingkey  = 'UTF-8',
      firstname         = 'Account',
      lastname          = 'Owner', 
      languagelocalekey = 'en_US',  
      localesidkey      = 'en_US', 
      profileid         = ProfileId,  
      timezonesidkey    = 'America/Los_Angeles', 
      username          = 'accountowner@compass.com'); 

    Database.insert (owner);
    
    // Create Partner Portal User
    
    Country__c country = new Country__c (
      Name           = 'Nowhere',
      CountryCode__c = 'NW',
      OwnerId        = owner.Id); 
    
    Database.insert (country);
       
    Account a = new Account (
      Name       = 'TEST ACCOUNT', 
      Street__c  = 'Rue des Ans', 
      City__c    = 'Paris', 
      ZipCode__c = '77650',
      Country__c = country.Id,
      OwnerId    = owner.Id); 
    
    Database.insert (a);
    
    Contact c = new Contact (
     accountId = a.id,
     email     = 'stephen.norbury@non.com', 
     firstname = 'firstname',
     lastname  = 'lastname'); 

    Database.insert (c);
    
    ProfileId = [select Id from Profile where Name = 'Gold Partner User'][0].Id;
    
    Id LicenceId = [select Id from UserLicense where Name = 'Gold Partner'][0].Id;
    
    RoleId    = [select Id from UserRole where Name = 'CEO'][0].Id;
    
    u = new User(
      contactId         = c.Id,
      alias             = 'hasrole', 
      email             = 'stephen.norbury@non.schneider-electric.com', 
      emailencodingkey  = 'UTF-8',
      firstname         = 'firstname',
      lastname          = 'Testing', 
      languagelocalekey = 'en_US',  
      localesidkey      = 'en_US', 
      profileid         = ProfileId,  
      timezonesidkey    = 'America/Los_Angeles', 
      username          = 'testuser@compass.com'); 

    Database.insert (u);
    }
    
    // Run Tests as Portal User
    
    System.runAs(u) 
    { 
      VFC_CustomerPortalClsProfile controller = new VFC_CustomerPortalClsProfile ();
    /* 
    account a = new account(name = 'name');
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345');
    
    controller.setusid(UserInfo.getUserId());
    controller.setcontid(c.id);
    controller.setaccid(a.id); 

    System.assertEquals(UserInfo.getUserId(), controller.getusid());
    System.assertEquals(c.id, controller.getcontid());
    System.assertEquals(a.id, controller.getaccid());
 
        controller.setContactName     ('Stephen Norbury');
    controller.setContactEmail    ('stephen.norbury@non.schneider-electric.com');
    controller.setContactFax      ('017272871');
    controller.setContactPhone    ('018382829');
    controller.setContactMobile   ('069393920');
    controller.setContactStreet   ('Rue des Graves');
    controller.setContactCity     ('Rueil Malmaison');
    controller.setContactZip      ('77650');
    controller.setContactState    ('IDF');
    controller.setContactCountry  ('France');
    controller.setAccountName     ('Schneider Electric France');
    controller.getContactName     ();
    controller.getContactEmail    ();
    controller.getContactFax      ();
    controller.getContactPhone    ();
    controller.getContactMobile   ();
    controller.getContactStreet   ();
    controller.getContactCity     ();
    controller.getContactZip      ();
    controller.getContactState    ();
    controller.getContactCountry  ();
    controller.getAccountName     ();
  */
   } 
  }
}