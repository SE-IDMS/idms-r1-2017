@isTest
public class AP_ParseListViewMetadata_TEST
{
  static testMethod void showDomainsOfExpertise()
  {
    
    
    CS_PRM_ApexJobSettings__c  cJobSetting = new CS_PRM_ApexJobSettings__c();
    cJobSetting.LastRun__c = Date.today();
    cJobSetting.InitialSync__c = true;
    cJobSetting.Name = 'ParseListViews';
    cJobSetting.ObjectToParse__c = 'Opportunity,Account,OpportunityRegistrationForm__c';
    cJobSetting.RelatedObjects__c = 'OPP_ProductLine__c,OpportunityRegistrationProducts__c,ActivityHistory,OpenActivity,Note';
    insert cJobSetting;
    
    AP_ParseListViewMetadata classInstance = new AP_ParseListViewMetadata();
    
    List<CS_PRM_ApexJobSettings__c> listValue = [SELECT LastRun__c, InitialSync__c, ObjectToParse__c, RelatedObjects__c FROM CS_PRM_ApexJobSettings__c];
    
    System.debug('ListValue------'+listValue);
   
    classInstance.doParse();
  }
}