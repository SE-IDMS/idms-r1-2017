/**
 * @author Benjamin LAMOTTE
 * @date Creation 12/01/2016
 * @description Class that handles logics on the ZQuote object
 */
public with sharing class ZuoraQuoteUtilities {


	/**
	 * @author Benjamin LAMOTTE
	 * @date Creation 12/01/2016
	 * @description Calculates initial and renewal terms on the quote based on its rate plans
	 * @param zQuote : Quote to process
	 */
	public static zqu__Quote__c calculateTerms(zqu__Quote__c zQuote){
            LIST<AggregateResult> termsResults = new List<AggregateResult>([SELECT MAX(zqu__ProductRatePlan__r.TermPeriod__c) initialTerm, MAX(zqu__ProductRatePlan__r.RenewPeriod__c) renewTerm
																			FROM zqu__QuoteRatePlan__c
																			WHERE zqu__Quote__c =: zQuote.Id AND (zqu__ProductRatePlan__r.zqu__ZProduct__r.Type__c =:'Primary' OR zqu__ProductRatePlan__r.zqu__ZProduct__r.Type__c =:'Standalone')
																			GROUP BY zqu__Quote__c]);
	
			if(termsResults.size() == 1){
				zQuote.zqu__InitialTerm__c = Integer.valueOf(termsResults[0].get('initialTerm'));
				zQuote.zqu__RenewalTerm__c = Integer.valueOf(termsResults[0].get('renewTerm'));	
			}

			LIST<zqu__QuoteRatePlan__c> qrpList = new List<zqu__QuoteRatePlan__c>([SELECT zqu__ProductRatePlan__r.AutoRenew__c
																					FROM zqu__QuoteRatePlan__c
																					WHERE zqu__Quote__c =: zQuote.Id 
																					AND (zqu__ProductRatePlan__r.zqu__ZProduct__r.Type__c =:'Primary' 
																					OR zqu__ProductRatePlan__r.zqu__ZProduct__r.Type__c =:'Standalone')]);

			boolean autoRenewRequired = false;
			for (zqu__QuoteRatePlan__c qrp : qrpList) {
				if (qrp.zqu__ProductRatePlan__r.AutoRenew__c == 'Y') {
					autoRenewRequired = true;
				}
			}

			if (autoRenewRequired) {
				zQuote.zqu__AutoRenew__c = true;
			} else {
				// deactivate autorenew ???
			}

			//if one of the terms is null, return a null objetc
			if(zQuote.zqu__InitialTerm__c == null  || zQuote.zqu__RenewalTerm__c == null){
				zQuote.zqu__InitialTerm__c = null;
				zQuote.zqu__RenewalTerm__c = null;
			}

			return zQuote;
	}

}