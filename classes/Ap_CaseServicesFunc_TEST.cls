/**
30/03/15    
Jyotiranjan Singhlal  
APR 15 Release    
Initial Creation: test class for Ap_CaseServicesFunc
 */
@isTest(SeeAllData=true)
private class Ap_CaseServicesFunc_TEST {
 
 public static testMethod void myUnitTest() {
 
        //Country
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='TCY';
        Database.insert(objCountry);
        
        //Account
       /* Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);*/
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        //Contact
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        //Location
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = objAccount .id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        //Brand        
        Brand__c brand1_1 = new Brand__c();
        brand1_1.Name ='Brand 1-1';
        brand1_1.SDHBRANDID__c = 'Test_BrandSDHID';
        brand1_1.IsSchneiderBrand__c = true;
        insert brand1_1 ;
        
        //Device Type
        DeviceType__c dt1_1 = new DeviceType__c();
        dt1_1.name = 'Device Type 1-1';
        dt1_1.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-1';
        insert dt1_1 ;
        
        //Range
        Category__c c1_1 = new Category__c();
        c1_1.Name =  'Range 1-1';
        c1_1.CategoryType__c = 'RANGE';
        c1_1.SDHCategoryID__c = 'Test_RangeSDHID1-1';
        insert c1_1;
        
        //Product
        Product2 p1 = new Product2();
        p1.SKU__c= 'SKU';
        p1.Name = 'Test Product ';
        p1.Brand2__c= brand1_1.id;
        p1.DeviceType2__c = dt1_1.id;
        p1.CategoryId__c = c1_1.id;
        insert p1;
        
        //Service contract
        SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
        testContract0.SVMXC__Company__c = objAccount.Id;
        testContract0.Name = 'Test Contract0';  
        testContract0.SVMXC__Contact__c = objContact.Id  ;    
        testContract0.SVMXC__Start_Date__c = system.today();
        //testContract0.SVMXC__Renewed_From__c = null;
        insert testContract0 ;
        
        //Service Line
        SVMXC__Service_Contract__c testContract1 = new SVMXC__Service_Contract__c();
        testContract1.SVMXC__Company__c = objAccount.Id;
        testContract1.Name = 'Test Contract1';  
        testContract1.SVMXC__Contact__c = objContact.Id  ;    
        testContract1.SVMXC__Start_Date__c = system.today();
        testContract1.ParentContract__c = testContract0.Id;
        insert testContract1 ;
        
        //Installed Product
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = objAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.SVMXC__Product__c = p1.id;
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c = 'device1';
        ip1.LifeCycleStatusOfTheInstalledProduct__c = system.label.CLAPR14SRV05;
        ip1.DecomissioningDate__c = date.today();
        ip1.AssetCategory2__c =system.label.CLAPR14SRV01;
        ip1.SVMXC__Serial_Lot_Number__c = '1234';
        insert ip1;
        
        //Case
        Case objCase =  New Case();
        objCase.Status='In Progress';
        objCase.AccountId=objAccount.id;
        objCase.ContactId=objContact.id;
        objCase.SupportCategory__c='6-General';
        objCase.Priority='Medium';
        objCase.Subject='Complaint';
        objCase.origin='Phone';
        objCase.SVMXC__Service_Contract__c =testContract1.Id;
        objCase.SVMXC__Component__c=ip1.Id;
        
        insert objCase;
    }
}