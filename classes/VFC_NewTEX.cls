/*   Author: Sukumar Salla (GD Team)  
     Date Of Creation: 10-July-2013   
     Description : Controller for the Technical Expert Assessment creation page 
*/

public class VFC_NewTEX
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/

    public TEX__c NewTEX{get;set;} // Current new TEX
    public Case relatedCase{get;set;} // Master Case
    public TEX__c relatedTex{get;set;}
    public CustomerSafetyIssue__c relatedCasecs{get;set;}
    Public Boolean IsCancel{get; set;} // Flag to display Cancel Button
       

    /*=======================================
      CONSTRUCTOR
    =======================================*/ 

    public VFC_NewTEX(ApexPages.StandardController controller)
    {
        NewTEX = (TEX__c)controller.getRecord();
      
    } 
    
    /*=======================================
      METHODS
    =======================================*/ 
 
    // Method to initialize the RMA with the defaulting values
    public void initializeTEX(TEX__c aNewTEX)
    {
        relatedCase = New Case();
        relatedCaseCS=new CustomerSafetyIssue__c();             
        String caseID = system.currentpagereference().getParameters().get(Label.CL00330);
                  
            if (caseID != null)
            {
                caseID = caseID.substring(1); 
            } 
 
        aNewTEX.AssessmentType__c = null;
        aNewTEX.ClosedDate__c     = null;
        aNewTEX.ExpertCenter__c   = null;
        aNewTEX.OpenDate__c       = null;
        aNewTEX.Prioritylevel__c  = null;
        aNewTEX.Status__c         = null;
        
        
        // Retrieve related case
        List<Case> caseList = [SELECT ID,Account.TECH_I2PNotification__c,Account.Country__r.FrontOfficeOrganization__c,Account.Country__r.FrontOfficeOrganization__r.Name,Contact.name,contact.WorkPhone__c, Contact.Email , Contact.Phone,CommercialReference__c,Family__c,CaseNumber, AccountID,GSAImage__c, ContactID,Subject,Status,Priority,CustomerRequest__c, Quantity__c  from Case WHERE ID=:caseID];       
        //system.debug('## caseList ##'+ caseList[0]);
        if(caseList.size() == 0)
        {
        caseList = [SELECT ID,Account.TECH_I2PNotification__c, Account.Country__r.FrontOfficeOrganization__c,Account.Country__r.FrontOfficeOrganization__r.Name,Contact.name,contact.WorkPhone__c, Contact.Email , Contact.Phone,CommercialReference__c,Family__c,CaseNumber, AccountID, GSAImage__c, ContactID,Subject,Status,Priority,CustomerRequest__c, Quantity__c from Case WHERE ID=:newTEX.Case__c];
        }
        
        if(caseList.size() == 1)
        {
            relatedCase = caseList[0];
        }
        
        /*//Ram Chilukuri added this code for set the Priority level
          List<CustomerSafetyIssue__c> ListCSI=[SELECT Id,RelatedbFOCase__c FROM CustomerSafetyIssue__c where RelatedbFOCase__c =: caseID];
            if(ListCSI.size() == 0)
            {
                 ListCSI=[SELECT Id,RelatedbFOCase__c FROM CustomerSafetyIssue__c where RelatedbFOCase__c =: newTEX.Case__c];
            }
            if(ListCSI.size() == 1)
                {
                    relatedCaseCS = ListCSI[0];
                }
         */
               
    }
    
    // Action to switch automatically to the edit page with pre-defaulted values
    public pagereference goToEditPage()
    {   
        // Pre-populate fields of the edit page
        initializeTEX(NewTEX);
        
        // TEX should be created from Case.
        // If CommercialReference is null on Case, we should not be able to create TEX.
       
        if(relatedCase.id==null)
        {
          IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT13RR22)); //'Technical Expert Assessment needs to be created from the Case.'
                return null;
        }
        
        else IF( relatedCase.CommercialReference__c==null && relatedCase.Family__c==null &&  !Test.isRunningTest())
            {
                IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT13RR26)); //'Technical Expert Assessment cannot be created without Commercial Reference or Family.'
                return null;
            }
           // SEP 2014 Release: BR5806 - kiran kareddy - 13Aug2014
            else IF( relatedCase.Status==Label.CLSEP14I2P03)
            {
                IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLSEP14I2P02)); //'Technical Expert Assessment cannot be created if case is closed.'
                return null;
            } 
            //Oct 15 Rel : BR-7743 -- Divya M 
            else IF((relatedCase.CustomerRequest__c == null || relatedCase.CustomerRequest__c == '' )|| (relatedCase.Quantity__c == null)){
                IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT15I2P01)); //'Technical Expert Assessment cannot be created if Customer Request or Quantity on the related Case is blank.'
                return null;
            }
            //Ends Divya M 
        // Create a new edit page for the RMA 
        PageReference aNewTEXEditPage = new PageReference('/'+SObjectType.TEX__c.getKeyPrefix()+'/e' );
        
        //April 2015 Release added by Uttara
        aNewTEXEditPage.getParameters().put(Label.CLOCT13RR38, relatedCase.Priority); // '00NA000000ALIka' -  Priority Field ID on TEX object
        
        // Set the return to the parent Case (retURL)
        aNewTEXEditPage.getParameters().put(Label.CL00330, NewTEX.Case__c); //'retURL'
         /*if(relatedCaseCS.id!=null)
          {
           //aNewTEXEditPage.getParameters().put(Label.CLOCT13RR38, Label.CLOCT13RR39); // 'High'
           aNewTEXEditPage.getParameters().put(Label.CLOCT13RR38, relatedCase.Priority); // '00NA000000ALIka' -  Priority Field ID on TEX object         
          }
          else
            {
             if (relatedCase.Account.TECH_I2PNotification__c== Label.CLOCT14I2P17) //'GSA'
                {
                  //aNewTEXEditPage.getParameters().put(Label.CLOCT13RR38,Label.CLOCT13RR40); // 'Critical'
                  aNewTEXEditPage.getParameters().put(Label.CLOCT13RR38, relatedCase.Priority); //'00NA000000ALIka' -  Priority Field ID on TEX object
                }
            }*/
            
            
        // Disable override
        aNewTEXEditPage.getParameters().put(Label.CL00690, Label.CL00691); //'nooverride', '1'
        
        // Pre-default the parent case with the ID (_lkid) and the CaseNumber          
        aNewTEXEditPage.getParameters().put(Label.CLOCT13RR01, NewTEX.Case__c); //'CF00NA000000ALIjo_lkid' - CF + TEX__c.Case__c Field ID + _lkid
        aNewTEXEditPage.getParameters().put(Label.CLOCT13RR02, relatedCase.CaseNumber); //'CF00NA000000ALIjo' - CF + TEX__c.Case__c Field ID
        
        //May 2014 Release :BR-5326
        if(relatedCase.CommercialReference__c!=null)
            aNewTEXEditPage.getParameters().put(Label.CLOCT13RR58, relatedCase.CommercialReference__c);  //'00NA000000ALIjm' - Affected Offer field id on TEX
        else
            aNewTEXEditPage.getParameters().put(Label.CLOCT13RR58, relatedCase.Family__c); //'00NA000000ALIjm' - Affected Offer field id on TEX
        
        // APRIL 2014 Release: BR4672 - RamPrasad Chilukuri - 03fEB2014
        aNewTEXEditPage.getParameters().put(Label.CLAPR14I2P08, String.valueOf(relatedCase.Contact.name));  //'00NA000000ALIju' - Contact Name field ID on TEX__c
        aNewTEXEditPage.getParameters().put(Label.CLAPR14I2P09,relatedCase.Contact.Email);  // '00NA000000ALIjt' - Contact Email field ID on TEX__c
        aNewTEXEditPage.getParameters().put(Label.CLAPR14I2P10,String.valueOf( relatedCase.contact.WorkPhone__c));   // '00NA000000ALIjv' - Contact Phone Field ID on TEX__c
        aNewTEXEditPage.getParameters().put(Label.CLAPR14I2P13,relatedCase.Account.Country__r.FrontOfficeOrganization__r.Name);  //'CF00NA000000ANQ6G' - CF+FrontOfficeOrganization field ID on TEX__c
         
         // SEP 2014 Release: BR4710 - kiran kareddy - 13Aug2014
        aNewTEXEditPage.getParameters().put(Label.CLSEP14I2P01, relatedCase.Subject);  //'00NA000000B0VcE'

        // pass the saveURL
        // CLOCT13RR04 - saveURL
        // CLOCT13RR03 - '/apex/VFP_InsertExpertCenter'
           aNewTEXEditPage.getParameters().put(Label.CLOCT13RR04, Label.CLOCT13RR03 ); //'saveURL', '/apex/VFP_InsertExpertCenter'
        return aNewTEXEditPage;                                                    
    }
    
    Public Pagereference doCancel()
    {
         /*   
         string CaseId = relatedCase.Id;
         Pagereference pr = new PageReference('/'+ CaseId);
         pr.setRedirect(true);
         return pr;
        */
         Pagereference pr ;
         string CaseId = relatedCase.Id;
         if(CaseId!=null)
         {
         pr = new PageReference('/'+ CaseId);
         pr.setredirect(true);
         return pr;
         }
         else
         {
         pr = new PageReference(label.CLOCT13RR56); // '/a8Y/o'
         //CLDEC12RR35 - /a3T/o
         pr.setredirect(true);
         return pr;
         }
    }               
}