public class Ap_Brand extends TriggerBase{

    
    private map<Id, Brand__c> newMap;
    private map<Id, Brand__c> oldMap;
    private List<Brand__c> newRecList;
    Set<String> ExistGoldenIDSet = new Set<String>();
    
    public Ap_Brand(boolean isBefore, boolean isAfter, boolean isUpdate, boolean isInsert, boolean isDelete,
        List<Brand__c> newList,
        map<Id, Brand__c > oldMap, map<Id, Brand__c> newMap) {
        super(isBefore, isAfter, isUpdate, isInsert, isDelete);

        this.newMap = newMap;
        this.oldMap = oldMap;
        this.newRecList = newList;
        
    }
    
    public Ap_Brand() {
    }
    public override void onBeforeInsert() {
        
        Set<String> GoldenAssetSet = new Set<String>();        
        
        for(Brand__c Obj :newRecList){
            
            if(Obj.SDHBRANDID__c != null)
            GoldenAssetSet.add(Obj.SDHBRANDID__c);         
        }
        if(GoldenAssetSet != null && GoldenAssetSet.size()>0)
        {
            for(Brand__c obj:[Select id,SDHBRANDID__c from Brand__c where SDHBRANDID__c in :GoldenAssetSet]){
                ExistGoldenIDSet.add(obj.SDHBRANDID__c);
            }
        }
        if(Utils_SDF_Methodology.canTrigger('SDHBRANDIDCHECK')|| Test.isRunningTest() )
        sdhGoldenUniqeCheck();
        
        
        
    }
    public  void sdhGoldenUniqeCheck() {
        for(Brand__c Obj :newRecList){
            
            if(Obj.SDHBRANDID__c != null)
            {
                if(ExistGoldenIDSet.contains(Obj.SDHBRANDID__c))
                Obj.addError('SDH Brand Id  already Exist');
                
            }
        }
        
    }



}