@isTest
private class VFC39_AddUpdateProductforWorkOrder_TEST
{
    static testMethod void TestWO() 
    {
        Account Acc = Utils_TestMethods.createAccount();
        insert Acc;
        Contact contact1 = Utils_TestMethods.createContact(Acc.id, 'Doe');
        Insert contact1;
        SVMXC__Service_Order__c workOrder1 = Utils_TestMethods.createWorkOrder(Acc.Id);
        insert WorkOrder1;
        
        string [] searchfilterList= new string[]{};
            
        for(Integer token=0;token<17;token++)
        {
            searchfilterList.add('a');          
        }
            
        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);
            
        sObject tempsObject = tempresult.recordList[0];
       
        VCC08_GMRSearch ProductforWo1 = new VCC08_GMRSearch();
        ProductforWo1.ActionType= 'simple';
        VCC06_DisplaySearchResults DisplaySearchResultsController =ProductforWo1.resultsController;  
        VCC05_DisplaySearchFields DisplaySearchFieldsController = new VCC05_DisplaySearchFields(); 
            
        DisplaySearchFieldsController.pickListType = 'PM0_GMR';
        DisplaySearchFieldsController.SearchFilter = new String[4];
        DisplaySearchFieldsController.SearchFilter[0] = '00';
        DisplaySearchFieldsController.SearchFilter[1] = Label.CL00355;    
        DisplaySearchFieldsController.searchText = 'search\\stest';
        DisplaySearchFieldsController.key = 'searchComponent';
        DisplaySearchFieldsController.PageController = ProductforWo1 ;
        DisplaySearchFieldsController.init();
        ProductforWo1.searchController = DisplaySearchFieldsController;   
        DisplaySearchFieldsController.searchText = 'search\\stest';
        ProductforWo1.search();
        ProductforWo1.clear();
            
        ProductforWo1.ObjID = workOrder1.ID;
        ProductforWo1.Cleared = false;
        
        
        ApexPages.StandardController WoStandardController = new ApexPages.StandardController(workOrder1);
        VFC39_AddUpdateProductforWorkOrder WOController = new VFC39_AddUpdateProductforWorkOrder(WoStandardController);
        
        Utils_GMRSearchMethods WOImplementation = WOController.WOImplementation;
        WOImplementation.init(workOrder1.id, ProductforWo1);
        WOImplementation.PerformAction(tempresult.recordList[0], ProductforWo1);
        //ProductforWo1.ActionType= 'multiple';
        //WOImplementation.PerformAction(tempresult.recordList[0], ProductforWo1);
        WOImplementation.cancel(ProductforWo1); 
    }
}