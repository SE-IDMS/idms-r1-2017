public class VFC_RunSpecifiedTestsNew
    {   
    public List<String> CompListID{get;set;}
    public List<APApexTestClass__c> NewCompList = new List<APApexTestClass__c>();
    public String compXMLResult {get;set;}   
    //Map<string,APApexTestClass__c> CompMap = new Map<String,APApexTestClass__c>();
    //Map<String, List<String>> packageComponentMap = new Map<String, List<String>>();
    //Map<Id, APApexTestClass__c> apxTestCls = new Map<Id, APApexTestClass__c>([Select id,Name,TestClass__c,Type__c FROM APApexTestClass__c]);
        
    public VFC_RunSpecifiedTestsNew(ApexPages.StandardSetController controller) 
    {          
        CompListID = new List<String>(); 
        List<APApexTestClass__c> components = (List<APApexTestClass__c>)controller.getSelected();
        
        for(APApexTestClass__c compItem:components) {  
            CompListID.add(compItem.ID);
        }
        
       If(CompListID != null && CompListID.size()>0){
           NewCompList = [Select Track__c,Name,RunTest__c,TechClassTriggerName__c,Type__c,TestClass__c from APApexTestClass__c Where Id IN:CompListID AND RunTest__c = TRUE];
       }
        
       Set<String> setApexTestClass = new Set<String>();
        
       for (Integer i = 0; i< NewCompList.size(); i++)
       {
           setApexTestClass.add(NewCompList[i].TestClass__c); // contains distict accounts
       }
       
       If(setApexTestClass!= null && setApexTestClass.size()>0)
       {    
           compXMLResult = RunSpecifiedTestXML(setApexTestClass);           
       }
       else
       {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Records found!, Check Run Test flag..');
        ApexPages.addmessage(myMsg);
       }
    }
    
    public string RunSpecifiedTestXML(Set<String> pckComponentMap)
    {
        XmlStreamWriter w = new XmlStreamWriter();  
           for(String objComponent: pckComponentMap)
           {   
              if(objComponent!=null){
                  w.writeStartElement(null, 'runTest', null);
                  w.writeCharacters(objComponent);
                  w.writeEndElement(); // End Members
              }             
           } 
            w.writeCharacters('\r\n');
            w.writeCharacters('*****************************************************************************************************************');
            w.writeCharacters('\r\n');
          for(String objComponent: pckComponentMap)
          {   
              if(objComponent!=null){
                  w.writeCharacters(objComponent);
                  w.writeCharacters(',\r\n');
              }              
          }                    
          String xmlOutput = w.getXmlString();         
          w.close();        
          return xmlOutput;
          } 
}