global class PartnerProgramResult {
    // WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfPrograms; 
    WebService PartnerProgram[] Programs { get; set; }
    WebService Datetime BatchMaxLastModifiedDate{get;set;}
}