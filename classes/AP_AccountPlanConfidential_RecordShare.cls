/*
 Author: Anjali Gopinath
 Date created: 17 Oct 2012
 Description: Common class for functions in Account Plan Confidential. Called from 
                 1. After Update Trigger on Account Plan 
                 2. After Insert Trigger on Account Plan Confidential 
*/
public with sharing class AP_AccountPlanConfidential_RecordShare
{
    /** START
        Function to share Account Plan Confidential record with the User in Account Plan "Account Plan Owner" field 
    */
    public static void shareAPCRecordWithAccountPlanOwner(List<SFE_AccPlan__c> lstNewAccPlan)
    {
        List <Utils_RecordSharing.RecordShare> lstRecordShare=new List<Utils_RecordSharing.RecordShare>();
        for(AccountPlanConfidential__c apc : [select id, AccountPlan__c, AccountPlan__r.AccountPlanOwner__c from AccountPlanConfidential__c where AccountPlan__r.ID IN :lstNewAccPlan])
        {             
             lstRecordShare.add(createRecordShareInstance(apc));  
        }
        if(lstRecordShare.size()>0)
        {
            try
            {
                Utils_RecordSharing.createShareObjectRecordInSFDC(lstRecordShare);    
            }
            catch(Exception e)
            {
                system.debug('...@...Exception in AP_AccountPlanConfidential.shareAPCRecordWithAccountPlanOwner()');
                system.debug('...@@...'+e);
            }
        }
    }    
    
    public static void shareRecordWithAccountPlanOwner(List<AccountPlanConfidential__c> lstNewAccPlanConf)
    {   
        
        List <Utils_RecordSharing.RecordShare> lstRecordShare=new List<Utils_RecordSharing.RecordShare>();
        for(AccountPlanConfidential__c apc : [select id, AccountPlan__c, AccountPlan__r.AccountPlanOwner__c from AccountPlanConfidential__c where ID IN :lstNewAccPlanConf])
        {            
            lstRecordShare.add(createRecordShareInstance(apc));    
        }
        if(lstRecordShare.size()>0)
        {
            try
            {
                Utils_RecordSharing.createShareObjectRecordInSFDC(lstRecordShare);    
            }
            catch(Exception e)
            {
                system.debug('...@...Exception in AP_AccountPlanConfidential.shareRecordWithAccountPlanOwner()');
                system.debug('...@@...'+e);
            }
        }
        
    }
    public static Utils_RecordSharing.RecordShare createRecordShareInstance(AccountPlanConfidential__c apcTemp)
    {        
        Utils_RecordSharing.RecordShare recordShare =new Utils_RecordSharing.RecordShare();        
        recordShare.strUserId = apcTemp.AccountPlan__r.AccountPlanOwner__c;
        recordShare.sobjToObject = new AccountPlanConfidential__Share();
        recordShare.strToRecordId =apcTemp.Id;
        recordShare.strRowCause = Schema.AccountPlanConfidential__Share.RowCause.Sharing_With_AccountPlan_AssignedTo_User__c;
        recordShare.strRecordAccessLevel = Label.CL10240;  //Read:CL00418 , Edit:CL10240
        return recordShare;
    }
     /** END
        Function to share Account Plan Confidential record with the User in Account Plan "Account Plan Owner" field 
    */
    
    
}