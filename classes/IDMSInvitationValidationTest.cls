/**
* This test class covers IDMSInvitationValidation,IDMSInvitationValidationRest and IDMSResponseWrapperInvitation classes.
**/

@isTest
class IDMSInvitationValidationTest{
    //This method tests Send Invitation rest API service.
    static testmethod void testInvitationValidationRestService(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/SendInvitation';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        test.Starttest();
        IDMSResponseWrapperInvitation responseWrapper=IDMSInvitationValidationRest.doPut('prashanthgs1990@gmail.com','Test Invitition1','https://www.google.com');
        System.assert(responseWrapper!=null);
        IDMSResponseWrapperInvitation responseWrapper1=IDMSInvitationValidationRest.doPut('prashanthgs1990gmail.com','Test Invitition1','https://www.google.com');
        System.assert(responseWrapper1!=null);
        IDMSResponseWrapperInvitation responseWrapper2=IDMSInvitationValidationRest.doPut('','','');
        System.assert(responseWrapper2!=null);
        IDMSResponseWrapperInvitation responseWrapper3=new IDMSResponseWrapperInvitation();
        
        IDMSStatusCodeMapping__c MapErrorCode = new  IDMSStatusCodeMapping__c();
        MapErrorCode.name = 'Invalid Picklist values';
        MapErrorCode.httpCode__c = 400; 
        insert MapErrorCode; 
        IDMSResponseWrapperInvitation responseWrapper4=new IDMSResponseWrapperInvitation();
        IDMSResponseWrapperInvitation responseWrapper5=new IDMSResponseWrapperInvitation('Test status1','Test message1');
        test.Stoptest(); 
    }
    //This method tests wrapper class of Send Invitation API. 
    static testmethod void testIDMSResponseWrapperInvitation(){
        IDMSResponseWrapperInvitation responseWrapper4=new IDMSResponseWrapperInvitation();
        IDMSResponseWrapperInvitation responseWrapper5=new IDMSResponseWrapperInvitation('Test status2','Test message2');
    }
    
}