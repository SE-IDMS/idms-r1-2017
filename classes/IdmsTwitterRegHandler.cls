//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This is Twitter registration handler which creates both @Home and @Work Users.
//
//*******************************************************************************************

global class IdmsTwitterRegHandler implements Auth.RegistrationHandler{
    global boolean canCreateUser(Auth.UserData data) {
        return true;
    }
    //Test 1: creating a user record using the auth data
    global User createUser(Id portalId, Auth.UserData data){
        
        String appid;
        String context;
        String registrationSource;
        //Custom setting to retrieve an application Id.
        IdmsSocialUser__c socialUser  = IdmsSocialUser__c.getInstance('IdmsKey');
        appid                         = socialUser.AppValue__c;
        
        //code for getting context and app details        
        IDMSApplicationMapping__c appMap;
        appMap = IDMSApplicationMapping__c.getInstance(appid);
        if(appMap!=null){
            context            = (String)appMap.get('context__c');
            registrationSource = (String)appMap.get('AppName__c');
        }
        if(context == 'Home' || context == 'home'){
            User usr_home;
            Id ProfileId = getProfileId(context);
            //Create Consumer Account and return ID.    
            try{
                Id contactId = createPersonAccountContact(data);
                if(ProfileId != null){
                    usr_home = createIdmsUser(data, ProfileId);
                }
                usr_home.contactId              = contactId;
                usr_home.IDMS_User_Context__c   = Label.CLQ316IDMS090;
                insert usr_home;
            }catch (Exception e){
                System.debug('Error:' + e.getMessage() + e.getStackTraceString());
            }
            return usr_home;
        }else {
            User usr_work;            
            Id ProfileId = getProfileId(context);
            try{
                if(ProfileId != null){
                    usr_work = createIdmsUser(data, ProfileId);
                }  
                //call bFO method to create at work users with Digital accounts.       
                bFoMatchingResult__c bfoMatchingRes = AP_MatchingModule.preMatch(usr_work);
                usr_work.ContactId                  = bfoMatchingRes.Contact__c;
                usr_work.IDMS_User_Context__c       = Label.CLQ316IDMS080;
                Insert usr_work;
            }catch (Exception e){
                System.debug('Error:' + e.getMessage() + e.getStackTraceString());
            }
            return usr_work;
        }
    }
    
    //Test 2: Create at home or at work users
    private User createIdmsUser(Auth.UserData data, Id ProfileId) {
        User u                          = new User();
        u.username                      = data.identifier+Label.CLQ316IDMS028+Label.CLJUN16IDMS71;
        u.email                         = data.identifier+Label.CLQ316IDMS028;
        u.lastName                      = data.fullName;
        u.alias                         = 'Alias';
        u.languagelocalekey             = 'en_US';
        u.localesidkey                  = 'en_US';
        u.emailEncodingKey              = 'UTF-8';
        u.timeZoneSidKey                = 'America/Los_Angeles';
        u.IDMS_Registration_Source__c = Label.CLQ316IDMS022;
        u.IsIDMSUser__c                 = True;        
        u.IDMSIdentityType__c           = Label.CLQ316IDMS091;
        u.profileid                     = ProfileId;
        return u;
    }
    //Test 3: Create person account using auth data
    private Id createPersonAccountContact(Auth.UserData data) {
        Account person = new Account();
        try{
            
            person.LastName                         = data.fullName;
            person.personEmail                      = data.identifier+Label.CLQ316IDMS028;
            person.IDMS_Contact_UserExternalId__pc  = data.identifier+Label.CLQ316IDMS028+Label.CLJUN16IDMS71;
            person.RecordTypeId = [Select Id From RecordType 
                                   Where SobjectType='Account' 
                                   AND isPersonType=true LIMIT 1].id;
            
            insert person;
        }catch(Exception e) {
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
        }
        /**
        * This next step is necessary to get a valid contact Id,
        * it won't exist until the PersonAcct is saved
        **/         
        Account a = [Select PersonContactId From Account Where Id = :person.Id];
        return a.PersonContactId; 
    }
    
    //method that provides the profile Id related to the context in parameter
    public Id getProfileId(String context){
        String profilename = IDMS_Profile_Mapping__c.getInstance(context).Profile_Name__c;
        List<Profile> ProfileIds = [SELECT Id FROM Profile WHERE Name=:profilename limit 1];
        if(ProfileIds.size() > 0){
            return ProfileIds[0].id;
        }
        return null;
    }
    
    //Test 4: update user details using auth data
    global void updateUser(Id userId, Id portalId, Auth.UserData data){
        
        User idmsUser = new User(Id=userId);
        try{
            idmsUser.IDMS_Registration_Source__c = Label.CLQ316IDMS022;
            update idmsUser;
        }catch(Exception e){
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
        }
    }
}