/*
    Author          : Bhuvana S
    Description     : Test class for AP_ProductLineBeforeInsertHandler
*/
@isTest
private class AP_ProductLineBeforeInsertHandler_TEST 
{
    static testMethod void testProdLine()
    {
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;
        
        OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
        pl1.Quantity__c = 2;
        insert pl1;
    }
}