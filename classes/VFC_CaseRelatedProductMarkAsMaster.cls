public with sharing class VFC_CaseRelatedProductMarkAsMaster {
    public CSE_RelatedProduct__c currentCaseRelatedProduct;
    public Id CaseId;
    
    
    public VFC_CaseRelatedProductMarkAsMaster(ApexPages.StandardController controller) {
        currentCaseRelatedProduct     =  (CSE_RelatedProduct__c)controller.getRecord();
        currentCaseRelatedProduct      = [Select Id, MasterProduct__c, Case__c, CommercialReference__c, TECH_GMRCode__c from CSE_RelatedProduct__c where Id=:currentCaseRelatedProduct.Id limit 1];
        CaseId = currentCaseRelatedProduct.Case__c;
        
    }
    public PageReference returnToCase() {
        PageReference returnPage = new PageReference('/' +CaseId);
        return returnPage;
    }
    public PageReference performAction() {
        PageReference ResultPage=null;
        Case objCase;
        
        if(CaseId!=null){
            List<CSE_RelatedProduct__c> lstExistingProducts = new List<CSE_RelatedProduct__c>([Select Id from CSE_RelatedProduct__c where MasterProduct__c =:true AND  Case__c =: CaseId AND ID !=:currentCaseRelatedProduct.Id]);
             
            
            if(lstExistingProducts.size()>0){            
                for(CSE_RelatedProduct__c objExistingProduct: lstExistingProducts){
                    objExistingProduct.MasterProduct__c=false;   
                }
                currentCaseRelatedProduct.MasterProduct__c=true;
                lstExistingProducts.add(currentCaseRelatedProduct); 
            }
            if(lstExistingProducts.size()==0){
                currentCaseRelatedProduct.MasterProduct__c=true;
                lstExistingProducts.add(currentCaseRelatedProduct); 
            }
            if(lstExistingProducts.size()>0){
                Database.SaveResult[] ExistingMasterProductSaveResult = Database.update(lstExistingProducts,false);
                for(Database.SaveResult objProductSaveResult: ExistingMasterProductSaveResult){
                    if(!objProductSaveResult.isSuccess()){
                        Database.Error DMLerror = objProductSaveResult.getErrors()[0];
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLerror.getMessage()));
                        return null;     
                    }
                }
            }
            if(currentCaseRelatedProduct.TECH_GMRCode__c!=null){
                if(currentCaseRelatedProduct.CommercialReference__c!=null){
                    List<Product2> lstProductRecord = new List<Product2>([Select id,ProductCode,Name,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c, GDPGMR__c,ProductGDPGMR__c,ProductFamilyId__c from Product2 where Name =:currentCaseRelatedProduct.CommercialReference__c AND ProductGDP__c != null]);
                    if(lstProductRecord.size()>0){
                        OPP_Product__c objProductHierarchy = lstProductRecord[0].ProductGDP__r;
                        if(objProductHierarchy!=null){
                            objCase = new Case(Id=CaseId);
                            objCase.ProductBU__c              = objProductHierarchy.BusinessUnit__c;
                            objCase.ProductLine__c            = objProductHierarchy.ProductLine__c;
                            objCase.ProductFamily__c          = objProductHierarchy.ProductFamily__c;
                            objCase.Family__c                 = objProductHierarchy.Family__c;
                            objCase.SubFamily__c              = objProductHierarchy.SubFamily__c;
                            objCase.ProductSuccession__c      = objProductHierarchy.Series__c;
                            objCase.ProductGroup__c           = objProductHierarchy.GDP__c;
                            objCase.ProductDescription__c     = lstProductRecord[0].ProductCode;
                            objCase.CommercialReference__c    = lstProductRecord[0].Name;
                            objCase.TECH_GMRCode__c           = lstProductRecord[0].ProductGDPGMR__c;
                            objCase.Family_lk__c              = lstProductRecord[0].ProductFamilyId__c;
                            objCase.CommercialReference_lk__c = lstProductRecord[0].id;
                            objCase.CheckedbyGMR__c           = TRUE;
                        }
                    }
                }
                else{
                    List<OPP_Product__c> lstProductHierarchy = new List<OPP_Product__c>([Select Id, BusinessUnit__c,ProductLine__c,ProductFamily__c,Family__c,TECH_PM0CodeInGMR__c from OPP_Product__c where CCCRelevant__c=true AND TECH_PM0CodeInGMR__c=:currentCaseRelatedProduct.TECH_GMRCode__c ]);
                    if(lstProductHierarchy.size()>0){
                        objCase = new Case(Id=CaseId);
                        objCase.Family_lk__c              = lstProductHierarchy[0].Id;
                        objCase.ProductBU__c              = lstProductHierarchy[0].BusinessUnit__c;
                        objCase.ProductLine__c            = lstProductHierarchy[0].ProductLine__c;
                        objCase.ProductFamily__c          = lstProductHierarchy[0].ProductFamily__c;
                        objCase.Family__c                 = lstProductHierarchy[0].Family__c;
                        objCase.TECH_GMRCode__c           = lstProductHierarchy[0].TECH_PM0CodeInGMR__c;
                        objCase.SubFamily__c              = null;
                        objCase.ProductSuccession__c      = null;
                        objCase.ProductGroup__c           = null;
                        
                        
                        objCase.CommercialReference_lk__c = null;
                        objCase.CommercialReference__c    = null;
                        objCase.CheckedbyGMR__c           = TRUE;
                    }
                }
                if(objCase!=null){
                    AP_Case_CaseHandler.triggerPM0HierarchyOnChildObject=false;
                    Database.SaveResult caseUpdateResult = Database.update(objCase,false);
                    if(!caseUpdateResult.isSuccess()){
                        Database.Error DMLerror = caseUpdateResult.getErrors()[0];
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLerror.getMessage()));
                        return null;     
                    }
                }
            }
            ResultPage = new PageReference('/' + CaseId);
        }
        return ResultPage;
    }
}