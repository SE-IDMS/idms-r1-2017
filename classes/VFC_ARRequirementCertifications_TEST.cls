@isTest
public class VFC_ARRequirementCertifications_TEST {
    static testMethod void myTest(){
        List<Profile> profiles = [select id from profile where name='System Administrator'];
            if(profiles.size()>0){
                User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                //usr.BypassWF__c = false;
                usr.BypassVR__c = true;
                System.runas(usr){
                   
                    CertificationCatalog__c certcat = new CertificationCatalog__c();
                    certcat.Active__c = true;
                    certcat.CertificationName__c = 'Test Cert';
                    insert certcat;
                    
                    RequirementCatalog__c reqcat = Utils_TestMethods.createRequirementCatalog();
                    reqcat.Certification__c = certcat.id;
                    reqcat.Name = 'Test Requirement';
                    insert reqcat;
                    
                    RequirementCertification__c reqcert = new RequirementCertification__c();
                    reqcert.CertificationCatalog__c = certcat.id;
                    reqcert.RequirementCatalog__c = reqcat.id;
                    insert reqcert;
                    
                    Apexpages.Standardcontroller cont = new Apexpages.Standardcontroller(reqcat);
                    Pagereference pg4 = Page.VFP_AddRemoveRequirementCertifications; 
                    Test.setCurrentPage(pg4);
                   
                    VFC_AddRemoveRequirementCertifications vfcon = new VFC_AddRemoveRequirementCertifications(cont);
                    vfcon.updaterequirementcertification();
                }
            }
        }
   }