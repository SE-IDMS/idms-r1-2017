/********************************************************************
* Company: Fielo
* Created Date: 09/07/2016
* Description: 
********************************************************************/
@isTest
public class FieloPRM_AP_BadgeMemberTriggersTest{
    
    static testMethod void unitTest1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){ 
            FieloEE.MockUpFactory.setCustomProperties(false);
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;
            FieloEE__Member__c member2 = new FieloEE__Member__c();
            member2.FieloEE__LastName__c = 'USUARIO UNIT TEST2 ';
            member2.FieloEE__FirstName__c = 'USUARIO UNIT TEST2 ';
            member2.FieloEE__Street__c = 'test';
            member2.F_Account__c = acc.id;
            insert member2;
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'test';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_TypeSelection__c = 'Program Level';
            badge.RecordTypeId = rtProgLevel.Id;
            
            badge.F_PRM_Segment__c = segment.id;
            badge.F_PRM_BadgeAPIName__c = '1';

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
            badge2.name = 'test';
            badge2.F_PRM_Type__c = 'Program Level';
            badge2.F_PRM_TypeSelection__c = 'Program Level';
            badge2.F_PRM_Segment__c = segment.id;
            badge2.F_PRM_BadgeAPIName__c = '2';
            badge2.RecordTypeId = rtProgLevel.Id;
            
            List<FieloEE__Badge__c> badges = new List<FieloEE__Badge__c>();
            badges.add(badge);
            badges.add(badge2);
            insert badges;

            FieloPRM_ValidBadgeTypes__c validBadgeAux = new FieloPRM_ValidBadgeTypes__c(FieloPRM_ApiExtId__c = '1', FieloPRM_Type__c = 'Program Level', name = 'test' );
            insert validBadgeAux;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c( FieloEE__Member2__c = member.Id, FieloEE__Segment2__c = seg.Id);
            Insert memSeg;
            
            List<FieloEE__BadgeMember__c> bdmembers = new List<FieloEE__BadgeMember__c>();
            FieloEE__BadgeMember__c badMAux1 = new FieloEE__BadgeMember__c(FieloEE__Badge2__c=badge.Id, FieloEE__Member2__c= member2.Id , F_PRM_Status__c  = 'Active', FieloEE__Quantity__c  =1);
            bdmembers.add(badMAux1);
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c=badge.Id, FieloEE__Member2__c= member.Id, F_PRM_Status__c  = 'Active', FieloEE__Quantity__c  =1 ) );
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c=badge2.Id, FieloEE__Member2__c= member.Id, F_PRM_Status__c  = 'Active', FieloEE__Quantity__c  =1 ) );
                                
            test.startTest();
            
            insert bdmembers;                
            badMAux1.FieloEE__Quantity__c  = 2;
            update badMAux1;
            badMAux1.F_PRM_Status__c  = 'Inactive';
            
            update badMAux1;

            test.stopTest();
        }
    }
    static testMethod void unitTest2(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){ 
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            FieloEE__Member__c member2 = FieloEE.MockUpFactory.createMember('Test2 member2', '56789', 'DNI');

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'test';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_TypeSelection__c = 'Program Level';
            badge.RecordTypeId = rtProgLevel.Id;
            
            badge.F_PRM_Segment__c = segment.id;
            badge.F_PRM_BadgeAPIName__c = '1';

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
            badge2.name = 'test';
            badge2.F_PRM_Type__c = 'Program Level';
            badge2.F_PRM_TypeSelection__c = 'Program Level';
            badge2.F_PRM_Segment__c = segment.id;
            badge2.F_PRM_BadgeAPIName__c = '2';
            badge2.RecordTypeId = rtProgLevel.Id;

            List<FieloEE__Badge__c> badges = new List<FieloEE__Badge__c>();
            badges.add(badge);
            badges.add(badge2);
            insert badges;

            FieloPRM_ValidBadgeTypes__c validBadgeAux = new FieloPRM_ValidBadgeTypes__c(FieloPRM_ApiExtId__c = '1', FieloPRM_Type__c = 'Program Level', name = 'test' );
            insert validBadgeAux;

            List<FieloEE__BadgeMember__c> bdmembers = new List<FieloEE__BadgeMember__c>();
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c=badge.Id, FieloEE__Member2__c= member2.Id , F_PRM_Status__c  = 'Active'));
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c=badge.Id, FieloEE__Member2__c= member.Id, F_PRM_Status__c  = 'Active' ) );
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c=badge2.Id, FieloEE__Member2__c= member.Id, F_PRM_Status__c  = 'Active' ) );
                                
            test.startTest();
            insert bdmembers;                
            bdmembers[0].F_PRM_Status__c  = 'Inactive';
            delete bdmembers;
            test.stopTest();
        }
    }
    static testMethod void unitTest3(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            list<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'Badge';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_Segment__c =  segment.id;
            badge.F_PRM_BadgeAPIName__c = 'apinametest';
            badge.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge) ;

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
                    
            badge2.name = 'Badge';
            badge2.F_PRM_Type__c = 'BFOProperties';
            badge2.F_PRM_Segment__c = segment.id;
            badge2.F_PRM_BadgeAPIName__c = 'apinametest2';
            badge2.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge2) ;
            insert listbadge;
            for(FieloEE__Badge__c b: listbadge){
                b.F_PRM_Type__c = 'Program Level';
            }
                     
            Update listbadge;

            FieloCH__Challenge__c challenge = FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
            
            FieloCH__MissionChallenge__c missionChallenge1 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );     
            FieloCH__MissionChallenge__c missionChallenge2 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );     
            test.startTest(); 
            FieloCH__ChallengeReward__c challengeReward = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );      
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;
   
            update challengeReward;

            challenge = FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);

            FieloEE__BadgeMember__c badgeMember = new FieloEE__BadgeMember__c();
            badgeMember.FieloEE__Badge2__c = badge.id;
            badgeMember.FieloEE__Member2__c = member.id;
            badgeMember.FieloEE__Quantity__c =  1;

            insert badgeMember;
            badgeMember.FieloEE__Quantity__c = 2;
            update badgeMember;

            test.stopTest();
        }
    
    }

    static testMethod void unitTest4(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            list<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'Badge';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_Segment__c =  segment.id;
            badge.F_PRM_BadgeAPIName__c = 'apinametest';
            badge.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge) ;

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
                    
            badge2.name = 'Badge';
            badge2.F_PRM_Type__c = 'BFOProperties';
            badge2.F_PRM_Segment__c =  segment.id;
            badge2.F_PRM_BadgeAPIName__c = 'apinametest2';
            badge2.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge2) ;
            insert listbadge;
            for(FieloEE__Badge__c b: listbadge){
                b.F_PRM_Type__c = 'Program Level';
            }
            
            Update listbadge;

            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
            
            FieloCH__MissionChallenge__c missionChallenge1 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );     
            FieloCH__MissionChallenge__c missionChallenge2 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );     
            test.startTest(); 
            FieloCH__ChallengeReward__c challengeReward = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );      
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;

            challengeReward.F_PRM_ExpirationType__c = 'Days';
            challengeReward.F_PRM_ExpirationNumber__c = 1;
                         
            update challengeReward;

            challenge = FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);

            FieloEE__BadgeMember__c badgeMember = new FieloEE__BadgeMember__c();
            badgeMember.FieloEE__Badge2__c = badge.id;
            badgeMember.FieloEE__Member2__c = member.id;
            badgeMember.FieloEE__Quantity__c = 1;

            insert badgeMember;
            badgeMember.FieloEE__Quantity__c = 2;
            update badgeMember;
            test.stopTest();
        }
    
    }

    static testMethod void unitTest5() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){ 
        
            FieloEE.MockUpFactory.setCustomProperties(false);
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;
            
            FieloEE__Member__c member2 = new FieloEE__Member__c();
            member2.FieloEE__LastName__c = 'USUARIO UNIT TEST2 ';
            member2.FieloEE__FirstName__c = 'USUARIO UNIT TEST2 ';
            member2.FieloEE__Street__c = 'test';
            member2.F_Account__c = acc.id;
            insert member2;
            
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');
            
            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'test',
                F_PRM_Type__c = 'Program Level',
                F_PRM_TypeSelection__c = 'Program Level',         
                F_PRM_Segment__c = segment.id,
                F_PRM_BadgeAPIName__c = '1',
                RecordTypeId = rtProgLevel.Id,
                F_PRM_Challenge__c = challenge.Id 
            );

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c(
                Name = 'test',
                F_PRM_Type__c = 'Program Level',
                F_PRM_TypeSelection__c = 'Program Level',
                F_PRM_Segment__c = segment.id,
                F_PRM_BadgeAPIName__c = '2',
                RecordTypeId = rtProgLevel.Id,
                F_PRM_Challenge__c = challenge.Id               
            );
            
            insert new List<FieloEE__Badge__c>{badge,badge2};

            FieloPRM_ValidBadgeTypes__c validBadgeAux = new FieloPRM_ValidBadgeTypes__c(FieloPRM_ApiExtId__c = '1', FieloPRM_Type__c = 'Program Level', name = 'test' );
            insert validBadgeAux;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c( FieloEE__Member2__c = member.Id, FieloEE__Segment2__c = seg.Id);
            Insert memSeg;
            
            test.startTest();

            FieloCH__ChallengeReward__c challengeReward = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge);      
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;
            challengeReward.FieloCH__Position__c = 1;
            update challengeReward;
   
            List<FieloEE__BadgeMember__c> bdmembers = new List<FieloEE__BadgeMember__c>();
            FieloEE__BadgeMember__c badMAux1 = new FieloEE__BadgeMember__c(FieloEE__Badge2__c = badge.Id, FieloEE__Member2__c = member.Id, F_PRM_Status__c = 'Active', FieloEE__Quantity__c = 1);
            insert badMAux1;
            
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c = badge.Id, FieloEE__Member2__c = member.Id, F_PRM_Status__c = 'Active', FieloEE__Quantity__c = 1));
            bdmembers.add(new FieloEE__BadgeMember__c(FieloEE__Badge2__c = badge2.Id, FieloEE__Member2__c = member.Id, F_PRM_Status__c = 'Active', FieloEE__Quantity__c = 1));
                               
            insert bdmembers; 
                           
            badMAux1.FieloEE__Quantity__c  = 2;
            update badMAux1;
            
            badMAux1.F_PRM_Status__c  = 'Inactive';
            
            update badMAux1;

            test.stopTest();
        }
    }

    static testMethod void unitTest6(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            list<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'Badge';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_Segment__c =  segment.id;
            badge.F_PRM_BadgeAPIName__c = 'apinametest';
            badge.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge) ;

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
                    
            badge2.name = 'Badge';
            badge2.F_PRM_Type__c = 'BFOProperties';
            badge2.F_PRM_Segment__c =  segment.id;
            badge2.F_PRM_BadgeAPIName__c = 'apinametest2';
            badge2.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge2) ;
            insert listbadge;
            for(FieloEE__Badge__c b: listbadge){
                b.F_PRM_Type__c = 'Program Level';
            }
            
            Update listbadge;

            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
            
            FieloCH__MissionChallenge__c missionChallenge1 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );     
            FieloCH__MissionChallenge__c missionChallenge2 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );     
            test.startTest(); 
            FieloCH__ChallengeReward__c challengeReward = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );      
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;

            challengeReward.F_PRM_ExpirationType__c = 'Days';
            challengeReward.F_PRM_ExpirationNumber__c = 1;
                         
            update challengeReward;

            challenge = FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);

            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c = 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'FieloPRM_CustomLogicImplementTest';
            
            insert feature;
            
            FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
            memberFeature.F_PRM_Feature__c = feature.id;
            memberFeature.F_PRM_Member__c = member.id;
            memberFeature.F_PRM_IsActive__c = true;
            memberFeature.PRMUIMSId__c = '123TEST';
            //insert memberFeature;     

            FieloEE__BadgeMember__c badgeMember = new FieloEE__BadgeMember__c();
            badgeMember.FieloEE__Badge2__c = badge.id;
            badgeMember.FieloEE__Member2__c = member.id;
            badgeMember.FieloEE__Quantity__c = 1;

            insert badgeMember;

            test.stopTest();
        }
    
    }

    static testMethod void unitTest7(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            insert member;

            CS_PRM_ApexJobSettings__c conf2 = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberFeatProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf2;

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            RecordType rtProgLevel = [SELECT Id FROM RecordType WHERE DeveloperName = 'F_PRM_ProgramLevel' AND sObjectType = 'FieloEE__Badge__c'];

            list<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'Badge';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_Segment__c =  segment.id;
            badge.F_PRM_BadgeAPIName__c = 'apinametest';
            badge.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge) ;

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
                    
            badge2.name = 'Badge';
            badge2.F_PRM_Type__c = 'BFOProperties';
            badge2.F_PRM_Segment__c =  segment.id;
            badge2.F_PRM_BadgeAPIName__c = 'apinametest2';
            badge2.RecordTypeId = rtProgLevel.Id;
            listbadge.add(badge2) ;
            insert listbadge;
            for(FieloEE__Badge__c b: listbadge){
                b.F_PRM_Type__c = 'Program Level';
            }
            
            system.debug('###badges: ' + [SELECT Id, RecordType.DeveloperName FROM FieloEE__Badge__c]);
            
            Update listbadge;

            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
            
            FieloCH__MissionChallenge__c missionChallenge1 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );     
            FieloCH__MissionChallenge__c missionChallenge2 = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );     
            test.startTest(); 
            FieloCH__ChallengeReward__c challengeReward = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );      
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;

            challengeReward.F_PRM_ExpirationType__c = 'Days';
            challengeReward.F_PRM_ExpirationNumber__c = 1;
                         
            update challengeReward;

            challenge = FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);

            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c = 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'FieloPRM_CustomLogicImplementTest';
            
            insert feature;

            list<FieloPRM_BadgeFeature__c> badgeFeatureList = new List<FieloPRM_BadgeFeature__c>();
            badgeFeatureList.add(new FieloPRM_BadgeFeature__c(
                F_PRM_Badge__c = badge.Id,
                F_PRM_Feature__c = feature.Id
            ));
            insert badgeFeatureList;

            FieloEE__BadgeMember__c badgeMember = new FieloEE__BadgeMember__c();
            badgeMember.FieloEE__Badge2__c = badge.id;
            badgeMember.FieloEE__Member2__c = member.id;
            badgeMember.FieloEE__Quantity__c = 1;
            badgeMember.F_PRM_Status__c = 'Pending';

            insert badgeMember;

            badgeMember.F_PRM_Status__c = 'To Process';
            update badgeMember;

            delete badgeMember;

            List<FieloEE__BadgeMember__c> bms = new List<FieloEE__BadgeMember__c>();

            FieloEE__BadgeMember__c badgeMember2 = new FieloEE__BadgeMember__c();
            badgeMember2.FieloEE__Badge2__c = badge.id;
            badgeMember2.FieloEE__Member2__c = member.id;
            badgeMember2.FieloEE__Quantity__c = 1;
            badgeMember2.F_PRM_Status__c = 'Pending';
            bms.add(badgeMember2);

            FieloEE__BadgeMember__c badgeMember3 = new FieloEE__BadgeMember__c();
            badgeMember3.FieloEE__Badge2__c = badge.id;
            badgeMember3.FieloEE__Member2__c = member.id;
            badgeMember3.FieloEE__Quantity__c = 1;
            badgeMember3.F_PRM_Status__c = 'Pending';
            bms.add(badgeMember3);
            insert bms;

            bms.get(0).F_PRM_Status__c = 'To Process';
            bms.get(1).F_PRM_Status__c = 'To Process';
            update bms;

            test.stopTest();
        }
    
    }

}