@isTest(seealldata=true)
Public class AP_OSACMeeting_Test
{
    public static testMethod void OSACMeeting() {
        //Creating Data 
        //Creating users
        string prefix='aa';
        user  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
        insert sampleUser;

        List<User> userList = new List<User>();
        User CSQUser1 = Utils_TestMethods.createStandardUser('CSQUser1'); 
        userList.add(CSQUser1);
        User CSQUser2 = Utils_TestMethods.createStandardUser('CSQUser2'); 
        userList.add(CSQUser2);
        Database.insert(userList);
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
         
        //Creating BusinessRiskEscalationEntity__c
        List<BusinessRiskEscalationEntity__c> breList = new List<BusinessRiskEscalationEntity__c>();     
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        breEntity.Entity__c='i2ptesetentityweq';
        insert(breEntity);
        BusinessRiskEscalationEntity__c breEntitySubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        breEntitySubEntity.Entity__c='i2ptesetentityweq';
        breEntitySubEntity.SubEntity__c='i2psubeintyewqw';
        insert(breEntitySubEntity);
        breList.add(breEntity);
        breList.add(breEntitySubEntity);   
        
        //Creating Stakeholder Data
        List<EntityStakeholder__c> eshList =  new List<EntityStakeholder__c>();
        List<CSGSACmeeting__c> csdata=createCustomSetting();
          
        Integer Count=0;
          
        eshList.clear();
        eshList.add(new EntityStakeholder__c(BusinessRiskEscalationEntity__c=breList[0].Id,User__c=userList[0].Id,Role__c='CS&Q Manager'));
        eshList.add(new EntityStakeholder__c(BusinessRiskEscalationEntity__c=breList[0].Id,User__c=sampleUser.id,Role__c=label.CLSEP12I2P15));
        System.debug('---eshListsize--->'+eshList.size());
        if(eshList  != null && eshList.size()>0){
            Database.insert(eshList);
        }
            
        for(EntityStakeholder__c  esh:eshList){
            System.debug('HKDebug-->>'+esh.Role__c);
            System.debug('HKDebug-->>'+esh);
        }
        
        //problem creation
        Problem__c prob=Utils_TestMethods.createProblem(breList[0].Id,12);
        prob.ProblemLeader__c=sampleUser.id;
        prob.ProductQualityProblem__c = false;
        Database.insert(prob); 
        
        // Creating Accounts
        List<Account> accList = new List<Account>();
        
        Account accGSA = Utils_TestMethods.createAccount();
        //accGSA.Type = 'GSA';
        accGSA.Account_VIPFlag__c = 'Gold';
        accGSA.ZipCode__c='201301';
        accList.add(accGSA);
        System.debug('------------>MSP'+accList);
        Database.insert(accList);
        
        
        BusinessRiskEscalations__c brescalation = Utils_TestMethods.createBusinessRiskEscalationwithProblem(accList[0].id,country.Id,sampleUser.Id,prob.Id,breList[1].Id);
        Database.insert(brescalation); 
        system.debug('brescalation'+brescalation);

        brescalation.RelatedProblem__c = Prob.id;
        brescalation.AffectedCustomer__c = accList[0].id;
        Database.update(brescalation);
        
        // Creating CustomerSafetyIssue
        List<OSACMeeting__c> OsiList = new List<OSACMeeting__c>();
        OSACMeeting__c  OSRec=new OSACMeeting__c();
        OSRec.Start__c = Date.today();
        OSRec.Problem__c =prob.id ;        
        OSRec.Decision__c = 'Pending';
        OSRec.ObjectiveOfMeeting__c='test';
        OSRec.Participants__c='test';
        OSRec.End__c = system.today();
        Insert OSRec;
        OSRec.Decision__c='Go';
        OSRec.MeetingFacilitator__c='test';
        OSRec.DecisionSummary__c='test2';
        OSRec.ProblemSummary__c='test';
        OSRec.ProblemLeader__c=sampleUser.id;
        OSRec.CSQManager__c=sampleUser.id;
        OSRec.End__c = system.today();
        update OSRec;  
        delete OSrec;
    }
    
    
    public static testMethod void OSACMeeting1() {
        
        test.startTest();
        //Creating Data 
        //Creating users
        string prefix='qq';
        user  sampleUser1=Utils_TestMethods.createStandardUser('sam12'+prefix);
        insert sampleUser1;
        string prefixx='ts';
        user  sampleUser2=Utils_TestMethods.createStandardUser('sam90'+prefixx);
        insert sampleUser2;

        List<User> userList = new List<User>();
        User CSQUser1 = Utils_TestMethods.createStandardUser('CSQUsr1'); 
        userList.add(CSQUser1);
        User CSQUser2 = Utils_TestMethods.createStandardUser('CSQUsr2'); 
        userList.add(CSQUser2);
        Database.insert(userList);
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        
        //Creating BusinessRiskEscalationEntity__c
        List<BusinessRiskEscalationEntity__c> breList = new List<BusinessRiskEscalationEntity__c>();     
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        breEntity.Entity__c='i2ptesetentity';
        insert(breEntity);
        BusinessRiskEscalationEntity__c breEntitySubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        breEntitySubEntity.Entity__c='i2ptesetentity';
        breEntitySubEntity.SubEntity__c='i2psubeinty';
        insert(breEntitySubEntity);
        breList.add(breEntity);
        breList.add(breEntitySubEntity);   
        
        
        //Creating Stakeholder Data
        List<EntityStakeholder__c> eshList =  new List<EntityStakeholder__c>();
        List<CSGSACmeeting__c> csdata=createCustomSetting();
          
        Integer Count=0;
        eshList.clear();
        eshList.add(new EntityStakeholder__c(BusinessRiskEscalationEntity__c=breList[1].Id,User__c=sampleUser2.id,Role__c='CS&Q Manager'));
        eshList.add(new EntityStakeholder__c(BusinessRiskEscalationEntity__c=breList[1].Id,User__c=sampleUser1.id,Role__c='Organization Manager'));
        if(eshList  != null && eshList.size()>0){
            try{
                Database.insert(eshList); 
            }
            catch(Exception ex){
                System.debug('HKDebug-->'+ex);
            }
        }
        
        
        //problem creation
        Problem__c prob=Utils_TestMethods.createProblem(breList[0].Id,12);
        prob.ProductQualityProblem__c = false;
        prob.ProblemLeader__c=sampleUser1.id;
        Database.insert(prob);       
        
        // Creating Accounts
        List<Account> accList = new List<Account>();
        
        Account accGSA = Utils_TestMethods.createAccount();
        //accGSA.Type = 'GSA';
        accGSA.Account_VIPFlag__c = 'Gold';
        accGSA.ZipCode__c='201301';
        accList.add(accGSA);
        System.debug('------------>MSP'+accList);
        Database.insert(accList);
        
        /*// creating BRE
        BusinessRiskEscalations__c brescalation = Utils_TestMethods.createBusinessRiskEscalationwithProblem(accList[0].id,country.Id,sampleUser1.Id,prob.Id,breList[1].Id);
        Database.insert(brescalation); 
        system.debug('brescalation'+brescalation);

        brescalation.RelatedProblem__c = Prob.id;
        brescalation.AffectedCustomer__c = accList[0].id;
        Database.update(brescalation);
        BusinessRiskEscalations__c brescalation1 = Utils_TestMethods.createBusinessRiskEscalationwithProblem(accList[0].id,country.Id,sampleUser2.Id,prob.Id,breList[1].Id);
        brescalation1.title__c='tst';
        Database.insert(brescalation1); 
        system.debug('brescalation'+brescalation1);

        brescalation1.RelatedProblem__c = Prob.id;
        brescalation1.AffectedCustomer__c = accList[0].id;
        Database.update(brescalation1);
        System.debug('---brescalation1---->'+brescalation1);
        System.debug('---brescalation1---->'+brescalation1);
        System.debug('---prob---->'+prob); */
        
        //OPP_Product__c oppproduct = [SELECT BusinessUnit__c, ProductLine__c,TECH_PM0CodeInGMR__c,Family__c, Name FROM OPP_Product__c LIMIT 1];
        OPP_Product__c oppproduct = Utils_TestMethods.createProduct('businessUnit', 'productLine', 'productFamily', 'family');
        insert oppproduct;
        
        CommercialReference__c CR = new CommercialReference__c();
        CR.Business__c = oppproduct.BusinessUnit__c;
        CR.CommercialReference__c = 'CommercialReference';
        CR.Family__c = oppproduct.Family__c;
        CR.GMRCode__c = oppproduct.TECH_PM0CodeInGMR__c;
        CR.ProductLine__c = oppproduct.ProductLine__c;
        CR.Problem__c = prob.Id;
        insert CR;
        
        ProductLineQualityContactMapping__c PLIS = new ProductLineQualityContactMapping__c();
        PLIS.Product__c = oppproduct.Id;
        PLIS.AccountableOrganization__c = breEntitySubEntity.Id;
        PLIS.QualityContact__c = userList[0].Id;
        insert PLIS;
             
        List<OSACMeeting__c> OsiList = new List<OSACMeeting__c>();
        OSACMeeting__c  OSRec=new OSACMeeting__c();
        OSRec.Start__c = Date.today();
        OSRec.Problem__c =prob.id ;  
        OSRec.ObjectiveOfMeeting__c='test';
        OSRec.Decision__c = 'Pending';
        OSRec.Participants__c='test';
        OSRec.End__c = system.today();
        Insert OSRec;
        
        OSRec.Decision__c='Go';
        OSRec.MeetingFacilitator__c='test';
        OSRec.DecisionSummary__c='test2';
        OSRec.ProblemSummary__c='test';
        OSRec.ProblemLeader__c=sampleUser1.id;
        OSRec.CSQManager__c=sampleUser1.id;
        OSRec.End__c = system.today();
        
        update OSRec;
        test.stopTest();
    }
        
    public static List<CSGSACmeeting__c> createCustomSetting() {
        List<CSGSACmeeting__c> ObjList = new List<CSGSACmeeting__c>();
        return ObjList;
    }    
}