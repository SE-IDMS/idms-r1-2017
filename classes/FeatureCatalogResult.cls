global class FeatureCatalogResult{
    // WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfFeatureCatalogs; 

    WebService FeatureCatalog[] Features { get; set; }
    WebService Datetime BatchMaxLastModifiedDate {get;set;}
}