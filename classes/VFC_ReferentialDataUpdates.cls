/*
    Description: custom controller for VFP_ReferentialDataUpdates to execute Batch_ReferentialDataUpdates by passing parameters
*/
public class VFC_ReferentialDataUpdates{
     public String obj{get;set;}
     public String wherecond{get;set;}
     public String updateField{get;set;}
     public String updateValue{get;set;}
     public String batchLimit{get;set;}{batchLimit=null;}
     public String query{get;set;}
     public Boolean querygenerated{get;set;}{querygenerated=FALSE;}
     public String updateCondition{get;set;}
     public String batchexecute{get;set;}
     public String queryLimit{get;set;}{queryLimit=null;}
     public boolean canRunBatch{get;set;}{canRunBatch=false;}
     public Id batchId{get;set;}{batchId=null;}
     public AsyncApexJob job{get;set;}{job=null;}
     public Boolean statusCheck{get;set;}{statusCheck=FALSE;}

    public VFC_ReferentialDataUpdates(){
        for(User user:[select id from User where Id=:UserInfo.getUserId() and BypassVR__c=True and BypassWF__c=True])
            canRunBatch=true; 
        if(Test.isRunningTest())
             canRunBatch=true;   
    }
    
    public pagereference reset(){
       PageReference ref = Page.VFP_ReferentialDataUpdates;
        ref.setRedirect(true);
        return ref;
    }
     public void queryGenerate(){
         querygenerated=TRUE;
         batchId=null;
         statusCheck=FALSE;
         query='SELECT ID,'+updateField+' FROM '+obj+' Where '+wherecond;
         if(queryLimit!=null && queryLimit!='')
             query+=' Limit '+queryLimit;
         updateCondition=updateField+'='+updateValue;
         if(batchLimit!=null && batchLimit!='')
           batchexecute='database.executebatch(new Batch_ReferentialDataUpdates('+obj+','+query+','+updateField+','+updateValue+'),'+batchLimit+')';
         else
           batchexecute='database.executebatch(new Batch_ReferentialDataUpdates('+obj+','+query+','+updateField+','+updateValue+')';
    }
    public void executeBatch(){
        if(canRunBatch) {
             if(batchLimit!=null && batchLimit!=''){
               Integer batchlimitnum=integer.valueof(batchLimit);
               batchId=database.executebatch(new Batch_ReferentialDataUpdates(obj,query,updateField,updateValue),batchlimitnum);
               checkStatus();
             }
             else{
               batchId=database.executebatch(new Batch_ReferentialDataUpdates(obj,query,updateField,updateValue));
               checkStatus();
               }
        }
    }
    public void checkStatus()
    {
       if(batchId !=null) {
           job=[SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchId Limit 1];
           statusCheck=TRUE;
      }
    }
}