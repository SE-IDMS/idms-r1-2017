global class ContactProgram {
    WebService string SCONT_PS { get; set; }
    
    WebService string SCONT_ID { get; set; }
    
    WebService string DML_ACTION { get; set; }

    WebService string PRM_PROG_PS { get; set; }
    
    WebService string PRM_PROG_ID { get; set; }

    WebService string LEVEL_ID { get; set; }

    WebService string IS_ACTIVE { get; set; }

    WebService Datetime SDH_VERSION { get; set; }

    public ContactProgram() {
        SCONT_PS = 'BFO';
        PRM_PROG_PS = 'BFO';
        DML_ACTION = 'UPSERT';
    }
}