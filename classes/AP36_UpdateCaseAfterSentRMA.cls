public class AP36_UpdateCaseAfterSentRMA
{
    public static void insertCaseEmail(List<Task> sentRMAEmails)
    {
        if(sentRMAEmails != null && sentRMAEmails.size() >0)
        {
            Set<ID> RMATasksID = new Set<ID>();
            Map<ID,RMA__c> relatedRMAMap = new Map<ID,RMA__c>(); // Map of related RMAs with their IDs
            
            for(Task anEmailTask:sentRMAEmails)
            {
                RMATasksID.add(anEmailTask.WhatID);
            }
            
            Set<ID> RMAIDs = new Set<ID>();

            // Build the map of related RMAs
            for(RMA__c aRMA:[SELECT ID, Case__c FROM RMA__c WHERE ID IN :RMATasksID])
            {
                RMAIDs.add(aRMA.ID);
                relatedRMAMap.put(aRMA.ID,aRMA); 
            }

            Set<ID> CaseIDs = new Set<ID>();
            Map<ID,Case> relatedCaseMap = new Map<ID,Case>();  // Map of related case            
            
            if(relatedRMAMap != null)
            {
                for(RMA__c aRMA:relatedRMAMap.values())
                {
                    CaseIDs.add(aRMA.Case__c);
                }
            }
            
            // Build the map of related cases with their IDs
            For(Case aCase:[SELECT ID FROM Case WHERE ID IN  :CaseIDs])
            {
                relatedCaseMap.put(aCase.ID,aCase);
            }
            
            List<EmailMessage> IncomingEmails = new List<EmailMessage>();

            For(Task aTask:sentRMAEmails)
            {
                if(relatedRMAMap != null && relatedRMAMap.get(aTask.WhatID) != null && relatedCaseMap != null && relatedCaseMap.get(relatedRMAMap.get(aTask.WhatID).Case__c) != null)
                {
                    // Save Email Messages
                    IncomingEmails.add(new EmailMessage( Incoming = false, Subject = aTask.Subject, TextBody = aTask.Description, ParentId = relatedCaseMap.get(relatedRMAMap.get(aTask.WhatID).Case__c).ID , Status='3', MessageDate=DateTime.now()));        
                }
            }
            try
            {
                List<DataBase.SaveResult> EmailSR = DataBase.insert(IncomingEmails );
            }
            catch(Exception e)
            {
                System.debug('---- DML Exception: ' + e.getmessage());
            }
        }
    }
}