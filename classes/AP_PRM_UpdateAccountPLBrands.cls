// This is a service class that is called from batch job to roll-up the brands as a 
// comma separated at the account linked to a program level that the account is enrolled into
public class AP_PRM_UpdateAccountPLBrands{

    // Get the list of BadgeMembers
    // 
    public static void UpdateAccount(List<Id> bmIds){
        System.debug('BMIDS :'+bmIds.size());
       // Create a map with key value Pair of Account and Badge with name --- mapAccBadge
       // Create another map with key value Pair of Badge and ProgramLevel with name ---- mapLevel 
       Map<Id,Id> mapLevel = new Map<Id,Id>();
       Map<Id,Id> mapAccBadge = new Map<Id,Id>();
        List<FieloEE__BadgeMember__c> bmList =new List<FieloEE__BadgeMember__c>([SELECT FieloEE__Badge2__c,FieloEE__Member2__r.F_Account__c,FieloEE__Badge2__r.F_PRM_ProgramLevel__c,Id FROM FieloEE__BadgeMember__c WHERE Id IN :bmIds]);
        System.debug('bmList :'+bmList.size());
        for (FieloEE__BadgeMember__c b : bmList){
            mapAccBadge.put(b.FieloEE__Member2__r.F_Account__c,b.FieloEE__Badge2__c);    
            mapLevel.put(b.FieloEE__Badge2__c,b.FieloEE__Badge2__r.F_PRM_ProgramLevel__c);
            System.debug('Level1 :' + mapLevel);
        }
        System.debug('Level2 :'+mapLevel);
        System.debug('mapLevel Size '+mapLevel.size()); 
        System.debug('Map 1 :'+mapAccBadge);
        System.debug('Map 2 '+mapAccBadge.size()); 
        // Create a map with key Value Pair as Program Level and List of Brands Associated With it name it as --- mapBrands
        List<Id> plvIds= mapLevel.values();
        System.debug('plvIds : '+plvIds);
        System.debug('plvIds Size :'+plvIds.size());
        Map<Id,List<String>> mapBrands= new Map<Id,List<String>>();
        List<ProgramLevelBrand__c> plbList= new List<ProgramLevelBrand__c>([SELECT Brand__r.Name,ProgramLevel__c FROM ProgramLevelBrand__c WHERE ProgramLevel__c in : plvIds]);
        System.debug('plbList :' +plbList);
        System.debug('PlbList size :'+plbList.size());
        for(ProgramLevelBrand__c p : plbList){
            if(mapBrands.containsKey(p.ProgramLevel__c)){
                mapBrands.get(p.ProgramLevel__c).add(p.Brand__r.Name);
            }
            else{
                mapBrands.put(p.ProgramLevel__c,new List<String>{p.Brand__r.Name});
                //mapBrands.get(p.ProgramLevel__c).add(p.Brand__r.Name);
                System.debug('Brands1 :'+mapBrands);
                }
        }
        System.debug('Map Brands :' +mapBrands);
        System.debug('Map Brands size :' +mapBrands.size());
        
         List<Account> accLists  = new List<Account>();
         List<Account> accs= new List<Account>([select id,BrandsAssociated__c from Account where Id in : mapAccBadge.keySet()]);
         System.debug('Accounts Retrieved :' +accs);
         System.debug('Accounts Retrieved size :' +accs.size());
        for(Account a :accs){
            List<String> selector = mapBrands.get(mapLevel.get(mapAccBadge.get(a.id))); 
            system.debug('**selector**'+selector);
            String str=' ';
            String separator = ', ';
            str=String.join(selector, separator);
            a.BrandsAssociated__c=str;
            accLists.add(a);
            System.debug('List of Accounts Updated '+accLists);
        }
        System.debug('List of Accounts Updated '+accLists);
        System.debug('List of Accounts Updated size '+accLists.size());
        if(accLists.size()>0){
            update accLists;
            system.debug('* updated values**'+accLists);
        }
    }
}