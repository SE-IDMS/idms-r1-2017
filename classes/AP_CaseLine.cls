/*
Created By-Deepak
For April Services Release-14
Puspose to put a validation role for Unique case and Installed product.
*/
public class AP_CaseLine
{   
    
    //
    
    public Static void CLValidation(List<SVMXC__Case_Line__c> clList) 
    {   
        List<SVMXC__Case_Line__c> caselineList = New List<SVMXC__Case_Line__c>();
        set<string> s = New set<string>();
        
        if(clList.size()>0)
        {
            for(SVMXC__Case_Line__c cl :clList)
            {
                s.add(String.valueOf(cl.SVMXC__Case__c).substring(0,15)+'-'+String.valueOf(cl.SVMXC__Installed_Product__c).substring(0,15));
                system.debug('1s:'+s);
            }
        }
        caselineList=[Select Id,Name,Tech_SetOfIdIPCase__c From SVMXC__Case_Line__c Where Tech_SetOfIdIPCase__c in :s];
        
        system.debug('caselineList'+caselineList);      
        if(caselineList.size()>0 && clList.size()>0)
        {
            for(SVMXC__Case_Line__c cl :clList)
            {
               cl.addError('Duplicate Record Found');
            }
        }
        
    }
}