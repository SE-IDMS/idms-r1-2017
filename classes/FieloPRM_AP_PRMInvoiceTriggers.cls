/******************************************
* Developer: Fielo Team                   *
*******************************************/
public without sharing class FieloPRM_AP_PRMInvoiceTriggers{

    public static void assignsCountryAndCurrency(){

        List<FieloPRM_Invoice__c> triggernew = trigger.new;
        
        Set<Id> setMembers = new Set<Id>();
        
        for(FieloPRM_Invoice__c invoice: triggernew){
            setMembers.add(invoice.F_PRM_Member__c);
        }
        
        Map<Id,Id> mapMemberCountry = new Map<Id,Id>();
               
        for(FieloEE__Member__c member: [SELECT Id, F_Country__c FROM FieloEE__Member__c WHERE Id IN: setMembers]){
            mapMemberCountry.put(member.Id,member.F_Country__c);
        }

        Map<Id,PRMCountry__c> mapCountryPRMCountry = new Map<Id,PRMCountry__c>();

        for(PRMCountry__c PRMCounrty: [SELECT Id, Country__c,F_PRM_RewardsEmailRecipient__c, Country__r.CurrencyIsoCode FROM PRMCountry__c WHERE Country__c IN: mapMemberCountry.values()]){
            mapCountryPRMCountry.put(PRMCounrty.Country__c,PRMCounrty);
        }

        for(FieloPRM_Invoice__c invoice: triggernew){
            if(mapMemberCountry.containsKey(invoice.F_PRM_Member__c) && mapCountryPRMCountry.containsKey(mapMemberCountry.get(invoice.F_PRM_Member__c))){
                invoice.F_PRM_Country__c = mapCountryPRMCountry.get(mapMemberCountry.get(invoice.F_PRM_Member__c)).Id;
                invoice.CurrencyIsoCode = mapCountryPRMCountry.get(mapMemberCountry.get(invoice.F_PRM_Member__c)).Country__r.CurrencyIsoCode;
                invoice.F_PRM_BackofficeEmailRecipient__c = mapCountryPRMCountry.get(mapMemberCountry.get(invoice.F_PRM_Member__c)).F_PRM_RewardsEmailRecipient__c;
            }
        }
             
    }
    
    public static void processInvoiceDetails(){

        List<FieloPRM_Invoice__c> triggernew = trigger.new;
        Map<Id,SObject> triggerOldMap = trigger.oldMap;
        
        Set<Id> setInvoices = new Set<Id>();
        Set<Id> setCountries = new Set<Id>();

        Date minDate = null;
        Date maxDate = null;        
        
        for(FieloPRM_Invoice__c invoice: triggernew){
            if(invoice.F_PRM_Status__c == 'Approved' && (invoice.F_PRM_Status__c != ((FieloPRM_Invoice__c)triggerOldMap.get(invoice.Id)).F_PRM_Status__c)){
                setCountries.add(invoice.F_PRM_Country__c);
                setInvoices.add(invoice.Id);         
            }
            if(minDate == null || invoice.F_PRM_InvoiceDate__c < minDate){
                minDate = invoice.F_PRM_InvoiceDate__c;
            }
            if(maxDate == null || invoice.F_PRM_InvoiceDate__c > maxDate){
                maxDate = invoice.F_PRM_InvoiceDate__c;
            }
        }
        
        if(!setCountries.isEmpty() && minDate != null && maxDate != null){
        
            List<FieloPRM_ProductPointList__c> listExistingProductLists = [SELECT Id, F_PRM_Country__c, F_PRM_StartDate__c, F_PRM_EndDate__c, Name FROM FieloPRM_ProductPointList__c 
                                                                           WHERE (F_PRM_StartDate__c >=: minDate AND F_PRM_EndDate__c <=: maxDate) OR F_PRM_Country__c IN: setCountries];        
               
            Map<String,String> mapInvoicePointList = new Map<String,String>();
            
            for(FieloPRM_Invoice__c invoice: triggernew){
                for(FieloPRM_ProductPointList__c pointList: listExistingProductLists){
                    if(invoice.F_PRM_InvoiceDate__c >= pointList.F_PRM_StartDate__c && invoice.F_PRM_InvoiceDate__c >= pointList.F_PRM_StartDate__c){
                        mapInvoicePointList.put(invoice.Id,pointList.Id);
                    }
                }
            }
            
            Map<Id,FieloPRM_InvoiceDetail__c> mapInvoiceDetails = new Map<Id,FieloPRM_InvoiceDetail__c>([SELECT Id, F_PRM_Invoice__c, F_PRM_LoyaltyEligibleProduct__c FROM FieloPRM_InvoiceDetail__c WHERE F_PRM_Invoice__c IN: triggernew]);
        
            Set<String> setProducts = new Set<String>();
        
            for(FieloPRM_InvoiceDetail__c invoiceDetail: mapInvoiceDetails.values()){
                setProducts.add(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c);
            }
            
            List<FieloPRM_ProductPointValue__c> listPointValues = [SELECT Id, F_PRM_PointList__c, F_PRM_LoyaltyEligibleProduct__c, F_PRM_PtValue__c FROM FieloPRM_ProductPointValue__c WHERE F_PRM_LoyaltyEligibleProduct__c IN: setProducts AND F_PRM_PointList__c IN: mapInvoicePointList.values() AND F_PRM_IsActive__c = true];
        
            Map<String,Map<String,FieloPRM_ProductPointValue__c>> mapListProductValues = new Map<String,Map<String,FieloPRM_ProductPointValue__c>>();
            
            for(FieloPRM_ProductPointValue__c pointValue: listPointValues){
                mapListProductValues.put(pointValue.F_PRM_PointList__c,new Map<String,FieloPRM_ProductPointValue__c>{pointValue.F_PRM_LoyaltyEligibleProduct__c => pointValue});
            }
            
            Boolean isFound;
            
            Set<String> setDefaultPoints = new Set<String>();
            Set<String> setProductsDefaultPoints = new Set<String>();
            
            for(FieloPRM_InvoiceDetail__c invoiceDetail: mapInvoiceDetails.values()){
                isFound = false;
                if(mapInvoicePointList.containsKey(invoiceDetail.F_PRM_Invoice__c)){
                    if(mapListProductValues.containsKey(mapInvoicePointList.get(invoiceDetail.F_PRM_Invoice__c))){
                        if(mapListProductValues.get(mapInvoicePointList.get(invoiceDetail.F_PRM_Invoice__c)).containsKey(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c)){
                            isFound = true;
                            invoiceDetail.F_PRM_UnitPointValue__c = mapListProductValues.get(mapInvoicePointList.get(invoiceDetail.F_PRM_Invoice__c)).get(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c).F_PRM_PtValue__c;
                            invoiceDetail.F_PRM_PointValueReference__c = mapListProductValues.get(mapInvoicePointList.get(invoiceDetail.F_PRM_Invoice__c)).get(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c).Id;
                        }
                    }
                }
                if(isFound == false){
                    setDefaultPoints.add(invoiceDetail.Id);
                    setProductsDefaultPoints.add(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c);
                }
            }
            
            Map<String,FieloPRM_LoyaltyEligibleProduct__c> mapProductsPoints = new Map<String,FieloPRM_LoyaltyEligibleProduct__c>([SELECT Id, F_PRM_PointsInstaller__c FROM FieloPRM_LoyaltyEligibleProduct__c WHERE Id IN: setProductsDefaultPoints]);
            
            for(FieloPRM_InvoiceDetail__c invoiceDetail: mapInvoiceDetails.values()){
                if(setDefaultPoints.contains(invoiceDetail.Id)){
                    if(mapProductsPoints.containsKey(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c)){
                        invoiceDetail.F_PRM_UnitPointValue__c = mapProductsPoints.get(invoiceDetail.F_PRM_LoyaltyEligibleProduct__c).F_PRM_PointsInstaller__c;
                    }
                }
            }           
            
            update mapInvoiceDetails.values();
            
            system.debug('### setInvoices' + setInvoices);
            
            //Map<Id,FieloPRM_InvoiceDetail__c> mapInvoiceDetails = new Map<Id, FieloPRM_InvoiceDetail__c>([SELECT Id, F_PRM_Invoice__c, F_PRM_PointsInstaller__c, F_PRM_Processed__c, F_PRM_LoyaltyEligibleProduct__c, F_PRM_SkipEngine__c, F_PRM_Volume__c 
            //                                                                                              FROM FieloPRM_InvoiceDetail__c WHERE F_PRM_Invoice__c IN: setInvoices AND F_PRM_Processed__c = false]);

            if(!mapInvoiceDetails.isEmpty()){
            
                List<FieloDRE__ObjectsToProcess__c> listObjectsToProcess = [SELECT Id, FieloDRE__FieldMember__c, FieloDRE__FieldToRuleAppliedMasterLookup__c, FieloDRE__FieldTotalPoints__c, FieloDRE__APINameObject__c, FieloDRE__RuleAppliedMasterLookup__c, FieloDRE__RuleRecordTypeDevName__c,FieloDRE__FieldProcessed__c FROM FieloDRE__ObjectsToProcess__c WHERE FieloDRE__RuleRecordTypeDevName__c = 'FieloPRM_InvoiceDetailInstaller' LIMIT 1];
        
                SavePoint sp = Database.setSavepoint();
        
                try{
                    if(!listObjectsToProcess.isEmpty()){
                        FieloDRE.DynamicRuleEngine.processRecords(mapInvoiceDetails,listObjectsToProcess[0]);  
                        system.debug('### processed');              
                    }
                    
                    map<Id,FieloEE__Transaction__c> mapInvTran = new map<Id,FieloEE__Transaction__c>();
                    
                    for(FieloPRM_Invoice__c invoice: [SELECT Id, F_PRM_Transaction__c, F_PRM_Status__c, F_PRM_Member__c, F_PRM_TotalPointsInstaller__c, F_PRM_TotalAmount__c, CurrencyIsoCode FROM FieloPRM_Invoice__c WHERE Id IN: setInvoices]){
                        if(invoice.F_PRM_Transaction__c == null && invoice.F_PRM_Member__c != null && invoice.F_PRM_Status__c != null){
                            mapInvTran.put(invoice.Id, new FieloEE__Transaction__c(
                                FieloEE__Member__c = invoice.F_PRM_Member__c,
                                FieloEE__Type__c = 'PRM Invoice',
                                F_PRM_Invoice__c = invoice.Id,
                                F_ExternalValue__c = invoice.F_PRM_TotalPointsInstaller__c,
                                FieloEE__Value__c = invoice.F_PRM_TotalAmount__c,
                                CurrencyIsoCode = invoice.CurrencyIsoCode
                            ));
                        }
                    }                

                    if(!mapInvTran.isEmpty()){
                    
                        insert mapInvTran.values();
                        
                        List<FieloPRM_Invoice__c> listInvoiceToUpdate = new List<FieloPRM_Invoice__c>();
                        for(FieloEE__Transaction__c tra: mapInvTran.values()){
                            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(Id = tra.F_PRM_Invoice__c, F_PRM_Transaction__c = tra.Id);
                            listInvoiceToUpdate.add(invoice);
                        }
                        if(!listInvoiceToUpdate.isEmpty()){
                            update listInvoiceToUpdate;
                        }
                    }

                                    
                }catch(DMLException e){
                
                    Database.rollback(sp);
                    
                    for(FieloPRM_Invoice__c invoice: triggernew){
                        if(setInvoices.contains(invoice.Id)){
                            invoice.addError('Error while processing the invoice.' + e.getMessage());
                        }
                    }                
                }
                
                for(FieloPRM_InvoiceDetail__c invoiceDetail: mapInvoiceDetails.values()){
                    invoiceDetail.F_PRM_Processed__c = true;
                } 
                
                update mapInvoiceDetails.values();    
            }
        }
    
    }

    public static void updatesCurrency(){

        List<FieloPRM_Invoice__c> triggerNew = trigger.new;
        Map<Id,SObject> triggerNewMap = trigger.newMap;
        Map<Id,SObject> triggerOldMap = trigger.oldMap;
        
        system.debug('###triggerNew' + triggerNew);

        Set<Id> setInvoices = new Set<Id>();
        
        for(FieloPRM_Invoice__c invoice: triggernew){
            if(invoice.CurrencyIsoCode != ((FieloPRM_Invoice__c)triggerOldMap.get(invoice.Id)).CurrencyIsoCode){
                setInvoices.add(invoice.Id);
            }
        }
       
        if(!setInvoices.isEmpty()){
        
            Set<FieloPRM_InvoiceDetail__c> setInvoicesDetailToUpdate = new Set<FieloPRM_InvoiceDetail__c>();
            
            for(FieloPRM_InvoiceDetail__c invoiceDetail: [SELECT Id, CurrencyIsoCode, F_PRM_Invoice__c, F_PRM_UnitPrice__c FROM FieloPRM_InvoiceDetail__c WHERE F_PRM_Invoice__c IN: setInvoices]){
                //if((FieloPRM_Invoice__c)triggerNewMap.containsKey(invoiceDetail.F_PRM_Invoice__c)){
                    if(((FieloPRM_Invoice__c)triggerNewMap.get(invoiceDetail.F_PRM_Invoice__c)).CurrencyIsoCode != invoiceDetail.CurrencyIsoCode){
                        invoiceDetail.CurrencyIsoCode = ((FieloPRM_Invoice__c)triggerNewMap.get(invoiceDetail.F_PRM_Invoice__c)).CurrencyIsoCode;
                        invoiceDetail.F_PRM_UnitPrice__c = invoiceDetail.F_PRM_UnitPrice__c;
                        setInvoicesDetailToUpdate.add(invoiceDetail);
                    }
                //}
            }
            
             List<FieloPRM_InvoiceDetail__c> listInvoicesDetailToUpdate = new List<FieloPRM_InvoiceDetail__c>();
            
            for(FieloPRM_InvoiceDetail__c invoiceDetail: setInvoicesDetailToUpdate){
                listInvoicesDetailToUpdate.add(invoiceDetail);
            }
            
            system.debug('###listInvoicesDetailToUpdate' + listInvoicesDetailToUpdate);
            
            if(!listInvoicesDetailToUpdate.isEmpty()){
                update listInvoicesDetailToUpdate;
            }
        }  
  
    }
    

    public static void checksDuplicates(){
        List<FieloPRM_Invoice__c> triggerNew = trigger.new;
        Map<Id,sObject> triggerOldMap = trigger.oldMap;
        Boolean triggerIsUpdate = trigger.isUpdate;
        checksDuplicates(triggerNew, triggerOldMap, triggerIsUpdate);
        
    }
    
    public static void checksDuplicates(List<FieloPRM_Invoice__c> triggerNew, Map<Id,sObject> triggerOldMap, Boolean triggerIsUpdate){
        
        Set<String> setNewUniqueKey = new Set<String>();
        Set<String> setCountrys = new Set<String>();
        Set<String> setNames = new Set<String>();
        Set<Date> setDates = new Set<Date>();
        Set<String> setRetailers = new Set<String>();
        Set<String> setEditInvoices = new Set<String>();
        
        String uniqueKey = '';
        for(FieloPRM_Invoice__c invoice: triggerNew){
            if(invoice.F_PRM_RetailerAccount__c != null && invoice.F_PRM_Status__c != 'Rejected'){
                
                if(!triggerIsUpdate || (triggerOldMap != null && (invoice.F_PRM_Country__c != ((FieloPRM_Invoice__c)triggerOldMap.get(invoice.Id)).F_PRM_Country__c || invoice.Name != ((FieloPRM_Invoice__c)triggerOldMap.get(invoice.Id)).Name || invoice.F_PRM_InvoiceDate__c != ((FieloPRM_Invoice__c)triggerOldMap.get(invoice.Id)).F_PRM_InvoiceDate__c || invoice.F_PRM_RetailerAccount__c != ((FieloPRM_Invoice__c)triggerOldMap.get(invoice.Id)).F_PRM_RetailerAccount__c))){
                    setCountrys.add(invoice.F_PRM_Country__c);
                    setNames.add(invoice.Name);
                    setDates.add(invoice.F_PRM_InvoiceDate__c);
                    setRetailers.add(invoice.F_PRM_RetailerAccount__c);
                    uniqueKey = invoice.F_PRM_Country__c + '-' + invoice.Name + '-' + invoice.F_PRM_InvoiceDate__c + '-' + invoice.F_PRM_RetailerAccount__c;
                    if(!setNewUniqueKey.contains(uniqueKey)){
                        setNewUniqueKey.add(uniqueKey);
                    }
                    else{
                         invoice.addError(label.CLJUN16PRM063 + ' ' + invoice.Name);  
                    }
                    if(String.isNotBlank(invoice.Id))
                        setEditInvoices.add(invoice.Id);
                }
            }
        }
        
        system.debug('### setNewUniqueKey: ' + setNewUniqueKey);
        
        List<FieloPRM_Invoice__c> listExistingInvoices = [SELECT Id, Name, F_PRM_InvoiceDate__c, F_PRM_RetailerAccount__c, F_PRM_Country__c 
                                                          FROM FieloPRM_Invoice__c 
                                                          WHERE F_PRM_Country__c IN: setCountrys AND Name IN: setNames AND F_PRM_RetailerAccount__c != null 
                                                          AND F_PRM_InvoiceDate__c IN: setDates AND F_PRM_RetailerAccount__c IN: setRetailers
                                                          AND F_PRM_Status__c != 'Rejected' AND Id NOT IN: setEditInvoices];

        Set<String> setExistingUniqueKeys = new Set<String>();
        
        system.debug('### listExistingInvoices: ' + listExistingInvoices);
        
        if(!listExistingInvoices.isEmpty()){
            for(FieloPRM_Invoice__c invoice: listExistingInvoices){
                uniqueKey = invoice.F_PRM_Country__c + '-' + invoice.Name + '-' + invoice.F_PRM_InvoiceDate__c + '-' + invoice.F_PRM_RetailerAccount__c;
                if(!setExistingUniqueKeys.contains(uniqueKey)){
                    setExistingUniqueKeys.add(uniqueKey);
                }      
            }
            
            system.debug('### setExistingUniqueKeys: ' + setExistingUniqueKeys);
    
            for(FieloPRM_Invoice__c invoice: triggerNew){
                if(invoice.F_PRM_RetailerAccount__c != null && invoice.F_PRM_Status__c != 'Rejected'){
                    uniqueKey = invoice.F_PRM_Country__c + '-' + invoice.Name + '-' + invoice.F_PRM_InvoiceDate__c + '-' + invoice.F_PRM_RetailerAccount__c;
                    if(setExistingUniqueKeys.contains(uniqueKey)){
                        System.debug('*** CLJUN16PRM062'+label.CLJUN16PRM062);
                        invoice.addError(label.CLJUN16PRM062); 
                        //The invoice you are trying to submit already exists in the system. Thanks to contact your local rewards admin in case you need support
                    }
                }
            }
        }
    }

}