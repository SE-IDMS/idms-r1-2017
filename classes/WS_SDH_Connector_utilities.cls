global class WS_SDH_Connector_utilities {


    //batch limit
    public static final integer WEBSERVICELIMIT = 200;
    public static final integer QUERYMAXLIMIT = 50000;
    
    //blank
    
    public static final string BLANK = '';
    
    //Generic object description 
    global class ObjectInfoFields{
          public string ObjectName{get;set;}
          public String IdIdentifier{get;set;}
          public string ObjeCtfield{get;set;}
          //we can add as many field as we want depanding of the needs
         
    }
        
    //description : verify the type of input
    //input string inputId, string objectName
    //ouput string result
    public static string validId (string inputId, string objectName){
         
        Id  validId;
        ObjectInfoFields OneObject = new ObjectInfoFields();
        try {
            
            if(objectName != 'StateProvince__c' ){
                //try to convert the input in id
                validId = inputId ;
               

            }
            else{
                String[] sarray =   inputId.split(':');
                 validId = sarray[0] ;

            }

            //fetch the object description
            OneObject = GetObjectInfo(objectName);
             System.debug('asdf3'+OneObject );
            
            
            //if object not recognized by the object description fonction return error
            if(OneObject.ObjectName == 'invalid'){
                return 'invalidObject';
            //verivy if the 3 first digits match to the iputid if not return error
            }else if(inputId.substring(0, 3) == OneObject.IdIdentifier) {
                return 'ValidMatch';
            }else{
                return 'InvalidMatch';
            }

            
        
        } catch (Exception ex) {                        
            //if not a id but valid object return sesa else return error
            
            if(objectName == 'User'){
                return 'User';
            }
            else if(objectName =='Account'){
                return 'Account';

            }
            else if(objectName =='Contact'){
                return 'Contact';

            }
            else if(objectName =='Country__c'){
                return 'Country__c';

            }else if(objectName =='StateProvince__c'){
                return 'StateProvince__c';

            }
            else if(objectName =='LegacyAccount__c'){
                return 'LegacyAccount__c';

            }
            else if(objectName =='DeviceType__c'){
                return 'DeviceType__c';

            }
            else if(objectName =='Brand__c'){
                return 'Brand__c';

            }
            else if(objectName =='Category__c'){
                return 'Category__c';

            }
            
            else{
                return 'invalidObject';
            }
            
        }
        

                
    }
    
    //description : format the list to inorporate to a dynamic request
    //By adding quotes between each values
    //input list of string
    //outpu string formated list
    public static string SOQLListFormat(list<string> input){   
        String SOQL_ListFormat = '';
        for (string Value : input) {
                String value_in_quotes = '\''+Value+'\'';
        if (SOQL_ListFormat!='') { SOQL_ListFormat+=','; }  //  add a comma if this isn't the first one
                SOQL_ListFormat += value_in_quotes;
        }   
        return SOQL_ListFormat;
    }
    
  
    //description : init the utility object depanding of the onject name in input
    //input string ObjectName
    //outpu ObjectInfoFields (utility object)
    public static ObjectInfoFields GetObjectInfo(string ObjectName){
         System.debug('asdf1'+ObjectName);
        ObjectInfoFields OneObject = new ObjectInfoFields();
        OneObject.ObjectName = ObjectName;
        //we can add as many object as we want depending of the needs
        if(ObjectName == 'User'){
            String SOPrefix = SObjectType.User.getKeyPrefix(); 
            OneObject.IdIdentifier = SOPrefix;
            OneObject.ObjeCtfield = 'federationIdentifier';
        }
        else if(ObjectName == 'Account'){                

                String SOPrefix = SObjectType.Account.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'SEAccountID__c';

        }
        else if(ObjectName == 'Contact'){                

                String SOPrefix = SObjectType.Contact.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'SEContactID__c';

        }
        else if(ObjectName == 'Country__c'){                

                String SOPrefix = SObjectType.Country__c.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'CountryCode__c';

        }
        else if(ObjectName == 'StateProvince__c'){                

                String SOPrefix = SObjectType.StateProvince__c.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'StateProvinceCode__c';

        }
        else if(ObjectName == 'LegacyAccount__c'){                

                String SOPrefix = SObjectType.LegacyAccount__c.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'LegacyKey__c';

        }
        else if(ObjectName == 'Brand__c'){                

                String SOPrefix = SObjectType.Brand__c.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'SDHBRANDID__c';

        }
        else if(ObjectName == 'DeviceType__c'){                

                String SOPrefix = SObjectType.DeviceType__c.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'SDHDEVICETYPEID__c';
        }
        else if(ObjectName == 'Category__c'){                

                String SOPrefix = SObjectType.Category__c.getKeyPrefix(); 
                OneObject.IdIdentifier = SOPrefix;
                OneObject.ObjeCtfield = 'SDHCategoryID__c';
        }

        else{
            //else retunr an invalid object to say that the input onject is not recongnized
            OneObject.ObjectName = 'invalid';
        }
        
        return OneObject;
    }

    


}