//Test class for VFC_ProjectCockpit- OCT 2015 release
@isTest
public class VFC_ProjectCockpit_TEST {
    static testMethod void testMethodVFC_ProjectCockpit() {
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        prj.CurrencyIsoCode = 'INR';
        update prj;  
        
        Opportunity opp1 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp1.Project__c=prj.Id;
        Insert opp1;
        
        Opportunity opp2 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp2.ParentOpportunity__c=opp1.Id;
        Insert opp2;
        
        Opportunity opp3 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp3.ParentOpportunity__c=opp2.Id;
        Insert opp3;
        
        test.startTest();
        try{
            Opportunity opp4 = Utils_TestMethods.createOpenOpportunity(acc.Id);
            opp4.Project__c=prj.Id;
            Insert opp4;
            PageReference myVfPage = Page.VFP_ProjectCockpit;
            Test.setCurrentPageReference(myVfPage);
            ApexPages.currentPage().getParameters().put('id',prj.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(prj);
            VFC_ProjectCockpit extController = new VFC_ProjectCockpit(sc);
            VFC_ProjectCockpit.getParentdata(prj.Id);
            VFC_ProjectCockpit.getChildren(opp1.Id);
        }
        Catch(Exception ex){}
        test.stopTest(); 
    }
}