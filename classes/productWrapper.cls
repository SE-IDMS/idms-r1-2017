public with sharing class productWrapper{
    public String businessLine{get;set;}
    public String productLine{get;set;}
    public String strategicProductFamily{get;set;}
    public String family{get;set;}
    public String commercialReference{get;set;}
    public String description{get;set;} 
    public String subFamily{get;set;}
    public String productSuccession{get;set;}
    public String productGroup{get;set;}
    public String level4GMRCode{get;set;}
    public String level7GMRCode{get;set;}
    public boolean selected{get;set;}
    public Integer indexNumber{get;set;}

}