global class Batch_PRMMigrationFromV1toV2 implements Database.Batchable<sObject> {
	
	String query;
	List<Id> programLevelIdList;
	
	global Batch_PRMMigrationFromV1toV2(List<Id> programLevelIdList) {
		this.programLevelIdList = programLevelIdList;
		query = 'SELECT Id,ProgramLevel__c,Contact__c from ContactAssignedProgram__c where Active__c=true and ProgramLevel__c in:programLevelIdList and ProgramLevel__c!=null';
		system.debug('programLevelIdList: '+programLevelIdList);
		system.debug('query: '+query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<ContactAssignedProgram__c> contactAssignProgList = (List<ContactAssignedProgram__c>)scope;   
		Map<Id ,Set<String>> badgesByContactToAssign = new Map<Id ,Set<String>>();
		for(ContactAssignedProgram__c contactAssignProg : contactAssignProgList){
    		if(badgesByContactToAssign.containsKey(contactAssignProg.Contact__c)){
                badgesByContactToAssign.get(contactAssignProg.Contact__c).add(contactAssignProg.ProgramLevel__c);
            }else{
            	system.debug('contactAssignProg.Contact__c: '+contactAssignProg.Contact__c);
            	system.debug('contactAssignProg.ProgramLevel__c: '+contactAssignProg.ProgramLevel__c);
                badgesByContactToAssign.put(contactAssignProg.Contact__c, new Set<String>{contactAssignProg.ProgramLevel__c});
            }
        }
		System.debug('contactAssignProgList: '+badgesByContactToAssign);
		System.debug('badgesByContactToAssign: '+badgesByContactToAssign);
		if(!badgesByContactToAssign.isEmpty())
    		PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactToAssign);
		        
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}