/*******
Object:Offer after Update

Release :March Q1 Release.

*********/


Public class AP_ELLAOffer{

    
    //Offer object user Acces
    public static void OfferReadWriteAccess(List<Offer_Lifecycle__c> listOffer,Map<id,Offer_Lifecycle__c> mapOldOffer) {
        
        Map<id,set<Id>> mapOfOfferList= new Map<id,set<Id>>();
        Map<string,set<string>> MapUsrRemoveAccs = new Map<string,set<string>>();
        List<Offer_Lifecycle__c> listOfferOwenrChange= new List<Offer_Lifecycle__c> (); 
        if(listOffer.size() > 0 && mapOldOffer.size() >0) {
            for(Offer_Lifecycle__c olc:listOffer) {
                if(olc.ownerid!=null && olc.ownerid==mapOldOffer.get(olc.id).ownerid ) {
                    mapOfOfferList= new Map<id,set<Id>>();
                    if(!mapOfOfferList.containskey(olc.id)) {
                        mapOfOfferList.put(olc.id,new set<id>());
                        MapUsrRemoveAccs.put(olc.id,new set<string>());
                        if(olc.Segment_Marketing_VP__c!=null && olc.Segment_Marketing_VP__c!=mapOldOffer.get(olc.id).Segment_Marketing_VP__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Segment_Marketing_VP__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Segment_Marketing_VP__c );
                        }
                        /*if(olc.Launch_Owner__c!=null && olc.Launch_Owner__c!=mapOldOffer.get(olc.id).Launch_Owner__c ) {
                           // mapOfOfferList.get(olc.id).add(olc.Launch_Owner__c);
                           // MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Launch_Owner__c );
                        }*/
                        if(olc.Launch_Area_Manager__c!=null && olc.Launch_Area_Manager__c!=mapOldOffer.get(olc.id).Launch_Area_Manager__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Launch_Area_Manager__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Launch_Area_Manager__c );
                        }
                        if(olc.Marketing_Director__c!=null && olc.Marketing_Director__c!=mapOldOffer.get(olc.id).Marketing_Director__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Marketing_Director__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Marketing_Director__c );
                        }
                        if(olc.Withdrawal_Owner__c!=null && olc.Withdrawal_Owner__c!=mapOldOffer.get(olc.id).Withdrawal_Owner__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Withdrawal_Owner__c); 
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Withdrawal_Owner__c );                     
                        }                   
                                            
                    }
                } else {
                    
                    listOfferOwenrChange.add(olc);
                }
              
            }
        }
        
        system.debug('TEST--->'+MapUsrRemoveAccs);
        if(MapUsrRemoveAccs.size() >0) {
          AP_ELLAOffsharing.RemoveAccesOnObject(MapUsrRemoveAccs,system.label.CLMAR16ELLAbFO03);
            updateprojectAccess(null,MapUsrRemoveAccs);
        
        }
        if(mapOfOfferList.size() > 0) {
            AP_ELLAOffsharing.ShareObjetReadW(mapOfOfferList,system.label.CLMAR16ELLAbFO03);//Offer_Lifecycle__share
            updateprojectAccess(mapOfOfferList,null);
        }
        if(listOfferOwenrChange.size() > 0) {
                OfferListUserToShare(listOfferOwenrChange);
                for(Offer_Lifecycle__c olc:listOfferOwenrChange) {
               
                    mapOfOfferList= new Map<id,set<Id>>();
                    if(!mapOfOfferList.containskey(olc.id)) {
                        mapOfOfferList.put(olc.id,new set<id>());
                        MapUsrRemoveAccs.put(olc.id,new set<string>());
                        if(olc.Segment_Marketing_VP__c!=null && olc.Segment_Marketing_VP__c!=mapOldOffer.get(olc.id).Segment_Marketing_VP__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Segment_Marketing_VP__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Segment_Marketing_VP__c );
                        }
                        if(olc.Launch_Owner__c!=null && olc.Launch_Owner__c!=mapOldOffer.get(olc.id).Launch_Owner__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Launch_Owner__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Launch_Owner__c );
                        }
                        if(olc.Launch_Area_Manager__c!=null && olc.Launch_Area_Manager__c!=mapOldOffer.get(olc.id).Launch_Area_Manager__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Launch_Area_Manager__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Launch_Area_Manager__c );
                        }
                        if(olc.Marketing_Director__c!=null && olc.Marketing_Director__c!=mapOldOffer.get(olc.id).Marketing_Director__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Marketing_Director__c);
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Marketing_Director__c );
                        }
                        if(olc.Withdrawal_Owner__c!=null && olc.Withdrawal_Owner__c!=mapOldOffer.get(olc.id).Withdrawal_Owner__c ) {
                            mapOfOfferList.get(olc.id).add(olc.Withdrawal_Owner__c); 
                            MapUsrRemoveAccs.get(olc.id).add(mapOldOffer.get(olc.id).Withdrawal_Owner__c );                     
                        }                   
                                            
                    }
                }
                if(MapUsrRemoveAccs.size() >0) {
                    updateprojectAccess(null,MapUsrRemoveAccs);

                }
                if(mapOfOfferList.size() > 0) {
                    updateprojectAccess(mapOfOfferList,null);
                }

        }       
    }

    //April 2015 release Hanamanth
    
    public static void BoxwebserviceOnOwnerChange(List<Offer_Lifecycle__c> listNewOffer,Map<id,Offer_Lifecycle__c> mapOldOffr) {
        Set<string>  OfferSetid = new  Set<string> ();
    
    
        for(Offer_Lifecycle__c olfObj:listNewOffer) {
            
            if(olfObj.Launch_Owner__c !=null && olfObj.Launch_Owner__c!=mapOldOffr.get(olfObj.id).Launch_Owner__c ) {
                OfferSetid.add(olfObj.id);
            
            }
            if(olfObj.Withdrawal_Owner__c!=null && olfObj.Withdrawal_Owner__c!=mapOldOffr.get(olfObj.id).Withdrawal_Owner__c) {
                OfferSetid.add(olfObj.id);
            
            }
        }
        
        // Ap_Box_FolderCreation.HttpWebserviceToFolderBox(trigger.new); 
     
        if(OfferSetid.size() > 0) {
            AP_ELLAToBoxFolderCreation.OfferCollbrCreationInBox(OfferSetid);
        }   

        
    }
    //On After Inser Trigger Update Share the Records .
    Public static void OfferListUserToShare(List<Offer_Lifecycle__c> listOffer) {
        Map<id,set<Id>> mapOfOfferList = new Map<id,set<Id>>();
        
        if(listOffer.size() > 0) {
            for(Offer_Lifecycle__c olc:listOffer) {
                if(!mapOfOfferList.containskey(olc.id)) {
                    mapOfOfferList.put(olc.id,new set<id>());
                    if(olc.Segment_Marketing_VP__c!=null) {
                        mapOfOfferList.get(olc.id).add(olc.Segment_Marketing_VP__c);
                        
                    }
                    if(olc.Launch_Owner__c!=null) {
                        mapOfOfferList.get(olc.id).add(olc.Launch_Owner__c);
                    }
                    if(olc.Launch_Area_Manager__c!=null) {
                        mapOfOfferList.get(olc.id).add(olc.Launch_Area_Manager__c);
                    }
                    if(olc.Marketing_Director__c!=null) {
                        mapOfOfferList.get(olc.id).add(olc.Marketing_Director__c);
                    }
                    if(olc.Withdrawal_Owner__c!=null) {
                        mapOfOfferList.get(olc.id).add(olc.Withdrawal_Owner__c);    
                    }                 
                                        
                }              
            }
        }
        
        //call method update sharing rule.
        if(mapOfOfferList.size() > 0) {
            AP_ELLAOffsharing.ShareObjetReadW(mapOfOfferList,system.label.CLMAR16ELLAbFO03);//Offer_Lifecycle__share
        }               
        
    }   
    
    //Before update
    public static void updateOfferBoxEmail(List<Offer_Lifecycle__c>  listNewOffer) {
        

        set<string> setUserId = new set<string>();
        if(trigger.isInsert && Trigger.isBefore) {

            for(Offer_Lifecycle__c trgObj:listNewOffer) {
                system.debug('HAna'+trgObj.Launch_Owner__r.email);
                // trgObj.BoxUserEmailId__c =trgObj.Launch_Owner__r.email; 
                if(trgObj.Launch_Owner__c!=null && trgObj.BoxUserEmailId__c==null ) {
                setUserId.add(trgObj.Launch_Owner__c);
                }else if(trgObj.Withdrawal_Owner__c!=null && trgObj.BoxUserEmailId__c==null) {
                    setUserId.add(trgObj.Withdrawal_Owner__c); 
                }

            }


        }
        Map<id,User> mapUser =new Map<id,User>([select id ,email from user where id =:setUserId]);
        if(mapUser.size() > 0) {
            for(Offer_Lifecycle__c OffObj:listNewOffer) {
                if(OffObj.Launch_Owner__c!=null) {  
                    OffObj.BoxUserEmailId__c=mapUser.get(OffObj.Launch_Owner__c).email;
                    OffObj.OwnerId=OffObj.Launch_Owner__c;
                }else if (OffObj.Withdrawal_Owner__c !=null) {
                    OffObj.BoxUserEmailId__c=mapUser.get(OffObj.Withdrawal_Owner__c).email;
                    OffObj.OwnerId=OffObj.Withdrawal_Owner__c; 
                }       
            }
        }       
    }
    
    //To give access to project
    public static void updateprojectAccess( Map<id,set<Id>> mapOfOfferUserforProject,Map<string,set<string>> mapUserRemovefrProject) {
        
        Map<id,set<Id>> mapOfOfferUserAcc= new Map<id,set<Id>>();
        Map<string,set<string>> mapOfOfferUserAccRemove= new Map<string,set<string>>();
        If(mapOfOfferUserforProject!=null){
            if((mapOfOfferUserforProject!=null && mapOfOfferUserforProject.size() >0 && mapOfOfferUserforProject.values().size() > 0)) {
                mapOfOfferUserAcc= new Map<id,set<Id>>();
            
                for(Milestone1_Project__c listmsProject:[select id,Offer_Launch__c from Milestone1_Project__c where Offer_Launch__c In:mapOfOfferUserforProject.keyset() ]) {
                    
                       
                    if(!mapOfOfferUserAcc.containskey(listmsProject.Offer_Launch__c) && mapOfOfferUserforProject.size() >0 && mapOfOfferUserforProject.values().size() > 0 ) {                      
                    
                        mapOfOfferUserAcc.put(listmsProject.id,mapOfOfferUserforProject.get(listmsProject.Offer_Launch__c));                      //mapOfOfferUserAcc.get(listmsProject.id).add());
                        
                    } 
                }               
            } 
        } 
        if(mapUserRemovefrProject!=null) {
                if(mapUserRemovefrProject!=null && mapUserRemovefrProject.size() > 0 && mapUserRemovefrProject.values().size() > 0)  {
                mapOfOfferUserAcc= new Map<id,set<Id>>();
            
                for(Milestone1_Project__c listmsProject:[select id,Offer_Launch__c from Milestone1_Project__c where Offer_Launch__c IN:mapUserRemovefrProject.keyset()  ]) {                  
                       
                    if(!mapOfOfferUserAccRemove.containskey(listmsProject.Offer_Launch__c) && mapUserRemovefrProject.size() > 0 && mapUserRemovefrProject.values().size() > 0) {
                        mapOfOfferUserAccRemove.put(listmsProject.id,mapUserRemovefrProject.get(listmsProject.Offer_Launch__c));
                    }               
                }               
            }            
        }       

        if(mapOfOfferUserAcc.size() > 0 && mapOfOfferUserAcc.values().size() > 0) {   
            AP_ELLAOffsharing.ShareObjetReadW(mapOfOfferUserAcc,system.label.CLMAR16ELLAbFO04);//Offer_Lifecycle__share 
        }
        if(mapOfOfferUserAccRemove.size() > 0 && mapOfOfferUserAccRemove.values().size() > 0) {   
            AP_ELLAOffsharing.RemoveAccesOnObject(mapOfOfferUserAccRemove,system.label.CLMAR16ELLAbFO04);           
        }
        
    }
    //Q3 2016 Release Update of Bu sell date 
    public static void updateOfferProjectField(Map<id,Offer_Lifecycle__c> MapnewOffer,Map<id,Offer_Lifecycle__c> MapOldOffer) {
        
        Map<string ,Milestone1_Project__c>  UpdateMProjectMap = new Map<string ,Milestone1_Project__c>();
        Map<String, List<Milestone1_Project__c>> MileStoneMap= new  Map<String, List<Milestone1_Project__c>>();

        if(MapnewOffer.size() > 0 ) {
            for(Milestone1_Project__c mpObj:[select id,TECH_IsOfferCycleApproved__c,TECH_IsOfferSELLDateChanged__c,TECH_IsOfferBUSELLDateChanged__c,TECH_IsOfferForecastStageAlert__c,Product_Picture__c,Offer_Launch__c from Milestone1_Project__c where  Offer_Launch__c=:MapnewOffer.keyset()]) {
            
            if(MileStoneMap.containsKey(mpObj.Offer_Launch__c)) {
                MileStoneMap.get(mpObj.Offer_Launch__c).add(mpObj);

            }else {
                MileStoneMap.put(mpObj.Offer_Launch__c,new List<Milestone1_Project__c>()); 
                MileStoneMap.get(mpObj.Offer_Launch__c).add(mpObj);
            }

            }
            for(Offer_Lifecycle__c obj:MapnewOffer.values()) {
                
                if(MileStoneMap.containskey(obj.id)) {
                     for(Milestone1_Project__c mpObj1:MileStoneMap.get(obj.id)){
                        if((obj.BD_Status__c=='Published' && obj.BD_Status__c!=MapOldOffer.get(obj.id).BD_Status__c) || (obj.Withdrawal_Notice_Status__c=='Published' && obj.Withdrawal_Notice_Status__c!=MapOldOffer.get(obj.id).Withdrawal_Notice_Status__c)) {
                            mpObj1.TECH_IsOfferCycleApproved__c=true;
                            UpdateMProjectMap.put(mpObj1.id,mpObj1);
                        }
                       
                        if((obj.Launch_Owner__c!=null && obj.Launch_Owner__c!=MapOldOffer.get(obj.id).Launch_Owner__c)  ) {
                            mpObj1.TECH_Offer_and_Withdrawal_Owner_Email__c=obj.LaunchOwnerEmail__c;
                            UpdateMProjectMap.put(mpObj1.id,mpObj1);
                        
                        }else if( obj.Withdrawal_Owner__c!=null && obj.Withdrawal_Owner__c!=MapOldOffer.get(obj.id).Withdrawal_Owner__c) {
                            mpObj1.TECH_Offer_and_Withdrawal_Owner_Email__c=obj.LaunchOwnerEmail__c;
                            UpdateMProjectMap.put(mpObj1.id,mpObj1);
                        }
                        if(obj.Forecast_Stage__c!=null && obj.Forecast_Stage__c!=MapOldOffer.get(obj.id).Forecast_Stage__c) {
                            mpObj1.TECH_IsOfferForecastStageAlert__c=true;
                            UpdateMProjectMap.put(mpObj1.id,mpObj1);
                        }
                        if(obj.Sales_Date__c!=null && obj.Sales_Date__c!=MapOldOffer.get(obj.id).Sales_Date__c) {
                            mpObj1.TECH_IsOfferBUSELLDateChanged__c=true;
                            mpObj1.TECH_IsOfferSELLDateChanged__c =true;
                            UpdateMProjectMap.put(mpObj1.id,mpObj1);
                        }
                        if(obj.End_of_Commercialization_Date__c!=null && obj.End_of_Commercialization_Date__c!=MapOldOffer.get(obj.id).End_of_Commercialization_Date__c) {
                            mpObj1.TECH_IsOfferBUSELLDateChanged__c=true;
                            UpdateMProjectMap.put(mpObj1.id,mpObj1);
                        }
                        //UpdateMProjectList.add(mpObj1);
                        system.debug('TEST+---------->'+UpdateMProjectMap);
                     }
                
                }
            }
                
                if(UpdateMProjectMap.size() >0 ) {
                    AP_bFO_COLA_OfferCountryBUTaskCreation.IsRec=true;
                    update UpdateMProjectMap.values(); 
                }
            
        }
    }
    
    
    public static void UpdateOfferAnnouncement (Map<id,Offer_Lifecycle__c> MapnewAnnOffer,Map<id,Offer_Lifecycle__c> MapOldAnnOffer) {
        
        
         //update the Description Announcement 
         Map<String, List<Announcement__c>> MapAnncement= new  Map<String, List<Announcement__c>>();
          Map<string ,Announcement__c>  UpdateAnnouncement = new Map<string ,Announcement__c>();
        if(MapnewAnnOffer.size() > 0 ) {
            for(Announcement__c mpObj:[select id,OfferDescription__c,Offer_Lifecycle__c,SubstitutionStrategy__c from Announcement__c where  Offer_Lifecycle__c=:MapnewAnnOffer.keyset()]) {
                if(MapAnncement.containsKey(mpObj.Offer_Lifecycle__c)) {
                    MapAnncement.get(mpObj.Offer_Lifecycle__c).add(mpObj);

                }else {
                    MapAnncement.put(mpObj.Offer_Lifecycle__c,new List<Announcement__c>()); 
                    MapAnncement.get(mpObj.Offer_Lifecycle__c).add(mpObj);

                }

            }
        }
        for(Offer_Lifecycle__c objOffer:MapnewAnnOffer.values()) {
            if(MapAnncement.containskey(objOffer.id)) {
                 for(Announcement__c objAnnment:MapAnncement.get(objOffer.id)){
                    if((objOffer.Description__c!=null && objOffer.Description__c!=MapOldAnnOffer.get(objOffer.id).Description__c)||(objOffer.Substitution_Strategy__c!=null && objOffer.Substitution_Strategy__c!=MapOldAnnOffer.get(objOffer.id).Substitution_Strategy__c)) {
                        objAnnment.OfferDescription__c=objOffer.Description__c;
                        objAnnment.SubstitutionStrategy__c =objOffer.Substitution_Strategy__c;
                        UpdateAnnouncement.put(objAnnment.id,objAnnment);
                    }
                 }
            
            }
        
        
        }
        if(UpdateAnnouncement.size() > 0) {
            update UpdateAnnouncement.values();
        }
    }
     
}//main class end