@isTest
class AP_SpecializationRequirement_Test {

    static testMethod void testAP_SpecializationRequirement_1() {
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        User u1;
        System.runAs ( thisUser ) {
          UserRole uRole = [SELECT Id FROM UserRole WHERE Name='CEO' LIMIT 1];

          List<User> lstUsers = new List<User>();

          Id profilesId = [select id from profile where name='System Administrator'].Id;
          u1= new User(Username = 'testUserTwo@schneider-electric.com', LastName = 'User11', alias = 'sysad2',
                          CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                          Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US' , ProfileID = profilesId, UserRoleId=uRole.Id,
                          ProgramOwner__c=true, ProgramManager__c=true,ProgramApprover__c=true, ProgramAdministrator__c=true,
                          PartnerORFConverter__c=true,BypassVR__c=true,Country__c='TCY' );
          lstUsers.add(u1);
          //insert lstUsers;

          System.debug('*** Partner User:' + u1);
        }

        System.runAs(u1) {
            Country__c country = Utils_TestMethods.createCountry();
            insert country;

            System.debug('User Country:' + u1.Country__c);

            List<User> lstPrgApprover = [select id from user where isActive=true and ProgramApprover__c=true limit 20];
            List<User> lstPrgAdmin = [select id from user where isActive=true and ProgramAdministrator__c=true limit 20];
            List<User> lstPrgManager = [select id from user where isActive=true and ProgramManager__c=true limit 20];
            List<User> lstPrgOwner = [select id from user where isActive=true and ProgramOwner__c=true limit 20];
            
            Recordtype acctSpeActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountSpecializationRequirement__c' limit 1];
            Recordtype acctPrgActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
            Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
            Recordtype SpeciliActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'SpecializationRequirement__c' limit 1];
            Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
            
            SpecializationCatalog__c spc1 = new SpecializationCatalog__c();
            spc1.name = 'Test Catalog 1';
            spc1.Active__c =true;
            spc1.ExpirationPeriod__c = '12';
            spc1.description__c = 'test 2';
            insert spc1;
            
            RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req;

            RequirementCatalog__c req2 = Utils_TestMethods.createRequirementCatalog();
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req2;

            ClassificationLevelCatalog__c lvl1 = new ClassificationLevelCatalog__c();
            lvl1.ClassificationLevelName__c = 'Test Class Level1';
            lvl1.Name='TESTSPZLVL1';
            lvl1.Active__c=true;
            insert lvl1;

            MarketSegmentCatalog__c sgmt = new MarketSegmentCatalog__c();
            sgmt.MarketSegmentName__c = 'Test Market Segment';
            sgmt.Name='TESTSPZSEG1';
            sgmt.Active__c=true;
            insert sgmt;

            DomainsOfExpertiseCatalog__c pdoe = new DomainsOfExpertiseCatalog__c();
            pdoe.Name='TESTSPZPDOE';
            pdoe.DomainsOfExpertiseName__c='Parent DOE';
            pdoe.Active__c=true;
            insert pdoe;

            DomainsOfExpertiseCatalog__c cdoe = new DomainsOfExpertiseCatalog__c();
            cdoe.Name='TESTSPZCDOE';
            cdoe.DomainsOfExpertiseName__c='Child DOE';
            cdoe.Active__c=true;
            cdoe.ParentDomainsOfExpertise__c=pdoe.Id;
            insert cdoe;

            CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
            newRecTypeMap.name = recCatalogActivityReq.Id;
            newRecTypeMap.AARRecordTypeID__c = acctPrgActivityReq .Id;
            newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
            newRecTypeMap.ASPRRecordTypeID__c= acctSpeActivityReq.Id;
            insert newRecTypeMap;

            Pagereference pg = Page.VFP_NewSpecialization ;
            pg.getParameters().put('retURL','/'+spc1.id); 
            Test.setCurrentPage(pg);
            VFC_NewSpecialization sn = new  VFC_NewSpecialization(new  Apexpages.Standardcontroller(new Specialization__c() ));
            
            sn.redirectToNewSpecialization();
            
            Specialization__c spe1 = new Specialization__c();
            spe1.name = 'spe 1';
            spe1.SpecializationCatalog__c = spc1.id;
            spe1.level__C= 'Basic';
            spe1.ExpirationPeriod__c = '12';
            spe1.status__c= 'Active';
            spe1.ProgramApprover__c = lstPrgApprover[0].id;
            spe1.ProgramAdministrator__c = lstPrgAdmin[0].id ;
            spe1.TECH_CountriesId__c=country.Id;
            insert spe1;

            SpecializationRequirement__c sr1 = new SpecializationRequirement__c();
            sr1.Active__c = false;
            sr1.RequirementCatalog__c = req.id;
            sr1.Specialization__c = spe1.id;
            insert sr1;
            
            sr1.Active__c = true;
            update sr1;
            
            SpecializationRequirement__c sr2 = new SpecializationRequirement__c();
            sr2.Active__c = false;
            sr2.RequirementCatalog__c = req2.id;
            sr2.Specialization__c = spe1.id;
            insert sr2;
            
            sr2.Active__c = true;
            update sr2;
            

            SpecializationClassification__c sc = new SpecializationClassification__c(
                    ClassificationLevelCatalog__c=lvl1.Id,
                    Specialization__c=spe1.Id
                );
            insert sc;

            SpecializationMarket__c sm = new SpecializationMarket__c(
                    MarketSegmentCatalog__c=sgmt.Id,
                    Specialization__c=spe1.Id
                );
            insert sm;

            SpecializationDomainsOfExpertise__c sd = new SpecializationDomainsOfExpertise__c(
                DomainsOfExpertiseCatalog__c=cdoe.Id,
                Specialization__c=spe1.Id
                );
            insert sd;
            
            Apexpages.Standardcontroller cont1 = new Apexpages.Standardcontroller(spe1);
            Pagereference pg1 = Page.VFP_NewCountrySpecialization ;
           
            pg1.getParameters().put('GlobalSpeId',spe1.id); 
            pg1.getParameters().put('type','cluster');  
            
            Test.setCurrentPage(pg1);
                    
            VFC_NewCountrySpecialization  rcp = new VFC_NewCountrySpecialization (cont1);
            rcp.createCountrySpecialization();
        
            Apexpages.Standardcontroller cont6 = new Apexpages.Standardcontroller(spe1);
            Pagereference pg6 = Page.VFP_NewCountrySpecialization ;
           
            pg6.getParameters().put('GlobalSpeId',spe1.id);
            pg6.getParameters().put('type','country');  
            Test.setCurrentPage(pg6);
                    
            VFC_NewCountrySpecialization  rcp2 = new VFC_NewCountrySpecialization (cont6);
            rcp2.createCountrySpecialization();
        }   
    }
}