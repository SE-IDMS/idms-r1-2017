public class VFC_ComponentEntry {

    public List<Component__c> ords {get; set;}
    public Final Component__c parOrd;
    public String selectedRec {get;set;}
    public string DispMessage {get;set;} 
    public String component {get;set;}
    public String selectedIndex {get;set;}
    public String val {get;set;}
    Public boolean varresult {get;set;}
    
     public VFC_ComponentEntry() {         
         ApexPages.StandardController sc = new ApexPages.StandardController(parOrd);
         parOrd=(Component__c)sc.getrecord();
         ords = new List<Component__c>();
         Component__c LitOrd = new Component__c();       
         LitOrd.Release__c = parOrd.Release__c; 
         Litord.Owner__c = Userinfo.getuserid();         
         LitOrd.IsMigrate__c = true;          
         ords.add(LitOrd);     
    }
     
    
    public VFC_ComponentEntry(ApexPages.StandardController myController) {
        parOrd=(Component__c)myController.getrecord();
        ords = new List<Component__c>();
        Component__c LitOrd = new Component__c();       
        LitOrd.Release__c = parOrd.Release__c;
        Litord.Owner__c = Userinfo.getuserid();
        LitOrd.IsMigrate__c = true;                   
        ords.add(LitOrd);          
        }
        
 
 
 
 
  public List<SelectOption> getName()
  {
    List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();     
    List<SelectOption> options = new List<SelectOption>();
    List<String> StrList = new List<String>();
    for(Schema.SObjectType f : gd)
    { 
      if(!f.getDescribe().getLabel().startswith('History') && !f.getDescribe().getLabel().startswith('Tag') && !f.getDescribe().getLabel().startswith('Feed'))     
           StrList.add(f.getDescribe().getName());
            //options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getLabel()));
    }
    If(StrList != null && StrList.size()>0){
        StrList.sort();
        options.add(New SelectOption('','Select'));
        for(String s:StrList){      
        options.add(new SelectOption(s,s));
        }
    }
    return options;
   }
   
    public String getDispMessage()
    {
      String StrDisp;
      return StrDisp;
    }
     
    public void addrow() {
        Component__c LitOrd = new Component__c();      
        LitOrd.Release__c = parOrd.Release__c;
        Litord.Owner__c = Userinfo.getuserid();
        ords.add(LitOrd);}
            
    public void removerow(){  
        list<String> lstInt = selectedRec.split(';');
        list<Integer> lstIndex = new list<Integer>();
        for(integer i=0;i<lstInt.size();i++)
        lstIndex.add(Integer.valueOf(lstInt[i]));
        lstIndex.sort();
            for(integer i=lstIndex.size()-1;i>-1;i--){
                ords.remove(lstIndex[i]);
            }
            selectedRec = '';      
        }
            
    public PageReference save() {     
    try{
    
    //ListMetadataQuery ds = new ListMetadataQuery();
        insert ords; 
        PageReference parrec = new PageReference('/'+parOrd.Release__c);
        parrec.setRedirect(true);
        return parrec; 
        }
        catch(DmlException ex){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
        return null;
        }   
         }
        
    public PageReference cancel() {
        Schema.DescribeSObjectResult d =   Release__c.sObjectType.getDescribe();
        String url='';
        url='/'+d.getKeyPrefix();
        PageReference parrec = new PageReference(url);
        parrec.setRedirect(true);
        return parrec; }    
        
    public void populateAPI()
    {
        system.debug('component :'+component );
        system.debug('selectedIndex :'+selectedIndex );
        system.debug('hello' );
        list<String> lstString = component.split(':');
        component = lstString[0];
        selectedIndex = lstString[1];
        system.debug('component :'+component );
        system.debug('selectedIndex :'+selectedIndex );
        if(component != null && component == 'Custom Field' && component == '<--Select-->'){
        varresult = true;
        }
        try
        {
            ComponentAPINames__c myCS2 = ComponentAPINames__c.getInstance(component);
            String componentAPIName = myCS2.APIName__c;
            ords.get(Integer.valueOf(selectedIndex)).Component_API_Name__c = componentAPIName;
        }
        catch(Exception e)
        {
            ords.get(Integer.valueOf(selectedIndex)).Component_API_Name__c = 'No Example available';
        }
    } 
 }