global class VFC_SRV_MyFSSetNewPassword extends VFC_SRV_MyFsLoginPage {

   global static boolean debugMode = true;  

   @RemoteAction
   global static ResetPasswordResult setNewPasswordRemoting(String newPassword, String token){
       
        ResetPasswordResult resetPasswordResult = new ResetPasswordResult();
        resetPasswordResult.Success = false;
        resetPasswordResult.ErrorMsg = newPassword;   
        
        Id loggedInUserId = UserInfo.getUserId();
        boolean result = false;
        
        try{
            if(String.isNotBlank(token)) {     
                
                token = EncodingUtil.URLDecode(token, 'UTF-8');
                
                uimsv2ImplServiceImsSchneiderComUM.UserManager_UIMSV2_ImplPort uimsPort = new uimsv2ImplServiceImsSchneiderComUM.UserManager_UIMSV2_ImplPort(); 
                uimsPort.endpoint_x = Label.SRV_MyFs_SetNewPasswordEndpoint;
                uimsPort.timeout_x = 120000;
                uimsPort.clientCertName_x = Label.SRV_MyFs_ClientCertName;
                
                result = uimsPort.setPassword(Label.SRV_MyFs_CallerId, token, newPassword); 
                System.debug('setNewPassword result : '+result);  
        
                resetPasswordResult.Success = true;
                resetPasswordResult.ErrorMsg = Label.SRV_MyFs_Success;
            }
            else {
                resetPasswordResult.Success = false;
                resetPasswordResult.ErrorMsg = Label.SRV_MyFs_InvalidLinkErrorMessage;             
            }
        }         
        catch(exception e){
            handleException(e.getMessage(), resetPasswordResult);
        }       
        
        return resetPasswordResult;   
    }
    
    global static void handleException(String exceptionMessage, ResetPasswordResult resetPasswordResult) {
    
            resetPasswordResult.Success = false;
            
            if(debugMode) {
                 resetPasswordResult.ErrorMsg = 'Error: ' + exceptionMessage;                               
            }
            else {
            
                if(exceptionMessage != null && exceptionMessage.contains('The link in the email has expired')) {
                     resetPasswordResult.ErrorMsg = Label.SRV_MyFs_ExpiredLink;         
                }
                else {
                    resetPasswordResult.ErrorMsg = Label.SRV_MyFs_GenericErrorMessage;                 
                }        
            }
            
            System.debug('setNewPassword Exception: ' + exceptionMessage);    
    }
}