@isTest
public class VFC_PRMUserLoginComponentController_Test {
    
    @testSetup
    static void setupPRMUserLogin() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='testprmuserlogin.login@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='testprmuserlogin.login@schneider-electric.com' + Label.CLMAR13PRM05,
                         UserRoleId = r.Id,
                        FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                         isActive = True,                       
                         BypassVR__c = True);
        INSERT u;       
        ApexPages.currentPage().getHeaders().put('country', 'US');
        
    }
    
    public static testMethod void testPRMUserLoginInit_1() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', 'IMSQueryParam');
        ApexPages.currentPage().getParameters().put('goto', 'IMSGotoParam');
        //ApexPages.currentPage().getHeaders().put('Referer', 'https://ims-sqe.btsec.dev.schneider-electric.com/opensso/UI/Login?errorMessage=auth.failed&Login.Submit=Login&goto=http%3A%2F%2Fims-sqe.btsec.dev.schneider-electric.com%2Fopensso%2Fidpssoinit%3FmetaAlias%3D%252Fidp%26spEntityID%3Dhttps%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&gx_charset=UTF-8&spEntityID=https%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&errorMessage=auth.failed');
        
        VFC_PRMUserLoginComponentController loginCtrlr = new VFC_PRMUserLoginComponentController(); 
        loginCtrlr.currentURL = '/login';  
        String params = loginCtrlr.getIsSSOParamSet();
    }

    public static testMethod void testPRMUserLoginInit_2() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', '');
        ApexPages.currentPage().getParameters().put('goto', '');
        ApexPages.currentPage().getHeaders().put('Referer', System.Label.CLAPR15PRM114);
        ApexPages.currentPage().getParameters().put('country', '');         
        VFC_PRMUserLoginComponentController loginCtrlr1 = new VFC_PRMUserLoginComponentController();
        ApexPages.currentPage().getParameters().put('country', 'US'); 
        VFC_PRMUserLoginComponentController loginCtrlr = new VFC_PRMUserLoginComponentController();
        String params = loginCtrlr.getIsSSOParamSet();  
        loginCtrlr.currentURL = '/login';       
        //loginCtrlr.redirectPage();
    }
    
    public static testMethod void testPRMUserLoginInit_212() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', '');
        ApexPages.currentPage().getParameters().put('goto', '');
        ApexPages.currentPage().getHeaders().put('Referer', System.Label.CLAPR15PRM114);
        ApexPages.currentPage().getParameters().put('language', '');    
        VFC_PRMUserLoginComponentController loginCtrlr12 = new VFC_PRMUserLoginComponentController();
        loginCtrlr12.ReferredFrom = '';
        loginCtrlr12.Country='US';
        loginCtrlr12.socialMediaType='';
        loginCtrlr12.socialLoginType='';
        VFC_PRMUserLoginComponentController.ResetPasswordResult  rpr = new VFC_PRMUserLoginComponentController.ResetPasswordResult();
        rpr.ErrorMsg ='';
        ApexPages.currentPage().getParameters().put('language', 'English'); 
        VFC_PRMUserLoginComponentController loginCtrlr = new VFC_PRMUserLoginComponentController();
        String params = loginCtrlr.getIsSSOParamSet();  
        loginCtrlr.currentURL = '/pomplogin';       
        //loginCtrlr.redirectPage();
        ApexPages.currentPage().getParameters().put('StartURL', '/login');
        //loginCtrlr.redirectPage();
    }

    public static testMethod void testPRMUserLoginInit_3() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', '');
        ApexPages.currentPage().getParameters().put('goto', '');
        ApexPages.currentPage().getHeaders().put('Referer', '');
        
        VFC_PRMUserLoginComponentController loginCtrlr = new VFC_PRMUserLoginComponentController();
        loginCtrlr.currentURL = '/login';   
        String params = loginCtrlr.getIsSSOParamSet();
    }
    public static testMethod void testResetPassword_1() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
        VFC_PRMUserLoginComponentController.ResetPasswordResult resetResult = VFC_PRMUserLoginComponentController.ResetPassword('testprmuserlogin.login@schneider-electric.com');
        System.debug('** Reset Pwd Result:' + resetResult);        
        Test.stopTest();
    }
    
    public static testMethod void testResetPassword_2() {
        Test.startTest();
        List<Country__c> lstCountrys = new List<Country__c>();
        
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
            
        //lstCountrys.add(testCountry); 
        INSERT testCountry;
        
        Country__c testCountry3 = new Country__c();
            testCountry3.Name   = 'Pakistan1';
            testCountry3.CountryCode__c = 'PK';
            testCountry3.InternationalPhoneCode__c = '91';
            testCountry3.Region__c = 'APAC';
            
         INSERT testCountry3;
         Country__c testCountry4 = new Country__c();
            testCountry4.Name   = 'Srilanka';
            testCountry4.CountryCode__c = 'SL';
            testCountry4.InternationalPhoneCode__c = '91';
            testCountry4.Region__c = 'APAC';
            
         INSERT testCountry4;
         Country__c testCountry5 = new Country__c();
            testCountry5.Name   = 'USA';
            testCountry5.CountryCode__c = 'US';
            testCountry5.InternationalPhoneCode__c = '001';
            testCountry5.Region__c = 'APAC';
            
         INSERT testCountry5;
         
         
        
        Country__c testCountry1 = new Country__c();
            testCountry1.Name   = 'France';
            testCountry1.CountryCode__c = 'FR';
            testCountry1.InternationalPhoneCode__c = '31';
            testCountry1.Region__c = 'APAC';
        INSERT testCountry1;
                
        List<PRMCountry__c> lstPRMCntry = new List<PRMCountry__c>();
        
        PRMCountry__c newPRMCntry = new PRMCountry__c(Name='IND',Country__c = testCountry.Id, PLDatapool__c = 'test111',CountryPortalEnabled__c=true);
        lstPRMCntry.add(newPRMCntry);
        
        newPRMCntry = new PRMCountry__c(Name='EMEA - FR1',Country__c = testCountry1.Id, PLDatapool__c = 'test112',CountryPortalEnabled__c =true,
                                    SupportedLanguage1__c='EN',SupportedLanguage2__c='ES',SupportedLanguage3__c='FR',MemberCountry1__c=testCountry3.Id,MemberCountry2__c=testCountry4.Id,
                                    MemberCountry3__c=testCountry5.Id);
                            
        lstPRMCntry.add(newPRMCntry);
        
        Insert lstPRMCntry;
        System.debug('Country Cluster'+lstPRMCntry);
        ApexPages.currentPage().getParameters().put('country', 'US');
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test()); 
        VFC_PRMUserLoginComponentController loginCtrlr4 = new VFC_PRMUserLoginComponentController(); 
        VFC_PRMUserLoginComponentController.ResetPasswordResult resetResult = VFC_PRMUserLoginComponentController.ResetPassword('demoprmuserlogin.login@schneider-electric.com');
        System.debug('** Reset Pwd Result:' + resetResult);        
        Test.stopTest();
    }

}