// Created by Uttara PR - Nov 2016 Release - Escape Notes project

@IsTest
public class AP_GetStockDetails_Test {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        String json = '{    \"targetERP\": \"OracleERP\",    \"product\": [      {        \"sku\": \"FJT1500RMJ2U\",        \"targetSku\": \"FJT1500RMJ2U\",        \"organization\": {          \"id\": \"129\",          \"code\": \"P1F\",          \"type\": \"MANUFACTURING\"        },        \"stock\": [          {            \"name\": \"NetQuantity\",            \"value\": \"34\"          },          {            \"name\": \"NonnettaleQuantity\",            \"value\": \"0\"          },          {            \"name\": \"AtpQuanity\",            \"value\": \"34\"          },          {            \"name\": \"TotalQtyOnHand\",            \"value\": \"35\"          },          {            \"name\": \"IntransitQty\",            \"value\": \"1\"          },          {            \"name\": \"HideFg\",            \"value\": \"0\"          },          {            \"name\": \"CurrentOrders\",            \"value\": \"0\"          },          {            \"name\": \"LongTermOrders\",            \"value\": \"0\" },          {            \"name\": \"ShortTermOrders\",            \"value\": \"0\"          },          {            \"name\": \"NetSafetyStock\",            \"value\": \"35\"          },          {            \"name\": \"DCSafetyStock\",            \"value\": \"34\"          },          {            \"name\": \"RevenueOfQtyGoesHold\",            \"value\": \"0\",       \"currency\": \"USD\"          },          {            \"name\": \"BodLeadTime\",            \"value\": \"0\"          }        ]      },      {        \"sku\": \"FJT1500RMJ2U\",        \"targetSku\": \"FJT1500RMJ2U\",        \"organization\": {          \"id\": \"2891\",          \"code\": \"JAD\",          \"type\": \"DISTRIBUTION\"        },        \"stock\": [          {            \"name\": \"NetQuantity\",            \"value\": \"1587\"          },          {            \"name\": \"NonnettaleQuantity\",            \"value\": \"5\"          },          {            \"name\": \"AtpQuanity\",            \"value\": \"1582\"          },          {            \"name\": \"TotalQtyOnHand\",            \"value\": \"1627\"          },          {            \"name\": \"IntransitQty\",            \"value\": \"40\"          },          {            \"name\": \"HideFg\",            \"value\": \"5\"          },          {            \"name\": \"CurrentOrders\",            \"value\": \"0\"          },          { \"name\": \"LongTermOrders\",            \"value\": \"0\"          },          {            \"name\": \"ShortTermOrders\",            \"value\": \"0\"          },          {            \"name\": \"NetSafetyStock\",            \"value\": \"1275\"          },          {            \"name\": \"DCSafetyStock\",            \"value\": \"1238\"          },          {            \"name\": \"RevenueOfQtyGoesHold\",            \"value\": \"6547722896\"          },          {            \"name\": \"BodLeadTime\",            \"value\": \"15\"          }        ]      },      {        \"sku\": \"FJT150345FD\",        \"error\": { \"message\": \"Item Not found in Oracle \" }      }    ]}';
        AP_GetStockDetails r = AP_GetStockDetails.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        AP_GetStockDetails.Organization objOrganization = new AP_GetStockDetails.Organization(System.JSON.createParser(json));
        System.assert(objOrganization != null);
        System.assert(objOrganization.id == null);
        System.assert(objOrganization.code == null);
        System.assert(objOrganization.type_Z == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        AP_GetStockDetails.Stock_Z objStock_Z = new AP_GetStockDetails.Stock_Z(System.JSON.createParser(json));
        System.assert(objStock_Z != null);
        System.assert(objStock_Z.name == null);
        System.assert(objStock_Z.value == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        AP_GetStockDetails objRoot = new AP_GetStockDetails(System.JSON.createParser(json));
        System.assert(objRoot != null);
        System.assert(objRoot.targetERP == null);
        System.assert(objRoot.product == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        AP_GetStockDetails.Product objProduct = new AP_GetStockDetails.Product(System.JSON.createParser(json));
        System.assert(objProduct != null);
        System.assert(objProduct.sku == null);
        System.assert(objProduct.targetSku == null);
        System.assert(objProduct.organization == null);
        System.assert(objProduct.stock == null);
        System.assert(objProduct.error == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        AP_GetStockDetails.Error objError = new AP_GetStockDetails.Error(System.JSON.createParser(json));
        System.assert(objError != null);
        System.assert(objError.message == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        AP_GetStockDetails.Stock objStock = new AP_GetStockDetails.Stock(System.JSON.createParser(json));
        System.assert(objStock != null);
        System.assert(objStock.name == null);
        System.assert(objStock.value == null);
        System.assert(objStock.currency1 == null);
    }
}