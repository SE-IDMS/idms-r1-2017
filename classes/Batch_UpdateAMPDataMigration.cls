/*
 Batch class to update AMP when Country weights or Global weights are changed
 
 Format to call this class
 String q='Select Id from AccountMasterProfile__c where Account__r.ClassLevel1__c =\'RT\' and Account__r.MarketSegment__c=\'BD1\' and Account__r.Country__c=\'a0H21000000nQ8e\'';
 Batch_UpdateAMPDataMigration ampBatch = new Batch_UpdateAMPDataMigration(q);
        Database.executeBatch(ampBatch);
        
*/

global class Batch_UpdateAMPDataMigration implements Database.Batchable<sObject>{
    public String query;
    
    global Batch_UpdateAMPDataMigration(String Q)
    {        
         query=Q;
    }
    
     global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AccountMasterProfile__c> scope) {
         List<AccountMasterProfile__c> amplst=new List<AccountMasterProfile__c>();
         for(AccountMasterProfile__c s:scope)
            amplst.add(s);

         Database.update(amplst,false);
    } 
    
    global void finish(Database.BatchableContext BC) {
        //After updating AMP, Account needs to be synced with updated AMP 
        Batch_UpdateAccountFromAMP ampBatch = new Batch_UpdateAccountFromAMP();
        Database.executeBatch(ampBatch,Integer.ValueOf(Label.CLQ316SLS009));   
    }
    
}