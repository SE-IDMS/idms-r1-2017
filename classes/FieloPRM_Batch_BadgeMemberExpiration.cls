/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

global class FieloPRM_Batch_BadgeMemberExpiration implements Database.Batchable<sObject>, Schedulable{
    
    public date auxdate {get;set;}
    private static final CS_PRM_ApexJobSettings__c config = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_BadgeMemberExpiration');
    
    global FieloPRM_Batch_BadgeMemberExpiration(){}
    
    //BATCH METHODS
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String todayDateString =  FieloPRM_UTILS_BadgeMember.getExpirationDateText(auxdate == null ? Date.today() : auxdate);
        
        String allFields = '';
        for(String fieldName : Schema.getGlobalDescribe().get('FieloEE__BadgeMember__c').getDescribe().fields.getMap().keySet()) {
            allFields += allFields == '' ? fieldName : ', ' + fieldName;
        }
        
        String query = 'SELECT ' + allFields + ' FROM FieloEE__BadgeMember__c WHERE F_PRM_ExpirationDateText__c <= \'' + todayDateString + '\' AND F_PRM_ExpirationDateText__c != NULL AND F_PRM_Status__c  = \'Active\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<FieloEE__BadgeMember__c> listBadgeMember = (List<FieloEE__BadgeMember__c>)scope;
        set<id> setIdBadge = new set<id>();
        set<id> setIdMember = new set<id>();
        for(FieloEE__BadgeMember__c badgemember :  listBadgeMember){
            badgemember.F_PRM_Status__c = 'Inactive';
            setIdMember.add(badgemember.FieloEE__Member2__c);
            setIdBadge.add(badgemember.FieloEE__Badge2__c);
        }
        try{
            update listBadgeMember;    
        }catch(Exception e){
            FieloEE__ErrorLog__c errorL = new FieloEE__ErrorLog__c(FieloEE__Message__c = e.getMessage(), FieloEE__StackTrace__c = e.getStackTraceString(), FieloEE__Type__c = e.getTypeName(), FieloEE__LineNumber__c = e.getLineNumber(), FieloEE__UserId__c = UserInfo.getUserId());
            insert errorL;
        }
        

        list<FieloCH__ChallengeReward__c> listChallengeRewards = [SELECT id, FieloCH__Challenge__c, FieloCH__Position__c, FieloCH__Badge__c FROM FieloCH__ChallengeReward__c WHERE FieloCH__Badge__c in : setIdBadge];
        
        set<id> setIdChallenge = new set<id>();
        map<id,id> mapIdBadgeChallenge = new map<id,id>();
        for(FieloCH__ChallengeReward__c challengereward:  listChallengeRewards){
            mapIdBadgeChallenge.put(challengereward.FieloCH__Badge__c, challengereward.FieloCH__Challenge__c);
            setIdChallenge.add(challengereward.FieloCH__Challenge__c);
        }
        
        //Badge with valid period
        list<FieloEE__BadgeMember__c> listBadgeMemberAux = [Select id, F_PRM_Status__c, FieloEE__Member2__c, FieloEE__Badge2__c, F_PRM_ExpirationDate__c, F_PRM_ExpirationDateText__c FROM FieloEE__BadgeMember__c WHERE FieloEE__Member2__c in : setIdMember];
        set<id> setIdBadgeAux = new set<id>();
        map<String,FieloEE__BadgeMember__c> mapBadgeMember = new map<String,FieloEE__BadgeMember__c>();
        for(FieloEE__BadgeMember__c badgeMember : listBadgeMemberAux){
            setIdBadgeAux.add(badgeMember.FieloEE__Badge2__c);
            mapBadgeMember.put(String.valueof(badgeMember.FieloEE__Member2__c) + String.valueof(badgeMember.FieloEE__Badge2__c) ,  badgeMember);
        }
        
        list<FieloCH__ChallengeReward__c> listChallengeRewards2 = [SELECT id,FieloCH__Badge__c, FieloCH__Challenge__c, FieloCH__Position__c FROM FieloCH__ChallengeReward__c WHERE FieloCH__Badge__c in : setIdBadgeAux AND FieloCH__Challenge__c in :setIdChallenge ORDER BY FieloCH__Position__c];

        map<id,List<FieloCH__ChallengeReward__c>> mapChallengeRewards = new map<id,List<FieloCH__ChallengeReward__c>>();
        for(FieloCH__ChallengeReward__c chReward:  listChallengeRewards2){
            list<FieloCH__ChallengeReward__c> listChallengeRewAux = new list<FieloCH__ChallengeReward__c>();
            if(mapChallengeRewards.containskey(chReward.FieloCH__Challenge__c)){
                listChallengeRewAux = mapChallengeRewards.get(chReward.FieloCH__Challenge__c);
            }
            listChallengeRewAux.add(chReward);
            mapChallengeRewards.put(chReward.FieloCH__Challenge__c, listChallengeRewAux);
        }
        
        List<FieloEE__BadgeMember__c> listBadgeMemberToUpdate = new List<FieloEE__BadgeMember__c>();
        for(FieloEE__BadgeMember__c badgeMember :  listBadgeMember){
            if(mapIdBadgeChallenge.containskey(badgeMember.FieloEE__Badge2__c)){
                id idChallenge = mapIdBadgeChallenge.get(badgeMember.FieloEE__Badge2__c);
                if(mapChallengeRewards.containskey(idChallenge)){
                    list<FieloCH__ChallengeReward__c> listChallengeRewAux = mapChallengeRewards.get(idChallenge);
                    for(FieloCH__ChallengeReward__c  chReward : listChallengeRewAux){

                        String keyMapAux = String.valueof(badgeMember.FieloEE__Member2__c) + String.valueof(chReward.FieloCH__Badge__c);
                        if(mapBadgeMember.containskey(keyMapAux)){
                            
                            FieloEE__BadgeMember__c badgeMemberToUpdate = mapBadgeMember.get(keyMapAux);
                            if(badgeMemberToUpdate.id != badgeMember.id && (badgeMemberToUpdate.F_PRM_ExpirationDate__c == null || badgeMemberToUpdate.F_PRM_ExpirationDate__c > date.today())){
                                badgeMemberToUpdate.F_PRM_Status__c = 'Active';
                                listBadgeMemberToUpdate.add(badgeMemberToUpdate);
                                break;
                            }
                        }
                    }
                }
            }
        }
        try{
            update listBadgeMemberToUpdate;  
        }catch(Exception e){
            FieloEE__ErrorLog__c errorL = new FieloEE__ErrorLog__c(FieloEE__Message__c = e.getMessage(), FieloEE__StackTrace__c = e.getStackTraceString(), FieloEE__Type__c = e.getTypeName(), FieloEE__LineNumber__c = e.getLineNumber(), FieloEE__UserId__c = UserInfo.getUserId());
            insert errorL;
        }
    }
    
    global void finish(Database.BatchableContext BC) {}
    
    //SCHEDULE METHODS
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new FieloPRM_Batch_BadgeMemberExpiration(), Integer.valueOf(config.F_PRM_BatchSize__c));
    }   
    
    public static void scheduleNow(){
        DateTime nowTime = datetime.now().addSeconds(65);
        String Seconds      = '0';
        String Minutes      = nowTime.minute() < 10 ? '0' + nowTime.minute() : String.valueOf(nowTime.minute());
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        System.schedule('FieloPRM_Batch_BadgeMemberExpiration '+system.now() + ':' + system.now().millisecond(), CronExpression, new FieloPRM_Batch_BadgeMemberExpiration());
    }
    
}