/**
* @createdBy   Ester Mendelson, 14/07/2015         
* @lastModifiedBy  Ester Mendelson, 02/08/2015 
*/
@isTest(seeAllData = true)
private class ZuoraBillingAccount_Test {
    /**
     * [test creating zuora account with selection of existing contact.]
     * @createdBy   Ester Mendelson, 14/07/2015         
     * @lastModifiedBy  Ester Mendelson, 02/08/2015 
     */
    @isTest static void TestSelectedExistingContact() {

        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }

        Account account = Utils_TestMethods.createAccount();
        account.CurrencyIsoCode = 'USD';
        account.country__c = country.id;
        insert account;

        StateProvince__c state = Utils_TestMethods.createStateProvince(country.id);
        insert state;

        Contact con = Utils_TestMethods.createContact(account.Id,'test_billTo');
        con.Email = 'ester@keresen.com';
        con.zqu__County__c = country.id;
        con.City__c = 'Tel-Aviv';
        con.StateProv__c =state.id;
        insert con;

        Test.startTest();

        ApexPages.currentPage().getParameters().put(Label.lbl_UrlKeyAccountId,account.id);

        ZuoraBillingAccount_CTRL ctrl = new ZuoraBillingAccount_CTRL(new ApexPages.StandardController(new Zuora__CustomerAccount__c()));
        ctrl.RedirectToAccount();
        ctrl.newBilToContact = false;
        ctrl.newSoldToContact = false;
        ctrl.zuoraAccount.ReferenceName__c = 'TEST';
        ctrl.zuoraAccount.Zuora__BillToId__c = con.id;
        ctrl.zuoraAccount.Zuora__SoldToId__c = con.id;
        ctrl.selectExistingBillToContact = con;
        ctrl.selectExistingSoldToContact = con;
        ctrl.demoTestClassResultsSuccess = true;
        ctrl.createZuoraAccountAndContact();
        //ctrl.demoTestClassResultsSuccess = false;
        ctrl.zuoraAccount.InvoiceOwner__c = 'Y';
        ctrl.initPaymentMethod();
        ctrl.initBillingAccountName();
        ctrl.zuoraAccount.InvoiceOwner__c = 'N';
        ctrl.initPaymentMethod();
        ctrl.zuoraAccount.ImmediateInvoiceAccount__c = 'No';
        ctrl.getBatchOptions();
        ctrl.zuoraAccount.ImmediateInvoiceAccount__c = 'Yes';
        ctrl.getBatchOptions();
        ctrl.zuoraAccount.InvoiceOwner__c = 'Y';
        ctrl.getBatchOptions();
        ctrl.createZuoraAccountAndContact();
        ctrl.setFieldList();
        ctrl.setTitleList();
        ctrl.getPaymentTermOptions();
        ctrl.soldToContactSelection();
        ctrl.soldToContactSelectExist();
        ctrl.soldToContactCopyBillTo();
        ctrl.billoContactSelectExist();
        ctrl.billoContactSelection();
        ctrl.restartContactSelection();
        ctrl.restartSoldToContactSelection();

        ctrl.checkDefaultPaymentMethodExists();
        ctrl.checkDefaultSubscriptionLocationExists();
        ctrl.CopyBillToEmailFromExistingContact();
        ctrl.CopySoldToEmailFromExistingContact();

        Test.stopTest();
    }
    /**
     * [test creating zuora account with selection of new contact.]
     * @createdBy   Ester Mendelson, 14/07/2015         
     * @lastModifiedBy  Ester Mendelson, 02/08/2015 
     */
    @isTest static void TestSelectedNewContact() {
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }

        Account account = Utils_TestMethods.createAccount();
        account.CurrencyIsoCode = 'USD';
        account.country__c = country.id;
        insert account;

        Test.startTest();
        ApexPages.currentPage().getParameters().put(Label.lbl_UrlKeyAccountId,account.id);
        ZuoraBillingAccount_CTRL ctrl = new ZuoraBillingAccount_CTRL(new ApexPages.StandardController(new Zuora__CustomerAccount__c()));
        ZuoraBillingAccount_CTRL newCTRL = new ZuoraBillingAccount_CTRL(new ApexPages.StandardController(new Zuora__CustomerAccount__c()));
        ZuoraBillingAccount_CTRL anotherCTRL = new ZuoraBillingAccount_CTRL(new ApexPages.StandardController(new Zuora__CustomerAccount__c()));
        
        ctrl.zuoraAccount.ReferenceName__c = 'TEST';
        ctrl.billToContact.firstName = 'ESTER';
        ctrl.billToContact.LastName = 'TEST';
        ctrl.billToContact.Country__c = country.id;
        
        ctrl.soldToContact.firstName = 'ESTER';
        ctrl.soldToContact.LastName = 'TEST';
        ctrl.soldToContact.Country__c = country.id;
        
        
        ctrl.newBilToContact = true;
        ctrl.newSoldToContact = true;
        
        ctrl.initBillingAccountName();
        ctrl.zuoraAccount.InvoiceOwner__c = 'Y';
        ctrl.zuoraAccount.InvoiceOwner__c = 'Y';
        ctrl.demoTestClassResultsSuccess = true;
        ctrl.createZuoraAccountAndContact();
        ctrl.soldToContact.Country__c = null;
        ctrl.billToContact.Country__c = null;
        ctrl.createZuoraAccountAndContact();
        ctrl.demoTestClassResultsSuccess = false;
        ctrl.createZuoraAccountAndContact();
        
        
        newCTRL.zuoraAccount.ReferenceName__c = '';
        newCTRL.billToContact.firstName = 'ESTER';
        newCTRL.billToContact.LastName = 'TEST';
        newCTRL.billToContact.Country__c = country.id;
        
        newCTRL.soldToContact.firstName = 'ESTER';
        newCTRL.soldToContact.LastName = 'TEST';
        newCTRL.soldToContact.Country__c = country.id;
        
        
        newCTRL.newBilToContact = true;
        newCTRL.newSoldToContact = true;
        
        newCTRL.initBillingAccountName();
        newCTRL.zuoraAccount.InvoiceOwner__c = 'Y';
        newCTRL.zuoraAccount.InvoiceOwner__c = 'Y';
        newCTRL.demoTestClassResultsSuccess = true;
        newCTRL.createZuoraAccountAndContact();
        newCTRL.soldToContact.Country__c = null;
        newCTRL.billToContact.Country__c = null;
        newCTRL.createZuoraAccountAndContact();
        newCTRL.demoTestClassResultsSuccess = false;
        newCTRL.createZuoraAccountAndContact();
        
        anotherCTRL.zuoraAccount.ReferenceName__c = '';
        anotherCTRL.zuoraAccount.CompanyLanguage__c = 'French';

        anotherCTRL.billToContact.firstName = 'ESTER';
        anotherCTRL.billToContact.LastName = 'TEST';
        anotherCTRL.billToContact.Country__c = country.id;
        
        anotherCTRL.soldToContact.firstName = 'ESTER';
        anotherCTRL.soldToContact.LastName = 'TEST';
        anotherCTRL.soldToContact.Country__c = country.id;
        
        
        anotherCTRL.newBilToContact = true;
        anotherCTRL.newSoldToContact = true;
        
        anotherCTRL.initBillingAccountName();
        anotherCTRL.zuoraAccount.InvoiceOwner__c = 'Y';
        anotherCTRL.zuoraAccount.InvoiceOwner__c = 'Y';
        anotherCTRL.demoTestClassResultsSuccess = true;
        anotherCTRL.createZuoraAccountAndContact();
        anotherCTRL.soldToContact.Country__c = null;
        anotherCTRL.billToContact.Country__c = null;
        anotherCTRL.createZuoraAccountAndContact();
        anotherCTRL.demoTestClassResultsSuccess = false;
        anotherCTRL.createZuoraAccountAndContact();
         
        Test.stopTest();
        
        
    }
     /**
     * [test error in request of creating zuora account without contact.]
     * @createdBy   Ester Mendelson, 16/07/2015         
     * @lastModifiedBy  Ester Mendelson, 02/08/2015 
     */
    @isTest static void TestNoContactSelectedContact() {
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }


        Account account = Utils_TestMethods.createAccount();
        account.CurrencyIsoCode = 'USD';
        account.country__c = country.id;
        insert account;

        Test.startTest();

        ApexPages.currentPage().getParameters().put(Label.lbl_UrlKeyAccountId,account.id);

        ZuoraBillingAccount_CTRL ctrl = new ZuoraBillingAccount_CTRL(new ApexPages.StandardController(new Zuora__CustomerAccount__c()));
        
        ctrl.createZuoraAccountAndContact();

        Test.stopTest();
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 18/01/2016
     * @description Test method for CheckUniqueBillTo method
     */
     @isTest
     static void TestCheckUniqueBillTo(){
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }

        StateProvince__c accountProvince = Utils_TestMethods.createStateProvince(country.Id);
        insert accountProvince;
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;

        Zuora__CustomerAccount__c ba = testObjectCreator.CreateBillingAccount('BillAcc_1', 'Con_1', acc.Id);
        ba.Zuora__BillToCountry__c = 'United States';
        ba.Zuora__BillToState__c = 'Arizona';
        ba.Zuora__BillToId__c = '2c92c0fb550fa3c9015522d9a558486f';
        ba.Zuora__BillToName__c = 'first last re';
        ba.Zuora__BillToWorkEmail__c = 'email@example.com';
        ba.Zuora__BillToAddress1__c = 'street';
        ba.Zuora__BillToAddress2__c = 'block';
        ba.Zuora__BillToCity__c = 'city';
        ba.Zuora__BillToPostalCode__c = 'zip';
        ba.Zuora__Account__c = acc.Id;
        ba.Id = null;
        insert ba;


        Test.startTest();

        ApexPages.currentPage().getParameters().put(Label.lbl_UrlKeyAccountId,acc.id);

        ZuoraBillingAccount_CTRL ctrl = new ZuoraBillingAccount_CTRL(new ApexPages.StandardController(ba));
        /*
        ctrl.refAccountForBillToContact.Zuora__BillToCountry__c = 'United States';
        ctrl.refAccountForBillToContact.Zuora__BillToState__c = 'Arizona';
        ctrl.refAccountForBillToContact.Zuora__BillToId__c = '2c92c0fb550fa3c9015522d9a558486f';
        ctrl.refAccountForBillToContact.Zuora__BillToName__c = 'first last re';
        ctrl.refAccountForBillToContact.Zuora__BillToWorkEmail__c = 'email@example.com';
        ctrl.refAccountForBillToContact.Zuora__BillToAddress1__c = 'street';
        ctrl.refAccountForBillToContact.Zuora__BillToAddress2__c = 'block';
        ctrl.refAccountForBillToContact.Zuora__BillToCity__c = 'city';
        ctrl.refAccountForBillToContact.Zuora__BillToPostalCode__c = 'zip';
        */
        //ctrl.checkUniqueBillTo();

        Test.stopTest();
     }
}