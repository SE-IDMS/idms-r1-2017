@IsTest
public class PRM_MemberExternalPropertiesUtilsTest{
	static testMethod void testExternalProperties() {
		User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;
        System.runAs(us){ 
			Id recordTypeId = [select id from RecordType where DeveloperName ='ExternalProperties' and SobjectType='ExternalPropertiesCatalog__c'].Id;
			ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='BFO',Type__c='MigrationToProgramV2',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Properties');
	        insert epcExternalSystemType;
	        ExternalPropertiesCatalog__c myEPC =  new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,ExternalPropertiesCatalogType__c=epcExternalSystemType.Id,PropertyName__c='test prop',Subtype__c='testprop');
	        insert myEPC;


	        Country__c country = Utils_TestMethods.createCountry();
	        country.countrycode__c ='US';
	        insert country;

	        Account acc = Utils_TestMethods.createAccount();
	        acc.country__c = country.id;
	        Insert acc;

	        acc.isPartner = true;
	        acc.PRMAccount__c = True;
	        acc.PRMUIMSId__c = acc.Id;
	        update acc;

	        Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
	        contact.PRMContact__c = true;
	        contact.PRMUIMSId__c = acc.Id;
	        insert contact;

	        FieloEE.MockUpFactory.setCustomProperties(false);

	        FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
	        FieloEE__Member__c member = new FieloEE__Member__c();
	        member.FieloEE__LastName__c= 'TestAkram';
	        member.FieloEE__FirstName__c = 'Akram';
	        member.FieloEE__Street__c = 'test';
	        member.FieloEE__Program__c = prog.Id;
	        member.F_Account__c = acc.Id;
	        insert member;


	        contact.FieloEE__Member__c = member.Id;
	        update contact;	

	        Map<String ,Set<String>> badgesByContact = new Map<String ,Set<String>>();
	        badgesByContact.put(contact.PRMUIMSId__c,new Set<String>{'testprop'});
	        PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByPRMUIMId('BFO','MigrationToProgramV2', badgesByContact);
	        PRM_MemberExternalPropertiesUtils.removeMemberExternalPropertyByPRMUIMId('BFO','MigrationToProgramV2', badgesByContact);


	        Map<Id ,Set<String>> badgesByContactId = new Map<Id ,Set<String>>();
	        badgesByContact.put(contact.Id,new Set<String>{'testprop'});
	        PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactId);
	        PRM_MemberExternalPropertiesUtils.removeMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactId);
	    }
	}

	static testMethod void testExternalProperties2() {
		User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;
        System.runAs(us){ 
			Id recordTypeId = [select id from RecordType where DeveloperName ='ExternalProperties' and SobjectType='ExternalPropertiesCatalog__c'].Id;
			ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='BFO',Type__c='MigrationToProgramV2',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Properties');
	        insert epcExternalSystemType;
	        ExternalPropertiesCatalog__c myEPC =  new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,ExternalPropertiesCatalogType__c=epcExternalSystemType.Id,PropertyName__c='test prop',Subtype__c='testprop2');
	        insert myEPC;

	        Country__c country = Utils_TestMethods.createCountry();
	        country.countrycode__c ='US';
	        insert country;

	        Account acc = Utils_TestMethods.createAccount();
	        acc.country__c = country.id;
	        Insert acc;

	        acc.isPartner = true;
	        acc.PRMAccount__c = True;
	        acc.PRMUIMSId__c = acc.Id;
	        update acc;

	        Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
	        contact.PRMContact__c = true;
	        contact.PRMUIMSId__c = acc.Id;
	        insert contact;

	        FieloEE.MockUpFactory.setCustomProperties(false);

	        FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
	        FieloEE__Member__c member = new FieloEE__Member__c();
	        member.FieloEE__LastName__c= 'TestAkram2';
	        member.FieloEE__FirstName__c = 'Akram2';
	        member.FieloEE__Street__c = 'test2';
	        member.FieloEE__Program__c = prog.Id;
	        member.F_Account__c = acc.Id;
	        insert member;


	        contact.FieloEE__Member__c = member.Id;
	        update contact;	

	        Map<String ,Set<String>> badgesByContact = new Map<String ,Set<String>>();
	        badgesByContact.put(contact.PRMUIMSId__c,new Set<String>{'testprop2'});
	        PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByPRMUIMId('BFO','MigrationToProgramV2', badgesByContact);

	        Map<Id ,Set<String>> badgesByContactId = new Map<Id ,Set<String>>();
	        badgesByContact.put(contact.Id,new Set<String>{'testprop2'});
	        PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactId);

	        Map<Id, MemberExternalProperties__c> mbrProp = new Map<Id, MemberExternalProperties__c> ([SELECT Id, Member__c, PRMUIMSId__c, FunctionalKey__c, DeletionFlag__c FROM MemberExternalProperties__c WHERE Member__c = :member.Id]);
	        Map<Id, MemberExternalProperties__c> mbrPropOld = mbrProp.clone ();
	        if (mbrProp != null && mbrProp.size() > 0) {
	        	for (String key : mbrProp.keySet()) {
	        		MemberExternalProperties__c t = (MemberExternalProperties__c)mbrProp.get(key);
	        		t.DeletionFlag__c = true;
	        	}

	        	Test.startTest();
	        	System.enqueueJob ( new PRM_Queueable_MemberProperties (mbrPropOld, mbrProp));
	        	Test.stopTest();
	        }
	    }
	}
}