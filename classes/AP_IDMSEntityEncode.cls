Public Class AP_IDMSEntityEncode{
    
    private static final String[] HTML_DECODED  = new String[]{ '&', '<', '>', '"', '\''};
    private static final String[] HTML_ENCODED  = new String[]{ '&amp;', '&lt;', '&gt;', '&quot;', '&#39;' };

    public static String encode(String input){
        if (input == null || input.length() == 0)
            return input;

        for (Integer i = 0; i < HTML_DECODED.size(); ++i) {
            input = input.replace(HTML_DECODED[i], HTML_ENCODED[i]);
        }
        
        return input;
    }

}