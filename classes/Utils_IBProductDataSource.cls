public without sharing class Utils_IBProductDataSource extends Utils_DataSource{
    
 
     static String query1 = 'Select id, Name, brand2__r.Name, deviceType2__r.name, range__c, SKU__c, CategoryId__r.id, CategoryId__r.name  from Product2 where isactive = TRUE ';
          //    Main Search Method
     
     Static String WhereClause ='';
     public override List<SelectOption> getColumns()
     {
         List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Field1__c',sObjectType.Product2.fields.Name.getLabel())); // First Column
        Columns.add(new SelectOption('Field2__c',sObjectType.Product2.fields.Brand2__c.getLabel())); 
        Columns.add(new SelectOption('Field3__c',sObjectType.Product2.fields.DeviceType2__c.getLabel()));
        Columns.add(new SelectOption('Field4__c',sObjectType.Product2.fields.CategoryId__c.getLabel())); 
        //Columns.add(new SelectOption('Field5__c',sObjectType.Product2.fields.Type__c.getLabel())); 
        //Columns.add(new SelectOption('Field6__c',sObjectType.Product2.fields.SubType__c.getLabel())); 
        Columns.add(new SelectOption('Field5__c',sObjectType.Product2.fields.SKU__c.getLabel()));    
        
        return Columns;
        return null;
     }
     
     public override Result Search(List<String> keyWords, List<String> filters)
     {
        //    Initializing Variables      
        String searchText; 
        String brand;
        String deviceType;
        String range;
        String type;
        String sstype;
        String[] compositeKey;
        List<sObject> results = New List<sObject>();
        List<sObject> tempResults = New List<sObject>();
        Utils_Sorter sorter = new Utils_Sorter();
        List<Id> compId = New List<Id>();
        Utils_DataSource.Result returnedResult = new Utils_DataSource.Result();   
        returnedResult.recordList = new List<DataTemplate__c>();
        Map<Object, String> countries = New Map<Object, String>();
        Map<String,sObject> resultData = New Map<String,sObject>();
        Map<Object, String> pickList = New Map<Object, String>();
        Map<Integer,DataTemplate__c> resultMap = new Map<Integer,DataTemplate__c>();
        List<Object> countryId = New List<Object>();
        List<sObject> resultset = New List<sObject>();
         String final_query;
 
        try{
        if(!(filters[0].equalsIgnoreCase(Label.CL00355)))
                brand = filters[0];
        
        if(filters != null && filters.size()>1){       
          if(!(filters[1].equalsIgnoreCase(Label.CL00355)))
                // devicetype picklist is returning "brandId_devicetypeId"
                compositeKey = filters[1].split('_');
                devicetype = compositeKey[1];
        }
        
        if(filters != null && filters.size()>1){       
          if(!(filters[2].equalsIgnoreCase(Label.CL00355)))
                range = filters[2];
        }
        /*
        if(filters != null && filters.size()>1){       
          if(!(filters[3].equalsIgnoreCase(Label.CL00355)))
                type= filters[3];
        }
        
        if(filters != null && filters.size()>1){       
          if(!(filters[4].equalsIgnoreCase(Label.CL00355)))
                sstype= filters[4];
        }*/
            
        }catch(Exception e){
          System.debug('#### ERROR :' + e.getMessage());
        }    

        if(keywords != null && keywords.size()>0)
            searchText = keyWords[0];
        else 
            searchText = '';       
                 
        if(searchText != NULL && searchText!= ''){
            WhereClause +=' AND (Name LIKE \'%'+searchText+'%\' ';
            WhereClause +=' OR Brand2__r.Name LIKE \'%'+searchText+'%\' ';
            WhereClause +=' OR DeviceType2__r.Name LIKE \'%'+searchText+'%\' ';
            //WhereClause +=' OR Range2__c LIKE \'%'+searchText+'%\' ';
            WhereClause +=' OR CategoryId__r.Name LIKE \'%'+searchText+'%\' ';
            WhereClause +=' OR SKU__c LIKE \'%'+searchText+'%\' )';
        }
        
        if(brand != null && brand != ''){
            WhereClause +=' AND Brand2__c =  \''+brand+'\' ';  
        }
        
        if(deviceType != null && deviceType != ''){
            WhereClause +=' AND DeviceType2__c = \''+deviceType+'\' ';    
        }
        
        if(range != null && range != ''){
            WhereClause +=' AND CategoryId__c = \''+range+'\' ';
                
        }
        
        /*
        if(type!= null && type!= ''){
            WhereClause +=' AND Type__c = \''+type+'\' ';       
        }
       
        if(sstype!= null && sstype!= ''){
            WhereClause +=' AND SubType__c = \''+sstype+'\' ';      
        }*/
        
       WhereClause +=' LIMIT 101'; 
        
       final_query=query1 +WhereClause;
         System.debug('#### final query: ' +  final_query );
          Utils_SDF_Methodology.log(' query: ',final_query); 
         results = Database.query(final_query);
        
        System.debug('#### results: ' +  results );
        
        
        //    Start of processing the Saerch Result to prepare the final result set
        tempResults.clear();
        for (sObject obj : results)
        { 
            Product2 records = (Product2)obj;
            DataTemplate__c solcc = New DataTemplate__c ();
            solcc.put('Field1__c',records.Name);
            solcc.put('Field2__c',records.Brand2__r.Name);
            solcc.put('Field3__c',records.DeviceType2__r.Name);
            solcc.put('Field4__c',records.CategoryId__r.Name);
            //solcc.put('Field5__c',records.Type__c);
            //solcc.put('Field6__c',records.SubType__c);
            solcc.put('Field5__c',records.SKU__c);
            solcc.put('Field6__c',records.id);
            //solcc.put('Field4__c',records.Id);
            
            
            tempResults.add(solcc);              
        }
        returnedResult.recordList.clear();
        returnedResult.recordList.addAll(tempResults);
        
        //    End of Processing and preparation of the final result Set
        
        returnedResult.recordList = (List<sObject>)sorter.getSortedList(returnedResult.recordList, 'Field1__c', true);
        System.debug('#### Final Result :' + returnedResult);
        
        resultset.addAll(returnedResult.recordList);
        returnedResult.numberOfRecord = resultset.size();

        if(returnedResult.numberOfRecord > 100)
        {
            returnedResult.recordList.clear();
            for(Integer count=0; count < 100; count++)
            {
                returnedResult.recordList.add(resultset[count]);      
            }
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return returnedResult;
     }   
}