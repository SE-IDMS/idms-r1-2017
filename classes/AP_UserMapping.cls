public class AP_UserMapping {

    public Static List<User> userList = new List<User>();

    public static set<string> stateSet = new Set<String>();
    public static set<String> countrySet = new Set<String>();
    Public Static map<String,id > stateMap = new Map<String,id>();
    public Static map<String,id > countryMap = new Map<String,id>();
    
    Public Static Id accountOwnerId;
    Public Static ID BusinnesRecID;
    Public Static ID DigitalRecID;
    Public Static ID CustomerRecID;
    
    public Static void getRecordTypes() {
    
        // RecordType Selection start
        List<recordType>AccRecType =   [SELECT id,name,DeveloperName,SobjectType from recordType where (DeveloperName =: label.CLQ316IDMS532 OR DeveloperName =: label.CLQ316IDMS533) and (SobjectType = : Label.CLQ316IDMS534 OR SobjectType = : Label.CLQ316IDMS535) limit 10] ;        
        For(integer i =0; i< AccRecType.size();i++ ){
            if(AccRecType[i].DeveloperName == Label.CLQ316IDMS532 && AccRecType[i].SobjectType == Label.CLQ316IDMS534){
                BusinnesRecID = AccRecType[i].id;
            }
            else if(AccRecType[i].DeveloperName == Label.CLQ316IDMS532 && AccRecType[i].SobjectType == Label.CLQ316IDMS535){
                CustomerRecID = AccRecType[i].id;
            }
            else if(AccRecType[i].DeveloperName == Label.CLQ316IDMS533 && AccRecType[i].SobjectType == Label.CLQ316IDMS534){
                DigitalRecID= AccRecType[i].id;
            }
        }
    }
    
    public static void getAccountOwnerId() {
    
        // Account Owner selection Start
        map<id,Integer> userAccountCount = new Map<id,Integer>();
        
        for (AggregateResult ar : [Select ownerid, COUNT(id) from Account 
                                   where owner.profile.Name = :Label.CLQ316IDMS530
                                    Group By OwnerId ])  {
                                       
            userAccountCount.put((Id)ar.get('ownerid'), (Integer)ar.get('expr0'));                                   
        }
                                   
        for(user users : [Select id, name from user where profile.Name = :Label.CLQ316IDMS530 and
                            isActive = TRUE Limit 50]){
            if(!userAccountCount.containsKey(users.id)){
                userAccountCount.put(users.id, 0);
            }
        }
       
        for(id ids : userAccountCount.keySet()){
            if(userAccountCount.get(ids)<9800){
                accountOwnerId = ids;
                break;
            }
        }
    }
    
    public static void buildMaps(User[] users) {

        if(!users.isEmpty()) {
        
            for(integer i=0; i<users.size(); i++){
                
                if(!stateSet.contains(users[i].Company_Country__c+users[i].Company_State__c)){
                    stateSet.add(users[i].Company_Country__c+users[i].Company_State__c);
                }
                
                if(!countrySet.contains(users[i].Company_Country__c)){
                    countrySet.add(users[i].Company_Country__c);
                }
            }

            list<StateProvince__c> stateList = new List<StateProvince__c>();
            list<Country__c> countryList = new list<Country__c>();
            
            for(StateProvince__c states :[Select id, Name,StateProvinceCode__c  from StateProvince__c where StateProvinceExternalId__c =:stateSet limit 2000]){
                stateMap.put(states.StateProvinceCode__c , states.Id);
            }
        
            for(Country__c country: [Select Id, CountryCode__c, Name from Country__c where CountryCode__c =: countrySet LIMIT 2000]){
                countryMap.put(country.CountryCode__c, country.Id);
            }         
        }
    }

    public static Account buildBusinessAccount(User user) {

        Account l2TempAcc = new Account();
        
        if(BusinnesRecID == null){
            getRecordTypes();
        }
        
        if(accountOwnerId == null) {
            getAccountOwnerId();        
        }
        
        if(user.contactid == null && user.contact.accountid == null ){
            if(accountOwnerId != null){
                l2TempAcc.ownerid = accountOwnerId;    
            }                       
        }
        
        l2TempAcc.TECH_RunIDMSAccountDuplicateRule__c = True;
        l2TempAcc.CurrencyIsoCode = user.DefaultCurrencyIsoCode;
        l2TempAcc.Name = user.CompanyName;
        l2TempAcc.ClassLevel1__c = user.IDMSClassLevel1__c;
        l2TempAcc.ClassLevel2__c = user.IDMSClassLevel2__c;
        l2TempAcc.MarketServed__c = user.IDMSCompanyMarketServed__c;
        l2TempAcc.EmployeeSize__c = user.IDMSCompanyNbrEmployees__c;
        l2TempAcc.AnnualRevenue = user.IDMSAnnualRevenue__c;
        if(!System.isFuture()){
            l2TempAcc.VATNumber__c = user.IDMSTaxIdentificationNumber__c;
        }
        l2TempAcc.Street__c               = user.Company_Address1__c;
        l2TempAcc.AdditionalAddress__c    = user.IDMS_AdditionalAddress__c;
        l2TempAcc.City__c                 = user.Company_City__c;
        l2TempAcc.ZipCode__c              = user.Company_Postal_Code__c;
        l2TempAcc.County__c               = user.IDMSCompanyCounty__c;
        l2TempAcc.POBox__c                = user.IDMSCompanyPoBox__c;
        
        l2TempAcc.Website = user.Company_Website__c;
        l2TempAcc.recordtypeId = BusinnesRecID;
                
        if(user.Company_State__c!= null){
            
            if(stateMap.get(user.Company_State__c)!= null){
                l2TempAcc.StateProvince__c = stateMap.get(user.Company_State__c);
            }            
        }
        
        if(user.Company_Country__c != null) {                
            l2TempAcc.Country__c = countryMap.get(user.Company_Country__c);
        }
        
        return l2TempAcc;
    }

    public static Account buildDigitalAccount(User user) {

        
        if(BusinnesRecID == null){
            getRecordTypes();
        }  
        
        if(accountOwnerId == null) {
            getAccountOwnerId();        
        }
        
        Account l1TempAcc = new Account();
              
        
        if(user.contactid == null && user.contact.accountid == null ){
            if(accountOwnerId != null){
                l1TempAcc.ownerid = accountOwnerId;    
            }                       
        }
        l1TempAcc.Name = Label.CLQ316IDMS531;
        l1TempAcc.recordTypeId = DigitalRecID;        
        l1TempAcc.TECH_RunIDMSAccountDuplicateRule__c = false;
        
        return l1TempAcc;

    }
        
    public static Account[] buildAccounts(User[] users) {

        buildMaps(users);        

        if(accountOwnerId == null) {
            getAccountOwnerId();        
        }
        
        if(BusinnesRecID == null){
            getRecordTypes();
        }
    
        Account[] accountArray = new Account[users.size()];
        
        if(!users.isEmpty()) {
        
            for(Integer i = 0; i<users.size();i++) {
                
                if(AP_MatchingModule.isLevel2(users[i])) {                
                    accountArray[i] = buildBusinessAccount(users[i]);
                }
                else {
                    accountArray[i] = buildDigitalAccount(users[i]);                
                }
            }
        
        }
        
        return accountArray;
    } 
      
    public static Contact[] buildContacts(User[] users) {
    
        Contact[] contactArray = new Contact[users.size()];
        
        if(countryMap.keySet().isEmpty()) {
            buildMaps(users);        
        }
        
        if(BusinnesRecID == null || CustomerRecID == null){
            getRecordTypes();
        }
        
        if(accountOwnerId == null) {
            getAccountOwnerId();        
        }
        
        Account[] accountArray = new Account[users.size()];
        
        if(!users.isEmpty()) {
        
            for(Integer i = 0; i<users.size();i++) {
                if(AP_MatchingModule.isLevel2(users[i])) {
                    contactArray[i] = builContact(users[i]);
                }
                else {
                    contactArray[i] = builEmptyContact(users[i]);                
                }
            }
        
        }

        return contactArray;
    }

    public static Contact builEmptyContact(User user) {
        
        if(BusinnesRecID == null || CustomerRecID == null){
            getRecordTypes();
        }
        
        if(accountOwnerId == null) {
            getAccountOwnerId();        
        }
        
        Contact l1TempCon = new Contact();

        l1TempCon.FirstName = user.FirstName;
        l1TempCon.LastName = user.LastName;
        if(user.contactid == null && user.contact.accountid == null ){
            if(accountOwnerId != null){
                l1TempCon.ownerid = accountOwnerId;    
            }                       
        }
        if(user.contactid != null && user.contact.accountid != null){
            l1TempCon.AccountId = user.contact.accountid;
        }
        
        l1TempCon.TECH_RunIDMSContactDuplicateRule__c = false;

        l1TempCon.Email              = null;
        l1TempCon.MobilePhone        = null;        
       
        l1TempCon.MarcomPrefEmail__c = user.IDMS_Email_opt_in__c;
                
        return l1TempCon;
    }

    public static Contact builContact(User user) {

        if(BusinnesRecID == null || CustomerRecID == null){
            getRecordTypes();
        }
        
        if(accountOwnerId == null) {
            getAccountOwnerId();        
        }
        
        Contact l2TempCon = new Contact();
        
        if(user.contactid == null && user.contact.accountid == null ){
            if(accountOwnerId != null){
                l2TempCon.ownerid = accountOwnerId;    
            }                       
        }
        l2TempCon.TECH_RunIDMSContactDuplicateRule__c = True;
        l2TempCon.Email = user.Email;
        l2TempCon.MobilePhone = user.MobilePhone;
        l2TempCon.WorkPhone__c = user.Phone;
        l2TempCon.FirstName = user.FirstName;
        l2TempCon.LastName = user.LastName;
    
        if(user.Company_Country__c!= null){
            
            if(countryMap.get(user.Company_Country__c) != null){
                l2TempCon.Country__c = countryMap.get(user.Company_Country__c);
            }
        }    
            
        l2TempCon.CorrespLang__c     = user.IDMS_PreferredLanguage__c;
        l2TempCon.CurrencyIsoCode    = user.DefaultCurrencyIsoCode;
        
        if(user.contactid != null && user.contact.accountid != null){
            l2TempCon.AccountId = user.contact.accountid;
        }
        
        l2TempCon.MidInit__c = user.IDMSMiddleName__c;
        l2TempCon.Salutation = user.IDMSSalutation__c;
        l2TempCon.Department = user.Department;
        l2TempCon.Fax = user.Fax;
        l2TempCon.RecordTypeId = CustomerRecID;
        l2TempCon.MarcomPrefEmail__c = user.IDMS_Email_opt_in__c;
        
        return l2TempCon;
    }
    

}