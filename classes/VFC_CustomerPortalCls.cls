//public without sharing class VFC_CustomerPortalCls {
public with sharing class VFC_CustomerPortalCls {
   
public class ticket
    {
      public Id id  {get; set;}
      public String CaseNumber  {get; set;}
      public String Subject     {get; set;}
      public String Status      {get; set;}
      public String CreatedDate {get; set;}
      public String ClosedDate  {get; set;}
      public Boolean HasUnreadComment    {get; set;}

    }
    
List <ticket> tickets = new List <ticket> ();

//public void setTickets (List<ticket> tik){this.tickets = tik;}

public List<ticket> setTicket (List<case> cases){
        
         List<ticket> tick = new List<ticket>();
         if (cases != null){
                     for (Case c : cases)
                             {
                              ticket tt = new ticket();
                              tt.id = c.id;
                              tt.CaseNumber  = c.CaseNumber;
                              tt.Subject     = c.Subject;
                              tt.Status = c.status;
                              tt.CreatedDate = c.CreatedDate.format();
                              tt.HasUnreadComment = c.HasCommentsUnreadByOwner;  
                              if (c.ClosedDate != null)
                              {
                              	if (c.status != null && c.status.equalsIgnoreCase('In Progress')) 
                              	{
                              		tt.ClosedDate = null;
                              	}
                                else 
                                {
                                	tt.ClosedDate = c.ClosedDate.format();
                                }
                              }
                              else
                              {
                                if (c.status != null && c.status.equalsIgnoreCase('Closed')) 
                              	{
                              		tt.ClosedDate = c.CreatedDate.format();
                              	}	
                              }
                              tick.add(tt);          
                      }        
         }return tick;
}  

Id usid;// {get;set;}
Id contid {get;set;}
Id accid {get;set;} 
List<Case> results;

public boolean getIsAuthenticated ()
{
  return (IsAuthenticated());
}

public boolean IsAuthenticated ()
{
  return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
}

String selectedPcasesview {get; set;}
public String getselectedPcasesview () {return selectedPcasesview ;}
public void setselectedPcasesview (String pt) {this.selectedPcasesview = pt;}

public Id getusid(){return this.usid;}
public void setusid(Id id){this.usid = id;}

public ID getcontid(){return this.contid;}
public void setcontid(Id id){this.contid = id;}

public ID getaccid (){return this.accid ;}
public void setaccid (Id id){this.accid  = id;}


//String searchText;
//boolean isSearch = false;

//public String getpcasesview (){return this.pcasesview ;}
public void setpcasesview (String s){this.pcasesview  = s;}
public List<SelectOption> getpcasesview ()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('AMC','All My Cases'));        
        options.add(new SelectOption('MOC','My Open Cases'));
        options.add(new SelectOption('MCC','My Closed Cases'));
        return options;
    }
 
public String pcasesview;// {get;set;}
        
        List<Case> casess;
                    
        //public Boolean clickedButton {get;set;}
         
        //Constructor
        public VFC_CustomerPortalCls() 
        {                                                   
          if (UserInfo.getUserType().equalsIgnoreCase('Guest'))
          {
          	
          }
          else
          {
             usid = UserInfo.getUserId();
             
             if (usid != null)
             {
                  contid = [Select ContactId from User where Id =: usid LIMIT 100][0].ContactId;
             
                  if ( contid != null)
                  {
                    accid = [Select AccountId from Contact where Id =: contid LIMIT 100][0].AccountId;
                  }
             }
          }
                      
          tickets = new List <ticket> ();
         }
         
        
         
//================SAERCH TEXT============================
/*
public String getSearchText(){
    return searchText;
    }
public void setSearchText(String s){
        searchText = s;
    }

public pageReference doSearch(){
    
    isSearch = true;    
    if(searchText != null){
    casess = [SELECT CaseNumber,Subject,Status,CreatedDate FROM Case WHERE caseNumber =: searchText LIMIT 1];
    }
    
    return null;
    }
    */

//==================LIST OF CASES====================
    
public ApexPages.StandardSetController setCon {      
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
[SELECT Status, Id, CaseNumber, Subject, Origin, CustomerRequest__c, CreatedDate, IsClosed, closedDate, HasCommentsUnreadByOwner, ActionNeededFrom__c FROM Case WHERE ContactId =: contid ORDER BY CreatedDate DESC LIMIT 100 ])); // AND CreatedDate >= LAST_N_DAYS:60 

}
            return setCon;
        }
        set;
    }
         
         
public pageReference casesList()
{
    //getCasess();
    getTickets();
    return null;
} 

// Initialize setCon and return a list of records
public void setCasess(List<case> lc){this.casess = lc;}   
    
public List<Case> getCasess() {return null;}

public List<ticket> getTickets() {

          casess = setCon.getRecords();
          List<Case> closedCases = new List<Case>();
            List<Case> newCases = new List<Case>();
            List<Case> openCases = new List<Case>();
            List<Case> waitingCases = new List<Case>();
            List<Case> inProgressCases = new List<Case>();
            List<Case> orderedCases = new List<Case>();
        
          ticket tik = new ticket ();
          List <ticket> OrderedTickets = new List <ticket>();
          /*List <ticket> TicketsWaiting    = new List <ticket>();
          List <ticket> TicketsClosed     = new List <ticket>();*/
          List <ticket> openTickets        = new List <ticket>();
          List <ticket> closedTickets = new List <ticket>();
    

    if (casess != null) 
    {
        for (Case c : casess)
        {                     
          if      (c.status.equalsIgnoreCase('New'))       {c.status = 'New';}
          else if (c.status.equalsIgnoreCase('Cancelled')) {c.status = 'Closed';}
          else if (c.status.equalsIgnoreCase('Closed'))    {c.status = 'Closed';}
          else if (c.ActionNeededFrom__c != null && c.ActionNeededFrom__c.equalsIgnoreCase('Customer')) {c.status = 'Waiting for Customer';} 
          else c.status = 'In Progress'; 
        }

         for (Case c : casess)
        {
             if      (c.status == 'Closed')                {closedCases.add(c);}
             else if (c.status == 'New')                   {newCases.add(c);}
             else if (c.status == 'Waiting for Customer' ) {waitingCases.add(c);}
             else                                          {inProgressCases.add(c);}
        }        
        
          if (waitingCases != null) { for (case t : waitingCases)       {orderedCases.add (t); openCases.add(t); }}
          if (newCases != null) { for (case t : newCases)               {orderedCases.add (t);  openCases.add(t);} }
          if (inProgressCases != null) { for (case t : inProgressCases) {orderedCases.add (t);  openCases.add(t);}}
          if (closedCases != null) { for (case t : closedCases)         {orderedCases.add (t);                   }}             

          if (selectedpcasesview == 'MOC') {openTickets = setTicket(openCases); return openTickets ;}
          else if (selectedpcasesview == 'MCC') {closedTickets = setTicket(closedCases); return closedTickets ;}
          else {OrderedTickets = setTicket(orderedCases); return OrderedTickets;}

    }
          
        return null;

    }
}