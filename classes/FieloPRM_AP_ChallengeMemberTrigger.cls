/********************************************************************
* Company: Fielo
* Created Date: 02/08/2016
* Description: Trigger class for object Challenge Member
********************************************************************/

public with sharing class FieloPRM_AP_ChallengeMemberTrigger{
    
    public static final String STATUS_UNSUBSCRIBE = 'Unsubscribe';
    
    public static void demotionChallengeReward(){
        
        List<FieloCH__ChallengeMember__c> listChallengeMemberToProcess = new List<FieloCH__ChallengeMember__c>();
        set<Id> setChallengeRewardIds = new set<Id>();
        
        for(FieloCH__ChallengeMember__c chm: (List<FieloCH__ChallengeMember__c>)trigger.new){
            if(chm.FieloCH__ChallengeReward__c != null && ((FieloCH__ChallengeMember__c)trigger.oldMap.get(chm.Id)).FieloCH__ChallengeReward__c == null){
                listChallengeMemberToProcess.add(chm);
                setChallengeRewardIds.add(chm.FieloCH__ChallengeReward__c);
            }
        }
        
        if(!setChallengeRewardIds.isEmpty()){
            map<Id, FieloCH__ChallengeReward__c> mapChallengeReward = new map<Id, FieloCH__ChallengeReward__c>([SELECT Id FROM FieloCH__ChallengeReward__c WHERE Id IN: setChallengeRewardIds AND F_PRM_ExpirationNumber__c != NULL]);
            
            List<FieloCH__ChallengeMember__c> listChallengeMemberToUpdate = new List<FieloCH__ChallengeMember__c>();
            for(FieloCH__ChallengeMember__c chm: listChallengeMemberToProcess){
                if(mapChallengeReward.containsKey(chm.FieloCH__ChallengeReward__c)){
                    listChallengeMemberToUpdate.add(new FieloCH__ChallengeMember__c(
                        Id = chm.Id,
                        FieloCH__ChallengeReward__c = null
                    ));
                }
            }
            if(!listChallengeMemberToUpdate.isEmpty()){
                update listChallengeMemberToUpdate;
            }
        }
    }
    
    /*Description: Inactivates the badge accounts from the current level*/
    public static void processUnsubscribeMember(){
    
        List<FieloCH__ChallengeMember__c> triggerNew = [SELECT FieloCH__ChallengeReward__c, FieloCH__ChallengeReward__r.FieloCH__Badge__c, FieloCH__Status__c FROM FieloCH__ChallengeMember__c WHERE Id IN: Trigger.New AND FieloCH__Challenge2__r.FieloCH__Mode__c = 'Teams'];
        Map<Id,FieloCH__ChallengeMember__c> triggerOldMap = (Map<Id,FieloCH__ChallengeMember__c>)Trigger.OldMap;
        List<FieloCH__ChallengeMember__c> teamsUnsubscribed = new List<FieloCH__ChallengeMember__c>(); 
        
        //get unsubscribed teams challenge
        for(FieloCH__ChallengeMember__c teamChallengeMember: triggerNew){
            if(teamChallengeMember.FieloCH__Status__c == STATUS_UNSUBSCRIBE && 
                triggerOldMap.get(teamChallengeMember.Id).FieloCH__Status__c != STATUS_UNSUBSCRIBE){
                teamsUnsubscribed.add(teamChallengeMember);
            }
        }
        
        List<Id> badgesIds = new List<Id>();
        if(!teamsUnsubscribed.isEmpty()){
            for(FieloCH__ChallengeMember__c teamChallengeMember: teamsUnsubscribed){
                if(teamChallengeMember.FieloCH__ChallengeReward__r.FieloCH__Badge__c != null){
                    if(teamChallengeMember.FieloCH__ChallengeReward__r.FieloCH__Badge__c != null){
                        badgesIds.add(teamChallengeMember.FieloCH__ChallengeReward__r.FieloCH__Badge__c);
                    }
                }
            }
            List<FieloPRM_BadgeAccount__c> badgeAccounts = [SELECT Id FROM FieloPRM_BadgeAccount__c WHERE F_PRM_Badge__c IN:badgesIds];
            for(FieloPRM_BadgeAccount__c badgeAccount : badgeAccounts){
                badgeAccount.F_PRM_IsCurrentLevel__c = false;
            }

            update badgeAccounts;
        }
    }

}