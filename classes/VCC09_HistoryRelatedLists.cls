/*
    Author          : Accenture Team
    Date Created    : 28/07/2011 
    Description     : Controller for C09_HistoryRelatedLists.This class displays the 
                      History related lists for the corresponding sObject passed.
*/
public class VCC09_HistoryRelatedLists 
{    
    // Variable Declaration
    public SObject sObjType {get; set;}
    public Integer recordLimit {get; set;}
    public String objectLabel{get; set;}
    public objectHistoryLine[] objectHistory; 
    public static final Map<String, Schema.sObjectType> sObjTypeMap = Schema.getGlobalDescribe();
    public static Map<String, Schema.SObjectField> sObjTypeFieldMap;
    public static List<Schema.PicklistEntry> historyFieldPicklistValues;
    public list<sObject> historyList;
    public Map<String, String> keyPrefixes = new Map<String, String>();        
    public Map<String, String> translationValues = new Map<string, String>();
    public set<String> picklistFields = new set<String>();
    String oldValue,newValue;
    /* This method gets the name of the sObject from the component and makes a describe call for the 
     * fields related of the object.
     */
    public List<objectHistoryLine> getObjectHistory()
    {
        Id sObjTypeId = String.valueOf(sObjType.get('Id'));
        
        //Getting the Details related to the passed sobject Type
        Schema.DescribeSObjectResult objectDescription = sObjType.getsObjectType().getDescribe();

        //Getting the field details for the corresponding sObject
        sObjTypeFieldMap = objectDescription.fields.getMap();
        
        //Gets the Object label for the sObject
        objectLabel = String.valueOf(objectDescription.getLabel());
        
        //Get the name of the history table
        String objectHistoryTableName = objectDescription.getName();
        
        //if we have a custom object we need to drop the 'c' off the end before adding 'History' to get the history tables name
        if (objectDescription.isCustom())
        {
            if(objectHistoryTableName!=null)
                objectHistoryTableName = objectHistoryTableName.substring(0, objectHistoryTableName.length()-1);
        }
        
        //Appending the History next to the table name
        objectHistoryTableName = objectHistoryTableName + Label.CL00535;
        
        Schema.DescribeFieldResult objectHistoryFieldField = sObjTypeMap.get(objectHistoryTableName).getDescribe().fields.getMap().get('Field').getDescribe();
        historyFieldPicklistValues = objectHistoryFieldField.getPickListValues();
                    
        list<objectHistoryLine> objectHistory = new list<objectHistoryLine>();
        
        String prevDate = '';
        String parentId;
        
        //Checks if the sobject type is Lead, if so assigns the LeadId as the parent Id
        if(objectDescription.getkeyPrefix() == sObjectType.Lead.getKeyPrefix())
            parentId = 'LeadId';
        else
            parentId = 'parentId';
            
        //Checks if the record limit has a value
        if (recordLimit== null)
        {
            recordLimit = 100;
        }
        
        //As the number of records retrieved is 100, DML rows are limited to 100
        if(Limits.getDMLRows()+100 > Limits.getLimitDMLRows())
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CL00173));            
        else
        {       
            //Queries the history table
            historyList = Database.query( 'SELECT CreatedDate,CreatedById,Field,NewValue,OldValue FROM ' + objectHistoryTableName + ' ' +
                                                    'WHERE '+parentId +'=\'' + sObjTypeId + '\' ' +
                                                    'ORDER BY CreatedDate DESC LIMIT ' + String.valueOf(recordLimit));
        }
        
        //Fetches the Keyprefix of the object associated with the passed sobject. This is done to filter the ID entry in the 
        //sobject.
        if(!historyList.isEmpty())
        {
            for(String fieldName: sObjTypeFieldMap.keySet())
            {
                Schema.DescribeFieldResult fieldResult = sObjTypeFieldMap.get(fieldName).getDescribe();
                List<Schema.sObjectType> fieldReference = fieldResult.getReferenceTo();
                keyPrefixes.put(Label.CL00615,User.getSobjectType().getDescribe().getKeyPrefix());
                if(!fieldReference.isEmpty() && fieldReference.size()>0)
                {
                    if(fieldResult.getName()=='OwnerId')
                        keyPrefixes.put(fieldResult.getName().substring(0,fieldResult.getName().length()-2),User.getSobjectType().getDescribe().getKeyPrefix());
                    else
                        keyPrefixes.put(fieldResult.getName(),fieldReference[0].getDescribe().getKeyPrefix());                        
                }
                if(fieldResult.getType() == Schema.DisplayType.PICKLIST || fieldResult.getType() == Schema.DisplayType.MULTIPICKLIST)
                {
                    picklistFields.add(fieldResult.getName());
                    List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
                    for(Schema.PicklistEntry pickEntry:picklistEntries)
                    {
                        translationValues.put(fieldResult.getName()+pickEntry.getValue(), pickEntry.getLabel());
                    }
                }
            }
        }
        for(Sobject historyLine:historyList)
        {
            if (!((keyPrefixes.containsKey(String.valueOf(historyLine.get('Field'))))  
                &&(((historyLine.get('newValue')!=null && keyPrefixes.get(String.valueOf(historyLine.get('Field')))!=null && String.valueOf(historyLine.get('newValue')).length()==18) 
                && (String.valueOf(historyLine.get('newValue')).startsWith(keyPrefixes.get(String.valueOf(historyLine.get('Field')))))) 
                || (((historyLine.get('oldValue')!=null && keyPrefixes.get(String.valueOf(historyLine.get('Field')))!=null  && String.valueOf(historyLine.get('oldValue')).length()==18) 
                && (String.valueOf(historyLine.get('oldValue')).startsWith(keyPrefixes.get(String.valueOf(historyLine.get('Field'))))))))))
            {
                    objectHistoryLine tempHistory = new objectHistoryLine();
                                
                    // Set the Date and who performed the action
                    if (String.valueOf(historyLine.get('CreatedDate')) != prevDate)
                    {
                        tempHistory.theDate = String.valueOf(historyLine.get('CreatedDate'));
                        tempHistory.userId = String.valueOf(historyLine.get('CreatedById'));
                        tempHistory.who = String.valueOf(historyLine.get('CreatedById'));
                    }
                    else
                    {
                        tempHistory.theDate = '';
                        tempHistory.who = '';
                        tempHistory.userId = String.valueOf(historyLine.get('CreatedById'));
                    }
                    prevDate = String.valueOf(historyLine.get('CreatedDate'));
                    
                    //This gets the translated value of picklist field
                    if(picklistFields.contains(String.valueOf(historyLine.get('Field'))))
                    {
                        oldValue = translationValues.get(String.valueOf(historyLine.get('Field'))+historyLine.get('oldValue'));
                        newValue = translationValues.get(String.valueOf(historyLine.get('Field'))+historyLine.get('newValue'));                        
                    }
                    else
                    {
                        oldValue = String.valueOf(historyLine.get('oldValue'));
                        newValue = String.valueOf(historyLine.get('newValue'));
                    }
                    // Get the field label
                    String fieldLabel = VCC09_HistoryRelatedLists.returnFieldLabel(String.valueOf(historyLine.get('Field')));
                    
                    // Set the Action value on the record creation
                    if (String.valueOf(historyLine.get('Field')).EqualsIgnoreCase('created')) 
                    {    
                         tempHistory.action = ((String.valueOf(historyLine.get('Field')).substring(0,1)).ToUpperCase()) +String.valueOf(historyLine.get('Field')).substring(1,String.valueOf(historyLine.get('Field')).length())+'.';
                    }
                    else if (historyLine.get('oldValue') != null && historyLine.get('newValue') == null)
                    {    
                        // when deleting a value from a field                                                
                        tempHistory.action = Label.CL00233 + ' <b> '+oldValue + ' </b> '+Label.CL00234+'<b> ' + fieldLabel + '</b>.';                          
                    }
                    else if (historyLine.get('oldValue') == null && historyLine.get('newValue') != null)
                    {   
                        // When a new value is added
                        tempHistory.action = ((Label.CL00126.substring(0,1)).ToUpperCase()) +Label.CL00126.substring(1,Label.CL00126.length()) +'<b> ' + fieldLabel + '</b> ' +Label.CL00128.toLowerCase()+' <b> '+ newValue+'</b>';                        
                    }
                    else
                    {  
                        //all other scenarios
                        String fromText = '';
                        String toText = '';
                        if (historyLine.get('oldValue') != null) 
                        {
                             fromText =  ' '+Label.CL00127.toLowerCase() + ' <b> '+oldValue+'</b>';                         
                             toText = ' '+ Label.CL00128.toLowerCase()+ ' <b> '+newValue+'</b>';                                                     
                        }                        

                        if (toText != '')
                        {
                            tempHistory.action =((Label.CL00126.substring(0,1)).ToUpperCase())+Label.CL00126.substring(1,Label.CL00126.length()) +' <b>' + fieldLabel + '</b>' + fromText + toText ;
                        }
                        else 
                        {
                            tempHistory.action = ((Label.CL00126.substring(0,1)).ToUpperCase())+Label.CL00126.substring(1,Label.CL00126.length()) +' <b>' + fieldLabel + '</b>';
                        }
                    }                       
                    // Add to the list
                    objectHistory.add(tempHistory);
            }
        }
         
        List<Id> userIdList = new List<Id>();
        for (objectHistoryLine myHistory : objectHistory)
        {
             userIdList.add(myHistory.userId);
        }
        Map<Id, User> userIdMap = new Map<ID, User>([SELECT Name FROM User WHERE Id IN : userIdList]);
         
        for (objectHistoryLine myHistory : objectHistory)
        {
            if (userIdMap.containsKey(myHistory.userId) & (myHistory.who != ''))
            {
                myHistory.who = userIdMap.get(myHistory.who).Name;
            }
        }
        return objectHistory;
    }    
    
    // Function to return Field Label of a object field given a Field API name
    public Static String returnFieldLabel(String fieldName)
    {

        if (VCC09_HistoryRelatedLists.sObjTypeFieldMap.containsKey(fieldName))
        {
            return VCC09_HistoryRelatedLists.sObjTypeFieldMap.get(fieldName).getDescribe().getLabel();
        }
        else 
        {
            for(Schema.PicklistEntry pickList : historyFieldPicklistValues)
            {
                if (pickList.getValue() == fieldName)
                {
                    if (pickList.getLabel() != null)
                    {
                        return pickList.getLabel();
                    }
                    else 
                    {
                        return pickList.getValue();
                    }
                }
            }
        }
        return '';
    }
    
    // Inner Class to store the detail of the object history lines    
    public class objectHistoryLine 
    {
        public String theDate {get; set;}
        public String who {get; set;}
        public Id userId {get; set;} 
        public String action {get; set;}
    }
}