public with sharing class ServiceMaxUtilsPartsOrder  {
	
	@future(callout=true)
	public static void updateGMRValidation(Set<Id> poLineIdSet)
	{
		Utils_DataSource dataSource = Utils_Factory.getDataSource('GMR');
		List<SVMXC__RMA_Shipment_Line__c> lineList = [SELECT Id, CommercialReference__c, GMRValidation__c FROM SVMXC__RMA_Shipment_Line__c WHERE Id IN :poLineIdSet];
		
		for (SVMXC__RMA_Shipment_Line__c line : lineList)
		{
			system.debug(line);
			List<String> keywords = new List<String>();
            keywords.addAll(line.CommercialReference__c.split('\\s'));
            try
            {
                Utils_DataSource.Result searchResult = null;
                
                if(!Test.isRunningTest())
                {
                    searchResult = dataSource.Search(keywords,new List<String>());
                }
                else
                {
                    Utils_DummyDataForTest testDataSource = new Utils_DummyDataForTest();
                    searchResult = testDataSource.Search(keywords,new List<String>());
                }
                    
                system.debug(searchResult);
                if(searchResult.NumberOfRecord == 0)
                {
                    line.GMRValidation__c = 'No Match';
                }
                else if (searchResult.NumberOfRecord == 1)
                {
                    line.GMRValidation__c = 'Exact Match';
                }
                else
                {
                    line.GMRValidation__c = 'Multiple Results Returned';
                }

            }
            catch(Exception exc)
            {
                system.debug(exc);
                line.GMRValidation__c = 'No Match';
            }
		}
		
		update lineList;
	}

}