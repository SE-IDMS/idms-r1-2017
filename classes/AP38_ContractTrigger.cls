public without sharing class AP38_ContractTrigger {

  public static Opportunity createNewContractOpportunity(Contract contract, Account account){
     DateTime dT = System.now();
     Opportunity opportunity = new Opportunity();
     // Project Name - Opportunity Scope - End User - Location - Account Name 
     opportunity.Name= contract.Name+' - '+contract.OpportunityScope__c+' - '+account.Name+' - '+dT.year();
     opportunity.OwnerId = contract.OwnerId;
     opportunity.CurrencyIsoCode = contract.CurrencyIsoCode;
     opportunity.AccountId = contract.AccountId;
     opportunity.Amount = contract.SupportAmount__c;
     opportunity.CloseDate = contract.EndDate;
     opportunity.CountryOfDestination__c = account.Country__c;
     opportunity.IncludedinForecast__c = 'Yes';
     opportunity.LeadingBusiness__c = contract.LeadingBusinessBU__c;
     opportunity.OpportunityCategory__c = Label.CL00548; //'Simple';
     opportunity.OpportunityScope__c = contract.OpportunityScope__c;
     opportunity.OpptyType__c = contract.OpportunityType__c;
     opportunity.StageName = Label.CL00146; //'3 - Identify & Qualify';
     opportunity.MarketSegment__c = contract.MarketSegment__c;
     opportunity.MarketSubSegment__c = contract.MarketSubSegment__c;
     opportunity.OpportunitySource__c = 'Service Contract';
     opportunity.SourceReference__c = contract.ContractNumber;
     opportunity.OpptyPriorityLevel__c = Label.CL00241; //'Standard';
     opportunity.Probability = 60;
     opportunity.ChannelType__c = contract.ChannelType__c;
     opportunity.BusinessMix__c = contract.BusinessMix__c;
     
     return opportunity;
  }
  public static Opportunity createNewServiceContractOpportunity(SVMXC__Service_Contract__c  contract, Account account){
        Map<String, CS_SerContOppMapping__c> scopmap = new Map<String, CS_SerContOppMapping__c>();
        scopmap = CS_SerContOppMapping__c.getAll();
        
        Sobject sob = new Opportunity();
        Sobject scont = (Sobject)contract;
        Sobject sAcc = (Sobject)account;
        DateTime dT = System.now();
        
        for(String stkey:scopmap.keySet()){
            
            CS_SerContOppMapping__c mc = scopmap.get(stkey);
            String st=mc.SourceFieldApi__c;
            List<String> stlist =st.split(';');
            if(stlist!= null && stlist.size()>0)
            {
                for(String s:stlist){
                    String fieldapi='';
                    String stvalue='';
                    if(s.startsWith('SVMXC__Service_Contract__c.')){
                       fieldapi=s.removeStart('SVMXC__Service_Contract__c.');
                       if(sob.get(mc.TargetFieldApi__c) == null)
                        {
                            if(scont.get(fieldapi) != null )
                            sob.put(mc.TargetFieldApi__c , scont.get(fieldapi) );
                        }
                       else{
                            sob.put(mc.TargetFieldApi__c ,sob.get(mc.TargetFieldApi__c) +' - '+scont.get(fieldapi));
                        }
                    }
                    else if(s.startsWith('Account.')){
                        fieldapi=s.removeStart('Account.');
                        if(sob.get(mc.TargetFieldApi__c) == null)
                        {
                            if(sAcc.get(fieldapi) != null ){
                                sob.put(mc.TargetFieldApi__c , sAcc.get(fieldapi) );
                            }
                        }
                        else{
                            sob.put(mc.TargetFieldApi__c ,sob.get(mc.TargetFieldApi__c) +' - '+sAcc.get(fieldapi));
                        }
                    }
                    else {
                        stvalue = s;
                        if(mc.Type__c == 'String')
                        sob.put(mc.TargetFieldApi__c , stvalue );
                        if(mc.Type__c == 'Integer')
                        sob.put(mc.TargetFieldApi__c , Integer.valueOf(stvalue) );
                        
                    }
                    if(fieldapi.length()>0)
                        System.debug('******'+fieldapi);
                            else{System.debug('***field value ***'+stvalue);}
                }
            }       
        
        }
        
        Opportunity opp = (Opportunity)sob; 
        opp.name = opp.name+' - '+dT.year();
        System.debug('*****'+opp );
        return opp;
  }
   public static OpportunityNotification__c createOpportunityNotification(SVMXC__Service_Contract__c  contract, Account account){
        Map<String, CS_SerContOpptyNotifMapping__c> scopmap = new Map<String, CS_SerContOpptyNotifMapping__c>();
        scopmap = CS_SerContOpptyNotifMapping__c.getAll();
        
        Sobject sob = new OpportunityNotification__c();
        Sobject scont = (Sobject)contract;
        Sobject sAcc = (Sobject)account;
        DateTime dT = System.now();
        
        for(String stkey:scopmap.keySet()){
            
            CS_SerContOpptyNotifMapping__c mc = scopmap.get(stkey);
            String st=mc.SourceFieldApi__c;
            List<String> stlist =st.split(';');
            if(stlist!= null && stlist.size()>0)
            {
                for(String s:stlist){
                    String fieldapi='';
                    String stvalue='';
                    if(s.startsWith('SVMXC__Service_Contract__c.')){
                       fieldapi=s.removeStart('SVMXC__Service_Contract__c.');
                       if(sob.get(mc.TargetFieldApi__c) == null)
                        {
                            if(scont.get(fieldapi) != null )
                            sob.put(mc.TargetFieldApi__c , scont.get(fieldapi) );
                        }
                       else{
                            sob.put(mc.TargetFieldApi__c ,sob.get(mc.TargetFieldApi__c) +' - '+scont.get(fieldapi));
                        }
                    }
                    else if(s.startsWith('Account.')){
                        fieldapi=s.removeStart('Account.');
                        if(sob.get(mc.TargetFieldApi__c) == null)
                        {
                            if(sAcc.get(fieldapi) != null ){
                                sob.put(mc.TargetFieldApi__c , sAcc.get(fieldapi) );
                            }
                        }
                        else{
                            sob.put(mc.TargetFieldApi__c ,sob.get(mc.TargetFieldApi__c) +' - '+sAcc.get(fieldapi));
                        }
                    }
                    else {
                        stvalue = s;
                        if(mc.Type__c == 'String')
                        sob.put(mc.TargetFieldApi__c , stvalue );
                        if(mc.Type__c == 'Integer')
                        sob.put(mc.TargetFieldApi__c , Integer.valueOf(stvalue) );
                        /*
                        if(mc.Type__c == 'Boolean')
                        {
                           if(mc.SourceFieldApi__c == 'TRUE')
                            sob.put(mc.TargetFieldApi__c , true );
                            if(mc.SourceFieldApi__c == 'FALSE')
                            sob.put(mc.TargetFieldApi__c , false);
                        }
                        */
                        
                    }
                    if(fieldapi.length()>0)
                        System.debug('******'+fieldapi);
                            else{System.debug('***field value ***'+stvalue);}
                }
            }       
        
        }
        
        OpportunityNotification__c opp = (OpportunityNotification__c)sob; 
        //if(contract.SVMXC__Sales_Rep__c  != null)
        //opp.OwnerId = contract.SVMXC__Sales_Rep__c;
        opp.TechCreatedFromSC__c = true;
        //Added for BR-7081 Dec 14 Release
        String OpptyName= contract.Name+'-'+account.Name;
        Integer StrLen = OpptyName.Length();
        if(StrLen<=70)
        {
            OpptyName= OpptyName;
        }
        else
            OpptyName = OpptyName.substring(0,StrLen-(StrLen-70));
        opp.name = OpptyName +' - '+dT.year();
        //opp.name = opp.name+' - '+dT.year();
        //End: Added for BR-7081 Dec 14 Release
        System.debug('*****'+opp );
        return opp;
  }

}