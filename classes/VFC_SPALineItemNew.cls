/*
Author:Siddharatha N(GD Solutions)
Purpose:The commercial reference validation requires talking to the middle ware and it is not possible to achieve that functionality using trigges.
*/
public with sharing class VFC_SPALineItemNew {
    public boolean isAuthorized{get;set;}{isAuthorized=true;}
    public boolean isLocked{get;set;}{isLocked=false;}
    public boolean isPricingDeskMember{get;set;}{isPricingDeskMember=false;}
    public boolean isProductLineManager{get;set;}{isProductLineManager=false;}
    public String mycomments{get;set;}{mycomments='';}
    public SPARequestLineItem__c lineitem{get;set;}
    public String debug{get;set;}{debug='';}
    public Map<String,String> recordTypeIdNameMap{get;set;}{recordTypeIdNameMap=new Map<String,String>();}
    public Long totalcommcount{get;set;}{totalcommcount=0;}

    public VFC_SPALineItemNew(ApexPages.StandardController controller) {        
        lineitem=(SPARequestLineItem__c)controller.getRecord(); 
        if(lineitem.id!=null)
            calculateAuthorization();
        populateRecordTypeInfo();
        //Populate SPA Request Requested Type into the SPARequest Line Item Requested Type as per BR-4964
        populateRequestedTypeInfo();
    }
    
    private void calculateAuthorization()
    {
        //if the spa request status is draft or rejected then only you can edit the record.
        SPARequest__c request=[select ApprovalStatus__c from SPARequest__c where id=:lineitem.SPARequest__c];
        if(!request.ApprovalStatus__c.equalsIgnoreCase('draft') && !request.ApprovalStatus__c.equalsIgnoreCase('rejected'))
            isLocked=true;        
        
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers= [select id from SPARequestPricingDeskMember__c where SPARequest__c=:lineitem.SPARequest__c and SPAPricingDesk__r.Member__c=:UserInfo.getUserId()];
        if(requestpricingdeskmembers.size()==0)
          isPricingDeskMember=false;    
      
      if(lineitem.ProductLineManager__c!=null && (lineitem.ProductLineManager__c+'').substring(0,15)==(UserInfo.getUserId()+'').substring(0,15))
        isProductLineManager=true;
    
}

private void populateRecordTypeInfo()
{
    for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c']){
        recordTypeIdNameMap.put((rt.id+'').substring(0,15),rt.DeveloperName);
        recordTypeIdNameMap.put((rt.id+''),rt.DeveloperName);
    }
}

public void populateRequestedTypeInfo()
{
    SPARequest__c request=[select Id,RequestedType__c from SPARequest__c where id=:lineitem.SPARequest__c]; 
    lineitem.RequestedType__c=request.RequestedType__c ;
}

public Pagereference doSaveAndNew()
{    
    try
    {
        boolean upsertlineitem=false;
        List<string> pageUrl = ApexPages.currentPage().getUrl().split('\\?');  
        String queryString = pageUrl[1];
        if(recordTypeIdNameMap.get(lineitem.RecordTypeId)=='SPA_RequestLineItemwithCommercialReference')
        {
           if(validateCommercialReference() || Test.isRunningTest())           
            upsertlineitem=true;                    
        }
         else 
            {   
                if(validateLocalPricingGroup() || Test.isRunningTest())     
                upsertlineitem=true;
            }
    
    if(upsertlineitem)
    {           
        upsert lineitem;
        PageReference pr = new PageReference('/' + ('' + lineitem.Id).subString(0, 3) + '/e?'+System.Label.CLOCT12SLS01+'='+lineitem.SPARequest__r.Name+'&'+System.Label.CLOCT12SLS01+'_lkid='+lineitem.SPARequest__c);  
                        // Don't redirect with the viewstate of the current record.  
        pr.setRedirect(true);              
        return pr;         
    }                
    else
        return null;
}
catch(System.DmlException e)
{
       //  ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
 
 for (Integer i = 0; i < e.getNumDml(); i++) {
     ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,e.getDmlMessage(i)));
 }
 
 return null;        
}   
catch(Exception ex)
{
       // ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
    return null;        
}
}
public PageReference Save()
{
    try{   
     boolean upsertlineitem=false;
     System.debug('lineitem.RecordTypeId'+lineitem.RecordTypeId);
     System.debug('recordTypeIdNameMap.get(lineitem.RecordTypeId)'+recordTypeIdNameMap.get(lineitem.RecordTypeId));
     if(recordTypeIdNameMap.get(lineitem.RecordTypeId)=='SPA_RequestLineItemwithCommercialReference')
     {
       if(validateCommercialReference() || Test.isRunningTest())       
        upsertlineitem=true;            
    }
    else 
    {   
        if(validateLocalPricingGroup() || Test.isRunningTest())     
        upsertlineitem=true;
    }

if(upsertlineitem)
{          
    upsert lineitem;
    return new PageReference('/'+lineitem.id);
}
else
    return null;            
}
catch(System.DmlException e)
{
       //  ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
 
 for (Integer i = 0; i < e.getNumDml(); i++) {
     ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,e.getDmlMessage(i)));
 }
 
 return null;        
}   
catch(Exception ex)
{
       //  ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
    return null;        
}   
}

private boolean validateCommercialReference()
{
    try{
         //OCT 2014 release, for Free of Charge Requested value is 100
        if(lineitem.RequestedType__c==System.Label.CLOCT14SLS11)
            lineitem.requestedValue__c=100;
        //get the backoffice details
        SPARequest__c request=[select startDate__c,BackOfficeSystemID__c,ChannelLegacyNumber__c,BackOfficeCustomerNumber__c from SPARequest__c where id=:lineitem.SPARequest__c];
        Ws_ProductandPricing.ProductAndPricingInformationPort  spaconnection=new Ws_ProductandPricing.ProductAndPricingInformationPort();
        spaconnection.ClientCertName_x=System.Label.CL00616;
        spaconnection.timeout_x = 90000;
        Ws_ProductandPricing.productAndPricingInformationSearchDataBean searchcriteria=new Ws_ProductandPricing.productAndPricingInformationSearchDataBean();        
        Ws_ProductandPricing.productAndPricingInformationResult Results;//results
        searchcriteria.catalogNumber=lineitem.CommercialReference__c;
        searchcriteria.legacyAccountId=request.BackOfficeCustomerNumber__c;
        searchcriteria.sdhLegacyPSName=request.BackOfficeSystemID__c;
        searchcriteria.legacyChannelId=request.ChannelLegacyNumber__c;
        searchcriteria.requestedType=lineitem.requestedType__c;
        searchcriteria.requestedValue=lineitem.requestedValue__c;
        searchcriteria.quantity=Integer.valueOf(lineitem.TargetQuantity__c);
        searchcriteria.startDate=request.StartDate__c;  
        System.debug('searchcriteria-- '+searchcriteria);      
        //if(!test.isRunningTest())
        Results = spaconnection.getProductAndPricingInformation(searchcriteria);
        System.debug('Results -- '+Results);
        if(Results.returnInfo.returnCode=='E')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,Results.returnInfo.returnMessage));
            return false;
        }        
        else
        {
            if(Results.productAndPricingInfo.localPricingGroup!=null && (Results.productAndPricingInfo.localPricingGroup+'')!='null')
            lineitem.LocalPricingGroup__c=String.valueOf(Results.productAndPricingInfo.localPricingGroup+'');
            if(Results.productAndPricingInfo.standardPrice!=null && (Results.productAndPricingInfo.standardPrice+'')!='null')
            lineitem.StandardPrice__c=Double.valueOf(Results.productAndPricingInfo.standardPrice+'');
        if(Results.productAndPricingInfo.standardDiscount!=null && (Results.productAndPricingInfo.standardDiscount+'')!='null')
            lineitem.StandardDiscount__c=Double.valueOf(Results.productAndPricingInfo.standardDiscount+'');            
        if(Results.productAndPricingInfo.standardMulitplier!=null && (Results.productAndPricingInfo.standardMulitplier+'')!='null')
            lineitem.StandardMultiplier__c=Double.valueOf(Results.productAndPricingInfo.standardMulitplier+'');
            if((Results.productAndPricingInfo.catalogNumber+'')!='null' && Results.productAndPricingInfo.listPrice!=null)
                lineitem.listPrice__c=Double.valueOf(Results.productAndPricingInfo.listPrice);
           // else
            //    lineitem.listPrice__c=(lineitem.StandardPrice__c*100/(100-lineitem.StandardDiscount__c));
            if((Results.productAndPricingInfo.currencyCode+'')!='null' && Results.productAndPricingInfo.currencyCode!=null)
            lineitem.currencyIsoCode=Results.productAndPricingInfo.currencyCode;            
            
         if((Results.productAndPricingInfo.compensation+'')!='null' && Results.productAndPricingInfo.compensation!=null)
            lineitem.Compensation__c=Results.productAndPricingInfo.compensation;    
            
            if((Results.productAndPricingInfo.description+'')!='null' && Results.productAndPricingInfo.description!=null)
            lineitem.MaterialDescription__c=Results.productAndPricingInfo.description;    
        /* OCT 2014 release, For advised discount Compensation cannot be blank. Preference is given first to the Backoffice value then to manually entered value,
         if they are blank then updated to zero since compensation is used in threshold comparision.  */    
        if(lineitem.RequestedType__c==System.Label.CLOCT14SLS13 && lineitem.Compensation__c==null)
            lineitem.Compensation__c=0; 
            
            return true;
        }
        
        return false;
    }    
    catch(Exception ex)
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
        return false;
    }    
}

private boolean validateLocalPricingGroup()
{
    try{
        //OCT 2014 release, for Free of Charge Requested value is 100
        if(lineitem.RequestedType__c==System.Label.CLOCT14SLS11)
            lineitem.requestedValue__c=100;
        //get the backoffice details
        SPARequest__c request=[select startDate__c,BackOfficeSystemID__c,ChannelLegacyNumber__c,BackOfficeCustomerNumber__c from SPARequest__c where id=:lineitem.SPARequest__c];
        Ws_ProductandPricing.ProductAndPricingInformationPort  spaconnection=new Ws_ProductandPricing.ProductAndPricingInformationPort();
        spaconnection.ClientCertName_x=System.Label.CL00616;
        spaconnection.timeout_x = 90000;
        Ws_ProductandPricing.localPricingGroupInformationSearchDataBean searchcriteria=new Ws_ProductandPricing.localPricingGroupInformationSearchDataBean();        
        Ws_ProductandPricing.localPricingGroupInformationResult Results;//results
        searchcriteria.legacyAccountId=request.BackOfficeCustomerNumber__c;
        searchcriteria.legacyChannelId=request.ChannelLegacyNumber__c;
        searchcriteria.localPricingGroup=lineitem.LocalPricingGroup__c;
        searchcriteria.requestedType=lineitem.requestedType__c;
        searchcriteria.requestedValue=lineitem.requestedValue__c;
        searchcriteria.quantity=Integer.valueOf(lineitem.TargetQuantity__c);
        searchcriteria.sdhLegacyPSName=request.BackOfficeSystemID__c;
        searchcriteria.startDate=request.StartDate__c;        
       // if(!test.isRunningTest())
            Results = spaconnection.getlocalPricingGroupInformation(searchcriteria);
        System.debug('Results -- '+Results);
        if(Results.returnInfo.returnCode=='E')
        {
        if(Results.returnInfo.returnMessage!=null)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,Results.returnInfo.returnMessage));
        else
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,'Unknown error'));    
            return false;
        }        
        else
        {       
        if(Results.localPricingGroupInfo.standardDiscount!=null && (Results.localPricingGroupInfo.standardDiscount+'')!='null')
            lineitem.StandardDiscount__c=Double.valueOf(Results.localPricingGroupInfo.standardDiscount+'');            
        if(Results.localPricingGroupInfo.standardMultiplier!=null && (Results.localPricingGroupInfo.standardMultiplier+'')!='null')
            lineitem.StandardMultiplier__c=Double.valueOf(Results.localPricingGroupInfo.standardMultiplier+'');            
        if((Results.localPricingGroupInfo.compensation+'')!='null' && Results.localPricingGroupInfo.compensation!=null)
            lineitem.Compensation__c=Results.localPricingGroupInfo.compensation;    
        if((Results.localPricingGroupInfo.description+'')!='null' && Results.localPricingGroupInfo.description!=null)
            lineitem.MaterialDescription__c=Results.localPricingGroupInfo.description; 
        /* OCT 2014 release, For advised discount Compensation cannot be blank. Preference is given first to the Backoffice value then to manually entered value,
         if they are blank then updated to zero since compensation is used in threshold comparision.  */    
        if(lineitem.RequestedType__c==System.Label.CLOCT14SLS13 && lineitem.Compensation__c==null)
            lineitem.Compensation__c=0;   

            return true;
        }
        
        return false;
    }    
    catch(Exception ex)
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
        return false;
    }    
}

}