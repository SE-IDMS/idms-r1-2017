/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 02/05/2013
    Description     : Test class for the Change Request Cost Forecast
*/


@isTest
private class CHR_CostForecast_TEST{
    static testMethod void CHRCostForecastTest(){


        Change_Request__c chr = new Change_Request__c(name='testCF',BusinessDomain__c='ERP Rationalization and Migration',Platform__c='Bridge');
        insert chr;
        DMTBudgetValueToChangeRequest__c  cr= new DMTBudgetValueToChangeRequest__c();
        cr.Name ='Test';
        cr.ChangeRequestAPIName__c = 'CashoutCapex__c';
        cr.CostItemFieldAPIName__c = 'ITCashoutCAPEX20';
        insert cr; 
        test.startTest();

        //creating Change request cost forecost1
        ChangeRequestCostsForecast__c crcf1 = new ChangeRequestCostsForecast__c();
        crcf1.ChangeRequest__c = chr.id;
        crcf1.Active__c = True;
        crcf1.LastActiveofBudgetYear__c = False;
        crcf1.Description__c = 'Test data';
        insert crcf1;
        //creating change request cost forecost2
        ChangeRequestCostsForecast__c crcf2 = new ChangeRequestCostsForecast__c();
        crcf2.ChangeRequest__c = chr.id;
        crcf2.Active__c =False ;
        crcf2.LastActiveofBudgetYear__c = False;
        crcf2.Description__c = 'Test data';
        insert crcf2;

           DMTCRCostForecastCostItemsFields__c pcfCs = new DMTCRCostForecastCostItemsFields__c();
           pcfCs.Name ='Test 1';
           pcfCs.CostItemFieldLabel__c = 'IT Cashout CAPEXY0';
           pcfCs.CostItemFieldAPIName__c = 'FY0CashoutCAPEX__c';
           pcfCs.Type__c = 'IT Cashout CAPEX';
           pcfCs.TypeOrder__c = 'Type 1';
           pcfCs.BackgroundColour__c = 'background-color:MistyRose';
           insert pcfCs;
           DMTCRCostForecastCostItemsFields__c pcfCs2 = new DMTCRCostForecastCostItemsFields__c();
           pcfCs2.Name ='Test 2';
           pcfCs2.CostItemFieldLabel__c = 'IT Cashout CAPEXY1';
           pcfCs2.CostItemFieldAPIName__c = 'FY1CashoutCAPEX__c';
           pcfCs2.Type__c = 'IT Cashout CAPEX';
           pcfCs2.TypeOrder__c = 'Type 1';
           pcfCs2.BackgroundColour__c = 'background-color:MistyRose';
           insert pcfCs2;
         
         crcf2.Active__c = True;
         crcf2.FY0PLImpact__c=null;
         crcf2.FY1PLImpact__c=null;
         crcf2.FY2PLImpact__c=null;
         crcf2.FY3PLImpact__c=null;
         crcf2.FY0CashoutCAPEX__c=null;
         update crcf2;
         delete crcf2;  
                      
           CHR_ManageProgramCostsForecasts pagePCF = new CHR_ManageProgramCostsForecasts(new ApexPages.StandardController(crcf1));
                      
           pagePCF.updateCostForecast();
           pagePCF.backToCostForecast();

         test.stopTest();             
    }
}