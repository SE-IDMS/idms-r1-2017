/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: VF Page Extension                                   *
* Page: Fielo_PageCustom                                    *
* Created Date: 25/08/2014                                  *
************************************************************/

public with sharing class Fielo_PageCustomExtension {
    
    public FieloEE__Menu__c menuCP {get;set;}
    public Boolean showMessageSuccess {get;set;}
    
    public FieloEE__ProgramLayout__c layout {get{return FieloEE.ProgramUtil.getFrontEndLayout();} set;}
    
    public Fielo_PageCustomExtension(FieloEE.PageController controller){}
    
    public Fielo_PageCustomExtension(FieloEE.ChangePasswordFieloController controller){
        String allFields = '';
        for(Schema.SObjectField nameAPIfield : Schema.SObjectType.FieloEE__Menu__c.fields.getMap().values()){
            allFields += allFields==''?String.ValueOf(nameAPIfield):', '+String.ValueOf(nameAPIfield);
        }
        try{menuCP = Database.query('SELECT '+allFields+' FROM FieloEE__Menu__c WHERE Id = \''+Apexpages.currentPage().getParameters().get('idMenu')+'\'');}catch(Exception e){}
    }
    
    public Fielo_PageCustomExtension(FieloEE.RegisterStep1Controller controller){}
    
    public Fielo_PageCustomExtension(Fielo_InvoiceUploadController controller){}
    
    private FieloEE.ShoppingCartItemsController sp_controller;
    public Fielo_PageCustomExtension(FieloEE.ShoppingCartItemsController controller){
        sp_controller = controller;
    }

    public Fielo_PageCustomExtension(ApexPages.StandardController controller){}
    
    public PageReference goToActivities(){
        return Fielo_Utilities.getMenuReference('My_Activity');
    }
    
    public PageReference doVoucherCustom(){
        showMessageSuccess = false;
        if(sp_controller.doVoucher() != null){
            showMessageSuccess = true;
        }
        return null;
    }
    
    public String getParameters(){
        String myString = '';
        for(string param: Apexpages.currentPage().getParameters().keySet()){
            myString += '&'+param+'='+Apexpages.currentPage().getParameters().get(param);
        }
        return myString;
    }
}