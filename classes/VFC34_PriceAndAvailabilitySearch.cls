public class VFC34_PriceAndAvailabilitySearch extends VFC_ControllerBase 
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    public VCC08_GMRSearch GMRSearchClass{get;set;} // Controller for the search in PM0 functionality
    Public String ExternalReferenceID = System.currentPagereference().getParameters().get('ExtRefID'); // Parameter coming from the URL
    public Boolean ReadOnly{get;set;} // To view a saved P&A, no search allowed
    public Boolean InitDone=false; // Set to true if the initialization is done
    public Case CurrentCase{get;set;} // Related Case
    public Account RelatedAccount{get;set;}
    public string relatedBackOffice{get;set;}
    public PAGMRSearch PAImplementation{get;set;}
    public DataTemplate__c fetchedPA{get;set;} // Retrieved P&A are stored in DataTemplate custom Object are there is no P&A one
    public List<SelectOption> columns{get;set;} 
    public Boolean DisplayResults{get;set;}  // Display results from the search
    public String TabInFocus{get;set;} 
    public Utils_PADataSource DataSource; // Data source to access the Web Service
    public Boolean DisableSearch{get;set;} // disable the search functionality if the Account remains unknown
    public DataTemplate__c DateCapturer{get;set;} // DateField1__c is used to capture the date format and the related Account ID
    public string PAbFOAccountID;
    public String PartNo{get;set;}  // Commercial reference of the product, can be filled with the search in PM0
    public Integer Quantity{get;set;}
    public String BackOffice{get;set;} 
    public List<SelectOption> BackOffices{get;set;} 
    public String SessionID{get;set;}
    public Boolean AdvancedPA{get;set;}
    
    public String RelatedAccountName
    {
        get{try{return [SELECT ID, Name FROM Account WHERE ID=:DateCapturer.Account__c].Name;}
            catch(Exception e){return Label.CL00577;}}
    }
    
     /*=======================================
      INNER CLASSES
    =======================================*/
    
   // Implementation of the Utils_GMRSearchMethods methods for the P&A search
    public class PAGMRSearch implements Utils_GMRSearchMethods
    {
       public Void Init(String ObjID, VCC08_GMRSearch Controller)
       {
           Case CurrentCase = new Case();
           
           if(!Controller.Cleared && !Controller.SearchHasBeenLaunched) // This is the first initialization
           {
                CurrentCase = [Select Id, CommercialReference__c, ProductBU__c, ProductLine__c, ProductFamily__c, Family__c, TECH_LaunchGMR__c from Case where Id=:ObjID limit 1];
          
                Controller.filters.add(CurrentCase.ProductBU__c);
                Controller.filters.add(CurrentCase.ProductLine__c);
                Controller.filters.add(CurrentCase.ProductFamily__c);
                Controller.filters.add(CurrentCase.Family__c);                                                
                Controller.searchKeyword = CurrentCase.CommercialReference__c;
                
                Controller.RelatedObject = (Case)CurrentCase;
           }
       }
       
       public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
       {
           VCC08_GMRSearch GMRController = (VCC08_GMRSearch)ControllerBase;
           VFC34_PriceAndAvailabilitySearch PAController = (VFC34_PriceAndAvailabilitySearch)GMRController.PageController;
           return PAController.DisplayCheckPA(obj);   
       }
       
       public PageReference Cancel( VCC08_GMRSearch Controller)
       { 
            PageReference CasePage = new ApexPages.StandardController((Case)Controller.RelatedObject).view();
            return CasePage;
       }   
    }
    
     
    /*=======================================
      CONSTRUCTOR
    =======================================*/     
    public VFC34_PriceAndAvailabilitySearch(ApexPages.StandardController controller) 
    { 
        SessionID = UserInfo.getSessionID();
       
        currentCase = (Case)controller.getRecord();
        fetchedPA = new DataTemplate__c();
        DateCapturer = new DataTemplate__c();
        DateCapturer = new DataTemplate__c();  
        DataSource = new Utils_PADataSource();      
        columns = new List<SelectOption>();
        GMRSearchClass = new VCC08_GMRSearch();
        PAImplementation = new PAGMRSearch();
             
        currentCase = [Select Id, Account.ID, CommercialReference__c, ProductBU__c, ProductLine__c, ProductFamily__c, Family__c, Country__c, TECH_LaunchGMR__c 
                    FROM Case WHERE Id=:CurrentCase.Id limit 1];
                            
        PartNo = currentCase .CommercialReference__c;
        DateCapturer.DateField1__c = Date.Today();
        DateCapturer.Account__c = currentCase .AccountID;
        BackOfficeRefresher();
        Quantity = 1;
        
        AdvancedPA = false;  
        DisplayResults = false;
        //--- TEST MODE: OFF
        DataSource.test = false;
        // --------------------                      
        InitDone = true;      
                
        SwitchToAdvanced();     
    }

    /*=======================================
      METHODS
    =======================================*/  
    //Search Method     
    public PageReference CheckPA()
    {        
        // pretreatment   
        if(PartNo == '')
        {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00478));
             return null;
        }
        
         if(Quantity == null)
         {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00479));
             return null;
        }  
        
        if(BackOffice == Label.CL00355) // Back Office == --None--
        {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00480));
             return null;
        }      

        // Search           
       try
       {
            Utils_datasource.result Results = DataSource.SearchPA(this);
            List<DataTemplate__c> RecordList = Results.recordList;
            if(RecordList != null)
                fetchedPA = RecordList[0];

            RelatedAccount = [SELECT ID, Name FROM Account WHERE ID=:DateCapturer.Account__c];            
            relatedBackOffice = BackOffice;             
            DisplayResults = true; 
                
            if(Results.ReturnMessage != null)
            {
                DisplayResults = false;
            
                if(Results.ReturnMessage == Label.CL00590)
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00592));
                else if(Results.ReturnMessage == Label.CL00591)    
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00592)); 
                else if(Results.ReturnMessage == Label.CL00595)   
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00594));    
                 else if(Results.ReturnMessage == 'More than one SDH Cross Ref')       
                     SwitchToAdvanced();                 
                else
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Results.ReturnMessage));                           
            }    
        }
        catch(Exception CalloutException)
        {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));   
        }
        
        return null;
    }
    
    // Clear method
    public void clearPA()
    {
         DateCapturer.DateField1__c = Date.today();
         PartNo = null;
         Quantity = 1;
         DisplayResults = false;
         DateCapturer.Account__c = CurrentCase.AccountID;   
    }
 
    // Display details for the selected P&A by clicking on the "view detail button" of the GMR Search Component.         
    public PageReference DisplayCheckPA(sObject obj)
    {
        DataTemplate__c GMRProduct = (DataTemplate__c)obj; 
        PartNo = GMRProduct.field8__c;
        TabInFocus = 'CheckPA';
        return null;
    }
        
    // Method called to refresh back offices on a change of Account
    public void BackOfficeRefresher()
    {
        List<LegacyAccount__c> LegacyAccounts = [SELECT ID, Name, Account__c, LegacyName__c, LegacyNumber__c 
                                                 FROM LegacyAccount__c WHERE Account__c=:DateCapturer.Account__c];
        
        BackOffices = new List<SelectOption>();     
        for(LegacyAccount__c LegacyAccount:LegacyAccounts)
        {
            BackOffices.add(new SelectOption(LegacyAccount.LegacyName__c,LegacyAccount.LegacyName__c));
        } 
         
        // If no back office is found select "--None--" 
        if(BackOffices.size()==0)
        {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00502)); 
             BackOffices.add(new SelectOption(Label.CL00355,Label.CL00355)); 
        }
        
        
        // If several back offices are found, select the first one by default          
        if(BackOffices.size()>1)
             BackOffice = BackOffices[0].getvalue();
    }
    
    public void ShowAllLegacy()
    {
        BackOffices.clear();
        Schema.DescribeFieldResult pickList = Schema.sObjectType.LegacyAccount__c.fields.LegacyName__c;
        
        for (Schema.PickListEntry PickVal : pickList.getPicklistValues())
        {
            BackOffices.add(new SelectOption(PickVal.getvalue(),PickVal.getlabel() ));
        } 
    }
    
    public pagereference SavePA()
    {
        CSE_ExternalReferences__c ExternalReference = new CSE_ExternalReferences__c();
        ExternalReference.Case__c = CurrentCase.ID;  
        ExternalReference.Type__c = 'P&A';
        ExternalReference.TECH_Account__c = PAbFOAccountID;
        ExternalReference.TECH_BackOffice__c = fetchedPA.field5__c; 
        ExternalReference.SystemReference__c = PartNo;
        
        String delimiter = ': ';
        String newline = '\n';
        
        ExternalReference.Description__c = '-- Simple P&A --';
        ExternalReference.Description__c += newline + Label.CL00469 + delimiter + String.valueof(fetchedPA.Datefield1__c);
        ExternalReference.Description__c += newline + Label.CL00460 + delimiter + fetchedPA.field2__c;     
        ExternalReference.Description__c += newline + Label.CL00473 + delimiter + fetchedPA.field3__c;
        ExternalReference.Description__c += newline + Label.CL00463 + delimiter + fetchedPA.field4__c;
        ExternalReference.Description__c += newline + Label.CL00465 + delimiter + fetchedPA.field5__c;
            
        DataBase.SaveResult InsertedExternalReference = DataBase.insert(ExternalReference); 
        System.Debug('---- DML Errors: ' + InsertedExternalReference .getErrors() ); 
                             
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00482 +' '+ExternalReference.Name)); 
        PageReference CasePage = new ApexPages.StandardController(CurrentCase).save(); 
        return CasePage;
    }
    
    public pagereference SwitchToAdvanced()
    {
        SessionID = UserInfo.getSessionID();
        AdvancedPA = true;
        TabInFocus = 'AdvancedPA';
        return null;
    }
    
    public pagereference SwitchToSimple()
    {
        AdvancedPA = false;
        TabInFocus = 'CheckPA';
        return null;
    }
    
    // Exit method
    public PageReference Exit()
    {
        PageReference CasePage =  new ApexPages.StandardController(CurrentCase).cancel();
        return CasePage;            
    }        
}