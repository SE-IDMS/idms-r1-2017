public class VFC_LiveAgent_PrechatForm_Global {
    public CS_LiveAgent_PrechatFormSettings__c config;
    public List<CS_LiveAgent_FormRoutingRule__c[]> lst_configRoutingRules;
    public String[] Lst_categories;
    //public String errMsg {get;set;}
    public String configName {get;set;}
    
    public Map<String,String> map_titleCode2Label = new Map<String,String>();
    
    // true if the configName defined in the deployment code is retrieved in the custom setting.
    public Boolean hasFoundConfig {get;set;} 
    
    public Boolean hasSearchedConfig {get;set;}
    
    // Allows to indicate to the visualforce that a config has been retrieved from the deployment 
    // but this config is not defined in the custom setting.
    // If hasConfigNameButNotFound = false : 
    //      - This is the first load of the page, the configName had not been retrieved yet.
    //      - The corresponding configuration has been found.
    //      - No config name was defined in the deployement code.
    // If hasConfigNameButNotFound = true :
    //      - A config Name was defined in the deployment code but it is not related to any configuration existed in the custom setting. 
    public Boolean hasConfigNameButNotFound {get;set;}
    
    public CS_LiveAgent_PrechatFormSettings__c getConfig() {
        hasConfigNameButNotFound = false;
        hasFoundConfig = false;
        if(configName != ''){
            if(CS_LiveAgent_PrechatFormSettings__c.getValues(configName) != null)
            {
                config = CS_LiveAgent_PrechatFormSettings__c.getValues(configName);
                hasFoundConfig = true;
            } 
            else if(CS_LiveAgent_PrechatFormSettings__c.getValues('Default') != null)
            {
                config = CS_LiveAgent_PrechatFormSettings__c.getValues('Default');
                hasFoundConfig = true;
            }
        }
        else
        {
            if(CS_LiveAgent_PrechatFormSettings__c.getValues('Default') != null)
            {
                config = CS_LiveAgent_PrechatFormSettings__c.getValues('Default');
                hasFoundConfig = true;
            }
            else
            {
                hasConfigNameButNotFound = true;
            }
        }
        hasSearchedConfig = true;
        return config;
    }
    
    public List<CS_LiveAgent_FormRoutingRule__c[]> getlst_configRoutingRules() {
        if(config != null && config.IsRoutingMenuVisible__c)
        {
            CS_LiveAgent_FormRoutingRule__c[] selectedRulesNoOrder = new CS_LiveAgent_FormRoutingRule__c[0];
            List<CS_LiveAgent_FormRoutingRule__c> lst_allConfigRoutings = CS_LiveAgent_FormRoutingRule__c.getall().values();
            for(CS_LiveAgent_FormRoutingRule__c rule : lst_allConfigRoutings)
            {
                if(rule.RegionCode__c == config.RegionCode__c
                    && rule.CountryCode__c == config.CountryCode__c 
                    && rule.ChatAudience__c == config.ChatAudience__c
                    && rule.CustomForm__c == config.CustomForm__c)
                {
                    selectedRulesNoOrder.add(rule);
                }
            }
            selectedRulesNoOrder.sort();
            
            List<CS_LiveAgent_FormRoutingRule__c[]> selectedRules = new List<CS_LiveAgent_FormRoutingRule__c[]>();
            Integer i = 0;
            String lastGroup = '';
            selectedRules.add(new CS_LiveAgent_FormRoutingRule__c[0]);
            for(CS_LiveAgent_FormRoutingRule__c rule : selectedRulesNoOrder) {
                // Selectize JS library cannot manage picklist using the same values several time.
                // Therefore an idex is added at the end of the Chat button Id when the page is generated (not in the bFO Database).
                // The index is removed by javascript when the form is submitted
                rule.ChatButtonID__c += '+' + i;
                i++;
                if (rule.Group__c != null && rule.Group__c.length() > 0)
                {
                    if (rule.Group__c != lastGroup)
                    {
                        selectedRules.add(new CS_LiveAgent_FormRoutingRule__c[0]);
                        lastGroup = rule.Group__c;
                    }
                    selectedRules[selectedRules.size()-1].add(rule);
                }
                else
                {
                    selectedRules[0].add(rule);
                }
            }
            return selectedRules;
        }
        else
        {
            return null;
        }
    }
    
    public String[] getlst_categories() {
        if(config != null && config.IsCategoryVisible__c && config.CategoryValues__c != null && config.CategoryValues__c.length() > 0){
            return config.CategoryValues__c.split(';', -1);
        }
        return new String[0];
    }
    
    public VFC_LiveAgent_PrechatForm_Global
        (){
        configName = '';
        
        hasSearchedConfig = false;
        List<CS_LiveAgent_FormRoutingRule__c> lst_configRoutingRules = new List<CS_LiveAgent_FormRoutingRule__c>();
        List<Schema.PicklistEntry> picklistValues = LiveChatTranscript.SuppliedTitle__c.getDescribe().getPicklistValues();
        for (Schema.PicklistEntry pe: picklistValues) {
            map_titleCode2Label.put(pe.getValue(), pe.getLabel());
        }
    }
    
    public pagereference saveConfigController() {        
        return null;
    }
}