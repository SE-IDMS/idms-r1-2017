public class PRM_CollaborationFeature implements FieloPRM_FeatureCustomLogic{
    
    public PRM_CollaborationFeature (){
    }
    
    
    public void promote(Set<String> PRMUIMSIdList){
       System.debug('PRM_FeatureCustomPermissionSet promote');
       //get Users
       assignPermissionSet(PRMUIMSIdList,'PRM_Collaboration');
    }

    /**
     * Method is Future to avoid MIXED_DML_OPERATION exception
     */
    private static void assignPermissionSet(Set<String> PRMUIMSIdList,String psetName){
       PermissionSet pset1 = [select id from PermissionSet where name =: psetName];
       List<PermissionSetAssignment> psetAssignList = new List<PermissionSetAssignment>();
       List<User> usertToUpdate = new List<User>();
       for(User user : [select Id,FederationIdentifier,Profile.Name,UserPermissionsSFContentUser from user where Contact.PRMUIMSId__c in: PRMUIMSIdList and Contact.PRMUIMSId__c!=null and FederationIdentifier!=null]){
            System.debug('###Assign pset '+psetName+' to user with FederationIdentifier: ' + user.FederationIdentifier + '. ,Profile.Name:'+user.Profile.Name);
            PermissionSetAssignment psetAssign = new PermissionSetAssignment();
            psetAssign.PermissionSetId = pset1.id;
            psetAssign.AssigneeId = user.Id;
            psetAssignList.add(psetAssign);
            user.UserPermissionsSFContentUser = true;
            usertToUpdate.add(user);
            
            //Adding user in a Public Group
            List<GroupMember> GMlist = new List<GroupMember>();
            GroupMember GM = new GroupMember();
            GM.GroupId = Label.CLOCT15CF058;
            GM.UserOrGroupId = user.Id;
            GMList.add(GM); 
            if(!GMList.isEmpty()) {
              System.debug('Group Member List is ' + GMList);
              insert GMList;
            }
            
       }
       
      
        
      insert psetAssignList;
      update usertToUpdate;
    }

    public void demote(Set<String> PRMUIMSIdList){
        System.debug('PRM_FeatureCustomPermissionSet demote');
        System.debug('PRMUIMSIdList: ' + PRMUIMSIdList);
        removePermissionSet(PRMUIMSIdList,'PRM_Collaboration');
    }

    private static void removePermissionSet(Set<String> PRMUIMSIdList,String psetName){
        List<PermissionSetAssignment> psetAssignList = [select Id From PermissionSetAssignment where Assignee.FederationIdentifier in:PRMUIMSIdList and Assignee.FederationIdentifier!=null and PermissionSet.Name =: psetName];
        System.debug('###psetAssignList '+psetAssignList);
        delete psetAssignList;
        List<User> usertToUpdate = new List<User>();
        for(User user : [select Id,UserPermissionsSFContentUser from user where Contact.PRMUIMSId__c in: PRMUIMSIdList and Contact.PRMUIMSId__c!=null and FederationIdentifier!=null]){
            user.UserPermissionsSFContentUser = false;
            usertToUpdate.add(user);
            
            //Remove user from a Public Group -- System.ListException: Missing id at index: 0
            List<GroupMember> delGroupIds = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember where groupId =: Label.CLOCT15CF058 AND UserOrGroupId =: user.Id limit 1];  
            System.debug('+++Deleting GroupMember :'+Label.CLOCT15CF058 + ' delGroupIds.size=' + delGroupIds.size() + ' delGroupIds=' + delGroupIds);
            If(delGroupIds.size() > 0) {
              delete delGroupIds;
            }            
        }
        update usertToUpdate;
    }

}