@istest
public with sharing class VFC_PRMProgressiveProfile_Test{ 
    static testmethod void unitTest() {
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Component__c comp;
        Id IdRTComponentPrgProf = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'C_ProgressiveProfile'].Id;
        FieloEE.MockUpFactory.setCustomProperties(false);

        menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'Test Menu'
        );
        insert menu;
        
        comp = new FieloEE__Component__c(
            Name = 'Prog Profile',
            FieloEE__Menu__c = menu.Id, 
            RecordTypeId = IdRTComponentPrgProf,
            FieloEE__Layout__c = 'Grid'
        );
        insert comp;
        
        Country__c TestCountry = new Country__c();
        TestCountry.Name   = 'India';
        TestCountry.CountryCode__c = 'IN';
        TestCountry.InternationalPhoneCode__c = '91';
        TestCountry.Region__c = 'APAC';
        INSERT TestCountry;
        
        StateProvince__c testStateProvince = new StateProvince__c();
        testStateProvince.Name = 'Karnataka';
        testStateProvince.Country__c = testCountry.Id;
        testStateProvince.CountryCode__c = testCountry.CountryCode__c;
        testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince;
        
        PRMCountry__c PRMCon = new PRMCountry__c(
                Country__c = TestCountry.Id,
                CountrySupportEmail__c = 'test1.lastName@yopmail.com',
                PLDatapool__c = 'US_en2',
                CountryPortalEnabled__c = true
            );
        Insert PRMCon;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild; 
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',City__c = 'Bengaluru', 
                                Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMCountry__c=TestCountry.id,PRMBusinessType__c='FI',
                                PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, StateProvince__c = testStateProvince.id,PRMAnnualSales__c='99999');
        insert PartnerAcc;
        
        Contact PartnerCon1 = new Contact(
              FirstName='Test1',
              LastName='lastname1',
              AccountId=PartnerAcc.Id,
              JobTitle__c='Z3',
              CorrespLang__c='EN',
              PRMMobilePhone__c='0987654321',
              Country__c=TestCountry.Id,
              WorkPhone__c='0987654321',
              PRMFirstName__c = 'Test1',
              PRMLastName__c = 'lastname1',
              PRMEmail__c = 'test1.lastName@yopmail.com',
              PRMTaxId__c = '12345'
              );      
              
              Insert PartnerCon1;   
        Id profilesId = [select id from profile where name='SE - Channel Partner (Community)'].Id; 
            
        String strOriginal = 'PHNhbWxwOlJlc3BvbnNlIHhtbG5zOnNhbWxwPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIERlc3RpbmF0aW9uPSJodHRwczovL2Rldm1hamJmby1zZWNvbW11bml0aWVzLmNzMi5mb3JjZS5jb20vcGFydG5lcnMvbG9naW4/c289MDBEUjAwMDAwMDF1d3pmIiBJRD0iczIyYzEwOWIxYmRkYmIxNjQ1ZmNjMGZlMmNmNGVhZDAyZDY0MzkxYmM3IiBJc3N1ZUluc3RhbnQ9IjIwMTYtMDYtMjdUMjI6MTU6NDVaIiBWZXJzaW9uPSIyLjAiPjxzYW1sOklzc3VlciB4bWxuczpzYW1sPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj5odHRwczovL2ltcy1pbnQuYnRzZWMuZGV2LnNjaG5laWRlci1lbGVjdHJpYy5jb206NDQzL29wZW5zc288L3NhbWw6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI3MyMmMxMDliMWJkZGJiMTY0NWZjYzBmZTJjZjRlYWQwMmQ2NDM5MWJjNyIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6VHJhbnNmb3JtcyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIi8+CjxkczpEaWdlc3RWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+dGRZTUt4SlkyenN3TU5lYnVsMDZMUUtnQW1rPTwvZHM6RGlnZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CmFNMWwxeC9VNlkyU0FGYVJvbU81YVdvdTVab013TzRpbU13RGZ4OW5vMFpFOGUyaFRYNkhoU0RHbmZrYUo0U05obVRseFpHMnB3VlUKdk41VXA4N2htZlZTYmVoR2ZPU00zUFVkV0tRNzhxTHF0b21FS1ZRQ1FTMDNPWE1FYWZiL1cybXBHbzhKNnZNT2FWaDhqUjU1ZC9nWApUNytsVzVqNFkvY2lyMkpxVk8wPQo8L2RzOlNpZ25hdHVyZVZhbHVlPgo8ZHM6S2V5SW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5RGF0YSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5Q2VydGlmaWNhdGUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgpNSUlDUURDQ0Fha0NCRWVOQjBzd0RRWUpLb1pJaHZjTkFRRUVCUUF3WnpFTE1Ba0dBMVVFQmhNQ1ZWTXhFekFSQmdOVkJBZ1RDa05oCmJHbG1iM0p1YVdFeEZEQVNCZ05WQkFjVEMxTmhiblJoSUVOc1lYSmhNUXd3Q2dZRFZRUUtFd05UZFc0eEVEQU9CZ05WQkFzVEIwOXcKWlc1VFUwOHhEVEFMQmdOVkJBTVRCSFJsYzNRd0hoY05NRGd3TVRFMU1Ua3hPVE01V2hjTk1UZ3dNVEV5TVRreE9UTTVXakJuTVFzdwpDUVlEVlFRR0V3SlZVekVUTUJFR0ExVUVDQk1LUTJGc2FXWnZjbTVwWVRFVU1CSUdBMVVFQnhNTFUyRnVkR0VnUTJ4aGNtRXhEREFLCkJnTlZCQW9UQTFOMWJqRVFNQTRHQTFVRUN4TUhUM0JsYmxOVFR6RU5NQXNHQTFVRUF4TUVkR1Z6ZERDQm56QU5CZ2txaGtpRzl3MEIKQVFFRkFBT0JqUUF3Z1lrQ2dZRUFyU1FjL1U3NUdCMkF0S2hiR1M1cGlpTGttSnpxRXNwNjRyRHhiTUoreERyeWUwRU4vcTFVNU9mKwpSa0RzYU4vaWdrQXZWMWN1WEVnVEw2UmxhZkZQY1VYN1F4RGhaQmhzWUY5cGJ3dE16aTRBNHN1OWhueEloVVJlYkdFbXhLVzlxSk5ZCkpzMFZvNStJZ2p4dUVXbmpublZnSFRzMSttcTVRWVRBN0U2WnlMOENBd0VBQVRBTkJna3Foa2lHOXcwQkFRUUZBQU9CZ1FCM1B3L1UKUXpQS1RQVFlpOXVwYkZYbHJBS013dEZmMk9XNHl2R1dXdmxjd2NOU1pKbVRKOEFSdlZZT01FVk5ic1Q0T0ZjZnUyL1BlWW9BZGlEQQpjR3kvRjJadWo4WEpKcHVRUlNFNlB0UXFCdURFSGpqbU9RSjByVi9yOG1PMVpDdEhSaHBaNXpZUmpoUkM5ZUNiang5VnJGYXgwSkRDCi9GZndXaWdtclcwWTBRPT0KPC9kczpYNTA5Q2VydGlmaWNhdGU+CjwvZHM6WDUwOURhdGE+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+PHNhbWxwOlN0YXR1cyB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIj4KPHNhbWxwOlN0YXR1c0NvZGUgVmFsdWU9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpzdGF0dXM6U3VjY2VzcyIgeG1sbnM6c2FtbHA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCI+Cjwvc2FtbHA6U3RhdHVzQ29kZT4KPC9zYW1scDpTdGF0dXM+PHNhbWw6QXNzZXJ0aW9uIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIElEPSJzMmMxYjdiMGFlNzBlNTg3N2VkMjNkMjU5MmMwYjE1MGM0NWE1ZjIxODEiIElzc3VlSW5zdGFudD0iMjAxNi0wNi0yN1QyMjoxNTo0NFoiIFZlcnNpb249IjIuMCI+CjxzYW1sOklzc3Vlcj5odHRwczovL2ltcy1pbnQuYnRzZWMuZGV2LnNjaG5laWRlci1lbGVjdHJpYy5jb206NDQzL29wZW5zc288L3NhbWw6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI3MyYzFiN2IwYWU3MGU1ODc3ZWQyM2QyNTkyYzBiMTUwYzQ1YTVmMjE4MSIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6VHJhbnNmb3JtcyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIi8+CjxkczpEaWdlc3RWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+b1M0QmxGamRIczZBY2dGZ0ZuekZaUE90RVRzPTwvZHM6RGlnZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CmtoR1EycHU1bTJDK3FkTStNTXJXWUtoTWNBTWlNQmVTVkJxMEozZStYcjlwb1AvaFB6VHd3SkhMVVpwaitzVjJnQklBUXF3cUxZbTIKWHpkSnFsVlJuUUw2eGJrbmFHNUY4SDNwTTZNa0IyYUprYzUvMjQ0SGV0SEhmUXpiTzlDejQwWWtXci9QS3plOVdUSEZSMDUyNkliOQo2ajBMVFhoLzkzblZ2Sm9qT1RJPQo8L2RzOlNpZ25hdHVyZVZhbHVlPgo8ZHM6S2V5SW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5RGF0YSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5Q2VydGlmaWNhdGUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgpNSUlDUURDQ0Fha0NCRWVOQjBzd0RRWUpLb1pJaHZjTkFRRUVCUUF3WnpFTE1Ba0dBMVVFQmhNQ1ZWTXhFekFSQmdOVkJBZ1RDa05oCmJHbG1iM0p1YVdFeEZEQVNCZ05WQkFjVEMxTmhiblJoSUVOc1lYSmhNUXd3Q2dZRFZRUUtFd05UZFc0eEVEQU9CZ05WQkFzVEIwOXcKWlc1VFUwOHhEVEFMQmdOVkJBTVRCSFJsYzNRd0hoY05NRGd3TVRFMU1Ua3hPVE01V2hjTk1UZ3dNVEV5TVRreE9UTTVXakJuTVFzdwpDUVlEVlFRR0V3SlZVekVUTUJFR0ExVUVDQk1LUTJGc2FXWnZjbTVwWVRFVU1CSUdBMVVFQnhNTFUyRnVkR0VnUTJ4aGNtRXhEREFLCkJnTlZCQW9UQTFOMWJqRVFNQTRHQTFVRUN4TUhUM0JsYmxOVFR6RU5NQXNHQTFVRUF4TUVkR1Z6ZERDQm56QU5CZ2txaGtpRzl3MEIKQVFFRkFBT0JqUUF3Z1lrQ2dZRUFyU1FjL1U3NUdCMkF0S2hiR1M1cGlpTGttSnpxRXNwNjRyRHhiTUoreERyeWUwRU4vcTFVNU9mKwpSa0RzYU4vaWdrQXZWMWN1WEVnVEw2UmxhZkZQY1VYN1F4RGhaQmhzWUY5cGJ3dE16aTRBNHN1OWhueEloVVJlYkdFbXhLVzlxSk5ZCkpzMFZvNStJZ2p4dUVXbmpublZnSFRzMSttcTVRWVRBN0U2WnlMOENBd0VBQVRBTkJna3Foa2lHOXcwQkFRUUZBQU9CZ1FCM1B3L1UKUXpQS1RQVFlpOXVwYkZYbHJBS013dEZmMk9XNHl2R1dXdmxjd2NOU1pKbVRKOEFSdlZZT01FVk5ic1Q0T0ZjZnUyL1BlWW9BZGlEQQpjR3kvRjJadWo4WEpKcHVRUlNFNlB0UXFCdURFSGpqbU9RSjByVi9yOG1PMVpDdEhSaHBaNXpZUmpoUkM5ZUNiang5VnJGYXgwSkRDCi9GZndXaWdtclcwWTBRPT0KPC9kczpYNTA5Q2VydGlmaWNhdGU+CjwvZHM6WDUwOURhdGE+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+PHNhbWw6U3ViamVjdD4KPHNhbWw6TmFtZUlEIEZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6MS4xOm5hbWVpZC1mb3JtYXQ6dW5zcGVjaWZpZWQiIE5hbWVRdWFsaWZpZXI9Imh0dHBzOi8vaW1zLWludC5idHNlYy5kZXYuc2NobmVpZGVyLWVsZWN0cmljLmNvbTo0NDMvb3BlbnNzbyIgU1BOYW1lUXVhbGlmaWVyPSJodHRwczovL2Rldm1hamJmby1zZWNvbW11bml0aWVzLmNzMi5mb3JjZS5jb20iPjliOTE5NGZlLTdlMjEtNGMwNi1hMjdjLWY5OTc0NjNjNzYyOTwvc2FtbDpOYW1lSUQ+PHNhbWw6U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPgo8c2FtbDpTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBOb3RPbk9yQWZ0ZXI9IjIwMTYtMDYtMjdUMjI6MjU6NDVaIiBSZWNpcGllbnQ9Imh0dHBzOi8vZGV2bWFqYmZvLXNlY29tbXVuaXRpZXMuY3MyLmZvcmNlLmNvbS9wYXJ0bmVycy9sb2dpbj9zbz0wMERSMDAwMDAwMXV3emYiLz48L3NhbWw6U3ViamVjdENvbmZpcm1hdGlvbj4KPC9zYW1sOlN1YmplY3Q+PHNhbWw6Q29uZGl0aW9ucyBOb3RCZWZvcmU9IjIwMTYtMDYtMjdUMjI6MDU6NDVaIiBOb3RPbk9yQWZ0ZXI9IjIwMTYtMDYtMjdUMjI6MjU6NDVaIj4KPHNhbWw6QXVkaWVuY2VSZXN0cmljdGlvbj4KPHNhbWw6QXVkaWVuY2U+aHR0cHM6Ly9kZXZtYWpiZm8tc2Vjb21tdW5pdGllcy5jczIuZm9yY2UuY29tPC9zYW1sOkF1ZGllbmNlPgo8L3NhbWw6QXVkaWVuY2VSZXN0cmljdGlvbj4KPC9zYW1sOkNvbmRpdGlvbnM+CjxzYW1sOkF1dGhuU3RhdGVtZW50IEF1dGhuSW5zdGFudD0iMjAxNi0wNi0yN1QyMjoxNTo0NFoiIFNlc3Npb25JbmRleD0iczIxZDNkNzg1YzQ5MjkwMDUxMDViMmQ2YjU1ODljOWY0MjhhMDE5ZDAxIj48c2FtbDpBdXRobkNvbnRleHQ+PHNhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY+dXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFjOmNsYXNzZXM6UGFzc3dvcmRQcm90ZWN0ZWRUcmFuc3BvcnQ8L3NhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY+PC9zYW1sOkF1dGhuQ29udGV4dD48L3NhbWw6QXV0aG5TdGF0ZW1lbnQ+PHNhbWw6QXR0cmlidXRlU3RhdGVtZW50PjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJwcmltYXJ5Q29udGFjdCI+PHNhbWw6QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj50cnVlPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9ImNvdW50cnlDb2RlIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPlVTPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9InN1cm5hbWUiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+Uzwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJnaXZlbk5hbWUiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+QWtpbGE8L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgTmFtZT0icHJlZmVycmVkTGFuZ3VhZ2UiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+ZW48L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgTmFtZT0iZmVkZXJhdGVkSWQiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+OWI5MTk0ZmUtN2UyMS00YzA2LWEyN2MtZjk5NzQ2M2M3NjI5PC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9ImVtYWlsIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPmFraWxhLmRldm1hamJmbzNAeW9wbWFpbC5jb208L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgTmFtZT0iY29tcGFueUlkIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPjYzNzJjODVkLTY1YWEtNGEwMS05MjFmLWNmMjRjMGUxOWMxMTwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJzb3VyY2VTeXN0ZW1JZCI+PHNhbWw6QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5QUk1fYkZPPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PC9zYW1sOkF0dHJpYnV0ZVN0YXRlbWVudD48L3NhbWw6QXNzZXJ0aW9uPjwvc2FtbHA6UmVzcG9uc2U+';
        User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                          CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                          Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
            PRMTemporarySamlToken__c = strOriginal,
                          LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId,ContactID = PartnerCon1.Id, UserPermissionsSFContentUser=true );        
        insert u1;  
        
        /*PermissionSet fieloPermissionSet = [Select ID from PermissionSet where Name='FieloPRM_SE' Limit 1];
        PermissionSetAssignment newPermissionSetAssignment1 = new PermissionSetAssignment();
        newPermissionSetAssignment1.AssigneeId = u1.id;
        newPermissionSetAssignment1.PermissionSetId = fieloPermissionSet.id;
        Insert newPermissionSetAssignment1; */
        PartnerAcc.Owner = u1;
        
        FieloEE__Member__c member = new FieloEE__Member__c(
            FieloEE__LastName__c = 'Polo',
            FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
            FieloEE__Street__c = 'test',
            F_Account__c = PartnerAcc.Id,
            F_PRM_IsActivePRMContact__c = false,
            F_PRM_CountryCodeInjectQuery__c = 'US',
            F_PRM_Country__c = PRMCon.Id,
            F_Country__c = TestCountry.Id,
            F_PRM_PrimaryChannel__c = 'FI1',
            FieloEE__User__c = u1.Id              
        );         
        insert member;
        
        PartnerCon1.FieloEE__Member__c = member.Id;
        update PartnerCon1;
    
        PRMProfileConfig__c pconfig = new PRMProfileConfig__c ();
        pconfig.Name='Test Profile Config';
        pconfig.PRMCountryClusterSetup__c = PRMCon.Id;
        insert pconfig;
        
        PRMProfileConfigInfo__c pinfo = new PRMProfileConfigInfo__c();
        pinfo.FieldAPIName__c = 'annualSales';
        pinfo.Mandatory__c = TRUE;
        pinfo.ObjectAPIName__c = 'Account';
        pinfo.PRMProfileConfig__c = pconfig.Id;
        insert pinfo;
        PRMProfileConfigInfo__c pinfo1 = new PRMProfileConfigInfo__c();
        pinfo1.FieldAPIName__c = 'cTaxId';
        pinfo1.Mandatory__c = TRUE;
        pinfo1.ObjectAPIName__c = 'Contact';
        pinfo1.PRMProfileConfig__c = pconfig.Id;
        insert pinfo1;
        
        ComponentProfileMap__c cpmap = new ComponentProfileMap__c();
        cpMap.Active__c = true;
        cpMap.Component__c = comp.Id;
        cpMap.PRMProfileConfig__c = pconfig.Id;
        insert cpmap;
        
        List<CS_PRM_ProgressiveProfileConfig__c > cs_PrgProf = new List<CS_PRM_ProgressiveProfileConfig__c >();
        CS_PRM_ProgressiveProfileConfig__c c1 = new CS_PRM_ProgressiveProfileConfig__c(Name='annualSales',FieldDataType__c='Picklist',FieldLabel__c='Your Account: Total Annual Sales',FieldSortOrder__c=29,MappedField__c='PRMAnnualSales__c',MappedObject__c='Account',CustomLabel__C='CLAPR15PRM088');
        CS_PRM_ProgressiveProfileConfig__c c2 = new CS_PRM_ProgressiveProfileConfig__c (Name='cTaxId',FieldDataType__c='Text',FieldLabel__c='Contact: Your Tax Id',FieldSortOrder__c=9,MappedField__c='PRMTaxId__c',MappedObject__c='Contact',CustomLabel__C='CLJUL15PRM028');
        cs_PrgProf.add(c1);
        cs_PrgProf.add(c2);
        insert cs_PrgProf;
        
        system.runas(u1){
            VFC_PRMProgressiveProfile vfPrf = new VFC_PRMProgressiveProfile();
            vfprf.componentId = comp.Id;
            AP_ProgressiveProfileCfgInfo prgInfo = new AP_ProgressiveProfileCfgInfo();
            prgInfo.accountId = PartnerAcc.Id;
            prgInfo.contactId=PartnerCon1.Id;
            prgInfo .companyFederatedId= '6372c85d-7d32-411e-8ab7-89e1f710ced4';
            prgInfo .federationId='f6b38ffb-4dbe-46b1-9b58-dfd62692f0ff';
            prgInfo .annualSales='99999';
            prgInfo .cTaxId='12345';
            Map<String,String> configList = new Map<String,String>();
            configList.put('annualSales','Picklist');
            configList.put('cTaxId','Text');
            VFC_PRMProgressiveProfile.updateProgProfile(prgInfo,configList,member.Id);
        }
            
    }
}