global without sharing class VFC_CaseTransfer{    
    global String lstofcases{get;set;}
    global VFC_CaseTransfer(ApexPages.StandardSetController stdsetcontroller){
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
        if (!Test.isRunningTest()) {      
            stdsetcontroller.addFields(new List<String>{'CaseNumber','Owner.Name','Team__r.Name','Status','Id','AccountableOrganization__c','Subject','Family_lk__c'});
        }
        lstofcases = JSON.serialize((List<Case>) stdsetcontroller.getSelected());
        System.debug(lstofcases);
    }
    global VFC_CaseTransfer(ApexPages.StandardController stdcontroller){
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');              
        if (!Test.isRunningTest()) {
            stdcontroller.addFields(new List<String>{'CaseNumber','Owner.Name','Team__r.Name','Status','Id','AccountableOrganization__c','Subject','Family_lk__c'});
        }      
        lstofcases = JSON.serialize(new List<Case>{(Case)stdcontroller.getRecord()});
    }
  

    @RemoteAction
    global static String updateRecords(String jsoncaserecords,boolean blnReleaseToTeam, boolean blnAssignDefaultTeam){
        System.debug(jsoncaserecords);        
        List<Case> CasesDeserialized = (List<Case>) JSON.deserialize(jsoncaserecords, List<Case>.class);        
        System.debug(CasesDeserialized);
        AP_Case_CaseHandler.blnAssignDefaultTeam = blnAssignDefaultTeam;  
        AP_Case_CaseHandler.blnReleaseToTeam = blnReleaseToTeam;
        AP_Case_CaseHandler.intAllowedIteration =2;
        List<Database.SaveResult> saveresults=Database.update(CasesDeserialized,false);
        return JSON.serializePretty(saveresults);
    }
}