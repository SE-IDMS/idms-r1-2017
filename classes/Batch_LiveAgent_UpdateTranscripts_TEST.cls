@isTest(SeeAllData=false) 
global class Batch_LiveAgent_UpdateTranscripts_TEST {
    
    public static Integer NB_RECORDS = 10;
    
	static testMethod void testBatch() {
        Map<Id, LiveChatButton> chatButtons = new Map<Id, LiveChatButton>([select Id, MasterLabel, Type from LiveChatButton where Type = 'Standard' limit 250]);
        Map<Id, LiveChatButton> chatInvites = new Map<Id, LiveChatButton>([select Id, MasterLabel, Type from LiveChatButton where Type != 'Standard' limit 250]);
        Map<Id, LiveChatDeployment> chatDeployments = new Map<Id, LiveChatDeployment>([select Id, MasterLabel from LiveChatDeployment limit 500]);
        
        LiveChatDeployment FranceDeployment = [select Id, MasterLabel from LiveChatDeployment where MasterLabel like 'FR%' limit 1];
        LiveChatDeployment NAMDeployment = [select Id, MasterLabel from LiveChatDeployment where MasterLabel like 'NAM%' limit 1];
        LiveChatDeployment UKIDeployment = [select Id, MasterLabel from LiveChatDeployment where MasterLabel like 'UKI%' limit 1];
        LiveChatDeployment VNDeployment = [select Id, MasterLabel from LiveChatDeployment where MasterLabel like 'Training%' limit 1];
        
        LiveChatButton FranceChatButton = [select Id, MasterLabel, Type from LiveChatButton where MasterLabel like 'FR%' limit 1];
        LiveChatButton NAMChatButton = [select Id, MasterLabel, Type from LiveChatButton where MasterLabel like 'NAM%' limit 1];
        LiveChatButton UKIChatButton = [select Id, MasterLabel, Type from LiveChatButton where MasterLabel like 'UKI%' limit 1];
        LiveChatButton VNChatButton = [select Id, MasterLabel, Type from LiveChatButton where MasterLabel like 'Training%' limit 1];
        
        Map<String, Country__c> countries = new Map<String, Country__c>();
        Integer j = 0;
        for (LiveChatDeployment dpl : chatDeployments.values()) {
            String countryOrRegionCode = dpl.MasterLabel.split('-')[0].trim();
            if (countryOrRegionCode.length() == 2 
                && countryOrRegionCode != 'FR' 
                && countryOrRegionCode != 'US' 
                && countryOrRegionCode != 'GB'
                && !countries.containsKey(countryOrRegionCode)) {
                Country__c newCountry = new Country__c(
                    Name = 'test-' + j,
                    CountryCode__c = countryOrRegionCode
                );
                countries.put(countryOrRegionCode, newCountry);
                j++;
            }
        }
        
        Country__c france = new Country__c(
        	Name = 'France',
            CountryCode__c = 'FR'
        );
        countries.put(france.CountryCode__c, france);
    
        Country__c usa = new Country__c(
        	Name = 'USA',
            CountryCode__c = 'US'
        );
        countries.put(usa.CountryCode__c, usa);
    
        Country__c uk = new Country__c(
        	Name = 'United Kingdom',
            CountryCode__c = 'GB'
        );
        countries.put(uk.CountryCode__c, uk);
        
        Country__c vn = new Country__c(
        	Name = 'Viet Nam',
            CountryCode__c = 'VN'
        );
        countries.put(vn.CountryCode__c, vn);
        
        insert countries.values();
        
        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;
        
        // Test 1
        // LiveChatButtonId is defined
        // SourceChatButtonId__c is not defined
        // Expected result: copy LiveChatButtonId to SourceChatButtonId__c
        LiveChatTranscript[] trancripts = new LiveChatTranscript[0];
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = chatDeployments.values()[Math.floor(Math.random()*chatDeployments.size()).intValue()].Id,
                LiveChatButtonId = chatButtons.values()[Math.floor(Math.random()*chatButtons.size()).intValue()].Id,
                ReportingChatButtonId__c = chatInvites.values()[Math.floor(Math.random()*chatInvites.size()).intValue()].Id,
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'ABC' + i
            );
            trancripts.add(t);
        }
        
        // Test 2
        // LiveChatButtonId is defined
        // ReportingChatButtonId__c is not defined
        // Expected result: copy LiveChatButtonId to ReportingChatButtonId__c
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = chatDeployments.values()[Math.floor(Math.random()*chatDeployments.size()).intValue()].Id,
                LiveChatButtonId = chatButtons.values()[Math.floor(Math.random()*chatButtons.size()).intValue()].Id,
                SourceChatButtonId__c = chatButtons.values()[Math.floor(Math.random()*chatButtons.size()).intValue()].Id,
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'BCDA' + i
            );
            trancripts.add(t);
        }
        for (Integer i=0; i<50; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = chatDeployments.values()[Math.floor(Math.random()*chatDeployments.size()).intValue()].Id,
                LiveChatButtonId = chatInvites.values()[Math.floor(Math.random()*chatInvites.size()).intValue()].Id,
                SourceChatButtonId__c = chatButtons.values()[Math.floor(Math.random()*chatButtons.size()).intValue()].Id,
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'BCDB' + i
            );
            trancripts.add(t);
        }
        
        // Test 3
        // If CountryCode__c is not defined, and ButtonId starts with a 2-char country code, CountryCode__c is populated with it
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = FranceDeployment.Id,
                LiveChatButtonId = FranceChatButton.Id,
                SourceChatButtonId__c = FranceChatButton.Id,
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'EFG' + i
            );
            trancripts.add(t);
        }
        
        // Test 4
        // If CountryCode__c is not defined, get the country name from location
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = NAMChatButton.Id,
                Location = 'null, null, United States',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'HIJ' + i
            );
            trancripts.add(t);
        }
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = UKIDeployment.Id,
                LiveChatButtonId = UKIChatButton.Id,
                SourceChatButtonId__c = UKIChatButton.Id,
                Location = 'null, null, United Kingdom',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'KLMA' + i
            );
            trancripts.add(t);
        }
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = VNDeployment.Id,
                LiveChatButtonId = VNChatButton.Id,
                Location = 'null, null, Vietnam',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'KLMB' + i
            );
            trancripts.add(t);
        }
        // test without country name is location field
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = VNDeployment.Id,
                LiveChatButtonId = VNChatButton.Id,
                Location = 'null, null',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'KLMC' + i
            );
            trancripts.add(t);
        }
        
        // Test 5
        // CountryCode__c is defined and used to populate Country__c
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = NAMChatButton.Id,
                Location = 'null, null, United States',
                CountryCode__c = 'US',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'NMO' + i
            );
            trancripts.add(t);
        }
        
        // Test 6
        // CountryCode__c is defined in lower case and used to populate Country__c
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = FranceDeployment.Id,
                LiveChatButtonId = FranceChatButton.Id,
                SourceChatButtonId__c = FranceChatButton.Id,
                Location = 'null, null, France',
                CountryCode__c = 'fr',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'PQR' + i
            );
            trancripts.add(t);
        }
        
        // Test 7
        // CountryCode__c is defined in lower case and used to populate Country__c
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = NAMChatButton.Id,
                Location = 'null, null, United States',
                CountryCode__c = 'ww',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'RST' + i
            );
            trancripts.add(t);
        }
        
        // Test 8
        // Country is Unknown and country code is undifined
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = NAMChatButton.Id,
                Location = 'null, null, Unknown',
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'UVW' + i
            );
            trancripts.add(t);
        }
        
        // Test 9
        // Chats assigned to Automated Process User
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = NAMChatButton.Id,
                CountryCode__c = 'de',
                OwnerId = System.Label.CLNOV16CCCLA01,
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'XYZ1' + i
            );
            trancripts.add(t);
        }
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = NAMChatButton.Id,
                CountryCode__c = 'fr',
                OwnerId = System.Label.CLNOV16CCCLA01,
                LiveChatVisitorId = visitor.Id,
                ChatKey = 'XYZ2' + i
            );
            trancripts.add(t);
        }
        // Test 10
        // Chats with 15-chat Id in Managing button id and Reporting button id text fields
        for (Integer i=0; i<NB_RECORDS; i++) {
            LiveChatTranscript t = new LiveChatTranscript(
                LiveChatDeploymentId = NAMDeployment.Id,
                LiveChatButtonId = NAMChatButton.Id,
                SourceChatButtonId__c = String.valueOf(NAMChatButton.Id).left(15),
                ReportingChatButtonId__c = String.valueOf(NAMChatButton.Id).left(15),
                RecalculateTranscriptFields__c = true,
                CountryCode__c = 'fr',
                LiveChatVisitorId = visitor.Id,
                ChatKey = '1234' + i
            );
            trancripts.add(t);
        }
        
        
        insert trancripts;
        
        Test.startTest();
        Batch_LiveAgent_Schedulable batch = new Batch_LiveAgent_Schedulable();
        batch.execute(null);
        Test.stopTest();
        
        Map<Id, LiveChatButton> allChatButtons = new Map<Id, LiveChatButton>();
        allChatButtons.putAll(chatButtons);
        allChatButtons.putAll(chatInvites);
        allChatButtons.put(FranceChatButton.Id, FranceChatButton);
        allChatButtons.put(NAMChatButton.Id, NAMChatButton);
        allChatButtons.put(UKIChatButton.Id, UKIChatButton);
        allChatButtons.put(VNChatButton.Id, VNChatButton);
        
        
        Map<Id, LiveChatDeployment> allChatDeployments = new Map<Id, LiveChatDeployment>();
        allChatDeployments.putAll(chatDeployments);
        allChatDeployments.put(FranceDeployment.Id, FranceDeployment);
        allChatDeployments.put(NAMDeployment.Id, NAMDeployment);
        allChatDeployments.put(UKIDeployment.Id, UKIDeployment);
        allChatDeployments.put(VNDeployment.Id, VNDeployment);
        
        Id[] transcriptIds = new Id[0];
        for (LiveChatTranscript t : trancripts) {
            transcriptIds.add(t.Id);
        }
        
        trancripts = [select Id, LiveChatDeploymentId, DeploymentName__c, LiveChatButtonId,
                      ReportingChatButtonId__c, ReportingChatButtonName__c,
                      SourceChatButtonId__c, SourceChatButtonName__c, ChatType__c,
                      ChatKey, ChatKey__c, Country__c, CountryCode__c, RegionCode__c, 
                      Application__c, OwnerId,
                      RecalculateTranscriptFields__c
                      from LiveChatTranscript where Id in : transcriptIds];
        
        LiveChatTranscript[]  transcriptsWithChatKey = new LiveChatTranscript[0];
        for (LiveChatTranscript t : trancripts) {
            
            System.assertEquals(allChatDeployments.get(t.LiveChatDeploymentId).MasterLabel, t.DeploymentName__c);
            
            // ChatKey should always be copied to ChatKey__c
            if (t.ChatKey != null) {
            	System.assertEquals(t.ChatKey, t.ChatKey__c);
            }
            
            // SourceChatButtonId__c is a Chat Button, Name of the chat button is copied in SourceChatButtonName__c
            System.assertEquals(allChatButtons.get(t.SourceChatButtonId__c).MasterLabel, t.SourceChatButtonName__c);
            
            // ReportingChatButtonId__c can be a chat invite or a chat button
            // The name should be copied in ReportingChatButtonName__c
            // And the right type as well
            if (t.ChatKey.startsWith('BCDA')) {
                System.assertEquals(chatButtons.get(t.ReportingChatButtonId__c).MasterLabel, t.ReportingChatButtonName__c);
                System.assertEquals(System.Label.CLSEP16CCCLA05, t.ChatType__c);
            } else if (t.ChatKey.startsWith('BCDB')) {
                System.assertEquals(chatInvites.get(t.ReportingChatButtonId__c).MasterLabel, t.ReportingChatButtonName__c);
                System.assertEquals(System.Label.CLSEP16CCCLA06, t.ChatType__c);
            }
            
            // Test 1
            // When LiveChatButtonId is defined and SourceChatButtonId__c is not, SourceChatButtonId__c should have the same value
            if (t.ChatKey.startsWith('ABC')) {
                System.assertEquals(t.LiveChatButtonId, t.SourceChatButtonId__c);
            }
            
            // Test 2
            // When LiveChatButtonId is defined and ReportingChatButtonId__c is not, ReportingChatButtonId__c should have the same value
            if (t.ChatKey.startsWith('BCD')) {
                System.assertEquals(t.LiveChatButtonId, t.ReportingChatButtonId__c);
            }
            
            // Test 3
            // If CountryCode__c is not defined chat button name starts with a 2-char country code
            if (t.ChatKey.startsWith('EFG')) {
                System.assertEquals('FR', t.RegionCode__c);
                System.assertEquals('FR', t.CountryCode__c);
                System.assertEquals(france.Id, t.Country__c);
            }
            
            // Test 4
            // CountryCode__c is defined according to the location field
            // Test 5 & 7 & 8
            // Country__c is populated
            if (t.ChatKey.startsWith('HIJ') || t.ChatKey.startsWith('MNO') || t.ChatKey.startsWith('RST') || t.ChatKey.startsWith('UVW')) {
                System.assertEquals('NAM', t.RegionCode__c);
                System.assertEquals('US', t.CountryCode__c);
                System.assertEquals(usa.Id, t.Country__c);
            }
            if (t.ChatKey.startsWith('KLMA')) {
                System.assertEquals('UKI', t.RegionCode__c);
                System.assertEquals('GB', t.CountryCode__c);
                System.assertEquals(uk.Id, t.Country__c);
            }
            if (t.ChatKey.startsWith('KLMB')) {
                System.assertEquals('VN', t.RegionCode__c);
                System.assertEquals('VN', t.CountryCode__c);
                System.assertEquals(vn.Id, t.Country__c);
            }
            if (t.ChatKey.startsWith('KLMC')) {
                System.assertEquals(null, t.RegionCode__c);
                System.assertEquals(null, t.CountryCode__c);
                System.assertEquals(null, t.Country__c);
            }
            // Test 6
            // Country__c is populated
            if (t.ChatKey.startsWith('PQR')) {
                System.assertEquals('FR', t.RegionCode__c);
                System.assertEquals('FR', t.CountryCode__c);
                System.assertEquals(france.Id, t.Country__c);
            }
            // Test 9
            // German missed chat are assigned to System.Label.CLNOV16CCCLA01
            if (t.ChatKey.startsWith('XYZ1')) {
                System.assertEquals('DE', t.CountryCode__c);
                System.assertEquals(System.Label.CLNOV16CCCLA01, t.OwnerId);
            }
            // German missed chat are assigned to Current User
            if (t.ChatKey.startsWith('XYZ2')) {
                System.assertEquals('FR', t.CountryCode__c);
                System.assertEquals(UserInfo.getUserId(), t.OwnerId);
            }
            
            // Test 10
            // The name should be copied in ReportingChatButtonName__c
            if (t.ChatKey.startsWith('1234')) {
                // 15 char Ids have been updated to 18 chat ids
                System.assertEquals(18, t.ReportingChatButtonId__c.length());
                System.assertEquals(18, t.SourceChatButtonId__c.length());
                System.assertEquals(NAMChatButton.MasterLabel, t.ReportingChatButtonName__c);
                System.assertEquals(NAMChatButton.MasterLabel, t.SourceChatButtonName__c);
            }
        }
    }
}