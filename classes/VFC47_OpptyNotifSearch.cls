public class VFC47_OpptyNotifSearch
{
    private static boolean debug = Boolean.valueOf(Label.CLAPR15SRV63);
    Public ID OppNotificationId;
    Public ApexPages.StandardController standardController;
    public OpportunityNotification__c oppNotif{ get; private set;}

    // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL00029);

    // collection of opportunities matching the search criteria
    public List<Opportunity> opportunities {get; private set;}

    public Boolean disableConvert{get;set;}
    public Boolean disableDuplicate{get;set;} 
    public static Savepoint sp;          
    public Boolean hasNext {
        get {
            return (opportunities == null || (opportunities != null && opportunities.size() > searchMaxSize));
        }
        set;
    }

    public VFC47_OpptyNotifSearch(ApexPages.StandardController controller) {
        standardController = controller;
        OppNotificationId = controller.getId();
        disableConvert = false;
        disableDuplicate = true;
    }

    // returns the search string to be used in SOSL query
    public String getSearchString() {
        String strSearch = 'FIND {*'+Utils_Methods.escapeForSOSL(oppNotif.Name)+'*} IN NAME FIELDS ';
        strSearch += ' RETURNING Opportunity(Id,Name,CountryOfDestination__c,Location__c,OwnerId,StageName,CloseDate,Amount,AccountId,ReferenceCase__c';
        List<String> whereConditions = new List<String>();

        if(oppNotif.CountryDestination__r.id != null)
                whereConditions.add('CountryOfDestination__c=\''+oppNotif.CountryDestination__r.id+'\'');

        if(whereConditions.size() > 0) {
                strSearch += ' WHERE ';
                for(String condition : whereConditions) {
                        strSearch += condition + ' AND ';
                }
                // delete last 'AND'
                strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
        }
        strSearch += ' ORDER BY Name';
        strSearch += ' LIMIT ' + (Integer)(searchMaxSize+1);
        strSearch += ')';
        return strSearch;
    }

        // execute the SOSL query
    public PageReference doSearchOpportunity(){

    //updated for dec release to query WorkOrder__r.SVMXC__Contact__r.id
    List<OpportunityNotification__c> OppNotifList = [SELECT ID, Name,Case__c, AccountForConversion__r.id, AccountForConversion__r.Name, 
                                                        AccountForConversion__r.OwnerId, CountryDestination__r.id, CountryDestination__r.Name, 
                                                        LeadingBusinessBU__c, OpportunityDetector__r.id, CurrencyIsoCode, status__c, Amount__c, 
                                                        CloseDate__c, IncludedinForecast__c, OpportunityCategory__c, OpportunityScope__c, OpportunityType__c, 
                                                        Stage__c, Description__c, TECH_Locked__c, OpportunityOwner__r.id, WorkOrder__r.SVMXC__Contact__r.id, 
                                                        WorkOrder__r.Name, RejectionReason__c, From_Mobile__c, RecordType.Name, SVMXPriority__c, From_Customer_Portal__c
                                                        FROM OpportunityNotification__c WHERE ID=:OppNotificationId LIMIT 1];
   
    //end of update
        if(OppNotifList.size()>= 1)
          oppNotif = OppNotifList.get(0);

        if(oppNotif.AccountForConversion__r.id == null){
          disableConvert = true;
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, 'Please select a valid Account before conversion.')); 
           
          return null;
        }
        //else if(oppNotif.Amount__c == null){ 
        else if(oppNotif.Amount__c == null || oppNotif.Amount__c==0){ //#Def-7312
          disableConvert = true;
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, Label.CLAPR15SRV51)); 
           
          return null;
        }

        // check for the opportunity name length
        if(oppNotif.Name == null || oppNotif.Name.trim().length() < 3) {
                oppNotif.Name.addError(System.Label.CL00028);
                return null;
        }
     // Start May Release 2013 BR-2767
     // Added condition for mobile vs non-mobile - Scott Fawcett 9/29/2015
     if(oppNotif.OpportunityScope__c== null && oppNotif.RecordType.Name != 'Mobile') 
     {
        disableConvert = true;
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide opportunity scope value.'));   
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, System.Label.CLOCT13CCC19));
        //oppNotif.OpportunityScope__c.addError('Please provide opportunity scope value');
        return null;
     }//End May Release 2013 BR-2767
        
        
        String s = getSearchString();
        Utils_SDF_Methodology.log('START search: ', s);
        Utils_SDF_Methodology.startTimer();
        try {
                opportunities = (List<Opportunity>)search.query(s).get(0);

                if(opportunities.size() > 0)
                  disableDuplicate=false;

        }catch(Exception ex){
                ApexPages.addMessages(ex);
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return null;
    }
    
    // Updated by Y. Tisserand for April'15
    // Creation of opportunity line
    // Attached of installed product at line level
    // Opportunity Team
    // Opportunity split
    public PageReference convert()
    {    
      debug('START convert');
      sp = Database.setSavepoint();    //created a savepoint for roll back  updated for dec release
      Opportunity opportunity = new Opportunity();
      opportunity.Name= oppNotif.Name;
      opportunity.OpportunityDetector__c = oppNotif.OpportunityDetector__r.id;
      opportunity.CurrencyIsoCode = oppNotif.CurrencyIsoCode;
      opportunity.AccountId = oppNotif.AccountForConversion__r.id;
      opportunity.OpptyPriorityLevel__c = oppNotif.SVMXPriority__c;
      
      // we only want to set this field if the opportunity notification is for non mobile types. 
      if (oppNotif.RecordType.Name != 'Mobile')
          opportunity.TECH_CrossProcessConversionAmount__c = oppNotif.Amount__c;
          
      opportunity.CloseDate = oppNotif.CloseDate__c;
      opportunity.CountryOfDestination__c = oppNotif.CountryDestination__c;
      opportunity.IncludedinForecast__c = oppNotif.IncludedinForecast__c;
      opportunity.LeadingBusiness__c = oppNotif.LeadingBusinessBU__c;
      opportunity.OpportunityCategory__c = oppNotif.OpportunityCategory__c;
      opportunity.OpportunityScope__c = oppNotif.OpportunityScope__c;
      opportunity.OpptyType__c = oppNotif.OpportunityType__c;
      opportunity.StageName = oppNotif.Stage__c;
      opportunity.Description = oppNotif.Description__c;
      opportunity.TECH_CreatedFromServices__c = true;
      opportunity.BusinessMix__c = oppNotif.LeadingBusinessBU__c;
      opportunity.ReferenceCase__c = oppNotif.Case__c; //OCT 2013 release
      
      if(oppNotif.WorkOrder__r.Name != null){
        opportunity.Work_Order_Number__c = oppNotif.WorkOrder__r.Name;
        // Added for May 15 Release on 07/05/2015 ******BR-7660 ****
        opportunity.WorkOrder__c = oppNotif.WorkOrder__c;
      }
      
      // by default, opportunity source = Service Visit  
      opportunity.OpportunitySource__c = Label.CL00680;
       
      // For CCC, BR-3441
      if(oppNotif.Case__c != null){
        opportunity.OpportunitySource__c = Label.CLOCT13CCC10;
      }
      
      // For bFS Customer Portal, BR-9943
      if(oppNotif.From_Customer_Portal__c){
        opportunity.OpportunitySource__c = Label.CLJUN16SRV10;
      }
           
      List<AccountTeamMember> accountTeamList = [select UserId from AccountTeamMember where AccountId = :oppNotif.AccountForConversion__c and TeamMemberRole = 'SSC'];  
        
      if(oppNotif.OpportunityOwner__r.id != null){  
          opportunity.OwnerId = oppNotif.OpportunityOwner__r.id;
      }else if(accountTeamList != null && accountTeamList.size() > 0){
          AccountTeamMember serviceSalesContributor = accountTeamList.get(0);
          opportunity.OwnerId = serviceSalesContributor.UserId;
      }else{
          opportunity.OwnerId = oppNotif.AccountForConversion__r.OwnerId;
      }
      
      debug('Opportunity header to be saved: '+opportunity);
      PageReference opportunityPage;
      database.saveResult sv = Database.Insert(opportunity, false);
      
      if(sv.issuccess())
      {
          debug('START adding opportunity team');
          List<Team_Members_Opportunity_Notification__c> teamMembersOpportunityNotificationList = [select Detector__c from Team_Members_Opportunity_Notification__c where Opportunity_Notification__c = :oppNotif.id];
          List<OpportunityTeamMember> opportunityTeam = new List<OpportunityTeamMember>();
          List<OpportunityTeamMember> opportunitySplit = new List<OpportunityTeamMember>();
          Set<ID> uniqueDetectorSet = new Set<ID>();
          
          // Only if not coming from myFS portal
          if(!oppNotif.From_Customer_Portal__c && opportunity.OpportunityDetector__c != null){
            //debug('Adding primary detector to  opportunity team: '+opportunity.OpportunityDetector__c);
            OpportunityTeamMember opportunityTeamMember = new OpportunityTeamMember ();
            opportunityTeamMember.OpportunityID = opportunity.id;
            opportunityTeamMember.UserID = opportunity.OpportunityDetector__c;
            opportunityTeamMember.TeamMemberRole = Label.CLAPR15SRV64;
            opportunityTeam.add(opportunityTeamMember);
            opportunitySplit.add(opportunityTeamMember);
            uniqueDetectorSet.add(opportunity.OpportunityDetector__c);  
          }
          
          if(UserInfo.getUserId() != null){
            debug('Adding convertor to  opportunity team: '+UserInfo.getUserId());
            OpportunityTeamMember opportunityTeamMember = new OpportunityTeamMember ();
            opportunityTeamMember.OpportunityID = opportunity.id;
            opportunityTeamMember.UserID = UserInfo.getUserId() ;
            opportunityTeamMember.TeamMemberRole = 'SSC';
            //opportunityTeamMember.OpportunityAccessLevel = 'Edit';
            opportunityTeam.add(opportunityTeamMember);
            //uniqueDetectorSet.add(UserInfo.getUserId());  
          }
             
          if(teamMembersOpportunityNotificationList != null && teamMembersOpportunityNotificationList .size() > 0){
             
            for(Team_Members_Opportunity_Notification__c teamMembersOpportunityNotification:teamMembersOpportunityNotificationList){
              
                if(!uniqueDetectorSet.contains(teamMembersOpportunityNotification.detector__c)){
                  debug('Adding secondary detectors to opportunity team: '+teamMembersOpportunityNotification);
                  OpportunityTeamMember opportunityTeamMember = new OpportunityTeamMember ();
                  opportunityTeamMember.OpportunityID = opportunity.id;
                  opportunityTeamMember.UserID = teamMembersOpportunityNotification.detector__c;
                  opportunityTeamMember.TeamMemberRole = Label.CLAPR15SRV64;
                  opportunityTeam.add(opportunityTeamMember);
                  opportunitySplit.add(opportunityTeamMember);
                  uniqueDetectorSet.add(teamMembersOpportunityNotification.detector__c); 
                }
            }
          }
          
          debug('Opportunity Team list to be saved: '+opportunityTeam);
          List<Database.SaveResult> oppTeamSaveResult = Database.Insert(opportunityTeam, false);
          
          // Add in new functionality to create Opportunity Lines within conversion as opposed to allowing the OpportunityAfterInsert.trigger/AP05_OpportunityTrigger.cls perform the work
          
          if (oppNotif.RecordType.Name == 'Mobile') {
            
                List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
                
                // need to query the related Interest Lines to Opportunity Notification and create Opportunity Product Lines
                List<InterestOnOpportunityNotif__c> oppNotifInterestList = [SELECT Leading_Business_BU__c, Interest__c, Type__c
                                                                            FROM InterestOnOpportunityNotif__c
                                                                            WHERE OpportunityNotification__c =:oppNotif.id];
                
                // get custom settings of mappings
                List<Opportunity_Notif_Mobility_Mapping__c> oppNotifMappingSettingsList = Opportunity_Notif_Mobility_Mapping__c.getAll().values(); 
                
                Map<String, Opportunity_Notif_Mobility_Mapping__c> oppNotifMappingLookup = new Map<String, Opportunity_Notif_Mobility_Mapping__c>();
                
                for (Opportunity_Notif_Mobility_Mapping__c onmm : oppNotifMappingSettingsList) {
                    String key = onmm.Product_BU__c + onmm.Type__c + onmm.Interest__c;
                    oppNotifMappingLookup.put(key, onmm);
                }
                
                // todo check if no interests have been specified and raise an error.
                // todo what to do if one of the three required input fields for the key are missing
                
                // determine Amount to be split evenly between all new Opportunity Lines
                Decimal oppLineAmount = 0;
                if (oppNotif.Amount__c > 0 && oppNotifInterestList.size() > 0)
                    oppLineAmount = oppNotif.Amount__c / oppNotifInterestList.size();
                
                for (InterestOnOpportunityNotif__c interest : oppNotifInterestList) {
                    
                     String lookupKey = interest.Leading_Business_BU__c + interest.Type__c + interest.Interest__c;
                        
                     OPP_ProductLine__c prodLine = new OPP_ProductLine__c();
                     prodLine.Amount__c = oppLineAmount;    
                     prodLine.Opportunity__c = opportunity.Id;
                     prodLine.CurrencyISOCode = opportunity.CurrencyISOCode;
                     prodLine.LineStatus__c = Label.CLSEP12PRM20;
                     
                     System.debug('########lookupKey' + lookupKey);
                     System.debug('########oppNotifMappingLookup' + oppNotifMappingLookup);
                     
                     if (!oppNotifMappingLookup.containsKey(lookupKey)) {
                         Database.rollback(sp); //rolling back in case of any exception  
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'The combination of Leading BU, Type & Interest were not found in the Custom Setting.  (' + lookupKey + ')  Please contact your Administrator to ensure its added.'));                    
                         opportunity.clear();
                         break;
                     
                     }
                     
                     prodLine.ProductFamily__c = oppNotifMappingLookup.get(lookupKey).Product_Family__c;
                     prodLine.ProductBU__c = oppNotifMappingLookup.get(lookupKey).Opportunity_Line_Product_BU__c;
                     prodLine.ProductLine__c = oppNotifMappingLookup.get(lookupKey).Product_Line__c;
                     prodLine.Family__c=oppNotifMappingLookup.get(lookupKey).family__c;

                     
                     prodLines.add(prodLine);
                }
                
                //Inserts the Opportunity Line
                List<Database.SaveResult> prodLinesInsert = Database.insert(prodLines, false);
          }
          
          // end new functionality to create Opportunity Lines for Mobility functionality
          
          debug('START to give Write access to convertor');
          List<OpportunityShare> shares = [select Id, OpportunityAccessLevel, RowCause from OpportunityShare where OpportunityId = :opportunity.id and RowCause = 'Team' and UserOrGroupId = :UserInfo.getUserId()];
          
          if(shares != null){
              
              for (OpportunityShare share : shares){  
                share.OpportunityAccessLevel = 'Edit';
              }
              
              update shares;
          }
          
          debug('START adding opportunity split');
          if(opportunitySplit != null && opportunitySplit.size() > 0){
              List<OpportunitySplit> opportunitySplits = new List<OpportunitySplit>();
              Double splitPercentage = 0;
              
              
              if (oppNotif.RecordType.Name != 'Mobile') {
                if(opportunity.TECH_CrossProcessConversionAmount__c != 0  && opportunity.TECH_CrossProcessConversionAmount__c != null){
                    splitPercentage = (opportunity.TECH_CrossProcessConversionAmount__c/opportunitySplit.size())/opportunity.TECH_CrossProcessConversionAmount__c*100;
                }
              } else {
                // splitPercent based on new mobility method 
                splitPercentage = (oppNotif.Amount__c/opportunitySplit.size())/oppNotif.Amount__c*100;
              }
              for(OpportunityTeamMember opportunityTeamMember:opportunitySplit){
                  OpportunitySplitType splitType = [select id from OpportunitySplitType where MasterLabel = 'Overlay'];
                  OpportunitySplit split = new OpportunitySplit();
                  split.OpportunityID = opportunity.id; 
                  split.SplitTypeID = splitType.id;
                  split.SplitOwnerID = opportunityTeamMember.userID;
                  split.SplitPercentage = splitPercentage;
                  opportunitySplits.add(split);
              }
              
              debug('Opportunity Split list to be saved: '+opportunitySplits);
              List<Database.SaveResult> oppSplitSaveResult = Database.Insert(opportunitySplits, false);
              debug('Opportunity Split SaveResult: '+oppSplitSaveResult);
          }
                 
          debug('START retrieving opportunity line');
          // only want to add Installed Products against one of the Opportunity Lines, hence the update to LIMIT it to 1
          //List<OPP_ProductLine__c> oplineList = [select id from OPP_ProductLine__c where Opportunity__c = :opportunity.id];
          List<OPP_ProductLine__c> oplineList = [select id from OPP_ProductLine__c where Opportunity__c = :opportunity.id LIMIT 1];
          
          if(oplineList != null && oplineList.size() == 1){
              OPP_ProductLine__c opLine = oplineList.get(0);
              debug('Current opportunity line: '+opLine);
              debug('START adding installed product at opportunity line level');
              List<InstalledProductOnOpportunityNotif__c> ipONOPPNotList = [select id, InstalledProduct__c,OpportunityNotification__c from InstalledProductOnOpportunityNotif__c where OpportunityNotification__c =:oppNotif.id ];
              List<Installed_product_on_Opportunity_Line__c> iponop = new List<Installed_product_on_Opportunity_Line__c>();
             
              for(InstalledProductOnOpportunityNotif__c ipONOPPNot :ipONOPPNotList ){
             
                if(ipONOPPNot.InstalledProduct__c != null){
                  Installed_product_on_Opportunity_Line__c obj = new Installed_product_on_Opportunity_Line__c();
                  obj.Installed_Product__c  = ipONOPPNot.InstalledProduct__c;
                  obj.Opportunity_Line__c = opline.id;
                  iponop.add(obj);
                }
             }
             debug('Installed products to be saved: '+iponop);
             
             // added May 12 2016 to adjust quantity on opLine to be equal to related IPs installed if greater than 0
             // so that VR PRL_VR03_AssIPandOppLineQuantity is not violated
           /*  if (iponop.size() > 1) {
                 
                 opLine.Quantity__c = iponop.size();
                 update opLine;
             }*/
             
             insert iponop;
          }
              
          /*debug('START creating opportunity line');
          List<OPP_Product__c>  oppProduct =[select id,BusinessUnit__c,Family__c,ProductFamily__c,ProductLine__c from OPP_Product__c where family__c = ''and ProductFamily__c = '' and ProductLine__c = :oppNotif.OpportunityScope__c];
          
          // Create opportunity line based on opp line scope
          OPP_ProductLine__c opline = new OPP_ProductLine__c();
          opline.Quantity__c = 1;
          opline.Amount__c = oppNotif.Amount__c;
          opline.CurrencyIsoCode = oppNotif.CurrencyIsoCode;
          opline.LineClosedate__c = oppNotif.CloseDate__c;
          opline.LineStatus__c = 'Pending';
          opline.IncludedinForecast__c = oppNotif.IncludedinForecast__c;
          opline.Opportunity__c = opportunity.id;
          
          if(oppProduct != null && oppProduct.size() == 1){
            OPP_Product__c oppprod= oppProduct.get(0);
            opline.ProductBU__c = oppprod.BusinessUnit__c;
            opline.ProductFamily__c = oppprod.ProductFamily__c;
            opline.ProductLine__c = oppprod.ProductLine__c;
          }
          
          debug('Opportunity Line to be saved: '+opline);
          database.saveResult oppLineSaveResult = Database.Insert(opline, false);
          */
          
          // look for any attachments related to the Opportunity Notification and "re-parent" them to the Opportunity             
          List<Attachment> attachmentsToInsert = new List<Attachment>();
          List<Attachment> attachmentsToDelete = new List<Attachment>();

          Attachment tempAtt;

          for (Attachment attachment: [SELECT SystemModstamp, ParentId, OwnerId, Name, LastModifiedDate, LastModifiedById, 
                IsPrivate, IsDeleted, Id, Description, CreatedDate, CreatedById, ContentType, BodyLength, Body 
                FROM Attachment WHERE parentId=:oppNotif.Id]) {
                    
            //tempAtt = attachment.clone(false,false);
            //tempAtt.parentId = opportunity.id;
            //attachmentstoInsert.add(tempAtt);
            // Y. Tisserand 03/11/16: Fix to avoid Heap size limit
            attachmentsToInsert.add(new Attachment(Name = attachment.Name, Body = attachment.Body, ParentId = opportunity.id));  
            attachmentsToDelete.add(attachment);
            

          }   
          insert attachmentsToInsert;
          delete attachmentsToDelete;
          
          // Mark Opp Notif as converted
          oppNotif.status__c = System.Label.CL00520;
          oppNotif.OpportunityLink__c = opportunity.id; //DEF-2986 OCT13 Release
          database.saveResult updt = Database.Update(oppNotif,false);  
          
          if(!updt.isSuccess()){
            Database.rollback(sp); //rolling back in case of any exception  
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,updt.getErrors()[0].getMessage()));                    
            opportunity.clear();
          }
          
          // Manage VCP creation
          if(oppNotif.WorkOrder__r.SVMXC__Contact__r.id !=null){
              OPP_ValueChainPlayers__c valuechainPlayer = new OPP_ValueChainPlayers__c();
              valuechainPlayer.OpportunityName__c=opportunity.id;
              valuechainPlayer.Contact__c=oppNotif.WorkOrder__r.SVMXC__Contact__r.id;
              valuechainPlayer.ContactRole__c=label.CL00657;
              database.saveResult svr = Database.Insert(valuechainPlayer, false);
              
              if(!svr.issuccess()){           
                  Database.rollback(sp); //rolling back in case of any exception  
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,svr.getErrors()[0].getMessage()));                    
                  opportunity.clear();
              }      
          }
          
          // April Release 2014 Started
          // Creaion of InstalledProductOnOpportunity__c
          /*
          List<InstalledProductOnOpportunityNotif__c> ipONOPPNotList = new List<InstalledProductOnOpportunityNotif__c>();
          List<InstalledProductOnOpportunity__c> ipONOPPList = new List<InstalledProductOnOpportunity__c>();
          try{
              ipONOPPNotList = [select id,InstalledProduct__c,OpportunityNotification__c from InstalledProductOnOpportunityNotif__c where OpportunityNotification__c =:oppNotif.id ];
              if(ipONOPPNotList != null && ipONOPPNotList.size()>0){
                    
                    for(InstalledProductOnOpportunityNotif__c obj:ipONOPPNotList)
                    {
                        InstalledProductOnOpportunity__c iponopp = new InstalledProductOnOpportunity__c();
                        iponopp.InstalledProduct__c = obj.InstalledProduct__c;
                        iponopp.Opportunity__c = opportunity.id;
                        ipONOPPList.add(iponopp);
                    }
              }
             if(ipONOPPList != null && ipONOPPList.size()>0) 
             List<Database.SaveResult> saveResults= Database.Insert(ipONOPPList, false);
          
          }
          catch(Exception ex){
              //    Database.rollback(sp); //rolling back in case of any exception
              //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sv.getErrors()[0].getMessage()));
             // opportunity.clear();
          }*/
          // April Release 2014 Ended 
          
        /*<BR-3441>
          Create Opportunity Line*/
          /*System.Debug('######## Opp product Line ->');
          OPP_ProductLine__c opportunityLine = new OPP_ProductLine__c();
          opportunityLine.Opportunity__c = opportunity.id;
          opportunityLine.Amount__c = oppNotif.Amount__c;
          opportunityLine.Quantity__c = 1;
          opportunityLine.ProductLine__c=oppNotif.OpportunityScope__c;
          
          database.saveResult svLine = Database.Insert(opportunityLine, false);
          if(!svLine.isSuccess()) {
              Database.Error err = svLine.getErrors()[0];
              System.Debug('######## Opp. Line Creation Error : '+err.getStatusCode()+' '+err.getMessage());
          }*/
         //</BR-3441>    
          
      } 
      else
      {    
          Database.rollback(sp); //rolling back in case of any exception
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sv.getErrors()[0].getMessage()));
          opportunity.clear();
      }  
      if(opportunity.id!=null)   
      opportunityPage= new ApexPages.StandardController(opportunity).view();
       //End OF update      
      return opportunityPage;
    }

    public PageReference markAsDuplicate()
    {
      
      
      if(oppNotif.RejectionReason__c == null){
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning, 'Please enter a rejection reason.')); 
           
          return null;
      }else if(oppNotif.RejectionReason__c == 'Duplicate'){
          oppNotif.status__c = System.Label.CL00521;
      }else{
          oppNotif.status__c = 'Cancelled';
      }
      /*    Modified By ACCENTURE
        Modifed for December Release as per SFDC Comments
        Modified Date: 25/11/2011
      */ 
      database.saveResult updt = Database.Update(oppNotif,false);        
      if(!updt.isSuccess())
      {
          Database.rollback(sp); //rolling back in case of any exception  
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,updt.getErrors()[0].getMessage()));                    
      }
      //End of Modification
      PageReference oppNotifPage = new ApexPages.StandardController(oppNotif).save();
      return oppNotifPage ;
    }
    
    public pagereference returnToOppNotification(){
        PageReference OppNotifiPage = new PageReference('/' + oppNotif.Id);
        return OppNotifiPage;
    }
    
     private static void debug(String aMsg) {
        if(debug) {
            System.debug('### '+aMsg);
        }
    }
    
   

}