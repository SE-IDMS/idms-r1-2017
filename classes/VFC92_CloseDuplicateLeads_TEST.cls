/**
    24/Apr/2012     Srinivas Nallapati  Mkt June    Test class for the VFC92_CloseDuplicateLeads  controller
 */
@isTest
private class VFC92_CloseDuplicateLeads_TEST 
{

    static testMethod void VFP92_Test() 
    {
        Database.DMLOptions objDMLOptions = new Database.DMLOptions();
        objDMLOptions.DuplicateRuleHeader.AllowSave = true;
        
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
         user.BypassWF__c = false;
         Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
         Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
         system.runas(user)
         {
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            Acc.Country__c=country.id;
            Database.SaveResult accountSaveResult = Database.insert(Acc,objDMLOptions); 
             
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            Database.SaveResult contactSaveResult1 = Database.insert(con1,objDMLOptions);
            
            Contact con2 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            Database.SaveResult contactSaveResult2 = Database.insert(con2,objDMLOptions);
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            
            List<Lead> leads = new List<Lead>();
            for(Integer i=0;i<20;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                if(math.mod(i,2) != 0)
                {
                   lead1.Contact__C = con2.id; 
                   lead1.Account__C= Acc.id;
                }   
                else
                    lead1.LeadContact__C = LC1.id;
                leads.add(lead1);   
            }
            List<Id> leadsId = new List<Id>();
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            
            for(Database.SaveResult sr: leadsInsert)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                } 
                else
                   leadsId.add(sr.Id);
            }
            
            
            leads.clear();
            leads = [select Status, subStatus__c from lead where Id in: leadsId];
            
            leads[0].status = '3 - Closed Duplicate';
            
            test.startTest();
            update leads[0];
          /*  
            leads[1].OpportunityType__c = 'Tests';                
            leads[1].ClassificationLevel1__c='LC';
            leads[1].LeadingBusiness__c='BD';
            leads[1].SuspectedValue__c=100;
            leads[1].ZipCode__c='123789';
            leads[1].OpportunityFinder__c ='Lead Agent';
            leads[1].City__c='US';
            leads[1].Street__c = 'testStreet';
            leads[1].substatus__c = null;
            update leads[1];
            system.debug('XXXXXXXXXXX '+leads[1]);
            Database.LeadConvert leadConv1 = new database.LeadConvert();
            leadConv1.setLeadId(leads[1].id);
            System.debug('Lead Record : '+leads[1]);
            leadConv1.setConvertedStatus('3 - Closed - Opportunity Created');
            Database.LeadConvertResult lcr1 = Database.convertLead(leadConv1);
            
            
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(leads[0]);
            VFC92_CloseDuplicateLeads searchpage1 = new VFC92_CloseDuplicateLeads(sc1);
            
            searchpage1.checkOpenLeads();
            for(VFC92_CloseDuplicateLeads.LeadWrapper lw : searchpage1.lstLeadWraps){
                lw.isSelected = true;
            }
            searchpage1.closeSelected();
            searchpage1.goToLeadDetailPage();
                                             
            ApexPages.StandardController sc2 = new ApexPages.StandardController(leads[1]);
            VFC92_CloseDuplicateLeads searchpage2 = new VFC92_CloseDuplicateLeads(sc2);
            
            searchpage2.checkOpenLeads();
            for(VFC92_CloseDuplicateLeads.LeadWrapper lw : searchpage2.lstLeadWraps){
                lw.isSelected = true;
            }
            searchpage2.closeSelected();
            searchpage2.goToLeadDetailPage();
            */
                       
            test.stopTest();
         }
       }
    }//End of method
    
    static testMethod void VFP92_Test2() 
    {
        Database.DMLOptions objDMLOptions = new Database.DMLOptions();
        objDMLOptions.DuplicateRuleHeader.AllowSave = true;
        
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
         user.BypassWF__c = false;
         Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
         Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
         system.runas(user)
         {
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            Acc.Country__c=country.id;
            Database.SaveResult accountSaveResult = Database.insert(Acc,objDMLOptions); 
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            Database.SaveResult contactSaveResult1 = Database.insert(con1,objDMLOptions);
            
            Contact con2 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            Database.SaveResult contactSaveResult2 = Database.insert(con2,objDMLOptions);
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            
            List<Lead> leads = new List<Lead>();
            for(Integer i=0;i<20;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                if(math.mod(i,2) != 0)
                {
                   lead1.Contact__C = con2.id; 
                   lead1.Account__C= Acc.id;
                }   
                else
                    lead1.LeadContact__C = LC1.id;
                leads.add(lead1);   
            }
            List<Id> leadsId = new List<Id>();
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            
            for(Database.SaveResult sr: leadsInsert)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                } 
                else
                   leadsId.add(sr.Id);
            }
            
            test.StartTest();
            leads.clear();
            leads = [select Status, subStatus__c from lead where Id in: leadsId];
            leads[1].OpportunityType__c = 'Tests';                
            leads[1].ClassificationLevel1__c='LC';
            leads[1].LeadingBusiness__c='BD';
            leads[1].SuspectedValue__c=100;
            leads[1].ZipCode__c='123789';
            leads[1].OpportunityFinder__c ='Lead Agent';
            leads[1].City__c='US';
            leads[1].Street__c = 'testStreet';
            leads[1].substatus__c = null;
            update leads[1];
            system.debug('XXXXXXXXXXX '+leads[1]);
            Database.LeadConvert leadConv1 = new database.LeadConvert();
            leadConv1.setLeadId(leads[1].id);
            System.debug('Lead Record : '+leads[1]);
            leadConv1.setConvertedStatus('3 - Closed - Opportunity Created');
            Database.LeadConvertResult lcr1 = Database.convertLead(leadConv1);
            
            
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(leads[0]);
            VFC92_CloseDuplicateLeads searchpage1 = new VFC92_CloseDuplicateLeads(sc1);
            
            searchpage1.checkOpenLeads();
            for(VFC92_CloseDuplicateLeads.LeadWrapper lw : searchpage1.lstLeadWraps){
                lw.isSelected = true;
            }
            searchpage1.closeSelected();
            searchpage1.goToLeadDetailPage();
                                             
            ApexPages.StandardController sc2 = new ApexPages.StandardController(leads[1]);
            VFC92_CloseDuplicateLeads searchpage2 = new VFC92_CloseDuplicateLeads(sc2);
            
            searchpage2.checkOpenLeads();
            for(VFC92_CloseDuplicateLeads.LeadWrapper lw : searchpage2.lstLeadWraps){
                lw.isSelected = true;
            }
            searchpage2.closeSelected();
            searchpage2.goToLeadDetailPage();
            test.stopTest();
            }
            }
            
    }
}//End of class