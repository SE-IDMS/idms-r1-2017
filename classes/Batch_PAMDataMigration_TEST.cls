@isTest
private class Batch_PAMDataMigration_TEST {
    static testMethod void testPAMDataMigration() {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        PAMandCompetition__c objPAM = Utils_TestMethods.createPAMandCompetition(objAccount.Id,'ProdBUA', 'ProdLineA', 'ProdFamilyA', 'FamilyA');
        objPAM.CommercialReference__c = 'CMREF123';
        objPAM.TECH_2014ReferentialUpdated__c = FALSE; 
        insert objPAM;
        
        TECH_CommRefAllLevel__c objCommRefAllLevel = new TECH_CommRefAllLevel__c();
        objCommRefAllLevel.Name = 'CMREF123'; 
        objCommRefAllLevel.BusinessUnit__c = 'ProdBUA1'; 
        objCommRefAllLevel.ProductLine__c = 'ProdLineA1';  
        objCommRefAllLevel.ProductFamily__c = 'ProdFamilyA1';  
        objCommRefAllLevel.Family__c = 'FamilyA1';  
        objCommRefAllLevel.SubFamily__c = 'SubFamilyA1';  
        objCommRefAllLevel.ProductSuccession__c = 'ProdSuccessA1';  
        objCommRefAllLevel.ProductGroup__c = 'ProdGroupA1';  
        objCommRefAllLevel.NewGMRCode__c = '01122435452516738B'; 
        
        insert objCommRefAllLevel;
    
        Test.startTest();
        Batch_PAMDataMigration objBatchJob = new  Batch_PAMDataMigration(1);
        Database.executeBatch(objBatchJob);
        Test.stopTest();
    }
}