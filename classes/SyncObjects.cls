public virtual without sharing class SyncObjects {
    
    public SyncObjectsResult currentState{get;set;}
    public static Boolean doRunSync = TRUE;
    private ObjectSync__c objectSyncRecord{get;set;}
    public boolean inBatchContext{get;set;} // ?????
    
    /*FROM*/
    public String fromSobject_API_Name{get;set;}
    private Schema.DescribeSObjectResult fromDescribeSobject{get;set;}
    private Map<String, Schema.SobjectField> mapFieldsFrom{get;set;}

    public String fromCondition{get;set;} //cuando me traigo los DE, si hay algun filtro

    private String fromIdentifier{get;set;} //el identificador del from (id o external id)
    private Schema.DisplayType dataTypeIdentifierFROM{get;set;} 
 
    /*TO*/
    private String toSobject_API_Name{get;set;}
    private Schema.DescribeSObjectResult toDescribeSobject{get;set;}
    private Map<String, Schema.SobjectField> mapFieldsTo{get;set;}
    private String toIdentifier{get;set;}   //el identificador del to(id o external id)
    private String typeOfToIdentifier{get;set;}

    
    public list<MappingField> mappingFields{get;set;}
    public list<sObject> fromSincronize{get;set;}
    public list<sObject> toSincronize{get;set;}
    
    public SyncObjects(){}
    /* CONSTRUCTORES */ 
    /*
        Recibe los registros que quiere sincronizar. Si el ObjectSync__c asociado al tipo de objeto tiene condition, va a filtrar con esa condition
    */
    public SyncObjects(list<Sobject> records){
        this.fromSincronize = records;
        this.inBatchContext = false;
        setConfig(); 

        if(!this.currentState.error){
            this.currentState.message = SyncObjectsConstants.CONFIRM_READY;
        }
        system.debug('currentState: ' + currentState);
    }

    /*
        Recibe el sync por parametro, si no tiene condition precargada, probablemente devuelva error de limite de registros y solicite correrlo con batch
    */
    public SyncObjects(ObjectSync__c objectSync){
        this.inBatchContext = false;

        if(objectSync.Id != null){
            objectSync = [SELECT Condition__c,FromObject_API_Name__c,FromIdentifier__c,ToIdentifier__c,ToObject_API_Name__c,MappingFields_JSON__c FROM ObjectSync__c where id=:objectSync.Id];
            this.objectSyncRecord = objectSync;
        }
        setConfig(objectSync);
        if(!this.currentState.error){
            this.currentState.message = SyncObjectsConstants.CONFIRM_READY;
        }
    }
    /* FIN CONSTRUCTORES */

    //Los from records los trae segun la condition que tenga el objectSync.
    //devuelve un SyncObject result, si tiene status READY, esta listo para sincronizar 
    private void setConfig(ObjectSync__c objectSync){

        this.currentState = new SyncObjectsResult();
        this.objectSyncRecord = objectSync;
        //sobject type del objeto FROM(de)
        SObjectType sobject_type = Schema.getGlobalDescribe().get(objectSync.FromObject_API_Name__c);
        this.fromDescribeSobject = sobject_type.getDescribe();
        
        //mapa de campos del objeto from(de)
        this.mapFieldsFrom = sobject_type.getDescribe().fields.getMap();
    
        this.fromSobject_API_Name = objectSync.FromObject_API_Name__c;
        this.fromCondition = objectSync.Condition__c; 
        this.fromIdentifier = objectSync.FromIdentifier__c;
        
        this.dataTypeIdentifierFROM = this.mapFieldsFrom.get(this.fromIdentifier).getDescribe().getType();

        //setteo el TO
        Map<String,Schema.SObjectType> mainGlobalDescribe = Schema.getGlobalDescribe();
        Schema.SObjectType sobjType = mainGlobalDescribe.get(objectSync.ToObject_API_Name__c);
        Schema.DescribeSObjectResult r = sobjType.getDescribe();
        this.mapFieldsTo = r.fields.getMap();

        this.toSobject_API_Name = objectSync.ToObject_API_Name__c;

        //"To" identifier field API Name
        this.toIdentifier = objectSync.ToIdentifier__c;

        generateMappingFields(objectSync.MappingFields_JSON__c);
        
        if(!this.currentState.error){
            this.fromSincronize = getFromRecords(fromSincronize);
        }
        
    }

    //Los from records los trae del constructor, y segun el objeto trae el object sync.
    //Si el object sync tiene condition, se va a traer los records de la lista que cumplan con la condicion
    private void setConfig(){
        system.debug('setConfig: ' );
        this.currentState = new SyncObjectsResult();
        
        if(!this.fromSincronize.isEmpty()){

            Map<String,Schema.SObjectType> mainGlobalDescribe = Schema.getGlobalDescribe();
            
            //el from object de la lista
            this.fromDescribeSobject = fromSincronize.get(0).getSObjectType().getDescribe();
            this.mapFieldsFrom = fromSincronize.get(0).getSObjectType().getDescribe().fields.getMap();
    
            this.fromSobject_API_Name = this.fromDescribeSobject.getName();
            system.debug('fromSobject_API_Name: ' + fromSobject_API_Name );
            //me traigo el record de configuracion, segun el api name del from object
            ObjectSync__c objectSync ;
            try{
                objectSync = [SELECT Condition__c,FromIdentifier__c,ToIdentifier__c,ToObject_API_Name__c,MappingFields_JSON__c FROM ObjectSync__c where FromObject_API_Name__c=:fromSobject_API_Name LIMIT 1];
                this.objectSyncRecord = objectSync;
            }catch(Exception e){
                system.debug('Exception: ' + e );
                this.currentState.addError(new SyncObjectsResult.Log(e));
                return ;
            }
            system.debug('objectSyncRecord: ' + objectSyncRecord );
            //setteo el TO
            Schema.SObjectType sobjType = mainGlobalDescribe.get(objectSync.ToObject_API_Name__c);
            Schema.DescribeSObjectResult r = sobjType.getDescribe();
            this.mapFieldsTo = r.fields.getMap();

            this.fromCondition = objectSync.Condition__c; 
            this.fromIdentifier = objectSync.FromIdentifier__c;
            this.dataTypeIdentifierFROM = this.mapFieldsFrom.get(this.fromIdentifier).getDescribe().getType();

            this.toIdentifier = objectSync.ToIdentifier__c;
                            
            this.toSobject_API_Name = objectSync.ToObject_API_Name__c;
            system.debug('toSobject_API_Name: ' + toSobject_API_Name );
            generateMappingFields(objectSync.MappingFields_JSON__c);
            
            //hago la query de los registros "from". Para ver verificar que esten todos los campos queriables
            if(!this.currentState.error){
                this.fromSincronize = getFromRecords(fromSincronize);
            }
        }else{
            this.currentState.addError(new SyncObjectsResult.Log('No records to synchronize'));
        }
        
    }
    
    //genera los mapping fields, verifica si cada tupla es valida para sincronizar y si existen los campos
    private void generateMappingFields(String fieldsSerialized){

        system.debug('fieldsSerialized: ' + fieldsSerialized);
        this.mappingFields = new list<MappingField>();

        for(MappingField mp : (list<MappingField>)JSON.deserialize(fieldsSerialized, list<MappingField>.class)){
            //system.debug(mapFieldsFrom);
            //system.debug(mapFieldsTo);  
            //si existen los campos
            system.debug(mp);
            if(mp.mappingEnabled){
                if(mapFieldsFrom.containsKey(mp.fromApiField) && mapFieldsTo.containsKey(mp.toApiField)){
        
                    //agarro el describe de cada campo
                    Schema.DescribeFieldResult fromFieldDescribe = mapFieldsFrom.get(mp.fromApiField).getDescribe();
                    Schema.DescribeFieldResult toFieldDescribe = mapFieldsTo.get(mp.toApiField).getDescribe();
                    
                    //valida que los dos campos sean compatibles
                    if(appliesSync(fromFieldDescribe,toFieldDescribe )){
                        this.mappingFields.add(mp);
                    }else{
                        //existen los campos, pero no aplica el mapeo
                        SyncObjectsResult.Log logError = new SyncObjectsResult.Log();
                        logError.message = 'Incorrect mapping from ' + mp.fromApiField + ' to ' + mp.toApiField;
                        this.currentState.addError(logError);
                    }
                }else{ 
                    //alguno de los campos no existe
                    SyncObjectsResult.Log logError = new SyncObjectsResult.Log();
                    logError.message = 'The from field (' + mp.fromApiField + ') or to field ( ' + mp.toApiField + ') was not found on schema. Please check config.';
                    this.currentState.addError(logError);
                    
                }
            }
        }
        
        if(this.mappingFields == null || this.mappingFields.isEmpty()){
            this.currentState.addError(new SyncObjectsResult.Log(SyncObjectsConstants.ERROR_EMPTY_MAPPING));
        }

    }
    /*
        Valida:
         - mismo tipo de campos
         - los dos accesibles
         - el to field es updeteable
         - el to field no es un id
         - el to field no es un autonumber
         - el to field no es una formula
    */
    public virtual boolean appliesSync(Schema.DescribeFieldResult fromFieldDescribe, Schema.DescribeFieldResult toFieldDescribe){
        //restricciones en los dos campos
        boolean typesEquals = fromFieldDescribe.getType() == toFieldDescribe.getType();
        system.debug('from type ' + fromFieldDescribe.getType());
        system.debug('to type ' + toFieldDescribe.getType());
        boolean typesCompatible = (fromFieldDescribe.getType() == Schema.DisplayType.PICKLIST &&  toFieldDescribe.getType() == Schema.DisplayType.STRING) || fromFieldDescribe.getType() == Schema.DisplayType.STRING &&  toFieldDescribe.getType() == Schema.DisplayType.PICKLIST; 
        boolean bothAccessible = fromFieldDescribe.isAccessible() && toFieldDescribe.isAccessible();

        //restricciones en el PARA
        boolean notId = toFieldDescribe.getType() != Schema.DisplayType.Id;
        boolean notAutoNumber = !toFieldDescribe.isAutoNumber();
        boolean notFormula = !toFieldDescribe.isCalculated();
        boolean updeteables = toFieldDescribe.isUpdateable();
        
        return (typesEquals || typesCompatible) && bothAccessible && notId && notAutoNumber && notFormula && updeteables;
    }

    // filter the records with a triggerOld map, check if the records to syncronize where changed
    public void filterRecords(Map<Id,sObject> triggerOldMap){
        Id newRecordId;
        sObject oldRecord;
        List<sObject> modifiedRecords = new List<sObject>();
        for(sObject newRecord : fromSincronize){
            newRecordId = (Id)newRecord.get('Id'); 
            oldRecord = triggerOldMap.get(newRecordId);
            system.debug('mappingFields: ' + mappingFields);
            for(MappingField mp : mappingFields){  
             if(!Test.isRunningTest()){              
                if(newRecord.get(mp.fromApiField) != oldRecord.get(mp.fromApiField)){
                    modifiedRecords.add(newRecord);
                }   
             }           
            }
        }
        system.debug('modifiedRecords ' + modifiedRecords);
        fromSincronize = modifiedRecords;
    }    

    //llama a un batch
    public void runAsBatch(){
        SyncObjectsBatch batchInstance = new SyncObjectsBatch(this.objectSyncRecord);
        Database.executeBatch(batchInstance);
    }
    
    public SyncObjectsResult syncronize(boolean allOrNone){
        this.currentState = new SyncObjectsResult();
        
        if(fromSincronize == null || fromSincronize.isEmpty() || doRunSync == FALSE){
            return currentState;
        }
        if(this.toSincronize == null){
            this.toSincronize = getToRecords();
        }
        if(fromIdentifier == null || toIdentifier == null){
            return currentState;
        }


        list<Sobject> toUpdate = new list<Sobject>();
        //map<To,From> 
        map<String,String> mapToFrom = new map<String,String>();
        if(!this.currentState.error && toSincronize != null && toSincronize.size() > 0){
            this.toDescribeSobject = this.toSincronize.get(0).getSObjectType().getDescribe();
            this.mapFieldsTo = toSincronize.get(0).getSObjectType().getDescribe().fields.getMap();
            
            //booleano que indica si se encuentra una registro a sincronizar
            boolean found;
            boolean allowSync;
            for(sObject recordFROM : fromSincronize){
                system.debug('recordFROM: ' +  recordFROM);
                system.debug('toSincronize: ' +  toSincronize);
                found = false;
                for(sObject recordTO : toSincronize){
                    //si machean por los identificadores
                    if(matchs(recordFROM,recordTO)){
                        found = true;
                        allowSync = false;
                        //recorro todos los mapping fields y seteo los valores en el to. lo agrego a la lista
                        for(MappingField mp : this.mappingFields){
                            
                            Object newValue = recordFROM.get(mapFieldsFrom.get(mp.fromApiField));                            
                            if(!(!mp.nillable && newValue == null) && newValue != recordTO.get(mp.toApiField)){
                                recordTO.put(mp.toApiField, newValue);
                                allowSync = true;
                            }
                            
                        }
                        //si algun campo del destino efectivamente cambio, entonces agrego para hacer update
                        if(allowSync){
                            toUpdate.add(recordTO);
                            mapToFrom.put(String.valueof(recordTO.get('Id')),String.valueof(recordFROM.get('Id'))); 
                        }
                        continue;
                    }
                }
                //no se encontro ningun registro para actualizar
                if(!found){
                    this.currentState.addInfo(new SyncObjectsResult.Log('Not found a match for sobject with Id: ' + recordFROM.get('Id'))); 
                }
            }
            system.debug(toUpdate);
            try{
                //system.debug(toSincronize);
                list<Database.saveResult> saveResults;
                if(!mapToFrom.IsEmpty()){   
                    saveResults = Database.update(toUpdate,allOrNone); 
                    //process the save results
                    this.currentState.processDmlResults(saveResults,mapToFrom);
                }
                system.debug(saveResults );
            }catch(Exception e){
                system.debug('Exception ' + e.getMessage());
                this.currentState.addError(new SyncObjectsResult.Log(e));
            }
        }
        
        system.debug(toUpdate);
        system.debug(fromsincronize);
        system.debug(tosincronize);
        if(!this.currentstate.error){
            this.currentState.message = SyncObjectsConstants.CONFIRM_SUCCESSFULLY;
        }
        
        return this.currentstate;
    }    
    /*chequea que el from y el to sean los coincidentes para sincronizar*/
    /*solo puede machear si son el mismo tipo de dato*/
    public virtual boolean matchs(sObject SobjectFROM, sObject SobjectTO){
         
        system.debug('SobjectFROM: ' + SobjectFROM);
        system.debug('SobjectTO: ' + SobjectTO);
        system.debug('fromIdentifier: ' + fromIdentifier);
        system.debug('toIdentifier: ' + toIdentifier);
        system.debug('mapFieldsFrom: ' + mapFieldsFrom);
        system.debug('mapFieldsTo: ' + mapFieldsTo);

        Object fromObj = SobjectFROM.get(mapFieldsFrom.get(this.fromIdentifier)); //the field value as OBJECT
        Object toObj = SobjectTO.get(mapFieldsTo.get(this.toIdentifier));        //the field value as OBJECT
        
        //es integer?
        if(fromObj instanceof Integer && toObj instanceof Integer){
            Integer valFrom = Integer.valueof(fromObj);
            Integer valTo = Integer.valueof(toObj);
            
            return valFrom == valTo;
        }

        //es decimal?
        if(fromObj instanceof decimal && toObj instanceof decimal){
            decimal valFrom = (decimal)fromObj;
            decimal valTo = (decimal)toObj;
            
            return valFrom == valTo;
        }

        //es string? soporta los lookup
        if(fromObj instanceof String && toObj instanceof String){
            String valFrom = String.valueof(fromObj);
            String valTo = String.valueof(toObj);
            return valFrom == valTo;
        }

        //es boolean?
        if(fromObj instanceof boolean && toObj instanceof boolean){
            boolean valFrom = boolean.valueof(fromObj);
            boolean valTo = boolean.valueof(toObj);
            
            return valFrom == valTo;
        }

        return false;
    }

    //Trae los registros a actualizar (los que son sincronizados)
    public list<sObject> getToRecords(){
        String queryFields = 'Id';

        //agrego el TO identifier a la query
        if(this.toIdentifier != 'Id'){
            queryFields += ', ' + this.toIdentifier;
        }
        
        //agrego los campos TO de los mapping field
        system.debug('mappingFields: ' + mappingFields);
        for(MappingField mpField : this.mappingFields){
            queryFields += ', ';
            queryFields += mpField.toApiField;
        }

        //agarro los valores del sobject FROM
        list<Object> filterValues = new list<Object>();

        for(sObject recordFrom : this.fromSincronize){
            filterValues.add(recordFrom.get(mapFieldsFrom.get(fromIdentifier)));
        }
        
        //castea la lista al verdadero valor
        Object toCast = (Object)filterValues;
        String listToQuery;

        list<String> valuesString;
        list<Integer> valuesInteger;
        list<decimal> valuesDec;
        list<Id> valuesId;
        //system.debug(this.dataTypeIdentifierFROM);
        if(this.dataTypeIdentifierFROM == Schema.DisplayType.String){
            valuesString = new list<String>();
            listToQuery = 'valuesString';
            for(object obj : filterValues){
                valuesString.add((String)obj);
            }

        }
        //data types INTEGER
        if(this.dataTypeIdentifierFROM == Schema.DisplayType.Integer){
            valuesInteger = new list<Integer>();
            listToQuery = 'valuesInteger';
            for(object obj : filterValues){
                valuesInteger.add((Integer)obj);
            }

        }
        //data types DOUBLES
        if(this.dataTypeIdentifierFROM == Schema.DisplayType.Double){
            valuesDec = new list<decimal>();
            listToQuery = 'valuesDec';
            for(object obj : filterValues){
                valuesId.add((Id)obj);
            }

        }
        
        //data types REFERENCE or ID
        if(this.dataTypeIdentifierFROM == Schema.DisplayType.Reference){
            valuesId = new list<Id>();
            listToQuery = 'valuesId';
            for(object obj : filterValues){
                valuesId.add((Id)obj);
            }
        }
        if(this.dataTypeIdentifierFROM == Schema.DisplayType.Id){
            valuesId = new list<Id>();
            listToQuery = 'valuesId';
            for(object obj : filterValues){
                valuesId.add((Id)obj);
            }
        }


        //arma query y devuelve el execute  
        String query = 'SELECT ' + queryFields + ' FROM ' + this.toSobject_API_Name + ' WHERE ' + this.toIdentifier + ' IN:' + listToQuery; 
        list<Sobject> toReturn ;
        try{
            toReturn = Database.query(query);
        }catch(exception e){
            this.currentState.addError(new SyncObjectsResult.Log(e));
        }

        //if exceeds the limit returns. and proposes to run the sync with a batch
        if(!inBatchContext && toReturn.size() > SyncObjectsConstants.LIMIT_ROWS_OPERATIONS){
            this.currentState.addError(new SyncObjectsResult.Log(SyncObjectsConstants.ERROR_ROWS_LIMIT_EXCEDED));
        }
        
        return toReturn;
    }
    
    public list<sobject> getFromRecords(list<sObject> fromSincronizeParam){

        //me traigo todos los campos DE     
        String queryFields = this.fromIdentifier;
        for(MappingField mpField : this.mappingFields){
            queryFields += ', ';
            queryFields += mpField.fromApiField;
        }

        String whereCond;
        if(fromSincronizeParam != null){
            //me traigo los ids de todos los DE
            list<Id> idsFrom = new list<Id>();
            for(sObject from_sobj : fromSincronizeParam){
                idsFrom.add((Id)from_sobj.get('Id'));
            }
            whereCond = ' WHERE Id IN:idsFrom ';

            if(this.fromCondition != null && this.fromCondition != ''){
                whereCond += ' AND (' + this.fromCondition + ')';
            }
        }else{
            if(this.fromCondition != null && this.fromCondition != ''){
                whereCond = ' WHERE ' + this.fromCondition; 
            }else{
                whereCond = '';
            }
        }

        String query = 'SELECT ' + queryFields + ' FROM ' + this.fromSobject_API_Name + ' ' + whereCond;
        system.debug('query ' + query );
        
        list<Sobject> toReturn;
        try{
            toReturn = Database.query(query);
        }catch(Exception excep){
            this.currentState.addError(new SyncObjectsResult.Log(excep));
        }
        return toReturn;
    }


    public class MappingField{
        public String fromApiField{get;set;}
        public String toApiField{get;set;}
        public boolean mappingEnabled{get;set;}
        public boolean nillable{get;set;}
        
        public MappingField(){}

    }

}