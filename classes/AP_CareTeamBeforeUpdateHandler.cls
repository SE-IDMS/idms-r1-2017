public class AP_CareTeamBeforeUpdateHandler{
    public void onBeforeUpdate(List<CustomerCareTeam__c> newTeams)
    {
        /* 
           These following lines checks if there are open cases, active point of contacts or any member with the respective team as its default.
           If yes, then the CC Team can’t be Inactive.
        */   
      
        List<Case> cases = new List<Case>();
        List<PointOfContact__c> pocs = new List<PointOfContact__c>();
        List<TeamAgentMapping__c> teamMembers = new List<TeamAgentMapping__c>();
        List<EmailToCaseKeyword__c> e2cKeywrds = new List<EmailToCaseKeyword__c>();
        Map<Id, Id> ctCasesIds = new Map<Id, Id>();
        Map<Id, Id> ctPocsIds = new Map<Id, Id>();
        Map<Id, Id> ctTeamAgentIds = new Map<Id, Id>();
        Map<Id, Id> ctE2cKeywrdIds = new Map<Id, Id>();
        Boolean ctCaseExists = false;
        Boolean ctPOCExists = false;
        Boolean ctTeamAgentExists = false;
        Boolean ctE2cKeywrdExists = false;
        
        //Queries the cases with Status not Closed, Cancelled
        /*cases =  [select Id,Team__c,CreatedDate,Status from Case where Team__c in :newTeams and (Status!=:Label.CLDEC13CCC04 and Status!=:Label.CL00326)];
        for(Case c: cases)
        {
            ctCasesIds.put(c.Team__c,c.Id);
        } */
        if ([select count() from Case where Team__c in :newTeams and (Status!=:Label.CLDEC13CCC04 and Status!=:Label.CL00326)] > 0) ctCaseExists = true;
        
        //Queries the Active Point of Contacts
        /*pocs = [select Id,DefaultRoutingTeam__c,IsActive__c from PointOfContact__c where IsActive__c = true and (DefaultRoutingTeam__c in :newTeams or DirectRoutingToTeam__c in :newTeams)];
        for(PointOfContact__c p :pocs)
        {
            ctPocsIds.put(p.DefaultRoutingTeam__c,p.Id);
        }*/
        if ([select count() from PointOfContact__c where IsActive__c = true and (DefaultRoutingTeam__c in :newTeams or DirectRoutingToTeam__c in :newTeams)] > 0) ctPOCExists = true;
        
        //Queries Team Member to check if it has respective CC Team as default
        /*teamMembers = [select Id,CCTeam__c,DefaultTeam__c from TeamAgentMapping__c where CCTeam__c in :newTeams and DefaultTeam__c = true];
        for(TeamAgentMapping__c tm :teamMembers)
        {
            ctTeamAgentIds.put(tm.CCTeam__c,tm.Id);
        }*/
        if ([select count() from TeamAgentMapping__c where CCTeam__c in :newTeams and DefaultTeam__c = true] > 0) ctTeamAgentExists = true;
        
        //Queries Email To Case Keywords to check if it has respective CC Team as default
        /*e2cKeywrds = [select Id,CaseTeam__c from EmailToCaseKeyword__c where CaseTeam__c in :newTeams and Active__c = true];
        for(EmailToCaseKeyword__c e2c :e2cKeywrds)
        {
            ctE2cKeywrdIds.put(e2c.CaseTeam__c,e2c.Id);
        }*/
        if ([select count() from EmailToCaseKeyword__c where CaseTeam__c in :newTeams and Active__c = true] > 0) ctE2cKeywrdExists = true;
        
        System.debug('Cases '+ctCaseExists );
        System.debug('Active Point of Contact '+ctPOCExists );
        System.debug('Default Team Member '+ctTeamAgentExists );
        System.debug('Active Email to Case Keywords '+ctE2cKeywrdExists );
        
        //Generate and display an error if there are open cases, active point of contacts, any member with the respective team as its default or active email to case keywords.
        for(CustomerCareTeam__c ct: newTeams)
        {
                String errorMsg = Label.CLOCT15CCC02+' ';
                if(ctCaseExists)    //ctCasesIds.containsKey(ct.Id)
                    errorMsg+=Label.CLOCT15CCC03+' ';
                if(ctPOCExists)    //ctPocsIds.containsKey(ct.Id)
                    errorMsg+=Label.CLOCT15CCC05+' ';
                if(ctTeamAgentExists)    //ctTeamAgentIds.containsKey(ct.Id)
                    errorMsg+=Label.CLOCT15CCC04+' ';
                if(ctE2cKeywrdExists)    //ctE2cKeywrdIds.containsKey(ct.Id)
                    errorMsg+=Label.CLOCT15CCC07+' ';
                
                if( (errorMsg.compareTo(Label.CLOCT15CCC02+' ')) != 0)
                {  
                    errorMsg+= Label.CLOCT15CCC06;    
                    // display generated error message
                    ct.addError(errorMsg);
                }
                    
        }
    }
}