/**
•   Created By: Otmane HALOUI
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is used to sync user data between IDMS and UIMS. 
**/
public  without sharing class IdmsUimsUserSync{
    
    Map<String,String> mapJobFunction               = IDMSCheckPicklistValues.getMapJobFunctionValues();
    Map<String,String> mapJobTitle                  = IDMSCheckPicklistValues.getMapJobTitleValues();
    Map<String,String> mapLanguageCode              = IDMSCheckPicklistValues.getMapLanguageCode();
    Map<String,String> mapSalutation                = IDMSCheckPicklistValues.getMapSalutation();
    Map<String,String> mapUserState                 = IDMSCheckPicklistValues.getMapUserState();
    Map<String,String> mapUserCountry               = IDMSCheckPicklistValues.getMapUserCountryCode();
    Map<String,String> MapUserCompanyCountryCode    = IDMSCheckPicklistValues.getMapUserCompanyCountryCode();
    Map<String,String> MapUserClassLevel1           = IDMSCheckPicklistValues.getMapUserClassLevel1();
    Map<String,String> MapUserClassLevel2           = IDMSCheckPicklistValues.getMapUserClassLevel2();
    Map<String,String> MapUserCompanyNbrEmployees   = IDMSCheckPicklistValues.getMapUserCompanyNbrEmployees();
    Map<String,String> MapUserMarketServed          = IDMSCheckPicklistValues.getMapUserMarketServed();
    Map<String,String> MapUserCompanyState          = IDMSCheckPicklistValues.getMapUserCompanyState();
    
    //This method is used for user sync logic 
    public void UserSyncLogic(Set<Id> uimsUserIds){
        //SELECT all necessary fields from updated UIMS Users that got converted
        List<UIMS_User__c> UIMSUsers = [select Id,Email__c,Cell__c,FirstName__c,LastName__c,CountryCode__c,LanguageCode__c,Phone__c,JobTitle__c, MiddleNameECS__c, 
                                        JobFunction__c,JobDescription__c,Street__c,AddInfoAddress__c,LocalityName__c,PostalCode__c,State__c, User__c,
                                        County__c,PostOfficeBox__c,MiddleName__c,Salutation__c,Fax__c,FederatedID__c,CompanyId__c,PrimaryContact__c,
                                        GoldenId__c,AddInfoAddressECS__c,CountyECS__c, GivenNameECS__c,LocalityNameECS__c,SnECS__c,StreetECS__c,Vnew__c,updateSource__c
                                        from UIMS_User__c where Id IN : uimsUserIds AND User__c != null];
        
        list<user> UpdatedIdmsUsers = new list<user>();
        User idmsUser;           
        for(UIMS_User__c uimsUser : UIMSUsers){
            system.debug('uimsUser :'+uimsUser );
            idmsUser = uimsIdmsUserMapping(uimsUser);
            UpdatedIdmsUsers.add(idmsUser); 
        }                            
        system.debug('Updated IDMS Users that will be sent to updateUsersAndAccounts: '+UpdatedIdmsUsers);
        updateUsersAndAccounts(UpdatedIdmsUsers);
    }
    //This method is used for company sync logic. 
    public void CompanySyncLogic(Set<Id> uimsCompanyIds){
        //SELECT all necessary fields from updated UIMS Companies that got convert      
        List<UIMS_Company__c> UIMSCompanies = [Select ID, OrganizationName__c,GoldenId__c,CountryCode__c,CurrencyCode__c,MarketSegment__c,PostalCode__c,FederatedID__c,
                                               PostOfficeBox__c,St__c,Street__c,TelephoneNumber__c,HeadQuarter__c,AddInfoAddress__c,PostalCity__c,WebSite__c,AnnualSales__c,
                                               MarketServed__c,EmployeeSize__c, account__c,Vnew__c,BusinessType__c,County__c,TaxIdentificationNumber__c, NameECS__c,StreetECS__c,AddInfoAddressECS__c,
                                               CityECS__c,CountyECS__c, CustomerClass__c,LocalityName__c                                         
                                               from UIMS_Company__c Where ID IN :Trigger.New];
        
        Set<String> CompanyFedIds = new Set<String>();
        Map<String,UIMS_Company__c> mapUIMSCompanyFedIdCompany = new Map<String,UIMS_Company__c>();
        
        for(UIMS_Company__c UIMSCompany : UIMSCompanies ){
            CompanyFedIds.add(UIMSCompany.FederatedID__c );
            mapUIMSCompanyFedIdCompany.put(UIMSCompany.FederatedID__c, UIMSCompany);
        }
        system.debug('CompanyFedIds: '+CompanyFedIds);
        //Select all the uims users that got converted and linked to these companies    
        List<UIMS_User__c> UIMSUsers = [Select Id, user__c, companyId__c from UIMS_User__c where user__c != null and companyId__c in :CompanyFedIds];
        System.debug('uims users that got converted and linked to these companies are: '+ UIMSUsers);
        
        Set<Id> userIdsSet = new Set<Id>();
        for(UIMS_User__c uimsUser : UIMSUsers){
            userIdsSet.add(uimsUser.user__c);
        }
        //Select IDMS Users related to these set of converted UIMS Users
        List<User> users = [Select Id from User where Id in :userIdsSet];
        Map<Id,User> mapUserIdUser = new Map<Id,User>();
        for(User usr : users){
            mapUserIdUser.put(usr.id,usr);
        }
        
        list<user> UpdatedIdmsUsers=new list<user>();
        User idmsUser;
        UIMS_Company__c company;
        for(UIMS_User__c UIMSUser : UIMSUsers){
            //Get the correspondig IDMS User and UIMS Company
            idmsUser = mapUserIdUser.get(UIMSUser.user__c);
            company = mapUIMSCompanyFedIdCompany.get(UIMSUser.companyId__c);
            //Enrich each user with its company info
            enrichUserCompanyInfo(idmsUser, company);
            UpdatedIdmsUsers.add(idmsUser); 
        }                            
        updateUsersAndAccounts(UpdatedIdmsUsers);       
    }
    //This method is used to update user and it's account. 
    public void updateUsersAndAccounts(list<User> idmsUsers){
        system.debug('List of IDMS users as parameters: '+idmsUsers);
        list<account> personAccountsToUpdate= new list<Account>();
        
        //Get the Ids of all users:
        Set<Id> idmsUserIds = new Set<Id>();
        for(User idmsUser : idmsUsers){ 
            idmsUserIds.add(idmsUser.Id);
        }
        //Get the context of all users
        List<User> usersContext = [select Id, IDMS_User_Context__c,contactid  from User where id In :idmsUserIds];           
        //Set a Map to link User Id with corresponding context
        Map<Id,User> mapUserIdContext = new Map<Id,User>();
        for(User usr : usersContext){
            mapUserIdContext.put(usr.Id, usr);
        }        
        //Update logic
        List<User> enrichAccountContactList = new List<User>();
        for(User idmsUser : idmsUsers){
            user userContext= mapUserIdContext.get(idmsUser.Id);         
            if(userContext.IDMS_User_Context__c.equalsignorecase('Work') || userContext.IDMS_User_Context__c.equalsignorecase('@Work')){ 
                enrichAccountContactList.add(idmsUser);
            }else if(userContext.IDMS_User_Context__c.equalsignorecase('Home') || userContext.IDMS_User_Context__c.equalsignorecase('@Home')){
                string personContactId=userContext.contactId;       
                if(personContactId!=null){
                    Account  IDMSUpdateAccount=updateAccount(personContactid,idmsUser);           
                    personAccountsToUpdate.add(IDMSUpdateAccount);     
                }           
            }            
        }
        
        system.debug('work users list to update:'+enrichAccountContactList);
        system.debug('person Account list to update:'+personAccountsToUpdate);
        
        //Call  BFO Method to update the account
        if(enrichAccountContactList.size()>0){
            system.debug('execution of enrichAccountAndContact + matchAndRelink for @work users');
            AP_UserMasterDataMethods.enrichAccountAndContact(enrichAccountContactList);
            AP_UserMasterDataMethods.matchAndRelink(enrichAccountContactList);
        }          
        //Call asynchrounous methods to update users and person accounts after serializing both idmsUsers and personAccountsToUpdatem
        if(personAccountsToUpdate.size() > 0){
            String personAccountsToUpdateSerialized = JSON.serialize(personAccountsToUpdate);
            IdmsUimsUserSync.updateIdmsAccounts(personAccountsToUpdateSerialized);
        }
        if(idmsUsers.size() > 0){
            String usersToUpdateSerialized = JSON.serialize(idmsUsers);
            IdmsUimsUserSync.updateIdmsUsers(usersToUpdateSerialized);
        }
    }
    //This future method is used to update idms user account.    
    @future
    public Static void updateIdmsAccounts(String accountsToUpdateSerialized){
        List<Account> accountsToUpdate = (List<Account>)JSON.deserialize(accountsToUpdateSerialized,List<Account>.class);
        update accountsToUpdate;
    }   
    //this future method is used to update user data in idms
    @future
    public Static void updateIdmsUsers(String usersToUpdateSerialized){
        List<User> userssToUpdate = (List<User>)JSON.deserialize(usersToUpdateSerialized,List<User>.class);
        update userssToUpdate;
    }     
    
    static user userdetails             = new user();
    static string personContactId       = '';
    static IDMSCaptureError errorLog    = new IDMSCaptureError();
    
    //this method is used to update user account
    public account updateAccount(String personContactId1,user userperacc1) {
        System.debug(userPerAcc1);
        Schema.FieldSet personAccField = Schema.SObjectType.Account.fieldSets.getMap().get(Label.CLJUN16IDMS105);
        List<Schema.FieldSetMember> personAccFieldObj=personAccField.getFields();       
        IDMS_field_Mapping__c userfieldname;
        String SOQLquery = 'select id';
        for(Schema.FieldSetMember personAccFieldObjItr:personAccFieldObj){
            SOQLquery = SOQLquery + ',' + personAccFieldObjItr.getFieldPath();
        }
        SOQLquery = SOQLquery + ' from account WHERE PersonContactId =:this.personContactId1 limit 1';
        account userAcc = Database.query(SOQLquery);
        
        for(Schema.FieldSetMember personAccFieldObjItr:personAccFieldObj){
            String accountfieldAPIname=personAccFieldObjItr.getFieldPath();
            userfieldname= IDMS_field_Mapping__c.getInstance(accountfieldAPIname);
            String userfieldAPIname=(String)userfieldname.get('userfield__c');
        }
        if(userPerAcc1.email!=null && userPerAcc1.email!=''){
            userAcc.IDMS_Contact_UserExternalId__pc=userPerAcc1.email+Label.CLJUN16IDMS71;
        }
        system.debug(userAcc);
        return userAcc; 
    }
    
    
    //this method is used to do mapping between idms and uims attributes
    public user uimsIdmsUserMapping(UIMS_User__c uimsUser){
        User usr                              = new User();               
        usr.Id                                = uimsUser.user__c;
        usr.Email                             = uimsUser.EMail__c;
        usr.fax                               = uimsUser.fax__c;
        usr.Username                          = usr.Email+Label.CLJUN16IDMS71;
        usr.alias                             ='Alias';
        usr.IsIDMSUser__c                     = true;
        usr.IDMS_AdditionalAddress__c         = uimsUser.AddInfoAddress__c;
        usr.Country                           = IDMSCheckPicklistValues.isValidUserCountry(uimsUser.Countrycode__c,mapUserCountry);
        usr.IDMSJobDescription__c             = uimsUser.JobDescription__c;
        usr.Job_Function__c                   = IDMSCheckPicklistValues.isValidJobFunction(uimsUser.JobFunction__c,mapJobFunction);
        usr.Job_Title__c                      = IDMSCheckPicklistValues.isValidJobTitle(uimsUser.JobTitle__c, mapJobTitle);
        usr.City                              = uimsUser.LocalityName__c;
        usr.IDMSMiddleName__c                 = uimsUser.MiddleName__c;
        usr.IDMS_POBox__c                     = uimsUser.PostOfficeBox__c;
        usr.IDMSSalutation__c                 = IDMSCheckPicklistValues.isValidSalutation(uimsUser.Salutation__c,mapSalutation);        
        usr.IDMSCompanyFederationIdentifier__c = uimsUser.CompanyId__c;
        usr.IDMSContactGoldenId__c             = uimsUser.GoldenId__c;
        usr.IDMSPrimaryContact__c              = uimsUser.PrimaryContact__c == 'TRUE';
        usr.Street                            = uimsUser.Street__c;
        usr.IDMSAddInfoAddressECS__c          = uimsUser.AddInfoAddressECS__c;
        usr.IDMSCountyECS__c                  = uimsUser.CountyECS__c;
        usr.IDMSGivenNameECS__c               = uimsUser.GivenNameECS__c;
        usr.IDMSLocalityNameECS__c            = uimsUser.LocalityNameECS__c;
        usr.IDMSMiddleNameECS__c              = uimsUser.MiddleNameECS__c;
        usr.PostalCode                        = uimsUser.PostalCode__c;
        usr.IDMS_County__c                    = uimsUser.County__c;
        usr.FederationIdentifier              = uimsUser.FederatedID__c;
        
        usr.MobilePhone                       = uimsUser.Cell__c;
        usr.IDMS_PreferredLanguage__c         = IDMSCheckPicklistValues.isValidPreferedLang(uimsUser.LanguageCode__c,mapLanguageCode);
        usr.LastName                          = uimsUser.LastName__c;
        usr.Firstname                         = uimsUser.FirstName__c;
        usr.State                             = IDMSCheckPicklistValues.isValidUserState(uimsUser.State__c,mapUserState);
        usr.Phone                             = uimsUser.Phone__c;
        usr.IDMSSnECS__c                      = uimsUser.SnECS__c;
        usr.IDMSStreetECS__c                  = uimsUser.StreetECS__c;
        
        // end
        usr.LocaleSidKey                      = 'en_US';
        usr.EmailEncodingKey                  ='UTF-8' ;
        usr.TimeZoneSidKey                    = 'America/Los_Angeles';
        usr.LanguageLocaleKey                 = 'en_US';            
        
        usr.IDMS_Registration_Source__c       = 'IDMS';
        
        //usr.IsPortalSelfRegistered = TRUE;
        if(string.isnotblank(uimsUser.Vnew__c)){
            usr.IDMS_VU_NEW__c  = uimsUser.Vnew__c;
            usr.IDMS_VU_OLD__c  = uimsUser.Vnew__c;                
        }
        if(uimsUser.updateSource__c != null){
            if(uimsUser.updateSource__c.equalsignorecase('UIMS')){
                usr.IDMS_Profile_update_source__c = 'UIMS';
            }
        }
        return usr;      
        
    }
    //this method is used to map user company info
    public void enrichUserCompanyInfo(User usr, UIMS_Company__c uimsCompany){
        usr.IDMSCompanyFederationIdentifier__c      = uimsCompany.FederatedId__c;
        usr.CompanyName                             = uimsCompany.OrganizationName__c;
        usr.IDMSCompanyGoldenId__c                  = uimsCompany.GoldenId__c;
        usr.Company_Country__c                      = IDMSCheckPicklistValues.isValidCompanyCountry(uimsCompany.CountryCode__c,MapUserCompanyCountryCode);
        usr.DefaultCurrencyIsoCode                  = uimsCompany.CurrencyCode__c;
        usr.IDMSClassLevel2__c                      = IDMSCheckPicklistValues.isValidIDMSClassLevel2(uimsCompany.CustomerClass__c,MapUserClassLevel2);
        usr.Company_Postal_Code__c                  = uimsCompany.PostalCode__c;
        usr.IDMSCompanyPoBox__c                     = uimsCompany.PostOfficeBox__c;
        usr.Company_State__c                        = IDMSCheckPicklistValues.isValidCompanyState(uimsCompany.St__c,MapUserCompanyState);
        usr.Company_Address1__c                     = uimsCompany.Street__c;
        usr.IDMSTaxIdentificationNumber__c          = uimsCompany.TaxIdentificationNumber__c;
        usr.IDMSCompanyHeadquarters__c              = uimsCompany.HeadQuarter__c == 'TRUE';
        usr.Company_Address2__c                     = uimsCompany.AddInfoAddress__c;
        usr.Company_City__c                         = uimsCompany.LocalityName__c;
        usr.Company_Website__c                      = uimsCompany.WebSite__c;
        usr.IDMSCompanyMarketServed__c              = IDMSCheckPicklistValues.isValidIDMSCompanyMarketServed(uimsCompany.MarketServed__c,MapUserMarketServed);
        usr.IDMSCompanyCounty__c                    = uimsCompany.County__c;
        usr.IDMSCompanyNbrEmployees__c              = IDMSCheckPicklistValues.isValidIDMSCompanyNbrEmployees(uimsCompany.EmployeeSize__c, MapUserCompanyNbrEmployees);
        String marketSegValue                       =  getMarketSegmentValue(uimsCompany.MarketSegment__c);
        if(marketSegValue.equalsIgnoreCase('MarketSegment')){
            usr.IDMSMarketSegment__c                = uimsCompany.MarketSegment__c;
        }else if (marketSegValue.equalsIgnoreCase('MarketSubSegment')){
            
            usr.IDMSMarketSubSegment__c             = uimsCompany.MarketSegment__c;            
        }
        //ECS
        usr.IDMSCompanyNameECS__c                   = uimsCompany.NameECS__c;
        usr.IDMSCompanyStreetECS__c                 = uimsCompany.StreetECS__c;
        usr.IDMSCompanyAddInfoAddressECS__c         = uimsCompany.AddInfoAddressECS__c;
        usr.IDMSCompanyCityECS__c                   = uimsCompany.CityECS__c;
        usr.IDMSCompanyCountyECS__c                 = uimsCompany.CountyECS__c;
        
        if(string.isnotblank(uimsCompany.Vnew__c)){
            usr.IDMS_VC_NEW__c                      = uimsCompany.Vnew__c;
            usr.IDMS_VC_OLD__c                      = uimsCompany.Vnew__c;  
        }
    }
    
    static Map<String,String> MarketSegmentMap;
    static Map<String,String> MarketSubSegmentMap;
    
    //this method is used to get market segment mapping    
    public Static Map<String,String> getMarketSegmentMap() {
        if(MarketSegmentMap == null) {
            
            MarketSegmentMap = new Map<String,String>();
            Schema.DescribeFieldResult marketseg = Account.MarketSegment__c.getDescribe();
            List<Schema.PicklistEntry> marketsegPle = marketseg.getPicklistValues();
            for( Schema.PicklistEntry f : marketsegPle){
                MarketSegmentMap.put(f.getValue(),f.getValue());
            }        
        }
        
        return MarketSegmentMap;
    }
    
    //this method is used for subsegment mapping
    public Static Map<String,String> getMarketSubSegmentMap() {
        if(MarketSubSegmentMap== null) {
            
            MarketSubSegmentMap = new Map<String,String>();
            Schema.DescribeFieldResult marketSubseg = Account.MarketSubSegment__c.getDescribe();
            List<Schema.PicklistEntry> MarketSubSegPle = marketSubseg.getPicklistValues();
            for( Schema.PicklistEntry f : MarketSubSegPle ){
                MarketSubSegmentMap .put(f.getValue(),f.getValue());
            }        
        }
        
        return MarketSubSegmentMap ;
    }
    
    //this method is uset to get market segment.
    Public Static String getMarketSegmentValue(String MarketSegment){
        String res='';       
        if(IdmsUimsUserSync.getMarketSegmentMap().containsKey(MarketSegment)){
            res = 'MarketSegment';
        }
        
        if( res == null && IdmsUimsUserSync.getMarketSubSegmentMap().containsKey(MarketSegment)){
            res = 'MarketSubSegment';
        }
        return res;
    } 
    
    public Static User createUserSaml(String FederatedId, String Context){
        List<UIMS_User__c> UIMSUsers = [select Id,Email__c,Cell__c,FirstName__c,LastName__c,CountryCode__c,LanguageCode__c,Phone__c,JobTitle__c, MiddleNameECS__c, 
                                        JobFunction__c,JobDescription__c,Street__c,AddInfoAddress__c,LocalityName__c,PostalCode__c,State__c, User__c,
                                        County__c,PostOfficeBox__c,MiddleName__c,Salutation__c,Fax__c,FederatedID__c,CompanyId__c,PrimaryContact__c,
                                        GoldenId__c,AddInfoAddressECS__c,CountyECS__c, GivenNameECS__c,LocalityNameECS__c,SnECS__c,StreetECS__c,Vnew__c,
                                        PhoneId__c, updateSource__c,Password__c
                                        from  UIMS_User__c 
                                        where federatedId__c = :FederatedId];
        try{
            if(UIMSUsers.size() > 0){
                system.debug('User exists in UIMS User table');
                UIMS_User__c UIMSUser = UIMSUsers[0];
                
                IDMS_UIMS_Mapping mapping = new IDMS_UIMS_Mapping();
                User IDMSUser;
                List<UIMS_Company__c> uimsCompanies = [Select ID, OrganizationName__c,GoldenId__c,CountryCode__c,CurrencyCode__c,MarketSegment__c,PostalCode__c,FederatedID__c,
                                                       PostOfficeBox__c,St__c,Street__c,TelephoneNumber__c,HeadQuarter__c,AddInfoAddress__c,PostalCity__c,WebSite__c,AnnualSales__c,
                                                       MarketServed__c,EmployeeSize__c, account__c,Vnew__c,BusinessType__c,County__c,NameECS__c,StreetECS__c,AddInfoAddressECS__c,
                                                       CityECS__c,CountyECS__c,CustomerClass__c,TaxIdentificationNumber__c                                           
                                                       from UIMS_Company__c 
                                                       where FederatedID__c =: UIMSUser.CompanyId__c Limit 1];
                if(uimsCompanies.size() > 0){                        
                    IDMSUser = mapping .CreateIDMSUserFromUIMS(UIMSUser,uimsCompanies[0],context);
                }else{
                    IDMSUser = mapping .CreateIDMSUserFromUIMS(UIMSUser,null,context);
                }
                System.debug('IDMSUser: '+IDMSUser);                       
                mapping.enrichUserByCompany(IDMSUser.IDMS_User_Context__c,IDMSUser);
                //Async call
               // VFC_IDMS_Consumer_SocialLogin.updateUIMSRecord(UIMSUser.Id,IDMSUser.Id);
               system.debug('IDMS User to be returned: ' + IDMSUser);
                return IDMSUser;
            }
            return null;
        } catch (Exception e){
            system.debug('Exception: '+e);
            return null;
        }
    }
    
    public static String getAppIdUIMSPost(String idpGoto){
        String relayState = EncodingUtil.base64Decode(idpGoto).toString();
        system.debug('relayState: '+ relayState);
        string appId = relayState.substringAfter('RelayState');
        system.debug('appId: '+appId);
        if(appId != null){
            appId = appId.substringAfter('app%3D');
            appId = appId.subString(0,15);
        }
        system.debug('appId: '+ appId);
        return appId;
    }    

}