/************************************************************
* Developer: Marco Paulucci                                 *
* Type: Test Class                                          *
* Created Date: 22.09.2014                                  *
* Tested Class: Fielo_ContactController                   *
************************************************************/

@isTest
public with sharing class Fielo_ContactControllerTest{
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
        insert deactivate;
        
        Account acct = new Account(Name = 'Polo Marco', Street__c = 'Calle Falsa 123', ZipCode__c = '666333');
        insert acct;
        
        Country__c cun = new Country__c(Name = 'United Arab Emirates', CountryCode__c = 'AE');
        insert cun;
        
        FieloEE__Member__c mem = new FieloEE__Member__c(FieloEE__LastName__c= 'Marco', FieloEE__FirstName__c = 'Polo', F_Country__c = cun.id, FieloEE__Email__c = 'test@test.test', F_CompanyName__c = 'TestCompany');
        insert mem;
        
        FieloEE.MemberUtil.setMemberId(mem.Id);
        
        Fielo_ContactInformation__c cti = new Fielo_ContactInformation__c(Name = 'test', F_ContactEmail__c = 'test@test.test', F_ContactName__c = '', F_ContactNumber__c = '+541153755058', F_ContactURLImage__c = '' );
        insert cti;

        
        Fielo_ContactController RL = new Fielo_ContactController();
        RL.getNatureOfIssueValues();
        RL.getNatureOfEnquiryValues();
        RL.message = 'T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H T E S T R E A L L Y L O N G M E S S A G E T H E L O N G E S T M E S S A G E O N E A R T H';
        RL.doSend();
        RL.message = 'Message';
        RL.doSend();
        RL.selectedNatureOfEnquiry = 'Issue'; RL.selectedNatureOfIssue = null;
        RL.doSend();
        RL.message = '';
        RL.doSend();

    }
}