/********************************************************************
* Company: Fielo
* Created Date: 26/04/2015
* Description: Controller class of vf component FieloPRM_C_CompletionPercentage
********************************************************************/
public with sharing class FieloPRM_AP_CompletionPercentageCon{

    public FieloCH__Challenge__c challenge{get;set;}    
    public Boolean buttonJoin{get;set;}
    public String challengeId{get{
            return this.challengeId;
        }set{
            this.challengeId = value;

            //get the member, the challenge and the challengeMissions
            member = FieloEE.MemberUtil.getAllFieldsLoggedMember();

            if(challengeId != null){
                challenge = [SELECT Id, Name, FieloCH__Mode__c, F_PRM_MenuProgram__c, F_PRM_MenuOverview__c,F_PRM_MenuProgram__r.fieloEE__ExternalName__C, 
                                                    (SELECT Id, FieloCH__Mission__c, FieloCH__Challenge__c FROM FieloCH__MissionsChallenge__r), 
                                                    (SELECT Id, FieloCH__Status__c, FieloCH__Member__c, FieloCH__Team__c FROM FieloCH__ChallengeMembers2__r) 
                                                    FROM FieloCH__Challenge__c WHERE Id=:challengeId];

                FieloCH__ChallengeReward__c challengeRewrad = [SELECT id, F_TypeReward__c,FieloCH__LogicalExpression__c  FROM FieloCH__ChallengeReward__c WHERE  FieloCH__Challenge__c =: challenge.id  ORDER By FieloCH__Position__c DESC limit 1];

                list<Decimal> listMission = new list<Decimal>();
                if(challengeRewrad.F_TypeReward__c != 'Join'){
                        if(challengeRewrad.FieloCH__LogicalExpression__c != null){
                            listMission =getMissionFromExpression(challengeRewrad.FieloCH__LogicalExpression__c);
                        }else{
                            listMission = new list<Decimal>();
                        }
                }
                
                List<FieloCH__ChallengeMission__c> challengeMissions;

                challengeMissions = Database.query('SELECT FieloCH__Mission2__c, FieloCH__Mission2__r.FieloCH__ObjectiveValue__c, FieloCH__Mission2__r.FieloCH__ObjectiveType__c, FieloCH__Mission2__r.FieloCH__DynamicObjective__c, FieloCH__Mission2__r.FieloCH__ObjectiveField__c, FieloCH__Mission2__r.RecordType.DeveloperName, FieloCH__Mission2__r.FieloCH__Operator__c, FieloCH__Team__c, FieloCH__Counter__c, FieloCH__MaxValue__c, FieloCH__Average__c, FieloCH__Summary__c, (SELECT Id, FieloCH__Counter__c, FieloCH__Summary__c,FieloCH__MaxValue__c, FieloCH__Average__c, FieloCH__AlternativeKey__c FROM FieloCH__ChallengeMissionsMembers__r WHERE FieloCH__ChallengeMember__r.FieloCH__Member__c =\'' + member.Id + '\') FROM FieloCH__ChallengeMission__c WHERE FieloCH__Challenge2__c =:challengeId');
                //get the map with missions and its %
                this.missionStatusMap = FieloPRM_UTILS_ChallengeMethods.loadMissionsStatus(member, challenge, challengeMissions);
                if(challengeRewrad.F_TypeReward__c != 'Join'){
                  
                    this.missions = [SELECT Name, FieloCH__Description__c, RecordType.DeveloperName, FieloCH__ObjectiveType__c, FieloCH__DynamicObjective__c, FieloCH__ObjectiveField__c 
                                FROM FieloCH__Mission__c 
                                WHERE Id In (SELECT FieloCH__Mission__c FROM FieloCH__MissionChallenge__c WHERE FieloCH__Challenge__c =: challengeId AND FieloCH__Order__c in: listMission)];         
                    buttonJoin = true;
                    if(missions == null || missions.size() == 0){
                        buttonJoin = false;
                    }
                    for(FieloCH__Mission__c mission :  missions){
                        if(missionStatusMap.containskey(mission.id) && missionStatusMap.get(mission.id) < 100){
                            buttonJoin = false;
                        }
                    }
                }
            }
        } 
    }


    public list<FieloCH__Mission__c> missions{get;set;}
    public map<Id,Decimal> missionStatusMap{get;set;}
    public FieloEE__Member__c member{get;set;}

    public list<Decimal> getMissionFromExpression(String logicalExpression){
        list<Decimal> listReturn = new list<Decimal>();

        logicalExpression = logicalExpression.toUpperCase();
        logicalExpression = logicalExpression.replaceAll('AND', ',');
        logicalExpression = logicalExpression.replaceAll('OR', ',');
        logicalExpression = logicalExpression.replaceAll('\\(', '');
        logicalExpression = logicalExpression.replaceAll('\\)', '');
        logicalExpression = logicalExpression.replaceAll(' ', '');
        String[] listString = logicalExpression.split(',', -1);
        for(integer i=0; i < listString.size() ; i++){
            listReturn.add(Decimal.valueof(listString[i]));
        }

        return listReturn;
    }

    public list<String> fieldset{
        get{
            if(this.fieldSet == null){
                this.fieldSet = new List<String>{'Name','progressbar'};
            }
            return this.fieldset;
        }
        set{
            this.fieldSet = new List<String>{'Name','progressbar'}; 
            }
        }

    public FieloPRM_AP_CompletionPercentageCon() {
         if(challengeId == null){
             string idMenu = ApexPages.currentPage().getParameters().get('idMenu');
             challengeId = [SELECT id FROM FieloCH__Challenge__c WHERE F_PRM_MenuOverview__c = :idMenu  limit 1].id;
        }
    }
   
    public PageReference JoinAction() {

        FieloEE__Event__c eventRegistration = new FieloEE__Event__c();
        eventRegistration.FieloEE__Member__c = member.id;
        eventRegistration.FieloEE__Type__c = 'Registration';
        eventRegistration.name = 'Registration' ;
        eventRegistration.F_PRM_ValueText__c = challenge.id ;
        
        insert eventRegistration;

        return new PageReference('/Menu/' + challenge.F_PRM_MenuProgram__r.fieloEE__ExternalName__C);
    }
}