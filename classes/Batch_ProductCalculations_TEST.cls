/*
    Description: Test Class for Batch_ProductCalculations
*/
@isTest
public class Batch_ProductCalculations_TEST
{
     static testMethod void testProduct2Updates()
    {
        Integer numBatchJobs;
        OPP_Product__c prod=new OPP_Product__c();
        prod.Name='ECOBUILDINGStest';
        prod.BusinessUnit__c='ECOBUILDINGStest';
        prod.TECH_PM0CodeInGMR__c='A1';
        insert prod;
        Product2 pr=new Product2();
        pr.Name='GIR_AD_LF_CB_08test';
        pr.ProductGDP__c=prod.id;
        pr.ExtProductId__c='123456';
        insert pr;
        numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5){
            Batch_ProductCalculations prodUpdates = new Batch_ProductCalculations(); 
            database.executebatch(prodUpdates );
        }
        Test.startTest();
        numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5){
            Batch_ProductCalculations sh1 = new Batch_ProductCalculations ();
            DateTime currentDate = System.Now().addHours(5)  ;
             String CRON_EXPRESSION = '' + 59 + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' 
                                  + currentDate.day() + ' ' + currentDate.month() + ' ? ' + currentDate.year();
        
            String jobId = System.schedule('Product2 update', CRON_EXPRESSION, sh1);
        }
        Test.stopTest();
    }
}