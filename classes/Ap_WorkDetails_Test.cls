@isTest(SeeAllData=true)
public class Ap_WorkDetails_Test{

    //static Recordtype rt1=[SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE DeveloperName='shipment' and SobjectType ='SVMXC__RMA_Shipment_Line__c'];
    //static Recordtype rt=[SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE DeveloperName='RMA' and SobjectType ='SVMXC__RMA_Shipment_Line__c'];
    static String ServicedProduct_RT = System.Label.CLQ316SRV02;
    static String Parts_RT = System.Label.CLJUL15SRV04;
    static String actualRT = Label.CLSSRV_WDD_RecordType;
    static Product2 sku = [SELECT id,Name,ProductCode FROM Product2 WHERE Name = : 'BK500'];
    
    static List<Country__c> countryList = new List<Country__c>();
    static Country__c country1;
    static List<StateProvince__c> stateProvinceList = new List<StateProvince__c>();
    static StateProvince__c stateCountry1_01;
    static StateProvince__c stateCountry1_02;
    
    static Account acc1;
    static SVMXC__Site__c site;
    
    static Set<Id> woid = new Set<Id>();
    
    static SVMXC__Service_Order__c workOrder;
    static SVMXC__Installed_Product__c ip0;
    static SVMXC__Installed_Product__c ip1;
    static SVMXC__Installed_Product__c ip2;
    static SVMXC__Installed_Product__c ip3;
    static SVMXC__Installed_Product__c ip4;
    static SVMXC__Installed_Product__c ip5;
    static SVMXC__Installed_Product__c ip6;
    static SVMXC__Installed_Product__c ip7;
    static SVMXC__Installed_Product__c ip8;
    static SVMXC__Installed_Product__c ip9;
    static SVMXC__Installed_Product__c ip99;
    
    static List<SVMXC__Service_Order_Line__c> wdlist;
    static SVMXC__Service_Order_Line__c wd;
    static SVMXC__Service_Order_Line__c wd1;
    static SVMXC__Service_Order_Line__c wd2;
    static SVMXC__Service_Order_Line__c wd3;
    static SVMXC__Service_Order_Line__c wd4;
    static SVMXC__Service_Order_Line__c wd5;
    static SVMXC__Service_Order_Line__c wd6;
    static SVMXC__Service_Order_Line__c wd7;
    static SVMXC__Service_Order_Line__c wd8;
    static SVMXC__Service_Order_Line__c wd9;
    
    static Set<string> poName;
    static SVMXC__RMA_Shipment_Line__c poline;
    
    static SVMXC__Service_Group__c st;
    
    static Set<string> rmaset;
    static SVMXC__Service_Group_Members__c group1;
    
    static Set<Id> polset;

    
    static {    

        country1 = Utils_TestMethods.createCountry();
        country1.name='France';
        country1.CountryCode__c = 'FR';
        countryList.add(country1);
        
        upsert countryList CountryCode__c;
        
        // Create States/Provinces
        
        stateCountry1_01 = new StateProvince__c(Name='FR - Paris', Country__c=country1.Id, CountryCode__c=country1.CountryCode__c, StateProvinceCode__c='75');
        stateProvinceList.add(stateCountry1_01);
        stateCountry1_02 = new StateProvince__c(Name='FR - Hauts-de-Seine', Country__c=country1.Id, CountryCode__c=country1.CountryCode__c, StateProvinceCode__c='92');
        stateProvinceList.add(stateCountry1_02);
        
        System.debug('#### Inserting stateProvinceList: '+stateProvinceList);
        insert stateProvinceList;
        System.debug('#### Inserted stateProvinceList: '+stateProvinceList);
        
        
        acc1 = Utils_TestMethods.createAccount();
        acc1.country__c = country1.Id;
        acc1.PostalStateProvince__c = stateCountry1_01.Id;
        acc1.Name = 'Account FR 01';
        acc1.City__c = 'Paris';
        acc1.PostalStateProvince__c = stateCountry1_01.Id;
        acc1.StateProvince__c = stateCountry1_01.Id;
        acc1.BillingCity = acc1.City__c;
        insert acc1;
        
    

        site = new SVMXC__Site__c();
        site.Name = 'Test Location12';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc1.id;
        site.TimeZone__c = 'Test';
        site.PrimaryLocation__c = true;
        insert site;

        
        workOrder =  Utils_TestMethods.createWorkOrder(acc1.id);
        workOrder.Work_Order_Category__c='On-site';                 
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.BackOfficeReference__c = '111111';
        workOrder.IsBillable__c = 'No';
        workOrder.Comments_to_Planner__c='testing';
        workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
        workOrder.SVMXC__Site__c = site.Id;
        insert workOrder;
        woid.add(workOrder.Id);
        
        
        //Installed prdoduct
        
        List <SVMXC__Installed_Product__c> ipList = new List<SVMXC__Installed_Product__c>();

        ip0 = new SVMXC__Installed_Product__c();
        ip0.SVMXC__Company__c = acc1.id;
        ip0.Name = 'Test Intalled Product IP0';
        ip0.SVMXC__Status__c= 'new';
        ip0.GoldenAssetId__c = 'GoldenAssertId0';
        ip0.BrandToCreate__c ='Test Brand';
        ip0.DeviceTypeToCreate__c ='Test Device Type';
        ip0.SVMXC__Site__c = site.id;
        
        ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = acc1.id;
        ip1.Name = 'Test Intalled Product 1';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoldenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.DeviceTypeToCreate__c ='Test Device Type';
        ip1.SVMXC__Site__c = site.id;
        
        ip2 = new SVMXC__Installed_Product__c();
        ip2.SVMXC__Company__c = acc1.id;
        ip2.Name = 'Test Intalled Product 2';
        ip2.SVMXC__Status__c= 'new';
        ip2.GoldenAssetId__c = 'GoldenAssertId2';
        ip2.BrandToCreate__c ='Test Brand1';
        ip2.DeviceTypeToCreate__c ='Test Device Type';
        ip2.SVMXC__Site__c = site.id;

        ip3 = new SVMXC__Installed_Product__c();
        ip3.SVMXC__Company__c = acc1.id;
        ip3.Name = 'Test Intalled Product 3';
        ip3.SVMXC__Status__c= 'new';
        ip3.GoldenAssetId__c = 'GoldenAssertId3';
        ip3.DeviceTypeToCreate__c ='Test Device Type';
        ip3.BrandToCreate__c ='Test Brand1';
        ip3.SVMXC__Site__c = site.id;
        
        ip6 = new SVMXC__Installed_Product__c();
        ip6.SVMXC__Company__c = acc1.id;
        ip6.Name = 'Test Intalled Product 6';
        ip6.SVMXC__Status__c= 'new';
        ip6.GoldenAssetId__c = 'GoldenAssertId6';
        ip6.DeviceTypeToCreate__c ='Test Device Type';
        ip6.BrandToCreate__c ='Test Brand1';
        ip6.SVMXC__Site__c = site.id;
        
        ip9 = new SVMXC__Installed_Product__c();
        ip9.SVMXC__Company__c = acc1.id;
        ip9.Name = 'Test Intalled Product 9';
        ip9.SVMXC__Status__c= 'new';
        ip9.GoldenAssetId__c = 'GoldenAssertId9';
        ip9.DeviceTypeToCreate__c ='Test Device Type';
        ip9.BrandToCreate__c ='Test Brand1';
        ip9.SVMXC__Site__c = site.id;
        
        ip99 = new SVMXC__Installed_Product__c();
        ip99.SVMXC__Company__c = acc1.id;
        ip99.Name = 'Test Intalled Product 99';
        ip99.SVMXC__Status__c= 'new';
        ip99.GoldenAssetId__c = 'GoldenAssertId99';
        ip99.DeviceTypeToCreate__c ='Test Device Type';
        ip99.BrandToCreate__c ='Test Brand1';
        ip99.SVMXC__Site__c = site.id;
        
        ipList.add(ip0);
        ipList.add(ip1);
        ipList.add(ip2);
        ipList.add(ip3);
        ipList.add(ip6);
        ipList.add(ip9);
        ipList.add(ip99);
        
        try{
            Database.SaveResult[] results = Database.insert(ipList,false);
            if (results != null){
                for (Database.SaveResult result : results) {
                    if (!result.isSuccess()) {
                        Database.Error[] errs = result.getErrors();
                        for(Database.Error err : errs)
                            System.debug('#### Error while inserting IP: '+err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }
        catch (DMLexception e){
            System.debug('###Error while trying to insert ipList: '+ipList+' - Error: '+e.getMessage());
        }
             
        st= new SVMXC__Service_Group__c(Name='test123');
        insert st;
       
        group1= new SVMXC__Service_Group_Members__c(Name='testfsr',SVMXC__Service_Group__c=st.id);
        insert group1;
       
        
        rmaset = new Set<string>();
        
        
        wdlist = new list<SVMXC__Service_Order_Line__c>();
        
    }
    
    
    private static testMethod void myTest() {
        SVMXC_WorkDetailsBefore.setBillableFlagFromWOAndIsAdminFlag(null, null, null, null);
        SVMXC_WorkDetailsBefore.clearSecondsFromStartAndEndTime(null, null, null, null);
        SVMXC_WorkDetailsBefore.setTechWhenNull(null, null, null, null);
        SVMXC_WorkDetailsBefore.setFSRTimesFromCustomerTimes(null, null, null, null);
    }
    
    
    
    static testMethod void TestWorkDetail1(){
        System.debug('#### Starting AP_WorkDetails_Test.TestWorkOrder()');
                
        poName = new Set<string>();
        SVMXC__RMA_Shipment_Order__c partsOrder1 = new SVMXC__RMA_Shipment_Order__c();
        partsOrder1.SVMXC__Company__c =acc1.Id ;
        partsOrder1.SVMXC__Service_Order__c = workOrder.Id;
        partsOrder1.recordtypeid = System.Label.CLJUL15SRV03;
        partsOrder1.SVMXC__Order_Status__c='Open';

        SVMXC__RMA_Shipment_Order__c partsOrder2 = new SVMXC__RMA_Shipment_Order__c();
        partsOrder2.SVMXC__Company__c =acc1.Id ;
        partsOrder2.SVMXC__Service_Order__c = workOrder.Id;
        partsOrder2.recordtypeid = System.Label.CLJUL15SRV03;
        partsOrder2.SVMXC__Order_Status__c='Open';
        
        SVMXC__RMA_Shipment_Order__c partsOrder3 = new SVMXC__RMA_Shipment_Order__c();
        partsOrder3.SVMXC__Company__c =acc1.Id ;
        partsOrder3.SVMXC__Service_Order__c = workOrder.Id;
        partsOrder3.recordtypeid = System.Label.CLJUL15SRV03;
        partsOrder3.SVMXC__Order_Status__c='Open';
        
        SVMXC__RMA_Shipment_Order__c partsOrder4 = new SVMXC__RMA_Shipment_Order__c();
        partsOrder4.SVMXC__Company__c =acc1.Id ;
        partsOrder4.SVMXC__Service_Order__c = workOrder.Id;
        partsOrder4.recordtypeid = System.Label.CLJUL15SRV03;
        partsOrder4.SVMXC__Order_Status__c='Open';
        
        List<SVMXC__RMA_Shipment_Order__c> partsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
        partsOrderList.add(partsOrder1);
        partsOrderList.add(partsOrder2);
        partsOrderList.add(partsOrder3);
        partsOrderList.add(partsOrder4);
        
        System.debug('#### Inserting parts orders: '+partsOrderList);
        insert partsOrderList;
        System.debug('#### Inserted parts orders: '+partsOrderList);
        
        List<SVMXC__RMA_Shipment_Order__c> insertedPartsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
        for (SVMXC__RMA_Shipment_Order__c insertedPartsOrder : [Select Id, Name, SVMXC__Company__c, RecordTypeID, SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where id in :partsOrderList]) {
            poName.add(insertedPartsOrder.Name);
            insertedPartsOrderList.add(insertedPartsOrder);
        }
        System.debug('#### Parts Order Names: '+poName);


        
        List<SVMXC__Service_Order_Line__c> workDetailsToInsert = new List<SVMXC__Service_Order_Line__c>();
        wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Service_Order__c = workOrder.Id;
        wd.SVMXC__From_Location__c = site.Id;
        wd.IsBillable__c = 'No';
        wd.SVMXC__Group_Member__c=group1.id;
        wd.SVMXC__Start_Date_and_Time__c = system.now();
        wd.TotalDuration__c = 30;
        //wd.RMANumber__c = 'WO-125';
        wd.ReturnedQuantity__c = 20;
        wd.SVMXC__Serial_Number__c = ip0.Id;
        wd.RecordTypeId = Parts_RT;
        wd.TrackingNumber__c = '124';
        wd.Return_Reason__c = 'test';
        wd.Parts__c = 'testclass';
        wd.Comments__c = 'AP_WorkDetails_Test - WD';
        System.debug('#### Inserting WD: '+wd);
        insert wd;
        System.debug('#### Inserted WD: '+wd);
        
        poline = new SVMXC__RMA_Shipment_Line__c();
        polset = new Set<Id>();
        poline.SVMXC__Serial_Number__c=wd.SVMXC__Serial_Number__c;
        //String RMA_RT = System.Label.CLAPR15SRV66;
        poline.Generic_reference__c=wd.Parts__c;
        poline.Tracking_number__c = wd.TrackingNumber__c;
        poline.recordtypeid='012A0000000nphY';
        poline.Return_Reason__c = wd.Return_Reason__c;
        poline.SVMXC__Expected_Quantity2__c=wd.ReturnedQuantity__c;
        poline.SVMXC__RMA_Shipment_Order__c=partsorder1.Id;
        

        System.debug('#### Removing SRV05 from hasRun list');
        Utils_SDF_Methodology.removeFromRunOnce('SRV05');
        System.debug('#### Starting test');
        
        System.debug('#### Inserting PO line: '+poline);
        insert poline;
        System.debug('#### Inserted PO line: '+poline);
    
        polset.add(poline.Id);

        

        wd1= new SVMXC__Service_Order_Line__c(PartsOrderLine__c=poline.id,recordtypeid='012A0000000nYLx',SVMXC__Service_Order__c=workOrder.Id,SVMXC__Serial_Number__c = ip1.Id);
        wd1.Comments__c = 'AP_WorkDetails_Test - WD1';

        //added by suhail 18816 start
        wd2 = new SVMXC__Service_Order_Line__c();
        wd2.SVMXC__Service_Order__c = workOrder.Id;
        wd2.SVMXC__From_Location__c = site.Id;
        wd2.IsBillable__c = 'No';
        wd2.SVMXC__Group_Member__c=group1.id;
        wd2.SVMXC__Start_Date_and_Time__c = system.now();
        wd2.TotalDuration__c = 30;
        //wd2.RMANumber__c = partsOrder1.Name;
        if (insertedPartsOrderList != null && insertedPartsOrderList.get(0) != null) {
            wd2.RMANumber__c = insertedPartsOrderList.get(0).Name;
        }
        wd2.ReturnedQuantity__c = 20;
        wd2.SVMXC__Serial_Number__c = ip2.Id;
        wd2.RecordTypeId =ServicedProduct_RT;
        wd2.IsSplitFlag__c = true;
        wd2.MIP_Flag__c = true;
        wd2.TrackingNumber__c = '124';
        wd2.Return_Reason__c = 'test';
        wd2.Parts__c = 'testclass';
        wd2.Comments__c = 'AP_WorkDetails_Test - WD2';
        try{
            //insert wd2;
        }
        catch (DMLexception e){
            System.debug('###Error while trying to insert wd2: '+wd2+' - Error: '+e.getMessage());
        }
        //added by suhail 18816 end

        //added by Nabil ZEGHACHE - BR-10471 start
        wd3 = new SVMXC__Service_Order_Line__c();
        wd3.SVMXC__Service_Order__c = workOrder.Id;
        wd3.SVMXC__From_Location__c = site.Id;
        wd3.IsBillable__c = 'No';
        wd3.SVMXC__Group_Member__c=group1.id;
        wd3.CustomerStartDateandTime__c = system.now();
        wd3.CustomerEndDateandTime__c = wd3.CustomerStartDateandTime__c.addHours(2);
        wd3.RecordTypeId = actualRT;
        wd3.Comments__c = 'AP_WorkDetails_Test - WD3';
        
        wd4 = new SVMXC__Service_Order_Line__c();
        wd4.SVMXC__Service_Order__c = workOrder.Id;
        wd4.SVMXC__From_Location__c = site.Id;
        wd4.IsBillable__c = 'No';
        wd4.SVMXC__Group_Member__c=group1.id;
        wd4.CustomerStartDateandTime__c = system.now();
        wd4.CustomerEndDateandTime__c = wd4.CustomerStartDateandTime__c.addHours(2);
        wd4.RecordTypeId = System.Label.CLJUL16SRV01;
        wd4.Comments__c = 'AP_WorkDetails_Test - wd4';
        
        
        wd5 = new SVMXC__Service_Order_Line__c();
        wd5.SVMXC__Service_Order__c = workOrder.Id;
        wd5.SVMXC__From_Location__c = site.Id;
        wd5.IsBillable__c = 'No';
        wd5.SVMXC__Group_Member__c=group1.id;
        wd5.CustomerStartDateandTime__c = system.now();
        wd5.CustomerEndDateandTime__c = wd5.CustomerStartDateandTime__c.addHours(2);
        wd5.RecordTypeId = System.Label.CLAPR15SRV59; //Estimate record type
        wd5.Comments__c = 'AP_WorkDetails_Test - wd5';
        
        wd6 = new SVMXC__Service_Order_Line__c();
        wd6.SVMXC__Service_Order__c = workOrder.Id;
        wd6.SVMXC__From_Location__c = site.Id;
        wd6.IsBillable__c = 'No';
        wd6.SVMXC__Group_Member__c=group1.id;
        wd6.SVMXC__Start_Date_and_Time__c = system.now();
        wd6.TotalDuration__c = 30;
        if (insertedPartsOrderList != null && insertedPartsOrderList.get(1) != null) {
            wd6.RMANumber__c = insertedPartsOrderList.get(1).Name;
        }
        wd6.ReturnedQuantity__c = 10;
        wd6.SVMXC__Serial_Number__c = ip6.Id;
        wd6.RecordTypeId = Parts_RT;
        wd6.TrackingNumber__c = '456';
        wd6.Return_Reason__c = 'test6';
        wd6.Parts__c = 'testclass6';
        wd6.Comments__c = 'AP_WorkDetails_Test - WD6';

        //added by Nabil ZEGHACHE - BR-10471 end
        
        //Creating WD's of Products_Serviced
        Id ProductServiced_RT = System.Label.CLQ316SRV02;
        SVMXC__Service_Order_Line__c WDps = new SVMXC__Service_Order_Line__c(
                                                                            recordtypeid = ProductServiced_RT,
                                                                            SVMXC__Service_Order__c=workOrder.Id,
                                                                            SVMXC__Serial_Number__c = ip99.Id,
                                                                            Comments__c = 'AP_WorkDetails_Test - WDps'
                                                                            );
        

        workDetailsToInsert.add(wd1);
        workDetailsToInsert.add(wd2);
        workDetailsToInsert.add(wd3);
        workDetailsToInsert.add(wd4);
        workDetailsToInsert.add(wd5);
        workDetailsToInsert.add(wd6);
        workDetailsToInsert.add(WDps);
        
        System.debug('#### Removing SRV05 from hasRun list');
        Utils_SDF_Methodology.removeFromRunOnce('SRV05');
        
        test.startTest();
        System.debug('#### Inserting Work Details: '+workDetailsToInsert);
        try {
            Database.SaveResult[] results = Database.insert(workDetailsToInsert,false);
            if (results != null){
                for (Database.SaveResult result : results) {
                    if (!result.isSuccess()) {
                        Database.Error[] errs = result.getErrors();
                        for(Database.Error err : errs)
                            System.debug('#### Error while inserting WD: '+err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }
        catch (Exception e) {
            System.debug('###Error while trying to insert workDetailsToInsert: '+workDetailsToInsert+' - Error: '+e.getMessage());
        }
        

        wdlist.add(wd);
        wdlist.add(wd1);
        wdlist.add(wd2);
        wdlist.add(wd3);
        wdlist.add(wd4);
        wdlist.add(wd5);
        wdlist.add(wd6);
        
        set <id> wdid = new set<id>();
        wdid.add(wd.id);
        wdid.add(wd1.id);
        wdid.add(wd2.id);
        wdid.add(wd3.id);
        wdid.add(wd4.id);
        wdid.add(wd5.id);
        wdid.add(wd6.id);

        rmaset.add(wd.RMANumber__c);
        rmaset.add(wd1.RMANumber__c);
        rmaset.add(wd2.RMANumber__c);
        rmaset.add(wd3.RMANumber__c);
        rmaset.add(wd4.RMANumber__c);
        rmaset.add(wd5.RMANumber__c);
        rmaset.add(wd6.RMANumber__c);
        
        wd3.CustomerStartDateandTime__c = wd3.CustomerStartDateandTime__c.addHours(1);
        wd3.CustomerEndDateandTime__c = wd3.CustomerStartDateandTime__c.addHours(4);
        
        if (insertedPartsOrderList != null && insertedPartsOrderList.get(2) != null) {
            wd6.RMANumber__c = insertedPartsOrderList.get(2).Name;
        }
        wd6.ReturnedQuantity__c = wd6.ReturnedQuantity__c -1;

        System.debug('#### Removing SRV06 from hasRun list');
        Utils_SDF_Methodology.removeFromRunOnce('SRV06');
        System.debug('#### Updating Work Details: '+workDetailsToInsert);
        try {
            Database.SaveResult[] results = Database.update(workDetailsToInsert,false);
            if (results != null){
                for (Database.SaveResult result : results) {
                    if (!result.isSuccess()) {
                        Database.Error[] errs = result.getErrors();
                        for(Database.Error err : errs)
                            System.debug('#### Error while inserting WD: '+err.getStatusCode() + ' - ' + err.getMessage());
                    }
                }
            }
        }
        catch (Exception e) {
            System.debug('###Error while trying to update wd3: '+wd3+' - Error: '+e.getMessage());
        }
                
        List<SVMXC__Service_Order_Line__c> wddelete = new list<SVMXC__Service_Order_Line__c>();
        wddelete.add(WDps);
 
 
        set <id> WDToDelete = new set<id>();
        map<id,id> WOMIPMap = new map<id,id>();
        map<id,string>WOSNProdMap = new map<id,string>();
        string testSNProdId = 'TEST#'+sku.id;
        for (SVMXC__Service_Order_Line__c wds : wddelete)
        {
            WDToDelete.add(wds.id);
            WOMIPMap.put(wds.SVMXC__Service_Order__c,wds.id);
            WOSNProdMap.put(wds.SVMXC__Service_Order__c,testSNProdId);    
        }
        //AP_WorkDetails.UpdateWD(woid,wdlist);
        AP_WorkDetails.Createpartorderline(rmaset,wdlist);
        AP_WorkDetails.partsordertracknumberupdate(polset,wdid);
        AP_WorkDetails.partsorderlineupdate(wdlist,polset,poName);
        AP_WorkDetails.DeleteWD(WDToDelete);
        try{
            AP_WorkDetails.UpdateWOMIP(WOMIPMap,WOSNProdMap);
        }
        catch (Exception e){
            System.debug('#### Error: '+e.getMessage());
        }
        test.StopTest();
    }

}