/**
 * @Author: Chris Hurd
 * Created Date: 05-03-2013
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class ServiceMaxSwapAsset_TEST {
    
    static testMethod void test1() {
    Map<id,id> ipmap= new Map<id,id>();
         SVMXC__Installed_Product__c dIP = new SVMXC__Installed_Product__c(Name = 'decommissioned IP');
           insert dIP;
    
        SVMXC__Installed_Product__c newIP = new SVMXC__Installed_Product__c(Name = 'New IP');
        insert newIP;
        string ProductServiced = Label.CLAPR15SRV60;
        Test.startTest();
        
        //Create asset swap object
        Swap_Asset__c sAsset = new Swap_Asset__c();
        sAsset.CommissioningDateInstallDate__c = system.today().addDays(5);
        sAsset.DecommissionedDate__c = system.today().addDays(4);
        sAsset.DeinstalledInstalledProduct__c = dIP.Id;
        sAsset.DeinstalledStatus__c = 'Not Installed';
        sAsset.InstalledProduct__c = newIP.Id;
        sAsset.Status__c = 'Installed';
        sAsset.ConfirmSwap__c = true;
         insert sAsset;  
       // ipmap.put(sAsset.DeinstalledInstalledProduct__c,sAsset.InstalledProduct__c);
        
        // Ap_InstalledProductSwap.IpSwapPmPlanCoverage(ipmap);
    SVMXC__PM_Plan__c pmplan = new SVMXC__PM_Plan__c(name='test123');
        insert pmplan;
        SVMXC__PM_Coverage__c pmcov= new SVMXC__PM_Coverage__c(SVMXC__Product_Name__c=sAsset.DeinstalledInstalledProduct__c,SVMXC__PM_Plan__c=pmplan.id);
        insert pmcov;
    SVMXC__Service_Order__c wo= new SVMXC__Service_Order__c(SVMXC__Order_Status__c='New',SVMXC__Component__c=sAsset.DeinstalledInstalledProduct__c);
        insert wo;
        SVMXC__Service_Order_Line__c wd= new SVMXC__Service_Order_Line__c(SVMXC__Serial_Number__c=sAsset.DeinstalledInstalledProduct__c,RecordTypeId=ProductServiced,SVMXC__Service_Order__c=wo.id);
        insert wd;
        Test.stopTest();
        
        
        
        
    }

    static testMethod void test() {
        SVMXC__Installed_Product__c dIP = new SVMXC__Installed_Product__c();
        dIP.Name = 'decommissioned IP';
        insert dIP;
        
        SVMXC__Installed_Product__c newIP = new SVMXC__Installed_Product__c();
        newIP.Name = 'New IP';
        insert newIP;
        
        Test.startTest();
        
        //Create asset swap object
        Swap_Asset__c sAsset = new Swap_Asset__c();
        sAsset.CommissioningDateInstallDate__c = system.today().addDays(5);
        sAsset.DecommissionedDate__c = system.today().addDays(4);
        sAsset.DeinstalledInstalledProduct__c = dIP.Id;
        sAsset.DeinstalledStatus__c = 'Not Installed';
        sAsset.InstalledProduct__c = newIP.Id;
        sAsset.Status__c = 'Installed';
        sAsset.ConfirmSwap__c = true;
        try{
         insert sAsset;   
        }
        catch (exception e){}
        
        
        //Check installed products
        newIP = [SELECT Id, CommissioningDateInstallDate__c, SVMXC__Date_Installed__c, DecomissioningDate__c, LifeCycleStatusOfTheInstalledProduct__c,
                  SVMXC__Status__c, SVMXC__Warranty_Start_Date__c FROM SVMXC__Installed_Product__c WHERE Id = :newIP.Id LIMIT 1];
      
        
        dIP = [SELECT Id, CommissioningDateInstallDate__c, SVMXC__Date_Installed__c, DecomissioningDate__c, LifeCycleStatusOfTheInstalledProduct__c,
                  SVMXC__Status__c, SVMXC__Warranty_Start_Date__c FROM SVMXC__Installed_Product__c WHERE Id = :dIP.Id LIMIT 1];
                  
      

        Test.stopTest();
    }
}