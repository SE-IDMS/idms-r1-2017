@IsTest
class AP12_CaseIntegrationMethods_TEST
{
    static testMethod void testAP12_CaseIntegrationMethodsForGMR() 
    {
       Account Acc = Utils_TestMethods.createAccount();
       Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
       Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
       
       insert Acc;
       insert Ctct;
       insert Cse;
       
       DataTemplate__c GMRProduct = new DataTemplate__c();
       GMRProduct.Field1__c='Test1';
       GMRProduct.Field2__c='Test2';
       GMRProduct.Field3__c='Test3';
       GMRProduct.Field4__c='Test4';
       GMRProduct.Field5__c='Test5';
       GMRProduct.Field6__c='Test6';
       GMRProduct.Field7__c='Test7';
       GMRProduct.Field8__c='Test8';
       GMRProduct.Field9__c='Test9';
       GMRProduct.Field10__c='Test10';
       GMRProduct.Field11__c='Test11';
              
       AP12_CaseIntegrationMethods.UpdateCasePM0(Cse,GMRProduct);
       AP12_CaseIntegrationMethods.UpdateCaseFamily(Cse,GMRProduct);
        
    }
    static testMethod void testAP12_CaseIntegrationMethodsForInquira() 
    {
       Account Acc = Utils_TestMethods.createAccount();
       Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
       Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
       
       insert Acc;
       insert Ctct;
       insert Cse;
       
       List<InquiraFAQ__c> FAQList = new List<InquiraFAQ__c>();
       InquiraFAQ__c FAQ = new InquiraFAQ__c();
       FAQList.add(FAQ);
       
       Case Cse1 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
       Insert Cse1;
       Cse1.AnswertoCustomer__c = 'Answer';
       Update Cse1;
       
       InquiraFAQ__c Inq = new InquiraFAQ__c();
       Inq.Title__c = 'Test';
       Inq.Excerpt__c = 'Test';
       Inq.URL__c = 'Test';
       Inq.Case__c = Cse1.Id;
       Insert(Inq);
           
       AP12_CaseIntegrationMethods.AttachInquiraFAQ(Cse,FAQList);
       AP12_CaseIntegrationMethods.CheckExistingFAQ(Cse);
       AP12_CaseIntegrationMethods.UpdateCaseWithInquiraFaQ(Cse1);      
        
    }
    static testMethod void testAP12_CaseIntegrationMethodsForDraftFAQ() 
    {
       Account Acc = Utils_TestMethods.createAccount();
       Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
       Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
       
       insert Acc;
       insert Ctct;
       insert Cse;
       String selectedlanguage='EN';
       String selectedCountry='US';
       String target='APS_Global';
       String title='This is test';
       String Answer='This is Test';
       List<Attachment> attachedfiles = new list<Attachment>();
       AP12_CaseIntegrationMethods.DraftInquiraFaQ(cse,selectedlanguage,selectedCountry,target,title,Answer,attachedfiles);   
        
    }
}