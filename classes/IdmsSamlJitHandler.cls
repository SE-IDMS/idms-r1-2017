//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
//June1
global class IdmsSamlJitHandler implements Auth.SamlJitHandler {


    IDMSCaptureError errorLog=new IDMSCaptureError ();
    
    @future (callout=true)
    public static void notifyUIMSforSync(String assertion, Map<String, String> attributes, String federationIdentifier){
      IDMSCaptureError log=new IDMSCaptureError ();
     try {
     
        System.debug('in updateUser method+++++'+attributes);
     
        WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort cls = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
        cls.clientCertName_x = System.Label.CLJUN16IDMS103;
            
           
                
        WS_IdmsUimsUserManagerV2Service.userV5 identity = new WS_IdmsUimsUserManagerV2Service.userV5();            
        
        If (federationIdentifier !=null && federationIdentifier.length()>0)
        identity.federatedID = federationIdentifier;
        system.debug('notifyUIMSforSync Fed id= '+ federationIdentifier);
        system.debug('notifyUIMSforSync Fed id in Identity object= '+ identity.federatedID);
        If (attributes.get('email') !=null && attributes.get('email').length()>0)
        identity.email = attributes.get('email') ;
        
        If (attributes.get('FirstName') !=null && attributes.get('FirstName').length()>0)
        identity.firstName =attributes.get('FirstName');
        
        system.debug('notifyUIMSforSync first name in Identity object= '+ identity.firstName);
        
        If (attributes.get('LastName') !=null && attributes.get('LastName').length()>0)
        identity.lastName = attributes.get('LastName');
        
        If (attributes.get('languageCode') !=null && attributes.get('languageCode').length()>0)
        identity.languageCode = attributes.get('languageCode');
        
        If (attributes.get('countryCode') !=null && attributes.get('countryCode').length()>0)
        identity.countryCode = attributes.get('countryCode');
        
        If (attributes.get('state') !=null && attributes.get('state').length()>0)
        identity.state = attributes.get('state');            
        
        If (attributes.get('county') !=null && attributes.get('county').length()>0)    
        identity.county = attributes.get('county');        
        
        If (attributes.get('street') !=null && attributes.get('street').length()>0)
        identity.street = attributes.get('street');
        
        
        If (attributes.get('postalCode') !=null && attributes.get('postalCode').length()>0)
        identity.postalCode = attributes.get('postalCode');
        
        If (attributes.get('postOfficeBoxCode') !=null && attributes.get('postOfficeBoxCode').length()>0)
        identity.postOfficeBoxCode= attributes.get('postOfficeBoxCode');
        
        If (attributes.get('mobile') !=null && attributes.get('mobile').length()>0)
        identity.cell= attributes.get('mobile'); 
        
        If (attributes.get('telephoneNumber') !=null && attributes.get('telephoneNumber').length()>0)       
        identity.phone=attributes.get('telephoneNumber');
        
        
        If (attributes.get('addInfoAddress') !=null && attributes.get('addInfoAddress').length()>0)
        identity.addInfoAddress=attributes.get('addInfoAddress');
        
        If (attributes.get('locality') !=null && attributes.get('locality').length()>0)
        identity.localityName=attributes.get('locality');
        
        String callerFid=Label.CLJUN16IDMS104;
        boolean ret=cls.updateUser(callerFid,assertion,identity);
        
        system.debug('UIMS update user after JIT creation = '+ret); 
     }
     
     catch (Exception e){
        
            system.debug('UIMS update user exception = '+e.getMessage());
            log.IDMScreateLog('IdmsSamlJitHandler','IdmsSamlJitHandler','updateUser',attributes.get('email')+Label.CLJUN16IDMS71,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'',federationIdentifier,false,'',false,null);            
        }
    }
    
    //Method executed when SAML assertion comes with Federation Id not available in User object
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
       
       User jitUser = new user(); 
       try{
           system.debug('in create user ++++++');
           
           list<user> userPresent=[select id,federationIdentifier from user where username=:attributes.get('email')+Label.CLJUN16IDMS71  or federationIdentifier=:federationIdentifier];
           
           if(userPresent!=null && userPresent.size()>0)
           {
           
           system.debug('user found ++++++');
           
           system.debug(userPresent[0]);
           
           userPresent[0].federationIdentifier=federationIdentifier;
           update userPresent[0];    
           
           list<USerSAMLAssertion__c> isRecordPresent=[select id from USerSAMLAssertion__c where UserId__c=:userPresent[0].id];
                
           system.debug('size of object ++++ '+isRecordPresent.size());
                
           if(isRecordPresent!=null && isRecordPresent.size()>0){
                system.debug('saml assertion present ++++++');
                updateUserAssertionRecord(isRecordPresent[0].id,assertion);
                
                }
                
           else{
                system.debug('saml assertion not present ++++++');  
                createUserAssertionRecord(userPresent[0].id,assertion);
                
                }
           
           return userPresent[0];
           }
           
           system.debug('user not present ++++++');
       
       
         
        System.debug('*** samlSsoProviderId=' + samlSsoProviderId);
        System.debug('*** communityId=' + communityId);
        System.debug('*** portalId=' + portalId);
        System.debug('*** federationIdentifier=' + federationIdentifier);
     
        System.debug('*** FirstName=' + attributes.get('FirstName'));
        System.debug('*** LastName =' + attributes.get('LastName'));
        System.debug('*** Email =' + attributes.get('email'));
        
        
        
        IdmsUserService userService = new IdmsUserService(); 
        
        
        If (federationIdentifier !=null && federationIdentifier.length()>0)        
                jitUser.FederationIdentifier = federationIdentifier;     
        
        If (attributes.get('LastName') !=null && attributes.get('LastName').length()>0)
                jitUser.LastName= attributes.get('LastName');

        If (attributes.get('FirstName') !=null && attributes.get('FirstName').length()>0)
            jitUser.FirstName=attributes.get('FirstName');
        
        If (attributes.get('email') !=null && attributes.get('email').length()>0)
            jitUser.Email=attributes.get('email');
        
        If (attributes.get('languageCode') !=null && attributes.get('languageCode').length()>0)
            jitUser.IDMS_PreferredLanguage__c=attributes.get('languageCode');
        
        If (attributes.get('county') !=null && attributes.get('county').length()>0)
            jitUser.IDMS_County__c=attributes.get('county');
        
        If (attributes.get('state') !=null && attributes.get('state').length()>0)
                jitUser.IDMS_State__c=attributes.get('state');    
        
        If (attributes.get('countryCode') !=null && attributes.get('countryCode').length()>0)
            jitUser.country__c=attributes.get('countryCode');
        
        If (attributes.get('mobile') !=null && attributes.get('mobile').length()>0)
            jitUser.MobilePhone=attributes.get('mobile');
        
        If (attributes.get('street') !=null && attributes.get('street').length()>0)
            jitUser.street=attributes.get('street');
        
        If (attributes.get('addInfoAddress') !=null && attributes.get('addInfoAddress').length()>0)
            jitUser.IDMS_AdditionalAddress__c=attributes.get('addInfoAddress');
        
        If (attributes.get('postOfficeBoxCode') !=null && attributes.get('postOfficeBoxCode').length()>0)
                    jitUser.IDMS_POBox__c=attributes.get('postOfficeBoxCode');
        
        If (attributes.get('postalCode') !=null && attributes.get('postalCode').length()>0)
            jitUser.PostalCode=attributes.get('postalCode');
        
        If (attributes.get('telephoneNumber') !=null && attributes.get('telephoneNumber').length()>0)
            jitUser.phone=attributes.get('telephoneNumber');
        
        If (attributes.get('locality') !=null && attributes.get('locality').length()>0)
            jitUser.city=attributes.get('locality');
        
        
        
        
        
        
        
        jitUser.IDMS_User_Context__c='Home';
        
       
        jitUser.IDMS_Registration_Source__c=label.CLJUN16IDMS145;
        
        system.debug('creating user ++++++');
        
        IDMSResponseWrapper idmsResponse= userService.createUser(jitUser);
        
        system.debug('User Salesforce Id = '+idmsResponse.IDMSUserRecord.Id);
        system.debug('response message = '+idmsResponse.Message);
        system.debug('response status = '+idmsResponse.Status);
        
        //insert line in USerSAMLAssertion__c to store SAML assertion for the new user
        system.debug('creating saml assertion record ++++++');
        
        createUserAssertionRecord(idmsResponse.IDMSUserRecord.Id,assertion);
        
        notifyUIMSforSync(assertion, attributes,federationIdentifier);
        
        }
        
       catch (Exception e){
       system.debug('Exception in JIT createUser = '+e.getMessage());
       errorLog.IDMScreateLog('IdmsSamlJitHandler','IdmsSamlJitHandler','createUser',attributes.get('email')+Label.CLJUN16IDMS71,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'',federationIdentifier,false,'',false,null);
       } 
        
        return jitUser;
                
    }

    //Method executed when SAML assertion comes with Federation Id already available in User object
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        
        try {
            system.debug('in update user ++++++'+attributes);
            
            System.debug('*** userId=' + userId);
            System.debug('*** FirstName=' + attributes.get('FirstName'));
            System.debug('*** LastName =' + attributes.get('LastName'));
            System.debug('*** Email =' + attributes.get('email'));
            System.debug('*** samlSsoProviderId=' + samlSsoProviderId);
            System.debug('*** communityId=' + communityId);
            System.debug('*** federationIdentifier=' + federationIdentifier);
     
            User jitUser = new user(); 
      
        
            IdmsUserService userService = new IdmsUserService(); 
        
        
            //verify that each attribute is non null and not empty
            
         
     
        If (federationIdentifier !=null && federationIdentifier.length()>0)        
                jitUser.FederationIdentifier = federationIdentifier;     
        
        If (attributes.get('LastName') !=null && attributes.get('LastName').length()>0)
                jitUser.LastName= attributes.get('LastName');

        If (attributes.get('FirstName') !=null && attributes.get('FirstName').length()>0)
            jitUser.FirstName=attributes.get('FirstName');
        
        If (attributes.get('email') !=null && attributes.get('email').length()>0)
            jitUser.Email=attributes.get('email');
        
        If (attributes.get('languageCode') !=null && attributes.get('languageCode').length()>0)
            jitUser.IDMS_PreferredLanguage__c=attributes.get('languageCode');
        
        If (attributes.get('county') !=null && attributes.get('county').length()>0)
            jitUser.IDMS_County__c=attributes.get('county');
        
        If (attributes.get('state') !=null && attributes.get('state').length()>0)
                jitUser.IDMS_State__c=attributes.get('state');    
        
        If (attributes.get('countryCode') !=null && attributes.get('countryCode').length()>0)
            jitUser.country__c=attributes.get('countryCode');
        
        If (attributes.get('mobile') !=null && attributes.get('mobile').length()>0)
            jitUser.MobilePhone=attributes.get('mobile');
        
        If (attributes.get('street') !=null && attributes.get('street').length()>0)
            jitUser.street=attributes.get('street');
        
        If (attributes.get('addInfoAddress') !=null && attributes.get('addInfoAddress').length()>0)
            jitUser.IDMS_AdditionalAddress__c=attributes.get('addInfoAddress');
        
        If (attributes.get('postOfficeBoxCode') !=null && attributes.get('postOfficeBoxCode').length()>0)
                    jitUser.IDMS_POBox__c=attributes.get('postOfficeBoxCode');
        
        If (attributes.get('postalCode') !=null && attributes.get('postalCode').length()>0)
            jitUser.PostalCode=attributes.get('postalCode');
        
        If (attributes.get('telephoneNumber') !=null && attributes.get('telephoneNumber').length()>0)
            jitUser.phone=attributes.get('telephoneNumber');
        
        If (attributes.get('locality') !=null && attributes.get('locality').length()>0)
            jitUser.city=attributes.get('locality');
            
            
            
            
            
            //add remaining mandatort fields such as country etc...
            jitUser.IDMS_Profile_update_source__c=label.CLJUN16IDMS145;
            
            //update line in USerSAMLAssertion__c object
            USerSAMLAssertion__c userSamlAssertionRecord=new USerSAMLAssertion__c();
            
            list<USerSAMLAssertion__c> isRecordPresent=[select id from USerSAMLAssertion__c where UserId__c=:userId];
            
            if(isRecordPresent!=null && isRecordPresent.size()>0){
                system.debug('in saml assertion present ++++++');
                updateUserAssertionRecord(isRecordPresent[0].id,assertion);
                
                }
                
            else{
                  system.debug('saml assertion not present ++++++');          
                createUserAssertionRecord(userId,assertion);
                
                }
            
            IDMSResponseWrapper idmsResponse=userService.updateUser(jitUser);
            
            
            } catch (Exception e) {
            //add custom error log
            system.debug('Exception in JIT updateUser = '+e.getMessage());
            errorLog.IDMScreateLog('IdmsSamlJitHandler','IdmsSamlJitHandler','updateUser',attributes.get('email')+Label.CLJUN16IDMS71,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'',federationIdentifier,false,'',false,null);
            }

            
        }
        
        @future
        public static void createUserAssertionRecord(String id,string assertion){
        
        USerSAMLAssertion__c userSamlAssertionRecord=new USerSAMLAssertion__c();
        userSamlAssertionRecord.UserId__c=id;
        userSamlAssertionRecord.Assertion__c=assertion;
        //userSamlAssertionRecord.RelatedUser__c=[select id from user where id=:id].id;
        if(id != null){
            userSamlAssertionRecord.RelatedUser__c=id;
        }
        insert userSamlAssertionRecord;
        
        }
        
        @future
        public static void updateUserAssertionRecord(String id,string assertion){
        
        USerSAMLAssertion__c userSamlAssertionRecord=new USerSAMLAssertion__c();
        
        userSamlAssertionRecord.Id=id;
        userSamlAssertionRecord.Assertion__c=assertion;
        
        update userSamlAssertionRecord;
        
        }
            
    }