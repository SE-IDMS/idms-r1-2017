/**
@Author: Prashanth GS
Description: This covers Social handler classes such as Google,LinkedIn,Twitter
Release: Sept 2016
Test class of idmsSocialReg handler
**/

@isTest
private class IdmsSocialRegHandlerTest{
    
    //test 1 : google registration
    static testMethod void testGoogleRegHandler() {
        IdmsGooglePlusRegHandler handler = new IdmsGooglePlusRegHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'google',
                                                     null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS Home'});
        
        test.startTest();
        handler.canCreateUser(sampleData);
        
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'Test123';
        insert cust_obj;
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Test123',
                                                                                        AppName__c='Home',context__c='home');
        insert iDMSApplicationMapping;
        
        IDMS_Profile_Mapping__c  ProfileMap = new IDMS_Profile_Mapping__c();
        ProfileMap.Name = 'home';
        ProfileMap.Profile_Name__c = 'SE - IDMS Home';
        insert ProfileMap;
        //System.assert(ProfileMap!=null);
        
        
        
        User objUser= handler.createUser(null, sampleData);
        //System.assert(objUser!=null);
        
        delete cust_obj;
        delete iDMSApplicationMapping;
        delete ProfileMap;
        
        IdmsSocialUser__c cust_obj1 = new IdmsSocialUser__c() ;
        cust_obj1.name=  'IdmsKey';
        cust_obj1.AppValue__c = 'Test123';
        insert cust_obj1;
        
        IDMSApplicationMapping__c iDMSApplicationMapping1= new IDMSApplicationMapping__c(Name='Test123',
                                                                                         AppName__c='Work',context__c='work');
        insert iDMSApplicationMapping1;
        
        IDMS_Profile_Mapping__c  ProfileMap1 = new IDMS_Profile_Mapping__c();
        ProfileMap1.Name = 'work';
        ProfileMap1.Profile_Name__c = 'SE - IDMS - Standard B2B User';
        insert ProfileMap1;
        //System.assert(ProfileMap1!=null);
        
        
        Auth.UserData sampleData2 = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                      'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'google',
                                                      null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS - Standard B2B User'});
        
        User objUser2= handler.createUser(null, sampleData2);
        
        handler.updateUser(objUser.Id, null, sampleData);
        test.stopTest();
    }
    
    //test 2 : linkedin registration
    static testMethod void testLinkedinRegHandler() {
        IdmsLinkedInRegHandler handler = new IdmsLinkedInRegHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'linkedin',
                                                     null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS Home'});
        
        test.startTest();
        handler.canCreateUser(sampleData);
        
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'Test123';
        insert cust_obj;
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Test123',
                                                                                        AppName__c='Home',context__c='home');
        insert iDMSApplicationMapping;
        
        IDMS_Profile_Mapping__c  ProfileMap = new IDMS_Profile_Mapping__c();
        ProfileMap.Name = 'home';
        ProfileMap.Profile_Name__c = 'SE - IDMS Home';
        insert ProfileMap;
        //System.assert(ProfileMap!=null);
        
        User objUser= handler.createUser(null, sampleData);
        //System.assert(objUser!=null);
        
        delete cust_obj;
        delete iDMSApplicationMapping;
        delete ProfileMap;
        
        IdmsSocialUser__c cust_obj1 = new IdmsSocialUser__c() ;
        cust_obj1.name=  'IdmsKey';
        cust_obj1.AppValue__c = 'Test123';
        insert cust_obj1;
        
        IDMSApplicationMapping__c iDMSApplicationMapping1= new IDMSApplicationMapping__c(Name='Test123',
                                                                                         AppName__c='Work',context__c='work');
        insert iDMSApplicationMapping1;
        
        IDMS_Profile_Mapping__c  ProfileMap1 = new IDMS_Profile_Mapping__c();
        ProfileMap1.Name = 'work';
        ProfileMap1.Profile_Name__c = 'SE - IDMS - Standard B2B User';
        insert ProfileMap1;
        //System.assert(ProfileMap1!=null);
        
        
        
        Auth.UserData sampleData2 = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                      'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'linkedin',
                                                      null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS - Standard B2B User'});
        
        User objUser2= handler.createUser(null, sampleData2);
        
        handler.updateUser(objUser.Id, null, sampleData);
        test.stopTest();
    }
    
    //test 1 : Twitter registration
    static testMethod void testTwitterRegHandler() {
        IdmsTwitterRegHandler handler = new IdmsTwitterRegHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'twitter',
                                                     null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS Home'});
        sampleData.identifier='758626877200297531';
        
        test.startTest();
        handler.canCreateUser(sampleData);
        
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'Test123';
        insert cust_obj;
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Test123',
                                                                                        AppName__c='Home',context__c='home');
        insert iDMSApplicationMapping;
        
        IDMS_Profile_Mapping__c  ProfileMap = new IDMS_Profile_Mapping__c();
        ProfileMap.Name = 'home';
        ProfileMap.Profile_Name__c = 'SE - IDMS Home';
        insert ProfileMap;
        //System.assert(ProfileMap!=null);
        
        User objUser= handler.createUser(null, sampleData);
        // System.assert(objUser!=null);
        
        delete cust_obj;
        delete iDMSApplicationMapping;
        delete ProfileMap;
        
        IdmsSocialUser__c cust_obj1 = new IdmsSocialUser__c() ;
        cust_obj1.name=  'IdmsKey';
        cust_obj1.AppValue__c = 'Test123';
        insert cust_obj1;
        
        IDMSApplicationMapping__c iDMSApplicationMapping1= new IDMSApplicationMapping__c(Name='Test123',
                                                                                         AppName__c='Work',context__c='work');
        insert iDMSApplicationMapping1;
        
        IDMS_Profile_Mapping__c  ProfileMap1 = new IDMS_Profile_Mapping__c();
        ProfileMap1.Name = 'work';
        ProfileMap1.Profile_Name__c = 'SE - IDMS - Standard B2B User';
        insert ProfileMap1;
        //System.assert(ProfileMap1!=null);
        
        
        
        Auth.UserData sampleData2 = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                      'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'twitter',
                                                      null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS - Standard B2B User'});
        
        User objUser2= handler.createUser(null, sampleData2);
        
        //handler.updateUser(objUser.Id, null, sampleData);
        test.stopTest();
    }
}