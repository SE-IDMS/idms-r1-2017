@isTest
private class DateTimeFieldUtils_UT {
    static testMethod void DateTimeFieldUtilsUnitTest() 
    {/*
        SVMXC__Service_Order__c so=new SVMXC__Service_Order__c();
         datetime dt = datetime.now().addDays(-1);
        so.SVMXC__Actual_Initial_Response__c =dt;
        insert so;*/
        Account account = new Account(
                                        Name ='Account',
                                        ShippingStreet='ABC',
                                        ShippingCity='PQR ',
                                        ShippingState = 'XYZ',
                                        ShippingPostalCode='111',
                                        ShippingCountry='XYZ',  
                                        BillingStreet='ABC',
                                        BillingCity='PQR ',
                                        BillingState = 'XYZ',
                                        BillingPostalCode='111',
                                        BillingCountry='XYZ'
                                        ); 
        insert account;
        
        system.assert(account != null);
 
        Contact contact = new Contact(LastName='Last',AccountId =account.Id,Email='svmxmailfortest@gmail.com');
        insert contact;
        
        system.assert(contact != null);
       
        Case case1 = new Case(Status ='New', Priority = 'Medium', 
                                        Origin = 'Email',                                        
                                        //CurrencyIsoCode = 'USD',                                            
                                        ContactId =contact.Id,
                                        AccountId=account.Id                                         
                                        ); 
        
        
        insert case1; 
        
        system.assert(case1 != null);

        ApexPages.currentPage().getParameters().put('ObjId', case1.id);
        ApexPages.currentPage().getParameters().put('FieldAPIName', 'SVMXC__Actual_Initial_Response__c');
        ApexPages.currentPage().getParameters().put('ObjAPIName', 'Case');
        DateTimeFieldUtils dtf=new DateTimeFieldUtils();
        dtf.updateObject();
    }
    static testMethod void DateTimeFieldUtilsUnitTest1() 
    {
        ApexPages.currentPage().getParameters().put('ObjId', '');
        ApexPages.currentPage().getParameters().put('FieldAPIName', '');
        ApexPages.currentPage().getParameters().put('ObjAPIName', '');
        DateTimeFieldUtils dtf=new DateTimeFieldUtils();
        dtf.updateObject();
        dtf.getRecid();
        system.assert(dtf!=null);
    }

}