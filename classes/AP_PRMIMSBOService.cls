global without sharing class AP_PRMIMSBOService{
    
    global static Boolean UpdateAccountInUIMS_AdminSync(AP_UserRegModel userRegModel,Boolean isProgressive) {
        boolean opSuccess;
        Contact primaryContact;
        String ppcFedId;
        List<Contact> conList;
        try{
            System.debug('**RegModel Passed**'+userRegModel);
            WS_PRM_UIMSCompanyMgmtService_Admin.AuthenticatedCompanyManager_UIMSV2_ImplPort  companyManagerImplPort = new WS_PRM_UIMSCompanyMgmtService_Admin.AuthenticatedCompanyManager_UIMSV2_ImplPort();
            companyManagerImplPort.endpoint_x   =     System.label.CLAPR15PRM158; //'https://ims2w-int.btsec.dev.schneider-electric.com/IMS-CompanyManager/UIMSV2/2WAuthenticated-CompanyManagement';//System.label.CLAPR15PRM158
            companyManagerImplPort.clientCertName_x = System.label.CLAPR15PRM018;
            companyManagerImplPort.timeout_x  =       Integer.valueOf(System.label.CLAPR15PRM021);
    
            /*   updateaccounts*/
            WS_PRM_UIMSCompanyMgmtServiceModel_Admin.companyV3 company = new WS_PRM_UIMSCompanyMgmtServiceModel_Admin.companyV3();
               
                company.street           = (isProgressive && String.isNotBlank(userRegModel.companyAddress1))?userRegModel.companyAddress1:(!isProgressive)?userRegModel.companyAddress1:'';
                company.localityName     = company.postalCity  = (isProgressive && String.isNotBlank(userRegModel.companyCity))?userRegModel.companyCity:(!isProgressive)?userRegModel.companyCity:'';
                company.marketServed     = (isProgressive && String.isNotBlank(userRegModel.marketServed))?userRegModel.marketServed:(!isProgressive)?userRegModel.marketServed:'';
                company.currencyCode     = (isProgressive && String.isNotBlank(userRegModel.companyCurrencyCode))?userRegModel.companyCurrencyCode:(!isProgressive)?userRegModel.companyCurrencyCode:'';
                company.employeeSize     = (isProgressive && String.isNotBlank(userRegModel.employeeSize))?userRegModel.employeeSize:(!isProgressive)?userRegModel.employeeSize:'';
                company.postalCode = (isProgressive && String.isNotBlank(userRegModel.companyZipcode))?userRegModel.companyZipcode:(!isProgressive)?userRegModel.companyZipcode:'';
                //company.globalVisibility = userRegModel.includeCompanyInfoInglobalSearch;
                
                company.telephoneNumber = (isProgressive && String.isNotBlank(userRegModel.companyPhone))?userRegModel.companyPhone:(!isProgressive)?userRegModel.companyPhone:'';
                company.webSite         = (isProgressive && String.isNotBlank(userRegModel.companyWebsite))?userRegModel.companyWebsite:(!isProgressive)?userRegModel.companyWebsite:'';
                company.taxIdentificationNumber = (isProgressive && String.isNotBlank(userRegModel.taxId))?userRegModel.taxId:(!isProgressive)?userRegModel.taxId:'';
                company.annualSales = (isProgressive && String.isNotBlank(userRegModel.annualSales))?userRegModel.annualSales:(!isProgressive)?userRegModel.annualSales:'';
                company.headQuarter = String.valueOf(userRegModel.companyHeaderQuarters);
                company.addInfoAddress = (isProgressive && String.isNotBlank(userRegModel.companyAddress2))?userRegModel.companyAddress2:(!isProgressive)?userRegModel.companyAddress2:'';
                company.preferredDistributor1 = (isProgressive && String.isNotBlank(userRegModel.prefdistributor1))?userRegModel.prefdistributor1:(!isProgressive)?userRegModel.prefdistributor1:'';
                company.preferredDistributor2 = (isProgressive && String.isNotBlank(userRegModel.prefdistributor2))?userRegModel.prefdistributor2:(!isProgressive)?userRegModel.prefdistributor2:'';
                company.preferredDistributor3 = (isProgressive && String.isNotBlank(userRegModel.prefdistributor3))?userRegModel.prefdistributor3:(!isProgressive)?userRegModel.prefdistributor3:'';
                
                company.federatedId      = userRegModel.companyFederatedId;
                if(String.isNotBlank(userRegModel.accountId)){
                        List<Contact> ppcLst = new List<Contact>([SELECT PRMUIMSID__c FROM Contact WHERE AccountId =: userRegModel.accountId AND PRMPrimaryContact__c=True]);
                        if(ppcLst != Null && ppcLst.size()>0)
                            ppcFedId= ppcLst[0].PRMUIMSID__c;
                }
                if(!isProgressive){
                    company.bfoId            = userRegModel.accountId;
                    company.businessType     = userRegModel.businessType;
                    company.customerclass    = userRegModel.areaOfFocus;
                    company.organizationName =  userRegModel.companyName;
                    if(String.isNotBlank(userRegModel.companyStateProvince)){
                        StateProvince__c state = [SELECT Id,StateProvinceCode__c,CountryCode__c FROM StateProvince__c WHERE Id=:userRegModel.companyStateProvince];
                        company.state = state.StateProvinceCode__c;
                    }
                    
                    if(String.isNotBlank(userRegModel.companyCountry)){
                        Country__c country = [SELECT Id, CountryCode__c FROM Country__c WHERE Id=:userRegModel.companyCountry];
                        company.countryCode = country.CountryCode__c;
                    }
                    
                }
            System.debug('company:'  +company);
            System.debug('ppcFedId:' + ppcFedId);
            opSuccess = companyManagerImplPort.updateCompany(System.label.CLAPR15PRM019, ppcFedId, userRegModel.companyFederatedId, company);
        }
        catch(Exception ex){
            System.debug(' ****There is an exception on updating the Account info in UIMS****' +ex.getMessage() + ex.getStackTraceString());
        }
        return opSuccess;
    }
    
    @future(callout=true)
    global static void UpdateAccountInUIMS_AdminAsync(List<Id> uimsCompanyId,Boolean isProgressive) {
        List<UIMSCompany__c> uimsList = new List<UIMSCompany__c> ([SELECT Id, UserRegistrationInformation__c FROM UIMSCompany__c WHERE Id IN :uimsCompanyId]);
        if(uimsList != null && uimsList.size() > 0 && String.isNotBlank(uimsList[0].UserRegistrationInformation__c)){
            AP_UserRegModel userRegModel = (AP_UserRegModel)JSON.deserialize(uimsList[0].UserRegistrationInformation__c,AP_UserRegModel.class);
            Boolean opSuccess = UpdateAccountInUIMS_AdminSync(userRegModel,isProgressive);
        }
    }
        
    global Static Boolean UpdateContactInUIMS_AdminSync(AP_UserRegModel userRegModel,Boolean isProgressive) {
        Boolean opSuccess = true;
        try{
            
            WS_PRM_UIMSUserMgmtService_Admin.AuthenticatedUserManager_UIMSV2_ImplPort userManagerImplPort = new WS_PRM_UIMSUserMgmtService_Admin.AuthenticatedUserManager_UIMSV2_ImplPort();
            userManagerImplPort.endpoint_x   =     System.label.CLAPR15PRM017; // 'https://ims2w-int.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager';//System.label.CLAPR15PRM017
            userManagerImplPort.clientCertName_x = System.label.CLAPR15PRM018;
            userManagerImplPort.timeout_x  =       Integer.valueOf(System.label.CLAPR15PRM021);
    
            /*   update Contacts*/
            
            WS_PRM_UIMSUserMgmtServiceModel_Admin.userV5 userV5= new WS_PRM_UIMSUserMgmtServiceModel_Admin.userV5();
                
            
            userV5.jobTitle    = (isProgressive && String.isNotBlank(userRegModel.jobTitle))?userRegModel.jobTitle:(!isProgressive)?(userRegModel.jobTitleCode == null? userRegModel.jobTitle : userRegModel.jobTitleCode):'';
            userV5.jobFunction = (isProgressive && String.isNotBlank(userRegModel.jobFunction))?userRegModel.jobFunction:(!isProgressive)?(userRegModel.jobFunctionCode == null? userRegModel.jobFunction : userRegModel.jobFunctionCode):'';
            
            userV5.street         = (isProgressive && String.isNotBlank(userRegModel.companyAddress1))?userRegModel.companyAddress1:(!isProgressive)?userRegModel.companyAddress1:'';
            userV5.addInfoAddress = (isProgressive && String.isNotBlank(userRegModel.companyAddress2))?userRegModel.companyAddress2:(!isProgressive)?userRegModel.companyAddress2:'';
            userV5.localityName   = (isProgressive && String.isNotBlank(userRegModel.companyCity))?userRegModel.companyCity:(!isProgressive)?userRegModel.companyCity:'';
            userV5.postalCode     = (isProgressive && String.isNotBlank(userRegModel.companyZipcode))?userRegModel.companyZipcode:(!isProgressive)?userRegModel.companyZipcode:'';
            userV5.federatedID = userRegModel.federationId;
            
            if(!isProgressive){
                userV5.email       = userRegModel.email;
                userV5.firstName   = userRegModel.firstName;
                userV5.lastName    = userRegModel.lastName;
                userV5.companyId   = userRegModel.uimsCompanyId;
                userV5.channel        = ((string.isNotBlank(userRegModel.companyBusinessTypeCode))&&(string.isNotBlank(userRegModel.businessType)) &&(userRegModel.companyBusinessTypeCode !=userRegModel.businessType) ) ? userRegModel.companyBusinessTypeCode : userRegModel.businessType;
                userV5.subChannel     = ((string.isNotBlank(userRegModel.companyAreaOfFocusCode))&&(string.isNotBlank(userRegModel.areaOfFocus)) &&(userRegModel.companyAreaOfFocusCode!=userRegModel.areaOfFocus ) ) ? userRegModel.companyAreaOfFocusCode: userRegModel.areaOfFocus ;
                userV5.primaryContact = userRegModel.primayContact;
                userV5.bfoId     = userRegModel.contactID;
                userV5.companyId = userRegModel.companyFederatedId;
                
                if(userRegModel.phoneType == 'Mobile') {
                    userV5.cell = userRegModel.phoneNumber;
                } else if(userRegModel.phoneType == 'Work') {
                    userV5.phone = userRegModel.phoneNumber;
                }
                
                if(String.isNotBlank(userRegModel.companyCountry)){
                    Country__c country = [SELECT Id, CountryCode__c FROM Country__c WHERE Id=:userRegModel.companyCountry];
                    userV5.countryCode = country.CountryCode__c;
                }
               
                if(String.isNotBlank(userRegModel.companyStateProvince)){
                    StateProvince__c state = [SELECT Id,StateProvinceCode__c FROM StateProvince__c WHERE Id=:userRegModel.companyStateProvince];
                    userV5.state = state.StateProvinceCode__c;
                }    
            }   
            System.debug('userV5:' + userV5);     
            opSuccess = userManagerImplPort.updateUser(System.label.CLAPR15PRM019, userRegModel.federationId, userV5);
    
        }
        catch(Exception ex) {
            System.debug(' ****There is an exception on updating the Contact info in UIMS****' +ex.getMessage() + ex.getStackTraceString());
        }
        return opSuccess;
    }
    
    @future(callout=true)
    global Static void UpdateContactInUIMS_AdminASync(List<Id> uimsCompanyId,Boolean isProgressive) {
         List<UIMSCompany__c> uimsList = new List<UIMSCompany__c> ([SELECT Id, UserRegistrationInformation__c FROM UIMSCompany__c WHERE Id IN :uimsCompanyId]);
        if(uimsList != null && uimsList.size() > 0 && String.isNotBlank(uimsList[0].UserRegistrationInformation__c)){
            AP_UserRegModel userRegModel = (AP_UserRegModel)JSON.deserialize(uimsList[0].UserRegistrationInformation__c,AP_UserRegModel.class);
            Boolean opSuccess = UpdateContactInUIMS_AdminSync(userRegModel,isProgressive);
        }
    }
    
    global static String UIMSReactivateUser(String UIMSId) {
    
        WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort uimsPort = new 
                WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();   
            
        WS_uimsv22ServiceImsSchneider.accessElement accElem = new WS_uimsv22ServiceImsSchneider.accessElement();
        accElem.type_x = System.label.CLAPR15PRM020;
        accElem.id     = System.label.CLAPR15PRM022;
        
        uimsport.endpoint_x = System.label.CLQ316IDMS129;
        uimsport.clientCertName_x = System.Label.CLAPR15PRM018;
        
        System.Debug('Access Element Type CLAPR15PRM020: ' + System.label.CLAPR15PRM020); 
        System.Debug('Access Element Id  CLAPR15PRM022: ' + System.label.CLAPR15PRM022);
        System.Debug('UIMS callerId CLAPR15PRM019: ' + System.label.CLAPR15PRM019); 
        System.Debug('UIMS Endpoint CLQ316IDMS129: ' + System.label.CLQ316IDMS129); 
        System.Debug('UIMS Client Cert CLAPR15PRM018: ' + System.label.CLAPR15PRM018);         
            
        String resp = uimsPort.reactivate(System.label.CLAPR15PRM019, UIMSId, accElem);
        
        System.Debug('UIMSReactivateUser Response  ' + resp );
        
        return resp;
    
    }
    global static AP_SearchUserResult SearchUser (String emailAddress) {
        AP_SearchUserResult result;

        try {
            uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort uimsPort = new
                        uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
            uimsPort.timeout_x = 120000;
            uimsPort.clientCertName_x = System.Label.CLAPR15PRM018;
            uimsPort.endpoint_x = System.Label.CLAPR15PRM017;
            uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType srchUsrResult = uimsPort.searchUser(System.Label.CLAPR15PRM019, emailAddress);
            if (srchUsrResult != Null && !String.isBlank(srchUsrResult.federatedId)) {
                result = new AP_SearchUserResult();
                result.UserFederatedId = srchUsrResult.federatedId;
                result.IsInternal = srchUsrResult.isInternal;
            }
        } catch (Exception ex) {
            System.debug('*** Search User Exception:' + ex.getMessage() + ex.getStackTraceString());
        }
        System.debug('*** Search Result:' + result);
        return result;
    }
     global static Boolean grantApplicationAccess(String federatedId, String targetApplicationRequested) {
        
        Boolean result;
        Boolean result2;    
        
        try{
            String callerFid   =  System.label.CLAPR15PRM019;
            //String federatedId =  '';

            uimsv2ServiceImsSchneiderComNew3.accessElement access = new uimsv2ServiceImsSchneiderComNew3.accessElement();
            access.type_x = System.label.CLAPR15PRM020;
            access.id     = System.label.CLAPR15PRM022;
            
            uimsv2ServiceImsSchneiderComNew3.accessElement access2 = new uimsv2ServiceImsSchneiderComNew3.accessElement();
            access2.type_x = System.label.CLAPR15PRM020;
            access2.id     = System.label.CLAPR15PRM304;
                        
            uimsv2ServiceImsSchneiderComNew3.accessElement access3 = new uimsv2ServiceImsSchneiderComNew3.accessElement();
            access3.type_x = System.label.CLAPR15PRM020;
            access3.id     = System.label.CLMAR16PRM010; //POMP
            
            uimsv2ImplServiceImsSchneiderComNew3.UserAccessManager_UIMSV2_ImplPort uimsPort = new uimsv2ImplServiceImsSchneiderComNew3.UserAccessManager_UIMSV2_ImplPort();
            uimsPort.endpoint_x = System.label.CLAPR15PRM273;//'https://ims2w-int.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserAccessManager?wsdl';
            uimsPort.clientCertName_x = System.label.CLAPR15PRM018;
            uimsPort.timeout_x        = Integer.valueOf(System.label.CLAPR15PRM021); 

            
            
            if(targetApplicationRequested.Equals(System.label.CLMAR16PRM011)) {
                result = uimsPort.grantAccessControlToUser(callerFid, federatedId, access);
                System.debug('**Result1**'+result);
                uimsPort.grantAccessControlToUser(callerFid, federatedId, access2);
                System.debug('**Result2**'+result);
            } else if(targetApplicationRequested.Equals(System.label.CLMAR16PRM010)) {
                result = uimsPort.grantAccessControlToUser(callerFid, federatedId, access3);
                System.debug('**Result3**'+result);   
            }
        } catch(Exception e){
            system.debug('**Error**'+e.getMessage());
        }
        return result;
    }
     global static AP_UserRegModel GetUIMSAccountContactInfo_Admin(String federatedId) {
        try {
    
            List<UIMSCompany__c> UIMSCompanyList = [SELECT id,UserRegistrationInformation__c FROM UIMSCompany__c WHERE UserFederatedId__c =: federatedId]; 
            
            AP_UserRegModel orgUserRegModel = UIMSCompanyList.size()>0 ? (UIMSCompanyList[0].UserRegistrationInformation__c != null ? (AP_UserRegModel)JSON.deserialize(UIMSCompanyList[0].UserRegistrationInformation__c, AP_UserRegModel.class) : null): null;
            AP_UserRegModel newUser = new AP_UserRegModel();
            
            WS_UIMSAdminService_Get.BackOutputServiceImplPort uimsport = new WS_UIMSAdminService_Get.BackOutputServiceImplPort();
            uimsport.endpoint_x = System.Label.CLJUL15PRM004;
            uimsport.timeout_x  =  Integer.valueOf(System.label.CLAPR15PRM021);
            uimsport.clientCertName_x = System.Label.CLAPR15PRM018;
            
            WS_UIMSAdminService_GetModel.contactBean uimsContact = uimsport.getContact(System.label.CLAPR15PRM019, federatedId);
            System.debug('**uimsContact**'+uimsContact);
            
            if(uimsContact != Null){
                newUser.uimsCompanyId = uimsContact.SACC_ID;
                newUser.federationId = uimsContact.ID;
                newUser.email = uimsContact.EMAIL;
                newUser.firstName = uimsContact.FIRST_NAME;
                newUser.firstNameTransliterated = uimsContact.FIRST_NAME_ECS;
                newUser.lastName  = uimsContact.LAST_NAME;
                newUser.lastNameTransliterated = uimsContact.LAST_NAME_ECS;
                if(String.isNotBlank(uimsContact.CELL)) {
                    newUser.phoneNumber = uimsContact.CELL;
                    newUser.phoneType = 'Mobile';
                } else if(String.isNotBlank(uimsContact.PHONE)) {
                    newUser.phoneNumber = uimsContact.PHONE;
                    newUser.phoneType = 'Work';
                }
                newUser.jobTitle = uimsContact.JOB_TITLE_CODE;
                newUser.jobFunction = uimsContact.JOB_FUNCTION_CODE;
                newUser.businessType = uimsContact.CHANNEL;
                newUser.areaOfFocus = uimsContact.SUBCHANNEL;
                newUser.isPrimaryContact = uimsContact.PRM_PRIMARY_CONTACT;
                newUser.companyAddress1 = uimsContact.STREET;
                newUser.companyAddress2 = uimsContact.ADD_INFO_ADDRESS;
                newUser.companyCity     = uimsContact.CITY;
                newUser.companyZipcode  = uimsContact.ZIP_CODE;
                newUser.userLanguage    = uimsContact.LANGUAGE_CODE;
                
                if(String.isNotBlank(uimsContact.COUNTRY_CODE)){
                    Country__c cntry = [SELECT Id FROM Country__c WHERE CountryCode__c = :uimsContact.COUNTRY_CODE];
                    newUser.companyCountry = cntry.id;
                    newUser.companyCountryIsoCode = uimsContact.COUNTRY_CODE;
                }
                system.debug('**UIMSState**'+uimsContact.STATE_PROVINCE_CODE);
                if(String.isNotBlank(uimsContact.STATE_PROVINCE_CODE)){
                    StateProvince__c state = [SELECT Id,StateProvinceCode__c,CountryCode__c FROM StateProvince__c WHERE StateProvinceCode__c = :uimsContact.STATE_PROVINCE_CODE AND CountryCode__c =:uimsContact.COUNTRY_CODE];
                    newUser.companyStateProvince = state.id;
                }
            
            }
            Pattern patternToMatch = Pattern.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
            System.debug('patternToMatch:' + patternToMatch);
            System.debug('**CompanyId**'+newUser.uimsCompanyId);
            if(String.isNotBlank(newUser.uimsCompanyId) && patternToMatch.matcher(newUser.uimsCompanyId).matches()){
                WS_UIMSAdminService_Get.BackOutputServiceImplPort uimsCompanyPort = new WS_UIMSAdminService_Get.BackOutputServiceImplPort();
                uimsCompanyPort.endpoint_x = System.Label.CLJUL15PRM004;
                uimsCompanyPort.timeout_x  =  Integer.valueOf(System.label.CLAPR15PRM021);
                uimsCompanyPort.clientCertName_x = System.Label.CLAPR15PRM018;
            
                WS_UIMSAdminService_GetModel.accountBean uimsCompany = uimsCompanyPort.getAccount(System.label.CLAPR15PRM019, newUser.uimsCompanyId);
                System.debug('**uimsCompany**'+uimsCompany);
                if(uimsCompany != Null){
                    newUser.companyFederatedId = uimsCompany.ID;
                    newUser.companyName = uimsCompany.Name== null?(uimsCompany.SCOMP_ID == null?null:uimsCompany.SCOMP_ID):uimsCompany.Name;
                    newUser.companyNameTransliterated = uimsCompany.NAME_ECS;
                    newUser.businessType = uimsCompany.BUSINESS_UNIT_CODE;
                    newUser.areaOfFocus  = uimsCompany.CUSTOMER_CLASS;
                    newUser.marketServed = uimsCompany.MARKET_SERVED;
                    newUser.employeeSize = uimsCompany.EMPLOYEE_SIZE;
                    newUser.companyAddress1 = uimsCompany.STREET;
                    newUser.companyAddress1Transliterated = uimsCompany.STREET_ECS;
                    newUser.companyAddress2 = uimsCompany.ADD_INFO_ADDRESS;
                    newUser.companyAddress2Transliterated = uimsCompany.ADD_INFO_ADDRESS_ECS;
                    newUser.companyCity     = uimsCompany.CITY;
                    newUser.companyCityTransliterated = uimsCompany.CITY_ECS;
                    newuser.companyZipcode  = uimsCompany.ZIP_CODE;
                    newUser.companyCurrencyCode = uimsCompany.CURRENCY_CODE;
                    //newUser.includeCompanyInfoInglobalSearch = uimsCompany.global_VISIBILITY;
                    newUser.userLanguage  = String.isNotBlank(newUser.userLanguage) ? newUser.userLanguage : uimsCompany.LANGUAGE_CODE; 
                    
                    if(uimsCompany.HEADQUARTER != null)
                        newUser.companyHeaderQuarters = Boolean.valueOf(uimsCompany.HEADQUARTER);
                    newUser.termsAndConditions = True;
                    
                }
            }
            else if(String.isNotBlank(newUser.uimsCompanyId) && !patternToMatch.matcher(newUser.uimsCompanyId).matches()){
                newUser.uimsCompanyId = newuser.companyFederatedId = '';
            }
            
    
            List<CountryChannels__c> cntryChannel = new List<CountryChannels__c>();
            if(String.isNotBlank(newUser.companyCountryIsoCode) && String.isNotBlank(newUser.areaOfFocus)){
                String pCntryCode = '%'+newUser.companyCountryIsoCode+'%';
                cntryChannel = [SELECT Id, ChannelCode__c, SubChannelCode__c, Active__c, 
                PRMCountry__r.CountryCode__c, PRMCountry__c, PRMCountry__r.CountryPortalEnabled__c, PRMCountry__r.TECH_Countries__c 
                FROM CountryChannels__c WHERE PRMCountry__r.TECH_Countries__c LIKE :pCntryCode AND SubChannelCode__c = :newUser.areaOfFocus AND PRMCountry__r.CountryPortalEnabled__c = TRUE AND Active__c = TRUE];
            }
            
            if(cntryChannel != null && cntryChannel.size() <= 0)
                newUser.businessType = newUser.areaOfFocus = '';
            
    
            if(orgUserRegModel != null){
                newUser.termsAndConditions = orgUserRegModel.termsAndConditions != null ? orgUserRegModel.termsAndConditions : True;
                newUser.doNotHaveCompany = orgUserRegModel.doNotHaveCompany != null ? orgUserRegModel.doNotHaveCompany : False;
                newUser.skippedCompanyStep = orgUserRegModel.skippedCompanyStep != null ? orgUserRegModel.skippedCompanyStep : False;
            }
            if (string.isNotBlank(newUser.uimsCompanyId)) {
                List<Account> lExtAcc = new List<Account>([SELECT Id, Name, PRMBusinessType__c, PRMAreaOfFocus__c,OwnerId FROM Account WHERE PRMUIMSID__c = :newUser.uimsCompanyId]);
                if (!lExtAcc.isEmpty()) {
                    newUser.accountId = lExtAcc[0].Id;
                    newUser.accOwnerId = lExtAcc[0].OwnerId;
                    system.debug('**Account In GetAccContactUIMInfo**'+newUser.accountId);
                
                    if(newuser.isPrimaryContact == False && (newUser.businessType != lExtAcc[0].PRMBusinessType__c || newuser.areaOfFocus != lExtAcc[0].PRMAreaOfFocus__c)) {
                        newuser.businessType1Code = newUser.businessType;
                        newuser.areaOfFocus1Code = newuser.areaOfFocus;
                        newUser.businessType = lExtAcc[0].PRMBusinessType__c;
                        newuser.areaOfFocus = lExtAcc[0].PRMAreaOfFocus__c;
                        
                    }
                }
                else{
                    List<LegacyAccount__c> legacyAcc = new List<LegacyAccount__c>([SELECT Id, LegacyName__c, OwnerId, Account__c, Account__r.OwnerId, LegacyKey__c FROM LegacyAccount__c WHERE LegacyKey__c = :newUser.uimsCompanyId]);
                    if(legacyAcc != null && legacyAcc.size() > 0){
                        newUser.accountId = legacyAcc[0].Account__c;
                        newUser.accOwnerId = legacyAcc[0].Account__r.OwnerId;
                    }
                }
            }
    
            System.debug('**RegModel Returned**'+newUser);
            return newUser;
    
        }
        Catch(exception ex){
            System.debug(' ****There is an exception on getting the Account & Contact info from UIMS****' +ex.getMessage() + ex.getStackTraceString());
            System.debug(' *** Exception thrown ***'+ex);
            throw ex;
            return null;
        }
    }
     global static List<AP_ApplicationType> getApplications_Admin (String federatedId) {
        List<AP_ApplicationType> lAppType = new List<AP_ApplicationType> ();
        WS_UIMSAdminService_Get.BackOutputServiceImplPort uimsport = new WS_UIMSAdminService_Get.BackOutputServiceImplPort();
        uimsport.endpoint_x = System.Label.CLJUL15PRM004;
        uimsport.timeout_x  =  Integer.valueOf(System.label.CLAPR15PRM021);
        uimsport.clientCertName_x = System.Label.CLAPR15PRM018;
        
        WS_UIMSAdminService_GetModel.accessTree uimsAccessTree = uimsPort.getAccessControl(System.label.CLAPR15PRM019, federatedId);
        System.debug('uimsAccessTree:' + uimsAccessTree);
        
        WS_UIMSAdminService_GetModel.accessList_element uimsAccessList; //= new WS_UIMSAdminService_GetModel.accessList_element();
        WS_UIMSAdminService_GetModel.accessElement uimsAccess; // = new WS_UIMSAdminService_GetModel.accessElement();
        
        if(uimsAccessTree != null) {
            uimsAccessList = uimsAccessTree.accessList;
        }
        system.debug('uimsAccessList:' +uimsAccessList);
        
        if(uimsAccessList != null && uimsAccessList.access != null) {
            if(uimsAccessList.access.size() > 0) {
                for(Integer i =0;i<uimsAccessList.access.size();i++) {
                    uimsAccess = uimsAccessList.access[i];
                    system.debug('uimsAccess:' +uimsAccess);
                    CS_PRM_ApplicationDisplayName__c appDisplayName = CS_PRM_ApplicationDisplayName__c.getInstance(uimsAccess.id);
                    if(appDisplayName != null){
                        AP_ApplicationType app = new AP_ApplicationType();
                        app.ApplicationId = uimsAccess.id;
                        app.ApplicationName = uimsAccess.type_x;
                        app.DisplayName = appDisplayName.PRM_DisplayName__c;
                        lAppType.add(app);
                        system.debug('app:' + app);
                    }
                }
            }
        
        }
        system.debug('uimsAccess:' + uimsAccess);
        
        system.debug('AppType:' + lAppType);
        return lAppType;
    }
    @RemoteAction
    global Static Boolean UpdatePrimaryContact_Admin(Contact oldContact, Contact newContact) {
        boolean opSuccess = false;
        try {
            WS_PRM_UIMSUserMgmtService_Admin.AuthenticatedUserManager_UIMSV2_ImplPort userManagerImplPort = new WS_PRM_UIMSUserMgmtService_Admin.AuthenticatedUserManager_UIMSV2_ImplPort();
            userManagerImplPort.endpoint_x   =     System.label.CLAPR15PRM017; 
            userManagerImplPort.clientCertName_x = System.label.CLAPR15PRM018;
            userManagerImplPort.timeout_x  =       Integer.valueOf(System.label.CLAPR15PRM021);
            
            String callerFid = System.label.CLAPR15PRM019;
    
            WS_PRM_UIMSUserMgmtServiceModel_Admin.userV5 userV1= new WS_PRM_UIMSUserMgmtServiceModel_Admin.userV5();
            WS_PRM_UIMSUserMgmtServiceModel_Admin.userV5 userV2= new WS_PRM_UIMSUserMgmtServiceModel_Admin.userV5();
            
            userv1.federatedID = oldContact.PRMUIMSID__c;
            userv1.primaryContact = false;
            
            userV2.federatedID = newContact.PRMUIMSID__c;
            userV2.primaryContact = true;
            
            System.debug('callerFid '+callerFid+'oldContact.PRMUIMSID__c '+oldContact.PRMUIMSID__c+'userV1 '+userV1+'userV2 '+userV2);
            
            userManagerImplPort.updateUser(callerFid,oldContact.PRMUIMSID__c,userV1);
            userManagerImplPort.updateUser(callerFid,newContact.PRMUIMSID__c,userV2);
        
            opSuccess = true;
        }
        catch(Exception ex) {
            System.debug(' ****There is an exception on updating the New PrimaryContact info in UIMS****' +ex.getMessage() + ex.getStackTraceString());
        }
        return opSuccess;
    }
    global static AP_UserRegModel GetContactInfo_Admin(String federatedId) {
        try {
        
            AP_UserRegModel newUser = new AP_UserRegModel();
            
            WS_UIMSAdminService_Get.BackOutputServiceImplPort uimsport = new WS_UIMSAdminService_Get.BackOutputServiceImplPort();
            uimsport.endpoint_x = System.Label.CLJUL15PRM004;
            uimsport.timeout_x  =  Integer.valueOf(System.label.CLAPR15PRM021);
            uimsport.clientCertName_x = System.Label.CLAPR15PRM018;
            
            WS_UIMSAdminService_GetModel.contactBean uimsContact = uimsport.getContact(System.label.CLAPR15PRM019, federatedId);
            System.debug('**uimsContact**'+uimsContact);
            
            if(uimsContact != Null){
                newUser.uimsCompanyId = uimsContact.SACC_ID;
                newUser.federationId = uimsContact.ID;
                newUser.email = uimsContact.EMAIL;
                newUser.firstName = uimsContact.FIRST_NAME;
                newUser.lastName  = uimsContact.LAST_NAME;
                if(String.isNotBlank(uimsContact.CELL)) {
                    newUser.phoneNumber = uimsContact.CELL;
                    newUser.phoneType = 'Mobile';
                } else if(String.isNotBlank(uimsContact.PHONE)) {
                    newUser.phoneNumber = uimsContact.PHONE;
                    newUser.phoneType = 'Work';
                }
                newUser.jobTitle = uimsContact.JOB_TITLE_CODE;
                newUser.jobFunction = uimsContact.JOB_FUNCTION_CODE;
                newUser.businessType = uimsContact.CHANNEL;
                newUser.areaOfFocus = uimsContact.SUBCHANNEL;
                newUser.isPrimaryContact = uimsContact.PRM_PRIMARY_CONTACT;
                newUser.companyAddress1 = uimsContact.STREET;
                newUser.companyAddress2 = uimsContact.ADD_INFO_ADDRESS;
                newUser.companyCity     = uimsContact.CITY;
                newUser.companyZipcode  = uimsContact.ZIP_CODE;
                newUser.userLanguage    = uimsContact.LANGUAGE_CODE;
                
                if(String.isNotBlank(uimsContact.COUNTRY_CODE)){
                    Country__c cntry = [SELECT Id FROM Country__c WHERE CountryCode__c = :uimsContact.COUNTRY_CODE];
                    newUser.companyCountry = cntry.id;
                }
                system.debug('**UIMSState**'+uimsContact.STATE_PROVINCE_CODE);
                if(String.isNotBlank(uimsContact.STATE_PROVINCE_CODE)){
                    StateProvince__c state = [SELECT Id,StateProvinceCode__c,CountryCode__c FROM StateProvince__c WHERE StateProvinceCode__c = :uimsContact.STATE_PROVINCE_CODE AND CountryCode__c =:uimsContact.COUNTRY_CODE];
                    newUser.companyStateProvince = state.id;
                }
            
            }
            return newUser;
    
        }
        Catch(exception ex){
            System.debug(' ****There is an exception on getting the Account & Contact info from UIMS****' +ex.getMessage() + ex.getStackTraceString());
            return null;
        }
        
    }
    global static void UpdateUIMSContactRegistrationStatus(string actionType, string userFederatedId, string ppcSamlAssertion) {
    
        WS_PRM_UIMSUserAdmService_User.UserAdministration_UIMSV2_ImplPort uimsPort = new WS_PRM_UIMSUserAdmService_User.UserAdministration_UIMSV2_ImplPort();
        uimsPort.endpoint_x       = System.Label.CLMAR16PRM052;
         
        uimsPort.clientCertName_x = System.label.CLAPR15PRM018;
        uimsPort.timeout_x        = Integer.valueOf(System.label.CLAPR15PRM021);
    
    
        List<String> federatedIds = new List<String>{userFederatedId};
        
        System.debug('** actionType**userfederatedId**samlAssertion**'+actionType+' '+userFederatedId+' '+ppcSamlAssertion);
        
        String callerFid = System.label.CLAPR15PRM019;
        WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReport uimsResult;
    
        if(String.isNotBlank(actionType) && actionType == 'Validated') {
            uimsResult = uimsPort.approveUsers(callerFid, ppcSamlAssertion, federatedIds);
            System.debug('**approveUsers ** '+uimsResult);
        } else if(String.isNotBlank(actionType) && actionType == 'Declined') {
            uimsResult = uimsPort.rejectUsers(callerFid, ppcSamlAssertion, federatedIds);
            System.debug('**rejectUsers Result ** '+uimsResult);    
        }


    }
}