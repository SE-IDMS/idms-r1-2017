Public class AP_IDMSAppIdAppName {

    Public Static String getAppNameFromAppId (String appId){
        //Loop on all the records of IDMSApplicationMapping and check the one having the same appId
        //then returns the related App Name
        List<IDMSApplicationMapping__c> apps = IDMSApplicationMapping__c.getall().values();
        for(IDMSApplicationMapping__c app : apps){
            if(app.AppId__c == appId){
                system.debug(appId + ' is related to :'+ app.Name);
                return app.Name;
            }
        }
        return null;
    }

}