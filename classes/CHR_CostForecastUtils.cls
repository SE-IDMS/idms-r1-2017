/*
    Author          : Priyanka Shetty (Schneider Electric) 
    Date Created    : 12/04/2013
    Description     : Class utilised by triggers ProgramCostForecastTriggersUtilsClass
                        
*/
public with sharing class CHR_CostForecastUtils {
       
        public static void deactivateActiveCRCostForecast(List<ChangeRequestCostsForecast__c> budgetListToProcessCONDITION1,boolean isUpdate)
        {
        
        Map<Id,ChangeRequestCostsForecast__c> projectNewBudgetMap = new Map<Id,ChangeRequestCostsForecast__c>();
        Map<Id,ChangeRequestCostsForecast__c> projectCurrBudgetMap = new Map<Id,ChangeRequestCostsForecast__c>();
        List<ChangeRequestCostsForecast__c> updateBudgetList = new List<ChangeRequestCostsForecast__c>();
        
        /* Get the list (lstA) of current active budgets for the list of change req IDs (from the budgetListToProcessCONDITION1 list) */
        for(ChangeRequestCostsForecast__c oBud:budgetListToProcessCONDITION1)   
        {
            projectNewBudgetMap.put(oBud.ChangeRequest__c,oBud);
            System.Debug('projectNewBudgetMap PR: ' + oBud.ChangeRequest__c + ',New Budget: ' + oBud.Id);
        }         
      
        /* Loop thru the list and Make a map of all the Project IDs (as key) and current active budget */
        for (ChangeRequestCostsForecast__c oBud:[select Id,Active__c,ChangeRequest__c,TechBCD__c from ChangeRequestCostsForecast__c where ChangeRequest__c in :projectNewBudgetMap.keyset() and Active__c = true])
        {
            projectCurrBudgetMap.put(oBud.ChangeRequest__c, oBud);
            System.Debug('projectCurrBudgetMap PR: ' + oBud.ChangeRequest__c + ',Curr Budget: ' + oBud.Id);
        }


        for (ID projID:projectCurrBudgetMap.keyset())
        {
            ChangeRequestCostsForecast__c currBud = projectCurrBudgetMap.get(projID);
            ChangeRequestCostsForecast__c newBud = projectNewBudgetMap.get(projID);

            currBud.Active__c = false;

            if(!isUpdate)
            {
                Date dtToday = Date.Today();
                if(dtToday.Year() > currBud.TechBCD__c.Year() || Test.isRunningTest())
                {
                    currBud.LastActiveofBudgetYear__c = true;
                }
            }                
            else if(newBud.TechBCD__c.Year() > currBud.TechBCD__c.Year() || Test.isRunningTest())
            {
                System.Debug('newBud.YearOrigin:' + newBud.TechBCD__c);
                currBud.LastActiveofBudgetYear__c = true;
            }
            updateBudgetList.add(currBud);
        }
        
        try
        {        
            if(updateBudgetList.size() > 0)
                update(updateBudgetList);
                
            //if(Test.isRunningTest())
                //throw new TestException('Test Exception');
        }
        catch(Exception exp)
        {
            System.Debug('Update Failed');
        }
        
        
        
        
    }
    public static void updateProgramFieldsonDelete(List<ChangeRequestCostsForecast__c> budgetListToProcessCONDITION1)
    {
        list<Change_Request__c> lstProgs = new list<Change_Request__c>{};
        Integer intCurrentYear = Integer.Valueof(System.Label.DMTYearValue);
        System.debug('--DeleteProjectReq'+budgetListToProcessCONDITION1);
        
        Try{
            for(ChangeRequestCostsForecast__c bud:budgetListToProcessCONDITION1){
                Change_Request__c sobjPrograms = new Change_Request__c(id=bud.ChangeRequest__c);
                for(DMTBudgetValueToProjectRequest__c csInstance:DMTBudgetValueToProjectRequest__c.getAll().values()){
                    for(Integer BYear = intCurrentYear; BYear<intCurrentYear + 4;BYear++){
                        System.debug('--DeleteProject'+sobjPrograms);
                            sobjPrograms.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BYear).right(2)+'__c',0);
                    }
                }
                lstProgs.add(sobjPrograms);
            }
            update lstProgs;
        }
        Catch(Exception e){
            System.debug('Error:'+e);
        }
    }
 
    //Populate the above Fields with the Respective values depending on Year they are created.
  
    public static void updateCRFieldsonInsertUpdate(List<ChangeRequestCostsForecast__c> budgetListToProcessCONDITION1){
    
        Map<String,DMTBudgetValueToChangeRequest__c> csMaps = DMTBudgetValueToChangeRequest__c.getAll();
        list<Change_Request__c> lstProjs = new list<Change_Request__c>{};
        Try{
            
            for(ChangeRequestCostsForecast__c bud:budgetListToProcessCONDITION1){
            
                Change_Request__c sobjProjects = new Change_Request__c(id=bud.ChangeRequest__c);
                if(bud.TotalITCostsCashout__c!=null||bud.TotalITCostsInternal__c!=null)
                    sobjProjects.TECHTotalPlannedITCosts__c = bud.TotalITCostsCashout__c + bud.TotalITCostsInternal__c;
                Integer intFieldYear = 0;
                Integer intCurrentYear = Integer.Valueof(System.Label.DMTYearValue);            
                for(Integer BYear = intCurrentYear - 1; BYear<intCurrentYear+10; BYear++){
                System.debug('inside for loop');
                    if(bud.Year_Origin__c == BYear){
                        System.debug('--Budget Year '+ BYear);
                        for(DMTBudgetValueToChangeRequest__c csInstance:csMaps.values()){
                            for(Integer BudYear = Integer.Valueof(String.Valueof(BYear).right(2)); BudYear<Integer.Valueof(String.Valueof(BYear).right(2))+4;BudYear++){
                            System.debug('--Program Name of Field '+ BudYear);
                            System.debug('--Cost Forecast Name of Field '+ intFieldYear);
                            if(BYear == intCurrentYear -1 && intFieldYear == 0 ){
                                BudYear = BudYear +1;
                                intFieldYear++;
                            }
                            if(intFieldYear<4 ){
                                    sobjProjects.put(csInstance.ChangeRequestAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY'+String.Valueof(intFieldYear)+csInstance.CostItemFieldAPIName__c));
                            }
                            intFieldYear++;
                            
                            }
                            intFieldYear =0;
                        }
                    } 
                }
                
                lstProjs.add(sobjProjects);
            }
            System.debug('Program records'+lstProjs);   
            update lstProjs;

        }
        Catch(Exception E){
            System.debug('Error:'+e);
        }    
    }
    
        //calculating P&L values
  
    public static void UpdatePLvalue(List<ChangeRequestCostsForecast__c> budgetListToProcessCONDITION1){

        Try{
            
            for(ChangeRequestCostsForecast__c bud:budgetListToProcessCONDITION1){
            for(DMTCRCostForecastCostItemsFields__c csInstance : DMTCRCostForecastCostItemsFields__c.getAll().values() ){
                        if(bud.get(csInstance.CostItemFieldAPIName__c) == null && csInstance.CostItemFieldAPIName__c !='FY0PLImpact__c' && csInstance.CostItemFieldAPIName__c !='FY1PLImpact__c' && csInstance.CostItemFieldAPIName__c !='FY2PLImpact__c' && csInstance.CostItemFieldAPIName__c !='FY3PLImpact__c')
                            bud.put(csInstance.CostItemFieldAPIName__c,0);
                    }
                   Decimal pLAmountCY =bud.FY0CashoutOPEX__c +
                                    bud.FY0SmallEnhancements__c +
                                    bud.FY0RunningITCostsImpact__c +
                                    bud.FY0Regions__c +
                                    bud.FY0AppServicesCustExp__c +
                                    bud.FY0TechnologyServices__c +
                                    bud.FY0GDCosts__c +
                                    bud.FY0NonIPOITCostsInternal__c ;                
                                  
                Decimal pLAmountCY1 = bud.FY1CashoutOPEX__c +
                                    bud.FY1SmallEnhancements__c +
                                    bud.FY1RunningITCostsImpact__c +
                                    bud.FY1Regions__c +
                                    bud.FY1AppServicesCustExp__c +
                                    bud.FY1TechnologyServices__c +
                                    bud.FY1GDCosts__c +
                                    bud.FY1NonIPOITCostsInternal__c +
                                    bud.FY0CashoutCAPEX__c/3 ;
                
                Decimal pLAmountCY2 = bud.FY2CashoutOPEX__c +
                                    bud.FY2SmallEnhancements__c +
                                    bud.FY2RunningITCostsImpact__c +
                                    bud.FY2Regions__c +
                                    bud.FY2AppServicesCustExp__c +
                                    bud.FY2TechnologyServices__c +
                                    bud.FY2GDCosts__c +
                                    bud.FY2NonIPOITCostsInternal__c +
                                    bud.FY0CashoutCAPEX__c/3 +
                                    bud.FY1CashoutCAPEX__c/3 ;
                                                     
                Decimal pLAmountCY3 = bud.FY3CashoutOPEX__c +
                                    bud.FY3SmallEnhancements__c +
                                    bud.FY3RunningITCostsImpact__c +
                                    bud.FY3Regions__c +
                                    bud.FY3AppServicesCustExp__c +
                                    bud.FY3TechnologyServices__c +
                                    bud.FY3GDCosts__c +
                                    bud.FY3NonIPOITCostsInternal__c +
                                    bud.FY0CashoutCAPEX__c/3 +
                                    bud.FY1CashoutCAPEX__c/3 +
                                    bud.FY2CashoutCAPEX__c/3 ;   

  
                    if(bud.FY0PLImpact__c ==null)
                        bud.FY0PLImpact__c = pLAmountCY;        

                    if(bud.FY1PLImpact__c ==null)
                        bud.FY1PLImpact__c = pLAmountCY1;

                    if(bud.FY2PLImpact__c ==null )
                        bud.FY2PLImpact__c = pLAmountCY2;
 
                    if(bud.FY3PLImpact__c ==null )
                        bud.FY3PLImpact__c = pLAmountCY3;     

            }           
                    if(Test.isRunningTest())
                        throw new TestException('Test Exception');
        }
        Catch(Exception E){
            System.debug('Error:'+e);
        }    
    }
    public class TestException extends Exception{}
}