@isTest
global class WS_MockClassPRM_Test implements WebServiceMock{

    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
           if(request instanceOf uimsv2ServiceImsSchneiderComAUM.searchUser){
                system.debug('**SearchUser**');
                uimsv2ServiceImsSchneiderComAUM.searchUserResponse respSearchUser = new uimsv2ServiceImsSchneiderComAUM.searchUserResponse(); 
                uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType srchUsrResult = new uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType();
                srchUsrResult.federatedId = 'Verify Email FederatedId';
                srchUsrResult.isInternal  = false;
                respSearchUser.return_x = srchUsrResult;
                response.put('response_x',respSearchUser);
                system.debug('**Response-->SearchUser**'+response);
           
           }
           
           system.debug('**Stub1**'+stub);
           system.debug('**request1**'+request);
           // CreateIdentity
           if(request instanceOf uimsv2ServiceImsSchneiderComAUM.createIdentity){
               system.debug('**CreateIdentity**');
               uimsv2ServiceImsSchneiderComAUM.createIdentityResponse resp = new uimsv2ServiceImsSchneiderComAUM.createIdentityResponse();
               //Map<String, uimsv2ServiceImsSchneiderComAUM.createIdentityResponse> response_map_x = new Map<String, uimsv2ServiceImsSchneiderComAUM.createIdentityResponse>(); 
               uimsv2ServiceImsSchneiderComAUM.createdIdentityReport respCreateIdentity = new uimsv2ServiceImsSchneiderComAUM.createdIdentityReport(); 
               
               respCreateIdentity.federatedID    = '188b80b1-c622-4f20-8b1d-3ae86af11c4a';
               respCreateIdentity.hasBeenCreated = true;
               respCreateIdentity.errorMessage   = 'No Error';
               resp.return_x = respCreateIdentity;
               response.put('response_x', resp);
               system.debug('**Response-->CreateIdentity**'+response);
           }
           
           system.debug('**Stub2**'+stub);
           system.debug('**request2**'+request);
           //CreateCompany
           if(request instanceOf uimsv2ServiceImsSchneiderCom.createCompany){
               system.debug('**CreateCompany**');
               uimsv2ServiceImsSchneiderCom.createCompanyResponse respCreateCompany = new uimsv2ServiceImsSchneiderCom.createCompanyResponse();
               
               respCreateCompany.companyId = '6372c85d-123b-4ff4-b134-088c4077111d';
               response.put('response_x', respCreateCompany);
               system.debug('**Response-->CreateCompany**'+response);
           
      }
      
      if(request instanceOf uimsv2ServiceImsSchneiderComUM.activateIdentity) {
          System.debug('**ActivateIdentity**');
          uimsv2ServiceImsSchneiderComUM.activateIdentityResponse respActivateIdentity = new uimsv2ServiceImsSchneiderComUM.activateIdentityResponse();
          respActivateIdentity.return_x = True;
          response.put('response_x', respActivateIdentity);
          system.debug('**Response-->ActivateIdentity**'+response);
      
      }
      
      if(request instanceOf uimsv2ServiceImsSchneiderComNew3.grantAccessControlToUser) {
          system.debug('**GrantAccessControlToUser**');
          uimsv2ServiceImsSchneiderComNew3.grantAccessControlToUserResponse respGrantAccess = new uimsv2ServiceImsSchneiderComNew3.grantAccessControlToUserResponse();
          respGrantAccess.return_x = True;    
          response.put('response_x',respGrantAccess);
          system.debug('**Response-->grantAccessControlToUser**'+response);
      
      }
      
      if(request instanceOf uimsv2ServiceImsSchneiderComNew2.getUser){
          system.debug('**GetUser**');
          uimsv2ServiceImsSchneiderComNew2.getUserResponse respGetUser = new uimsv2ServiceImsSchneiderComNew2.getUserResponse();
          uimsv2ServiceImsSchneiderComNew2.userV5 uimsUser = new uimsv2ServiceImsSchneiderComNew2.userV5();
          uimsUser.federatedID = '188b80b1-c622-4f20-8b1d-3ae86af11c4a';
          uimsUser.email       = 'test1@test.com';
          uimsUser.firstName = 'Subhashish_Test1';
          uimsUser.lastName  = 'Sahu_Test1';
          uimsUser.phone     = '9999999999';     
            //uimsUser.cell
          uimsUser.jobTitle    = 'ZC';
          uimsUser.jobFunction = 'Z019';
          uimsUser.channel     = 'FI'; 
          uimsUser.subChannel  = 'FI3'; 
          uimsUser.primaryContact = True; //Not Always True
          uimsUser.companyId      = '6372c85d-123b-4ff4-b134-088c4077111d';
        //uimsUser.street         = '';
        //uimsUser.addInfoAddress = ;
          uimsuser.localityName   = 'Bangalore'; 
            //uimsuser.postalCode;
          uimsUser.countryCode = 'IN';
          uimsUser.state= '10';
          
          respGetUser.return_x = uimsUser;
          response.put('response_x',respGetUser);
          system.debug('**Response-->getUser**'+response);
          
          
      }
      
      /*if(request instanceOf  uimsv2ServiceImsSchneiderComNew.getCompany){
          system.debug('**GetComapny**');
          uimsv2ServiceImsSchneiderComNew.getCompanyResponse respGetCompany = new uimsv2ServiceImsSchneiderComNew.getCompanyResponse();
          uimsv2ServiceImsSchneiderComNew.companyV3 companyInfo = new uimsv2ServiceImsSchneiderComNew.companyV3();
          
          companyInfo.organizationName = 'SchneiderTest';
          companyInfo.businessType     = 'FI';
          companyInfo.customerClass  = 'FI3';
          //companyInfo.marketServed;
          //companyInfo.employeeSize;
          companyInfo.street            = 'Elnath street';
          companyInfo.localityName      = 'Bangalore';
          companyInfo.postalCode        = '560103';
          companyInfo.federatedId       ='6372c85d-123b-4ff4-b134-088c4077111d';
          companyInfo.publicVisibility  = True;
          companyInfo.headQuarter       = 'True';
          respGetCompany.company  =  companyInfo;
          response.put('response_x',respGetCompany);
          system.debug('**Response-->getCompany**'+response);
      
      }*/
      
      if(request instanceOf flowServiceImsSchneiderCom.updateAccounts){
        system.debug('**UpdateAccounts**');
        flowServiceImsSchneiderCom.updateAccountsResponse respUpdateAccounts = new flowServiceImsSchneiderCom.updateAccountsResponse();
        flowServiceImsSchneiderCom.updateReturnBean[] accountBeanLst = new flowServiceImsSchneiderCom.updateReturnBean[]{};
        flowServiceImsSchneiderCom.updateReturnBean accountBean = new flowServiceImsSchneiderCom.updateReturnBean();
        
        accountBean.BFO_ID      = '';
        accountBean.DML_ACTION  = '';
        accountBean.ID          = '';
        accountBean.PS          = '';
        accountBean.REF_ID      = ''; 
        accountBean.REF_PS      = '';
        accountBean.SDH_VERSION = '';
        accountBean.message     = '';
        accountBean.returnCode  = 1;
        
        accountBeanLst.add(accountBean);
        
        respUpdateAccounts.return_x = accountBeanLst;
        response.put('response_x',respUpdateAccounts);
        system.debug('**Response-->updateAccounts**'+response);
      
      
      }
      
      if(request instanceOf WS_UIMSAdminService_UpdateModel.updateAccounts){
        system.debug('**UpdateAccounts**');
        WS_UIMSAdminService_UpdateModel.updateAccountsResponse respUpdateAccounts = new WS_UIMSAdminService_UpdateModel.updateAccountsResponse();
        WS_UIMSAdminService_UpdateModel.updateReturnBean[] accountBeanLst = new WS_UIMSAdminService_UpdateModel.updateReturnBean[]{};
        WS_UIMSAdminService_UpdateModel.updateReturnBean accountBean = new WS_UIMSAdminService_UpdateModel.updateReturnBean();
        
        accountBean.BFO_ID      = '';
        accountBean.DML_ACTION  = '';
        accountBean.ID          = '';
        accountBean.PS          = '';
        accountBean.REF_ID      = ''; 
        accountBean.REF_PS      = '';
        accountBean.SDH_VERSION = '';
        accountBean.message     = '';
        accountBean.returnCode  = 1;
        
        accountBeanLst.add(accountBean);
        
        respUpdateAccounts.return_x = accountBeanLst;
        response.put('response_x',respUpdateAccounts);
        system.debug('**Response-->updateAccounts**'+response);
      
      
      }
      
      if(request instanceOf flowServiceImsSchneiderCom.updateContacts ){
        system.debug('**UpdateContacts**');
        flowServiceImsSchneiderCom.updateContactsResponse  respUpdateContacts = new flowServiceImsSchneiderCom.updateContactsResponse();
        flowServiceImsSchneiderCom.updateReturnBean[] contactBeanLst = new flowServiceImsSchneiderCom.updateReturnBean[]{};
        flowServiceImsSchneiderCom.updateReturnBean contactBean = new flowServiceImsSchneiderCom.updateReturnBean();
        
        contactBean.BFO_ID      = '';
        contactBean.DML_ACTION  = '';
        contactBean.ID          = '';
        contactBean.PS          = '';
        contactBean.REF_ID      = ''; 
        contactBean.REF_PS      = '';
        contactBean.SDH_VERSION = '';
        contactBean.message     = '';
        contactBean.returnCode  = 2;
        
        contactBeanLst.add(contactBean);
        
        respUpdateContacts.return_x = contactBeanLst;
        response.put('response_x',respUpdateContacts);
        system.debug('**Response-->updateContacts**'+response);
      }
      
      if(request instanceOf WS_UIMSAdminService_UpdateModel.updateContacts ){
        system.debug('**UpdateContacts**');
        WS_UIMSAdminService_UpdateModel.updateContactsResponse  respUpdateContacts = new WS_UIMSAdminService_UpdateModel.updateContactsResponse();
        WS_UIMSAdminService_UpdateModel.updateReturnBean[] contactBeanLst = new WS_UIMSAdminService_UpdateModel.updateReturnBean[]{};
        WS_UIMSAdminService_UpdateModel.updateReturnBean contactBean = new WS_UIMSAdminService_UpdateModel.updateReturnBean();
        
        contactBean.BFO_ID      = '';
        contactBean.DML_ACTION  = '';
        contactBean.ID          = '';
        contactBean.PS          = '';
        contactBean.REF_ID      = ''; 
        contactBean.REF_PS      = '';
        contactBean.SDH_VERSION = '';
        contactBean.message     = '';
        contactBean.returnCode  = 2;
        
        contactBeanLst.add(contactBean);
        
        respUpdateContacts.return_x = contactBeanLst;
        response.put('response_x',respUpdateContacts);
        system.debug('**Response-->updateContacts**'+response);
      }
      
      if(request instanceOf WS_UIMSAdminService_GetModel.getContact ){
        system.debug('**getContact**');
        WS_UIMSAdminService_GetModel.getContactResponse respGetContact = new WS_UIMSAdminService_GetModel.getContactResponse();
        
        
        WS_UIMSAdminService_GetModel.contactBean contactBean = new WS_UIMSAdminService_GetModel.contactBean(); 
        
        contactBean.BFO_ID      = '123456789';
        contactBean.ID          = '9876543210';
        contactBean.SACC_ID     = '6372c85d-83f8-4421-b132-d6bc81d59b69'; //'1111111111'
        contactBean.EMAIL       = '';
        contactBean.FIRST_NAME  = '';
        contactBean.LAST_NAME   = '';
        contactBean.CELL        = '9999999999';
        contactBean.PHONE       = '1212121212';
        contactBean.COUNTRY_CODE = 'IN';
        contactBean.STATE_PROVINCE_CODE = '10';
        contactBean.CHANNEL = 'FI';
        contactBean.SUBCHANNEL   = 'FI1';
        contactBean.PRM_PRIMARY_CONTACT = false;
        
        
        respGetContact.return_x = contactBean;
        response.put('response_x',respGetContact);
        system.debug('**Response-->getContacts**'+response);
      }
      
      if(request instanceOf WS_UIMSAdminService_GetModel.getAccount ){
        system.debug('**getAccount**');
        WS_UIMSAdminService_GetModel.getAccountResponse respGetAccount = new WS_UIMSAdminService_GetModel.getAccountResponse();
        
        
        WS_UIMSAdminService_GetModel.accountBean accountBean = new WS_UIMSAdminService_GetModel.accountBean(); 
            
        
        accountBean.ID = '123456789098765';
        accountBean.NAME = 'getAccount';
        accountBean.BUSINESS_UNIT_CODE = 'FI'; 
        accountBean.CUSTOMER_CLASS      = 'FI1';
        accountBean.MARKET_SERVED = '';
        accountBean.EMPLOYEE_SIZE     = '';
        accountBean.STREET = '';
        accountBean.CITY   = '';
        accountBean.ZIP_CODE = '';
        accountBean.CURRENCY_CODE = '';
        accountBean.PUBLIC_VISIBILITY = True;
        accountBean.LANGUAGE_CODE = 'Fr';
        accountBean.HEADQUARTER = True;
        
        
        respGetAccount.return_x = accountBean;
        response.put('response_x',respGetAccount);
        system.debug('**Response-->getAccounts**'+response);
      }
      
      if(request instanceOf uimsv2ServiceImsSchneiderComUM.setPassword) {
      
          uimsv2ServiceImsSchneiderComUM.setPasswordResponse respSetPassword = new uimsv2ServiceImsSchneiderComUM.setPasswordResponse();    
          respSetPassword.return_x = True;
          response.put('response_x',respSetPassword);
          system.debug('**Response-->updateAccounts**'+response);
      }
      
      if(request instanceOf uimsv2ServiceImsSchneiderCom.searchPublicCompany) {
        uimsv2ServiceImsSchneiderCom.searchPublicCompanyResponse respSearchPublicCompany = new uimsv2ServiceImsSchneiderCom.searchPublicCompanyResponse(); 
        uimsv2ServiceImsSchneiderCom.companyV3[] companyInfoLst = new uimsv2ServiceImsSchneiderCom.companyV3[]{}; 
        uimsv2ServiceImsSchneiderCom.companyV3 companyInfo = new uimsv2ServiceImsSchneiderCom.companyV3();
        
        companyInfo.organizationName = 'SchneiderTest';
        companyInfo.telephoneNumber  = '987654321';
        companyInfo.webSite          = 'Https://schneider-electric.com';
        companyInfo.headQuarter      = 'True';
        companyInfo.street           = 'Elnath street';
        companyInfo.localityName     = 'Bangalore';
        companyInfo.postalCode       = '560103';
        companyInfo.countryCode      = 'IN';
        companyInfo.state            = '10';
        
        companyInfoLst.add(companyInfo);
        
        respSearchPublicCompany.companies = companyInfoLst;
        response.put('response_x',respSearchPublicCompany);
        system.debug('**Response-->searchPublicCompany**'+response);
      }
      
      if(request instanceOf uimsv2ServiceImsSchneiderComUM.updatePassword) {
        uimsv2ServiceImsSchneiderComUM.updatePasswordResponse respUpdatePassword = new uimsv2ServiceImsSchneiderComUM.updatePasswordResponse();
        respUpdatePassword.return_x = True;
        response.put('response_x',respUpdatePassword);  
        system.debug('**Response-->updatePassword**'+response);
      }

      if (request instanceOf uimsv2ServiceImsSchneiderComAUM.resetPassword) {
        uimsv2ServiceImsSchneiderComAUM.resetPasswordResponse resetPwdResponse = new uimsv2ServiceImsSchneiderComAUM.resetPasswordResponse();
        resetPwdResponse.return_x = 'True';
        response.put('response_x',resetPwdResponse);  
        system.debug('**Response-->updatePassword**' + response);
      }
      
      if(request instanceOf  WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompany){
          system.debug('**GetComapny**');
          WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse respGetCompany = new WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse();
          WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 companyInfo = new WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3();
          
          companyInfo.organizationName = 'SchneiderTest';
          companyInfo.businessType     = 'FI';
          companyInfo.customerClass  = 'FI3';
          companyInfo.street            = 'Elnath street';
          companyInfo.localityName      = 'Bangalore';
          companyInfo.postalCode        = '560103';
          companyInfo.federatedId       ='6372c85d-123b-4ff4-b134-088c4077111d';
          companyInfo.publicVisibility  = True;
          companyInfo.headQuarter       = 'True';
          respGetCompany.company  =  companyInfo;
          response.put('response_x',respGetCompany);
          system.debug('**Response-->getCompany**'+response);
      
      }
      
      if(request instanceOf  WS_PRM_UIMSUserMgmtServiceModel_Admin.updateUser){
          system.debug('**GetComapny**');
          WS_PRM_UIMSUserMgmtServiceModel_Admin.updateUserResponse respUpdateUser = new WS_PRM_UIMSUserMgmtServiceModel_Admin.updateUserResponse();
          
          respUpdateUser.return_x = True;
          response.put('response_x',respUpdateUser);
          system.debug('**Response-->UpdateUser**'+response);
      
      }
      
      if(request instanceOf  WS_PRM_UIMSUserAdmServiceModel_User.approveUsers){
          system.debug('**approveUsers**');
          WS_PRM_UIMSUserAdmServiceModel_User.approveUsersResponse respApproveUser = new WS_PRM_UIMSUserAdmServiceModel_User.approveUsersResponse();
          
          WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReport uSAR = new WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReport();
          
          WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReportElement[] uSARELst = new WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReportElement[]{};  
          
          WS_PRM_UIMSUserAdmServiceModel_User.userSimpleActionReportElement uSARE = new WS_PRM_UIMSUserAdmServiceModel_User.userSimpleActionReportElement();
          
          uSARE.federatedId = '123456789oiup';
          uSARE.status      = True;
          uSARE.message     = 'Success Approve';
          
          uSARELst.add(uSARE);
          
          uSAR.elements = uSARELst;
          
          respApproveUser.return_x = uSAR;
          response.put('response_x',respApproveUser);
          system.debug('**Response-->ApproveUser**'+response);
      
      }
      
      if(request instanceOf  WS_PRM_UIMSUserAdmServiceModel_User.rejectUsers){
          system.debug('**rejectUsers**');
          WS_PRM_UIMSUserAdmServiceModel_User.rejectUsersResponse respRejectUser = new WS_PRM_UIMSUserAdmServiceModel_User.rejectUsersResponse();
          
          WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReport uSAR = new WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReport();
          
          WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReportElement[] uSARELst = new WS_PRM_UIMSUserAdmserviceModel_User.userSimpleActionReportElement[]{};  
          
          WS_PRM_UIMSUserAdmServiceModel_User.userSimpleActionReportElement uSARE = new WS_PRM_UIMSUserAdmServiceModel_User.userSimpleActionReportElement();
          
          uSARE.federatedId = '123456789oiup';
          uSARE.status      = True;
          uSARE.message     = 'Success Reject';
          
          uSARELst.add(uSARE);
          
          uSAR.elements = uSARELst;
          
          respRejectUser.return_x = uSAR;
          response.put('response_x',respRejectUser);
          system.debug('**Response-->RejectUser**'+response);
      
      }
      
      if(request instanceOf  WS_PRM_UIMSUserAdmServiceModel_User.getUserList){
          system.debug('**rejectUsers**');
          WS_PRM_UIMSUserAdmServiceModel_User.getUserListResponse respgetUser = new WS_PRM_UIMSUserAdmServiceModel_User.getUserListResponse();
          
          WS_PRM_UIMSUserAdmserviceModel_User.identityWithStatus[] iWSLst = new WS_PRM_UIMSUserAdmserviceModel_User.identityWithStatus[]{};  
          WS_PRM_UIMSUserAdmServiceModel_User.identityWithStatus      iWS = new WS_PRM_UIMSUserAdmServiceModel_User.identityWithStatus();
          
          iWS.federatedID = '123456789oiup';
          iWS.email       = 'test@test.com';
          iWS.firstName   = 'User FirstName';
          iWS.lastName    = 'User FirstName';
          iWS.languageCode= 'en_US';
          iWS.status      = True;
          
          iWSLst.add(iWS);
          
          respgetUser.return_x = iWSLst;
          response.put('response_x',respgetUser);
          system.debug('**Response-->getUserList**'+response);
      
      }
      
      if(request instanceOf  WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUid){
          system.debug('**getUserListByInvitationUid**');
          WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUidResponse respInvitationUID = new WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUidResponse();
          
          WS_PRM_UIMSUserAdmserviceModel_User.identity[] idenLst = new WS_PRM_UIMSUserAdmserviceModel_User.identity[]{};  
          WS_PRM_UIMSUserAdmServiceModel_User.identity      iden = new WS_PRM_UIMSUserAdmServiceModel_User.identity();
          
          iden.federatedID = '123456789oiup';
          iden.email       = 'test@test.com';
          iden.firstName   = 'User FirstName';
          iden.lastName    = 'User FirstName';
          iden.languageCode= 'en_US';
          
          
          idenLst.add(iden);
          
          respInvitationUID.return_x = idenLst;
          response.put('response_x',respInvitationUID);
          system.debug('**Response-->getUserListByInvitationUid**'+response);
      
      }
      
      if(request instanceOf WS_uimsv22ServiceImsSchneider.reactivate) {
        WS_uimsv22ServiceImsSchneider.reactivateResponse rResponse = new WS_uimsv22ServiceImsSchneider.reactivateResponse();
        rResponse.return_x = 'test123@abc.com';
        response.put('response_x',rResponse);  
        system.debug('**Response-->updatePassword**' + response);
      }

    }
}