@isTest
private class VFCServiceContractaccountCockpit_TEST {

    static testMethod void myUnitTest() {
        // create records for test
        Account a1 = Utils_TestMethods.createAccount();
        insert a1;
        system.debug('a1=='+a1.id);
        SVMXC__Service_Contract__c ct = new SVMXC__Service_Contract__c(Name = 'contract',SVMXC__Company__c=a1.id,SVMXC__Active__c = true);
        ct.SVMXC__Contract_Price2__c =100;
        ct.LeadingBusinessBU__c ='ITB';
        ct.ContractType__c ='test';
        ct.SVMXC__Start_Date__c= system.today();
        ct.SVMXC__End_Date__c =system.today();
        
        insert ct;
        system.debug('ct=='+ct.id);
        PageReference pageRef=Page.VFPServiceContractaccountCockpit;
        Test.setCurrentPageReference(pageRef);
        VFCServiceContractaccountCockpit myController=new VFCServiceContractaccountCockpit(new ApexPages.StandardController(ct));
    }
}