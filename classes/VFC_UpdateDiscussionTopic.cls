/*
Author:sid
*/
global without sharing class VFC_UpdateDiscussionTopic{
    public Event eventrecord{get;set;}
    public VFC_UpdateDiscussionTopic(ApexPages.StandardController controller){
        //eventrecord=(Event)controller.getRecord();   
        //HotFix to make discussion topic inline VFP work in the archived events ; ALL ROWS in the soql will fetched the archived events  
        Id eveId=(Id)System.currentPageReference().getParameters().get('id');
        System.debug('>>>>>id>>>'+eveId);
        eventrecord=[Select Id,Success__c,Off3Comments__c,Off3Comments2__c,Off3Comments3__c,Off3Comments4__c,Off3Comments5__c,X3DiscussedTopic1__c,X3DiscussedTopic2__c,X3DiscussedTopic3__c,X3DiscussedTopic4__c,X3DiscussedTopic5__c from Event where IsDeleted = false and id=:eveId ALL ROWS];
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
    }
    
    @RemoteAction
    global static String getDiscussionTopics(){
        User userCountry = [select Country__c from User where id=:userinfo.getUserId()];
        //Added Order by Name in below query for Mar 2016 release - BR-8612
        return JSON.serializePretty([select Name,DiscussionTopicDescription__c from DiscussionTopic__c Where Name like :userCountry.Country__c+'%' Order by Name]);
    }
    
    @RemoteAction
    global static String updateEvent(String eventString){
        Event eventrecord=(Event)JSON.deserialize(eventstring,Event.Class);
        Database.SaveResult saveresult=Database.update(eventrecord,false);
        return JSON.serializePretty(saveresult);
    }   
}