/*
25-Feb-2014     Srinivas Nallapati  Apr 14 Prm

To Run this batch from console
    Batch_processPartnerPermissionSets batch_ppsa = new Batch_processPartnerPermissionSets(); 
    batch_ppsa.query = 'Select Active__c, Contact__c,FeatureCatalog__c, LastModifiedDate from ContactAssignedFeature__c Where LastModifiedDate =Last_N_Days:15';
    Database.executebatch(batch_ppsa);

*/
global class Batch_ProcessPartnerPermissionSets implements Database.Batchable<sObject>,Queueable{
    
    public String query;
    List<Id> contIdLst = new List<Id>();
    List<CS_PRM_Contact_Assign_Permission__c> conAssignPermission = CS_PRM_Contact_Assign_Permission__c.getall().values();
     
    public void execute(QueueableContext context) {
     Batch_ProcessPartnerPermissionSets bpps = new Batch_ProcessPartnerPermissionSets();
      Database.executebatch(bpps,50);       
    }
    
    global Batch_ProcessPartnerPermissionSets() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query== null)
            query = 'Select Active__c, Contact__c,FeatureCatalog__c, LastModifiedDate from ContactAssignedFeature__c Where LastModifiedDate =Today';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ContactAssignedFeature__c> scope) {
        
        List<ContactAssignedFeature__c> activeCAF = new List<ContactAssignedFeature__c>();
        List<ContactAssignedFeature__c> inactiveCAF = new List<ContactAssignedFeature__c>();
        
        for(ContactAssignedFeature__c caf : scope)
        {
            if(caf.Active__c)
                activeCAF.add(caf);
            else
                inactiveCAF.add(caf);   
        }

        if(activeCAF.size() > 0)
            AP_ContactAssignedFeature.createPermissionSetAssignments2(activeCAF);
        if(inactiveCAF.size() > 0)
            AP_ContactAssignedFeature.deletePermissionSetAssignments2(inactiveCAF);

    } 
    
    global void finish(Database.BatchableContext BC) {
        // Deleting the data in the custom setting after the Batch processing is Success.
        if(conAssignPermission != Null && conAssignPermission.size() >0)
            if(!Test.isRunningTest())
                Delete conAssignPermission;
            
    }   
    
}