@istest
public class AP_AffectedProducts_Test {
    static testmethod void unitTest1(){
        test.startTest();
        BusinessRiskEscalationEntity__c breEntity = new BusinessRiskEscalationEntity__c (Name='Test Name', Entity__c='Test Entity-1', Location_Type__c='Plant');
        insert breEntity;
        CS_Severity__c cs = new CS_Severity__c(Name = 'Minor', Value__c = 1);
         insert cs;
         Problem__c prblm = new Problem__c(Title__c = 'Test Problem - Divya', RecordTypeid =Label.CLI2PAPR120014, Sensitivity__c = 'Confidential', Status__c = 'New', AccountableOrganization__c = breEntity.id, Severity__c ='Minor', AffectedProductLines__c = '');  
         insert prblm;
          Problem__c prblm2 = new Problem__c(Title__c = 'Test Problem - Divya1234', RecordTypeid =Label.CLI2PAPR120014, Sensitivity__c = 'Confidential', Status__c = 'New', AccountableOrganization__c = breEntity.id, Severity__c ='Minor', AffectedProductLines__c = '123Pr');  
         insert prblm2;
        /* Problem__c Prblm = Utils_TestMethods.createProblem(breEntity.id,1); 
        Database.insert(Prblm);
         */
         List<CommercialReference__c> crlst = new list<CommercialReference__c>();
         CommercialReference__c cr1 = new CommercialReference__c(Problem__c = prblm.id, Family__c= 'Family1234', ProductLine__c='1234ProductLine', CommercialReference__c='CommercialReference1234' );
         crlst.add(cr1);
         CommercialReference__c cr2 = new CommercialReference__c(Problem__c = prblm.id, Family__c= 'Family12345', ProductLine__c='1234ProductLine5', CommercialReference__c='CommercialReference12345' );
         crlst.add(cr2);
         CommercialReference__c cr3 = new CommercialReference__c(Problem__c = prblm2.id, Family__c= 'Family123', ProductLine__c='123ProductLine', CommercialReference__c='CommercialReference123' );
         crlst.add(cr3);
         CommercialReference__c cr4 = new CommercialReference__c(Problem__c = prblm2.id, Family__c= 'Family12', ProductLine__c='12ProductLine', CommercialReference__c='CommercialReference12' );
         crlst.add(cr4);
         insert crlst;
         
         Delete cr3;
         test.stopTest();
    }
}