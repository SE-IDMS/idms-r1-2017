@isTest
public class VFC_PASubscriberCase_TEST {
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.City__c = 'Bangalore';
        testAccount.SEAccountID__c = '12345678';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Case objCase = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id,'testing');
        insert objCase;
        List<Subscriber__c> lstSubscribe = new List<Subscriber__c>();
        Subscriber__c testSubsrcibe = new Subscriber__c(Account__c = testAccount.Id, Case__c = objCase.Id, Email__c = 'test@test.com');
        lstSubscribe.add(testSubsrcibe);
        Subscriber__c testSubsrcibe1 = new Subscriber__c(Account__c = testAccount.Id, Case__c = objCase.Id, Email__c = 'test1@test1.com');
        lstSubscribe.add(testSubsrcibe1);
        insert lstSubscribe;
    }
    
    @isTest static void testMethod1() {
        Case caseObj = [select id from case limit 1];
        apexpages.currentpage().getparameters().put('id', caseObj.Id);
        VFC_PASubscriberCase controller = new VFC_PASubscriberCase();
        VFC_PASubscriberCase.getlstCase();
    }
    
    @isTest static void testMethod2() {
        Account testAccount = [select id from Account limit 1];
        Contact testcontact1 = [select id from contact limit 1];
        Contract testContract1 = Utils_TestMethods.createContract(testAccount.Id, testcontact1.Id);
        insert testContract1;
        CTR_ValueChainPlayers__c testCVCP1 = Utils_TestMethods.createCntrctValuChnPlyr(testContract1.Id);
        testCVCP1.legacypin__c ='1234';
        testCVCP1.Contact__c = testcontact1.Id;
        insert testCVCP1;
        
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact2');
        testcontact.SEContactID__c = '12345678';
        testcontact.Email = 'test1@test.com';
        testcontact.WorkPhone__c='12345678900';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='12345';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
        
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='1234';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        
        WS_PAPortletUserManagement.UserInformation uInfo1 = new WS_PAPortletUserManagement.UserInformation();
        uInfo1.strContactGoldenId='12345'; 
        uInfo1.strAccountGoldenId='12345678';
        uInfo1.strCustomerFirstId='12345';
        uInfo1.strWebUserId='';
        uInfo1.strWebUserName='testInvv'; 
        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.createUser(uInfo1);
        
        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo.strAccountGoldenId = '12345678';
        manualShareInfo.strSharingAccessLevel = 'Edit';
        manualShareInfo.strWebUserName = 'testInv';
        
        WS_PAPortletUserManagement.SiteResult userresult6 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
        system.assertEquals('0', userresult6.ErrorCode);
        
        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo1 = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo1.strAccountGoldenId = '12345678';
        manualShareInfo1.strSharingAccessLevel = 'Edit';
        manualShareInfo1.strWebUserName = 'testInvv';
        
        WS_PAPortletUserManagement.SiteResult userresult7 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo1);
        system.assertEquals('0', userresult7.ErrorCode);
        
        list<VFC_PASubscriberCase.CaseWrap> lstCheckedUser = new list<VFC_PASubscriberCase.CaseWrap>();
        Set<Id> userIds = new Set<Id>();
        userIds.add(userresult.UserId);
        userIds.add(userresult1.UserId);
        Case caseObj = [select id from case limit 1];
        for(User objUser :[select id ,FirstName,LastName,Email,Contact.Email, ContactId,Contact.Account.Name from User where Id IN: userIds]){
            User objU = new User(id = objUser.id, FirstName = objUser.FirstName,LastName=objUser.LastName,Email =objUser.contact.email);
            VFC_PASubscriberCase.CaseWrap  cu = new VFC_PASubscriberCase.CaseWrap(false,objU,(String)caseObj.Id,null,objUser.Contact.Account.Name, objUser.Contact.AccountId);
            lstCheckedUser.add(cu);
         }
         Test.startTest();
         String jsonStr = JSON.serialize(lstCheckedUser);
         VFC_PASubscriberCase.subscriberResult('test', caseObj.Id, jsonStr);
         VFC_PASubscriberCase.subscriberResult('testtt', caseObj.Id, jsonStr);
         VFC_PASubscriberCase.subscriberResult('', caseObj.Id, jsonStr);
         VFC_PASubscriberCase.getClassInstances(jsonStr, caseObj.Id);
         VFC_PASubscriberCase.getClassInstances('[]', caseObj.Id);
         Test.stopTest();
    }
}