/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 11/03/2011
    Description     : Class for triggers BUD_BudgetBeforeInsert and BUD_BudgetBeforeUpdate.
                      Creates a list of budget records to be processed.
*/

public with sharing class BUD_BudgetTriggers 
{

    public static void budgetBeforeUpdate(Map<Id,Budget__c> newMap,Map<Id,Budget__c> oldMap)
    {
        System.Debug('****** budgetBeforeUpdate Start ****'); 
        List<Budget__c> budgetListToProcessCONDITION1 = new List<Budget__c>();
        
        for (ID budId:newMap.keyset())
        {
            Budget__c newBud = newMap.get(budId);
            Budget__c oldBud = oldMap.get(budId);
            
            if(newBud.Active__c == true && oldbud.Active__c == false)
            {
                budgetListToProcessCONDITION1.add(newBud);                
            }
        }
        
        
        if (budgetListToProcessCONDITION1.size()>0) 
            BUD_BudgetUtils.deactivateActiveBudget(budgetListToProcessCONDITION1,true);
        if(newMap.values().size()>0)
            BUD_BudgetUtils.updateBudgetFields(newMap.values());            
        
        System.Debug('****** budgetBeforeUpdate End ****'); 
    }
    
    public static void budgetBeforeInsert(List<Budget__c> newBudgets)
    {
        System.Debug('****** budgetBeforeInsert Start ****'); 
        List<Budget__c> budgetListToProcessCONDITION1 = new List<Budget__c>();
        
        for (Budget__c newBud:newBudgets)
        {                                  
            if(newBud.Active__c == true)
            {
                budgetListToProcessCONDITION1.add(newBud);                
            }
        }
        
        
        if (budgetListToProcessCONDITION1.size()>0) 
            BUD_BudgetUtils.deactivateActiveBudget(budgetListToProcessCONDITION1,false);
        if(newBudgets.size()>0)
            BUD_BudgetUtils.updateBudgetFields(newBudgets);            
            
        System.Debug('****** budgetBeforeInsert End ****');         
        
    }
    public static void budgetAfterDelete(List<Budget__c> oldBudgets)
    {
        System.Debug('****** budgetBeforeDelete Start ****'); 
        List<Budget__c> budgetListToProcessCONDITION1 = new List<Budget__c>();
        
        for (Budget__c oldBud:oldBudgets)
        {                                  
            if(oldBud.Active__c == true)
            {
                budgetListToProcessCONDITION1.add(oldBud);                
            }
        }
        
        if(oldBudgets.size()>0)
            BUD_BudgetUtils.updateProjectRequestFieldsonDelete(budgetListToProcessCONDITION1);            
            
        System.Debug('****** budgetBeforeDelete End ****');         
        
    }
    

        
}