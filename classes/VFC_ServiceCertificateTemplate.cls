public with sharing class VFC_ServiceCertificateTemplate {
    private static boolean debug = Boolean.valueOf(Label.CLAPR15SRV62);
    public SVMXC__Service_Contract__c scheader{get;set;}
    public Map<String, Integer> pmVisitMap {get;set;}
    public String keyString {get;set;}
    public String language {get;set;}
    public String renderFormat {get;set;}
    public String strDispTyp {get;set;}
    public boolean showCertificate {get; set;}
    
    list<SVMXC__Service_Contract__c> sllist = new list<SVMXC__Service_Contract__c>();
    Map<id,SVMXC__Service_Contract__c> slMap = new Map<id,SVMXC__Service_Contract__c>();
    public Map<id,List<SVMXC__Service_Contract_Products__c>> cpMap = new Map<id,List<SVMXC__Service_Contract_Products__c>>();
    public List<List<SVMXC__Service_Contract_Products__c>>  cpRecList {get;set;}
   // public list<SVMXC__PM_Coverage__c> pmCoverageList {get;set;}

    //public string strDispTyp = System.currentPageReference().getParameters().get('DispTyp');
    public string getstrDispTyp(){
        String displayType = null;
        
        if(this.strDispTyp == 'pdf'){
          displayType = this.strDispTyp;
        }
        debug('getstrDispTyp(): '+displayType);
         
        return displayType;
    }
    
     public string contype {get; set;}//System.currentPageReference().getParameters().get('DispTyp');
     
     /*
    public string getcontype (){
      String contentType = '';
      
      if(strDispTyp == 'doc'){
        contentType = 'application/msWord#service-certificate.docx';
      }
      //COMM_Utils_Reports.getcontype(strDispTyp);
      debug('getcontype(): '+contentType);      
      return contentType;
    }*/
    
     string RecordId = System.currentPageReference().getParameters().get('RecordId');
    public VFC_ServiceCertificateTemplate(ApexPages.StandardController controller) { 
       language = System.currentPageReference().getParameters().get('language');
       strDispTyp = System.currentPageReference().getParameters().get('DispTyp');
       
        
       if(language != null){
         showCertificate = true;
         
         if(strDispTyp == 'doc'){
          strDispTyp = null;
          contype = 'application/msWord#service-certificate.docx';
        }else if(strDispTyp == 'xls'){
          strDispTyp = null;
          contype = 'application/vnd.ms-excel#service-certificate.xls';
        }
       }else{
         showCertificate = false;
         renderFormat = strDispTyp;
         strDispTyp = null;
       }
       
       // Y. Tisserand: adding logic to calculate PM visit on the fly
       debug('START VFC_ServiceCertificateTemplate');
       Map<Id, List<SVMXC__PM_Schedule_Definition__c>> pmScheduleDefMap = new Map<Id, List<SVMXC__PM_Schedule_Definition__c>>();
       Map<Id, List<SVMXC__PM_Coverage__c>> pmCoverageMap = new Map<Id, List<SVMXC__PM_Coverage__c>>();
       pmVisitMap = new Map<String, Integer>();
       List<SVMXC__PM_Plan__c> pmList = [select id, name, SVMXC__Service_Contract__c, SVMXC__Start_Date__c, SVMXC__End_Date__c, (select id, name, SVMXC__Frequency__c, SVMXC__Frequency_Unit__c,SVMXC__Recurring__c,SVMXC__Processed__c  from SVMXC__PM_Schedule_Definition__r), (select id, name, SVMXC__Product_Name__c from SVMXC__PM_Coverage__r) from SVMXC__PM_Plan__c where SVMXC__Service_Contract__r.ParentContract__c = :RecordId];
       Integer countPMVisit = 0; //Added by VISHNU C for Q2 Release 2016 DEF-9996    

        for(SVMXC__PM_Plan__c pm : pmList ){
           debug('Current PM Plan: '+pm);
           List<SVMXC__PM_Schedule_Definition__c> pmScheduleDefList = pm.getSObjects('SVMXC__PM_Schedule_Definition__r'); 
           debug('List of PM schedule definition: '+pmScheduleDefMap); 
           List<SVMXC__PM_Coverage__c> pmCoverageList = pm.getSObjects('SVMXC__PM_Coverage__r');
           debug('List of PM coverage: '+pmCoverageMap);
            
           if( pmScheduleDefList != null && pmScheduleDefList.size() > 0 )//Added by VISHNU C for Q2 Release DEF-9996 2016
           {
                countPMVisit = countPMVisit(pm, pmScheduleDefList, pmCoverageList);       
           }
           
           
           if(pmCoverageList != null && pmCoverageList.size()>0){
               for(SVMXC__PM_Coverage__c pmCoverage:pmCoverageList ){
                 debug('Current PM Coverage: '+pmCoverage);
               
                 if(!pmVisitMap.containsKey(pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c)){
                   debug('New serviceline/installed product: '+pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c+' = '+countPMVisit);
                   pmVisitMap.put(pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c, countPMVisit);
                   keyString += ';'+pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c;
                 }else{  
                   Integer tmpPMVisit = pmVisitMap.get(pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c);
                   //debug('Existing serviceline/installed product: '+pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c+' = '+tmpPMVisit+countPMVisit);
                   pmVisitMap.put(pm.SVMXC__Service_Contract__c+':'+pmCoverage.SVMXC__Product_Name__c, tmpPMVisit+countPMVisit);
                 }
               }
           }
           
           pmScheduleDefMap.put(pm.Id, pmScheduleDefList );
           pmCoverageMap.put(pm.Id, pmCoverageList); 
       }
   
       debug('Final PM visit Map: '+pmVisitMap);
       // End Y. Tisserand

            scheader = new SVMXC__Service_Contract__c();
            cpRecList = new List<List<SVMXC__Service_Contract_Products__c>>();
            Map<String, SObjectField> fieldMap = Schema.SObjectType.SVMXC__Service_Contract__c.fields.getMap();
                String fieldNames = getFieldNamesFromList(getFieldList(fieldMap,true));
                fieldNames +=' ,   SVMXC__Company__r.Name ';
                fieldNames +=' ,   SVMXC__Contact__r.Name';//Added for Defect# DEF-7165
                fieldNames +=' ,   SoldtoAccount__r.Name';
                fieldNames +=' ,   SoldtoAccount__r.Street__c';
                fieldNames +=' ,   SoldtoAccount__r.City__c';
                fieldNames +=' ,   SoldtoAccount__r.StateProvince__r.Name';
                fieldNames +=' ,   SoldtoAccount__r.Country__r.Name';
                fieldNames +=' ,   SoldtoAccount__r.ZipCode__c';
              
                fieldNames +=' ,   DefaultInstalledAtAccount__r.Name';
                fieldNames +=' ,   SVMXC__Sales_Rep__r.Name';
       
        string Query = 'select '+ fieldNames +'  from SVMXC__Service_Contract__c s where s.id = \'' + RecordId + '\'';
        scheader  = database.query(Query );
        sllist  = [select id,name,SVMXC__Active__c,ParentContract__c from SVMXC__Service_Contract__c where ParentContract__c =:RecordId];  //and SVMXC__Active__c=true ];
        slMap.putAll(sllist );
        List<SVMXC__Service_Contract_Products__c> cpList = [select Golden_Asset_Id__c, id,Schedule_PM_visit__c,name,InstallAt__c,SVMXC__Installed_Product__r.SVMXC__Company__c,SVMXC__Installed_Product__r.SVMXC__Company__r.name,SVMXC__Start_Date__c,SVMXC__End_Date__c,SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c,SVMXC__Installed_Product__r.name, SVMXC__Service_Contract__r.SVMXC__Service_Plan__r.name from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c in : slMap.keyset()  order by SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c];
        for(SVMXC__Service_Contract_Products__c cp:cpList ){
             
             if(cp.SVMXC__Installed_Product__r.SVMXC__Company__c != null )
             {
                 
                 if(cpMap.containskey(cp.SVMXC__Installed_Product__r.SVMXC__Company__c ))
                 {
                     cpMap.get(cp.SVMXC__Installed_Product__r.SVMXC__Company__c ).add(cp);
                 }
                 else{
                     cpMap.put(cp.SVMXC__Installed_Product__r.SVMXC__Company__c , new List<SVMXC__Service_Contract_Products__c>{ cp});
                 }
             }
              
        }
        
        for(Id aid: cpMap.keySet()){
        
            cpRecList.add(cpMap.get(aid));
        }
        
        
    }
    
    //Getter Setter for report generation as PDF , DOC , XLS
       

    public String getREPTAG001(){ return system.Label.SCONREP001_REPTAG001;}
    public String getREPTAG002(){ return system.Label.SCONREP001_REPTAG002;}
    public String getREPTAG0011(){ return system.Label.SCONREP001_REPTAG0011;}
    public String getREPTAG0012(){ return system.Label.SCONREP001_REPTAG0012;}
    public String getREPTAG0014(){ return system.Label.SCONREP001_REPTAG0014;}
    public String getREPTAG0015(){ return system.Label.SCONREP001_REPTAG0015;}
    public String getREPTAG0016(){ return system.Label.SCONREP001_REPTAG0016;}
    public String getREPTAG0017(){ return system.Label.SCONREP001_REPTAG0017;}
    public String getREPTAG0018(){ return system.Label.SCONREP001_REPTAG0018;}
    public String getREPTAG0019(){ return system.Label.SCONREP001_REPTAG0019;}
    public String getREPTAG0020(){ return system.Label.SCONREP001_REPTAG0020;}
    public String getREPTAG0021(){ return system.Label.SCONREP001_REPTAG0021;}
    public String getREPTAG0022(){ return system.Label.SCONREP001_REPTAG0022;}
    public String getREPTAG0023(){ return system.Label.SCONREP001_REPTAG0023;}
    public String getREPTAG0024(){ return system.Label.SCONREP001_REPTAG0024;}
    public String getREPTAG0025(){ return system.Label.SCONREP001_REPTAG0025;}
    public String getREPTAG0026(){ return system.Label.SCONREP001_REPTAG0026;}
    
    public static String getFieldNamesFromList(List<String> fieldList) 
   {
    String fieldNames = '';
    for (String field : fieldList) {
        if (fieldNames.length()>0) {
            fieldNames += ',';
        }
        
        
        fieldNames += field;
    }        
    return fieldNames;
   }
     // Methods used in generating Dynamic SOQL    
    public static List<String> getFieldList( Map<String,Schema.SObjectField> fieldMap, Boolean selectAllFields)
    {
        List<String> fieldList = new List<String>();        
        //build dynamic list of fieldnames
        for (String fieldKey : fieldMap.keySet()) 
            {
            Schema.SObjectField fsObj = fieldMap.get(fieldKey);
            Schema.DescribeFieldResult f = fsObj.getDescribe();
            String fieldName = f.getName();            
            if (selectAllFields) 
                {
                    fieldList.add(fieldName);
                } 
            else {
                if (f.getName()=='Id' || f.isNameField() || f.isCustom()) {
                    fieldList.add(fieldName);
                }
            }
    }        
    return fieldList;
    }
   
   private Integer countPMVisit(SVMXC__PM_Plan__c pm, List<SVMXC__PM_Schedule_Definition__c> pmScheduleDefList, List<SVMXC__PM_Coverage__c> pmCoverageList){
      debug('START countPMVisit');
      Date startDate = pm.SVMXC__Start_Date__c;
      Date endDate = pm.SVMXC__End_Date__c;
      Integer numberOfPMCoverage = 0;
      Integer count = 0;
      Integer count1;
      Integer count2;
      
       if(pmCoverageList != null && pmCoverageList.size()>0){
           numberOfPMCoverage = pmCoverageList.size();
       }      

          for(SVMXC__PM_Schedule_Definition__c pmScheduleDef: pmScheduleDefList){
             debug('current count: '+count );
                
                 //Hypothesis: eather all the PM Definition are reccuring or no one of them reccuring
                Boolean reccuring = pmScheduleDef.SVMXC__Recurring__c;
              
                if (reccuring == FALSE){
                      // Count of PM Schedule elements
                      count1 = [select count() from SVMXC__PM_Schedule__c where SVMXC__PM_Plan__c =: pm.id];
                      // Count of PM Work Orders elements already proceesed
                      //count2 = [select count() from SVMXC__Service_Order__c where SVMXC__PM_Plan__c =: pm.id];
                      //Count PM History having a WO
                      count2 = [select count() from SVMXC__PM_History__c where SVMXC__PM_Plan__c =: pm.id and SVMXC__Work_Order__c <> ''];
                      
                    if(numberOfPMCoverage != 0){
                        count2 = count2 / numberOfPMCoverage;
                    }
                    
                    // PM visit (for 1 asset) = Total PM Schedule +  (Total PM History with WO not null / # of covered assets) 
                    count = count1 + count2;
                      break;
                }
                
              //  SVMXC__PM_Schedule__c  pmsch = [select SVMXC__Scheduled_On__c, SVMXC__Last_Run_Date__c from SVMXC__PM_Schedule__c where SVMXC__PM_Schedule_Definition__c =: pmScheduleDef.id];
              


                Date tmpDate = startDate;
                Integer freq = pmScheduleDef.SVMXC__Frequency__c.intValue();
                String freqUnit = pmScheduleDef.SVMXC__Frequency_Unit__c;
    

                    while(tmpDate < endDate ){
                        debug('current tpmDate: '+tmpDate);

                        if(freqUnit == 'Months'){
                          tmpDate = tmpDate.addMonths(freq);
                        }else if(freqUnit == 'Weeks'){
                           tmpDate = tmpDate.addDays(7*freq);
                        }else if(freqUnit == 'Years'){
                          tmpDate = tmpDate.addYears(freq);
                        }
                    
                        if(tmpDate < endDate){
                            count ++;
                        }
                }
               debug('Final count: '+count);
            
            }

     return count;
   
  } 
    
   private static void debug(String aMsg) {
        if(debug) {
            System.debug('### '+aMsg);
        }
    }
    
    public PageReference setLanguage(){
      debug('language: '+language);
      debug('format: '+renderFormat);
      debug('Content Type: '+contype);
      //strDispTyp = 'pdf';
      
      String url = '/apex/VFP_ServiceCertificateTemplate?DispTyp='+renderFormat+'&language='+language+'&RecordId='+RecordId;
      PageReference pref = new PageReference(url);
      //pref.getHeaders().put('Content-Disposition', 'attachment; filename="service-certificate.doc"');
      pref.setRedirect(true);
      
      return pref;
    }
    
    public List<SelectOption> getItems() {
        String availableLanguages = Label.CLOCT16SRV02;
        List<SelectOption> options = new List<SelectOption>();
         
        if(String.isNotEmpty(availableLanguages)){
           List<String> languageList = availableLanguages.split(',');
           
           for(String language:languageList){
             String[] codeValue = language.split('__');
             options.add(new SelectOption(codeValue[1],codeValue[0]));
           }
       }
        
        return options;
    }
    
    public pagereference backMethod(){
        String url = '/apex/VFP_ServiceContractCertificate?id='+RecordId+'&ObjName=SVMXC.Service_Contract';
        Pagereference pg = new Pagereference(url); 
        pg.setRedirect(true);
        return pg;
    }

   
}