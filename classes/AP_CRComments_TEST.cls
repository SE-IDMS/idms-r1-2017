/*
 	Author        : Swapnil Saurav
    Date Created  : 18/04/2016
    Description   : Test Class for AP_CRComments
    ---------------------------------------------------
 
*/

@isTest //(SeeAllData=true)
private class AP_CRComments_TEST {
    
    static Testmethod void  complaintRequestCR() {
        
		List<ComplaintRequest__c> listlcr = new List<ComplaintRequest__c>();
     // Create Country
        Country__c CountryObjLCR = Utils_TestMethods.createCountry();
       
        Insert CountryObjLCR;
        // Create User
        User LCRTestUser2 = Utils_TestMethods.createStandardUser('LCROwner');
        Insert LCRTestUser2 ;
        
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
        Insert LCRTestUserCq ;
        
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
       
        insert ContactObj;
        
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
        CaseObj.SupportCategory__c = 'Troubleshooting*';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
        CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
        Insert CaseObj;
         BusinessRiskEscalationEntity__c lCROrganzObj01 = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1');
        Insert lCROrganzObj01; 
         BusinessRiskEscalationEntity__c lCROrganzObj02 = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity');
        Insert lCROrganzObj02; 
         BusinessRiskEscalationEntity__c lCROrganzObj03 = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity2');
        Insert lCROrganzObj03; 
        
        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity',Location__c='LCR Location',Location_Type__c= 'Adaptation Center',Street__c = 'LCR Street',RCPOBox__c = '123456', RCCountry__c = CountryObjLCR.Id,RCCity__c = 'LCR City', RCZipCode__c = '500055',ContactEmail__c ='SCH@schneider.com',PlantCode__c='12345');
        Insert lCROrganzObj; 
         BusinessRiskEscalationEntity__c lCROrganzObj2 = new BusinessRiskEscalationEntity__c(Name='LCR TEST2',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity2',Location__c='LCR Location2', Location_Type__c= 'Adaptation Center', Street__c = 'LCR Street',RCPOBox__c = '123456', RCCountry__c = CountryObjLCR.Id, RCCity__c = 'LCR City', RCZipCode__c = '500055',ContactEmail__c ='SCH@schneider.com',PlantCode__c='143');
        Insert lCROrganzObj2; 
         EntityStakeholder__c  LCROrgStakeHold1= new EntityStakeholder__c(
                                User__c=LCRTestUser2.id,
                                AdditionalInformation__c='TestLCR',
                                Department__c ='TestLCR',
                                BusinessRiskEscalationEntity__c =lCROrganzObj.id,
                                Role__c ='CS&Q Manager'
                                
                                ) ;
        insert LCROrgStakeHold1; 
        List<CR_Comments__c> listComments = new List<CR_Comments__c>();
        CR_Comments__c singleComment = new CR_Comments__c();
        CR_Comments__c nextComment = new CR_Comments__c();
    Test.startTest();
    
        ComplaintRequest__c  CompRequestObj = new ComplaintRequest__c(
                Status__c='Open', 
                Case__c  =CaseObj.id,
                Category__c ='Troubleshooting*',
                reason__c='Marketing Activity Response',
                ForceManualSelection__c=False,
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj.id,
                Comments__c = null,
                PlantCode__c='12345'
               
                ); 
            
                listlcr.add(CompRequestObj);

        insert listlcr;
        for(ComplaintRequest__c cr:listlcr) {
        	singleComment.Complaint_Request__c = cr.id;
            singleComment.Comment__c = 'This is the first comment';
            listComments.add(singleComment);
        }
        insert listComments;
        listComments.clear();
        for(ComplaintRequest__c cr:listlcr) {
            nextComment.Complaint_Request__c = cr.id;
            nextComment.Comment__c = 'This is the second comment';
            listComments.add(nextComment);
        }
        insert nextComment;
        //insert listComments;
        AP_CRComments.updateCommentsOnCR(listComments);
        List<ComplaintRequest__c> CommentsList = [Select Id,Comments__c from ComplaintRequest__c where ID =: CompRequestObj.id];
        System.debug('List='+CommentsList);
	}
}