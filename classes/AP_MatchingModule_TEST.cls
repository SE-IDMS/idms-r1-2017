/*
Author          : Debashis Babu (SFDC) 
Date Created    : 19/08/2016
Description     : Test Class created for MatchingModule class
*/
@isTest
public class AP_MatchingModule_TEST{
    
    public static Id profileId = [select id from profile where name ='SE - Solution Development Team Advanced' limit 1].id;
    public static user runUser = Utils_TestMethods.createStandardUser(profileId,'runUser1');
    
    @testSetup static void setup() { 
        
        runUser.UserName = 'runUser1@anoop.com';
        System.runAs(runUser){
            
            Country__c country = Utils_TestMethods.createCountry();
            insert country ;
            StateProvince__c state = Utils_TestMethods.createStateProvince(country.id);
            state .StateProvinceExternalId__c='FR01';
            insert state ;
            
            list<Account> accList = new List<Account>();
            Account acc = Utils_TestMethods.createAccount();
            acc.ClassLevel1__c ='SI';
            acc.City__c='testCity';
            acc.Country__c = country.id; 
            accList.add(acc);
            
            Account acc1 = Utils_TestMethods.createAccount();
            acc1.ClassLevel1__c ='SI';
            acc1.City__c='CityDup';
            acc1.Country__c = country.id; 
            acc1.AdditionalAddress__c = 'Duplicate';   
            accList.add(acc1);
            
            Account acc2 = Utils_TestMethods.createAccount();
            acc2.ClassLevel1__c ='SI';
            acc2.City__c='CityDup';
            acc2.Country__c = country.id; 
            accList.add(acc2);
            
            insert accList;
            
            List<Contact> conList = New List<Contact>();
            
            // for checking golden id contact
            Contact con = Utils_TestMethods.createContact(acc.id,'IDMS');
            con.Email='IDMS1@accenture.com';
            con.MobilePhone='8885720489';
            con.StateProv__c = state.id;
            con.SEContactID__c='12345';
            conList.add(con);
            
            // Contact for duplicate check 2 contacts
            Contact con1 = Utils_TestMethods.createContact(acc1.id,'IDMS');
            con1.Email='IDMS@accenture.com';
            con1.MobilePhone='8885720489';
            con1.StateProv__c = state.id;
            conList.add(con1);
            
            // Contact for duplicate check 2 contacts           
            
            Contact dupcon = Utils_TestMethods.createContact(acc1.id,'IDMS');
            dupcon.Email='IDMS@accenture.com';
            dupcon.MobilePhone='8885720489';
            dupcon.StateProv__c = state.id;
            conList.add(dupcon);
            
            insert conList;
            
            
        }
        
        
    }
    
    //single l2 account and duplicate l2 contact, also checking the l1 account and contact
    static testMethod void L1andL2UserCheck()
    {
        runUser.UserName = 'runUser2@anoop.com';
        System.runAs(runUser){
        Test.StartTest() ; 
        
        User L2usr = Utils_TestMethods.createStandardUser('IDMS');
        L2usr.IDMSClassLevel1__c='SI';
        L2usr.CompanyName='TestAccount';
        L2usr.Company_Country__c ='FR';
        L2usr.Company_City__c = 'testCity';
        L2usr.Company_Address1__c ='New Street';
        L2usr.Company_State__c ='01';
        L2usr.Company_Postal_Code__c = '560100'  ; 
        L2usr.FirstName = 'Test';
        L2usr.LastName = 'IDMS';
        
        User L3usr = Utils_TestMethods.createStandardUser('IDMS');
        L3usr.IDMSClassLevel1__c='SI';
        L3usr.CompanyName='TestAccount';
        L3usr.Company_Country__c ='FR';
        L3usr.Company_City__c = 'CityDup';
        L3usr.Company_Address1__c ='New Street';
        L3usr.Company_State__c ='01';
        L3usr.Company_Postal_Code__c = '560100'  ; 
        L3usr.FirstName = 'Test';
        L3usr.LastName = 'IDMS';
        
        User L1usr = Utils_TestMethods.createStandardUser('IDMS1');
        L1usr.Company_Country__c ='FR';
        L1usr.Company_City__c = 'testCity';
        L1usr.Company_Address1__c ='New Street';
        L1usr.Company_State__c ='01';
        L1usr.Company_Postal_Code__c = '560100'  ;
        
        List<User> userList = new List<User>{L2usr,L1usr,L3usr};
            AP_MatchingModule.preMatch(userList); 
        Test.StopTest();
        }
    } 
    
    //pass golden Id to User
    static testMethod void GoldenIdCheck()
    {
        runUser.UserName = 'runUser3@anoop.com';
        System.runAs(runUser){
        Test.StartTest() ; 
        
        User L2usr= Utils_TestMethods.createStandardUser('IDMS');
        L2usr.Company_Country__c ='FR';
        L2usr.Company_City__c = 'testCity';
        L2usr.Company_Address1__c ='New Street';
        L2usr.Company_State__c ='01';
        L2usr.Company_Postal_Code__c = '560100'  ; 
        L2usr.IDMSContactGoldenId__c='12345';
        
        AP_MatchingModule.preMatch(L2usr); 
        Test.StopTest();
        }
    } 
    
    //create case,opportunity,product data
    static testMethod void createRelatedRecords()
    {
        runUser.UserName = 'runUser4@anoop.com';
        System.runAs(runUser){
        Test.StartTest() ; 
        Id accIds = [select id from account where AdditionalAddress__c = 'Duplicate' limit 1].id;
        Id conIds = [select id from contact where accountID = :accIds limit 1].id;
        
        User L2usr= Utils_TestMethods.createStandardUser('IDMS');
        L2usr.email = 'IDMS@accenture.com';
        L2usr.IDMSClassLevel1__c='SI';
        L2usr.CompanyName='TestAccount';
        L2usr.Company_Country__c ='FR';
        L2usr.Company_City__c = 'CityDup';
        L2usr.Company_Address1__c ='New Street';
        L2usr.Company_State__c ='01';
        L2usr.Company_Postal_Code__c = '560100'  ; 
        L2usr.FirstName = 'Test';
        L2usr.LastName = 'IDMS';
        
        Case cs= Utils_TestMethods.createCase(accIds,conIds,'Closed');
        cs.AnswerToCustomer__c ='TestAnswer';
        cs.OtherSymptom__c='Test';
        insert cs;
        
        AP_MatchingModule.preMatch(L2usr); 
        Test.StopTest();
        }
        
    }
    static testMethod void checkOldest()
    {
        runUser.UserName = 'runUser5@anoop.com';
        System.runAs(runUser){
        Test.StartTest() ; 
        
        User L2usr= Utils_TestMethods.createStandardUser('IDMS');
        L2usr.IDMSClassLevel1__c='SI';
        L2usr.CompanyName='TestAccount';
        L2usr.Company_Country__c ='FR';
        L2usr.Company_City__c = 'testCity';
        L2usr.Company_Address1__c ='New Street';
        L2usr.Company_State__c ='01';
        L2usr.Company_Postal_Code__c = '560100'  ; 
        L2usr.FirstName = 'Test';
        L2usr.LastName = 'IDMS';
        
        AP_MatchingModule.preMatch(L2usr); 
        Test.StopTest();
        }
        
    }
    static testMethod void SingleAccountgoldenId()
    {
        runUser.UserName = 'runUser6@anoop.com';
        System.runAs(runUser){
        Test.StartTest() ; 
        User L2usr= Utils_TestMethods.createStandardUser('IDMS');
        L2usr.IDMSClassLevel1__c='SI';
        L2usr.CompanyName='TestAccount';
        L2usr.Company_Country__c ='FR';
        L2usr.Company_City__c = 'testCity';
        L2usr.Company_Address1__c ='New Street';
        L2usr.Company_State__c ='01';
        L2usr.Company_Postal_Code__c = '560100'  ; 
        L2usr.FirstName = 'Test';
        L2usr.LastName = 'IDMS';
        list<Account> accountList = new List<Account>();
        
        Account account = [select id,SEAccountId__c,AdditionalAddress__c from account where AdditionalAddress__c = 'Duplicate' limit 1];
        
        account.SEAccountId__c = '334';
        
        update account;
        
        
        AP_MatchingModule.preMatch(L2usr); 
        Test.stopTest();
        }
        
        
    }
    
    static testMethod void checkerror()
    {
        runUser.UserName = 'runUser7@anoop.com';
        System.runAs(runUser){
        Test.StartTest() ; 
        
        User L2usr = new User(); 
        
        L2usr.IDMSClassLevel1__c='SI';
        L2usr.CompanyName='TestAccount';
        L2usr.Company_Country__c ='FR';
        L2usr.Company_City__c = 'testCity';
        L2usr.Company_Address1__c ='New Street';
        //L2usr.Company_State__c ='01';
        //L2usr.Company_Postal_Code__c = '560100'  ; 
        L2usr.FirstName = 'Test';
        L2usr.LastName = 'IDMS';
        L2usr.Company_Address1__c ='New Street';
        
        
        AP_MatchingModule.preMatch(L2usr); 
        Test.StopTest();
        }
        
    }
    
}