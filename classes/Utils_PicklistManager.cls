/*
    Author          : Nicolas Palitzyne ~ nicolas.palitzyne@accenture.com 
    Date Created    : 28/04/2011
    Description     : Utility abstract class for Picklist Management
*/

public virtual interface Utils_PicklistManager
{
    // Return dependent picklist value 
    List<SelectOption> getPicklistValues(Integer i, String S);
    
    // Return name of the levels
    list<String> getPickListLabels();
}