/*
    Author          : ACN 
    Date Created    : 03/08/2011
    Description     : Test class for VFC54_WarningMessage
*/
@isTest
private class VFC54_WarningMessage_TEST 
{
    static testMethod void testvalidateLeadFields() 
    {
         List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
         if(profiles.size()>0)
         {
          User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43');                
          system.runas(user)
          {
            test.startTest();
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,2);
            insert markCallBack;
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;

            Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead1;    
            
            ApexPages.StandardController sc = new ApexPages.StandardController(lead1);
            VFC54_WarningMessage warningMsg= new VFC54_WarningMessage(sc);
            warningMsg.validateLeadFields();

            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            system.assertEquals(2,lead1.CallBackLimit__c);
            
            lead1.CallAttempts__c = 3;
            update lead1;
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            system.assertEquals(3,lead1.CallAttempts__c);
            
            markCallBack.CallBackLimit__c = 2;
            update markCallBack;
            
            update lead1;
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            sc = new ApexPages.StandardController(lead1);
            warningMsg= new VFC54_WarningMessage(sc);
            warningMsg.validateLeadFields();
            
            lead1.Priority__c = 999;
            update lead1;
            sc = new ApexPages.StandardController(lead1);
            warningMsg= new VFC54_WarningMessage(sc);
            warningMsg.validateLeadFields();
            
            lead1.Status = Label.CL00510;
            lead1.SubStatus__c = Label.CL00511;
            update lead1;
            sc = new ApexPages.StandardController(lead1);
            warningMsg= new VFC54_WarningMessage(sc);
            warningMsg.validateLeadFields();
            
            lead1 = [select Status, SERep__c, SEPartner__c,PurchasedFrom__c,PurchasedProduct__c,PurchasedDate__c,Value__c,convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            system.assertEquals(Label.CL00510,lead1.status);
            system.assertEquals(Label.CL00511,lead1.Substatus__c);            
            system.assertEquals(null,lead1.SERep__c);            
                        
            lead1.Status = Label.CL00510;
            lead1.SubStatus__c = Label.CL00512;
            update lead1;
            sc = new ApexPages.StandardController(lead1);
            warningMsg= new VFC54_WarningMessage(sc);
            warningMsg.validateLeadFields();

            lead1 = [select Status, SERep__c, SEPartner__c,PurchasedFrom__c,PurchasedProduct__c,PurchasedDate__c,Value__c,convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            system.assertEquals(Label.CL00510,lead1.status);
            system.assertEquals(Label.CL00512,lead1.Substatus__c);            
            system.assertEquals(null,lead1.SEPartner__c);            
            
            lead1.Status = Label.CL00510;
            lead1.SubStatus__c = Label.CL00513;
            update lead1;
            sc = new ApexPages.StandardController(lead1);
            warningMsg= new VFC54_WarningMessage(sc);
            warningMsg.validateLeadFields();
            lead1 = [select Status, SERep__c, SEPartner__c,PurchasedFrom__c,PurchasedProduct__c,PurchasedDate__c,Value__c,convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            system.assertEquals(Label.CL00510,lead1.status);
            system.assertEquals(Label.CL00513,lead1.Substatus__c);            
            system.assertEquals(null,lead1.PurchasedFrom__c);            
            system.assertEquals(null,lead1.PurchasedProduct__c);            
            system.assertEquals(null,lead1.PurchasedDate__c);            
            system.assertEquals(null,lead1.Value__c);  
            test.stopTest();                                              
          }
        }  
    }
}