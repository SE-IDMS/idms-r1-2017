/*
  Class Name: WS03_NewFrameOpportunity
  Author: Amitava Dutta
  Created On: 15/12/2010
  Purpose: Basically this class works as generator of one Redirection URL being the controller for the 
  WS03_NewFrameOpportunity page to redirect the user with requisite parameters 
  to generate Standard New Opportunity Page and populate some prefilled values.
  Parameter : Agreement ID [To get the Id From respective Agreement Page]
  Controller Reference: Opportunity Standard Controller
  
*/

global with sharing class WS03_NewFrameOpportunity
{
    public static integer totalOpps = 0 ;
    public static string specificAgreementID{get;set;}
    public static Schema.DescribeSObjectResult getDescribeSobject(string objectname){
        Map<String, Schema.SObjectType> m = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = m.get(objectname) ;
        return s.getDescribe() ;
    }
    Webservice static String Redirect(String agreementId)
    {  
        Schema.DescribeSObjectResult newOpportunity = getDescribeSobject('Opportunity');
        string recordTypeId;        
        string strURL;     
        OppAgreement__c agreements = new OppAgreement__c();
        List<RecordType> recordTypes = new List<RecordType>();
        WS02_SetConstantValue.iscreateseries = false;
        agreements = [SELECT Name, AccountName__c, CurrencyIsoCode, LeadingBusinessBU__c, BusinessMix__c, CREATEDBYID, CreatedBy.FirstName, AccountName__r.Name, Amount__c, Agreementvaliduntil__c, Opportunity_Type__c,PhaseSalesStage__c, AgreementType__c  FROM OppAgreement__c WHERE ID =: agreementId];
          
       
            if(agreements != Null)
            {
                string opportunityName= '';                
                string actualParametersForBm= '' ;            
                List<string> actualBmArray = new List<string>();
                string strCloseDate ;
                string strBM;
                string strYear,strMonth,strDay;
               
                
                /*if(opportunityName.IndexOf('&') > 0)
                {
                    opportunityName= EncodingUtil.urlEncode(opportunityName,'UTF-8');
                }*/ 
                
                if(agreements.BusinessMix__c !=null && agreements.BusinessMix__c !='')
                {                    
                    strBM = agreements.BusinessMix__c;
                    actualBmArray = strBM.split(';'); 
                }
                if(actualBmArray.size() !=0)
                {                                            
                    for(Integer i=0; i<actualBmArray.Size(); i++)
                    {                    
                        actualParametersForBm= actualParametersForBm+ Label.CL00258 + '=' + actualBmArray[i] + '&' ;  
                    }
                }
                
                Date dtEndDate = agreements.Agreementvaliduntil__c;
                strCloseDate = dtEndDate.format();
               
                opportunityName= opportunityName+ agreements.Name + '_'+ strCloseDate;
                
                string recordtype;
                //MODIFIED FOR BR-7111
                //If(agreements.AgreementType__c =='OEM Agreement')
                //{
                    /////March Release: Rahul Thankachan changed the custom labels//////
                    if(agreements.Opportunity_Type__c.equalsIgnoreCase(System.Label.CL00240))
                    { 
                        if(agreements.PhaseSalesStage__c.substring(0,1)=='3')
                        {
                            recordtype=Label.CL00144;
                        }
                        else if (agreements.PhaseSalesStage__c.substring(0,1)=='4')
                        {
                            recordtype=Label.CL00145;
                        }
                        else if (agreements.PhaseSalesStage__c.substring(0,1)=='5')
                        {
                            recordtype=Label.CLMARSLS01;/// Stage 5 Solution(Added in march release)
                        }
                        else if (agreements.PhaseSalesStage__c.substring(0,1)=='6' || agreements.PhaseSalesStage__c.substring(0,1)=='7')
                        {
                            recordtype=Label.CLMARSLS02;/// Stage 6 Sol(Added in March Release)
                        }
                    }
                    else if(agreements.Opportunity_Type__c.equalsIgnoreCase(System.Label.CL00241) || agreements.Opportunity_Type__c.equalsIgnoreCase(System.Label.CL00724))
                    { 
                        if(agreements.PhaseSalesStage__c.substring(0,1)=='3')
                        {
                            recordtype=Label.CL00551;
                        }
                        else if (agreements.PhaseSalesStage__c.substring(0,1)=='4')
                        {
                            recordtype=Label.CL00552;
                        }
                        else if (agreements.PhaseSalesStage__c.substring(0,1)=='5')
                        {
                            recordtype=Label.CL00553;
                        }
                        else if (agreements.PhaseSalesStage__c.substring(0,1)=='6'  || agreements.PhaseSalesStage__c.substring(0,1)=='7')
                        {
                            recordtype=Label.CLAPR15SLS66;
                        }
                    }
                    
                    system.debug(agreements.Opportunity_Type__c + 'bbbbbbbbbbb' + agreements.PhaseSalesStage__c + 'nnnnnnnn'+recordType);
                    strURL ='/' + newOpportunity.getKeyPrefix() + '/e?retURl=' + agreements.Id + '&cancelURl=' + agreements.ID +'&'+Label.CL00549+'='+agreements.Opportunity_Type__c+ '&RecordType=' + recordtype +    '&ent=Opportunity&' + Label.CL00229 + '=' + EncodingUtil.urlEncode(agreements.Name,'UTF-8') + '&' + System.Label.CL00212 + '=' + agreements.ID + '&accid=' + agreements.AccountName__c + '&' + System.Label.CL00213 + '=' + EncodingUtil.urlEncode(agreements.AccountName__r.Name,'UTF-8') +'&' + System.Label.CL00214 + '=' + agreements.AccountName__c + '&' + System.Label.CL00215 + '=' + agreements.LeadingBusinessBU__c + '&opp3=' + EncodingUtil.urlEncode(opportunityName,'UTF-8') + '&opp11=' + EncodingUtil.urlEncode(agreements.PhaseSalesStage__c,'UTF-8') + '&opp12=' + '100' + '&opp16=' + agreements.CurrencyIsoCode +'&'+ System.Label.CL00216 + '=' + 'Simple' + '&' + System.Label.CL00219 + '=' + 'Standard' +'&' + System.Label.CL00217 + '=' + 'Yes' + '&opp7=' + agreements.Amount__c + '&opp9=' + strCloseDate + '&' + Label.CL00257 + '=1' + '&nooverride=1' + '&' + actualParametersForBm;
                /*}
                else
                {
                    strURL ='/' + newOpportunity.getKeyPrefix() + '/e?retURl=' + agreements.Id + '&cancelURl=' + agreements.ID +'&'+Label.CL00549+'='+agreements.Opportunity_Type__c+ '&RecordType=' + Label.CL00278 + '&ent=Opportunity&' + Label.CL00229 + '=' + EncodingUtil.urlEncode(agreements.Name,'UTF-8') + '&' + System.Label.CL00212 + '=' + agreements.ID + '&accid=' + agreements.AccountName__c + '&' + System.Label.CL00213 + '=' + EncodingUtil.urlEncode(agreements.AccountName__r.Name,'UTF-8') +'&' + System.Label.CL00214 + '=' + agreements.AccountName__c + '&' + System.Label.CL00215 + '=' + agreements.LeadingBusinessBU__c + '&opp3=' + EncodingUtil.urlEncode(opportunityName,'UTF-8') + '&opp11=' + EncodingUtil.urlEncode(System.Label.CL00218,'UTF-8') + '&opp12=' + '100' + '&opp16=' + agreements.CurrencyIsoCode + '&'+ System.Label.CL00216 + '=' + 'Simple' + '&' + System.Label.CL00219 + '=' + 'Standard' +'&' + System.Label.CL00217 + '=' + 'Yes' + '&opp7=' + agreements.Amount__c + '&opp9=' + strCloseDate + '&' + Label.CL00257 + '=1' + '&nooverride=1' + '&' + actualParametersForBm;
                }*/
                
                                    
                
                }  
                return strURL;  
        // }
    }
    // -- As there should always be One Frame Opportunity for the Agreement --
    // -- So we calculate only the Number of Frame Opportunity which should / always be One
    // -- And Send the Counted figure to the Calling Function.
    public static Integer CountOpportunityForAgreement(string agreementId)
    {       
        totalOpps = [SELECT COUNT() FROM Opportunity WHERE AgreementReference__c =: agreementId ];
        
        return totalOpps;        
    }
    
    
    // Region Global Methods    
    // Return the Duration for the Agreement
    webService static String GetAgreementInterval(string vstrAgreementID)
    {
        double mdecInterval;
        string mstrVal;
        decimal mdecPatternFigure;
        double dblInterval;
        integer mintTotCurrentOpps;
        List<OppAgreement__c> objOpAgree;
        
        /// 1. Get the Monthly Interval And Master Opportunity created on this Frame Opportuntiy based on the duration of Agreement
        specificAgreementID = vstrAgreementID;
        
        if(Limits.getQueries() > Limits.getLimitQueries())
            System.Debug('######## WS02 Error Querying : '+ Label.CL00173);
        else
            objOpAgree = [SELECT Agreementvalidfrom__c, Agreementvaliduntil__c, AgreementTimeSpan__c, FrameOpportunityInterval__c FROM OppAgreement__c WHERE ID =: specificAgreementID ];
        
        if(Limits.getQueries() > Limits.getLimitQueries())
            System.Debug('######## WS02 Error Querying : '+ Label.CL00173);
        else
            mintTotCurrentOpps = [SELECT COUNT() FROM OPPORTUNITY WHERE AgreementReference__c =: vstrAgreementID];
        
        if(objOpAgree.size() !=0)
        {               
            if(mintTotCurrentOpps > 0)
            {
                if(objOpAgree[0].FrameOpportunityInterval__c !=null)
                {
                    if(objOpAgree[0].FrameOpportunityInterval__c.contains(System.Label.CL00222))
                    {
                        mdecPatternFigure =3;
                    }
                    if(objOpAgree[0].FrameOpportunityInterval__c == System.Label.CL00221)
                    {
                        mdecPatternFigure =12;
                    }
                    if(objOpAgree[0].FrameOpportunityInterval__c == System.Label.CL00223)
                    {
                        mdecPatternFigure =  6;
                    }
                    if(objOpAgree[0].FrameOpportunityInterval__c == System.Label.CL00224)
                    {
                        mdecPatternFigure = 6;
                    }
                    else if(objOpAgree[0].FrameOpportunityInterval__c == System.Label.CL00225)
                    {
                        mdecPatternFigure = 1;
                    }
                    mdecInterval= objOpAgree[0].AgreementTimeSpan__c / mdecPatternFigure;
                    System.Debug('****Agreement Time Span: '+objOpAgree[0].AgreementTimeSpan__c);
                    System.Debug('****MInterval: '+mdecInterval);
                }
                else
                {
                    mdecInterval = 0;
                }  
            }
            else
            {
                mdecInterval = 0;
            } 
        } 
        
    
        if(objOpAgree[0].FrameOpportunityInterval__c == System.Label.CL00222)
        {
            integer totalAgreementDaysinStart = date.daysInMonth(objOpAgree[0].Agreementvalidfrom__c.year(),objOpAgree[0].Agreementvalidfrom__c.month());
            integer mintTotDaysToBeAdded = totalAgreementDaysinStart  - objOpAgree[0].Agreementvalidfrom__c.day();
            Date dtCloseDateStart = objOpAgree[0].Agreementvalidfrom__c.addDays(mintTotDaysToBeAdded); 
            
            Date probableagreementenddate = objOpAgree[0].Agreementvaliduntil__c.addMonths(Double.valueOf(mdecInterval).intValue());
                  
            integer totalAgreementDaysinEnd = date.daysInMonth(probableagreementenddate.year(),probableagreementenddate.month());
            integer totaldaystobeaddedforlastdate = totalAgreementDaysinEnd - probableagreementenddate.day();
            Date dtCloseDateAgreement=objOpAgree[0].Agreementvaliduntil__c.addDays(totaldaystobeaddedforlastdate);
            
            // Get the remaining of the Frame Interval
            integer mintRemaining =Math.mod(Double.valueOf(mdecInterval).intValue(),3);
            
            if(dtCloseDateAgreement > objOpAgree[0].Agreementvaliduntil__c)
            {
                mdecInterval = mdecInterval - 1;
            }
        }

        
        
        return String.valueOf(Double.valueOf(mdecInterval).intValue()); 
    }
    
}