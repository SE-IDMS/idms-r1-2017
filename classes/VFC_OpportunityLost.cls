public with sharing class VFC_OpportunityLost{
public Opportunity oppt{get;set;}
public String compexceptionmessage{get;set;}{compexceptionmessage=null;}
public String sourcePage;
public boolean displayCompitetorSelection{get;set;}{displayCompitetorSelection=false;}
public boolean displayOppClose{get;set;}{displayOppClose=false;}
Map<String,CS_OpportunityWinnerCompetitor__c> owcMap = new Map<String,CS_OpportunityWinnerCompetitor__c>();


    public VFC_OpportunityLost(ApexPages.StandardController controller) {
            
           
           Opportunity oppRec = (Opportunity)controller.getRecord();
           ID oppID=oppRec.id;
           System.debug('----->>>>'+oppID);
               this.oppt = [select id,Name,StageName,Amount,Status__c,Reason__c,(SELECT Name ,winner__c,CompetitorName__c,Price__c,Comments__c FROM Competitors__r) from Opportunity where id =:oppId];
            
           for(CS_OpportunityWinnerCompetitor__c owc: CS_OpportunityWinnerCompetitor__c.getAll().Values()){
               owcMap.put(owc.Status__c ,owc);
           }
           
            
    }
    public PageReference pageLoad()
   {     
         
        //lets say it have to retain
        String st=System.Label.CLSEP12SLS46;
        Set<string> stSet= new Set<String>();
        for(String s:st.split(',')){
               stSet.add(s);
        }
       if(!stSet.contains(this.oppt.StageName)){
       
             if(system.currentpagereference().getParameters().get('status') != NULL)
            {
                oppt.Status__c=system.currentpagereference().getParameters().get('status');
            }
            
            if(system.currentpagereference().getParameters().get('reason') != NULL)
            {
                oppt.Reason__c=system.currentpagereference().getParameters().get('reason');
            } 
            if(system.currentpagereference().getParameters().get('amount') != NULL)
            {
                Decimal d=Decimal.valueOf(system.currentpagereference().getParameters().get('amount'));
                oppt.Amount=d;
            }     
            
            boolean tempWinner=false;    
            if(oppt.Status__c ==Label.CLSEP12SLS14)
            {
                for(OPP_OpportunityCompetitor__c comp:oppt.Competitors__r)
                if(comp.winner__c)
                tempWinner=true;
                
                if(!tempWinner)        
                displayCompitetorSelection=true;              
                else
                return saveopp();
            } 
              
       }
       else{
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.label.CLSEP12SLS45)); 
           displayOppClose =  true;
       }
        
        return null;
    }
    public PageReference saveopp()
    {
        try
        {
            boolean tempWinner=false;
            if(this.oppt.Status__c==Label.CLSEP12SLS14 )
            {
                if(oppt.Reason__c != null){
                
                    for(OPP_OpportunityCompetitor__c comp:this.oppt.Competitors__r)
                    if(comp.winner__c)
                    tempWinner=true;
                    
                    if(!tempWinner)        
                    displayCompitetorSelection=true;
                }
                else{
                    update this.oppt;
                   
                
                }  
            }
            else{
                
                if(owcMap.containsKey(oppt.Status__c)){
                    oppt.StageName = owcMap.get(oppt.Status__c).Stage__c;
                }
                System.debug('*******************'+oppt.Status__c);
                try
                {
                    update oppt;
                    return new PageReference(Site.getPathPrefix()+'/'+oppt.id);
                    if(Test.isRunningTest()){
                        throw new TestException();
                   }
                }
                catch(Exception ex)
                {
                    System.debug('******************* Catch Block *********************'+ex);
                    ApexPages.addMessages(ex);
                    return null;
                }
                
            }
            if(tempWinner){
                if(owcMap.containsKey(oppt.Status__c)){
                    oppt.StageName = owcMap.get(oppt.Status__c).Stage__c;
                }
                update oppt;
                return new PageReference(Site.getPathPrefix()+'/'+oppt.id);
            }
            else{
                 if(owcMap.containsKey(oppt.Status__c)){
                    oppt.StageName = owcMap.get(oppt.Status__c).Stage__c;
                }
            Savepoint sp = Database.setSavepoint();
            Database.SaveResult sr=Database.update(oppt,false);         
            if(!sr.isSuccess())        
            compexceptionmessage= sr.getErrors()[0].getMessage();
            
            Database.rollback(sp);
            
            return  null;
          }
            
        }
        catch(Exception ex)
        {
            
            ApexPages.addMessages(ex);
            return null;
        }
        //return null;
    }
    public PageReference saveWinningCompetitors()
    {
        PageReference  pref=null;
        List<OPP_OpportunityCompetitor__c> allComps=this.oppt.Competitors__r;
        if(allComps!=null && allComps.size()>0)
        {
            try{
                Boolean ischeck =false;
                if(allComps != null && allComps.size()>0){
                    for(OPP_OpportunityCompetitor__c oppcomp:allComps){
                        if(oppcomp.Winner__c)
                        ischeck = true;
                    }
                }
                if(ischeck ){
                    //update allComps;
                    Database.SaveResult[] sr=Database.update(allComps,false); 
                    
                    for(Database.SaveResult srs:sr){
                       if(!srs.isSuccess()){
                           Database.Error err = srs.getErrors()[0];
                           compexceptionmessage =srs.getErrors()[0].getMessage();
                          }
                          else{
                              return saveopp();
                            //displayCompitetorSelection=false;
                        }
                            
                    }
                    
                    
                }
                else{
                    compexceptionmessage =System.Label.CLSEP12SLS44;
                   // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please select a compitetors')); 
                }
                
                if(Test.isRunningTest()){
                    throw new TestException();
                }
            }
            catch(Exception ex)
            {
              compexceptionmessage=ex+'';  
              displayCompitetorSelection=true;            
            }
            
        }  
        return pref;      
    }
    
    public PageReference newOpportunityCompetitor()
    {      
        String url='';
        url=Site.getPathPrefix()+'/apex/VFP_CompetitorSearch?'+Label.CL00515+'='+this.oppt.Name+'&'+Label.CL00452+'='+this.oppt.id+'&amount='+this.oppt.Amount+'&sfdc.override=1&sourcePage=OpptyLost';
        PageReference  pref = new PageReference(url); 
        pref.getParameters().put( 'status', this.oppt.Status__c);
        pref.getParameters().put( 'reason', this.oppt.Reason__c);
        //pref.getParameters().put( 'saveURL', '/apex/VFP_OpportunityLost?id='+this.oppt.id+'&status='+this.oppt.Status__c+'&reason='+this.oppt.Reason__c+'&amount='+this.oppt.Amount);  
        pref.getParameters().put( 'retURL',Site.getPathPrefix()+'/apex/VFP_OpportunityLost?id='+this.oppt.id+'&status='+this.oppt.Status__c+'&reason='+this.oppt.Reason__c+'&amount='+this.oppt.Amount); 
        System.debug('---->>>> '+pref.getUrl() );
        return pref ;           
    }

    //BR-9556 - Added Default Competitor functinality - June 2016 Release - START
     public PageReference newDefaultOpportunityCompetitor()
    {      
        String url='';
        url=Site.getPathPrefix()+'/apex/VFP_AddDefaultOpportunityCompetitor?oppID='+this.oppt.id+'&amount='+this.oppt.Amount+'&sfdc.override=1&sourcePage=OpptyLost';
        PageReference  pref = new PageReference(url);
        pref.getParameters().put( 'status', this.oppt.Status__c);
        pref.getParameters().put( 'reason', this.oppt.Reason__c);
        System.debug('---->>>> '+pref.getUrl() );
        return pref ;           
    }
    //BR-9556 - Added Default Competitor functinality - June 2016 Release - END
    public void cancelWinningCompetitors()
    {
        displayCompitetorSelection=false;       
    }
    public class TestException extends Exception {}   
    
}