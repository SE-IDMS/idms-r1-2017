/*
 * Test class of IDMSCheckPicklistValues class 
 * */

@isTest
public class IDMSCheckPicklistValuesTest{
    
    //This method test the whole checkPicklistValues methods.
    static testMethod void IDMSCheckPicklistValuesTestMethod() {
        String CompCountry = 'testcompctry';
        String CompanyState = 'Testcompst';
        String classLevel1 = 'Testclslev1';
        String classLevel2 = 'Testclslev2';
        String IDMSCompanyMarketServed = 'Testcmpmarser';
        String IDMSCompanyNbrEmployees = 'Testcomnbremp';
        String jobFunction = 'Testjbfnct';
        String jobTitle = 'Testjbttl';
        String PreferedLang = 'Testpreflang';
        String Salutation = 'Testsal';
        String UserCountry = 'Testctry';
        String UserState = 'Testuserst';
        Map<String, String> MapCompCountryValues= new Map<String, String>();
        Map<String, String> MapCompanyStateValues= new Map<String, String>();
        Map<String, String> MapClassLevel1Values= new Map<String, String>();
        Map<String, String> MapClassLevel2Values= new Map<String, String>();
        Map<String, String> MapIDMSCompanyMarketServedValues= new Map<String, String>();  
        Map<String, String> MapIDMSCompanyNbrEmployeesValues= new Map<String, String>();
        Map<String, String> MapJobFunctionValues= new Map<String, String>();
        Map<String, String> MapJobTitleValues= new Map<String, String>();
        Map<String, String> MapPreferedLangValues= new Map<String, String>();
        Map<String, String> MapSalutationValues= new Map<String, String>();
        Map<String, String> MapUserCountryValues= new Map<String, String>();
        Map<String, String> MapUserStateValues= new Map<String, String>(); 
        
        IDMSCheckPicklistValues.getMapJobFunctionValues();
        IDMSCheckPicklistValues.getMapJobTitleValues();
        IDMSCheckPicklistValues.getMapLanguageCode();
        IDMSCheckPicklistValues.getMapSalutation();
        IDMSCheckPicklistValues.getMapUserClassLevel1();
        IDMSCheckPicklistValues.getMapUserClassLevel2();
        IDMSCheckPicklistValues.getMapUserCompanyCountryCode();
        IDMSCheckPicklistValues.getMapUserCompanyNbrEmployees();
        IDMSCheckPicklistValues.getMapUserCompanyState();
        IDMSCheckPicklistValues.getMapUserCountryCode();
        IDMSCheckPicklistValues.getMapUserMarketServed();
        IDMSCheckPicklistValues.getMapUserState();
        IDMSCheckPicklistValues.isValidCompanyCountry(CompCountry,MapCompCountryValues);
        IDMSCheckPicklistValues.isValidCompanyState(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidIDMSClassLevel1(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidIDMSClassLevel2(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidIDMSCompanyMarketServed(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidIDMSCompanyNbrEmployees(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidJobFunction(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidJobTitle(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidPreferedLang(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidSalutation(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidUserCountry(CompanyState,MapCompanyStateValues);
        IDMSCheckPicklistValues.isValidUserState(CompanyState,MapCompanyStateValues);
        
    }
}