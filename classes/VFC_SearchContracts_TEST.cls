@isTest
private class VFC_SearchContracts_TEST 
{
    static testMethod void testSearchContracts() 
    {       
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        Account accounts = Utils_TestMethods.createAccount();
        accounts.country__c=country.id;
        insert accounts;
        
        contact contact =Utils_TestMethods.createContact(accounts.Id,'TestContact');
        contact.country__c=country.id;
        contact.CorrespLang__c='french';
        insert contact;
        
        Case cse = Utils_TestMethods.createCase(accounts.id,contact.Id,'Open');
        Insert cse;
        
        Contract con = Utils_TestMethods.createContract(accounts.id,contact.Id);
        Insert con;
        con.status = 'Activated';
        update con;  
        
        CTR_ValueChainPlayers__c cvp = Utils_TestMethods.createCntrctValuChnPlyr(con.Id);
        cvp.account__c=accounts.id;
        insert cvp;     
         
         
        CTR_ValueChainPlayers__c cvp1 = Utils_TestMethods.createCntrctValuChnPlyr(con.Id);
        cvp1.account__c=accounts.id;
        insert cvp1; 
        
        CTR_ValueChainPlayers__c p = new CTR_ValueChainPlayers__c();        
        Datatemplate__c d = new Datatemplate__c();
        
        test.startTest();     
        PageReference pageRef= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
        ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '12342');
     
                
        ApexPages.StandardController SContoller = new ApexPages.StandardController(p);        
        VFC_SearchContracts  SearchController = new VFC_SearchContracts(SContoller);
        SearchController.newValueChainPlayer = cvp;
        SearchController.clearFilters();    
        d.field7__c = cvp.contract__c;
        d.field8__c = cvp.Account__c;
        d.field9__c = cvp.contact__c;
        d.field10__c = 'Test Contact';
        d.field5__c = 'Test';
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();                         
        SearchController.searchContract();         
        SearchController.PerformAction(d,basecontroller );  
            
       CTR_ValueChainPlayers__c p1 = new CTR_ValueChainPlayers__c();    
        Datatemplate__c d1 = new Datatemplate__c();   
        PageReference pageRef1= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
       // ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '12342');
     
                
        ApexPages.StandardController SContoller1 = new ApexPages.StandardController(p1);        
        VFC_SearchContracts  SearchController1 = new VFC_SearchContracts(SContoller1);
        SearchController1.newValueChainPlayer = cvp;
        SearchController1.clearFilters();    
        d1.field7__c = cvp.contract__c;
        d1.field8__c = cvp.Account__c;
        d1.field9__c = cvp.contact__c;
        d1.field10__c = 'Test Contact';
        d1.field5__c = 'Test';
        VFC_ControllerBase basecontroller1 = new VFC_ControllerBase();                         
        SearchController1.searchContract();         
        SearchController1.PerformAction(d1,basecontroller1 ); 
          
         CTR_ValueChainPlayers__c p2 = new CTR_ValueChainPlayers__c();    
        Datatemplate__c d2 = new Datatemplate__c();   
        PageReference pageRef2= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
        ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        //ApexPages.currentPage().getParameters().put('PINNumber', '12342');
     
                
        ApexPages.StandardController SContoller2 = new ApexPages.StandardController(p2);        
        VFC_SearchContracts  SearchController2 = new VFC_SearchContracts(SContoller2);
        SearchController2.newValueChainPlayer = cvp;
        SearchController2.clearFilters();    
        d2.field7__c = cvp.contract__c;
        d2.field8__c = cvp.Account__c;
        d2.field9__c = cvp.contact__c;
        d2.field10__c = 'Test Contact';
        d2.field5__c = 'Test';
        VFC_ControllerBase basecontroller2 = new VFC_ControllerBase();                         
        SearchController2.searchContract();         
        SearchController2.PerformAction(d2,basecontroller2 );      
        
        CTR_ValueChainPlayers__c p3 = new CTR_ValueChainPlayers__c();    
        Datatemplate__c d3 = new Datatemplate__c();   
        PageReference pageRef3= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef3);
         ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        //ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
        ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '12342');
     
                
        ApexPages.StandardController SContoller3 = new ApexPages.StandardController(p3);        
        VFC_SearchContracts  SearchController3 = new VFC_SearchContracts(SContoller3);
        SearchController3.newValueChainPlayer = cvp;
        SearchController3.clearFilters();    
        d3.field7__c = cvp.contract__c;
        d3.field8__c = cvp.Account__c;
        d3.field9__c = cvp.contact__c;
        d3.field10__c = 'Test Contact';
        d3.field5__c = 'Test';
        VFC_ControllerBase basecontroller3 = new VFC_ControllerBase();                         
        SearchController3.searchContract();         
        SearchController3.PerformAction(d3,basecontroller3);      
              
              
         CTR_ValueChainPlayers__c p4= new CTR_ValueChainPlayers__c();    
        Datatemplate__c d4 = new Datatemplate__c();   
        PageReference pageRef4= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef4);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
       //ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '');
     
                
        ApexPages.StandardController SContoller4 = new ApexPages.StandardController(p4);        
        VFC_SearchContracts  SearchController4 = new VFC_SearchContracts(SContoller4);
        SearchController4.newValueChainPlayer = cvp;
        SearchController4.clearFilters();    
        d4.field7__c = cvp.contract__c;
        d4.field8__c = cvp.Account__c;
        d4.field9__c = cvp.contact__c;
        d4.field10__c = 'Test Contact';
        d4.field5__c = 'Test';
        VFC_ControllerBase basecontroller4 = new VFC_ControllerBase();                         
        SearchController4.searchContract();         
        SearchController4.PerformAction(d4,basecontroller4); 
        
        CTR_ValueChainPlayers__c p5= new CTR_ValueChainPlayers__c();    
        Datatemplate__c d5 = new Datatemplate__c();   
        PageReference pageRef5= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef5);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        //ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
        ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '');
     
                
        ApexPages.StandardController SContoller5 = new ApexPages.StandardController(p5);        
        VFC_SearchContracts  SearchController5 = new VFC_SearchContracts(SContoller5);
        SearchController5.newValueChainPlayer = cvp;
        SearchController5.clearFilters();    
        d5.field7__c = cvp.contract__c;
        d5.field8__c = cvp.Account__c;
        d5.field9__c = cvp.contact__c;
        d5.field10__c = 'Test Contact';
        d5.field5__c = 'Test';
        VFC_ControllerBase basecontroller5 = new VFC_ControllerBase();                         
        SearchController5.searchContract();         
        SearchController5.PerformAction(d5,basecontroller5);
        
        CTR_ValueChainPlayers__c p6= new CTR_ValueChainPlayers__c();    
        Datatemplate__c d6 = new Datatemplate__c();   
        PageReference pageRef6= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef6);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        //ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
        // ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '12342');
     
                
        ApexPages.StandardController SContoller6 = new ApexPages.StandardController(p6);        
        VFC_SearchContracts  SearchController6 = new VFC_SearchContracts(SContoller6);
        SearchController6.newValueChainPlayer = cvp;
        SearchController6.clearFilters();    
        d6.field7__c = cvp.contract__c;
        d6.field8__c = cvp.Account__c;
        d6.field9__c = cvp.contact__c;
        d6.field10__c = 'Test Contact';
        d6.field5__c = 'Test';
        VFC_ControllerBase basecontroller6 = new VFC_ControllerBase();                         
        SearchController6.searchContract();         
        SearchController6.PerformAction(d6,basecontroller6);  
        
        CTR_ValueChainPlayers__c p7= new CTR_ValueChainPlayers__c();    
        Datatemplate__c d7 = new Datatemplate__c();   
        PageReference pageRef7= Page.VFP_SearchContracts;       
        Test.setCurrentPage(pageRef7);
        ApexPages.currentPage().getParameters().put('caseId', cse.Id);   
        ApexPages.currentPage().getParameters().put('accountid', accounts.Id);
         ApexPages.currentPage().getParameters().put('contactid', contact.Id); 
        ApexPages.currentPage().getParameters().put('PINNumber', '12342');
     
                
        ApexPages.StandardController SContoller7 = new ApexPages.StandardController(p7);        
        VFC_SearchContracts  SearchController7 = new VFC_SearchContracts(SContoller7);
        SearchController7.newValueChainPlayer = cvp;
        SearchController7.clearFilters();    
        d7.field7__c = cvp.contract__c;
        d7.field8__c = cvp.Account__c;
        d7.field9__c = cvp.contact__c;
        d7.field10__c = 'Test Contact';
        d7.field5__c = 'Test';
        VFC_ControllerBase basecontroller7 = new VFC_ControllerBase();                         
        SearchController7.searchContract(); 
       
        SearchController7.PerformAction(d7,basecontroller7);       
        
        
               
        test.stopTest();
        
    }
}