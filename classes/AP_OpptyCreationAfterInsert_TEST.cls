@isTest
Public class AP_OpptyCreationAfterInsert_TEST 
{   
    static testMethod void testATT()
    {   
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        Country__c ctry = new Country__c(Name ='Test',CountryCode__c = 'TS');
        insert ctry;
        SVMXC__Service_Order__c wo = Utils_TestMethods.createWorkOrder(acc.Id);
        //insert wo;
        OpportunityNotification__c oppnot =New OpportunityNotification__c(Name='Test',
                                                                           Tech_CreatedFromIB__c=true,
                                                                           AccountForConversion__c=acc.Id,
                                                                           LeadingBusinessBU__c='energy',
                                                                           status__c='t',
                                                                           Amount__c=5000.00,
                                                                           CloseDate__c=date.today(),
                                                                           Description__c='testing',
                                                                           CountryDestination__c=ctry.Id,
                                                                           WorkOrder__c=wo.Id,
                                                                           TechCreatedFromSC__c=false
                                                                            );
        insert  oppnot; 
        
        InstalledProductOnOpportunityNotif__c  ipopn = new InstalledProductOnOpportunityNotif__c();
        ipopn.Tech_CreatedFromIpSFM__c = true;
        ipopn.OpportunityNotification__c = oppnot.id;
        insert ipopn;
        
        // commented by Hari
        /*
        
    
    Opportunity opp = new Opportunity();
    opp.Name= oppnot.Name;
    opp.AccountId = oppnot.AccountForConversion__r.id;
    opp.CloseDate = oppnot.CloseDate__c;
    opp.LeadingBusiness__c = oppnot.LeadingBusinessBU__c;
    opp.Description = oppnot.Description__c;
    opp.OpportunityNotification__c = oppnot.Id;
    opp.CountryOfDestination__c = ctry.Id;
    opp.StageName = 'test';
    insert opp; 
    */
    }
}