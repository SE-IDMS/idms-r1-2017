/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: Below class and method are generated from WSDL
*                This below method are user to connect UIMS Company Manager 
**/ 


public class WS_IDMSUIMSAuthCompanyServiceImpl{

    public class CompanyManager_UIMSV2_ImplPort {
        public String endpoint_x = 'https://ims-int.btsec.dev.schneider-electric.com/IMS-CompanyManager/UIMSV2/CompanyManagement';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://uimsv2.impl.service.ims.schneider.com/', 'WS_IDMSUIMSAuthCompanyServiceImpl', 'http://uimsv2.service.ims.schneider.com/', 'WS_IDMSUIMSAuthCompanyService'};
        // Removes Primary Contact from UIMS
        public Boolean removePrimaryContact(String callerFid,String samlAssertion) {
            WS_IDMSUIMSAuthCompanyService.removePrimaryContact request_x = new WS_IDMSUIMSAuthCompanyService.removePrimaryContact();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            WS_IDMSUIMSAuthCompanyService.removePrimaryContactResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.removePrimaryContactResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.removePrimaryContactResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'removePrimaryContact',
              'http://uimsv2.service.ims.schneider.com/',
              'removePrimaryContactResponse',
              'WS_IDMSUIMSAuthCompanyService.removePrimaryContactResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        // Get Company Detail from UIMS
        public WS_IDMSUIMSAuthCompanyService.companyV3 getCompany(String callerFid,String samlAssertionOrToken,String federatedId) {
            WS_IDMSUIMSAuthCompanyService.getCompany request_x = new WS_IDMSUIMSAuthCompanyService.getCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertionOrToken = samlAssertionOrToken;
            request_x.federatedId = federatedId;
            WS_IDMSUIMSAuthCompanyService.getCompanyResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.getCompanyResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.getCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'getCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'getCompanyResponse',
              'WS_IDMSUIMSAuthCompanyService.getCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.company;
        }
        // Reject Company Merge in UIMS
        public Boolean rejectCompanyMerge(String callerFid,String authentificationToken,String federatedId) {
            WS_IDMSUIMSAuthCompanyService.rejectCompanyMerge request_x = new WS_IDMSUIMSAuthCompanyService.rejectCompanyMerge();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            request_x.federatedId = federatedId;
            WS_IDMSUIMSAuthCompanyService.rejectCompanyMergeResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.rejectCompanyMergeResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.rejectCompanyMergeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'rejectCompanyMerge',
              'http://uimsv2.service.ims.schneider.com/',
              'rejectCompanyMergeResponse',
              'WS_IDMSUIMSAuthCompanyService.rejectCompanyMergeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        // Update Company Detail in UIMS
        public Boolean updateCompany(String callerFid,String samlAssertion,String federatedId,WS_IDMSUIMSAuthCompanyService.companyV3 company) {
            WS_IDMSUIMSAuthCompanyService.updateCompany request_x = new WS_IDMSUIMSAuthCompanyService.updateCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.federatedId = federatedId;
            request_x.company = company;
            WS_IDMSUIMSAuthCompanyService.updateCompanyResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.updateCompanyResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.updateCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'updateCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'updateCompanyResponse',
              'WS_IDMSUIMSAuthCompanyService.updateCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        //Accept Company Merge in UIMS
        public Boolean acceptCompanyMerge(String callerFid,String authentificationToken,String federatedId) {
            WS_IDMSUIMSAuthCompanyService.acceptCompanyMerge request_x = new WS_IDMSUIMSAuthCompanyService.acceptCompanyMerge();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            request_x.federatedId = federatedId;
            WS_IDMSUIMSAuthCompanyService.acceptCompanyMergeResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.acceptCompanyMergeResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.acceptCompanyMergeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'acceptCompanyMerge',
              'http://uimsv2.service.ims.schneider.com/',
              'acceptCompanyMergeResponse',
              'WS_IDMSUIMSAuthCompanyService.acceptCompanyMergeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        // Search Company in UIMS
        public WS_IDMSUIMSAuthCompanyService.companyV3[] searchCompany(String callerFid,String samlAssertion,WS_IDMSUIMSAuthCompanyService.companyV3 template) {
            WS_IDMSUIMSAuthCompanyService.searchCompany request_x = new WS_IDMSUIMSAuthCompanyService.searchCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.template = template;
            WS_IDMSUIMSAuthCompanyService.searchCompanyResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.searchCompanyResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.searchCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'searchCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'searchCompanyResponse',
              'WS_IDMSUIMSAuthCompanyService.searchCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.companies;
        }
        // Create Company in UIMS
        public String createCompany(String callerFid,String samlAssertion,WS_IDMSUIMSAuthCompanyService.companyV3 company) {
            WS_IDMSUIMSAuthCompanyService.createCompany request_x = new WS_IDMSUIMSAuthCompanyService.createCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.company = company;
            WS_IDMSUIMSAuthCompanyService.createCompanyResponse response_x;
            Map<String, WS_IDMSUIMSAuthCompanyService.createCompanyResponse> response_map_x = new Map<String, WS_IDMSUIMSAuthCompanyService.createCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'createCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'createCompanyResponse',
              'WS_IDMSUIMSAuthCompanyService.createCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.federatedId;
        }
    }
}