/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 05-October-2012
    Modification Log    : 
    Description         : Connect Target Tool Report
*/

public with sharing class ConnectTargetDashboardController{


public ConnectTargetDashboardController(ApexPages.StandardController controller){

   
    
}

Public string getFin(){
return(Fin);
}

Public String Fin{get;set;}
 
    public String getYears() {
        return Years;
    }

    
    
    public ConnectTargetDashboardController() {
    Entity = Label.ConnectCorporateKPIGlobal;
    Quarter = Label.Connect_Quarter;
     Years = Label.ConnectYear;
    }
 
 public string RenPDF{get; set;}
 public string Entity{get;set;}
 public string UserId;
 public string Quarter {get; set;}
 public string Years{get; set;}
 public List<Id> lst = new List<Id>();
 
 List<KPILst> CE = new List<KPILst>();
 List<KPILst> CC = new List<KPILst>();
 List<KPILst> CEF = new List<KPILst>();
 List<KPILst> CEF2 = new List<KPILst>();
 List<KPILst> CP = new List<KPILst>();
 List<KPILst> Overall1 = new List<KPILst>();


 public pagereference FilterChange(){
     
     return null;
    
    }
    
 //public List<id> getlst()
//{
 //userId = UserInfo.getUserId();
 //for(Global_KPI_Targets__Share kpi : [Select parentid,UserOrGroupId from Global_KPI_Targets__Share where UserOrGroupId = :userid]){
 //lst.add(kpi.parentid);
// }
 //return(lst);
 //}
 
public pagereference QuarterChange(){
     
     return null;
    
    }
    
 public List<SelectOption> getFilterList() {
       List<SelectOption> options2013 = new List<SelectOption>();
       List<SelectOption> options2014 = new List<SelectOption>();
       options2013.add(new SelectOption('Global','Global'));
       options2013.add(new SelectOption('China','China Operations')); 
       options2013.add(new SelectOption('Buildings','Cities - Buildings'));   
       options2013.add(new SelectOption('Energy','Cities - Energy'));  
       options2013.add(new SelectOption('Professional Services','Cities - Professional Services')); 
       options2013.add(new SelectOption('Smart Infrastructure','Cities - Smart Infrastructure'));  
       options2013.add(new SelectOption('Finance','Finance')); 
       options2013.add(new SelectOption('GM','GM'));
       options2013.add(new SelectOption('Global Operations','Global Operations'));     
       options2013.add(new SelectOption('GSC-Global','GSC-Global'));  
       //options2013.add(new SelectOption('GSC China','GSC-Regional China'));    
       //options2013.add(new SelectOption('EAJP','GSC-Regional EAJP'));  
       //options2013.add(new SelectOption('EMEA Products','GSC-Regional EMEA Products'));  
       //options2013.add(new SelectOption('EMEA Lifespace','GSC-Regional EMEA Lifespace'));  
       //options2013.add(new SelectOption('EMEA Equipment','GSC-Regional EMEA Equipment'));     
       //options2013.add(new SelectOption('India','GSC-Regional India'));  
       //options2013.add(new SelectOption('GSC North America','GSC-Regional North America'));  
       //options2013.add(new SelectOption('South America','GSC-Regional South America'));        
       options2013.add(new SelectOption('HR','HR')); 
       options2013.add(new SelectOption('IPO','IPO')); 
       options2013.add(new SelectOption('Industry','Industry'));     
       options2013.add(new SelectOption('IT','IT'));  
       options2013.add(new SelectOption('North America','North America Operations'));      
       options2013.add(new SelectOption('Partner','Partner'));  
      // options2013.add(new SelectOption('Eco Business','Partner Division Eco Business'));
       //options2013.add(new SelectOption('Partner Projects','Partner Division Partner Projects'));
      // options2013.add(new SelectOption('Retail','Partner Division Retail'));
         options2013.add(new SelectOption('Strategy','Strategy'));
         
         // 2014 options
       
     
       options2014.add(new SelectOption('Global','Global'));
       options2014.add(new SelectOption('China','China Operations')); 
         options2014.add(new SelectOption('Energy','Energy')); 
       options2014.add(new SelectOption('Finance','Finance')); 
       options2014.add(new SelectOption('Global Operations','Global Operations'));   
       options2014.add(new SelectOption('Global Solutions','Global Solutions'));   
       options2014.add(new SelectOption('GM','GM'));        
       options2014.add(new SelectOption('GSC-Global','GSC-Global'));  
       //options2014.add(new SelectOption('GSC China','GSC-Regional China'));    
       //options2014.add(new SelectOption('EAJP','GSC-Regional EAJP'));  
       //options2014.add(new SelectOption('EMEA Products','GSC-Regional EMEA Products'));  
       //options2014.add(new SelectOption('EMEA Lifespace','GSC-Regional EMEA Lifespace'));  
       //options2014.add(new SelectOption('EMEA Equipment','GSC-Regional EMEA Equipment'));     
       //options2014.add(new SelectOption('India','GSC-Regional India'));  
       //options2014.add(new SelectOption('GSC North America','GSC-Regional North America'));  
       //options2014.add(new SelectOption('South America','GSC-Regional South America'));        
       options2014.add(new SelectOption('HR','HR')); 
       options2014.add(new SelectOption('IPO','IPO')); 
       options2014.add(new SelectOption('Industry','Industry'));     
       options2014.add(new SelectOption('IT','IT'));  
       options2014.add(new SelectOption('North America','North America Operations'));      
       options2014.add(new SelectOption('Partner','Partner'));  
      // options2014.add(new SelectOption('Eco Business','Partner Division Eco Business'));
      // options2014.add(new SelectOption('Partner Projects','Partner Division Partner Projects'));
       //options2014.add(new SelectOption('Retail','Partner Division Retail'));
       //options2014.add(new SelectOption('Building Division','Partner Division Building'));
       options2014.add(new SelectOption('Strategy','Strategy'));
       
       If(Years == '2013')
         return options2013;
       else
         return options2014;
      
   }
 public List<SelectOption> getFilterQuarter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('Q1','Q1'));  
       options.add(new SelectOption('Q2','Q2'));     
       options.add(new SelectOption('Q3','Q3'));  
       options.add(new SelectOption('Q4','Q4/Yearly'));       
       return options;
  }
   public List<SelectOption> getFilterYear() {
      List<SelectOption> optionYear = new List<SelectOption>();
       optionYear.add(new SelectOption('2013','2013'));  
       optionYear.add(new SelectOption('2014','2014'));               
       return optionYear;
  }
  public pagereference YearChange(){
     
    return null;
    
    }
   
    
 public pagereference RenderPDF(){
 renPDF = 'PDF';
 return null ;
 }


public List<KPILst> getConnectEverywhere()
 {
  CE = new List<KPILst>();   
 
iF(Entity == Label.ConnectCorporateKPIGlobal){
   retrieveGlobalKPIs(CE,Label.ConnectTransEverywhere);
    }
    
iF(Entity != Label.ConnectCorporateKPIGlobal){

 retrieveEntityKPIs(CE,Label.ConnectTransEverywhere);
  }
  return(CE);
  }
 
 public List<KPILst> getConnectToCustomer()
 {
 CC = new List<KPILst>();
 iF(Entity == Label.ConnectCorporateKPIGlobal){
   retrieveGlobalKPIs(CC,Label.ConnectTransCustomer);
    }
    
iF(Entity != Label.ConnectCorporateKPIGlobal){

 retrieveEntityKPIs(CC,Label.ConnectTransCustomer);
  }
  return(CC);
  }
 
 
 public List<KPILst> getConnectPeople()
 {
 CP = new List<KPILst>();
 iF(Entity == Label.ConnectCorporateKPIGlobal){
   retrieveGlobalKPIs(CP,Label.ConnectTransPeople);
    }
    
iF(Entity != Label.ConnectCorporateKPIGlobal){

 retrieveEntityKPIs(CP,Label.ConnectTransPeople);
  }
 
 return(CP);
 }
 
 public List<KPILst> getConnectforEfficiency()
 {
 CEF = new List<KPILst>();
 iF(Entity == Label.ConnectCorporateKPIGlobal){
   retrieveGlobalKPIs(CEF,Label.ConnectTransEfficiency);
    }
    
iF(Entity != Label.ConnectCorporateKPIGlobal){

 retrieveEntityKPIs(CEF,Label.ConnectTransEfficiency);
  }
  return(CEF);
  }
    
 
 public List<KPILst> getOverall()
 {
  Overall1 = new List<KPILst>();
 iF(Entity == Label.ConnectCorporateKPIGlobal){
   retrieveGlobalKPIs(Overall1, 'Connect');
    }
    
iF(Entity != Label.ConnectCorporateKPIGlobal){

 retrieveEntityKPIs(Overall1,'Connect');
  }
  return(Overall1);
  }
  
  
private void retrieveGlobalKPIs(List<KPILst> lstTransformation, string lblTransformation)
    {
    
    List<Global_KPI_Targets__c> GlobalKPI = new List<Global_KPI_Targets__c>();
    
    Fin = ApexPages.CurrentPage().getParameters().get('f');   
    
    System.debug('Print P ' + Fin);
    
    if(Fin == 'True')
        GlobalKPI = [Select id, KPI_Name__c,KPI_Acronym__c,KPI_Targets__c, Global_KPI_Target_Q1__c, Global_KPI_Target_Q2__c, Global_KPI_Target_Q3__c, Global_KPI_Actual_Q1__c,Global_KPI_Actual_Q2__c,Global_KPI_Actual_Q3__c,Global_KPI_Actual_Q4_Yearly__c, Transformation__c, Unit_of_Measure__c, OverallKPIOrder__c  from Global_KPI_Targets__c where KPI_Type__c = :Label.ConnectGlobalKPI  and KPI_Acronym__c != :Label.ConnectGrowthPriority and Year__c = :Years order by OverallKPIOrder__c];
    else
       GlobalKPI = [Select id, KPI_Name__c,KPI_Acronym__c,KPI_Targets__c, Global_KPI_Target_Q1__c, Global_KPI_Target_Q2__c, Global_KPI_Target_Q3__c, Global_KPI_Actual_Q1__c,Global_KPI_Actual_Q2__c,Global_KPI_Actual_Q3__c,Global_KPI_Actual_Q4_Yearly__c, Transformation__c, Unit_of_Measure__c, OverallKPIOrder__c  from Global_KPI_Targets__c where KPI_Type__c = :Label.ConnectGlobalKPI  and KPI_Acronym__c != :Label.ConnectGrowthPriority and Year__c = :Years and IsVisible__c = True order by OverallKPIOrder__c];
      
      
        String Target;
        String Actual;
        String Perf;
        String Dir;
        Map<string,ConnectGSCS__c> kpics = ConnectGSCS__c.getall();
     
     
        for(Global_KPI_Targets__c GKPI: GlobalKPI)
        { 
           System.Debug('Print KPI Acronym ' + GKPI.KPI_Acronym__c + '   ' +  GKPI.KPI_Name__c);
            if(GKPI.Transformation__c == lblTransformation)
            {
                KPILst lst = new KPILst();
                lst.KPIname = GKPI.KPI_Name__c;
                lst.kpiid = GKPI.id;                
              if(Quarter == 'Q4'){
                Target = GKPI.KPI_Targets__c;
                Actual = GKPI.Global_KPI_Actual_Q4_Yearly__c;
                Perf = ArrowColor(GKPI.KPI_Acronym__c, Target, Actual);
                Dir = ArrowDir(GKPI.Global_KPI_Actual_Q3__c,GKPI.Global_KPI_Actual_Q4_Yearly__c);
                lst.ac = Perf;
                lst.ad = Dir;
                }
               else
                if(Quarter == 'Q3'){
                   Target = GKPI.Global_KPI_Target_Q3__c;
                   Actual = GKPI.Global_KPI_Actual_Q3__c;
                   
                   // Check if rule apply for all quarter
                  Try{
                   if(kpics.get(GKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c == 'Yes')                  
                    Perf = ArrowColor(GKPI.KPI_Acronym__c, Target, Actual);
                  else 
                   Perf = 'Grey';
                  }Catch(Exception e){ System.debug('No Custom Setting found ' + GKPI.KPI_Acronym__c);}
                   Dir = ArrowDir(GKPI.Global_KPI_Actual_Q2__c,GKPI.Global_KPI_Actual_Q3__c);                   
                   lst.ac = Perf;
                   lst.ad = Dir;
                   }
              else
                if(Quarter == 'Q2'){
                   Target = GKPI.Global_KPI_Target_Q2__c;
                   Actual = GKPI.Global_KPI_Actual_Q2__c;                   
                   
                   // Check if rule apply for all quarter   
                   Try{  
                   System.Debug( ' Custom Setting -- Print  ' + kpics.get(GKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c);              
                   if(kpics.get(GKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c == 'Yes')                   
                    Perf = ArrowColor(GKPI.KPI_Acronym__c, Target, Actual);
                   else 
                     Perf = 'Grey';                    
                     }Catch(Exception e){System.debug('No Custom Setting found ' + GKPI.KPI_Acronym__c); }
                      Dir = ArrowDir(GKPI.Global_KPI_Actual_Q1__c,GKPI.Global_KPI_Actual_Q2__c);
                      System.debug(' Direct ..... '  + Dir);
                   lst.ac = Perf;
                   lst.ad = Dir;
                   }
              else
                if(Quarter == 'Q1'){
               
                   Target = GKPI.Global_KPI_Target_Q1__c;
                   Actual = GKPI.Global_KPI_Actual_Q1__c;
                   // Check if rule apply for all quarter
                   Try{
                   if(kpics.get(GKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c == 'Yes')                   
                    Perf = ArrowColor(GKPI.KPI_Acronym__c, Target, Actual);
                   else 
                      Perf = 'Grey'; 
                    }Catch(Exception e) {System.debug('Error no Custom Setting found ' + GKPI.KPI_Acronym__c);}                  
                   Dir = ArrowDir(GKPI.Global_KPI_Actual_Q1__c,GKPI.Global_KPI_Actual_Q1__c);
                   
                   lst.ac = Perf;
                   lst.ad = Dir;
                   
                   }
               if(Target == null)
                  Target = '';
               if(Actual == null)
                  Actual = '';
                                                  
              if(GKPI.Unit_of_Measure__c !=null){
                  lst.um = GKPI.Unit_of_Measure__c;
                  }
               lst.Target = 'Target:' + Target +  ' Actual:' + Actual;
                              
                lstTransformation.add(lst);                 
            
            }
        }
      
    }
    // Entity Targets
    
 private void retrieveEntityKPIs(List<KPILst> lstTransformation, string lblTransformation)
    {
    
List<Cascading_KPI_Target__c> EntityKPI = new List<Cascading_KPI_Target__c>();
String Target;
String Actual;
String Perf;
String Dir;
Map<string,ConnectGSCS__c> kpics = ConnectGSCS__c.getall();

If(Fin == 'True'){

if (Entity.Contains('Eco Business') || Entity.Contains('Retail') || Entity.Contains('Partner Projects') )
    
    EntityKPI = EntityKPI = [Select id, KPI_Name__c,KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c,Transformation__c,Entity__c, Unit_of_Measure__c, EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and Partner_Sub_Entity__c = :Entity  order by  EntityOverallKPIOrder__c];
 
else if (Entity.Contains('EAJP') || Entity.Contains('India') || Entity.Contains('South America')|| Entity.Contains('EMEA Products')|| Entity.Contains('EMEA Lifespace')|| Entity.Contains('EMEA Equipment') )
    EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c,Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and GSC_Region__c = :Entity  order by EntityOverallKPIOrder__c];
    
else if (Entity.Contains('Buildings') || Entity.Contains('Smart Infrastructure') || Entity.Contains('Professional Services')|| (Entity.Contains('Energy') && Years == '2013'))
     EntityKPI = EntityKPI = [Select id, KPI_Name__c,KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c,Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c, EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and Smart_Cities_Division__c = :Entity  order by EntityOverallKPIOrder__c];

else if (Entity.Contains('GSC North America'))
     EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and GSC_Region__c = :'North America'  order by EntityOverallKPIOrder__c];

else if (Entity.Contains('GSC China'))
     EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and GSC_Region__c = :'China'  order by EntityOverallKPIOrder__c];
    
else  
     EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and Entity__c = :Entity  order by EntityOverallKPIOrder__c];

}
// Non Financial KPIs
Else {

if (Entity.Contains('Eco Business') || Entity.Contains('Retail') || Entity.Contains('Partner Projects'))
    
    EntityKPI = EntityKPI = [Select id, KPI_Name__c,KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c,Transformation__c,Entity__c, Unit_of_Measure__c, EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and Partner_Sub_Entity__c = :Entity and isVisible__c = 'True' order by  EntityOverallKPIOrder__c];
 
else if (Entity.Contains('EAJP') || Entity.Contains('India') || Entity.Contains('South America')|| Entity.Contains('EMEA Products')|| Entity.Contains('EMEA Lifespace')|| Entity.Contains('EMEA Equipment') )
    EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c,Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and GSC_Region__c = :Entity and isVisible__c = 'True' order by EntityOverallKPIOrder__c];
    
else if (Entity.Contains('Buildings') || Entity.Contains('Smart Infrastructure') || Entity.Contains('Professional Services')|| Entity.Contains('Energy') && Years == '2013')
     EntityKPI = EntityKPI = [Select id, KPI_Name__c,KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c,Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c, EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and Smart_Cities_Division__c = :Entity and isVisible__c = 'True' order by EntityOverallKPIOrder__c];

else if (Entity.Contains('GSC North America'))
     EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and GSC_Region__c = :'North America' and isVisible__c = 'True' order by EntityOverallKPIOrder__c];

else if (Entity.Contains('GSC China'))
     EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and GSC_Region__c = :'China' and isVisible__c = 'True' order by EntityOverallKPIOrder__c];
    
else  
     EntityKPI = EntityKPI = [Select id, KPI_Name__c, KPI_Acronym__c,Entity_KPI_Target_Q1__c, Entity_KPI_Target_Q2__c, Entity_KPI_Target_Q3__c, Entity_KPI_Target_Q4__c, Entity_Actual_Q1__c, Entity_Actual_Q2__c, Entity_Actual_Q3__c,Entity_Actual_Q4_Yearly__c, Transformation__c,Entity__c, Unit_of_Measure__c,  EntityOverallKPIOrder__c, Smart_Cities_Division__c, Partner_Sub_Entity__c, GSC_Region__c  from Cascading_KPI_Target__c where KPI_Type__c = :Label.ConnectGlobalKPI and Year__c =:Years and Entity__c = :Entity and isVisible__c = 'True' order by EntityOverallKPIOrder__c];

}

Try{
for(Cascading_KPI_Target__c EKPI: EntityKPI)
   {
      KPILst lst = new KPILst();
      if(EKPI.Transformation__c == lblTransformation)
         {
          lst.kpiName = EKPI.KPI_Name__c;
          lst.kpiid = EKPI.Id;
                    
                   
                    if(Quarter == 'Q1'){
                        Target = EKPI.Entity_KPI_Target_Q1__c;
                        Actual = EKPI.Entity_Actual_Q1__c;
                     Try{
                    // Check if rule apply for all quarter
                   if(kpics.get(EKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c == 'Yes')                   
                    Perf = ArrowColor(EKPI.KPI_Acronym__c, Target, Actual);
                   else 
                    Perf = 'Grey';    
                    }Catch(Exception e) {System.debug('Error ' + e);}               
                        Dir = ArrowDir(EKPI.Entity_Actual_Q1__c,EKPI.Entity_Actual_Q1__c);
                        lst.ac = Perf;
                        lst.ad = Dir;
                        }
                    else if(Quarter == 'Q2'){
                        Target = EKPI.Entity_KPI_Target_Q2__c;
                        Actual = EKPI.Entity_Actual_Q2__c;
                        // Check if rule apply for all quarter
                   Try{
                   if(kpics.get(EKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c == 'Yes')                   
                    Perf = ArrowColor(EKPI.KPI_Acronym__c, Target, Actual);
                   else 
                    Perf = 'Grey';    
                    }Catch(Exception e) {System.debug('Error ' + e);}                      
                        Dir = ArrowDir(EKPI.Entity_Actual_Q1__c,EKPI.Entity_Actual_Q2__c);
                        lst.ac = Perf;
                        lst.ad = Dir;
                        }
                    else if(Quarter == 'Q3'){
                        Target = EKPI.Entity_KPI_Target_Q3__c;
                        Actual = EKPI.Entity_Actual_Q3__c;
                        
                      Try{
                        // Check if rule apply for all quarter
                   if(kpics.get(EKPI.KPI_Acronym__c + '_' + Years).All_Quarter__c == 'Yes')                   
                    Perf = ArrowColor(EKPI.KPI_Acronym__c, Target, Actual);
                   else 
                    Perf = 'Grey';  
                    }Catch(Exception e) {System.debug('Error ' + e);}                        
                        Dir = ArrowDir(EKPI.Entity_Actual_Q2__c,EKPI.Entity_Actual_Q3__c);
                        lst.ac = Perf;
                        lst.ad = Dir;
                        }
                    else if(Quarter == 'Q4'){
                        Target = EKPI.Entity_KPI_Target_Q4__c;
                        Actual = EKPI.Entity_Actual_Q4_Yearly__c;
                        Perf = ArrowColor(EKPI.KPI_Acronym__c, Target, Actual);                                     
                        Dir = ArrowDir(EKPI.Entity_Actual_Q3__c,EKPI.Entity_Actual_Q4_Yearly__c);
                        lst.ac = Perf;
                        lst.ad = Dir;
                        }
                     if(Target == null)
                        Target = '';
                     if(Actual == null)
                       Actual = '';
                          
                    if(EKPI.Unit_of_Measure__c != null)
                    {
                       lst.um = EKPI.Unit_of_Measure__c;
                     }
                        
                        lst.Target = 'Target: ' + Target + ' Actual: ' + Actual;
                   
                    lstTransformation.add(lst);     
                }
         }   
         }Catch(Exception e)
          {
            System.Debug('Error :' + e);
           }   
        
    }
    
    // Get Arrow Color
    
   Public String ArrowColor(String Acr, String Target, String Actual)
   {
     String ACL;
     decimal Msr;
     decimal Act;
     decimal Tar;
  Try{
     if (Actual == null) 
        Act = 0;
     else 
       Act = Decimal.Valueof(Actual);
       
     if (Target == null)
        Tar = 0;
     else
        Tar = Decimal.Valueof(Target);
     
     }Catch(Exception e){
       System.debug('Error ' + e);
      }
     
    Map<string,ConnectGSCS__c> kpim = ConnectGSCS__c.getall();
    
    Acr = Acr + '_' + Years;
    System.debug('Testing ' + Acr);
  
  Try{
    if (kpim.get(Acr).Measure_Type__c == 'Percentage' && Tar != 0)
      {
       System.Debug( 'Actual ' + String.ValueOf(Act) + 'Target ' +  String.ValueOf(Tar));
       Msr =  (Act / Tar) * 100;
       System.debug('Measure .... ' + Acr + '  ' + Msr + kpim.get(Acr).Green__c + kpim.get(Acr).Yellow__c + kpim.get(Acr).Red__c );
      If (Msr >= kpim.get(Acr).Green__c)
        ACL = 'Green';
      Else 
        If (Msr < kpim.get(Acr).Green__c && Msr >= kpim.get(Acr).Yellow__c)
         ACL = 'Yellow';
      Else
       If (Msr < kpim.get(Acr).Red__c)
         ACL = 'Red';
       }
       
    Else
       if (kpim.get(Acr).Measure_Type__c == 'Number')
       {
          if (Act >= Tar) 
              ACL = 'Green';
          else
            if ( (Tar - kpim.get(Acr).Yellow__c) < = Act && Tar > Act)
              ACL = 'Yellow';
          else
             if(Act < Tar - kpim.get(Acr).Red__c)
               ACL = 'Red';
         }
    } Catch (Exception e){
       System.debug('Error ' + e);
       }
       if (ACL == null)
          ACL = 'Grey';
        
        system.debug('Arrow color ' + ACL);
          
    return(ACL);
   }
 
 
  // Get Arrow Direction
  
  Public String ArrowDir(String QtrOne, String QtrTwo)
  {
    decimal FQ;
    decimal SQ;
    String AD;
    
    Try{
    
    if(QtrOne == null)
      FQ = null;
    else
      FQ = Decimal.ValueOf(QtrOne);
   
    if(QtrTwo == null)
      SQ = null;
     else 
      SQ = Decimal.ValueOf(QtrTwo);
     }Catch(Exception e){System.Debug('Error ' + e);}
    
    if (SQ != null && FQ !=null){
      if (SQ < FQ)
         AD = 'Down';
      else
        if(SQ > FQ)
          AD = 'Up';
      else
        if(SQ == FQ)
          AD = 'LFT';
      }    
     else
        AD = 'LFT';
    return(AD);
    }
 
 Public Class KPILst{
 public id kpiid{get;set;}
 public string KPIname{get;set;}
 public string Target{get;set;}
 public string um{get;set;}
 public string ac{get;set;}
 public string ad{get;set;}
 }
 
   }