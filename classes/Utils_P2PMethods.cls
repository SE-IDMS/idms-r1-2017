public class Utils_P2PMethods{
    public static Map<ID,EntityStakeholder__c[]> retrieveOrganizationUserDetails(Map<ID,String> orgRoleMap){
        //*********************************************************************************
        // Method Name : retrieveOrganizationUserDetails when a Org 
        // Purpose : To  retrieve Orgnaization StakeHolder based on Org Id and Role.
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 18th Augest 2011
        // Modified by :
        // Date Modified :
        // Remarks : For Oct - 11 Release
        ///********************************************************************************/  
        
        System.Debug('****Utils_P2PMethods retrieveOrganizationUserDetails Started ****');
          
        Map<ID,String>  orgSpecificRoleMap = new Map<Id,String>();
        Set<Id> orgAllRoleID = new Set<ID>();
        List<EntityStakeholder__c> entityStakeHolderList = new List<EntityStakeholder__c>();
        Map<ID,EntityStakeholder__c[]>  stakeholderMap = new Map<Id,EntityStakeholder__c[]>();
        if(!orgRoleMap.isEmpty()){
            entityStakeHolderList = [Select Id,Name,BusinessRiskEscalationEntity__c,Role__c,User__c 
                     from EntityStakeholder__c where BusinessRiskEscalationEntity__c In: orgRoleMap.Keyset() ];          
    
            for(EntityStakeholder__c entityStakeHolder: entityStakeHolderList ){
                if(stakeholderMap.containsKey(entityStakeHolder.BusinessRiskEscalationEntity__c)){
                    if((orgRoleMap.get(entityStakeHolder.BusinessRiskEscalationEntity__c)==entityStakeHolder.Role__c) || (orgRoleMap.get(entityStakeHolder.BusinessRiskEscalationEntity__c)=='ALL')){
                        stakeholderMap.get(entityStakeHolder.BusinessRiskEscalationEntity__c).add(entityStakeHolder);    
                    }
                }
                else{
                    if((orgRoleMap.get(entityStakeHolder.BusinessRiskEscalationEntity__c)==entityStakeHolder.Role__c) || (orgRoleMap.get(entityStakeHolder.BusinessRiskEscalationEntity__c)=='ALL')){
                        List<EntityStakeholder__c> OrgEntityList = new List<EntityStakeholder__c>();
                        OrgEntityList.add(entityStakeHolder);
                        stakeholderMap.put(entityStakeHolder.BusinessRiskEscalationEntity__c,OrgEntityList);
                    }
                }
            }
        }
        System.Debug('****Utils_P2PMethods retrieveOrganizationUserDetails Finished ****');
        return stakeholderMap;
    }
    public static Problem__Share createProblemShare(Id ParentId,Id UserOrGroupId,String strSharingReason){
        //*********************************************************************************
        // Method Name      : createProblemShare
        // Purpose          : To create the Object for Team Share with required information
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 25th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/

        System.Debug('****AP1000_Problem createProblemShare  Started****');

        Problem__Share tempPrblmShare = new Problem__Share();
        tempPrblmShare.ParentId = ParentId;
        tempPrblmShare.UserOrGroupId = UserOrGroupId;
        tempPrblmShare.AccessLevel = 'Edit';
        //tempPrblmShare.RowCause = 'Manual';
        tempPrblmShare.RowCause = strSharingReason;
        System.Debug('****AP1000_Problem createProblemShare  Finished****');
        return tempPrblmShare;
    }
    public static void insertPrblmShare(List<Problem__Share> prblmShareList){
        //*********************************************************************************
        // Method Name      : insertPrblmShare
        // Purpose          : To create Sharing for Problem Team Member when Problem Team Member Record is created 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 19th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****Utils_P2PMethods insertPrblmShare  Started****');
        
        if(prblmShareList.Size() > 0){
            if(prblmShareList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                 System.Debug('######## AP1000-Share Error Inserting : '+ Label.CL00264);
            }
            else{
                Database.SaveResult[] prblmShareInsertResult = Database.insert(prblmShareList, false);
                for(Database.SaveResult sr: prblmShareInsertResult){
                    if(!sr.isSuccess()){
                        Database.Error err = sr.getErrors()[0];
                        System.Debug('######## AP1000-Share Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                    }
                }
            }
        }   
        System.Debug('****Utils_P2PMethods insertPrblmShare  Finished****');
    }
    public static void deletePrblmShare(List<Problem__Share> prblmShareList){
        //*********************************************************************************
        // Method Name      : deletePrblmShare
        // Purpose          : To create Sharing for Problem Team Member when Problem Team Member Record is created 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 19th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****Utils_P2PMethods deletePrblmShare  Started****');
        
        if(prblmShareList.Size() > 0){
            if(prblmShareList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                 System.Debug('######## AP1000-Share Error Inserting : '+ Label.CL00264);
            }
            else{
                Database.DeleteResult[] prblmShareDeleteResult = Database.delete(prblmShareList);
                for(Database.DeleteResult dr: prblmShareDeleteResult){
                    if(!dr.isSuccess()){
                        Database.Error err = dr.getErrors()[0];
                        System.Debug('######## AP1000-Share Error Deleting : '+err.getStatusCode()+' '+err.getMessage());
                    }
                }
            }
        }   
        System.Debug('****Utils_P2PMethods deletePrblmShare  Finished****');
    }
    
       public static Map<Id, Map<Integer,List<EntityStakeholder__c>>> getProblemEntityStakeHolderList(List<Problem__c> prblmList){
        //*********************************************************************************
        // Method Name      : getProblemEntityStakeHolderList
        // Purpose          : To create The Map for All the Problem and associated Entity StakeHolder List. List includes StakeHolder from all Levels 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 06th September 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****Utils_P2PMethods getProblemEntityStakeHolderList  Started****');
        
        Map<Id, Integer> prblmLevelMap = new Map<Id, Integer>();
        Map<Id, Integer> orgLevelMap = new Map<Id, Integer>();
        Map<String, BusinessRiskEscalationEntity__c[]> accOrgMap = new Map<String, BusinessRiskEscalationEntity__c[]>();
        Map<Id, BusinessRiskEscalationEntity__c[]> prblmOrgMap = new Map<Id, BusinessRiskEscalationEntity__c[]>();
        Map<ID,EntityStakeholder__c[]> entityStakeHolderMap = new Map<ID,EntityStakeholder__c[]>();
        Map<Id, Map<Integer,List<EntityStakeholder__c>>> prblmEntityStakeHolderwithLevelMap=new Map<Id, Map<Integer,List<EntityStakeholder__c>>>();     
       // Map<Id, List<EntityStakeHolderWithLevel>> prblmEntityStakeHolderwithLevelMap = new Map<Id, List<EntityStakeHolderWithLevel>>();
        Set<String> strEntitySet = new Set<String>();
        List<BusinessRiskEscalationEntity__c> businessRiskEntityList = new List<BusinessRiskEscalationEntity__c> ();
        Map<ID,String> orgRoleMap =new Map<ID,String>();
        
        prblmLevelMap = getProblemAccountableOrgLevel(prblmList);
        for(Problem__c prblm: prblmList){           
            strEntitySet.add(prblm.AccountableOrganization__r.Entity__c);
        }
        
        //=========== SUKUMAR SALLA - MAY2013RELEASE - START =================================
        Map<Id,BusinessRiskEscalationEntity__c> businessRiskMap = new Map<Id,BusinessRiskEscalationEntity__c>([Select Id,Entity__c,SubEntity__c,Location__c,Location_Type__c from BusinessRiskEscalationEntity__c Where Entity__c IN: strEntitySet]);
        // Map<Id,BusinessRiskEscalationEntity__c> businessRiskMap = new Map<Id,BusinessRiskEscalationEntity__c>([Select Id,Entity__c,SubEntity__c,Location__c,Location_Type__c from BusinessRiskEscalationEntity__c Where Entity__c IN: strEntitySet AND Location_Type__c!=:label.CLMAY13I2P10]);
         //=========== SUKUMAR SALLA - MAY2013RELEASE - END =================================
        List<BusinessRiskEscalationEntity__c> businessRiskList = businessRiskMap.values();
        
        for(BusinessRiskEscalationEntity__c bre: businessRiskList){
            if(accOrgMap.containsKey(bre.Entity__c)){
                accOrgMap.get(bre.Entity__c).add(bre);
            }
            else{
                List<BusinessRiskEscalationEntity__c> breChildList = new List<BusinessRiskEscalationEntity__c> ();
                breChildList.add(bre);
                accOrgMap.put(bre.Entity__c, breChildList);
            }
        }
        List<BusinessRiskEscalationEntity__c> prblmBreList = new List<BusinessRiskEscalationEntity__c> ();
        for(Problem__c prblm: prblmList){
            if(prblmLevelMap.containsKey(prblm.Id)){
                prblmBreList.clear();
                if(prblmLevelMap.get(prblm.Id)==1){
                    if(businessRiskMap.containsKey(prblm.AccountableOrganization__c)){  
                        
                        prblmBreList.add(businessRiskMap.get(prblm.AccountableOrganization__c));
                        prblmOrgMap.put(prblm.id,prblmBreList);
                    }
                }
                else if(prblmLevelMap.get(prblm.Id)==2){
                    if(accOrgMap.containsKey(prblm.AccountableOrganization__r.Entity__c)){
                        List<BusinessRiskEscalationEntity__c> breChildList = new List<BusinessRiskEscalationEntity__c> ();
                        breChildList=accOrgMap.get(prblm.AccountableOrganization__r.Entity__c);
                        for(BusinessRiskEscalationEntity__c bre:breChildList){
                            if((bre.Location__c==null) && (bre.SubEntity__c == null || bre.SubEntity__c == prblm.AccountableOrganization__r.SubEntity__c)){
                                prblmBreList.add(bre);
                            }
                        }
                        if(prblmBreList.size()>0){
                            prblmOrgMap.put(prblm.id,prblmBreList);
                        }
                    }
                }
                else if(prblmLevelMap.get(prblm.Id)==3){
                    if(accOrgMap.containsKey(prblm.AccountableOrganization__r.Entity__c)){
                        List<BusinessRiskEscalationEntity__c> breChildList = new List<BusinessRiskEscalationEntity__c> ();
                        breChildList=accOrgMap.get(prblm.AccountableOrganization__r.Entity__c);
                        for(BusinessRiskEscalationEntity__c bre:breChildList){
                            if( ( (bre.Location__c == null) || (bre.Location__c == prblm.AccountableOrganization__r.Location__c && bre.Location_Type__c ==  prblm.AccountableOrganization__r.Location_Type__c)) && 
                                  ((bre.SubEntity__c == null) || (bre.SubEntity__c == prblm.AccountableOrganization__r.SubEntity__c))){
                                prblmBreList.add(bre);
                            }
                        }
                        if(prblmBreList.size()>0){
                            prblmOrgMap.put(prblm.id,prblmBreList);
                        }
                    }
                }
            }
        }
        for(Problem__c prblm: prblmList){
            if(prblmOrgMap.containsKey(prblm.id)){
                businessRiskEntityList.addAll(prblmOrgMap.get(prblm.id));
            }
        }
        for(BusinessRiskEscalationEntity__c bre: businessRiskEntityList){
            orgRoleMap.put(bre.id,'ALL');
        }
        if(orgRoleMap.size()>0){
            entityStakeHolderMap = Utils_P2PMethods.retrieveOrganizationUserDetails(orgRoleMap);
        }
        if(businessRiskEntityList.size()>0){
            orgLevelMap=getAccountableOrgLevel(businessRiskEntityList);
        }
        Map<Integer,EntityStakeholder__c[]> entyStakeHolderWithLevelListMap=new Map<Integer,EntityStakeholder__c[]>();
        for(Problem__c prblm: prblmList){
            if(prblmOrgMap.containsKey(prblm.id)){
                List<BusinessRiskEscalationEntity__c> OrgList= prblmOrgMap.get(prblm.id);
                //List<EntityStakeHolderWithLevel> entyStakeHolderWithLevelList = new List<EntityStakeHolderWithLevel>();
                entyStakeHolderWithLevelListMap.clear();
                for(BusinessRiskEscalationEntity__c Org: OrgList){
                    if((entityStakeHolderMap.containsKey(Org.Id)) && (orgLevelMap.containsKey(Org.Id))){
                        System.debug('##OrgId###:' + Org.Id);
                        System.debug('##OrgLevel###:' + orgLevelMap.get(Org.Id));
                      //  EntityStakeHolderWithLevel objEntityStakeHolderWithLevel = new EntityStakeHolderWithLevel(orgLevelMap.get(Org.Id),entityStakeHolderMap.get(Org.Id));
                        entyStakeHolderWithLevelListMap.put(orgLevelMap.get(Org.Id),entityStakeHolderMap.get(Org.Id));                                    
                      //  entyStakeHolderWithLevelList.add(objEntityStakeHolderWithLevel);
                    }
                }
                System.debug('##entityStakeHolderMap##'+entityStakeHolderMap);
                
                //if entyStakeHolderWithLevelList is blank, a Blank entyStakeHolderWithLevelList will be associated with Problem ID
             //   prblmEntityStakeHolderwithLevelMap.put(prblm.id,entyStakeHolderWithLevelList);
                prblmEntityStakeHolderwithLevelMap.put(prblm.id,entyStakeHolderWithLevelListMap);
            }    
        }
        System.Debug('****Utils_P2PMethods getProblemEntityStakeHolderList  Finished****');
        return prblmEntityStakeHolderwithLevelMap;
    }
    
    
    public static Map<Id, Integer> getProblemAccountableOrgLevel(List<Problem__c> prblmList){
        //*********************************************************************************
        // Method Name      : getProblemAccountableOrgLevel
        // Purpose          : To create The Map for All the Problem and associated Level of the Accountable Organization
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 06th September 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****Utils_P2PMethods getProblemAccountableOrgLevel  Started****');
        
        Map<Id, Integer> prblmLevelMap = new Map<Id, Integer>();
        for(Problem__c prblm: prblmList){
            if(prblm.AccountableOrganization__r.Location__c!=null)
                prblmLevelMap.put(prblm.id,3);
            else if(prblm.AccountableOrganization__r.SubEntity__c !=null && prblm.AccountableOrganization__r.Location__c ==null)
                prblmLevelMap.put(prblm.id,2);
            else if(prblm.AccountableOrganization__r.SubEntity__c ==null && prblm.AccountableOrganization__r.Location__c ==null)
                prblmLevelMap.put(prblm.id,1);
        }
        System.Debug('****Utils_P2PMethods getProblemAccountableOrgLevel  Finished ****');
        return prblmLevelMap;
    }
    public static Integer getOrgLevel(Problem__c prblm){
                //*********************************************************************************
        // Method Name      : getOrgLevel
        // Purpose          : To create The Accountable Organization Level using Problem 
        // Created by       : HariKrishna - Global Delivery Team
        // Date created     : 09th September 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****Utils_P2PMethods getOrgLevel  Started****');
        Integer OrgLevel;
            if(prblm.AccountableOrganization__r.Location__c != null)                
                OrgLevel = 3;
            else if(prblm.AccountableOrganization__r.SubEntity__c != null && prblm.AccountableOrganization__r.Location__c == null)
                OrgLevel = 2;
            else if(prblm.AccountableOrganization__r.SubEntity__c == null && prblm.AccountableOrganization__r.Location__c == null)
               OrgLevel =1; 
                    
            System.Debug('****Utils_P2PMethods getOrgLevel  Finished****');
            
            return OrgLevel;
    }
    
    public static Map<Id, Integer> getAccountableOrgLevel(List<BusinessRiskEscalationEntity__c> breList){
        //*********************************************************************************
        // Method Name      : getProblemAccountableOrgLevel
        // Purpose          : To create The Map for All the Accountable Organization and associated Level
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 06th September 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****Utils_P2PMethods getAccountableOrgLevel  Started****');
        
        Map<Id, Integer> OrgLevelMap = new Map<Id, Integer>();
        for(BusinessRiskEscalationEntity__c bre: breList){
            if(bre.Location__c!=null)
                OrgLevelMap.put(bre.id,3);
            else if(bre.SubEntity__c !=null && bre.Location__c ==null)
                OrgLevelMap.put(bre.id,2);
            else if(bre.SubEntity__c ==null && bre.Location__c ==null)
                OrgLevelMap.put(bre.id,1);
        }
        System.Debug('****Utils_P2PMethods getAccountableOrgLevel  Finished****');
        return OrgLevelMap;
        
    }
    /*
    public class EntityStakeHolderWithLevel{   
        public Integer orgLevel;
        public List<EntityStakeHolder__c> entityStakeHolderList;
        
        public EntityStakeHolderWithLevel(Integer orgLevel,List<EntityStakeHolder__c> entityStakeHolderList){       
            this.orgLevel=orgLevel;
            this.entityStakeHolderList=entityStakeHolderList;  
        }       
    }
    */
}