global class AggregateResultIterator implements Iterator<AggregateResult> {
   AggregateResult [] results {get;set;}
   // tracks which result item is returned
   Integer index {get; set;}
          
    //Code by Johan HAZEBROUCK on 13/03/2015
    global AggregateResultIterator() {
        index = 0;
        List<String> listPartnerContactId = new List<String>();
        for(AggregateResult ar : [SELECT PRMUIMSSEContactId__c, COUNT(Name) FROM Contact
            WHERE PRMOnly__c = true AND PRMUIMSID__c <> null
            GROUP BY PRMUIMSSEContactId__c HAVING COUNT(Name) = 1 LIMIT 200]){
                listPartnerContactId.add(String.valueOf(ar.get('PRMUIMSSEContactId__c')));
        }
        results = [SELECT SEContactID__c, COUNT(Name) FROM Contact WHERE PRMOnly__c = false AND PRMUIMSID__c =: null
                   AND SEContactID__c IN: listPartnerContactId 
                   GROUP BY SEContactID__c HAVING COUNT(Name) = 1 LIMIT 200];
    }
   
   global boolean hasNext(){
      return results != null && !results.isEmpty() && index < results.size();
   }   
    
   global AggregateResult next(){       
      return results[index++];           
   }      
}