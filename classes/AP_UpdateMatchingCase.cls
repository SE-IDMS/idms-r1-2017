/*******************************************************************************************************
        // Class Name : AP_UpdateMatchingCase 
        // Purpose : To check for existing of Open Problems with same Product and Symptom as that of Case
        // Created by : Uttara PR - Global Delivery Team
        // Date created : 10-10-2015
        // Remarks : For OCT 2015 Release
//******************************************************************************************************/

public class AP_UpdateMatchingCase {    
    
    @Future
    public static void performMatching(Set<Id> PrbIds) {
        
        Map<Id,Problem__c> mapPrb = new Map<Id,Problem__c>();
        Map<String,Problem__c> mapCRPrb = new Map<String,Problem__c>();
        Map<String,Problem__c> mapComRefPrb = new Map<String,Problem__c>();
        Map<String,Problem__c> mapFamilyPrb = new Map<String,Problem__c>();
        Set<Id> matchedCaseIds = new Set<Id>();
        Set<Id> matchedProblemIds = new Set<Id>();
        Set<string> stPLCID = new Set<string>();
        List<ProblemCaseUnifiedLink__c> lstPCUL = new List<ProblemCaseUnifiedLink__c>();
        List<ProblemCaseUnifiedLink__c> lstPCULExisting = new List<ProblemCaseUnifiedLink__c>();
        List<ProblemCaseUnifiedLink__c> lstPCULDelete = new List<ProblemCaseUnifiedLink__c>();
        Set<Id> PreviousCaseIds = new Set<Id>();
        
        for(Problem__c prb : [SELECT TECH_Symptoms__c, Severity__c, Sensitivity__c, Status__c FROM Problem__c WHERE Id IN: PrbIds]) {
            mapPrb.put(prb.Id,prb);
        }
        System.debug('#### mapPrb : ' + mapPrb);
                
        for(CommercialReference__c cr : [SELECT GMRCode__c, CommercialReference__c, Family__c, Problem__c, Problem__r.Name, Problem__r.TECH_Symptoms__c, Problem__r.Severity__c, Problem__r.Sensitivity__c, Problem__r.Status__c FROM CommercialReference__c WHERE (Problem__c IN: PrbIds AND Problem__r.Sensitivity__c = 'Public' AND (Problem__r.Status__c =: Label.CLOCT15I2P31 OR Problem__r.Status__c =: Label.CLOCT15I2P32 OR Problem__r.Status__c =: Label.CLOCT15I2P33)) ]) {
            // CLOCT15I2P31 = New
            // CLOCT15I2P32 = Stand By
            // CLOCT15I2P33 = In Progress 
            if(cr.Problem__r.TECH_Symptoms__c != null) {
            
                System.debug('#### Symptoms and product present. Problem is Open and Public');
                mapCRPrb.put(cr.GMRCode__c,mapPrb.get(cr.Problem__c)); 
                if(cr.CommercialReference__c != null)
                    mapComRefPrb.put(cr.CommercialReference__c,mapPrb.get(cr.Problem__c));
                if(cr.CommercialReference__c == null)
                    mapFamilyPrb.put(cr.Family__c,mapPrb.get(cr.Problem__c));
            }
        }
                
        System.debug('#### mapCRPrb : ' + mapCRPrb);
        System.debug('#### mapComRefPrb : ' + mapComRefPrb);
        System.debug('#### mapFamilyPrb : ' + mapFamilyPrb);
        
        for(ProblemCaseLink__c varplctemp : [SELECT id,Case__c,Problem__c FROM ProblemCaseLink__c WHERE Problem__c IN: mapCRPrb.values()]) {
                stPLCID.add(varplctemp.Case__c);
        }
                
        for(Case varcase : [SELECT CommercialReference__c, Family__c, CaseNumber, Status, TECH_GMRCode__c, CaseSymptom__c, CaseSubsymptom__c FROM Case WHERE TECH_GMRCode__c IN: mapCRPrb.keySet() AND (CommercialReference__c IN: mapComRefPrb.keySet() OR Family__c IN: mapFamilyPrb.keySet()) AND (Status =: Label.CLOCT15I2P31 OR Status =: Label.CLOCT15I2P33 OR Status =: Label.CLOCT15I2P34) AND (CaseSymptom__c != null AND CaseSubSymptom__c != null)]) {
            // CLOCT15I2P31 = New
            // CLOCT15I2P33 = In Progress
            // CLOCT15I2P34 = Answer Provided to Customer
            
            if(!stPLCID.contains(varcase.Id) && mapCRPrb.get(varcase.TECH_GMRCode__c).TECH_Symptoms__c.contains(varcase.CaseSymptom__c+' - '+varcase.CaseSubSymptom__c)) {
            
                System.debug('#### Symptom on case and Problem matched.');
                
                matchedCaseIds.add(varcase.Id);
                matchedProblemIds.add(mapCRPrb.get(varcase.TECH_GMRCode__c).Id);
                
                if(mapCRPrb.get(varcase.TECH_GMRCode__c).Severity__c == Label.CLOCT14I2P22)  {   // 'Safety Related'
                    ProblemCaseUnifiedLink__c pcul = new ProblemCaseUnifiedLink__c();
                    pcul.Case__c = varcase.Id;
                    pcul.Problem__c = mapCRPrb.get(varcase.TECH_GMRCode__c).Id;
                    pcul.ProblemSeverity__c = 'OSA';
                    lstPCUL.add(pcul); 
                } 
                
                else {
                    ProblemCaseUnifiedLink__c pcul = new ProblemCaseUnifiedLink__c();
                    pcul.Case__c = varcase.Id;
                    pcul.Problem__c = mapCRPrb.get(varcase.TECH_GMRCode__c).Id;
                    pcul.ProblemSeverity__c = 'non OSA';
                    lstPCUL.add(pcul);     
                }    
            }
        }    
        
        System.debug('#### matchedCaseIds : ' + matchedCaseIds);
        System.debug('#### lstPCUL : ' + lstPCUL);
        
        lstPCULExisting = [SELECT Case__c, Case__r.Status, Problem__c FROM ProblemCaseUnifiedLink__c WHERE (Case__c IN: matchedCaseIds AND Problem__c IN: matchedProblemIds AND (Case__r.Status =: Label.CLOCT15I2P31 OR Case__r.Status =: Label.CLOCT15I2P33 OR Case__r.Status =: Label.CLOCT15I2P34))];
        System.debug('#### lstPCULExisting : ' + lstPCULExisting);
        Database.Delete(lstPCULExisting);
        Database.Insert(lstPCUL);
          
        if(matchedCaseIds.isEmpty()) {
            System.debug('#### Match did not occur.');
            for(ProblemCaseUnifiedLink__c pcul : [SELECT Case__c, Case__r.Status, Problem__c FROM ProblemCaseUnifiedLink__c WHERE Problem__c IN: mapPrb.keySet() AND (Case__r.Status =: Label.CLOCT15I2P31 OR Case__r.Status =: Label.CLOCT15I2P33 OR Case__r.Status =: Label.CLOCT15I2P34)]) {
                PreviousCaseIds.add(pcul.Case__c);
                lstPCULDelete.add(pcul);
            }  
            
            if(!PreviousCaseIds.isEmpty()) {
                System.debug('#### PreviousCaseIds :' + PreviousCaseIds);
                Database.Delete(lstPCULDelete);
                AP_UpdateMatchingCase.CaseWithOpenProblems(PreviousCaseIds);    
            }      
        }
        
    }
    
    public static void CaseWithOpenProblems(Set<ID> CaseIds) {
    
        Map<String,Case> mapProductGMRCase = new Map<String,Case>();
        Map<String,Case> mapComRefCase = new Map<String,Case>();
        Map<String,Case> mapFamilyCase = new Map<String,Case>();
        Set<Id> stNoProblem = new Set<Id>();
        Set<Id> matchedProblemIds = new Set<Id>(); 
        Set<Id> matchedCaseIds = new Set<Id>();
        Set<string> stPLCID = new Set<string>();  
        List<ProblemCaseUnifiedLink__c> lstPCUL = new List<ProblemCaseUnifiedLink__c>(); 
        List<ProblemCaseUnifiedLink__c> lstPCULExisting = new List<ProblemCaseUnifiedLink__c>();
        List<ProblemCaseUnifiedLink__c> lstPCULDelete = new List<ProblemCaseUnifiedLink__c>();     
        
        for(Case cs : [SELECT CommercialReference__c,Family__c, Status, TECH_GMRCode__c, CaseSymptom__c, CaseSubsymptom__c FROM Case WHERE (Id IN: CaseIds AND (Status =: Label.CLOCT15I2P31 OR Status =: Label.CLOCT15I2P33 OR Status =: Label.CLOCT15I2P34))]) {
            // CLOCT15I2P31 = New            
            // CLOCT15I2P33 = In Progress            
            // CLOCT15I2P34 = Answer Provided to Customer  
            
            if(cs.TECH_GMRCode__c != null && cs.CaseSymptom__c != null && cs.CaseSubsymptom__c != null) {
                mapProductGMRCase.put(cs.TECH_GMRCode__c,cs);
                if(cs.CommercialReference__c != null)
                    mapComRefCase.put(cs.CommercialReference__c,cs);
                if(cs.CommercialReference__c == null)
                    mapFamilyCase.put(cs.Family__c,cs);
            }    
                
            else
                stNoProblem.add(cs.Id);    
        }
        System.debug('#### mapProductGMRCase : ' + mapProductGMRCase );
        System.debug('#### mapComRefCase : ' + mapComRefCase);
        System.debug('#### mapFamilyCase : ' + mapFamilyCase);
        
        for(ProblemCaseLink__c varplctemp : [SELECT id,Case__c,Problem__c FROM ProblemCaseLink__c WHERE Case__c IN: mapProductGMRCase.values()]) {
            stPLCID.add(varplctemp.Problem__c);
        }
                
        for(CommercialReference__c cr : [SELECT GMRCode__c,CommercialReference__c,Family__c, Problem__c, Problem__r.TECH_Symptoms__c, Problem__r.Severity__c, Problem__r.Sensitivity__c, Problem__r.Status__c FROM CommercialReference__c WHERE (GMRCode__c IN: mapProductGMRCase.keySet() AND (CommercialReference__c IN: mapComRefCase.keySet() OR Family__c IN: mapFamilyCase.keySet()) AND Problem__r.Sensitivity__c = 'Public' AND (Problem__r.Status__c =: Label.CLOCT15I2P31 OR Problem__r.Status__c =: Label.CLOCT15I2P32 OR Problem__r.Status__c =: Label.CLOCT15I2P33)) ]) {
            // CLOCT15I2P31 = New
            // CLOCT15I2P32 = Stand By
            // CLOCT15I2P33 = In Progress 
              System.debug('#### ram 1'+cr.Problem__r.TECH_Symptoms__c);
              System.debug('#### SRam 2'+mapProductGMRCase.get(cr.GMRCode__c).CaseSymptom__c+' - '+mapProductGMRCase.get(cr.GMRCode__c).CaseSubSymptom__c);
              
              
              
                if(!stPLCID.contains(cr.Problem__c) && cr.Problem__r.TECH_Symptoms__c != null && cr.Problem__r.TECH_Symptoms__c.contains(mapProductGMRCase.get(cr.GMRCode__c).CaseSymptom__c+' - '+mapProductGMRCase.get(cr.GMRCode__c).CaseSubSymptom__c)) {
                
                    System.debug('#### Symptom on case and Problem matched.');
                    matchedProblemIds.add(cr.Problem__c);
                    matchedCaseIds.add(mapProductGMRCase.get(cr.GMRCode__c).Id); 
                    
                    if(cr.Problem__r.Severity__c == Label.CLOCT14I2P22)  {   // 'Safety Related'
                        ProblemCaseUnifiedLink__c pcul = new ProblemCaseUnifiedLink__c();
                        pcul.Case__c = mapProductGMRCase.get(cr.GMRCode__c).Id;
                        pcul.Problem__c = cr.Problem__c;
                        pcul.ProblemSeverity__c = 'OSA';
                        lstPCUL.add(pcul); 
                    } 
                    
                    else {
                        ProblemCaseUnifiedLink__c pcul = new ProblemCaseUnifiedLink__c();
                        pcul.Case__c = mapProductGMRCase.get(cr.GMRCode__c).Id;
                        pcul.Problem__c = cr.Problem__c;
                        pcul.ProblemSeverity__c = 'non OSA';
                        lstPCUL.add(pcul);     
                    } 
            } 
        }
        
        System.debug('#### matchedProblemIds : ' + matchedProblemIds);
        System.debug('#### lstPCUL : ' + lstPCUL);
        
        lstPCULExisting = [SELECT Case__c, Case__r.Status, Problem__c FROM ProblemCaseUnifiedLink__c WHERE Case__c IN: matchedCaseIds AND Problem__c IN: matchedProblemIds AND (Case__r.Status =: Label.CLOCT15I2P31 OR Case__r.Status =: Label.CLOCT15I2P33 OR Case__r.Status =: Label.CLOCT15I2P34)];
        Database.Delete(lstPCULExisting);
        Database.Insert(lstPCUL);
        
        if(matchedProblemIds.isEmpty()) {
            for(Id idd : CaseIds)
                stnoProblem.add(idd);   
            
            System.debug('#### stNoProblem : ' + stNoProblem);
            if(!stnoProblem.isEmpty()) {
                System.debug('#### Match did not occur.');
                lstPCULDelete = [SELECT Case__c,Case__r.Status, Problem__c FROM ProblemCaseUnifiedLink__c WHERE Case__c IN: stNoProblem AND (Case__r.Status =: Label.CLOCT15I2P31 OR Case__r.Status =: Label.CLOCT15I2P33 OR Case__r.Status =: Label.CLOCT15I2P34)];
                Database.Delete(lstPCULDelete);
            } 
        }
        
    }
    // Below methods moved from AP_Case_handler class. 
    public static void checkCMI_CSI(Map <Id, Case> newMap){
        //*********************************************************************************
        // Method Name : checkCMI_CSI
        // Purpose : To check which Potential Major Issues cases whether the case has CSI record or not , if not it will through the error. 
         // Created by : Ram Chilukuri - Global Delivery Team
         // Date created : 10th June 2015
         // Modified by :
         // Date Modified :
         // BR :BR-7781
         // Remarks : For Oct 2015 Release
         ///*********************************************************************************/       
        Map <string, Case>  lstPMICaseIds = new Map <string, Case>();
        Map <string, string> mapCaseAcctId=new Map<string,string>();
        map<id,account> mapaccount= new map<id,account>();
        for( Id newCaseId: newMap.keyset())
        {
            System.debug('@@ Severity__c, CustomerMajorIssue__c ' + newMap.get(newCaseId).Severity__c + ' * ' + newMap.get(newCaseId).CustomerMajorIssue__c+' * '+newMap.get(newCaseId).Account.TECH_I2PNotification__c +' * '+newMap.get(newCaseId).TECH_IsSymptomMajor__c ); 
            
             if(newMap.get(newCaseId).Severity__c==label.CLOCT15I2P03 && newMap.get(newCaseId).CustomerMajorIssue__c=='Yes' )
             {           
                lstPMICaseIds.put(String.valueOf(newMap.get(newCaseId).Id),newMap.get(newCaseId));
            }
            mapCaseAcctId.put(String.valueOf(newMap.get(newCaseId).id),String.valueOf(newMap.get(newCaseId).AccountId));
           
        } 
        System.debug('@@ mapCaseAcctId '+ mapCaseAcctId);
        if(mapCaseAcctId.size()>0){
                 mapaccount=new map<id,account>([select id,TECH_I2PNotification__c,Type from account where id in:mapCaseAcctId.values() ]);
            }
             System.debug('@@ mapaccount ' + mapaccount);
             if(mapaccount.size()>0 && newMap.size() > 0){
             for( Id newCaseId: newMap.keyset())
                {
                    if(mapaccount.containsKey(mapCaseAcctId.get(newCaseId))) {
                    if(newMap.get(newCaseId).CustomerMajorIssue__c!='No' && newMap.get(newCaseId).CustomerMajorIssue__c!='Yes'){
                        System.debug('@@ mapaccountmapCaseAcctId  ' +mapaccount.get(mapCaseAcctId.get(newCaseId)).TECH_I2PNotification__c);
                        if(newMap.get(newCaseId).Severity__c==Label.CLOCT15I2P03 ||  mapaccount.get(mapCaseAcctId.get(newCaseId)).TECH_I2PNotification__c=='GSA' && (newMap.get(newCaseId).Severity__c==Label.CLOCT15I2P71 || newMap.get(newCaseId).Severity__c==Label.CLOCT15I2P72 || newMap.get(newCaseId).Severity__c==Label.CLOCT15I2P73 || newMap.get(newCaseId).TECH_IsSymptomMajor__c==true ))
                        {
                      newMap.get(newCaseId).CustomerMajorIssue__c='To be reviewed';
                      newMap.get(newCaseId).TECH_IsPMIFlag__c=true;
                        }
                    else
                        { 
                    newMap.get(newCaseId).CustomerMajorIssue__c='';
                    newMap.get(newCaseId).TECH_IsPMIFlag__c=false;
                    }
                    }
                    }
             }}
        System.debug('@@ lstPMICaseIds'+ lstPMICaseIds);      
        If(lstPMICaseIds.size()>0)
        {
            AP_InvokeNotification.checkCSI(lstPMICaseIds);
        }
    } 
    
    public static void checkPMI_CMI(Map <Id, Case> oldMap, Map <Id, Case> newMap){
        //*********************************************************************************
        // Method Name : checkPMI_CMI
        // Purpose : To check which cases can be Potential Major Issues or Customer Major Issues.
        // 
         // Created by : Sukumar Salla - Global Delivery Team
         // Date created : 21st July 2014
         // Modified by :
         // Date Modified :
         // Remarks : For Oct 2014 Release
         ///*********************************************************************************/
         
 
        List<String> lstPMICaseIds = new List<String>();
        List<String> lstCMICaseIds = new List<String>();
        map<string, case> mapCMICases = new map<string, case>();
     
       
        System.debug('@@ List of case new ids'+ newMap.keyset());
        
        // Added by Ram Start
        map<string,String> mapPLChangeCaseIds = new  map<string,String>();
      map<string,String> mapPLChangeCaseIdsCmi = new  map<string,String>();
        
        // Added by Ram End
     
        for( Id newCaseId: newMap.keyset())
        {  
            // To prepare list of Cases of Potential Major Issues
           
            // Added by Ram Start 
            if(oldMap!=null ) {
             if( (newMap.get(newCaseId).TECH_IsPMIFlag__c &&  newMap.get(newCaseId).CustomerMajorIssue__c=='To be reviewed' )&& (( oldMap.get(newCaseId).TECH_GMRCode__c!=null && newMap.get(newCaseId).TECH_GMRCode__c!=null  && oldMap.get(newCaseId).TECH_GMRCode__c.substring(0,4)!= newMap.get(newCaseId).TECH_GMRCode__c.substring(0,4) ) || (oldMap.get(newCaseId).TECH_GMRCode__c==null && newMap.get(newCaseId).TECH_GMRCode__c!=null  )))
            {System.debug('***** PMI 1');
            mapPLChangeCaseIds.put(String.valueOf(newMap.get(newCaseId).Id),'');
            lstPMICaseIds.add(String.valueOf(newMap.get(newCaseId).Id));
            }         
            
            // Added by Ram End
            else if((oldMap.get(newCaseId).TECH_IsPMIFlag__c!=newMap.get(newCaseId).TECH_IsPMIFlag__c) && (newMap.get(newCaseId).TECH_IsPMIFlag__c))
            {System.debug('***** PMI 2');
                System.debug('@@ Case Id'+ String.valueOf(newMap.get(newCaseId).Id));
                lstPMICaseIds.add(String.valueOf(newMap.get(newCaseId).Id));
            }
            // To prepare list of Cases of Customer Major Issues
            // Added by Ram Start
            if(  (newMap.get(newCaseId).CustomerMajorIssue__c=='Yes') && (( oldMap.get(newCaseId).TECH_GMRCode__c!=null && newMap.get(newCaseId).TECH_GMRCode__c!=null  && oldMap.get(newCaseId).TECH_GMRCode__c.substring(0,4)!= newMap.get(newCaseId).TECH_GMRCode__c.substring(0,4) ) || (oldMap.get(newCaseId).TECH_GMRCode__c==null && newMap.get(newCaseId).TECH_GMRCode__c!=null )  ))
            {System.debug('***** CMI 1');
            mapPLChangeCaseIdsCmi.put(String.valueOf(newMap.get(newCaseId).Id),'');
            lstCMICaseIds.add(String.valueOf(newMap.get(newCaseId).Id));
            }    
            }
            
            // Added by Ram End
            if(oldMap!=null) {
            if((oldMap.get(newCaseId).CustomerMajorIssue__c!=newMap.get(newCaseId).CustomerMajorIssue__c)&& newMap.get(newCaseId).CustomerMajorIssue__c=='Yes' )
            {System.debug('***** CMI 2');
                lstCMICaseIds.add(String.ValueOf(newMap.get(newCaseId).Id));
                mapCMICases.put(newMap.get(newCaseId).Id, newMap.get(newCaseId));
            } 
            }
            if((oldMap==null || oldMap.size()==0) && newMap.get(newCaseId).CustomerMajorIssue__c=='Yes'&& newMap.get(newCaseId).Severity__c!=label.CLOCT15I2P04) 
            {
                lstCMICaseIds.add(String.ValueOf(newMap.get(newCaseId).Id));
                mapCMICases.put(newMap.get(newCaseId).Id, newMap.get(newCaseId));
            }
        }
      
        System.debug('@@ lstPMICaseIds'+ lstPMICaseIds);
        System.debug('@@ lstCMICaseIds'+ lstCMICaseIds);
      
        If(lstPMICaseIds.size()>0)
        {System.debug('***** PMI 3');
        // Added by Ram Start 
        AP_InvokeNotification.invokePMINotifications(lstPMICaseIds,mapPLChangeCaseIds);
        // Added by Ram End
            //AP_InvokeNotification.invokePMINotifications(lstPMICaseIds);
        } 
      
        If(lstCMICaseIds.size()>0)
        {System.debug('***** CMI 3');
        // Added by Ram Start
        AP_InvokeNotification.invokeCMINotifications(lstCMICaseIds,mapPLChangeCaseIdsCmi);
        // Added by Ram End
            //AP_InvokeNotification.invokeCMINotifications(lstCMICaseIds);
        }
 
    }
    
    
    public static void updateCaseStakeholders(map<id, Case> mapCaseObj){
    
     //*********************************************************************************
     // Method Name : updateCaseStakeholders
     // Purpose : To update Case Status on related Case Stakeholders when case status is closed
     // Created by : Sukumar Salla - Global Delivery Team
     // Date created : 21st July 2014
     // Remarks : For Oct 2014 Release
    ///*********************************************************************************/
       
       list<CaseStakeholders__c> lstFinalCaseStakehodlers = new list<CaseStakeholders__c>(); 
       for(CaseStakeholders__c cs: [select id, Case__c, TECH_CaseStatus__c from CaseStakeholders__c where Case__c in:mapCaseObj.keyset()])
       {
           cs.TECH_CaseStatus__c = mapCaseObj.get(cs.Case__c).Status;
           lstFinalCaseStakehodlers.add(cs);
       } 
       
       if(lstFinalCaseStakehodlers.size()>0)
       update lstFinalCaseStakehodlers;
    
    }   
    public static void updateLineOfBusinessOnTEX(Map<id,Case> caseIds){
        //*********************************************************************************
        // Method Name : updateLineOfBusinessOnTEX
        // Purpose : To Update Line of Business on TEX whenever the Product line on the case is Updated 
         // Created by : Divya M  - Global Delivery Team
         // Date created : 17th June 2015
         // Desc  :BR-7761 -->  For Oct 2015 Release
         ///*********************************************************************************/    
        AP_TechnicalExpertAssessment.updateLineOfBusinessOnTEXfromCase(caseIds);
    }        
}