@isTest
public class VFC_PASubscriberAccount_TEST {
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.City__c = 'Bangalore';
        testAccount.SEAccountID__c = '12345678';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Case objCase = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id,'testing');
        insert objCase;
        List<Subscriber__c> lstSubscribe = new List<Subscriber__c>();
        Subscriber__c testSubsrcibe = new Subscriber__c(Account__c = testAccount.Id, Case__c = objCase.Id, Email__c = 'test@test.com');
        lstSubscribe.add(testSubsrcibe);
        Subscriber__c testSubsrcibe1 = new Subscriber__c(Account__c = testAccount.Id, Case__c = objCase.Id, Email__c = 'test1@test1.com');
        lstSubscribe.add(testSubsrcibe1);
        insert lstSubscribe;
    }
    
    @isTest static void testMethod1() {
        Account accObj = [select id from Account limit 1];
        apexpages.currentpage().getparameters().put('id', accObj.Id);
        VFC_PASubscriberAccount controller = new VFC_PASubscriberAccount();
        VFC_PASubscriberAccount.getlstAccount();
    }
    
    @isTest static void testMethod2() {
        Account testAccount = [select id from Account limit 1];
        Contact testcontact1 = [select id from contact limit 1];
        Contract testContract1 = Utils_TestMethods.createContract(testAccount.Id, testcontact1.Id);
        insert testContract1;
        CTR_ValueChainPlayers__c testCVCP1 = Utils_TestMethods.createCntrctValuChnPlyr(testContract1.Id);
        testCVCP1.legacypin__c ='1234';
        testCVCP1.Contact__c = testcontact1.Id;
        insert testCVCP1;
        
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact2');
        testcontact.SEContactID__c = '12345678';
        testcontact.Email = 'test1@test.com';
        testcontact.WorkPhone__c='12345678900';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='12345';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
        
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='1234';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        
        WS_PAPortletUserManagement.UserInformation uInfo1 = new WS_PAPortletUserManagement.UserInformation();
        uInfo1.strContactGoldenId='12345'; 
        uInfo1.strAccountGoldenId='12345678';
        uInfo1.strCustomerFirstId='12345';
        uInfo1.strWebUserId='';
        uInfo1.strWebUserName='testInvv'; 
        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.createUser(uInfo1);
        
        list<VFC_PASubscriberAccount.Accountwrap> lstCheckedUser = new list<VFC_PASubscriberAccount.Accountwrap>();
        Set<Id> userIds = new Set<Id>();
        userIds.add(userresult.UserId);
        userIds.add(userresult1.UserId);
        Account accObj = [select id from Account limit 1];
        for(User objUser :[select id ,FirstName,LastName,Email,Contact.Email, ContactId,Contact.Account.Name from User where Id IN: userIds]){
            User objU = new User(id = objUser.id, FirstName = objUser.FirstName,LastName=objUser.LastName,Email =objUser.contact.email);
            VFC_PASubscriberAccount.Accountwrap  cu = new VFC_PASubscriberAccount.Accountwrap(false,objU,accObj.Id,null);
            lstCheckedUser.add(cu);
         }
         Test.startTest();
         String jsonStr = JSON.serialize(lstCheckedUser);
         VFC_PASubscriberAccount.subscriberResult('test', accObj.Id, jsonStr);
         VFC_PASubscriberAccount.subscriberResult('testtt', accObj.Id, jsonStr);
         VFC_PASubscriberAccount.getClassInstances(jsonStr, accObj.Id);
         VFC_PASubscriberAccount.getClassInstances('[]', accObj.Id);
         Test.stopTest();
    }
}