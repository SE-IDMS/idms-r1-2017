/**
* @author: Deepak
* Date Created (YYYY-MM-DD): April-15 Release
* Test class: AP_WorkDetails_Test
* Description: This class manages the Business logic on Work Detail object
* **********
* Scenarios:
* **********
* 1.  Set the start date/time in WO time zone and end date/time in WO time zone
* 
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Nabil ZEGHACHE
* Modified Date (YYYY-MM-DD: 2016-08-30)
* Description: BR-10471 / Customer start and stop in local timezone on the Work Details
* -----------------------------------------------------------------
*/

public Class AP_WorkDetails {
  
  static String actuals_RT = Label.CLSSRV_WDD_RecordType;
  static final String DATE_TIME_FORMAT = 'yyyy-MM-dd HH:mm';
  
    
    /**
     * This method is used to retrieve all the relevant information from the related Work Orders to be used by any method related to Work Details.
     * This is used to reduce the number of SOQL queries, as many Work Detail related methods are making queries against Work Order.
     * @param aWOIDSet a set of Work Order IDs to retrieve information
     */
  public static Map<Id, SVMXC__Service_Order__c> getWOMapForIDSet(Set<Id> aWOIDSet) {
    Map<Id, SVMXC__Service_Order__c> result = null;
    if (aWOIDSet != null && aWOIDSet.size() > 0) {
      result = new Map<Id, SVMXC__Service_Order__c>(
      [
      SELECT id, SVMXC__Group_Member__c,SVMXC__Group_Member__r.Level__c, IsBillable__c, RecordType.Name, Shipped_Serial_Number__c, SVMXC__Component__c, SVMXC__Product__c, SVMXC__Service_Contract__c, SVMXC__PM_Plan__c, SVMXC__PM_Plan__r.SVMXC__Service_Contract__c, CustomerTimeZone__c 
      FROM SVMXC__Service_Order__c 
      WHERE id = :aWOIDSet
      ]);
    }
    return result;
  }
  
    //Added by VISHNU C for Q3 2016 Release BR-9850
    public static void DeleteWD(Set<Id> WDToDelete) { //This method will delete the WD's from the original WO which got split in the new WO.
        list<SVMXC__Service_Order_Line__c> WDToDeleteList= new list<SVMXC__Service_Order_Line__c>();
        system.debug('WDToDelete vishnu ********'+WDToDelete);
        if( WDToDelete.size() > 0 && !(WDToDelete.isempty()) )
        {
            system.debug('Inside IF loop WDToDelete vishnu ********'+WDToDelete);
            for(SVMXC__Service_Order_Line__c wd : [select Id,SplitFromWDBORef__c,Tech_WDOriginal__c from SVMXC__Service_Order_Line__c where Id in :WDToDelete])
            {
                system.debug('Inside For Loop********'+wd);
                system.debug('SplitFromWDBORef__c is'+wd.SplitFromWDBORef__c+ 'Tech_WDOriginal__c is' +wd.Tech_WDOriginal__c);
                WDToDeleteList.add(wd);
                system.debug('To be Deleted Wds'+WDToDeleteList);
            }
            if( WDToDeleteList.size() > 0 && !(WDToDeleteList.isempty()) )
            {
                system.debug('To be Deleted Wds'+WDToDeleteList);
                //Database.SaveResult[] results=  database.delete(WDToDeleteList,false);
                delete WDToDeleteList;
                
            }
            
        }
        
    }
    
    public static void UpdateWOMIP( map<id,id> WOMIPMap,map<id,string> WOSNProdMap ) { //This method will update the WO's MIP with the Primary WD's IP
        list<SVMXC__Service_Order__c> wolist= new list<SVMXC__Service_Order__c>();
        list<SVMXC__Service_Order__c> woToUpdatelist= new list<SVMXC__Service_Order__c>();
        set<id>woid= new set<id>();
        system.debug('WOMIPMap is '+WOMIPMap+ 'WOSNProdMap' +WOSNProdMap);
        for( Id woid1: WOMIPMap.keyset() )
        {
            woid.add(woid1);
            
        }
        if ( woid != null ) {
            wolist = [select id,Shipped_Serial_Number__c,SVMXC__Component__c,SVMXC__Product__c from SVMXC__Service_Order__c where id in :woid ];
        }
        if( wolist.size() > 0 && !(wolist.isempty()))
        {
            for( SVMXC__Service_Order__c worec :wolist )
            {
                if( WOMIPMap.containskey(worec.id) )
                    try{
                        worec.SVMXC__Component__c = WOMIPMap.get(worec.id);
                    }
                catch(exception e)
                {
                    worec.SVMXC__Component__c = null;
                }
                
                if( WOSNProdMap.containskey(worec.id))
                {
                    system.debug('The SN and Prod id is'+WOSNProdMap.get(worec.id));
                    string SNProdId = WOSNProdMap.get(worec.id);
                    string [] ArraySNProdId = SNProdId.split('#');
                    string SN = ArraySNProdId[0];
                    string sProdId = string.valueof(ArraySNProdId[1]);
                    //system.debug('The Prod id is'+ArraySNProdId[1]);
                    system.debug('The Prod id is after Id'+sProdId);
                    try{
                        Id ProdId = id.valueof(sProdId);
                        //ProdId = ArraySNProdId[1];
                        worec.SVMXC__Product__c = ProdId ;
                        system.debug('The produ id is set');
                    }
                    catch(exception e)
                    {
                        system.debug('The produ id is unset and went inside exception');
                        worec.SVMXC__Product__c = null;
                    }
                    
                    //worec.SVMXC__Product__c = ProdId ;
                    if( worec.Shipped_Serial_Number__c != null )
                        worec.Shipped_Serial_Number__c = SN ;
                    
                }
                system.debug('worec is' +worec);
                woToUpdatelist.add(worec);
                system.debug('woToUpdatelist is' +woToUpdatelist);
            }
            if( woToUpdatelist.size() > 0 && !(woToUpdatelist.isempty()) )
            {
                update woToUpdatelist;
            }
            
        }
        
    }
    //Changes by VISHNU C Ends here
    
    /* Commented by Nabil ZEGHACHE on 2016-09-02 as this method is not correct:
    * 1. It has a loop inside a loop that can be avoided
    * 2. The logic is incorrect as if we receive 1 WO that is Billable and 1 WO that is not billable, then all the WDs of both WO will be billable = 'No'
    Public static void UpdateWD(Set<Id> woidToProcess, List<SVMXC__Service_Order_Line__c> wdlistTobeProcess)
    {   
        for(SVMXC__Service_Order__c wo :[Select Id,IsBillable__c From SVMXC__Service_Order__c Where Id in :woidToProcess])
        {
            for(SVMXC__Service_Order_Line__c wd :wdlistTobeProcess)   
            {
                if(wo.IsBillable__c !=null && (wo.IsBillable__c=='No' || wo.IsBillable__c=='Sales Order'))
                {
                    wd.IsBillable__c = 'No';
                }
                if(wd.SVMXC__Start_Date_and_Time__c !=null && wd.TotalDuration__c !=null )//&& wd.RecordTypeId ==Label.CLAPR15SRV62){
                {
                    wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c + (wd.TotalDuration__c/24);
                }    
            }  
        }
    }
    */


    public static void UpdateWD(Map<Id, SVMXC__Service_Order__c> aWOMap, List<SVMXC__Service_Order_Line__c> wdlistTobeProcess) {   
      //Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([SELECT Id,IsBillable__c FROM SVMXC__Service_Order__c WHERE Id in :woidToProcess]);
      SVMXC__Service_Order__c wo;
        for(SVMXC__Service_Order_Line__c wd :wdlistTobeProcess) {
          if (wd.SVMXC__Service_Order__c != null) {
            wo = aWOMap.get(wd.SVMXC__Service_Order__c);
              if(wo != null && wo.IsBillable__c !=null && (wo.IsBillable__c=='No' || wo.IsBillable__c=='Sales Order')) {
                  wd.IsBillable__c = 'No';
              }
              if(wd.SVMXC__Start_Date_and_Time__c !=null && wd.TotalDuration__c !=null ) { //&& wd.RecordTypeId ==Label.CLAPR15SRV62)
                  wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c + (wd.TotalDuration__c/24);
              }    
          }
        }  
    }
    
    
  /******************************************************************************      
  * Added by Anand
  * For bFS May-15 Release
  * Purpose-Auto Parts Order Line RMA Creation
  ****************************************************************************/ 
    public static void Createpartorderline( set<string>rmaset,list<SVMXC__Service_Order_Line__c> wodetaillist1)
    {
        
        String RMA_RT = System.Label.CLAPR15SRV66;
        String Parts_RT = System.Label.CLAPR15SRV61;
        String ProductServiced_RT = System.Label.CLAPR15SRV60;
        
        list<SVMXC__RMA_Shipment_Line__c> partlist= new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Order__c> partsorder= new list<SVMXC__RMA_Shipment_Order__c>();
        map<id,SVMXC__RMA_Shipment_Line__c> partmap2= new  map<id,SVMXC__RMA_Shipment_Line__c> ();
        List<SVMXC__Service_Order_Line__c> toupdate = new List<SVMXC__Service_Order_Line__c>();   
        partsorder=[select id, name from SVMXC__RMA_Shipment_Order__c  where name in :rmaset];
        if(wodetaillist1!=null && wodetaillist1.size()>0){
            for(SVMXC__Service_Order_Line__c  wodetail:wodetaillist1)
            {
                SVMXC__RMA_Shipment_Line__c  parts= new SVMXC__RMA_Shipment_Line__c();
                
                if(wodetail.recordtypeid==ProductServiced_RT)
                    parts.SVMXC__Serial_Number__c=wodetail.SVMXC__Serial_Number__c;
                if(wodetail.recordtypeid==Parts_RT)
                    parts.SVMXC__Product__c=wodetail.SVMXC__Product__c;
                parts.Generic_reference__c=wodetail.Parts__c;
                if(wodetail.recordtypeid==Parts_RT || wodetail.recordtypeid==ProductServiced_RT){
                    parts.Tracking_number__c = wodetail.TrackingNumber__c;
                    parts.recordtypeid=RMA_RT;
                    parts.Return_Reason__c = wodetail.Return_Reason__c;
                    parts.SVMXC__Expected_Quantity2__c=wodetail.ReturnedQuantity__c;
                    if(partsorder.size()>0)
                        parts.SVMXC__RMA_Shipment_Order__c=partsorder[0].Id;
                    partlist.add(parts);
                    //partmap2.putall(partlist);
                }
            }
        }
        //if(partlist!=null && partlist.size()>0)
        if(partmap2!=null)
        {
            
            Database.SaveResult[] results=  database.insert(partlist,false);
            
            for(Integer k=0;k<results.size();k++ )
            {
                Database.SaveResult sr =results[k];
                
                wodetaillist1[k].Parts_Order_Line_RMA__c = partlist[k].id;
                
            }
        }
        
    }
    
    /******************************************************************************      
Added by Anand
For May-15 Release
Purpose-Parts order shipment clone
****************************************************************************/ 
    
    public static void partsorderlineupdate(list<SVMXC__Service_Order_Line__c> workDetailList,set<id> shipmentset,set<string>rmastringset){
        String RMA_RT = System.Label.CLAPR15SRV66;
        String Parts_RT = System.Label.CLAPR15SRV61;
        String ProductServiced_RT = System.Label.CLAPR15SRV60;
        list<SVMXC__Service_Order_Line__c>  wdtoprocess = new list<SVMXC__Service_Order_Line__c>();
        list<SVMXC__RMA_Shipment_Order__c> partsorder2= new list<SVMXC__RMA_Shipment_Order__c>();
        Map<id,id> wdidshipmentidmap = new Map<id,id>();
        Map<id,id> shipmentidwdidmap = new Map<id,id>();
        list<SVMXC__RMA_Shipment_Line__c> shipmentlist= new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Line__c> shipmentlisttoupdate= new list<SVMXC__RMA_Shipment_Line__c>();
        Map<id,SVMXC__RMA_Shipment_Line__c> shipmentmap = new Map<id,SVMXC__RMA_Shipment_Line__c>();
        Map<id,SVMXC__Service_Order_Line__c> workdetailMap = new Map<id,SVMXC__Service_Order_Line__c>();
        Map<id,id> shipidrmaidmap = new Map<id,id>();
        if(rmastringset!=null)
            partsorder2=[select id, name from SVMXC__RMA_Shipment_Order__c  where name in :rmastringset];
        
        for(SVMXC__Service_Order_Line__c wd:workDetailList){
            if(wd.PartsOrderLine__c != null && (wd.recordtypeid==ProductServiced_RT || wd.recordtypeid==Parts_RT))
            {
                wdtoprocess.add(wd);
                wdidshipmentidmap.put(wd.id,wd.PartsOrderLine__c);
                shipmentidwdidmap.put(wd.PartsOrderLine__c,wd.id);
            }
            
        }
        if(wdtoprocess != null && wdtoprocess.size()>0){
            workdetailMap.putAll(wdtoprocess);
            
            list<SVMXC__RMA_Shipment_Line__c> shipmentRecs= new list<SVMXC__RMA_Shipment_Line__c>();
            list<SVMXC__RMA_Shipment_Line__c> rmaList= new list<SVMXC__RMA_Shipment_Line__c>();
            if(shipmentset != null && shipmentset.size()>0){
                shipmentRecs = [select id, name,Tracking_number__c,SVMXC__Expected_Quantity2__c,Return_Reason__c,SVMXC__RMA_Shipment_Order__c,Free_text_reference__c, Generic_reference__c,SVMXC__Serial_Number__c,SVMXC__Product__c from  SVMXC__RMA_Shipment_Line__c where id in:shipmentset];
                for(SVMXC__RMA_Shipment_Line__c shiprec:shipmentRecs){
                    
                    SVMXC__Service_Order_Line__c workdetailObj = workdetailMap.get(shipmentidwdidmap.get(shiprec.id));
                    if (workdetailObj != null) {
                      sobject  sobj = (Sobject)shiprec;
                      Sobject nobj = sobj.clone(false,true,false,false);
                      SVMXC__RMA_Shipment_Line__c rmaobj = (SVMXC__RMA_Shipment_Line__c)nobj;
                      rmaobj.RecordTypeid=RMA_RT;
                      if(partsorder2!=null && partsorder2.size()>0)
                          rmaobj.SVMXC__RMA_Shipment_Order__c=partsorder2[0].id;
                      rmaobj.Return_Reason__c=workdetailObj.Return_Reason__c;
                      rmaobj.Tracking_number__c=workdetailObj.TrackingNumber__c;
                      rmaobj.SVMXC__Expected_Quantity2__c=workdetailObj.ReturnedQuantity__c;
                      rmaobj.Free_text_reference__c=workdetailObj.Free_Text_Reference__c;
                      rmaobj.Generic_reference__c = workdetailObj.Generic_Reference__c;
                      rmaobj.BOMaterialReference__c=workdetailObj.PartNumber__c;
                      //rmaobj.SVMXC__Serial_Number__c = shiprec.SVMXC__Serial_Number__c;
                      //rmaobj.SVMXC__Product__c=shiprec.SVMXC__Product__c;
                      rmaList.add(rmaobj);
                    }
                }
                
            }
            if(rmaList != null && rmaList.size()>0){
                insert rmaList;
                for(Integer i=0;i<rmaList.size();i++)                       
                    shipidrmaidmap.put(shipmentRecs[i].id,rmaList[i].id);
            }
            
            for(SVMXC__Service_Order_Line__c wd:wdtoprocess){
                wd.Parts_Order_Line_RMA__c =  shipidrmaidmap.get(wd.PartsOrderLine__c);
            }
            
        }
        
    }
    /******************************************************************************      
Added by Anand
For May-15 Release
Purpose-updating partsorder tracking number Return Reason
****************************************************************************/ 
    
    @future
    Public static void partsordertracknumberupdate(set<id> partset2,set<id> wdId){
        list<SVMXC__RMA_Shipment_Line__c> partlist= new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Line__c> partlist3= new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__Service_Order__c> wolist= new list<SVMXC__Service_Order__c>();
        list<SVMXC__Service_Order_Line__c> wodetailist= new list<SVMXC__Service_Order_Line__c>();
        list<SVMXC__Service_Order_Line__c> wodetailisttoUpdate= new list<SVMXC__Service_Order_Line__c>();
        
        Map<id,SVMXC__Service_Order_Line__c> womap= new Map<id,SVMXC__Service_Order_Line__c>();
        Map<id,SVMXC__RMA_Shipment_Line__c> partorderline = new Map<id,SVMXC__RMA_Shipment_Line__c>();
        Map<id,SVMXC__Service_Order__c> woServiceCompletemap= new Map<id,SVMXC__Service_Order__c>();
        Set<id> pidset =  new Set<id>();
        Set<id> woid =  new Set<id>();
        wodetailist = [select id,Parts_Order_Line_RMA__c,TrackingNumber__c,ReturnedQuantity__c,Return_Reason__c,SVMXC__Service_Order__c from SVMXC__Service_Order_Line__c where id in :wdId];
        system.debug('++++wodetailist' +wodetailist);
        wodetailisttoUpdate = [select id from SVMXC__Service_Order_Line__c where id in :wdId ];
        if(partset2!=null && partset2.size()>0)  
            partlist=[select id, name,Tracking_number__c,SVMXC__Service_Order_Line__c,SVMXC__Expected_Quantity2__c,Return_Reason__c,SVMXC__Service_Order__c from SVMXC__RMA_Shipment_Line__c where id in:partset2];
        if(partlist!=null && partlist.size()>0)
            partorderline.putAll(partlist);
        system.debug('+++++partorderline' +partorderline);
        for(SVMXC__RMA_Shipment_Line__c po:partlist)
        {
            woid.add(po.SVMXC__Service_Order__c);
            
        }
        if(woid.size() > 0 && woid != null )
            wolist = [select SVMXC__Order_Status__c,id from SVMXC__Service_Order__c where id in :woid and SVMXC__Order_Status__c = 'Service Complete' ];
        system.debug('++++wolist'+wolist);
        
         if(wolist!=null && wolist.size()>0)
            woServiceCompletemap.putAll(wolist);
        system.debug('+++++woServiceCompletemap' +woServiceCompletemap);
        
        if(wodetailist != null && wodetailist.size()>0 && woServiceCompletemap.size() > 0 && woServiceCompletemap != null)
        {
            for(SVMXC__Service_Order_Line__c wodetail:wodetailist ){
                if(partorderline.containskey(wodetail.Parts_Order_Line_RMA__c) && (!pidset.contains(wodetail.Parts_Order_Line_RMA__c)) && woServiceCompletemap.containskey(wodetail.SVMXC__Service_Order__c))
                {
                    system.debug('Inside to set expected quantity');
                    SVMXC__RMA_Shipment_Line__c part = partorderline.get(wodetail.Parts_Order_Line_RMA__c);
                    part.Tracking_number__c = wodetail.TrackingNumber__c;
                    part.SVMXC__Expected_Quantity2__c = wodetail.ReturnedQuantity__c;
                    part.Return_Reason__c = wodetail.Return_Reason__c;
                    partlist3.add(part);
                    pidset.add(wodetail.Parts_Order_Line_RMA__c);
                    
                }
            }
            
        }
        if(partlist3!=null && partlist3.size()>0){
            update partlist3;
        }
        /*if( wodetailisttoUpdate.size() > 0 && wodetailisttoUpdate != null )
        {
            for(SVMXC__Service_Order_Line__c wodetail1:wodetailisttoUpdate )
            {
                //wodetail1.FromCompleteWOSFM__c = false;
                
            }
            update wodetailisttoUpdate;
        }*/
    }
    
    /**
    * @author: Nabil ZEGHACHE
    * Date Created: 2016-08-30
    * Description: this method will set the work detail start date/time and end date/time in customer time zone based on the customer start date/time and customer end date/time and the WO time zone.
    * @param aListOfWD A list of work details to be processed.
    * @param aWOMap A map of ID and work orders corresponding to the work details to be processed.
    * -----------------------------------------------------------------
    *                    MODIFICATION HISTORY
    * -----------------------------------------------------------------
    * Modified By: Authors Name
    * Modified Date: Date
    * 
    * Description: Brief Description of Change + BR/Hotfix
    * -----------------------------------------------------------------
    */
    public static void setDateTimeInCustomerTimeZone(List<SVMXC__Service_Order_Line__c> aListOfWD, Map<Id, SVMXC__Service_Order__c> aWOMap) {
        if (aListOfWD != null && aListOfWD.size() > 0 && aWOMap != null) {
            Set<Id> woSet = new Set<Id>(); // variable to hold the list of WOs to which the WDs are tied.
            // Build the set of WOs
            for (SVMXC__Service_Order_Line__c currentWD : aListOfWD) {              
                woSet.add(currentWD.SVMXC__Service_Order__c);
            }
            
            // Create a map to hold the WO ID and the corresponding Time Zone.
            Map<Id, String> woTimeZoneMap = new Map<Id, String>();
            for (SVMXC__Service_Order__c currentWO : aWOMap.values()) {
                woTimeZoneMap.put(currentWO.Id, currentWO.CustomerTimeZone__c);
            }

            String currentTimeZone;
            for (SVMXC__Service_Order_Line__c currentWD : aListOfWD) {
                currentTimeZone = woTimeZoneMap.get(currentWD.SVMXC__Service_Order__c);
                System.debug('#### Current WD: '+currentWD.Name+' ('+currentWD.Id+')');
                if (currentTimeZone != null) {
                    System.debug('#### Time zone found for WO: '+currentWD.SVMXC__Service_Order__r.Name+' ('+currentWD.SVMXC__Service_Order__c+'): '+currentTimeZone);
                    if (currentWD.CustomerStartDateandTime__c != null) {
                        currentWD.CustomerStartDatetimeAtWOTimeZone__c = currentWD.CustomerStartDateandTime__c.format(DATE_TIME_FORMAT, currentTimeZone);
                    }
                    if (currentWD.CustomerEndDateandTime__c != null) {
                        currentWD.CustomerEndDatetimeAtWOTimeZone__c = currentWD.CustomerEndDateandTime__c.format(DATE_TIME_FORMAT, currentTimeZone);
                    } 
                }
                else {
                    System.debug(logginglevel.WARN, 'No time zone found for WO: '+currentWD.SVMXC__Service_Order__r.Name+' ('+currentWD.SVMXC__Service_Order__c+')');
                }
                woSet.add(currentWD.SVMXC__Service_Order__c);
            }
        }
    }
    

    /**
    * @author: Nabil ZEGHACHE
    * Date Created: 2016-09-02
    * Description: this method will set the work detail's FSR when it is empty using the current logged in user if not null or the primary FSR of the related WO otherwise.
    * @param aListOfWD A list of work details to be processed.
    * -----------------------------------------------------------------
    *                    MODIFICATION HISTORY
    * -----------------------------------------------------------------
    * Modified By: Authors Name
    * Modified Date: Date
    * 
    * Description: Brief Description of Change + BR/Hotfix
    * -----------------------------------------------------------------
    */
    public static void setPrimaryFSR(Map<Id, SVMXC__Service_Order__c> aWOMap, List<SVMXC__Service_Order_Line__c> aListOfWD) {
        if (aListOfWD != null && aListOfWD.size() > 0) {
          
          /*
          // Retrieve the list of WO Id to which the current Work Details belong to
          List<Id> woIdList = new List<Id>();
          for (SVMXC__Service_Order_Line__c currentWD : aListOfWD) {
            woIdList.add(currentWD.SVMXC__Service_Order__c);
          }
          
          // Retrieve a Map of associated Work Orders
          Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>(
          [
          select id, SVMXC__Group_Member__c,SVMXC__Group_Member__r.Level__c 
          from SVMXC__Service_Order__c 
          where id = :woIdList
          ]
          );
          */
                    
          // Retrieve the Technician related to the current logged in User
          List<SVMXC__Service_Group_Members__c> techRelatedToCurrentLoggedInUserList = 
          [
          SELECT id, Name ,SVMXC__Salesforce_User__c,Level__c 
          FROM SVMXC__Service_Group_Members__c 
          WHERE SVMXC__Salesforce_User__c = :UserInfo.getUserId() 
          LIMIT 1
          ];
          
          
          if (techRelatedToCurrentLoggedInUserList != null && techRelatedToCurrentLoggedInUserList.size() > 0) {
            // We have an FSR linked to the current logged in user
            SVMXC__Service_Group_Members__c fsr = techRelatedToCurrentLoggedInUserList.get(0);
            if (fsr != null) {
              for (SVMXC__Service_Order_Line__c currentWD : aListOfWD) {
                if (currentWD.SVMXC__Group_Member__c == null) {
                  currentWD.SVMXC__Group_Member__c = fsr.id;
                  currentWD.Level_Required__c = fsr.Level__c;
                }
              }
            }
          }
          else if (aWOMap != null) {
            // We do not have an FSR linked to the current logged in user (it could be a Manager, a planner or a delegated admin entering WD on behalf of FSRs)
            for (SVMXC__Service_Order_Line__c currentWD : aListOfWD) {
              if (currentWD.SVMXC__Group_Member__c == null && currentWD.SVMXC__Service_Order__c != null && aWOMap.get(currentWD.SVMXC__Service_Order__c) != null) {
                currentWD.SVMXC__Group_Member__c = aWOMap.get(currentWD.SVMXC__Service_Order__c).SVMXC__Group_Member__r.Id;
                currentWD.Level_Required__c = aWOMap.get(currentWD.SVMXC__Service_Order__c).SVMXC__Group_Member__r.Level__c;
              }
            }
          }      
        }
    }
    

    public static void setBillableFlagFromWOAndIsAdminFlag(List<SVMXC__Service_Order_Line__c> aWorkDetailList, Map<Id, SVMXC__Service_Order__c> aWOMap) {
        /*
            Method to set Billable flag from value on WO (Written March 25, 2015)
            
            Updated : Feb 15, 2016 - Added in Support to set Is Admin WO Flag
            
        */
        
        if (aWOMap != null && !aWOMap.isEmpty()) {
            for (SVMXC__Service_Order_Line__c sol2 : aWorkDetailList) {
              if (sol2.IsBillable__c == null && aWOMap.get(sol2.SVMXC__Service_Order__c) != null) {
                  String billableFlagOnWO = aWOMap.get(sol2.SVMXC__Service_Order__c).IsBillable__c;
                  if (billableFlagOnWO == 'Yes') {
                      sol2.SVMXC__Is_Billable__c = true;
                      sol2.IsBillable__c = 'Yes';
                  } else if (billableFlagOnWO == 'No') {
                      sol2.SVMXC__Is_Billable__c = false;
                      sol2.IsBillable__c = 'No';
                  }
                  
                  if (aWOMap.containsKey(sol2.SVMXC__Service_Order__c) && aWOMap.get(sol2.SVMXC__Service_Order__c).RecordType.Name == 'Administration Work Order')
                      sol2.Is_For_Admin_Work_Order__c = true;
                
              }
            }
        }
    }

    public static void clearSecondsFromStartAndEndTime(List<SVMXC__Service_Order_Line__c> aListOfWD) { 
        /*
            Method to set clear out the seconds in the start and end time (Written Sept 9, 2014)
            
        */
        
        for (SVMXC__Service_Order_Line__c sol : aListOfWD) {
            if (sol.SVMXC__Start_Date_and_Time__c != null) {
                DateTime workingTime = sol.SVMXC__Start_Date_and_Time__c;
                sol.SVMXC__Start_Date_and_Time__c = DateTime.newInstance(workingTime.year(), workingTime.month(), workingTime.day(), workingTime.hour(), workingTime.minute(), 0);
            }
            if (sol.SVMXC__End_Date_and_Time__c != null) {
                DateTime workingTime2 = sol.SVMXC__End_Date_and_Time__c;
                sol.SVMXC__End_Date_and_Time__c = DateTime.newInstance(workingTime2.year(), workingTime2.month(), workingTime2.day(), workingTime2.hour(), workingTime2.minute(), 0);
            }
        }       
    }

    public static void setTechWhenNull (List<SVMXC__Service_Order_Line__c> triggerNew, Map<Id, SVMXC__Service_Order_Line__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set Technician/Equipment on insert/update if the value is null.  
            It will be set to the running users related tech record if it exists (Written 12/11/2013)
            
            Updated to only consider Actual lines to set the Technician (August 3, 2015)
            
        */
        
        /*
        // can't use the api call because it uses translated values and that could be different depending on user localization
        //actualsRTId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Actuals').getRecordTypeId();
        Id usageConsumptionRTId = [SELECT Name, Id, SobjectType FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' and Name = 'Usage/Consumption'].Id;
             
        Set<Id> WOLIds = new Set<Id>();
        
        for (SVMXC__Service_Order_Line__c sol : triggerNew) {
            if (sol.SVMXC__Group_Member__c == null && sol.RecordTypeId == usageConsumptionRTId)
                WOLIds.add(sol.Id); 
        }
        
        if (!WOLIds.isEmpty()) {
            
            List<SVMXC__Service_Group_Members__c> techList = [SELECT Id FROM SVMXC__Service_Group_Members__c 
                                WHERE SVMXC__Salesforce_User__c =: UserInfo.getUserId()];
            
            if (!techList.isEmpty()) {
                
                // assumes that only one tech record is found for running user
                Id techId = techList[0].Id;
                
                for (SVMXC__Service_Order_Line__c sol1 : triggerNew) {
                    if (WOLIds.contains(sol1.Id) && sol1.SVMXC__Group_Member__c == null)
                        sol1.SVMXC__Group_Member__c = techId;
                }   
            }
        }
        */
    }

    /**
     * Update the SVMXC__Start_Date_and_Time__c (FSR Start date/time) with the CustomerStartDateandTime__c if SVMXC__Start_Date_and_Time__c is empty.
     * Update the SVMXC__End_Date_and_Time__c (FSR End date/time) with the CustomerEndDateandTime__c if SVMXC__End_Date_and_Time__c is empty.
     * @param aListOfWD List of Work Details being saved with the new values 
     */
    public static void setFSRTimesFromCustomerTimes(List<SVMXC__Service_Order_Line__c> aListOfWD) {
        //filter out in order to only deal with Actuals
        //Id usageConsumptionRTId = [SELECT Name, Id, SobjectType FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' and Name = 'Usage/Consumption'].Id;
        //Set<Id> WOLIds = new Set<Id>();
        
        for (SVMXC__Service_Order_Line__c sol : aListOfWD) {
            if (sol.SVMXC__Start_Date_and_Time__c == null && sol.RecordTypeId == actuals_RT) {
                sol.SVMXC__Start_Date_and_Time__c = sol.CustomerStartDateandTime__c; 
            }
            if (sol.SVMXC__End_Date_and_Time__c == null && sol.RecordTypeId == actuals_RT) {
                sol.SVMXC__End_Date_and_Time__c = sol.CustomerEndDateandTime__c; 
            }
             
        }
    }

    
    
}