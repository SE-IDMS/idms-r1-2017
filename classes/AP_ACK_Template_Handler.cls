public class AP_ACK_Template_Handler{
	public static void unCheckOtherDefaultTemplates(Map<Id, AcknowledgmentTemplate__c> MapOldAckTemplates, Map<Id, AcknowledgmentTemplate__c> MapNewAckTemplates,boolean blnIsInsert){
        //*********************************************************************************
        // Method Name   :unCheckOtherDefaultTemplates
        // Purpose       : When User sets one of Acknowledgement Template as Default for a POC, and 
        //                If Default  was already set on a different Acknowledgement Template for the Same PoC, 
        //                then uncheck all other Default for that PoC
        // Created by    : Vimal Karunakaran - Global Delivery Team
        // Date created  : 11th March 2015
        // Modified by   :
        // Date Modified :
        // Remarks       : For April - 15 (Major) Release
        ///*********************************************************************************/
        
        Set<Id> PoCIDSet = new Set<Id>();
        Set<Id> AckTemplateIDSet = new Set<Id>();
        
        for(AcknowledgmentTemplate__c objAckTemplate: MapNewAckTemplates.values()){
			if(blnIsInsert){
				if(MapNewAckTemplates.get(objAckTemplate.id).DefaultTemplate__c){
					PoCIDSet.add(objAckTemplate.PointOfContact__c);
					AckTemplateIDSet.add(objAckTemplate.Id);
				}
			}
			else{
				if((MapOldAckTemplates.get(objAckTemplate.id).DefaultTemplate__c!= MapNewAckTemplates.get(objAckTemplate.id).DefaultTemplate__c) && MapNewAckTemplates.get(objAckTemplate.id).DefaultTemplate__c){
					PoCIDSet.add(objAckTemplate.PointOfContact__c);
					AckTemplateIDSet.add(objAckTemplate.Id);
				}
			}
        }
        
        if(PoCIDSet.size()>0){
            List<AcknowledgmentTemplate__c> lstExistingAckTemplates = new List<AcknowledgmentTemplate__c>([Select Id from AcknowledgmentTemplate__c where DefaultTemplate__c =:true AND  PointOfContact__c =: PoCIDSet AND ID !=:AckTemplateIDSet]);
            if(lstExistingAckTemplates.size()>0){
                for(AcknowledgmentTemplate__c objExistingDefaultAckTemplates: lstExistingAckTemplates){
                    objExistingDefaultAckTemplates.DefaultTemplate__c=false;   
                }
                Database.SaveResult[] ExistingDefaultAckTemplateSaveResult = Database.update(lstExistingAckTemplates,false);
            }
        }
    }
}