/*
    Author          : Renjith Jose
    Date Created    : 29/10/2013
    Description     : Test class for the apex class VFC_UpdateDebitPoints.
*/


@isTest

private class VFC_UpdateDebitPoints_TEST {
        
    static testMethod void burnPointsMethod(){
    
        Test.startTest();
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        insert objContact;
        
        Country__c Country = new Country__c (Name='TCY', CountryCode__c='TCY'); 
        insert country;
        
        CustomerCareTeam__c PrimaryCareTeam = new CustomerCareTeam__c (Name='PrimaryCareTeam', CCCountry__c = Country.Id);
        insert PrimaryCareTeam;
        
        Contract objContract  = Utils_TestMethods.createContract(objAccount.Id, objContact.Id);
        insert objContract;
        objContract.LegacyContractId__c = 'A111111111';
        objContract.Status = 'Activated';        
        update objContract;
        
        CTR_ValueChainPlayers__c objValueChainPlayers = Utils_TestMethods.createCntrctValuChnPlyr(objContract.id);
        insert objValueChainPlayers;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        objCase.CTRValueChainPlayer__c = objValueChainPlayers.id;
        objCase.Points__c = 1;
        objCase.PointDebitReason__c = 'Test Data';
        insert objCase;
                
        PageReference PageRef = new PageReference('/apex/VFP_UpdateDebitPoints?caseId='+objCase.ID);
        Test.setCurrentPage(PageRef); 
        PageReference cPageRef = ApexPages.currentPage();
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC_UpdateDebitPoints burnPoints = new VFC_UpdateDebitPoints(CaseStandardController);
        objCase.FreeOfCharge__c=true;
        update objCase;
        burnPoints.UpdateDebitPoints(); 
        objCase.FreeOfCharge__c=false;
        burnPoints.UpdateDebitPoints();        
        objCase.DebitPoints__c =  true;
        update objCase;
        PageReference PageRef1 = new PageReference('/apex/VFP_UpdateDebitPoints?caseId='+objCase.ID);
        Test.setCurrentPage(PageRef1); 
        PageReference cPageRef1 = ApexPages.currentPage();

        ApexPages.StandardController CaseStandardController1 = new ApexPages.StandardController(objCase);
        VFC_UpdateDebitPoints burnPoints1 = new VFC_UpdateDebitPoints(CaseStandardController1);       
        burnPoints1.UpdateCase();
        burnPoints1.UpdateDebitPoints();
        burnPoints1.ReturnToCase();
        
            
        objCase.CTRValueChainPlayer__c = null;
        objCase.Points__c = 0;
        objCase.PointDebitReason__c = null;
        update objCase;
        /*
        
        PageReference PageRef2 = new PageReference('/apex/VFP_UpdateDebitPoints?caseId='+objCase.ID);
        Test.setCurrentPage(PageRef2); 
        PageReference cPageRef2 = ApexPages.currentPage();
        
        ApexPages.StandardController CaseStandardController2 = new ApexPages.StandardController(objCase);
        VFC_UpdateDebitPoints burnPoints2 = new VFC_UpdateDebitPoints(CaseStandardController2);
        burnPoints2.UpdateDebitPoints();
        
        objCase.Team__c = PrimaryCareTeam.id;
        update objCase;
        PageRef2 = new PageReference('/apex/VFP_UpdateDebitPoints?caseId='+objCase.ID);
        Test.setCurrentPage(PageRef2); 
        cPageRef2 = ApexPages.currentPage();
        
        CaseStandardController2 = new ApexPages.StandardController(objCase);
        burnPoints2 = new VFC_UpdateDebitPoints(CaseStandardController2);
        burnPoints2.UpdateDebitPoints();
        burnPoints2.returnToCase();
        objCase.Status = 'Closed';
        burnPoints2.remainPoints =20000;
        burnPoints2.UpdateCase();
        */
        
        
        Test.stopTest();
    }
    static testMethod void testBurnPointsMethod(){
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        insert objContact;
        
        Country__c Country = new Country__c (Name='TCY', CountryCode__c='TCY'); 
        insert country;
        
        Test.startTest();
        
        CustomerCareTeam__c PrimaryCareTeam = new CustomerCareTeam__c (Name='PrimaryCareTeam', CCCountry__c = Country.Id);
        insert PrimaryCareTeam;
        
        Contract objContract  = Utils_TestMethods.createContract(objAccount.Id, objContact.Id);
        insert objContract;
        objContract.LegacyContractId__c = 'A111111111';
        objContract.Status = 'Activated';
        update objContract;
        
        CTR_ValueChainPlayers__c objValueChainPlayers = Utils_TestMethods.createCntrctValuChnPlyr(objContract.id);
        insert objValueChainPlayers;
        
        CustomerCareTeam__c objPrimaryCareTeam = new CustomerCareTeam__c (Name='FR_TSC', CCCountry__c = Country.Id);
        insert objPrimaryCareTeam;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        objCase.CTRValueChainPlayer__c = objValueChainPlayers.id;
        objCase.Team__c = objPrimaryCareTeam.id;
        objCase.Points__c = 1000;
        objCase.DebitPoints__c = TRUE;
        objCase.PointDebitReason__c = null;
        objCase.CTRValueChainPlayer__c = objValueChainPlayers.id;
        objCase.ProductBU__c = 'BU111';
        objCase.ProductLine__c = 'PRODLI';
        objCase.ProductFamily__c = 'PRODFA';
        objCase.Family__c = 'FAMI';
        objCase.CommercialReference__c = 'CR1234';
        objCase.TECH_GMRCode__c = '1133445557799944';
        
        objCase.PointDebitReason__c = 'Test Data';
        insert objCase;
    
        PageReference PageRef = new PageReference('/apex/VFP_UpdateDebitPoints?caseId='+objCase.ID);
        Test.setCurrentPage(PageRef); 
        PageReference cPageRef = ApexPages.currentPage();
        
        ApexPages.StandardController objCaseStandardController = new ApexPages.StandardController(objCase);
        VFC_UpdateDebitPoints objBurnPoints = new VFC_UpdateDebitPoints(objCaseStandardController);
        objBurnPoints.UpdateCase();
        objBurnPoints.UpdateDebitPoints();
        
      
        Test.stopTest();
    }
}