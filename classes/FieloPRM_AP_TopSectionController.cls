/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 13/04/2016
* Description: 
*******************************************************************/

public with sharing class FieloPRM_AP_TopSectionController{
    
    public List<FieloEE__News__c> contentFeeds {get;set;}
    public String docURL {get;set;}
    public String fieldTitle {get;set;}
    public String fieldExtractText {get;set;}
    public String fieldLinkLabel {get;set;}
    public String fieldGoToLink {get;set;}
    public String welcomeMessage {get;set;}
    
    public FieloEE__Component__c componentObj{
        get{
            return this.componentObj;
        }set{
            if(this.componentObj != value){
                this.componentObj = value;
                if (componentObj != null) {
                    init();
                }    
            }
        }
    }
    
    //Fake CONSTRUCTOR
    public void init(){
        String instance = FieloEE__PublicSettings__c.getOrgDefaults().FieloEE__Instance__c;
        docURL = String.isNotBlank(instance) ? ('//c.' + instance + '.content.force.com') : '';
        
        List<Contact> listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1];
        if(!listContact.isEmpty()){
            welcomeMessage = label.FieloEE.Welcome + ' ' + listContact[0].PRMFirstName__c + ' ' + listContact[0].PRMLastName__c;
        }
        
        fieldTitle       = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__News__c(), 'Title__c',       null);
        fieldExtractText = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__News__c(), 'ExtractText__c', null);
        fieldLinkLabel   = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__News__c(), 'F_PRM_LinkLabel__c',      null);
        fieldGoToLink    = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__News__c(), 'F_PRM_GoToLink__c',       null);
        
        set<String> contentFeedFieldSet = new set<String>{
            'Id',
            'F_PRM_Style__c',
            'F_PRM_PartnerNameWelcomeTitle__c',
            'AttachmentId__c',
            'F_PRM_BackgroundColor__c',
            'F_PRM_ContentFilter__c',
            fieldTitle,
            fieldExtractText,
            fieldLinkLabel,
            fieldGoToLink
        };
        
        Id programId = FieloEE.ProgramUtil.getProgramByDomain().Id;
        Id memberId = FieloEE.MemberUtil.getMemberId();
        Integer limitQuery = componentObj.FieloEE__Layout__c == 'Slider' ? 5 : (componentObj.FieloEE__Layout__c == 'Grid' ? 1 : null);
        String orderQuery = 'F_PRM_Parent__c NULLS FIRST, FieloEE__Order__c';
        
        contentFeeds = FieloEE.NewsService.getNews(contentFeedFieldSet, programId, memberId, componentObj.Id, null, null, limitQuery, null, orderQuery);
    }
    
}