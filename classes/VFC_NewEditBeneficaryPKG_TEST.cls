/**
13-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for VFC_NewEditBeneficaryPKG
 */
@isTest
private class VFC_NewEditBeneficaryPKG_TEST {

    static testMethod void myUnitTest() {
    
        //Test converage for the myPage visualforce page

        PageReference pageRef = Page.VFP_NewEditBeneficiaryPKG;
        
        Test.setCurrentPageReference(pageRef);
        
        // create an instance of the controller
        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        PKGTemplate.PackageDuration__c = 12;
        insert PKGTemplate;
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id , null);
        insert testContract;
        
        CTR_ValueChainPlayers__c CVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        insert CVCP;
        
        Package__c Pkg = Utils_TestMethods.createPackage(testContract.Id , PKGTemplate.Id);
        insert Pkg;
        
        BeneficiaryPAckage__c testBenPKG = Utils_TestMethods.createBeneficiaryPKG(Pkg.Id,CVCP.Id);
        ApexPages.currentPage().getParameters().put('cvcpId',CVCP.Id);      
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(testBenPKG);
        VFC_NewEditBeneficaryPKG myPageCon = new VFC_NewEditBeneficaryPKG(sdtCon);
        
        myPageCon.search();
        
        myPageCon.saveandnew();
        myPageCon.CVCPPkg.package__c =null;
        myPageCon.saveandnew();
       // myPageCon.CVCPPkg.package__c = Pkg.Id;
        //myPageCon.CVCPPkg.ContractValueChainPlayer__c = null;
        //
        myPageCon.pkgId   = Pkg.Id;
        myPageCon.save();
        myPageCon.saveandnew();
        myPageCon.CVCPPkg.package__c = Pkg.Id;
         myPageCon.save();
        myPageCon.SelectPKG();
      
//catch(Exception e)
       // {
      //      System.assert( e.getMessage().contains(System.Label.CLSEP12CCC27), e.getMessage() );
     //   }
        
    }
    
     static testMethod void myUnitTest2() {
    
        //Test converage for the myPage visualforce page

        PageReference pageRef = Page.VFP_NewEditBeneficiaryPKG;
        
        Test.setCurrentPageReference(pageRef);
        
        // create an instance of the controller
        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        PKGTemplate.PackageDuration__c = 12;
        insert PKGTemplate;
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id , null);
        insert testContract;
        
        CTR_ValueChainPlayers__c CVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        insert CVCP;
        
        Package__c Pkg = Utils_TestMethods.createPackage(testContract.Id , PKGTemplate.Id);
        insert Pkg;
        
        BeneficiaryPAckage__c testBenPKG = Utils_TestMethods.createBeneficiaryPKG(Pkg.Id,CVCP.Id);
        insert testBenPKG ;
        ApexPages.currentPage().getParameters().put('cvcpId',CVCP.Id);      
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(testBenPKG);
        VFC_NewEditBeneficaryPKG myPageCon = new VFC_NewEditBeneficaryPKG(sdtCon);
        
        myPageCon.search();
        
        myPageCon.saveandnew();
        myPageCon.CVCPPkg.package__c =null;
        myPageCon.saveandnew();
       // myPageCon.CVCPPkg.package__c = Pkg.Id;
        //myPageCon.CVCPPkg.ContractValueChainPlayer__c = null;
        //
        myPageCon.pkgId   = Pkg.Id;
        myPageCon.save();
        myPageCon.saveandnew();
        myPageCon.CVCPPkg.package__c = Pkg.Id;
         myPageCon.save();
        myPageCon.SelectPKG();
      
//catch(Exception e)
       // {
      //      System.assert( e.getMessage().contains(System.Label.CLSEP12CCC27), e.getMessage() );
     //   }
        
    }
    
        
}