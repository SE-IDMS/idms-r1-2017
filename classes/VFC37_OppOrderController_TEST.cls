@isTest
private class VFC37_OppOrderController_TEST
{    
    static TestMethod void OppOrderControllerTest() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;    
        
        Opportunity opp1 = Utils_TestMethods.createOpportunity (account1.Id);
        insert opp1;
        
        OPP_OrderLink__c OrdLk =  Utils_TestMethods.createOrderLink(opp1.Id);
        insert OrdLk; 
        Profile profile;
        // Profile profile = [select id from profile where name='SE - Salesperson'];        
        // User user = new User(alias = 'user', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
        // lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
        // BypassWF__c = false, timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');                   
        // insert user;
        
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('user','SE - Salesperson');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }
        
        ApexPages.StandardController OrdLkStandardController = new ApexPages.StandardController(OrdLk);
        VFC37_OppOrderController OppOrderController = new VFC37_OppOrderController(OrdLkStandardController);
            
        VCC07_OrderConnectorController OrderController = new VCC07_OrderConnectorController();
        OrderController.OrderMethods = OppOrderController.OppImplementation; 
                    
        /* TESTS */         
          
        OrderController.RelatedRecordAttribute = OrdLk; 
        OrderController.StandardObjectId = OrdLk.ID;
          
        OrderController.getInit();
        OrderController .SelectedOrder = OrdLk;
        OrderController.SearchOrder();
        if(OrderController.FetchedRecords.size()>0){
            OrderController.PerformAction(OrderController.FetchedRecords[0],OrderController);
            OrderController.ActionNumber=2;
            OrderController.PerformAction(OrderController.FetchedRecords[0],OrderController);               
            OrderController.Action();
        }
        OrderController.SelectedOrder = OrdLk;
        OrderController.Action();        
        OrderController.Cancel();
        OrderController.Clear();
        AccountShare aShare = new AccountShare();
        aShare.accountId= account1.Id;
        aShare.UserOrGroupId = user.Id;
        aShare.accountAccessLevel = 'edit';
        aShare.OpportunityAccessLevel = 'read';
        aShare.CaseAccessLevel = 'edit';
        insert aShare;                 
        //OpportunityTeamMember OppTeamMember = Utils_TestMethods.createOppTeamMember(opp1.ID, user.Id);
        system.runAs(user){
            Utils_TestAccessRights.TestRightsOnOpportunity(opp1.ID);
            OrderController.getInit();
            OrderController.SearchOrder();
            OrderController.SelectedOrder = OrdLk;
            OrderController.Action();        
            OrderController.Cancel();
            OrderController.Clear();
        }
        
        Opportunity opp2 = new Opportunity(Name='Test Opp',  AccountId=account1.id , StageName='0 - Closed', 
                            CloseDate=Date.today(), OwnerID = user.id);
        insert opp2;
        system.runAs(user){
            Utils_TestAccessRights.TestRightsOnOpportunity(opp2.ID);
        }
        
        profile = [select id from profile where name='System Administrator'];        
        user = new User(alias = 'user2', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
        lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
        BypassWF__c = false, timezonesidkey='Europe/London', username='user2' + '@bridge-fo.com');
        system.runAs(user){
            Utils_TestAccessRights.TestRightsOnOpportunity(opp1.ID);
        }

    }
}