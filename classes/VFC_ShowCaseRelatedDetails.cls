/*
    Author          : Shruti Karn
    Date Created    : 02/05/2012
    Description     : Controller searches the all open Tasks and Actions related to Case
    
*/
public class VFC_ShowCaseRelatedDetails
{
    public list<CCCAction__c> lstCCCActions{get;set;}
    public list<Task> lstTasks {get;set;}
    public Case currentCase {get;set;}
    public Boolean showMoreTask {get;set;}
    public Boolean showMoreActions {get;set;}
    public String cccAcctionURL {get;set;} // stores the custom label value of CLSEP12CCC03
    public VFC_ShowCaseRelatedDetails(ApexPages.StandardController standardController)
    {
        currentCase = (Case)standardController.getRecord();
        showMoreTask = false;
        showMoreActions = false;
        cccAcctionURL = '/apex/VFP_NewCCCAction?'+System.Label.CLSEP12CCC03+'='+currentCase.Id+'&'+System.Label.CLSEP12CCC02+'='+currentCase.CaseNumber; 
        if(currentCase.Id == null)
        {
            ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.WARNING,'No related records');
            ApexPages.addmessage(msg1); 
        
        }
        else
        {
            lstCCCActions = new list<CCCAction__c>();
            lstTasks = new list<Task>();
            String queryCCCActions = 'Select id, ActionType__c, DueDate__c from CCCAction__c where Case__c= '+'\'' + currentCase.id +'\''+ ' and Status__c != '+'\'' +System.Label.CLSEP12CCC21  +'\''+' order by duedate__c desc limit 101';
            lstCCCActions = database.query(queryCCCActions);
            if(lstCCCActions.size() == 101)
            {
                lstCCCActions.remove(100);
                showMoreActions = true;
            }
            String queryTask = 'Select id, Subject, ActivityDate from Task where whatid= '+'\'' + currentCase.id +'\''+ ' and Status != '+'\'' +System.Label.CLSEP12CCC22  +'\''+' order by ActivityDate desc limit 101';
            lstTasks = database.query(queryTask);
            if(lstTasks.size() == 101)
            {
                lstTasks.remove(100);
                showMoreTask = true;
            }
        }
    }
}