public class FieloPRM_AP_NewsDetailComController{


    

    public Id newsRecordId{get;
        set{
            if(record == null){
                String queryNews = 'SELECT ' +  String.join(listfields, ',') + ' FROM  FieloEE__News__c  WHERE Id = : value limit 1';
                this.record = (FieloEE__News__c) Database.query(queryNews); 
            }   

        }

    }

    public String fieldTitle {get;set;}
    private String lang = FieloEE.OrganizationUtil.getLanguage();


    public FieloEE__News__c record{get;set;}

    public list<String> listfields {get;set;}

    public FieloPRM_AP_NewsDetailComController(){

        //newsRecordId = ApexPages.currentPage().getParameters().get('idNew');

        fieldTitle = 'Title_' + lang + '__c';
        if(!Schema.SObjectType.FieloEE__News__c.fields.getMap().containskey(fieldTitle)){
            fieldTitle = 'FieloEE__Title__c';
        }



        listfields = new list<String>();
        Map<String, Schema.SObjectField> newsFieldsMap = Schema.SObjectType.FieloEE__News__c.fields.getMap();
        
        for(Schema.FieldSetMember field : Schema.SObjectType.FieloEE__News__c.fieldSets.FieloEE__View.getFields()){
           
                string nameAux = field.getFieldPath();
                nameAux =  nameAux.replace('FieloEE__','');
                nameAux =  nameAux.replace('__c','');
                nameAux =  nameAux + '_' +  lang + '__c';
                Boolean bodyExist = newsFieldsMap.containskey(nameAux);
                string fieldOptionalName = '';
                
                if(bodyExist){
                    fieldOptionalName = nameAux;
                }else{
                    fieldOptionalName = string.valueof(field.getFieldPath());
                }

                listfields.add(fieldOptionalName);
           
        }
        
    }
}