public with sharing class CHR_CurrentActorController {

	public ApexPages.StandardSetController controller {get;set;}
	
	public CHR_ChangeReq__c oChangeReq {get;set;}
	
	public List<CHR_ChangeReq__c> lChangeReq {get;set;}
	
	public String retURL {get; set;}

	/**
	* Constructor
	**/	
	public CHR_CurrentActorController (ApexPages.StandardSetController stdController) {
 		retURL = ApexPages.currentPage().getParameters().get('retURL');
 		controller = stdController;
 		// Retrieve the Selected Records
		lChangeReq = (List<CHR_ChangeReq__c>) controller.getSelected();
	}
	
	public CHR_CurrentActorController (ApexPages.StandardController stdController) {
 		retURL = '/'+stdController.getId();
 		list<CHR_ChangeReq__c> setList = new list<CHR_ChangeReq__c>();
 		setList.add((CHR_ChangeReq__c)stdController.getRecord());
 		controller = new ApexPages.StandardSetController(setList);
 		lChangeReq = setList;
	}
	
	

	/**
	* Method updating the selected records
	**/
	public pagereference updateCurrentActor() {
		
		// List containing the record to be updated
		List<CHR_ChangeReq__c> toUpdate = new List<CHR_ChangeReq__c>();
		for (CHR_ChangeReq__c req:lChangeReq) {
			req.CurrentActor__c = UserInfo.getUserId();
			toUpdate.add(req);
		}
		
		// Execute the Update
		List<Database.SaveResult> results = Database.update(toUpdate,false);
		
		// Give information after the updating process
		for(Database.SaveResult result : results){
         	if (result.isSuccess()) {
              	System.debug('Record updated successfully : ' + result.getId());
         	} else if (!result.isSuccess()){
         		Database.Error err = result.getErrors()[0];
         		System.debug('Issue encountered when updating the record : ' + err.getStatusCode()+' '+err.getMessage());
         	}
		}
		
		// Redirect to the same initial page
        PageReference pageRef = new PageReference(retURL);
      	pageRef.setRedirect(true);
      	return pageRef;
	}
}