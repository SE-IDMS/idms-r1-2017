@isTest
private class ConnectCountryCasMilTriggers_Test {
static testMethod void testConnectCountryCascadingMilestone() {
    User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('conCtry1');
     User runAsUser2 = Utils_TestMethods_Connect.createStandardConnectUser('conCtry2');
     insert runAsUser2;
     User runAsUser3 = Utils_TestMethods_Connect.createStandardConnectUser('conCtry3');
     insert runAsUser3;
     System.Debug('The user is:'+runAsUser2);
    
              System.runAs(runAsUser){
                        Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
                        insert inti;
                        
            Country_Champions__c cchamp = new Country_Champions__c(Country__c = 'test',User__c = runAsUser2.id );
             cchamp.Year__c = '2013';
              cchamp.isDefault__c = 'yes';
            insert cchamp;
            System.Debug('The champion is:'+cchamp);
             Country_Champions__c cchamp1 = new Country_Champions__c(Country__c = 'test2',User__c = runAsUser3.id );
             cchamp1.isDefault__c = 'yes';
             cchamp1.Year__c = '2013';
             insert cchamp1;
             cchamp1.Country__c = 'test';
             update cchamp1;
           System.Debug('The champion 1 is:'+cchamp1);
            Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',Global_Functions__c='GM',Global_Business__c='ITB',Power_Region__c='Global Operations');
            insert cp;
            
            Team_Members__c C=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Functions', Entity__c='GM');
            insert C;
            
            
            Team_Members__c C1=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Business', Entity__c='ITB');
            insert C1;
            
            Team_Members__c C2=new Team_Members__c(Program__c=cp.id,Team_Name__c='Power Region', Entity__c='Global Operations');
            insert C2;
            
            Connect_Milestones__c mile=new Connect_Milestones__c(Program_Number__c=cp.id,Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Power_Region__c='Global Operations');
            insert mile;
            
            Cascading_Milestone__c CM=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=mile.id,
            Program_Number__c=cp.id,Program_Scope__c='Global Functions',Entity__c='GM',Progress_Summary_by_Champions__c='Completed');
            insert CM;
            Cascading_Milestone__c CM1=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm1',Milestone_Name__c=mile.id,
            Program_Number__c=cp.id,Program_Scope__c='Operation Regions',Entity__c='Global Operations',Countries__c = 'test,test2',Progress_Summary_by_Champions__c='Completed');
            insert CM1;
            Country_Cascading_Milestone__c CCM=new Country_Cascading_Milestone__c(Country_Cascading_Milestone_Name__c='CCM1',Cascading_Milestone__c=CM1.id,Country_Deployment_Leader__c=label.Connect_Email,Country__c='test');
            insert CCM;
            cchamp.User__c = label.Connect_Email;
            update cchamp;
            CCM.Country_Cascading_Milestone_Name__c='CCM2';
            CCM.Country__c = 'test2';
            CCM.Country_Deployment_Leader__c= runAsUser2.id;
            Update CCM;
            
            
            }
    }
}