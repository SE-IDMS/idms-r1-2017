public class PRJ_updateFundAuthController
{

 public ApexPages.StandardController controller;
    public PRJ_ProjectReq__c oPR{get;set;}
    public String result{get;set;}
    public PRJ_updateFundAuthController(ApexPages.StandardController controller)
    {        
        this.controller = controller;    
        oPR = (PRJ_ProjectReq__c) controller.getRecord();
        
        
    }
    public void callFundAuth()
    {
       system.debug('Inside callFundAuth' );
        
        result = PRJ_CreateDeepClone.PRJ_createDeepClones(oPR.Id);
        if(result=='true')
           result = ' The Project is now moved to Valid. An email has been sent for more information.';
        else if(result == 'false' || result == null)
            result = 'You are not authorized to process this request.To modify Cost & Funding on Funding authorized Project,Please go to your Financial Controller.'; 
        system.debug('the result is : ' + result);
          
      } 
 
            
     
}