public class AP_ITB_Assets_PrimaryAssetCheck{
    public static void setPrimaryAsset(List<ITB_Asset__c> lstITBAssets){
        //*********************************************************************************
        // Method Name  :setPrimaryAsset
        // Purpose      : To set the first Asset as Primary while creating the Assets (Only if no assets are primary in existing attached Assets)
        // Assumption   : The Asstes are attached only through bFO Search Screen
        // Created by   : Vimal Karunakaran - Global Delivery Team
        // Date created : 11th March 2013
        // Modified by :
        // Date Modified :
        // Remarks : For May - 13 (Major) Release
        ///*********************************************************************************/
        
        
        Set<Id> caseIDSet = new Set<Id>();
        
        for(ITB_Asset__c objAsset: lstITBAssets){
            caseIDSet.add(objAsset.Case__c);    
        }    
        
        if(caseIDSet.size()>0){
            List<ITB_Asset__c> lstExistingAssets = new List<ITB_Asset__c>([Select Id from ITB_Asset__c where PrimaryFlag__c =:true AND  Case__c =: caseIDSet]);
            if(lstExistingAssets.size()==0){
                lstITBAssets[0].PrimaryFlag__c=true;
            }
        }
    }
    public static void updateCaseSerialNumber(List<ITB_Asset__c> lstITBAssets){
        //*********************************************************************************
        // Method Name  :updateCaseSerialNumber
        // Purpose      : Update the associated Case with the Serial Number for Primary Asset
        // Assumption   : The Asstes are attached only through bFO Search Screen
        // Created by   : Vimal Karunakaran - Global Delivery Team
        // Date created : 11th March 2013
        // Modified by :
        // Date Modified :
        // Remarks : For May - 13 (Major) Release
        ///*********************************************************************************/
        
        
        Set<Id> caseIDSet = new Set<Id>();
        
        for(ITB_Asset__c objAsset: lstITBAssets){
            if(objAsset.PrimaryFlag__c==true){
                List<Case> lstCase = new List<Case>([Select Id, SerialNumber__c from Case where Id=:objAsset.Case__c LIMIT 1]);
                if (lstCase.size()>0){
                    try{
                        lstCase[0].SerialNumber__c = objAsset.SerialNumber__c;
                        Database.SaveResult[] CaseUpdateResult = Database.update(lstCase, false);
                    }
                    catch(Exception ex){
                        System.debug(ex.getMessage());
                    }
                }
                break;
            }
        }    
    }
    public static void unCheckOtherPrimaryAssets(Map<Id, ITB_Asset__c> MapOldAssets, Map<Id, ITB_Asset__c> MapNewAssets){
        //*********************************************************************************
        // Method Name   :unCheckOtherPrimaryAssets
        // Purpose       : When User sets one of asset as primary asset for a Case, and 
        //                If primary is already set on a different asset for the Same Case, 
        //                then uncheck all other Primary Assets on the Case
        // Created by    : Vimal Karunakaran - Global Delivery Team
        // Date created  : 08th March 2013
        // Modified by   :
        // Date Modified :
        // Remarks       : For May - 13 (Major) Release
        ///*********************************************************************************/
        
        Set<Id> caseIDSet = new Set<Id>();
        Set<Id> AssetIDSet = new Set<Id>();
        
        for(ITB_Asset__c objAsset: MapNewAssets.values()){
            if((MapOldAssets.get(objAsset.id).PrimaryFlag__c!= MapNewAssets.get(objAsset.id).PrimaryFlag__c) && MapNewAssets.get(objAsset.id).PrimaryFlag__c){
                caseIDSet.add(objAsset.Case__c);
                AssetIDSet.add(objAsset.ID);    
            }    
        }
        System.debug('## MapNewAssets.values(): ##' + MapNewAssets.values());
        System.debug('## AssetIDSet ##' + AssetIDSet);
        System.debug('## caseIDSet ##' + caseIDSet);
        
        if(caseIDSet.size()>0){
            List<ITB_Asset__c> lstExistingAssets = new List<ITB_Asset__c>([Select Id from ITB_Asset__c where PrimaryFlag__c =:true AND  Case__c =: caseIDSet AND ID !=:AssetIDSet]);
            if(lstExistingAssets.size()>0){
                for(ITB_Asset__c objExistingPrimaryAsset: lstExistingAssets){
                    objExistingPrimaryAsset.PrimaryFlag__c=false;   
                }
                Database.SaveResult[] ExistingPrimaryAssetSaveResult = Database.update(lstExistingAssets,false);
            }
        }
    }
}