/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  03-Mar-2012
    Modificaion Log     : Added Partner-Region on 09-May-2012
    Description         : Class for Connect Milestone Triggers, Checks and Validates the Scope Selection and Appends the Text columns Roadblock / Decision to be taken and Achievements
    // of past quarter from the respective summary columns.  This code also Auto Creates Cascading Milestones
*/

public with sharing class ConnectMilestoneTriggers
{
    Public Static void ConnectMilestoneBeforeInsertUpdate(List<Connect_Milestones__c> Mile)
      {
      
          System.Debug('****** ConnectMilestoneBeforeInsertUpdate Start ****');
          list<Project_NCP__c> Program = new List<Project_NCP__c>(); 
          
          // Check the Scope Selected for Power Regions
          
          integer PW;
          integer GBs;
          integer GSC_R;
          integer PAReg;
          integer GFn;
          integer SCDs;
          Program = [select Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c, Name,Partner_Region__c,Smart_Cities_Division__c from Project_NCP__c where Year__c = :Mile[0].Year__c];
          for(Connect_Milestones__c M:Mile){
          
          
                 PW = 0; 
                 GBs = 0;
                 GSC_R = 0;
                 GFn = 0;
                 PAReg = 0;
                 SCDs = 0;
                 for(Project_NCP__c P:Program){
                 
                 // Check if the Scope in Null in Program or Milestone
                 
                  if((P.Name == M.Program__c) && (M.Power_Region__c != null) && (P.Power_Region__c == null))
                      M.Validate_Insert_Update__c = True; 
                  else if((P.Name == M.Program__c) && (M.Global_Business__c != null) && (P.Global_Business__c == null))
                      M.Validate_Insert_Update__c = True; 
                  else if((P.Name == M.Program__c) && (M.Global_Functions__c != null) && (P.Global_Functions__c == null))
                       M.Validate_Insert_Update__c = True;
                                     
                // Check the Scope Selected for Power Regions
                   if((P.Name == M.Program__c) && (M.Power_Region__c != null)&& (P.Power_Region__c !=null)){                        
                     for(String PR: M.Power_Region__c.Split(';')){    
                      if(!(P.Power_Region__c.Contains(PR)))
                           PW = PW + 1;                      
                     } // For Loop Ends
                    } //if Ends
                  // Check the Scope Selected for Global Business
                   if((P.Name == M.Program__c) && (M.Global_Business__c != null) && (P.Global_Business__c !=null)){                        
                     for(String GB: M.Global_Business__c.Split(';')){    
                      if(!(P.Global_Business__c.Contains(GB)))
                           GBs = GBs + 1;                      
                     } // For Loop Ends
                    } // If Ends
                    
                  // Check the Scope Selected for Global Function
                   if((P.Name == M.Program__c) && (M.Global_Functions__c != null)&& (P.Global_Functions__c !=null)){                        
                     for(String GF: M.Global_Functions__c.Split(';')){    
                      if(!(P.Global_Functions__c.Contains(GF)))
                           GFn = GFn + 1;                      
                     } // For Loop Ends
                    } // If Ends
                    
                 // Check the Scope Selected for GSC
                   if((P.Name == M.Program__c) && (M.GSC_Regions__c != null) && (P.GSC_Regions__c !=null)){                        
                     for(String GSC: M.GSC_Regions__c.Split(';')){    
                      if(!(P.GSC_Regions__c.Contains(GSC)))
                           GSC_R = GSC_R + 1;                      
                     } // For Loop Ends
                    } // If Ends
                    
                    // Check the Scope Selected for SCD
                   if((P.Name == M.Program__c) && (M.Smart_Cities_Division__c != null) && (P.Smart_Cities_Division__c !=null)){                        
                     for(String SCD: M.Smart_Cities_Division__c.Split(';')){    
                      if(!(P.Smart_Cities_Division__c.Contains(SCD)))
                           SCDs = SCDs + 1;                      
                     } // For Loop Ends
                    } // If Ends
                    
                    
                     // Check the Scope Selected for Partner-Region
                   if((P.Name == M.Program__c) && (M.Partner_Region__c != null) && (P.Partner_Region__c !=null)){                        
                     for(String PAR: M.Partner_Region__c.Split(';')){    
                      if(!(P.Partner_Region__c.Contains(PAR)))
                           PAReg = PAReg + 1;                      
                     } // For Loop Ends
                    } // If Ends
                    
                    
                      if ((PW > 0 ) || (GBs > 0) || (GFn > 0) || (GSC_R > 0) || (PAReg > 0) || (SCDs > 0))
                       M.Validate_Insert_Update__c = True;  
                    } // For Loop Ends
                 
                              
                 }// For Loop Ends
                 
             System.Debug('****** ConnectMilestoneBeforeInsertUpdate End ****');
            
            }   
            
       // Append Text Columns Roadblock Decision to be taken and Acheivements of past quarter from the respective summary columns
            
    /*  Public Static void ConnectMilestoneBeforeInsertUpdate_AppendTextColoumns(List<Connect_Milestones__c> Mile){
      
      System.Debug('****** MilestoneBeforeInsert_AppendTextColoumns Start ****');   
             
             
            List<Connect_Milestones__c> Milestones = new List<Connect_Milestones__c>();
              Milestones = [select Name, Roadblock_Decision_to_be_taken_short__c,Achievements_in__c,Achievements_in_past_quarter_Summary__c from Connect_Milestones__c];
               for(Connect_Milestones__c M:Mile){
               
                            
                  if(M.Roadblock_decision_to_be__c == null)
                    M.Roadblock_decision_to_be__c = '';
                               
                   if(M.Achievements_in__c == null)
                     M.Achievements_in__c = '';
           
              for(Connect_Milestones__c GM:Milestones ){
              
               // Appends Roadblock / Decision to be taken column with the Roadblock /Decision to be (short) column 
              // if it is updated in the list view 
             
                   if(GM.Name == M.Name){
                        if( M.Roadblock_Decision_to_be_taken_Short__c != null){
                               M.Roadblock_decision_to_be__c = M.Roadblock_decision_to_be__c + '\n' + ' - ' + M.Roadblock_Decision_to_be_taken_Short__c;
                               M.Roadblock_Decision_to_be_taken_Short__c = null;
                               } // if Ends
                               
                       // Appends Achievement in past quarter column with the Achievements in past quarter summary column 
                       // if it is updated in the list view 
                 
                         if( M.Achievements_in_past_quarter_Summary__c != null){
                               M.Achievements_in__c = M.Achievements_in__c + '\n' + ' - ' + M.Achievements_in_past_quarter_Summary__c;
                               M.Achievements_in_past_quarter_Summary__c = null;
                               } // if Ends
                               
                            } // if Ends
             
                       } // For Loop Ends
                 
                       } // For Loop Ends
                       
                        System.Debug('****** MilestoneBeforeInsert_AppendTextColoumns Stop ****');   
              
              }  */
              
              
              
 // Description     : Class for Connect Milestone Triggers, Create Cascading Milestones automatically.
 
 /*Public Static void ConnectMilestoneAfterInsertUpdateCreateCascadingMilestone(List<Connect_Milestones__c> Mile){
 
    String GF_Team;
    String GB_Team;
    String PR_Team;
    String GSC_Team;
    String Partner_Team;
    
    
    List<Cascading_Milestone__c> CMilestone = new List<Cascading_Milestone__c>();
    CMilestone = [Select Name, Milestones_Name__c,Milestone_Name__c, Program_Scope__c, Entity__c, GSC_Region__c,Partner_Region__c from Cascading_Milestone__c];
    for(Connect_Milestones__c M:Mile){
            GF_Team = '';
            GB_Team = '';
            PR_Team = '';
            GSC_Team = '';
            Partner_Team = '';
       
          for(Cascading_Milestone__c CMile : CMilestone){
         
           if((CMile.Milestones_Name__c == M.Milestone_Name__c))
           
            {
              System.Debug('GSC_Regions ' + CMile.GSC_Region__c);
              // Check for Global Function Scope - check if a Cascading Milestone already exists for the GF Scope in Parent Milestone
              
               if(M.Global_Functions__c !=null){
                if((CMile.Program_Scope__c == Label.Connect_Global_Functions) && (M.Global_Functions__c.Contains(CMile.Entity__c)))
                    GF_Team = GF_Team + CMile.Entity__c + ';';
                    } // If Ends
            
                // Check for Global Business Scope - check if a Cascading Milestone already exists for the GB Scope in Parent Milestone
              
               if((M.Global_Business__c !=null) && (CMile.Entity__c != null)){
                if((CMile.Program_Scope__c == Label.Connect_Global_Business) && (M.Global_Business__c.Contains(CMile.Entity__c)))
                    GB_Team = GB_Team + CMile.Entity__c + ';';
                    } // If Ends
                        
                // Check for Power Region Scope - check if a Cascading Milestone already exists for the PR Scope in Parent Milestone
              
               if((M.Power_Region__c !=null) && (CMile.Entity__c != null)){
                if((CMile.Program_Scope__c == Label.Connect_Power_Region) && (M.Power_Region__c.Contains(CMile.Entity__c)))
                    PR_Team = PR_Team + CMile.Entity__c + ';';
                    } // If Ends
                
              // Check for GSC Region Scope - check if a Cascading Milestone already exists for the GSC Scope in Parent Milestone
              
               if((M.GSC_Regions__c !=null)&& (CMile.GSC_Region__c != null)){
                if((M.GSC_Regions__c.Contains(CMile.GSC_Region__c)))
                    GSC_Team = GSC_Team + CMile.GSC_Region__c + ';';
                    } // If Ends
                    
              // Check for Partner Region Scope - check if a Cascading Milestone already exists for the GSC Scope in Parent Milestone
              
               if((M.Partner_Region__c !=null)&& (CMile.Partner_Region__c!= null)){
                if((M.Partner_Region__c.Contains(CMile.Partner_Region__c)))
                    Partner_Team = Partner_Team + CMile.Partner_Region__c + ';';
                    } // If Ends
                    
                } // If Ends
               } //Cascading Milestone For Ends
               
                // Check and Create Cascading Milestone for Global Function and GSC Scope
                
                    if((M.Global_Functions__c !=null) && (GF_Team == '')){
                      for(string GF:M.Global_Functions__c.Split(';')){
                       if(GF != Label.Connect_GSC)
                         CreateCascadingMilestone(M,Label.Connect_Global_Functions,GF);
                        else if (GF == Label.Connect_GSC){
                        system.Debug('GSC Region' + M.GSC_Regions__c);
                          if((M.GSC_Regions__c !=null) && (GSC_Team == '')){
                            for(string GSC:M.GSC_Regions__c.Split(';')){
                               
                              CreateCascadingMilestone(M,Label.Connect_GSC,GSC);
                               } // GSC For Ends
                              } // If Ends
                             } // Else If Ends      
                           } // GF For Ends
                          } // If Ends

                     
                     // Check and Create Cascading Milestone for Global Business Scope
                        
                    if((M.Global_Business__c !=null) && (GB_Team == '')){
                      for(string GB:M.Global_Business__c.Split(';')){
                       if(GB != Label.Connect_Partner)
                        CreateCascadingMilestone(M,Label.Connect_Global_Business,GB);
                        
                        else if (GB == Label.Connect_Partner){
                        
                          if((M.Partner_Region__c !=null) && (Partner_Team == '')){
                            for(string PAR:M.Partner_Region__c.Split(';')){                               
                              CreateCascadingMilestone(M,Label.Connect_Partner,PAR);
                            } //Partner-Region For Ends
                           } //Partner-Region IF Ends
                         } // Else If Ends
                      } // GB For Ends
                     } // If Ends   
                     
                   // Check and Create Cascading Milestone for Power Region Scope
                   
                    if((M.Power_Region__c !=null) && (PR_Team == '')){
                      for(string PR:M.Power_Region__c.Split(';')){
                        CreateCascadingMilestone(M,Label.Connect_Power_Region,PR);
                      } // PR For Ends
                     } // If Ends   
                     
                          
                    
                
                // Check for Change in Scope in the Parent Milestone and update(create) respective Cascading Milestone
                   
                   
                   // Check and Create Cascading Milestone for Global Function Scope , if the parent scope is updated
                
                    if((M.Global_Functions__c !=null) && (GF_Team != '')){
                      for(string GF:M.Global_Functions__c.Split(';')){
                       system.debug('------'+GF_Team+'--------'+GF_Team.Contains(GF));
                       if(!(GF_Team.Contains(GF))){ 
                        if(GF != Label.Connect_GSC)                     
                         CreateCascadingMilestone(M,Label.Connect_Global_Functions,GF);
                        else if(GF == Label.Connect_GSC){
                         system.Debug('GSC Team ' + GSC_Team);
                          if((M.GSC_Regions__c !=null) && (GSC_Team != '')){
                              for(string GSC:M.GSC_Regions__c.Split(';')){
                              if(!( GSC_Team.Contains( GSC)))
                                CreateCascadingMilestone(M,Label.Connect_GSC,GSC);
                                 } // GSC For Ends
                                }// GSC If Ends
                              else if((M.GSC_Regions__c !=null) && (GSC_Team == '')){
                            for(string GSC:M.GSC_Regions__c.Split(';')){                               
                              CreateCascadingMilestone(M,Label.Connect_GSC,GSC);
                                 } // GSC For Ends
                                } // If Ends
                               } // Else If Ends   
                              } // GF If Ends
                           else if(GF_Team.Contains(GF) && GF == Label.Connect_GSC){
                             //  System.Debug('GSC-Teams ' + GSC_Team);
                               if((M.GSC_Regions__c !=null) && (GSC_Team != '')){
                                  for(string GSC:M.GSC_Regions__c.Split(';')){
                                  System.Debug('GSC- ' + GSC);
                                  if(!( GSC_Team.Contains( GSC)))
                                    CreateCascadingMilestone(M,Label.Connect_GSC,GSC);
                                    } // GSC For Ends
                                }// GSC If Ends
                               } // Else If Ends   
                      } // GF For Ends
                     } // If Ends
                     
                     // Check and Create Cascading Milestone for Global Business Scope , if the parent scope is updated
                      
                    if((M.Global_Business__c !=null) && (GB_Team != '')){
                      for(string GB:M.Global_Business__c.Split(';')){
                       if(!(GB_Team.Contains(GB))){
                         if(GB != Label.Connect_Partner)    
                            CreateCascadingMilestone(M,Label.Connect_Global_Business,GB);    
                        else if(GB == Label.Connect_Partner){                
                          if((M.Partner_Region__c !=null) && (Partner_Team != '')){
                              for(string PAR:M.Partner_Region__c.Split(';')){
                              if(!( GSC_Team.Contains( PAR)))
                                CreateCascadingMilestone(M,Label.Connect_Partner,PAR);
                                 } // Partner-Region For Ends
                                }// Partner-Region If Ends
                            else if((M.Partner_Region__c !=null) && (Partner_Team == '')){
                            for(string PAR:M.Partner_Region__c.Split(';')){                               
                              CreateCascadingMilestone(M,Label.Connect_Partner,PAR);
                                 } // Partner-Region For Ends
                                } // If Ends
                               } // Else If Ends                            
                         } // GB IF Ends
                         
                         else if(GB_Team.Contains(GB) && GB == Label.Connect_Partner){
                        
                               if((M.Partner_Region__c !=null) && (Partner_Team != '')){
                                  for(string PAR:M.Partner_Region__c.Split(';')){
                               
                                  if(!( Partner_Team.Contains(PAR)))
                                    CreateCascadingMilestone(M,Label.Connect_Partner,PAR);
                                    } // Partner-Region For Ends
                                }// Partner-Region If Ends
                               } // Else If Ends   
                               
                      } // GB For Ends
                     } // If Ends   
                     
                   // Check and Create Cascading Milestone for Power Region Scope , if the parent scope is updated
                   
                    if((M.Power_Region__c !=null) && (PR_Team != '')){
                      for(string PR:M.Power_Region__c.Split(';')){
                      if(!(PR_Team.Contains(PR)))
                        CreateCascadingMilestone(M,Label.Connect_Power_Region,PR);
                      } // PR For Ends
                     } // If Ends   
                     
                                   
                } // Milestone For Ends
               }
  
  // Create Cascading Milestones for the respective Parent Milestone
           
Public static void CreateCascadingMilestone(Connect_Milestones__c GM, String Scope, String Entity){
            
            
            //  Create Cascading Milestone 
            
                Cascading_Milestone__c New_CMile = new Cascading_Milestone__c();
               
                New_CMile.Milestone_Name__r = GM;
                New_CMile.Milestone_Name__c = GM.Id;
                New_CMile.Program_Number__r = GM.Program_Number__r;
                New_CMile.Program_Number__c = GM.Program_ID__c;
                system.debug('Program ID ' + GM.Program_ID__c);
                New_CMile.Cascading_Milestone_Name__c = Entity + ' - ' + GM.Milestone_Name__c;
                
                if(Scope == Label.Connect_Global_Functions){
                    New_CMile.Program_Scope__c = Label.Connect_Global_Functions;
                    New_CMile.Entity__c = Entity;
                    }
                
                if(Scope == Label.Connect_Global_Business){
                    New_CMile.Program_Scope__c = Label.Connect_Global_Business;
                    New_CMile.Entity__c = Entity;
                    }
                
                if(Scope == Label.Connect_Power_Region){
                    New_CMile.Program_Scope__c = Label.Connect_Power_Region;
                    New_CMile.Entity__c = Entity;
                    }
                    
                if(Scope == Label.Connect_GSC){
                  
                    New_CMile.Program_Scope__c = Label.Connect_Global_Functions;
                    New_CMile.Entity__c = Label.Connect_GSC;                
                    New_CMile.GSC_Region__c = Entity;
                    }
                    
                if(Scope == Label.Connect_Partner){
                  
                    New_CMile.Program_Scope__c = Label.Connect_Global_Business;
                    New_CMile.Entity__c = Label.Connect_Partner;                
                    New_CMile.Partner_Region__c = Entity;
                    }
                    
                 try{                    
                    Insert New_CMile;
                    }
                 catch (DMLException e) {
                  System.Debug('Error Creating Cascading Milestone.');
                  }
             }*/
                
      // Check if Child Cascading Milestones Exists before updating / removing scope from parent milestone
                
   Public Static void ConnectGlobalMilestoneBeforeUpdate_CheckCascadingMilestoneScope(List<Connect_Milestones__c> Milenew,List<Connect_Milestones__c> Mileold )
      {
      
          System.Debug('****** ConnectGlobalMilestoneBeforeUpdate_CheckCascadingMilestoneScope Start ****');
          
          List<Cascading_Milestone__c> CMile = New List<Cascading_Milestone__c>();
          
          CMile = [Select Name, Program_Scope__c, Entity__c, GSC_Region__c, Milestones_Name__c,Partner_Region__c,Smart_Cities_Division__c from Cascading_Milestone__c];
          
          for(Connect_Milestones__c M : Milenew){
              for(Cascading_Milestone__c CM: CMile){    
                if(M.Milestone_Name__c == CM.Milestones_Name__c) { 
                  System.Debug('Milestone Updation ' + M.ValidateScopeonUpdate__c);
                   // Check if Entiy in the Milestone is Null for any Scope (All Entities Removed)
                
                if(((M.Global_Functions__c == null)&& (CM.Program_Scope__c == Label.Connect_Global_Functions)) || ((M.Global_Business__c == null)&& (CM.Program_Scope__c == Label.Connect_Global_Business)) || ((M.Power_Region__c == null)&& (CM.Program_Scope__c == Label.Connect_Power_Region))  || ((M.GSC_Regions__c == null)&& (CM.GSC_Region__c !=null))  || ((M.Smart_Cities_Division__c == null)&& (CM.Smart_Cities_Division__c !=null))){
                    M.ValidateScopeonUpdate__c = True;
                    System.Debug('Milestone Updation ');
                     }
                     
                       // Check for Every Scope
                         
                    
             if((M.Global_Functions__c != null) && (CM.Program_Scope__c == Label.Connect_Global_Functions) && (!(M.Global_Functions__c.Contains(CM.Entity__c)))){
                          System.Debug('Cascading Milestone for GF ' + M.Global_Functions__c + '......' + CM.Entity__c);
                          M.ValidateScopeonUpdate__c = True;
                          }
                      else if((M.Global_Business__c != null)&& (CM.Program_Scope__c == Label.Connect_Global_Business) && (!(M.Global_Business__c.Contains(CM.Entity__c))))
                          M.ValidateScopeonUpdate__c = True;
                      else if((M.Power_Region__c != null) && (CM.Program_Scope__c == Label.Connect_Power_Region) && (!(M.Power_Region__c.Contains(CM.Entity__c)))){
                          System.Debug('Milestone Updation ' + CM.Entity__c);
                          M.ValidateScopeonUpdate__c = True;
                          }
                          
                       else if((M.GSC_Regions__c != null)&& (CM.Entity__c == Label.Connect_GSC) && (!(M.GSC_Regions__c.Contains(CM.GSC_Region__c))))
                          M.ValidateScopeonUpdate__c = True;
                          
                          else if((M.Smart_Cities_Division__c != null)&& (CM.Entity__c == Label.Connect_Smart_Cities) && (!(M.Smart_Cities_Division__c.Contains(CM.Smart_Cities_Division__c))))
                          M.ValidateScopeonUpdate__c = True;
                          
                       else if((M.Partner_Region__c != null)&& (CM.Entity__c == Label.Connect_Partner) && (!(M.Partner_Region__c.Contains(CM.Partner_Region__c))))
                          M.ValidateScopeonUpdate__c = True;
                     
                    } // If Ends
                  } // For Ends
                   System.Debug('Milestone Updation ' + M.ValidateScopeonUpdate__c);
                 }// For Ends
                 
                 System.Debug('****** ConnectGlobalMilestoneBeforeUpdate_CheckCascadingMilestoneScope Stop ****');
                }     
     
              // Auto update Progress Summary on Completion of Global Milestone
             
             Public Static void GlobalMilestoneBeforeUpdate_ProgressSummaryUpdate(List<Connect_Milestones__c> GMile) {
             
               for(Connect_Milestones__c M:GMile){
               
                   if ((M.Progress_Summary__c !=null) && (M.Progress_Summary__c == 'Completed')){
                        M.Progress_Summary_Q2__c = M.Progress_Summary__c;
                        M.Progress_Summary_Q3__c = M.Progress_Summary__c;
                        M.Progress_Summary_Q4__c = M.Progress_Summary__c;
                        }
                        
                   if ( (M.Progress_Summary_Q2__c != null) && (M.Progress_Summary_Q2__c == 'Completed')){
                        M.Progress_Summary_Q3__c = M.Progress_Summary_Q2__c;
                        M.Progress_Summary_Q4__c = M.Progress_Summary_Q2__c;
                        }
                        
                   if ( (M.Progress_Summary_Q3__c !=null ) && (M.Progress_Summary_Q3__c == 'Completed')){
                        M.Progress_Summary_Q4__c = M.Progress_Summary_Q3__c;
                        }
                   
                    } // For Loop Ends
                  }
                           

        
      
          }