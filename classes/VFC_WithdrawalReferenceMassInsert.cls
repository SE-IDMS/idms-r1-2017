/*****
Comments :Inserting withdrawal reference 
******/


public class VFC_WithdrawalReferenceMassInsert {

   
    public Boolean IsErr{get;set;}
    public string nameFile{get;set;}
    public Blob ContentFile{get;set;}
    public boolean noFile{get; set;}
    public boolean vFlag{get; set;}
    public Integer totalNoOfHeaders;
    public List<WithdrawalReference__c> validWRs {get; set;}
    public boolean eFlag{get;set;}
    public boolean IsCheck{get; set;}
    public Offer_Lifecycle__c OfferLifecycleObj {get; set;}{OfferLifecycleObj = new Offer_Lifecycle__c();}
    public string RCName{get; set;}
    public boolean afterValidate{get; set;}    
    public boolean IsError{get; set;}
    public boolean canCreate{get; set;}
    public string ImageValue{get; set;}
    public string ValidCheck{get; set;}
    public string poco{get; set;}
    public boolean NoRCs {get; set;}
    public boolean InvalidCommRef{get; set;}
    public boolean InvalidCustRes{get; set;}
    public boolean isDuplicate{get; set;}
    public boolean isNoCommRef{get;set;}
    public boolean validCustResRet{get;set;}
    public boolean validCustResRep{get;set;}
    public boolean isNoCusRes{get;set;}
    public boolean isNoQty{get;set;}
    public integer intRowNumber{get; set;}
    public List<WrapperWithdrawalReference> lstWrapperRIs{get;set;}
    public List<WrapperWithdrawalReference> lstWrapperRIs_AfterValidate{get;set;}
   
    
    map<string, list<string>>mapCaps=new map<string,list<string>>();
    Public list<selectOption> lstSelectOption{get;set;}
    Public string strProdF{get; set;}
    Public string strProductFamily {get; set;}
    set <String> stCustomerResolutions = new set <String> ();
    set <String> stProducFamily = new set <String> ();
    Set <string>setCRpicklistvalues = new Set<String>(); 
    Public set<string> keySet {get; set;}
    set<String> MasterKeySet = new Set<String> ();
    List<CS_WithdrawalSubstitutionCSVMap__c> HeaderValues {get;set;}
    map<String, string> mapCRType = new map<string, string>();
    //March release.
    public boolean blnIsAccess{get;set;}
    
    public VFC_WithdrawalReferenceMassInsert(ApexPages.StandardSetController controller) {
         IsErr=False; 
         //canCreate = false;
         blnIsAccess=false;
         set<Id> userAccSet = new set<Id>();    
         lstWrapperRIs_AfterValidate=new list<WrapperWithdrawalReference>();
         HeaderValues = new List<CS_WithdrawalSubstitutionCSVMap__c> ();  
          id profileId = [Select Id,Name from Profile where name = 'System Administrator'].id;
         if(apexpages.currentpage().getparameters().containskey('Id')) {
            string offerRecordId=ApexPages.CurrentPage().getParameters().get('Id');
            LIST<Offer_Lifecycle__c> offerList =[select Id,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c,Name,BoxSharedLink__c,BoxFolderId__c from Offer_Lifecycle__c where Id=: offerRecordId];
            if(offerList.size() >0) {
                OfferLifecycleObj=offerList[0];
                userAccSet = new set<Id>{OfferLifecycleObj.ownerid,OfferLifecycleObj.Segment_Marketing_VP__c,OfferLifecycleObj.Launch_Area_Manager__c,OfferLifecycleObj.Marketing_Director__c,OfferLifecycleObj.Withdrawal_Owner__c};
            }
         }
         //check the Duplicates
         if(userAccSet.contains(Userinfo.getuserid()) || userinfo.getProfileid()==profileId) {
         list<WithdrawalReference__c> listWithdrawal=[select id,Offer__c,OldCommercialReference__c from WithdrawalReference__c where Offer__c=:OfferLifecycleObj.id];
         
         if(listWithdrawal.size() > 0) {
            existingReference(listWithdrawal);
         }
         //CS_WithdrawalSubstitutionCSVMap__c
         List<CS_WithdrawalSubstitutionCSVMap__c> csList =CS_WithdrawalSubstitutionCSVMap__c.getall().values();
         system.debug('TEST------->'+CS_WithdrawalSubstitutionCSVMap__c.getall().values());
         for(CS_WithdrawalSubstitutionCSVMap__c CsWS:csList ) {
            if(CsWS.Object__C=='WithdrawalReference__c') {
                 //CS_WithdrawalSubstitutionCSVMap__c test=new CS_WithdrawalSubstitutionCSVMap__c ();
                 //test=CsWS;
                 HeaderValues.add(CsWS);
            }
         }
        totalNoOfHeaders=HeaderValues.size();
         }else {
             
               blnIsAccess=True;
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));//   'You dont have access to create Withdrawal'
         }
        system.debug('header value is-->'+HeaderValues);

    }
    
    //Checking Duplicates  // prepare MasterKey Set
    
    public void existingReference(list<WithdrawalReference__c> listExistWithdrawal) {
           string strKey;
         for(WithdrawalReference__c WRObj : listExistWithdrawal ){
         
            if(WRObj.OldCommercialReference__c!=null){
                MasterKeySet.add(WRObj.OldCommercialReference__c);
            }   
         }
     
    } 
    
    //
    //
    public Pagereference displayCSVEntries(){
     
        List<List<String>> CsvList = new List<List<String>>();
        List<String> headerRow=new List<String>();
        afterValidate = False;
        lstWrapperRIs=new list<WrapperWithdrawalReference>();
        
       

        // Check whether CSV content and Parse
        if(ContentFile!=null )
        {
            try{
                CsvList = parseCSV(ContentFile.tostring(),false);
                system.debug('## CsvList after Parsing ##'+ CsvList );
                }
            catch(Exception ex)     
            {
                noFile=True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR43)); // CLOCT13RR43 -> Invalid file content.
            }
        }
        else
        {
            noFile=True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT13RR42)); // CLOCT13RR42->Please Choose File before you Click UploadFile button
            return null;
        }
        system.debug('parsed csv list is --->'+CsvList+'--Size is -->'+CsvList.size());
        // Check Parsed CsvList 
        if(CsvList.isempty() || CsvList.size()==1)
        {
            noFile=True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR44)); //CLOCT13RR44 -> No Data to import.
            return null;
        }
        else
        {
            // get the Header row from the CSV and validate whether columns are as per template
            headerRow=csvList[0];   
            system.debug('@@csvlist size'+ csvList.size());
            if(validateHeaders(headerRow))//if(!validateHeaders(headerRow))
            {
            noFile=True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR45));  //CLOCT13RR45 -> Invalid File
            return null;
            }  
            else
            {
               if(csvList.size()>integer.valueof(Label.CLOCT13RR69))  //CLOCT13RR69 - 400
               {
                   noFile=True;
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR68));  //CLOCT13RR68 -> Number of rows in the CSV File exceeds the Limit.
                   return null;
               }
              
              try{

                 //String CN = ReturnRequestRecord.Case__r.Account.Country__c;    
                 //String ProdCond = ReturnRequestRecord.ProductCondition__c;        
                 String gmr = '';
                    for (Integer i=1;i<CsvList.size();i++)
                    {
                        String[] inputvalues = new String[]{};
                        inputvalues = CsvList[i];   
                        Integer rNum;
                       
                            rNum=i+1;
                        
                       //CommRef- inputvalues[0],ProdF - inputvalues[1], Qty - inputvalues[2], CRS - inputvalues[3], SN - inputvalues[4], DC - inputvalues[5]
                        for(integer k=1; k>=1; k--)
                        { 
                            if(inputvalues.size()<=headerRow.size()-k)
                            {
                                inputvalues[totalNoOfHeaders-k]='';
                            }
                            else
                            {
                                inputvalues[totalNoOfHeaders-k]= removeSpclChar(inputvalues[totalNoOfHeaders-k]).toUpperCase();
                            }
                            system.debug('@@inputvalues['+string.valueof(totalNoOfHeaders-k)+']'+inputvalues[totalNoOfHeaders-k] );
                        }
                        lstWrapperRIs.add(new WrapperWithdrawalReference( rNum,isDuplicate,IsCheck,inputvalues[0],gmr,false,false,false)); 
                    }
                }
              catch(Exception ex)     
              {
                 noFile=True;
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getmessage() + ex.getStackTraceString()));
              }
           }        
        }
        system.debug(' @@ lstWrapperRIs - Before Validation'+ lstWrapperRIs);
        return null;
       }
       
       
       //ParseCSV method helps to Parse the CSV content (blob) into list of Strings.
      private List<List<String>> parseCSV(String contents,Boolean skipHeaders)  {
     
        List<String> fieldList = new List<string>();
        List<List<String>> allFields = new List<List<String>>();
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field        
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try 
        {
            lines = contents.split('\r'); // using carriage return accomodates windows, unix, and mac files
            //http://www.maxi-pedia.com/Line+termination+line+feed+versus+carriage+return+0d0a
        } 
        catch (System.ListException e) 
        {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        
        for(String line: lines) 
        {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            List<String> fields = line.split(',');
            while(fields.size()!=totalNoOfHeaders)  // if the last column in the csv file of any row doesnt have any value then replace that with empty string.
            {
               fields.add('');
            } 
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field: fields) 
            {                
                if (field.startsWith('"') && field.endsWith('"')) 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } 
                else if (field.startsWith('"')) 
                {
                    makeCompositeField = true;
                    compositeField = field;
                } 
                else if (field.endsWith('"')) 
                {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } 
                else if (makeCompositeField) 
                {
                    compositeField +=  ',' + field;
                } 
                else 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
               
        if (skipHeaders) allFields.remove(0);
        if(allFields.size() > 0)
            fieldList = allFields[0];
        return allFields;       
    }
    
    public boolean validateHeaders(List<String> csvHeaderRow){
    
      boolean hFlag = true;
      if(totalNoOfHeaders==csvHeaderRow.size()){
      
          for(integer i=0;i<=csvHeaderRow.size();i++)
          { 
             for(CS_WithdrawalSubstitutionCSVMap__c cs: HeaderValues)
             {
                system.debug('## cs.ColumnNumber__c'+ i+'=' +integer.valueof(cs.ColumnNumber__c) );
                if(i==integer.valueof(cs.ColumnNumber__c)) { if(csvHeaderRow[i]!=cs.Name){hFlag=false;}}
             }
          }
      }
      else { 
        hFlag=false;
      }
      system.debug('##hflag'+ hFlag);
      return hFlag; 
    }
    
    // removeSpclChar method helps to remove the special characters \n and " from the CSV Columns
    @TestVisible private string removeSpclChar(String inputValue) {
   
       if(inputValue.contains('\n')) {inputValue = inputValue.remove('\n');}
       if(inputValue.contains('"')) {inputValue=inputValue.remove('"');}
       return inputValue.trim(); 
    }
    
    // displayReturnItemsAfterValidation method is used to validate and display the CSV data.
    Public void displayWithdrReferenceAfterValidation(){
        try{
        vFlag=true;
        //eFlag = false;
        canCreate = false;
        lstWrapperRIs_AfterValidate=new list<WrapperWithdrawalReference>();
        map<string, List<WrapperWithdrawalReference>> mapWrapperItemsAfterValidation = new map<string, List<WrapperWithdrawalReference>>();
        map<string, string> mapgetCoomercialRef = new map<string, string>();
        keySet = new set<string>();
         List<string> lstCommRef = new List<String>();
        mapWrapperItemsAfterValidation = ValidCSVData();
        
        if(mapWrapperItemsAfterValidation.containskey('Valid')) {
        
            List<WrapperWithdrawalReference> validWrapperReturnItems = mapWrapperItemsAfterValidation.get(Label.CLOCT13RR70);  //CLOCT13RR70 -  Valid
            for(WrapperWithdrawalReference wri : validWrapperReturnItems){
              
                   lstCommRef.add(wri.strWithdrawalComRef);
                 
              }
              
             //Product
             if(lstCommRef.size() > 0) {
                mapgetCoomercialRef = getOldCommRef(lstCommRef);  
             }
             
             for(WrapperWithdrawalReference wri : validWrapperReturnItems) {
                if(mapgetCoomercialRef.containsKey(wri.strWithdrawalComRef)) {
                    IsError = false;
                    IsCheck = false;
                    IsDuplicate = False;
                    
                    InvalidCommRef = true;// it referes "isValidCommRef" in the wrapper
                    
                    lstWrapperRIs_AfterValidate.add(new WrapperWithdrawalReference(wri.intRowNumber,isDuplicate,IsCheck,wri.strWithdrawalComRef,null,InvalidCommRef,IsError,false));   
                }else {
                    validCheck = Label.CLOCT13RR65; //CLOCT13RR65 - 'InvalidCommercialReference' 
                
                }
                
                //
                
                if(validCheck== Label.CLOCT13RR65){
                  
                    if(mapWrapperItemsAfterValidation.containskey(validCheck))
                    {
                      mapWrapperItemsAfterValidation.get(validCheck).add(wri);
                    }
                    else
                    {
                      mapWrapperItemsAfterValidation.put(validCheck, new List<WrapperWithdrawalReference>());
                      mapWrapperItemsAfterValidation.get(validCheck).add(wri);
                    }
                }
                //
                 validCheck='';
             }
             
        
        }
        
        // Process and display all invalid records
       List<WrapperWithdrawalReference> inValidWrapperReturnItems = new List<WrapperWithdrawalReference>();
        for(string mkey: mapWrapperItemsAfterValidation.keySet()) {
       
            if(mkey!='Valid')
            {    validCustResRet=False;
                 validCustResRep=False;
                 isNoCommRef = False;
                 isNoCusRes = False;
                 isNoQty = False;
                 IsDuplicate = False;
                 InvalidCustRes = False;
                 InvalidCommRef = False;
                 NoRCs = False;
                 isError = true;
                 IsCheck=true;  // This flage is always true as these all are invalid records.
                
                 if(mKey==Label.CLOCT13RR66){IsDuplicate=True;}        // CLOCT13RR66 - Duplicate
                 
                 if(mKey==Label.CLOCT13RR65){
                 IsError = true;
                 }   // CLOCT13RR65 - InvalidCommercialReference         //InvalidCommRef = True;                    
                          

                 for(WrapperWithdrawalReference wrapperItem: mapWrapperItemsAfterValidation.get(mkey))
                 {
                    //Wrapper for the invalid records
                    lstWrapperRIs_AfterValidate.add(new WrapperWithdrawalReference(wrapperItem.intRowNumber,isDuplicate,IsCheck,wrapperItem.strWithdrawalComRef,null,InvalidCommRef,IsError,false));   
                      
                 }

            }
       }
       
       if(lstWrapperRIs_AfterValidate.size()>0)
        {
        
            for(WrapperWithdrawalReference wrapperItem: lstWrapperRIs_AfterValidate){
              
                If(wrapperItem.isRCOverride == false) {
                    canCreate=true;
                    break;
                }
            }   
          afterValidate=True;
          sortWrapperItems(lstWrapperRIs_AfterValidate); //call sortWrapperItems method to sort the WrapperItems before it displays
       }
       
        
    }
      catch(Exception ex)  {   
        
          noFile=True;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getmessage() + ex.getStackTraceString()));
        }
     
     //return null;
    
    }
    
    public pagereference insertWithdrReference() {
    
    system.debug(' $$ lstWrapperRIs_AfterValidate $$'+ lstWrapperRIs_AfterValidate );
          validWRs = new List<WithdrawalReference__c>();
          pagereference pr;
          for(WrapperWithdrawalReference wrapperItem: lstWrapperRIs_AfterValidate)
          {
            If(wrapperItem.isRCOverride == false) {//true
                system.debug('-===  entered===');
                WithdrawalReference__c withRefObj = new WithdrawalReference__c(Offer__c=OfferLifecycleObj.id,OldCommercialReference__c=wrapperItem.strWithdrawalComRef);
                //withRefObj.OldCommercialReference__c=wrapperItem.strWithdrawalComRef;
                validWRs.add(withRefObj);
            }
          }
          
          system.debug('valid wrs --->'+validWRs+'--size is -->'+validWRs.size());
          if(validWRs.size() <1)
          {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.CLOCT13RR53));   //CLOCT13RR53 - No records to import
            eFlag=True;
            lstWrapperRIs_AfterValidate.clear();
           displayWithdrReferenceAfterValidation();
            pr = null;
          }
          else
          {
             Insert validWRs;
             pr = new PageReference('/'+OfferLifecycleObj.id );
          }
          return pr;
    
    }
    
    Public map<string, List<WrapperWithdrawalReference>> ValidCSVData(){

      Set<string> keySet = new Set<string>();
      boolean itsInvalidCR;
      itsInvalidCR = true;
      IsDuplicate = false;
      string requestType;
      map<string, List<WrapperWithdrawalReference>> mapWrapperItems = new map<string, List<WrapperWithdrawalReference>>();
      
     
      for(WrapperWithdrawalReference wrapperItem: lstWrapperRIs)  {
     
           
        if(wrapperItem.strWithdrawalComRef==null || wrapperItem.strWithdrawalComRef=='')
         {
            validCheck=Label.CLOCT13RR46;    // CLOCT13RR46 -> NoCommercialRef
         }
         
         else{
        
            IsDuplicate=checkDuplicate(wrapperItem.strWithdrawalComRef);
            system.debug('@@IsDuplicate'+ IsDuplicate);
            if(IsDuplicate) {
                ValidCheck= Label.CLOCT13RR66;
            } // CLOCT13RR66 - Duplicate
         
         }
         if(validCheck!=Label.CLOCT13RR46 && validCheck!=Label.CLOCT13RR66 ) {
         
            validCheck = Label.CLOCT13RR70; //CLOCT13RR70 - Valid
         }
           // create a map with the ValidCheck and WrapperItem
        if(mapWrapperItems.containskey(validCheck)){

            mapWrapperItems.get(validCheck).add(wrapperItem);
        }
        else{

            mapWrapperItems.put(validCheck, new List<WrapperWithdrawalReference>());
            mapWrapperItems.get(validCheck).add(wrapperItem);
        } 
            validCheck='';
      }
        return mapWrapperItems;
    }
    
     Public boolean checkDuplicate(String CommRef){
        String Key;
        key = CommRef;
        if(keySet.size()==0){
            keySet.add(key);
            if(MasterKeySet.contains(key)) {
                isDuplicate=True;
                IsCheck = False;
            }
        }
        else{

            if(keySet.contains(key)|| MasterKeySet.contains(key)){

                isDuplicate=True;
                IsCheck = False;
            }
            else{

                isDuplicate=False;
                keySet.add(key);
            }
        }
        SYSTEM.DEBUG('@@ CHECK DUPLICATE - ISDUPLICATE'+ isDuplicate);
        return IsDuplicate;
    }
    
    
    //Checking Valid commercial  ref
    public  Map<string, string> getOldCommRef(List<String> lstCommRef)
     {
        
        map<String, string> mapOldCommRef = new map<string, string>();
        
         List<Product2>  listOppProduct=[SELECT  Name,ProductCode,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,GDPGMR__c from Product2 where Name In:lstCommRef];
         if(listOppProduct.size() > 0) {
            for(Product2 oppPrd:listOppProduct) {
                mapOldCommRef.put(oppPrd.name,oppPrd.name);
            }
         }
         
         SYSTEM.DEBUG('@@ Hanmanth'+ mapOldCommRef);
        return mapOldCommRef;
     }

     public void sortWrapperItems(List<WrapperWithdrawalReference> lstWrapperRIs_AfterValidate){
  
      boolean swapped=true;
      integer j = 0;
      WrapperWithdrawalReference tmp;
      while (swapped){
        swapped = false;
        j++;
        for (integer i = 0; i < lstWrapperRIs_AfterValidate.size() - j; i++) {
          if (lstWrapperRIs_AfterValidate[i].intRowNumber > lstWrapperRIs_AfterValidate[i + 1].intRowNumber){
               tmp = lstWrapperRIs_AfterValidate[i];
               lstWrapperRIs_AfterValidate[i] = lstWrapperRIs_AfterValidate[i + 1];
               lstWrapperRIs_AfterValidate[i + 1] = tmp;
               swapped = true;
           }
        }
      }
  }
    
    public pagereference docancel()  {
   
       PageReference pr = new PageReference('/'+OfferLifecycleObj.id );
       return pr;
    }
    
    
    //Wrapper class
    
    public class WrapperWithdrawalReference{
        public boolean isRCOverride{get;set;}         
      
        public string  strWithdrawalComRef{get;set;}
       
        public string strImage{get;set;}
        public boolean isValidCommRef{get;set;}
        public boolean Duplicate{get;set;}
       
        public integer intRowNumber{get; set;}
        public boolean isError{get; set;}
        public boolean bNoCommRef{get;set;}
       
       
        public WrapperWithdrawalReference(Integer RowNumber,boolean Dup,boolean isSelected,string OldCommRef, string Image, boolean vCommRef,  boolean Error, boolean noCommRef) 
          {
            isRCOverride = isSelected;
           
            strWithdrawalComRef = OldCommRef;        
            strImage= Image;
            isValidCommRef = vCommRef;          
            Duplicate=Dup;           
            intRowNumber = RowNumber;
            isError = Error;
            bNoCommRef=noCommRef;
           
         }
     
    }
    
}//