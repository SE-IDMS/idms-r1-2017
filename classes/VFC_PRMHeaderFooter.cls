public without sharing class VFC_PRMHeaderFooter {

    public FieloEE__ProgramLayout__c Layout { get; set; }
    public FieloEE__Program__c Program { get; set; }
    public String userName { get; set; }
    public String headerFooterLogo { get; set; }
    
    public List<PRMHeaderService.MenuTreeItem> MainMenu { get; set;}
    public List<PRMHeaderService.MenuTreeItem> TopMenu { get; set; }
    public List<PRMHeaderService.MenuTreeItem> FooterMenu { get; set; }
    public List<PRMHeaderService.MenuTreeItem> SocialMenu { get; set; }

    public VFC_PRMHeaderFooter() {
        String orgId = UserInfo.getOrganizationId();
        String imageId = System.Label.CLJUN16PRM066;
        headerFooterLogo = '/pomp/servlet/servlet.ImageServer?id='+imageId +'&oid='+orgId;
        MainMenu = new List<PRMHeaderService.MenuTreeItem>();
        TopMenu = new List<PRMHeaderService.MenuTreeItem>();
        FooterMenu = new List<PRMHeaderService.MenuTreeItem>();
        SocialMenu = new List<PRMHeaderService.MenuTreeItem>();
        List<FieloEE__ProgramLayout__c> layoutList = new List<FieloEE__ProgramLayout__c>([SELECT FieloEE__BackgroundAttribute__c,FieloEE__BackgroundId__c,FieloEE__BodyBackgroundAttribute__c,FieloEE__BodyBackgroundId__c,FieloEE__Color01__c,FieloEE__Color02__c,FieloEE__Color03__c,FieloEE__Color04__c,FieloEE__Color05__c,FieloEE__Color06__c,FieloEE__Color07__c,FieloEE__Color08__c,FieloEE__Color09__c,FieloEE__Color10__c,FieloEE__Color11__c,FieloEE__ColorFont01__c,FieloEE__ColorFont02__c,FieloEE__ColorFont03__c,FieloEE__ColorFont04__c,FieloEE__ColorFont05__c,FieloEE__ColorFont06__c,FieloEE__ColorFont07__c,FieloEE__ColorFont08__c,FieloEE__ColorFont09__c,FieloEE__ColorFont10__c,FieloEE__ColorFont11__c,FieloEE__CSS__c,FieloEE__Default__c,FieloEE__Font__c,FieloEE__FooterBackgroundAttribute__c,FieloEE__FooterBackgroundId__c,FieloEE__Footer__c,FieloEE__HeaderBackgroundAttribute__c,FieloEE__headerBackgroundId__c,FieloEE__Header__c,FieloEE__Home__c,FieloEE__HTMLHeadLayout__c,FieloEE__Logo__c,FieloEE__Menu__c,FieloEE__Program__c,FieloEE__SearchRewardMenu__c,FieloEE__Segment__c,FieloEE__Target__c,FieloEE__Title__c,Id,IsDeleted,IsLocked,LastModifiedById,LastModifiedDate,MayEdit,Name,OwnerId,SystemModstamp FROM FieloEE__ProgramLayout__c WHERE FieloEE__Program__c = :System.Label.CLAPR15PRM149 AND FieloEE__Target__c = 'Default' LIMIT 1]);
        
        if(layoutList.size() > 0 && layoutList != null)
            Layout = layoutList[0];
            
        User u = [SELECT Id,Name,contactId FROM User WHERE Id = :userinfo.getUserId()];
        system.debug('User:' + u);
        
        userName =u.Name;
        
        Program = FieloPRM_UTILS_ProgramMethods.getProgramByContact(u.contactId);
        system.debug('program:' + program);
        
        if((Program != null) || test.isRunningTest()){
        
            FieloPRM_UTILS_MenuMethods.MenusContainer menuContainer = FieloPRM_UTILS_MenuMethods.getMenus('Main',u.contactId);
            FieloPRM_UTILS_MenuMethods.MenusContainer menuTopContainer = FieloPRM_UTILS_MenuMethods.getMenus('Top',u.contactId);
            FieloPRM_UTILS_MenuMethods.MenusContainer menuFooterContainer = FieloPRM_UTILS_MenuMethods.getMenus('Footer',u.contactId);
            FieloPRM_UTILS_MenuMethods.MenusContainer menuSocialContainer = FieloPRM_UTILS_MenuMethods.getMenus('Social',u.contactId);
            
            MainMenu = PRMHeaderService.fromFieloMenuContainerToMenuItemList(menuContainer,program,false);  
            TopMenu = PRMHeaderService.fromFieloMenuContainerToMenuItemList(menuTopContainer,program,false);
            FooterMenu = PRMHeaderService.fromFieloMenuContainerToMenuItemList(menuFooterContainer,program,false);
            SocialMenu = PRMHeaderService.fromFieloMenuContainerToMenuItemList(menuSocialContainer,program,true);
            
            System.debug('mainMenu :' + mainMenu );
            System.debug('topMenu :' + topMenu );
            System.debug('footerMenu :' + footerMenu );
            System.debug('socialMenu :' + socialMenu );
        }
    }
}