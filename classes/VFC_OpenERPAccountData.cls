/*
06-Nov-2012    Srinivas Nallapati    Initial Creation: BR-2081    Used to display additional account data from the ERP system. Redirects the user to the EPR page

*/
public with sharing class VFC_OpenERPAccountData{
    
    public Account acc {get;set;} {acc = new Account();}
    //To hold the user access on the Account rVFC_OpenERPAccountDataecord
    public boolean UserHasEditAccess = false;
    //Constructor
    public VFC_OpenERPAccountData(ApexPages.StandardController controller) {
        acc = (Account)controller.getRecord();
        acc = [select name,SEAccountID__c, AccountStatus__c from Account where id = :acc.id ];
    }
    
    //This method is called from the VF page action, to redirect user to ERP page after velidating his permission requirements
    public PageReference redirect()
    {
        UserRecordAccess ura = [SELECT RecordId, HasEditAccess,HasReadAccess FROM UserRecordAccess WHERE RecordId = :acc.id AND UserId = :UserInfo.getUserid() ];
        System.Debug(ura+ '  '+acc);
        //Redirect user to ERP page
        if(ura.HasEditAccess &&  acc.SEAccountID__c != null && acc.AccountStatus__c == System.Label.CLDEC12AC05)
        {
            PageReference erpPage = new PageReference(System.Label.CLDEC12AC06);
            //erpPage.getParameters().put('bFoId',acc.SEAccountID__c );
            erpPage.getParameters().put(System.Label.CLDEC12AC10,acc.id);
            return erpPage ;
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12AC07)); 
            
            return null;
        }    
    }
}