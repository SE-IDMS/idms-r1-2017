@isTest(SeeAllData=true)
private class AP_InvokeNotification_TEST{

    static testMethod void AP_InvokeNotificationTestMethod() {
    //Start of Test Data preparation
    map<string,String> mapPLChangeCaseIdsCmi = new  map<string,String>();
List<String> lstCMICaseIds = new List<String>();
        User newUser = Utils_TestMethods.createStandardUser('TestUser');         
        Database.insert(newUser, false);  
        
        User newUser1 = Utils_TestMethods.createStandardUser('TestUser');         
        Database.insert(newUser1, false);
        
        User newUser2 = Utils_TestMethods.createStandardUser('TestUser');         
        Database.insert(newUser2, false);
        
        
        
        User newAccUser = Utils_TestMethods.createStandardUser('TestUser1');         
        Database.insert(newAccUser , false); 
        
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCRRasg');
        Insert LCRTestUser ;
        BusinessRiskEscalationEntity__c  objOrg1 = Utils_TestMethods.createBusinessRiskEscalationEntity();
        objOrg1.Entity__c = 'testentity1';
        insert objOrg1;
        BusinessRiskEscalationEntity__c  objOrg2 = Utils_TestMethods.createBusinessRiskEscalationEntity();
        objOrg2.Entity__c = 'testentity1';
        objOrg2.SubEntity__c= 'testsubentity123';
        insert objOrg2;
        BusinessRiskEscalationEntity__c  objOrg = Utils_TestMethods.createBusinessRiskEscalationEntity();
        objOrg.Entity__c = 'testentity1';
        objOrg.SubEntity__c= 'testsubentity123';
        objOrg.Location__c= 'testlocation12';
        
        insert objOrg;
        BusinessRiskEscalationEntity__c  objOrg6 = Utils_TestMethods.createBusinessRiskEscalationEntity();
        objOrg6.Entity__c = 'testentity1';
        objOrg6.SubEntity__c= 'testsubentity123';
        objOrg6.Location__c= 'testlocation124534345';
        objOrg6.Location_Type__c= 'Customer Care Center';
        objOrg6.VerticalisedCustomerCare__c=true;
        
        insert objOrg6;
        
        EntityStakeholder__c SH1 = Utils_TestMethods.createEntityStakeholder(objOrg.Id,newUser.Id,'CS&Q Manager');
        insert SH1; 
        
        EntityStakeholder__c SH2 = Utils_TestMethods.createEntityStakeholder(objOrg.Id,newUser1.Id,'CMI Informed');
        insert SH2; 
        EntityStakeholder__c SHx = Utils_TestMethods.createEntityStakeholder(objOrg6.Id,newUser1.Id,'CMI Informed');
        insert SHx; 
        EntityStakeholder__c SH3 = Utils_TestMethods.createEntityStakeholder(objOrg.Id,newUser2.Id,'GSA CS&Q Manager');
        insert SH3; 
        EntityStakeholder__c SH4 = Utils_TestMethods.createEntityStakeholder(objOrg.Id,newAccUser.Id,'Potential Safety Issue Reviewer');
        insert SH4; 
        OPP_Product__c ProductObj =Utils_TestMethods.createProduct('Energy', 'INENP - IED ELEC NETWORKS PROT', 'PROTEC RELAY ACCESSORY', 'TEST BLOC P991 RANGE');
        ProductObj.TECH_PM0CodeInGMR__c='TestGMRCode';
        ProductObj.IsActive__c= true;
        insert ProductObj;
        
       ProductLineQualityContactMapping__c lCROrganzObj = new ProductLineQualityContactMapping__c(
                                            AccountableOrganization__c=objOrg.id,
                                            Product__c=ProductObj.id,
                                            QualityContact__c=LCRTestUser.id   
                                                                                    
                                            );
        Insert lCROrganzObj; 
        Country__c objCountry =null;
      /* Country__c objCountry =  Utils_TestMethods.createCountry();
        objCountry.FrontOfficeOrganization__c = objOrg.id;
        Database.insert(objCountry);*/
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            objCountry=countries[0];
        else{               
            objCountry =new country__c(Name='TestCountry',CountryCode__c='IN');
            insert objCountry;    
        }

        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Type='NA';
        objAccount.Account_VIPFlag__c='VIP1';
        objAccount.Country__c = objCountry.id;
        Database.insert(objAccount);
        Account objAccount1 = Utils_TestMethods.createAccount();
        objAccount1.Country__c = objCountry.id;
        objAccount1.Type='GSA';
        objAccount1.Account_VIPFlag__c='VIP1';
        Database.insert(objAccount1);
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
        system.debug('****objAccount***'+objAccount);
        system.debug('****objAccount1***'+objAccount1);
        List<Case> CaseList = new List<Case>();
             Test.startTest();   
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        objOpenCase.CommercialReference__c= 'Test Commercial Reference';
        objOpenCase.ProductBU__c = 'Business';
        objOpenCase.Severity__c = 'Potential Safety Issue';
        objOpenCase.ProductLine__c = 'ProductLine';
        objOpenCase.ReportingOrganization__c=objOrg6.id;
        objOpenCase.ProductFamily__c ='ProductFamily';
        objOpenCase.Family__c ='Family';
        objOpenCase.TECH_GMRCode__c ='Test';//lCROrganzObj.TECH_PLGMRCode__c ;
        Utils_SDF_Methodology.removeFromRunOnce('CaseAfterUpdate');
       // CaseList.add(objOpenCase);
        insert objOpenCase;
        Case objOpenCase1 = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        objOpenCase1.CommercialReference__c= 'Test Commercial Reference';
        objOpenCase1.ProductBU__c = 'Business';
        objOpenCase1.ProductLine__c = 'ProductLine';
        objOpenCase1.ProductFamily__c ='ProductFamily';
        objOpenCase1.ReportingOrganization__c=objOrg6.id;
         objOpenCase1.Severity__c = 'High Financial Impact';
        objOpenCase1.Family__c ='Family';
        objOpenCase1.AssetOwner__c = objAccount1.Id;
        objOpenCase1.TECH_GMRCode__c ='Test';//lCROrganzObj.TECH_PLGMRCode__c ;
        Utils_SDF_Methodology.removeFromRunOnce('CaseAfterUpdate');
        //CaseList.add(objOpenCase1 );
        insert objOpenCase1;
        //INSERT CaseList;
        //system.debug('****case***'+CaseList[0]);
      //  CaseList[0].Severity__c = 'Potential Safety Issue';
      //  update CaseList[0];
        objOpenCase.CustomerMajorIssue__c = 'Yes';
        Utils_SDF_Methodology.removeFromRunOnce('CaseAfterUpdate');
        update objOpenCase;    
        objOpenCase.Severity__c = 'High Financial Impact';
         objOpenCase.CustomerMajorIssue__c = 'To be reviewed';
        update objOpenCase;
        
        objOpenCase.CustomerMajorIssue__c = 'Yes';
        Utils_SDF_Methodology.removeFromRunOnce('CaseAfterUpdate');
        update objOpenCase;           
        objOpenCase1.CustomerMajorIssue__c = 'Yes';
        Utils_SDF_Methodology.removeFromRunOnce('CaseAfterUpdate');
        update objOpenCase1;
        lstCMICaseIds.add(objOpenCase.id);
        lstCMICaseIds.add(objOpenCase1.id); 
          AP_InvokeNotification.invokeCMINotifications(lstCMICaseIds,mapPLChangeCaseIdsCmi);
system.debug('****objOpenCase11***'+objOpenCase);   
system.debug('****objOpenCase12***'+objOpenCase1);  
    //    CaseList[1] .Severity__c = 'High Financial Impact';
    //    update CaseList;
        
   //     CaseList[1] .CustomerMajorIssue__c = 'Yes';
    //    update CaseList;
        //AP_Case_CaseHandler.checkCMI_CSI();
   //     CaseList[1] .Status = 'Closed';
   //     update CaseList;
   objOpenCase.Status = 'Closed';
   Utils_SDF_Methodology.removeFromRunOnce('CaseAfterUpdate');
        update objOpenCase;
        CaseList.add(objOpenCase);
        CaseList.add(objOpenCase1);
        system.debug('****objOpenCase***'+objOpenCase);
        map<id, Case> mapCaseObj = new map<id, Case>();
        
        for(case caseObj:CaseList) {
            mapCaseObj.put(caseObj.id,caseObj);
            
        }
        
        AP_updateMatchingCase.updateCaseStakeholders(mapCaseObj);
Test.StopTest(); 
      /*
        objOpenCase.AssetOwner__c = objAccount.Id;
        list<Case> lstCase = [select id, Severity__c from Case where id=: objOpenCase.Id]; 
        set<string> setIds = new set<String>();
        for(case c: lstCase)
        {
            setIds.add(c.id);
        }      

        Test.startTest();       
        AP_InvokeNotification.invokePMINotifications(setIds);
        AP_InvokeNotification.invokeCMINotifications(setIds);
        Test.stopTest();
        */
    }
}