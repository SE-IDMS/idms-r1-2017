public with sharing class SVMXC_ApprovalsController {
 
	public List<timesheet> timesheetsList {get; set;}
	public class timesheet {
		public Boolean checked {get; set;}
		public String style {get; set;}
		public String delegate {get; set;}
		public SVMXC_Timesheet__c ts {get; set;}
		public Boolean hasRelatedUnapprovedTER {get; set;}	
		public String hasRelatedUnapprovedTERString {get; set;}		
	}

    private List<SVMXC_Timesheet__c> timesheetList;
    
    public List<timeEntryRequest> timeEntryRequestsList {get; set;}
	public class timeEntryRequest {
		public Boolean checked {get; set;}
		public String style {get; set;}
		public String delegate {get; set;}
		public SVMXC_Time_Entry_Request__c ter {get; set;}	
	}

    private List<SVMXC_Time_Entry_Request__c> timeEntryRequestList;

	public Boolean recordsToBeApproved {get; set;}
	
	private List<SelectOption> YesNoList;
	
	public List<SelectOption> getYesNoList() {
		
		if (YesNoList == null) {
			YesNoList = new List<SelectOption>();
			YesNoList.add(new SelectOption('SEL_REQ', 'Make Selection'));	
			YesNoList.add(new SelectOption('Yes', 'Yes'));
			YesNoList.add(new SelectOption('No', 'No'));
		}
		return YesNoList;
	} 
	
	private Set<Id> actorIds;
	
    public Boolean DisplayBlockLevelMessage = false;
    public Boolean getDisplayBlockLevelMessage(){ return DisplayBlockLevelMessage; }
    List<String> ErrorMessages = new  List<String> ();
    public void AddToErrorsList(String msg) { ErrorMessages.add(msg); }
    public List<String> getErrorsList() {    return ErrorMessages; } 
    
    public SVMXC_ApprovalsController() {

		// look to see if the running user is a delegate for other user(s)
		List<User> delegateUsers = [SELECT Name, DelegatedApproverId, Id FROM User WHERE DelegatedApproverId =: UserInfo.getUserId()];
		
		actorIds = new Set<Id>();
		Map <Id, String> UserNameLookupMap = new Map <Id, String>();		
		Map<Id, String> delegateRecordWOUserMap = new Map<Id, String>();
		
		if (!delegateUsers.isEmpty()) {
			for (User u : delegateUsers) {
				actorIds.add(u.Id);
				if (!UserNameLookupMap.containsKey(u.Id))
					UserNameLookupMap.put(u.Id, u.Name);
			}
		}

		// add running user
		if (!actorIds.contains(UserInfo.getUserId()))
			actorIds.add(UserInfo.getUserId());

		System.debug('#################Actor Ids searched : ' + actorIds);

		List<ProcessInstanceWorkitem> outstandingItems = [SELECT SystemModstamp, ProcessInstance.TargetObjectId, ProcessInstanceId, 
															OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById, ActorId 
															FROM ProcessInstanceWorkitem WHERE ActorId IN : actorIds];

		Set<Id> timeSheetIds = new Set<Id>();
		Set<Id> timeEntryReqIds = new Set<Id>();
			
		for (ProcessInstanceWorkitem piw : outstandingItems) {
			
			String objectAPIName = '';
			Id ObjId = piw.ProcessInstance.TargetObjectId;
		 	objectAPIName = ObjId.getSObjectType().getDescribe().getName();
		 	
		 	if (objectAPIName == 'SVMXC_Timesheet__c')
		 		timeSheetIds.add(ObjId);
		 	
		 	if (objectAPIName == 'SVMXC_Time_Entry_Request__c')
		 		timeEntryReqIds.add(ObjId);
		    	
		    // check for delegate record
		   	if (piw.ActorId != UserInfo.getUserId())
		   		delegateRecordWOUserMap.put(ObjId, UserNameLookupMap.get(piw.ActorId));	
		}
	
		recordsToBeApproved = false;
		timesheetsList = new List<timesheet>();
		timeEntryRequestsList = new List<timeEntryRequest>();

		if (!timeEntryReqIds.isEmpty()) {
			
			recordsToBeApproved = true;
			
       		timeEntryRequestList = [SELECT Id, Name, Technician__r.Name, Start_Date__c, End_Date__c, Technician__r.Id, 
       							Activity__c, Notes__c, All_Day_Event__c
                        		FROM SVMXC_Time_Entry_Request__c 
                        		WHERE Id IN : timeEntryReqIds];
        
        	// setup time Entry Request
	        for (SVMXC_Time_Entry_Request__c ter : timeEntryRequestList) {
	        	timeEntryRequest terworking = new timeEntryRequest();
	        	terworking.checked = false;
	        	terworking.ter = ter;
	        
	        	if (delegateRecordWOUserMap.containsKey(ter.Id))
	        		terworking.delegate = 'Yes - on behalf of ' + delegateRecordWOUserMap.get(ter.Id);	    
	        	else
	        		terworking.delegate = 'No';
	        
	        	timeEntryRequestsList.add(terworking);
	        } 
		}	

		if (!timeSheetIds.isEmpty()) {
			
			recordsToBeApproved = true;
			
       		timesheetList = [SELECT Id, Name, Technician__r.Name, Start_Date__c, End_Date__c, Technician__r.Id, Utilization__c, 
                        		LastModifiedById, LastModifiedBy.Name, LastModifiedDate, Reviewed_by_FST_Lead__c, Total_Hours__c
                        		FROM SVMXC_Timesheet__c 
                        		WHERE Id IN : timeSheetIds];
        
        	// setup time Sheet
	        for (SVMXC_Timesheet__c tsl : timesheetList) {
	        	timesheet tsworking = new timesheet();
	        	tsworking.checked = false;
	        	tsworking.ts = tsl;
	    		tsworking.hasRelatedUnapprovedTER = false;
	    		tsworking.hasRelatedUnapprovedTERString = 'No';
	    		
	    		// check if any of the pending Time Entry Requests are related to one of the pending Timesheets
	    		if (!timeEntryReqIds.isEmpty()) {
		    		for (SVMXC_Time_Entry_Request__c ter : timeEntryRequestList) {
		    			Date currentDate = Date.newinstance(ter.Start_Date__c.year(), ter.Start_Date__c.month(), ter.Start_Date__c.day());	
		    		
		    			if (tsl.Start_Date__c <= currentDate && tsl.End_Date__c >= currentDate && tsl.Technician__r == ter.Technician__r) {
		    				tsworking.hasRelatedUnapprovedTER = true;
		    				tsworking.hasRelatedUnapprovedTERString	= 'Yes';
		    				tsworking.style = 'color:red;';
		    				break;
		    			}		
		    		}
	    		}
	    		  
	    		if (!tsworking.hasRelatedUnapprovedTER) {  	  	
		        	if (tsl.Utilization__c > 95)
		        		tsworking.style = 'color:green;';
		        	else if (tsl.Utilization__c > 85)
		        		tsworking.style = 'color:blue;';
		        	else
		        		tsworking.style = 'color:orange;';
	    		}
	        
	        	if (delegateRecordWOUserMap.containsKey(tsl.Id))
	        		tsworking.delegate = 'Yes - on behalf of ' + delegateRecordWOUserMap.get(tsl.Id);	    
	        	else
	        		tsworking.delegate = 'No';
	        
	        	timesheetsList.add(tsworking);
	        } 
		}
		            
        String message = ApexPages.currentPage().getParameters().get('msg');
        
        if(null != message && message.trim().length() > 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '', message));
        }
    }
    
    public void DisplayErrorsNWarnings() {

        if( ErrorMessages.size()>0) {
            DisplayBlockLevelMessage = false; 
            for (String msg : ErrorMessages) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'',msg)) ;  
            }
        }   
    }

    public PageReference Approve() {

        Set<Id> CheckedIdSet = new Set<Id>();        
        Map<String, String> SecondLevelApp = new Map<String, String>();     
        PageReference scPage = new PageReference('/apex/SVMXC_TimesheetApproval');
	
		// find any checked Ids and add them to Master Set
		for (timeEntryRequest ter : timeEntryRequestsList) {
			if (ter.checked)
				CheckedIdSet.add(ter.ter.Id);
		}
		
		Boolean hasRelatedUnapprovedTER = false;
		
		for (timesheet tsl: timesheetsList) {
			if (tsl.checked) {
				if (tsl.hasRelatedUnapprovedTER) {
					hasRelatedUnapprovedTER = true;
					break;	
				} else 
					CheckedIdSet.add(tsl.ts.Id);
			}		
		}
			
        if (CheckedIdSet.size() > 0 && !hasRelatedUnapprovedTER) {
	
				try {
	            	for (ProcessInstanceWorkitem workItem : [SELECT Id, processInstance.TargetObjectId FROM ProcessInstanceWorkitem 
	            												WHERE processInstance.TargetObjectId IN :CheckedIdSet AND ActorId IN : actorIds]) {
	                	Approval.Processworkitemrequest req = new Approval.Processworkitemrequest();
	                    req.setComments('Mass approval');
	                    req.setAction('Approve');
	                    req.setWorkitemId(workItem.Id);
	                    
	                    Approval.Processresult result = Approval.process(req);
	                    
	                    // DEF : WO0000000411423 (AMO, 3th Oct 2016) : disable the condition for the trigger recursivity on insert
	                    // 	the check overlap has to be performed in this particular case
	                    ServiceMaxTimesheetUtils.trg_insert_fired = false;
					}
				} catch (Exception e) {
					scPage = new PageReference('/apex/SVMXC_Approvals?msg=' + EncodingUtil.urlEncode('An exception occured with at least one of the record approvals :  ' + e.getMessage(), 'UTF-8'));
					scPage.setRedirect(true);
        			return scPage; 
				}
                    scPage = new PageReference('/apex/SVMXC_Approvals?msg=' + EncodingUtil.urlEncode('Records(s) successfully approved.', 'UTF-8'));

                } else {
                	if (hasRelatedUnapprovedTER)
                		scPage = new PageReference('/apex/SVMXC_Approvals?msg=' + EncodingUtil.urlEncode('You must first approve/reject the related Time Entry Request before approving the Timesheet.', 'UTF-8'));	
                		
                	else
                		scPage = new PageReference('/apex/SVMXC_Approvals?msg=' + EncodingUtil.urlEncode('No Records(s) selected.', 'UTF-8'));
                }

        scPage.setRedirect(true);

        return scPage;   
    }   
}