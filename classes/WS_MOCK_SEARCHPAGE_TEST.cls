@isTest
global class WS_MOCK_SEARCHPAGE_TEST implements WebServiceMock 
{
   public string respType;
   public WS_MOCK_SEARCHPAGE_TEST(String responseType)
   {
       this.respType = responseType;
       System.debug('>>>>>>respType >>>>>'+respType );
   }
   public List<ApexClass> apexClasses()
   {
       List<ApexClass> acs = [select Id, Name,Body, NamespacePrefix from ApexClass where NamespacePrefix=null limit 1];
       return acs;
   }
   public List<ApexPage> pages()
   {
       List<ApexPage> acs = [select Id, Name,Markup, NamespacePrefix from ApexPage where NamespacePrefix=null limit 1];
       return acs;
   }
   public List<Profile> profiles()
   {
       List<Profile> acs = [select Id, Name from Profile limit 1];
       return acs;
   }
   public List<EmailTemplate> emailTemps()
   {
       List<EmailTemplate> acs = [select Id, Name,Subject,Body, NamespacePrefix from EmailTemplate where NamespacePrefix=null limit 1];
       return acs;
   }
   public List<ApexComponent> components()
   {
       List<ApexComponent> acs = [select Id, Name,Markup, NamespacePrefix from ApexComponent where NamespacePrefix=null limit 1];
       return acs;
   }
   public List<ApexTrigger> triggers()
   {
       List<ApexTrigger> acs = [select Id, Name,Body, NamespacePrefix from ApexTrigger where NamespacePrefix=null limit 1];
       return acs;
   }
   
   public MetadataService.IReadResult valRuleReadResult()
   {
       MetadataService.readMetadata_element request_x = new MetadataService.readMetadata_element();
       request_x.type_x = 'ValidationRule';       
       request_x.fullNames = new List<String>{'Asset.TestrVR'};
       MetadataService.IReadResponseElement response_x;
       MetadataService.ReadValidationRuleResult valRuleRes = new MetadataService.ReadValidationRuleResult();
       MetadataService.ValidationRule[] records1 = new List<MetadataService.ValidationRule>();
       MetadataService.ValidationRule valRule = new MetadataService.ValidationRule();
       valRule.type = 'ValidationRule';
       valRule.fullName = 'TestVR';
       valRule.active = true;
       valRule.description = 'testDesc';
       valRule.errorConditionFormula = 'name!=null';
       valRule.errorDisplayField= 'Name';
       valRule.errorMessage = 'testErrorMessage';
       records1.add(valRule);
       valRuleRes.records = records1;
       MetadataService.readValidationRuleResponse_element respElement = new MetadataService.readValidationRuleResponse_element();
       respElement.result = valRuleRes;
       response_x = respElement;
       Map<String, MetadataService.IReadResponseElement> response_map_x = new Map<String, MetadataService.IReadResponseElement>();
       response_map_x.put('response_x', response_x);
       return respElement.getResult();
       
   }
   
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
           {              
               System.debug('>>>>>>>>>>>>respType '+respType );
               
               if(respType == 'ReadMetadata')
               {
                    MetadataService.listMetadataResponse_element resp = new MetadataService.listMetadataResponse_element();
                    List<MetadataService.FileProperties> fileProperties = new List<MetadataService.FileProperties>();
                    MetadataService.FileProperties fp = new MetadataService.FileProperties();
                    fp.fullName = 'Account.ACC_VR_OCT14ACC01_PrefCCCTeamMandatory';
                    fp.ManageableState = 'unmanaged';
                    fileProperties.add(fp);
                    resp.result = fileProperties;
                    Map<String, MetadataService.listMetadataResponse_element> response_map_x = new Map<String, MetadataService.listMetadataResponse_element>();
                    response_map_x.put('response_x', resp);
                    response.put('response_x', resp);
               }        
               else if(respType == 'Profiles')
               {
                   MetadataService.IReadResponseElement response_x;
                   MetadataService.ReadProfileResult profileRes= new MetadataService.ReadProfileResult();
                   MetadataService.Profile[] records1 = new List<MetadataService.Profile>();
                   MetadataService.Profile prfl = new MetadataService.Profile();
                   MetadataService.ProfileApexClassAccess[] prfApexClassAcc = new List<MetadataService.ProfileApexClassAccess>();
                   MetadataService.ProfileApexPageAccess[] prfApexPageAcc = new List<MetadataService.ProfileApexPageAccess>();
                   
                   MetadataService.ProfileApexClassAccess clAcc = new MetadataService.ProfileApexClassAccess();
                   clAcc.ApexClass = 'TestClass';
                   clAcc.enabled = true;
                   
                   MetadataService.ProfileApexPageAccess pgAcc = new MetadataService.ProfileApexPageAccess();
                   pgAcc.apexPage = 'TestPage';
                   pgAcc.enabled = true;
                   prfApexClassAcc.add(clAcc);
                   prfApexPageAcc.add(pgAcc);
                   
                   prfl.type = 'ValidationRule';
                   prfl.fullName = 'TestProfile';
                   prfl.classAccesses = prfApexClassAcc;
                   prfl.pageAccesses = prfApexPageAcc;
                   records1.add(prfl);
                   profileRes.records = records1;
                   MetadataService.readProfileResponse_element respElement = new MetadataService.readProfileResponse_element();
                   respElement.result = profileRes;
                   response_x = respElement;
                   Map<String, MetadataService.IReadResponseElement> response_map_x = new Map<String, MetadataService.IReadResponseElement>();
                   response_map_x.put('response_x', response_x);
                   response.put('response_x', response_x);
               }        
       }
}