/******************************************************/
/*
/* Question of the Week; 
/*
/* Question Controller
/*
/* Created on: 03/09/2012 SFN
/*
/* Purpose:
/*  handle the QOTWs.
/*
/******************************************************/

public with sharing class VFC_QuestionController 
{    
  
  public List <AnswerChoice> AnswerChoices = new List <AnswerChoice>();
  public List <SelectOption> AnswerOptions = new List <SelectOption>();

  public List <SelectOption> getChoices() 
  {
    return AnswerOptions; 
  }

  public List <AnswerChoice> getAnswerChoices ()    
  {
    return AnswerChoices;    
  }

  public String   Choice          { get; set;} // to be deleted
  
  public String   SelectedChoice          { get; set;}
  public String   Query                   { get; set;}
  public String   Answer                  { get; set;}
  public String   OtherAnswer             { get; set;}
  public boolean  OtherAnswerSelected     { get; set;}
  public String   OtherRemarks            { get; set;}
  public boolean  Declined                { get; set;}
  public boolean  QuestionNotAvailable    { get; set;}
  public boolean  QuestionAlreadyAnswered { get; set;}

  public class AnswerChoice 
  {
    public String  Answer   { get; set;}
    public boolean Selected { get; set;}    
  } 
  
  public Contact             customer { get; set;}
  public QuestionTemplate__c template { get; set;}

  public String OpeningComments { get; set;}
  public String ClosingComments { get; set;}
  public String DisplayStyle    { get; set;}

  /* Constructor */
             
  public VFC_QuestionController (ApexPages.StandardController controller)   
  {      
    customer = (Contact) controller.getRecord();
 
    // Fetch the Active QOTW
    
    List <QOTW__c> questions = [select Id, StartDate__c, EndDate__c from QOTW__c where IsActive__c = true];
    
    if (questions != null && questions.size() == 1)
    {
      // Fetch the Question Templates
      
      QOTW__c question = questions[0];
       
      List <QuestionTemplate__c> templates = [select Id, OpeningComments__c, ClosingComments__c, Question__c, Answers__c, DisplayStyle__c, IncludeOther__c, IncludeCommentsBox__c, Language__c from QuestionTemplate__c where QuestionoftheWeek__c = :question.Id];
    
      if (templates != null && templates.size() != 0)
      {
        // Select the template by Language and Build the Page
        
        //AF Afrikaans
        //AR Arabic
        //AZ Azerbaijani
        //BG Bulgarian
        //CA Catalan
        //CS Czech
        //DA Danish
        //DE German
        //EL Greek
        //EN English
        //ES Spanish
        //ET Estonian
        //FI Finnish
        //FR French
        //HE Hebrew
        //HR Croatian
        //HU Hungarian
        //ID Indonesian
        //IS Icelandic
        //IT Italian
        //JA Japanese
        //JP Japanese
        //KA Georgian
        //KO Korean
        //LT Lithuanian
        //LV Latvian
        //MS Malaysian
        //NL Dutch
        //NO Norwegian
        //PL Polish
        //PT Portuguese
        //RO Romanian
        //RU Russian
        //SH Serbo-Croatian
        //SK Slovakian
        //SL Slovenian
        //SQ Albanian
        //SR Serbian
        //SV Swedish
        //TG Tajik
        //TH Thai
        //TK Turkmen
        //TR Turkish
        //UK Ukrainian
        //UZ Uzbek

        
        // Ensure that the Contact has a Prefered Language or default to English
                
        if (customer.CorrespLang__c == null || customer.CorrespLang__c.length() == 0) customer.CorrespLang__c = 'EN';
        
        // Fetch the matching template by Language
        
        template = null; QuestionTemplate__c englishTemplate = null;
         
        for (QuestionTemplate__c t : templates)
        {
          // Match the Language
          
          if (t.Language__c == 'English') 
          {                    
            englishTemplate = t;
          }

          if (customer.CorrespLang__c == 'AF' && t.Language__c == 'Afrikaans'      ||
              customer.CorrespLang__c == 'AR' && t.Language__c == 'Arabic'         ||
              customer.CorrespLang__c == 'AZ' && t.Language__c == 'Azerbaijani'    ||
              customer.CorrespLang__c == 'BG' && t.Language__c == 'Bulgarian'      ||  
              customer.CorrespLang__c == 'CA' && t.Language__c == 'Catalan'        ||
              customer.CorrespLang__c == 'CS' && t.Language__c == 'Czech'          ||
              customer.CorrespLang__c == 'DA' && t.Language__c == 'Danish'         ||
              customer.CorrespLang__c == 'DE' && t.Language__c == 'German'         ||
              customer.CorrespLang__c == 'EL' && t.Language__c == 'Greek'          ||
              customer.CorrespLang__c == 'EN' && t.Language__c == 'English'        ||
              customer.CorrespLang__c == 'ES' && t.Language__c == 'Spanish'        ||
              customer.CorrespLang__c == 'ET' && t.Language__c == 'Estonian'       ||
              customer.CorrespLang__c == 'FI' && t.Language__c == 'Finnish'        ||
              customer.CorrespLang__c == 'FR' && t.Language__c == 'French'         ||
              customer.CorrespLang__c == 'HE' && t.Language__c == 'Hebrew'         ||
              customer.CorrespLang__c == 'HR' && t.Language__c == 'Croatian'       ||
              customer.CorrespLang__c == 'HU' && t.Language__c == 'Hungarian'      ||   
              customer.CorrespLang__c == 'ID' && t.Language__c == 'Indonesian'     ||
              customer.CorrespLang__c == 'IS' && t.Language__c == 'Icelandic'      ||
              customer.CorrespLang__c == 'IT' && t.Language__c == 'Italian'        ||
              customer.CorrespLang__c == 'JA' && t.Language__c == 'Japanese'       ||
              customer.CorrespLang__c == 'JP' && t.Language__c == 'Japanese'       ||
              customer.CorrespLang__c == 'KA' && t.Language__c == 'Georgian'       ||
              customer.CorrespLang__c == 'KO' && t.Language__c == 'Korean'         ||
              customer.CorrespLang__c == 'LT' && t.Language__c == 'Lithuanian'     ||
              customer.CorrespLang__c == 'LV' && t.Language__c == 'Latvian'        ||
              customer.CorrespLang__c == 'MS' && t.Language__c == 'Malaysian'      ||
              customer.CorrespLang__c == 'NL' && t.Language__c == 'Dutch'          ||
              customer.CorrespLang__c == 'NO' && t.Language__c == 'Norwegian'      ||   
              customer.CorrespLang__c == 'PL' && t.Language__c == 'Polish'         ||
              customer.CorrespLang__c == 'PT' && t.Language__c == 'Portuguese'     ||
              customer.CorrespLang__c == 'RO' && t.Language__c == 'Romanian'       ||
              customer.CorrespLang__c == 'RU' && t.Language__c == 'Russian'        ||
              customer.CorrespLang__c == 'SH' && t.Language__c == 'Serbo-Croatian' ||
              customer.CorrespLang__c == 'SK' && t.Language__c == 'Slovakian'      ||
              customer.CorrespLang__c == 'SL' && t.Language__c == 'Slovenian'      ||
              customer.CorrespLang__c == 'SQ' && t.Language__c == 'Albanian'       ||
              customer.CorrespLang__c == 'SR' && t.Language__c == 'Serbian'        ||
              customer.CorrespLang__c == 'SV' && t.Language__c == 'Swedish'        ||
              customer.CorrespLang__c == 'TG' && t.Language__c == 'Tajik'          ||
              customer.CorrespLang__c == 'TH' && t.Language__c == 'Thai'           ||          
              customer.CorrespLang__c == 'TK' && t.Language__c == 'Turkmen'        ||
              customer.CorrespLang__c == 'TR' && t.Language__c == 'Turkish'        ||
              customer.CorrespLang__c == 'UK' && t.Language__c == 'Ukrainian'      ||
              customer.CorrespLang__c == 'UZ' && t.Language__c == 'Uzbek')
          {                    
            template = t;
          }
        }
        
        // Use English Template if defered Language Template does not exist
        
        if (template == null) template = englishTemplate;
            
        if (template != null)
        {
            List <String> Choices = new List <String>();
            
            Choices = template.Answers__c != null ? template.Answers__c.split('\n') : null;
  
            if (Choices != null)
            {
              AnswerChoice answer = null;
      
              for (String sc : Choices)
              {
                sc = sc.replaceAll(';','');
                sc = sc.replaceAll('\n','');
                sc = sc.replaceAll('\r','');
                
                answer = new AnswerChoice();
        
                answer.Answer   = sc;
                answer.Selected = false;
        
                AnswerChoices.add (answer);
        
                AnswerOptions.add(new SelectOption(sc,sc));
              }
            }
        
            if (template.IncludeOther__c == true)
            {
              AnswerOptions.add(new SelectOption('Other','Other'));
            }
        }
      }
      
      QuestionNotAvailable = false;

      // See if the Contact has already answered the Question
      
      List <CustomerQuickQuestion__c> responses = [select Id, Question__c, Response__c, Remarks__c, Declined__c from CustomerQuickQuestion__c where Contact__c = :customer.Id and QuestionTemplate__c = :template.Id and Declined__c = false];
    
      if (responses != null && responses.size() != 0)
      {
        CustomerQuickQuestion__c previousResponse = responses[0];

        Query  = previousResponse.Question__c != null ? previousResponse.Question__c : '';
        Answer = previousResponse.Response__c != null ? previousResponse.Response__c : '';
    
        OtherRemarks = previousResponse.Remarks__c;
        Declined     = previousResponse.Declined__c;

        QuestionAlreadyAnswered = true;
      }
      else
      {
        QuestionAlreadyAnswered = false;
      }
    }
    else
    {
      // Active QotW is not available
      
      QuestionNotAvailable = true;
    }
  }
  
  public PageReference SaveResponse ()
  {
    // Construct the Customer Response
      
    CustomerQuickQuestion__c response = new CustomerQuickQuestion__c();
    
    response.Contact__c           = customer.Id;
    response.Account__c           = customer.AccountId;
    response.QuestionTemplate__c  = template.Id;
    response.Question__c          = template.Question__c;
    response.Response__c          = '';
    
    if (SelectedChoice != null)
    {
      // Single Choice
      
      response.Response__c = (SelectedChoice == 'Other' ? OtherAnswer : SelectedChoice);
    }
    else
    {
      // Multi-Choice  
    
      for (AnswerChoice c : AnswerChoices)
      {
        if (c.Selected)
        { 
          response.Response__c += (response.Response__c.length() != 0 ? ';' + c.Answer : c.Answer);
        }
      }
          
      if (OtherAnswerSelected != null && OtherAnswerSelected && OtherAnswer != null) response.Response__c += (response.Response__c.length() != 0 ? ';' + OtherAnswer : OtherAnswer);
    }
    
    response.Remarks__c  = OtherRemarks;
    response.Declined__c = Declined;

    try 
    {
      insert response;
    }
    catch (Exception ex)
    {
    }
        
    return null;
  }
   
  // End        
}