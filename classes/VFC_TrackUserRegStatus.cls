public class VFC_TrackUserRegStatus {
    public String userFederatedId { get; set; }
    public List<UserRegTrackingWrapper> URTrackingSearchResult { get; set; }
    public String userRegDetails { get; set; }
    public String userEmailId { get; set; }
    public Integer refIndex { get; set; }
    public String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
    public Map<String,String> ChannelSubChannelMap;
    public Boolean ActionsAvailable {get;set;}
    public UIMSCompany__c AuthenticationTokenUIMSCompany ;
    
    public VFC_TrackUserRegStatus(ApexPages.StandardController controller){
        URTrackingSearchResult = new list<UserRegTrackingWrapper>();
        ChannelSubChannelMap = getChannelSubChannel();
        ActionsAvailable = True;
        AuthenticationTokenUIMSCompany = null;
    }

    public void doSearch(){
        ActionsAvailable = True;
        URTrackingSearchResult.clear();
        Map<String,UIMSCompany__c> fedIdUIMSMap = New  Map<String,UIMSCompany__c>();
        Map<String,User> fedUserMap = New Map<String,User>();
        Map<String,String> fedMemberMap = New Map<String,String>();
        Set<String> federatedId = New Set<String>();
        String qryFilter = '';
        List<String> lFlds = AP_PRMUtils.RegistrationTrackingFields();
        if(!String.isBlank(userEmailId)){
            qryFilter = 'Email__c = \''+userEmailId+'\'';
            String userId =  userEmailId+Label.CLMAR13PRM05;
            /*List<User> searchUser = [SELECT ID,FederationIdentifier FROM User WHERE Username = :userId];
            for(User u:searchUser){
                federatedId.add(u.FederationIdentifier);
            } */
            federatedId.add(userEmailId);
            
        }
        
        if(!String.isBlank(userFederatedId)){
            qryFilter = 'UserFederatedId__c = \''+userFederatedId+'\'';
            federatedId.add(userFederatedId);
        }
        
        if(!federatedId.isEmpty()){
            List<UIMSCompany__c> uIMSSearchResultList = Database.query('SELECT Id,'+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE '+qryFilter+' ORDER BY LastModifiedDate DESC');
                
            /*List<UIMSCompany__c> uIMSSearchResultList = [SELECT InformationMessage__c,UserFederatedId__c,UimsCompanyId__c,BusinessType__c,AreaofFocus__c,DoNotHaveCompany__c,
                                                        SkippedCompanyInfoStep__c,UIMSActivationStatus__c,AccountCreationStatus__c,ContactCreationStatus__c,
                                                        MemberCreationStatus__c,bFOUserCreationStatus__c,AuthenticationToken__c,UserRegistrationInformation__c,bFOContact__c,
                                                        bFOAccount__c,PRMCountryClusterSetup__c,createdDate,RegistrationType__c  FROM UIMSCompany__c WHERE UserFederatedId__c IN :federatedId ORDER BY LastModifiedDate ASC];
            */
            if(!uIMSSearchResultList.isEmpty()){
                for(UIMSCompany__c uCom:uIMSSearchResultList){
                    AuthenticationTokenUIMSCompany = uCom;
                    //fedIdUIMSMap.put(uCom.UserFederatedId__c,uCom);
                    fedIdUIMSMap.put(uCom.Email__c,uCom);
                }
                // Getting the map of FedId and Memberdetais
                fedMemberMap = getMemberDetails(fedIdUIMSMap.keySet());
                List<User> fedUser = [SELECT ID,FederationIdentifier,Email FROM User WHERE Email IN : fedIdUIMSMap.keySet()];
                for(User u:fedUser){
                    //fedUserMap.put(u.FederationIdentifier,u);
                    fedUserMap.put(u.Email,u);
                }
                integer i = 0;
                for(String fedId: fedIdUIMSMap.keySet()){
                    UIMSCompany__c uCom = fedIdUIMSMap.get(fedId);
                    UserRegTrackingWrapper uSrcRes = new UserRegTrackingWrapper();
                    system.debug('***'+fedUserMap.containsKey(uCom.UserFederatedId__c));
                    /*if(fedUserMap.containsKey(uCom.UserFederatedId__c)){
                        uSrcRes.userDetails = sfdcBaseURL+'/'+fedUserMap.get(uCom.UserFederatedId__c).Id;
                    }
                    */
                    if(fedUserMap.containsKey(uCom.Email__c)){
                        uSrcRes.userDetails = sfdcBaseURL+'/'+fedUserMap.get(uCom.Email__c).Id;
                    }
                    else if(String.isNotBlank(uCom.AuthenticationToken__c))
                    uSrcRes.ActivationLink = System.Label.CLOCT15PRM030+'token='+uCom.AuthenticationToken__c+'&federatedId='+uCom.UserFederatedId__c+'&pOverride=1';
                    uSrcRes.index = i;
                    uSrcRes.uCom = uCom;
                    
                    if(String.isNotBlank(uCom.bFOMember__c)) uSrcRes.MemberDetails = sfdcBaseURL+'/'+uCom.bFOMember__c;
                    else if(fedMemberMap.containsKey(uCom.Email__c+'Member')) uSrcRes.MemberDetails = sfdcBaseURL+'/'+fedMemberMap.get(uCom.Email__c+'Member');
                    
                    if(String.isNotBlank(uCom.bFOAccount__c)) uSrcRes.AccountDetails = sfdcBaseURL+'/'+uCom.bFOAccount__c;
                    else if(fedMemberMap.containsKey(uCom.Email__c+'Account')) uSrcRes.AccountDetails = sfdcBaseURL+'/'+fedMemberMap.get(uCom.Email__c+'Account');
                    
                    if(String.isNotBlank(uCom.bFOContact__c)) uSrcRes.ContactDetails = sfdcBaseURL+'/'+uCom.bFOContact__c;
                    
                    else if(fedMemberMap.containsKey(uCom.Email__c+'Contact')) uSrcRes.ContactDetails = sfdcBaseURL+'/'+fedMemberMap.get(uCom.Email__c+'Contact');
                    uSrcRes.businessType = ChannelSubChannelMap.get(uCom.BusinessType__c);
                    uSrcRes.areaOfFocus = ChannelSubChannelMap.get(uCom.AreaofFocus__c);
                    uSrcRes.UserRegModel = getUserRegModel(uCom.UserRegistrationInformation__c);
                    if(uSrcRes.uCom.AccountCreationStatus__c && uSrcRes.uCom.ContactCreationStatus__c && uSrcRes.uCom.MemberCreationStatus__c && uSrcRes.uCom.bFOUserCreationStatus__c){
                        uSrcRes.ProvisionBfoAccessButton = false;
                        system.debug('***'+uSrcRes.ProvisionBfoAccessButton);
                        if(ActionsAvailable) ActionsAvailable = False;
                    }
                    URTrackingSearchResult.add(uSrcRes);
                    i++;
                }
            }
        }
        
        if(URTrackingSearchResult.isEmpty())
        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.CLJUN15PRM001));
        
    }
    
    public Static Map<String,String> getChannelSubChannel(){
        Map<String,String> chnlSubchnl = new Map<String,String>();
        Schema.DescribeFieldResult bType = Account.PRMBusinessType__c.getDescribe();
        Schema.DescribeFieldResult aOFocus = Account.PRMAreaOfFocus__c.getDescribe();
        for(Schema.PicklistEntry ple: bType.getPicklistValues()){
            chnlSubchnl.put(ple.getValue(),ple.getLabel());
        }
        for(Schema.PicklistEntry ple1: aOFocus.getPicklistValues()){
            chnlSubchnl.put(ple1.getValue(),ple1.getLabel());
        }
        return chnlSubchnl;
    }

    @RemoteAction
    public Static Id doProvisionbFOAccess(String email){
        Id jobId;
        List<String> lFlds;  
        lFlds = AP_PRMUtils.RegistrationTrackingFields();
        UIMSCompany__c AuthenticationTokenUIMSCompany = Database.query('SELECT Id,'+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE Email__c = \''+ email +'\' ORDER BY LastModifiedDate DESC');
        String FederateId = String.isNotBlank(email)? email:AuthenticationTokenUIMSCompany.Email__c;
        List<UIMSCompany__c> lCmp = new List<UIMSCompany__c>();
        lCmp = Database.query('SELECT Id,'+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE Email__c = \''+FederateId+'\' ORDER BY LastModifiedDate DESC LIMIT 1');
        System.debug('**lCmp** :'+lCmp);
        AP_UserRegModel userregModel;
        if (!lCmp.isEmpty()) {
           userregModel = (AP_UserRegModel)JSON.deserialize(lCmp[0].UserRegistrationInformation__c, AP_UserRegModel.class);
           System.debug('**userregModel123** :'+userregModel);
        }   
        system.debug('*****The AP User Reg Model ***'+userregModel);
        if(userregModel != Null){   
           jobId = AP_PRMUtils.InitiateRegistrationProcessing(userregModel,lCmp[0]);
           System.debug('**JobId123**'+jobId);                  
        }   
        return jobId;
    }
    
    @RemoteAction
    public Static String jobtimeoutStatus(Id jobIds) {
        String jobStatus;
        System.debug('jobId123 :'+jobIds);
        AsyncApexJob jobRec= [SELECT Id,Status FROM AsyncApexJob WHERE Id =: jobIds Limit 1];
        System.debug('jobRec123  :'+jobRec);
        if(jobRec != Null){
            jobStatus = jobRec.Status;
            System.debug('jobStatus123  :'+jobStatus);
        }       
        return jobStatus;
    }

    public Map<String,String> getMemberDetails(Set<String> userFedId){
        Map<String,String> fedMemMap = New Map<String,String>();
        List<Contact> conList = [Select Id,FieloEE__Member__c,PRMUIMSID__c,AccountID FROM Contact WHERE PRMUIMSID__c = :userFedId];
        System.debug('The contact values are '+conList.Size());
        for(Contact con:conList){
            if(!string.isempty(con.FieloEE__Member__c)) fedMemMap.put(con.PRMUIMSID__c+'Member',con.FieloEE__Member__c);
            if(!string.isempty(con.AccountID)) fedMemMap.put(con.PRMUIMSID__c+'Account',con.AccountID);
            fedMemMap.put(con.PRMUIMSID__c+'Contact',con.Id);
        }
        return fedMemMap;
    }
    
    public List<String> getUserRegModel(String serlizedString){
        List<String> UserRegModel = New List<String>();
        if(!String.isBlank(serlizedString)){
            serlizedString = serlizedString.remove('"');
            serlizedString = serlizedString.remove('{');
            serlizedString = serlizedString.remove('}');
            String[] unFormattedList = serlizedString.split(',');
            for(String s:unFormattedList){
                if(!s.contains(':null') && !s.contains(':false')) UserRegModel.add(s);
            }
        }
        return UserRegModel;
    }
    
    public class UserRegTrackingWrapper {
        public UIMSCompany__c uCom {get;set;}
        public Boolean ProvisionBfoAccessButton {get;set;}
        public String userDetails {get;set;}
        public Integer index {get;set;}
        public String businessType {get; set;}
        public String areaOfFocus {get; set;}
        public String MemberDetails {get;set;}
        public String AccountDetails {get;set;}
        public String ContactDetails {get;set;}
        public List<String> UserRegModel {get;set;}
        public String ActivationLink {get;set;}
        public UserRegTrackingWrapper() {
            uCom = new UIMSCompany__c();
            ProvisionBfoAccessButton = true;
            userDetails = Null;
            MemberDetails = Null;
            AccountDetails = Null;
            ContactDetails = Null;
            ActivationLink = Null;
        }
    }

}