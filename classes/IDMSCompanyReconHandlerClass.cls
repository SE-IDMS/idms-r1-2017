global class IDMSCompanyReconHandlerClass {
    public static void idmsCompanyToRecon(string resErrorMsg, string idmsUserId){
        System.debug('****Inside create logs*****');
        try{
            IDMSCompanyReconciliationBatchHandler__c companyToReconciliate = new IDMSCompanyReconciliationBatchHandler__c();
            companyToReconciliate.AtCreate__c = true;
            companyToReconciliate.AtUpdate__c = false;
            companyToReconciliate.HttpMessage__c = resErrorMsg;
            companyToReconciliate.IdmsUser__c = idmsUserId;
            companyToReconciliate.NbrAttempts__c = 1;
            insert companyToReconciliate;
            System.debug('*****Company handler has been created***'+companyToReconciliate);
        }catch (DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
}