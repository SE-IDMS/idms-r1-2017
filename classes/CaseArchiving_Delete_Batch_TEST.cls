@isTest
private class CaseArchiving_Delete_Batch_TEST{
     static testMethod void deleteBatch(){
          Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
                 
          contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
          Database.insert(con);     
    
          Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'closed');
          Database.insert(caseobj);
          
          caseobj.ArchivedStatus__c='Success';
          update caseobj;
          
          Test.startTest();
          CaseArchiving_Delete_Batch  cadb = new CaseArchiving_Delete_Batch (); 
          ID batchprocessid =Database.executeBatch(cadb);
          Test.stopTest();    
             
         
         
     
     }
     static testmethod void deleteBatchScheduleTestmethod() {
        Test.startTest();
        String CRON_EXP = '0 0 0 1 1 ? 2025';  
        String jobId = System.schedule('Schdule casearchiving_delete_Batch', CRON_EXP, new casearchiving_Delete_BatchSchedule() );
        Test.stopTest();
    }
}