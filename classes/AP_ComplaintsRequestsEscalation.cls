/*****
Hanamanth
*****/


public class AP_ComplaintsRequestsEscalation {

    public static boolean isExcecute=false; 
    public static void updateComplaintsRequestCRS(List<ComplaintsRequestsEscalationHistory__c> listCRH) {
        isExcecute=false;
        set<id> compRequsSet= new set<id> ();
        Map<string,ComplaintsRequestsEscalationHistory__c>  mapToOrg = new  Map<string,ComplaintsRequestsEscalationHistory__c>();
        map<id,ComplaintsRequestsEscalationHistory__c> mapCREH = new map<id,ComplaintsRequestsEscalationHistory__c>();
        List<ComplaintsRequestsEscalationHistory__c>  lisCrCRH = new List<ComplaintsRequestsEscalationHistory__c>();
        map<id,string>  mapCpmReq=new  map<id,string>();
        id AccountableOrg;
        string Depatrtment;
        for(ComplaintsRequestsEscalationHistory__c cEH:listCRH) {
                 compRequsSet.add(cEH.ComplaintRequest__c);
        }
        
        lisCrCRH=[select id,name,ComplaintRequest__c,From_IssueOwner__c,ContactEmailofFromAcctOrg__c,ToDepartment__c,Department__c, ComplaintRequest__r.AccountableOrganization__c,ToOrg__c, FromOrg__c, ComplaintRequest__r.Department__c, EscalationLevel__c, OrgSelectable__c, KeepInvolved__c  from ComplaintsRequestsEscalationHistory__c where ComplaintRequest__c in :compRequsSet and Event_Type__c != :Label.CLAPR15I2P07  order by Event_Date__c ASC];
    
        for(ComplaintsRequestsEscalationHistory__c cobj:lisCrCRH) {
               mapCpmReq.put(cobj.ComplaintRequest__r.AccountableOrganization__c,cobj.ComplaintRequest__r.Department__c);
               AccountableOrg=cobj.ComplaintRequest__r.AccountableOrganization__c;
               Depatrtment=cobj.ComplaintRequest__r.Department__c;
             }
         List<ComplaintsRequestsEscalationHistory__c>  lisAccOrgCrCRH = new List<ComplaintsRequestsEscalationHistory__c>();
        system.debug('TEST 67---->1'+mapCpmReq);
      
        boolean isbreak;
        
        for(ComplaintsRequestsEscalationHistory__c cREHObj:lisCrCRH) {
                isbreak=false;
                system.debug('cREHObj------->'+cREHObj.name);
                system.debug('cREHObj.ToOrg__c'+cREHObj.ToOrg__c);
                system.debug('AccountableOrg------->'+AccountableOrg);
                system.debug('cREHObj.Department__c---->'+cREHObj.TODepartment__c);
                system.debug('Depatrtment'+Depatrtment);
                lisAccOrgCrCRH.add(cREHObj);
                if(cREHObj.ToOrg__c==AccountableOrg && cREHObj.TODepartment__c==Depatrtment) {
                    system.debug('HANU Inside');
                    isbreak=true;
                     break; 
                  }
     
         }
         
        //System.debug('lisAccOrgCrCRH.size()'+lisAccOrgCrCRH.size());
        //System.debug('lisAccOrgCrCRH'+lisAccOrgCrCRH[lisAccOrgCrCRH.size()-1]);
        for(integer i=lisAccOrgCrCRH.size()-1 ; i>=0 ; i--) {
           
            ComplaintsRequestsEscalationHistory__c cObj=lisAccOrgCrCRH[i];
            system.debug('mapToOrg1'+cObj);
            if(cObj.KeepInvolved__c==true) {
                mapCREH.put(cObj.ComplaintRequest__c,cObj);
                break;
            }  
            
        }
        system.debug('mapCREH'+mapCREH);
      
        
        List<ComplaintRequest__c> lis_CR = new List<ComplaintRequest__c>();
        if(mapCREH.size() > 0) {
            for(id strId :mapCREH.keyset()) {
                ComplaintRequest__c compRt= new ComplaintRequest__c(id=strId );
                compRt.TECH_AnswerToOrganization__c=mapCREH.get(strId).FromOrg__c;
                compRt.TECH_AnswerToIssueOwner__c  =mapCREH.get(strId).From_IssueOwner__c ;
                compRt.TECH_AnswerToContactEmailofOrg__c =mapCREH.get(strId).ContactEmailofFromAcctOrg__c; 
                
                
                
                //System.debug(' mapCREH.get(strId).Department__c -->'+mapCREH.get(strId).Department__c);
                if (mapCREH.get(strId).Department__c!=null)
                {
                    compRt.TECH_AnswerToDepartment__c=mapCREH.get(strId).Department__c;
                }
                else
                {
                    System.debug('Department blank');
                compRt.TECH_AnswerToDepartment__c='';
                }
                lis_CR.add(compRt);
                
            }   
        
        }
        if(lis_CR.size() > 0) {
        
            update lis_CR;
            isExcecute=true;
        }
        
        
    
    
    }



}