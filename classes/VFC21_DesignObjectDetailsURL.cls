/*
    Description: Basically this class is used to generate the Url
    which redirects the user to the Proper Page Layout for creating the Objective 
    details under one Objective. Layout depends on the Interval period for the Objective
    thus the recordtype of various interval plays crucial role in designing the Layout to
    the user.
    
    Author: Amitava Dutta
    

*/

public class VFC21_DesignObjectDetailsURL
{
    
    public string mstrObjSettingsID;
    public string ActualVal{get;set;}
   
    public String ObjSettingsID
    {
        get{return mstrObjSettingsID;}
        set{mstrObjSettingsID = value;}
    }
    public VFC21_DesignObjectDetailsURL()
    {
        ObjSettingsID = ApexPages.currentPage().getParameters().get('id');
       
    }
    /*
    public Schema.DescribeSObjectResult getDescribeSobject(string objectname){
        Map<String, Schema.SObjectType> m = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = m.get(objectname) ;
        return s.getDescribe() ;
    }
    
    public PageReference GetObjDetailUrl()
    {
        PageReference objPr = null;
        
        // we call the getDescribeSobject function to retrieve the KeyPrefix of the object Objectives_Detail__c
        Schema.DescribeSObjectResult objdetail = getDescribeSobject('ObjectivesDetail__c');
        
        List<ObjectivesSettings__c> objLstOS = [SELECT PeriodType__c,NAME FROM 
        ObjectivesSettings__c WHERE ID=:ObjSettingsID];
        
        List<RecordType> objLstOBDQ = null;
        if(objLstOS[0].PeriodType__c == Label.CL00226)
        {
            objLstOBDQ = [Select Id, name, SobjectType From RecordType  
            where SobjectType=:'ObjectivesDetail__c' and Name=:Label.CL00222 ];
        }
        else if(objLstOS[0].PeriodType__c == Label.CL00227)
        {
            objLstOBDQ = [Select Id, name, SobjectType From RecordType  
            where SobjectType=:'ObjectivesDetail__c' and Name=:Label.CL00225 ];
        }
        else
        {
             objLstOBDQ = [Select Id, name, SobjectType From RecordType  
            where SobjectType=:'ObjectivesDetail__c' and Name=:Label.CL00221 ];
        }
        
        if(objLstOS.size()!=0)
        {
           
             objPr = new PageReference('/'+objdetail.getKeyPrefix()+'/e?' + Label.CL00255 + '='+ objLstOS[0].Name  + '&' + Label.CL00256 +   '=' + ObjSettingsID + '&retURL='+ ObjSettingsID + '&RecordType='+objLstOBDQ[0].Id);
             objPr.setRedirect(true);   
              
               
           
        }
         
         return objPr ;
        
    }*/



}