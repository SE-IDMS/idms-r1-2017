/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
@isTest
global class FieloPRM_Schedule_MemberFeatSerialTest{

    static testmethod void test1() {
        User us = new user(id = userinfo.getuserId());

        us.BypassVR__c = true;
        update us;
        FieloEE.MockUpFactory.setCustomProperties(false);

        CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberFeatProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;

        FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '56789', 'DNI');

        List<FieloPRM_Feature__c> feats = new List<FieloPRM_Feature__c>();

        FieloPRM_Feature__c feature = new FieloPRM_Feature__c(
            Name = 'TestRamiro',
            F_PRM_FeatureAPIName__c = 'TestRamiroApiName',
            F_PRM_FeatureCode__c = 'Fecodetest',
            F_PRM_CustomLogicClass__c = 'FieloPRM_RewardsPSetImplements');
        feats.add(feature);

        FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c(
            Name = 'TestRamiro',
            F_PRM_FeatureAPIName__c = 'TestRamiroApiName2',
            F_PRM_FeatureCode__c = 'Fecodetest2',
            F_PRM_CustomLogicClass__c = 'FieloPRM_RewardsPSetImplements');
        feats.add(feature2);

        insert feats;

        List<FieloPRM_MemberFeature__c> memfeats = new List<FieloPRM_MemberFeature__c>();

        FieloPRM_MemberFeature__c mf = new FieloPRM_MemberFeature__c(
            F_PRM_Member__c = member.Id, 
            F_PRM_Feature__c = feats[0].Id,
            F_PRM_TryCount__c = 0);
        memfeats.add(mf);

        FieloPRM_MemberFeature__c mf2 = new FieloPRM_MemberFeature__c(
            F_PRM_Member__c = member.Id, 
            F_PRM_Feature__c = feats[1].Id,
            F_PRM_TryCount__c = 0);
        memfeats.add(mf2);

        insert memfeats;
        
        System.runAs(us){
            test.StartTest();
            try{
                FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(true, 'OK', memfeats, null);
                member.F_PRM_MemberFeatures__c = 'asd';
                update member;
                FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(false, 'OK', memfeats, null);
                FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(true, 'ERROR', memfeats, null);    
            }catch(Exception e){
                
            }
            test.StopTest();
        }
     }
}