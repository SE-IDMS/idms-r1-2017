// Added by Uttara - Q2 2016 - Notes to bFO

public class AP_LocalizedFSB {
    
    public static void populateAccUser(Map<LocalizedFSB__c,String> mapLocOrgId) {
        
        System.debug('!!!! mapLocOrgId.keySet() : ' + mapLocOrgId.keySet());
        System.debug('!!!! mapLocOrgId.values() : ' + mapLocOrgId.values());
        
        for(LocalizedFSB__c loc : mapLocOrgId.keySet()) {
            for(EntityStakeholder__c OrgStak : [SELECT Role__c, User__c, BusinessRiskEscalationEntity__c FROM EntityStakeholder__c WHERE BusinessRiskEscalationEntity__c IN: mapLocOrgId.values() AND Role__c =: 'Regional FSB Leader' ORDER BY CreatedDate DESC LIMIT 1]) {
                loc.AccountableUser__c = OrgStak.User__c;   
                System.debug('!!!! loc.AccountableUser__c  Regional FSB Leader : ' + loc.AccountableUser__c);
            }
            if(loc.AccountableUser__c == null) {
                for(EntityStakeholder__c OrgStak : [SELECT Role__c, User__c, BusinessRiskEscalationEntity__c FROM EntityStakeholder__c WHERE BusinessRiskEscalationEntity__c IN: mapLocOrgId.values() AND Role__c =: 'CS&Q Manager' ORDER BY CreatedDate DESC LIMIT 1]) {
                    loc.AccountableUser__c = OrgStak.User__c;
                    System.debug('!!!! loc.AccountableUser__c  CS&Q : ' + loc.AccountableUser__c);
                }                
            }
        }
    }
}