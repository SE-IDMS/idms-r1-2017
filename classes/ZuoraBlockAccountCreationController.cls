public with sharing class ZuoraBlockAccountCreationController {

   private final ApexPages.StandardController controller;
   
   private Zuora.zApi zuoraApi = null;
   
   private zqu__Quote__c quote;
   private String pageURL;
   
   
   public ZuoraBlockAccountCreationController(ApexPages.StandardController controller) {
      this.controller = controller;
      /*if (this.validate()) {
         try {
            
            String objectId = this.controller.getRecord().Id;     
            System.Debug('###### Object Id: ' + objectId);
        
            if (String.isNotBlank(objectId)) {

            }
            
         } catch(Exception e) { appendMessage(ApexPages.Severity.ERROR, e.getMessage()); }
      }*/
   }
   
   
   public PageReference navigateBack() {
       String targetUrl = this.pageURL;
       
       if (targetUrl.contains('//c.')) {
           targetUrl = targetUrl.replace('//c.','//zqu.');
        }
        else {
              if (targetUrl.contains('--c.')) {
                 targetUrl = targetUrl.replace('--c.','--zqu.');
              }
        } 
        targetUrl = targetUrl.replace('ZuoraBlockAccountCreationPage','quoteEnhancement');
        targetUrl = targetUrl.replace('stepNumber=2','stepNumber=1');
        
        PageReference returnPage = new PageReference(targetUrl);
        
        System.Debug('###### returnPage: ' + returnPage);
        return returnPage;
   }
   
   /*
   private Boolean validate() {
      if (false) {
         appendMessage(ApexPages.Severity.ERROR, 'Validation.');
         return false;
      }
      return true;
   }
  */
   
    public PageReference onload() {
        
      System.Debug('###### INFO: getPageURL() = ' + getPageURL());
      this.pageUrl = getPageURL();
      System.Debug('###### INFO: ApexPages.currentPage().getParameters().get(\'billingAccountId\'); = ' + ApexPages.currentPage().getParameters().get('billingAccountId'));
      System.Debug('###### INFO: ApexPages.currentPage().getParameters().get(\'crmAccountId\'); = ' + ApexPages.currentPage().getParameters().get('crmAccountId'));

      String billingAccountId = ApexPages.currentPage().getParameters().get('billingAccountId');
      String crmAccountId = ApexPages.currentPage().getParameters().get('crmAccountId');
        //*
        if ( ((billingAccountId != null) && (billingAccountId != '')) || (Test.isRunningTest()) ) {
            System.Debug('###### INFO: starting redirect to standard Quote creation page.');
            
            String targetUrl = getPageURL();
            System.Debug('###### originalUrl: ' + targetUrl);
           if (targetUrl.contains('//c.')) {
               targetUrl = targetUrl.replace('//c.','//zqu.');
            }
            else {
                  if (targetUrl.contains('--c.')) {
                     targetUrl = targetUrl.replace('--c.','--zqu.');
                  }
            } 
            targetUrl = targetUrl.replace('ZuoraBlockAccountCreationPage','CreateQuote');
            String quoteId = createQuote(crmAccountId, billingAccountId);
            if(quoteId == null){
              appendMessage(ApexPages.Severity.ERROR, 'You must first create contacts for the account.');
              return null;              
            }
            targetUrl += '&Id='+quoteId;
            
            PageReference newPage = new PageReference(targetUrl);
            
            System.Debug('###### New Page: ' + newPage);
            return newPage;
            
        } else {
            appendMessage(ApexPages.Severity.ERROR, 'You must create the billing account first in the account screen or select existing ones.');
            return null;
        }
    }
    
    
    private String getPageURL() {
      if (Test.isRunningTest()) {
          String myString = 'abcd';
          return 'https://c.cs9.visual.force.com/apex/ZuoraBlockAccountCreationPage?crmAccountId=001K0000019An2HIAS&quoteType=Subscription&retUrl=%2Fapex%2Fzqu__quotelist&stepNumber=2';
       } else {
          //String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
          //String urlVal = Apexpages.currentPage().getUrl();
          String urll = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + Apexpages.currentPage().getUrl();
          return urll;
      }
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 15/01/2016
     * @description Method to create a ZQuote with default values
     * @param crmAccountId : Id of the Salesforce Account
     * @param billingAccountId : Zuora Id of the Billing Account
     * @param Id : Id of the created ZQuote
     */
    private Id createQuote(String crmAccountId, String billingAccountId){
      Date today = Date.today();
      
      List<Zuora__CustomerAccount__c> listZAccount = [SELECT Id, CurrencyIsoCode, Zuora__Batch__c, ImmediateInvoiceAccount__c, Name, Zuora__BillToName__c, Zuora__SoldToName__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :billingAccountId];
      /* GP1 update 04/02/2016 use the contact in the billing account */
      /* List<Contact> listCon = [SELECT Id FROM Contact WHERE AccountId= :crmAccountId LIMIT 1];
      Id contactId =null;

      if(listCon.size()>0){
        contactId = listCon.get(0).Id;
      }
      */
      if(listZAccount.size()>0){
        Zuora__CustomerAccount__c zAccount = listZAccount.get(0);
        String defaultQuoteName = 'Quote for ' + zAccount.Name + ' - ' + Datetime.now().format();
        defaultQuoteName = defaultQuoteName.left(80);

        zqu__Quote__c zQuote = new zqu__Quote__c(zqu__Account__c=crmAccountId, Name = defaultQuoteName, zqu__ZuoraAccountID__c=billingAccountId,
                                                zqu__ValidUntil__c=today, zqu__StartDate__c=today, zqu__InitialTerm__c=1, 
                                                zqu__BillingBatch__c= zAccount.Zuora__Batch__c,zqu__RenewalTerm__c=1, 
                                                zqu__Currency__c=zAccount.CurrencyIsoCode,CurrencyIsoCode=zAccount.CurrencyIsoCode,
                                                Bill2Contact__c=zAccount.Zuora__BillToName__c, Sold2Contact__c=zAccount.Zuora__SoldToName__c);
/*  GP1 update 04/02/2016  use the contact in the billing account zqu__BillToContact__c=zAccount.Zuora__BillToId__c, zqu__SoldToContact__c=zAccount.Zuora__SoldToId__c);*/
        insert zQuote;
        return zQuote.Id;
      }

      return null;
    }
   
   
   public static void appendMessage(ApexPages.Severity messageType, String message) {
      ApexPages.addMessage(new ApexPages.Message(messageType, message));
   }  
}