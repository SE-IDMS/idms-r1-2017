/*
    Author          : Yannick Tisserand ~ yannick.tisserand@accenture.com 
    Date Created    : 12/07/2011
    Description     : This class contains methods to update Work Order fields with values coming from integrated systems
*/

public class AP18_WorkOrderIntegrationMethods
{
    /** Convention for Product as DataTamplate__c :
        Field1__c : Business 
        Field2__c : Product Line
        Field3__c : Product Family
        Field4__c : Family 
        Field5__c : Sub Family
        Field6__c : Product Succession
        Field7__c : Product Group
        Field8__c : Commercial Reference
        Field9__c : Product Description
        Field10__c : GMR Code
    **/
   
   // Update field commercial reference in a Work Order
    public static void UpdateCommercialReferenceWorkOrder(SVMXC__Service_Order__c workOrder, DataTemplate__c Product)
    {   
        if(Product.field1__c != null)
            workOrder.BusinessUnit__c = Product.field1__c;
        
        if(Product.field2__c != null)
            workOrder.ProductLine__c = Product.field2__c;
        
        if(Product.field3__c != null)
            workOrder.ProductFamily__c = Product.field3__c;
        
        if(Product.field4__c != null)
            workOrder.Family__c = Product.field4__c;
        
        if(Product.field8__c != null)
            workOrder.Commercial_Reference__c = Product.field8__c;  
        
        if(Product.field9__c != null)
            workOrder.ProductDescription__c = Product.field9__c;

        workOrder.CheckedbyGMR__c = true;
        workOrder.TECH_LaunchGMR__c = false;
        
        Database.SaveResult workOrderUpdate = Database.Update(workOrder);   
        List<Database.Error> DMLError = workOrderUpdate.getErrors(); 
        System.Debug('#### DML Errors : ' + DMLError );                                  
    }
    
    public static void UpdateAdditionalCommercialReferencesWorkOrder(SVMXC__Service_Order__c workOrder, List<DataTemplate__c> products)
    {                          
            
        for(DataTemplate__c Product : products)
        {                                                
           if(workOrder.AdditionalCommercialReferences__c != null)
              workOrder.AdditionalCommercialReferences__c += '\n'+Product.field8__c;
           else
              workOrder.AdditionalCommercialReferences__c = Product.field8__c;     
                      
       }
        
       if(products.size() > 0)
       {
           Database.SaveResult workOrderUpdate = Database.Update(workOrder);   
           List<Database.Error> DMLError = workOrderUpdate.getErrors(); 
           System.Debug('#### DML Errors : ' + DMLError ); 
       }                          
    }
   
}