public class AP_SendTEXEscalationMails
{//Notif to:  Diagnostic Expert, Expert Center contact email, Front Office TEX manager, TEX escalation informed from the Expert Center.
/*BR-7353
CLOCT15I2P56 --> Diagnostic Expert
CLOCT15I2P57 --> TEX_SendReminderNotifications
CLOCT15I2P58 --> Expert Center
CLOCT15I2P59 --> TEX Escalation Informed
CLOCT15I2P60 --> Front Office TEX Manager
CLOCT15I2P61 --> Closed
CLOCT15I2P62 --> Cancelled
CLOCT15I2P63 --> Expert Center Contact
CLOCT15I2P64 --> Sub-entity
CLOCT15I2P65 --> Entity
CLOCT15I2P66 --> FO Organization
CLOCT15I2P67 --> &%
CLMAR16I2P04 -->Expert Assessment report sent to customer
*/
   public static void createTEXStakeholder(List<TEX__c> texList){

        system.debug('list of TEX===>'+texList);
        List<TEXStakeholder__c> listStholder=new List<TEXStakeholder__c>();
        list<TEX__c> textoUpdate = new list<TEX__c>();
        map<string,wrapperdetails> mapRoleandEmail = new map<string,wrapperdetails>();
        map<string,wrapperdetails> mapEscRoleandEmail = new map<string,wrapperdetails>();
        Date notifdate ;
        integer escalationday;
        integer CSEscday;
        string struseremail;
        Map<string,CS_TEXStakeholders__c> csvalues;
        csvalues = CS_TEXStakeholders__c.getAll();
        for(TEX__c tex : texList){
            mapRoleandEmail = new map<string,wrapperdetails>();
           notifdate=null;
           escalationday=null;
           CSEscday=null;
           struseremail='';
            //Reminder mails when the remainder date is today
            if(tex.ExpertAssessmentCompleted__c == null && tex.ReminderDate__c == system.today()){ //tex.ExpertAssessmentCompleted__c == '' || 
                struseremail = '';
                if(tex.OfferExpert__c != null ){
                    mapRoleandEmail.put(tex.id+Label.CLOCT15I2P67+tex.OfferExpert__r.email, new wrapperdetails(Label.CLOCT15I2P56,tex.OfferExpert__r.email,tex.id,system.today(),0,Label.CLOCT15I2P57));
                }if(tex.ExpertCenter__c != null ){
                    mapRoleandEmail.put(tex.id+Label.CLOCT15I2P67+tex.ExpertCenter__r.ContactEmail__c, new wrapperdetails(Label.CLOCT15I2P58,tex.ExpertCenter__r.ContactEmail__c,tex.id,system.today(),0,Label.CLOCT15I2P57)); 
                    
                    struseremail = getuserEmail(Label.CLOCT15I2P59,tex.ExpertCenter__r.id);

                    system.debug('useremail is 2====>'+struseremail);
                    mapRoleandEmail.put(tex.id+Label.CLOCT15I2P67+struseremail, new wrapperdetails(Label.CLOCT15I2P59,struseremail,tex.id,system.today(),0,Label.CLOCT15I2P57)); 
                }

                system.debug('FO TEX manger====>'+tex.FrontOfficeTEXManager__c);
                if(tex.FrontOfficeTEXManager__c != null ){
                    mapRoleandEmail.put(tex.id+Label.CLOCT15I2P67+tex.FrontOfficeTEXManager__r.Email, new wrapperdetails(Label.CLOCT15I2P60,tex.FrontOfficeTEXManager__r.Email,tex.id,system.today(),0,Label.CLOCT15I2P57)); 
                }
                if(mapRoleandEmail.size()>0){
                    insertTexSH(mapRoleandEmail);
                }
            }
           
            //Escalation emails

            system.debug('===Status is==>'+tex.Status__c+'== Escalation date is==>'+tex.EscalationDate__c+'===tex id is===>'+tex.id);
            if(( tex.Status__c != Label.CLOCT15I2P61 && tex.Status__c != Label.CLOCT15I2P62 && tex.Status__c != Label.CLMAR16I2P04) && ( tex.EscalationDate__c == system.today() || tex.EscalationDate__c < system.today() )){ 
               if(tex.LastNotificationDay__c == 1){
                    notifdate = getBusinessDueDate(tex.EscalationDate__c,3);
                    escalationday = 3;
                }else if(tex.LastNotificationDay__c == 3){
                    notifdate = getBusinessDueDate(tex.EscalationDate__c,8);
                    escalationday = 8;
                }else if(tex.LastNotificationDay__c >= 8){
                    notifdate = getBusinessDueDate(tex.EscalationDate__c, integer.valueof(tex.LastNotificationDay__c) + 5);
                    escalationday = 8;
                }else if(tex.LastNotificationDay__c == 0){
                notifdate = getBusinessDueDate(tex.EscalationDate__c,1);
                escalationday = 1;

                system.debug('Escalation date is===>'+tex.EscalationDate__c+'====escalationday ==>'+escalationday);
                }

                system.debug('Escalation date is===>'+tex.EscalationDate__c+'====escalationday ==>'+escalationday);
            }

            system.debug('Notification day ====>'+notifdate+'== Escalation days===>'+escalationday);
            if(notifdate <= system.today() || Test.isRunningTest()){

            system.debug('Entered indide Notification day ====>'+notifdate+'== Escalation days===>'+escalationday);
               for(CS_TEXStakeholders__c cs : csvalues.values()){
                    
                    if(cs.EscalationDays__c == escalationday){

                    system.debug('escalationday==>'+escalationday+'==Current CS is==>'+cs);
                    struseremail = '';
                        if(cs.Role__c == Label.CLOCT15I2P56 && (tex.OfferExpert__c != null )){

                        system.debug('cs.Role__c == '+Label.CLOCT15I2P56 +'&& (tex.OfferExpert__c != null )');
                            mapEscRoleandEmail.put(tex.id+Label.CLOCT15I2P67+tex.OfferExpert__r.email,new wrapperdetails(cs.Role__c,tex.OfferExpert__r.email,tex.id,system.today(),cs.EscalationDays__c,cs.EMAILTEMPLATE__C));

                            system.debug('role Diagnostic Expert ==>'+tex.OfferExpert__r.email);
                        }else if(tex.ExpertCenter__c != null && cs.FROMORG__C == Label.CLOCT15I2P58 && (cs.Role__c != '' && cs.Role__c != null)){
                            if(cs.Role__c == Label.CLOCT15I2P63){

                           system.debug('tex.ExpertCenter__c != null && cs.FROMORG__C == '+Label.CLOCT15I2P58 +'&& (cs.Role__c != banks && cs.Role__c != null)===>'+cs.role__C);
                            mapEscRoleandEmail.put(tex.id+Label.CLOCT15I2P67+tex.ExpertCenter__r.ContactEmail__c,new wrapperdetails(cs.Role__c,tex.ExpertCenter__r.ContactEmail__c,tex.id,system.today(),cs.EscalationDays__c,cs.EMAILTEMPLATE__C)); 
                            } //else {

                            system.debug('tex.ExpertCenter__c != null && cs.FROMORG__C == '+Label.CLOCT15I2P58 +'&& (cs.Role__c != blanks && cs.Role__c != null)===>'+cs.role__C+'==else block');
                                if(cs.LEVEL__C == Label.CLOCT15I2P64){ 

                                    system.debug('subentity level ==>'+tex.ExpertCenter__r.SubEntity__c);
                                    if(tex.ExpertCenter__r.SubEntity__c != null && tex.ExpertCenter__r.SubEntity__c != '')

                                    system.debug('subentity level ==>'+tex.ExpertCenter__r.SubEntity__c);
                                    struseremail = getEmailfromOrg(cs.Role__c,tex.ExpertCenter__r.id,tex.ExpertCenter__r.Entity__c,tex.ExpertCenter__r.SubEntity__c, cs.LEVEL__C) ;
                                }else if(cs.LEVEL__C == Label.CLOCT15I2P65){
                                    struseremail = getEmailfromOrg(cs.Role__c,tex.ExpertCenter__r.id,tex.ExpertCenter__r.Entity__c,'', cs.LEVEL__C) ;
                                }else {
                                    struseremail = getEmailfromOrg(cs.Role__c,tex.ExpertCenter__r.id,tex.ExpertCenter__r.Entity__c,'', '') ;
                                }
                                if(struseremail != '' && struseremail != null)
                                    mapEscRoleandEmail.put(tex.id+Label.CLOCT15I2P67+struseremail,new wrapperdetails(cs.Role__c,struseremail,tex.id,system.today(),cs.EscalationDays__c,cs.EMAILTEMPLATE__C));
                           // }
                        } else if(cs.Role__c == Label.CLOCT15I2P60 && tex.FrontOfficeTEXManager__c != null){
                            mapEscRoleandEmail.put(tex.id+Label.CLOCT15I2P67+tex.FrontOfficeTEXManager__r.Email,new wrapperdetails(cs.Role__c,tex.FrontOfficeTEXManager__r.Email,tex.id,system.today(),cs.EscalationDays__c,cs.EMAILTEMPLATE__C)); 
                        }else if(tex.FrontOfficeOrganization__c != null && cs.FROMORG__C == Label.CLOCT15I2P66 && (cs.Role__c != '' && cs.Role__c != null)){

                            system.debug('tex.FrontOfficeOrganization__c != null && cs.FROMORG__C == '+Label.CLOCT15I2P66 +'&& (cs.Role__c != blanks && cs.Role__c != null)');
                            if(cs.LEVEL__C == Label.CLOCT15I2P64){ 
                                if(tex.FrontOfficeOrganization__r.SubEntity__c != null && tex.FrontOfficeOrganization__r.SubEntity__c != '')

                                    system.debug('subentity level ==>'+tex.FrontOfficeOrganization__r.SubEntity__c);
                                    struseremail = getEmailfromOrg(cs.Role__c,tex.FrontOfficeOrganization__r.id,tex.FrontOfficeOrganization__r.Entity__c, tex.FrontOfficeOrganization__r.SubEntity__c, cs.LEVEL__C) ;
                            } else if(cs.LEVEL__C == Label.CLOCT15I2P65) {
                                struseremail = getEmailfromOrg(cs.Role__c,tex.FrontOfficeOrganization__r.id,tex.FrontOfficeOrganization__r.Entity__c, '', cs.LEVEL__C) ;
                            }else {
                                struseremail = getEmailfromOrg(cs.Role__c,tex.FrontOfficeOrganization__r.id,tex.FrontOfficeOrganization__r.Entity__c, '', '') ;
                            }
                            if(struseremail != '' && struseremail != null)
                                mapEscRoleandEmail.put(tex.id+Label.CLOCT15I2P67+struseremail,new wrapperdetails(cs.Role__c,struseremail,tex.id,system.today(),cs.EscalationDays__c,cs.EMAILTEMPLATE__C));
                        }else if(tex.LineofBusiness__c != null || tex.LineofBusiness__c != ''){

                            system.debug('tex.LineofBusiness__c != null || tex.LineofBusiness__c != blanks');
                            if(cs.LEVEL__C == Label.CLOCT15I2P64){ 
                                if(tex.LineofBusiness__r.SubEntity__c != null && tex.LineofBusiness__r.SubEntity__c != '')

                                    system.debug('subentity level ==>'+tex.FrontOfficeOrganization__r.SubEntity__c);
                                    struseremail= getEmailfromOrg(cs.Role__c,tex.LineofBusiness__r.id,tex.LineofBusiness__r.Entity__c,tex.LineofBusiness__r.SubEntity__c,cs.LEVEL__C);
                            }else if(cs.LEVEL__C == Label.CLOCT15I2P65){
                                struseremail= getEmailfromOrg(cs.Role__c,tex.LineofBusiness__r.id,tex.LineofBusiness__r.Entity__c,'', cs.LEVEL__C);
                            }else {
                                struseremail= getEmailfromOrg(cs.Role__c,tex.LineofBusiness__r.id,tex.LineofBusiness__r.Entity__c,'','');
                            }
                            if(struseremail != '' && struseremail != null)
                                mapEscRoleandEmail.put(tex.id+Label.CLOCT15I2P67+struseremail,new wrapperdetails(cs.Role__c,struseremail,tex.id,system.today(),cs.EscalationDays__c,cs.EMAILTEMPLATE__C));
                        }
                    }
                }
                if(tex.LastNotificationDay__c < 8 && escalationday < 8){
                tex.LastNotificationDay__c = escalationday;

                system.debug('Notification day is--->'+tex.LastNotificationDay__c);
                }else if(tex.LastNotificationDay__c < 8 && escalationday == 8){
                tex.LastNotificationDay__c = 8;

                system.debug('Notification day is--->'+tex.LastNotificationDay__c);
                }else if(tex.LastNotificationDay__c >= 8 && escalationday == 8){
                tex.LastNotificationDay__c = integer.valueof(tex.LastNotificationDay__c) + 5;

                system.debug('Notification day is--->'+tex.LastNotificationDay__c);
                }
                if(notifdate!=null) {
                    tex.TECH_NotificationDate__c = System.today();
                
                }
                textoUpdate.add(tex);
            }
        }

        system.debug('id and email keyset===>'+mapEscRoleandEmail.keyset());
         if(mapEscRoleandEmail.size()>0){
            insertTexSH(mapEscRoleandEmail);
        }

        system.debug('tex updated is ===>'+textoUpdate);
        if(textoUpdate.size()>0){
            update textoUpdate;
        }
    }  
    // this 'getuserandEmail' methods returns , User email of the particular orgnization stake whole is role same as the role of the input.
    public static string getuserEmail(string role,id orgId){
        string useremail = '';

        system.debug('getuserEmail method role ==>'+role+'== orgid==>'+orgId);
        list<EntityStakeholder__c> orgSH = [Select id,Name,BusinessRiskEscalationEntity__c,Role__c,User__c,User__r.email,User__r.id from EntityStakeholder__c where BusinessRiskEscalationEntity__c =: orgId];
        for(EntityStakeholder__c esh : orgSH){

            system.debug('org id is ==>'+esh.BusinessRiskEscalationEntity__c+'==role is==>'+esh.Role__c+'== user id is===>'+esh.User__r.id+'== input role is==>'+role);
            if(esh.Role__c == role && (esh.User__r.email != null || esh.User__r.email != '') ){

                system.debug('id is====>'+esh.User__r.id+'=== email is ===>'+esh.User__r.email);
                useremail = esh.User__r.email;
            }
        }

        system.debug('return email from getuserEmail is===>'+useremail);
        return useremail;
    }
    
    static Integer countt=0;
    public static date getBusinessDueDate(Date duedate,integer dys){
        date ModifiedDate;
        integer days;

        system.debug('duedate inside getBusinessDueDate is===>'+duedate+'====and days are===>'+dys);
        date edate=duedate+dys;

        System.debug('edate:'+countt+':'+edate);
        System.debug('edate.day():'+countt+':'+edate.day());
        datetime startdate=datetime.newInstance(duedate.year(), duedate.month(),duedate.day());
        datetime endDate=datetime.newInstance(edate.year(), edate.month(),edate.day());

        System.debug('edate.day():'+countt+':'+edate.day());
        //datetime endDate=datetime.newInstance(edate, 0);

        System.debug('endDate:'+countt+':'+endDate);
        countt++;
        days=daysBetweenExcludingWeekends(startdate + 1,endDate);     

        system.debug('@@@@@@@days%%'+days+'  @@@@@@@%dys%%'+dys);       
            ModifiedDate=duedate+dys+days;
            DateTime myDateTime = (DateTime)ModifiedDate;
        String dayOfWeek = myDateTime.format('E');
        if (dayOfWeek=='Sun')
                {
                ModifiedDate=ModifiedDate+1;
                }
        else if(dayOfWeek=='Sat'){ModifiedDate=ModifiedDate+2;}
        

            system.debug('%%%%%@@@@@@@1@'+ModifiedDate);
     
         return ModifiedDate; 
    }
    public static Integer daysBetweenExcludingWeekends(Datetime startDate, Datetime endDate) {
       Integer i = 0;

        while (startDate <= endDate) {
            if (startDate.format('EEE')== 'Sat'){i = i + 1; endDate = endDate.addDays(1);}else if( startDate.format('EEE') == 'Sun') {
                i = i + 1;

                System.debug('ed:'+endDate);
                endDate = endDate.addDays(1);

                System.debug('ed:'+endDate);
            }
            startDate = startDate.addDays(1);
        }

        return i;
    }       
    // Insert TEX stakeholders
    public static void insertTexSH(map<string,wrapperdetails> mapRoleandEmail){
         map<string,TEXStakeholder__c> mapstrTEXSH = new map<string,TEXStakeholder__c>();
         string Emailid;
         for(string str : mapRoleandEmail.keyset()){
            Emailid = '';

            system.debug('SH is ===>'+str);
            system.debug('SH2 is ===>'+mapRoleandEmail.get(str));
            Emailid = str.substringAfterLast(Label.CLOCT15I2P67);

            system.debug('email id is ===>'+Emailid);
            if(Emailid != null && Emailid != '' && Emailid.contains('.com')){

                system.debug('email id entereed is ===>'+Emailid);
           TEXStakeholder__c texSH = new TEXStakeholder__c();
            texSH.TEX__c = mapRoleandEmail.get(str).texId; 
            texSH.Email__c = Emailid;
            texSH.Tech_NotificationDate__c = mapRoleandEmail.get(str).lastNotDate; 
            texSH.Tech_NotificationDay__c = mapRoleandEmail.get(str).lastNotDay;
            texSH.Tech_EmailTemplate__c = mapRoleandEmail.get(str).EmailTemp;

            system.debug('tex stakeholder is===>'+texSH);
            mapstrTEXSH.put(str,texSH);
            }
        }
        
        if(mapstrTEXSH.size()>0){
            if(!Test.isRunningTest())
            insert mapstrTEXSH.values();
        }
    }

    // get the email of the particular role from sub entity level stakeholder email
    public static string getEmailfromOrg(string strRole, id orgID, string OrgEntity, string OrgSubEntity, string orglevel){
         string stremail;
         string orgQuery;

         system.debug('getEmailfromOrg rols is==>'+strRole+'== orgId==>'+orgID+'==Org Entity==>'+OrgEntity+'Org Level==>'+orglevel+'=========');
         system.debug('OrgEntity is===>'+OrgEntity+'====');
       
        id orgSHId ;
        stremail = '';
        if(orglevel != null && orglevel != ''){

            system.debug('== org level is===>'+orglevel);
              if(orglevel == Label.CLOCT15I2P65){// || (orglevel == '' && orglevel == null)){
                orgQuery = 'select id,name,Entity__c,SubEntity__c,Location__c  from BusinessRiskEscalationEntity__c where Entity__c = \''+OrgEntity+'\'AND SubEntity__c = \'\' AND Location__c = \'\''; 
                } else if(orglevel == Label.CLOCT15I2P64){
                   orgQuery = 'select id,name,Entity__c,SubEntity__c,Location__c  from BusinessRiskEscalationEntity__c where Entity__c = \''+OrgEntity+'\' AND SubEntity__c = \''+OrgSubEntity+'\'AND Location__c = \'\''; 
                }

                system.debug('query is ==>'+orgQuery);
                list<BusinessRiskEscalationEntity__c> orgLst = database.query(orgQuery);

                system.debug('result of the Query is===>'+orgLst);
        if(orgLst.size()>0){
            for(BusinessRiskEscalationEntity__c bre : orgLst){

                system.debug('Org Name is ===>'+bre.name);
                if( orglevel == Label.CLOCT15I2P64 && bre.Entity__c == OrgEntity && bre.SubEntity__c == OrgSubEntity ){

                    system.debug('Added Org Name is ===>'+bre.name);
                        orgSHId = bre.id;
                }else if(bre.Entity__c == OrgEntity && ( orglevel == Label.CLOCT15I2P65|| (orglevel == '' || orglevel == null))){

                        system.debug('Added Org Name is ===>'+bre.name);
                        orgSHId = bre.id;
                }
            }
            stremail = getuserEmail(strRole,orgSHId);
        }
        } else if(orglevel == null || orglevel == ''){
            stremail = getuserEmail(strRole,orgID);
        }

        system.debug('return email from getEmailfromOrg is===>'+stremail);
        return stremail;
    }
    
    // Wrapper class to get the details of the Tex stakeholder
    public class wrapperdetails{
        public string usrRole ;
        public id texId ;
        public date lastNotDate ; 
        public decimal lastNotDay ;
        public string usrEmail ;
        public string EmailTemp ;
        public wrapperdetails(string usrRole1, string usrEmail1,id texId1, date lastNotDate1, decimal lastNotDay1, string EmailTemp1){
            usrRole = usrRole1;
            texId = texId1;
            lastNotDate = lastNotDate1;
            lastNotDay = lastNotDay1;
            usrEmail = usrEmail1;
            EmailTemp = EmailTemp1; 
        }
    
    }
}