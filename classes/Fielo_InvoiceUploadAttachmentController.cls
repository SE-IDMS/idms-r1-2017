/************************************************************
* Developer: Juan Pablo Cubo                                *
* Type: VF Component Controller                             *
* Component: Fielo_InvoiceUploadAttachment                  *
* Created Date: 13/08/2014                                  *
************************************************************/

public with sharing class Fielo_InvoiceUploadAttachmentController {

    public transient blob documentBody {get; set;}
    public string errorMessage {get; set;}
    public string successMessage {get; set;}
    public string fileName {get; set;}
    public List<Document> uploadedDocuments {get; set;}
    
    private Set<string> validExtensions = new Set<string>{'.pdf','.jpg','.jpeg','.png','.gif','.bmp'};
    private string randomAttachmentName;
    
    //Constructor
    public Fielo_InvoiceUploadAttachmentController(){
        randomAttachmentName = ApexPages.currentPage().getParameters().get('attachmentName');
        uploadedDocuments = new List<Document>();
    }
    
    public Pagereference doUploadFile(){
        
        errorMessage = null;
        successMessage = null; 

        if(fileName == null){
            errorMessage = Label.Fielo_InvoiceUploadFileMissing;
        }
        string fileExtension = '';
        if(fileName != null){
            if(fileName.contains('.')){
                fileExtension = fileName.substring( fileName.lastIndexOf('.'), fileName.length() );
            }
            if(!validExtensions.contains(fileExtension.toLowerCase())){
                errorMessage = Label.Fielo_InvoiceUploadAttachmentFileMsg;
                fileName = null;
            }
            if(documentBody.size() > 5000000){
                errorMessage = Label.Fielo_InvoiceUploadAttachmentFileSizeMsg;
                fileName = null;
            }
        }
                     
        if(errorMessage != null){
            errorMessage = '* '+errorMessage;
            return null;
        }
        try{
          Document doc = new Document();
          doc.Body = documentBody;
          doc.Name = randomAttachmentName + fileName;
          doc.IsPublic = false;
          doc.folderid = UserInfo.getUserId();
          insert doc;
          successMessage = Label.Fielo_InvoiceUploadAttachmentMsgPart1 + ' ' + fileName + ' ' + Label.Fielo_InvoiceUploadAttachmentMsgPart2;
        }catch(Exception e){
          errorMessage = e.getMessage();
        }
        listDocuments();
        return null;
    }
    
    public void doDeleteDocument(){
      string docId = ApexPages.currentPage().getParameters().get('docId');
      delete [SELECT Id FROM Document WHERe Id =: docId];
      listDocuments();
      fileName = null;
    }
    
    private void listDocuments(){
      errorMessage = null;
      successMessage = null;
      uploadedDocuments = [SELECT Id, Name FROM Document WHERE Name LIKE: randomAttachmentName + '%'];
      
      for (Document doc : uploadedDocuments){
        doc.Name = doc.Name.replace(randomAttachmentName, '');
      }
    }
    
}