/*
    Author          : ACN 
    Date Created    : 20/07/2011
    Description     : Test class for VFC44_LeadDetail class 
*/
@isTest
private class VFC44_LeadDetail_TEST 
{
    static testMethod void testdisplayChildLeads() 
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43');                
         system.runas(user)
         {
            test.startTest();
            Country__c country= Utils_TestMethods.createCountry();
            insert country;

            Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead1;

            Lead lead3 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead3;            
            
            lead3.parentLead__c = lead1.Id;
            lead3.ClassificationLevel1__c='LC';
            lead3.LeadingBusiness__c='BD';
            lead3.SuspectedValue__c=100;
            lead3.ZipCode__c='123789';
            lead3.OpportunityFinder__c ='Lead Agent';
            lead3.City__c='US';
            lead3.Street__c = 'testStreet';
            update lead3;
            
            Lead lead2 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead2;
            
            lead2.ParentLead__c = lead1.Id;            
            update lead2;
            
            Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(lead3.id);
            leadConv.setConvertedStatus('3 - Closed - Opportunity Created');            
            Database.LeadConvertResult lcr = Database.convertLead(leadConv);

            ApexPages.StandardController sc = new ApexPages.StandardController(lead1);
            VFC44_LeadDetail leadDetails= new VFC44_LeadDetail(sc);
            leadDetails.displayChildLeads();            
            test.stopTest();
         }
       }
    }
}