global class BatchSRVConcurrentOrderIPAssignment  implements Database.Batchable<sObject>, Schedulable {
    public Integer rescheduleInterval = Integer.ValueOf(Label.CLQ416FS03);
    public Integer scopelimit = Integer.ValueOf(Label.CLQ416FS04);
    
    public String query = 'select id, name, ShippedSerialNumber__c, SVMXC__Product__c, SVMXC__Installed_Product__c from SVMXC__Service_Contract_Products__c where ShippedSerialNumber__c != null AND SVMXC__Product__c != null AND SVMXC__Installed_Product__c = null limit '+Label.CLAPR14SRV14;
    
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
    
    private static Boolean isDebugEnabled = false;
    static {
        try {
            isDebugEnabled = Boolean.valueOf(Label.CLAPR14SRV20);
        }
        catch(Exception e) {
            System.debug('#### Error getting debug enabled status from Custom Label CLAPR14SRV20: '+e.getMessage());
        }        
    }
    
    private static void myDebug(String aMsg) {
        if (isDebugEnabled) {
            System.debug('#### '+aMsg);
        }
    }
    
    
    public String scheduledJobName = 'SRVConcurrentOrderIPAssignment';
    
    global Database.Querylocator start(Database.BatchableContext BC){
        myDebug('Starting BatchSRVConcurrentOrderIPAssignment Batch');
        myDebug('rescheduleInterval: '+rescheduleInterval);
        myDebug('Query: '+query);
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC, List <sObject> scope)
    {
        myDebug('Executing BatchSRVConcurrentOrderIPAssignment Batch');
        myDebug('Scope size: '+scope.size());
        myDebug('Scope: '+scope);
        AP_IPAssociationHandler.attachIPForConcurrentOrders((List<SVMXC__Service_Contract_Products__c>) scope);
    }
    
    global void finish(Database.BatchableContext BC) {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        myDebug('scheduledJobDetailList: '+scheduledJobDetailList);
                     
        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            // There is at least one scheduled job already for this name
            // Looking if one of scheduled job with the correct name is in an 'Active' state
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            myDebug('scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new BatchSRVConcurrentOrderIPAssignment(), scheduledJobName, rescheduleInterval);
        }
    }
    
    global void execute(SchedulableContext sc) {
        myDebug('Executing BatchSRVConcurrentOrderIPAssignment Batch with schedulable context: '+sc);
        BatchSRVConcurrentOrderIPAssignment attachIPForConcurrentOrder = new BatchSRVConcurrentOrderIPAssignment();
        Database.executeBatch(attachIPForConcurrentOrder,scopelimit);
        //SMUSKU added scopelimit to control number records to be processed in each batch size on 8th Dec 16
    }    

}