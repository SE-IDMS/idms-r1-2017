@isTest

private class FieloPRM_REST_GetProgLevelsByAccountTest{

    static testMethod void unitTest(){
    
         
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.PRMUIMSID__c = 'PRMUIMSIDTEST';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'LASTaCCtEST';
            member.FieloEE__FirstName__c = 'fIRSTACCtEST';
            member.FieloEE__Street__c = 'testSreet';
          
            insert member;
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level'; 
            insert badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;

            FieloPRM_BadgeAccount__c badgeAcc = new FieloPRM_BadgeAccount__c ();  
            badgeAcc.F_PRM_Badge__c = badge.id;
            badgeAcc.F_PRM_Account__c = acc.id;
            insert badgeAcc;
            
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;
            
            list<String> listIds = new list<String>(); 
            
            listIds.add(acc.PRMUIMSID__c);
        
            FieloPRM_REST_GetProgLevelsByAccount rest = new FieloPRM_REST_GetProgLevelsByAccount();
            FieloPRM_REST_GetProgLevelsByAccount.getProgramLevelsByAccount(listIds );
        }

    }
    
}