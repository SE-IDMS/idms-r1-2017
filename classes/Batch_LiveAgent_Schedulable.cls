global class Batch_LiveAgent_Schedulable implements Schedulable {

    global void execute(SchedulableContext sc) {
        Batch_LiveAgent_UpdateTranscripts  batchObj = new Batch_LiveAgent_UpdateTranscripts(); 
        Database.executeBatch(batchObj, 500);
    }
}