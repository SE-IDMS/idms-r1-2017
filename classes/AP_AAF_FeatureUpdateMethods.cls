/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 31 July 2013
    Description : For October 2013 Release:
                 
                 1. Add the stand-alone feature for Contacts.
                 2. Add Feature REquirements to Account Assigned Requirment for stand-alone Features.
      
********************************************************************************************************************/
public class AP_AAF_FeatureUpdateMethods{
    
    /*****************************************************************************************************
        To create new Contact stand-alone feature when a new account stand-alone feature is added.
    *****************************************************************************************************/
    public static void createContactFeature(map<Id,list<AccountAssignedFeature__c>> mapAccountFeature)
    {
        try
        {
            list<Contact> lstContact = new list<Contact>();
            
            //lstContact = [Select id,AccountId FROM Contact WHERE AccountID in: mapAccountFeature.keySet() limit 10000];
            lstContact = [Select id,AccountId FROM Contact WHERE AccountID in: mapAccountFeature.keySet() and PRMContact__c = true limit 10000];
            list<ContactAssignedFeature__c> lstContactFeature = new list<ContactAssignedFeature__c>();
            Id standaloneFeatureRecordType = [Select id from RecordType where developername='StandAloneFeature'and SobjectType ='ContactAssignedFeature__c' limit 1].Id;            
            for(Contact con : lstContact)
            {
                if(mapAccountFeature.containsKey(con.AccountId))
                for(AccountAssignedFeature__c acctFeature : mapAccountFeature.get(con.AccountId))
                {
                    ContactAssignedFeature__c contFeature = new ContactAssignedFeature__c();
                    contFeature.AccountAssignedFeature__c = acctFeature.Id;
                    contFeature.AssignmentComments__c = acctFeature.AssignmentComments__c;
                    contFeature.ExpirationDate__c = acctFeature.ExpirationDate__c;
                    contFeature.RecordTypeID = standaloneFeatureRecordType ;//Label.CLOCT13PRM23;
                    contFeature.Contact__c = con.Id;
                    contFeature.FeatureCatalog__c = acctFeature.FeatureCatalog__c;
                    lstContactFeature.add(contFeature);
                }
            }
            insert lstContactFeature;
            
            
        }
        catch(Exception e)
        {
            system.debug('Exception inserting Contact stand-alone feature:'+e.getMessage());
        }
    }
    
    /*****************************************************************************************************
        To create new Contact stand-alone requirement and  a new account stand-alone requirement .
    *****************************************************************************************************/
    public static void createFeatureRequirement(map<Id,list<AccountAssignedFeature__c>> mapFeatureReq)
    {
        list<FeatureRequirement__c> lstFTRReq = new list<FeatureRequirement__c>();
        list<AccountAssignedRequirement__c> lstAccountRequirement = new list<AccountAssignedRequirement__c>();
        list<ContactAssignedRequirement__c> lstContactRequirement = new list<ContactAssignedRequirement__c>();
        map<Id, list<ContactAssignedFeature__c>> mapContactFeature = new map<Id, list<ContactAssignedFeature__c>>();
        list<ContactAssignedFeature__c> lstContactFeature = new list<ContactAssignedFeature__c>();
        try
        {
            lstContactFeature = [Select id, FeatureCatalog__c,Contact__c FROM ContactAssignedFeature__c WHERE FeatureCatalog__c in:mapFeatureReq.keySet() limit 10000];             for(ContactAssignedFeature__c contFeature : lstContactFeature)
            {
                if(!mapContactFeature.containsKey(contFeature.FeatureCatalog__c))
                    mapContactFeature.put(contFeature.FeatureCatalog__c , new list<ContactAssignedFeature__c> {(contFeature)});
                else
                    mapContactFeature.get(contFeature.FeatureCatalog__c).add(contFeature);
            }
            lstFTRReq = [Select id, RequirementName__r.RecordTypeId, TECH_CertificationID__c,FeatureName__c,RequirementName__r.Applicableto__c from FeatureRequirement__C where FeatureName__c in :mapFeatureReq.keySet() limit 10000];
            
            for(FeatureRequirement__c ftrReq : lstFTRReq)
            {
                if(mapFeatureReq.containsKey(ftrReq.FeatureName__c))
                {
                    for(AccountAssignedFeature__c accFeature : mapFeatureReq.get(ftrReq.FeatureName__c))
                    {
                        AccountAssignedRequirement__c accountReq = new AccountAssignedRequirement__c();
                        accountReq.AccountAssignedFeature__c = accFeature.Id;
                        accountReq.FeatureRequirement__c = ftrReq.Id;
                        accountReq.Account__c = accFeature.Account__c;
                        accountReq.recordtypeid = CS_RequirementRecordType__c.getValues(ftrReq.RequirementName__r.RecordTypeId).AARRecordTypeID__c;
                        lstAccountRequirement.add(accountReq);  
                    }
                }
                if(ftrReq.RequirementName__r.Applicableto__c == Label.CLMAY13PRM11)
                {
                    if(mapContactFeature.containsKey(ftrReq.FeatureName__c))
                    {
                        for(ContactAssignedFeature__c conFeature : mapContactFeature.get(ftrReq.FeatureName__c))
                        {
                            ContactAssignedRequirement__c contactReq = new ContactAssignedRequirement__c();
                            contactReq.ContactAssignedFeature__c = conFeature.Id;
                            contactReq.FeatureRequirement__c = ftrReq.Id;
                            contactReq.Contact__c = conFeature.Contact__c;
                            contactReq.recordtypeid = CS_RequirementRecordType__c.getValues(ftrReq.RequirementName__r.RecordTypeId).CARRecordTypeID__c;
                            lstContactRequirement.add(contactReq);  
                        }
                    }
                }
            }
            insert lstAccountRequirement;
            insert lstContactRequirement;
        }
        catch(Exception e)
        {
            system.debug('Exception in inserting Feature Requirement:'+e.getMessage());
        }
    }
    
    /*******************************************************************************************
    To update Contact Assigned Features when Account Assigned Feature are updated
    ********************************************************************************************/
    public static void updateContactFeature(map<Id,AccountAssignedFeature__c> mapAcctFeature)
    {
        list<ContactAssignedFeature__c> lstContFeature = new list<ContactAssignedFeature__c>();
        set<Id> setAccountFeatureID = new set<Id>();
        String contactFeatureQuery = 'Select id , AccountAssignedFeature__c, ';
        List<CS_AccountFeaturetoContactFeature__c> lstContactFields = CS_AccountFeaturetoContactFeature__c.getall().values();
        for(CS_AccountFeaturetoContactFeature__c contactFields : lstContactFields)
            contactFeatureQuery = contactFeatureQuery + contactFields.ContactAssignedFeatureField__c+' ,';
        system.debug('contactFeatureQuery :'+contactFeatureQuery );
        if (contactFeatureQuery.endsWith(','))
        {
            contactFeatureQuery = contactFeatureQuery.substring(0,contactFeatureQuery.lastIndexOf(','));
        }
        setAccountFeatureID.addALL(mapAcctFeature.keySet());
        contactFeatureQuery = contactFeatureQuery +' FROM ContactAssignedFeature__c WHERE AccountAssignedFeature__c in:setAccountFeatureID LIMIT 10000';
        lstContFeature =  database.query(contactFeatureQuery);
        for(ContactAssignedFeature__c contFeature : lstContFeature)
        {
            for(CS_AccountFeaturetoContactFeature__c contactFields : lstContactFields)
                contFeature.put(contactFields.ContactAssignedFeatureField__c , mapAcctFeature.get(contFeature.AccountAssignedFeature__c).get(contactFields.Name));
        }
        try
        { 
            if(!lstContFeature.isEmpty())
                update lstContFeature; 
        }
        catch(Exception e)
        {
            for (Integer i = 0; i < mapAcctFeature.size(); i++) 
            { 
                mapAcctFeature.values().get(i).addError(e.getMessage()); 
                System.debug(e.getMessage()); 
            }   
        }
             
    }

}