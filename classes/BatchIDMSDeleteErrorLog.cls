global class BatchIDMSDeleteErrorLog  implements Database.Batchable<sObject>, Schedulable {
    public Integer rescheduleInterval = Integer.ValueOf(Label.CLJUN16IDMS139);
    public String scheduledJobName = 'IDMSErrorLogDelete';
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
    
    public String query = 'select id from IDMS_ErrorLog__c where TecherrorLog__c=True limit '+Label.CLAPR14SRV14;

    global Database.Querylocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC, List <IDMS_ErrorLog__c> scope)
    {

        Delete Scope;
    }
    
     global void finish(Database.BatchableContext BC)
  {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);

            if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) 
            {
                
                List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
                System.debug('### scheduledJobList: '+scheduledJobList);
                if (scheduledJobList != null && scheduledJobList.size()>0) 
                {
                    canReschedule = False;
                }
            }
            if (canReschedule && !Test.isRunningTest() ) 
            {
                System.scheduleBatch(new BatchIDMSDeleteErrorLog(), scheduledJobName, rescheduleInterval);
            }
    }
        global void execute(SchedulableContext sc) 
        {
            BatchIDMSDeleteErrorLog   errorlogdelete = new BatchIDMSDeleteErrorLog();
            Database.executeBatch(errorlogdelete);
        }   

       
}