@isTest
private class VFC_ImportPRMContacts_TEST{
    static testMethod void testMethod1(){
        PageReference pageRef = Page.VFP_ImportPRMContacts;
        Test.setCurrentPage(pageRef); 
        
        List<RecordType> recordTypeLsts = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'MassRegistration' LIMIT 1]);
        
        UIMSCompany__c uimsCompany = new UIMSCompany__c();
        uimsCompany.RecordTypeId = recordTypeLsts[0].Id;
        
        String csvheaders = 'Email,First Name,Last Name,Country,Language,Business Type,Area of Focus,Phone Number,Phone Type,Job Function,Job Title,Contact Tax Id,T & C Accepted?,Company Name,Do Not Have Company?,Headquarters,Company Phone Number,Company Website,Address 1,Address 2,City,State / Province,Zip / Postal Code,Include Company Info In Public Search,Company Tax Id';
        String csvcontent1 = 'testclass1@yopmail.com,Akila,S,US,en_US,SP,SPE,123456789,Work Phone,Z016,ZC,12345,TRUE,Test - Company,FALSE,FALSE,123456789,abc@def.com,Street 12,5th Block,Boston,AL,12345,FALSE,43223543';
        String csvcontent2 = 'testclass2@yopmail.com,Akila,S,US,en_US,SP,SPE,123456789,Mobile,Z016,ZC,12345,TRUE,Test - Company,FALSE,FALSE,123456789,abc@def.com,Street 12,5th Block,Boston,AL,12345,FALSE,65467566';
        
        String content = csvheaders + '\r\n' + csvcontent1 + '\r\n' + csvcontent2;
        
        Country__c cntrys = Utils_TestMethods.createCountry();
        cntrys.countrycode__c ='US';
        insert cntrys;
        
        StateProvince__c state = new StateProvince__c();
        state.CountryCode__c = 'US';
        state.StateProvinceCode__c = 'AL';
        state.Country__c = cntrys.Id;
        insert state;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'OM', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true);
        insert cLC;
        
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'OM3', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild;
                
        PRMCountry__c pCntrys = new PRMCountry__c();
        pCntrys.Name = 'USA';
        pCntrys.CountryPortalEnabled__c = true;
        pCntrys.Country__c = cntrys.Id;
        pCntrys.SupportedLanguage1__c = 'en_US';
        pCntrys.PLDatapool__c = 'en_US2';
        insert pCntrys;
        
        CountryChannels__c cntryChannel = new CountryChannels__c();
        cntryChannel.Active__c = true;
        cntryChannel.Channel__c = cLC.Id;
        cntryChannel.SubChannel__c = cLChild.Id;
        cntryChannel.Country__c = cntrys.Id;
        cntryChannel.PRMCountry__c = pCntrys.Id;
        insert cntryChannel;
        
        ApexPages.StandardController stanPage = new ApexPages.StandardController(uimscompany); 
        VFC_ImportPRMContacts controller = new  VFC_ImportPRMContacts(stanPage);
        controller.contentFile = Blob.valueOf(content);
        controller.prmOriginSource = 'Partner Called-in';
        controller.originSourceInfo = 'Test';
        controller.getOriginSourceOptions();
        controller.ReadFile();
        controller.processRecords();              
    }
    
}