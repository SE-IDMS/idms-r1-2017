public class AP_CCCAction_Handler {
	public static void handleCCCActionBeforeUpdate(map<Id,CCCAction__c> mapNewCCCAction , map<Id,CCCAction__c> mapOldAction){
	//*********************************************************************************
    // Method Name      : handleCCCActionBeforeUpdate
    // Purpose          : To populate TECH_CaseOwnerEmailId from CCC Action and Process 
    //					: notification if required
    // Created by       : Vimal Karunakaran - Global Delivery Team
    // Date created     : 27th january 2016
    // Modified by      :
    // Date Modified    :
    // Remarks          : For March - 16 Release
    ///********************************************************************************/
    	Set<Id> setCaseId = new Set<Id> ();
    	Map<Id, List<CCCAction__c>> mapCaseAction = new Map<Id, List<CCCAction__c>>();
    	List<CCCAction__c> lstNoNotificationAction = new List<CCCAction__c>();
		for(CCCAction__c objCCCAction : mapNewCCCAction.values()){
			if(objCCCAction.SendNotificationonResponse__c){
				if(objCCCAction.TECH_AnswerNotificationRequired__c){
					if(objCCCAction.Case__c!=null){
						setCaseId.add(objCCCAction.Case__c);
						if(mapCaseAction.containsKey(objCCCAction.Case__c)){
							mapCaseAction.get(objCCCAction.Case__c).add(objCCCAction);
						}
						else{
							List<CCCAction__c> lstCCAction = new List<CCCAction__c>();
							lstCCAction.add(objCCCAction);
							mapCaseAction.put(objCCCAction.Case__c,lstCCAction);	
						}
					}
				}
			}
			else{
				lstNoNotificationAction.add(objCCCAction);
			}	
		}
		if(setCaseId.size()>0){
			List<Case> lstCaseWithAction = new List<Case>([Select Id, OwnerId, Owner.Email from Case where Id in: setCaseId]);
			String strUserKeyPrefix = SObjectType.User.getKeyPrefix();

			for(Case objCase: lstCaseWithAction){
				if(String.valueOf(objCase.OwnerId).startsWith(strUserKeyPrefix) && objCase.Owner.Email.length()>0){
					if(mapCaseAction!=null && mapCaseAction.containsKey(objCase.Id)){
						for(CCCAction__c objCCCAction: mapCaseAction.get(objCase.Id)){
							objCCCAction.TECH_CaseOwnerEmail__c = objCase.Owner.Email;
						}
					}
				}
			}
		}
		for(CCCAction__c objCCCAction : lstNoNotificationAction){
			objCCCAction.TECH_AnswerNotificationRequired__c =false;
		}
	}
	public static void handleCCCActionAfterUpdate(map<Id,CCCAction__c> mapNewCCCAction , map<Id,CCCAction__c> mapOldAction){
	//*********************************************************************************
    // Method Name      : handleCCCActionAfterUpdate
    // Purpose          : To update the Case Status and Last Modified Date when
    //					: CCC Action is updated
    // Created by       : Vimal Karunakaran - Global Delivery Team
    // Date created     : 27th january 2016
    // Modified by      :
    // Date Modified    :
    // Remarks          : For March - 16 Release
    ///********************************************************************************/
	
		Map<Id, Boolean> mapCaseIdWithStatusUpdate = new Map<Id, Boolean>();
    	for(CCCAction__c objCCCAction: mapNewCCCAction.values()){
        	if(objCCCAction.Case__c!=null){
            	if(objCCCAction.Status__c != mapOldAction.get(objCCCAction.Id).Status__c && objCCCAction.Status__c == Label.CLOCT15CCC01  && objCCCAction.Answer__c != mapOldAction.get(objCCCAction.Id).Answer__c){                    
                	mapCaseIdWithStatusUpdate.put(objCCCAction.Case__c,true);
            	}
            	else{
                	mapCaseIdWithStatusUpdate.put(objCCCAction.Case__c,false);
            	}
        	}
    	}
    	if(mapCaseIdWithStatusUpdate.size()>0){
        	updateCaseDetails(mapCaseIdWithStatusUpdate);
    	}		
    }
    private static void updateCaseDetails(Map<Id, Boolean> mapCaseIdWithStatusUpdate){
    	String strErrorId='';
        String strErrorMessage='';
        Set<Id> setCaseIdForStatusUpdate = new Set<Id>();
        List<Case> lstCaseForStatusUpdate = new List<Case>();
        if(mapCaseIdWithStatusUpdate.size()>0){
            List<Case> lstCase = new List<Case>();
            
            for(Id objCaseId: mapCaseIdWithStatusUpdate.keySet()){
            	if(mapCaseIdWithStatusUpdate.containsKey(objCaseId)){
            		if(!mapCaseIdWithStatusUpdate.get(objCaseId)){
		                Case objCase = new Case(Id=objCaseId);
		                objCase.LastActivityDate__c = System.now();
		                lstCase.add(objCase);
	            	}
	            	else{
	            		setCaseIdForStatusUpdate.add(objCaseId);
	            	}
                }
            }
            if(setCaseIdForStatusUpdate.size()>0){
            	lstCaseForStatusUpdate = new List<Case>([Select Id, Status, ActionNeededFrom__c, LevelOfExpertise__c, LastActivityDate__c from Case where Id in:setCaseIdForStatusUpdate]);
            	if(lstCaseForStatusUpdate.size()>0){
            		for(Case objCase: lstCaseForStatusUpdate){
            			objCase.Status = 'In Progress';
            			objCase.ActionNeededFrom__c=objCase.LevelOfExpertise__c;
            			objCase.LastActivityDate__c = System.now();
            			lstCase.add(objCase);	
            		}
            	}
            }
            
            if(lstCase.size()>0){
                try{ 
                    Database.SaveResult[] CaseUpdateResult = Database.update(lstCase, false);
                    for(Database.SaveResult objSaveResult: CaseUpdateResult){
                        if(!objSaveResult.isSuccess()){
                            Database.Error err = objSaveResult.getErrors()[0];
                            strErrorId = objSaveResult.getId() + ':' + '\n';
                            strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                            System.debug(strErrorId + ' ' + strErrorMessage);
                        }
                    }
                }    
                catch(Exception ex){
                    strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
                    System.debug(strErrorMessage);
                    
                }
            }
        }
    }   
}