/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016  
*   This is global REST API class to set or Update user password via IFW.
**/

@RestResource(urlMapping='/IDMSPassword/*')
global with sharing class IDMSPasswordRest{
     
    //Post method called by API to set user password. 
    @HttpPost
    global static IDMSResponseWrapper doPost(String Id,String FederationIdentifier,String NewPwd,String IDMS_Profile_update_source,String Token){
        
        IDMSPasswordService pwdService      = new IDMSPasswordService();             
        IDMSResponseWrapper Response        = pwdService.setPassworduser(Id,FederationIdentifier,NewPwd,IDMS_Profile_update_source,Token);
        RestResponse res                    = RestContext.response;
        if(Response.Status.equalsignorecase('Error')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS001);
            if(Response.Message.startsWith('Error in')){
                res.statuscode = integer.valueOf(Label.CLDEC16IDMS001);
            }            
            if(Response.Message.startsWith('User not')){
                res.statuscode = integer.valueOf(Label.CLDEC16IDMS002);
            }
            //JIRA #IQMMR-217 
            if(Response.Message.contains('Error in Updating User Password.')){
                res.statuscode = integer.valueOf(Label.CLDEC16IDMS005);
            }
        }
        
        return Response;
    }
    
    //Put method called by API to update user password.
    @HttpPut
    global static IDMSResponseWrapper doPut(String ExistingPwd, String NewPwd,String IDMS_Profile_update_source){
        
        IDMSPasswordService pwdService  = new IDMSPasswordService();             
        IDMSResponseWrapper Response    = pwdService.updatePassworduser(ExistingPwd,NewPwd,IDMS_Profile_update_source);
        RestResponse res                = RestContext.response;
        if(Response.Status.equalsignorecase('Error')){
            res.statuscode              = integer.valueOf(Label.CLDEC16IDMS001);
            if(Response.Message.startsWith('Error in')){
                res.statuscode          = integer.valueOf(Label.CLDEC16IDMS001);
            }
            if(Response.Message.startsWith('User not')){
                res.statuscode          = integer.valueOf(Label.CLDEC16IDMS002);
            }
        }
        return Response;
    } 
    
}