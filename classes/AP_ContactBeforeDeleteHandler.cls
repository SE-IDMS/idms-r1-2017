public class AP_ContactBeforeDeleteHandler
{    
    public void onBeforeDelete(List<Contact> newContacts)
    {
        // EXECUTE BEFORE DELETE LOGIC
        /* These following lines checks if there are open cases, leads, contract value chain players, Value Chain Players and Activities (customer visits, task, events etc). 
               If yes, the contact can’t be deleted.    
        */
         
        Map<Id, Id> conCasesIds = new Map<Id, Id>();
        Map<Id, Id> conLeadsIds= new Map<Id, Id>();        
        Map<Id, Id> conContractIds= new Map<Id, Id>();
        Map<Id, Id> conVCPIds= new Map<Id, Id>();
        Map<Id, Id> conTasksIds= new Map<Id, Id>();
        Map<Id, Id> conEventsIds= new Map<Id, Id>();
        Map<Id, Id> conCustSurIds= new Map<Id, Id>();
        List<Case> cases = new List<Case>();
        List<Lead> leads = new List<Lead>();
        List<CTR_ValueChainPlayers__c> contractVCP = new List<CTR_ValueChainPlayers__c>();
        List<OPP_ValueChainPlayers__c> oppVCP = new List<OPP_ValueChainPlayers__c>();
        List<Task> tasks = new List<Task>();
        List<Event> events = new List<Event>();
        List<CustomerSatisfactionSurvey__c> custSur = new List<CustomerSatisfactionSurvey__c>();
       
            //Queries the cases for the corresponding Contact
            cases  = [select Id,ContactId,CreatedDate,Status from Case where ContactId in: newContacts and ((Status!=:Label.CLDEC13CCC04 and Status!=:Label.CL00326) or CreatedDate>:Date.today().addmonths(-6))];
            
            for(Case c: cases)
            {
                conCasesIds.put(c.ContactId,c.Id);
            }
            
            //Queries the Leads for the corresponding Contact
            leads = [select Id,Contact__c from Lead where Contact__c in: newContacts];
            for(Lead l: leads)
            {
                conLeadsIds.put(l.Contact__c, l.Id);
            }

            //Queries Contract Value Chain Player for the corresponding Contact
            contractVCP = [select Id,Contact__c from CTR_ValueChainPlayers__c where Contact__c in: newContacts];
            for(CTR_ValueChainPlayers__c cntrct: contractVCP)
            {
                conContractIds.put(cntrct.Contact__c, cntrct.Id);
            }

            //Queries Value Chain Player for the corresponding Contact
            oppVCP = [select Id,Contact__c from OPP_ValueChainPlayers__c where Contact__c in: newContacts];
            for(OPP_ValueChainPlayers__c vcp: oppVCP)
            {
                conVCPIds.put(vcp.Contact__c, vcp.Id);
            }

            //Queries Tasks for the corresponding Contact
            tasks = [select Id, WhoId from Task where WhoId in: newContacts];
            for(Task t: tasks)
            {
                conTasksIds.put(t.WhoId, t.Id);
            }

            //Queries Events for the corresponding Contact
            events = [select Id, WhoId from Event where WhoId in: newContacts];
            for(Event eve: events)
            {
                conEventsIds.put(eve.WhoId, eve.Id);
            }
            
            //Queries the Customer Experience Feedback records for the corresponding Account
            custSur = [select Id, bFOContactId__c from CustomerSatisfactionSurvey__c where bFOContactId__c in: newContacts];
            for(CustomerSatisfactionSurvey__c csat: custSur)
            {
                conCustSurIds.put(csat.bFOContactId__c, csat.Id);
            }

        //Displays an error if there are open cases, leads, contract value chain players, value chain players, activities, work order, service roles, service contract or opportunity notification . 
        for(Contact con: newContacts)
        {
           // if(con.ToBeDeleted__c==true && con.ReasonForDeletion__c!=Label.CL00521)
           
           
                if((con.ReasonForDeletion__c!=Label.CL00521) && ((conCasesIds.containsKey(con.Id)) || (conLeadsIds.containsKey(con.Id)) || (conCustSurIds.containsKey(con.Id)) || (conContractIds.containsKey(con.Id)) || (conVCPIds.containsKey(con.Id)) || (conTasksIds.containsKey(con.Id)) || (conEventsIds.containsKey(con.Id)) || (con.TECH_IsSVMXRecordPresentForContact__c==true)))
                {                      
                    con.addError(Label.CLAPR14ACC01);
                }
            
        }
    }
}