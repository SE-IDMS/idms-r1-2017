@isTest
public class AP_PerformInsertForPlaftormAccounts_TEST{
    public static testMethod void Testrun1() {
    Id userId=userInfo.getUserId(); 
    Account a = Utils_TestMethods.createAccount();
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userid);
    insert cap;
    SFE_PlatformingScoring__c pltfromscoring=Utils_TestMethods.createPlatformScoring(cap.id,a.id,10,'2');
    insert pltfromscoring;
    Test.startTest();
    System.enqueueJob(new AP_PerformInsertForPlaftormAccounts(new list<SFE_PlatformingScoring__c>{pltfromscoring}));
    Test.stopTest();

    }
}