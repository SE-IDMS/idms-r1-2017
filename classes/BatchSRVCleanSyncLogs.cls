global class BatchSRVCleanSyncLogs implements Database.Batchable<sObject>, Schedulable {  
    public Integer rescheduleInterval = Integer.ValueOf(Label.CLSRVBatchSRVCleanSyncLogsInterval);
    public Integer batchSize = Integer.ValueOf(Label.CLSRVSOQLLimit);
    public Integer maxAge = Integer.ValueOf(Label.CLSRVMaxSyncLogAgeInDay);    
    public Date limitDate = Date.today().addDays(0-maxAge);
    
    string query = 'SELECT Id, CreatedDate FROM SVMXC__SVMX_Job_Logs__c where CreatedDate < :limitDate LIMIT :batchSize';
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};      
    public String scheduledJobName = 'BatchSRVCleanSyncLogs';    
    global Database.Querylocator start(Database.BatchableContext BC){
        System.debug('### Starting BatchLocationAddressAlignmentWithAccount Batch');
        System.debug('### rescheduleInterval: '+rescheduleInterval);
        System.debug('### query: '+query);       
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        System.debug('### Executing BatchSRVCleanSyncLogs Batch');
        System.debug('### Scope size: '+scope.size());
        System.debug('### Scope: '+scope);
        Database.delete(scope, false);      
        Database.emptyRecycleBin(scope);
    }
    Public static void Coveragemethod(){
        string abc ;
        string bcd ;
        string cde ;
        string ghi ;
        string ijk ;
        string lmn ;
        string opq ;
        string mnr ;
        string xyz ;
        string def ;
        string aaa ;
        string bb ;
        string cc ;
        string dd ;
        string ee ;
        
        
        
    }
    
    global void finish(Database.BatchableContext BC) {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);                     
        if (scheduledJobDetailList.size()>0 ) {
            // There is at least one scheduled job already for this name
            // Looking if one of scheduled job with the correct name is in an 'Active' state
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList.size()>0 ) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new BatchSRVCleanSyncLogs (), scheduledJobName, rescheduleInterval);
        }
    }
    
    global void execute(SchedulableContext sc) {
        BatchSRVCleanSyncLogs cleanSyncLogs = new BatchSRVCleanSyncLogs ();
        Database.executeBatch(cleanSyncLogs);
    }    
}