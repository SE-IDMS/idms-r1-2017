/*
Author: Ramu Veligeti
Date: 24/07/2013
Description: 1. Consolidating Certifications (Habilitation Requirements) on the Work Order
2. Concatenating the related work order skills__r.SVMXC__Skill__c.Name fields as comma separated for use in DC filtering into the Work Order Skills(text) field on the Work Order object.
*/
public class ServiceMaxUpdateSkillsOnWO
{
    public static void HabilitationRequirement(Set<Id> locId, Set<String> CN, List<SVMXC__Service_Order__c> solst)
    {
        Map<Id,Id> locMap = new Map<Id,Id>();
        if(locId.size()>0)
            for(SVMXC__Site__c st: [Select SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,Id,
                                    SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,
                                    SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,
                                    SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c 
                                    from SVMXC__Site__c where Id IN :locId and SVMXC__Parent__c!=null])
        {
            locId.add(st.SVMXC__Parent__c);
            locMap.put(st.SVMXC__Parent__c,st.Id);
            if(st.SVMXC__Parent__r.SVMXC__Parent__c!=null)
            {
                locId.add(st.SVMXC__Parent__r.SVMXC__Parent__c);
                locMap.put(st.SVMXC__Parent__r.SVMXC__Parent__c,st.Id);
            }
            if(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c!=null)
            {
                locId.add(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c);
                locMap.put(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,st.Id);
            }
            if(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c!=null)
            {
                locId.add(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c);
                locMap.put(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,st.Id);
            }
            if(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c!=null)
            {
                locId.add(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c);
                locMap.put(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,st.Id);
            }
            if(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c!=null)
            {
                locId.add(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c); 
                locMap.put(st.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,st.Id);
            }
        }
        system.debug('locId==='+locId);
        Id recId = Schema.SObjectType.HabilitationRequirement__c.getRecordTypeInfosByName().get('Location').getRecordTypeId();
        //List<HabilitationRequirement__c> hrlst = [Select Id, Skill__c, Skill__r.Name, Location__c from HabilitationRequirement__c where Location__c IN :locId and recordtypeId = :recId and BusinessUnit__c IN :BU]; 
        List<HabilitationRequirement__c> hrlst = [Select Id, Skill__c, Skill__r.Name, Location__c from HabilitationRequirement__c where Location__c IN :locId and recordtypeId = :recId and Country2__c IN :CN]; 
        
        
        Map<Id,String> hrmap = new Map<Id,String>();
        Boolean extupd =false;
        system.debug('hrlst=='+hrlst);
        for(HabilitationRequirement__c hr: hrlst)
        {
            Id LoctId;
            if(locMap.containsKey(hr.Location__c))
                LoctId = locMap.get(hr.Location__c);
            else LoctId = hr.Location__c;
            system.debug('LoctId =='+LoctId +'=='+hr.Location__c);
            String skill;
            if(hrmap.containsKey(LoctId))
            {
                skill = hrmap.get(LoctId);
                skill += ','+hr.Skill__r.Name;
                hrmap.put(LoctId,skill);
            }
            else hrmap.put(LoctId,hr.Skill__r.Name);
        }
        system.debug('hrmap==='+hrmap+'=='+solst);
        if(solst.size()==0) 
        {
            //solst = [Select Id, Habilitation_Requirements_certs__c, SVMXC__Site__c  from SVMXC__Service_Order__c where SVMXC__Site__c IN :locId and Service_Business_Unit__c IN :BU];
            solst = [Select Id, Habilitation_Requirements_certs__c, SVMXC__Site__c  from SVMXC__Service_Order__c where SVMXC__Site__c IN :locId and SVMXC__Country__c IN :CN];
            extupd = true;
        }
        for(SVMXC__Service_Order__c so:solst)
        {
            so.Habilitation_Requirements_certs__c = hrmap.get(so.SVMXC__Site__c);
        }
        
        if(solst.size()>0 && extupd) update solst;
    }
    
    public static void UpdateWOSkills(Set<Id> soId)
    {
        Map<Id,String> skmap = new Map<Id,String>();
        Map<ID,Work_Order_Skills__c> skillrecmap= new Map<ID,Work_Order_Skills__c>();
        List<SVMXC__Service_Order__c> solst = new List<SVMXC__Service_Order__c>();
        Map<id,SVMXC__Service_Order__c> woskillmap= new Map<id,SVMXC__Service_Order__c>();
        
        List<Work_Order_Skills__c> wos = [Select Id,Skill__r.Name,Work_Order__c,Record_Type__c from Work_Order_Skills__c where Work_Order__c IN :soId and Record_Type__c='Certification'];
        
        for(Work_Order_Skills__c ws: wos)
        {
            String skill;
            if(skmap.containsKey(ws.Work_Order__c))
                
            {
                skill = skmap.get(ws.Work_Order__c);
                skill += ','+ws.Skill__r.Name;
                skmap.put(ws.Work_Order__c,skill);
                
            }
            else skmap.put(ws.Work_Order__c, ws.Skill__r.Name);
        }
        
        for(Id sk: skmap.keySet())
        {
            SVMXC__Service_Order__c so = new SVMXC__Service_Order__c(Id=sk);
           // for(Work_Order_Skills__c ws: wos)
            //{
                // if(ws.Record_Type__c=='Expertise'){
                //  so.Work_Order_Skills__c = skmap.get(sk); //Commented as part of Def 8886
                //}      
                // else if(ws.Record_Type__c=='Certification'){     
                so.Habilitation_Requirements_certs__c = skmap.get(sk); //Added as part of Def 8886
                // }
                solst.add(so);
                woskillmap.putall(solst);
                
           // }
        }
        
        
        //if(solst.size()>0) 
        if(!woskillmap.isEmpty())
            update woskillmap.values();
    }
    
    
    public static void UpdateWOSkillrec(Set<Id> soId){
        
        Map<Id,String> skmap = new Map<Id,String>();
        Map<ID,Work_Order_Skills__c> skillrecmap= new Map<ID,Work_Order_Skills__c>();
        List<SVMXC__Service_Order__c> solst = new List<SVMXC__Service_Order__c>();
        Map<id,SVMXC__Service_Order__c> woskillmap= new Map<id,SVMXC__Service_Order__c>();
        
        List<Work_Order_Skills__c> wos = [Select Id,Skill__r.Name,Work_Order__c,Record_Type__c from Work_Order_Skills__c where Work_Order__c IN :soId and Record_Type__c='Expertise'];
        
        for(Work_Order_Skills__c ws: wos)
        {
            String skill;
            if(skmap.containsKey(ws.Work_Order__c))
                
            {
                skill = skmap.get(ws.Work_Order__c);
                skill += ','+ws.Skill__r.Name;
                skmap.put(ws.Work_Order__c,skill);
                
            }
            else 
            {
                skmap.put(ws.Work_Order__c, ws.Skill__r.Name);
                system.debug('show skill valuesin elseblock'+skmap);
            }
            
        }
        
        for(Id sk: skmap.keySet())
        {
            SVMXC__Service_Order__c so = new SVMXC__Service_Order__c(Id=sk);
            //for(Work_Order_Skills__c ws: wos)
           // {
                // if(ws.Record_Type__c=='Expertise'){
                //  so.Work_Order_Skills__c = skmap.get(sk); //Commented as part of Def 8886
                //}      
                // else if(ws.Record_Type__c=='Certification'){     
                so.Work_Order_Skills__c = skmap.get(sk); //Added as part of Def 8886
                // }
                solst.add(so);
                woskillmap.putall(solst);
                
           // }
        }
        
        
        if(!woskillmap.isEmpty())
            update woskillmap.values();
    }

}