/**
* This test class covers IDMSCodePinRegenerationRest,IDMSResponseWrapperCodePIN and IDMSResendPinCode classes.
**/

@isTest
class IDMSResendPinCodeTest{
    //This method checks PIN generation rest service.
    static testmethod void testIDMSCodePinRegenerationRestService(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ResendPinCode';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        
        User userObj1 = new User(alias = 'user', email='user12345671' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='user12345671' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='Test3Pras-Kuld1234',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111');
        
        insert userObj1;
        System.assert(userObj1!=null); 
        
        test.Starttest();
        IDMSResponseWrapperCodePIN responseWrapper1=IDMSCodePinRegenerationRest.doPut(userObj1.Id,userObj1.FederationIdentifier);
        System.assert(responseWrapper1!=null);            
        IDMSResponseWrapperCodePIN responseWrapper2=IDMSCodePinRegenerationRest.doPut('','');
        System.assert(responseWrapper2!=null);
        IDMSResponseWrapperCodePIN responseWrapper3=IDMSCodePinRegenerationRest.doPut('',userObj1.FederationIdentifier);
        System.assert(responseWrapper3!=null);
        IDMSResponseWrapperCodePIN responseWrapper=new IDMSResponseWrapperCodePIN();            
        test.Stoptest();
    }
    
}