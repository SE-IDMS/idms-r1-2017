Public class VFC_NewPartnerProgram{
    
    /*=======================================
      CONSTRUCTOR
    =======================================*/   
    
    // VFC_NewPartnerProgramstandard controller extension
    public VFC_NewPartnerProgram(ApexPages.StandardController controller){
        System.debug('VFC_NewPartnerProgram.Constructor.INFO - Constructor is called.');
        System.debug('VFC_NewPartnerProgram.Constructor.INFO - End of constructor.'); 
    }
    /*=======================================
      Action Method, Called from VF Page
     =======================================*/   
    
    public pagereference redirectToNewPartnerProgram(){
        System.Debug('VFC_NewPartnerProgram redirectToNewPartnerProgram - Method is called.');
        // Create a new edit page for the CCC Action 
        PageReference newPartnerProgramURL = new PageReference('/'+SObjectType.PartnerProgram__c.getKeyPrefix()+'/e' );
        
        try {
            
            String retURL = system.currentpagereference().getParameters().get('retURL'); // Get return URL paramater
           
            // Set the return URL to the edit page with the value from the current visual force page
            if(retURL  != null) {
                newPartnerProgramURL.getParameters().put(Label.CL00330, retURL);  
           
            }
            
            RecordType NewRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Tech_NewGlobalProgram' Limit 1];
            
            //newPartnerProgramURL.getParameters().put(Label.CLMAY13PRM32, Label.CLMAY13PRM15); //Commented for New Global Program 
                newPartnerProgramURL.getParameters().put(Label.CLMAY13PRM32, NewRecType.id);
               newPartnerProgramURL.getParameters().put(Label.CL00690, Label.CL00691);             
        }
        catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                System.debug('VFC_NewPartnerProgram.redirectToNewPartnerProgram'+e.getTypeName()+' - '+ e.getMessage());
        }
        System.Debug('VFC_NewPartnerProgram End of Method.');
        return newPartnerProgramURL;
    }
}