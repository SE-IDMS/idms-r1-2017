@isTest
private class VFC93_AddUpdateProductforRevLine_TEST {

    static testMethod void VFC93_AddUpdateProductforRevenueLineTest() {
        
    // Profile profile = [select id from profile where name='SE - Sales Manager'];
    // User user = new User(alias = 'user', email='user' + '@accenture.com', 
    // emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    // localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
    // timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Sales Manager');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }
        
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Opportunity opportunity1 = Utils_TestMethods.createOpenOpportunity(account1.Id);
        insert opportunity1;
        
        Revenue_Line__c rl= new Revenue_Line__c();
        rl.opportunity__c = opportunity1.id;
        
        Revenue_Line__c rl2 = new Revenue_Line__c();
        rl2.opportunity__c = opportunity1.id;
        insert rl2;
        
        ApexPages.StandardController stdC= new ApexPages.StandardController(rl);
        VFC93_AddUpdateProductforRevenueLine  myController = new VFC93_AddUpdateProductforRevenueLine (stdC);
        
        VCC08_GMRSearch GMRSearchComponent = new VCC08_GMRSearch();
        GMRSearchComponent.pageController = myController ;
        
        PageReference pageRef;
        pageRef = Page.VFP93_AddUpdateProductforRevenueLine; 
        Test.setCurrentPageReference(pageRef);
        apexpages.currentpage().getParameters().put('retURL', opportunity1.id);
        myController.ITB_GMRSearch.Init(rl2.id,GMRSearchComponent );
        myController.ITB_GMRSearch.PerformAction(new DataTemplate__c(), GMRSearchComponent );
        
        insert rl;
        ApexPages.StandardController stdC2 = new ApexPages.StandardController(rl);
        VFC93_AddUpdateProductforRevenueLine  myController2 = new VFC93_AddUpdateProductforRevenueLine (stdC2);
        VCC08_GMRSearch GMRSearchComponent2 = new VCC08_GMRSearch();
        GMRSearchComponent2.pageController = myController2 ;

        apexpages.currentpage().getParameters().put('retURL', rl.id); 
        myController2.ITB_GMRSearch.PerformAction(new DataTemplate__c(), GMRSearchComponent2 );
        myController.ITB_GMRSearch.Cancel(GMRSearchComponent);
        
        system.runAs(user){
           myController2.checkaccess(); 
           myController2.ITB_GMRSearch.PerformAction(new DataTemplate__c(), GMRSearchComponent2 );
        } 
        myController2.checkaccess(); 
    }
}