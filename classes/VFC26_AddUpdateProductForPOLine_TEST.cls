@isTest(SeeAllData=true)
private class VFC26_AddUpdateProductForPOLine_TEST {

    static testMethod void VFC25_AddUpdateProductforCaseTest() {
    //Start of Test Data preparation
    
        SVMXC__RMA_Shipment_Order__c shipment = new SVMXC__RMA_Shipment_Order__c();
        insert shipment;
        
        Test.startTest();
        SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c();
        line.SVMXC__RMA_Shipment_Order__c = shipment.Id;
        line.CommercialReference__c = 'Commercial Reference test';
        insert line;
        
        Test.stopTest();
                       
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(line);   
        VFC26_AddUpdateProductForPartOrderLine GMRSearchPage = new VFC26_AddUpdateProductForPartOrderLine(CaseStandardController );
        
        VCC08_GMRSearch GMRSearchComponent = new VCC08_GMRSearch();
        GMRSearchComponent.pageController = GMRSearchPage;
        
        GMRSearchPage.GMRsearchForPOLine.Init('TEST',GMRSearchComponent );
        GMRSearchComponent.actionNumber=1;
        GMRSearchPage.GMRsearchForPOLine.PerformAction(new DataTemplate__c(),GMRSearchComponent );
        GMRSearchPage.GMRsearchForPOLine.Cancel(GMRSearchComponent);   
        
    }
}