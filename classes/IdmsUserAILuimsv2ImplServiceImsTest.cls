/**
Description: This covers IdmsUserAILuimsv2ImplServiceIms class
**/
@IsTest public class IdmsUserAILuimsv2ImplServiceImsTest{
    //Test 1: test grant access
    static testmethod void testGrantOperation(){
        try {
            IdmsUserAILuimsv2ServiceIms.accessElement access = new IdmsUserAILuimsv2ServiceIms.accessElement(); 
            IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort test = new IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort();
            test.grantAccessControlToUser('callerFid','f9876544',access); 
        }catch(exception e){}
    }
    //Test 2: test revike operations
    static testmethod void testRevokeOperation(){
        try {
            IdmsUserAILuimsv2ServiceIms.accessElement access = new IdmsUserAILuimsv2ServiceIms.accessElement(); 
            IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort test = new IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort();
            test.revokeAccessControlToUser('callerFid','f9876544',access); 
        }catch(exception e){}
    }
    //Test 3: test conditions of revoke access
    static testmethod void testRevokeOperation2(){
        try {
            IdmsUserAILuimsv2ServiceIms.accessElement access = new IdmsUserAILuimsv2ServiceIms.accessElement(); 
            IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort test = new IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort();
            test.revokeAccessControlToUser('callerFid','f987766',access); 
        }catch(exception e){}
    }
    //Test 4: Test get operation
    static testmethod void testGetOperation(){
        try {
            IdmsUserAILuimsv2ServiceIms.getAccessControlByUser access = new IdmsUserAILuimsv2ServiceIms.getAccessControlByUser(); 
            String callfid = access.callerFid= 'callerFid';
            String fedId = access.federatedId = 'f987766';
            IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort test = new IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort();
            test.getAccessControlByUser(callfid ,fedId ); 
        }catch(exception e){}
    }
    //Test 5: Test all services 
    static testmethod void allService(){
        IdmsUserAILuimsv2ServiceIms.RequestedEntryNotExistsException obj1 = new IdmsUserAILuimsv2ServiceIms.RequestedEntryNotExistsException();
        IdmsUserAILuimsv2ServiceIms.UnexpectedLdapResponseException obj2 = new IdmsUserAILuimsv2ServiceIms.UnexpectedLdapResponseException();
        IdmsUserAILuimsv2ServiceIms.getAccessControlByUserResponse obj3 = new IdmsUserAILuimsv2ServiceIms.getAccessControlByUserResponse();
        IdmsUserAILuimsv2ServiceIms.InvalidImsServiceMethodArgumentException obj4 = new IdmsUserAILuimsv2ServiceIms.InvalidImsServiceMethodArgumentException();
        IdmsUserAILuimsv2ServiceIms.childs_element obj5 = new IdmsUserAILuimsv2ServiceIms.childs_element();
        IdmsUserAILuimsv2ServiceIms.accessTree obj6 = new IdmsUserAILuimsv2ServiceIms.accessTree();
        IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUserResponse obj7 = new IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUserResponse();
        IdmsUserAILuimsv2ServiceIms.accessList_element obj8 = new IdmsUserAILuimsv2ServiceIms.accessList_element();
        IdmsUserAILuimsv2ServiceIms.IMSServiceSecurityCallNotAllowedException obj9 = new IdmsUserAILuimsv2ServiceIms.IMSServiceSecurityCallNotAllowedException();
        IdmsUserAILuimsv2ServiceIms.grantAccessControlToUserResponse obj10 = new IdmsUserAILuimsv2ServiceIms.grantAccessControlToUserResponse();
        IdmsUserAILuimsv2ServiceIms.LdapTemplateNotReadyException obj11 = new IdmsUserAILuimsv2ServiceIms.LdapTemplateNotReadyException();
        IdmsUserAILuimsv2ServiceIms.SecuredImsException obj12  = new IdmsUserAILuimsv2ServiceIms.SecuredImsException();
        
    }
    
}