public without sharing class IdmsUimsReconcCreateCompanyBulk {
    
    static IDMSCaptureError errorLog=new IDMSCaptureError();
    public static void createUimsCompanies(List<IDMSCompanyReconciliationBatchHandler__c> recCompaniesToCreate, String batchId){
        Set<string> idmsUids = new Set<String>();
        for(IDMSCompanyReconciliationBatchHandler__c idmsUser : recCompaniesToCreate){
            idmsUids.add(idmsUser.IdmsUser__c);
        }
        //retrieve list of idms users
        //ToDo: remove unncessary fields from SOQL once mapping is verified.
        List<User> idmsUsers = [Select Id, Email,MobilePhone,Phone,FirstName,LastName,IDMS_Email_opt_in__c,IDMS_User_Context__c,IDMSMiddleName__c,CompanyName, 
                                Country,IDMS_PreferredLanguage__c,IDMSCompanyGoldenId__c, Company_Country__c, DefaultCurrencyIsoCode, IDMSClassLevel2__c,
                                Street,City,PostalCode,State,IDMS_County__c,Job_Title__c,Company_City__c, IDMSCompanyCounty__c , Company_Postal_Code__c, IDMSCompanyPoBox__c, IDMSCompanyHeadquarters__c,
                                Company_State__c, Company_Address1__c, Company_Website__c, IDMSMarketSegment__c, IDMSMarketSubSegment__c, IDMSCompanyMarketServed__c, IDMSCompanyNbrEmployees__c, 
                                IDMSClassLevel1__c, IDMSTaxIdentificationNumber__c,IDMSCompanyFederationIdentifier__c,
                                IDMS_POBox__c,FederationIdentifier,IDMS_Registration_Source__c,IDMS_AdditionalAddress__c,IDMSMiddleNameECS__c,Job_Function__c,Company_Address2__c,
                                IDMSSalutation__c,IDMSSuffix__c,Fax,IDMSDelegatedIdp__c,IDMSIdentityType__c,IDMSJobDescription__c, IDMSPrimaryContact__c,
                                IDMS_VC_NEW__c, IDMS_VC_OLD__c from User where Id IN:idmsUids];
        
        //stub class logic starts
        IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkService = new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
        string callerFid = LAbel.CLJUN16IDMS104;
        bulkService.endpoint_x = Label.CLNOV16IDMS020;
        bulkService.clientCertName_x = System.Label.CLJUN16IDMS103;
        
        //Request of UIMS
        IdmsUsersResyncflowServiceIms.accountCreateRequest objCreateCompany = new IdmsUsersResyncflowServiceIms.accountCreateRequest();
        objCreateCompany.accounts = new List<IdmsUsersResyncflowServiceIms.accountBean>();
        
        //Response from UIMS
        IdmsUsersResyncflowServiceIms.createAccountsResponse objCreateCompanyRes = new IdmsUsersResyncflowServiceIms.createAccountsResponse();
        objCreateCompanyRes.return_x = new List<IdmsUsersResyncflowServiceIms.createReturnBean>();
        
        Map<Integer,User> mapIndexUser = new Map<Integer,User>();
        integer index = 0;
        for(User usr :idmsUsers){
            index++;
            //mapping for company information
            IdmsUsersResyncflowServiceIms.accountBean uimsAccountBean = IdmsUimsReconcCompanyMapping.mapIdmsCompanyUims(usr); 
            objCreateCompany.accounts.add(uimsAccountBean);
            mapIndexUser.put(index,usr);
        }
        objCreateCompanyRes.return_x = bulkService.createAccounts(callerFid,objCreateCompany);
        //Uims response email ids set
        Set<String> uimsCompanyIds = new Set<String>();
        List<User> updateIdmsUsersCompInfo = new List<User>();
        List<IDMSCompanyReconciliationBatchHandler__c> handlerRecordsToDel = new List<IDMSCompanyReconciliationBatchHandler__c>();
        List<IDMSReconciliationBatchLog__c> createBatchLogs= new List<IDMSReconciliationBatchLog__c>();
        
        List<IDMSCompanyReconciliationBatchHandler__c> handlerRecordsToUpdate = new List<IDMSCompanyReconciliationBatchHandler__c>();
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        
        Map<String, User> mapUimsCompIdmsUser = new Map<String, User>();
        Map<String, IDMSCompanyReconciliationBatchHandler__c> mapIdmsUserHandlerToDel = new Map<String, IDMSCompanyReconciliationBatchHandler__c>();
        Map<String, IDMSCompanyReconciliationBatchHandler__c> mapIdmsUserHandlerToUpdate = new Map<String, IDMSCompanyReconciliationBatchHandler__c>();
        Set<String> idmsUserIds = new Set<String>();
        try{
            if(objCreateCompanyRes.return_x.size()>0){
                //all company ids in the list.
                for(IdmsUsersResyncflowServiceIms.createReturnBean responseUims : objCreateCompanyRes.return_x){
                    if(responseUims.ID!= null){
                        uimsCompanyIds.add(responseUims.ID);
                    }
                }
               
                //corresponding company handler list to delete
                List<IDMSCompanyReconciliationBatchHandler__c> userHandlerListToDel = [select Id, IdmsUser__c, NbrAttempts__c, HttpMessage__c  from IDMSCompanyReconciliationBatchHandler__c];
                for(IDMSCompanyReconciliationBatchHandler__c userHandlerToDel: userHandlerListToDel){
                    mapIdmsUserHandlerToDel.put(userHandlerToDel.IdmsUser__c, userHandlerToDel);
                }
                
                //traverse whole uims response and treat only for success onces. 
                integer IndexResponse = 0;
                for(IdmsUsersResyncflowServiceIms.createReturnBean responseValue : objCreateCompanyRes.return_x){
                    IndexResponse++;                    
                    if(responseValue.ID!= null && responseValue.returnCode == 0){
                        User processIdmsUser = mapIndexUser.get(IndexResponse); //mapUimsCompIdmsUser.get(responseValue.ID);
                        if(processIdmsUser != null){
                            system.debug('processIdmsUser: '+processIdmsUser);
                            processIdmsUser.IDMS_VC_OLD__c =  processIdmsUser.IDMS_VC_NEW__c;
                            processIdmsUser.IDMSCompanyFederationIdentifier__c = responseValue.Id;
                            updateIdmsUsersCompInfo.add(processIdmsUser); 
                            System.debug('*****Update Idms users and company fed id list****:'+updateIdmsUsersCompInfo);
                            
                            IDMSReconciliationBatchLog__c idmsBatchLog = IdmsReconcBatchLogsHandler.batchLogForCreateCompany(processIdmsUser, responseValue, batchId);
                            createBatchLogs.add(idmsBatchLog);     
                        
                            //Add handler record in the list to delete.
                            IDMSCompanyReconciliationBatchHandler__c handlerRecToDelete = mapIdmsUserHandlerToDel.get(processIdmsUser.Id);
                            handlerRecordsToDel.add(handlerRecToDelete);
                            system.debug('*****Delete records from company handler*****'+handlerRecordsToDel);
                        }
                    }         
                }
                //Failed handler records. we can't find and update error response for respective users. If company create get failed then we don't have Id. 
                List<IDMSCompanyReconciliationBatchHandler__c> userHandlerListToUpdate = [select Id, IdmsUser__c, IdmsUser__r.IDMSCompanyFederationIdentifier__c, NbrAttempts__c, HttpMessage__c from IDMSCompanyReconciliationBatchHandler__c where IdmsUser__c NOT IN: idmsUserIds];                
                for(IDMSCompanyReconciliationBatchHandler__c userHandlerToUpdate : userHandlerListToUpdate){
                    if(userHandlerToUpdate.NbrAttempts__c < attemptsMaxLmt){
                        userHandlerToUpdate.NbrAttempts__c = userHandlerToUpdate.NbrAttempts__c + 1;
                    }
                    handlerRecordsToUpdate.add(userHandlerToUpdate);
                    
                    IDMSReconciliationBatchLog__c idmsBatchFailedLog = IdmsReconcBatchLogsHandler.batchLogForFailedCreateCompany(userHandlerToUpdate.IdmsUser__c, userHandlerToUpdate, batchId);
                    createBatchLogs.add(idmsBatchFailedLog);     
                }
            }
            Update updateIdmsUsersCompInfo;
            Insert createBatchLogs;
            Delete handlerRecordsToDel;
            Update handlerRecordsToUpdate;
        }
        catch(Exception e){
            System.debug('Exception occurs:'+e.getStackTraceString());
            errorLog.IDMScreateLog('IDMSReconciliationBatch','IdmsUimsReconcCreateCompanyBulk','Bulk Company creation in UIMS','',Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),batchId,Datetime.now(),'','',false,'',false,null);
            
        }
    }
}