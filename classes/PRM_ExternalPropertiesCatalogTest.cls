@isTest
private class PRM_ExternalPropertiesCatalogTest {
    
    @isTest static void test_method_one() {
        //Id recordId = Schema.getGlobalDescribe().get('ExternalPropertiesCatalog__c').getDescribe().getRecordTypeInfosByName().get('BFOProperties').getRecordTypeId();
        //Id recordId = System.Label.CLJUN16PRM057;
        Id recordId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName = 'BFOProperties'].Id;
        List<ExternalPropertiesCatalog__c> epcList = new List<ExternalPropertiesCatalog__c>();
        
        for (Integer i = 0; i < 4; i++) {
            ExternalPropertiesCatalog__c epc = new ExternalPropertiesCatalog__c();
            epc.Active__c = true;
            epc.TableName__c = 'Contact';
            epc.SubType__c = 'abcSubType'+i;
            epc.RecordTypeId = recordId;
            epc.PropertyName__c = 'PropertyName'+i;
            epc.PRMCountryFieldName__c  = 'PRMCountry__c';
            if (i == 0) epc.FieldName__c = 'PRMEmail__c';
            else if(i == 1) epc.FieldName__c = 'PRMEmail';
            else if(i == 2) epc.FieldName__c = 'PRMCustomerClassificationLevel2__c';
            else epc.FieldName__c = 'Phone';
            
            epcList.add(epc);
        }
        
        Test.startTest();
        Database.SaveResult[] results = Database.insert(epcList, false);
        System.debug(results[0]);
        System.assertEquals(true,results[0].isSuccess());
        System.debug(results[2]);
        System.assertEquals(false,results[1].isSuccess());
        System.debug(results[2]);
        System.assertEquals(true,results[2].isSuccess());
        System.debug(results[3]);
        System.assertEquals(true,results[3].isSuccess());
        Test.stopTest();
    }
    
}