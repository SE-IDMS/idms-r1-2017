public class VFC_CountryChannelUserRegistration { 
    public CountryChannels__c CtryChanel{get;set;}
    public PRMAccountOwnerAssignment__c PRMAccountOwAssRule{get; set;}
    public List<SelectOption> FldList{get;set;}
    public Map<String,CS_PRM_UserRegistration__c> mapUserRegFlds;
    public List<List<wrapperUserReg>> lstExistUserRegFldsAppliedTo {get;set;}
    public List<String> sectionLabel{get;set;}
    public list<wrapperUserReg> appliedTo;
    public boolean inLine{get;set;}
    public Map<String,Map<String,Schema.SObjectField>> sObjectFieldsMap = new Map<String,Map<String,Schema.SObjectField>>();
    
    public VFC_CountryChannelUserRegistration(ApexPages.StandardController controller){
        PRMAccountOwAssRule = new PRMAccountOwnerAssignment__c();
        map<string,wrapperUserReg> mExtFields = new map<string,wrapperUserReg>();
        FldList = new List<SelectOption>();  
        List<SelectOption> lstCS = new List<SelectOption>();
        FldList.add(new SelectOption('','--None--')); 
        lstExistUserRegFldsAppliedTo = new List<List<wrapperUserReg>>(); 
        //CountryChannels__c conChn = (CountryChannels__c)controller.getRecord();
        
        String conChn = apexpages.currentpage().getparameters().get('Name');
        System.debug('**** Do we get a value ****'+conChn);
        if(conChn != NUll)
        inLine = True;
        CtryChanel = [select id,Name,F_PRM_ReviewLocatorListing__c ,Active__c,CurrencyIsoCode,Channel_Name__c, ChannelCode__c, Sub_Channel__c, SubChannelCode__c, Country__c,CompleteUserRegistrationonFirstLogin__c,AllowPrimaryToManage__c,PredictiveCompanyMatchEnabled__c,PRMCountry__c From CountryChannels__c Where id = :apexpages.currentpage().getparameters().get('id') limit 1];
        List<PRMAccountOwnerAssignment__c> PRMAccountOwAssRuleLst = [SELECT Id, Classification__c, SubClassification__c, Country__c, AccountOwner__c FROM PRMAccountOwnerAssignment__c WHERE CountryChannel__c = :CtryChanel.Id];
        if(PRMAccountOwAssRuleLst.size()>0)
            PRMAccountOwAssRule = PRMAccountOwAssRuleLst[0];
        //CS_PRM_UserRegistration__c.getall(); 
        List<CS_PRM_UserRegistration__c> lstUserRegFlds = [SELECT Id,FieldLabel__c,Name FROM CS_PRM_UserRegistration__c order by FieldSortOrder__c ASC];
        mapUserRegFlds = CS_PRM_UserRegistration__c.getAll();
        for(CS_PRM_UserRegistration__c aFld :lstUserRegFlds){          
            lstCS.add(new SelectOption(aFld.Name,aFld.FieldLabel__c));           
        }   
        System.Debug('*** Custom Setting Fields Info:' + lstUserRegFlds);
        System.Debug('*** Select Option:' + lstCS);
        //lstCS = SortOptionList(lstCS);
        FldList.addAll(lstCS);      
        
        list<CountryChannelUserRegistration__c> lstSavedFlds = [SELECT id,UserRegFieldName__c,DefaultValue__c,Mandatory__c,MandatoryOnProfile__c,Hidden__c,TECH_UserRegFieldName__c, HideField__c,ValidationType__c,Expression__c
                        FROM CountryChannelUserRegistration__c WHERE CountryChannels__c = :CtryChanel.id ];
        
        System.debug('** Configured Fields:' + lstSavedFlds.size());
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.DescribeFieldResult hidefieldResult = CountryChannelUserRegistration__c.HideField__c.getDescribe();
        Schema.DescribeFieldResult validationfieldResult = CountryChannelUserRegistration__c.ValidationType__c.getDescribe();
        for(CS_PRM_UserRegistration__c cSValues:mapUserRegFlds.Values()){
            if(sObjectFieldsMap.isEmpty()){
                Map<String,Schema.SObjectField> FielMap = schemaMap.get(cSValues.MappedObject__c).getDescribe().fields.getMap();
                sObjectFieldsMap.put(cSValues.MappedObject__c, FielMap);
            }
            else if(!sObjectFieldsMap.containsKey(cSValues.MappedObject__c)){
                Map<String,Schema.SObjectField> FielMap = schemaMap.get(cSValues.MappedObject__c).getDescribe().fields.getMap();
                sObjectFieldsMap.put(cSValues.MappedObject__c, FielMap);
            }
        }
        for(CountryChannelUserRegistration__c aRegFld :lstSavedFlds) {
            System.debug('** Field Name:' + aRegFld.UserRegFieldName__c);

            if(mapUserRegFlds.containsKey(aRegFld.UserRegFieldName__c)){
                wrapperUserReg SavedUserRegFld = new wrapperUserReg();
                
                SavedUserRegFld.userRegFlds = aRegFld;
                SavedUserRegFld.RecordId = aRegFld.id;
                SavedUserRegFld.appliedTo = mapUserRegFlds.get(aRegFld.UserRegFieldName__c).AppliedTo__c;
                if (SavedUserRegFld.userRegFlds.UserRegFieldName__c != Null) {
                    SavedUserRegFld.deleteDisabled = true;
                    SavedUserRegFld.fieldDisabled = true;
                    SavedUserRegFld.requiredDisabled = true;
                    SavedUserRegFld.hiddenDisabled = mapUserRegFlds.get(aRegFld.UserRegFieldName__c).HiddenDisabled__c;
                    SavedUserRegFld.defaultDisabled = mapUserRegFlds.get(aRegFld.UserRegFieldName__c).AlwaysMandatory__c;
                    SavedUserRegFld.defaultDisabledProfile = mapUserRegFlds.get(aRegFld.UserRegFieldName__c).MandatoryOnProfile__c;
                    SavedUserRegFld.allowCustomValidation = mapUserRegFlds.get(aRegFld.UserRegFieldName__c).AllowCustomValidation__c;

                    SavedUserRegFld.HideFieldOptions = getpickListValues(hidefieldResult);
                    SavedUserRegFld.ValidationFieldOptions = getpickListValues(validationfieldResult);
          
                    Schema.DescribeFieldResult fieldResults = getFieldResults(aRegFld.UserRegFieldName__c);
                    SavedUserRegFld.fieldType = String.valueOf(fieldResults.getType());
                    if(SavedUserRegFld.fieldType == 'REFERENCE'){
                        SavedUserRegFld.isPicklist = True;
                        if(fieldResults.getName() == 'PRMCountry__c')
                            SavedUserRegFld.picklistValues = getCompanyCountry();
                        else if(fieldResults.getName() == 'PRMStateProvince__c')
                            for(CountryChannelUserRegistration__c aRegFldIn :lstSavedFlds){
                                if(aRegFldIn.UserRegFieldName__c == System.Label.CLAPR15PRM146 && aRegFldIn.DefaultValue__c != '')
                                SavedUserRegFld.picklistValues = companyStateProvince(aRegFldIn.DefaultValue__c);
                            }
                    }
                    else if(SavedUserRegFld.fieldType == 'PICKLIST'){
                        SavedUserRegFld.picklistValues = getpickListValues(fieldResults);
                        SavedUserRegFld.isPicklist = True;
                    }
                    else if(SavedUserRegFld.fieldType == 'MultiPicklist'){
                        SavedUserRegFld.picklistValues = getpickListValues(fieldResults);
                        SavedUserRegFld.isMultiPicklist = True;
                    }
                    else if(SavedUserRegFld.fieldType == 'BOOLEAN')
                    SavedUserRegFld.isboolean = True;
                }
                mExtFields.put(SavedUserRegFld.userRegFlds.UserRegFieldName__c,SavedUserRegFld);
            }
        }
        System.debug('** Mapped Fields:' + mExtFields);
        
        sectionLabel = new List<String>{System.Label.CLAPR15PRM418,System.Label.CLAPR15PRM416,System.Label.CLAPR15PRM417};
        for(integer i=1; i<=3; i++){
            appliedTo = new list<wrapperUserReg>();
            for (CS_PRM_UserRegistration__c wur : lstUserRegFlds) {
                if(mExtFields.containsKey(wur.Name)){
                    if(mExtFields.get(wur.Name).appliedTo == i)
                    appliedTo.add(mExtFields.get(wur.Name));
                }
            }
            lstExistUserRegFldsAppliedTo.add(appliedTo);
        }
    }
    
    //Returns the Selectoption of countries
    public List<SelectOption> getcompanyCountry(){

        List<Country__c> countries =[Select Id,Name,CountryCode__c,Region__c from Country__c];
        List<SelectOption> selectCountry = new List<SelectOption>();
        selectCountry.add(new SelectOption('','--None--')); 
        for(Country__c con:countries){
            selectCountry.add(new SelectOption(con.Id,con.Name));
        }
        return selectCountry;
    }
    
    public List<SelectOption> companyStateProvince(String Country){
        List<StateProvince__c> stateProvince = [Select Id,Name,Country__c From StateProvince__c where Country__c = :Country];
        List<SelectOption> selectStateProvince = new List<SelectOption>();
        selectStateProvince.add(new SelectOption('','--None--'));
        for(StateProvince__c sPro:stateProvince ){
            selectStateProvince.add(new SelectOption(sPro.Id,sPro.Name));
        }
        return selectStateProvince;
    }
    
    public Schema.DescribeFieldResult getFieldResults(String fieldApiName){
        if(mapUserRegFlds.containsKey(fieldApiName)){
            String sObjectName =  mapUserRegFlds.get(fieldApiName).MappedObject__c;
            String fieldName = mapUserRegFlds.get(fieldApiName).MappedField__c;
            Schema.DescribeFieldResult fieldResults = sObjectFieldsMap.get(sObjectName).get(fieldName).getDescribe();
            return fieldResults;
        }
        else return Null;
          
    }
    
    public List<SelectOption> getpickListValues(Schema.DescribeFieldResult fieldResults){
        List<SelectOption> options = new List<SelectOption>();
        List<SelectOption> sortedOptions = new List<SelectOption>(); 
        for(Schema.PicklistEntry p: fieldResults.getPicklistValues()){
            if(p.isActive())
            options.add(new SelectOption(p.getValue(),p.getLabel()));
        }
        sortedOptions.add(new SelectOption('','--None--'));
        sortedOptions.addall(AP_PRMUtils.selectOptionSortByLabel(options));
        return sortedOptions;
    }
        
    public pagereference doSave(){
        pagereference pg = null;
        boolean isError = false;
        list<CountryChannelUserRegistration__c> updateUserRegFormFields = new List<CountryChannelUserRegistration__c>();
        map<String,String> mapNewFlds = new map<String,String>();
        system.debug('** AppliedTo***'+lstExistUserRegFldsAppliedTo);
        for(integer j=0; j<lstExistUserRegFldsAppliedTo.Size(); j++ ){
            for(wrapperUserReg ExistUserRegFlds:lstExistUserRegFldsAppliedTo[j]){
                updateUserRegFormFields.add(ExistUserRegFlds.userRegFlds);
            }
        }

        try
        { 
            if(updateUserRegFormFields.size() >0){
                Database.SaveResult[] saveResults = Database.Update(updateUserRegFormFields, false);
                for(Database.SaveResult sr : saveResults)
                {
                    if (!sr.isSuccess()){
                        System.debug('*** Error in Deletion '); 
                        for(Database.Error err : sr.getErrors()){
                            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, err.getMessage()));
                            isError = (!isError)? true : false;
                        }
                    }
                }
            }
            
        }
        catch(DMLException e)
        {    
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage())); 
            isError = (!isError) ? true : false;          
        }
        
        if(!isError)
            return new pagereference('/'+CtryChanel.Id);        
        else
            return null;
    
    }
    public class wrapperUserReg{
        public Id RecordId { get; set; }
        public boolean requiredDisabled { get; set; }
        public boolean hiddenDisabled { get; set; }
        public boolean deleteDisabled { get; set; }
        public boolean defaultDisabled { get; set; }
        public boolean defaultDisabledProfile { get; set; }
        public boolean fieldDisabled { get; set; }
        public CountryChannelUserRegistration__c userRegFlds {get;set;} 
        public boolean isPicklist { get;set; }
        public boolean isMultiPicklist { get;set; }
        public List<SelectOption> picklistValues{get; set;}
        public String fieldType{ get;set; }
        public boolean isboolean { get;set; }
        public boolean fieldBoolean { get;set; }
        public Decimal appliedTo { get;set; }
        public List<SelectOption> HideFieldOptions { get; set; }
        public List<SelectOption> ValidationFieldOptions { get; set; }
        public boolean allowCustomValidation { get; set;}

        public wrapperUserReg() {
            userRegFlds = new CountryChannelUserRegistration__c();
        }
    }
}