/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 26/01/2012
    Description: Apex class storing methods related to Customer Care Team management
*/

public class AP42_TeamAssignmentMethods
{
    public static Boolean AP42_bypass= false;

    // Makes sure only one team is marked as the default one for every CC agents
    public static void uncheckOtherDefaultTeams(List<TeamAgentMapping__c> aTeamMembershipList)
    {
        System.debug('AP42.uncheckOtherDefaultTeams.INFO - Method is called.');

        if(aTeamMembershipList !=null && aTeamMembershipList.size() != null && !AP42_bypass) {
            
            try {     
                Map<Id,List<TeamAgentMapping__c>> userTeamsMap = new Map<Id,List<TeamAgentMapping__c>>();
                Set<TeamAgentMapping__c> recordWithError = new Set<TeamAgentMapping__c>();
                
                for(TeamAgentMapping__c teamMemberItem:aTeamMembershipList) {
                    userTeamsMap.put(teamMemberItem.CCAgent__c, new List<TeamAgentMapping__c>());
                }

                // Add CC agent ID in the set
                for(User currentUser:[SELECT Id, (SELECT ID, Name, CCAgent__c, CCTeam__c, DefaultTeam__c FROM CCTeamsAssignments__r) FROM User WHERE ID IN :userTeamsMap.keySet()]) {
                    userTeamsMap.put(currentUser.Id, currentUser.CCTeamsAssignments__r);
                }

                System.debug('AP42.uncheckOtherDefaultTeams.INFO - Map user with their existing team membership  =' + userTeamsMap);

                // List teams already marked as default team that should be updated
                List<TeamAgentMapping__c> otherPrimaryTeamList = new List<TeamAgentMapping__c>();
        
                /*
                    For every newly created/updated team-agent assignment, the assignment is compared to all existing 
                    team assignment for the same agent. 
                    If a same assignment is found, meaning that both agent and team are the same than for the new one, an error is
                    added to the new record in order to prevent its insert/update.
                    If the new record is marked as "default" assignment and if a different assignment is found, marked as "default"
                    as well, the existing will be unchecked. Only the new one will remain marked as "default".
                    
                    This verification is performed against existing records retrieved via the SOQL query. In order to remain
                    bulk compliant, duplicates and multiple default team should be verified against the new list of assignment as well.
                    
                    
                */
                
                System.debug('AP42.uncheckOtherDefaultTeams.INFO - Beginning loop on all related Agent.');      
                
                Boolean isDuplicateFound=false;
                
                for(Integer i=0; i<aTeamMembershipList.size();i++) {
                
                    // Check duplicates and multiple default team against the list of new team-agent assignment
                    if(aTeamMembershipList.size() > 0) {
                        System.debug('AP42.uncheckOtherDefaultTeams.INFO - Beginning third loop on new team assignments for the related agent.');
                        for(Integer j=0; j<aTeamMembershipList.size();j++) {
                        
                            System.debug('AP42.uncheckOtherDefaultTeams.INFO - i = ' + i);    
                            System.debug('AP42.uncheckOtherDefaultTeams.INFO - j = ' + j); 
                            System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment i ID = ' + aTeamMembershipList[i].id); 
                            System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment j ID = ' + aTeamMembershipList[j].id);
                        
                            if (i != j && ((aTeamMembershipList[j].id != aTeamMembershipList[i].id) || aTeamMembershipList[j].id == null || aTeamMembershipList[i].id == null )) {
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment i Agent ID: ' + aTeamMembershipList[i].CCagent__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment j Agent ID: ' + aTeamMembershipList[j].CCAgent__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment i Team ID: ' + aTeamMembershipList[i].CCTeam__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment j Team ID: ' + aTeamMembershipList[j].CCTeam__c);                               
                          
                                // Check for duplicates
                                if( aTeamMembershipList[i].CCTeam__c == aTeamMembershipList[j].CCTeam__c && aTeamMembershipList[i].CCagent__c == aTeamMembershipList[j].CCAgent__c) {
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO -  Duplicate new assignments.' );
                                    isDuplicateFound = true;
                                    aTeamMembershipList[i].addError('Duplicate record found: '+ aTeamMembershipList[j]);
                                    aTeamMembershipList[j].addError('Duplicate record found: '+ aTeamMembershipList[i]);
                                }
                                else {
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO - Not a duplicate assignment. ' );
                                }
                                
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment i is Primary?: ' + aTeamMembershipList[i].DefaultTeam__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment j is Primary?  ' + aTeamMembershipList[j].DefaultTeam__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment i Agent ID: ' + aTeamMembershipList[i].CCAgent__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment j Agent ID: ' + aTeamMembershipList[j].CCAgent__c);
                                
                                // Ensures unicity of the default team for every agent
                                if(aTeamMembershipList[i].DefaultTeam__c && aTeamMembershipList[j].DefaultTeam__c && aTeamMembershipList[i].CCagent__c == aTeamMembershipList[j].CCAgent__c) {
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO - Other default team found ='+aTeamMembershipList[j]);
                                    aTeamMembershipList[i].DefaultTeam__c.addError('You cannot define multiple primary teams for an agent.');
                                    aTeamMembershipList[j].DefaultTeam__c.addError('You cannot define multiple primary teams for an agent.');
                                    recordWithError.add(aTeamMembershipList[i]);
                                    recordWithError.add(aTeamMembershipList[j]);
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO - Errors added.');
                                }
                            }
                        }
                        
                        System.debug('AP42.uncheckOtherDefaultTeams.INFO - End of third loop on new team assignments for the related agent.');
                    }
                    else {
                        System.debug('AP42.uncheckOtherDefaultTeams.INFO - Single record; multiple new assignment check bypassed.');
                    }
                
                    if(userTeamsMap.keyset().contains(aTeamMembershipList[i].CCagent__c)) {
                    
                        System.debug('AP42.uncheckOtherDefaultTeams.INFO - The map with all teams does contain the related agent ID.');
                        System.debug('AP42.uncheckOtherDefaultTeams.INFO - Beginning second loop on existing team assignments for the related agent.');   
                        
                        for(TeamAgentMapping__c existingAssignment:userTeamsMap.get(aTeamMembershipList[i].CCagent__c)) {
                            
                            if(existingAssignment.id != aTeamMembershipList[i].id ) {
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment Agent ID: ' + aTeamMembershipList[i].CCagent__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - Existing assignment Agent ID: ' + existingAssignment.CCAgent__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment Team ID: ' + aTeamMembershipList[i].CCTeam__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - Existing assignment Team ID: ' + existingAssignment.CCTeam__c);
    
                                // Checks duplicates
                                if( existingAssignment.CCAgent__c == aTeamMembershipList[i].CCAgent__c && existingAssignment.CCTeam__c == aTeamMembershipList[i].CCTeam__c) {
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO -  Duplicate assignment.' );
                                    isDuplicateFound = true;
                                    aTeamMembershipList[i].addError('Agent is already part of the Customer Care Team.');
                                }
                                else {
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO - Not a duplicate assignment. ' );
                                }
                                
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment is Primary?: ' + aTeamMembershipList[i].DefaultTeam__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - Existing assignment is Primary?  ' + existingAssignment.DefaultTeam__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - New assignment Agent ID: ' + aTeamMembershipList[i].CCAgent__c);
                                System.debug('AP42.uncheckOtherDefaultTeams.INFO - Existing assignment Agent ID: ' + existingAssignment.CCAgent__c);
                                
                                // Ensures unicity of the default team for every agent
                                if(existingAssignment.CCAgent__c == aTeamMembershipList[i].CCAgent__c  && aTeamMembershipList[i].DefaultTeam__c && existingAssignment.DefaultTeam__c) {
                                    System.debug('AP42.uncheckOtherDefaultTeams.INFO - Other default team found ='+existingAssignment);
                                    
                                    if(!recordWithError.contains(existingAssignment)) {
                                        existingAssignment.DefaultTeam__c = false;
                                        otherPrimaryTeamList.add(existingAssignment);
                                    }
                                    else {
                                         System.debug('AP42.uncheckOtherDefaultTeams.INFO - An error has already been added to this record');
                                    }
                                }
                            }
                        }
                        System.debug('AP42.uncheckOtherDefaultTeams.INFO - End of the second loop on existing team assignments for the related agent.');
                        
                    }
                    else {
                        System.debug('AP42.uncheckOtherDefaultTeams.ERROR - The map with all teams does not contain the related agent ID.');
                    }
                }
                
                System.debug('AP42.uncheckOtherDefaultTeams.INFO - End of the first loop on new team assignments for the related agent.');
                
                if(!isDuplicateFound) {
                    System.debug('AP42.uncheckOtherDefaultTeams.ERROR - No duplicate found.');
                }
                
                if(otherPrimaryTeamList.size() == 0) {
                    System.debug('AP42.uncheckOtherDefaultTeams.SUCCESS - No existing team assignment by default found.');
                }
                else {
                    System.debug('AP42.uncheckOtherDefaultTeams.INFO - List of team to uncheck "team by default" ='+otherPrimaryTeamList);
                }
                
                // Update records
                AP42_bypass= true;
                List<Database.SaveResult> otherPrimaryTeamList_SR = Database.Update(otherPrimaryTeamList,false);

                For(Database.SaveResult TeamsList_SR:otherPrimaryTeamList_SR){
                    if(!TeamsList_SR.isSuccess()) {
                        DataBase.Error DMLerror = TeamsList_SR.getErrors()[0];
                        System.debug('AP42.uncheckOtherDefaultTeams.DML_EXCEPTION - Error When updating team-assignment.');
                        System.debug('AP42.uncheckOtherDefaultTeams.DML_EXCEPTION - Error Message: ' + DMLerror.getMessage());
                    }
                    else {
                        System.debug('AP42.uncheckOtherDefaultTeams.SUCCESS - Assignment updated successfully. ');
                    }
                }
            }
            catch(Exception e) { 
                System.debug('AP42.uncheckOtherDefaultTeam.'+e.getTypeName()+'- '+ e.getMessage());
                System.debug('AP42.uncheckOtherDefaultTeam.'+e.getTypeName()+'- Line = '+ e.getLineNumber());
                System.debug('AP42.uncheckOtherDefaultTeam.'+e.getTypeName()+'- Stack trace = '+ e.getStackTraceString());
            }
        }   
        else {
            System.debug('AP42.uncheckOtherDefaultTeams.ERROR - Entry map null or empty.'); 
        }
        
        System.debug('AP42.uncheckOtherDefaultTeams.INFO - End of method.');
    }
    
}