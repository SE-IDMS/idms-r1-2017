/******************************************
* Developer: Fielo Team                   *
*******************************************/
public with sharing class FieloPRM_AP_PRMInvoiceDetailTriggers{

    public static void assignsCurrency(){

        List<FieloPRM_InvoiceDetail__c> triggernew = trigger.new;
        
        Set<Id> setInvoices = new Set<Id>();
        
        for(FieloPRM_InvoiceDetail__c invoiceDetail: triggernew){
            setInvoices.add(invoiceDetail.F_PRM_Invoice__c);
        }
        
        Map<Id,String> mapInvoiceCurrency = new Map<Id,String>();
               
        for(FieloPRM_Invoice__c invoice: [SELECT Id, CurrencyIsoCode FROM FieloPRM_Invoice__c WHERE Id IN: setInvoices]){
            mapInvoiceCurrency.put(invoice.Id,invoice.CurrencyIsoCode);
        }

        for(FieloPRM_InvoiceDetail__c invoiceDetail: triggernew){
            if(mapInvoiceCurrency.containsKey(invoiceDetail.F_PRM_Invoice__c)){
                invoiceDetail.CurrencyIsoCode = mapInvoiceCurrency.get(invoiceDetail.F_PRM_Invoice__c);
            }
        }
             
    }

}