/*
@Author: Deepak
Date: 04/03/2014
Description: To update TECH_IsSVMXRecordPresentForContact__c on Contact object if the Contact is used in 
             ServiceMax related objects like WorkOrder,Opportunity Notification,Service Role,Service Contract etc..
*/
public class AP_IsSVMXRelatedContact
{
    public static void UpdateContact(Set<Id> ConId)
    {
        if( (ConId.size() > 0) && !(ConId.isempty())) //Added by VISHNU C July 2016 Release Too Many SOQL WO Interface
        {
            List<Contact> conlst1 = new List<Contact>();
            conlst1 = [select id,TECH_IsSVMXRecordPresentForContact__c from Contact where id in : ConId and TECH_IsSVMXRecordPresentForContact__c =false]; 
            List<Contact> conlst = new List<Contact>();
            if(conlst1.size()>0 && conlst1 != null)
            {
                /*for(Id ac: ConId)
                {
                    Contact con = new Contact(Id=ac);
                    con.TECH_IsSVMXRecordPresentForContact__c = true;
                    conlst.add(con);
                }*/
                for(Contact cobj: conlst1 ){
                   cobj.TECH_IsSVMXRecordPresentForContact__c = true;
               } 
                update conlst1 ;
            }
            //if(conlst.size()>0) update conlst;
        }
    }
    
    Public static void CheckDuplicateRole(List<Role__c> objList,String event){
    
        set<id> ipidset = new Set<id>();
        Set<id> roleidset = new Set<id>();
        for(Role__c obj:objList){
        
            if(obj.InstalledProduct__c != null ){
               ipidset.add(obj.InstalledProduct__c);
            }
            if(event == 'UPDATE')
            {
                roleidset.add(obj.id);
            }
        
        }
        List<Role__c> roleList = new List<Role__c>();
        Map<id,List<Role__c>> existingroleMap = new Map<id,List<Role__c>>();
        if(event == 'UPDATE')
        {           
            if(roleidset != null && ipidset != null ){            
                roleList =[select id ,Role__c,InstalledProduct__c,ToDelete__c  from Role__c where InstalledProduct__c in :ipidset and id NOT IN :roleidset  ];                
            }        
        }
        else{
            if(ipidset != null && ipidset != null ){            
                roleList =[select id ,Role__c,InstalledProduct__c,ToDelete__c  from Role__c where InstalledProduct__c in :ipidset   ];                
            }
        
        }
        if(roleList != null && roleList.size()>0){
            
            for(Role__c obj:roleList ){
                if(!existingroleMap.containskey(obj.InstalledProduct__c))
                {
                    existingroleMap.put(obj.InstalledProduct__c,new List<Role__c>{obj});
                }
                else{ existingroleMap.get(obj.InstalledProduct__c).add(obj);}
            }
            
        }
        for(Role__c obj:objList  ){
        
            if(obj.InstalledProduct__c != null  ){
                if(existingroleMap.containskey(obj.InstalledProduct__c))
                {
                  for(Role__c  eobj:existingroleMap.get(obj.InstalledProduct__c))
                  {
                      if(obj.Role__c == eobj.Role__c && eobj.ToDelete__c==false)
                      { 
                          obj.AddError(System.Label.CLJUL14SRV02);
                      }
                  }
                }
            }
           
        
        }
    
    
    }
}