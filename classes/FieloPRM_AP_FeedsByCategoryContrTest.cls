/**************************************************************************************
    Author: Fielo Team
    Date: 14/04/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/
@isTest
public with sharing class FieloPRM_AP_FeedsByCategoryContrTest{
  
    public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Tag__c tag= new FieloEE__Tag__c();
        FieloEE__Category__c cat = new FieloEE__Category__c();
        User u = new User();
         
        FieloEE__Component__c comp;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c= 'test'
            );
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc= new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;
        
            /*FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(
                FieloEE__MemberId__c= mem.Id
            );
            Insert memb;*/
            
            menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'test'
            );
            Insert menu;

            String idRTrComponent = [SELECT Id, DeveloperName, sObjecttype FROM RecordType WHERE sObjecttype = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_PartnerJourneyWidget'].Id;
                      
            //Category
            String idRecordTypeCat = [Select id, developerName, sobjecttype from RecordType WHERE sobjecttype = 'FieloEE__Category__c' and developerName = 'CategoryItem'].id;

            tag = new FieloEE__Tag__c(
                Name = 'tag',
                FieloEE__AttachmentId__c = 'test'
            );
            Insert tag;

            cat = FieloPRM_UTILS_MockUpFactory.createCategory(menu.id);
            
            insert new CS_PRM_RichTextFields__c(Name = 'FieloEE__Component__c', Fields__c = 'FieloEE__Content__c', Pattern__c = 'img');
            
            comp = new FieloEE__Component__c(
                Name = 'test', 
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = idRTrComponent,
                FieloEE__Tag__c = tag.Id,
                FieloEE__Category__c = cat.Id
            );
            Insert comp;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            FieloEE__RedemptionRuleCriteria__c rrc = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__FieldName__c = 'name',
                FieloEE__RedemptionRule__c = seg.Id
            );
            insert rrc;

            FieloEE__News__c news = new FieloEE__News__c(
                //RecordTypeId = rtNews, 
                FieloEE__Title__c = 'Test', 
                FieloEE__QuantityComments__c = 0, 
                FieloEE__IsActive__c = true,
                FieloEE__Body__c = 'Testing', 
                FieloEE__QuantityLikes__c = 0, 
                FieloEE__HasSegments__c = true,
                FieloEE__CategoryItem__c = cat.Id
            );
            Insert news;

            FieloEE__SegmentDomain__c segDom = new FieloEE__SegmentDomain__c(
                FieloEE__Segment__c = seg.id, 
                FieloEE__News__c =  news.Id
            );
            Insert segDom;
            
            FieloEE__Tag__c tag2 = new FieloEE__Tag__c(
                Name = 'tag2',
                FieloEE__AttachmentId__c = 'test'
            );
            Insert tag2;

            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag.Id, 
                FieloEE__News__c = news.id
            );
            Insert tagItem;
            
            FieloEE__TagItem__c tagItem2 = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag2.Id, 
                FieloEE__News__c = news.id
            );
            Insert tagItem2;

            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = mem.Id, 
                FieloEE__Segment2__c = seg.Id
            );
            Insert memSeg;
            
            FieloEE__Member__c membertest = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c WHERE id =: mem.id ];

            u = [SELECT Id, UserName FROM User WHERE Id =: membertest.FieloEE__User__c ];
        }
        system.runAs(u){
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            FieloPRM_AP_FeedsByCategoryContr controller = new FieloPRM_AP_FeedsByCategoryContr();
            system.debug('comp: ' +comp);
            controller.componentObj = comp;
            String fieldB = controller.fieldBody;
            String fieldT = controller.fieldTitle;
            String fieldLL = controller.fieldLinkLabel;
            String fieldEL = controller.fieldExternalLink;
            String fieldTN = controller.fieldTagName;
            controller.init();
            Set<String> setQueryFields = new Set<String>{'Id'};
            setQueryFields.add('FieloEE__Body__c');
            controller.iswidget(tag.id,cat.Id,setQueryFields);
        }

    }
    
    public static testMethod void testUnit2(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Tag__c tag= new FieloEE__Tag__c();
        User u = new User();
         
        FieloEE__Component__c comp;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c= 'test'
            );
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc= new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;
        
            /*FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(
                FieloEE__MemberId__c= mem.Id
            );
            Insert memb;*/
            
            menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'test'
            );
            Insert menu;

            String idRTrComponent = [SELECT Id, DeveloperName, sObjecttype FROM RecordType WHERE sObjecttype = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_C_FeedsByCategory'].Id;
                      
            //Category
            String idRecordTypeCat = [Select id, developerName, sobjecttype from RecordType WHERE sobjecttype = 'FieloEE__Category__c' and developerName = 'CategoryItem'].id;

            tag = new FieloEE__Tag__c(
                Name = 'tag',
                FieloEE__AttachmentId__c = 'test'
            );
            Insert tag;

            FieloEE__Category__c cat = FieloPRM_UTILS_MockUpFactory.createCategory(menu.id);
            
            insert new CS_PRM_RichTextFields__c(Name = 'FieloEE__Component__c', Fields__c = 'FieloEE__Content__c', Pattern__c = 'img');
            
            comp = new FieloEE__Component__c(
                Name = 'test', 
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = idRTrComponent,
                F_PRM_PartnerJourneyBehaviour__c = 'On click'
            );
            Insert comp;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            FieloEE__RedemptionRuleCriteria__c rrc = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__FieldName__c = 'name',
                FieloEE__RedemptionRule__c = seg.Id
            );
            insert rrc;

            FieloEE__News__c news1 = new FieloEE__News__c(
                //RecordTypeId = rtNews, 
                FieloEE__Title__c = 'Test', 
                FieloEE__QuantityComments__c = 0, 
                FieloEE__IsActive__c = true,
                FieloEE__Body__c = 'Testing', 
                FieloEE__QuantityLikes__c = 0, 
                FieloEE__HasSegments__c = true,
                FieloEE__Component__c = comp.Id,
                FieloEE__CategoryItem__c = cat.Id,
                FieloEE__AttachmentId__c = 'test'
            );
            Insert news1;
            
            FieloEE__News__c news2 = new FieloEE__News__c(
                //RecordTypeId = rtNews, 
                FieloEE__Title__c = 'Test', 
                FieloEE__QuantityComments__c = 0, 
                FieloEE__IsActive__c = true,
                FieloEE__Body__c = 'Testing', 
                FieloEE__QuantityLikes__c = 0, 
                FieloEE__HasSegments__c = true,
                FieloEE__Component__c = comp.Id,
                FieloEE__CategoryItem__c = cat.Id,
                F_PRM_Parent__c = news1.Id,
                FieloEE__AttachmentId__c = 'test'
            );
            Insert news2;                       

            FieloEE__SegmentDomain__c segDom = new FieloEE__SegmentDomain__c(
                FieloEE__Segment__c = seg.id, 
                FieloEE__News__c =  news1.Id
            );
            Insert segDom;
            
            FieloEE__Tag__c tag2 = new FieloEE__Tag__c(
                Name = 'tag2',
                FieloEE__AttachmentId__c = 'test'
            );
            Insert tag2;

            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag.Id, 
                FieloEE__News__c = news1.id
            );
            Insert tagItem;
            
            FieloEE__TagItem__c tagItem2 = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag2.Id, 
                FieloEE__News__c = news1.id
            );
            Insert tagItem2;

            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = mem.Id, 
                FieloEE__Segment2__c = seg.Id
            );
            Insert memSeg;
            
            FieloEE__Member__c membertest = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c WHERE id =: mem.id ];

            u = [SELECT Id, UserName FROM User WHERE Id =: membertest.FieloEE__User__c ];
        }
        system.runAs(u){
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            FieloPRM_AP_FeedsByCategoryContr controller = new FieloPRM_AP_FeedsByCategoryContr();
            controller.componentObj = comp;
            String fieldB = controller.fieldBody;
            String fieldT = controller.fieldTitle;
            String fieldLL = controller.fieldLinkLabel;
            String fieldEL = controller.fieldExternalLink;
            String fieldTN = controller.fieldTagName;
            controller.init();
        }

    }

    public static testMethod void testUnit3(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Tag__c tag= new FieloEE__Tag__c();
        User u = new User();
         
        FieloEE__Component__c comp;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c= 'test'
            );
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc= new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;
        
            /*FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(
                FieloEE__MemberId__c= mem.Id
            );
            Insert memb;*/
            
            menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'test'
            );
            Insert menu;

            String idRTrComponent = [SELECT Id, DeveloperName, sObjecttype FROM RecordType WHERE sObjecttype = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_PartnerJourneyWidget'].Id;
                      
            //Category
            String idRecordTypeCat = [Select id, developerName, sobjecttype from RecordType WHERE sobjecttype = 'FieloEE__Category__c' and developerName = 'CategoryItem'].id;

            tag = new FieloEE__Tag__c(
                Name = 'tag'
            );
            Insert tag;

            FieloEE__Category__c cat = FieloPRM_UTILS_MockUpFactory.createCategory(menu.id);
            
            insert new CS_PRM_RichTextFields__c(Name = 'FieloEE__Component__c', Fields__c = 'FieloEE__Content__c', Pattern__c = 'img');
            
            comp = new FieloEE__Component__c(
                Name = 'test', 
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = idRTrComponent,
                F_PRM_PartnerJourneyBehaviour__c = 'On click'
            );
            Insert comp;
            
            Id rtSeg  = [SELECT Id, DeveloperName, SObjectType FROM RecordType WHERE SobjectType = 'FieloEE__RedemptionRule__c' and DeveloperName = 'Manual'].Id;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            FieloEE__RedemptionRuleCriteria__c rrc = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__FieldName__c = 'name',
                FieloEE__RedemptionRule__c = seg.Id
            );
            insert rrc;

            FieloEE__News__c news = new FieloEE__News__c(
                //RecordTypeId = rtNews, 
                FieloEE__Title__c = 'Test', 
                FieloEE__QuantityComments__c = 0, 
                FieloEE__IsActive__c = true,
                FieloEE__Body__c = 'Testing', 
                FieloEE__QuantityLikes__c = 0, 
                FieloEE__HasSegments__c = true,
                FieloEE__CategoryItem__c = cat.Id
            );
            Insert news;
                                
            FieloEE__SegmentDomain__c segDom = new FieloEE__SegmentDomain__c(
                FieloEE__Segment__c = seg.id, 
                FieloEE__News__c =  news.Id
            );
            Insert segDom;
            
            FieloEE__Tag__c tag2 = new FieloEE__Tag__c(
                Name = 'tag2'
            );
            Insert tag2;

            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag.Id, 
                FieloEE__News__c = news.id
            );
            Insert tagItem;
            
            FieloEE__TagItem__c tagItem2 = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag2.Id, 
                FieloEE__News__c = news.id
            );
            Insert tagItem2;

            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = mem.Id, 
                FieloEE__Segment2__c = seg.Id
            );
            Insert memSeg;
            
            FieloEE__Member__c membertest = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c WHERE id =: mem.id ];

            u = [SELECT Id, UserName FROM User WHERE Id =: membertest.FieloEE__User__c ];
        }
        system.runAs(u){
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            ApexPages.currentPage().getParameters().put('idTag', tag.id);
            //ApexPages.currentPage().getParameters().put('idCategory', cat.id);
            FieloPRM_AP_FeedsByCategoryContr controller = new FieloPRM_AP_FeedsByCategoryContr();
            controller.componentObj = comp;
            String fieldB = controller.fieldBody;
            String fieldT = controller.fieldTitle;
            String fieldLL = controller.fieldLinkLabel;
            String fieldEL = controller.fieldExternalLink;
            String fieldTN = controller.fieldTagName;
            controller.init();
        }

    }

    public static testMethod void testUnit4(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Tag__c tag= new FieloEE__Tag__c();
        User u = new User();
         
        FieloEE__Component__c comp;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c= 'test'
            );
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc= new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;
        
            /*FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(
                FieloEE__MemberId__c= mem.Id
            );
            Insert memb;*/
            
            menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'test'
            );
            Insert menu;

            String idRTrComponent = [SELECT Id, DeveloperName, sObjecttype FROM RecordType WHERE sObjecttype = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_PartnerJourneyWidget'].Id;
                      
            

            tag = new FieloEE__Tag__c(
                Name = 'tag'
            );
            Insert tag;

            FieloEE__Category__c cat = FieloPRM_UTILS_MockUpFactory.createCategory(menu.id);
                
            insert new CS_PRM_RichTextFields__c(Name = 'FieloEE__Component__c', Fields__c = 'FieloEE__Content__c', Pattern__c = 'img');
            
            comp = new FieloEE__Component__c(
                Name = 'test', 
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = idRTrComponent,
                F_PRM_PartnerJourneyBehaviour__c = 'On click'
            );
            Insert comp;
            
            Id rtSeg  = [SELECT Id, DeveloperName, SObjectType FROM RecordType WHERE SobjectType = 'FieloEE__RedemptionRule__c' and DeveloperName = 'Manual'].Id;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            FieloEE__RedemptionRuleCriteria__c rrc = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__FieldName__c = 'name',
                FieloEE__RedemptionRule__c = seg.Id
            );
            insert rrc;

            FieloEE__News__c news = new FieloEE__News__c(
                //RecordTypeId = rtNews, 
                FieloEE__Title__c = 'Test', 
                FieloEE__QuantityComments__c = 0, 
                FieloEE__IsActive__c = true,
                FieloEE__Body__c = 'Testing', 
                FieloEE__QuantityLikes__c = 0, 
                FieloEE__HasSegments__c = true,
                FieloEE__CategoryItem__c = cat.Id
            );
            Insert news;
                                
            FieloEE__SegmentDomain__c segDom = new FieloEE__SegmentDomain__c(
                FieloEE__Segment__c = seg.id, 
                FieloEE__News__c =  news.Id
            );
            Insert segDom;
            
            FieloEE__Tag__c tag2 = new FieloEE__Tag__c(
                Name = 'tag2'
            );
            Insert tag2;

            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag.Id, 
                FieloEE__News__c = news.id
            );
            Insert tagItem;
            
            FieloEE__TagItem__c tagItem2 = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag2.Id, 
                FieloEE__News__c = news.id
            );
            Insert tagItem2;

            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = mem.Id, 
                FieloEE__Segment2__c = seg.Id
            );
            Insert memSeg;
            
            FieloEE__Member__c membertest = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c WHERE id =: mem.id ];

            u = [SELECT Id, UserName, LanguageLocaleKey FROM User WHERE Id =: membertest.FieloEE__User__c ];
            
            u.LanguageLocaleKey = 'th';
            
            update u;
        }
        system.runAs(u){
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            ApexPages.currentPage().getParameters().put('idTag', tag.id);
            //ApexPages.currentPage().getParameters().put('idCategory', cat.id);
            FieloPRM_AP_FeedsByCategoryContr controller = new FieloPRM_AP_FeedsByCategoryContr();
            controller.componentObj = comp;
            String fieldB = controller.fieldBody;
            String fieldT = controller.fieldTitle;
            String fieldLL = controller.fieldLinkLabel;
            String fieldEL = controller.fieldExternalLink;
            String fieldTN = controller.fieldTagName;
            controller.init();
        }

    }

}