global class BatchCpAlignmentBackOffice implements Database.Batchable<sObject>, Schedulable {

    public Integer rescheduleInterval = Integer.ValueOf(Label.CLAPR14SRV13);
    public String query = 'SELECT Id,InstalledAtAccount__c,TECH_isAddressSynchedWithAccount_c__c,installedAtStreet__c , InstalledAtAccount__r.Street__c , installedAtCity__c , InstalledAtAccount__r.City__c, installedAtStateProvinceCode__c , InstalledAtAccount__r.StateProvince__c, installedAtZipCode__c , InstalledAtAccount__r.ZipCode__c  FROM SVMXC__Service_Contract_Products__c WHERE TECH_isAddressSynchedWithAccount_c__c= False limit '+Label.CLAPR14SRV14;
      public String scheduledJobName = 'BatchCoveredProductAlignmentBackOffice';
      public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
     global Database.Querylocator start(Database.BatchableContext BC){
       
        return Database.getQueryLocator(query);    
    }
    
     global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        List<SVMXC__Service_Contract_Products__c> cplist = new List<SVMXC__Service_Contract_Products__c>();
        for(Sobject sobj: scope)
        {
            
            SVMXC__Service_Contract_Products__c cp = (SVMXC__Service_Contract_Products__c)sobj;
            cp.installedAtStreet__c = cp.InstalledAtAccount__r.Street__c;
            cp.installedAtCity__c = cp.InstalledAtAccount__r.City__c;
            cp.installedAtStateProvinceCode__c = cp.InstalledAtAccount__r.StateProvince__c;
            cp.installedAtZipCode__c = cp.InstalledAtAccount__r.ZipCode__c;
            cplist.add(cp);
            
            
        }
        
        DataBase.update(cplist,false);
        
    }
    
    global void finish(Database.BatchableContext BC) {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);
                     
        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            // There is at least one scheduled job already for this name
            // Looking if one of scheduled job with the correct name is in an 'Active' state
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new BatchLocationAddressAlignmentWithAccount(), scheduledJobName, rescheduleInterval);
        }
    }
    
    global void execute(SchedulableContext sc) {
        BatchCpAlignmentBackOffice CoverPorduct = new BatchCpAlignmentBackOffice ();
        Database.executeBatch(CoverPorduct );
    }    

}