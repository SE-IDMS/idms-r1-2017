public class AP_UpdateTeamSharing {

    public static Map<String,Schema.SObjectType> getLookupFieldMap() {
        Schema.DescribeSObjectResult d =  TeamMember__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = Schema.sObjectType.TeamMember__c.fields.getMap();
        Set<String> setLookUpFields = new Set<String>();
        Map<String,Schema.SObjectType> mapLookupFieldObj = new Map<String,Schema.SObjectType>();
        Schema.DisplayType fieldType = Schema.DisplayType.REFERENCE;

        Set<String> setFieldsToRemove = new Set<String>(  CS_StdFieldsToRemove__c.getall().keySet() );
        system.debug('setFieldsToRemove:'+setFieldsToRemove);
        for(String f1 : fieldMap.keySet()) {
            if(fieldMap.get(f1).getDescribe().getType() == fieldType && !setFieldsToRemove.contains(f1))
                mapLookupFieldObj.put(fieldMap.get(f1).getDescribe().getName(),fieldMap.get(f1).getDescribe().getReferenceTo().get(0));
        }
        return mapLookupFieldObj;
    }

    private static Map<String, List<SObject>> getShareRecordList(Map<String,Schema.SObjectType> mapLookupFieldObj, Map<Id, TeamMember__c> mapShare) {
        List<SObject> lstShareRecord = new list<sObject>();
        List<SObject> lstUsers = new List<SObject>();
        Map<String, List<SObject>> tobeShared = new Map<String, List<SObject>>();
        
        for(Id userShareID :  mapShare.keySet()) {
            for(String fieldName : mapLookupFieldObj.keySet()) {
                if(mapShare.get(userShareID).get(fieldName) != null) {

                    Schema.SObjectType ObjectName1 = mapLookupFieldObj.get(fieldName);
                    Schema.SObjectType ObjectName2 = mapLookupFieldObj.get(fieldName);

                    String rowCause = String.valueOf(ObjectName2).replace('__c','Team__c');
                    String ObjectName = String.valueOf(ObjectName1).replace('__c','__Share');

                    if (String.valueOf(ObjectName2).compareTo('PartnerProgram__c') == 0) {
                        SObject user = Schema.getGlobalDescribe().get('User').newSObject();
                        user.put('Id', mapShare.get(userShareID).User__c);
                        lstUsers.add (user);
                    }
                    
                    SObject shareRecord = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;
                    shareRecord.put('ParentId', mapShare.get(userShareID).get(fieldName));
                    shareRecord.put('UserOrGroupId', mapShare.get(userShareID).User__c);
                    if (mapShare.get(userShareID).AccessLevel__c == null)
                        shareRecord.put('AccessLevel', 'Edit');
                    else
                        shareRecord.put('AccessLevel', mapShare.get(userShareID).AccessLevel__c);
                    shareRecord.put('RowCause', rowCause);
                    lstShareRecord.add(shareRecord);
                }
            }
        }
        tobeShared.put('Share', lstShareRecord);
        tobeShared.put('User', lstUsers);
        return tobeShared;
    }

    private static Map<String, String> getShareRecordIdList(Map<String,Schema.SObjectType> mapLookupFieldObj, Map<Id, TeamMember__c> mapShare) {
        List<SObject> lstShareRecord = new list<sObject>();
        Map<String, String> shareRecordMap = new Map<String, String>();
        
        for(Id userShareID :  mapShare.keySet()) {
            for(String fieldName : mapLookupFieldObj.keySet()) {
                if(mapShare.get(userShareID).get(fieldName) != null) {

                    Schema.SObjectType ObjectName1 = mapLookupFieldObj.get(fieldName);
                    Schema.SObjectType ObjectName2 = mapLookupFieldObj.get(fieldName);

                    String ObjectName = String.valueOf(ObjectName1).replace('__c','__Share');
                    String rowCause = String.valueOf(ObjectName2).replace('__c','Team__c');
                    String condition = '';
                    if (shareRecordMap.containsKey (ObjectName) ) {
                        condition = shareRecordMap.get(ObjectName);
                    }
                    if (condition != '') condition += ' OR ';
                    condition += ' (ParentId=\'' + mapShare.get(userShareID).get(fieldName) + '\' AND UserOrGroupId=\'' + mapShare.get(userShareID).User__c + '\' AND RowCause = \'' +  rowCause  +'\')';
                    
                    shareRecordMap.put(ObjectName, condition);
                }
            }
        }
        return shareRecordMap;
    }

    public static void setSharing(Map<Id, TeamMember__c> newUserShare) {
    
        Map<String,Schema.SObjectType> mapLookupFieldObj = getLookupFieldMap();
        Map<String, List<SObject>> tobeShared = getShareRecordList(mapLookupFieldObj, newUserShare);
        Id[] usrArr;
        List<SObject> lstShareRecord = tobeShared.get('Share');
        
        if (tobeShared.get('User') != null && tobeShared.get('User').size() > 0) {
            Integer cnt = 0;
            Integer size = tobeShared.get('User').size();
            usrArr = new Id[size];
            // Granting PermissionSet is a Setup object operation and needs to be executed 
            // in a different context. Hence a call to @future method is done, and to get 
            // around the limitations on types of parameters that can be passed, 
            // List<SObject> is converted to an array of Id
            for(SObject record : tobeShared.get('User')) {
                usrArr[cnt] = ((User) record).Id;
                cnt++;
            }
        }
        System.debug('**Team Sharing:' + lstShareRecord);
        insert lstShareRecord;
    }

    public static void removeSharing(Map<Id, TeamMember__c> oldUserShare) {
    
        Map<String,Schema.SObjectType> mapLookupFieldObj = getLookupFieldMap();
        Map<String, String> mapShareRecord = getShareRecordIdList(mapLookupFieldObj, oldUserShare);

        List<SObject> toDelete = new List<SObject>();
        
        for (String objectName : mapShareRecord.keySet()) {
            String query = 'SELECT Id  FROM '+ objectName + ' WHERE ' + mapShareRecord.get(objectName) + ' LIMIT 1000';

            SObject [] records = Database.query(query);
            toDelete.addAll ( records );
        }
        
        delete toDelete;
    }
    
    // Assigns permission sets to the appropriate users
    // This is run as part of the setup wizard and after installations.
    /*
    @future
    public static void AssignPermissionSets(String psName, Id[] lstUsers) {
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = :psName];
        List <PermissionSetAssignment> PSAssignments = new List <PermissionSetAssignment>();
        Map<Id,User> users = new Map<Id,User>([SELECT ID,(SELECT ID FROM PermissionSetAssignments WHERE PermissionSetID = :ps.id) FROM User WHERE Profile.UserLicense.Name= 'Salesforce Platform' and IsActive = true LIMIT 10000]);
            
        for( Id u : lstUsers ) {
            if(!users.ContainsKey(u)) {
                PSAssignments.add( new PermissionSetAssignment(AssigneeId = u,PermissionSetId = ps.ID ) );
            }
        }
        insert PSAssignments;
    }
    */
}