@isTest
public class IdmsUimsReconcUpdateCompanyBulkTest { 
    
    static testmethod void testCompanyOne(){
    
        String env43 = System.Label.CLQ316IDMS043 ; 
        String env44 = System.Label.CLQ316IDMS044 ;
    
        user userObj1 = new User(alias = 'user', email='test3127415' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                             timezonesidkey='Europe/London', username='test3127415' + '@accenture.com'+System.Label.CLJUN16IDMS71,Company_Postal_Code__c='12345',
                             Company_Phone_Number__c='9986995000',FederationIdentifier ='188b80b1-c622-4f20-8b1d-3ae86af11c4d',IDMS_Registration_Source__c = 'Test',
                             IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                             IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432', IDMS_VU_NEW__c = '1', IDMS_VU_OLD__c='1', IDMS_VC_NEW__c='1',IDMS_VC_OLD__c='1',
                             CompanyName='Accenture',Company_Country__c = '01',IDMSClassLevel2__c = '12',Company_City__c ='US',IDMSCompanyFederationIdentifier__c = '188b80b1-c622-4f20-8b1d-3ae86af11c4d' ,
                             IDMSCompanyPoBox__c = 'US' ,Company_State__c ='US',Company_Address1__c = 'US',Company_Address2__c ='US',
                             IDMSCompanyHeadquarters__c = true ,Company_Website__c = 'www.google.com',IDMSTaxIdentificationNumber__c='1234');
                             
        insert userObj1;
        System.assertEquals(userObj1.email, 'test3127415' + '@accenture.com');
        System.assertEquals(userObj1.IDMS_User_Context__c , 'work');
        if(userObj1 != null){
         IDMSCompanyReconciliationBatchHandler__c companyhandler=new IDMSCompanyReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
         
         insert companyhandler; 
         System.assertEquals(companyhandler.AtCreate__c, true);
        
         List<IDMSCompanyReconciliationBatchHandler__c> lstCompanyHandler=new List<IDMSCompanyReconciliationBatchHandler__c>();
         lstCompanyHandler.add(companyhandler);
         String callFid = 'IDMSAdmin';
         
             try{
             Test.startTest();
             Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
             Map<String, User> mapUimsEmailIdmscompany = new Map<String, User>();
             mapUimsEmailIdmscompany.put(userObj1.email, userObj1);
             IdmsUimsReconcUpdateCompanyBulk.updateUimsCompanies(lstCompanyHandler,'batchId');
             Test.stopTest();
             }catch(exception e){}
         }
       }
    }