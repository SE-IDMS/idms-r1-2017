@isTest
private class VFC90_SearchSolutionCenter_TEST {

//*********************************************************************************
// Controller Name  : VFC90_SearchSolutionCenter_TEST
// Purpose          : Test Controller class for Search Solution Center
// Created by       : Hari Krishna  Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For Sales April 2012 Release
///********************************************************************************/

    static testMethod void VFC79_SearchSolutionCenterTest() {
        //Start of Test Data preparation
     
         
     //   Profile profile = [select id from profile where name='System Administrator' LIMIT 1];
     //   User user = Utils_TestMethods.createStandardUser(profile.id, 'test');
        
          Profile profile1 = [select id from profile where name='System Administrator' LIMIT 1];    
     //   User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse2');
     //   user2.userRoleId = '00EA0000000MiDK';
     //   database.insert(user2);
            
        User user3 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse3');
        user3.userRoleId = '00EA0000000MiDK';                // Role = 'CEO'
        database.insert(user3);
        
    //    User user4 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse4');
    //    user4.userRoleId = '00EA0000000MiDK'; 
    //    database.insert(user4);
    
        PageReference pageRef;
        
             
        //End of Test Data preparation.
    
        system.runAs(user3)
        {        
                
        Country__c country1 = Utils_TestMethods.createCountry();
        insert country1;
    
    
    
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
            
        Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
        oppty.OwnerId = user3.id;      
        insert oppty;  
        
        
        SolutionCenter__c solcenter= Utils_TestMethods.createSolutionCenter();
        solcenter.Name = 'Test for VFP90_SearchSolutionCenter';
        solcenter.BusinessesServed__c ='BD';
        solcenter.TierCategory__c = 'A';
        solcenter.CountryOfLocation__c = country1.id;
        insert  solcenter;
        
        SolutionCenterCountry__c  scc = Utils_TestMethods.createSolutionCenterCountry(solcenter,country1);
        insert scc;
    
        List<String> keywords = New List<String>();
        keywords.add('Test');
        List<String> filters = New List<String>();
        filters.add('BD');
        filters.add(country1.id);
        filters.add('A');
        
        Utils_DataSource dataSource = Utils_Factory.getDataSource('SOCC');
        Utils_DataSource.Result searchResult = dataSource.Search(keywords,filters);
        sObject tempsObject = searchResult.recordList[0];
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();   
                    
            ApexPages.StandardController oc1 = new ApexPages.StandardController(new OPP_SupportRequest__c());        
            VFC90_SearchSolutionCenter SearchSolutionCenter = new VFC90_SearchSolutionCenter(oc1);
            pageRef= Page.VFP90_SearchSolutionCenter; 
            pageRef.getParameters().put('CF00NA000000559Lc_lkid', oppty.id);
            pageRef.getParameters().put('CF00NA000000559Lc', oppty.Name);
    
            System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
            VCC06_DisplaySearchResults componentcontroller = SearchSolutionCenter.resultsController; 
            SearchSolutionCenter.searchText='Test';
            SearchSolutionCenter.level1 = 'BD';
            SearchSolutionCenter.level2 = country1.id;
            SearchSolutionCenter.level3 = 'A';
            Test.setCurrentPageReference(pageRef);        
            SearchSolutionCenter.PerformAction(tempsObject, SearchSolutionCenter);
           // system.debug('********search****'+)
            
            
            SearchSolutionCenter.search();
            SearchSolutionCenter.clear();
            SearchSolutionCenter.cancel();
            
            SearchSolutionCenter.searchText='';
            SearchSolutionCenter.level1 = Label.CL00355;
            SearchSolutionCenter.level2 = Label.CL00355;
            SearchSolutionCenter.level3 = Label.CL00355;
            SearchSolutionCenter.search();
    
            for(Integer index=0; index<1000; index++)
            {
                SearchSolutionCenter.searchText=SearchSolutionCenter.searchText+'1234567890';
            }
            SearchSolutionCenter.search();             
            /*
            OPP_OpportunityCompetitor__c opComp = new OPP_OpportunityCompetitor__c(TECH_CompetitorName__c=comp.id, OpportunityName__C = oppty.id);
            insert opComp;
            SearchSolutionCenter.PerformAction(tempsObject, SearchSolutionCenter);
            
            pageRef.getParameters().put(Label.CL00452, null);
            Test.setCurrentPageReference(pageRef);         
            SearchSolutionCenter.cancel();    
            */       
        }
    
         pageRef = NULL;
     
    
    }
}