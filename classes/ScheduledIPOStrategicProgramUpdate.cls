/*
    Author              : Priyanka Shetty (Schneider Electric)
    Date Created        :  26-Jul-2013
    Description         : Scheduler class to Schedule BatchIPOMilestoneUpdate and BatchIPOCascadingMilestoneUpdate
*/

global class ScheduledIPOStrategicProgramUpdate implements Schedulable{
     global void execute(SchedulableContext sc) {
        BatchIPOMilestoneUpdate b = new BatchIPOMilestoneUpdate();
        database.executebatch(b);
        BatchIPOCascadingMilestoneUpdate c = new BatchIPOCascadingMilestoneUpdate();
        database.executebatch(c);
  }
}