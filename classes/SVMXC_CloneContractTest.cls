@isTest(SeeAllData=true)
private class SVMXC_CloneContractTest {

	static testMethod void testCloneContract() {
		
        Account acct = Utils_TestMethods.createAccount();
        insert acct;
        
        Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
        insert prd;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.SVMXC__Account__c = acct.Id;
        site.Name = 'Test Site';
        insert site;
        
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = site.Id, SVMXC__Company__c = acct.Id,RangeToCreate__c='test',SVMXC__Product__c=prd.Id,SKUToCreate__c='Test2',BrandToCreate__c='schneider-electric',DeviceTypeToCreate__c='Circuit Breaker',TECH_CreatedfromSFM__c =true);
        insert ip;
            
       RecordType rte = [SELECT Id FROM RecordType WHERE SobjectType = 'Role__c' AND DeveloperName = 'InstalledProductAccountRole' LIMIT 1];
        
       Role__c role = new Role__c(               
            Location__c = site.Id,
            Role__c = 'Preferred FSE Buildings',
            RecordTypeId=rte.Id,
            InstalledProduct__c =ip.Id);
            
        insert role;
        
        SVMXC__Service_Contract__c contract1parent = new SVMXC__Service_Contract__c(Name = 'Test Parent', SVMXC__Company__c = acct.Id);
        insert contract1parent;
        
        SVMXC__Service_Contract__c contract1 = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acct.Id,
        							ParentContract__c = contract1parent.Id);
        insert contract1;
        
        SVMXC__Service_Contract__c parent = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acct.Id,
        				SVMXC__Renewed_From__c = contract1parent.Id);
        insert parent;
        
        //SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acct.Id, 
        //				ParentContract__c = parent.Id);
        //insert contract;
        
        SVMXC__Service_Contract_Services__c is = new SVMXC__Service_Contract_Services__c(SVMXC__Service_Contract__c = contract1.Id);
        insert is;

        SVMXC__Service_Contract_Products__c covProd = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c = contract1.Id, SVMXC__Installed_Product__c = ip.Id, IncludedService__c = is.Id);
        insert covProd;
                
        SVMXC__PM_Plan__c pm = new SVMXC__PM_Plan__c(Name = 'test', SVMXC__Service_Contract__c = contract1.Id, SVMXC__Work_Order_Assign_To_User__c = UserInfo.getUserId());
        insert pm;
        
        Test.startTest();
        
        ApexPages.StandardController controller = new ApexPages.StandardController(parent);       
              
        SVMXC_CloneContractExtension cce = new SVMXC_CloneContractExtension(controller);
        
        Test.setCurrentPage(new PageReference('/apex/SVMXC_CloneContract'));
        
        cce.init();

        Test.stopTest();
        
	}
       
}