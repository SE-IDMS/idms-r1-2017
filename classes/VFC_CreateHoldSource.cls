// Created by Uttara PR - Nov 2016 Release - Escape Notes project (RIN Connector) 

Global with sharing class VFC_CreateHoldSource {

    // Variables
    public List<ShipHoldRequest__c> lstSHR {get; set;} // List of Ship Hold Requests selected
    public Boolean noSHRselected {get; set;} // Boolean used to display error message when Ship Hold Request records are not selected
    public Boolean moreSHRselected {get; set;} //Boolean used to display error message when more number of Ship Hold Requests are selected
    public String PrbId {get; set;} // Id of the current problem
    public RequestHoldSource requestParameters = new RequestHoldSource(); // Stores all the paramters that have to sent to IFW
    public Boolean unlockscreen1 {get; set;} // Boolean used to display the message after successful connection
    public Boolean unlockscreen2 {get; set;} // Boolean used to display the message after unsuccessful connection
    public Boolean confirmationmsg {get; set;} // Boolean used to display the confirmation message
    public ResponseObtained ResponseResult = new ResponseObtained(); // It stores all the response obtained
    public Boolean alreadysent {get; set;} // Boolean used to display error message when a successfully sent SHR is being selected again
    public Integer countAlreadySent {get; set;} // Integer used to count a successfully sent SHR is being selected again
    public Integer countunsuccessful {get; set;} // Integer used to count a unsuccessfully sent SHR
    public Boolean noOrgId {get; set;} // Boolean used to display error message when Organization Id not present in Back Office System
    
    public VFC_CreateHoldSource(ApexPages.StandardSetController controller) {
        
        noSHRselected = false;
        moreSHRselected = false;
        unlockscreen1 = false;
        unlockscreen2 = false;
        confirmationmsg = false;
        alreadysent = false;
        countAlreadySent = 0;
        countunsuccessful = 0;
        noOrgId = false;
        
        PrbId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('!!!! PrbId : ' + PrbId);
        
        if (this.lstSHR == null) {
            this.lstSHR = (List<ShipHoldRequest__c>)controller.getSelected();
            System.debug('!!!! lstSHR : ' + lstSHR);            
        }  
        if(lstSHR.size() == 0) {
            noSHRselected = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P09));
        }
        if(lstSHR.size() > Integer.valueOf(Label.CL112016I2P06)) {
            moreSHRselected = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P10));
        }
        
        List<Organization> lstOrg = new List<Organization>();
        
        for(ShipHoldRequest__c shr : [SELECT AffectedProduct__r.StartDatecode__c, AffectedProduct__r.EndDatecode__c, AffectedProduct__r.InitialSerialN__c, AffectedProduct__r.LastSerialN__c, CommercialReference__c, Problem__r.Name, BackOfficeSystem__r.ExternalId__c, Status__c, Message__c, ContainmentAction__c FROM ShipHoldRequest__c WHERE Id IN: lstSHR]) {
            
            if(shr.Status__c == Label.CL112016I2P18) // CL112016I2P18 = 'Success'
                countAlreadySent++;
                        
            ReqProduct orgprod = new ReqProduct();
            orgprod.sku = shr.CommercialReference__c;
            if(shr.AffectedProduct__r.StartDatecode__c != null && String.valueOF(shr.AffectedProduct__r.StartDatecode__c) != '')
                orgprod.startDate = String.valueOf(shr.AffectedProduct__r.StartDatecode__c);
            else
                orgprod.startDate = '';
            if(shr.AffectedProduct__r.EndDatecode__c != null && String.valueOF(shr.AffectedProduct__r.EndDatecode__c) != '')
                orgprod.endDate = String.valueOf(shr.AffectedProduct__r.EndDatecode__c);
            else
                orgprod.endDate = '';
            if(shr.AffectedProduct__r.InitialSerialN__c != null && String.valueOF(shr.AffectedProduct__r.InitialSerialN__c) != '')
                orgprod.initialSerialNumber =  shr.AffectedProduct__r.InitialSerialN__c;
            else
                orgprod.initialSerialNumber = '';
            if(shr.AffectedProduct__r.LastSerialN__c != null && String.valueOF(shr.AffectedProduct__r.LastSerialN__c) != '')
                orgprod.lastSerialNumber = shr.AffectedProduct__r.LastSerialN__c;
            else
                orgprod.lastSerialNumber = '';
                
            Organization org = new Organization();
            org.lineId = '1';
            org.id = shr.BackOfficeSystem__r.ExternalId__c;
            if(org.id == null || org.id == '')
                noOrgId = true;
                
            org.product = orgprod;  
            
            requestParameters.targetERP = Label.CL112016I2P19; // CL112016I2P19 = 'OracleERP'
            requestParameters.id = shr.Problem__r.Name.substring(4); // removing 'PRB-' from Problem Name
            lstOrg.add(org);                
        }
        requestParameters.organization = lstOrg;
        
        if(countAlreadySent > 0) {
            alreadysent = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P15));
        }
        else if(noOrgId == true)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL112016I2P28)); 
        else
            confirmationmsg = true;
    }
    
    public PageReference Cancel() {
        
        PageReference cancelpage = new PageReference('/' + PrbId);
        return cancelpage;
    }
    
    public void SendShipHold() {
        
        confirmationmsg = false;
        
        String accessToken = Label.CL112016I2P20; // CL112016I2P20 = 'ad223e6d06938489aa2f29f74d0e4457'
        Http h1 = new Http();
        
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(System.Label.CL112016I2P12);
        req1.setHeader('Authorization','Bearer '+ accessToken);
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Accept',System.Label.CLOCT15FIO018);
        req1.setHeader(System.Label.CLOCT15FIO043,System.Label.CLOCT15FIO044);
        req1.setHeader(System.Label.CLOCT15FIO045,System.Label.CLOCT15FIO046);
        req1.setHeader(System.Label.CLOCT15FIO047,System.Label.CLOCT15FIO048);
        req1.setHeader(System.Label.CLOCT15FIO049,System.Label.CLOCT15FIO048);
        req1.setTimeout(120000);
        req1.setMethod('POST');
        req1.setBody(JSON.serialize(requestParameters));
        
        System.debug('!!!! JSON.serialize(requestParameters) : ' + JSON.serialize(requestParameters));
        System.debug('!!!! req1 : ' + req1);
        
        HttpResponse res1 = new HttpResponse();
        res1 = h1.send(req1);
        String responseReturned = ''; // String which stores the response from the REST API in JSON form
        responseReturned = res1.getBody();
        
        System.debug('!!!! res1.getBody() : ' + res1.getBody());
        
        if(responseReturned.contains(Label.CL112016I2P21)) { // CL112016I2P21 = 'fault'
            countunsuccessful++;
            
            for(ShipHoldRequest__c shr : lstSHR) {
                
                shr.Status__c = Label.CL112016I2P22; // CL112016I2P22 = 'Error'
                shr.Message__c = Label.CL112016I2P27; // CL112016I2P27 = An error occurred during the creation of Ship Hold Request in the Back Office ERP System. Check Technical Error field for more details.
				shr.TechnicalError__c = responseReturned.substringAfter('description');
                countunsuccessful++;
                
            }
            System.debug('!!!! lstSHR : ' + lstSHR);
            Database.update(lstSHR);
            
            unlockscreen2 = true;
        }    
        
        if(countunsuccessful == 0) {
            ResponseResult = (ResponseObtained)JSON.deserialize(res1.getBody(), ResponseObtained.class); // Deserializing the data
            
            System.debug('!!!! ResponseResult : ' + ResponseResult);
            
            for(ShipHoldRequest__c shr : lstSHR) {
                
                if(ResponseResult.Status.code != null && ResponseResult.status.code == Label.CL112016I2P18) { // CL112016I2P18 = 'Success'
                    shr.Status__c = Label.CL112016I2P18; // CL112016I2P18 = 'Success'
                    shr.Message__c = Label.CL112016I2P18; // CL112016I2P18 = 'Success'
                }                
                else {
                    shr.Status__c = Label.CL112016I2P22; // CL112016I2P22 = 'Error'
					shr.Message__c = Label.CL112016I2P27; // CL112016I2P27 = An error occurred during the creation of Ship Hold Request in the Back Office ERP System. Check Technical Error field for more details.
					shr.TechnicalError__c = responseReturned.substringAfter('description');
                    countunsuccessful++;
                }
            }
            
            System.debug('!!!! lstSHR : ' + lstSHR);
            Database.update(lstSHR);
            
            if(countunsuccessful > 0)
                unlockscreen2 = true;
            else
                unlockscreen1 = true;
        }
    }
    
    // Request Schema
        
    global class ReqProduct {
        
        public String sku; 
        public String startDate;
        public String endDate;
        public String initialSerialNumber;
        public String lastSerialNumber;
    }
    
    global class Organization {
        
        public String lineId;
        public String id;
        public ReqProduct product;
    }
    
    global class RequestHoldSource {
        
        public String targetERP; 
        public String id;
        public List<Organization> organization = new List<Organization>(); 
    }
    
    // Response Schema
    
    global class status {
        
        public String code;
        public String message;
    }
    
    global class ResponseObtained {
        
        public String id;
        public status Status = new Status();
        public String targetERP;
    }
}