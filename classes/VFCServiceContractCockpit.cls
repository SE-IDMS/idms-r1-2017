public with sharing class VFCServiceContractCockpit {

    
    private ID mySerConId;
    public String mData{get;set;}
    public Boolean fCompliancy{get;set;}
    //public List<dobbleWrapper>wOutput{get;set;}{wOutput=new List<dobbleWrapper>();}
    public VFCServiceContractCockpit(ApexPages.StandardController controller){
        mySerConId=controller.getId();
        cockpit_hierarchy();
        //cockpit_news();
    }
    public Map<Id,String> parentSerConIdSet {get;set;}
    public string profileImageUrl    { get; set; }

    public string updopptyId {get;set;}
    public boolean updopptyaction {get;set;}
    public string str_updopptyaction {get{return updopptyaction?'Yes':'No';}}
    public boolean SB_isok {get;set;}{SB_isok=true;}
    
    private void cockpit_hierarchy(){
        Boolean fTopIsOn=false;
        Integer fCount=0;
        parentSerConIdSet=new Map<Id,String>();
        //profileImageUrl=new Map<Id,String>();
        mData='data.addRows([';
        parentSerConIdSet.put(mySerConId,null);
        List<Integer>IntegerListToNullify=new List<Integer>();
        List<String> ListStringTemp1=new List<String>();
        List<String> ListStringTemp2=new List<String>();
        Integer secureBarrier=0;
        Integer i,iTemp;
        Integer IntegerRowLine=0;
        String lastSentenceJS='';
        List<SVMXC__Service_Contract__c> mySerConList;
        try{
            mySerConList=[SELECT Id,ParentContract__c,ParentContract__r.ParentContract__c , (SELECT id FROM ServiceLines__r) FROM SVMXC__Service_Contract__c WHERE Id=:mySerConId AND SVMXC__Active__c = True];
            if(mySerConList.size()==1){
                if(mySerConList[0].Id==mySerConList[0].ParentContract__c||mySerConList[0].Id==mySerConList[0].ParentContract__r.ParentContract__c)throw new BException(System.Label.CL00120);
                SVMXC__Service_Contract__c  myServiceCont=mySerConList[0];
                parentSerConIdSet.put(myServiceCont.id,null);
                if(myServiceCont.ParentContract__c!=null){
                    parentSerConIdSet.put(myServiceCont.ParentContract__c,null);
                    if(myServiceCont.ParentContract__r.ParentContract__c!=null)parentSerConIdSet.put(myServiceCont.ParentContract__r.ParentContract__c,null);
                }
                for(SVMXC__Service_Contract__c  o:myServiceCont.ServiceLines__r)parentSerConIdSet.put(o.Id,null);
            }
            for(SVMXC__Service_Contract__c   o:[SELECT Id FROM SVMXC__Service_Contract__c  WHERE ParentContract__c IN :parentSerConIdSet.keySet()])parentSerConIdSet.put(o.Id,null);
            if(mySerConList[0].ParentContract__r!=null)for(SVMXC__Service_Contract__c  o:[SELECT Id FROM SVMXC__Service_Contract__c  WHERE ParentContract__c IN :parentSerConIdSet.keySet()])parentSerConIdSet.put(o.Id,null);
            List<SVMXC__Service_Contract__c> SerConListForLoop=[SELECT ofVisists__c, SVMXC__Company__c,SVMXC__Company__r.Name,Owner.Name,SVMXC__Activation_Notes__c, SVMXC__Active__c, SVMXC__All_Contacts_Covered__c, SVMXC__All_Products_Covered__c, SVMXC__All_Services_Covered__c, SVMXC__All_Sites_Covered__c, BackOfficeReference__c, BackOfficeSystem__c, BillingOption__c, BillingPlan__c, SVMXC__Billing_Schedule__c, SVMXC__Business_Hours__c, BusinessMix__c, SVMXC__Cancelation_Notes__c, SVMXC__Canceled_By__c, SVMXC__Canceled_On__c, ChannelType__c, SVMXC__Contact__c, ContractClassification__c, Name, SVMXC__Contract_Price2__c, SVMXC__Contract_Price__c, toLabel(ContractType__c), CountryofBackOffice__c, CreatedById, CreatedDate, Credits__c, CurrencyIsoCode, DONOTDELIVER__c, DONOTPRINT__c, SVMXC__Default_Group_Member__c, SVMXC__Default_Parts_Price_Book__c, SVMXC__Default_Service_Group__c, SVMXC__Default_Travel_Price__c, SVMXC__Default_Travel_Unit__c, IsDeleted, SVMXC__Discount__c, SVMXC__Discounted_Price2__c, SVMXC__Discounted_Price__c, SVMXC__End_Date__c, SVMXC__EndpointURL__c, SVMXC__Exchange_Type__c, InitialPrice__c, IsEvergreen__c, IsLocked, Jobonsitetriggerinvoice__c, LastActivityDate, LastModifiedById, LastModifiedDate, toLabel(LeadingBusinessBU__c), MarketSegment__c, MarketSubSegment__c, MayEdit, SVMXC__Minimum_Labor__c, SVMXC__Minimum_Travel__c, OpportunityScope__c, OpportunityType__c, OwnerExpirationNotice__c, OwnerId, ParentContract__c, SVMXC__Primary_Technician__c, Id, SVMXC__Renewal_Date__c, SVMXC__Renewal_Notes__c, SVMXC__Renewal_Number__c, SVMXC__Renewed_From__c, SVMXC__Round_Labor_To_Nearest__c, SVMXC__Round_Travel_To_Nearest__c, SVMXC__Labor_Rounding_Type__c, SVMXC__Travel_Rounding_Type__c, SVMXC__SESSION_ID__c, SVMXC__Service_Level__c, SVMXC__Sales_Rep__c, SVMXC__Select__c, SendCustomerRenewalNotification__c, SendWelcomePackage__c, SVMXC__Service_Contract_Notes__c, SVMXC__Service_Plan__c, SVMXC__Service_Pricebook__c, SVMXC__Start_Date__c, SupportAmount__c, SystemModstamp, TECH_ContactEmail__c, TECH_NotificationDate__c, SVMXC__Weeks_To_Renewal__c, SVMXC__Zone__c FROM SVMXC__Service_Contract__c WHERE Id IN :parentSerConIdSet.keyset() AND SVMXC__Active__c = True];
            Map<id,user> uidphoturlmap = new Map<id,user>();
            Set<id> owneridset = new Set<id>();
            for(SVMXC__Service_Contract__c sc:SerConListForLoop){
                owneridset.add(sc.ownerid);
                }
            List<user> userList=[select SmallPhotoUrl from user where id in: owneridset];
            uidphoturlmap.putAll(userList);
            
            ListStringTemp1.add(null);
            secureBarrier=0;
            fCompliancy=true;
            while(secureBarrier<5000){
                i=0;
                for(SVMXC__Service_Contract__c o:SerConListForLoop){
                    //profileImageUrl =[select FullPhotoUrl from User where Id =o.OwnerId];
                        for(String s:ListStringTemp1)if(o.ParentContract__c==s){
                        Boolean bIncludedInForecast = true;
                        String photourl;
                        if(uidphoturlmap.containskey(o.ownerid))
                        photourl='<img src="'+uidphoturlmap.containskey(o.ownerid)+'" class="HPic"/>';
                        mData+='[{v:\''+o.Id+'\',f:\'<div class="HOppty"><a title="'+System.Label.CLMAY13SRV07+'" href="/'+o.Id+'">'+esc(o.Name)+'</a></div><div class="HAccount"><a title="'+System.Label.CL00122+'" href="/'+o.SVMXC__Company__c+'">'+esc(o.SVMXC__Company__r.Name)+'</a></div><div class="HAmount">'+(o.SVMXC__Contract_Price2__c!=null?((o.SVMXC__Contract_Price2__c/1000).intValue() +'k '+UserInfo.getDefaultCurrency()):' ')+'</div><div class="HLeadingBusiness">'+(o.LeadingBusinessBU__c!=null?((o.LeadingBusinessBU__c)):' ')+'</div><div class="HContracttype">'+(o.ContractType__c!=null?((o.ContractType__c)):' ')+'</div><div class="HStartDate">Start Date:'+o.SVMXC__Start_Date__c.format()+'</div><div class="HCloseDate">End Date:'+o.SVMXC__End_Date__c.format()+'</div><div><a class="HActions" title="'+(o.SVMXC__Active__c?esc('Active'):esc('InActive'))+'"><img id="pic'+o.id+'" src="'+esc(System.Label.CL00125)+'" class="'+(o.SVMXC__Active__c?'Hexclude':'Hinclude')+'"/></a></div><div class="HOwner"><a title="'+System.Label.CLMAY13SRV08+'" href="/'+o.OwnerId+'">'+esc(o.owner.name)+'</a><img src="'+uidphoturlmap.get(o.ownerid).SmallPhotoUrl+'" class="HPic"/></div><div class="CockpitFollow" id="follow1'+o.id+'"></div>\'},\''+(o.ParentContract__c!=null?o.ParentContract__c:o.Id)+'\',\''+esc(o.Name)+'\'],';
                        lastSentenceJS+=bIncludedInForecast?'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:5px solid green;\');':'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:3px solid #888888;\');';
                        //if(o.TobeDeleted__c)lastSentenceJS+='data.setRowProperty('+IntegerRowLine+',\'style\',\'border: 3px solid red;\');';
                        ListStringTemp2.add(o.Id);
                        IntegerListToNullify.add(i);
                        IntegerRowLine++;
                    }
                    i++;
                }
                for(Integer j=IntegerListToNullify.size()-1;j>=0;j--)SerConListForLoop.remove(IntegerListToNullify.get(j));
                if(SerConListForLoop.isEmpty())break;
                ListStringTemp1=ListStringTemp2.clone();
                ListStringTemp2.clear();
                IntegerListToNullify.clear();
                secureBarrier++;
            }
            mData=mData.substring(0,mData.length()-1)+']);'+lastSentenceJS;
                
            
           
        }
        catch(Exception exc){ApexPages.addMessages(exc);}
    }

    private string esc(string s){return s!=null?String.escapeSingleQuotes(s):null;}
    /*
    public class wrapperFeed{
        public String myPic{get;set;}
        public String myText{get; set;}
        public String myWhere_Name{get; set;}
        public String myWhere_Id{get; set;}
        public String myWhen{get;set;}
        public String myWho_Id{get;set;}
        public String myWho_Name{get;set;}
        public List<wrapperFeed>myComments {get;set;}
    }
    public class dobbleWrapper{
        public dobbleWrapper(List<wrapperFeed> w,String c){myWrapper=w;myClass=c;}
        public List<wrapperFeed> myWrapper{get; set;}
        public String myClass{get; set;}
    }*/
    
    public class BException extends Exception {} 

    

}