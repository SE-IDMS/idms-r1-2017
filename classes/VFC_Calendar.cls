/**
 *  About
 *  -----
 *  Author: Paul Roth <proth@salesforce.com>
 *  Created for: Project SE
 *  Create date: Wed Aug 07 13:41:28 CDT 2013 @820 /Internet Time/
 *  
 *  Details
 *  -----
 *  Controller for the Icon Calendar Page
 *  
 *  Update History
 *  -----
 *  Wed Aug 07 13:41:28 CDT 2013 @820 /Internet Time/ - initial version
 *  
 *  Issues / TODOs 
 *  -----
 *  (There are a number of tasks, please see rally project)
**/
global with sharing class VFC_Calendar {
    
    /**
     *  Translate the locale to a culture name
     *  @see https://github.com/jquery/globalize
     *  @return (String)
    **/
    public static String getLocale(){
        //-- @TODO: use a custom setting for the ballback instead of a separate file.
        String localization = UserInfo.getLocale();
        //localization = 'cucamonga';
        localization = 'en-US';//localization.replace( '_', '-' );
        return( localization );
    }
    
    /**
     *  Translate the locale to a culture name
     *  @see https://github.com/jquery/globalize
     *  @return (String)
    **/
    public static String getLang(){
        String lang = UserInfo.getLanguage();
        lang = lang.replace( '_', '-' );
        return( lang );
    }
    
    
    /**
     *  Determines the events for a particular range
     *  @param startDate (Date)
     *  @param endDate (Date)
    **/
    public static VFC_CalendarEvent[] getEventsRange( Date startDate, Date endDate ){
        VFC_CalendarEvent[] results = new VFC_CalendarEvent[]{};
        
        if( startDate == null || endDate == null ){
            return( results );
        }
        
        for( Event e : [ select Id, WhoId, Who.name, WhatId, what.name, Subject, Location, IsAllDayEvent, ActivityDateTime, ActivityDate, DurationInMinutes, StartDateTime, EndDateTime, Description, AccountId, ClientConfirmed__c
            from Event
            where  activityDate >= :startDate and activityDate <= :endDate and OwnerId = :UserInfo.getUserId()
        ]){
            results.add( new VFC_CalendarEvent( e ) );
        }
        
        return( results );
    }
    
    /**
     *  Determines the tasks for a particular range
     *  @param startDate (Date)
     *  @param endDate (Date)
    **/
    public static VFC_CalendarEvent[] getTasksRange( Date startDate, Date endDate ){
        VFC_CalendarEvent[] results = new VFC_CalendarEvent[]{};
        
        if( startDate == null || endDate == null ){
            return( results );
        }
        
        for( Task t: [select Id, RecordTypeId, WhoId, Who.Name, WhatId, What.Name, Subject, Description, ActivityDate, Status, ReminderDateTime
            from Task
            where ActivityDate >= :startDate and ActivityDate <= :endDate and OwnerId = :UserInfo.getUserId()
        ]){
            results.add( new VFC_CalendarEvent( t ) );
        }
        
        return( results );
    }
    
    /**
     *  Determines the events and tasks for a particular range
     *  @param startDateStr (String) - parsable date
     *  @param endDateStr (String) - parsable date
    **/
    @RemoteAction
    global static VFC_CalendarEvent[] getEventsRange( String startDateStr, String endDateStr ){
        Date startDate = null;
        Date endDate = null;
        VFC_CalendarEvent[] results = new VFC_CalendarEvent[]{};
        
        try {
            startDate = Date.valueOf( startDateStr );
        } catch( Exception err ){
            System.debug( 'Error occurred while parsing startDate[' + startDateStr + ']' );
            return( results );
        }
        try {
            endDate = Date.valueOf( endDateStr );
        } catch( Exception err ){
            System.debug( 'Error occurred while parsing endDate[' + endDateStr + ']' );
            return( results );
        }
        
        results = getEventsRange( startDate, endDate );
        results.addAll( getTasksRange( startDate, endDate ));
        
        return( results );
    }
}