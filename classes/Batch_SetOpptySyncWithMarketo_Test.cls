@isTest
private class Batch_SetOpptySyncWithMarketo_Test {
    static testMethod void TestSetOpptySynchronizeWithMarketo(){
        List<Profile> profiles1 = [select id from profile where name='SE - Marcom Lead Converter'];
        if(profiles1.size()>0){
            User user = Utils_TestMethods.createStandardUser(profiles1[0].Id,'tUsrAP19');  
            //user.bypassWF__c = false;  
            user.bypassVR__c = true;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            
            Contact con1 = new Contact(SynchronizeWithMarketo__c = 1.0, LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            Opportunity opp= Utils_TestMethods.createOpenOpportunity(Acc.Id);
            insert opp;
            
            OpportunityContactRole oppConrole = new OpportunityContactRole(ContactId =con1.Id, IsPrimary=true, OpportunityId=opp.Id, Role='Decision Maker');
            insert oppConrole;
            Test.StartTest();
                /*Id batchJobId = Database.executeBatch(new Batch_SetOpptySynchronizeWithMarketo(), 10);
                opp.SynchronizeWithMarketo__c = 0.0;
                update opp;
                oppConrole.Role='Primary Customer Contact';
                update oppConrole;*/
                String CRON_EXP = '0 0 * * * ? ';
                 String jobId = System.schedule('Test Opportunity Synchronize With Marketo',
                        CRON_EXP, 
                        new Batch_SetOpptySynchronizeWithMarketo());
            Test.StopTest();
        }
    }
}