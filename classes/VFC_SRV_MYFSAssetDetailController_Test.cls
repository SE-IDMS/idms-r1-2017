@istest
Public Class VFC_SRV_MYFSAssetDetailController_Test{

 Static TestMethod Void UnitTest1(){
    Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
        Country__c Country1 = new Country__c();
        Country1.name= 'France';
        Country1.CountryCode__c = 'FR';
        Insert Country1;
        Account acc1 = new Account(Name = 'TestAccount',Country__c = Country1.Id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
        insert acc1;
        Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        Profile p = [SELECT Id FROM Profile WHERE Name = 'SE - SRV bFS myFS webapp community User'];
        
        /*user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testprmuserlogin.login@schneider-electric.com', 
                          isActive = True, BypassVR__c = True, contactId = cnct.id);
        insert u;*/
         test.starttest();
                
            SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1',  SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False );
            insert location1;
            
            RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                      and developerName = 'LocationContactRole'
                                                      limit 1
                                                     ]; //[0].Id;
           /* Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
            insert ro;*/
            SVMXC__Site__c location2 = new SVMXC__Site__c(Name = 'TestLocation2', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,SVMXC__Parent__c = location1.id );
            insert location2;
            SVMXC__Site__c location3 = new SVMXC__Site__c(Name = 'TestLocation3', SVMXC__Location_Type__c = 'Building', SVMXC__Account__c = acc1.Id,SVMXC__Parent__c = location2.id );
            insert location3;
           
            Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
            insert prd;
                     
            
           Date startdate = date.Today().adddays(-40);
           Date  enddate = date.Today().adddays(+2);
           SVMXC__Installed_Product__c InstalledProduct = new SVMXC__Installed_Product__c();
           InstalledProduct.SVMXC__Company__c =acc1.Id;
           InstalledProduct.SVMXC__Site__c = location1.Id;
           InstalledProduct.UnderContract__c =True;
           InstalledProduct.SVMXC__Warranty_End_Date__c = enddate ;
           InstalledProduct.SVMXC__Warranty_End_Date__c = startdate ;
           Insert InstalledProduct; 
            
           SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
           Contract.SoldtoAccount__c = acc1.id;
           Contract.SVMXC__Service_Contract_Notes__c = 'Test';
           Contract.name= 'TestContract';
           Contract.Status__c= 'PCV';
           Contract.LeadingBusinessBU__c= 'GC';
           Contract.SVMXC__Start_Date__c = startdate;
           Contract.SVMXC__end_Date__c = enddate;
           insert contract;
          
           SVMXC__Service_Contract_Services__c ServiceFeature = new SVMXC__Service_Contract_Services__c();
           ServiceFeature.SVMXC__Service_Contract__c = contract.id;
           
           SVMXC__Service_Contract_Products__c CoveredInstalledProduct = new SVMXC__Service_Contract_Products__c();
           CoveredInstalledProduct.SVMXC__Service_Contract__c = contract.id;
           CoveredInstalledProduct.SVMXC__Installed_Product__c = InstalledProduct.id;
           CoveredInstalledProduct.AssetLocation__c = location1.id;
           insert CoveredInstalledProduct;
            
            VFC_SRV_MYFSAssetDetailController AssetController= new VFC_SRV_MYFSAssetDetailController();
            
            AssetController.site='View All';
            AssetController.assetId = InstalledProduct.Id; 
            AssetController.UltimateParentAccount=acc1.Id;
            AssetController.SelectedCountry = Country1.CountryCode__c;
            AssetController.AssetMethod();
            Test.stoptest();
        
       }
}