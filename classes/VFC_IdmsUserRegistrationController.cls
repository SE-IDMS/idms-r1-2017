public without sharing class VFC_IdmsUserRegistrationController{
// When users enter data into the registration page, their inputs are stored
// in these variables. 
    public String email {get; set;} 
    public String firstName{get; set;} 
    public String lastname{get; set;}   
    public String phomenum{get; set;}
    public String ucountry{get; set;}
    public Boolean notvalid{get;set;}
    public Boolean checked{get;set;}
    public String gridOut{get; set;}
    public Boolean popUPrender{get;set;}
    public Boolean displayPopup{get;set;}
    public Boolean checkedEmailOpt{get;set;}
    public String reg1Check{get; set;}
    public String regCheckEmailOpt{get; set;}
    public List<SelectOption> statusOptionsCountry{get;set;}
    public List<SelectOption> statusOptionsEmailOptIn{get;set;}
    public String redirecturl{get; set;}
    public String redirecturlMsg{get; set;}
    public Boolean showCaptcha{get;set;}
    
    
    public string registrationSource=ApexPages.currentPage().getParameters().get('app');
    public string context = ApexPages.currentPage().getParameters().get('context');
   
     
  // public string registrationSource =Label.CLJUN16IDMS18; 
    
    
    
    
    
    //variable added for recaptch
    public Boolean enterRecaptcha=false;
    public Boolean enterRecaptchcorrect=false;
    private static String baseUrl = Label.CLJUN16IDMS14;
    // The keys you get by signing up for reCAPTCHA for your domain
    private static String privateKey = Label.CLJUN16IDMS15;
    public String publicKey { 
        get { return Label.CLJUN16IDMS16; }
    }
    
     // Create properties for the non-VF component input fields generated
     // by the reCAPTCHA JavaScript.
    public String challenge { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS27);
        }
    }
    public String response  { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS28);
        }
    }
    
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }
    
    public VFC_IdmsUserRegistrationController() {
        this.verified = false;
        String toShowCaptcha=Label.CLJUL16IDMS6; //should get from custom label
        if(toShowCaptcha.equalsIgnoreCase('true')){
        showCaptcha=true;
        } 
        else{
        showCaptcha=false;
        }
        /** OD: This should not get pushed to UAT on 10June2016
        //Pre populate User details only for Social login users 
        String RegSourceFb = [select Id, IDMS_Registration_Source__c from User where Id=:UserInfo.getUserId() limit 1].IDMS_Registration_Source__c;
        if(RegSourceFb == Label.CLJUN16IDMS154)
        {
            email = UserInfo.getUserEmail();
            firstName= UserInfo.getFirstName();
            lastname= UserInfo.getLastName();
            panel1 = false;
        }
        **/
    }
    
     public PageReference verify() {
        enterRecaptcha=true;
        System.debug('reCAPTCHA verification attempt');
        // On first page load, form is empty, so no request to make yet
        if ( challenge == null || response == null ) { 
            System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        }
                    
        HttpResponse r = makeRequest(baseUrl,
            'privatekey=' + privateKey + 
            '&remoteip='  + remoteHost + 
            '&challenge=' + challenge +
            '&response='  + response
        );
        
        if ( r!= null ) {
            this.verified = (r.getBody().startsWithIgnoreCase('true'));
        }
        
        if(this.verified) {
            // If they pass verification
            // Or simply return a PageReference to the "next" page
            enterRecaptchcorrect=true;
            return null;
        }
        else {
            // stay on page to re-try reCAPTCHA
            return null; 
        }
    }
  public PageReference reset() {
        return null; 
    }   

   /* Private helper methods */
    
    private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
        
    private String remoteHost { 
        get { 
            String ret = '127.0.0.1';
            // also could use x-original-remote-host 
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }

//This method is used to load the country picklist and get the terms and condition from custom label
    public void autoRun()
        {
            //String url=Label.CLJUN16IDMS29;
            //String url='<a href='+ Label.IDMS_Term_Condition + ' target=_blank>Terms & Conditions</a>';
            gridOut='color:white;background-color:green;background-image:none;;float:left;';
            
           // reg1Check=Label.CLJUN16IDMS19+' '+ +url+' '+ Label.CLJUN16IDMS20;
            regCheckEmailOpt=Label.CLJUN16IDMS07;
            //redirect url and it's message 
            redirecturl=Label.CLJUN16IDMS26;
            redirecturlMsg=Label.CLJUN16IDMS17;
            Schema.DescribeFieldResult statusFieldCountry = Schema.User.Country__c.getDescribe();
            
            
            
            statusOptionsCountry=new list<SelectOption>();
            //country pick list
            statusOptionsCountry.add(new SelectOption(Label.CLJUN16IDMS06,Label.CLJUN16IDMS05));
            for (Schema.Picklistentry picklistCountry : statusFieldCountry.getPicklistValues())
                {
                    if(picklistCountry.getValue()==Label.CLJUN16IDMS06){
                    continue;
                    }
                    else{
                    statusOptionsCountry.add(new SelectOption(picklistCountry.getValue(),picklistCountry.getLabel()));
                    }
                }
                
                //code for terms condition mapping with app
                
        String termUrl;
        IdmsTermsAndConditionMapping__c termMap;
        termMap=IdmsTermsAndConditionMapping__c.getInstance(registrationSource);
        if(termMap!=null){
         termUrl= (String)termMap.get('url__c');

         if(termMap==null){
         termUrl=Label.CLJUN16IDMS118;
         }
        // languageUser= (String)cntrmap.get('LanguageLocaleKey__c');
         //planguageUser= (String)cntrmap.get('LanguagePreferred_c__c');
         //localeUser= (String)cntrmap.get('LocaleSidKey__c');
         //timezoneUser= (String)cntrmap.get('TimeZoneSidKey__c');
        }
        
        else{
        termUrl=Label.CLJUN16IDMS118;
         }
        
            String url='<a href='+ termUrl + ' target=_blank>Terms & Conditions</a>';
            gridOut='color:white;background-color:green;background-image:none;;float:left;';
            
            reg1Check=Label.CLJUN16IDMS19+' '+ +url+' '+ Label.CLJUN16IDMS20;
        
              //get ap url on the basis of app name
       // String appUrl;
        IdmsAppMapping__c appmap;
        appmap=IdmsAppMapping__c.getInstance(registrationSource);
        if(appmap!=null){
         redirecturl= (String)appmap.get('AppURL__c');

         if(redirecturl==null){
         redirecturl=Label.CLJUN16IDMS26;
         }
               }
        
        else{
        redirecturl=Label.CLJUN16IDMS26;
         }
    

        
               }


  

//This method is used to validate the fields of registration page which user entered and call the create user method 
        public PageReference save() {
        
        // for values of context
        
        //if(context.equalsIgnoreCase('athome')){
        //context='@Home';
         //}
         
         
        //Validation for email to check blank value
            if (email==null||email=='') {
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS02);
            ApexPages.addMessage(msgareaOfF);
            
            return null;
            }
        //validation for Email to check correct format
            if(!Pattern.matches(Label.CLJUN16IDMS30, email))
            {
            notvalid=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS08)); 
            popUPrender=false;
            return null;
            }
        
        else{notvalid=false;}
        
        
        
        //validation for firstname and last name 
            if (firstName==null||firstName==''||lastName==null||lastName=='') {
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS12);
            ApexPages.addMessage(msgareaOfF);
            
            return null;
            }
        
        //validation for phone no. format check
        Boolean result=false;
            if (phomenum!=''){
            
            String PhValid= Label.CLJUN16IDMS31;
            Pattern mypattern=Pattern.compile(PhValid);
            Matcher myMatcher=mypattern.matcher(phomenum);
            result=myMatcher.matches();
            If(result==false){
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS13);
            ApexPages.addMessage(msgareaOfF);
            
            return null;
            }
            else {result = true;}}
            
        //validation for terms and condition check
            if (checked==false) {
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS21);
            
            ApexPages.addMessage(msgntChkd );
            return null;
            }
        
        //validation check if email already exist
            List<User> existingContacts = [SELECT id, email FROM User WHERE email = :email];
            
            if(existingContacts.size() > 0)
            {
            
            popUPrender= true;
            return null;
            
            
            }
        
            else{
            popUPrender=false;
            if((existingContacts.size() == 0) && (Pattern.matches(Label.CLJUN16IDMS30, email))){
            //ApexPages.Message msg2= new ApexPages.Message(ApexPages.Severity.ERROR, 'Email id not exists');
            
            
            
            
            gridOut='color:white;background-color:grey;background-image:none;;float:left;';
            }
            
            }
          /*  //validation for recaptch
            if(enterRecaptchcorrect!=true && enterRecaptcha!=true){
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Enter the Recaptcha');
            
            ApexPages.addMessage(msgntChkd );
            return null;
            
            }
        */
        //get currency,timezone,language,preflanguage,local on the basis of country
         String currencyUser;
        IdmsCountryMapping__c cntrmap;
        cntrmap=IdmsCountryMapping__c.getInstance(ucountry);
        if(cntrmap!=null){
         currencyUser= (String)cntrmap.get('DefaultCurrencyIsoCode__c');

         if(currencyUser==null){
         currencyUser=Label.CLJUN16IDMS04;
         }
        // languageUser= (String)cntrmap.get('LanguageLocaleKey__c');
         //planguageUser= (String)cntrmap.get('LanguagePreferred_c__c');
         //localeUser= (String)cntrmap.get('LocaleSidKey__c');
         //timezoneUser= (String)cntrmap.get('TimeZoneSidKey__c');
        }
        
        else{
        currencyUser=Label.CLJUN16IDMS04;
         }
           //currencyUser=Label.IdmsCurrencyIsoCode;
           String languageUser=Label.CLJUN16IDMS09;
           String planguageUser=Label.CLJUN16IDMS10;
           String localeUser=Label.CLJUN16IDMS11;
           String timezoneUser=Label.CLJUN16IDMS22;
            
            //set email opt in
            String emailOpt='false';
            if(checkedEmailOpt==true){
            emailOpt='Y';
            }
        else{
        emailOpt='U';
        }
        
        //captch call to verify
        if(showCaptcha==true){
         if (challenge!= null || response!= null) { 
            verify();
            if(enterRecaptchcorrect!=true){
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS32);
            ApexPages.addMessage(msgntChkd );
            return null; 
            }
            else{
            enterRecaptchcorrect=true;
            
            }
           
        }
        
        else{
        ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS32);
            ApexPages.addMessage(msgntChkd );
            return null;
        
        }}
         system.debug('context ----------'+context );
          system.debug('registrationSource----------'+registrationSource);
        //Creating user object and setting it's attributes values
            User user = new User();
            user.Email = email;
            user.Country__c=ucountry;
            user.firstName=firstName;
            user.lastName=lastname;
            user.MobilePhone=phomenum;
            user.LanguageLocaleKey=languageUser;
            user.LocaleSidKey=localeUser;
            user.TimeZoneSidKey=timezoneUser;
            user.IDMS_PreferredLanguage__c=planguageUser;
            user.DefaultCurrencyIsoCode=currencyUser;
            user.IDMS_Registration_Source__c=registrationSource;  
              
            user.IDMS_User_Context__c =context;
            user.IsPortalSelfRegistered=true;
            user.IDMS_Email_opt_in__c=emailOpt;
            system.debug('user----------'+user);
        
        
        
        //After setting all the required fields calling the createUser method 
            IDMSResponseWrapper data;
                try{
                IDMSUserService obj = new IDMSUserService();
                data= obj.createUser(user);
                    if(data.IDMSUserRecord.Id!=null){
                    PageReference page = System.Page.UserRegistrationMessage;
                    page.setRedirect(true);
                    return page;
                    }
                    else{
                    
                    ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, data.message);
                    ApexPages.addMessage(msgntChkd );
                   
                    return null;
                    }
                }
        
        catch(Exception e){
        System.debug(e.getMessage());
          ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error in registering user, please try after some time.');
          ApexPages.addMessage(msgntChkd );
        return null;
        }     
        
        
        }

/**
    OD: This should not get pushed to UAT on 10June2016
    public PageReference updateUser()
    {
     User sc_user = [select Id, AccountId, IDMS_Registration_Source__c, IDMS_User_Context__c from User where Id=:UserInfo.getUserId()  limit 1];
     system.debug('Entered in update user to update this user:' +sc_user);
     
     //Profile objProfileid = [Select Id, Name from Profile where Name =: 'bFO System User' limit 1].id;

     
     
     try
     {
     //User sc_user = new User(Id=ScUser_Id );
     //Change Social User profile to standard Home Profile
     //String profilename = IDMS_Profile_Mapping__c.getInstance(sc_user.IDMS_User_Context__c).Profile_Name__c;                    
     //Id ProfileId =[SELECT Id FROM Profile WHERE Name=:profileName limit 1].id;
     
        //get currency,timezone,language,preflanguage,local on the basis of country
        String currencyUser;
        IdmsCountryMapping__c cntrmap;
        cntrmap=IdmsCountryMapping__c.getInstance(ucountry);
        if(cntrmap!=null){
         currencyUser= (String)cntrmap.get('DefaultCurrencyIsoCode__c');

         if(currencyUser==null)
         {
         currencyUser=Label.CLJUN16IDMS04;
         }
         }
         else
         {
         currencyUser=Label.CLJUN16IDMS04;
         }
         
         //set email opt in
         String emailOpt='false';
         if(checkedEmailOpt==true)
         {
            emailOpt='Y';
         }
        else{
        emailOpt='N';
        }
        
     sc_user.Email = email;
     sc_user.Country__c=ucountry;
     sc_user.firstName=firstName;
     sc_user.lastName=lastname;
     sc_user.MobilePhone=phomenum;
     sc_user.LanguageLocaleKey=Label.CLJUN16IDMS09;
     sc_user.LocaleSidKey=Label.CLJUN16IDMS11;
     sc_user.TimeZoneSidKey=Label.CLJUN16IDMS22;
     sc_user.IDMS_PreferredLanguage__c=Label.CLJUN16IDMS10;
     sc_user.DefaultCurrencyIsoCode=currencyUser;
     sc_user.IDMS_User_Context__c =context;
     sc_user.IDMS_Email_opt_in__c=emailOpt;
     //Salesforce limitation, we can't modify the user profile. 
     //sc_user.profileid = '00eR0000000QmTJ';
     update sc_user;
     updatePersonAccountContact(sc_user);
     
     } catch (Exception e)
     {
         System.debug('An unexpected error has occurred: ' + e.getMessage());
        return null;
     } 
        PageReference pr_identitycomm = new PageReference(Label.CLJUN16IDMS155); 
        //PageReference pr_fb = new PageReference('https://devmajbfo-secommunities.cs2.force.com/identity/UserRegistration'); 
        pr_identitycomm.setRedirect(true);
        return pr_identitycomm;
    
    }
    
    
    private void updatePersonAccountContact(User sc_user) {
        
        // Update Consumer Account associated to the Social User
                 
        
        Account prsnAccountContact = [Select ID, PersonContactId From Account Where Id = :sc_user.AccountId];
        
      
        prsnAccountContact .LastName = lastname;
        prsnAccountContact .firstName = firstName;
        prsnAccountContact .personEmail = email;
        prsnAccountContact .IDMS_Contact_UserExternalId__pc = email+Label.CLJUN16IDMS71;
        update prsnAccountContact; 
        

    }
   **/  


}