public class VFC_ManageMemberExternalInfo extends VFC_ExternalInfoDashboardController {
     public String ReturnToTab { get; set; }
     
     public VFC_ManageMemberExternalInfo(){
         
     }
     
    private PageReference GetFullUrl(String statusParam) {
        return this.GetFullUrl(statusParam, '');
    }

    private PageReference GetFullUrl(String statusParam, string pgName) {
        PageReference tPR = new PageReference('/apex/' + (String.isBlank(pgName) ? 'VFP_ManageMemberExternalInfo' : pgName) + '?tab=' + this.ReturnToTab + '&id=' + RecordId + (String.isNotBlank(statusParam) ? '&status=' + statusParam : ''));
        tPR.setRedirect(true);
        return tPR;
    }

    public PageReference doSave(){
        PageReference pr = null;
        pr = this.GetFullUrl('');
        return pr;
    }
}