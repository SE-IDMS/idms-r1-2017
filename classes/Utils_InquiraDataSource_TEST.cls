@isTest
private class Utils_InquiraDataSource_TEST
{
       static testMethod void InquiraDataSource_TestMethod()
       {
           Utils_DataSource InquiraDataSource = Utils_Factory.getDataSource('Inquira');
           Utils_InquiraDataSource Inquira = new Utils_InquiraDataSource();
           List<SelectOption> testColumns = InquiraDataSource.getColumns();
           List<SelectOption> Columns = Inquira.getColumns();
           List<String> DummyList = new List<String>();
           List<String> KeyWord = new List<String>();
           KeyWord.add('Test');
           DummyList.add('Test');
           Utils_DataSource.Result testResult = InquiraDataSource.search(DummyList,DummyList);
           Utils_DataSource.Result Result = Inquira.search(KeyWord,DummyList);
           WS05_Inquira.faqBean tstBean = new WS05_Inquira.faqBean();
           tstBean.id = 'Testid';
           tstBean.date_x = system.now();
           tstBean.excerpt = 'Test';
           tstBean.title = 'test';
           tstBean.url ='Test';
           list<WS05_Inquira.faqBean> lstBean = new list<WS05_Inquira.faqBean>();
           lstBean.add(tstBean);
           Inquira.Results = lstBean;
           Result = Inquira.search(KeyWord,DummyList);
       }
}