/*
* Test for vfc_updateEmailAddress
* */
@isTest
public class vfc_updateEmailAddressTest{
    //Test 1: Creating a user and updating email address and creating pagereference parameter  
    static testMethod void updateEmailAddressTestMethod() {
        Test.startTest();
        User userObj,userObj2;
        String rId = [select id from UserRole where  name = 'CEO' Limit 1].Id;
        String RecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'Consumer Account'].Id;
        User userObj1 = new User(alias = 'user', email='test312740' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='testuser' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='tej23414',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500',UserRoleId=rId);
        
        insert userObj1;
        system.runas(userObj1){
            
            Account personAcc = new Account(FirstName='Test1',LastName='Account', ZipCode__c='12345',RecordTypeId = RecordTypeId,OwnerId=userObj1.Id);
            insert personAcc;
            
            String contId = [select PersonContactId FROM account where ID =: personAcc.Id].PersonContactId;
            
            
            userObj = new User(alias = 'user', email='test312741' + '@cognizant.com', 
                               emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                               localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                               timezonesidkey='Europe/London', username='test1user' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='tej23415',IDMS_Registration_Source__c = 'Test',
                               IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                               IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432');
            
            insert userObj;
            userObj2 = new User(alias = 'user', email='test312741' + '@cognizant.com', 
                                emailencodingkey='UTF-8', lastname='Testlast1', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = '00e12000000Ruoi', BypassWF__c = true,BypassVR__c = true,
                                timezonesidkey='Europe/London', username='test1user1' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='tej23416',IDMS_Registration_Source__c = 'Test',
                                IDMS_User_Context__c = 'Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432',ContactId=contId);
            
            insert userObj2;
            
            userObj2.idmsnewemail__c = 'test21user@bridge-fo.com';
            userObj2.IDMS_User_Context__c='Home';  
            Update userObj2;
            String userecord = JSON.serialize(userObj);
            String personContactId = userObj2.ContactId;
        }
        ApexPages.currentPage().getParameters().put('Id',userObj.Id);
        vfc_updateEmailAddress updateEmailAddress=new vfc_updateEmailAddress();    
        updateEmailAddress.updateEmail();
        ApexPages.currentPage().getParameters().put('Id',userObj2.Id);
        vfc_updateEmailAddress updateEmailAddress1=new vfc_updateEmailAddress();    
        updateEmailAddress1.updateEmail();
        Test.stopTest();
    }
}