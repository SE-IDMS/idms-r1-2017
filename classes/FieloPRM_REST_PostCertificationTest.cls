@isTest

private class FieloPRM_REST_PostCertificationTest{

    static testMethod void unitTest(){
        
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
        FieloEE.MockUpFactory.setCustomProperties(false);

        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'certificationFirstName';
        member.FieloEE__FirstName__c = 'certificationLastName';
        member.FieloEE__Street__c = 'test';
   
        
        insert member;

        
        Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
        con1.SEContactID__c = 'test';
        update con1;
        
        FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
        badgeAux.name =  'testnane';                                      
        badgeAux.F_PRM_BadgeAPIName__c = 'test';
        badgeAux.F_PRM_Type__c = 'Training';
        insert badgeAux;
        
        FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
        badMemAux.FieloEE__Badge2__c = badgeAux.id;
        badMemAux.FieloEE__Member2__c = member.id ;
        badMemAux.F_PRM_FromDate__c = Date.Today(); 
        badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
        insert badMemAux;

          
        List<FieloPRM_REST_PostCertification.certificationWrapper> listWrapp = new List<FieloPRM_REST_PostCertification.certificationWrapper>();
        
        FieloPRM_REST_PostCertification.certificationWrapper wrapp = new FieloPRM_REST_PostCertification.certificationWrapper(); 
        wrapp.name = 'TestName';
        wrapp.uniqueCode = 'Test' ;
        wrapp.dateFrom = Date.Today();
        wrapp.dateTo = wrapp.dateFrom.addDays(1);
        
        listWrapp.add(wrapp);
        
        Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>> mapContactExtIdCertifications = new Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>>();
        mapContactExtIdCertifications.put('test' , listWrapp );
        
        FieloPRM_REST_PostCertification rest = new FieloPRM_REST_PostCertification();
        FieloPRM_REST_PostCertification.postCertification(mapContactExtIdCertifications );
        }
      
    }  
    
    static testMethod void unitTest2(){
        
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
        FieloEE.MockUpFactory.setCustomProperties(false);

        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'certificationFirstName';
        member.FieloEE__FirstName__c = 'certificationLastName'+ String.ValueOf(DateTime.now().getTime());
        member.FieloEE__Street__c = 'test';
   
        
        insert member;
        
        Contact con1 = new Contact();
        con1.FirstName = 'conFirst';
        con1.LastName = 'conLast';
        con1.SEContactID__c = 'test';
        con1.PRMUIMSId__c = 'test';
        insert con1;
        
        
        FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
        badgeAux.name =  'testnane';                                      
        badgeAux.F_PRM_BadgeAPIName__c = 'test';
        badgeAux.F_PRM_Type__c = 'Training';
        insert badgeAux;
        
        FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
        badMemAux.FieloEE__Badge2__c = badgeAux.id;
        badMemAux.FieloEE__Member2__c = member.id ;
        badMemAux.F_PRM_FromDate__c = Date.Today(); 
        badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
        insert badMemAux;

          
        List<FieloPRM_REST_PostCertification.certificationWrapper> listWrapp = new List<FieloPRM_REST_PostCertification.certificationWrapper>();
        
        FieloPRM_REST_PostCertification.certificationWrapper wrapp = new FieloPRM_REST_PostCertification.certificationWrapper(); 
        wrapp.name = 'TestName';
        wrapp.uniqueCode = 'Test' ;
        wrapp.dateFrom = Date.Today();
        wrapp.dateTo = wrapp.dateFrom.addDays(1);
        
        listWrapp.add(wrapp);
        
        Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>> mapContactExtIdCertifications = new Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>>();
        mapContactExtIdCertifications.put('test' , listWrapp );
        
        FieloPRM_REST_PostCertification rest = new FieloPRM_REST_PostCertification();
        FieloPRM_REST_PostCertification.postCertification(mapContactExtIdCertifications );
        }
      
    }  
    
}