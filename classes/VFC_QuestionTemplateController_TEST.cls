/*
    Author          : Ramesh Rajasekaran (SFDC) 
    Date Created    : 08/10/2012
    Description     : Test class for VFC_QuestionTemplateController 
*/
@isTest
private class VFC_QuestionTemplateController_TEST {

    static testMethod void testcontrollermethod() {
        QOTW__c qc = new QOTW__c(StartDate__c = system.date.today(),Enddate__c = system.date.today());
        insert qc;
        QuestionTemplate__c q = new QuestionTemplate__c(QuestionoftheWeek__c = qc.id,
        IncludeOther__c = true,
        Answers__c = 'test\ntest\n');
        insert q;
        VFC_QuestionTemplateController qtp = new VFC_QuestionTemplateController(new ApexPages.StandardController(q));
        qtp.getAnswerChoices();
        qtp.getChoices();
    }
}