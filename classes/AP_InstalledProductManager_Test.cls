@isTest //(SeeAllData=true)
public class AP_InstalledProductManager_Test{

    static testMethod void testInstalledProductManager() 
    {   
        User user = new User(alias = 'user2', email='user2' + '@Schneider-electric.com', 
        emailencodingkey='UTF-8', lastname='Testing2', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP30;AP_ContactBeforeUpdateHandler;AP_Contact_PartnerUserUpdate;AP_WorkOrderTechnicianNotification;SVMX21;AP_LocationManager',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12343455');
        insert user;
        
        system.runAs(user)
        { 
        
            Account objAccount = Utils_TestMethods.createAccount();
            objAccount.RecordTypeid = Label.CLOCT13ACC08;
            insert objAccount;
             test.startTest();
            SVMXC__Site__c site1 = new SVMXC__Site__c();
            site1.Name = 'Test Location';
            site1.SVMXC__Street__c  = 'Test Street';
            site1.SVMXC__Account__c = objAccount .id;
            //site1.PrimaryLocation__c = true;
            insert site1;
            
                 SVMXC__Site__c site2 = new SVMXC__Site__c(Name = 'Test Location',SVMXC__Account__c = null);
           
            //site1.SVMXC__Street__c  = 'Test Street';
           
           // site1.PrimaryLocation__c = true;
            //insert site2;
            
            Account objAccount2 = Utils_TestMethods.createAccount();
            objAccount2.RecordTypeid = Label.CLOCT13ACC08;
            insert objAccount2;

            
              
            
            Country__c country= Utils_TestMethods.createCountry(); 
            country.CountryCode__c= 'IN';   
           // insert country;   
            Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
            objContact.Country__c= country.id;
            //insert objContact;
                  
            Brand__c brand1_1 = new Brand__c();

            brand1_1.Name ='Brand 1-1';
            brand1_1.SDHBRANDID__c = 'Test_BrandSDHID';
            brand1_1.IsSchneiderBrand__c = true;
            insert brand1_1 ;
            
            DeviceType__c dt1_1 = new DeviceType__c();
            dt1_1.name = 'Device Type 1-1';
            dt1_1.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-1';
            insert dt1_1 ;

            Category__c c1_1 = new Category__c();
            c1_1.Name =  'Range 1-1';
            c1_1.CategoryType__c = 'RANGE';
            c1_1.SDHCategoryID__c = 'Test_RangeSDHID1-1';
               insert c1_1;
            
             Product2 prod= new Product2(name='test prod',ExtProductId__c=brand1_1.SDHBRANDID__c+'_'+dt1_1.SDHDEVICETYPEID__c+'_'+c1_1.SDHCategoryID__c);
            insert prod;
            
            List<SVMXC__Installed_Product__c> ipobjlist = new List<SVMXC__Installed_Product__c>();
      
            SVMXC__Installed_Product__c ip3 = new SVMXC__Installed_Product__c(Brand2__c = brand1_1.id,
            DeviceType2__c = dt1_1.id,Category__c = c1_1.id,SVMXC__Product__c=prod.id,SVMXC__Site__c = site1.id,SVMXC__Company__c=null,TECH_CreateFromWS__c = true);

            SVMXC__Installed_Product__c ip4 = new SVMXC__Installed_Product__c(Brand2__c = brand1_1.id,
            DeviceType2__c = dt1_1.id,Category__c = c1_1.id,SVMXC__Product__c=prod.id,SVMXC__Site__c = site1.id);
           // insert ip4;
             //update ip4;

            
            for(integer i=10;i<20;i++){
            
                SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
                ip1.SVMXC__Company__c = objAccount.id;
                ip1.Name = 'Test Intalled Product '+i;
                ip1.SVMXC__Status__c= 'new';
                ip1.GoldenAssetId__c = 'GoledenAssertId'+i;
                //ip1.BrandToCreate__c ='Test Brand';
                //ip1.DeviceTypeToCreate__c
                ip1.SVMXC__Site__c = null;
                ip1.SVMXC__Product__c=null;
                ip1.Brand2__c = brand1_1.id;
                ip1.DeviceType2__c = dt1_1.id;
                ip1.Category__c = c1_1.id;
                ip1.TECH_CreateFromWS__c = true;
                ipobjlist.add(ip1);
            
            
            }
            
            ipobjlist.add(ip3);
            ipobjlist.add(ip4);
            insert ipobjlist ;
            update ipobjlist;
            
            objAccount.id=ipobjlist[0].SVMXC__Company__c;
            update objAccount;
            
           
            AP_InstalledProductManager.checkAssetGoldenId(ipobjlist);

           // List<Sobject> sobjlist =new  List<Sobject>();
            //sobjlist= (List<Sobject>)ipobjlist ;
           
            AP_InstalledProductManager.ProcessAssets(ipobjlist );
         
           // List<SVMXC__Site__c> stlist =[select id from SVMXC__Site__c where PrimaryLocation__c = true and SVMXC__Account__c =:objAccount.id];
           // if(stlist!= null && stlist.size()>0)
           // delete stlist ;
           AP_InstalledProductManager.TestCoverageMehod();
           test.stopTest();
        }
    }
    
}