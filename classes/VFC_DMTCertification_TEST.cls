@isTest
private class VFC_DMTCertification_TEST {    

static testMethod void testDMTCert()    {

test.startTest();

DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');        
insert testDMTA1; 

PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();            
testProj.ParentFamily__c = 'Business';            
testProj.Parent__c = 'Buildings';            
testProj.BusGeographicZoneIPO__c = 'Global';            
testProj.BusGeographicalIPOOrganization__c = 'CIS';            
testProj.GeographicZoneSE__c = 'Global';            
testProj.Tiering__c = 'Tier 1';            
testProj.ProjectRequestDate__c = system.today();            
testProj.Description__c = 'Test';            
testProj.GlobalProcess__c = 'Customer Care';            
testProj.ProjectClassification__c = 'Infrastructure';            
testProj.BusinessTechnicalDomain__c = 'Supply Chain';           
insert testProj;

ApexPages.StandardController sc1 = new ApexPages.StandardController(testProj);
VFC_DMTCertification dmtcert = new  VFC_DMTCertification(sc1);
dmtcert.RedirectLink();

}

}