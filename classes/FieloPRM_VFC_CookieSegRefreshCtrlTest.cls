@IsTest
public class FieloPRM_VFC_CookieSegRefreshCtrlTest {
    public static testmethod void unittest1(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
    
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
        
        RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
        
        FieloEE__RedemptionRule__c segment1 = new FieloEE__RedemptionRule__c(           
            Name = 'Test Segment1',
            FieloEE__isActive__c = true,
            RecordTypeId = rt.Id
        );   
        insert segment1;
        
        FieloEE__MemberSegment__c memberSegment = new FieloEE__MemberSegment__c(
            FieloEE__Member2__c = mem.Id,
            FieloEE__Segment2__c = segment1.Id           
        );
        insert memberSegment;
        
        Test.startTest();
        
        FieloPRM_VFC_CookieSegmentRefreshCtrl cont = new FieloPRM_VFC_CookieSegmentRefreshCtrl();
        FieloPRM_VFC_CookieSegmentRefreshCtrl.getSegments(mem.Id);
        
        Test.stopTest();
    }
}