/*    Author          : Accenture Team    
      Date Created    : 24/10/2011     
      Description     : Controller extensions for VFP63_AddUpdateProductForPAC. */
                        
public with sharing  class VFC63_AddUpdateProductForPAC extends VFC_ControllerBase
{
    //Iniatialize variables
    private PAMandCompetition__c pam = new PAMandCompetition__c();
    private ID accId;
    private DataTemplate__c inputPL = new DataTemplate__c();          
    private List<PAMandCompetition__c> prodLines = new List<PAMandCompetition__c>();    
    public List<String> filters{get; set;}    
    public Utils_DataSource dataSource;
    public List<SelectOption> columns{get;set;}
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public String interfaceType{get;set;}
    public String searchKeyword{get; set;}
    public List<String> keywords;
    public List<String> picklistsValues{get;set;}
    public Boolean DisplayResults{get;set;}
    private OPP_Product__c product = new OPP_Product__c();
    public Utils_DataSource.Result searchResult ;
 
    public VCC05_DisplaySearchFields searchController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC05_DisplaySearchFields displaySearchFields;
                displaySearchFields = (VCC05_DisplaySearchFields)getcomponentControllerMap().get('searchComponent');
                system.debug('--------displaySearchFields-------'+displaySearchFields );
                if(displaySearchFields!= null)
                return displaySearchFields;
            } 
            return new VCC05_DisplaySearchFields();
        }
    } 
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
     //Constructor
     public VFC63_AddUpdateProductForPAC(ApexPages.StandardController controller) 
     {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC63_AddUpdateProductForPAC******');            
        keywords = new List<String>();
        filters = new List<String>();
        columns = new List<SelectOption>();
        interfaceType = 'GMR';        
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
        dataSource = Utils_Factory.getDataSource(interfaceType );
        Columns = new List<SelectOption>();
        Columns.add(new SelectOption('Field8__c',Label.CL00374)); // First Column
        Columns.add(new SelectOption('Field9__c',Label.CL00375));
        Columns.add(new SelectOption('Field4__c',Label.CL00354));
        Columns.add(new SelectOption('Field3__c',Label.CL00353));
        Columns.add(new SelectOption('Field2__c',Label.CL00352));
        Columns.add(new SelectOption('Field1__c',Label.CL00351)); // Last Column
        fetchedRecords = new List<DataTemplate__c>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC63_AddUpdateProductForPAC******');
    }
    
    public PageReference  search() 
    {     
    /* This method will be called on click of "Search" Button.This method calls the factory methods
     * and fetches the search results
     */
       System.Debug('****** Searching Begins  for VFC63_AddUpdateProductForPAC******');
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();

        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchController.searchText!=null)
            searchKeyword = searchController.searchText.trim();
        if(searchKeyword != null)
            keywords = searchKeyword.split('\\s');
            if(searchController.level1!=null)            
        filters.add(searchController.level1);
            if(searchController.level2!=null)
        filters.add(searchController.level2);
            if(searchController.level2!=null && searchController.level3!=null)
        filters.add(searchController.level3);
            if(searchController.level2!=null && searchController.level3!=null && searchController.level4!=null)
        filters.add(searchController.level4); 
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
            if(((!filters.ISEmpty()) && filters[0] != Label.CL00355) || ((!keywords.ISEmpty())&& keywords[0].length()>0))
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the webservice call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                       
                    DisplayResults = true;
                     if(searchResult.NumberOfRecord>=Utils_GMRDataSource.GMR_RECORD_PER_PAGE && fetchedRecords.size()>1)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling GMR WS '+ exc.getMessage() );
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));
                }                
            }
            else if(filters[0] == Label.CL00355 && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CL00347));

            }             
        
        System.Debug('****** Searching Ends  for VFC63_AddUpdateProductForPAC******');        
        return null;          
    }
    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC63_AddUpdateProductForPAC******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchKeyword = null;
        keywords = new List<String>();
        filters = new List<String>();        
        searchController.init();
        DisplayResults = false;
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC63_AddUpdateProductForPAC******');     
        return null;
    }
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly inserts/updates
     * the product line.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** Insertion of PAM & Competitors Begins******');
        pagereference pg;
        Id recordId;
        VFC63_AddUpdateProductForPAC thisController = (VFC63_AddUpdateProductForPAC)controllerBase;
        //Accountid is fetched from the URL and passed on the next page as URL parameter
            if(system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_10').Values__c)!=null)
            {
                accId = system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_10').Values__c);
                inputPL = (DataTemplate__c)obj;
            }                   
           
            //Inserts Pam and Competitor
                if(inputPL!=null)
                {
                    pam = new PAMandCompetition__c();   
                    if(inputPL.field1__c!=null)
                    pam.ProductBU__c=inputPL.field1__c;
                    if(inputPL.field2__c!=null)
                    pam.ProductLine__c=inputPL.field2__c;
                    if(inputPL.field3__c!=null)
                    pam.ProductFamily__c=inputPL.field3__c;                        
                    if(inputPL.field4__c!=null)
                    pam.Family__c=inputPL.field4__c;
                    /*UPDATED FOR CR - Accenture - 01/12/2011*/
                    if(inputPL.field8__c!=null)
                    pam.CommercialReference__c = inputPL.field8__c;
                    //End Of Update 
                    if(inputPL.field9__c!=null)
                    pam.ProductDescription__c=inputPL.field9__c;   
                    if(accId!=null)
                    pam.Account__c=accId;
                    if(inputPL.field10__c!=null)
                    {                    
                        string query1 = CS005_Queries__c.getvalues('CS005_8').StaticQuery__c +' '+ 'WHERE TECH_PM0CodeInGMR__c =\''+inputPL.Field10__c+'\''+'limit 1'; //Querying ID from Product.
                        product = database.query(query1);
                    }
                    If(product!=null)
                    {
                         pam.Product__c = product.Id;   
                    }
                    if(pam!=null)
                    {                    
                        Database.saveResult sv = Database.insert(pam,false);
                        if(!sv.issuccess())
                        {
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,label.CL00414));
                        }
                        
                    }
            }
            if(pam.id!=null)                   
            pg = new pagereference('/'+pam.id+'/e?'+Label.CL00330+'=%2F'+pam.id);
        System.Debug('****** Insertion of PAM and Competitors ends Ends******');
        return pg;
    }    
}