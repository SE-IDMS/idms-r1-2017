public class VFC_ProgramLevelBrand {
    public list<ProgramLevelBrand__c> lstLevelBrands {get;set;}
        //public list<String> lstLevelBrands {set;}
        String tmp;
        public VFC_ProgramLevelBrand(ApexPages.StandardController controller) {
            lstLevelBrands  = [Select Brand__r.Name
                           from  ProgramLevelBrand__c where ProgramLevel__c =:controller.getId()
                          order by ProgramLevelBrand__c.Name limit 1000];
        }    
}