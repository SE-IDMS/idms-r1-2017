/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class provides methods of log creation based on different use cases
**/ 
global with sharing class IDMSCaptureError{
    
    //this method inserts logs in case of user creation failure in the use case of data synchronization
    public void IDMScreateLog(String API_Name,String Classname,String MethodName,String Username,String Exception_StackTrace,
                              String Registration_Source,DateTime Exception_Date,String User_Context,String FederationId,
                              boolean Data_Sync,String Contacted_email,boolean Email_to_be_sent,String Functional_Step){
                                  
        IDMS_ErrorLog__c errorLog       = new IDMS_ErrorLog__c();
        errorLog.API_Name__c            = API_Name;
        errorLog.Classname__c           = Classname;
        errorLog.Contacted_email__c     = Contacted_email;
        errorLog.Data_Sync__c           = Data_Sync;
        errorLog.Email_to_be_sent__c    = Email_to_be_sent;
        errorLog.ExceptionDate__c       = Exception_Date;
        errorLog.Exception_StackTrace__c= Exception_StackTrace;
        errorLog.FederationId__c        = FederationId;
        errorLog.MethodName__c          = MethodName;
        errorLog.Registration_Source__c = Registration_Source;
        errorLog.Username__c            = username;
        errorLog.UserContext__c         = User_Context;
        errorLog.Functional_Step__c     = Functional_Step;
        insert errorLog;
    }
    
    //this method returns errorLog sObject record in case of user creation failure in the use case of data synchronization
    public IDMS_ErrorLog__c CreateErrorLogRecord(String API_Name,String Classname,String MethodName,String Username,String Exception_StackTrace,
                                                 String Registration_Source,DateTime Exception_Date,String User_Context,String FederationId,
                                                 boolean Data_Sync,String Contacted_email,boolean Email_to_be_sent,String Functional_Step){
        
        IDMS_ErrorLog__c  errorLogRecord        = new IDMS_ErrorLog__c();
        errorLogRecord.API_Name__c              = API_Name;
        errorLogRecord.Classname__c             = Classname;
        errorLogRecord.Contacted_email__c       = Contacted_email;
        errorLogRecord.Data_Sync__c             = Data_Sync;
        errorLogRecord.Email_to_be_sent__c      = Email_to_be_sent;
        errorLogRecord.ExceptionDate__c         = Exception_Date;
        errorLogRecord.Exception_StackTrace__c  = Exception_StackTrace;
        errorLogRecord.FederationId__c          =  FederationId;
        errorLogRecord.MethodName__c            = MethodName;
        errorLogRecord.Registration_Source__c   = Registration_Source;
        errorLogRecord.Username__c              = username;
        errorLogRecord.UserContext__c           = User_Context;    
        errorLogRecord.Functional_Step__c       = Functional_Step;
        
        return errorLogRecord;
    }
    
    //this method creates logs in case of user creation failure in the use case of non data synchronization
    public void IDMScreateLog(String API_Name,String Classname,String MethodName,String Username,String Exception_StackTrace,
                              String Registration_Source,DateTime Exception_Date,String User_Context,String FederationId,
                              String Contacted_email,boolean Email_to_be_sent,String Functional_Step){
        
        IDMS_ErrorLog__c errorLog           = new IDMS_ErrorLog__c();
        errorLog.API_Name__c                = API_Name;
        errorLog.Classname__c               = Classname;
        errorLog.Contacted_email__c         = Contacted_email;
        errorLog.Email_to_be_sent__c        = Email_to_be_sent;
        errorLog.ExceptionDate__c           = Exception_Date;
        errorLog.Exception_StackTrace__c    = Exception_StackTrace;
        errorLog.FederationId__c            = FederationId;
        errorLog.MethodName__c              = MethodName;
        errorLog.Registration_Source__c     = Registration_Source;
        errorLog.Username__c                = username;
        errorLog.UserContext__c             = User_Context;
        errorLog.Functional_Step__c         = Functional_Step;
        insert errorLog;
    }
  
    //this method returns errorLog sObject record in case of user creation failure in the use case of non data synchronization
    public IDMS_ErrorLog__c CreateErrorLogRecord(String API_Name,String Classname,String MethodName,String Username,String Exception_StackTrace,
                                                 String Registration_Source,DateTime Exception_Date,String User_Context,String FederationId,
                                                 String Contacted_email,boolean Email_to_be_sent,String Functional_Step){
        IDMS_ErrorLog__c  errorLogRecord        = new IDMS_ErrorLog__c();
        errorLogRecord.API_Name__c              = API_Name;
        errorLogRecord.Classname__c             = Classname;
        errorLogRecord.Contacted_email__c       = Contacted_email;
        errorLogRecord.Email_to_be_sent__c      = Email_to_be_sent;
        errorLogRecord.ExceptionDate__c         = Exception_Date;
        errorLogRecord.Exception_StackTrace__c  = Exception_StackTrace;
        errorLogRecord.FederationId__c          = FederationId;
        errorLogRecord.MethodName__c            = MethodName;
        errorLogRecord.Registration_Source__c   = Registration_Source;
        errorLogRecord.Username__c              = username;
        errorLogRecord.UserContext__c           = User_Context;    
        errorLogRecord.Functional_Step__c       = Functional_Step;
        
        return errorLogRecord;
    }
    
    
}