//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : Controller class of VFP idmsSocialLogin 
//
//*******************************************************************************************

Public with sharing class IdmsSocialLoginController{
    Public String appid= System.currentPagereference().getParameters().get('app');
    public string registrationSource;
    public string context;
    public string socialRegUrl;
    public String oAuthUrl1;
    public String oAuthUrl2;
    public String oAuthUrl3;
    public String appName;
    
    //VFP Constructor
    public IdmsSocialLoginController(){
        //Get app details from Custom setting
        IDMSApplicationMapping__c appMap;
        appMap = IDMSApplicationMapping__c.getInstance(appid);
        if(appMap != null){
            context             = (String)appMap.get('context__c');
            registrationSource  = (String)appMap.get('AppName__c');
            socialRegUrl        = (String)appMap.get('UrlAppSocialRegistrationPage__c');
            appName             = (String)appMap.get('Name');
            appId               = (String)appMap.get('AppId__c');
        }   
        
        IdmsSocialUser__c socialUsrApp;
        socialUsrApp = IdmsSocialUser__c.getInstance(Label.CLNOV16IDMS064);
        if(socialUsrApp != null){
            oAuthUrl1 = (String)socialUsrApp.get('oAuthUrl__c');
            oAuthUrl2 = (String)socialUsrApp.get('oAuthUrl2__c');
            oAuthUrl3 = (String)socialUsrApp.get('oAuthUrl3__c');
        }
    }
    
    //redirect button implementation to social media site
    public PageReference redirect_fb(){ 
        String oAuthUrl = oAuthUrl1 + oAuthUrl2 + oAuthUrl3;
        // new working code starts:
        User Sc_User = [select Id, IDMS_Registration_Source__c, Country, Country__c from User where Id=:UserInfo.getUserId() limit 1];
        if(checkCountry(Sc_User))
        {           
            if(socialRegUrl == null)   
            {
                if(context == Label.CLQ316IDMS301 || context == Label.CLQ316IDMS302 || context == Label.CLQ316IDMS304 || context == Label.CLQ316IDMS305){
                    system.debug('User came without country with at work context.');
                    PageReference redirectToIdmsRegistration = new PageReference(Label.CLQ316IDMS303+appName ); 
                    redirectToIdmsRegistration.setRedirect(true);
                    return redirectToIdmsRegistration;
                }else {
                    system.debug('User came without country with at home context.');
                    PageReference redirectToIdmsRegistration = new PageReference(Label.CLQ316IDMS210+appName );
                    redirectToIdmsRegistration.setRedirect(true);
                    return redirectToIdmsRegistration;
                }
            }else {
                system.debug('social registration value is not empty in custom settings:'+socialRegUrl);
                PageReference redirectToAppRegistration = new PageReference(socialRegUrl); 
                redirectToAppRegistration.setRedirect(true);
                return redirectToAppRegistration;
            }
        }else {
            if(oAuthUrl1 == null && oAuthUrl2 == null && oAuthUrl3 == null){
                system.debug('****Inside saml****');
                pagereference pr_saml = new Pagereference(Label.CLQ316IDMS226+appId);
                pr_saml.setRedirect(true);
                return pr_saml;
            }else {
                system.debug('****inside oAuth****');
                pagereference pr_oauth = new Pagereference(Label.CLNOV16IDMS065+oAuthUrl); 
                pr_oauth.setRedirect(true);
                return pr_oauth;
            }
        }
    }
    
    //Method that check if social media returns a user country or not.
    //If no country value returned by social media, then method returns FALSE. Else, returns TRUE
    public boolean checkCountry(User Sc_User){    
        system.debug('Currently logged in User:'+Sc_User);
        system.debug('Currently logged in User country and registration source:'+Sc_User.Country+Sc_User.Country__c+Sc_User.IDMS_Registration_Source__c);
        if((Sc_User.IDMS_Registration_Source__c == Label.CLJUN16IDMS154 || Sc_User.IDMS_Registration_Source__c == Label.CLQ316IDMS018 ||
            Sc_User.IDMS_Registration_Source__c == Label.CLQ316IDMS020 || Sc_User.IDMS_Registration_Source__c == Label.CLQ316IDMS022) &&
           (String.isBlank(Sc_User.Country) || String.isBlank(Sc_User.Country__c)))
        {
            return TRUE;
        } 
        return false;
    }
}