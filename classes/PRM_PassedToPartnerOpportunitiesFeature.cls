public class PRM_PassedToPartnerOpportunitiesFeature extends PRM_FeatureCustomPermissionSet {

    public PRM_PassedToPartnerOpportunitiesFeature(){
      super();
    }

    
    public override String getPermissionSetName(){
        return 'PRMPassedToPartnerOpportunitiesCommunity';
    }
}