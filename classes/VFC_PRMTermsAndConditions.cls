/**
* @author: Anil Sistla (GD)
* @date: 15/10/2016
* @description: This controller class loads TnC to be displayed to the user
*/

global class VFC_PRMTermsAndConditions {

    public transient String componentId { get; set; }
    public transient FieloEE__Member__c currentMember { get; set; }
    private transient FieloEE__Component__c comp;
    public Boolean ServeMinified {
        get {
            CS_PRM_ApexJobSettings__c tSetting = CS_PRM_ApexJobSettings__c.getInstance ('SERVE_MINIFIED');
            if (tSetting == null) return false;
            else return tSetting.InitialSync__c;
        }
        private set;
    }

    String language = 'EN';

    public VFC_PRMTermsAndConditions () {
        System.debug('*** Current Member: ' + this.currentMember);

        if (this.currentMember != null) {
            String tLang = this.currentMember.FieloEE__User__r.LanguageLocaleKey;
            if (String.isNotBlank(tLang) && tLang.indexOf('_') > 0)
                language = tLang.substringBefore('_');
            else if (String.isNotBlank(tLang))
                language = tLang;
        }
    }

    public String IsTnCAgreed {
        get {
            String tnc = '';
            if (this.currentMember != null && this.currentMember.FieloEE__User__r.ContactId != null) {
                tnc = String.isBlank(this.currentMember.FieloEE__User__r.Contact.PRMTnC__c) ? 'N' : this.currentMember.FieloEE__User__r.Contact.PRMTnC__c;
            }
            return tnc;
        }
        private set;
    }

    public String getTnCText () {
        String tText = '';
        comp = this.getComponent();

        if (comp.get('Text_' + language + '__c') != null)
            tText = (String)comp.get('Text_' + language + '__c');
        return tText;
    }

    private FieloEE__Component__c getComponent () {

        String qry = 'SELECT FieloEE__CSSClasses__c, FieloEE__BannerPlacement__c, FieloEE__Category__c, ' +
                        'FieloEE__Tag__c, FieloEE__FieldSet__c, FieloEE__Modal__c, ' +
                        'FieloEE__OptionalName__c' + ', OptionalName_' + language + '__c, ' +
                        'FieloEE__Text__c, Text_' + language + '__c ' +
                        'FROM FieloEE__Component__c WHERE Id = \'' + this.componentId + '\'';

        System.debug('*** Query: ' + qry);
        FieloEE__Component__c tComp = Database.query(qry);
        return tComp;
    }

    @RemoteAction
    global static void UpdateTnC (String tncStatus) {
        System.debug('*** Current UserId:' + UserInfo.getUserId());

        User thisUser = [SELECT Id, ContactId, Contact.PRMRegistrationActivationDate__c, Contact.PRMPrimaryContact__c,
                            Contact.AccountId, Contact.Account.PRMRegistrationActivationDate__c
                        FROM User WHERE Id = :UserInfo.getUserId()];
        Account tA;
        if (thisUser != null && thisUser.ContactId != null) {
            Contact tC = new Contact (Id = thisUser.ContactId, PRMTnc__c = ('AGREE'.equalsIgnoreCase(tncStatus) ? 'A' : 'D'));
            if (thisUser.Contact.PRMRegistrationActivationDate__c == null)
            tC.PRMRegistrationActivationDate__c = System.Now();

            if (thisUser.Contact.PRMPrimaryContact__c == true && thisUser.Contact.Account.PRMRegistrationActivationDate__c == null)
                tA = new Account (Id = thisUser.Contact.AccountId, PRMRegistrationActivationDate__c = System.Now());                    
            if(!Test.isRunningTest())
                UPDATE tC;

            if (tA != null) UPDATE tA;
        }
    }
}