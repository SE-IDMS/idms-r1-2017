/**
* @author Matias Trionfetti
* @date 6/07/2013
* @description Controller for the css component
*/
global with sharing class Fielo_CSSController{

    /*
    public MemberPortal__c memberPortal {get{
        if(memberPortal == null) return MemberPortal__c.getOrgDefaults();
        else return memberPortal;
    } set;}*/

    public FieloEE__Program__c program {get{
        FieloEE__Program__c p = FieloEE.ProgramUtil.getProgramByDomain();
        if ( p == null ) p = FieloEE.ProgramUtil.getDefaultProgram();
            return p;
        }
    }
    public FieloEE__ProgramLayout__c layout {get{
        return FieloEE.ProgramUtil.getFrontEndLayout();
    }set;}
    
    //public String documentCSSString {get;set;}       
    
    /*
    public Fielo_CSSController(){
        //get advanced css
        try{
            Document documentCSS = [SELECT Body FROM Document WHERE Id =: MemberPortal__c.getOrgDefaults().CSSFile__c];
            documentCSSString = documentCSS.Body.toString();
        }catch(Exception ex){
            documentCSSString = '';
        }
    }
    */
    
    /**
    * @author Alejandro Spinelli
    * @date 12/11/2012
    * @description Method to get the font color 05 bis
    * @return String
    */    
    public String getFontColor05bis(){
        if(layout.FieloEE__ColorFont05__c == 'FFFFFF')
            return '000000'; 
        else
            return 'FFFFFF';
    }
    
    /**
    * @author Alejandro Spinelli
    * @date 12/11/2012
    * @description Method to get settings of the org
    * @return FieloEE__Program__c
    */
    public FieloEE__Program__c getSettings(){    
        return FieloEE.ProgramUtil.getDefaultProgram();    
    }
    
}