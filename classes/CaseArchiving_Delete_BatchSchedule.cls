global class CaseArchiving_Delete_BatchSchedule implements Schedulable {
    global void execute(SchedulableContext SC) {
        Integer size = Integer.Valueof(System.Label.CLJUN16CCC10);
        CaseArchiving_Delete_Batch  testh= new CaseArchiving_Delete_Batch();
        Database.executeBatch(testh,size);
    }
}