/*Date Created    : 07/05/2015
  Description     : Test class for the apex class AP_CustomPermissionsReader
*/
@IsTest
private class AP_CustomPermissionsReaderTest {
 
    /**
     * This will need to be modified to reflect a Custom Permission in the org, 
     *   since DML in test code cannot create them :-(
     **/
    private static final String TEST_CUSTOM_PERMISSION = 'AllowCloseLeads';
 
    @IsTest
    private static void testCustomPermissionAssigned() {
 
        // Create PermissionSet with Custom Permission and asisgn to test user
        PermissionSet ps = new PermissionSet();
        ps.Name = 'CustomPermissionsReaderTest';
        ps.Label = 'CustomPermissionsReaderTest';
        insert ps;
        SetupEntityAccess sea = new SetupEntityAccess();
        sea.ParentId = ps.Id;
        sea.SetupEntityId = [select Id from CustomPermission where DeveloperName = :TEST_CUSTOM_PERMISSION][0].Id;
        insert sea;
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = UserInfo.getUserId();
        psa.PermissionSetId = ps.Id;
        insert psa;
 
        // Create reader
        //   Note: SObjectType for managed package developers should be a Custom Object from that package
        AP_CustomPermissionsReader cpr = new AP_CustomPermissionsReader(Account.SObjectType);
 
        // Assert the CustomPermissionsReader confirms custom permission assigned
        System.assertEquals(true, cpr.hasPermission(TEST_CUSTOM_PERMISSION));
    }
 
    @IsTest
    private static void testCustomPermissionNotAssigned() {
 
        // Assert the CustomPermissionsReader confirms custom permission not assigned
        System.assertEquals(false, new AP_CustomPermissionsReader(Account.SObjectType).hasPermission(TEST_CUSTOM_PERMISSION));
    }   
 
 
    @IsTest
    private static void testCustomPermissionNotValid() {
 
        try {
            // Assert the CustomPermissionsReader throws an exception for an invalid custom permission
            System.assertEquals(false, new AP_CustomPermissionsReader(Account.SObjectType).hasPermission('NotValid'));
            System.assert(false, 'Expected an exception');
        } catch (Exception e) {
            System.assertEquals('Custom Permission NotValid is not valid.', e.getMessage());
        }
    }   
 
    @IsTest
    private static void testCustomPermisionDefaultConstructor() {
        // Assert the CustomPermissionsReader confirms custom permission not assigned
        System.assertEquals(false, new AP_CustomPermissionsReader().hasPermission(TEST_CUSTOM_PERMISSION));        
    }
}