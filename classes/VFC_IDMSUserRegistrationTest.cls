/**
@Author: Prashanth GS
Description: This covers VFC_IDMSUserRegistration class
Release: Sept 2016
**/
// test class for user registeration controller
@isTest(SeeAllData=true)
class VFC_IDMSUserRegistrationTest{
    
    // test method with saveral test cases included for user registeration
    static testmethod void testIDMSUserRegistration(){
        PageReference mypage= Page.UserRegistration;
        Test.setCurrentPage(mypage);
        
        
        User userObj1 = new User(alias = 'user', email='user12341' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='Pras-Kuld1234',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = '@Work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',MobilePhone='1234567890',Phone='null');
        try{
            insert userObj1;
            
            System.assert(userObj1!=null);
        }
        catch(Exception e){}
        IDMS_Send_Invitation__c objSendInvitition=new IDMS_Send_Invitation__c(City__c='Bangalore',Company_Country__c='US',Company_State__c='',
                                                                              Country__c='US',CurrencyIsoCode='EUR',Email__c='test984567_16July@yopmail.com',
                                                                              FirstName__c='Test984567',IDMSClassLevel1__c='EU',IDMSClassLevel2__c='SP4',IDMSMarketSegment__c='ID5',
                                                                              IDMSMarketSubSegment__c='B43',IDMSSalutation__c='Z003',IDMSSignatureGenerationDate__c=System.now().date().adddays(2),
                                                                              IDMSToken__c='GxlAzLTvbHWP0rPI5AfH',IDMS_Email_opt_in__c='U',IDMS_PreferredLanguage__c='EN',IDMS_Registration_Source__c='uims',
                                                                              IDMS_User_Context__c='work',LastName__c='16July16_1',Mobile_Phone__c='987645678',State__c='');
        try{ insert objSendInvitition;
            
            System.assert(objSendInvitition!=null);
           }
        catch(Exception e){}
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        
        
        ApexPages.currentPage().getParameters().put(Label.CLJUN16IDMS27,'True');
        ApexPages.currentPage().getParameters().put('app','IDMS');
        ApexPages.currentPage().getParameters().put('InvitationId',objSendInvitition.Id);
        
        VFC_IDMSUserRegistration idmsRegistration=new VFC_IDMSUserRegistration();
        
        idmsRegistration.mobCntryCode='+21';
        idmsRegistration.Phone='1234567890';
        idmsRegistration.workCntryCode='+11';
        
        
        test.Starttest();
        System.Runas(userObj1){
            
            
            idmsRegistration.clone();
            idmsRegistration.getCompanyPicklist();
            
            idmsRegistration.nextToCompany();
            idmsRegistration.reset();
            idmsRegistration.submit();
            idmsRegistration.submitHome();
            idmsRegistration.verify();
            ApexPages.currentPage().getParameters().put(Label.CLJUN16IDMS28,'True');
            idmsRegistration.verify();
            idmsRegistration.IDMS_Email_opt_in='true';
            idmsRegistration.Company_State='';
            idmsRegistration.Company_Country='US';
            idmsRegistration.ClassLevel1='';
            idmsRegistration.ClassLevel2='';
            idmsRegistration.MarketSegment=null;
            idmsRegistration.MarketSubSegmen=null;
            idmsRegistration.CompanyMarketServed=null;
            idmsRegistration.CompanyNbrEmployees=null;
            idmsRegistration.CompanyHeadquarters=null;
            idmsRegistration.AnnualRevenue=null;
            idmsRegistration.TaxIdentificationNumber=null;
            idmsRegistration.Fax=null;
            idmsRegistration.smsIdentity=null;
            idmsRegistration.userId='';
            idmsRegistration.showHome=true;
            idmsRegistration.iscmpny=true;
            idmsRegistration.isconfirm=true;
            idmsRegistration.isHomeConfirm=true;
            
            
            
            idmsRegistration.FirstName='Test9876785';
            idmsRegistration.LastName='Test98767851';
            idmsRegistration.email='Test9876785';
            idmsRegistration.nextToCompany();
            idmsRegistration.email='user12341@cognizant.com';
            idmsRegistration.nextToCompany();
            idmsRegistration.email='user12341@gmail.com';
            idmsRegistration.MobilePhone='user12341@gmail.com';
            idmsRegistration.nextToCompany();
            idmsRegistration.MobilePhone='9999976545466';
            idmsRegistration.isPwdIncluded=true;
            idmsRegistration.password='Welcome@123';
            idmsRegistration.nextToCompany();
            idmsRegistration.cpassword='Welcome@123';
            idmsRegistration.nextToCompany();
            idmsRegistration.CompanyName='Cognizant';
            idmsRegistration.submit();
            idmsRegistration.Company_City='Bangalore';
            idmsRegistration.submit();
            idmsRegistration.checkedEmailOpt=true;
            idmsRegistration.submit();
            idmsRegistration.checkedEmailOpt=false;
            idmsRegistration.submit();
            idmsRegistration.isPwdIncluded=false;
            idmsRegistration.submit();
            idmsRegistration.isPwdIncluded=true;
            idmsRegistration.password='Welcome@123';
            idmsRegistration.submit();
            idmsRegistration.FirstName='Test9876784';
            idmsRegistration.LastName='Test98767852';
            idmsRegistration.email='Test9876785';
            idmsRegistration.submitHome();
            idmsRegistration.email='user12341@cognizant.com';
            idmsRegistration.submitHome();
            idmsRegistration.email='user12341@gmail.com';
            idmsRegistration.MobilePhone='user12341@gmail.com';
            idmsRegistration.submitHome();
            idmsRegistration.MobilePhone='9999976545466';
            idmsRegistration.isPwdIncluded=true;
            idmsRegistration.password='Welcome@123';
            idmsRegistration.submitHome();
            idmsRegistration.cpassword='Welcome@123';
            idmsRegistration.submitHome();
            idmsRegistration.isPwdIncluded=false;
            idmsRegistration.submitHome();            
            String strPublicKey=idmsRegistration.publicKey;
            idmsRegistration.showCaptcha=false;
            idmsRegistration.submitHome(); 
            idmsRegistration.mobCntryCode='';
            idmsRegistration.submitHome(); 
            idmsRegistration.nextToCompany();
            
            
            idmsRegistration.MobilePhone='1234567890';
            idmsRegistration.email='';
            idmsRegistration.submitHome(); 
            idmsRegistration.nextToCompany();
            
            
            
            idmsRegistration.password='Welcome02';
            idmsRegistration.cpassword='Welcome02';
            idmsRegistration.submitHome(); 
            idmsRegistration.nextToCompany();
            idmsRegistration.submit();
            
            idmsRegistration.isPwdIncluded=false;
            idmsRegistration.checked=false;
            idmsRegistration.submitHome(); 
            idmsRegistration.submit();
            
            idmsRegistration.isPwdIncluded=false;
            idmsRegistration.checked=true;
            idmsRegistration.submitHome(); 
            idmsRegistration.nextToCompany();
            idmsRegistration.submit();
            
            idmsRegistration.checkedEmailOpt=true;
            idmsRegistration.submitHome(); 
            idmsRegistration.nextToCompany();
            idmsRegistration.submit();
            
            idmsRegistration.isPwdIncluded=true;
            idmsRegistration.checkedEmailOpt=false;
            idmsRegistration.submitHome(); 
            idmsRegistration.nextToCompany();
            idmsRegistration.submit();
            
            idmsRegistration.showCaptcha=false;  
            idmsRegistration.submit();
            idmsRegistration.showCaptcha=true;  
            idmsRegistration.submitHome(); 
            idmsRegistration.submit();
        }
        test.Stoptest();
    }
}