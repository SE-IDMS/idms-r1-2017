/************************************************************
* Developer: Tomás García E                                 *
* Type: rest class                                          *                                       
* Created Date: 13/03/2015                                  *
************************************************************/
public class FieloPRM_UTILS_Badge{
    
    public static final string programLevelType = 'Program Level';
    
  
    public static map<String,String> getProgramLevels( ){
    
        Set<String> setBadgesIds = new Set<String>();
        map<String,String> codsIdsToReturn = new map<String,String>();
        map<String, FieloCH__ChallengeReward__c> mapBadgeIdToChallenge = new map<String, FieloCH__ChallengeReward__c>();
        
        list<FieloEE__Badge__c> listBadges = [SELECT id, F_PRM_Type__c, F_PRM_BadgeAPIName__c,F_PRM_Challenge__c, F_PRM_Challenge__r.F_PRM_ProgramCode__c FROM FieloEE__Badge__c WHERE F_PRM_Type__c =: programLevelType AND F_PRM_Type__c != null];
        
        system.debug('listBadges '+ listBadges );
        
        if(!listBadges .isEmpty()){
            for( FieloEE__Badge__c bad : listBadges ){
                setBadgesIds.add(bad.id);
            }    
        }
        
        system.debug('setBadgesIds '+ setBadgesIds);
        
        list<FieloCH__ChallengeReward__c> listChallengesR = [SELECT id, FieloCH__Badge__c, FieloCH__Challenge__r.FieloCH__IsActive__c FROM FieloCH__ChallengeReward__c  WHERE FieloCH__Challenge__r.FieloCH__IsActive__c = TRUE AND FieloCH__Badge__c IN: setBadgesIds ];
        
        system.debug('listChallengesR '+ listChallengesR );
        
        if(!listChallengesR.isEmpty()){
            for( FieloCH__ChallengeReward__c cha : listChallengesR ){
                mapBadgeIdToChallenge.put(cha.FieloCH__Badge__c, cha );
            }    
        }
        
        system.debug('mapBadgeIdToChallenge '+ mapBadgeIdToChallenge);
        
        if(!listBadges .isEmpty()){
            for( FieloEE__Badge__c bad : listBadges ){
                if(mapBadgeIdToChallenge.containsKey(bad.id)){
                    codsIdsToReturn.put(bad.F_PRM_BadgeAPIName__c, bad.F_PRM_Challenge__r.F_PRM_ProgramCode__c );
                } 
            }    
        }
        
        system.debug('codsIdsToReturn '+ codsIdsToReturn);

               
        return codsIdsToReturn;
    }
    

  
}