/*
 Handler for Platforming scoring trigger
 -Populate the weight based on the master data.
 -Preference while fetching the master data weights
 1. Country weight based on the Account country, Account classification and Market Segment
 2. Country weight based on the Account Country, Account Classification
 3. Global weight based on the Account Classification
 4. Default Global weight if nothing found in above 3 conditions.
*/

public class AP_PlatformingScoringTriggerHandler{ 
    public static void populateWeight(List<SFE_PlatformingScoring__c> pslist)
    {
        List<Id> acclist=new List<Id>();
        List<CAPCountryWeight__c> pscountryWeightlist;
        List<CAPGlobalMasterData__c> psglobalWeightlist;
        List<CAPGlobalMasterData__c> psDefaultglobalWeightlist;
        Map<String,List<CAPCountryWeight__c>> countryWeightMap=new Map<String,List<CAPCountryWeight__c>>();
        Map<String,List<CAPGlobalMasterData__c>> globalWeightMap=new Map<String,List<CAPGlobalMasterData__c>>();

        for(SFE_PlatformingScoring__c ps:pslist)
            acclist.add(ps.PlatformedAccount__c);
            
        Map<Id,Account> accMap=new Map<Id,Account>([select Id,Name,Country__c,ClassLevel1__c,MarketSegment__c from Account where id in :acclist]);
        List<CAPCountryWeight__c> countryWeightlist=[Select ClassificationLevel1__c,Country__c,CountryWeight__c,MarketSegment__c,QuestionSequence__c from CAPCountryWeight__c ORDER BY QuestionSequence__c ASC];
        List<CAPGlobalMasterData__c> globalWeightlist=[Select ClassificationLevel1__c,Weight__c,QuestionSequence__c from CAPGlobalMasterData__c ORDER BY QuestionSequence__c ASC];


        //Country weight
        for(CAPCountryWeight__c cwt:countryWeightlist){
            String ky=cwt.Country__c+'_'+cwt.ClassificationLevel1__c+'_'+cwt.MarketSegment__c;//marketsegment may or not have value
            if(countryWeightMap.containskey(ky))
                countryWeightMap.get(ky).add(cwt);
            else
                countryWeightMap.put(ky,new List<CAPCountryWeight__c>{(cwt)});
        }
        //Global weight
        for(CAPGlobalMasterData__c cwt:globalWeightlist){
            if(globalWeightMap.containskey(cwt.ClassificationLevel1__c))
                globalWeightMap.get(cwt.ClassificationLevel1__c).add(cwt);
            else
                globalWeightMap.put(cwt.ClassificationLevel1__c,new List<CAPGlobalMasterData__c >{(cwt)});
        }
                    
        for(SFE_PlatformingScoring__c ps:pslist){
            ps.Q1MasterDataWeight__c=0;
            ps.Q2MasterDataWeight__c=0;
            ps.Q3MasterDataWeight__c=0;
            ps.Q4MasterDataWeight__c=0;
            ps.Q5MasterDataWeight__c=0;
            ps.Q6MasterDataWeight__c=0;
            ps.Q7MasterDataWeight__c=0;
            ps.Q8MasterDataWeight__c=0;
           
           pscountryWeightlist=new List<CAPCountryWeight__c>();
           psglobalWeightlist=new List<CAPGlobalMasterData__c>();
           psDefaultglobalWeightlist=new List<CAPGlobalMasterData__c>();

            System.debug('******'+pscountryWeightlist+'>>>>>'+psglobalWeightlist+'<<<<<<<'+psDefaultglobalWeightlist);
            //first preference with market segment key
            String ky=accMap.get(ps.PlatformedAccount__c).Country__c+'_'+accMap.get(ps.PlatformedAccount__c).ClassLevel1__c+'_'+accMap.get(ps.PlatformedAccount__c).MarketSegment__c;
            if(!countryWeightMap.ContainsKey(ky))
                 ky=accMap.get(ps.PlatformedAccount__c).Country__c+'_'+accMap.get(ps.PlatformedAccount__c).ClassLevel1__c+'_'+null; // without market segment fetch the value
            if(countryWeightMap.ContainsKey(ky))
                pscountryWeightlist=countryWeightMap.get(ky);//Country weight
            if(globalWeightMap.ContainsKey(accMap.get(ps.PlatformedAccount__c).ClassLevel1__c))
                psglobalWeightlist=globalWeightMap.get(accMap.get(ps.PlatformedAccount__c).ClassLevel1__c);// global weight based on classification
            if(globalWeightMap.ContainsKey('Default'))
                psDefaultglobalWeightlist=globalWeightMap.get('Default'); //Default global weight
             System.debug('******'+pscountryWeightlist+'>>>>>'+psglobalWeightlist+'<<<<<<<'+psDefaultglobalWeightlist);

            if(pscountryWeightlist.size()==8){// Country weight
                ps.Q1MasterDataWeight__c=pscountryWeightlist[0].CountryWeight__c;
                ps.Q2MasterDataWeight__c=pscountryWeightlist[1].CountryWeight__c;
                ps.Q3MasterDataWeight__c=pscountryWeightlist[2].CountryWeight__c;
                ps.Q4MasterDataWeight__c=pscountryWeightlist[3].CountryWeight__c;
                ps.Q5MasterDataWeight__c=pscountryWeightlist[4].CountryWeight__c;
                ps.Q6MasterDataWeight__c=pscountryWeightlist[5].CountryWeight__c;
                ps.Q7MasterDataWeight__c=pscountryWeightlist[6].CountryWeight__c;
                ps.Q8MasterDataWeight__c=pscountryWeightlist[7].CountryWeight__c;
                System.debug('>>>>CountryWeight'+ps.Q1MasterDataWeight__c+ps.Q2MasterDataWeight__c+ps.Q3MasterDataWeight__c+ps.Q4MasterDataWeight__c+ps.Q5MasterDataWeight__c+ps.Q6MasterDataWeight__c+ps.Q7MasterDataWeight__c+ps.Q8MasterDataWeight__c);
            }
            else if(psglobalWeightlist.size()==8){//Global weight based on classification
                ps.Q1MasterDataWeight__c=psglobalWeightlist[0].Weight__c;
                ps.Q2MasterDataWeight__c=psglobalWeightlist[1].Weight__c;
                ps.Q3MasterDataWeight__c=psglobalWeightlist[2].Weight__c;
                ps.Q4MasterDataWeight__c=psglobalWeightlist[3].Weight__c;
                ps.Q5MasterDataWeight__c=psglobalWeightlist[4].Weight__c;
                ps.Q6MasterDataWeight__c=psglobalWeightlist[5].Weight__c;
                ps.Q7MasterDataWeight__c=psglobalWeightlist[6].Weight__c;
                ps.Q8MasterDataWeight__c=psglobalWeightlist[7].Weight__c;
               System.debug('>>>>globalweight'+ps.Q1MasterDataWeight__c+ps.Q2MasterDataWeight__c+ps.Q3MasterDataWeight__c+ps.Q4MasterDataWeight__c+ps.Q5MasterDataWeight__c+ps.Q6MasterDataWeight__c+ps.Q7MasterDataWeight__c+ps.Q8MasterDataWeight__c);
            }
            else if(psDefaultglobalWeightlist.size()==8){ // Default weight
                ps.Q1MasterDataWeight__c=psDefaultglobalWeightlist[0].Weight__c;
                ps.Q2MasterDataWeight__c=psDefaultglobalWeightlist[1].Weight__c;
                ps.Q3MasterDataWeight__c=psDefaultglobalWeightlist[2].Weight__c;
                ps.Q4MasterDataWeight__c=psDefaultglobalWeightlist[3].Weight__c;
                ps.Q5MasterDataWeight__c=psDefaultglobalWeightlist[4].Weight__c;
                ps.Q6MasterDataWeight__c=psDefaultglobalWeightlist[5].Weight__c;
                ps.Q7MasterDataWeight__c=psDefaultglobalWeightlist[6].Weight__c;
                ps.Q8MasterDataWeight__c=psDefaultglobalWeightlist[7].Weight__c;
                System.debug('>>>>Defaultweight'+ps.Q1MasterDataWeight__c+ps.Q2MasterDataWeight__c+ps.Q3MasterDataWeight__c+ps.Q4MasterDataWeight__c+ps.Q5MasterDataWeight__c+ps.Q6MasterDataWeight__c+ps.Q7MasterDataWeight__c+ps.Q8MasterDataWeight__c);

            }
            System.debug('>>>>after'+ps.Q1MasterDataWeight__c+ps.Q2MasterDataWeight__c+ps.Q3MasterDataWeight__c+ps.Q4MasterDataWeight__c+ps.Q5MasterDataWeight__c+ps.Q6MasterDataWeight__c+ps.Q7MasterDataWeight__c+ps.Q8MasterDataWeight__c);
        }
    }
}