public with sharing class VFC_ExpertAppNameplate {
    private final ID installedProductId;
    private SVMXC__Installed_Product__c installedProduct;
    private String redirectUrl;
    

    public VFC_ExpertAppNameplate(ApexPages.StandardController controller) {
        installedProductId = controller.getID();      
    }

    public PageReference doStuffAndRedirect() {   
        installedProduct = [select SVMXC__Site__r.id from SVMXC__Installed_Product__c where id=:installedProductId];
        
        redirectUrl = 'http://88.84.91.92/ExpertAppWeb/bFOIntegration/api10';
        redirectUrl += '?user='+Userinfo.getUserId();
        redirectUrl += '&urlserver=https://cs9.salesforce.com/services/Soap/u/28.0/'+UserInfo.getOrganizationId();
        redirectUrl += '&content=Nameplate';
        redirectUrl += '&account='+installedProduct.SVMXC__Site__r.id;
        redirectUrl += '&contentid='+installedProductId;
        redirectUrl += '&sessionid='+Userinfo.getSessionId();
        
        PageReference ReturnPage = new PageReference(redirectUrl);        
        ReturnPage.setRedirect(true);
        return ReturnPage;
    }

}