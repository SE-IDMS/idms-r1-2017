@isTest
Public Class VFC_GCSSitesListPage_TEST{
     Private Static TestMethod  Void UnitTest1(){
     Integer max = 5;
      
      Account accounts = Utils_TestMethods.createAccount();
      //accounts.OwnerId = Usr.Id;
      accounts.SEAccountID__c ='12345689';
      Database.insert(accounts);
      
      Country__c Contry = New Country__c ();
      Contry.Name='France';Contry .CountryCode__c='Fr';
      insert Contry;
      
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      
      Database.insert(con); 
      Con.Country__c = Contry.Id;
      update Con; 
      
      User userObj=Utils_TestMethods.createStandardUser('Test');
      insert userObj;
      
      Test.StartTest();
       Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'New');
       Database.insert(caseobj);
       Test.StopTest();
       
       
       GCSSite__c  siteObj = new GCSSite__c();
       siteObj.Account__c=accounts.id;
       siteObj.LegacyIBSiteID__c='TestIBSiteID1';
       siteObj.Name='Testsitess';
       siteObj.Active__c=true;
       siteObj.LegacySiteId__c='TestLegacySiteID';
       insert siteObj; 
       
       GCSSite__c  siteObj1 = new GCSSite__c();
       siteObj1.Account__c=accounts.id;
       siteObj1.LegacyIBSiteID__c='TestIBSiteID2';
       siteObj1.Name='Testsitess';
       siteObj1.Active__c=true;
       siteObj1.LegacySiteId__c='TestLegacySiteID1';
       insert siteObj1;
       
       GCSSite__c  siteObj2 = new GCSSite__c();
       siteObj2.Account__c=accounts.id;
       siteObj2.LegacyIBSiteID__c='TestIBSiteID2';
       siteObj2.Name='Testsitess';
       siteObj2.Active__c=true;
       siteObj2.LegacySiteId__c='TestLegacySiteID2';
       insert siteObj2; 
               GCSSite__c  siteObj3 = new GCSSite__c();
       siteObj3.Account__c=accounts.id;
       siteObj3.LegacyIBSiteID__c='TestIBSiteID3';
       siteObj3.Name='Testsitess';
       siteObj3.Active__c=true;
       siteObj3.LegacySiteId__c='TestLegacySiteID3';
       insert siteObj3;
       
       
       GCSSite__c  siteObj4 = new GCSSite__c();
       siteObj4.Account__c=accounts.id;
       siteObj4.LegacyIBSiteID__c='TestIBSiteID4';
       siteObj4.Name='Testsitess';
       siteObj4.Active__c=true;
       siteObj4.LegacySiteId__c='TestLegacySiteID4';
       insert siteObj4;
       
       GCSSite__c  siteObj5 = new GCSSite__c();
       siteObj5.Account__c=accounts.id;
       siteObj5.LegacyIBSiteID__c='TestIBSiteID5';
       siteObj5.Name='Testsitess';
       siteObj5.Active__c=true;
       siteObj5.LegacySiteId__c='TestLegacySiteID5';
       insert siteObj5;
       
       GCSSite__c  siteObj6 = new GCSSite__c();
       siteObj6.Account__c=accounts.id;
       siteObj6.LegacyIBSiteID__c='TestIBSiteID6';
       siteObj6.Name='Testsitess';
       siteObj6.Active__c=true;
       siteObj6.LegacySiteId__c='TestLegacySiteID6';
       insert siteObj6;
       
       GCSSite__c  siteObj7 = new GCSSite__c();
       siteObj7.Account__c=accounts.id;
       siteObj7.LegacyIBSiteID__c='TestIBSiteID7';
       siteObj7.Name='Testsitess';
       siteObj7.Active__c=true;
       siteObj7.LegacySiteId__c='TestLegacySiteID7';
       insert siteObj7;
       
       GCSSite__c  siteObj8 = new GCSSite__c();
       siteObj8.Account__c=accounts.id;
       siteObj8.LegacyIBSiteID__c='TestIBSiteID8';
       siteObj8.Name='Testsitess';
       siteObj8.Active__c=true;
       siteObj8.LegacySiteId__c='TestLegacySiteID8';
       insert siteObj8;
       
       GCSSite__c  siteObj9 = new GCSSite__c();
       siteObj9.Account__c=accounts.id;
       siteObj9.LegacyIBSiteID__c='TestIBSiteID9';
       siteObj9.Name='Testsitess';
       siteObj9.Active__c=true;
       siteObj9.LegacySiteId__c='TestLegacySiteID9';
       insert siteObj9;
       
       GCSSite__c  siteObj10 = new GCSSite__c();
       siteObj10.Account__c=accounts.id;
       siteObj10.LegacyIBSiteID__c='TestIBSiteID10';
       siteObj10.Name='Testsitess';
       siteObj10.Active__c=true;
       siteObj10.LegacySiteId__c='TestLegacySiteID10';
       insert siteObj10;
       
       GCSSite__c  siteObj11 = new GCSSite__c();
       siteObj11.Account__c=accounts.id;
       siteObj11.LegacyIBSiteID__c='TestIBSiteID11';
       siteObj11.Name='Testsitess';
       siteObj11.Active__c=true;
       siteObj11.LegacySiteId__c='TestLegacySiteID11';
       insert siteObj11;
       
          
       ApexPages.StandardController Ctrl =New ApexPages.StandardController(siteObj);
       VFC_GCSSitesListPage GCSSites= New VFC_GCSSitesListPage(Ctrl);
       GCSSites.index =1;
       GCSSites.start = 1;
        GCSSites.next();
     //  GCSSites.previous();
       
       
      }
     
     
     Private Static TestMethod Void UnitTest2(){
     
     Integer max = 5;
      User userObj=Utils_TestMethods.createStandardUser('Test');
      insert userObj;
      
      
      Test.StartTest();
      Account accounts = Utils_TestMethods.createAccount();
      //accounts.OwnerId = Usr.Id;
      accounts.SEAccountID__c ='123456891';
      Database.insert(accounts);
            
      Country__c Contry = New Country__c ();
      Contry.Name='France';Contry .CountryCode__c='Fr';
      insert Contry;
      
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      
      Database.insert(con); 
      Con.Country__c = Contry.Id;
      update Con; 
        
       Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'New');
       Database.insert(caseobj);
       Test.StopTest();
           GCSSite__c  siteObj3 = new GCSSite__c();
       siteObj3.Account__c=accounts.id;
       siteObj3.LegacyIBSiteID__c='TestIBSiteID3';
       siteObj3.Name='Testsitess';
       siteObj3.Active__c=False;
       siteObj3.LegacySiteId__c='TestLegacySiteID3';
       insert siteObj3;
       
       
       GCSSite__c  siteObj4 = new GCSSite__c();
       siteObj4.Account__c=accounts.id;
       siteObj4.LegacyIBSiteID__c='TestIBSiteID4';
       siteObj4.Name='Testsitess';
       siteObj4.Active__c=False;
       siteObj4.LegacySiteId__c='TestLegacySiteID4';
       insert siteObj4;
       
       GCSSite__c  siteObj5 = new GCSSite__c();
       siteObj5.Account__c=accounts.id;
       siteObj5.LegacyIBSiteID__c='TestIBSiteID5';
       siteObj5.Name='Testsitess';
       siteObj5.Active__c=False;
       siteObj5.LegacySiteId__c='TestLegacySiteID5';
       insert siteObj5;
       
       GCSSite__c  siteObj6 = new GCSSite__c();
       siteObj6.Account__c=accounts.id;
       siteObj6.LegacyIBSiteID__c='TestIBSiteID6';
       siteObj6.Name='Testsitess';
       siteObj6.Active__c=False;
       siteObj6.LegacySiteId__c='TestLegacySiteID6';
       insert siteObj6;
       
       GCSSite__c  siteObj7 = new GCSSite__c();
       siteObj7.Account__c=accounts.id;
       siteObj7.LegacyIBSiteID__c='TestIBSiteID7';
       siteObj7.Name='Testsitess';
       siteObj7.Active__c=False;
       siteObj7.LegacySiteId__c='TestLegacySiteID7';
       insert siteObj7;
       
       GCSSite__c  siteObj8 = new GCSSite__c();
       siteObj8.Account__c=accounts.id;
       siteObj8.LegacyIBSiteID__c='TestIBSiteID8';
       siteObj8.Name='Testsitess';
       siteObj8.Active__c=False;
       siteObj8.LegacySiteId__c='TestLegacySiteID8';
       insert siteObj8;
       
       GCSSite__c  siteObj9 = new GCSSite__c();
       siteObj9.Account__c=accounts.id;
       siteObj9.LegacyIBSiteID__c='TestIBSiteID9';
       siteObj9.Name='Testsitess';
       siteObj9.Active__c=False;
       siteObj9.LegacySiteId__c='TestLegacySiteID9';
       insert siteObj9;
       
       GCSSite__c  siteObj10 = new GCSSite__c();
       siteObj10.Account__c=accounts.id;
       siteObj10.LegacyIBSiteID__c='TestIBSiteID10';
       siteObj10.Name='Testsitess';
       siteObj10.Active__c=False;
       siteObj10.LegacySiteId__c='TestLegacySiteID10';
       insert siteObj10;
       
       ApexPages.StandardController Ctrl1 =New ApexPages.StandardController(siteObj10);
       VFC_GCSSitesListPage GCSSites1= New VFC_GCSSitesListPage(Ctrl1);
       GCSSites1.Start =1;
       GCSSites1.index =2;
       //GCSSites1.Start =11;
       GCSSites1.next();
       
       /*ApexPages.StandardController Ctrl2 =New ApexPages.StandardController(siteObj9);
       VFC_GCSSitesListPage GCSSites2= New VFC_GCSSitesListPage(Ctrl2);
       GCSSites2.Start =11;
       GCSSites2.index =5;
       GCSSites2.Start =11;
       GCSSites2.previous();*/
     }

    
    Private Static TestMethod Void UnitTest3(){
     
     Integer max = 5;
      
      User RunningUser = [select id,name,IsActive,BypassVR__c from user where profile.Name = 'System Administrator' and BypassVR__c=true and IsActive=True limit 1];
      system.RunAs(RunningUser){
      UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
      String randomString = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,max).toUpperCase();
      Profile profile = [select id from profile where name='System Administrator'];
      User Usr =New User();
      Usr.UserRoleId =portalRole.Id; Usr.alias = 'testusr';Usr.email='testusr'+ '@accenture.com';Usr.emailencodingkey='UTF-8';Usr.lastname='Testing';Usr.languagelocalekey='en_US'; 
      Usr.localesidkey='en_US';Usr.profileid = profile.Id;Usr.BypassWF__c = true;
      Usr.timezonesidkey='Europe/London';Usr.username='testusr'+randomString+ '@bridge-fo.com';Usr . CAPRelevant__c=true;Usr . VisitObjDay__c=1;
      Usr.AvTimeVisit__c=3;Usr .WorkDaysYear__c=200;Usr .BusinessHours__c=8;Usr .UserBusinessUnit__c='Cross Business';Usr .Country__c='FR';
      Usr.BypassVR__c=true;//Usr.ContactId=con.Id;
      Insert Usr;
      
      Test.StartTest();
      Account accounts = Utils_TestMethods.createAccount();
      accounts.OwnerId = Usr.Id;
      accounts.SEAccountID__c ='1234568911';
      Database.insert(accounts);
            
      Country__c Contry = New Country__c ();
      Contry.Name='France';Contry .CountryCode__c='Fr';
      insert Contry;
      
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      
      Database.insert(con); 
      Con.Country__c = Contry.Id;
      update Con; 
        
       Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'New');
       Database.insert(caseobj);
       Test.StopTest();
           GCSSite__c  siteObj3 = new GCSSite__c();
       siteObj3.Account__c=accounts.id;
       siteObj3.LegacyIBSiteID__c='TestIBSiteID3';
       siteObj3.Name='Testsitess';
       siteObj3.Active__c=False;
       siteObj3.LegacySiteId__c='TestLegacySiteID3';
       insert siteObj3;
       
       
       GCSSite__c  siteObj4 = new GCSSite__c();
       siteObj4.Account__c=accounts.id;
       siteObj4.LegacyIBSiteID__c='TestIBSiteID4';
       siteObj4.Name='Testsitess';
       siteObj4.Active__c=False;
       siteObj4.LegacySiteId__c='TestLegacySiteID4';
       insert siteObj4;
       
       GCSSite__c  siteObj5 = new GCSSite__c();
       siteObj5.Account__c=accounts.id;
       siteObj5.LegacyIBSiteID__c='TestIBSiteID5';
       siteObj5.Name='Testsitess';
       siteObj5.Active__c=False;
       siteObj5.LegacySiteId__c='TestLegacySiteID5';
       insert siteObj5;
       
       GCSSite__c  siteObj6 = new GCSSite__c();
       siteObj6.Account__c=accounts.id;
       siteObj6.LegacyIBSiteID__c='TestIBSiteID6';
       siteObj6.Name='Testsitess';
       siteObj6.Active__c=False;
       siteObj6.LegacySiteId__c='TestLegacySiteID6';
       insert siteObj6;
       
       GCSSite__c  siteObj7 = new GCSSite__c();
       siteObj7.Account__c=accounts.id;
       siteObj7.LegacyIBSiteID__c='TestIBSiteID7';
       siteObj7.Name='Testsitess';
       siteObj7.Active__c=False;
       siteObj7.LegacySiteId__c='TestLegacySiteID7';
       insert siteObj7;
       
       GCSSite__c  siteObj8 = new GCSSite__c();
       siteObj8.Account__c=accounts.id;
       siteObj8.LegacyIBSiteID__c='TestIBSiteID8';
       siteObj8.Name='Testsitess';
       siteObj8.Active__c=False;
       siteObj8.LegacySiteId__c='TestLegacySiteID8';
       insert siteObj8;
       
       GCSSite__c  siteObj9 = new GCSSite__c();
       siteObj9.Account__c=accounts.id;
       siteObj9.LegacyIBSiteID__c='TestIBSiteID9';
       siteObj9.Name='Testsitess';
       siteObj9.Active__c=False;
       siteObj9.LegacySiteId__c='TestLegacySiteID9';
       insert siteObj9;
       
       GCSSite__c  siteObj10 = new GCSSite__c();
       siteObj10.Account__c=accounts.id;
       siteObj10.LegacyIBSiteID__c='TestIBSiteID10';
       siteObj10.Name='Testsitess';
       siteObj10.Active__c=False;
       siteObj10.LegacySiteId__c='TestLegacySiteID10';
       insert siteObj10;
       
       
       ApexPages.StandardController Ctrl2 =New ApexPages.StandardController(siteObj9);
       VFC_GCSSitesListPage GCSSites2= New VFC_GCSSitesListPage(Ctrl2);
       GCSSites2.Start =0;
       GCSSites2.index =5;
       GCSSites2.Start =11;
       GCSSites2.previous();
     }

    }
    Private Static TestMethod  Void UnitTest4(){
     Integer max = 5;
      
      Account accounts = Utils_TestMethods.createAccount();
      //accounts.OwnerId = Usr.Id;
      accounts.SEAccountID__c ='12345689';
      Database.insert(accounts);
      
      Country__c Contry = New Country__c ();
      Contry.Name='France';Contry .CountryCode__c='Fr';
      insert Contry;
      
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      
      Database.insert(con); 
      Con.Country__c = Contry.Id;
      update Con; 
      
      User userObj=Utils_TestMethods.createStandardUser('Test');
      insert userObj;
      
      Test.StartTest();
       Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'New');
       Database.insert(caseobj);
       Test.StopTest();
       
       
       GCSSite__c  siteObj = new GCSSite__c();
       siteObj.Account__c=accounts.id;
       siteObj.LegacyIBSiteID__c='TestIBSiteID1';
       siteObj.Name='Testsitess';
       siteObj.Active__c=true;
       siteObj.LegacySiteId__c='TestLegacySiteID';
       insert siteObj; 
       
       GCSSite__c  siteObj1 = new GCSSite__c();
       siteObj1.Account__c=accounts.id;
       siteObj1.LegacyIBSiteID__c='TestIBSiteID2';
       siteObj1.Name='Testsitess';
       siteObj1.Active__c=true;
       siteObj1.LegacySiteId__c='TestLegacySiteID1';
       insert siteObj1;
       
       GCSSite__c  siteObj2 = new GCSSite__c();
       siteObj2.Account__c=accounts.id;
       siteObj2.LegacyIBSiteID__c='TestIBSiteID2';
       siteObj2.Name='Testsitess';
       siteObj2.Active__c=true;
       siteObj2.LegacySiteId__c='TestLegacySiteID2';
       insert siteObj2; 
               GCSSite__c  siteObj3 = new GCSSite__c();
       siteObj3.Account__c=accounts.id;
       siteObj3.LegacyIBSiteID__c='TestIBSiteID3';
       siteObj3.Name='Testsitess';
       siteObj3.Active__c=true;
       siteObj3.LegacySiteId__c='TestLegacySiteID3';
       insert siteObj3;
       
       
       GCSSite__c  siteObj4 = new GCSSite__c();
       siteObj4.Account__c=accounts.id;
       siteObj4.LegacyIBSiteID__c='TestIBSiteID4';
       siteObj4.Name='Testsitess';
       siteObj4.Active__c=true;
       siteObj4.LegacySiteId__c='TestLegacySiteID4';
       insert siteObj4;
       
       GCSSite__c  siteObj5 = new GCSSite__c();
       siteObj5.Account__c=accounts.id;
       siteObj5.LegacyIBSiteID__c='TestIBSiteID5';
       siteObj5.Name='Testsitess';
       siteObj5.Active__c=true;
       siteObj5.LegacySiteId__c='TestLegacySiteID5';
       insert siteObj5;
       
       GCSSite__c  siteObj6 = new GCSSite__c();
       siteObj6.Account__c=accounts.id;
       siteObj6.LegacyIBSiteID__c='TestIBSiteID6';
       siteObj6.Name='Testsitess';
       siteObj6.Active__c=true;
       siteObj6.LegacySiteId__c='TestLegacySiteID6';
       insert siteObj6;
       
       GCSSite__c  siteObj7 = new GCSSite__c();
       siteObj7.Account__c=accounts.id;
       siteObj7.LegacyIBSiteID__c='TestIBSiteID7';
       siteObj7.Name='Testsitess';
       siteObj7.Active__c=true;
       siteObj7.LegacySiteId__c='TestLegacySiteID7';
       insert siteObj7;
       
       GCSSite__c  siteObj8 = new GCSSite__c();
       siteObj8.Account__c=accounts.id;
       siteObj8.LegacyIBSiteID__c='TestIBSiteID8';
       siteObj8.Name='Testsitess';
       siteObj8.Active__c=true;
       siteObj8.LegacySiteId__c='TestLegacySiteID8';
       insert siteObj8;
       
       GCSSite__c  siteObj9 = new GCSSite__c();
       siteObj9.Account__c=accounts.id;
       siteObj9.LegacyIBSiteID__c='TestIBSiteID9';
       siteObj9.Name='Testsitess';
       siteObj9.Active__c=true;
       siteObj9.LegacySiteId__c='TestLegacySiteID9';
       insert siteObj9;
       
       GCSSite__c  siteObj10 = new GCSSite__c();
       siteObj10.Account__c=accounts.id;
       siteObj10.LegacyIBSiteID__c='TestIBSiteID10';
       siteObj10.Name='Testsitess';
       siteObj10.Active__c=true;
       siteObj10.LegacySiteId__c='TestLegacySiteID10';
       insert siteObj10;
       
       GCSSite__c  siteObj11 = new GCSSite__c();
       siteObj11.Account__c=accounts.id;
       siteObj11.LegacyIBSiteID__c='TestIBSiteID11';
       siteObj11.Name='Testsitess';
       siteObj11.Active__c=true;
       siteObj11.LegacySiteId__c='TestLegacySiteID11';
       insert siteObj11;
       
          
       ApexPages.StandardController Ctrl4 =New ApexPages.StandardController(siteObj4);
       VFC_GCSSitesListPage GCSSites4= New VFC_GCSSitesListPage(Ctrl4);
       GCSSites4.start = 10;
       GCSSites4.index =1;
     
       GCSSites4.previous();
       
       
       
      }
}