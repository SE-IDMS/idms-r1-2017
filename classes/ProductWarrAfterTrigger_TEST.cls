/**
Deepak
 */
@isTest(SeeAlldata=true)
private class ProductWarrAfterTrigger_TEST {
 
    public static testMethod void myUnitTest() {
        
        //Creating Data
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeid = Label.CLOCT13ACC08;
        insert objAccount;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = objAccount .id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        Country__c country= Utils_TestMethods.createCountry(); 
        country.CountryCode__c= 'hkh';   
        insert country; 
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        objContact.Country__c= country.id;
        insert objContact;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCase.SVMXC__Site__c = site1.id;
        insert objCase;
        
        Brand__c brand1_1 = new Brand__c();

        brand1_1.Name ='Brand 1-1';
        brand1_1.SDHBRANDID__c = 'Test_BrandSDHID';
        brand1_1.IsSchneiderBrand__c = true;
        insert brand1_1 ;
        
        DeviceType__c dt1_1 = new DeviceType__c();
        dt1_1.name = 'Device Type 1-1';
        dt1_1.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-1';
        insert dt1_1 ;
        
        OPP_Product__c opProduct= New OPP_Product__c();
        opProduct.Name='opp_product';
        opProduct.BusinessUnit__c='BD';
        opProduct.ProductFamily__c='ITB21';
        opProduct.ProductLine__c='PWACB';
        insert opProduct;
        
        Product2 prod = New Product2();
        prod.Name = 'Product_test';
        //prod.Serviceability__c =
        prod.Brand2__c=brand1_1.Id;
        prod.DeviceType2__c=dt1_1.Id;
        prod.ProductGDP__c=opProduct.Id;
        insert prod;
        
        list<SVMXC__Installed_Product__c> iplist = new list<SVMXC__Installed_Product__c>();
         
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = objAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.RecordTypeId = Label.CLAPR14SRV09;
        ip1.SVMXC__Product__c = prod.Id;
        ip1.SVMXC__Serial_Lot_Number__c = 'TestSerialLotNumber';
        ip1.EndOfWarrantyOpportunityGenerated__c= false;
        ip1.SVMXC__Warranty_End_Date__c = date.today() + 120;
        ip1.ObsolescenceOpportunityGenerated__c= false;
        ip1.SVMXC__Warranty_Start_Date__c= date.today()+5;
        //date myDate = date.newInstance(2015, 12, 17);
        ip1.CommissioningDateInstallDate__c=date.today();
        ip1.Tech_IpAssociatedToSC__c= false;
        ip1.AssetCategory2__c='Category 1';
        ip1.UnderContract__c=true;
        ip1.SKUToCreate__c='jhgfus';
        insert ip1;
        
        
        
        
        SVMXC__Warranty__c pw = new SVMXC__Warranty__c();
        pw.SVMXC__Installed_Product__c = ip1.Id;
        pw.SVMXC__Start_Date__c= date.today() + 10;
        pw.SVMXC__End_Date__c=date.today() + 30;
        
        insert pw;
        //update pw
        pw.SVMXC__Start_Date__c= date.today() + 12;
        pw.SVMXC__End_Date__c=date.today() + 32;
        update pw;
        
        ip1.SVMXC__Warranty_Start_Date__c =pw.SVMXC__Start_Date__c;
        ip1.SVMXC__Warranty_End_Date__c =pw.SVMXC__End_Date__c;
        update ip1;
               
    }

}