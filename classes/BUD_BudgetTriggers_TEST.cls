/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 11/03/2011
    Description     : Test class for the classes BUD_BudgetTriggers and BUD_BudgetUtils.
*/


@isTest
private class BUD_BudgetTriggers_TEST
{
    static testMethod void BudgetTriggersUnitTest() 
    {
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tst1');
        
        insert runAsUser;
        System.runAs(runAsUser)
        {
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            insert testProj;
            Budget__c testBud1 = Utils_TestMethods.createBudget(testProj,false);
            insert testBud1;
            Budget__c testBud2 = Utils_TestMethods.createBudget(testProj,false);
            insert testBud2;
            Budget__c testBud3 = Utils_TestMethods.createBudget(testProj,true);
            insert testBud3;
            
            test.startTest();
            
            Budget__c testBud4 = Utils_TestMethods.createBudget(testProj,true);
            insert testBud4;
            //check if testBud4 is active and testBud3 is inactive
            Budget__c testRetrievedBudget = [Select Active__c from Budget__c where Id = :testBud3.Id limit 1];
            System.AssertEquals(false,testRetrievedBudget.Active__c);
            testRetrievedBudget = [Select Active__c from Budget__c where Id = :testBud4.Id limit 1];
            System.AssertEquals(true,testRetrievedBudget.Active__c);
            testBud2.Active__c = true;
            update testBud2;
            List<Budget__c> listBudget = new List<Budget__c>();
            listBudget.add(testBud2);
            BUD_BudgetUtils.updateProjectRequestFields(listBudget);
            //check if testBud2 is active
            testRetrievedBudget = [Select Active__c from Budget__c where Id = :testBud2.Id limit 1];
            System.AssertEquals(true,testRetrievedBudget.Active__c);
            Delete testBud2;                        
            
            
            
        }
        test.stopTest();
        
    }   
}