/**
11-June-2012    Srinivas Nallapati  Sept Mkt Release    Initial Creation: test class for VFC_MassLeadOwnerChange
 */
@isTest
private class VFC_ConvertLead_TEST
{

    static testMethod void testConsumerLead() {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0) {
            User user1 = Utils_TestMethods.createStandardUser('78VFC92');
            Insert user1;
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            // user.BypassWF__c = false;
            user.BypassVR__c = true;
            user.Champion__c = user1.Id;
            insert user;
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
            system.runas(user) {
                list<CS_LeadToOpportunity__c> lstLeadtoOpp =new   list<CS_LeadToOpportunity__c>();
                CS_LeadToOpportunity__c csLead1 = new CS_LeadToOpportunity__c(Name='Account',Source__c='Account',Destination__c='Account');
                lstLeadtoOpp.add(csLead1);
                Insert lstLeadtoOpp;
                           
                Country__c country= Utils_TestMethods.createCountry();
                insert country;
                
                Account consumerAccount1  = Utils_TestMethods.createConsumerAccount(country.Id);
                Account consumerAccount2  = Utils_TestMethods.createConsumerAccount(country.Id);
               
                System.Debug('Account to be inserted next' + consumerAccount1);
                insert (new list<Account>{ consumerAccount1, consumerAccount2});
                          
                MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                insert markCallBack;

                List<Lead> conleads = new List<Lead>();
                
                for(Integer i=0; i < 1;i++)
                {
                    Lead lead2 = Utils_TestMethods.createConsumerLead(null, country.Id, 100);
                    if(mapRecordType.get('Marcom Lead')!= null)    
                        lead2.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                    lead2.firstname = 'j';
                    lead2.lastName = 'k';
                    lead2.Status = '1 - New';
                    lead2.LeadingBusiness__c = 'ITB';
                    lead2.OpportunityType__c='Standard';
                    lead2.SuspectedValue__c = 1234;
                    lead2.OpportunityFinder__c = 'abed';
                    lead2.City__c = 'city1';
                    lead2.Street__c = 'abced';
                    lead2.ClassificationLevel1__c = 'Internal';
                    lead2.ZipCode__c = '12345';
                    
                    if(math.mod(i,2) != 0) {
                       lead2.Account__c = consumerAccount1.id;
                    }    
                    else {
                        lead2.Account__c = consumerAccount2.id;
                    }
                    
                                   
                    conleads.add(lead2);   
                }
                System.Debug('The records in conleads is :' +conleads);
                List<Id> conleadIds = new List<Id>();
                
                Database.SaveResult[] conleadsInsert = Database.insert(conleads, true);

                for(Database.SaveResult sr1: conleadsInsert) {
                     if(!sr1.isSuccess()) {
                         Database.Error err1 = sr1.getErrors()[0];
                         System.Debug('######## AP19_Lead_TEST Error Inserting: '+err1.getStatusCode()+' '+err1.getMessage());
                     }
                     else
                         conleadIds.add(sr1.Id);
                }

                lead led2 = [select id, Account__c, contact__c from lead where Account__c != null and lead.Account__r.isPersonAccount = true and Contact__c!= null and status='1 - New' and TECH_Ready2Convert__c='true' limit 1];
                List<LeadValueChainPlayer__c> lstLeadVCP = new list<LeadValueChainPlayer__c>();
                for(integer i=0; i<1;i++) {
                    LeadValueChainPlayer__c LeadVCP1 = New LeadValueChainPlayer__c(Account__c=consumerAccount1.id,Lead__c=led2.id);
                      
                    lstLeadVCP.add(LeadVCP1);
                }
                Database.SaveResult[] leadVCPInsert= Database.insert(lstLeadVCP, true);
                 
                Apexpages.Standardcontroller controller4 = new Apexpages.Standardcontroller(conleads[0]);
                Pagereference pg4 = Page.VFP_ConvertLead; 
                Test.setCurrentPage(pg4);
                
                
                VFC_ConvertLead vfcon4 = new VFC_ConvertLead(controller4);
                vfcon4.getLeadStatus();
                //vfcon4.getlstCompanyInfo();
                //vfcon4.getlstContactInfo();
                vfcon4.getReminderTimes(); 
                vfcon4.SourceLead.OwnerId = user1.Id; //[select id from user where isActive=true and profile.name='System Administrator' limit 1].id;
                vfcon4.donotcreatetask = true;
                vfcon4.NewTask.IsReminderSet = true;
                vfcon4.NewTask.TECH_ReminderDate__c = system.today()+1;
                vfcon4.ReminderTime = '11';
                vfcon4.OpportunityName = 'testopp3';
                vfcon4.strAccountId = led2.Account__c;
                //vfcon4.strContactId = '1';
                vfcon4.CheckContact();
                vfcon4.strContactId = led2.Contact__c;
                vfcon4.NewTask.OwnerId = vfcon4.SourceLead.OwnerId;
                vfcon4.strAccountId = vfcon4.SourceLead.Account__c;
                
                test.startTest();
                vfcon4.convertLead();
                test.stopTest();
                          
            }
        }

    }
    
    static testMethod void testConsumerLeadwithoutacc() {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0) {
            User user1 = Utils_TestMethods.createStandardUser('78VFC92');
            Insert user1;
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            // user.BypassWF__c = false;
            user.BypassVR__c = true;
            user.Champion__c = user1.Id;
            insert user;
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
            system.runas(user) {
                list<CS_LeadToOpportunity__c> lstLeadtoOpp =new   list<CS_LeadToOpportunity__c>();
                CS_LeadToOpportunity__c csLead1 = new CS_LeadToOpportunity__c(Name='Account',Source__c='Account',Destination__c='Account');
                lstLeadtoOpp.add(csLead1);
                Insert lstLeadtoOpp;
                           
                Country__c country= Utils_TestMethods.createCountry();
                insert country;
                
                Account consumerAccount3  = Utils_TestMethods.createConsumerAccount(country.Id);
                Account consumerAccount4  = Utils_TestMethods.createConsumerAccount(country.Id);
                //consumerAccount3.Name = consumerAccount4.Name = 'TestAccount';
                //consumerAccount3.Firstname = consumerAccount4.Firstname = null;
                //consumerAccount3.LastName = consumerAccount4.LastName = null;
                //consumerAccount3.Phone = consumerAccount4.Phone = '1232142';
                //consumerAccount3.City__c = consumerAccount4.City__c = 'city1';
                System.Debug('Account to be inserted next' + consumerAccount3);
                insert (new list<Account>{ consumerAccount3, consumerAccount4});
                          
                MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                insert markCallBack;

                List<Lead> conleads1 = new List<Lead>();
                
                for(Integer i=0; i < 1;i++)
                {
                    Lead lead3 = Utils_TestMethods.createConsumerLead(null, country.Id, 100);
                    if(mapRecordType.get('Marcom Lead')!= null)    
                        lead3.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                    lead3.firstname = 'j';
                    lead3.lastName = 'k';
                    lead3.Status = '1 - New';
                    lead3.LeadingBusiness__c = 'ITB';
                    lead3.OpportunityType__c='Standard';
                    lead3.SuspectedValue__c = 1234;
                    lead3.OpportunityFinder__c = 'abed';
                    lead3.City__c = 'city1';
                    lead3.Street__c = 'New Street';
                    lead3.ClassificationLevel1__c = 'Internal';
                    lead3.ZipCode__c = '12345';
                    Lead3.email = 'consumer@consumera.com';
                    Lead3.Company = 'TestAccount';
                    Lead3.Phone = '1232142';
                    /*
                    if(math.mod(i,2) != 0) {
                       lead3.Account__c = consumerAccount3.id;
                    }    
                    else {
                        lead3.Account__c = consumerAccount4.id;
                    }
                    */
                    conleads1.add(lead3);   
                }
                System.Debug('The records in conleads is :' +conleads1);
                List<Id> conleadIds1 = new List<Id>();
                
                Database.SaveResult[] conleadsInsert1 = Database.insert(conleads1, true);

                for(Database.SaveResult sr2: conleadsInsert1) {
                     if(!sr2.isSuccess()) {
                         Database.Error err2 = sr2.getErrors()[0];
                         System.Debug('######## AP19_Lead_TEST Error Inserting: '+err2.getStatusCode()+' '+err2.getMessage());
                     }
                     else
                         conleadIds1.add(sr2.Id);
                }
                /*
                lead led3 = [select id, Account__c, contact__c from lead where Account__c != null and lead.Account__r.isPersonAccount = true and Contact__c!= null and status='1 - New' and TECH_Ready2Convert__c='true' limit 1];
                List<LeadValueChainPlayer__c> lstLeadVCP1 = new list<LeadValueChainPlayer__c>();
                for(integer i=0; i<1;i++) {
                    LeadValueChainPlayer__c LeadVCP2 = New LeadValueChainPlayer__c(Account__c=consumerAccount3.id,Lead__c=led3.id);
                      
                    lstLeadVCP1.add(LeadVCP2);
                }
                Database.SaveResult[] leadVCPInsert1= Database.insert(lstLeadVCP1, true);
                */
                Apexpages.Standardcontroller controller5 = new Apexpages.Standardcontroller(conleads1[0]);
                Pagereference pg5 = Page.VFP_ConvertLead; 
                Test.setCurrentPage(pg5);
                
                
                VFC_ConvertLead vfcon5 = new VFC_ConvertLead(controller5);
                vfcon5.getLeadStatus();
                //vfcon5.getlstCompanyInfo();
                //vfcon5.getlstContactInfo();
                vfcon5.getReminderTimes(); 
                vfcon5.SourceLead.OwnerId = user1.Id; //[select id from user where isActive=true and profile.name='System Administrator' limit 1].id;
                vfcon5.donotcreatetask = true;
                vfcon5.NewTask.IsReminderSet = true;
                vfcon5.NewTask.TECH_ReminderDate__c = system.today()+1;
                vfcon5.ReminderTime = '11';
                vfcon5.NewTask.OwnerId = vfcon5.SourceLead.OwnerId;
                vfcon5.OpportunityName = 'testopp3';
                vfcon5.strAccountId = '';
                //vfcon5.strAccountId = '1';
                vfcon5.strContactId = '';
                // vfcon5.CheckContact();
                // vfcon5.strContactId = led3.Contact__c;
                              
                test.startTest();
                vfcon5.checkContact();
                test.stopTest();
                          
            }
        }

    }
 
 static testMethod void myUnitTest2()
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
            User user1 = Utils_TestMethods.createStandardUser('78VFC92');
            Insert user1;
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            //user.BypassWF__c = false;
            user.BypassVR__c = true;
            user.Champion__c = user1.Id;
            insert user;
         Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
         Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
         system.runas(user)
         {
            list<CS_LeadToOpportunity__c> lstLeadtoOpp =new   list<CS_LeadToOpportunity__c>();
            CS_LeadToOpportunity__c csLead1 = new CS_LeadToOpportunity__c(Name='Account',Source__c='Account',Destination__c='Account');
            lstLeadtoOpp.add(csLead1);
            Insert lstLeadtoOpp;
                       
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            
            //Account acc1 = Utils_TestMethods.createAccount();
            //insert acc1;
            
            System.Debug('Accounts created' +Acc); 
            //System.Debug('Account 2 created is:'+acc1);
                      
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            Contact con2 = new Contact(LastName ='TestCOnt2',firstName = 'TestFirst23', AccountId=Acc.id, email = 'texttt@test.com', Country__c = country.id);
            insert con2;
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            
            List<Lead> leads = new List<Lead>();

            for(Integer i=0;i<2;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                lead1.Company = 'a';
                lead1.firstname = 'j';
                lead1.Status = '1 - New';
                lead1.LeadingBusiness__c = 'ITB';
                lead1.OpportunityType__c='Standard';
                lead1.SuspectedValue__c = 1234;
                lead1.OpportunityFinder__c = 'abed';
                lead1.City__c = 'city1';
                lead1.Street__c = 'abced';
                lead1.ClassificationLevel1__c = 'Internal';
                lead1.ZipCode__c = '12345';
                //lead1.StateProvince__c = 
                
                
                if(math.mod(i,2) != 0)
                {
                   lead1.Account__c = con2.AccountId;
                   lead1.Contact__c = con2.id;
                }    
                else
                {
                    lead1.LeadContact__c = LC1.id;
                    
                }
                leads.add(lead1);   
            }
            System.Debug('The record in leads :'+leads);
          
            List<Id> leadsId = new List<Id>();
            
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            
            for(Database.SaveResult sr: leadsInsert)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                } 
                else
                   leadsId.add(sr.Id);
            }
            System.Debug('The lead ids are:'+leadsId);
        
           // leads.clear(); 
           // leads = [select Status, subStatus__c from lead where Id in: leadsId];
        // test.startTest();
        ///////////////////////
        
        lead led1 = [select id, Account__c, contact__c from lead where Account__c != null and Contact__c!= null and status='1 - New' and TECH_Ready2Convert__c='true' limit 1];
        System.Debug('The lead is:'+led1);
        
        List<LeadValueChainPlayer__c> lstLeadVCP = new list<LeadValueChainPlayer__c>();
        for(integer i=0; i<10;i++)
        {
                LeadValueChainPlayer__c LeadVCP1 = New LeadValueChainPlayer__c(Account__c=Acc.id,Lead__c=led1.id);

                if(math.mod(i,2)!=0)
                    LeadVCP1.Contact__c = Con1.id;
                else
                    LeadVCP1.Contact__c = Con2.id;
                
                lstLeadVCP.add(LeadVCP1);
         }
         Database.SaveResult[] leadVCPInsert= Database.insert(lstLeadVCP, true);
         
      System.Debug('The leads inserted into the database are:'+leadVCPInsert);
        Apexpages.Standardcontroller controller2 = new Apexpages.Standardcontroller(leads[1]);
        test.StartTest();
        Pagereference pg2 = Page.VFP_ConvertLead; 
        Test.setCurrentPage(pg2);
        
        VFC_ConvertLead vfcon2 = new VFC_ConvertLead(controller2);
        vfcon2.getLeadStatus();
        //vfcon2.getlstCompanyInfo();
        //vfcon2.getlstContactInfo();
        vfcon2.getReminderTimes(); 
        vfcon2.SourceLead.OwnerId = user1.Id; //[select id from user where isActive=true and profile.name='System Administrator' limit 1].id;
        vfcon2.donotcreatetask = true;
        vfcon2.NewTask.IsReminderSet = true;
        vfcon2.NewTask.TECH_ReminderDate__c = system.today()+1;
        vfcon2.ReminderTime = '11';
        vfcon2.OpportunityName = 'testopp2';
        vfcon2.strAccountId = led1.Account__c;
        //vfcon2.strContactId = '1';
        vfcon2.CheckContact();
        vfcon2.strContactId = led1.Contact__c;
        vfcon2.addTeamMember();  
        vfcon2.removeSelectedTeamMember();
        vfcon2.convertLead();
        test.StopTest();
         }//End of runas
           
    }//End of if
    
    }
    /*
     static testMethod void myUnitTest()
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
         // user.BypassWF__c = false;
         user.BypassVR__c = true;
         Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
         Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
         system.runas(user)
         {
            list<CS_LeadToOpportunity__c> lstLeadtoOpp =new   list<CS_LeadToOpportunity__c>();
            CS_LeadToOpportunity__c csLead1 = new CS_LeadToOpportunity__c(Name='Account',Source__c='Account',Destination__c='Account');
            lstLeadtoOpp.add(csLead1);
            Insert lstLeadtoOpp;
                       
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            
            Account acc1 = Utils_TestMethods.createAccount();
            insert acc1;
            
            System.Debug('Accounts created' +Acc); 
            System.Debug('Account 2 created is:'+acc1);
                      
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            Contact con2 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con2;
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            
            List<Lead> leads = new List<Lead>();

            for(Integer i=0;i<1;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                lead1.Company = 'a';
                lead1.firstname = 'j';
                lead1.Status = '1 - New';
                lead1.LeadingBusiness__c = 'ITB';
                lead1.OpportunityType__c='Standard';
                lead1.SuspectedValue__c = 1234;
                lead1.OpportunityFinder__c = 'abed';
                lead1.City__c = 'city1';
                lead1.Street__c = 'abced';
                lead1.ClassificationLevel1__c = 'Internal';
                lead1.ZipCode__c = '12345';
                //lead1.StateProvince__c = 
                
              
                if(math.mod(i,2) != 0)
                {
                   lead1.Account__c = con2.AccountId;
                   lead1.Contact__c = con2.id;
                }    
                else
                {
                    lead1.LeadContact__c = LC1.id;
                    
                }
               
                leads.add(lead1);   
            }
            System.Debug('The record in leads :'+leads);
          
            List<Id> leadsId = new List<Id>();
            
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            
            for(Database.SaveResult sr: leadsInsert)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                } 
                else
                   leadsId.add(sr.Id);
            }
            System.Debug('The lead ids are:'+leadsId);
        
           // leads.clear(); 
           // leads = [select Status, subStatus__c from lead where Id in: leadsId];
        // test.startTest();
        ///////////////////////
       
        lead led1 = [select id, Account__c, contact__c from lead where Account__c != null and Contact__c != null and status='1 - New' and TECH_Ready2Convert__c='true' limit 1];
        System.Debug('The lead is:'+led1);
        
        List<LeadValueChainPlayer__c> lstLeadVCP = new list<LeadValueChainPlayer__c>();
        for(integer i=0; i<10;i++)
        {
                LeadValueChainPlayer__c LeadVCP1 = New LeadValueChainPlayer__c(Account__c=Acc.id,Lead__c=led1.id);
 
                if(math.mod(i,2)!=0)
                    LeadVCP1.Contact__c = Con1.id;
                else
                    LeadVCP1.Contact__c = Con2.id;
                
                lstLeadVCP.add(LeadVCP1);
         }
         Database.SaveResult[] leadVCPInsert= Database.insert(lstLeadVCP, true);
         
      System.Debug('The leads inserted into the database are:'+leadVCPInsert);
            
       Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(led1);
        Pagereference pg1 = Page.VFP_ConvertLead;
        Test.setCurrentPage(pg1);
        
        VFC_ConvertLead vfcon1 = new VFC_ConvertLead(controller1);
        vfcon1.getLeadStatus();
        vfcon1.getlstCompanyInfo();
        vfcon1.getlstContactInfo();
        vfcon1.getReminderTimes();
        vfcon1.SourceLead.OwnerId = [select id from user where isActive=true and profile.name='System Administrator' limit 1].id;
        vfcon1.NewTask.IsReminderSet = true;
        vfcon1.NewTask.TECH_ReminderDate__c = system.today()+1;
        vfcon1.ReminderTime = '11';
        vfcon1.OpportunityName = 'testopp1';
        
        vfcon1.CheckContact();
        vfcon1.addTeamMember();  
        vfcon1.removeSelectedTeamMember();
       
         }//End of runas
           
    }//End of if
    
    }
    */
     static testMethod void myUnitTest1()
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
         User user1 = Utils_TestMethods.createStandardUser('78VFC92');
         Insert user1;
        // user.BypassWF__c = false;
         user.BypassVR__c = true;
         user.Champion__c = user1.Id;
         insert user;
         Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
         Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
         system.runas(user)
         {
            list<CS_LeadToOpportunity__c> lstLeadtoOpp =new   list<CS_LeadToOpportunity__c>();
            CS_LeadToOpportunity__c csLead1 = new CS_LeadToOpportunity__c(Name='Account',Source__c='Account',Destination__c='Account');
            lstLeadtoOpp.add(csLead1);
            Insert lstLeadtoOpp;
                       
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            
            System.Debug('Accounts created' +Acc); 
                      
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            Contact con2 = new Contact(LastName ='TestCOnt5',firstName = 'TestFirst5', AccountId=Acc.id, email = 'texttt@test.com', Country__c = country.id);
            insert con2;
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            
            List<Lead> leads = new List<Lead>();

            for(Integer i=0;i<=1;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                lead1.Company = 'a';
                lead1.firstname = 'j';
                lead1.Status = '1 - New';
                lead1.LeadingBusiness__c = 'ITB';
                lead1.OpportunityType__c='Standard';
                lead1.SuspectedValue__c = 1234;
                lead1.OpportunityFinder__c = 'abed';
                lead1.City__c = 'city1';
                lead1.Street__c = 'abced';
                lead1.ClassificationLevel1__c = 'Internal';
                lead1.ZipCode__c = '12345';
             
                if(math.mod(i,2) != 0)
                {
                   lead1.Account__c = con2.AccountId;
                   lead1.Contact__c = con2.id;
                }    
                else
                {
                    lead1.LeadContact__c = LC1.id;
                    
                }
                leads.add(lead1);   
            }
            System.Debug('The record in leads :'+leads);
          
            List<Id> leadsId = new List<Id>();
            
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            
            for(Database.SaveResult sr: leadsInsert)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                } 
                else
                   leadsId.add(sr.Id);
            }
       
           // leads.clear(); 
           // leads = [select Status, subStatus__c from lead where Id in: leadsId];
        // test.startTest();
        ///////////////////////
     
        lead led1 = [select id, Account__c, contact__c from lead where Account__c != null and Contact__c!= null and status='1 - New' and TECH_Ready2Convert__c='true' limit 1];
        List<LeadValueChainPlayer__c> lstLeadVCP = new list<LeadValueChainPlayer__c>();
        for(integer i=0; i<10;i++)
        {
                LeadValueChainPlayer__c LeadVCP1 = New LeadValueChainPlayer__c(Account__c=Acc.id,Lead__c=led1.id);

                if(math.mod(i,2)!=0)
                    LeadVCP1.Contact__c = Con1.id;
                else
                    LeadVCP1.Contact__c = Con2.id;
                
                lstLeadVCP.add(LeadVCP1);
         }
         Database.SaveResult[] leadVCPInsert= Database.insert(lstLeadVCP, true);
    
        Apexpages.Standardcontroller controller3 = new Apexpages.Standardcontroller(leads[1]);
        Pagereference pg3 = Page.VFP_ConvertLead; 
        Test.setCurrentPage(pg3);
        
        VFC_ConvertLead vfcon3 = new VFC_ConvertLead(controller3);
        test.StartTest();
            vfcon3.getLeadStatus();
            //vfcon3.getlstCompanyInfo();
            //vfcon3.getlstContactInfo();
            vfcon3.getReminderTimes(); 
            vfcon3.SourceLead.OwnerId = System.Label.CLMAY13MKT002; // [select id from user where isActive=true and profile.name='System Administrator' limit 1].id;
            System.debug('The ownerId is '+vfcon3.SourceLead.OwnerId);
            System.debug('The ownerId is '+System.Label.CLMAY13MKT002);
            vfcon3.donotcreatetask = true;
            vfcon3.NewTask.IsReminderSet = true;
            vfcon3.NewTask.TECH_ReminderDate__c = system.today()+1;
            vfcon3.ReminderTime = '11';
            vfcon3.OpportunityName = 'testopp2';
            vfcon3.CheckContact();
            vfcon3.strAccountId = Acc.Id;
            vfcon3.strContactId = con1.Id;
            vfcon3.bolCreateOpp = false;
        
        
            vfcon3.convertLead();
        test.StopTest();
         }//End of runas
           
    }//End of if
    
    }
    static testMethod void myUnitTest3()
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
            User user1 = Utils_TestMethods.createStandardUser('78VFC92');
            Insert user1;
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            user.BypassWF__c = false;
            //user.BypassVR__c = true;
            user.Champion__c = user1.Id;
            insert user;
         Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
         Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
         system.runas(user)
         {
            list<CS_LeadToOpportunity__c> lstLeadtoOpp =new   list<CS_LeadToOpportunity__c>();
            CS_LeadToOpportunity__c csLead1 = new CS_LeadToOpportunity__c(Name='Account',Source__c='Account',Destination__c='Account');
            lstLeadtoOpp.add(csLead1);
            Insert lstLeadtoOpp;
                       
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', City__c = 'city1', Country__c=country.id);
            insert Acc;
            
            System.Debug('Accounts created' +Acc); 
                      
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com'); //, Country__c = country.id, City__c = 'city1'
            System.debug('Contact Added --- '+con1);
            insert con1;
            
            Contact con2 = new Contact(LastName ='TestCOnt5',firstName = 'TestFirst5', AccountId=Acc.id, email = 'texttt@test.com'); //, Country__c = country.id
            System.debug('Contact Added --- '+con2);
             insert con2;
            
            Lead LC1 = Utils_TestMethods.createLead(null, country.Id,100);
            LC1.status = 'Active';
            LC1.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC1;
            
            Lead LC2 = Utils_TestMethods.createLead(null, country.Id,100);
            LC2.status = 'Active';
            LC2.RecordTypeId = mapRecordType.get('Lead Contact').getRecordTypeId();
            insert LC2;
            
            List<Lead> leads = new List<Lead>();

            for(Integer i=0;i<=1;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead')!= null)    
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                lead1.Company = 'TestAccount';
                lead1.firstname = 'TestCOnt';
                Lead1.lastName = 'TestFirst';
                lead1.Status = '1 - New';
                lead1.LeadingBusiness__c = 'ITB';
                lead1.OpportunityType__c='Standard';
                lead1.SuspectedValue__c = 1234;
                lead1.OpportunityFinder__c = 'abed';
                lead1.City__c = 'city1';
                lead1.Street__c = 'New Street';
                lead1.ClassificationLevel1__c = 'Internal';
                lead1.ZipCode__c = '12345';
                lead1.email = 'textt@test.com';
                leads.add(lead1);   
            }
            System.Debug('The record in leads :'+leads);
          
            List<Id> leadsId = new List<Id>();
            
            Database.SaveResult[] leadsInsert= Database.insert(leads, false);
            
            for(Database.SaveResult sr: leadsInsert)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                } 
                else
                   leadsId.add(sr.Id);
            }
       
           // leads.clear(); 
           // leads = [select Status, subStatus__c from lead where Id in: leadsId];
        // test.startTest();
        ///////////////////////
    
        Apexpages.Standardcontroller controller4 = new Apexpages.Standardcontroller(leads[0]);
        Pagereference pg3 = Page.VFP_ConvertLead; 
        Test.setCurrentPage(pg3);
        
        VFC_ConvertLead vfcon3 = new VFC_ConvertLead(controller4);
        test.StartTest();
            vfcon3.getLeadStatus();
            //vfcon3.getlstCompanyInfo();
            //vfcon3.getlstContactInfo();
            vfcon3.getReminderTimes(); 
            vfcon3.SourceLead.OwnerId = user1.Id; //[select id from user where isActive=true and profile.name='System Administrator' limit 1].id;
            System.debug('The ownerId is '+vfcon3.SourceLead.OwnerId);
            System.debug('The ownerId is '+System.Label.CLMAY13MKT002);
            vfcon3.donotcreatetask = false;
            vfcon3.NewTask.IsReminderSet = true;
            vfcon3.NewTask.TECH_ReminderDate__c = system.today()+1;
            vfcon3.ReminderTime = '11';
            vfcon3.OpportunityName = 'testopp2';
            vfcon3.strAccountId = '';
            vfcon3.strContactId = '';
            vfcon3.bolCreateOpp = false;
            vfcon3.CheckContact();
            vfcon3.strAccountId = Acc.Id;
            vfcon3.convertLead();
            vfcon3.contactSelected();
        test.StopTest();
         }//End of runas
           
    }//End of if
    
    }
    }