@isTest

private class FieloPRM_REST_PostTrainingTest{

    static testMethod void unitTest(){
        
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
        FieloEE.MockUpFactory.setCustomProperties(false);

        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'trainingName';
        member.FieloEE__FirstName__c = 'trainingFirstName';
        member.FieloEE__Street__c = 'tsst';
   
        
        insert member;
     
         Contact con1 = new Contact();
        con1.FirstName = 'conFirst';
        con1.LastName = 'conLast';
        con1.SEContactID__c = 'test';
        con1.PRMUIMSId__c = 'test';
        con1.FieloEE__Member__c = member.id;
        insert con1;
        
        FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
        badgeAux.name =  'testnane';                                      
        badgeAux.F_PRM_BadgeAPIName__c = 'test';
        badgeAux.F_PRM_Type__c = 'Training';
        insert badgeAux;
        
        FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
        badMemAux.FieloEE__Badge2__c = badgeAux.id;
        badMemAux.FieloEE__Member2__c = member.id ;
        badMemAux.F_PRM_FromDate__c = Date.Today(); 
        badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
        insert badMemAux;

          
        List<FieloPRM_REST_PostTraining.trainingWrapper> listWrapp = new List<FieloPRM_REST_PostTraining.trainingWrapper>();
        
        FieloPRM_REST_PostTraining.trainingWrapper wrapp = new FieloPRM_REST_PostTraining.trainingWrapper(); 
        wrapp.name = 'TestName';
        wrapp.uniqueCode = 'Test' ;
        wrapp.porcentage = 40;
        wrapp.dateFrom = Date.Today();
        wrapp.dateTo = wrapp.dateFrom.addDays(1);
        
        listWrapp.add(wrapp);
        
        Map<String,List<FieloPRM_REST_PostTraining.trainingWrapper>> mapContactExtIdTrainings = new Map<String,List<FieloPRM_REST_PostTraining.trainingWrapper>>();
        mapContactExtIdTrainings.put('test' , listWrapp );
        
        FieloPRM_REST_PostTraining rest = new FieloPRM_REST_PostTraining ();
        FieloPRM_REST_PostTraining.postTraining(mapContactExtIdTrainings);
        }
      
    }  
    
}