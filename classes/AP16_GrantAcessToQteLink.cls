/*
    Author          : Abhishek Yadav (ACN) 
    Date Created    : 18/05/2011
    Description     : Class utilised by triggers QuoteLinkAfterInsert.
*/

Public Class AP16_GrantAcessToQteLink{
    
    // Inner class
    class QuoteLinkException extends Exception {}
     
    Public static void grtAprEdtAcc(Map<ID,OPP_QuoteLink__c> newQuote)
    {
        System.Debug('******GrtAprEdtAcc method in GrantAcessToQteLink Class Start****');
        Map<String,Id> quoteApprover = new Map<String,Id>();
        Map<String,Id> quoteApproverParent = new Map<String,Id>();       
        set<string> quoteApprovers= new set<string>();
        List<OPP_QuoteLink__c> quoteLinkLogs = new List<OPP_QuoteLink__c>();
        List<ID> quoteLinkIds = new List<ID>();
        
        For(OPP_QuoteLink__c qteLink:newQuote.Values())
        {
             quoteApprovers.add(qteLink.Approver1__c);                                                             
             quoteApprovers.add(qteLink.Approver2__c);
             quoteApprovers.add(qteLink.Approver3__c);
             quoteApprovers.add(qteLink.Approver4__c);
             quoteApprovers.add(qteLink.Approver5__c);
             quoteApprovers.add(qteLink.Approver6__c);
             quoteApprovers.add(qteLink.Approver7__c);
             quoteApprovers.add(qteLink.Approver8__c);
             quoteApprovers.add(qteLink.Approver9__c);
             quoteApprovers.add(qteLink.Approver10__c);
             quoteLinkIds.add(qteLink.id);
             
             for(String app:quoteApprovers)
             {
                  if(app!=null)
                  {
                      quoteApprover.put(qteLink.id+app,app);
                      quoteApproverParent.put(qteLink.id+app,qteLink.id);
                  }
             }            
        }
        grntAcs(quoteApprover,quoteApproverParent);        
        quoteApprover.clear();
        quoteApproverParent.clear();
        
        if(quoteLinkIds.size() > 0){
          List<OPP_QuoteLink__c> quoteLinksToSave = new List<OPP_QuoteLink__c>();
          List<OPP_QuoteLink__c> quoteLinks = [select ID, Approver1__c, Approver2__c, Approver3__c, Approver4__c, Approver5__c, Approver6__c, Approver7__c, Approver8__c, Approver9__c, Approver10__c, TECH_GrantAcessToQteLinkDebug__c from OPP_QuoteLink__c where id in :quoteLinkIds];
          
          for(OPP_QuoteLink__c qteLink:quoteLinks){
            appendDebug('AP16_GrantAcessToQteLink.grtAprEdtAcc - Creation values of Approver1: '+qteLink.Approver1__c+', Approver2: '+qteLink.Approver2__c+', Approver3: '+qteLink.Approver3__c+', Approver4: '+qteLink.Approver4__c+', Approver5: '+qteLink.Approver5__c+', Approver6: '+qteLink.Approver6__c+', Approver7: '+qteLink.Approver7__c+', Approver8: '+qteLink.Approver8__c+', Approver9: '+qteLink.Approver9__c+', Approver10: '+qteLink.Approver10__c, qteLink); 
            quoteLinksToSave.add(qteLink);
          }   
          saveQuoteLinkLogs(quoteLinksToSave);
        }
        System.Debug('******GrtAprEdtAcc method in GrantAcessToQteLink Class Start****');        
      }

     Public Static Void updtAprAcc(Map<ID,OPP_QuoteLink__c> newQuote,Map<ID,OPP_QuoteLink__c> oldQuote)    
      {
          system.debug('oldQuote'+oldQuote);
          Map<String,Id> quoteApprover = new Map<String,Id>();
          Map<String,Id> quoteApproverParent = new Map<String,Id>();
          Map<String,Id> oldQuoteLinkApprover= new Map<String,Id>();
          set<string> newquoteApprovers= new set<string>();
          set<string> quoteApprovers= new set<string>();
          set<String> commonApprovers= new set<String>();
          set<String> commomDelegatedApprovers= new set<String>(); 
          List<ID> quoteLinkIds = new List<ID>();
          //Upated to resolve the Approver issue
          set<Id> setParentID = new set<ID>();
              
        for(OPP_QuoteLink__c qteLink:newQuote.Values())
        {
             newQuoteApprovers.add(qteLink.Approver1__c);                                                             
             newQuoteApprovers.add(qteLink.Approver2__c);
             newQuoteApprovers.add(qteLink.Approver3__c);
             newQuoteApprovers.add(qteLink.Approver4__c);
             newQuoteApprovers.add(qteLink.Approver5__c);
             newQuoteApprovers.add(qteLink.Approver6__c);
             newQuoteApprovers.add(qteLink.Approver7__c);
             newQuoteApprovers.add(qteLink.Approver8__c);
             newQuoteApprovers.add(qteLink.Approver9__c);
             newQuoteApprovers.add(qteLink.Approver10__c);
             quoteLinkIds.add(qteLink.id); 
             
             for(String app:newQuoteApprovers)
             {
                  if(app!=null)
                  {
                      quoteApprover.put(qteLink.id+app,app);
                      quoteApproverParent.put(qteLink.id+app,qteLink.id);
                  }
             }             
        }
        
        for(OPP_QuoteLink__c qteLink:oldQuote.Values())
        {
             quoteApprovers.add(qteLink.Approver1__c);                                                             
             quoteApprovers.add(qteLink.Approver2__c);
             quoteApprovers.add(qteLink.Approver3__c);
             quoteApprovers.add(qteLink.Approver4__c);
             quoteApprovers.add(qteLink.Approver5__c);
             quoteApprovers.add(qteLink.Approver6__c);
             quoteApprovers.add(qteLink.Approver7__c);
             quoteApprovers.add(qteLink.Approver8__c);
             quoteApprovers.add(qteLink.Approver9__c);
             quoteApprovers.add(qteLink.Approver10__c);
             
              for(String app:quoteApprovers)
              {
                  if(app!=null)
                  {
                      oldQuoteLinkApprover.put(qteLink.id+app,app);
                          
                      if(!setParentID.contains(qteLink.id))
                          setParentID.add(qteLink.id);                              
                  }
              }             
        }
      
        for(String newQuoteUser:quoteApprover.keySet())
          {
              if(oldQuoteLinkApprover.keyset().contains(newQuoteUser))
              {
                   oldQuoteLinkApprover.remove(newQuoteUser);  
                   commonApprovers.add(newQuoteUser);      
                   commomDelegatedApprovers.add(quoteApprover.get(newQuoteUser));
              }              
          }   
    
        for(String newQuoteUser:commonApprovers)
          {
               quoteApprover.keySet().remove(newQuoteUser);         
               quoteApproverParent.keySet().remove(newQuoteUser);
          }                              
      if(oldQuoteLinkApprover.size()>0)
      {  
          revokeAcs(oldQuoteLinkApprover,commomDelegatedApprovers,setParentID);            
          oldQuoteLinkApprover.clear();
      } 
      if(quoteApprover.size()>0)
      {
          grntAcs(quoteApprover,quoteApproverParent);
          quoteApprover.clear();
          quoteApproverParent.clear();    
          commonApprovers.clear();      
      }  
      
       if(quoteLinkIds.size() > 0){
          List<OPP_QuoteLink__c> quoteLinksToSave = new List<OPP_QuoteLink__c>();
          List<OPP_QuoteLink__c> quoteLinks = [select ID, Approver1__c, Approver2__c, Approver3__c, Approver4__c, Approver5__c, Approver6__c, Approver7__c, Approver8__c, Approver9__c, Approver10__c, TECH_GrantAcessToQteLinkDebug__c from OPP_QuoteLink__c where id in :quoteLinkIds];
          
          for(OPP_QuoteLink__c qteLink:quoteLinks){
            appendDebug('AP16_GrantAcessToQteLink.updtAprAccmethod - Updated values of Approver1: '+qteLink.Approver1__c+', Approver2: '+qteLink.Approver2__c+', Approver3: '+qteLink.Approver3__c+', Approver4: '+qteLink.Approver4__c+', Approver5: '+qteLink.Approver5__c+', Approver6: '+qteLink.Approver6__c+', Approver7: '+qteLink.Approver7__c+', Approver8: '+qteLink.Approver8__c+', Approver9: '+qteLink.Approver9__c+', Approver10: '+qteLink.Approver10__c, qteLink); 
            quoteLinksToSave.add(qteLink);
          }   
          saveQuoteLinkLogs(quoteLinksToSave);
        }    
      
      }
     
     /*Method to remove Access from an Approver*/
      public static void revokeAcs( Map<String,Id> oldApprover,set<String> commomDelegatedApprovers,set<Id> setParentId)  
      {
      System.Debug('******revokeAcs method in GrantAcessToQteLink Class Start****');       
          
       list<OPP_QuoteLink__Share> deleteShareUser=[select ParentId,UserOrGroupId,AccessLevel,RowCause from OPP_QuoteLink__Share where UserOrGroupId IN:oldApprover.values() and parentid in :setParentId and RowCause ='GrantAccessToApprover__c'];                
      /*--Modified by Abhishek 0n 30/05/2011--*/
      set<id> delgtUser= new set<id>();
      list<user> deleteDelgtUser=[select DelegatedApproverID,name from User where id in:oldApprover.values()];      
      Map<String,id> deleteDelgtAppr= new Map<String,id>();
      Map<String,id> deleteDelgtApprParent= new Map<String,id>();
      for(user users:deleteDelgtUser)
      {
          delgtUser.add(users.DelegatedApproverID);
      }
      List<OPP_QuoteLink__Share> deleteDelgtApprFrmShare = [select ParentId,UserOrGroupId,AccessLevel,RowCause from OPP_QuoteLink__Share where UserOrGroupId IN:delgtUser and RowCause ='GrantAccessToDelegatedApprover__c'];
      for(OPP_QuoteLink__Share deleteDelgt:deleteDelgtApprFrmShare)
      {
          deleteDelgtAppr.put(string.valueof(deleteDelgt.ParentId)+string.valueof(deleteDelgt.UserOrGroupId),deleteDelgt.UserOrGroupId);
          deleteDelgtApprParent.put(string.valueof(deleteDelgt.ParentId)+string.valueof(deleteDelgt.UserOrGroupId),deleteDelgt.ParentId);
      }
      system.debug('deleteDelgtApprFrmShare'+deleteDelgtApprFrmShare);
      
      list<user> existingDelgtApprFrmShare = [select DelegatedApproverID,name from User where id in:commomDelegatedApprovers]; 
      set<id> existingDelgtAppr = new set<id>();
      for(user exst:existingDelgtApprFrmShare)
      {
              if(exst.DelegatedApproverID!=null)
              {
              existingDelgtAppr.add(exst.DelegatedApproverID); 
              system.debug('existingDelgtAppr'+existingDelgtAppr); 
              }
      }
      List<OPP_QuoteLink__Share> delgtApprFrmShare = [select ParentId,UserOrGroupId,AccessLevel,RowCause from OPP_QuoteLink__Share where UserOrGroupId IN:existingDelgtAppr and RowCause ='GrantAccessToDelegatedApprover__c'];
      system.debug('delgtApprFrmShare'+delgtApprFrmShare);  
      for(OPP_QuoteLink__Share deleteDelgt:delgtApprFrmShare)
      {
          if(deleteDelgtAppr.keyset().contains(string.valueof(deleteDelgt.ParentId)+string.valueof(deleteDelgt.UserOrGroupId)))
          {
             deleteDelgtAppr.keyset().remove(string.valueof(deleteDelgt.ParentId)+string.valueof(deleteDelgt.UserOrGroupId)); 
             deleteDelgtApprParent.keyset().remove(string.valueof(deleteDelgt.ParentId)+string.valueof(deleteDelgt.UserOrGroupId));  
          }  
      }
      system.debug('deleteDelgtAppr'+deleteDelgtAppr); 
      List<OPP_QuoteLink__Share> finalDeletedelgtApprFrmShare= [select ParentId,UserOrGroupId,AccessLevel,RowCause from OPP_QuoteLink__Share where UserOrGroupId IN:deleteDelgtAppr.values() and parentID in:deleteDelgtApprParent.values() and RowCause ='GrantAccessToDelegatedApprover__c'];         
      if(finalDeletedelgtApprFrmShare.Size() > 0)
      {
          deleteShareUser.addAll(finalDeletedelgtApprFrmShare);
      }
     if(deleteShareUser.Size() > 0)
        {
            try{
            
            if(deleteShareUser.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP16 Error updating : '+ Label.CL00264);
                throw new QuoteLinkException('Error when updating : '+ Label.CL00264); 
            }
            else
            {
            Database.deleteResult[] lsr = Database.delete(deleteShareUser,false);
                for( Database.deleteResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];               
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                        err.getMessage().contains('AccessLevel'))
                        { 
                        system.debug('Success *Trivial Access*');
                        } 
                        else{
                        system.debug('failed * Event share*'); 
                        } 
                        throw new QuoteLinkException('Error when saving QuoteShare: '+err.getMessage());  
                    } 
                }
            }
            
            }catch(Exception e){
            
              List<OPP_QuoteLink__c> quoteLinks = new List<OPP_QuoteLink__c>();
              for(OPP_QuoteLink__Share sharing:deleteShareUser){ 
                  OPP_QuoteLink__c aQuoteLink = [select ID, TECH_GrantAcessToQteLinkDebug__c from OPP_QuoteLink__c where id = :sharing.parentID];
                  appendDebug('Exception in AP16_GrantAcessToQteLink.revokeAcs method: '+e.getMessage(), aQuoteLink); 
                  quoteLinks.add(aQuoteLink); 
               }
               saveQuoteLinkLogs(quoteLinks);
            } 
        }      
      system.debug('finalDeletedelgtApprFrmShare'+finalDeletedelgtApprFrmShare); 
      System.Debug('******revokeAcs method in GrantAcessToQteLink Class End****');
      }

     /*Method to grant Access to an Approver*/  
    public static void grntAcs(Map<String,ID> quoteLink, Map<String,ID> quoteLinkParent)  
    {
    System.Debug('******grntAcs method in GrantAcessToQteLink Class Start****');  
               
    list<OPP_QuoteLink__Share> quoteShare= new list<OPP_QuoteLink__Share>();            
    list<OPP_QuoteLink__Share> shareUser = [select ParentId,UserOrGroupId,AccessLevel,RowCause from OPP_QuoteLink__Share where ParentId IN:quoteLinkParent.values() and RowCause='GrantAccessToApprover__c'];           
    set<String> salesUser= new set<String>();
    List<user> user=[select id,ProfileId,Permission_sets_assigned__c from user where id in:quotelink.values() and (ProfileId=:Label.CLJUL13SLS01 or profileID =:label.CL00419)];// and profileID =:label.CL00419];
    user=Utils_Methods.filterUsers(user,new Set<String>{System.Label.CLJUL13SLS02},Label.CLJUL13SLS01,Label.CL00419);
    for(User users:user)
    {
        salesUser.add(users.id);        
    }
    for(String ID:quotelink.keyset())  
    {
        OPP_QuoteLink__Share newApproverAccess= new OPP_QuoteLink__Share();
        newapproverAccess.UserOrGroupId=quoteLink.get(Id);
        newapproverAccess.ParentID=quoteLinkParent.get(ID);
        if(salesUser.contains(quoteLink.get(Id)))
        {
            newapproverAccess.AccessLevel = label.CL00418;
        }
        else
        {
            newapproverAccess.AccessLevel = Label.CL00203;
        }
        newapproverAccess.RowCause = Schema.OPP_QuoteLink__Share.RowCause.GrantAccessToApprover__c;
        quoteShare.add(newapproverAccess);
    }
    
     /* modified on 30/05/2010 delegated Approver*/              
    list<user> delegateApprover=[select DelegatedApproverID,name,profileID from User where id in:quoteLink.values()];               
    list<OPP_QuoteLink__Share> newquoteShare= new list<OPP_QuoteLink__Share>(); 
    Set<String> delegatedSalesUser= new Set<String>();
    Set<String> delegatedSalesPerson=new Set<String>();
    for(User users:delegateApprover)
    {
        delegatedSalesUser.add(users.DelegatedApproverID);
    }
    List<user> delegateUser=[select id,ProfileId,Permission_sets_assigned__c from user where id in:delegatedSalesUser and (ProfileId=:Label.CLJUL13SLS01 or profileID =:label.CL00419)];// and profileID =:label.CL00419];
    delegateUser=Utils_Methods.filterUsers(user,new Set<String>{System.Label.CLJUL13SLS02},Label.CLJUL13SLS01,Label.CL00419);
    for(User users:delegateUser)
    {
        delegatedSalesPerson.add(users.id);
    }
    for(OPP_QuoteLink__Share qteshare:quoteShare)
    {
        for(user users:delegateApprover)
        {
            if(users.id==qteshare.UserOrGroupId)
            {
                if(users.DelegatedApproverID!=null)
                {
                    OPP_QuoteLink__Share newDelegateApproverAccess= new OPP_QuoteLink__Share();
                    newDelegateApproverAccess.UserOrGroupId=users.DelegatedApproverID;
                    newDelegateApproverAccess.ParentID=qteShare.parentId;
                    if(delegatedSalesPerson.contains(users.DelegatedApproverID))
                    {
                        newDelegateApproverAccess.AccessLevel = label.CL00418;
                    }
                    else
                    {
                        newDelegateApproverAccess.AccessLevel = Label.CL00203;
                    }
                    newDelegateApproverAccess.RowCause = Schema.OPP_QuoteLink__Share.RowCause.GrantAccessToDelegatedApprover__c;
                    newquoteShare.add(newDelegateApproverAccess);
                }
            }
        }
    }
    if(newquoteShare.size()>0)
    {
       quoteShare.addAll(newquoteShare);
    }
    if(quoteShare.Size() > 0)
    {
        try{
        
        if(quoteShare.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
        {
            System.Debug('######## AP16 Error updating : '+ Label.CL00264);
            throw new QuoteLinkException('Error when updating : '+ Label.CL00264);
        }
        else
        {
            Database.SaveResult[] lsr = Database.insert(quoteShare,false);
            for( Database.SaveResult triggersr : lsr) 
            {
                if(triggersr.isSuccess())
                {  
                    system.debug('success Event share'); 
                }
                else
                {  
                    Database.Error err = triggersr.getErrors()[0];               
                    if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                    err.getMessage().contains('AccessLevel')){ 
                        system.debug('Success *Trivial Access*');

                    }else{
                        system.debug('failed * Event share*'); 
                    } 
                    throw new QuoteLinkException('Error when saving QuoteShare: '+err.getMessage());           
                } 
            }
        }
        
        }catch(Exception e){

           List<OPP_QuoteLink__c> quoteLinks = new List<OPP_QuoteLink__c>();
              for(OPP_QuoteLink__Share sharing:quoteShare){ 
                  OPP_QuoteLink__c aQuoteLink = [select ID, TECH_GrantAcessToQteLinkDebug__c from OPP_QuoteLink__c where id = :sharing.parentID];
                  appendDebug('Exception in AP16_GrantAcessToQteLink.grntAcs method: '+e.getMessage(), aQuoteLink); 
                  quoteLinks.add(aQuoteLink); 
               }
               saveQuoteLinkLogs(quoteLinks);
        }        
    }    
    quoteLink.clear();
    quoteLinkParent.clear();
    System.Debug('******grntAcs method in GrantAcessToQteLink Class End****');
    }
    
    public static void appendDebug(String debugLog, OPP_QuoteLink__c aQuoteLink)
    {
        //OPP_QuoteLink__c aQuoteLink = [select ID, TECH_GrantAcessToQteLinkDebug__c from OPP_QuoteLink__c where id = :id];
        
        if(aQuoteLink!= null)
        {
            if (aQuoteLink.TECH_GrantAcessToQteLinkDebug__c== null) {
                aQuoteLink.TECH_GrantAcessToQteLinkDebug__c= System.now().format('yyyy-MM-dd HH:mm:ss.SSS Z')+' - '+debugLog;
            }
            else {
                aQuoteLink.TECH_GrantAcessToQteLinkDebug__c+= '\n' + System.now().format('yyyy-MM-dd HH:mm:ss.SSS Z')+' - '+debugLog;
                aQuoteLink.TECH_GrantAcessToQteLinkDebug__c= TruncateString(aQuoteLink.TECH_GrantAcessToQteLinkDebug__c,Integer.valueOf(Label.CL00619));
            }
        }
       
    }
    
     public static void saveQuoteLinkLogs(List<OPP_QuoteLink__c> aQuoteLink) {
          // Update the QuoteLink to save the logs
          try {
              DataBase.update(aQuoteLink);
          }
          catch (Exception e) {
              System.debug('----- QuoteLink could not be updated: '+e.getMessage());
          }
    } 
    
     public static String TruncateString(String aString, Integer aNbrOfCharacters)
       {
           String truncatedString='';
           
           if(aString!=null && aNbrOfCharacters!=null)
           {
               if(aNbrOfCharacters>0)
               {
                   // If Plain Text Body is too long, truncate the value 
                   if(aString.length() > aNbrOfCharacters)
                   {
                       truncatedString = aString.substring(0,aNbrOfCharacters);
                   }
                   else
                   {
                        truncatedString = aString;
                   } 
               }  
           }
           
           return truncatedString;     
       } 
       
       /* Method added for BR-8607 (Mar 2016) by Uttara
          Purpose : Grant access to Support Request Members (PM tender , Assigned To - User) */
       public static void SupportRequestSharing(Map<String,Set<String>> mapQLsharedToUsers) {
           
           System.debug('!!!! mapQLsharedToUsers : ' + mapQLsharedToUsers);
           
           List<OPP_QuoteLink__Share> SRshare = new List<OPP_QuoteLink__Share>();
           
           for(String QLid : mapQLsharedToUsers.keySet())  {                              
               for(String userid : mapQLsharedToUsers.get(QLid)) {
                   OPP_QuoteLink__Share singleSRshare = new OPP_QuoteLink__Share();
                   singleSRshare.ParentId = QLid;
                   singleSRshare.AccessLevel = 'Edit';
                   singleSRshare.RowCause = Schema.OPP_QuoteLink__Share.RowCause.GrantAccessToSupportRequestMembers__c;
                   singleSRshare.UserOrGroupId = userid;  
                   SRshare.add(singleSRshare);
               }
                 
           }  
           Database.insert(SRshare,true);       
       }  
}