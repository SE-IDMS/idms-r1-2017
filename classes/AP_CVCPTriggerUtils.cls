/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 2 July 2012
    Modified Date: 06- Sep 2012
    Modified By   : Vimal Karunakaran
    Description  : 1. populateAccountFromContact()
                       (a) populate the Account Id based on Contact Id
                  
***************************************************************************************************************************/
public class AP_CVCPTriggerUtils{
    public static void populateAccountFromContact(list<CTR_ValueChainPlayers__c> lstCVCP){
        Set<Id> contactId = new Set<Id>();
        for(CTR_ValueChainPlayers__c cvcp : lstCVCP){
            if(cvcp.Contact__c !=null){
                contactId.add(cvcp.Contact__c);
            }
        }
        if(contactId.size()>0){
            Map<Id,Contact> mapContactAccount =new Map<Id,Contact>([Select id,AccountId from Contact where Id in: contactId]);
            for(CTR_ValueChainPlayers__c cvcp : lstCVCP){
                if(mapContactAccount.containsKey(cvcp.Contact__c)){
                    cvcp.Account__c = mapContactAccount.get(cvcp.Contact__c).AccountId;
                }
            }
        }
    }  
}