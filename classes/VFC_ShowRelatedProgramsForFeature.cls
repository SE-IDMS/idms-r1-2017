public class VFC_ShowRelatedProgramsForFeature {
    Public ContactAssignedFeature__c caf {get;set;} {caf =new ContactAssignedFeature__c();}    
    public List<ProgramLevel__c> lstRelatedLevels {get; set;} {lstRelatedLevels = new List<ProgramLevel__c>();}
    
    public VFC_ShowRelatedProgramsForFeature(ApexPages.StandardController cont) {
        
        if(!Test.isRunningTest())
        {
            cont.addFields(new List<String> {'Contact__C','FeatureCatalog__C', 'Active__c' });
        }                        
        caf = (ContactAssignedFeature__c )cont.getRecord();
        
        List<ContactAssignedProgram__c> lstCAP = [Select Id, PartnerProgram__c, Contact__C, ProgramLevel__C , Active__C 
            From ContactAssignedProgram__c WHERE Active__c = true AND Contact__C=:caf.contact__c];
        set<id> lvlIds = new set<id>();
        for(ContactAssignedProgram__c cap : lstCAP)
        {
            lvlIds.add(cap.ProgramLevel__C);
        }
        //To show only assocoaied levels to this contact
        //List<ProgramFeature__c> lstProgFea = [Select Id, ProgramLevel__c, PartnerProgram__c 
        //    FROM ProgramFeature__c WHERE ProgramLevel__c in:lvlIds AND FeatureCatalog__c=:caf.FeatureCatalog__c AND FeatureStatus__c = 'Active'];
        
        //To Show all levels in system
        List<ProgramFeature__c> lstProgFea = [Select Id, ProgramLevel__c, PartnerProgram__c 
            FROM ProgramFeature__c WHERE FeatureCatalog__c=:caf.FeatureCatalog__c AND FeatureStatus__c = 'Active'];
        
        
        set<id> prgLvlIds = new set<id>();    
        for(ProgramFeature__c pf : lstProgFea )
        {
            prgLvlIds.add(pf.ProgramLevel__c);
        }
        
        lstRelatedLevels = [Select Name, PartnerProgram__C, LevelStatus__C, RecordTypeId FROM ProgramLevel__c WHERE id IN:prgLvlIds and RecordTypeId=:System.Label.CLMAY13PRM03];
    }
    
    
}