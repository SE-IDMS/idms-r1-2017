/*
Srinivas Nallapati	30-Oct-2013		Nov13 PRM Release	
*/
global class Assessment
{
    
    // bFO ID of Assessment
    WebService string ID { get; set; }
    
    // Name of the Assessment
    WebService string NAME { get; set; }
    
    // Description of the Assessment
    WebService string DESCRIPTION { get; set; }

    // Maximum Score of the Assessment
    WebService string MAXIMUMSCORE { get; set; }

    // Program of the Assessment
    WebService string PROGRAM { get; set; }

    // Program Level of the Assessment
    WebService string PROGRAMLEVEL { get; set; }

    // URL of the Assessment
    WebService string URL { get; set; }
    
    // LastUpdatetdDate timestamp in bFO
    WebService Datetime SDH_VERSION { get; set; }
    
    public Assessment() {
        
    }
}