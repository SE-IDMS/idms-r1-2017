/*
created as part of connectors for November release

Modefied on 10 feb 2014 for EUS#032552

*/
global class AP_LocationManager{

    global static string STANDARD_LOCATION = Label.CLNOV13SRV01;
    global static string SE_READONLYSITE = Label.CLNOV13SRV02;    
    global static string SITESUFFIX = Label.CLFEB14SRV01;  
    global static string SPACESUFFIX = ' '; 
    
    global static void ProcessLocations(List<Sobject> scope)
    {      
        Map<Id,List<SVMXC__Site__c>> accountLocationmap=new Map<Id,List<SVMXC__Site__c>>();
        Map<Id,SVMXC__Site__c> accountPrimaryLocationmap=new Map<Id,SVMXC__Site__c>();
        Map<Id,Account> accountInformation=new Map<Id,Account>();

        Map<Id,SVMXC__Site__c> newPrimaryLocations=new Map<Id,SVMXC__Site__c>();
        Set<id> parentSiteset = new Set<id>();
        List<SVMXC__Site__c> childSiteWithParent = new List<SVMXC__Site__c>();

        for(Sobject sobjectRecord:scope)
        {
            SVMXC__Site__c siteRecord=(SVMXC__Site__c)sobjectRecord;
            //sobjectRecord.put('TECH_LocSendToBatch__c',false);
            siteRecord.TECH_LocSendToBatch__c=false;
            if(siteRecord.SVMXC__Parent__c==null && (siteRecord.RecordTypeId == SE_READONLYSITE || siteRecord.RecordTypeId == STANDARD_LOCATION) && siteRecord.SVMXC__Account__c!=null){            
                if(accountLocationmap.containsKey(siteRecord.SVMXC__Account__c))
                    accountLocationmap.get(siteRecord.SVMXC__Account__c).add(siteRecord);
                else
                    accountLocationmap.put(siteRecord.SVMXC__Account__c,new List<SVMXC__Site__c>{siteRecord});              
            } 
            // New code Started for EUS#032552
            else if(siteRecord.SVMXC__Parent__c !=null && (siteRecord.RecordTypeId == SE_READONLYSITE || siteRecord.RecordTypeId == STANDARD_LOCATION) && siteRecord.SVMXC__Account__c!=null){
                parentSiteset.add(siteRecord.SVMXC__Parent__c);
                childSiteWithParent.add(siteRecord);
            }
            // New code End for EUS#032552
        }

        System.debug('accountLocationmap'+accountLocationmap);
        if(accountLocationmap.size()>0){
            for(SVMXC__Site__c siteRecord:[select id,SVMXC__Account__c,PrimaryLocation__c from SVMXC__Site__c where  SVMXC__Account__c in :accountLocationmap.keySet() and PrimaryLocation__c=true])
            { 
                accountPrimaryLocationmap.put(siteRecord.SVMXC__Account__c,siteRecord);                                       
            }

            accountInformation=new Map<Id,Account>([Select id, Name,Street__c,StateProvince__c,ZipCode__c,Country__c,City__c from Account where id in :accountLocationmap.keySet()]);        

            for(Id accountId:accountLocationmap.keySet())
            {            
                if(accountPrimaryLocationmap.containsKey(accountId))
                {
                //primary
                    System.debug('Processing the locations without parent for an account that already has a Primary location');
                    for(SVMXC__Site__c siteRecord:accountLocationmap.get(accountId))
                    {

                        /* EUS 47747 Start - A primary location should not have a parent - Otherwise, its parent would be itself and this will create a circular reference prenventing the update to complete */
                        if (siteRecord.PrimaryLocation__c!=true) {
                            siteRecord.SVMXC__Parent__c=accountPrimaryLocationmap.get(accountId).id;
                        }
                        /* EUS 47747 End*/
                        if(accountInformation.containskey(accountId))
                        {                      
                            Account accountRecord=accountInformation.get(accountId);
                            if(accountRecord.StateProvince__c != null)
                            siteRecord.StateProvince__c=accountRecord.StateProvince__c;
                            if(accountRecord.ZipCode__c != null)
                            siteRecord.SVMXC__Zip__c=accountRecord.ZipCode__c;
                            if(accountRecord.Street__c != null)
                            siteRecord.SVMXC__Street__c=accountRecord.Street__c;
                            if(accountRecord.City__c != null)
                            siteRecord.SVMXC__City__c=accountRecord.City__c;
                            if(accountRecord.Country__c!= null)
                            siteRecord.LocationCountry__c=accountRecord.Country__c;  
                        }                  
                    }                    
                }
                else
                {
                //we go for creation                              
                    if(accountInformation.containsKey(accountId))
                    {
                        Account accountRecord=accountInformation.get(accountId);
                        string accnametmp = accountRecord.Name; 
                        if (accnametmp.length() > 75){
                            accnametmp= accnametmp.substring(0, 75);
                        }
                        SVMXC__Site__c newsiterecord=new SVMXC__Site__c(Name=accnametmp+SPACESUFFIX+SITESUFFIX,RecordTypeId=STANDARD_LOCATION,SVMXC__Location_Type__c='Site',PrimaryLocation__c=true,StateProvince__c=accountRecord.StateProvince__c,SVMXC__Zip__c=accountRecord.ZipCode__c,SVMXC__Street__c=accountRecord.Street__c,SVMXC__City__c=accountRecord.City__c,LocationCountry__c=accountRecord.Country__c,SVMXC__Account__c=accountRecord.id);
                        newPrimaryLocations.put(accountId,newsiterecord);                    
                    }

                }

            }

        //insert the new primary records     
            for(Database.SaveResult sr:Database.insert(newPrimaryLocations.values(),false))
            {
                if(!sr.isSuccess())
                    System.debug('*****************\nERROR\n Id:'+sr.getId()+'\nMessage :'+sr.getErrors()[0].message+'\n*****************');
                else
                {
                }
            }
            System.debug('newprimaryLocations.values()'+newPrimaryLocations.values());
            for(Id accountId:newPrimaryLocations.keySet())
            {
                if(accountLocationmap.containsKey(accountId))
                {
                    for(SVMXC__Site__c siteRecord:accountLocationmap.get(accountId))
                    {                                        
                        siteRecord.SVMXC__Parent__c=newPrimaryLocations.get(accountId).id;
                        // New code Started for EUS#032552
                        if(newPrimaryLocations.get(accountId).StateProvince__c != null)
                        siteRecord.StateProvince__c=newPrimaryLocations.get(accountId).StateProvince__c;
                        if(newPrimaryLocations.get(accountId).SVMXC__Zip__c != null)
                        siteRecord.SVMXC__Zip__c=newPrimaryLocations.get(accountId).SVMXC__Zip__c;
                        if(newPrimaryLocations.get(accountId).SVMXC__Street__c != null)
                        siteRecord.SVMXC__Street__c=newPrimaryLocations.get(accountId).SVMXC__Street__c;
                        if(newPrimaryLocations.get(accountId).SVMXC__City__c != null)
                        siteRecord.SVMXC__City__c=newPrimaryLocations.get(accountId).SVMXC__City__c;
                        if(newPrimaryLocations.get(accountId).LocationCountry__c!= null)
                        siteRecord.LocationCountry__c=newPrimaryLocations.get(accountId).LocationCountry__c; 
                        //New code End for EUS#032552
                    }
                }            
            }
        }  
        // New code Started for EUS#032552
        if(childSiteWithParent != null && childSiteWithParent.size()>0)
        {
            Map<id, SVMXC__Site__c> pidRecMap = new Map<id,SVMXC__Site__c>();
             
                List<SVMXC__Site__c> parentList = [select id, StateProvince__c,SVMXC__Zip__c,SVMXC__Street__c,SVMXC__City__c,LocationCountry__c from SVMXC__Site__c where id in : parentSiteset];
                pidRecMap.putAll(parentList);
                for(SVMXC__Site__c obj:childSiteWithParent){
                
                    if(pidRecMap.containsKey(obj.SVMXC__Parent__c))
                    {
                        
                        SVMXC__Site__c pObj = pidRecMap.get(obj.SVMXC__Parent__c);
                        if(obj.StateProvince__c == null)
                        {
                            if(pObj.StateProvince__c != null)
                            obj.StateProvince__c = pObj.StateProvince__c;
                        }
                        if(obj.SVMXC__Zip__c == null)
                        {
                            if(pObj.SVMXC__Zip__c != null)
                            obj.SVMXC__Zip__c = pObj.SVMXC__Zip__c;
                        }
                        if(obj.SVMXC__Street__c == null)
                        {
                            if(pObj.SVMXC__Street__c != null)
                            obj.SVMXC__Street__c = pObj.SVMXC__Street__c;
                        }
                        if(obj.SVMXC__City__c == null)
                        {
                            if(pObj.SVMXC__City__c != null)
                            obj.SVMXC__City__c = pObj.SVMXC__City__c;
                        }
                        if(obj.LocationCountry__c == null)
                        {
                            if(pObj.LocationCountry__c != null)
                            obj.LocationCountry__c = pObj.LocationCountry__c;
                        }
                       // obj.TECH_LocSendToBatch__c=false;
                    }
                
                }
                
               // update childSiteWithParent;
                 for(Database.SaveResult sr:Database.update(childSiteWithParent,false))
                {
                    if(!sr.isSuccess())
                        System.debug('*****************\nERROR\n Id:'+sr.getId()+'\nMessage :'+sr.getErrors()[0].message+'\n*****************');
                    else
                    {
                    }
                }
        
        }
        // New code End for EUS#032552
    }

    // Check GoldenLoctionId duplicates
    Private static void checkLocationGoldenId(List<SVMXC__Site__c> newSites)
    {
       Map<String,SVMXC__Site__c> mapGoldenLocationIdtoLocation=new Map<String,SVMXC__Site__c>();
     //Integer remainingRecordsforLimit=Limits.getLimitQueryRows()-Limits.getQueryRows();
       for(SVMXC__Site__c SiteRecord:newSites)
       {    
         if(SiteRecord.GoldenLocationId__c != null && SiteRecord.GoldenLocationId__c.trim() != ''){
             system.debug('### RHA enter map locations');
             mapGoldenLocationIdtoLocation.put(SiteRecord.GoldenLocationId__c,SiteRecord);
         }

     }    
        for(SVMXC__Site__c existingLocationrecord :[select id,name,GoldenLocationId__c from SVMXC__Site__c where GoldenLocationId__c IN :mapGoldenLocationIdtoLocation.keySet()])// limit :remainingRecordsforLimit])
{           
    if(mapGoldenLocationIdtoLocation.containsKey(existingLocationrecord.GoldenLocationId__c))
        mapGoldenLocationIdtoLocation.get(existingLocationrecord.GoldenLocationId__c).addError(Label.CLNOV13SRV12);            
}      
}

    //Trigger related function to checkLocationGoldenId
public static void beforeHandler(List<SVMXC__Site__c> newSites, Map<Id,SVMXC__Site__c> newSitesMap)
{       
        //if(UserInfo.getProfileId()==Id.valueOf(System.LABEL.CLNOV13SRV03))
        //{    
            // check the golden Location Id duplicates in case of SDH upload 

    AP_LocationManager.checkLocationGoldenId(newSites);
            // Rule for routing cordys user to route to trigger
    if(System.LABEL.CLNOV13SRV10=='FOR_SDH_REGULAR_UPDATES'){
        AP_LocationManager.ProcessLocations(newSites);
    }
       // }       
}

}