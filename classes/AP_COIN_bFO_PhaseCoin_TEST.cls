/***********************************
Test Class for AP_COIN_bFO_PhaseCoin and AP_Coin_bFO_Milestone1Task
Description:May Release
HBP
************************************/


@isTest//(seealldata=true)
    private class AP_COIN_bFO_PhaseCoin_TEST{
    static testMethod void myUnitTest(){
 Test.startTest();
    
         Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
            inOffLif.Offer_Name__c='Test Schneider';
            inOffLif.Leading_Business_BU__c ='IT';
            inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Kilo Euro';
            inOffLif.Launch_Master_Assets_Status__c='Red';
            inOffLif.Stage__c ='0 - Define';
            
            Insert inOffLif; 
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            mileProject.RecordTypeId=MPRecordTypeId;
            mileProject.Name ='By Country Project ';
            mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.ForecastValidator__c=userInfo.getUserId();
            mileProject.ForecastStatus__c='Submitted for Validated';
            
            mileProject.Country__c=cotryObj.id;
            mileProject.Country_Announcement_Date__c=Date.today();
            
            insert mileProject ;
            
            Milestone1_Milestone__c createMMPhase = new Milestone1_Milestone__c();
            createMMPhase.Name='1.DEPLOY';
            createMMPhase.Project__c=mileProject.id;
            
            insert createMMPhase;
            Milestone1_Task__c  prTsk = new Milestone1_Task__c();
            prTsk.Task_Stage__c ='Not Started';
            prTsk.Project_Milestone__c=createMMPhase.id; 
            
            insert prTsk; 
            prTsk.Task_Stage__c ='Completed';
            update prTsk;
            
             Milestone1_Milestone__c createMMPhase1 = new Milestone1_Milestone__c();
            createMMPhase1.stage__c='Completed';
            createMMPhase1.Project__c=mileProject.id;
            
            insert createMMPhase1;
            //creation of Coin Task
          
            Test.stopTest();           
              
    
    }
    static testMethod void myTestTaskPhase(){
        Test.startTest(); 
         Offer_Lifecycle__c inOffLif1= new Offer_Lifecycle__c(); 
            inOffLif1.Offer_Name__c='Test Schneider';
            inOffLif1.Leading_Business_BU__c ='IT';
            inOffLif1.Offer_Classification__c ='Major';
            inOffLif1.Forecast_Unit__c='Kilo Euro';
            inOffLif1.Launch_Master_Assets_Status__c='Red';
            inOffLif1.Stage__c ='0 - Define';
            
            Insert inOffLif1; 
            Country__c cotryObj1= new Country__c();
            cotryObj1.Name ='India';
            cotryObj1.CountryCode__c='1IN';
            Insert cotryObj1;
            
            Milestone1_Project__c mileProject1= new Milestone1_Project__c(); 
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            mileProject1.RecordTypeId=MPRecordTypeId;
            mileProject1.Name ='By Country Project ';
            mileProject1.Offer_Launch__c=inOffLif1.id;
            mileProject1.ForecastValidator__c=userInfo.getUserId();
            mileProject1.ForecastStatus__c='Submitted for Validated';
            
            mileProject1.Country__c=cotryObj1.id;
            mileProject1.Country_Announcement_Date__c=Date.today()+2;
            
            insert mileProject1 ;
            
            Milestone1_Milestone__c createMMPhase1 = new Milestone1_Milestone__c();
            createMMPhase1.Name='1.DEPLOY';
            createMMPhase1.Stage__c ='Completed';
            createMMPhase1.Project__c=mileProject1.id;
            
            
            insert createMMPhase1;
            //creation of Coin Task
            Milestone1_Task__c  prTsk1 = new Milestone1_Task__c();
            prTsk1.Task_Stage__c ='Completed';
            prTsk1.Project_Milestone__c=createMMPhase1.id; 
            //AP_Coin_bFO_Milestone1Task.isTaskByPass=false;
            insert prTsk1; 
           // prTsk1.Task_Stage__c ='Completed';
           // update prTsk1;
            
            Milestone1_Milestone__c createMMPhase2 = new Milestone1_Milestone__c();
            createMMPhase2.Name='2. DEPLOY';
            createMMPhase2.Project__c=mileProject1.id;
            //insert createMMPhase2;
            
             //creation of Coin Task
            Milestone1_Task__c  prTsk11 = new Milestone1_Task__c();
            prTsk11.Task_Stage__c ='In Prograss';
            prTsk11.Project_Milestone__c=createMMPhase2.id; 
            
            //insert prTsk11; 
            //prTsk11.Task_Stage__c ='Completed';
            //update prTsk11;
            
          //*/
           Test.stopTest(); 
              
    
    }
    
}