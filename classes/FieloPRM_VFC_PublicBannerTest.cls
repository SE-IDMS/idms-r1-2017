/**
* @author Lucas Caceres 
* @date 08/06/2016
* @description Test BannerController
*/
@isTest
private class FieloPRM_VFC_PublicBannerTest {

   
    @isTest(seeAllData=false) static void unitTest(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            //creates a member
            FieloEE.MockUpFactory.setcustomProperties(false);        
            FieloEE__Member__c member ;
            try{
                member = FieloEE.MockUpFactory.createMember('Test', 'Passport', '445364565');            
            }catch(Exception  ex){
                member = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c limit 1];
            }
            
            //creates a menu
            FieloEE__Menu__c menu = new FieloEE__Menu__c(Name = 'test');
            insert menu;
            
            //creates a component
            RecordType rt = [SELECT ID FROM RecordType WHERE DeveloperName = 'Banner'];
            FieloEE__Component__c comp = new FieloEE__Component__c(RecordTypeId = rt.Id, FieloEE__Menu__c = menu.Id);
            insert comp;
            
            //creates a banner
            FieloEE__Banner__c ban = new FieloEE__Banner__c(Name = 'testBanner', FieloEE__Placement__c = 'Home', FieloEE__Component__c = comp.Id);
            insert ban;
            
            //attachs an image to the banner
            Attachment attachment = new Attachment();
            attachment.body = Blob.valueOf( 'this is an attachment test' );
            attachment.name = 'fake attachment';
            attachment.parentId = ban.id; 
            insert attachment;
            
            //active the banner
            ban.FieloEE__AttachmentId__c = attachment.Id;
            ban.FieloEE__IsActive__c = true;
            update ban;
            
            //run as the created member
            User customerPortal = [SELECT Id FROM User WHERE Id =: member.FieloEE__User__c];        
            system.runAs(customerPortal){
                FieloPRM_VFC_PublicBanner controller = new FieloPRM_VFC_PublicBanner();
                
                //set the attributes            
                controller.quantity = 2;
                controller.sectionPlacement = 'Home';
                controller.ComponentId = comp.Id;
                
                //look for the requested records
                List<FieloEE__Banner__c> banners = controller.getBannerItems();
                
                FieloPRM_VFC_PublicBanner.fieldsML fields = FieloPRM_VFC_PublicBanner.fieldsML;
                
                FieloPRM_VFC_PublicBanner.getBanners(new Set<String>{'Id', 'Name'}, null, member.Id, comp.Id, 2, 1, 'Name', null);
                
                String str = FieloPRM_VFC_PublicBanner.docURL;
                
                FieloEE__Component__c compo = controller.comp;
                //system.assertEquals(1,banners.size());
            }
        }        
    }
}