@isTest(SeeAllData=true)

private class VFC_AddProblemLinkedCase_Common_TEST{
    
/* @Method <This method caseStatusTestMethod is used test the AP07_CaseStatusAge class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Not throwing any Exception>
*/

    static testMethod void PLCTestMethod()
    {
        
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        Database.insert(breEntity);
        ID BreEntID =  breEntity.id;
        List<User> UserList = new List<User>();
        User PLdr = Utils_TestMethods.createStandardUser('PLdr'); 
        User PLdr1 = Utils_TestMethods.createStandardUser('PLdr1');
        UserList.add(PLdr);
        UserList.add(PLdr1);
        Database.insert(UserList);
        
        List<Problem__c> prblmList = new List<Problem__c>();
        List<Problem__c> prblmUpdateList = new List<Problem__c>();
        Problem__c PrblmNew = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmNew.TECH_Symptoms__c='A - a;B - b';
        Problem__c PrblmInProgress = Utils_TestMethods.createProblem(BreEntID,1);
        PrblmInProgress.TECH_Symptoms__c='B - b;C - c;D - d;E - E' ;      
       
        Problem__c PrblmInProgressSafety = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmInProgressSafety.TECH_Symptoms__c='C - c;A - a';    
        Problem__c PrblmSevMinExecuting = Utils_TestMethods.createProblem(BreEntID,1);
        PrblmSevMinExecuting.TECH_Symptoms__c='D - d;A - a';     
        Problem__c PrblmSevSafetyExecuting = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmSevSafetyExecuting.TECH_Symptoms__c='D - d;A - a';
        Problem__c PrblmSevMinCompleted = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmSevMinCompleted.TECH_Symptoms__c='D - d;A - a';
        Problem__c PrblmSevSafetyCompleted = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmSevSafetyCompleted.TECH_Symptoms__c='E - e;A - a';
        Problem__c PrblmStandBy = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmStandBy.TECH_Symptoms__c='E - e;A - a';
        Problem__c PrblmCanceled = Utils_TestMethods.createProblem(BreEntID,1);
       
        prblmList.add(PrblmNew);
        prblmList.add(PrblmInProgress);
        
     
        prblmList.add(PrblmSevMinExecuting);
        prblmList.add(PrblmSevSafetyExecuting);
        prblmList.add(PrblmSevMinCompleted);
        prblmList.add(PrblmSevSafetyCompleted);
        prblmList.add(PrblmStandBy);
        prblmList.add(PrblmInProgressSafety);
        prblmList.add(PrblmCanceled); 
             
              
        
        Country__c count = Utils_TestMethods.createCountry();
        insert count;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.country__c=count.id;
        insert account1;
            
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        insert contact1;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c='acd';
        case1.CaseSymptom__c = 'A';
        case1.CaseSubSymptom__c= 'a';
        case1.TECH_GMRCode__c='123456789';
        insert case1;
         Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case2.CommercialReference__c='abd';
        case2.TECH_GMRCode__c='123456789';
        case2.CaseSymptom__c = 'A';
        case2.CaseSubSymptom__c= 'a';
        insert case2;
        
          Database.SaveResult[] PrblmInsertResult = Database.insert(prblmList, false);
      
        List<CommercialReference__c> lstctr=new list<CommercialReference__c>();
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='acd',GMRCode__c='123456789',ProductGroup__c='sch'));
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='abd',GMRCode__c='123456789',ProductGroup__c='Sd'));
        Database.SaveResult[] lstSave = Database.insert(lstctr,false);
        ProblemCaseLink__c ObjPlC=new ProblemCaseLink__c(problem__c=prblmList[1].id,case__c=case2.id);
        insert ObjPlC;
        system.debug('------prblmList[0].id-------------->'+prblmList[0].id);
        
        ApexPages.currentPage().getParameters().put('pid', prblmList[0].id);
        ApexPages.currentPage().getParameters().put('act', 'ad');
        ApexPages.currentPage().getParameters().put('cid','');
        VFC_AddProblemLinkedCase_Common clsupprb1 =new VFC_AddProblemLinkedCase_Common();
        VFC_AddProblemLinkedCase_Common.WrapperCase wrap = new VFC_AddProblemLinkedCase_Common.WrapperCase(true,'','', 'string CaseStatus','string casecategorey','string caseCountryname','string strCommercialReftrence','string strSymptom','string casenumber','string caseCreationdate','string caseSymptom', 'string caseSubSymptom');
        wrap.isPRBSelected =true;
        clsupprb1.isNoGMRDATA=false;
        clsupprb1.isshowPB1=false;
        clsupprb1.isshowPB2=false;
        clsupprb1.isadd=false;
        
        clsupprb1.addProblemLinkedCase();
        clsupprb1.cancelAddRemovePLC();
    
    }
    static testMethod void PLCCaseTestMethod()
    {
        
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        Database.insert(breEntity);
        ID BreEntID =  breEntity.id;
        List<User> UserList = new List<User>();
        User PLdr = Utils_TestMethods.createStandardUser('PLdr'); 
        User PLdr1 = Utils_TestMethods.createStandardUser('PLdr1');
        UserList.add(PLdr);
        UserList.add(PLdr1);
        Database.insert(UserList);
        
        List<Problem__c> prblmList = new List<Problem__c>();
        List<Problem__c> prblmUpdateList = new List<Problem__c>();
        Problem__c PrblmNew = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmNew.TECH_Symptoms__c='A - a';
        Problem__c PrblmInProgress = Utils_TestMethods.createProblem(BreEntID,1);
        PrblmInProgress.TECH_Symptoms__c='A - a;B - b;C - c;D - d;E - E';  
       /*     
        Problem__c PrblmInProgressSafety = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmInProgressSafety.TECH_Symptoms__c='C - c;A - a';    
        Problem__c PrblmSevMinExecuting = Utils_TestMethods.createProblem(BreEntID,1);
        PrblmSevMinExecuting.TECH_Symptoms__c='D - d;A - a';     
        Problem__c PrblmSevSafetyExecuting = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmSevSafetyExecuting.TECH_Symptoms__c='D -d;A - a';
        Problem__c PrblmSevMinCompleted = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmSevMinCompleted.TECH_Symptoms__c='D - d;A - a';
        Problem__c PrblmSevSafetyCompleted = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmSevSafetyCompleted.TECH_Symptoms__c='E - e;A - a';
        Problem__c PrblmStandBy = Utils_TestMethods.createProblem(BreEntID,1); 
        PrblmStandBy.TECH_Symptoms__c='E - e;A - a';
        Problem__c PrblmCanceled = Utils_TestMethods.createProblem(BreEntID,1);
        */
        PrblmNew.Sensitivity__c ='testpq';
        prblmList.add(PrblmNew);
        prblmList.add(PrblmInProgress);
       /*
        prblmList.add(PrblmSevMinExecuting);
        prblmList.add(PrblmSevSafetyExecuting);
        prblmList.add(PrblmSevMinCompleted);
        prblmList.add(PrblmSevSafetyCompleted);
        prblmList.add(PrblmStandBy);
        prblmList.add(PrblmInProgressSafety);
        prblmList.add(PrblmCanceled); 
       */          
        
      
        
        
        Country__c count = Utils_TestMethods.createCountry();
        insert count;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.country__c=count.id;
        insert account1;
        
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        insert contact1;
      /* OPP_Product__c product =Utils_TestMethods.createProduct('INDUSTRIAL AUTOMATION', 'IDIBS - INDUSTRY SERVICES', 'EXCHANGE/REPAIR', 'HMI/SCADA');
        product.Name='HMI/SCADA';
        insert product;
        
        system.debug('******Product'*/
        OPP_Product__c  prod = new OPP_Product__c();
        
        prod.Name='RHDSA';
        prod.BusinessUnit__c='Energy';
        prod.ProductLine__c='INMVP - PRODUCT - SWITCHGEAR';
        prod.ProductFamily__c='OUTDOOR CIRCT BREAKER';
        prod.Family__c='GIE';
        prod.SubFamily__c='GIE';
        prod.Series__c='test';
        prod.GDP__c='test';
        prod.SDHID__c='LI 2856';
        prod.TECH_PM0CodeInGMR__c ='ACC21ZJ7------7415';
        prod.HierarchyType__c ='PM0-BUSINESS-UNIT';
        insert prod;
        
        Product2 prd2 = new Product2();
        prd2.ProductCode ='B6591_GL0-CMP81001 Description language';
        prd2.Name='B6591_GL0-AAA8N001';
        prd2.ProductGDP__c = prod.id;
        insert  prd2;
        
        //Product2 p= [select id from product2 where CreatedDate = LAST_N_DAYS:365 order by createddate DESC Limit 1];
        
        //insert p;
        
         system.debug('*********Product****'+prd2);
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c='BV_129AF_ADP_CAS5'; //acd -> BV_129AF_ADP_CAS5
        case1.CaseSymptom__c = 'A';
        case1.CaseSubSymptom__c= 'a';
        case1.CommercialReference_lk__c=prd2.id;// ''01t17000000Hq5Q';//
       // case1.TECH_GMRCode__c='123456789';
        insert case1;
        
        Database.SaveResult[] PrblmInsertResult = Database.insert(prblmList, false);
        
        //select id,CommercialReference__c,TECH_GMRCode__c ,Family__c,CaseSymptom__c,CaseSubSymptom__c from Case where id=:ApexPages.currentPage().getParameters().get('cid')
        //select id ,CommercialReference__c,Problem__c,Problem__r.Symptoms__c,Problem__r.TECH_Symptoms__c,Family__c,GMRCode__c  from CommercialReference__c where  (CommercialReference__c=:cs.CommercialReference__c OR GMRCode__c Like :strCaseFamily)
        List<CommercialReference__c> lstctr=new list<CommercialReference__c>();
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='BV_129AF_ADP_CAS5',GMRCode__c='123456789',ProductGroup__c='sch'));
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='abd',GMRCode__c='123456789',ProductGroup__c='Sd'));
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='BV_129AF_ADP_CAS5',GMRCode__c='123456789',ProductGroup__c='sch'));
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='abd',GMRCode__c='123456789',ProductGroup__c='Sd'));

        Database.SaveResult[] lstSave = Database.insert(lstctr,false);

        Case cs=case1;
        String strCaseFamily='anaidniandioadioanklank';
        List<CommercialReference__c> lstCR=[select id ,CommercialReference__c,Problem__c,Problem__r.Symptoms__c,Problem__r.TECH_Symptoms__c,Family__c,GMRCode__c  from CommercialReference__c where  (CommercialReference__c=:cs.CommercialReference__c OR GMRCode__c Like :strCaseFamily)];
        System.debug('lstCR:'+lstCR);
        
        system.debug('------prblmList[0].id-------------->'+prblmList[0].id);
        
        ProblemCaseLink__c pcl=new ProblemCaseLink__c(Case__c=case1.id, Problem__c=prblmList[0].id);
        insert pcl;
        
        PageReference pageRef = Page.VFP_AddProblemLinkedCase_Common;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('cid',case1.id );
        ApexPages.currentPage().getParameters().put('act', 'ad');
        //ApexPages.currentPage().getParameters().put('pid','');
        ApexPages.currentPage().getParameters().put('pid',prblmList[0].id);
        System.debug('case1.CommercialReference__c1:'+case1.CommercialReference__c);
        VFC_AddProblemLinkedCase_Common clsupprb2 =new VFC_AddProblemLinkedCase_Common();
        System.debug('case1.CommercialReference__c2:'+case1.CommercialReference__c);
        clsupprb2.isNoGMRDATA=false;
        clsupprb2.isshowPB1=false;
        clsupprb2.isshowPB2=false;
        clsupprb2.isadd=false;
      
        clsupprb2.addProblemLinkedCase();
        clsupprb2.cancelAddRemovePLC();
    
    //k:
        System.debug('case1.CommercialReference__c3:'+case1.CommercialReference__c);
        PageReference pageRef1 = Page.VFP_AddProblemLinkedCase_Common;
        System.debug('case1.CommercialReference__c4:'+case1.CommercialReference__c);
        pageRef1.getParameters().put('cid',case1.id );
        Test.setCurrentPage(pageRef1);
        
        
        //prblmList[0].Problem__r.TECH_Symptoms__c=null;
        ApexPages.currentPage().getParameters().put('cid',case1.id );
        //ApexPages.currentPage().getParameters().put('act', 'ad');
        //ApexPages.currentPage().getParameters().put('pid','');
        clsupprb2 =new VFC_AddProblemLinkedCase_Common();
        System.debug('case1.CommercialReference__c5:'+case1.CommercialReference__c);
        clsupprb2.isNoGMRDATA=false;
        clsupprb2.isshowPB1=false;
        clsupprb2.isshowPB2=false;
        clsupprb2.isadd=false;
      
        clsupprb2.addProblemLinkedCase();
        clsupprb2.cancelAddRemovePLC();

    
    
    }
    static testMethod void PLCCaseTestMethodNoData()
    {
        
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        Database.insert(breEntity);
        ID BreEntID =  breEntity.id;
        List<User> UserList = new List<User>();
        User PLdr = Utils_TestMethods.createStandardUser('PLdr'); 
        User PLdr1 = Utils_TestMethods.createStandardUser('PLdr1');
        UserList.add(PLdr);
        UserList.add(PLdr1);
        Database.insert(UserList);
        
        List<Problem__c> prblmList = new List<Problem__c>();
        List<Problem__c> prblmUpdateList = new List<Problem__c>();
        Problem__c PrblmNew = Utils_TestMethods.createProblem(BreEntID,1); 
        
        Problem__c PrblmInProgress = Utils_TestMethods.createProblem(BreEntID,1);
        //PrblmInProgress.TECH_Symptoms__c='a - A';      
        //PrblmNew.TECH_Symptoms__c='a - A'; 
        PrblmNew.Symptoms__c ='sc-sc';
        PrblmInProgress.Symptoms__c='sc-sc';
        PrblmNew.Sensitivity__c ='testpq';
        prblmList.add(PrblmNew);
        prblmList.add(PrblmInProgress);
       
                
        //Added 21 OCT
         OPP_Product__c  prod = new OPP_Product__c();
        
        prod.Name='RHDSA';
        prod.BusinessUnit__c='Energy';
        prod.ProductLine__c='INMVP - PRODUCT - SWITCHGEAR';
        prod.ProductFamily__c='OUTDOOR CIRCT BREAKER';
        prod.Family__c='GIE';
        prod.SubFamily__c='GIE';
        prod.Series__c='test';
        prod.GDP__c='test';
        prod.SDHID__c='LI 2856';
        prod.TECH_PM0CodeInGMR__c ='ACC21ZJ7------7415';
        prod.HierarchyType__c ='PM0-BUSINESS-UNIT';
        insert prod;
        
        Product2 prd2 = new Product2();
        prd2.ProductCode ='B6591_GL0-CMP81001 Description language';
        prd2.Name='B6591_GL0-AAA8N001';
        prd2.ProductGDP__c = prod.id;
        insert  prd2;
      
        
        
        Country__c count = Utils_TestMethods.createCountry();
        insert count;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.country__c=count.id;
        insert account1;
        
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        insert contact1;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c='B6591_GL0-AAA8N00';
        // case1.TECH_GMRCode__c='schneider 9';//'123456789';
        //case1.TECH_CaseSymptoms__c='A'; //k:
        case1.CommercialReference_lk__c=prd2.id;
        case1.CaseSymptom__c ='sc';
        case1.CaseSubSymptom__c='sc';
        insert case1;
        system.debug('(((((((Case)))))))))'+case1);
          Database.SaveResult[] PrblmInsertResult = Database.insert(prblmList, false);
        //problem link  
        ProblemCaseLink__c pcl=new ProblemCaseLink__c(Case__c=case1.id, Problem__c=prblmList[0].id);
        insert pcl;
      //select id ,CommercialReference__c,Problem__c,Problem__r.Symptoms__c,Problem__r.TECH_Symptoms__c,Family__c,GMRCode__c  from CommercialReference__c where  (CommercialReference__c=:cs.CommercialReference__c OR GMRCode__c Like :strCaseFamily);
        List<CommercialReference__c> lstctr=new list<CommercialReference__c>();
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='B6591_GL0-AAA8N00',GMRCode__c='ACC21ZJ7------7415',ProductGroup__c='sch'));
        lstctr.add(new  CommercialReference__c(Problem__c=prblmList[0].id,CommercialReference__c='abd',GMRCode__c='ACC21ZJ7------7415',ProductGroup__c='Sd'));
        Database.SaveResult[] lstSave = Database.insert(lstctr,false);
        
        system.debug('------prblmList[0].id-------------->'+prblmList[0].id);
        //Problem__c varPRB=prblmList;
        ApexPages.currentPage().getParameters().put('pid',prblmList[0].id );
        ApexPages.currentPage().getParameters().put('act', 'ad');
        ApexPages.currentPage().getParameters().put('cid',case1.id);
        VFC_AddProblemLinkedCase_Common clsupprb2 =new VFC_AddProblemLinkedCase_Common();
        VFC_AddProblemLinkedCase_Common.WrapperProblem warPrb = new VFC_AddProblemLinkedCase_Common.WrapperProblem(false,string.valueof(prblmList[0].id),prblmList[0].name,prblmList[0].Title__c,prblmList[0].WhatIsTheProblem__c,prblmList[0].WhyIsItaProblem__c,prblmList[0].Status__c,'acd',prblmList[0].Symptoms__c,prblmList[0].Severity__c,'20/03/2015');
        clsupprb2.isNoGMRDATA=false;
        clsupprb2.isshowPB1=false;
        clsupprb2.isshowPB2=false;
        clsupprb2.isadd=false;
      
        clsupprb2.addProblemLinkedCase();
       clsupprb2.cancelAddRemovePLC();
    
    }
}