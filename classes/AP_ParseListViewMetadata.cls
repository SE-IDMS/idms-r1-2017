public class AP_ParseListViewMetadata {

    private String accessToken = '';
    private CS_PRM_ApexJobSettings__c jobInfo;
    private Datetime thisRuntime;
    private final Integer maxCalloutLimit = 10;
    private Integer usedCallouts = 0;
    private Map<String, Datetime> mLVDates = new Map<String, Datetime>();
    private List<String> lKeys = new List<String>();
    
    // Wrapper class for OAuthTokenizer
    public class OAuthTokenizer {
        public OAuthTokenizer() { }
        public String id;
        public String issued_at;
        public String token_type;
        public String instance_url;
        public String signature;
        public String access_token;
    }

    public AP_ParseListViewMetadata () {
        thisRuntime = Datetime.NOW();
        jobInfo = CS_PRM_ApexJobSettings__c.getInstance('ParseListViews');
        System.debug('jobinfo inside AP_ParseListViewMetadata:'+jobInfo);
    }

    // Helper method
    private static MetadataService.MetadataPort createService(string accessToken) {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.timeout_x = 30000;
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = accessToken;
        return service;
    }

    // Helper method to perform OAuth and get the access token
    
    private String getAccessToken() {
        if (accessToken == null || accessToken == '') {
            Http h = new Http();
            String body = 'grant_type=password';
                   body = body + '&client_id=' + System.Label.CLOCT14PRM05;
                   body = body + '&client_secret=' + System.Label.CLOCT14PRM06;
                   body = body + '&username=' + System.Label.CLOCT14PRM03;
                   body = body + '&password=' + System.Label.CLOCT14PRM04;

            //System.Debug('*** Body:' + body);

            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.Label.CLOCT14PRM07 + '/services/oauth2/token');
            req.setMethod('POST');
            req.setBody(body);
            //System.Debug('Request Sent: ' + req);
            HttpResponse res = h.send(req);
            String oAuthReponse = res.getBody();
            //System.Debug('*** oAuthReponse: ' + oAuthReponse);

            OAuthTokenizer tokenizer = (OAuthTokenizer)JSON.deserialize(oAuthReponse, OAuthTokenizer.class);

            System.Debug('*** Object Response: ' + tokenizer.access_token);
            accessToken = tokenizer.access_token;
            usedCallouts += 1;
        }
        return accessToken;
    }

    // Helper method to get the list of listview for (Accounts, Leads, Opportunity & OpportunityRegistrationForm__c)
    // This method will return only those object listview that have been identified as
    // to be parsed and have been modified since the LastRun__c. For both, refer to custom setting CS_PRM_ApexJobSettings__c
    private Map<String, String> getObjectListViews () {
        
        Map<String, String> mListViews = new Map<String, String>();
        if (!Test.isRunningTest()) {
        List<MetadataService.ListMetadataQuery> listQueries = new List<MetadataService.ListMetadataQuery>();
        MetadataService.ListMetadataQuery listView = new MetadataService.ListMetadataQuery();
        listView.type_x = 'ListView';
        listQueries.add(listView);
        System.debug('jobinfo inside getObjectListViews:'+jobInfo);
        Set<String> mObjs = new Set<String>(jobInfo.ObjectToParse__c.split(','));
        String token;
        if (!Test.isRunningTest())
            token = getAccessToken();
        MetadataService.MetadataPort service = createService(token);
        MetadataService.FileProperties[] fileProperties = service.listMetadata(listQueries, 25);
        usedCallouts += 1;
        // sortList(fileProperties, 'asc');
        
        List<MetadataService.FileProperties> resultList = new List<MetadataService.FileProperties>();
        //Create a map that can be used for sorting
        Map<object, List<MetadataService.FileProperties>> objectMap = new Map<object, List<MetadataService.FileProperties>>();      
        List<Datetime> keys = new List<Datetime>();
        for(MetadataService.FileProperties fp : fileProperties) {
            String objPrefix = fp.fullName.subString(0, fp.fullName.indexOf('.'));
            System.Debug('*** Fullname: ' + fp.fullName);
            if (!mObjs.contains(objPrefix) || !(fp.lastModifiedDate > jobInfo.LastRun__c)) 
                continue;

            if(objectMap.get(fp.lastModifiedDate) == null){  // For non MetadataService.FileProperties use obj.ProperyName
                objectMap.put(fp.lastModifiedDate, new List<MetadataService.FileProperties>());
            }
            objectMap.get(fp.lastModifiedDate).add(fp);
            keys.add(fp.lastModifiedDate);
        }
        keys.sort();    //Sort the keys
        
        for(object key : keys){
            resultList.addAll(objectMap.get(key));
        }
        for(MetadataService.FileProperties ob : resultList) {
            lKeys.add(ob.fullName);
            // System.Debug('*** Unique Name: ' + ob.fullName);
            mListViews.put(ob.fullName, ob.id);
            mLVDates.put(ob.fullName, ob.lastModifiedDate);
        }
        /*
        //Apply the sorted values to the source list
        if(order.toLowerCase() == 'asc'){
            for(MetadataService.FileProperties ob : resultList) {
                lKeys.add(ob.fullName);
                mListViews.put(ob.fullName, ob.id);
                mLVDates.put(ob.fullName, ob.lastModifiedDate);
            }
        }else if(order.toLowerCase() == 'desc'){
            for(integer i = resultList.size()-1; i >= 0; i--) {
                lKeys.add(resultList[i].fullName);
                mListViews.put(resultList[i].fullName, resultList[i].id);
                mLVDates.put(resultList[i].fullName, resultList[i].lastModifiedDate);
            }
        }
        */
        /*
        for (MetadataService.FileProperties fileProperty : fileProperties){
            
            if (mObjs.contains(objPrefix) && fileProperty.lastModifiedDate > jobInfo.LastRun__c) {
                mListViews.put(fileProperty.fullName, fileProperty.id);
                mLVDates.put(fileProperty.fullName, fileProperty.lastModifiedDate);
            }
        }
        */
        System.debug('mListViews:'+mListViews);
        }
       return mListViews;
    
    }
    /*
    private void sortList(List<MetadataService.FileProperties> items, String order){

        List<MetadataService.FileProperties> resultList = new List<MetadataService.FileProperties>();

        //Create a map that can be used for sorting
        Map<object, List<MetadataService.FileProperties>> objectMap = new Map<object, List<MetadataService.FileProperties>>();
        List<Datetime> keys = new List<Datetime>();
        for(MetadataService.FileProperties ob : items){
                if(objectMap.get(ob.lastModifiedDate) == null){  // For non MetadataService.FileProperties use obj.ProperyName
                    objectMap.put(ob.lastModifiedDate, new List<MetadataService.FileProperties>());
                }
                objectMap.get(ob.lastModifiedDate).add(ob);
                keys.add(ob.lastModifiedDate);
        }
        //Sort the keys
        keys.sort();
    
        for(object key : keys){
            resultList.addAll(objectMap.get(key));
        }

        //Apply the sorted values to the source list
        items.clear();
        if(order.toLowerCase() == 'asc'){
            for(MetadataService.FileProperties ob : resultList) {
                lKeys.add(ob.fullName);
                items.add(ob);
            }
        }else if(order.toLowerCase() == 'desc'){
            for(integer i = resultList.size()-1; i >= 0; i--) {
                lKeys.add(resultList[i].fullName);
                items.add(resultList[i]);
            }
        }
    }

    */
    private Map<String, String> getListViewColumns (String objectName, List<MetadataService.ListView> objListViews) {
        
        Map<String, String> lLVColumns = new Map<String, String>();
        if (!Test.isRunningTest()) {
        // Find all the fieldAPI in the custom setting
        Map<String, CS_PRM_ListViewFieldMapping__c> fieldMap = CS_PRM_ListViewFieldMapping__c.getAll();

        for (MetadataService.ListView lvw : objListViews) {
            String viewCols='';
            String oPrefix = lvw.fullName.subString(0, lvw.fullName.indexOf('.') + 1);
            System.Debug('*** UniqueName: ' + lvw.fullName);
            System.Debug('*** Object Prefix: ' + oPrefix);
            String uniqName = (objectName != '' ? (objectName + '.') : '') + lvw.fullName;
            if(lvw.columns != null) {
                String kCol;
                for(String column : lvw .columns) {
                    kCol = oPrefix + column;
                    if (fieldMap.get(kCol) != null || fieldMap.get(column) != null)
                        column = (fieldMap.get(kCol) != null ? fieldMap.get(kCol).FieldAPI__c : fieldMap.get(column).FieldAPI__c);
                    viewCols = viewCols + ((column != null && column != '') ? column + ',' : '');
                }
            }
            if (viewCols.endsWith(','))
                viewCols = viewCols.subString(0, viewCols.length() - 1);

            // When No columns are added to list view, SFDC automatically show Id, Name (with Name as hyperlink)
            if (viewCols.length() == 0)
                viewCols = 'Id, Name';

            System.Debug('*** Comma Columns: ' + viewCols);
            lLVColumns.put (uniqName, viewCols);
        }
        }
        return lLVColumns;
    
    }
    // This helper method returns a list of UserAccessibleListViews__c
    // Input Parameters to be passed in the following order
    // #1. MAP of LISTVIEW UniqueName & Columns
    // #2. MAP of LISTVIEW names & ListViewID
    // #3. MAP of EXISTING LISTVIEW RECORDID that were already parsed
    private List<UserAccessibleListViews__c> getTargetObjectFormat(
                                Map<String, String> lVWColumns,
                                Map<String, String> lvNames,
                                Map<Id, UserAccessibleListViews__c> extListViews) {

        List<UserAccessibleListViews__c> lObj = new List<UserAccessibleListViews__c>();

        for (String key : lVWColumns.keyset()) {
            //System.Debug('*** EView Id:' + lvNames.get(key));
            UserAccessibleListViews__c vObj = null;
            if (extListViews != null && extListViews.containsKey(lvNames.get(key)))
                vObj = extListViews.get(lvNames.get(key));
            else
                vObj = new UserAccessibleListViews__c();

            vObj.Columns__c = lVWColumns.get(key);
            vObj.UniqueName__c = key;
            vObj.ViewId__c = lvNames.get(key);
            vObj.LastModifiedOn__c = mLVDates.get(key);
            
            //System.Debug('*** UserAccessibleListViews__c: ' + vObj);
            lObj.add (vObj);
        }

        return lObj;
    }

    public void doParse () {
        if (!Test.isRunningTest()) {
        Map<String, String> listViews = getObjectListViews ();
        
        if (listViews != null && listViews.size() <= 0) {
            System.Debug('0 Listviews modified that need to be processed');
            return;
        }
        /*
        for (String k : lKeys) {
            System.Debug('*** Key: ' + k);
        }
        */

        List<UserAccessibleListViews__c> lAllViews = new List<UserAccessibleListViews__c>();
        List<UserAccessibleListViews__c> tELVws = [SELECT Id, Name, ViewId__c, UniqueName__c, Columns__c, LastModifiedOn__c FROM UserAccessibleListViews__c WHERE ViewId__c IN :listViews.values() LIMIT 10000];
        System.Debug('*** Existing Parsed Views: ' + tELVws.size());

        Map<Id, UserAccessibleListViews__c> eListViews = new Map<Id, UserAccessibleListViews__c>();
        
        for (UserAccessibleListViews__c ua : tELVws) {
            System.Debug('*** List ViewID: ' + ua.ViewId__c + ', *** RecordId: ' + ua.Id + ' *** LastModified:' + mLVDates.get(ua.UniqueName__c));
            eListViews.put(ua.ViewId__c, ua);
        }
        
        String token;
        if (!Test.isRunningTest())
            token = getAccessToken();
        MetadataService.MetadataPort service = createService(token);
        Integer iSubLimit = 0;
        List<String> lUniqNames = new List<String>();
        Datetime lastRead = DateTime.parse('01/01/2010 01:00 AM');

        /*
        for (String lvKey : lKeys) {
            lUniqNames.add(lvKey);
            lastRead = mLVDates.get(lvKey);
            if (iSubLimit >= 9 && usedCallouts < maxCalloutLimit) {
                System.Debug('*** Listview List:' + lUniqNames);
                MetadataService.ReadListViewResult lvwResult =
                        ( MetadataService.ReadListViewResult)service.readMetadata('ListView', lUniqNames);
                Map<String, String> lvColumns = getListViewColumns('', lvwResult.records);

                List<UserAccessibleListViews__c> aLVWs = getTargetObjectFormat(lvColumns,  listViews, eListViews);
                lAllViews.addAll(aLVWs);
                lUniqNames = new List<String>();
                iSubLimit = 0;
                usedCallouts += 1;
            }
            else if (usedCallouts >= maxCalloutLimit) break;
            else iSubLimit += 1;
        }
        */
        Integer lSize = lKeys.size();
        for (Integer i = 1; i <= lSize; i++) {
            String lvKey = lKeys[i - 1];
            lUniqNames.add(lvKey);
            lastRead = mLVDates.get(lvKey);
            
            if (usedCallouts >= maxCalloutLimit) break;
            else if (( Math.mod(i, 9) == 0 || i == lSize) && usedCallouts < maxCalloutLimit) {
                System.Debug('*** Listview List:' + lUniqNames);
                MetadataService.ReadListViewResult lvwResult =
                        ( MetadataService.ReadListViewResult)service.readMetadata('ListView', lUniqNames);
                Map<String, String> lvColumns = getListViewColumns('', lvwResult.records);
                System.Debug('*** Columns List: ' + lvColumns);
                List<UserAccessibleListViews__c> aLVWs = getTargetObjectFormat(lvColumns,  listViews, eListViews);
                lAllViews.addAll(aLVWs);
                lUniqNames = new List<String>();
                // iSubLimit = 0;
                usedCallouts += 1;
            }
        }
        
        if (lAllViews.size() > 0) {
            System.Debug('*** Update List Size: ' + lAllViews.size());
            Database.UPSERT(lAllViews);
        }

        jobInfo.LastRun__c = jobInfo.InitialSync__c == true ? lastRead : thisRuntime;
        UPDATE jobInfo;
    }
    }
}