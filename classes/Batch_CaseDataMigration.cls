global class Batch_CaseDataMigration implements Database.Batchable<sObject>{
    
    global final Set<String> openCaseStatus=new Set<String>{'New','Open','Open/ Level 1 Escalation','Open/ Level 2 Escalation','Open/ Level 3 Escalation','Open/ Answer Available To L1','Open/ Answer Available To L2','Open/ Waiting For Details - Customer','Open/ Waiting For Details - Internal'};
    global final Set<String> closedCaseStatus=new Set<String>{'Closed'}; 
    global final Set<String> closedPendingCaseStatus=new Set<String>{'Closed/ Pending Action'};
    global Integer intLimitNumber;
    global List<String> toBeProcessedCaseStatus=new List<String>();
    global List<Case> toBeUpdatedCases=new List<Case>();

    global Batch_CaseDataMigration(){}
    global Batch_CaseDataMigration(String CaseStatusForBatchProcess,Integer intLmtNumber){
        if(CaseStatusForBatchProcess == 'Open'){
            toBeProcessedCaseStatus.addAll(openCaseStatus);
        }
        else if (CaseStatusForBatchProcess == 'ClosedPending'){
            toBeProcessedCaseStatus.addAll(closedPendingCaseStatus);
        }
        else if (CaseStatusForBatchProcess == 'Closed'){
            toBeProcessedCaseStatus.addAll(closedCaseStatus);
        }
        else{
            toBeProcessedCaseStatus.addAll(openCaseStatus);
            toBeProcessedCaseStatus.addAll(closedPendingCaseStatus);
            toBeProcessedCaseStatus.addAll(closedCaseStatus);   
        }
    
        if(intLmtNumber==0 || intLmtNumber==null)
            intLmtNumber=2000000;
        intLimitNumber=intLmtNumber;
   }
    

  

    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([Select Id, SupportCategory__c, Symptom__c, SubSymptom__c, Status, ActionNeededFrom__c, LevelofExpertise__c, CaseSymptom__c,CaseSubSymptom__c  from Case where  Status in :toBeProcessedCaseStatus AND TECH_CaseDataUpdated__c =false  Order by CreatedDate DESC limit :intLimitNumber]);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){      
        Map<Id,Case> caseMap=new Map<Id,Case>();    
        for(sObject objScope : scope){
            Case objCase=(Case)objScope;   
            caseMap.put(objCase.id,objCase);
        }
        updateDataOnCase(caseMap);
    }

    global void updateDataOnCase(Map<Id,Case> caseMap){
        Map<Id,Case>mapCasesWithSymptoms = new Map<Id,Case>();
        Map<Id,Case>mapCasesWithCategory = new Map<Id,Case>();
        //Set<String> CommRefSet = new Set<String>();
        Map<String,CS_CaseSymptomMapping__c> mapSymptomMapping = new Map<String,CS_CaseSymptomMapping__c>();
        Map<String,CS_CaseCategoryMapping__c> mapCategoryMapping = new Map<String,CS_CaseCategoryMapping__c>();
        Map<String,String> mapStatusMapping = new Map<String,String>();
        //Set<String> OldGMRCodeSet = new Set<String>();
        String strErrorId='';
        String strErrorMessage='';
        String strErrorType='';
        String strErrorStackTrace='';
        boolean canRunBatch=false;
        
        mapStatusMapping.put('New','New');
        mapStatusMapping.put('Open','In Progress');
        mapStatusMapping.put('Open/ Level 1 Escalation','In Progress');
        mapStatusMapping.put('Open/ Level 2 Escalation','In Progress');
        mapStatusMapping.put('Open/ Level 3 Escalation','In Progress');
        mapStatusMapping.put('Open/ Answer Available To L1','In Progress');
        mapStatusMapping.put('Open/ Answer Available To L2','In Progress');
        mapStatusMapping.put('Open/ Waiting For Details - Customer','In Progress');
        mapStatusMapping.put('Open/ Waiting For Details - Internal','In Progress');
        mapStatusMapping.put('Closed/ Pending Action','Answer provided to Customer');
        mapStatusMapping.put('Closed','Closed');

        for(User user:[select id from User where Id=:UserInfo.getUserId() and BypassVR__c=true and BypassWF__c=true])
            canRunBatch=true;
        if(canRunBatch || Test.isRunningTest() ){   
            List<CS_CaseSymptomMapping__c> lstSymptomMapping = new List<CS_CaseSymptomMapping__c>([SELECT Name, UniqueId__c, NewCaseSubSymptom__c, NewCaseSymptom__c FROM CS_CaseSymptomMapping__c]);
            for(CS_CaseSymptomMapping__c objCaseSymptomMapping: lstSymptomMapping){
                mapSymptomMapping.put(objCaseSymptomMapping.UniqueId__c,objCaseSymptomMapping);
            }
            List<CS_CaseCategoryMapping__c> lstCategoryMapping = new List<CS_CaseCategoryMapping__c>([SELECT NewCategory__c,NewReason__c,UniqueId__c FROM CS_CaseCategoryMapping__c]);
            
            for(CS_CaseCategoryMapping__c objCaseCategoryMapping: lstCategoryMapping){
                mapCategoryMapping.put(objCaseCategoryMapping.UniqueId__c,objCaseCategoryMapping);
            }
            
            for(Case objCase:caseMap.values()){
                if(objCase.CaseSymptom__c !=null && objCase.CaseSymptom__c!=''){
                    if(mapSymptomMapping.containsKey(objCase.CaseSymptom__c)){
                        objCase.CaseSubSymptom__c = mapSymptomMapping.get(objCase.CaseSymptom__c).NewCaseSubSymptom__c;
                        objCase.CaseSymptom__c = mapSymptomMapping.get(objCase.CaseSymptom__c).NewCaseSymptom__c;
                    }
                    else{
                        objCase.CaseOtherSymptom__c = objCase.CaseSymptom__c; 
                        objCase.CaseSymptom__c = 'Other';
                        objCase.CaseSubSymptom__c = 'Symptom not described above';
                    }
                }
                if(objCase.SupportCategory__c !=null && objCase.SupportCategory__c!=''){
                    String strCaseUniqueTaxonomy = '';
                    strCaseUniqueTaxonomy = objCase.SupportCategory__c.toUpperCase().trim();
                    if(objCase.Symptom__c !=null && objCase.Symptom__c!=''){
                        strCaseUniqueTaxonomy += ' : ' + objCase.Symptom__c.toUpperCase().trim();
                    }
                    if(objCase.SubSymptom__c !=null && objCase.SubSymptom__c!=''){
                        strCaseUniqueTaxonomy += ' : ' + objCase.SubSymptom__c.toUpperCase().trim();
                    }
                    if(mapCategoryMapping.containsKey(strCaseUniqueTaxonomy)){
                        objCase.SupportCategory__c = mapCategoryMapping.get(strCaseUniqueTaxonomy).NewCategory__c;
                        objCase.Symptom__c = mapCategoryMapping.get(strCaseUniqueTaxonomy).NewReason__c;
                        objCase.SubSymptom__c = '';
                    }
                }
                if(mapStatusMapping.containsKey(objCase.status)){
                    objCase.status = mapStatusMapping.get(objCase.status);
                }
                if(objCase.LevelofExpertise__c!=null && objCase.LevelofExpertise__c!='' )
                    objCase.ActionNeededFrom__c = objCase.LevelofExpertise__c;
                else
                    objCase.ActionNeededFrom__c = 'Primary';
                objCase.TECH_CaseDataUpdated__c=true;
                toBeUpdatedCases.add(objCase);
            }
            
            try{
                Database.SaveResult[] CaseUpdateResult = Database.update(toBeUpdatedCases, false);
                for(Database.SaveResult objSaveResult: CaseUpdateResult){
                    if(!objSaveResult.isSuccess()){
                        Database.Error err = objSaveResult.getErrors()[0];
                        strErrorId = objSaveResult.getId() + ':' + '\n';
                        strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                    }
                }
            }    
            catch(Exception ex){
                strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
                strErrorType += ex.getTypeName()+'- '+ ex.getLineNumber() + '\n';
                strErrorStackTrace += ex.getTypeName()+'- '+ ex.getStackTraceString() + '\n';
            }
            finally{
                if(strErrorId.trim().length()>0 || Test.isRunningTest()){
                    try{
                        DebugLog__c objDebugLog = new DebugLog__c(ExceptionType__c=strErrorType,Description__c=strErrorMessage,StackTrace__c=strErrorStackTrace, DebugLog__c= strErrorId );
                        Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                        DMLoption.optAllOrNone = false;
                        DMLoption.AllowFieldTruncation = true;
                        DataBase.SaveResult debugLog_SR = Database.insert(objDebugLog, DMLoption);
                    }
                    catch(Exception ex){
                    }
                }
            }
        }
        else{
            DebugLog__c objDebugLog = new DebugLog__c(ExceptionType__c='Exception',Description__c='WorkFlow and Validation Rules Not bypassed For User',StackTrace__c='', DebugLog__c= 'WorkFlow and Validation Rules Not bypassed For User' );
            Database.DMLOptions DMLoption = new Database.DMLOptions(); 
            DMLoption.optAllOrNone = false;
            DMLoption.AllowFieldTruncation = true;
            DataBase.SaveResult debugLog_SR = Database.insert(objDebugLog, DMLoption);
        }
    }

    global void finish(Database.BatchableContext BC){
    }
}