global class ReadCategoryLangTranslationsMockImpl  implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
            // test for the type of the response, OP was just doing readMetaData()
        if (request instanceof MetadataService.readMetadata_element) {
              MetadataService.readMetadata_element requestReadMetadata_element  = (MetadataService.readMetadata_element) request;
              // This allows you to generalize the mock response by type of metadata read
              if (requestReadMetadata_element.type_x == 'CustomObjectTranslation') { 
                 MetadataService.readCustomObjectTranslationResponse_element mockRes   = new MetadataService.readCustomObjectTranslationResponse_element();
                 mockRes.result = new MetaDataService.ReadCustomObjectTranslationResult();
                 mockRes.result.records = createCustomObjects();
                 response.put('response_x', mockRes);
              }
                  else if (requestReadMetadata_element.type_x == 'CustomField') { 
                    //response.put('response_x', RCOT);
                  }        
             }
         }
          public static MetadataService.CustomObjectTranslation[] createCustomObjects () {
              List<VFC_ManageLanguageTranslations.TranslationWrapper> mylist = new List<VFC_ManageLanguageTranslations.TranslationWrapper>();
              VFC_ManageLanguageTranslations.TranslationWrapper lblWrapper = new VFC_ManageLanguageTranslations.TranslationWrapper();
              //myprofil category under partner portal application
              lblWrapper.LabelName  = 'CLAPR15PRM004';
              lblWrapper.Usage = 'Email field label on User Registration, My Profile, and Login';
              lblWrapper.TranslationValue = 'testmail';
              mylist.add(lblWrapper);
              
              lblWrapper.LabelName  = 'CLAPR15PRM007';
              lblWrapper.Usage = 'Phone Number field label on User Registration, and My Profile';
              lblWrapper.TranslationValue = 'Phone Number';
              mylist.add(lblWrapper);
              
              lblWrapper.LabelName  = 'CLAPR15PRM029';
              lblWrapper.Usage = 'Company Name field label on User Registration, and My Profile';
              lblWrapper.TranslationValue = 'Company Name';
              mylist.add(lblWrapper);
              
              lblWrapper.LabelName  = 'CLAPR15PRM031';
              lblWrapper.Usage = 'Address 1 field label on User Registration, and My Profile';
              lblWrapper.TranslationValue = 'Address 1';
              mylist.add(lblWrapper);
              
              lblWrapper.LabelName  = 'CLAPR15PRM032';
              lblWrapper.Usage = 'Address 2 field label on User Registration, and My Profile';
              lblWrapper.TranslationValue = 'Address 2';
              mylist.add(lblWrapper);
            
               List<MetadataService.CustomFieldTranslation> cList = new List<MetadataService.CustomFieldTranslation>();
               MetadataService.CustomFieldTranslation c = new MetadataService.CustomFieldTranslation();
               c.name = 'AccountName__c';
               c.Label = 'mylist';
               cList.add(c);
               
              MetadataService.CustomFieldTranslation cc = new MetadataService.CustomFieldTranslation();
               cc.name = 'lData';
               cc.Label = 'mylist';
               cList.add(cc);
               
               MetadataService.CustomObjectTranslation newRes = new MetadataService.CustomObjectTranslation();
                 newRes.fullName = 'Account-en_US';
                 newRes.fields = cList;
         
            return (new List<MetadataService.CustomObjectTranslation> {newRes});
              
          }
}