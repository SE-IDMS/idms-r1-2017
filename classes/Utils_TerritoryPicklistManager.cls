/*
    Author          : Accenture On-shore Team 
    Date Created    : 06/02/2012
    Description     : Utility class to populate Picklist Filter values for Territory search
                      extending abstract Data Source Class
*/

Public Class Utils_TerritoryPicklistManager implements Utils_PicklistManager
{
    // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(Label.CL00351); //Business
        //Labels.add(Label.CL10085); //Country
        Labels.add(Label.CL10086); //State Province
        return Labels;
    }
    
     // Return All Picklist Values 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
    
       
       List<SelectOption> options = new  List<SelectOption>();
       
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Business fields and Country when Page Loads
       if(i==1)
       {
            // Populate Business Picklist Values
            Schema.DescribeFieldResult business = Schema.sObjectType.Competitor__c.fields.MultiBusiness__c;
            for (Schema.PickListEntry PickVal : business.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }
       if(i==2)
       {
       
            // Populate State/province Picklist Values
            for(StateProvince__c stateprovince: [Select Id, Name, Country__c from StateProvince__c where Country__c =:S order by Name asc])
            {
            
                options.add(new SelectOption(stateprovince.Id,stateprovince.Name));
            }
            
            //options.add(new SelectOption('france','france'));
       }
       

       return  options;  
    }

}