@istest
private class Batch_AssignAccountChannelFeature_TEST {
   static testMethod void myUnitTest_manual() {
    
   
    Test.startTest();
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        acc.ClassLevel1__c = 'Panel Builder';
        acc.ClassLevel2__c = 'Control Panel Makers';
        insert acc;
        acc.IsPartner  = true;
        update acc;
        
        
        CS_PRM_ApexJobSettings__c  cJobSetting = new CS_PRM_ApexJobSettings__c();
        cJobSetting.LastRun__c = Date.today();
        cJobSetting.InitialSync__c =false;
        cJobSetting.Name='AssignChannelFeatureByAccount';
        insert cJobSetting;
        
                
        Batch_AssignAccountChannelFeature batch_ppsa1 = new Batch_AssignAccountChannelFeature(); 
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_ppsa1,1000);
        
    Test.stopTest();
 }
 
 
  static testMethod void myUnitTest_schedule() {
            Test.startTest();
                    String CRON_EXP = '0 0 0 1 1 ? 2025';  
                    String jobId = System.schedule('AssignAccountChannelFeature', CRON_EXP, new Batch_AssignAccountChannelFeature() );

            Test.stopTest();
    }

}