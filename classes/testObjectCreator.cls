@isTest
public class testObjectCreator {
    public testObjectCreator() {        
    }

    public static Country__c CreateCountry(string name){
        Country__c Cou = new Country__c();
        Cou.Name = name;
        Cou.CountryCode__c = '123';
        insert Cou;
        System.debug('Country Id created:'+Cou.Id);
        return Cou;
    }

    public static Account CreateAccount(string name, Id countryId){
        Account a = new Account();
        a.Name = name;
        a.Country__c = countryId;
        a.BillingCountry = 'IN';
        insert a;
        System.debug('Account Id created:'+a.Id);
        return a;
    }

        public static Zuora__CustomerAccount__c CreateBillingAccount(string name, string billSoldName, Id accId){
        Zuora__CustomerAccount__c ba = new Zuora__CustomerAccount__c();
        ba.Name = name;
        ba.ReferenceName__c = name;
        ba.Zuora__Account__c = accId;
        ba.Zuora__BillToId__c = '2c92c0f84b07957f014b11cbaac6685a';
        ba.Zuora__SoldToId__c = '2c92c0f84b07957f014b11cbaac6685a';
        ba.Zuora__BillToName__c = billSoldName;
        ba.Zuora__SoldToName__c = billSoldName;
        insert ba;
        System.debug('BillingAccount Id created:'+ba.Id);
        return ba;
        }

    public static Contact CreateContact(string name, Id accId){
        Contact con = new Contact();
        con.AccountId = accId;
        con.Email = 'yaniv.segev@kerensen.com';
        con.LastName = name;
        con.FirstName = name;
        insert con;
        System.debug('Contact Id created:'+con.Id);
        return con;
    }

        public static Zuora__Subscription__c CreateSubscription(string name, Id accId, Id billAccId){
        Zuora__Subscription__c sub = new Zuora__Subscription__c();
        sub.Zuora__Account__c = accId;
        sub.Zuora__CustomerAccount__c = billAccId;
        insert sub;
        System.debug('Subscription Id created:'+sub.Id);
        return sub;
        }

        public static zqu__Quote__c createZQuote(Id accId) 
        {
        zqu__Quote__c zquote = new zqu__Quote__c();
        zquote.zqu__Account__c = accId;
        zquote.zqu__ZuoraAccountID__c = '2c92c0f84b07957f014b11cbaac6685e';
        zquote.zqu__ZuoraSubscriptionID__c = '2c92c0f84b07957f014b2bbde6254c52';
        zquote.zqu__Status__c = 'new';
        insert zquote;
        System.debug('zquote Id created:'+zquote.Id);
        return zquote;
        }

}