public without sharing class AP_PRMCFL_UploadDeviceController {
    public  String inputValue { get; set; }  
    public  String myProfileName { get; set; }      
    public  String title { get; set; }   
    public  String description { get; set; }   
    public  String characteristic { get; set; }   
    //public  String tagcsv { get; set; }   
    public  String username { get; set; }   
    public  String companyname { get; set; }   
    public  String productPartNumber { get; set; }   
    public  String otherBrand { get; set; }   
    public  String otherProduct { get; set; }   
    /*
    public  Boolean isValidatedBySE { 
        get {
            return false;
        } 
        set {
            isValidatedBySE = false;
        } 

    } */  
    public  Boolean isPartnerUser { get; set; }   

    public List<SelectOption> countrySelectList {get; set;}
    //--- --> value is not set with dependant picklist //Strange exception appearing in VF page
    public String countrySelected  { get; set; }   
        
    public List<SelectOption> productBrandSelectList {get; set;}
    public  String productBrandSelected {get; set;}

    public List<SelectOption> deviceTypeSelectList {get; set;}
    public  String deviceTypeSelected { get; set; }   
    
    public List<SelectOption> documentCategorySelectList {get; set;}
    public String documentCategorySelected { get; set; }
    
    public List<SelectOption> sharingSelectList {get; set;}
    public  String sharingSelected {get; set;}


    public List<ContentVersion> getContentVersions() {
        return [select id, Title, Description, FileType,
        Owner.Name, VersionNumber from ContentVersion
        Where IsLatest = true];
    }

    Map<String,List<String>> mapResults = null;

    //Quick & Dirty way to handle Other values -> beeing translated
    //Need to change code for new added country
    public List<SelectOption> getBrandByCountry() {        
        List<SelectOption> brands = new List<SelectOption>();
        System.debug('>>> countrySelected1=' + countrySelected); 
    

        if (countrySelected=='FR') {
            for (String brand:mapResults.get('FR')) {
                if (brand=='Autre') {
                    brands.add(new SelectOption('Other',brand));        
                    } else {
                    brands.add(new SelectOption(brand,brand));            
                    }
                    
            }    
        } else
        if (countrySelected=='US') {
             for (String brand:mapResults.get('US')) {
                if (brand=='Autre') {
                    brands.add(new SelectOption('Other',brand));        
                    } else {
                    brands.add(new SelectOption(brand,brand));            
                    }
            }    
       } 

        return brands;
    }

    //Quick & Dirty way
    public List<SelectOption> productBrandSelectList() {        
        List<SelectOption> productBrandSelectList = new List<SelectOption>();        
        Schema.DescribeFieldResult describeFieldResult_ProductManufacturer = ContentVersion.Product_Manufacturer__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry_ProductManufacturer = describeFieldResult_ProductManufacturer.getPicklistValues();
        Set<String> mySetFR = new Set<String>();
        mySetFR.addAll(mapResults.get('FR'));
        Set<String> mySetUS = new Set<String>();
        mySetUS.addAll(mapResults.get('US'));
        for( Schema.PicklistEntry ple : picklistEntry_ProductManufacturer)
        {            

            if (countrySelected=='FR') {                
                if (mySetFR.contains(ple.getValue())) {
                    productBrandSelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));        
                }                
            } else
            if (countrySelected=='US') {                
                if (mySetUS.contains(ple.getValue())) {
                    productBrandSelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));        
                }                
            }            
        }
        return productBrandSelectList;
    }    






    public AP_PRMCFL_UploadDeviceController () {
        //isValidatedBySE = false;
        //deviceTypeSelected = 'Other';
        //productBrandSelected = 'Other';

        if (UserInfo.getLanguage().startsWithIgnoreCase('FR')) {
          countrySelected = 'FR';
        } else countrySelected = 'US';

        System.debug('**** Step1 - GetDependentOptions and build dataFR and dataUS');
        mapResults = GetDependentOptions('ContentVersion', 'Content_Country__c', 'Product_Manufacturer__c');
        System.debug('dataFR=' + mapResults.get('FR'));
        System.debug('dataUS=' + mapResults.get('US'));        
        

        //to be clarified with business - NOT working as expected
        /*
        ID contactId = [Select contactid from User where id =: Userinfo.getUserid() limit 1].contactId;
        if (contactId!=null) {
            ID accID  = [Select AccountID from Contact where id =: contactId limit 1].AccountId;    
            if (accID != null) {
                isPartnerUser  = [Select isPartner from Account where id =: accID limit 1].isPartner;    
            } else isPartnerUser = false;
            
        }*/
        //By default isPartnerUser = true, set it to false give the ability to check the checkbox 'validatedBySchneider' ==> strong power
        isPartnerUser = true;
        if (Userinfo.getUserType()=='PowerPartner') {
                isPartnerUser  = true;
            } else if (Userinfo.getUserType()=='Standard') { 
                isPartnerUser = false;
            } else {
                isPartnerUser = true;    
        }
        
            
           
       
        Profile profile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        myProfileName = profile.name;
        

        System.debug('*** AP_PRMCFL_UploadDeviceController');
        username = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        companyname = UserInfo.getOrganizationName();

        contentToUpload = new ContentVersion();
        RecordType recordType = [SELECT Id From RecordType WHERE Name = 'Partners - Com\'X' and SobjectType='ContentVersion' and isActive=true];
        contentToUpload.RecordTypeId =  recordType.Id;

        deviceTypeSelectList = New List<SelectOption>();
        Schema.DescribeFieldResult describeFieldResult_ProductBrand = ContentVersion.Product_range__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry = describeFieldResult_ProductBrand.getPicklistValues();
        for( Schema.PicklistEntry ple : picklistEntry)
        {
            deviceTypeSelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));
        }  
        
        //-------------------------------------------------------------------------
        //TODO: Got the value based on record type COMX - Need to mix with Ajax Call in VFP
        // https://developer.salesforce.com/docs/atlas.en-us.ajax.meta/ajax/sforce_api_ajax_more_samples.htm
        // describeLayout Example: 
        // https://www.interactiveties.com/blog/archive/mimic-lookup.php#.VgK71N-qpBc
        //DescribeLayoutResult result = binding.describeLayout("Account", new string[] { "01230000000xxXxXXX" } );
        //PicklistEntry[] values = result.recordTypeMappings[0].picklistsForRecordType[12345].picklistValues;
        //https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_calls_describelayout_describelayoutresult.htm
        //http://www.tgerm.com/2012/01/recordtype-specific-picklist-values.html
        //-------------------------------------------------------------------------
        documentCategorySelectList = New List<SelectOption>();
        Schema.DescribeFieldResult describeFieldResult_DocumentCategory = ContentVersion.DocumentCategory__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry_DocumentCategory = describeFieldResult_DocumentCategory.getPicklistValues();
        for( Schema.PicklistEntry ple : picklistEntry_DocumentCategory)
        {
            if ((ple.getValue()=='Com\'X Model') || (ple.getValue()=='Documentation')) {
                documentCategorySelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));    
            }
            
        }  

        countrySelectList = New List<SelectOption>();
        Schema.DescribeFieldResult describeFieldResult_Content_Country = ContentVersion.Content_Country__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry_Content_Country = describeFieldResult_Content_Country.getPicklistValues();
        for( Schema.PicklistEntry ple : picklistEntry_Content_Country)
        {
            countrySelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));
        }  

        //Version Original
        /*
        productBrandSelectList = New List<SelectOption>();
        Schema.DescribeFieldResult describeFieldResult_ProductManufacturer = ContentVersion.Product_Manufacturer__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry_ProductManufacturer = describeFieldResult_ProductManufacturer.getPicklistValues();
        for( Schema.PicklistEntry ple : picklistEntry_ProductManufacturer)
        {            
            productBrandSelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));
        } */ 


        productBrandSelectList = New List<SelectOption>();
        Schema.DescribeFieldResult describeFieldResult_ProductManufacturer = ContentVersion.Product_Manufacturer__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry_ProductManufacturer = describeFieldResult_ProductManufacturer.getPicklistValues();
        Set<String> mySet = new Set<String>();
        for( Schema.PicklistEntry ple : picklistEntry_ProductManufacturer)
        {            

            if (countrySelected=='FR') {
                mySet.addAll(mapResults.get('FR'));
                if (mySet.contains(ple.getValue())) {
                    productBrandSelectList.add(new SelectOption(ple.getValue(),ple.getLabel())); 
                    System.debug('**** Step3.1 - productBrandSelectList use dataFR');      
                }                
            } else
            if (countrySelected=='US') {
                mySet.addAll(mapResults.get('US'));
                if (mySet.contains(ple.getValue())) {
                    productBrandSelectList.add(new SelectOption(ple.getValue(),ple.getLabel()));        
                    System.debug('**** Step3.2 - productBrandSelectList use dataUS');      
                }                
            }            
        }


        sharingSelectList = New List<SelectOption>();
        sharingSelectList.add(new SelectOption(Label.CLOCT15CF022,'Com\'X Collaborative Lib'));
    }

      public Document  document {
        get {
          if (document == null)
            document = new Document ();
          return document;
        }
        set;
      }
 



   public ContentVersion contentToUpload {get;set;}

   public Blob fileContent {get;set;}



    public PageReference uploadContents() {

        try {

            System.debug('*** uploadContents Starts');
            //By default, title == filemane
            //title = contentToUpload.PathOnClient;
            if(title == '' || title == null) {
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,Label.CLOCT15CF023));
               return null;
            }
            
            if(contentToUpload.PathOnClient == '' || contentToUpload.PathOnClient == null) {
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,Label.CLOCT15CL024));
               return null;
            }
            sharingSelected = Label.CLOCT15CF022;
            

            List<ContentWorkSpace> CWList = [SELECT Id, Name From ContentWorkspace WHERE Name = 'Com\'X Collaborative Library'];
            contentToUpload.description = description;
            contentToUpload.VersionData = fileContent;

            contentToUpload.Title = title;
            
            contentToUpload.DocumentCategory__c = documentCategorySelected;
            contentToUpload.Content_Country__c = countrySelected;
            contentToUpload.Product_Part_Number__c = productPartNumber;
            //contentToUpload.Validated_by_Schneider__c = isValidatedBySE;
            contentToUpload.Characteristic__c = characteristic;
            System.debug('------------------------------------------------------------------------------------------------------');
            System.debug('*** Marque/Brand=' + productBrandSelected + ' Autre Marque/Brand=' + otherBrand);
            System.debug('*** Type Appareil/Device Type=' + deviceTypeSelected + ' Autre Type Appareil/Device Type=' + otherProduct);
                        
            contentToUpload.Product_Manufacturer__c = productBrandSelected;                
             if (productBrandSelected == 'Other') {
                contentToUpload.Product_Brand_New_Value__c = otherBrand ;                     
            }

            contentToUpload.Product_range__c =  deviceTypeSelected;                
            if (deviceTypeSelected == 'Other') {
                contentToUpload.Device_Type_New_Value__c = otherProduct ;                                
            }
            
            //contentToUpload.Device_Type_New_Value__c = null ; 
            insert contentToUpload;
            
            contentToUpload = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentToUpload.Id];
            ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
            cwd.ContentDocumentId = contentToUpload.ContentDocumentId;
            cwd.ContentWorkspaceId = CWList.get(0).Id;
            insert cwd;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,Label.CLOCT15CF021));
            System.debug('-----uploadContents End-----');        
            PageReference pg = new PageReference('/apex/VFP_PRMCFL_AfterUpload');
            pg.setRedirect(true);
            return pg;

        } catch(System.DmlException e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
                return null;
            }
            catch(exception e) {
            System.debug('-----Exception:' + e.getMessage());        
            //ApexPages.addMessages(e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, e.getMessage()));
            //ApexPages.addMessages(e);
            return null;
            
        }
        
        
        }

        public PageReference redirectPageLogin(){
            system.debug('redirectPageLogin UserInfo.getSessionId(): ' + UserInfo.getSessionId());
            String sessionId = UserInfo.getSessionId();
            if ((sessionId==null) || (sessionId=='')) {   
                return new pageReference('/collaboration');    
            } else return null;

        }



    // ******************************** GetDependentOptions ********************************
    // based originally on:
    // http://titancronus.com/blog/2014/07/03/acquiring-dependent-picklists-in-apex-contd/
    // *************************************************************************************
    // Converts a base64 string into a list of integers representing the encoded bytes
    public  List<Integer> B64ToBytes (String sIn) {
        Map<Integer,Integer> base64 = new Map<Integer,Integer>{65=>0,66=>1,67=>2,68=>3,69=>4,70=>5,71=>6,72=>7,73=>8,74=>9,75=>10,76=>11,77=>12,78=>13,79=>14,80=>15,81=>16,82=>17,83=>18,84=>19,85=>20,86=>21,87=>22,88=>23,89=>24,90=>25
                                                               ,97=>26,98=>27,99=>28,100=>29,101=>30,102=>31,103=>32,104=>33,105=>34,106=>35,107=>36,108=>37,109=>38,110=>39,111=>40,112=>41,113=>42,114=>43,115=>44,116=>45,117=>46,118=>47,119=>48,120=>49,121=>50,122=>51
                                                               ,48=>52,49=>53,50=>54,51=>55,52=>56,53=>57,54=>58,55=>59,56=>60,57=>61,43=>62,47=>63};

        List<Integer> lstOut = new List<Integer>();
        if ( sIn == null || sIn == '' ) return lstOut;
        
        sIn += '='.repeat( 4 - Math.mod( sIn.length(), 4) );

        for ( Integer idx=0; idx < sIn.length(); idx += 4 ) {
            if ( base64.get(sIn.charAt(idx+1)) != null ) lstOut.add( (base64.get(sIn.charAt(idx)) << 2) | (base64.get(sIn.charAt(idx+1)) >>> 4) );
            if ( base64.get(sIn.charAt(idx+2)) != null ) lstOut.add( ((base64.get(sIn.charAt(idx+1)) & 15)<<4) | (base64.get(sIn.charAt(idx+2)) >>> 2) );
            if ( base64.get(sIn.charAt(idx+3)) != null ) lstOut.add( ((base64.get(sIn.charAt(idx+2)) & 3)<<6) | base64.get(sIn.charAt(idx+3)) );
        }

        //System.Debug('B64ToBytes: [' + sIn + '] = ' + lstOut);
        return lstOut;
    }//B64ToBytes
    public  List<Integer> BlobToBytes (Blob input) {
        return B64ToBytes( EncodingUtil.base64Encode(input) );
    }//BlobToBytes

    // Converts a base64 string into a list of integers indicating at which position the bits are on
    public  List<Integer> cnvBits (String b64Str) {
        List<Integer> lstOut = new List<Integer>();
        if ( b64Str == null || b64Str == '' ) return lstOut;

        List<Integer> lstBytes = B64ToBytes(b64Str);

        Integer i, b, v;
        for ( i = 0; i < lstBytes.size(); i++ ) {
            v = lstBytes[i];
            //System.debug ( 'i['+i+'] v['+v+']' );
            for ( b = 1; b <= 8; b++ ) {
                //System.debug ( 'i['+i+'] b['+b+'] v['+v+'] = ['+(v & 128)+']' );
                if ( ( v & 128 ) == 128 ) lstOut.add( (i*8) + b );
                v <<= 1;
            }
        }

        //System.Debug('cnvBits: [' + b64Str + '] = ' + lstOut);
        return lstOut;
    }//cnvBits

    public class TPicklistEntry{
        public string active {get;set;}
        public string defaultValue {get;set;}
        public string label {get;set;}
        public string value {get;set;}
        public string validFor {get;set;}
        public TPicklistEntry(){
        }
    }//TPicklistEntry
     
    public  Map<String,List<String>> GetDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName) {
        Map<String,List<String>> mapResults = new Map<String,List<String>>();

        //verify/get object schema
        Schema.SObjectType pType = Schema.getGlobalDescribe().get(pObjName);
        System.debug('*** pType=' + pType);

        if ( pType == null ) return mapResults;
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        System.debug('*** objFieldMap=' + objFieldMap);

        //verify field names
        if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName)) return mapResults;     

        //get the control & dependent values   
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();

        //clear heap
        objFieldMap = null;

        //initialize results mapping
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){     
            mapResults.put( ctrl_ple[pControllingIndex].getLabel(), new List<String>());
        }
        //cater for null and empty
        mapResults.put('', new List<String>());
        mapResults.put(null, new List<String>());

        //serialize dep entries        
        List<TPicklistEntry> objDS_Entries = new List<TPicklistEntry>();
        objDS_Entries = (List<TPicklistEntry>)JSON.deserialize(JSON.serialize(dep_ple), List<TPicklistEntry>.class);
        
        System.debug('*** objDS_Entries=' + objDS_Entries);

        List<Integer> validIndexes;
        for (TPicklistEntry objDepPLE : objDS_Entries){

            validIndexes = cnvBits(objDepPLE.validFor);
            //System.Debug('cnvBits: [' + objDepPLE.label + '] = ' + validIndexes);

            for (Integer validIndex : validIndexes){                
            System.debug('*** Label=' + objDepPLE.label);
            System.debug('*** Value=' + objDepPLE.value);

                mapResults.get( ctrl_ple[validIndex-1].getLabel() ).add( objDepPLE.label );
            }
        }

        //clear heap
        objDS_Entries = null;

        return mapResults;
    }//GetDependentOptions



}