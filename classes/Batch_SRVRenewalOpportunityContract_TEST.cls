@isTest
public class Batch_SRVRenewalOpportunityContract_TEST{

     static testMethod void test_SRVTechRenOppCreat(){
         SVMXC__Service_Contract__c sc= new SVMXC__Service_Contract__c(name='test sc',Tech_RenewalOpportunityCreat__c = true);
        insert sc;
             
     Test.startTest();
         Batch_SRVRenewalOpportunityContract bac = new Batch_SRVRenewalOpportunityContract();
         bac.execute( (Database.BatchableContext) null, new List<SVMXC__Service_Contract__c>{ sc } );         
         Batch_SRVRenewalOpportunityContract SRVbatch= new Batch_SRVRenewalOpportunityContract();
         ID batchSRVid=Database.executeBatch(SRVbatch);        
         string CORN_EXP = '0 0 0 1 4 ?';               
         string jobid4 = system.schedule('my batch job4', CORN_EXP, new Batch_SRVRenewalOpportunityContract());
         CronTrigger ct4 = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobid4];     
     Test.stopTest();
        
    }
 }