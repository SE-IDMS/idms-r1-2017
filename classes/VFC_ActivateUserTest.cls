// test class for VFC_ActivateUser 
@isTest

public class VFC_ActivateUserTest{
    //test method for success case
    static testmethod void activateUserActionTest(){
        User useract = new User(alias = 'useract', email='useract' + '@accenture.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='useract' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'UIMS',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert useract ;
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        system.runAs(userAct){
            PageReference pRef = Page.ActivateUser;
            pRef.getParameters().put('id', useract.id);
            pRef.getParameters().put('app', 'Heroku');
            Test.setCurrentPage(pRef);     
            
            VFC_ActivateUser activateu = new VFC_ActivateUser();
            activateu.activateUserAction(); 
        }
    }
    
}