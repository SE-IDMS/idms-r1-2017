public class VFC_updateProgramLevelBrand {
    
    public Boolean hasError{get;set;}
    public SelectOption[] allBrands{get;set;}
    public SelectOption[] selBrands{get;set;}
    public ProgramLevel__c progLevel {get;set;} 
    public class CodeCoverageException extends Exception {
        public String message = 'This exception is thrown to do code coverage';
    }

    public static Boolean throwExceptionInTest;
    
    public VFC_updateProgramLevelBrand(ApexPages.StandardController controller) {
        
        progLevel = (ProgramLevel__c)controller.getRecord();
        System.debug('**progLevel**'+progLevel);
        
        allBrands = new List<SelectOption>();
        selBrands = new List<SelectOption>();
        List<Brand__c> brandLst = new List<Brand__c>([Select Name From Brand__c WHERE IsAssociatedSchneiderBrand__c = true]);
        System.debug('**Brand List**'+brandLst);
        
        
        for(Brand__c b: brandLst)
            allBrands.add(new SelectOption(b.Id,b.Name));
            
        List<ProgramLevelBrand__c> progLvlBrand = new List<ProgramLevelBrand__c>([SELECT Brand__c,Brand__r.Name FROM ProgramLevelBrand__c WHERE ProgramLevel__c=:progLevel.Id]);
        for(ProgramLevelBrand__c pLB: progLvlBrand)
            selBrands.add(new SelectOption(pLB.Brand__c, pLB.Brand__r.Name));
            
        System.debug('**Selected Brands**'+selBrands);
        
        
    }//End of Constructor
    
    public PageReference Save() {
        Savepoint sp ;
        try { 
             if(throwExceptionInTest == False && Test.isRunningTest()){
             throw new CodeCoverageException('Exception to do the code coverage');
            }  
            
            List<ProgramLevelBrand__c> progLvlBrandLst = new List<ProgramLevelBrand__c>();
            
            List<ProgramLevelBrand__c> progLvlBrandExtLstDel = [Select Id FROM ProgramLevelBrand__c WHERE ProgramLevel__c=: progLevel.Id];
            System.debug('** Program Level Brand to Delete **'+progLvlBrandExtLstDel);

            System.debug('**Selected Brands**'+selBrands);
            for(SelectOption so : selBrands) {
                ProgramLevelBrand__c progLvlBrand = new ProgramLevelBrand__c(Brand__c=so.getValue(),ProgramLevel__c=progLevel.Id);
                progLvlBrandLst.add(progLvlBrand);                     
            }
            
            sp =  Database.setSavepoint();
            If(!progLvlBrandExtLstDel.isEmpty())
                Delete progLvlBrandExtLstDel;
                
            System.debug('** Program Level Brand to INSert **'+progLvlBrandLst);
            INSERT progLvlBrandLst;
                
        }
        catch(Exception e) {
            if (sp!=null) Database.rollback(sp);

            hasError = true;     
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage(),'')); System.debug(e.getMessage()); 
            return null;
        }
        return new pagereference('/'+progLevel.Id); 
    }

}//End of class