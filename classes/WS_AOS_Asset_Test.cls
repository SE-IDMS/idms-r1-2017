@isTest

private class WS_AOS_Asset_Test {
    
    
    static testMethod void AOS_Asset_method() {
        List<WS_AOS_Asset.InstalledproductResult> iplist = new List<WS_AOS_Asset.InstalledproductResult>();
        
        
        SVMXC__Installed_Product__c ip =  new SVMXC__Installed_Product__c(ToBeSentForUpdate__c=true,Remote_System__c='AOS',GoldenAssetId__c='GoledenAssertId12345');
        insert ip;
        
        WS_AOS_Asset.InstalledproductResult ipres= new WS_AOS_Asset.InstalledproductResult();
               ipres.BFOInstalledProductID=ip.id;
               ipres.GoldenID=ip.GoldenAssetId__c;
               ipres.ToBeSentForUpdate=true;
          iplist.add(ipres);
        WS_AOS_Asset.getAOSAssets();
        WS_AOS_Asset.updateAOSAssets(iplist);
        
    }

}