Public Class Utils_WebServiceAdministration
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/

    public String SessionID{get;set;}
    public WebServiceClass SimplePA{get;set;}
    public WebServiceClass OrderStatus{get;set;} 
    public WebServiceClass OrderDetails{get;set;}
    public WebServiceClass GMR{get;set;}
    public WebServiceClass Inquira{get;set;}
    public WebServiceClass InTouch_Asset {get;set;}
    public WebServiceClass InTouch_WON {get;set;}
    public List<WebServiceClass> WSList;
            
    /*=======================================
      INNER CLASSES
    =======================================*/   

    virtual public class WebServiceClass
    {
        public string Name{get;set;}
        public string Status{get;set;}
        public string Issue{get;set;} 
        public string URLCustomLabel{get;set;}
        public Exception relatedException{get;set;}
        public string Endpoint{get;set;}
        public string NewEndpoint{get;set;}
        public Boolean EditMode{get;set;}
        
        
        
        WebServiceClass()
        {
            EditMode = false;
        }
                
        public virtual void Test(){}
        
        public void EditURL()
        {
            EditMode=true;
            NewEndpoint = Endpoint;
        }
        
        public void SaveURL()
        {
            Endpoint = NewEndpoint; 
            EditMode=false;   
        }
    }
    
    public class GMR_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();

            List<String> KeyWords = new List<String>();
            List<String> Filters = new List<String>();
              
            // Instanciate the Web Service and its inner classes
            WS04_GMR.GMRSearchPort GMRConnection = new WS04_GMR.GMRSearchPort();    
            
            GMRConnection.endpoint_x =  Endpoint;
            GMRConnection.timeout_x = 40000;
            GMRConnection.ClientCertName_x = Label.CL00616;
            
            WS04_GMR.criteriaInDataBean Criteria = new WS04_GMR.criteriaInDataBean();
            WS04_GMR.paginationInDataBean Pagination = new WS04_GMR.paginationInDataBean();
            
            // Set pagination parameters
            Pagination.maxLimitePerPage = 25;
            Pagination.pageNumber = 1;
            
            // Set search keywords and criteria    
            Criteria.keyWordList = KeyWords;
            
            if(Filters.size()>=1 && Filters[0] != Label.CL00355)          
                Criteria.businessLine = Filters[0];    
            if(Filters.size()>=2 && Filters[1] != Label.CL00355) 
                Criteria.productLine = Filters[1].substring(2,4);
            if(Filters.size()>=3 && Filters[2] != Label.CL00355)
                Criteria.strategicProductFamily = Filters[2].substring(4,6);
            if(Filters.size()>=4 && Filters[3] != Label.CL00355)
                Criteria.Family = Filters[3].substring(6,8);
              
            try
            {
                WS04_GMR.resultDataBean Results = GMRConnection.globalSearch(Criteria,Pagination);
               
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                Issue = e.getMessage();
            }  
        }
    }
    
    public class Inquira_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();
            
            WS_Inquira.localeBean locale = new WS_Inquira.localeBean();
            locale.isoCountry   = 'FR';
            locale.isoLanguage  = 'fr';
        
            WS_Inquira.faqCKMSearchResponse objSearchFaqResponse = new WS_Inquira.faqCKMSearchResponse();
            
            WS_Inquira.FaqCKMServiceImplPort InquiraConnection = new WS_Inquira.FaqCKMServiceImplPort();
            InquiraConnection.endpoint_x         = Endpoint;
            InquiraConnection.ClientCertName_x   =Label.CLMAY14CCC23;
            InquiraConnection.timeout_x = Integer.valueOf(System.Label.CLMAY13CCC39);
            
            String userID = UserInfo.getUserName();
            userID = (userID != null && userID.contains('@')) ? userID.Left(userID.indexOf('@')).toUpperCase() : userID;   
        
            
            String strKeyword='ATV21';
            String strBUEntity = '';
            
            try{
                objSearchFaqResponse = InquiraConnection.searchFaq(strKeyword,strBUEntity,locale,userID);
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                Issue = e.getMessage();
                
                if(Issue == 'IO Exception: Read timed out')
                    Status = 'Timeout';
            }   
        }
    } 
 
    private final String certName = 'Test_Cert';
    
    public class OrderStatus_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();

            WS06_OrderList.orderStatusListOrderDataBean ListOrder = new WS06_OrderList.orderStatusListOrderDataBean();
                    
            ListOrder.column = 'RELEASE_DATE'; 
            ListOrder.OrderBy = 'DESCENDING'; 
            
            List<WS06_OrderList.orderStatusListOrderDataBean> OrderByList = new List<WS06_OrderList.orderStatusListOrderDataBean>();
            OrderByList.add(ListOrder);
            
            WS06_OrderList.orderStatusListSearchDataBean OrderSearchCriteria = new WS06_OrderList.orderStatusListSearchDataBean();
             
            OrderSearchCriteria.bfoAccountId = 'TEST'; 
            OrderSearchCriteria.catalogNumber = 'TEST';    
            OrderSearchCriteria.partialOrderNumber = 'TEST';
            OrderSearchCriteria.partialPONumber = 'TEST';
            OrderSearchCriteria.rangeEnd = 50; // TO DO
            OrderSearchCriteria.rangeStart = 0; // TO DO
            OrderSearchCriteria.releaseDateFrom = DateTime.now();
            OrderSearchCriteria.releaseDateTo = DateTime.now();
            OrderSearchCriteria.sdhLegacyPSName = new List<String>();
            OrderSearchCriteria.sdhLegacyPSName.add('TEST');
            OrderSearchCriteria.orderByList = OrderByList;
                 
            // ---- WEB SERVICE CALLOUT -----
            WS06_OrderList.OrderStatusListPort OrderConnector = new WS06_OrderList.OrderStatusListPort();
            OrderConnector.endpoint_x = Endpoint;
            OrderConnector.ClientCertName_x = Label.CL00616;
            
            WS06_OrderList.orderStatusResult WebServiceResults = new WS06_OrderList.orderStatusResult();
            
            try
            {
                WebServiceResults = OrderConnector.orderStatusList(OrderSearchCriteria); 
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                relatedException = e;
                Issue = e.getMessage();
                
                if(Issue == 'IO Exception: Read timed out')
                    Status = 'Timeout';
            }   
        }
    }         

    public class OrderDetails_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();

             // --- PRE TREATMENT ----   
             WS07_OrderDetails.orderDetailSearchDataBean OrderDetailSearchCriteria = new WS07_OrderDetails.orderDetailSearchDataBean();
             OrderDetailSearchCriteria.bfoAccountId = '001K0000003rhReIAI';
             OrderDetailSearchCriteria.orderNumber = '6006046935';
             OrderDetailSearchCriteria.sdhLegacyPSName = 'TU_CRM';
                      
             // ---- WEB SERVICE CALLOUT -----         
             WS07_OrderDetails.OrderStatusDetailPort OrderDetailConnector = new WS07_OrderDetails.OrderStatusDetailPort();
             WS07_OrderDetails.orderStatusDetailResult OrderDetailsResults = new WS07_OrderDetails.orderStatusDetailResult();
             OrderDetailConnector.endpoint_x = Endpoint;
             OrderDetailConnector.ClientCertName_x = Label.CL00616;
             
            try
            {
                OrderDetailsResults = OrderDetailConnector.orderDetail(OrderDetailSearchCriteria );   
                // System.debug('### GGA : Code : ' + OrderDetailConnector.outputHttpHeaders_x.get('Code'));
                System.debug('#### ORDER DETAILS RESULT: ' + OrderDetailsResults);
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                relatedException = e;
                Issue = e.getMessage();
                
                if(Issue == 'IO Exception: Read timed out')
                    Status = 'Timeout';
            }     

        }
    } 
    
    public class SimplePA_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();

            // --- PRE TREATMENT ----
            WS08_SimplePA.priceAndAvailabilitySearchDataBean PASearchCriteria = new WS08_SimplePA.priceAndAvailabilitySearchDataBean();
            PASearchCriteria.bfoAccountId = 'TEST';
            PASearchCriteria.catalogNumber = 'TEST';
            PASearchCriteria.priceDate = DateTime.now();
            PASearchCriteria.quantity = 5;
            PASearchCriteria.sdhLegacyPSName = 'TEST';
                 
            // ---- WEB SERVICE CALLOUT -----    
            WS08_SimplePA.PriceAndAvailabilityPort PAConnector = new WS08_SimplePA.PriceAndAvailabilityPort();
            PAConnector.Endpoint_x = Endpoint;
            PAConnector.ClientCertName_x = Label.CL00616;
            
            WS08_SimplePA.priceAndAvailabilityResult  PA_WS = new WS08_SimplePA.priceAndAvailabilityResult ();
            
            try
            {
                PA_WS = PAConnector.priceAndAvailability(PASearchCriteria);    
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                relatedException = e;
                Issue = e.getMessage();
                
                if(Issue == 'IO Exception: Read timed out')
                    Status = 'Timeout';
            } 
        }   
    }
    
    public class InTouchAsset_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();

            List<String> KeyWords = new List<String>();
            List<String> Filters = new List<String>();
              
            // Instanciate the Web Service and its inner classes
            WS_InTouch_AssetService.ITBCCConnectorPort InTouchConnection = new WS_InTouch_AssetService.ITBCCConnectorPort();
            
            InTouchConnection.endpoint_x=Endpoint;
            InTouchConnection.timeout_x=Integer.valueOf(System.Label.CLMAY13CCC39);
            InTouchConnection.ClientCertName_x=Label.CL00616;
            
            WS_InTouch_AssetService.searchAssetInDataBean searchAssetCriteria = new WS_InTouch_AssetService.searchAssetInDataBean();
            WS_InTouch_AssetService.simpleAccountInDataBean searchAccountCriteria = new WS_InTouch_AssetService.simpleAccountInDataBean();
                
            searchAssetCriteria.owningAccount                           =searchAccountCriteria;
              
            try
            {
                WS_InTouch_AssetService.assetReponseDataBean inTouchAssetSearchResult = InTouchConnection.searchAsset(searchAssetCriteria);
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                Issue = e.getMessage();
            }  
        }
    }
    
    public class InTouchWON_WS extends WebServiceClass
    {
        public override void Test()
        {
            if(EditMode)
                SaveURL();

            List<String> KeyWords = new List<String>();
            List<String> Filters = new List<String>();
              
            // Instanciate the Web Service and its inner classes
            
            WS_InTouch_WONService.BFOWONPort BFOWONConnection = new WS_InTouch_WONService.BFOWONPort();

            BFOWONConnection.endpoint_x=Endpoint;
            BFOWONConnection.timeout_x=Integer.valueOf(System.Label.CLMAY13CCC39);
            BFOWONConnection.ClientCertName_x=Label.CL00616;
            
            WS_InTouch_WONService.createWONInDataBean bFOWONDataBean = new WS_InTouch_WONService.createWONInDataBean();
                          
            try
            {
                WS_InTouch_WONService.createWONDataBean inTouchActivitiesResult = BFOWONConnection.createSRForService(bFOWONDataBean);
                Status = 'Ok'; 
            }
            catch(Exception e)
            {
                Status = 'Fail';
                Issue = e.getMessage();
            }  
        }
    }
       

    /*=======================================
      CONSTRUCTOR
    =======================================*/   
    
    public Utils_WebServiceAdministration()
    {
        SessionID = UserInfo.getSessionID();
        WSList = new List<WebServiceClass>();
        
        SimplePA = new SimplePA_WS();
        SimplePA.Name = 'Simple P&A';
        SimplePA.Endpoint =  Label.CL00566;
        SimplePA.URLCustomLabel = 'CL00566';
        WSList.add(SimplePA);
        
        OrderStatus = new OrderStatus_WS();
        OrderStatus.Name = 'Order Status';
        OrderStatus.Endpoint = Label.CL00564;
        OrderStatus.URLCustomLabel = 'CL00564';
        WSList.add(OrderStatus);
        
        OrderDetails = new OrderDetails_WS();
        OrderDetails.Name = 'Order Details';       
        OrderDetails.Endpoint =  label.CL00565;
        OrderDetails.URLCustomLabel ='CL00565';
        WSList.add(OrderDetails);
        
        GMR = new GMR_WS();
        GMR.Name = 'GMR';
        GMR.Endpoint = Label.CL00376;
        GMR.URLCustomLabel = 'CL00376';
        WSList.add(GMR);
        
                
        Inquira = new Inquira_WS();
        Inquira.Name = 'Inquira'; 
        /*  
        Inquira.URLCustomLabel = 'CL00372';
        Inquira.Endpoint = Label.CL00372; 
        */
        Inquira.URLCustomLabel = 'CLMAY14CCC25';
        Inquira.Endpoint = Label.CLMAY14CCC25;
        WSList.add(Inquira);
        
        InTouch_Asset = new InTouchAsset_WS();
        InTouch_Asset.Name = 'InTouch Asset';
        InTouch_Asset.Endpoint = Label.CLMAY13CCC06;
        InTouch_Asset.URLCustomLabel = 'CLMAY13CCC06';
        WSList.add(InTouch_Asset);

        InTouch_WON = new InTouchWON_WS();
        InTouch_WON.Name = 'InTouch WON';
        InTouch_WON.Endpoint = Label.CLMAY13CCC26;
        InTouch_WON.URLCustomLabel = 'CLMAY13CCC26';
        WSList.add(InTouch_WON);
        

        
    }
    
    /*=======================================
      METHODS
    =======================================*/     
    
    public void TestAll()
    {
        For(WebServiceClass WS:WSList)
        {
            WS.Test();
        }
    }
}