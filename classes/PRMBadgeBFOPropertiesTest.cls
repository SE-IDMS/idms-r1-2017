@IsTest
public class PRMBadgeBFOPropertiesTest {

  static Integer testSize = 2;





  public static testMethod void testInsertDeletePRMBadgeBFOProperties(){
    FieloEE.MockUpFactory.setCustomProperties(false);
    User us = Utils_TestMethods.createStandardUser('123!');
    us.BypassVR__c=True;
    insert us;
        
    System.RunAs(us){
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        Integer zipCodeExistant = 92000;

        List<Account> oldAcc = PRM_FieldChangeListenerTEST.getAccountsInMass(testSize,france.Id,zipCodeExistant);
        
        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'MaarcoLast'+ String.ValueOf(DateTime.now().getTime());
        member.FieloEE__FirstName__c = 'MaarcoFirst';
        member.FieloEE__Street__c = 'tests';
        insert member;
    

        Account acc = oldAcc.get(0);
        Contact con = Utils_TestMethods.createContact(acc.Id, 'Contact');
        con.Country__c = france.Id;
        con.PRMUIMSID__c = 'prmUimsUnique1234';
        con.FieloEE__Member__c = member.Id;
        insert con;
        System.debug('Account acc ' + acc);
        System.debug('Contact con ' + con);
        System.debug('FieloEE__Member__c member ' + member);
        Test.startTest();
        List<PRMBadgeBFOProperties__c> propList1 = PRM_FieldChangeListenerTEST.insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Electrical_Contractor_ZIP_CODE_LIST','Account');
        System.debug('propList1 ' + propList1);
        propList1[0].AllowRetroCalculation__c = true;
        update propList1[0];
        List<MemberExternalProperties__c> listBadgeMem = [SELECT id FROM MemberExternalProperties__c WHERE Member__c =: member.Id];
        System.assertEquals(1,listBadgeMem.size());

        delete propList1[0];
        //listBadgeMem =  [SELECT id FROM MemberExternalProperties__c WHERE Member__c =: member.Id];
        //System.assertEquals(0,listBadgeMem.size());

        Test.stopTest();
    }   
  }


  public static testMethod void testUpdateBFOProperties(){
    Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
    insert france;
    Integer zipCodeNonExistant = 80000;
    Integer zipCodeExistant = 92000;

    List<Account> oldAcc = PRM_FieldChangeListenerTest.getAccountsInMass(testSize,france.Id,zipCodeExistant);
    oldAcc[0].PRMZipCode__c = oldAcc[0].PRMZipCode__c+'-50';
    update oldAcc;

    FieloEE.MockUpFactory.setCustomProperties(false);
    User us = Utils_TestMethods.createStandardUser('123!');
    us.BypassVR__c=True;
    insert us;
        
    System.RunAs(us){
        FieloEE__Badge__c badge = new FieloEE__Badge__c();
        badge.Name = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST';
        badge.F_PRM_Type__c = 'BFOProperties';
        badge.F_PRM_BadgeAPIName__c = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST'; 
        insert badge;

        FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
        badge2.Name = 'Proficient_Consulting_Engineer_ZIP_CODE_LIST';
        badge2.F_PRM_Type__c = 'BFOProperties';
        badge2.F_PRM_BadgeAPIName__c = 'Proficient_Consulting_Engineer_ZIP_CODE_LIST'; 
        insert badge2;

        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'MaarcoLast'+ String.ValueOf(DateTime.now().getTime());
        member.FieloEE__FirstName__c = 'MaarcoFirst';
        member.FieloEE__Street__c = 'tests';
        insert member;


        List<PRMBadgeBFOProperties__c> propList1 = PRM_FieldChangeListenerTEST.insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
        
        for(PRMBadgeBFOProperties__c prop: propList1){
          prop.BFOProperties__c = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST';
        }
        update propList1;
    }
  }

  public static testMethod void testUpdateBFOPropertiesContacts(){
    Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
    insert france;
    Integer zipCodeNonExistant = 80000;
    Integer zipCodeExistant = 92000;

    List<Account> oldAcc = PRM_FieldChangeListenerTest.getAccountsInMass(testSize,france.Id,zipCodeExistant);
    
    FieloEE.MockUpFactory.setCustomProperties(false);
    User us = Utils_TestMethods.createStandardUser('123!');
    us.BypassVR__c=True;
    insert us;
        
        System.RunAs(us){
        FieloEE__Badge__c badge = new FieloEE__Badge__c();
        badge.Name = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST';
        badge.F_PRM_Type__c = 'BFOProperties';
        badge.F_PRM_BadgeAPIName__c = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST'; 
        insert badge;



        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'MaarcoLast'+ String.ValueOf(DateTime.now().getTime());
        member.FieloEE__FirstName__c = 'MaarcoFirst';
        member.FieloEE__Street__c = 'tests';
        insert member;


        List<PRMBadgeBFOProperties__c> propList1 = PRM_FieldChangeListenerTEST.insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Electrical_Contractor_ZIP_CODE_LIST','Account');
    
        Account acc = oldAcc.get(0);
        Contact con = Utils_TestMethods.createContact(acc.Id, 'Contact');
        con.Country__c = france.Id;
        con.PRMUIMSID__c = 'prmUimsUnique1234';
        con.FieloEE__Member__c = member.Id;
        insert con;
        Test.startTest();
        List<FieloEE__BadgeMember__c> listBadgeMem = [SELECT id FROM FieloEE__BadgeMember__c WHERE FieloEE__Member2__c =: member.Id and FieloEE__Badge2__r.F_PRM_BadgeAPIName__c = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST'];
        //System.assertEquals(1,listBadgeMem.size());

        Account acc2 = PRM_FieldChangeListenerTest.getBusinessAccount('String name',12523,france.Id,251);
        acc2.PRMUIMSID__c ='anotherOne';
        insert acc2;
        con.AccountId = acc2.Id;
        update con;


        listBadgeMem = [SELECT id FROM FieloEE__BadgeMember__c WHERE FieloEE__Member2__c =: member.Id and FieloEE__Badge2__r.F_PRM_BadgeAPIName__c = 'Proficient_Electrical_Contractor_ZIP_CODE_LIST' ];
        //System.assertEquals(0,listBadgeMem.size());
        Test.stopTest();
    }
  }

}