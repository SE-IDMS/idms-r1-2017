/*
    Author          : Karthik Shivprakash (ACN) 
    Date Created    : 13/01/2011 
    Description     : Controller extension for Budget Form.This class renders component for VFP15_BudgetView VF page.
*/

public with sharing class VFC15_BudgetView {

    private PageReference redirectPage;
    public RIT_Budget__c budget;
    public List<RIT_Budget__c> budgetlst = new List<RIT_Budget__c>();
    public static Map<String,String> fieldLabel = New Map<String,String>();
    List<WrapperClass> listWrap = New List<WrapperClass>();
    
    //Constructor
    public VFC15_BudgetView(ApexPages.StandardController stdController)
    {
        budget = (RIT_Budget__c)stdController.getRecord();
        getAccountFields();
    }
    
    //This method deletes the corresponding Budget record
    public PageReference delButton()
    {
        if(budget!=null && budget.QuoteLink__c!=null)
        {
            budgetlst.add(budget);
            if(budgetlst.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CL00173));            
            else
            {        
                Database.DeleteResult[] ldr = Database.delete(budgetlst);
                for(Database.DeleteResult dr : ldr)
                {
                if(!dr.isSuccess())
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, dr.getErrors()[0].getMessage()));
                    }         
                }
                if(budget.QuoteLink__c!=null)
                redirectPage = new Pagereference(Page.VFP13_RITEForm.getURL()+'?id='+budget.QuoteLink__c);
            }
        }
        if(redirectPage != null)
            return redirectPage;
        else 
            return null;
    }
    
    //This method redirects the user to RITE form on click of "Back to RITE form" button.
    public PageReference backToRiteForm()
    {
        if(budget!=null && budget.QuoteLink__c!=null)
        {
            redirectPage = new Pagereference(Page.VFP13_RITEForm.getURL()+'?id='+budget.QuoteLink__c);
        }
        if(redirectPage != null)
            return redirectPage;
        else 
            return null;
    }
    
    //Wrapper class to retrieve Budget History
    public class WrapperClass
    {
        public RIT_Budget__History budgetHistory{get; set;}
        public String fieldName{get; set;}
        public String createdDate{get; set;}
        public boolean showCreatedDt{get;set;}

        //Constructor
        public WrapperClass(RIT_Budget__History budgetHist,String action)
        {
            if(action == 'clear')
            {
                 showCreatedDt = false;
            }
            else
            {
                 showCreatedDt = true;
            }
            budgetHistory = budgetHist;
            if(budgetHist.CreatedDate!=null)
            createdDate =budgetHist.CreatedDate.format();
            fieldName = VFC15_BudgetView.FieldLabel.get(budgetHist.Field);  
        }
    }    

    //Method to get a list of modified Budget fields that are trackable 
    public List<WrapperClass> getHistory()
    {
        List<RIT_Budget__History> budgetHist = new List<RIT_Budget__History>();
        if(Limits.getQueries() > Limits.getLimitQueries())
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CL00173));            
        else
        {
            budgetHist= [Select a.OldValue, a.NewValue, a.Id, a.Field, a.CreatedDate, a.CreatedBy.Name, a.CreatedById, a.ParentId
                                              From  RIT_Budget__History a where a.ParentId=: budget.Id ORDER BY a.CreatedDate DESC limit 500];                
        }
        for(integer i=0;i<budgetHist.size();i++)
        {
            //If the CreatedDate of the modified Budget are same, clear the action
            if(i>0 && budgetHist[i-1].CreatedDate == budgetHist[i].CreatedDate)
            {
                WrapperClass objWrap = new WrapperClass(budgetHist[i],'clear');
                listWrap.add(objWrap);
            }
            else
            {
                WrapperClass objWrap = new WrapperClass(budgetHist[i],'no clear');
                listWrap.add(objWrap);
            } 
        }
        return listWrap;
    }  

    //Method to set the label of the modified Budget fields
    public Void getAccountFields()
    {
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.RIT_Budget__c.fields.getMap();    
        List<Schema.SObjectField> fieldLabels = new List<Schema.SObjectField>();
        fieldLabels = fieldMap.values();
        for(Schema.SObjectField field:fieldLabels)
        {
            if(field.getDescribe().isUpdateable()==true)
            {               
                FieldLabel.put(field.getDescribe().getName(),field.getDescribe().getLabel());          
            }
        }
    }
}