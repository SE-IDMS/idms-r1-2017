global class WS_SDH_Connector {

    //description : generic result object used for the GetInfoObject webservice
    global class ObjectInfoResult{
        webservice  String bfofIeldRes;
        webservice  string bfoIdRes;
        webservice  string ResultType; 
        webservice  String errorMessage;
         
    }
    global class CountryInfoResult{
        webservice  String bfoCCRes;
        webservice  string bfoCCIdRes;
        webservice  string ResultType; 
        webservice  String errorMessage;
         
    }
    
    global class ServerTimeInfoResult{
        webservice  String bfoServerTime;        
        webservice  string ResultType; 
        webservice  String errorMessage;
         
    }
    global class AccountOwnerInfoResult{
        webservice  String OwnerbfoId;
        webservice  String AccountbfoId;         
        webservice  string ResultType; 
        webservice  String errorMessage;
         
    }
    
    
    
    //global list of generic result object (ObjectInfoResult)
    global static list<ObjectInfoResult> ObjectInfRes;
    global static list<CountryInfoResult> CountryInfRes;    
    global static ServerTimeInfoResult ServerTimeInfoRes;
    
    class bFOException extends Exception {}
    
   //description : for any object return the related ID or SESA depanding
   //of the input list (sesa or id)
   //input : list of string input (sesa or id)
   //        string objectName (name of the object)
   //output : list of ObjectInfoResult
    WebService static list<ObjectInfoResult> GetInfoObject (list<string> input, string objectName){  
        ObjectInfRes = new list<ObjectInfoResult>();
        ObjectInfoResult OneObjectInfRes = new ObjectInfoResult();
        list <sObject> genericRequestRes = new list <sObject>();
        string DynamicRequest;
        string ValidIdResult;
        
        //check if the input list is an id or sesa/code and if its valid by calling the utiliy class
        if(objectName !='StateProvince__c'){
          ValidIdResult = WS_SDH_Connector_utilities.validId(input.get(0),objectName);
        }
        else{
            String[]  sarray =   input.get(0).split(':');
            ValidIdResult = WS_SDH_Connector_utilities.validId(sarray[0],objectName);
        }
        string SOQL_ListFormat;
        string SOQL_ListCountryCodeFormat;
        string SOQL_ListStateCodeFormat;

        System.debug('***********************'+ValidIdResult);
        //if its an id...
        if(ValidIdResult == 'ValidMatch' ){
            //format the dynamic request catching id /sesa for the object
            DynamicRequest = QueryIdentifier(objectName,'ID');      
        //if it's a sesa..
        }else if (ValidIdResult == 'User'){
            //format the dynamic requestid /sesa for the object
            DynamicRequest = QueryIdentifier(objectName,'User');  
                    
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }else if (ValidIdResult == 'Account'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'Account');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }else if (ValidIdResult == 'Contact'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'Contact');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
        else if (ValidIdResult == 'Country__c'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'Country__c');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
        else if (ValidIdResult == 'StateProvince__c'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'StateProvince__c');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
        else if (ValidIdResult == 'LegacyAccount__c'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'LegacyAccount__c');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
         else if (ValidIdResult == 'Brand__c'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'Brand__c');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
         else if (ValidIdResult == 'DeviceType__c'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'DeviceType__c');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
        else if (ValidIdResult == 'Category__c'){
            //format the dynamic requestid /Account for the object
            DynamicRequest = QueryIdentifier(objectName,'Category__c');  
               
        //if the first 3 didgits does not corespond to the relates object in input return an error
        }
        
        else if (ValidIdResult == 'InvalidMatch'){
            OneObjectInfRes.ResultType = 'Failure';
            OneObjectInfRes.errorMessage = 'The id does not match the object';
            ObjectInfRes.add(OneObjectInfRes);
            return ObjectInfRes;
        //if the object is not recongnized return an error
        }else if (ValidIdResult == 'invalidObject'){
            OneObjectInfRes.ResultType = 'Failure';
            OneObjectInfRes.errorMessage = 'The object is not recognized';
            ObjectInfRes.add(OneObjectInfRes);
            return ObjectInfRes;
        }
        
        //format the input list to integrate it correctly to the dynamic request
        SOQL_ListFormat = WS_SDH_Connector_utilities.SOQLListFormat(input);
        
        if(objectName !='StateProvince__c'){
            //apend the list + the limit to the request
            DynamicRequest = DynamicRequest +'('+ SOQL_ListFormat +') limit '+ WS_SDH_Connector_utilities.QUERYMAXLIMIT;
        }
        else{

            List<String> stateCodeList = new List<String>();
            List<String> countryCodeList = new List<String>();

            for(String st:input){

                 List<String>  sarray =   st.split(':');
                 if(sarray.size()>0)
                 stateCodeList.add(sarray[0].trim());
                 if(sarray.size()>1)
                 countryCodeList.add(sarray[1].trim());
                 
                 

            }
            SOQL_ListStateCodeFormat = WS_SDH_Connector_utilities.SOQLListFormat(stateCodeList);
            SOQL_ListCountryCodeFormat =  WS_SDH_Connector_utilities.SOQLListFormat(countryCodeList);
            if(ValidIdResult == 'ValidMatch'){
             /*
             DynamicRequest = DynamicRequest +' where id in '+'('+ SOQL_ListStateCodeFormat +') and Country__c in '+ '('+ SOQL_ListCountryCodeFormat +')'+' limit '+ WS_SDH_Connector_utilities.QUERYMAXLIMIT;
             */
               String WhereClause ='';
               if(SOQL_ListStateCodeFormat.length()>0)
               {
                    WhereClause=' where id in '+'('+ SOQL_ListStateCodeFormat +')';
                    
               }
               if(SOQL_ListCountryCodeFormat.length()>0){
                    if(WhereClause.length()>0)
                    {
                        WhereClause+=' and Country__c in '+ '('+ SOQL_ListCountryCodeFormat +')';
                    }
               }
                DynamicRequest = DynamicRequest +WhereClause+' limit '+ WS_SDH_Connector_utilities.QUERYMAXLIMIT;
             
            }
            else{
                
                    
                String WhereClause ='';
               if(SOQL_ListStateCodeFormat.length()>0)
               {
                    WhereClause=' where StateProvinceCode__c in '+'('+ SOQL_ListStateCodeFormat +')';
                    
               }
               if(SOQL_ListCountryCodeFormat.length()>0){
                    if(WhereClause.length()>0)
                    {
                        WhereClause+=' and CountryCode__c in '+ '('+ SOQL_ListCountryCodeFormat +')';
                    }
               }
                DynamicRequest = DynamicRequest +WhereClause+' limit '+ WS_SDH_Connector_utilities.QUERYMAXLIMIT;
                
                /*
                DynamicRequest = DynamicRequest +' where StateProvinceCode__c in '+'('+ SOQL_ListStateCodeFormat +') and CountryCode__c in '+ '('+ SOQL_ListCountryCodeFormat +')'+' limit '+ WS_SDH_Connector_utilities.QUERYMAXLIMIT;
                System.debug('****** asdf '+DynamicRequest);
                */
            } 
        }
        
        
        try{  
            //execute request to a généric sobject list   
            System.debug('****** asdf '+DynamicRequest);    
             System.debug('\n CLog '+DynamicRequest);         
            genericRequestRes = Database.query(DynamicRequest);
        }catch(Exception e){
          system.debug('An exception occured in the WS_SDH_Connector.GetInfoObject query: '+e.getMessage());
          throw new bFOException('An exception occured in the WS_SDH_Connector.GetInfoObject query: '+e.getMessage());
        }
        
        //call the fonction to fetching the results
        if(ValidIdResult == 'ValidMatch' ){
            setResult(genericRequestRes,objectName,'ID',input);
        }else{
            setResult(genericRequestRes,objectName,'SESAORCODE',input);
        }
        
        //return the result
        return ObjectInfRes;
    }
       
    //description : dynamic request depending of the object
    //input : string objectName, string action (id, sesa)
    //output : string formated query
    public static string QueryIdentifier(string objectName, string action){
        string query;
        //call the related utility object class to fetch its correct field
        WS_SDH_Connector_utilities.ObjectInfoFields ObjectInfo = new WS_SDH_Connector_utilities.ObjectInfoFields();
        ObjectInfo = WS_SDH_Connector_utilities.GetObjectInfo(objectName);
        
        query = 'select id,';
        
        //add the dynamic field depending of the object
        if(ObjectInfo.ObjectName != 'StateProvince__c'){
            query = query + ObjectInfo.ObjeCtfield;
        }
        else{
            query = query + ObjectInfo.ObjeCtfield+' , CountryCode__c , Country__c ';
        }

        //add dynamic object name
        query = query + ' from ' + objectName;
        
        //add filter depdending of the action and object
        if(action == 'ID'){
            if(ObjectInfo.ObjectName != 'StateProvince__c')
            query = query + ' where id in ';
            else{
                // nothing 
            }
        }else{

            if(ObjectInfo.ObjectName != 'StateProvince__c')
                query = query + ' where '+ ObjectInfo.ObjeCtfield +' in ';
            else{
                
               // nothing

            }

        
        }
                    
        return query;
        
        
    }
    
    //description fetch and set the result of the webservice
    //input list <sObject> genericRequestRes (query)
    //      string objectName
    //      string action,list<string> input
    public static void setResult(list <sObject> genericRequestRes,string objectName,string action,list<string> input){
        
        ObjectInfoResult OneObjectInfRes;
        //call the related utility object class to fetch its correct field
        WS_SDH_Connector_utilities.ObjectInfoFields ObjectInfo = new WS_SDH_Connector_utilities.ObjectInfoFields();
        ObjectInfo = WS_SDH_Connector_utilities.GetObjectInfo(objectName);

        //for each input we will compare it to each result to know if the id / sesa is found in bFO
        for(string oneRes : input){
            
            OneObjectInfRes = new ObjectInfoResult();
            boolean found = false;
            for(sObject OneObject : genericRequestRes){
                
                //if action = ID we try to match the input id with the request result id's  
                if(objectName !='StateProvince__c'){
                    
                    System.debug('\n cLog *******'+objectName);
                    System.debug('\n cLog *******'+oneRes);                 
                    System.debug('\n cLog *******'+OneObject);
                    
                    if(action == 'ID'){                 
                        if(OneObject.Id == oneRes ){
                            system.debug('------------------------------> i found an id' + OneObject.Id);
                            OneObjectInfRes.bfoIdRes = OneObject.Id;                
                            //we convert the sObject field to  a string value result depanding of the object (ex : federationindentifier, territorycode etc..)
                            if((string)OneObject.get(ObjectInfo.ObjeCtfield) != null && (string)OneObject.get(ObjectInfo.ObjeCtfield) !='')
                            {
                                OneObjectInfRes.bfofIeldRes = (string)OneObject.get(ObjectInfo.ObjeCtfield);        
                                OneObjectInfRes.ResultType = 'Success';
                                found = true;
                            }
                            else{
                                OneObjectInfRes.bfofIeldRes = (string)OneObject.get(ObjectInfo.ObjeCtfield);        
                                OneObjectInfRes.ResultType = 'Failure';
                                if(objectName =='User')
                                OneObjectInfRes.errorMessage = 'SESA not found !';
                                if(objectName =='Account')
                                OneObjectInfRes.errorMessage = 'SEAccountID not found !';
                                if(objectName =='Contact')
                                OneObjectInfRes.errorMessage = 'SEContactID not found !';
                                if(objectName =='Country__c')
                                OneObjectInfRes.errorMessage = 'Country Code not found !';
                                if(objectName =='StateProvince__c')
                                OneObjectInfRes.errorMessage = 'State/Province not found !';
                                if(objectName =='LegacyAccount__c')
                                OneObjectInfRes.errorMessage = 'LegacyKey not found !';
                                if(objectName =='Brand__c')
                                OneObjectInfRes.errorMessage = 'BrandKey not found !';
                                if(objectName =='DeviceType__c')
                                OneObjectInfRes.errorMessage = 'DeviceTypeKey not found !';
                                if(objectName =='Category__c')
                                OneObjectInfRes.errorMessage = 'SDHCategoryID not found !';
                                found = true;
                                
                            }
                        }
                    //we convert the sObject field to  a string value result depanding of the object (ex : federationindentifier, territorycode etc..)
                    //if action = sesa we try to match the input sesa with the request result sesa's    
                    }else if((string)OneObject.get(ObjectInfo.ObjeCtfield) == oneRes ){
                            OneObjectInfRes.bfoIdRes = OneObject.Id;                    
                            //we convert the sObject field to  a string value result depanding of the object (ex : federationindentifier, territorycode etc..)
                                OneObjectInfRes.bfofIeldRes = (string)OneObject.get(ObjectInfo.ObjeCtfield);                      
                                OneObjectInfRes.ResultType = 'Success';
                                found = true;
                            
                    }
                }
                else{
                    List<String>  sarray =   oneRes.split(':');
                   // System.debug('\n cLog '+sarray[0]+':'+sarray[1]);
                    if(action == 'ID'){
                        
                        if(OneObject.Id == sarray[0] ){
                            system.debug('------------------------------> i found an id' + OneObject.Id);
                            if((string)OneObject.get('StateProvinceCode__c') != null && (string)OneObject.get('StateProvinceCode__c') !=''){
                                OneObjectInfRes.bfoIdRes = (string)OneObject.get('StateProvinceCode__c');                           
                                OneObjectInfRes.bfofIeldRes = oneRes;                               
                                OneObjectInfRes.ResultType = 'Success';
                                found = true;
                            }
                            else{
                                
                                OneObjectInfRes.bfoIdRes = (string)OneObject.get('StateProvinceCode__c');                           
                                OneObjectInfRes.bfofIeldRes = oneRes;   
                                OneObjectInfRes.ResultType = 'Failure';                             
                                OneObjectInfRes.errorMessage = 'State/Province not found !';
                                
                                
                            }
                        }
                    //we convert the sObject field to  a string value result depanding of the object (ex : federationindentifier, territorycode etc..)
                    //if action = sesa we try to match the input sesa with the request result sesa's    
                    }else if((string)OneObject.get(ObjectInfo.ObjeCtfield) == sarray[0]  && (string)OneObject.get('CountryCode__c') == sarray[1]  ){
                         System.debug('\n cLog *******');
                         System.debug('\n cLog '+sarray[0]+':'+sarray[1]);
                         System.debug('\n cLog '+(string)OneObject.get(ObjectInfo.ObjeCtfield)+' --- '+(string)OneObject.get('CountryCode__c') == sarray[1]);
                            OneObjectInfRes.bfoIdRes = OneObject.Id;                    
                            //we convert the sObject field to  a string value result depanding of the object (ex : federationindentifier, territorycode etc..)
                            //OneObjectInfRes.bfofIeldRes = (string)OneObject.get(ObjectInfo.ObjeCtfield)+':'+(string)OneObject.get('CountryCode__c');   
                            OneObjectInfRes.bfofIeldRes = oneRes;                       
                            OneObjectInfRes.ResultType = 'Success';
                            found = true;                                               
                    }
                    
                
                }
            }
             System.debug('\n cLog *******'+found);    
            //if id / sesa not found add an error message result    
            if(!found){
                //System.debug('\n cLog ******* not found');
                OneObjectInfRes.ResultType = 'Failure';
                System.debug('\n cLog ******* not found');
                if(action == 'ID'){
                    OneObjectInfRes.bfoIdRes = oneRes;
                    OneObjectInfRes.errorMessage = 'ID not found !';
                }else{
                    OneObjectInfRes.bfofIeldRes = oneRes;
                    OneObjectInfRes.errorMessage = 'Record not found !';
                    
                    /*
                    if(objectName =='User')
                    OneObjectInfRes.errorMessage = 'SESA not found !';
                    if(objectName =='Account')
                    OneObjectInfRes.errorMessage = 'SEAccountID not found !';
                    if(objectName =='Contact')
                    OneObjectInfRes.errorMessage = 'SEContactID not found !';
                    if(objectName =='Country__c')
                    OneObjectInfRes.errorMessage = 'Country Code not found !';
                    if(objectName =='StateProvince__c')
                    OneObjectInfRes.errorMessage = 'State/Province not found !';
                    if(objectName =='LegacyAccount__c')
                    OneObjectInfRes.errorMessage = 'LegacyKey not found !';
                    */
                }                                   
            }else{
                found = false;
            }
            
            ObjectInfRes.add(OneObjectInfRes);
            System.debug('\n cLog ******* '+OneObjectInfRes);
            System.debug('\n cLog ******* '+ObjectInfRes);
        }
            
         
    }
    
    WebService static list<CountryInfoResult> GetBfoCountryInfo (){
    
         CountryInfRes = new list<CountryInfoResult>();
         try{
         
            for(Country__c c:[SELECT Id,CountryCode__c,Region__c FROM Country__c])
            {
                CountryInfoResult cinfo = new CountryInfoResult();
                cinfo.bfoCCRes = c.CountryCode__c;
                cinfo.bfoCCIdRes = c.id;
                cinfo.ResultType='Success'; 
                CountryInfRes.add(cinfo);
            }
            
         }
         catch(Exception ex){
         
         }
         
         //return the result
        return CountryInfRes;
        
    }
    
    WebService static ServerTimeInfoResult GetBfoServerTime (){
    
         ServerTimeInfoRes = new ServerTimeInfoResult(); 
         
         try{
                 DateTime dt = System.now();
                  //ServerTimeInfoRes.bfoServerTime = String.valueOf(dt)+'.'+dt.millisecond(); 
                 String strDT =dt.formatGMT('yyyy-MM-dd HH:mm:ssZ');
                        strDT= strDT.replace(' ','T');
                        strDT= strDT.replace('+0000','Z');
                 ServerTimeInfoRes.bfoServerTime = strDT ;
                        System.debug(strDT); 
                                            
                 ServerTimeInfoRes.ResultType = 'Success';  
            
         }
         catch(Exception ex){
         }
         
         //return the result
        return ServerTimeInfoRes;
        
    }
    WebService static List<AccountOwnerInfoResult> GetAccountOwnerInfo (List<id> accountIds){
    
      
        
        List<AccountOwnerInfoResult> results = new List<AccountOwnerInfoResult>();
        
        if(accountIds != null && accountIds.size()>0)
        {
          try{
            for(Account accObj : [select id, OwnerId  from Account where id in :accountIds])
            {
                AccountOwnerInfoResult  aoir = new AccountOwnerInfoResult();
                aoir.AccountbfoId =  accObj.id;
                aoir.OwnerbfoId =  accObj.OwnerId ;
                aoir.ResultType =  'Success'; 
                results.add(aoir);
            }
          }
          catch(Exception ex){
                AccountOwnerInfoResult  aoir = new AccountOwnerInfoResult();
                aoir.ResultType =  'Failure'; 
                aoir.errorMessage = ex.getMessage();
                results.add(aoir);
          }
        }
        else{
                AccountOwnerInfoResult  aoir = new AccountOwnerInfoResult();
                aoir.ResultType =  'Failure'; 
                aoir.errorMessage = 'Please send Valid Account id ';
                results.add(aoir);          
        }        
         
       
         
         //return the result
        return results;
        
    }   
    
    
    
          

}