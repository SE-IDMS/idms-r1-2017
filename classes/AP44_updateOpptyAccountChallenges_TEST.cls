@isTest
private class AP44_updateOpptyAccountChallenges_TEST{
    static testMethod void testAP44_updateOpptyAccountChallenges()
    {
       
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        //Check Before insert trigger execution
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp.Amount= 10000;
        opp.Probability=10;                
        opp.stageName=Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue();
        insert opp;                
        
        //update opp;
        
        SFE_AccPlan__c accountplan=createAccountPlan(acc.id);
        insert accountplan;
        SFE_AccPlan__c objAccPlan=new SFE_AccPlan__c(name='ip account plan',Account__c=acc.id,AccountTurnover__c=100);
        insert objAccPlan;
        AccountPlanConfidential__c objAPC=new AccountPlanConfidential__c(AccountPlan__c=objAccPlan.id,Strengths__c='strength');
        insert objAPC;
        
        
        AccountChallenge__c accountchallenge=createAccountChallenge(accountplan.id);
        accountchallenge.AccountPlanConfidential__c = objAPC.id;
        
        insert accountchallenge;
        
        OpportunityAccountChallenge__c oppaccchallenge=createOpportunityAccountChallenge(accountchallenge.id,opp.id);        
        oppaccchallenge.OpptyContributiontoAcctChallenge__c=10;
        insert oppaccchallenge;
        opp.Amount= 100000;    
        update opp;   
         AP44_updateOpportunityAccountChallenges.updateOpportunityAccountChallenges(new List<Opportunity>{opp}) ;            
         
    }
    
    static testMethod void testAP44_updateOpptyAccountChallenges1()
    {
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        //Check Before insert trigger execution
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp.Amount= 10000;
        opp.Probability=10;                
        opp.stageName=Opportunity.StageName.getDescribe().getPicklistValues()[7].getValue();
        insert opp;   
        opp.Amount= 100000;
        update opp;             
        
        
        SFE_AccPlan__c accountplan=createAccountPlan(acc.id);
        insert accountplan;
        SFE_AccPlan__c objAccPlan=new SFE_AccPlan__c(name='ip account plan',Account__c=acc.id,AccountTurnover__c=100);
        insert objAccPlan;
        AccountPlanConfidential__c objAPC=new AccountPlanConfidential__c(AccountPlan__c=objAccPlan.id,Strengths__c='strength');
        insert objAPC;
        
        AccountChallenge__c accountchallenge=createAccountChallenge(accountplan.id);
        accountchallenge.AccountPlanConfidential__c = objAPC.id;
        insert accountchallenge;
        
        OpportunityAccountChallenge__c oppaccchallenge=createOpportunityAccountChallenge(accountchallenge.id,opp.id);        
        oppaccchallenge.OpptyContributiontoAcctChallenge__c=10;
        insert oppaccchallenge;
                        
        update opp;  
        AP44_updateOpportunityAccountChallenges.updateOpportunityAccountChallenges(new List<Opportunity>{opp}) ;              
         
    }
    
    
    /*
    This method should be part of the utils_test methods
    */    
    static OpportunityAccountChallenge__c createOpportunityAccountChallenge(Id AccountchallengeId,Id OpportunityId)
    {
        return new OpportunityAccountChallenge__c (Opportunity__c=OpportunityId,AccountChallenge__c=AccountchallengeId);
    }
    
    static AccountChallenge__c createAccountChallenge(Id AccountPlanId)
    {
        return new AccountChallenge__c(RelatedAccountPlan__c=AccountPlanId);
    }
    
    static SFE_AccPlan__c createAccountPlan(Id AccountId)
    {
        return new SFE_AccPlan__c(Name='SampleAccountPlan',Account__c=AccountId);
    }
    
}