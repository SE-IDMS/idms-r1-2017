@isTest
private class VFC_ConvertLevel1_TEST {

    public static testMethod void testConversion() {
        
        Id partnerAccRTId          = [SELECT Id FROM RecordType WHERE SobjectType='Account' 
                                      AND developerName='PRMAccount' limit 1][0].Id;
                                      
        Country__c testCountry = new Country__c(CountryCode__c = 'XX');
        insert testCountry;
        
        StateProvince__c testState = new StateProvince__c(Country__c = testCountry.Id, 
                                                          CountryCode__c = 'XX', 
                                                          StateProvinceCode__c='ZZ');
        insert testState;
        
        Account partnerTestAccount = new Account(Name                           = 'Sample Name',
                                                 recordtypeId                   = partnerAccRTId,
                                                 PRMStreet__c                   = 'Sample PRM Street',
                                                 PRMAdditionalAddress__c        = 'PRM Additional Address',
                                                 PRMAreaOfFocus__c              = 'Sample Area of Focus',
                                                 PRMBusinessType__c             = 'Sample Business Type',
                                                 PRMCity__c                     = 'Sample City',
                                                 PRMCompanyName__c              = 'Sample Company Name',
                                                 PRMCompanyPhone__c             = 'Sample Company Phone',
                                                 PRMWebsite__c                  = 'Sample Website',
                                                 PRMCountry__c                  = testCountry.Id,
                                                 PRMCurrencyIsoCode__c          = 'Sample Currency Code',
                                                 PRMEmployeeSize__c             = 'Sample Employee Size',
                                                 PRMCorporateHeadquarters__c    = true,
                                                 PRMMarketServed__c             = 'Sample Market Served',
                                                 PRMAccType__c                  = 'Sample Account Type',
                                                 PRMStateProvince__c            = testState.Id,
                                                 PRMTaxId__c                    = '000000',
                                                 PRMAnnualSales__c              = '50000',
                                                 PRMZipCode__c                  = '75000');      
        
        insert partnerTestAccount;
        
        Contact partnerContact     = new Contact(FirstName        = 'Sample First Name',
                                                 LastName         = 'Sample Last Name',
                                                 Email            = 'sample@email.com',
                                                 AccountId        = partnerTestAccount.Id,
                                                 JobTitle__c      = 'Z3',
                                                 Country__c       = testCountry.Id);
        
        insert partnerContact;
        
        Test.setCurrentPageReference(new PageReference('Page.VFP_ConvertLevel1')); 
        System.currentPageReference().getParameters().put('contactId', partnerContact.Id);
        
        VFC_ConvertLevel1 conversionController = new VFC_ConvertLevel1();
        conversionController.convertAccount();
        
        Test.setCurrentPageReference(new PageReference('Page.VFP_ConvertLevel1')); 
        System.currentPageReference().getParameters().put('retURL', '/');        

        conversionController.back();
    }
}