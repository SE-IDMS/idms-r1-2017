/*
    Pooja Suresh
    Release     : Jul16 Release (BR-10118) - Automatic Ownership Re-Assignment from “External User”
    Purpose     : Batch reassigns ownership for an account created by an external agent with profile SE - Agent (Global - Community) using queue-able job.
                  An internal user is assigned as account owner based on rules defined in Account Ownership Rule for EAO functionality
                  Simultaneouly external agent is added to account team with read/write access to account, oppty and case
*/
global class Batch_ReassignExternalAccountOwner implements Database.Batchable<sObject>,Schedulable
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Datetime releasedate;
        if(Test.IsRunningTest())
            releasedate = datetime.newInstance(System.Today().year(), System.Today().month(), System.Today().day());    //For test clas coverage
        else
            releasedate = Datetime.newInstance(2016, 7, 29);    //July16 Release Go-Live Date
        String agentProfileId = String.valueOF(Label.CLJUL16ACC01);
        String accRecordTypeId = String.valueOF(System.Label.CLOCT13ACC08);
        String code = Label.CLNOV16ACC002; //CLNOV16ACC002 = IT,ES,AD
        List<String> countryCode = code.split(',');
        System.debug('Country are ->'+countryCode);
        
        String query='SELECT Id, OwnerId, Owner.ProfileId, BillingCountryCode, BillingStateCode, ClassLevel1__c FROM Account WHERE RecordTypeId =:accRecordTypeId and Owner.ProfileId=:agentProfileId and CreatedDate >= :releasedate and BillingCountryCode IN: countryCode'; // CLOCT13ACC08: Business Account record Type ID
        System.debug('The query used is \n'+query);   
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        System.debug('--- Batch_ReassignExternalAccountOwner.excute Begins ---');

        String county;  //Stores the county for a respective state
        String key;     //Stores the unique key used for obtaining the internal account owner
        String countryCode = Label.CLNOV16ACC002; //CLNOV16ACC002 = IT,ES,AD
        List<Account> updateAccountLst = new List<Account>();   //updates changes to owner and externalSalesResp fields on Account
        List<sObject> createAcctTeamMemlst = new List<sObject>(); //inserts AccountTeamMember and AccountShare records
        Map<String,AccountOwnershipRule__c> assignmentRules = new Map<String,AccountOwnershipRule__c>();
        Map<String,String> stateCountyMap = new Map<String,String>();
        if (!scope.isEmpty()) {

            //Extract ownership rules and assigned internal owner details for functionality 'EAO'->External Account Ownership
            for(AccountOwnershipRule__c extOwnRule: [SELECT Id, TECH_CountryCode__c, ClassLevel1__c, County__c, TECH_StateProvinceCode__c, AccountOwner__c, TECH_CriteriaCode__c FROM AccountOwnershipRule__c WHERE Functionality__c='EAO' ])
            {
                System.debug('------ extOwnRule ---- '+ extOwnRule);
                if(extOwnRule.TECH_CriteriaCode__c!=null)
                    assignmentRules.put(extOwnRule.TECH_CriteriaCode__c,extOwnRule);
            }
            for(CS_StateCountyMapping__c st:[SELECT Name,County__c from CS_StateCountyMapping__c]) {
                stateCountyMap.put(st.Name,st.County__c);
            }
            
            try
            {
                for (Account acc : scope)
                {
                    //Reassign externally owned Italy accounts
                    if (acc.BillingCountryCode != null && acc.BillingCountryCode != '' && countryCode.contains(acc.BillingCountryCode)) 
                    {
                        //Populate external user in account field 'External Sales Responsible'
                        acc.ExternalSalesResponsible__c=acc.OwnerId;

                        //Add the External User to AccountTeam with Read/Write access to account, opportunity and case
                        AccountTeammember atm = new AccountTeammember(UserId = acc.OwnerId,AccountID = acc.Id,TeamMemberRole='ESR',AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit' );
                        createAcctTeamMemlst.add(atm);

                        //Assign account owner to an internal user based on Account Ownership Rules
                        if(acc.BillingStateCode != null && acc.BillingStateCode != '' && stateCountyMap.containsKey(acc.BillingStateCode))
                            county = CS_StateCountyMapping__c.getInstance(acc.BillingStateCode).County__c;
                        System.debug('key from custom setting'+county);
                        if(county == null && acc.BillingStateCode != null) {
                            key=acc.BillingCountryCode+acc.ClassLevel1__c+acc.BillingStateCode; // For county who don't have county
                        }
                        else if(county == null && acc.BillingStateCode == null) {
                            key=acc.BillingCountryCode+acc.ClassLevel1__c; // For country who don't have county or state/province
                        }
                        else if(county!=null) {
                            key=acc.BillingCountryCode+acc.ClassLevel1__c+county; // For country who have both county and state/province
                        }
                        System.debug('The Key is ---> '+key);
                        acc.OwnerId = assignmentRules.get(key).AccountOwner__c;
                        updateAccountLst.add(acc);
                        
                    }
                    /*
                    if(acc.BillingCountryCode == Label.CLNOV16ACC006 && acc.BillingStateCode !=null) {
                        //Populate external user in account field 'External Sales Responsible'
                        acc.ExternalSalesResponsible__c=acc.OwnerId;

                        //Add the External User to AccountTeam with Read/Write access to account, opportunity and case
                        AccountTeammember atm = new AccountTeammember(UserId = acc.OwnerId,AccountID = acc.Id,TeamMemberRole='ESR',AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit' );
                        createAcctTeamMemlst.add(atm);

                        //Assign account owner to an internal user based on Account Ownership Rules
                        //county = CS_StateCountyMapping__c.getInstance(acc.BillingStateCode).County__c;
                        key=acc.BillingCountryCode+acc.ClassLevel1__c+acc.BillingStateCode;
                        System.debug('The Key is ---> '+key);
                        acc.OwnerId = assignmentRules.get(key).AccountOwner__c;
                        updateAccountLst.add(acc);
                    }
                    
                    if(countryCode.contains(acc.BillingCountryCode) && acc.BillingStateCode == null) {
                        //Populate external user in account field 'External Sales Responsible'
                        acc.ExternalSalesResponsible__c=acc.OwnerId;

                        //Add the External User to AccountTeam with Read/Write access to account, opportunity and case
                        AccountTeammember atm = new AccountTeammember(UserId = acc.OwnerId,AccountID = acc.Id,TeamMemberRole='ESR',AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit' );
                        createAcctTeamMemlst.add(atm);

                        //Assign account owner to an internal user based on Account Ownership Rules
                        //county = CS_StateCountyMapping__c.getInstance(acc.BillingStateCode).County__c;
                        key=acc.BillingCountryCode+acc.ClassLevel1__c;
                        System.debug('The Key is ---> '+key);
                        acc.OwnerId = assignmentRules.get(key).AccountOwner__c;
                        updateAccountLst.add(acc);
                    }
                    */
                }

                if(!updateAccountLst.isEmpty())
                    Database.update(updateAccountLst,false);

                if(!createAcctTeamMemlst.isEmpty())
                    Database.insert(createAcctTeamMemlst,false);
                
            }
            catch(Exception e)
            {
                System.debug('Error Message: '+e.getMessage());
                System.debug('Stack Trace: '+e.getStackTraceString());
            }
        }
    }

    global void finish(Database.BatchableContext BC) {}

    //Schedule method
    global void execute(SchedulableContext sc) 
    {
        Batch_ReassignExternalAccountOwner batchAccOwn = new Batch_ReassignExternalAccountOwner();
        Database.executebatch(batchAccOwn); 
    }
}