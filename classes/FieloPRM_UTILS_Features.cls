/************************************************************
* Developer: Tomás García E                                 *
* Type: rest class                                          *                                       
* Created Date: 06/03/2015                                  *
************************************************************/
public class FieloPRM_UTILS_Features{
    
    public static final string programLevelType = 'Program Level';
    public static final string featureType = 'Feature';


    public static Map<String,List<String>> getFeaturesByContact( List<String> listContactPRMUIMSIds){
    
        system.debug('listContactPRMUIMSIds '+ listContactPRMUIMSIds);
    
        Set<String> setContactIds = new Set<String>();
        list<Contact> Contacts = new list<Contact>();
        map<String,Contact> mapMemberToContact = new map<String,Contact>();
        Set<String> setContactPRMUIMIds = new Set<String>();

   
        if(!listContactPRMUIMSIds.isEmpty()){
            for( String idPRMUIMSCon : listContactPRMUIMSIds){
                setContactPRMUIMIds.add(idPRMUIMSCon);
            }    
        }
        
        system.debug('setContactPRMUIMIds '+ setContactPRMUIMIds);
        
        Contacts = [SELECT id, PRMUIMSId__c , FieloEE__Member__c FROM Contact WHERE PRMUIMSId__c  IN: setContactPRMUIMIds ];
        
        system.debug('Contacts 1 '+ Contacts );
   
        if(!Contacts.isEmpty()){
            for(Contact con : Contacts ){
                mapMemberToContact.put(con.FieloEE__Member__c , con);
            }    
        }
        
        system.debug('mapMemberToContact '+ mapMemberToContact);
        
        list<FieloEE__BadgeMember__c> listBadgeMember = [SELECT id, FieloEE__Member2__c, FieloEE__Badge2__c, FieloEE__Badge2__r.F_PRM_BadgeAPIName__c FROM FieloEE__BadgeMember__c WHERE FieloEE__Member2__c IN : mapMemberToContact.KeySet() AND FieloEE__Badge2__r.F_PRM_Type__c =: programLevelType  ];
        
        system.debug('listBadgeMember '+ listBadgeMember );
        
        map<String,List<String>> mapIdMemToBadgesIds = new map<String,List<String>>(); /// cambiar nombre a tobadge
        map<String,List<String>> mapIdConToExtIds = new map<String,List<String>>();
        set<String> setBadgesIds = new set<String>();
        
        if(!listBadgeMember.isEmpty()){
            for(FieloEE__BadgeMember__c badgeMem : listBadgeMember ){
                setBadgesIds.add(badgeMem.FieloEE__Badge2__c); 
                if(mapIdMemToBadgesIds.ContainsKey(badgeMem.FieloEE__Member2__c )){
                    list<String> aux = mapIdMemToBadgesIds.get(badgeMem.FieloEE__Member2__c); 
                    aux.add(badgeMem.FieloEE__Badge2__c);
                    mapIdMemToBadgesIds.put(badgeMem.FieloEE__Member2__c ,aux);                        
                }else{
                    list<String> aux = new list<String>();
                    aux.add(badgeMem.FieloEE__Badge2__c);
                    mapIdMemToBadgesIds.put(badgeMem.FieloEE__Member2__c ,aux);   
                }
            }    
        }
        
        system.debug('setBadgesIds '+ setBadgesIds);
        system.debug('mapIdMemToBadgesIds '+ mapIdMemToBadgesIds);
        
        map<String,list<String>> mapIdBadgeToCodFeature = new map<String,list<String>>();
        list<FieloPRM_BadgeFeature__c> listBadgeFeature = [SELECT id, F_PRM_Badge__c, F_PRM_Feature__c, F_PRM_Feature__r.F_PRM_FeatureAPIName__c FROM FieloPRM_BadgeFeature__c WHERE F_PRM_Badge__c IN: setBadgesIds ];
        
        system.debug('listBadgeFeature '+ listBadgeFeature );
        
         if(!listBadgeFeature .isEmpty()){
            for(FieloPRM_BadgeFeature__c badgeFea : listBadgeFeature ){    
                if(mapIdBadgeToCodFeature.ContainsKey(badgeFea.F_PRM_Badge__c  )){
                    list<String> aux = mapIdBadgeToCodFeature.get(badgeFea.F_PRM_Badge__c ); 
                    aux.add(badgeFea.F_PRM_Feature__r.F_PRM_FeatureAPIName__c);
                    mapIdBadgeToCodFeature.put(badgeFea.F_PRM_Badge__c  ,aux);                        
                }else{
                    list<String> aux = new list<String>();
                    aux.add(badgeFea.F_PRM_Feature__r.F_PRM_FeatureAPIName__c);
                    mapIdBadgeToCodFeature.put(badgeFea.F_PRM_Badge__c  ,aux);   
                }            
            }    
        }
        
        system.debug('mapIdBadgeToCodFeature '+ mapIdBadgeToCodFeature);
        
        map<String, list<String>> idPRMUIMConToExtCods = new map<String, list<String>>();
        
        if( mapIdMemToBadgesIds.KeySet().Size() > 0){
            system.debug('1');
            for(String idMem : mapIdMemToBadgesIds.KeySet()){  // por cada member
                system.debug('2');
                for(String idBadge : mapIdMemToBadgesIds.get(idMem)){ // por cada badge de cada member
                    system.debug('3');
                    if(idPRMUIMConToExtCods.ContainsKey(mapMemberToContact.get(idMem).PRMUIMSId__c)){ // si existe un cod para ese badge
                        system.debug('entro 4');
                        list<String> aux = new list<String>();
                        mapIdBadgeToCodFeature.put(mapMemberToContact.get(idMem).id , mapIdBadgeToCodFeature.get(idBadge) );                    
                    }else{

                            idPRMUIMConToExtCods.put(mapMemberToContact.get(idMem).PRMUIMSId__c , mapIdBadgeToCodFeature.get(idBadge));     
                    }                       
                }
            }    
        }
        
        
     /*   if( mapIdMemToBadgesIds.KeySet().Size() > 0){
            system.debug('1');
            for(String idMem : mapIdMemToBadgesIds.KeySet()){  // por cada member
                system.debug('2');
                for(String idBadge : mapIdMemToBadgesIds.get(idMem)){ // por cada badge de cada member
                    system.debug('3');
                    if(mapIdBadgeToCodFeature.ContainsKey(idBadge)){ // si existe un cod para ese badge
                        system.debug('entro 4');
                        if(idPRMUIMConToExtCods.ContainsKey(mapMemberToContact.get(idMem).id )){
                            list<String> aux = idPRMUIMConToExtCods.get(idMem); 
                            aux.add(mapIdBadgeToCodFeature.get(idBadge));
                            idPRMUIMConToExtCods.put(mapMemberToContact.get(idMem).id ,aux);                        
                        }else{
                            list<String> aux = new list<String>();
                            aux.add(mapIdBadgeToCodFeature.get(idBadge));
                            idPRMUIMConToExtCods.put(mapMemberToContact.get(idMem).id ,aux);     
                        }                       

                    }
                }
            }    
        }*/
               
        return idPRMUIMConToExtCods;
    }
    
    public static List<String> getFeatures( ){
    
        Set<String> setContactIds = new Set<String>();
        list<String> codsIdsToReturn = new list<String>();
        
        list<FieloPRM_Feature__c> listFeatures = [SELECT id, F_PRM_FeatureAPIName__c FROM FieloPRM_Feature__c  ];
       
        if(!listFeatures.isEmpty()){
            for( FieloPRM_Feature__c fea: listFeatures ){
                codsIdsToReturn.add(fea.F_PRM_FeatureAPIName__c);
            }    
        }

               
        return codsIdsToReturn;
    }
    

    
    public static List<String> getUpdatedFeatureOnContacts( String fromTime , String toTime ){

        if(fromTime.length() != 17 || fromTime == null ){
            return null;
        }
        
        String yearFromTime = fromTime.substring(0, 4); 
        String monthFromTime = fromTime.substring(4, 6);     
        String dayFromTime = fromTime.substring(6, 8);       
        String hoursFromTime = fromTime.substring(8, 10);       
        String minutesFromTime = fromTime.substring(10, 12); 
        String secondsFromTime = fromTime.substring(12, 14); 
        String miliSecondsFromTime = fromTime.substring(14, 17); 
        
        Date fromDate = Date.newInstance(integer.ValueOf(yearFromTime), integer.ValueOf(monthFromTime), integer.ValueOf(dayFromTime));
        Time FromTimeToDateTime = Time.newInstance(integer.ValueOf(hoursFromTime), integer.ValueOf(minutesFromTime), integer.ValueOf(secondsFromTime), integer.ValueOf(miliSecondsFromTime) );

        dateTime fromTimeDate = DateTime.newInstanceGmt(fromDate ,FromTimeToDateTime );
        system.debug('fromTimeDate ' + fromTimeDate );
        
        if(toTime.length() != 17 || toTime== null ){
            return null;
        }
        
        String yearToTime = toTime .substring(0, 4); 
        String monthToTime = toTime .substring(4, 6);     
        String dayToTime = toTime .substring(6, 8);       
        String hoursToTime = toTime .substring(8, 10);       
        String minutesToTime = toTime .substring(10, 12); 
        String secondsToTime = toTime.substring(12, 14); 
        String miliSecondsToTime = toTime.substring(14, 17);
        
        Date toDate = Date.newInstance(integer.ValueOf(yearToTime ), integer.ValueOf(monthToTime), integer.ValueOf(dayToTime));
        Time toTimeToDateTime = Time.newInstance(integer.ValueOf(hoursToTime ), integer.ValueOf(minutesToTime ), integer.ValueOf(secondsToTime ), integer.ValueOf(miliSecondsToTime ) );

        dateTime toTimeDate = DateTime.newInstanceGmt(toDate ,toTimeToDateTime); 
       
        system.debug('ToTimeDate ' + ToTimeDate );
        
        list<FieloPRM_MemberUpdateHistory__c> historyList = [SELECT id, F_PRM_Type__c, F_PRM_Member__c FROM FieloPRM_MemberUpdateHistory__c WHERE CreatedDate >: fromTimeDate AND CreatedDate <: ToTimeDate AND F_PRM_Type__c =: featureType ];
        
        system.debug('size:  ' + historyList.Size() );
        
        set<String> setMembersIds = new set<String>();
        
        if(!historyList.isEmpty()){
            for( FieloPRM_MemberUpdateHistory__c his : historyList ){
                setMembersIds.add(his.F_PRM_Member__c);
            }    
        }
        
        system.debug('setMembersIds:  ' + setMembersIds.Size() );
        
        list<Contact> ContactList = [SELECT id, PRMUIMSId__c FROM Contact WHERE FieloEE__Member__c IN : setMembersIds];
        list<String> ContactPRMUIMSIds = new list<String>();
        
        if(!ContactList.isEmpty()){
            for( Contact con : ContactList ){
                ContactPRMUIMSIds.add(con.PRMUIMSId__c );
            }    
        }
        
        if(!ContactPRMUIMSIds.isEmpty()){
            return ContactPRMUIMSIds;    
        }else{    
            return null;
        }

    }
  
}