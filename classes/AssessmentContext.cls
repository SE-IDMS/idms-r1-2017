/*
Srinivas Nallapati  30-Oct-2013     Nov13 PRM Release   
*/
global class AssessmentContext
{
    WebService String[] assessmentIDs;

    WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
     
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public AssessmentContext() {
    }

    /**
    * A constructor based on an Program @param a Program
    */
    public AssessmentContext(List<String> assessmentIDs) {
        this.assessmentIDs = assessmentIDs;

    }
}