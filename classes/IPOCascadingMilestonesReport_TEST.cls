@isTest
public class IPOCascadingMilestonesReport_TEST{
    static testMethod void testIPOCascadingMilestonesReport(){
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOAt'); 
        System.runAs(runAsUser){
            PageReference pageRef = Page.IPOMilestonesnCascadingMilestonesReport;
            Test.setCurrentPage(pageRef); 
            string Years = '2014'; 
            
            //User u = [SELECT Id, Name FROM User LIMIT 1];
            
            
            IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative'); 
            insert IPOinti; 
            
            IPO_Strategic_Program__c IPOp = new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',
                                                                       IPO_Strategic_Program_Leader_1__c = runasuser.id,
                                                                       IPO_Strategic_Program_Leader_2__c = runasuser.id, 
                                                                       Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',
                                                                       Operation__c='NA;APAC', Year__c = years,
                                                                       Program_Owner__c = runAsUser .Id); 
            insert IPOp;
            
            String ProgOwner = IPOp.Program_Owner__r.FirstName + ' ' + IPOp.Program_Owner__r.LastName;
            
            IPO_Strategic_Milestone__c IPOM=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile',Year__c = years,IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation_Regions__c='NA;APAC');  
            Insert IPOM;
            
            IPO_Strategic_Cascading_Milestone__c IPOC = new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Cascading_Milestone_Name__c = 'test',Year__c = years,IPO_Strategic_Milestone__c = IPOM.Id);
            insert IPOC;
            
            ApexPages.currentPage().getparameters().put('Id',IPOp.id);  
            
            ApexPages.StandardController stanPage = new ApexPages.StandardController(IPOp); 
            IPOMilestonesnCascadingMilestonesReport controller = new IPOMilestonesnCascadingMilestonesReport(stanPage);  
            
            controller.getFilterYear(); 
            controller.getFilterProgramOwnerList();   
            controller.YearChange(); 
            controller.MilestoneReport(); 
               
                
        }
    }
}