/*
* Test for IdmsUimsUserSync
* */
@isTest
public class IdmsUimsUserSyncTest{
    //Test 1: this method is used to add sync logic
    static testmethod void UserSyncLogicHomeTest(){
        Set<Id> uimsIds = new Set<Id>(); 
        User user1= new User(alias = 'userpass', email='userpass' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                             BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='userpass' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                             Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test987',
                             IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                             IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert user1 ;
        
        UIMS_User__c uims1= new UIMS_User__c(email__c='tstuser122434' + '@accenture.com',Cell__c='testcell',FirstName__c='test1',LastName__c='test2',
                                             CountryCode__c='US',LanguageCode__c='LT',Phone__c='3929439',JobTitle__c='Testtitle',
                                             JobFunction__c='testing',JobDescription__c='testdesc',Street__c='some',
                                             AddInfoAddress__c='testadd',LocalityName__c='mylocal',PostalCode__c='2358483',
                                             State__c='CA', User__c=user1.Id,County__c='IN',PostOfficeBox__c='boxtest',
                                             MiddleName__c='midtest',Salutation__c='Z003',Fax__c='3928483',FederatedID__c='85839204',
                                             CompanyId__c='84930',PrimaryContact__c='testprim',GoldenId__c='59492',AddInfoAddressECS__c='testecs',
                                             CountyECS__c='tstsce', GivenNameECS__c='testname',LocalityNameECS__c='localname',
                                             SnECS__c='testsn',StreetECS__c='tstst',Vnew__c='testnew',updateSource__c='UIMS');
        insert uims1 ;
        
        uimsIds.add(uims1.id) ; 
        IdmsUimsUserSync userSync = new IdmsUimsUserSync() ; 
        userSync.UserSyncLogic(uimsIds) ; 
        
    }
    //Test 2: this method is used to add sync logic for test
    static testmethod void UserSyncLogicWrkTest(){
        Set<Id> uimswrkIds = new Set<Id>(); 
        User userwrk= new User(alias = 'userwrk', email='userwrk' + '@accenture.com', 
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userwrk' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test987',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userwrk;
        
        UIMS_User__c uimswrk = new UIMS_User__c(email__c='tstuser122454' + '@accenture.com',Cell__c='testcell',FirstName__c='test1',LastName__c='test2',
                                                CountryCode__c='US',LanguageCode__c='LT',Phone__c='3929439',JobTitle__c='Testtitle',
                                                JobFunction__c='testing',JobDescription__c='testdesc',Street__c='some',
                                                AddInfoAddress__c='testadd',LocalityName__c='mylocal',PostalCode__c='2358483',
                                                State__c='CA', User__c=userwrk.Id,County__c='IN',PostOfficeBox__c='boxtest',
                                                MiddleName__c='midtest',Salutation__c='Z003',Fax__c='3928483',FederatedID__c='85839212',
                                                CompanyId__c='84930',PrimaryContact__c='testprim',GoldenId__c='59492',AddInfoAddressECS__c='testecs',
                                                CountyECS__c='tstsce', GivenNameECS__c='testname',LocalityNameECS__c='localname',
                                                SnECS__c='testsn',StreetECS__c='tstst',Vnew__c='testnew',updateSource__c='UIMS');
        insert uimswrk ;
        
        uimswrkIds.add(uimswrk.id) ; 
        IdmsUimsUserSync userSync = new IdmsUimsUserSync() ; 
        userSync.UserSyncLogic(uimswrkIds) ; 
        
    }
    //Test 3: this method is used to add sync logic for company
    static testmethod void enrichUserCompanyInfoTest(){
        User user= new User(alias = 'userens', email='userens' + '@accenture.com', 
                            emailencodingkey='UTF-8', lastname='userens', languagelocalekey='en_US', 
                            localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                            BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                            timezonesidkey='Europe/London', username='userens' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                            Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test987',
                            IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                            IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert user;
        
        Boolean isPerson = false; 
        Account Acc = new Account();
        if(isPerson){
            RecordType rt = getRecordType('Account','PersonAccount');
            Acc.recordtypeid = rt.id;
            Acc.firstname = 'test';
            Acc.lastname = 'test';
            Acc.personemail = 'Scenario2Exception@accenture.com';
            
        }
        else{
            RecordType rt = getRecordType('Account','Supplier');
            Acc.Name = 'Demo Account Sept16';
            Acc.recordtypeid = rt.id;
        }
        
        insert Acc;
        
        UIMS_Company__c userCompany= new UIMS_Company__c(OrganizationName__c='testorg',GoldenId__c='12342',CountryCode__c='IN',
                                                         CurrencyCode__c='INR',MarketSegment__c='ID5',PostalCode__c='2134534',FederatedID__c='95838524',
                                                         PostOfficeBox__c='testbox',St__c='CA',Street__c='teststreet',TelephoneNumber__c='38483845',
                                                         HeadQuarter__c='testhead',AddInfoAddress__c='testinfo',PostalCity__c='post',WebSite__c='testweb',
                                                         AnnualSales__c='testsale',MarketServed__c='testmarket',EmployeeSize__c='34',account__c = Acc.id,Vnew__c='1');
        
        insert userCompany;
        
        
        IdmsUimsUserSync userSync = new IdmsUimsUserSync() ; 
        userSync.enrichUserCompanyInfo(user,userCompany) ; 
        
        UIMS_Company__c userCompany1= new UIMS_Company__c(OrganizationName__c='testorg',GoldenId__c='12342',CountryCode__c='IN',
                                                          CurrencyCode__c='INR',MarketSegment__c='ID5',PostalCode__c='2134534',FederatedID__c='95838524',
                                                          PostOfficeBox__c='testbox',St__c='CA',Street__c='teststreet',TelephoneNumber__c='38483845',
                                                          HeadQuarter__c='testhead',AddInfoAddress__c='testinfo',PostalCity__c='post',WebSite__c='testweb',
                                                          AnnualSales__c='testsale',MarketServed__c='testmarket',EmployeeSize__c='34',account__c = Acc.id,Vnew__c='1');
        
        insert userCompany1;
        userSync.enrichUserCompanyInfo(user,userCompany1) ; 
        
    }
    //Test 4: this method is used to update Account
    static testmethod void updateAccountTest(){
        List<IDMS_field_Mapping__c> LstFiedMap = new List<IDMS_field_Mapping__c>();
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'AdditionalAddress__pc',userfield__c='IDMS_AdditionalAddress__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'AdditionalAddress__c',userfield__c='IDMS_AdditionalAddress__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'City__c',userfield__c='City'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'City__pc',userfield__c='City'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'CorrespLang__pc',userfield__c='IDMS_PreferredLanguage__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Country__pc',userfield__c='Country'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Country__c',userfield__c='Country'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'County__pc',userfield__c='IDMS_County__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'County__c',userfield__c='IDMS_County__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'CurrencyIsoCode',userfield__c='DefaultCurrencyIsoCode'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Fax',userfield__c='Fax'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'IDMS_Account_UserExternalId__c',userfield__c='username'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'IDMS_Contact_UserExternalId__pc',userfield__c='username'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalCity__c',userfield__c='City'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalCounty__c',userfield__c='IDMS_County__c'));                
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalAdditionalAddress__c',userfield__c='IDMS_AdditionalAddress__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalFirstName__pc',userfield__c='Firstname'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalLastName__pc',userfield__c='Lastname'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'MarcomPrefEmail__pc',userfield__c='IDMS_Email_opt_in__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'PersonEmail',userfield__c='Email'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'WorkPhone__pc',userfield__c='Phone'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'PersonMobilePhone',userfield__c='MobilePhone'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'POBox__c',userfield__c='IDMS_POBox__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'POBox__pc',userfield__c='IDMS_POBox__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Street__c',userfield__c='Street'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'StreetLocalLang__c',userfield__c='Street'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Street__pc',userfield__c='Street'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'ZipCode__c',userfield__c='PostalCode'));      
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'ZipCode__pc',userfield__c='PostalCode'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'StateProvince__c',userfield__c='State'));
        if(LstFiedMap != null && LstFiedMap.size() > 0)
            insert LstFiedMap;
        
        Boolean isPerson = false; 
        Account Acc = new Account();
        if(isPerson){
            RecordType rt = getRecordType('Account','PersonAccount');
            Acc.recordtypeid = rt.id;
            Acc.firstname = 'test';
            Acc.lastname = 'test';
            Acc.personemail = 'Scenario2Exception@accenture.com';
            
        }
        else{
            RecordType rt = getRecordType('Account','Supplier');
            Acc.Name = 'Demo Account Sept16';
            Acc.recordtypeid = rt.id;
        }
        
        insert Acc;
        User user= new User(alias = 'useracc', email='useracc' + '@accenture.com', 
                            emailencodingkey='UTF-8', lastname='userens', languagelocalekey='en_US', 
                            localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                            BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                            timezonesidkey='Europe/London', username='useracc' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                            Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test987',
                            IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                            IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert user;
        Contact Ct= new Contact(FirstName='Test',
                                LastName='ContactName',
                                AccountId=Acc.Id,
                                JobTitle__c='Z3',
                                CorrespLang__c='EN',
                                WorkPhone__c='1234567890',
                                IDMS_Contact_UserExternalId__c=user.username);  
        insert Ct ; 
        user.Contact = Ct ;
        update user ; 
        
        User contactUser = [select id,contactid from user where id =: user.id] ; 
        
        Test.startTest();
        IdmsUimsUserSync userSync = new IdmsUimsUserSync() ; 
        userSync.updateAccount(String.valueOf(contactUser.contactid),user) ; 
        Test.stopTest();
        
    }
    //Test 5: this method is used to update IDMS Account
    static testmethod void updateIdmsAccountsTest(){
        Boolean isPerson = false ; 
        Account Acc = new Account();
        if(isPerson){
            RecordType rt = getRecordType('Account','PersonAccount');
            Acc.recordtypeid = rt.id;
            Acc.firstname = 'test';
            Acc.lastname = 'test';
            Acc.personemail = 'Scenario2Exception@accenture.com';
            
        }
        else{
            RecordType rt = getRecordType('Account','Supplier');
            Acc.Name = 'Demo Account Sept16';
            Acc.recordtypeid = rt.id;
        }
        
        insert Acc; 
        List<Account> listAccount = new List<Account>(); 
        listAccount.add(Acc);
        
        String accountJson = JSON.serialize(listAccount);
        Test.startTest();
        IdmsUimsUserSync.updateIdmsAccounts(accountJson) ; 
        
        Test.stopTest();
        
    }
    //Test 6: this method is used to get record type
    public static RecordType getRecordType(String SObjectApiName , String recordTypeDevName){
        RecordType recType = [Select  DeveloperName,Name, Id From RecordType where SobjectType = :SObjectApiName and DeveloperName = :recordTypeDevName];
        return recType;
    }
    //Test 7: this method is used to test market segment
    static testmethod void testMarketSegment(){
        IDMS_UIMS_Mapping.getMarketSubSegmentMap();
    }
}