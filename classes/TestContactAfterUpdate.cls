@isTest
public class TestContactAfterUpdate {
    static testMethod void test(){
        
        Set<String> conSet = new Set<String>();     
        Country__c cou = testObjectCreator.CreateCountry('Cou_1');

        Account acc = testObjectCreator.CreateAccount('Acc_1',cou.Id);

        Contact con = testObjectCreator.CreateContact('Con_1',acc.Id);

        Zuora__CustomerAccount__c ba = testObjectCreator.CreateBillingAccount('BillAcc_1', 'Con_1', acc.Id);

        con.Email = 'Test@kerensen.com';

        try{
            update con;
        }catch(Exception e){
            try{
                conSet.add(con.Id);
                Handler_ContactAfterUpdate.updateContact(conSet);
            }catch(Exception e2){
            }
        }
     
    }
   
}