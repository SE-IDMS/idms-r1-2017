/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: VF Page Controller                                  *
* Page: Fielo_Contact                                       *
* Created Date: 14/08/2014                                  *
************************************************************/
public without sharing class Fielo_ContactController{
    
    public FieloEE__Member__c fMember {get; set;}
    public String message {get;set;}
    public Boolean isLogged {get;set;}
    public String selectedNatureOfEnquiry {get;set;}
    public String selectedNatureOfIssue {get;set;}
    public Fielo_ContactInformation__c contactInfo {get;set;}
    public Boolean showForm {get;set;}
    
    public List<Fielo_ContactInformation__c> listContactInfo;

    
    public Fielo_ContactController(){
    
        showForm = true;

        listContactInfo = [SELECT Id, F_ContactName__c, F_ContactNumber__c, F_ContactURLImage__c, F_ContactEmail__c FROM Fielo_ContactInformation__c LIMIT 1];

        contactInfo = listContactInfo[0];


        String memId = FieloEE.MemberUtil.getMemberId();
        if(memId != null){
            isLogged = true;
            fMember = [SELECT Id, FieloEE__Email__c, F_Title__c, FieloEE__FirstName__c, FieloEE__LastName__c, F_CompanyName__c, F_CompanyPhoneNumber__c, FieloEE__Phone__c FROM FieloEE__Member__c WHERE Id =: memId];
        }else{
            isLogged = false;
            fMember = new FieloEE__Member__c();
        }
        message = '';

    }
    
    public List<SelectOption> getNatureOfEnquiryValues(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        options.add(new SelectOption(Label.Fielo_ContactControllerIssue, Label.Fielo_ContactControllerIssue));
        options.add(new SelectOption(Label.Fielo_ContactControllerAdditionalInfo, Label.Fielo_ContactControllerAdditionalInfo));  
        options.add(new SelectOption(Label.Fielo_ModifyMyProfile, Label.Fielo_ModifyMyProfile)); 
        return options;
    }

    public List<SelectOption> getNatureOfIssueValues(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        if([SELECT Name FROM Profile WHERE Id =: UserInfo.getProfileId() LIMIT 1].Name == 'Fielo Member Site'){
            options.add(new SelectOption(Label.Fielo_ContactControllerGifts, Label.Fielo_ContactControllerGifts));
            options.add(new SelectOption(Label.Fielo_ContactControllerInvoices, Label.Fielo_ContactControllerInvoices));   
        }
        options.add(new SelectOption(Label.Fielo_ContactControllerWebUsage, Label.Fielo_ContactControllerWebUsage));
        options.add(new SelectOption(Label.Fielo_ContactControllerOther, Label.Fielo_ContactControllerOther));  
        return options;
    }
    
    public pageReference doSend(){
        
        if(fMember.FieloEE__FirstName__c == null || fMember.FieloEE__LastName__c == '' || fMember.FieloEE__Email__c == null || fMember.F_CompanyName__c == null || message == ''){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_ContactControllerReqMessage));
            return null;
        }
        
        
        if(selectedNatureOfEnquiry == 'Issue' && selectedNatureOfIssue == null){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_ContactControllerReqField));
            return null;
        }
        
        if(message.split(' ').size() > 1500){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_ContactControllerCannotWords));
            return null;             
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {listContactInfo[0].F_ContactEmail__c};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Loyalty Program Contact');
         
        string replace1 = 'Dear ' + listContactInfo[0].F_ContactName__c + ',<br/><br/>';
        replace1 += 'A new contact form was submitted for the Loyalty Program.<br/><br/>';
        replace1 += '<b>Nature of the enquiry:</b> ' + selectedNatureOfEnquiry + '<br/>';
        replace1 += '<b>Nature of the issue:</b> ' + selectedNatureOfIssue + '<br/>';                 
        replace1 += '<b>Message:</b> ' + message + '<br/><br/><br/>';  
        
        string replace2;
        if(isLogged){
            replace2 = 'Member information <br/><br/>'; 
        }else{
            replace2 = 'Guest user information <br/><br/>'; 
        } 
        replace2 += '<b>Title:</b> ' + (fMember.F_Title__c == null ? '' : fMember.F_Title__c) + '<br/>';  
        replace2 += '<b>First Name:</b> ' + fMember.FieloEE__FirstName__c + '<br/>';  
        replace2 += '<b>Last Name:</b> ' + fMember.FieloEE__LastName__c + '<br/>';  
        replace2 += '<b>E-mail:</b> ' + fMember.FieloEE__Email__c + '<br/>';  
        replace2 += '<b>Company Name:</b> ' + fMember.F_CompanyName__c + '<br/>';           
        
        List<Document> logo = [SELECT id FROM Document WHERE DeveloperName = 'Fielo_SchneiderLogo']; 
        
        String cuerpoDeMail = '<table border="1"><tbody><tr valign="top"><td width="598" bgcolor="#FFFFFF" valign="middle"><div align="center"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr valign="top"><td width="588" valign="middle"><div align="right"><img src="' + (logo.size() > 0 ? URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.ImageServer?id=' + logo[0].Id + '&oid=' + UserInfo.getOrganizationId()  : '') + '" width="147" height="51" alt="Schneider-Electric Logo"></div></td></tr><tr valign="top"><td width="588" valign="middle"><ul style="padding-left:0pt"><font size="3" face="serif"><br> </font></ul></td></tr><tr valign="top"><td width="588" bgcolor="#009933" valign="middle"><ul style="padding-left:8pt"><font size="4" color="#FFFFFF" face="Arial">Schneider Electric Rewards Program - Contact Request </font></ul></td></tr><tr valign="top"><td width="588" valign="middle"><ul style="padding-left:0pt"><font size="3" face="serif"><br> </font></ul></td></tr><tr valign="top"><td width="588" bgcolor="#DDDDDF" valign="middle"><ul style="padding-left:8pt"><font size="1" color="#626469" face="Helvetica">' + replace1 +'</font></ul></td></tr><tr valign="top"><td width="588" valign="middle"><ul style="padding-left:0pt"><font size="3" face="serif"><br> </font></ul></td></tr><tr valign="top"><td width="588" valign="middle"><ul style="padding-left:8pt"><font size="1" color="#626469" face="Helvetica">' + replace2 + '</font></ul></td></tr><tr valign="top"><td width="588" valign="middle"><ul style="padding-left:0pt"><font size="3" face="serif"><br> </font></ul></td></tr><tr valign="top"><td width="588" bgcolor="#DDDDDF" valign="middle"><ul style="padding-left:0pt"><font size="1" color="#626469" face="Verdana">© 2013 Schneider Electric. All rights reserved.</font></ul></td></tr></tbody></table></div></td></tr></tbody></table>';

        mail.setHTMLBody(cuerpoDeMail);         
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            showForm = false;
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, Label.Fielo_ContactControllerSuccessMessage));
        }catch(Exception e){
        
        }

        return null;    
    
    }

}