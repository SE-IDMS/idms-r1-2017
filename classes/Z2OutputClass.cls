Global class Z2OutputClass
{
    global class IdMap
    {
        webservice String Id1;
        webservice String Id2;
    }
    
    global class cloneContractResult
    {
        webservice String ContractID;
        webservice List <IdMap> NotesMap;
        webservice List <IdMap> AttachmentsMap;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static cloneContractResult createcloneContractResult (String Success, String ErrorMessage, String ContractID, Map <Id, Id> NotesMap, Map <Id, Id> AttachmentsMap)
    {
        cloneContractResult result = new cloneContractResult();
        
        IdMap idPair = null;
        
        if (ContractID != null)
        {
          result.ContractID = ContractID;
            
          // Copy the Attachment Ids into the result set
            
          if (AttachmentsMap != null)
          {
          	result.AttachmentsMap = new List <IdMap>();
          	
            for (String attkey : AttachmentsMap.keySet())
            {
              idPair = new IdMap();
              
              idPair.Id1 = attkey;
              idPair.Id2 = (String)AttachmentsMap.get(attkey);
              
              result.AttachmentsMap.add (idPair);
            }
          }
          
          if (NotesMap != null)
          {  
            // Copy the Note Ids into the result set
            
            result.NotesMap = new List <IdMap>();
            
            for (String notekey : NotesMap.keySet())
            {
              idPair = new IdMap();
              
              idPair.Id1 = notekey;
              idPair.Id2 = (String)NotesMap.get(notekey);
              
              result.NotesMap.add (idPair);
            }
          }
        }    
        
        result.errorCode    = success;
        result.errorMessage = errorMessage; 
        
        return result;
    } 
    
    global class CVCPResult
    {
        webservice String CVCPID;
        webservice String ContractID;
        webservice String ContractNumber;
        webservice String ContractName;
        
    }
    
    global class ProductResult
    {
        webservice list<CVCPResult> lstCVCP;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static ProductResult createProductResult(String Success, String ErrorMessage, list<CTR_ValueChainPlayers__c> lstCVCP)
    {
        ProductResult result = new ProductResult();
        result.lstCVCP = new list<CVCPResult>();
        if(lstCVCP != null && ! lstCVCP.isEmpty())
        {
            for(CTR_ValueChainPlayers__c resultCVCP : lstCVCP)
            {
                CVCPResult newResult = new CVCPResult();
                newResult.CVCPID= resultCVCP.Id;
                newResult.ContractID= resultCVCP.contract__c;
                newResult.ContractNumber= resultCVCP.contract__r.contractnumber;
                newResult.ContractName= resultCVCP.contract__r.name;
                result.lstCVCP.add(newResult);
            }
        }    
            result.errorCode= success;
            result.errorMessage = errorMessage; 
        
        return result;
    } 
    
    global class CaseCreationResult
    {
        //webservice String CaseNumber;
        webservice String CaseID;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static CaseCreationResult createCaseResult(String Success, String ErrorMessage, Case resultCase)
    {
        CaseCreationResult result = new CaseCreationResult();
        if(resultCase!= null)
        {
            result.CaseID = resultCase.Id;
            
        }    
            result.errorCode= success;
            result.errorMessage = errorMessage; 
        
        return result;
    } 
    
    global class CaseSearchResult
    {
        
        webservice String ContactName;
        webservice String AccountName;
        webservice String ParentAccount;
        webservice String Status;
        webservice String CaseID;
        webservice String CaseNumber;
        webservice String Subject;
        webservice String Family;
        webservice DateTime DateOpen;
        webservice DateTime DateClosed;
        
    }
    
    global class CaseSearchResultList
    {
        webservice list<CaseSearchResult> lstCaseResults;       
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static CaseSearchResultList createCaseSearchResult(String Success, String ErrorMessage, list<Case> lstResultCase)
    {
        CaseSearchResultList SearchResult = new CaseSearchResultList();
        list<CaseSearchResult> lstCaseSearchResult = new list<CaseSearchResult>();
        if(lstResultCase!= null && !lstResultCase.isEmpty())
        {
            for(Case resultCase : lstResultCase)
            {
                CaseSearchResult result = new CaseSearchResult();
                result.ContactName= resultCase.Contact.Name;
                result.AccountName= resultCase.Account.Name;
                if(resultCase.Account.Parent != null)
                    result.ParentAccount= resultCase.Account.Parent.Name;
                else
                    result.ParentAccount= resultCase.Account.Name;
                    //result.ParentAccount= System.Label.CLOCT12CCC02;
                result.Status= resultCase.Status;
                result.CaseID= resultCase.Id;
                result.CaseNumber= resultCase.CaseNumber;
                result.Subject= resultCase.Subject;
                result.Family= resultCase.Family__c;
                result.DateOpen= resultCase.CreatedDate;
                result.DateClosed= resultCase.ClosedDate;
                lstCaseSearchResult.add(result);
            }
        }    
            SearchResult.errorCode= success;
            SearchResult.errorMessage = errorMessage; 
            SearchResult.lstCaseResults = lstCaseSearchResult;
            return SearchResult ;
    } 
    
    global class CaseResult
    {
        webservice String ContactName;
        webservice String AccountName;
        webservice String CaseNumber;
        webservice String CaseID;
        webservice String Status;
        webservice String CCCCountry;
        webservice String CaseOrigin;
        webservice String Priority;
        webservice String Subject;
        webservice String Reason;
        webservice String SubReason;
        webservice String Family;
        webservice String CustomerRequest;
        webservice String AnswerToCustomer;
        webservice DateTime DateOpened;
        webservice DateTime DateClosed;
        webservice String ContractNumber;
        webservice String ContractID;
        webservice String GMRCode;
        webservice Boolean MyComment;
        webservice list<CaseCommentResult> lstcaseComment;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    global class CaseCommentResult
    {
         webservice String CommentOwner;
         webservice String Comments;
         webservice DateTime CommentCreatedDate;
    }
    public static CaseResult createCaseResult(String Success, String ErrorMessage, Case resultCase, list<CaseComment> lstCaseComment,Boolean MyComment)
    {
        CaseResult result = new CaseResult();
        result.lstcaseComment = new list<CaseCommentResult>();
        if(resultCase!= null)
        {
            result.ContactName= resultCase.Contact.Name;
            result.AccountName= resultCase.Account.Name;
            result.CaseNumber= resultCase.CaseNumber;
            result.CaseID= resultCase.Id;
            result.Status= resultCase.Status;
            result.CCCCountry= resultCase.CCCountry__r.Name;
            result.CaseOrigin= resultCase.Origin;
            result.Priority= resultCase.Priority;
            result.Subject= resultCase.Subject;
            result.Reason= resultCase.Symptom__c;
            //result.SubReason= resultCase.SubSymptom__c; 
            result.Family= resultCase.Family__c;
            result.CustomerRequest= resultCase.CustomerRequest__c;
            result.AnswerToCustomer= resultCase.AnswerToCustomer__c;
            result.DateOpened= resultCase.CreatedDate;
            result.DateClosed= resultCase.ClosedDate;
            result.ContractNumber= resultCase.RelatedContract__r.ContractNumber;
            result.GMRCode = resultCase.TECH_GMRCode__c;
            if(resultCase.RelatedContract__r.Status == System.Label.CLSEP12CCC54 && resultCase.RelatedContract__r.expired__c == false && resultCase.RelatedContract__r.webaccess__c == System.Label.CLOCT12CCC07 && resultCase.RelatedContract__r.startDate <= system.today() && resultCase.RelatedContract__r.EndDate >=system.today())
                result.ContractID= resultCase.RelatedContract__c;
            result.MyComment= MyComment;

            if(lstCaseComment != null && !lstCaseComment.isempty())
            {
                for(CaseComment comment : lstCaseComment )
                {
                    CaseCommentResult resultComment = new CaseCommentResult();
                    resultComment.CommentOwner = comment.CreatedBy.Name;
                    resultComment.Comments = comment.CommentBody;
                    resultComment.CommentCreatedDate = comment.createddate;
                    result.lstcaseComment.add(resultComment);
                }
            }
        }    
            result.errorCode= success;
            result.errorMessage = errorMessage; 
        
        return result;
    } 
    
    global class ContractSearchResult
    {
         webservice String ContractID;
         webservice String ContractNumber;
         webservice String AccountName;
         webservice String AccountCountry;
         webservice String Company;
         webservice list<PackageTemplateResult> lstPackageTemplate;
         webservice String Status;
         webservice DateTime StartDate;
         webservice DateTime EndDate;
         
    }
    
    global class PackageTemplateResult
    {
        webservice String PackageTemplateName;
        webservice String ServiceNumber;
    }    
    
    global class ContractSearchResultList
    {
        webservice list<ContractSearchResult> lstContractSearchResult;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
   /* public static ContractSearchResultList createContractSearchResult(String Success, String ErrorMessage, map<Id,Contract> mapResultContract, map<Id,set<PackageTemplate__c>> mapContractPKG)
    {

        ContractSearchResultList SearchResult = new ContractSearchResultList();
        list<ContractSearchResult> lstContractResult = new list<ContractSearchResult>();
        //map<Id,ContractSearchResult> mapIdResult = new map<Id,ContractSearchResult>();
        if(mapResultContract!= null && !mapResultContract.isEmpty())
        {
            for(ID resultContract : mapResultContract.keySet())
            {
                ContractSearchResult result = new ContractSearchResult();
                result.ContractID= resultContract;
                result.ContractNumber= mapResultContract.get(resultContract).ContractNumber;
                result.AccountName= mapResultContract.get(resultContract).Account.Name;
                result.AccountCountry= mapResultContract.get(resultContract).Account.Country__r.Name;
                if(mapResultContract.get(resultContract).ContactName__r.Account.Parent.Name != null && mapResultContract.get(resultContract).ContactName__r.Account.Parent.Name != '')
                    result.Company = mapResultContract.get(resultContract).Account.Parent.Name;
                else
                    result.Company = mapResultContract.get(resultContract).Account.Name;
                result.Status= mapResultContract.get(resultContract).Status;
                result.StartDate= mapResultContract.get(resultContract).startdate;
                result.EndDate= mapResultContract.get(resultContract).enddate;
                
                result.lstPackageTemplate = new list<PackageTemplateResult>();
                if(mapContractPKG.containsKey(resultContract))
                for(PackageTemplate__c pkgTemplate : mapContractPKG.get(resultContract))
                {
                    PackageTemplateResult resultPKG = new PackageTemplateResult();
                    resultPKG.PackageTemplateName= pkgTemplate.Name;
                    resultPKG.ServiceNumber = pkgTemplate.servicenumber__C    ;
                    result.lstPackageTemplate.add(resultPKG);
                }
                lstContractResult.add(result);
            }
            
        }    
            SearchResult.errorCode= success;
            SearchResult.errorMessage = errorMessage; 
            SearchResult.lstContractSearchResult = lstContractResult ;
            return SearchResult;
    }*/
// Added on 15 Nov for new contract definition
    public static ContractSearchResultList createContractSearchResult(String Success, String ErrorMessage, map<Id,Package__c> mapResultContract, map<Id,list<Package__c>> mapContractPKG)
    {

        ContractSearchResultList SearchResult = new ContractSearchResultList();
        list<ContractSearchResult> lstContractResult = new list<ContractSearchResult>();
        //map<Id,ContractSearchResult> mapIdResult = new map<Id,ContractSearchResult>();
        if(mapResultContract!= null && !mapResultContract.isEmpty())
        {
            for(ID resultContract : mapResultContract.keySet())
            {
                ContractSearchResult result = new ContractSearchResult();
                result.ContractID= resultContract;
                result.ContractNumber= mapResultContract.get(resultContract).Contract__r.ContractNumber;
                result.AccountName= mapResultContract.get(resultContract).Contract__r.Account.Name;
                result.AccountCountry= mapResultContract.get(resultContract).Contract__r.Account.Country__r.Name;
                if(mapResultContract.get(resultContract).Contract__r.Account.Parent.Name != null && mapResultContract.get(resultContract).Contract__r.Account.Parent.Name != '')
                    result.Company = mapResultContract.get(resultContract).Contract__r.Account.Parent.Name;
                else
                    result.Company = mapResultContract.get(resultContract).Contract__r.Account.Name;
                result.Status= mapResultContract.get(resultContract).Contract__r.Status;
                result.StartDate= mapResultContract.get(resultContract).Contract__r.startdate;
                result.EndDate= mapResultContract.get(resultContract).Contract__r.enddate;
                
                result.lstPackageTemplate = new list<PackageTemplateResult>();
                if(mapContractPKG.containsKey(resultContract))
                for(Package__c pkgTemplate : mapContractPKG.get(resultContract))
                {
                    PackageTemplateResult resultPKG = new PackageTemplateResult();
                    resultPKG.PackageTemplateName= pkgTemplate.PackageTemplate__r.Name;
                    resultPKG.ServiceNumber = pkgTemplate.PackageTemplate__r.servicenumber__C    ;
                    result.lstPackageTemplate.add(resultPKG);
                }
                lstContractResult.add(result);
            }
            
        }    
            SearchResult.errorCode= success;
            SearchResult.errorMessage = errorMessage; 
            SearchResult.lstContractSearchResult = lstContractResult ;
            return SearchResult;
    }
//End of change on 15 Nov
    global class ContractResult
    {
         webservice String ContractNumber;
         webservice String ContractID;
         webservice String ContractName;
         webservice String ContactName;
        // webservice String AccountName;
         webservice String LevelOfContract;
         webservice String Status;
         webservice DateTime StartDate;
         webservice DateTime EndDate;
         webservice Decimal Points;
         webservice String ServiceSales;
         webservice String DistributorInsideSales;
         webservice String DistributorContact;
         webservice String AdministrativeContact;
         webservice String Company;
         webservice String Site;
         webservice String SiteCountry;
         webservice list<PackageResult> lstPackage;
         webservice String ErrorCode;
         webservice String errorMessage;
    }    
    
    global class PackageResult
    {
        webservice String PackageName;
        webservice String ServiceNumber;
        webservice String Status;
        webservice DateTime StartDate;
        webservice DateTime EndDate;
        webservice Integer CasesPurchased;
        webservice Integer CasesLeft;
    }
    
    public static ContractResult createContractResult(String Success, String ErrorMessage, Contract resultContract, list<Package__c> lstPKG)
    {
        ContractResult result = new ContractResult();
        if(resultContract!= null)
        {
            result.ContractNumber= resultContract.ContractNumber;
            result.ContractId= resultContract.Id;
            result.ContractName= resultContract.Name;
            result.ContactName= resultContract.ContactName__c;
            result.Site = resultContract.Account.Name;
            if(resultContract.ContactName__r.Account.Parent.Name != null && resultContract.ContactName__r.Account.Parent.Name!= '')
                result.Company = resultContract.Account.Parent.Name;
            else
                result.Company = resultContract.Account.Name;
            result.SiteCountry = resultContract.Account.Country__r.Name;
            result.LevelOfContract= resultContract.SupportContractLevel__c;
            result.Status= resultContract.Status;
            result.StartDate= resultContract.startDate;
            result.EndDate= resultContract.enddate;
            result.Points= resultContract.Points__c;
            result.lstPackage = new list<PackageResult>();
            if(lstPKG != null && !lstPKG.isempty())
            for(Package__c pkg : lstPKG)
            {
                PackageResult resultPKG = new PackageResult();
                resultPKG .PackageName= pkg.PackageTemplate__r.name;
                resultPKG .ServiceNumber= pkg.packagetemplate__r.servicenumber__c;
                resultPKG .Status= pkg.status__c;
                resultPKG .StartDate= pkg.startdate__c;
                resultPKG .EndDate= pkg.enddate__c;
                resultPKG .CasesPurchased= 0;
                resultPKG .CasesLeft= 0;
                result.lstPackage.add(resultPKG );
            }
        }    
            result.errorCode= success;
            result.errorMessage = errorMessage; 
        
        return result;
    } 
    
    global class CaseNoteResult
    {
        webservice String CaseNumber;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static CaseNoteResult createNoteResult(String Success, String ErrorMessage, Case resultCase)
    {
        CaseNoteResult result = new CaseNoteResult();
        if(resultCase!= null)
        {
            result.CaseNumber= resultCase.casenumber;
            
        }
         result.errorCode= success;
         result.errorMessage = errorMessage; 
         return result;
    }
    
    global class ContactResult
    {
        webservice String FirstName;
        webservice String LastName;
        webservice String AccountName;
        webservice String Email;
        webservice String AccountPhone;
        webservice String ParentAccountName;
        webservice String ParentAccountPhone;
        webservice String AccountFax;
        webservice String Street;
        webservice String AdditionalInformation  ;
        webservice String City;
        webservice String ZipCode;
        webservice String StateProvince;
        webservice String County;
        webservice String Country;
        webservice String POBox;
        webservice String POBoxZipCode;
        webservice String ContactWorkPhone;
        webservice String WorkFax;
              
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static ContactResult createContactResult(String Success, String ErrorMessage, Contact resultContact)
    {
        ContactResult result = new ContactResult();
        if(resultContact!= null)
        {
            result.FirstName= resultContact.firstname;
            result.LastName= resultContact.lastname;
            result.AccountName= resultContact.account.name;
            result.Email= resultContact.email;
            result.AccountPhone= resultContact.account.phone;
            if(resultContact.account.parent != null)
            {
                result.ParentAccountName= resultContact.account.parent.name;
                result.ParentAccountPhone= resultContact.account.parent.phone;
            }
            else
            {
                //result.ParentAccountName= System.Label.CLOCT12CCC02;
                result.ParentAccountName= resultContact.account.name;
                result.ParentAccountPhone= resultContact.account.phone;
            }
            result.AccountFax= resultContact.account.fax;
            result.Street= resultContact.Account.Street__c;
            result.AdditionalInformation  = resultContact.Account.AdditionalAddress__c;
            result.City= resultContact.Account.City__c;
            result.ZipCode= resultContact.Account.ZipCode__c;
            result.StateProvince= resultContact.Account.StateProvince__r.Name;
            result.County= resultContact.Account.County__c;
            result.Country= resultContact.Account.Country__r.Name;
            //result.POBox= resultContact.Account.POBox__c;
            //result.POBoxZipCode= resultContact.POBoxZip__c;
            result.POBox = '';
            result.POBoxZipCode='';
            result.ContactWorkPhone= resultContact.WorkPhone__c;
            result.WorkFax= resultContact.Fax;
            
        }
         result.errorCode= success;
         result.errorMessage = errorMessage; 
         return result;
    }
    
    global class ContactUpdateResult
    {
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static ContactUpdateResult createContactUpdateResult(String Success, String ErrorMessage)
    {
        ContactUpdateResult result = new ContactUpdateResult();
        result.ErrorCode = Success;
        result.ErrorMessage = ErrorMessage;
        return result;
    }
    
    global class PKGTemplateListResult
    {
        webservice String PackageTemplateID;
        webservice String PackageTemplateName;
        webservice String PackageTemplateServiceName;
       
    }
    
    global class PackageTemplateListResult
    {
        webservice list<PKGTemplateListResult> lstPackageTemplate;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static PackageTemplateListResult createTemplateResult(String Success, String ErrorMessage, list<PackageTemplate__c> lstPKGTemplate)
    {
        PackageTemplateListResult resultList = new PackageTemplateListResult();
        list<PKGTemplateListResult> lstTemplate = new list<PKGTemplateListResult>();
        if(lstPKGTemplate != null && !lstPKGTemplate.isEmpty())
        {
            for(PackageTemplate__c resultPKG : lstPKGTemplate)
            {
                PKGTemplateListResult result = new PKGTemplateListResult();
                result.PackageTemplateID= resultPKG.Id;
                result.PackageTemplateName= resultPKG.Name;
                result.PackageTemplateServiceName= resultPKG.ServiceNumber__c;
                lstTemplate.add(result);
            }
        }
        resultList.lstPackageTemplate = lstTemplate;
        resultList.ErrorCode = Success;
        resultList.ErrorMessage = ErrorMessage;
        return resultList;
    }
    
    global class ValidContactResult
    {
        webservice Boolean isDistributorContact;
        webservice Boolean isDistributorInsideSales;
        webservice Boolean isSchneiderServiceSales;
        webservice String ErrorCode;
        webservice String ErrorMessage;
    }
    public static ValidContactResult createRoleResult(String Success, String ErrorMessage, Boolean isDistributorContact , Boolean isDistributorInsideSales,Boolean isSchneiderServiceSales )
    {
        ValidContactResult result = new ValidContactResult();
        result.isDistributorContact = isDistributorContact;
        result.isDistributorInsideSales = isDistributorInsideSales;
        result.isSchneiderServiceSales = isSchneiderServiceSales;
        result.ErrorCode = Success;
        result.ErrorMessage = ErrorMessage;
        return result;
    }
}