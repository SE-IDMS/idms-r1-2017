@isTest
private class AP52_ContractLinkFieldUpdate_TEST {
   private static Account acc;
    private static Opportunity opp;
    
    static{
        // create test data
      //  System.debug('....here 1....');
       acc = Utils_TestMethods.createAccount();
       insert acc;
      // System.debug('....here 2....'+acc.id);
        Country__c cntry = new Country__c (name='FRANCE',CountryCode__c='FR');
        insert cntry;
        opp = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
      //  System.debug('....here 3....'+opp.id);
    }
   
    
    static testMethod void testContractLinkUpdates() {
        //coverage for current approver and last approver field populate
       // system.debug('..@...'+opp.id);
       // System.debug('...#...'+UserInfo.getUserId());
        OPP_ContractLink__c c00 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, TECH_ApprovalStep__c = 0);
        OPP_ContractLink__c c01 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 1);
        OPP_ContractLink__c c02 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 2);
        OPP_ContractLink__c c03 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 3);
        OPP_ContractLink__c c04 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 4);
        OPP_ContractLink__c c05 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 5);
        OPP_ContractLink__c c06 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 6);
        OPP_ContractLink__c c07 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 7);
        OPP_ContractLink__c c08 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), Approver8__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 8);
        OPP_ContractLink__c c09 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), Approver8__c = UserInfo.getUserId(), Approver9__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 9);
        OPP_ContractLink__c c10 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), Approver8__c = UserInfo.getUserId(), Approver9__c = UserInfo.getUserId(), Approver10__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 10);
        OPP_ContractLink__c c11 = new OPP_ContractLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 1,ContractStatus__c=label.CL00242); //label.CL00242 = 'Approved'
        List<OPP_ContractLink__c> cl_list = new List<OPP_ContractLink__c>();
        cl_list.add(c00);
        cl_list.add(c01);
        cl_list.add(c02);
        cl_list.add(c03);
        cl_list.add(c04);
        cl_list.add(c05);
        cl_list.add(c06);
        cl_list.add(c07);
        cl_list.add(c08);
        cl_list.add(c09);
        cl_list.add(c10);
        cl_list.add(c11);
        insert cl_list;
        
        cl_list[0].TECH_ApprovalStep__c = 1;
        update cl_list[0];
        
        cl_list[0].ContractStatus__c = 'Approved';
        update cl_list[0];
        
        cl_list[0].ActiveContract__c = true;
        update cl_list[0];
    }
}