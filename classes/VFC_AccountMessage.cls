public with sharing class VFC_AccountMessage{
public boolean displayErrorMessage{get;set;}{displayErrorMessage=false;}
private Account Accountrecord=null;
    public VFC_AccountMessage(ApexPages.StandardController controller) {
        Accountrecord=(Account)controller.getRecord();                
        if(Accountrecord.ToBeDeleted__c){
        for(Account acc: [Select id, (select id from Leads__r),(select id from Opportunities), (select id from Cases) From Account Where id=:Accountrecord.id])
        {
            if(acc.Leads__r.size()>0 || acc.Opportunities.size()>0 || acc.Cases.size()>0)
                displayErrorMessage=true;
            else
                displayErrorMessage=false;
        }                
    }

}
}