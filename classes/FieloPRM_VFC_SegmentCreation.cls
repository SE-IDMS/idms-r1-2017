public with sharing class FieloPRM_VFC_SegmentCreation {
	

	public FieloPRM_VFC_SegmentCreation(ApexPages.StandardController controller) {
		
	}

	public PageReference redirect(){

		PageReference algoPage = ApexPages.currentPage();


		PageReference returnPage = new PageReference('/apex/FieloEE__RedemptionRuleEditBackEnd');		

		for(String saux : algoPage.getParameters().keyset()){

			returnPage.getParameters().put(saux,algoPage.getParameters().get(saux));
		}
		returnPage.getParameters().put('showCriteria','1');
		//new PageReference('/apex/FieloEE__RedemptionRuleEditBackEnd?retURL=%2FaAa%2Fo&RecordType=012A0000000oDfD&ent=01IA0000002KfGH&save_new=1&sfdc.override=1');


		return returnPage;
	}

}