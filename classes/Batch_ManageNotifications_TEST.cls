@isTest
private class Batch_ManageNotifications_TEST{
    public static testmethod void testMethod2(){
        List<RecordType> recordTypeLsts = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Notification' LIMIT 1]);
        FieloEE.MockUpFactory.setCustomProperties(false);
        Country__c cntrys = Utils_TestMethods.createCountry();
        cntrys.countrycode__c ='US';
        insert cntrys;
        
        PRMCountry__c pCntrys = new PRMCountry__c();
        pCntrys.Name = 'USA';
        pCntrys.CountryPortalEnabled__c = true;
        pCntrys.Country__c = cntrys.Id;
        pCntrys.SupportedLanguage1__c = 'en_US';
        pCntrys.PLDatapool__c = 'en_US2';
        insert pCntrys;
        
        SetupAlerts__c setupAlerts = new SetupAlerts__c();
        setupAlerts.Name = 'Test Alert';
        setupAlerts.PRMCountryClusterSetup__c = pCntrys.Id;
        setupAlerts.Status__c = 'Published';
        setupAlerts.Processed__c = false;
        setupAlerts.RecordTypeId = recordTypeLsts[0].Id;
        insert setupAlerts;
        
        Test.startTest();
        setupAlerts.Status__c = 'Revoked';
        update setupAlerts;
        Batch_RevokeNotifications batchInstance1 = new Batch_RevokeNotifications();
        ID batchprocessid1 = Database.executeBatch(batchInstance1);
        batchInstance1.query = 'SELECT Id, Name FROM SetupAlerts__c WHERE Status__c = \'Revoked\'';
        batchProcessId1 = Database.executeBatch(batchInstance1);
        Batch_RevokeNotifications sh1 = new Batch_RevokeNotifications();
        String sch = '0 0 19 * * ? *'; 
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest();
    }
    public static testmethod void testMethod1()
    {
         List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0)
        {
            User user = new User(alias = 'test', email='test'+ '@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profiles[0].Id,  
            timezonesidkey='Europe/London', username='test'+ '@bridge-fo.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='USA',bypassVR__c = true,IsActive = true);
            system.runas(user)
            {
                FieloEE.MockUpFactory.setCustomProperties(false);
                Country__c cntry = Utils_TestMethods.createCountry();
                cntry.countrycode__c ='US';
                insert cntry;
                
                Country__c cntry1 = Utils_TestMethods.createCountry();
                cntry1.countrycode__c ='IN';
                insert cntry1;
                
                Country__c cntry2 = Utils_TestMethods.createCountry();
                cntry2.countrycode__c ='RU';
                insert cntry2;
                
                Country__c cntry3 = Utils_TestMethods.createCountry();
                cntry3.countrycode__c ='CA';
                insert cntry3;
                
                Country__c cntry4 = Utils_TestMethods.createCountry();
                cntry4.countrycode__c ='FR';
                insert cntry4;
                
                Country__c cntry5 = Utils_TestMethods.createCountry();
                cntry5.countrycode__c ='GB';
                insert cntry5;
                
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = cntry.id;
                insert acc;
                
                acc.isPartner = true;
                acc.PRMAccount__c = true;
                update acc;
                        
                PRMCountry__c pCntry = new PRMCountry__c();
                pCntry.Name = 'USA';
                pCntry.CountryPortalEnabled__c = true;
                pCntry.Country__c = cntry.Id;
                pCntry.SupportedLanguage1__c = 'en_US';
                pCntry.PLDatapool__c = 'en_US2';
                pCntry.MemberCountry1__c = cntry1.Id;
                pCntry.MemberCountry2__c = cntry2.Id;
                pCntry.MemberCountry3__c = cntry3.Id;
                pCntry.MemberCountry4__c = cntry4.Id;
                pCntry.MemberCountry5__c = cntry5.Id;
                
                insert pCntry;
                ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'OM', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true);
                insert cLC;
                ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'OM3', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true, ParentClassificationLevel__c = cLC.id);
                insert cLChild;
                
                List<RecordType> recordTypeLst = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Notification' LIMIT 1]);
                String recordTypeSegment = Schema.Sobjecttype.FieloEE__RedemptionRule__c.getRecordTypeInfosByName().get('Manual').getRecordTypeId();
                
                System.debug('>>>>recordTypeSegment:'+recordTypeSegment);
                FieloEE__Program__c prgm = new FieloEE__Program__c();
                prgm.Name = 'PRM Is On';
                prgm.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
                prgm.FieloEE__SiteURL__c = System.Label.CLAPR15PRM308;
                prgm.FieloEE__RecentRewardsDays__c = 10.0;
                insert prgm;
                
                FieloEE__Member__c member = new FieloEE__Member__c();
                member.F_Account__c = acc.Id;
                member.F_AreaOfFocus__c = 'OM3';
                member.F_BusinessType__c = 'OM';
                member.F_Country__c = cntry.Id;
                member.FieloEE__Program__c = prgm.Id;
                member.FieloEE__LastName__c = 'Test';
                member.FieloEE__User__c = user.Id;
                member.F_PRM_PrimaryChannel__c = 'OM3';
                insert member;
                
                Contact cont = Utils_TestMethods.createContact(acc.Id,'TestContact');
                cont.Country__c = acc.country__c;
                cont.PRMCountry__c = acc.country__c;
                cont.PRMCountryClusterSetup__c = pCntry.Id;
                cont.PRMCustomerClassificationLevel1__c = 'OM';
                cont.PRMCustomerClassificationLevel2__c = 'OM3';
                cont.PRMContact__c = true;
                cont.PRMUIMSID__c = '188b80b1-c622-4f20-8b1d-3ae86af11c4g';
                cont.FieloEE__Member__c = member.Id;
                insert cont;
                
                System.debug('>>>>member:'+cont.FieloEE__Member__c);
                System.debug('>>>>member user:'+member.FieloEE__User__c);
                
                CountryChannels__c cntryChannel = new CountryChannels__c();
                cntryChannel.Active__c = true;
                cntryChannel.Channel__c = cLC.Id;
                cntryChannel.SubChannel__c = cLChild.Id;
                cntryChannel.Country__c = cntry.Id;
                cntryChannel.PRMCountry__c = pCntry.Id;
                insert cntryChannel;
                Test.StartTest();
                System.debug('>>>>cntryChannel:'+cntryChannel);
                RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
                FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c(           
                Name = 'Test Segment',
                FieloEE__isActive__c = true,
                F_PRM_Type__c = 'Hybrid',
                RecordTypeId = rt.Id,
                F_PRM_Country__c = pCntry.Id,
                F_PRM_SegmentFilter__c ='Hybrid' + Datetime.NOW()

            );
            insert segment; 
                
                FieloEE__MemberSegment__c membersegment = new FieloEE__MemberSegment__c();
                membersegment.FieloEE__Member2__c = member.Id;
                membersegment.FieloEE__Segment2__c = segment.Id;
                insert membersegment;
                
                SetupAlerts__c setupAlert = new SetupAlerts__c();
                setupAlert.Name = 'Test Alert';
                setupAlert.PRMCountryClusterSetup__c = pCntry.Id;
                setupAlert.Status__c = 'Published';
                setupAlert.Processed__c = false;
                setupAlert.RecordTypeId = recordTypeLst[0].Id;
                insert setupAlert;
                
                ChannelSystemAlert__c channelAlert = new ChannelSystemAlert__c();
                channelAlert.CountryChannels__c = cntryChannel.Id;
                channelAlert.SystemAlert__c = setupAlert.Id;
                insert channelAlert;
                
                SegmentSystemAlert__c segmentAlert = new SegmentSystemAlert__c();
                segmentAlert.FieloPRM_Segment__c = segment.Id;
                segmentAlert.SystemAlert__c = setupAlert.Id;
                insert segmentAlert;
                
                CS_PRM_ApexJobSettings__c mc1 = new CS_PRM_ApexJobSettings__c();
                mc1.Name = 'BatchProcessNotifications';
                mc1.LastRun__c = System.Now();
                insert mc1;
                
                
                Batch_ManageNotifications batchInstance = new Batch_ManageNotifications();
                ID batchprocessid = Database.executeBatch(batchInstance);
                batchInstance.query = 'SELECT Id, Name, Content__c, PRMCountryClusterSetup__r.Country__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry3__c,PRMCountryClusterSetup__r.MemberCountry4__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry5__c, Status__c, ToDate__c,SegmentAlertsCount__c,ChannelAlertCount__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c,'+
                                    '(SELECT Id, Name, CountryChannels__c, CountryChannels__r.Active__c, CountryChannels__r.ChannelCode__c,'+ 
                                            'CountryChannels__r.PRMCountry__c,CountryChannels__r.PRMCountry__r.CountryCode__c, CountryChannels__r.SubChannelCode__c,'+
                                            'CountryChannels__r.Country__r.CountryCode__c, SystemAlert__c FROM Channel_System_Alerts__r),'+
                                    '(SELECT Id, Name, FieloPRM_Segment__c, FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c, SystemAlert__c FROM Segment_System_Alerts__r)'+
                        ' FROM SetupAlerts__c WHERE id=\''+setupAlert.id+'\'';
                        
                        batchProcessId = Database.executeBatch(batchInstance); 
                
                Test.StopTest();
            }
        }
    }
    public static testmethod void testMethod3()
    {
         List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0)
        {
            User user = new User(alias = 'test', email='test'+ '@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profiles[0].Id, BypassWF__c = True,  
            timezonesidkey='Europe/London', username='test'+ '@bridge-fo.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='USA',bypassVR__c = true,IsActive = true);
            system.runas(user)
            {
                FieloEE.MockUpFactory.setCustomProperties(false);
                Country__c cntry = Utils_TestMethods.createCountry();
                cntry.countrycode__c ='US';
                insert cntry;
                
                Country__c cntry1 = Utils_TestMethods.createCountry();
                cntry1.countrycode__c ='IN';
                insert cntry1;
                
                Country__c cntry2 = Utils_TestMethods.createCountry();
                cntry2.countrycode__c ='RU';
                insert cntry2;
                
                Country__c cntry3 = Utils_TestMethods.createCountry();
                cntry3.countrycode__c ='CA';
                insert cntry3;
                
                Country__c cntry4 = Utils_TestMethods.createCountry();
                cntry4.countrycode__c ='FR';
                insert cntry4;
                
                Country__c cntry5 = Utils_TestMethods.createCountry();
                cntry5.countrycode__c ='GB';
                insert cntry5;
                
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = cntry.id;
                insert acc;
                
                acc.isPartner = true;
                acc.PRMAccount__c = true;
                update acc;
                        
                PRMCountry__c pCntry = new PRMCountry__c();
                pCntry.Name = 'USA';
                pCntry.CountryPortalEnabled__c = true;
                pCntry.Country__c = cntry.Id;
                pCntry.SupportedLanguage1__c = 'en_US';
                pCntry.PLDatapool__c = 'en_US2';
                pCntry.MemberCountry1__c = cntry1.Id;
                pCntry.MemberCountry2__c = cntry2.Id;
                pCntry.MemberCountry3__c = cntry3.Id;
                pCntry.MemberCountry4__c = cntry4.Id;
                pCntry.MemberCountry5__c = cntry5.Id;
                insert pCntry;
                ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'OM', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true);
                insert cLC;
                ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'OM3', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true, ParentClassificationLevel__c = cLC.id);
                insert cLChild;
                
                List<RecordType> recordTypeLst = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Notification' LIMIT 1]);
                
                FieloEE__Program__c prgm = new FieloEE__Program__c();
                prgm.Name = 'PRM Is On';
                prgm.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
                prgm.FieloEE__SiteURL__c = System.Label.CLAPR15PRM308;
                prgm.FieloEE__RecentRewardsDays__c = 10.0;
                prgm.FieloEE__MenuFieldSet__c = 'x:F_PRM_MultiLanguageCustomLabel__c,l:Name';
                insert prgm;
                
                FieloEE__Member__c member = new FieloEE__Member__c();
                member.F_Account__c = acc.Id;
                member.F_AreaOfFocus__c = 'OM3';
                member.F_BusinessType__c = 'OM';
                member.F_Country__c = cntry.Id;
                member.FieloEE__Program__c = prgm.Id;
                member.FieloEE__LastName__c = 'Test';
                member.FieloEE__User__c = user.Id;
                member.F_PRM_PrimaryChannel__c = 'OM3';
                insert member;
                
                Contact cont = Utils_TestMethods.createContact(acc.Id,'TestContact');
                cont.Country__c = acc.country__c;
                cont.PRMCountry__c = acc.country__c;
                cont.PRMCountryClusterSetup__c = pCntry.Id;
                cont.PRMCustomerClassificationLevel1__c = 'OM';
                cont.PRMCustomerClassificationLevel2__c = 'OM3';
                cont.PRMContact__c = true;
                cont.PRMUIMSID__c = '188b80b1-c622-4f20-8b1d-3ae86af11c4g';
                cont.FieloEE__Member__c = member.Id;
                insert cont;
                
                System.debug('>>>>member:'+cont.FieloEE__Member__c);
                System.debug('>>>>member user:'+member.FieloEE__User__c);
                
                CountryChannels__c cntryChannel = new CountryChannels__c();
                cntryChannel.Active__c = true;
                cntryChannel.Channel__c = cLC.Id;
                cntryChannel.SubChannel__c = cLChild.Id;
                cntryChannel.Country__c = cntry.Id;
                cntryChannel.PRMCountry__c = pCntry.Id;
                insert cntryChannel;
                
                System.debug('>>>>cntryChannel:'+cntryChannel);
                
                SetupAlerts__c setupAlert = new SetupAlerts__c();
                setupAlert.Name = 'Test Alert';
                setupAlert.PRMCountryClusterSetup__c = pCntry.Id;
                setupAlert.Status__c = 'Published';
                setupAlert.Processed__c = false;
                setupAlert.RecordTypeId = recordTypeLst[0].Id;
                insert setupAlert;
                
                ChannelSystemAlert__c channelAlert = new ChannelSystemAlert__c();
                channelAlert.CountryChannels__c = cntryChannel.Id;
                channelAlert.SystemAlert__c = setupAlert.Id;
                insert channelAlert;
               
                CS_PRM_ApexJobSettings__c mc1 = new CS_PRM_ApexJobSettings__c();
                mc1.Name = 'BatchProcessNotifications';
                mc1.LastRun__c = System.Now();
                insert mc1;
                
                Test.StartTest();
                Batch_ManageNotifications batchInstance3 = new Batch_ManageNotifications();
                ID batchprocessid3 = Database.executeBatch(batchInstance3);
                batchInstance3.query = 'SELECT Id, Name, Content__c, PRMCountryClusterSetup__r.Country__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry3__c,PRMCountryClusterSetup__r.MemberCountry4__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry5__c, Status__c, ToDate__c,SegmentAlertsCount__c,ChannelAlertCount__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c,'+
                                    '(SELECT Id, Name, CountryChannels__c, CountryChannels__r.Active__c, CountryChannels__r.ChannelCode__c,'+ 
                                            'CountryChannels__r.PRMCountry__c,CountryChannels__r.PRMCountry__r.CountryCode__c, CountryChannels__r.SubChannelCode__c,'+
                                            'CountryChannels__r.Country__r.CountryCode__c, SystemAlert__c FROM Channel_System_Alerts__r),'+
                                    '(SELECT Id, Name, FieloPRM_Segment__c, FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c, SystemAlert__c FROM Segment_System_Alerts__r)'+
                        ' FROM SetupAlerts__c WHERE id=\''+setupAlert.id+'\'';
                        
                        batchProcessId3 = Database.executeBatch(batchInstance3); 
                
                Test.StopTest();
            }
        }
    }
    public static testmethod void testMethod4()
    {
         List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0)
        {
            User user = new User(alias = 'test', email='test'+ '@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profiles[0].Id,  
            timezonesidkey='Europe/London', username='test'+ '@bridge-fo.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='USA',bypassVR__c = true,IsActive = true);
            system.runas(user)
            {
                FieloEE.MockUpFactory.setCustomProperties(false);
                Country__c cntry = Utils_TestMethods.createCountry();
                cntry.countrycode__c ='US';
                insert cntry;
                
                Country__c cntry1 = Utils_TestMethods.createCountry();
                cntry1.countrycode__c ='IN';
                insert cntry1;
                
                Country__c cntry2 = Utils_TestMethods.createCountry();
                cntry2.countrycode__c ='RU';
                insert cntry2;
                
                Country__c cntry3 = Utils_TestMethods.createCountry();
                cntry3.countrycode__c ='CA';
                insert cntry3;
                
                Country__c cntry4 = Utils_TestMethods.createCountry();
                cntry4.countrycode__c ='FR';
                insert cntry4;
                
                Country__c cntry5 = Utils_TestMethods.createCountry();
                cntry5.countrycode__c ='GB';
                insert cntry5;
                
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = cntry.id;
                insert acc;
                
                acc.isPartner = true;
                acc.PRMAccount__c = true;
                update acc;
                        
                PRMCountry__c pCntry = new PRMCountry__c();
                pCntry.Name = 'USA';
                pCntry.CountryPortalEnabled__c = true;
                pCntry.Country__c = cntry.Id;
                pCntry.SupportedLanguage1__c = 'en_US';
                pCntry.PLDatapool__c = 'en_US2';
                pCntry.MemberCountry1__c = cntry1.Id;
                pCntry.MemberCountry2__c = cntry2.Id;
                pCntry.MemberCountry3__c = cntry3.Id;
                pCntry.MemberCountry4__c = cntry4.Id;
                pCntry.MemberCountry5__c = cntry5.Id;
                insert pCntry;
                ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'OM', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true);
                insert cLC;
                ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'OM3', ClassificationLevelName__c = 'Original Equipment Manufacturers', Active__c = true, ParentClassificationLevel__c = cLC.id);
                insert cLChild;
                
                List<RecordType> recordTypeLst = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Notification' LIMIT 1]);
                String recordTypeSegment = Schema.Sobjecttype.FieloEE__RedemptionRule__c.getRecordTypeInfosByName().get('Manual').getRecordTypeId();
                
                System.debug('>>>>recordTypeSegment:'+recordTypeSegment);
                FieloEE__Program__c prgm = new FieloEE__Program__c();
                prgm.Name = 'PRM Is On';
                prgm.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
                prgm.FieloEE__SiteURL__c = System.Label.CLAPR15PRM308;
                prgm.FieloEE__RecentRewardsDays__c = 10.0;
                insert prgm;
                
                FieloEE__Member__c member = new FieloEE__Member__c();
                member.F_Account__c = acc.Id;
                member.F_AreaOfFocus__c = 'OM3';
                member.F_BusinessType__c = 'OM';
                member.F_Country__c = cntry.Id;
                member.FieloEE__Program__c = prgm.Id;
                member.FieloEE__LastName__c = 'Test';
                member.FieloEE__User__c = user.Id;
                member.F_PRM_PrimaryChannel__c = 'OM3';
                insert member;
                
                Contact cont = Utils_TestMethods.createContact(acc.Id,'TestContact');
                cont.Country__c = acc.country__c;
                cont.PRMCountry__c = acc.country__c;
                cont.PRMCountryClusterSetup__c = pCntry.Id;
                cont.PRMCustomerClassificationLevel1__c = 'OM';
                cont.PRMCustomerClassificationLevel2__c = 'OM3';
                cont.PRMContact__c = true;
                cont.PRMUIMSID__c = '188b80b1-c622-4f20-8b1d-3ae86af11c4g';
                cont.FieloEE__Member__c = member.Id;
                insert cont;
                
                System.debug('>>>>member:'+cont.FieloEE__Member__c);
                System.debug('>>>>member user:'+member.FieloEE__User__c);
                
                CountryChannels__c cntryChannel = new CountryChannels__c();
                cntryChannel.Active__c = true;
                cntryChannel.Channel__c = cLC.Id;
                cntryChannel.SubChannel__c = cLChild.Id;
                cntryChannel.Country__c = cntry.Id;
                cntryChannel.PRMCountry__c = pCntry.Id;
                insert cntryChannel;
                
                System.debug('>>>>cntryChannel:'+cntryChannel);
                Test.StartTest();
                RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
                FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c(           
                Name = 'Test Segment',
                FieloEE__isActive__c = true,
                F_PRM_Type__c = 'Hybrid',
                RecordTypeId = rt.Id,
                F_PRM_Country__c = pCntry.Id,
                F_PRM_SegmentFilter__c ='Hybrid' + Datetime.NOW()

            );
            insert segment; 
                
                FieloEE__MemberSegment__c membersegment = new FieloEE__MemberSegment__c();
                membersegment.FieloEE__Member2__c = member.Id;
                membersegment.FieloEE__Segment2__c = segment.Id;
                insert membersegment;
                
                SetupAlerts__c setupAlert = new SetupAlerts__c();
                setupAlert.Name = 'Test Alert';
                setupAlert.PRMCountryClusterSetup__c = pCntry.Id;
                setupAlert.Status__c = 'Published';
                setupAlert.Processed__c = false;
                setupAlert.RecordTypeId = recordTypeLst[0].Id;
                insert setupAlert;
                
                SegmentSystemAlert__c segmentAlert = new SegmentSystemAlert__c();
                segmentAlert.FieloPRM_Segment__c = segment.Id;
                segmentAlert.SystemAlert__c = setupAlert.Id;
                insert segmentAlert;
                
                CS_PRM_ApexJobSettings__c mc1 = new CS_PRM_ApexJobSettings__c();
                mc1.Name = 'BatchProcessNotifications';
                mc1.LastRun__c = System.Now();
                insert mc1;
                
                
                Batch_ManageNotifications batchInstance4 = new Batch_ManageNotifications();
                ID batchprocessid4 = Database.executeBatch(batchInstance4);
                batchInstance4.query = 'SELECT Id, Name, Content__c, PRMCountryClusterSetup__r.Country__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry3__c,PRMCountryClusterSetup__r.MemberCountry4__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry5__c, Status__c, ToDate__c,SegmentAlertsCount__c,ChannelAlertCount__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,'+
                                    'PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c,'+
                                    '(SELECT Id, Name, CountryChannels__c, CountryChannels__r.Active__c, CountryChannels__r.ChannelCode__c,'+ 
                                            'CountryChannels__r.PRMCountry__c,CountryChannels__r.PRMCountry__r.CountryCode__c, CountryChannels__r.SubChannelCode__c,'+
                                            'CountryChannels__r.Country__r.CountryCode__c, SystemAlert__c FROM Channel_System_Alerts__r),'+
                                    '(SELECT Id, Name, FieloPRM_Segment__c, FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c, SystemAlert__c FROM Segment_System_Alerts__r)'+
                        ' FROM SetupAlerts__c WHERE id=\''+setupAlert.id+'\'';
                        
                        batchProcessId4 = Database.executeBatch(batchInstance4); 
                
                Test.StopTest();
            }
        }
    }
}