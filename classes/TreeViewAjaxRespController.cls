public class TreeViewAjaxRespController {

    public List<JsonObject> json {get;set;}
	List<String> parts = null;
	Set<String> uniqueCustomObjectSet = new Set<String>();
	private String hierarchy;
	
     
    /*========== CONSTRUCTOR ============*/ 
	public TreeViewAjaxRespController (){ 
	}

    /** invoked on an Ajax request */   
    public void doSearch() {
    	//Represents the parameters sent by to this controller bt the jquery tree
    	Map<String, String> params = ApexPages.currentPage().getParameters();
    	//Represents the id hierarchy of a tree node
    	List<String> parts = new List<String>();
    	
    	json = new List<JsonObject>();
    	/*for(String key:params.keySet()){ 
    		System.debug(key);
    		System.debug(params.get(key));
    		
    	}*/ 
    	parts = (params.get('root')).split('-');
    	hierarchy = params.get('root');

    	//for (integer a=0; a<parts.size();a++) {
    	//	System.debug('parts[' + a + '] : ' + parts[a]);
    	//}
    		
    	if ((params.get('object.type') != null)&&(params.get('core.apexpages.devmode.url')=='1')) {
    		json = new List<JsonObject>();
    		uniqueCustomObjectSet = new Set<String>();
    		if (parts.size()==1) {
    			for (REF_RoutingBOBS__c niveau2 : [select id,L2__c,L2__r.name from REF_RoutingBOBS__c where L1__c = :parts[0]]) {
					if ((niveau2.L2__r.name != null) && (!uniqueCustomObjectSet.contains(niveau2.L2__r.name))) {
					uniqueCustomObjectSet.add(niveau2.L2__r.name);
					JsonObject json1 = createJsonObject(niveau2.L2__r.name, parts[0]+'-'+niveau2.L2__c, true);
    				json.add(json1);
					}
    			}
    		} else if (parts.size()==2) {
				for (REF_RoutingBOBS__c niveau3 : [select id,L3__r.name from REF_RoutingBOBS__c where L1__c = :parts[0] and L2__c = :parts[1] ]) {	
					if ((niveau3.L3__r.name != null) && (!uniqueCustomObjectSet.contains(niveau3.L3__r.name))) {
						uniqueCustomObjectSet.add(niveau3.L3__r.name);
						JsonObject json1 = createJsonObject(niveau3.L3__r.name, parts[0]+'-'+ parts[1]+'-'+niveau3.L3__c, true);
    					json.add(json1);
					}
				}
			} else if (parts.size()==3) {
				for (REF_RoutingBOBS__c niveau4 : [select id,L4__r.name from REF_RoutingBOBS__c where L1__c = :parts[0] and L2__c = :parts[1] and L3__c = :parts[2]]) {	
					if ((niveau4.L4__r.name!=null)&&(!uniqueCustomObjectSet.contains(niveau4.L4__r.name))) {
						uniqueCustomObjectSet.add(niveau4.L4__r.name);
			
						JsonObject json1 = createJsonObject(niveau4.L4__r.name, parts[0]+'-'+niveau4.L4__c, true);
    					json.add(json1);
					}
				}
			}// end of loop retrieving the right level of Organization record
			
			
			/*============================================================================
			SCA: commented out: we do not need the teams anymore to be displayed in the tree
			================================================================================*/
			/*if (json.isEmpty()) {// corresponds to the case where we hit the Team
				String soql = 'select id,TeamReference__c,TeamReference__r.name from REF_RoutingBOBS__c where ';
				//L1__c = :parts[0] and L2__c = :parts[1]'
				String whereStatement = '';
				for (integer i=0;i<parts.size();i++) {
					whereStatement = whereStatement + 'L' + (i+1) + '__c=\'' + parts[i] + '\'';
					if (i != (parts.size() - 1)) {
						whereStatement = whereStatement + ' AND ';
					}
				}
				REF_RoutingBOBS__c routing;
				try {
					routing = Database.query(soql + whereStatement + ' limit 1');
					} catch (Exception e) {
						System.Debug(e.getMessage());
					}
					if (routing != null) {
						JsonObject json1 = createJsonObject(routing.TeamReference__r.name, hierarchy + '-' + routing.TeamReference__c, false, 'team');
    					json.add(json1);
					} else {
						System.Debug('No record');
					}
			}*/
    	}
    }// end of doSearch method
    	
    private JsonObject createJsonObject(String text, String id, boolean children) {
    	return createJsonObject(text, id, children, null);
    }
    	
    private JsonObject createJsonObject(String text, String id, boolean children, String classes) {
    	JsonObject jsonObj = new JsonObject();
    	jsonObj.putOpt('text', new JsonObject.value(text));
    	jsonObj.putOpt('id', new JsonObject.value(id));
    	jsonObj.putOpt('hasChildren', new JsonObject.value(children));
    	if (classes != null) {
    		jsonObj.putOpt('classes', new JsonObject.value(classes));
    	}
    	return jsonObj;
    }
    	
    public String getResult() {
        String output='';
        for (integer i=0;i<json.size();i++) {
        	if (i>0) {
        		output= output+',';
        	}
        	output= output+json[i].valueToString();
        }
        return output;
    }
}