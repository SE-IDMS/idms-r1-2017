global class BatchCreateTimesheetsSchedule implements Schedulable{
/*
    Schedulable class to create Timesheets based on a date x days into the future or past.  

*/
    global void execute(SchedulableContext SC) {

        BatchCreateTimesheets batch = new BatchCreateTimesheets();

        Integer daysFromToday = Integer.valueOf(System.label.SVMX_TimeSheet_Days_From_Today);
            
        String query = 'SELECT Id, Manager__r.ProfileId, ';
        
        query += 'SVMXC__Salesforce_User__r.ProfileId ';
        query += 'FROM SVMXC__Service_Group_Members__c ';
        query += 'WHERE SVMXC__Active__c = true ';
        query += 'AND SVMXC__Salesforce_User__r.IsActive = true ';
        
        if (System.label.SVMX_TimeSheet_Record_Type_Ids != 'None Specified')
            query += 'AND RecordTypeId =\'' + System.label.SVMX_TimeSheet_Record_Type_Ids + '\' ';
    
        batch.query = query;
        batch.daysFromToday = daysFromToday;

        System.debug('#### query : ' + query);
        Database.executeBatch(batch);

    }
}