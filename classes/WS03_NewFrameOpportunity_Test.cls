/* 
    Test Class For New Frame Opportunity.
    Test Class Name: WS03_NewFrameOpportunity_Test
    Author : Amitava Dutta
    Date: 25/01/2011.
    
    Description: Basically this class works as generator of one Redirection URL being the controller for the 
    VFP10_NewFrameOpportunity page to redirect the user with requisite parameters 
    to generate Standard New Opportunity Page and populate some prefilled values.
    
    Parameter : Agreement ID [To get the Id From respective Agreement Page]
    Controller Reference: Opportunity Standard Controller
*/

@isTest
private class WS03_NewFrameOpportunity_Test
{    
  
    
    static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
    static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
    static User user,newUser,newUser2 ;
    static Country__c newCountry;
    static OppAgreement__c objAgree;
    static Account acc;
    static Opportunity objOpp,opp2,opp3,opp4;
    static OppAgreement__c objAgree2,objAgree3;
    static List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();   
    static List<OppAgreement__c> oa = new List<OppAgreement__c>();
    static OPP_Product__c  prod = new OPP_Product__c();
    static
    {
        VFC61_OpptyClone.isOpptyClone = true;
        acc = Utils_TestMethods.createAccount();
         insert acc;
        
        opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName = '3 - Identify & Qualify';
        opp2.OpptyType__c = Label.CL00240;
        insert opp2; 
        objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree2.AgreementValidFrom__c = Date.today();         
        objAgree2.FrameOpportunityInterval__c = 'Monthly';
        objAgree2.SignatureDate__c = Date.today();
        objAgree2.Opportunity_Type__c = Label.CL00240;             
        objAgree2.Agreement_Duration_in_month__c = '3';
        objAgree2.BusinessMix__c  ='BD';        
        insert objAgree2;
        
        prod.ProductLine__c = null;
        prod.BusinessUnit__c = 'EcoBuilding';
        insert prod;
        opp2.AgreementReference__c = objAgree2.Id;
        update opp2;        
        
        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        
        Insert prodLines; 
        objAgree3 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree3.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree3.AgreementValidFrom__c = Date.today();    
        objAgree3.Opportunity_Type__c = Label.CL00241;     
        objAgree3.FrameOpportunityInterval__c = Label.CL00222;
        objAgree3.SignatureDate__c = Date.today();
        objAgree3.Agreement_Duration_in_month__c = '2';
        objAgree3.BusinessMix__c  ='BD';
        insert objAgree3; 
        
        opp4 = Utils_TestMethods.createOpportunity(acc.Id);   
        opp4.OpptyType__c = label.CL00241;
        opp4.StageName = '3 - Identify & Qualify';
        opp4.AgreementReference__c = objAgree3.Id;        
        insert opp4;       
        
    }     

    static testmethod void testMassUpdate5()
    {   
        WS03_NewFrameOpportunity.Redirect(objAgree2.Id); 
        WS03_NewFrameOpportunity.CountOpportunityForAgreement(objAgree2.Id); 
        WS03_NewFrameOpportunity.GetAgreementInterval(objAgree2.Id);     
        
    }
    
    static testmethod void testMassUpdate6()
    {   
        WS03_NewFrameOpportunity.Redirect(objAgree3.Id); 
        WS03_NewFrameOpportunity.CountOpportunityForAgreement(objAgree3.Id); 
        WS03_NewFrameOpportunity.GetAgreementInterval(objAgree3.Id);     
        
    }
}