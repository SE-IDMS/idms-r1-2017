/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

@isTest
public class FieloPRM_AP_MemberPropertiesTriggersTest {

    public static testMethod void testUnit1(){      
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            List<Account> accounts = new List<Account>();
            
            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            accounts.add(acc);
            
            Account acc2 = new Account();
            acc2.Name = 'acc2';
            insert acc2;
            accounts.add(acc2);
            
            ExternalPropertiesCatalog__c cat = new ExternalPropertiesCatalog__c(
                FieldName__c = 'Name'
            );
            insert cat;
            
            CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberPropProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;

            MemberExternalProperties__c memberExternalProperties = new MemberExternalProperties__c(
                Type__c = 'ApplicationAccess',
                SubType__c = 'QuoteFast',
                PRMUIMSId__c = 'TEST1',
                FunctionalKey__c = 'ApplicationAccessQuoteFastTEST1',
                Member__c = null,
                ExternalPropertiesCatalog__c = cat.Id
            );
            insert memberExternalProperties;
            
            id recordtypeManual = [SELECT Id  FROM RecordType WHERE DeveloperName = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'  ].id;

            FieloEE__RedemptionRule__c redemptionRule = new FieloEE__RedemptionRule__c(Name = 'testRedemptionRule', FieloEE__isActive__c = true, F_PRM_Type__c = 'Hybrid',
                RecordTypeid = recordtypeManual, F_PRM_SegmentFilter__c ='Hybrid' + Datetime.NOW());
            insert redemptionRule;
            FieloEE__RedemptionRuleCriteria__c criterias = new FieloEE__RedemptionRuleCriteria__c(FieloEE__RedemptionRule__c = redemptionRule.Id, 
                FieloEE__Operator__c = 'contains', FieloEE__Values__c = 'test', FieloEE__FieldType__c = 'Text', FieloEE__FieldName__c = 'FieloEE__Street__c');
            insert criterias;

            FieloEE__Member__c member0 = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo';
            member.FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
                       
            insert member;
            members.add(member);

            member.FieloEE__Street__c = 'test2';
            member.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member;
            test.startTest();
            
            Contact con = [SELECT PRMUIMSID__c FROM Contact WHERE FieloEE__Member__c =: member0.Id];
            
            con.PRMUIMSID__c = 'ApplicationAccessQuoteFastTEST1';
            
            update con;

            RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
            
            FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c(           
                Name = 'Test Segment',
                FieloEE__isActive__c = true,
                F_PRM_Type__c = 'Hybrid',
                RecordTypeId = rt.Id,
                F_PRM_SegmentFilter__c ='Hybrid' + Datetime.NOW()

            );
            insert segment;            
            
            FieloEE__MemberSegment__c memberSegment = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = member.Id,
                FieloEE__Segment2__c = segment.Id           
            );
            insert memberSegment;
            
            member.F_PRM_IsActivePRMContact__c = true;
            member.F_PRM_ProgramLevels__c = '2';
            //update member;        

            member0.F_PRM_IsActivePRMContact__c = true;
            member0.F_PRM_ProgramLevels__c = '2';
            //update member0; 
            
            update new List<FieloEE__Member__c>{member,member0};
            
            FieloEE__Member__c member2 = new FieloEE__Member__c();
            member2.FieloEE__LastName__c= 'Polo2'+ String.ValueOf(DateTime.now().getTime());
            member2.FieloEE__FirstName__c = 'Marco2';
            member2.FieloEE__Street__c = 'test2';
            member2.F_Account__c = acc2.Id;
            
            insert member2;
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member2;
            members.add(member2);
            
            test.stopTest();
        }
    }

    public static testMethod void testUnit2(){      
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            List<Account> accounts = new List<Account>();
            
            CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberPropProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;

            CS_PRM_ApexJobSettings__c conf2 = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberFeatProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf2;
            
            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            accounts.add(acc);
            
            Account acc2 = new Account();
            acc2.Name = 'acc2';
            insert acc2;
            accounts.add(acc2);

            Country__c testCountry = new Country__c();
            testCountry.Name   = 'Global';
            testCountry.CountryCode__c = 'WW';
            
            INSERT testCountry;
            
            PRMCountry__c testPRmcountry = new PRMCountry__c();
              testPRmcountry.Name   = 'Global';
              testPRmcountry.Country__c = testCountry.id;
              testPRmcountry.PLDatapool__c = '123123test';
            INSERT testPRmcountry;

            FieloEE__Member__c member0 = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo';
            member.FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
                       
            insert member;

            FieloPRM_Feature__c feature = new FieloPRM_Feature__c(
                Name = 'feature test2',
                F_PRM_FeatureCode__c = 'PRM_Rewards'  
            );
            insert feature;

            RecordType rtMemberFeature = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloPRM_MemberFeature__c' AND DeveloperName = 'Manual'];

            FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c(
                F_PRM_Feature__c =  feature.id,
                F_PRM_Member__c =  member.id,
                PRMUIMSId__c = '123TEST',
                RecordTypeId = rtMemberFeature.Id      
            );
            insert memberFeature;

            FieloPRM_MemberFeature__c memFeature = [SELECT Id, F_PRM_IsActive__c, F_PRM_Feature__r.F_PRM_FeatureCode__c FROM FieloPRM_MemberFeature__c WHERE Id =: memberFeature.Id];
                
            system.debug('###memFeature.F_PRM_IsActive__c: ' + memFeature.F_PRM_IsActive__c + ' - ' + '###memFeature.F_PRM_Feature__r.F_PRM_FeatureCode__c: ' + memFeature.F_PRM_Feature__r.F_PRM_FeatureCode__c);
                
            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'test ProgramLevel',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'asd'
            );
            insert badge;

            RecordType rtEventCatalogAutomatic = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloPRM_EventCatalog__c' AND DeveloperName = 'Automatic'];

            FieloPRM_EventCatalog__c eventcat = new FieloPRM_EventCatalog__c();
            eventcat.Name = 'event test';
            eventcat.F_PRM_isActive__c = true;
            eventcat.F_PRM_Country__c = testPRmcountry.id;
            eventcat.RecordTypeId = rtEventCatalogAutomatic.Id;
            
            insert eventcat;

            PRMTransactionCatalog__c transCatalog = new PRMTransactionCatalog__c(
                Approved__c = true,
                ExternalValue__c = 100,
                PurchaseValue__c = 10,
                Type__c = 'Purchase'
            );
            insert transCatalog;
            
            ExternalPropertiesCatalog__c cat = new ExternalPropertiesCatalog__c(
                FieldName__c = 'Name',
                Active__c = true,
                Badge__c = badge.Id,
                TransactionCatalog__c = transCatalog.Id
            );
            insert cat;
            
            PRMExternalPropertyEvent__c externalPropertyEvent = new PRMExternalPropertyEvent__c(
                EventCatalog__c = eventcat.Id,
                ExternalPropertiesCatalog__c = cat.Id
            );
            insert externalPropertyEvent;

            MemberExternalProperties__c memberExternalProperties = new MemberExternalProperties__c(
                Type__c = 'ApplicationAccess',
                SubType__c = 'QuoteFast',
                PRMUIMSId__c = 'TEST1',
                FunctionalKey__c = 'ApplicationAccessQuoteFastTEST1',
                Member__c = member.Id,
                ExternalPropertiesCatalog__c = cat.Id
            );
            insert memberExternalProperties;
            
            id recordtypeManual = [SELECT Id  FROM RecordType WHERE DeveloperName = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'  ].id;

            FieloEE__RedemptionRule__c redemptionRule = new FieloEE__RedemptionRule__c(Name = 'testRedemptionRule', FieloEE__isActive__c = true, F_PRM_Type__c = 'Hybrid',
                RecordTypeid = recordtypeManual, F_PRM_SegmentFilter__c ='Hybrid' + Datetime.NOW());
            insert redemptionRule;
            FieloEE__RedemptionRuleCriteria__c criterias = new FieloEE__RedemptionRuleCriteria__c(FieloEE__RedemptionRule__c = redemptionRule.Id, 
                FieloEE__Operator__c = 'contains', FieloEE__Values__c = 'test', FieloEE__FieldType__c = 'Text', FieloEE__FieldName__c = 'FieloEE__Street__c');
            insert criterias;

            members.add(member);

            member.FieloEE__Street__c = 'test2';
            member.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member;
            test.startTest();
            
            Contact con = [SELECT PRMUIMSID__c FROM Contact WHERE FieloEE__Member__c =: member0.Id];
            
            con.PRMUIMSID__c = 'ApplicationAccessQuoteFastTEST1';
            
            update con;

            RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
            
            FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c(           
                Name = 'Test Segment',
                FieloEE__isActive__c = true,
                F_PRM_Type__c = 'Hybrid',
                RecordTypeId = rt.Id,
                F_PRM_SegmentFilter__c ='Hybrid' + Datetime.NOW()

            );
            insert segment;            
            
            FieloEE__MemberSegment__c memberSegment = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = member.Id,
                FieloEE__Segment2__c = segment.Id           
            );
            insert memberSegment;
            
            member.F_PRM_IsActivePRMContact__c = true;
            member.F_PRM_ProgramLevels__c = '2';
            //update member;        

            member0.F_PRM_IsActivePRMContact__c = true;
            member0.F_PRM_ProgramLevels__c = '2';
            //update member0; 
            
            update new List<FieloEE__Member__c>{member,member0};
            
            FieloEE__Member__c member2 = new FieloEE__Member__c();
            member2.FieloEE__LastName__c= 'Polo2'+ String.ValueOf(DateTime.now().getTime());
            member2.FieloEE__FirstName__c = 'Marco2';
            member2.FieloEE__Street__c = 'test2';
            member2.F_Account__c = acc2.Id;
            
            insert member2;
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member2;
            members.add(member2);

            test.stopTest();
        }
    }

    public static testMethod void testUnit3(){      
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            test.startTest();
            FieloEE.MockUpFactory.setCustomProperties(false);

            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            List<Account> accounts = new List<Account>();
            
            Account acc = new Account();
            acc.Name = 'acc';
            //insert acc;
            accounts.add(acc);
            
            Account acc2 = new Account();
            acc2.Name = 'acc2';
            //insert acc2;
            accounts.add(acc2);

            insert accounts;

            FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            member.F_Account__c = acc.Id;
            member.FieloEE__User__c = us.id;
            update member;

            Contact con = [SELECT id, PRMUIMSId__c  FROM Contact WHERE FieloEE__Member__c = : member.id limit 1 ];
            con.PRMUIMSId__c = 'asdqwe';
            update con;

            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'test ProgramLevel',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'asd'
            );
            insert badge;
            
            ExternalPropertiesCatalog__c cat = new ExternalPropertiesCatalog__c(FieldName__c = 'Name', Badge__c = badge.Id);
            insert cat;

            CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberPropProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;

            MemberExternalProperties__c memberExternalProperties = new MemberExternalProperties__c(
                Type__c = 'ApplicationAccess',
                SubType__c = 'QuoteFast',
                PRMUIMSId__c = 'TEST1',
                FunctionalKey__c = 'ApplicationAccessQuoteFastTEST1',
                Member__c = member.id,
                ExternalPropertiesCatalog__c = cat.Id
            );
            
            insert memberExternalProperties;

            memberExternalProperties.F_PRM_TryCount__c = 6;
            update memberExternalProperties;

            memberExternalProperties.F_PRM_TryCount__c = 0;
            memberExternalProperties.DeletionFlag__c = true;
            update memberExternalProperties;

            delete memberExternalProperties;

            MemberExternalProperties__c memberExternalProperties2 = new MemberExternalProperties__c(
                PRMUIMSId__c = 'TEST1',
                FunctionalKey__c = 'ApplicationAccessQuoteFastTEST1',
                Member__c = member.id,
                ExternalPropertiesCatalog__c = cat.Id,
                F_PRM_TryCount__c = 6,
                F_PRM_IsSynchronic__c = true
            );

            insert memberExternalProperties2;

            memberExternalProperties.F_PRM_TryCount__c = 0;
            memberExternalProperties.F_PRM_Status__c = 'TODELETE';
            memberExternalProperties2.DeletionFlag__c = true;
            update memberExternalProperties2;

            try{
                MemberExternalProperties__c memberExternalProperties3 = new MemberExternalProperties__c(
                    PRMUIMSId__c = 'TEST3',
                    FunctionalKey__c = 'ApplicationAccessQuoteFastTEST3',
                    Member__c = member.id,
                    ExternalPropertiesCatalog__c = cat.Id,
                    F_PRM_TryCount__c = 0,
                    DeletionFlag__c = true
                );

                insert memberExternalProperties3;
            }catch(Exception e){

            }
            
            test.stopTest();
        }
    }   
}