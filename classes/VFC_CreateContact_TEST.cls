@isTest
private class VFC_CreateContact_TEST {
 
    static testMethod void testVFC_ContactCreateAsACCCAgent() {
        
        User cccAgent;
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        
        
        List<Profile> profiles = [select id from profile where name='SE - CCC Standard User'];
        if(profiles.size()>0)
        {
            cccAgent = Utils_TestMethods.createStandardUser(profiles[0].Id,'tCCCAg12');
            cccAgent.BypassVR__c=false;
            insert cccAgent;
        }
        
        
        System.RunAs(cccAgent)
        {
                     
            Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
            acc.City__c='city';
            insert acc;
            
            Account  account1= Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
            account1.City__c='testcity1';
            account1.Street__c='teststreet1';
            insert account1;
            
            Account  account2= Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
            account2.City__c='testcity2';
            account2.Street__c='teststreet2';
            insert account2;
            
            Contact  con= new Contact(FirstName='Test',LastName='Contact3',AccountId=acc.Id);
            con.OtherCountryCode=country.CountryCode__c;
            con.Country__c=country.id;
            con.StateProv__c=stateProv.id;
            con.Email='test@se.prod';
            insert con;
            
            Case CaseObj= Utils_TestMethods.createCase(acc.Id,Con.Id,'Open');
            insert CaseObj; 
                   
            List<Account> accts = [SELECT Name, AccountLocalName__c, City__c, Country__c FROM Account];
            System.debug('-----List of Accts : ' + accts);
            List<Contact> conts = [SELECT FirstName, LastName, Email, WorkPhone__c, MobilePhone FROM Contact];
            System.debug('-----List of Contacts: ' + conts);
            
            test.starttest();

            PageReference pageRef_CreateContact2 = Page.VFP_CreateContact;
            Test.setCurrentPage(pageRef_CreateContact2);
            ApexPages.currentPage().getParameters().put('recType',System.Label.CLOCT13ACC08);     
            System.currentPageReference().getParameters().put('accRecordId',accts[0].id);
            ApexPages.currentPage().getParameters().put('FirstName','Test');
            ApexPages.currentPage().getParameters().put('LastName','TestContact');
            ApexPages.currentPage().getParameters().put('LocalFirstName','conFirstNameLocal');
            ApexPages.currentPage().getParameters().put('LocalLastName','conLastNameLocal');
            ApexPages.currentPage().getParameters().put('AccountName','TestAccount');
            ApexPages.currentPage().getParameters().put('LocalAccountName','Account Local Name');
            ApexPages.currentPage().getParameters().put('Street','teststreet2');
            ApexPages.currentPage().getParameters().put('StreetLocalLang','StreetLocalLang');
            ApexPages.currentPage().getParameters().put('City','testcity2');
            ApexPages.currentPage().getParameters().put('LocalCity','CityLocal');
            ApexPages.currentPage().getParameters().put('ZipCode','zipcode');
            ApexPages.currentPage().getParameters().put('State/Province',stateProv.Id);
            ApexPages.currentPage().getParameters().put('Country',country.Id);
            ApexPages.currentPage().getParameters().put('POBox','POBox');
            ApexPages.currentPage().getParameters().put('Email','test@se.prod');
            ApexPages.currentPage().getParameters().put('CallerPhoneNumber','1234567890');
                
            VFC_CreateContact  objVFC_CreateContact4 = new VFC_CreateContact();
            objVFC_CreateContact4.contact.MobilePhone='1234567890';
            objVFC_CreateContact4.contact.WorkPhoneExt__c='1234567890';
            objVFC_CreateContact4.contact.Email='test@se.prod';
            objVFC_CreateContact4.ischecked=false;
            
            objVFC_CreateContact4.saveAccount();
            objVFC_CreateContact4.saveDupAccount();
            objVFC_CreateContact4.saveDupContact();
            
            objVFC_CreateContact4.account.City__c='testCity';
            objVFC_CreateContact4.saveAccount();
            
            test.stoptest(); 
                       
            List<Contact> contss = [SELECT FirstName, LastName, Email, WorkPhone__c, MobilePhone, AccountId FROM Contact];
            System.debug('-----List of Contacts: ' + contss);
            
            System.currentPageReference().getParameters().put('Case',CaseObj.id);  
            System.currentPageReference().getParameters().put('accRecordId',account2.id);
            VFC_CreateContact  objVFC_CreateContact2 = new VFC_CreateContact();
            objVFC_CreateContact2.saveDupContact();  
            
            System.currentPageReference().getParameters().put('Case',''); 
            System.currentPageReference().getParameters().put('accRecordId',account1.id);
            VFC_CreateContact  objVFC_CreateContact1 = new VFC_CreateContact();
            objVFC_CreateContact1.saveDupContact();    
                        
        }
        
    }
    
    
    static testMethod void testVFC_ContactCreateAsAdmin1() {

        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        
        Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId());
        acc.Country__c=country.id;
        insert acc;
        
        Contact   con= Utils_TestMethods.createContact(acc.id,'TestContact');
        insert con;
        
        Case CaseObj= Utils_TestMethods.createCase(acc.Id,Con.Id,'Open');
        insert CaseObj;
              
        List<Account> acct = [SELECT FirstName, LastName, City__c, Country__c FROM Account LIMIT 1];
        System.debug('----- Account ID: ' + acct[0]);
        List<Contact> cont = [SELECT FirstName, LastName, City__c, Country__c FROM Contact LIMIT 1];
        System.debug('----- Contact ID: ' + cont[0]);
        
        
        //for Business account
        test.starttest();
               
        PageReference pageRef_CreateContact = Page.VFP_CreateContact;
        Test.setCurrentPage(pageRef_CreateContact);
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLOCT13ACC08);//CLDEC12ACC01
        System.currentPageReference().getParameters().put('Case',CaseObj.id);       
        System.currentPageReference().getParameters().put('conRecordId',cont[0].id); 
        System.currentPageReference().getParameters().put('accRecordId',acct[0].id);
        ApexPages.currentPage().getParameters().put('FirstName','conFirstName');
        ApexPages.currentPage().getParameters().put('LastName','conLastName');
        ApexPages.currentPage().getParameters().put('LocalFirstName','conFirstNameLocal');
        ApexPages.currentPage().getParameters().put('LocalLastName','conLastNameLocal');
        ApexPages.currentPage().getParameters().put('AccountName','TEST VFC_ContactSearch');
        ApexPages.currentPage().getParameters().put('LocalAccountName','Account Local Name');
        ApexPages.currentPage().getParameters().put('Street','Street');
        ApexPages.currentPage().getParameters().put('StreetLocalLang','StreetLocalLang');
        ApexPages.currentPage().getParameters().put('City','city');
        ApexPages.currentPage().getParameters().put('LocalCity','CityLocal');
        
        ApexPages.currentPage().getParameters().put('ZipCode','zipcode');
        ApexPages.currentPage().getParameters().put('State/Province',stateProv.Id);
        ApexPages.currentPage().getParameters().put('Country',country.Id);
        ApexPages.currentPage().getParameters().put('POBox','POBox');
        ApexPages.currentPage().getParameters().put('Email','abc@gmail.com');
        ApexPages.currentPage().getParameters().put('CallerPhoneNumber','123456');
            
        VFC_CreateContact  objVFC_CreateContact = new VFC_CreateContact();
        
        objVFC_CreateContact.account.ToBeDeleted__c = false;
        objVFC_CreateContact.account.CorporateHeadquarters__c = true;
        objVFC_CreateContact.account.LeadingBusiness__c = 'BD';
        objVFC_CreateContact.account.MarketSegment__c = 'BDZ'; 
        objVFC_CreateContact.account.RecordTypeId=System.Label.CLOCT13ACC08;
        objVFC_CreateContact.contact.Country__c = country.Id;  
              
        objVFC_CreateContact.saveDupAccCon();
        
        objVFC_CreateContact.ischecked=False;
        objVFC_CreateContact.saveAccount();
        objVFC_CreateContact.saveDupAccount();
        objVFC_CreateContact.saveDupContact();
        objVFC_CreateContact.redirectTo();
        objVFC_CreateContact.cancelContact();
        
        test.stoptest();
        
    }
    
    static testMethod void testVFC_ContactCreateAsAdmin2() {
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        
        Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId());
        acc.Country__c=country.id;
        insert acc;
        
        Contact   con= Utils_TestMethods.createContact(acc.id,'TestContact');
        insert con;
        
        Case CaseObj= Utils_TestMethods.createCase(acc.Id,Con.Id,'Open');
        insert CaseObj;
              
        List<Account> acct = [SELECT FirstName, LastName, City__c, Country__c FROM Account LIMIT 1];
        System.debug('----- Account ID: ' + acct[0]);
        List<Contact> cont = [SELECT FirstName, LastName, City__c, Country__c FROM Contact LIMIT 1];
        System.debug('----- Contact ID: ' + cont[0]);
        
        test.starttest();

        
        PageReference pageRef_CreateContact = Page.VFP_CreateContact;
        Test.setCurrentPage(pageRef_CreateContact);
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLOCT13ACC08);//CLDEC12ACC01
        System.currentPageReference().getParameters().put('Case','');
        System.currentPageReference().getParameters().put('conRecordId',cont[0].id); 
        System.currentPageReference().getParameters().put('accRecordId',acct[0].id);
        ApexPages.currentPage().getParameters().put('FirstName','conFirstName');
        ApexPages.currentPage().getParameters().put('LastName','conLastName');
        ApexPages.currentPage().getParameters().put('LocalFirstName','conFirstNameLocal');
        ApexPages.currentPage().getParameters().put('LocalLastName','conLastNameLocal');
        ApexPages.currentPage().getParameters().put('AccountName','TEST VFC_ContactSearch');
        ApexPages.currentPage().getParameters().put('LocalAccountName','Account Local Name');
        ApexPages.currentPage().getParameters().put('Street','Street');
        ApexPages.currentPage().getParameters().put('StreetLocalLang','StreetLocalLang');
        ApexPages.currentPage().getParameters().put('City','city');
        ApexPages.currentPage().getParameters().put('LocalCity','CityLocal');
        
        ApexPages.currentPage().getParameters().put('ZipCode','zipcode');
        ApexPages.currentPage().getParameters().put('State/Province',stateProv.Id);
        ApexPages.currentPage().getParameters().put('Country',country.Id);
        ApexPages.currentPage().getParameters().put('POBox','POBox');
        ApexPages.currentPage().getParameters().put('Email','abc@gmail.com');
        ApexPages.currentPage().getParameters().put('CallerPhoneNumber','123456');
        
        VFC_CreateContact  objVFC_CreateContact1 = new VFC_CreateContact();
        
        objVFC_CreateContact1.account.ToBeDeleted__c = false;
        objVFC_CreateContact1.account.CorporateHeadquarters__c = true;
        objVFC_CreateContact1.account.LeadingBusiness__c = 'BD';
        objVFC_CreateContact1.account.MarketSegment__c = 'BDZ'; 
        objVFC_CreateContact1.account.RecordTypeId=System.Label.CLOCT13ACC08;

        //objVFC_CreateContact1.ischecked=false;
        objVFC_CreateContact1.saveAccount();
        objVFC_CreateContact1.saveDupAccCon();
        objVFC_CreateContact1.saveDupAccount();
        objVFC_CreateContact1.saveDupContact();
        
        
        test.stoptest();
    
    }
    
    static testMethod void testVFC_ContactCreateForConsumerAccounts() {
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        
        Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId());
        acc.Country__c=country.id;
        insert acc;
        
        Contact   con= Utils_TestMethods.createContact(acc.id,'TestContact');
        insert con;
        
        Case CaseObj= Utils_TestMethods.createCase(acc.Id,Con.Id,'Open');
        insert CaseObj;
        
        test.starttest();
        
        // For Consumer Account
        PageReference pageRef_CreateContact1 = Page.VFP_CreateContact;
        Test.setCurrentPage(pageRef_CreateContact1);
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLDEC12ACC01);//CLDEC12ACC01
        System.currentPageReference().getParameters().put('Case',CaseObj.id);       
        
        ApexPages.currentPage().getParameters().put('FirstName','conFirstName');
        ApexPages.currentPage().getParameters().put('LastName','conLastName');
        ApexPages.currentPage().getParameters().put('LocalFirstName','conFirstNameLocal');
        ApexPages.currentPage().getParameters().put('LocalLastName','conLastNameLocal');
        ApexPages.currentPage().getParameters().put('AccountName','TEST VFC_ContactSearch');
        ApexPages.currentPage().getParameters().put('LocalAccountName','Account Local Name');
        ApexPages.currentPage().getParameters().put('Street','Street');
        ApexPages.currentPage().getParameters().put('StreetLocalLang','StreetLocalLang');
        ApexPages.currentPage().getParameters().put('City','testcity1');
        ApexPages.currentPage().getParameters().put('LocalCity','CityLocal');
        
        ApexPages.currentPage().getParameters().put('ZipCode','zipcode');
        ApexPages.currentPage().getParameters().put('State/Province',stateProv.Id);
        ApexPages.currentPage().getParameters().put('Country',country.Id);
        ApexPages.currentPage().getParameters().put('POBox','POBox');
        ApexPages.currentPage().getParameters().put('Email','abc@gmail.com');
        ApexPages.currentPage().getParameters().put('CallerPhoneNumber','123456');
            
        VFC_CreateContact  objVFC_CreateContact2 = new VFC_CreateContact();
        
        objVFC_CreateContact2.account.ToBeDeleted__c = false;
        objVFC_CreateContact2.account.CorporateHeadquarters__c = true;
        objVFC_CreateContact2.account.LeadingBusiness__c = 'BD';
        objVFC_CreateContact2.account.MarketSegment__c = 'BDZ'; 
        objVFC_CreateContact2.account.RecordTypeId=System.Label.CLDEC12ACC01;

        objVFC_CreateContact2.ischecked=True;
        objVFC_CreateContact2.saveAccount();
        
        
        Test.setCurrentPage(pageRef_CreateContact1);
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLDEC12ACC01);//CLDEC12ACC01
        System.currentPageReference().getParameters().put('Case','');         
        
        ApexPages.currentPage().getParameters().put('FirstName','conFirstName');
        ApexPages.currentPage().getParameters().put('LastName','conLastName');
        ApexPages.currentPage().getParameters().put('LocalFirstName','conFirstNameLocal');
        ApexPages.currentPage().getParameters().put('LocalLastName','conLastNameLocal');
        ApexPages.currentPage().getParameters().put('AccountName','TEST VFC_ContactSearch');
        ApexPages.currentPage().getParameters().put('LocalAccountName','Account Local Name');
        ApexPages.currentPage().getParameters().put('Street','Street');
        ApexPages.currentPage().getParameters().put('StreetLocalLang','StreetLocalLang');
        ApexPages.currentPage().getParameters().put('City','city');
        ApexPages.currentPage().getParameters().put('LocalCity','CityLocal');        
        ApexPages.currentPage().getParameters().put('ZipCode','zipcode');
        ApexPages.currentPage().getParameters().put('State/Province',stateProv.Id);
        ApexPages.currentPage().getParameters().put('Country',country.Id);
        ApexPages.currentPage().getParameters().put('POBox','POBox');
        ApexPages.currentPage().getParameters().put('Email','abc@gmail.com');
        ApexPages.currentPage().getParameters().put('CallerPhoneNumber','123456');
        ApexPages.currentPage().getParameters().put('cas11','cas11');
        
        VFC_CreateContact  objVFC_CreateContact3 = new VFC_CreateContact();
        
        objVFC_CreateContact3.account.ToBeDeleted__c = false;
        objVFC_CreateContact3.account.CorporateHeadquarters__c = true;
        objVFC_CreateContact3.account.LeadingBusiness__c = 'BD';
        objVFC_CreateContact3.account.MarketSegment__c = 'BDZ'; 
        objVFC_CreateContact3.account.RecordTypeId=System.Label.CLDEC12ACC01;

        objVFC_CreateContact3.ischecked=True;
        objVFC_CreateContact3.saveAccount();
        
        ApexPages.currentPage().getParameters().put('Country','');
        VFC_CreateContact  objVFC_CreateContact5 = new VFC_CreateContact();
        objVFC_CreateContact5.saveAccount();
        
        test.stoptest();
        
    } 
    
    
}