global class AP_PRMEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = notifyPRMPrimaryContact(email);
        return result;
    }
    private Messaging.InboundEmailResult notifyPRMPrimaryContact(Messaging.InboundEmail email){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        try{
            String conId = email.Subject.substringAfter('ReceivedId: ');
            Contact newCon =[SELECT Id,AccountId,PRMContactRegistrationStatus__c,PRMAdminEmail__c FROM Contact WHERE Id =:conId];
            List<Contact> primaryContactList = [SELECT Id,Account.Country__c, PRMContactRegistrationStatus__c,PRMReasonForDecline__c,PRMPrimaryContact__c,PRMCustomerClassificationLevel1__c, PRMCustomerClassificationLevel2__c, PRMCountry__c,Country__c,Account.PRMCountry__c,Account.PRMAreaOfFocus__c,Account.PRMBusinessType__c,PRMEmail__c FROM Contact WHERE PRMPrimaryContact__c = True AND AccountId =: newCon.AccountId];
            
            if(!PrimaryContactList.isEmpty()){
                Contact primaryContact = primaryContactList[0];
                String Country = String.isNotBlank(primaryContact.Account.PRMCountry__c) ? primaryContact.Account.PRMCountry__c:primaryContact.Account.Country__c;
                //List<CountryChannels__c> conChList = [SELECT Id,AllowPrimaryToManage__c,PRMCountry__c FROM CountryChannels__c WHERE Country__c = :primaryContact.Account.PRMCountry__c AND ChannelCode__c =:primaryContact.Account.PRMBusinessType__c AND SubChannelCode__c = :primaryContact.Account.PRMAreaOfFocus__c Active__c = true];
                //if(!conChList.isEmpty() && !string.isBlank(primaryContact.PRMEmail__c)){
                    Boolean itsTrue = True;
                    // Call the Flow
                    Map<String, Object> params = new Map<String, Object>();
                    params.put('getContact',primaryContact);
                    params.put('PrimaryContact',itsTrue);
                    params.put('getContactCountry',Country);
                    params.put('getSecondaryContact',newCon);
                    Flow.Interview.PRMEmailNotificationsFlow PRMPrimaryEmailFlow = new Flow.Interview.PRMEmailNotificationsFlow(params);
                    PRMPrimaryEmailFlow.start();
                //}
            }
            result.success = true;
        }
        catch(exception e){
            result.success = false;
            result.message = e.getMessage();
            System.debug('**** An exception ****'+e.getMessage());
        }
        return result;
    }
}