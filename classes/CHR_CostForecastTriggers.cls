/*
    Author          : Priyanka Shetty (Schneider Electric) 
    Date Created    : 12/04/2013
    Description     : Class utilised by triggers ChangeRequestCostForecastTriggerAfterUpdate .
                        Custom field ChangeRequest costs forecast Data Auto populated in the Program.
*/

public with sharing class CHR_CostForecastTriggers {



    public static void CRCostForecastAfterDelete(List<ChangeRequestCostsForecast__c> CRCostForecastListToProcessCONDITION1)
    {
        System.Debug('****** CRCostForecastAfterDelete Start ****'); 
        List<ChangeRequestCostsForecast__c> progListToProcessCONDITION1 = new List<ChangeRequestCostsForecast__c>();
        
        for (ChangeRequestCostsForecast__c oldBud:CRCostForecastListToProcessCONDITION1)
        {                                  
            if(oldBud.Active__c == true)
            {
                progListToProcessCONDITION1.add(oldBud);                
            }
        }
        
        if(CRCostForecastListToProcessCONDITION1.size()>0)
            CHR_CostForecastUtils.updateProgramFieldsonDelete(progListToProcessCONDITION1);            
            
        System.Debug('****** CRCostForecastAfterDelete End ****');         
        
    }
    
    
    
    public static void CRCostForecastbeforeupdate(Map<Id,ChangeRequestCostsForecast__c > newMap,Map<Id,ChangeRequestCostsForecast__c > oldMap)
    {
        System.Debug('****** CRCostForecastbeforeupdate Start ****'); 
        List<ChangeRequestCostsForecast__c > budgetListToProcessCONDITION1 = new List<ChangeRequestCostsForecast__c >();
        List<ChangeRequestCostsForecast__c > budgetListActiveToProcessCONDITION1 = new List<ChangeRequestCostsForecast__c >();
        
        for (ID budId:newMap.keyset())
        {
            ChangeRequestCostsForecast__c  newBud = newMap.get(budId);
            ChangeRequestCostsForecast__c  oldBud = oldMap.get(budId);
            
            if(newBud.Active__c == true && oldbud.Active__c == false)
            {
                budgetListToProcessCONDITION1.add(newBud);                
            }
            if(newBud.Active__c == true)
            {
                budgetListActiveToProcessCONDITION1.add(newBud);           
            }
        }
        
        
        if (budgetListToProcessCONDITION1.size()>0) 
            CHR_CostForecastUtils.deactivateActiveCRCostForecast(budgetListToProcessCONDITION1,true);
        
        if (budgetListActiveToProcessCONDITION1.size()>0){
            CHR_CostForecastUtils.updateCRFieldsonInsertUpdate(budgetListActiveToProcessCONDITION1);
            CHR_CostForecastUtils.UpdatePLvalue(budgetListActiveToProcessCONDITION1);  
		}
        System.Debug('****** CRCostForecastbeforeupdate End ****'); 
             
    }
    
    public static void CRCostForecastbeforeInsert(List<ChangeRequestCostsForecast__c> newBudgets)
    {
        System.Debug('****** CRCostForecastbeforeInsert Start ****'); 
        List<ChangeRequestCostsForecast__c> budgetListToProcessCONDITION1 = new List<ChangeRequestCostsForecast__c>();
        
        for (ChangeRequestCostsForecast__c newBud:newBudgets)
        {                                  
            if(newBud.Active__c == true)
            {
                budgetListToProcessCONDITION1.add(newBud);                
            }
        }
        
        
        if (budgetListToProcessCONDITION1.size()>0) 
            CHR_CostForecastUtils.deactivateActiveCRCostForecast(budgetListToProcessCONDITION1,false);
            CHR_CostForecastUtils.updateCRFieldsonInsertUpdate(budgetListToProcessCONDITION1);
         
            
        System.Debug('****** CRCostForecastbeforeInsert End ****');         
        
    }
    
}