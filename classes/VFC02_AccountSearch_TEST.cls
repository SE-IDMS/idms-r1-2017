/**
 * This class contains unit tests for validating the behavior of Apex controller VFC02_AccountSearch.
 *
 * ~ Created by Adrian MODOLEA / Mohamed EL MOUSSAOUI
 * ~ SCHNEIDER SR.0 - RELEASE - October 18th 2010
 * 
**/
/*    Modified By ACCENTURE IDC
      Modifed for December Release Business Requirement BR-924
      Modified Date: 16/11/2011
*/

@isTest
private class VFC02_AccountSearch_TEST {

    static testMethod void testVFC02_AccountSearch() {
        System.debug('#### START test method for VFC02_AccountSearch ####');
        // create unit country 
        Country__c country = Utils_TestMethods.createCountry();
        insert country;

        // create unit stateProv 
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
             
        // start test
        PageReference vfPage = Page.VFP02_AccountSearch;
        Test.setCurrentPage(vfPage);
        
       // Add parameters to page URL  
        ApexPages.currentPage().getParameters().put('retURL', '/001');
        ApexPages.currentPage().getParameters().put('save_new', '1');               
        Account account = new Account();
        
        VFC02_AccountSearch vfCtrl = new VFC02_AccountSearch(new ApexPages.StandardController(account));
        //Start of December Release Modification
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VCC06_DisplaySearchResults componentcontroller1 =vfCtrl.resultsController; 
        //End of December Release Modification
        System.debug('vfCtrl.hasNext: ' + vfCtrl.hasNext);
        
        // 1/ test methods without setting the account fields
        vfCtrl.doSearchAccount();
        
        // 2/ test methods with all account fields

        account.Name = 'TEST VFC02_AccountSearch';
        account.AccountLocalName__c = 'Account Local Name';
        account.Street__c = 'Street';
        account.ZipCode__c = 'zipcode';
        account.City__c = 'city';
        account.StateProvince__c = stateProv.Id;
        account.Country__c = country.Id;
        account.POBox__c = 'pobox';
        account.POBoxZip__c = 'poboxzip';
        account.StreetLocalLang__c = 'streetlocal';
        account.ToBeDeleted__c = false;
        account.CorporateHeadquarters__c = true;
        account.LeadingBusiness__c = 'BD';
        account.ClassLevel1__c = 'EU';
        
        
        vfCtrl.doSearchAccount();
        vfCtrl.continueCreation();
        
        account.Name = 'TEST VFC02_AccountSearch';
        account.AccountLocalName__c = 'Account Local Name';
        account.Street__c = 'Street';
        account.ZipCode__c = 'zipcode';
        account.City__c = 'city';
        account.StateProvince__c = stateProv.Id;
        account.Country__c = country.Id;
        account.POBox__c = 'pobox';
        account.POBoxZip__c = 'poboxzip';
        account.StreetLocalLang__c = 'streetlocal';
        account.ToBeDeleted__c = false;
        account.CorporateHeadquarters__c = true;
        account.LeadingBusiness__c = 'BD';
        account.MarketSegment__c = 'BDZ'; 
        
        vfCtrl.doSearchAccount();
        vfCtrl.continueCreation();
        
        List<Account> accs = new List<Account>();
        for(Integer i=0;i<20;i++)
        {
        Account a = new Account();
        a.Name = 'TEST VFC02_AccountSearch';
        a.AccountLocalName__c = 'Account Local Name';
        a.Street__c = 'Street';
        a.ZipCode__c = 'zipcode';
        a.City__c = 'city';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
        a.StreetLocalLang__c = 'streetlocal';
        a.ToBeDeleted__c = false;
        a.CorporateHeadquarters__c = true;
        a.LeadingBusiness__c = 'BD';
        a.MarketSegment__c = 'BDZ';
       
            accs.add(a);
            
        }
        insert accs;
        
        vfCtrl.doSearchAccount();
        ApexPages.currentPage().getParameters().put('soql', '1');
        vfCtrl.getSearchString(); //Modified for December Release
        vfCtrl.doSearchAccount();
        
        vfCtrl.continueCreation();
        
        /********************************************************************************/
        /*          START: OCT 2013 REL.POP FIRST & LAST NAME FOR CONSUMER ACCOUNTS     */
        /********************************************************************************/     
        
        PageReference vfPage1 = Page.VFP02_AccountSearch;
        Test.setCurrentPage(vfPage1);
        ApexPages.currentPage().getParameters().put('Accname','Test Account');
        ApexPages.currentPage().getParameters().put('RecordType', [SELECT r.Id, r.Name, r.SobjectType from RecordType r Where SobjectType='Account' And IsActive=true AND r.Name = :System.Label.CLDEC12AC04].Id);
        VFC02_AccountSearch vfCtr2 = new VFC02_AccountSearch(new ApexPages.StandardController(account));
        vfCtr2.continueCreation();
        
        vfPage1 = Page.VFP02_AccountSearch;
        Test.setCurrentPage(vfPage1);
        ApexPages.currentPage().getParameters().put('Accname','Account1');
        ApexPages.currentPage().getParameters().put('RecordType', [SELECT r.Id, r.Name, r.SobjectType from RecordType r Where SobjectType='Account' And IsActive=true AND r.Name = :System.Label.CLDEC12AC04].Id);
        vfCtr2 = new VFC02_AccountSearch(new ApexPages.StandardController(account));
        vfCtr2.continueCreation();
        
        /********************************************************************************/
        /*          END: OCT 2013 REL.POP FIRST & LAST NAME FOR CONSUMER ACCOUNTS     */
        /********************************************************************************/    
    }
}