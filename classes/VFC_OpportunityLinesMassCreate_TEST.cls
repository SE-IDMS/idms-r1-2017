//Test class for the VFC_OpportunityLinesMassCreate (Mass create oppty lineitems) - OCT 2015 Release
@isTest
public class VFC_OpportunityLinesMassCreate_TEST {
    static testMethod void testMethodMassUploadLines() {
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
        insert opp;
        
        OPP_Product__c prd=Utils_TestMethods.createProduct('INDUSTRIAL AUTOMATION','IDSDS','DRIVE SYSTEMS LV','STANDARD EQUIPMENT');
        insert prd;
        
        Product2 prd2= Utils_TestMethods.createCommercialReference('CMREF123','MaterialDescription For CMREF123','BU-CMREF123', 'PL-CMREF123', 'PF-CMREF123', 'F-CMREF123',false, false, false);
        insert prd2;
        
        CS_CurrencyDecimalSeperator__c cd= new CS_CurrencyDecimalSeperator__c(GroupingSeperator__c=',',DecimalSeperator__c='.',Name=UserInfo.getLocale());
           insert cd;
        
         List<CS_OpportunityLineItemCSVMap__c> csopptylineitemdata=new List<CS_OpportunityLineItemCSVMap__c>();
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Commercial Reference',ApiName__c='TECH_CommercialReference__c',FieldType__c='',isRequired__c=True,MassCreationType__c='Commercial Reference'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Family',ApiName__c='Family__c',FieldType__c='',isRequired__c=False,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Product BU',ApiName__c='ProductBU__c',FieldType__c='',isRequired__c=True,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Product Family',ApiName__c='ProductFamily__c',FieldType__c='',isRequired__c=False,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Product Line',ApiName__c='ProductLine__c',FieldType__c='',isRequired__c=True,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Quantity',ApiName__c='Quantity__c',FieldType__c='integer',isRequired__c=True,MassCreationType__c='Common'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Unit Amount',ApiName__c='Amount__c',FieldType__c='currency',isRequired__c=True,MassCreationType__c='Common'));
         insert csopptylineitemdata;
         Test.startTest();
        ApexPages.StandardSetController controller= new ApexPages.StandardSetController(new List<Opportunity>{opp});            
        VFC_OpportunityLinesMassCreate controllerinstance=new VFC_OpportunityLinesMassCreate(controller);            
        ApexPages.currentPage().getParameters().put('id',opp.id);     
        controllerinstance=new VFC_OpportunityLinesMassCreate(controller);    
        StringListWrapper sampleStringListWrapper=new StringListWrapper();               
        sampleStringListWrapper.stringList.add(new List<String>{'Commercial Reference','Quantity','Unit Amount'});
        sampleStringListWrapper.stringList.add(new List<String>{'CMREF123','1k','12M'});
        controllerinstance.csvList=sampleStringListWrapper;
        controllerinstance.type='Commercial Reference';
        controllerinstance.typeSelect();
        controllerinstance.populateRequiredMap();
        controllerinstance.getItems();
        controllerinstance.preapareLineItemList(); 
        controllerinstance.create();  
        controllerinstance.proceedWithInsert();
       
        ApexPages.StandardSetController controller1= new ApexPages.StandardSetController(new List<Opportunity>{opp});            
        VFC_OpportunityLinesMassCreate controllerinstance1=new VFC_OpportunityLinesMassCreate(controller1);            
        ApexPages.currentPage().getParameters().put('id',opp.id);     
        controllerinstance1=new VFC_OpportunityLinesMassCreate(controller1);    
        StringListWrapper sampleStringListWrapper1=new StringListWrapper();               
        sampleStringListWrapper1.stringList.add(new List<String>{'Product BU','Product Line','Product Family','Family','Unit Amount','Quantity'});
        sampleStringListWrapper1.stringList.add(new List<String>{'INDUSTRIAL AUTOMATION','IDSDS','DRIVE SYSTEMS LV','STANDARD EQUIPMENT','1b','12'});
        controllerinstance1.csvList=sampleStringListWrapper1;
        controllerinstance1.type='PM0 Level';
        controllerinstance1.typeSelect();
        controllerinstance1.populateRequiredMap();
        controllerinstance1.getItems();
        controllerinstance1.preapareLineItemList(); 
        controllerinstance1.create();  
        controllerinstance1.proceedWithInsert();
        Test.stopTest();     
    }
    
    static testMethod void testMethodMassUploadLines2() {
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
        insert opp;
        
        OPP_Product__c prd=Utils_TestMethods.createProduct('INDUSTRIAL AUTOMATION','IDSDS','DRIVE SYSTEMS LV','STANDARD EQUIPMENT');
        insert prd;
        OPP_Product__c prd1=Utils_TestMethods.createProduct('INDUSTRIAL AUTOMATION','IDSDS','DRIVE SYSTEMS LV','STANDARD EQUIPMENT');
        insert prd1;
        
        Product2 prd2= Utils_TestMethods.createCommercialReference('CMREF123','MaterialDescription For CMREF123','BU-CMREF123', 'PL-CMREF123', 'PF-CMREF123', 'F-CMREF123',false, false, false);
        insert prd2;
        Product2 pr0d2= Utils_TestMethods.createCommercialReference('CMREF1234','MaterialDescription For CMREF1243','BU-CMREF1234', 'PL-CMREF1234', 'PF-CMREF1234', 'F-CMREF1234',false, false, false);
        insert pr0d2;
        
        CS_CurrencyDecimalSeperator__c cd= new CS_CurrencyDecimalSeperator__c(GroupingSeperator__c=',',DecimalSeperator__c='.',Name=UserInfo.getLocale());
           insert cd;
        
         List<CS_OpportunityLineItemCSVMap__c> csopptylineitemdata=new List<CS_OpportunityLineItemCSVMap__c>();
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Commercial Reference',ApiName__c='TECH_CommercialReference__c',FieldType__c='',isRequired__c=True,MassCreationType__c='Commercial Reference'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Family',ApiName__c='Family__c',FieldType__c='',isRequired__c=False,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Product BU',ApiName__c='ProductBU__c',FieldType__c='',isRequired__c=True,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Product Family',ApiName__c='ProductFamily__c',FieldType__c='',isRequired__c=False,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Product Line',ApiName__c='ProductLine__c',FieldType__c='',isRequired__c=True,MassCreationType__c='PM0 level'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Quantity',ApiName__c='Quantity__c',FieldType__c='integer',isRequired__c=True,MassCreationType__c='Common'));
         csopptylineitemdata.add(new CS_OpportunityLineItemCSVMap__c(Name='Unit Amount',ApiName__c='Amount__c',FieldType__c='currency',isRequired__c=True,MassCreationType__c='Common'));
         insert csopptylineitemdata;
         Test.startTest();
        ApexPages.StandardSetController controller= new ApexPages.StandardSetController(new List<Opportunity>{opp});            
        VFC_OpportunityLinesMassCreate controllerinstance=new VFC_OpportunityLinesMassCreate(controller);            
        ApexPages.currentPage().getParameters().put('id',opp.id);     
        controllerinstance=new VFC_OpportunityLinesMassCreate(controller);    
        StringListWrapper sampleStringListWrapper=new StringListWrapper();               
        sampleStringListWrapper.stringList.add(new List<String>{'Commercial Reference','Quantity','Unit Amount'});
        sampleStringListWrapper.stringList.add(new List<String>{'CMREF123','1k','12M'});
        sampleStringListWrapper.stringList.add(new List<String>{'CMREF1234','1K','12k'});
        //sampleStringListWrapper.stringList.add(new List<String>{'CMREF123456','1M','12M'});
        try{
        controllerinstance.csvList=sampleStringListWrapper;
        controllerinstance.flag=true;
        controllerinstance.rowNum=12;
        controllerinstance.typeSelect();
        controllerinstance.type='Commercial Reference';
        controllerinstance.typeSelect();
        controllerinstance.populateRequiredMap();
        controllerinstance.getItems();
        controllerinstance.preapareLineItemList(); 
        controllerinstance.create();  
        controllerinstance.proceedWithInsert();
       }
       Catch(Exception ex){}
        ApexPages.StandardSetController controller1= new ApexPages.StandardSetController(new List<Opportunity>{opp});            
        VFC_OpportunityLinesMassCreate controllerinstance1=new VFC_OpportunityLinesMassCreate(controller1);            
        ApexPages.currentPage().getParameters().put('id',opp.id);     
        controllerinstance1=new VFC_OpportunityLinesMassCreate(controller1);    
        StringListWrapper sampleStringListWrapper1=new StringListWrapper();               
        sampleStringListWrapper1.stringList.add(new List<String>{'Product BU','Product Line','Product Family','Family','Unit Amount','Quantity'});
        sampleStringListWrapper1.stringList.add(new List<String>{'INDUSTRIAL AUTOMATION','IDSDS','DRIVE SYSTEMS LV','STANDARD EQUIPMENT','1b','12'});
        sampleStringListWrapper1.stringList.add(new List<String>{'INDUSTRIAL AUTOMATION','IDSDS','DRIVE SYSTEMS LV','STANDARD EQUIPMENT','1b','12'});
        sampleStringListWrapper1.stringList.add(new List<String>{'','IDSDS-blah','','','1M','12'});
        sampleStringListWrapper1.stringList.add(new List<String>{'','','','','1M','12'});
        try{
            controllerinstance1.csvList=sampleStringListWrapper1;
            controllerinstance1.type='PM0 Level';
            controllerinstance1.typeSelect();
            controllerinstance1.populateRequiredMap();
            controllerinstance1.getItems();
            controllerinstance1.preapareLineItemList(); 
            controllerinstance1.create();  
            controllerinstance1.proceedWithInsert();
        }
        Catch(Exception ex){}
        Test.stopTest();     
    }
}