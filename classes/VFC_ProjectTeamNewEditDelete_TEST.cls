/*
    Description: Test Class for VFC_ProjectTeamNewEditDelete
*/
@isTest
public class VFC_ProjectTeamNewEditDelete_TEST {
     static testMethod void testMethodVFC_ProjectTeamNewEditDelete()
    {
         Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        
        OPP_ProjectTeam__c  prjTeam = Utils_TestMethods.createMasterProjectTeamMember(prj.Id,UserInfo.getUserId());
        insert prjTeam;
        
        OPP_ProjectTeam__c  prjTeam1 = Utils_TestMethods.createMasterProjectTeamMember(prj.Id,UserInfo.getUserId());
        
        PageReference myVfPage = Page.VFP_ProjectTeamNewEdit;
        Test.setCurrentPageReference(myVfPage);
        ApexPages.currentPage().getParameters().put('id',prjTeam.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prjTeam);
        VFC_ProjectTeamNewEditDelete extController = new VFC_ProjectTeamNewEditDelete(sc);
        try{
            extController.custId=prj.Id;
            extController.prjTeam=prjTeam;
            extController.pgeMsg=null;
            extController.saveAndNew(); 
            extController.prjTeam=prjTeam1;
            extController.deletePrjTeam();
            extController.saveAndNew();      
            extController.deletePrjTeam();
            extController.prjTeam=prjTeam1;
            extController.deletePrjTeam();
            
       }
       Catch(Exception ex){
           System.debug('>>>>>Error'+ex.getMessage());
       }
       extController.prjTeam=prjTeam;          
       extController.deletePrjTeam();
        
    }
     static testMethod void testMethodVFC_ProjectTeamNewEditDelete2()
    {
         Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        
        OPP_ProjectTeam__c  prjTeam1 = Utils_TestMethods.createMasterProjectTeamMember(prj.Id,UserInfo.getUserId());
        OPP_ProjectTeam__c  prjTeam2 = Utils_TestMethods.createMasterProjectTeamMember(prj.Id,UserInfo.getUserId());
        try{
             PageReference myVfPage = Page.VFP_ProjectTeamNewEdit;
            Test.setCurrentPageReference(myVfPage);
            ApexPages.currentPage().getParameters().put('custId',prj.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(prjTeam1);
            VFC_ProjectTeamNewEditDelete extController = new VFC_ProjectTeamNewEditDelete(sc);
            extController.saveAndNew();
            sc = new ApexPages.StandardController(prjTeam2);
            extController = new VFC_ProjectTeamNewEditDelete(sc);
            extController.saveAndNew();
        }
        Catch(Exception ex){
            System.debug('>>>>>Error'+ex.getMessage());
        }
    
    }
}