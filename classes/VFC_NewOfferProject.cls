/**********   
    Author: Hanamanth B PGD Team)  
    Date Of Creation:  
    Description : Oct release 2014
*************/

public class VFC_NewOfferProject{

    public VFC_NewOfferProject(ApexPages.StandardSetController controller) {

    }

   
    public boolean blnIsAccs{get;set;}
    public Milestone1_Project__c NewProject{get;set;} 
    public Offer_Lifecycle__c Offer{get;set;} 
    private String offerID;
   
    Public Boolean blnCancel{get; set;} 
    public VFC_NewOfferProject (){
        offerID = ApexPages.currentPage().getParameters().get(Label.CLOCT14COLAbFO02);
        system.debug('TEst Id ------->'+offerID);
                
    }
    
    public VFC_NewOfferProject(ApexPages.StandardController controller){
        NewProject = (Milestone1_Project__c)controller.getRecord();
      
    } 
    
    public void redirecttoMilestone1Project(Milestone1_Project__c newOfferProject){
        Offer = New Offer_Lifecycle__c(); 
        blnIsAccs=false;        
        offerID= system.currentpagereference().getParameters().get('id');
        id profileId =label.CLMAR16ELLAbFO15;// [Select Id,Name from Profile where name = 'System Administrator'].id;
        system.debug('TEst IdHBP ------->'+offerID);  
        set<Id> userAccSet = new set<Id>();         

        List<Offer_Lifecycle__c> offerList = [SELECT ID,name,OfferScope__c,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c,RecordTypeid from Offer_Lifecycle__c WHERE ID=:offerID];       

        if(offerList.size() == 1)  {

            Offer = offerList[0];
            userAccSet = new set<Id>{Offer.ownerid,Offer.Segment_Marketing_VP__c,Offer.Launch_Area_Manager__c,Offer.Marketing_Director__c,Offer.Withdrawal_Owner__c};
        }
        if(!userAccSet.contains(Userinfo.getuserid()) && userinfo.getProfileid()!=profileId) {
            blnIsAccs=true;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));//You dont have access to create Project'
        }
        
        System.debug('$$$$'+offerList[0].RecordTypeid);
       
    }
    public pagereference projectEditPage()  {
   
    PageReference NewProjectEditPage;  
    redirecttoMilestone1Project(NewProject);
    if(!blnIsAccs) {
    Id offerlaunchid = [Select Id From RecordType  Where SobjectType = 'Offer_Lifecycle__c' and DeveloperName = 'Offer_Launch_Product'][0].Id;
    Id offerwithdrawalid = [Select Id From RecordType  Where SobjectType = 'Offer_Lifecycle__c' and DeveloperName = 'Offer_Withdrawal_Product'][0].Id;
    Id offersolutionlaunchid = [Select Id From RecordType  Where SobjectType = 'Offer_Lifecycle__c' and DeveloperName = 'Offer_Launch_Solution'][0].Id;
    
    Id projectofferlaunchid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Introduction_Project'][0].Id;
    Id projectofferwithdrawalid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Withdrawal_Project'][0].Id;
    Id projectoffersolutionlaunchid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Launch_Solution_Project'][0].Id;
       
        
    Id IntroductionRegionId = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Introduction Region').getRecordTypeId();
    Id projectSolutionRegionId = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Launch Solution Region').getRecordTypeId();
    Id projectWithdrawalRegionid = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Withdrawal Region').getRecordTypeId();        
       
         // project
        NewProjectEditPage = new PageReference('/'+SObjectType.Milestone1_Project__c.getKeyPrefix()+'/e' );
        
        NewProjectEditPage.getParameters().put(Label.CLOCT14COLAbFO04, Label.CLOCT14COLAbFO05);
        
        if(Offer.id!=null && Offer.OfferScope__c!=null) {
            
            if(Offer.OfferScope__c!=label.CLMAR16ELLAbFO16) {//'Region'
            
                /**retURL**/
                NewProjectEditPage.getParameters().put(Label.CLOCT14COLAbFO06, Offer.id);   
                 
                /** Id_lkid **/
                NewProjectEditPage.getParameters().put(Label.CLOCT14COLAbFO02, Offer.id);
                NewProjectEditPage.getParameters().put(Label.CLOCT14COLAbFO01, Offer.name);
                /**RecordType **/
                system.debug('$$$$'+Offer.RecordTypeid);
                System.debug('$$$$'+offersolutionlaunchid);
                 system.debug('Test1111111111111'+Label.CLOCT14COLAbFO03);
                
                if(string.valueOf(Offer.RecordTypeid)==offerlaunchid) {//012170000008U3Q
                    NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO17,projectofferlaunchid); // project Introduction recordtype
                }
                if(string.valueOf(Offer.RecordTypeid)==offersolutionlaunchid) {//012170000008U3Q
                    NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO17,projectoffersolutionlaunchid); // solution recordtype
                    System.debug('I am here');
                }if(string.valueOf(Offer.RecordTypeid)==offerwithdrawalid){
                     NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO17,projectofferwithdrawalid);//Withdrawal recordtype
                }
            }
            If(Offer.OfferScope__c==label.CLMAR16ELLAbFO16) {//Region
                
                NewProjectEditPage = new PageReference(label.CLMAR16ELLAbFO18);//'/apex/VFP_ProjectRegionCountrySelection?' );
                /**retURL**/
                NewProjectEditPage.getParameters().put(Label.CLOCT14COLAbFO06, Offer.id);   

                /** Id_lkid **/
                NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO19, Offer.id);//'urlOfferId'
                
                if(string.valueOf(Offer.RecordTypeid)==offerlaunchid) {//012170000008U3Q
                    NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO17,IntroductionRegionId); // Region Introduction recordtype
                }
                if(string.valueOf(Offer.RecordTypeid)==offersolutionlaunchid) {//012170000008U3Q
                    NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO17,projectSolutionRegionId); // solution recordtype
                    System.debug('I am here');
                }if(string.valueOf(Offer.RecordTypeid)==offerwithdrawalid){
                     NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO17,projectWithdrawalRegionid);//Withdrawal recordtype
                }
            }
           
            
        }
        return NewProjectEditPage;    
    }else {
        
        return null;
    }       
         
                                                        
    }
    //
    
    Public Pagereference offerCancel()   {
         Pagereference prf ;
         string OfferId = Offer.id;
         if(OfferId!=null){
         
             prf  = new PageReference('/'+ OfferId);
             prf .setredirect(true);
            
        }
         return prf ; 
    }               
}