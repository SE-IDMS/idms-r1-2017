@isTest
private class VFC_OpportunityLost_TEST{
   
    public  static testMethod void OpportunityLostTest() {
        
         getCustomeSettingData();
         
         Account acc = Utils_TestMethods.createAccount();
         Database.insert(acc);
         Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
         Database.insert(opp);
         Competitor__c competitor = Utils_TestMethods.createCompetitor();
         Database.insert(competitor);
         OPP_OpportunityCompetitor__c oppcomp = Utils_TestMethods.createOppCompetitor(competitor.id,opp.id);
         Database.insert(oppcomp);
         test.startTest();
         PageReference pageRef = Page.VFP_OpportunityLost;
         pageRef.getParameters().put('status', Label.CLSEP12SLS14);
         pageRef.getParameters().put('reason', 'Financing');
         pageRef.getParameters().put('amount','10');
         Test.setCurrentPage(pageRef);  
         ApexPages.StandardController ssc = new ApexPages.StandardController(opp );  
         VFC_OpportunityLost opplost = new VFC_OpportunityLost(ssc);
         oppcomp.Winner__c =  true;
         database.update(oppcomp);
         opplost.pageLoad();
         opplost.oppt.Status__c = Label.CLSEP12SLS14;
         opplost.oppt.Reason__c = 'Financing';
         opplost.saveopp();
         oppcomp.Winner__c =  true;
         database.update(oppcomp);
         opplost.saveWinningCompetitors();
         opplost.newOpportunityCompetitor();
         opplost.newDefaultOpportunityCompetitor();//
         opplost.cancelWinningCompetitors();
        
         Opportunity opp2 = Utils_TestMethods.createOpenOpportunity(acc.id);
         Database.insert(opp2);
         PageReference pageRef2 = Page.VFP_OpportunityLost;
         pageRef2.getParameters().put('status', Label.CLSEP12SLS14);
         pageRef2.getParameters().put('reason', 'Financing');
         pageRef2.getParameters().put('amount','10');
         Test.setCurrentPage(pageRef2);  
         ApexPages.StandardController ssc2 = new ApexPages.StandardController(opp2 ); 
         VFC_OpportunityLost opplost2 = new VFC_OpportunityLost(ssc2);
         opplost2.pageLoad();
         opplost2.oppt.Status__c = Label.CLSEP12SLS14;
         opplost2.oppt.Reason__c = 'Financing';
         opplost2.saveopp();
         opplost2.pageLoad();
         test.stopTest();
     }
    
     public  static testMethod void OpportunityLostTest2() {
         getCustomeSettingData();
         
         Account acc = Utils_TestMethods.createAccount();
         Database.insert(acc);
         Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
         opp.status__c = 'Won - Deliver & Validate by Partner';
         Database.insert(opp);
         Competitor__c competitor = Utils_TestMethods.createCompetitor();
         Database.insert(competitor);
         OPP_OpportunityCompetitor__c oppcomp = Utils_TestMethods.createOppCompetitor(competitor.id,opp.id);
         oppcomp.Winner__c =  True;
         Database.insert(oppcomp);
         
         PageReference pageRef = Page.VFP_OpportunityLost;
         Test.setCurrentPage(pageRef);  
         ApexPages.StandardController ssc = new ApexPages.StandardController(opp );  
         VFC_OpportunityLost opplost = new VFC_OpportunityLost(ssc);
         opplost.pageLoad();
         opplost.oppt.Status__c = Label.CLSEP12SLS14;
         opplost.oppt.Reason__c = 'Financing';
         opplost.saveopp();
         oppcomp.Winner__c =  true;
         database.update(oppcomp);
         opplost.saveWinningCompetitors();
     }
    
     public  static testMethod void OpportunityLostTest3() {
         getCustomeSettingData();
         
         Account acc = Utils_TestMethods.createAccount();
         Database.insert(acc);
         Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
         opp.status__c = 'Won - Deliver & Validate by Partner';
         Database.insert(opp);
         Competitor__c competitor = Utils_TestMethods.createCompetitor();
         Database.insert(competitor);
         OPP_OpportunityCompetitor__c oppcomp = Utils_TestMethods.createOppCompetitor(competitor.id,opp.id);
         oppcomp.Winner__c =  True;
         Database.insert(oppcomp);
         
         PageReference pageRef = Page.VFP_OpportunityLost;
         Test.setCurrentPage(pageRef);  
         ApexPages.StandardController ssc = new ApexPages.StandardController(opp );  
         VFC_OpportunityLost opplost = new VFC_OpportunityLost(ssc);
         //opplost.oppt.Status__c = Label.CLSEP12SLS14;
         //opplost.oppt.Reason__c = 'Financing';
         opplost.pageLoad();    
         opplost.saveopp();         
     }
     
     public  static testMethod void OpportunityLostTest4() {
         getCustomeSettingData();
         
         Account acc = Utils_TestMethods.createAccount();
         Database.insert(acc);
         Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
         opp.status__c = Label.CLSEP12SLS14;
         opp.Reason__c = 'Other';
         Database.insert(opp);
         Competitor__c competitor = Utils_TestMethods.createCompetitor();
         Database.insert(competitor);
         Competitor__c competitor2 = Utils_TestMethods.createCompetitor();
         Database.insert(competitor2);
         OPP_OpportunityCompetitor__c oppcomp = Utils_TestMethods.createOppCompetitor(competitor.id,opp.id);
         oppcomp.Winner__c =  True;
         Database.insert(oppcomp);
         OPP_OpportunityCompetitor__c oppcomp2 = Utils_TestMethods.createOppCompetitor(competitor2.id,opp.id);
         oppcomp2.Winner__c =  false;
         Database.insert(oppcomp2);
         
         PageReference pageRef = Page.VFP_OpportunityLost;
         Test.setCurrentPage(pageRef);  
         ApexPages.StandardController ssc = new ApexPages.StandardController(opp);  
         VFC_OpportunityLost opplost = new VFC_OpportunityLost(ssc);
         opplost.pageLoad();
         opplost.oppt.Status__c = Label.CLSEP12SLS14;
         opplost.oppt.Reason__c = 'Financing';      
         opplost.saveopp();         
     }
    
     public Static List<CS_OpportunityWinnerCompetitor__c> getCustomeSettingData(){
         
         List<CS_OpportunityWinnerCompetitor__c> csoppwinnerComp = new List<CS_OpportunityWinnerCompetitor__c>();
         CS_OpportunityWinnerCompetitor__c owcone= new CS_OpportunityWinnerCompetitor__c();
         owcone.Name = 'Testone';
         owcone.Stage__c = '7 - Deliver & Validate';
         owcone.Status__c = 'Won - Deliver & Validate by Schneider';
         csoppwinnerComp.add(owcone);
         CS_OpportunityWinnerCompetitor__c owctwo= new CS_OpportunityWinnerCompetitor__c();
         owctwo.Name = 'Testtwo';
         owctwo.Stage__c = '7 - Deliver & Validate';
         owctwo.Status__c = 'Won - Deliver & Validate by Partner';
         csoppwinnerComp.add(owctwo);
         CS_OpportunityWinnerCompetitor__c owcthree= new CS_OpportunityWinnerCompetitor__c();
         owcthree.Name = 'Testthree';
         owcthree.Stage__c ='0 - Closed' ;
         owcthree.Status__c = 'Cancelled by Schneider';
         csoppwinnerComp.add(owcthree);
         CS_OpportunityWinnerCompetitor__c owcfour= new CS_OpportunityWinnerCompetitor__c();
         owcfour.Name = 'Testfour';
         owcfour.Stage__c ='0 - Closed' ;
         owcfour.Status__c ='Cancelled by Customer';
         csoppwinnerComp.add(owcfour);
         CS_OpportunityWinnerCompetitor__c owcfive= new CS_OpportunityWinnerCompetitor__c();
         owcfive.Name = 'Testfive';
         owcfive.Stage__c = '0 - Closed';
         owcfive.Status__c = 'Lost';
         csoppwinnerComp.add(owcfive);
         CS_OpportunityWinnerCompetitor__c owcsix= new CS_OpportunityWinnerCompetitor__c();
         owcsix.Name = 'Testsix';
         owcsix.Stage__c ='0 - Closed' ;
         owcsix.Status__c ='Account Not Selected';
         csoppwinnerComp.add(owcsix);
         
         Database.insert(csoppwinnerComp);
         
         return csoppwinnerComp ;
     }
}