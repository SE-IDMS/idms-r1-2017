@istest
public with sharing class VFC_MyProfileController_Test{ 
    
    @testSetup static void testSetupMethodPRM() {
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
        for(Profile p : ps)
        {
             profiles.put(p.name, p.id);
        }        
                  
        user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];
        admin.BypassVR__c =True;
        Test.startTest();
        ID  profilesId;
        system.runas(admin){
            
            Country__c TestCountry = new Country__c();
            TestCountry.Name   = 'India';
            TestCountry.CountryCode__c = 'IN';
            TestCountry.InternationalPhoneCode__c = '91';
            TestCountry.Region__c = 'APAC';
            INSERT TestCountry;
            
            StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            INSERT testStateProvince;
            
            PRMCountry__c PRMCon = new PRMCountry__c(
                    Country__c = TestCountry.Id,
                    CountrySupportEmail__c = 'test1.lastName@yopmail.com',
                    PLDatapool__c = 'US_en2',
                    CountryPortalEnabled__c = true
                );
            Insert PRMCon;
            
            ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
            insert cLC;
            ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
            insert cLChild; 
            
            CountryChannels__c cChannels = new CountryChannels__c();
            cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
            cChannels.Country__c = TestCountry.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true; cChannels.PRMCountry__c = PRMCon.Id;
            insert cChannels;
            
            Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',City__c = 'Bengaluru', 
                                    Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMCountry__c=TestCountry.id,PRMBusinessType__c='FI',
                                    PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, StateProvince__c = testStateProvince.id);
            insert PartnerAcc;
            
            
          
            Contact PartnerCon = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='999121212',
                  Country__c=TestCountry.Id,
                  PRMFirstName__c = 'Test',
                  PRMLastName__c = 'lastname',
                  PRMEmail__c = 'test.lastName@yopmail.com',
                  PRMWorkPhone__c = '999121212',
                  PRMPrimaryContact__c = true
                  );
                  
                  Insert PartnerCon;
                  
            Contact PartnerCon1 = new Contact(
                  FirstName='Test1',
                  LastName='lastname1',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  PRMMobilePhone__c='0987654321',
                  Country__c=TestCountry.Id,
                  WorkPhone__c='0987654321',
                  PRMFirstName__c = 'Test1',
                  PRMLastName__c = 'lastname1',
                  PRMEmail__c = 'test1.lastName@yopmail.com'
                  );      
                  
                  Insert PartnerCon1;   
            profilesId = [select id from profile where name='SE - Channel Partner (Community)'].Id; 
                
            String strOriginal = 'PHNhbWxwOlJlc3BvbnNlIHhtbG5zOnNhbWxwPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIERlc3RpbmF0aW9uPSJodHRwczovL2Rldm1hamJmby1zZWNvbW11bml0aWVzLmNzMi5mb3JjZS5jb20vcGFydG5lcnMvbG9naW4/c289MDBEUjAwMDAwMDF1d3pmIiBJRD0iczIyYzEwOWIxYmRkYmIxNjQ1ZmNjMGZlMmNmNGVhZDAyZDY0MzkxYmM3IiBJc3N1ZUluc3RhbnQ9IjIwMTYtMDYtMjdUMjI6MTU6NDVaIiBWZXJzaW9uPSIyLjAiPjxzYW1sOklzc3VlciB4bWxuczpzYW1sPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj5odHRwczovL2ltcy1pbnQuYnRzZWMuZGV2LnNjaG5laWRlci1lbGVjdHJpYy5jb206NDQzL29wZW5zc288L3NhbWw6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI3MyMmMxMDliMWJkZGJiMTY0NWZjYzBmZTJjZjRlYWQwMmQ2NDM5MWJjNyIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6VHJhbnNmb3JtcyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIi8+CjxkczpEaWdlc3RWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+dGRZTUt4SlkyenN3TU5lYnVsMDZMUUtnQW1rPTwvZHM6RGlnZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CmFNMWwxeC9VNlkyU0FGYVJvbU81YVdvdTVab013TzRpbU13RGZ4OW5vMFpFOGUyaFRYNkhoU0RHbmZrYUo0U05obVRseFpHMnB3VlUKdk41VXA4N2htZlZTYmVoR2ZPU00zUFVkV0tRNzhxTHF0b21FS1ZRQ1FTMDNPWE1FYWZiL1cybXBHbzhKNnZNT2FWaDhqUjU1ZC9nWApUNytsVzVqNFkvY2lyMkpxVk8wPQo8L2RzOlNpZ25hdHVyZVZhbHVlPgo8ZHM6S2V5SW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5RGF0YSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5Q2VydGlmaWNhdGUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgpNSUlDUURDQ0Fha0NCRWVOQjBzd0RRWUpLb1pJaHZjTkFRRUVCUUF3WnpFTE1Ba0dBMVVFQmhNQ1ZWTXhFekFSQmdOVkJBZ1RDa05oCmJHbG1iM0p1YVdFeEZEQVNCZ05WQkFjVEMxTmhiblJoSUVOc1lYSmhNUXd3Q2dZRFZRUUtFd05UZFc0eEVEQU9CZ05WQkFzVEIwOXcKWlc1VFUwOHhEVEFMQmdOVkJBTVRCSFJsYzNRd0hoY05NRGd3TVRFMU1Ua3hPVE01V2hjTk1UZ3dNVEV5TVRreE9UTTVXakJuTVFzdwpDUVlEVlFRR0V3SlZVekVUTUJFR0ExVUVDQk1LUTJGc2FXWnZjbTVwWVRFVU1CSUdBMVVFQnhNTFUyRnVkR0VnUTJ4aGNtRXhEREFLCkJnTlZCQW9UQTFOMWJqRVFNQTRHQTFVRUN4TUhUM0JsYmxOVFR6RU5NQXNHQTFVRUF4TUVkR1Z6ZERDQm56QU5CZ2txaGtpRzl3MEIKQVFFRkFBT0JqUUF3Z1lrQ2dZRUFyU1FjL1U3NUdCMkF0S2hiR1M1cGlpTGttSnpxRXNwNjRyRHhiTUoreERyeWUwRU4vcTFVNU9mKwpSa0RzYU4vaWdrQXZWMWN1WEVnVEw2UmxhZkZQY1VYN1F4RGhaQmhzWUY5cGJ3dE16aTRBNHN1OWhueEloVVJlYkdFbXhLVzlxSk5ZCkpzMFZvNStJZ2p4dUVXbmpublZnSFRzMSttcTVRWVRBN0U2WnlMOENBd0VBQVRBTkJna3Foa2lHOXcwQkFRUUZBQU9CZ1FCM1B3L1UKUXpQS1RQVFlpOXVwYkZYbHJBS013dEZmMk9XNHl2R1dXdmxjd2NOU1pKbVRKOEFSdlZZT01FVk5ic1Q0T0ZjZnUyL1BlWW9BZGlEQQpjR3kvRjJadWo4WEpKcHVRUlNFNlB0UXFCdURFSGpqbU9RSjByVi9yOG1PMVpDdEhSaHBaNXpZUmpoUkM5ZUNiang5VnJGYXgwSkRDCi9GZndXaWdtclcwWTBRPT0KPC9kczpYNTA5Q2VydGlmaWNhdGU+CjwvZHM6WDUwOURhdGE+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+PHNhbWxwOlN0YXR1cyB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIj4KPHNhbWxwOlN0YXR1c0NvZGUgVmFsdWU9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpzdGF0dXM6U3VjY2VzcyIgeG1sbnM6c2FtbHA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCI+Cjwvc2FtbHA6U3RhdHVzQ29kZT4KPC9zYW1scDpTdGF0dXM+PHNhbWw6QXNzZXJ0aW9uIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIElEPSJzMmMxYjdiMGFlNzBlNTg3N2VkMjNkMjU5MmMwYjE1MGM0NWE1ZjIxODEiIElzc3VlSW5zdGFudD0iMjAxNi0wNi0yN1QyMjoxNTo0NFoiIFZlcnNpb249IjIuMCI+CjxzYW1sOklzc3Vlcj5odHRwczovL2ltcy1pbnQuYnRzZWMuZGV2LnNjaG5laWRlci1lbGVjdHJpYy5jb206NDQzL29wZW5zc288L3NhbWw6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIiB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI3MyYzFiN2IwYWU3MGU1ODc3ZWQyM2QyNTkyYzBiMTUwYzQ1YTVmMjE4MSIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6VHJhbnNmb3JtcyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIi8+CjxkczpEaWdlc3RWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+b1M0QmxGamRIczZBY2dGZ0ZuekZaUE90RVRzPTwvZHM6RGlnZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVWYWx1ZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CmtoR1EycHU1bTJDK3FkTStNTXJXWUtoTWNBTWlNQmVTVkJxMEozZStYcjlwb1AvaFB6VHd3SkhMVVpwaitzVjJnQklBUXF3cUxZbTIKWHpkSnFsVlJuUUw2eGJrbmFHNUY4SDNwTTZNa0IyYUprYzUvMjQ0SGV0SEhmUXpiTzlDejQwWWtXci9QS3plOVdUSEZSMDUyNkliOQo2ajBMVFhoLzkzblZ2Sm9qT1RJPQo8L2RzOlNpZ25hdHVyZVZhbHVlPgo8ZHM6S2V5SW5mbyB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5RGF0YSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CjxkczpYNTA5Q2VydGlmaWNhdGUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgpNSUlDUURDQ0Fha0NCRWVOQjBzd0RRWUpLb1pJaHZjTkFRRUVCUUF3WnpFTE1Ba0dBMVVFQmhNQ1ZWTXhFekFSQmdOVkJBZ1RDa05oCmJHbG1iM0p1YVdFeEZEQVNCZ05WQkFjVEMxTmhiblJoSUVOc1lYSmhNUXd3Q2dZRFZRUUtFd05UZFc0eEVEQU9CZ05WQkFzVEIwOXcKWlc1VFUwOHhEVEFMQmdOVkJBTVRCSFJsYzNRd0hoY05NRGd3TVRFMU1Ua3hPVE01V2hjTk1UZ3dNVEV5TVRreE9UTTVXakJuTVFzdwpDUVlEVlFRR0V3SlZVekVUTUJFR0ExVUVDQk1LUTJGc2FXWnZjbTVwWVRFVU1CSUdBMVVFQnhNTFUyRnVkR0VnUTJ4aGNtRXhEREFLCkJnTlZCQW9UQTFOMWJqRVFNQTRHQTFVRUN4TUhUM0JsYmxOVFR6RU5NQXNHQTFVRUF4TUVkR1Z6ZERDQm56QU5CZ2txaGtpRzl3MEIKQVFFRkFBT0JqUUF3Z1lrQ2dZRUFyU1FjL1U3NUdCMkF0S2hiR1M1cGlpTGttSnpxRXNwNjRyRHhiTUoreERyeWUwRU4vcTFVNU9mKwpSa0RzYU4vaWdrQXZWMWN1WEVnVEw2UmxhZkZQY1VYN1F4RGhaQmhzWUY5cGJ3dE16aTRBNHN1OWhueEloVVJlYkdFbXhLVzlxSk5ZCkpzMFZvNStJZ2p4dUVXbmpublZnSFRzMSttcTVRWVRBN0U2WnlMOENBd0VBQVRBTkJna3Foa2lHOXcwQkFRUUZBQU9CZ1FCM1B3L1UKUXpQS1RQVFlpOXVwYkZYbHJBS013dEZmMk9XNHl2R1dXdmxjd2NOU1pKbVRKOEFSdlZZT01FVk5ic1Q0T0ZjZnUyL1BlWW9BZGlEQQpjR3kvRjJadWo4WEpKcHVRUlNFNlB0UXFCdURFSGpqbU9RSjByVi9yOG1PMVpDdEhSaHBaNXpZUmpoUkM5ZUNiang5VnJGYXgwSkRDCi9GZndXaWdtclcwWTBRPT0KPC9kczpYNTA5Q2VydGlmaWNhdGU+CjwvZHM6WDUwOURhdGE+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+PHNhbWw6U3ViamVjdD4KPHNhbWw6TmFtZUlEIEZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6MS4xOm5hbWVpZC1mb3JtYXQ6dW5zcGVjaWZpZWQiIE5hbWVRdWFsaWZpZXI9Imh0dHBzOi8vaW1zLWludC5idHNlYy5kZXYuc2NobmVpZGVyLWVsZWN0cmljLmNvbTo0NDMvb3BlbnNzbyIgU1BOYW1lUXVhbGlmaWVyPSJodHRwczovL2Rldm1hamJmby1zZWNvbW11bml0aWVzLmNzMi5mb3JjZS5jb20iPjliOTE5NGZlLTdlMjEtNGMwNi1hMjdjLWY5OTc0NjNjNzYyOTwvc2FtbDpOYW1lSUQ+PHNhbWw6U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPgo8c2FtbDpTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBOb3RPbk9yQWZ0ZXI9IjIwMTYtMDYtMjdUMjI6MjU6NDVaIiBSZWNpcGllbnQ9Imh0dHBzOi8vZGV2bWFqYmZvLXNlY29tbXVuaXRpZXMuY3MyLmZvcmNlLmNvbS9wYXJ0bmVycy9sb2dpbj9zbz0wMERSMDAwMDAwMXV3emYiLz48L3NhbWw6U3ViamVjdENvbmZpcm1hdGlvbj4KPC9zYW1sOlN1YmplY3Q+PHNhbWw6Q29uZGl0aW9ucyBOb3RCZWZvcmU9IjIwMTYtMDYtMjdUMjI6MDU6NDVaIiBOb3RPbk9yQWZ0ZXI9IjIwMTYtMDYtMjdUMjI6MjU6NDVaIj4KPHNhbWw6QXVkaWVuY2VSZXN0cmljdGlvbj4KPHNhbWw6QXVkaWVuY2U+aHR0cHM6Ly9kZXZtYWpiZm8tc2Vjb21tdW5pdGllcy5jczIuZm9yY2UuY29tPC9zYW1sOkF1ZGllbmNlPgo8L3NhbWw6QXVkaWVuY2VSZXN0cmljdGlvbj4KPC9zYW1sOkNvbmRpdGlvbnM+CjxzYW1sOkF1dGhuU3RhdGVtZW50IEF1dGhuSW5zdGFudD0iMjAxNi0wNi0yN1QyMjoxNTo0NFoiIFNlc3Npb25JbmRleD0iczIxZDNkNzg1YzQ5MjkwMDUxMDViMmQ2YjU1ODljOWY0MjhhMDE5ZDAxIj48c2FtbDpBdXRobkNvbnRleHQ+PHNhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY+dXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFjOmNsYXNzZXM6UGFzc3dvcmRQcm90ZWN0ZWRUcmFuc3BvcnQ8L3NhbWw6QXV0aG5Db250ZXh0Q2xhc3NSZWY+PC9zYW1sOkF1dGhuQ29udGV4dD48L3NhbWw6QXV0aG5TdGF0ZW1lbnQ+PHNhbWw6QXR0cmlidXRlU3RhdGVtZW50PjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJwcmltYXJ5Q29udGFjdCI+PHNhbWw6QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj50cnVlPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9ImNvdW50cnlDb2RlIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPlVTPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9InN1cm5hbWUiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+Uzwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJnaXZlbk5hbWUiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+QWtpbGE8L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgTmFtZT0icHJlZmVycmVkTGFuZ3VhZ2UiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+ZW48L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgTmFtZT0iZmVkZXJhdGVkSWQiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+OWI5MTk0ZmUtN2UyMS00YzA2LWEyN2MtZjk5NzQ2M2M3NjI5PC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9ImVtYWlsIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPmFraWxhLmRldm1hamJmbzNAeW9wbWFpbC5jb208L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgTmFtZT0iY29tcGFueUlkIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPjYzNzJjODVkLTY1YWEtNGEwMS05MjFmLWNmMjRjMGUxOWMxMTwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJzb3VyY2VTeXN0ZW1JZCI+PHNhbWw6QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5QUk1fYkZPPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PC9zYW1sOkF0dHJpYnV0ZVN0YXRlbWVudD48L3NhbWw6QXNzZXJ0aW9uPjwvc2FtbHA6UmVzcG9uc2U+';
            User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                              CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                              Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                PRMTemporarySamlToken__c = strOriginal,
                              LanguageLocaleKey = 'en_US' ,BypassVR__c= True,BypassTriggers__c='AP_AccountBeforeInsertHandler', ProfileID = profilesId,ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );        
            insert u1;  

            PermissionSet fieloPermissionSet = [Select ID,UserLicenseId from PermissionSet where Name='FieloPRM_SE' Limit 1];
            PermissionSetAssignment newPermissionSetAssignment1 = new PermissionSetAssignment();
            newPermissionSetAssignment1.AssigneeId = u1.id;
            newPermissionSetAssignment1.PermissionSetId = fieloPermissionSet.id;
            Insert newPermissionSetAssignment1; 
            PartnerAcc.Owner = u1;
        }
    }
    
    static testmethod void updateMemberStatus_Test(){
        Test.startTest();
            //System.debug('*** Account id->'+accId);
            Country__c TestCountry = [SELECT Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c WHERE Name   = 'India'];
            Account PartnerAcc = [SELECT Id,Name,Street__c,POBox__c,ZipCode__c,PRMAreaOfFocus__c,PLShowInPartnerLocatorForced__c,PRMCountry__c,Country__c 
                                  FROM Account WHERE Name='TestAccount']; //AND ZipCode__c='12345' AND PRMAreaOfFocus__c='FI1' AND PRMBusinessType__c='FI'
            
            Contact PartnerCon = [SELECT ID,FirstName,LastName,AccountId,JobTitle__c,CorrespLang__c,PRMMobilePhone__c,Country__c,WorkPhone__c,PRMFirstName__c, 
                                PRMLastName__c,PRMEmail__c,PRMPrimaryContact__c FROM Contact Where FirstName = 'Test' AND LastName = 'lastname' AND 
                                PRMEmail__c = 'test.lastName@yopmail.com' AND PRMPrimaryContact__c = true];
                  
            User   u1= [SELECT ID,Username,LastName,alias,CommunityNickName,PRMTemporarySamlToken__c FROM User WHERE CommunityNickName = 'testUser1' AND LastName = 'User11' AND Email = 'testUser@schneider-electric.com' 
                        AND BypassVR__c= True AND ContactID = :PartnerCon.Id];
            system.runas(u1){
                VFC_MyProfileController.updateMemberStatus(PartnerCon.id,'Validated','');
                
                //Setting of UserRegModel
                AP_UserRegModel userinfo = new AP_UserRegModel();
                userinfo.contactID = PartnerCon.id;
                userinfo.firstname = 'test1';
                userinfo.lastname = 'lastname1';
                userinfo.email = 'test1@yopmail.com';
                userinfo.jobFunctionCode = '';
                userinfo.jobTitleCode = '';
                userinfo.businessType1Code = '';
                userinfo.areaOfFocus1Code = '';
                userinfo.businessType2Code = '';
                userinfo.areaOfFocus2Code = '';
                userinfo.businessType3Code = '';
                userinfo.areaOfFocus3Code = '';     
                userinfo.phoneNumber = '080-121212';
                userinfo.phoneType = 'Work Phone';
                
                VFC_MyProfileController.updateMyProfile(userinfo);
                
                
            }
        Test.stopTest();
    }
    static testmethod void updateCompanyInfo_Test(){
        Test.startTest();
            Country__c TestCountry = [SELECT Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c WHERE Name   = 'India'];
            Account PartnerAcc = [SELECT Id,Name,Street__c,POBox__c,ZipCode__c,PRMAreaOfFocus__c,PLShowInPartnerLocatorForced__c,PRMCountry__c,Country__c 
                                    FROM Account WHERE Name='TestAccount'];
            
            Contact PartnerCon = [SELECT ID,FirstName,LastName,AccountId,JobTitle__c,CorrespLang__c,PRMMobilePhone__c,Country__c,WorkPhone__c,PRMFirstName__c, 
                                PRMLastName__c,PRMEmail__c,PRMPrimaryContact__c FROM Contact Where FirstName = 'Test' AND LastName = 'lastname' AND 
                                PRMEmail__c = 'test.lastName@yopmail.com' AND PRMPrimaryContact__c = true];
                  
            User   u1= [SELECT ID,Username,LastName,alias,CommunityNickName,PRMTemporarySamlToken__c FROM User WHERE CommunityNickName = 'testUser1' AND LastName = 'User11' AND Email = 'testUser@schneider-electric.com' 
                        AND BypassVR__c= True AND ContactID = :PartnerCon.Id];
            system.runas(u1){
                AP_UserRegModel companyinfo = new AP_UserRegModel();
                companyinfo.accountID = PartnerAcc.Id;
                companyinfo.companyPhone = '9912121212';
                companyinfo.companyHeaderQuarters = true;
                companyinfo.companyAddress1 = '';
                companyinfo.companyAddress2 = '';
                companyinfo.companyCity = 'Bangalore';
               // companyinfo.companyStateProvinceCode = '';
                //companyinfo.companyCountryCode = '';
                companyinfo.companyZipcode = '';
                companyinfo.companyBusinessTypeCode = '';
                companyinfo.companyAreaOfFocusCode = '';
                companyinfo.prefDistributor1 = 'Distributo1';
                companyinfo.prefDistributor2 = '';
                companyinfo.prefDistributor3 = 'Distributo3';
                companyinfo.prefDistributor4 = '';
                companyinfo.marketServedCode = '';
                companyinfo.employeeSizeCode = '';
                companyinfo.companyCurrencyCode = '';
                companyinfo.annualSalesCode = '';
                companyinfo.seAccountNumbers = '';
                companyinfo.taxId = 'IND-15';           
                
                companyinfo.includeCompanyInfoInPublicSearch = true;
                        
                
                VFC_MyProfileController.updateCompanyInfo(companyinfo,'companyprofile');        
                
                
                //SkipInfo
                companyinfo.skipStep5 = true;
                companyinfo.skipStep6 = false;
                VFC_MyProfileController.updateCompanyInfo(companyinfo,'skipinfo');
                //Complete Profile Update
                VFC_MyProfileController.updateCompanyInfo(companyinfo,'');
                
            /*    AP_UserRegModel plInfo = new AP_UserRegModel();
                plInfo.accountID = PartnerAcc.Id;
                plInfo.plCompanyName = 'PLCompany';
                plInfo.plCompanyCountryCode = '';
                plInfo.plCompanyAddress1 = 'Elnath 9th Floor';
                plInfo.plCompanyAddress2 = '';
                plInfo.plCompanyCity = 'Bangalore';
                plInfo.plCompanyStateProvinceCode = '';
                plInfo.plCompanyZipcode ='';
                plInfo.plmarketServedCode ='';
                plInfo.plBusinessType = '';
                plInfo.companyAreaOfFocusCode ='';
                plInfo.plMainContactEmail ='testpl@yopmail.com';
                plInfo.plMainContactFirstName = 'PlFirstName';
                plInfo.plMainContactLastName = 'PLLastName';
                plInfo.plMainContactphone = '12121212';
                plInfo.plWebsite = 'www.testYopmail.com';
                plInfo.plCompanyDescription = '';
                plInfo.plDatapool ='';
                plInfo.plDomainsOfExpertise ='';
                plInfo.plFax = '';
                plInfo.plShowInPartnerLocator = true;
                plInfo.includeCompanyInfoInPublicSearch =true;
                
                
                VFC_MyProfileController.UpdatePLInfo(plInfo);*/
            }
        Test.stopTest();
    }
    
    static testmethod void updatePLInfo_Test(){
        Test.startTest();
            Country__c TestCountry = [SELECT Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c WHERE Name   = 'India'];
            Account PartnerAcc = [SELECT Id,Name,Street__c,POBox__c,ZipCode__c,PRMAreaOfFocus__c,PLShowInPartnerLocatorForced__c,PRMCountry__c,Country__c 
                                    FROM Account WHERE Name='TestAccount'];
            
            Contact PartnerCon = [SELECT ID,FirstName,LastName,AccountId,JobTitle__c,CorrespLang__c,PRMMobilePhone__c,Country__c,WorkPhone__c,PRMFirstName__c, 
                                PRMLastName__c,PRMEmail__c,PRMPrimaryContact__c FROM Contact Where FirstName = 'Test' AND LastName = 'lastname' AND 
                                PRMEmail__c = 'test.lastName@yopmail.com' AND PRMPrimaryContact__c = true];
                  
            User   u1= [SELECT ID,Username,LastName,alias,CommunityNickName,PRMTemporarySamlToken__c FROM User WHERE CommunityNickName = 'testUser1' AND LastName = 'User11' AND Email = 'testUser@schneider-electric.com' 
                        AND BypassVR__c= True AND ContactID = :PartnerCon.Id];
            system.runas(u1){
                ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
                insert cLC;
                ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
                insert cLChild; 
        
                CountryChannels__c cChannels = new CountryChannels__c();
                cChannels.Active__c = true; 
                cChannels.AllowPrimaryToManage__c = True; 
                cChannels.Channel__c = cLC.Id; 
                cChannels.SubChannel__c = cLChild.id;
                cChannels.Country__c = TestCountry.id; 
                cChannels.CompleteUserRegistrationonFirstLogin__c = true;
                insert cChannels;                               
                
                System.debug('*** Inserting Country Channels ****');
                System.debug('**** cChannels ID ->'+cChannels.id);
                
                //List<CountryChannels__c> lstCountry = [Select id,Country__c,ChannelCode__c,SubChannelCode__c,Active__c from CountryChannels__c where id = :cChannels.id];
                //System.debug('**** Country Channels ->');
                //System.debug('**** Country Channels ->'+lstCountry[0]);
                
                
                //List<CountryChannels__c> lstCountry1 = [SELECT id, AllowPrimaryToManage__c FROM CountryChannels__c WHERE (Country__c = :TestCountry.id AND 
                //ChannelCode__c = :lstCountry[0].ChannelCode__c AND SubChannelCode__c = :lstCountry[0].SubChannelCode__c AND Active__c = TRUE)];
                
                //System.debug('**** Country Channels SQL->'+lstCountry1[0]);
                //System.debug('*** Country Channel ID-> '+cChannels.Country__c +'TestCountry ->'+TestCountry.id);
                
                Assessment__c newAssessment = Utils_TestMethods.createAssessment();
                newAssessment.Name = 'Test Assessment1';
                newAssessment.CountryChannels__c = cChannels.id;
                newAssessment.RecordTypeId = System.label.CLAPR15PRM145;
                newAssessment.Active__c = true;
                insert newAssessment;
                
                OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(newAssessment.Id, 'picklist', 4);
                insert opassq;
                
                OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
                insert opassa;
                
                List<VFC_MyProfileController.QueAnsWrapper> lstAssessment1 = VFC_MyProfileController.prepareAssmentData(newAssessment.Id);
                
                List<VFC_MyProfileController.QueAnsWrapper> lstAssessment = new List<VFC_MyProfileController.QueAnsWrapper>();
                //if(lstAssessment.size() >0)
                //  VFC_MyProfileController.saveAdditionalInfo(lstAssessment);
                
                /*VFC_MyProfileController.QueAnsWrapper aAssessment = VFC_MyProfileController.QueAnsWrapper();
                lstAssessment.add(aAssessment);
                VFC_MyProfileController.saveAdditionalInfo(lstAssessment);
                */
                
                VFC_MyProfileController.QueAnsWrapper aAssessment = new VFC_MyProfileController.QueAnsWrapper();
                
                aAssessment.getlstSelectedAnswer();
                String[] s1;
                aAssessment.setlstSelectedAnswer(s1);
                
                aAssessment.question =  new OpportunityAssessmentQuestion__c();
                aAssessment.question.AnswerType__c = 'Text';
                aAssessment.question.Assessment__c = newAssessment.Id;
                aAssessment.question.Required__c = false;
                aAssessment.question.Sequence__c = 1;
                aAssessment.question.id = opassq.id;        
                
                aAssessment.response = new OpportunityAssessmentResponse__c();
                aAssessment.response.Question__c = opassq.id;
                aAssessment.response.ResponseText__c = 'Test';
                
                lstAssessment.add(aAssessment);
                
                //VFC_MyProfileController.saveAdditionalInfo(lstAssessment);        
                
                Contact PartnerCon2 = new Contact(
                  FirstName='Test2',
                  LastName='lastname2',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  PRMMobilePhone__c='1234567890',
                  Country__c=TestCountry.Id,
                  WorkPhone__c='1234567890',
                  PRMFirstName__c = 'Test2',
                  PRMLastName__c = 'lastname2',
                  PRMEmail__c = 'test2.lastName@yopmail.com'
                  );      
                
                Insert PartnerCon2;  
                VFC_MyProfileController.ReassignPrimaryContact(PartnerCon2.id);
                
               // VFC_MyProfileController.setNewPassword('Hello@1234','prm12345');
               VFC_MyProfileController.changeEmail('newEmail@yopmail.com');
                ApexPages.currentPage().getParameters().put('country','IN');
                
                VFC_MyProfileController myProf = new VFC_MyProfileController(); 
                myProf.companyFormFields = '';
                myProf.myProfileFormFields='';
                myProf.CompleteFormFields='';
                myProf.AssessmentCount = 1;
                myProf.additionalData = '';
                myProf.channelFormFields = '';
                myProf.allowManageLocatorListing = true;
                myProf.SelectedAssmentId = newAssessment.id;
                
                
                AP_UserRegModel plInfo1 = new AP_UserRegModel();
                plInfo1.accountID = PartnerAcc.Id;
                plInfo1.plCompanyAddress1 = 'Elnath 9th Floor';
                plInfo1.plCompanyAddress2 = '';
                plInfo1.plCompanyCity = 'Bangalore';
                plInfo1.plCompanyZipcode ='';
                plInfo1.plmarketServedCode ='';
                plInfo1.plBusinessType = '';
                plInfo1.companyAreaOfFocusCode ='';
                plInfo1.plMainContactEmail ='testpl@yopmail.com';
                plInfo1.plMainContactFirstName = 'PlFirstName';
                plInfo1.plMainContactLastName = 'PLLastName';
                plInfo1.plMainContactphone = '12121212';
                plInfo1.plWebsite = 'www.testYopmail.com';
                plInfo1.plCompanyDescription = '';
                plInfo1.plDatapool ='';
                plInfo1.plDomainsOfExpertise ='';
                plInfo1.plFax = '';
                plInfo1.plShowInPartnerLocator = true;
                plInfo1.includeCompanyInfoInPublicSearch =true;
                               
                VFC_MyProfileController.UpdatePLInfo(plInfo1);  
            }
        Test.stopTest();
    }
    
    static testMethod void VFC_MyProfileController_Test(){
        
                 
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
        for(Profile p : ps)
        {
             profiles.put(p.name, p.id);
        }        
                  
        user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];
        Test.startTest();
        ID  profilesId;
        system.runas(admin){
            Country__c TestCountry = [SELECT Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c WHERE Name   = 'India'];
            Account PartnerAcc = [SELECT Id,Name,Street__c,POBox__c,ZipCode__c,PRMAreaOfFocus__c,PLShowInPartnerLocatorForced__c,PRMCountry__c,Country__c 
                                From Account WHERE Name='TestAccount' ];
                                
            Contact PartnerCon = [SELECT ID FROM Contact Where FirstName = 'Test' AND LastName = 'lastname' AND 
                                PRMEmail__c = 'test.lastName@yopmail.com' AND PRMPrimaryContact__c = true];
                  
            Contact PartnerCon1 = [SELECT ID,FirstName,LastName,AccountId,JobTitle__c,CorrespLang__c,PRMMobilePhone__c,Country__c,WorkPhone__c,PRMFirstName__c, 
                                PRMLastName__c,PRMEmail__c,PRMPrimaryContact__c FROM Contact Where FirstName = 'Test1' AND LastName = 'lastname1' AND 
                                PRMEmail__c = 'test1.lastName@yopmail.com'];
            
            User   u1= [SELECT ID,Username,LastName,alias,CommunityNickName,PRMTemporarySamlToken__c FROM User WHERE CommunityNickName = 'testUser1' AND LastName = 'User11' AND Email = 'testUser@schneider-electric.com' 
                        AND BypassVR__c= True AND ContactID = :PartnerCon.Id];
        
            system.runas(u1){
                User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
                user1.ProgramAdministrator__c = true;
                Country__c country = Utils_TestMethods.createCountry();
                country.countrycode__c ='ASM';
                insert country;               

                PartnerPRogram__c gp1 = new PartnerPRogram__c();
                Assessment__c asm = new Assessment__c();
                user1.country__c = country.countrycode__c;

                gp1 = Utils_TestMethods.createPartnerProgram();
                gp1.TECH_CountriesId__c = country.id;
                gp1.Country__c = country.id;
                gp1.recordtypeid = Label.CLMAY13PRM15;
                gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                gp1.Name = Label.CLAPR14PRM25;
                insert gp1;
                /*
                ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                prg1.Name = Label.CLAPR14PRM25;
                prg1.levelstatus__c = 'active';
                insert prg1;   */     
               
                
                Assessment__c ass = new Assessment__c();
                    ass = Utils_TestMethods.createAssessment();
                    //ass.recordTypeId = prgORFAssessmentRecordType.Id ;
                    ass.AutomaticAssignment__c = true;
                    ass.Active__c = true;
                    ass.Name = 'Sample Assessment';
                    ass.PartnerProgram__c = gp1.id;
                    //ass.ProgramLevel__c = prg1.id;
                insert ass;
                 Account acc = Utils_TestMethods.createAccount();
                   insert acc;
                   Opportunity  opp = Utils_TestMethods.createOpenOpportunity(acc.id);
                   insert opp;
                OpportunityAssessmentQuestion__c  oppAssQue=Utils_TestMethods.createOpportunityAssessmentQuestion(ass.id,'Picklist',1);
                insert oppAssQue;
                OpportunityAssessmentAnswer__c oppAssAns = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue.id);
               insert oppAssAns ;
               OpportunityAssessment__c oppAss=Utils_TestMethods.createOpportunityAssessment(opp.id,ass.id);
               insert oppAss;
               OpportunityAssessmentResponse__c  oppAssRes = Utils_TestMethods.createOpportunityAssessmentResponse(oppAssQue.id,oppAssAns.id,oppAss.id);
               insert oppAssRes ;
                
                VFC_MyProfileController.getPartnerDetails();
                VFC_MyProfileController.getCompanyProfile();
                VFC_MyProfileController.getPartnerMembers();
                
                VFC_MyProfileController.getManageProfileAvailablePicklistValues();
                VFC_MyProfileController.getCompanyProfileAvailablePicklistValues();
                VFC_MyProfileController.getDependentPickList();
                List<integer> lstApplied = new List<integer>{2,3};
                
                //VFC_MyProfileController.getStaticLabelTranslations('Complete Profile');
                VFC_MyProfileController.getContCountryChannelFormFields(lstApplied,'en_US');
                VFC_MyProfileController.getActiveChannels('en_US');
                VFC_MyProfileController.setChannelsToAccountHistory('FI','FI1');
                VFC_MyProfileController.getPrimaryContactDetails();
                VFC_MyProfileController.GetPLInfo();
                VFC_MyProfileController.SetProfileCompletionStatus(PartnerAcc,'CompleteProfile');
                List<VFC_MyProfileController.QueAnsWrapper> qawLst = new List<VFC_MyProfileController.QueAnsWrapper>();
                VFC_MyProfileController.QueAnsWrapper qaw = new VFC_MyProfileController.QueAnsWrapper();
                qaw.question= oppAssQue;
                qaw.selAnswer= oppAssAns.Id;
                qaw.response= oppAssRes;
                qawLst.add(qaw);
                VFC_MyProfileController.saveAdditionalInfo(qawLst);
            }
        }
        Test.stopTest();
    }
}