//*********************************************************************************
//Class Name :  AP_ProcessFeedComment
// Purpose : triggers FeedCommentAfterInsert and FeedCommentAfterUpdate use this class 
// Created by : Shivdeep Gupta - Global Delivery Team
// Date created :  March 02, 2015
// Modified by :
// Date Modified :
// Remarks : For April 15 Release  
// ********************************************************************************


public class AP_ProcessFeedComment{

    BusinessRiskEscalations__c oBRE;
    List<BusinessRiskEscalations__c> listBRE = new List<BusinessRiskEscalations__c>();
    List<ID> bREIDs = new List<ID>();


    
    public void updateFCommentsBREActualUserUdate(List<FeedComment> listFeedComment){
        Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
        // string bREObjectPrefixChar = globalDescribe.get('BusinessRiskEscalations__c').getDescribe().getkeyprefix();
        
        // Label  CLAPR15I2P11 = BusinessRiskEscalations__c
        string bREObjectPrefixChar = globalDescribe.get(System.Label.CLAPR15I2P11).getDescribe().getkeyprefix(); 
        system.debug( 'Test 1'+ bREObjectPrefixChar  );
        
        for(FeedComment oFC : listFeedComment){
            if(oFC.ParentID != null){
                if(string.Valueof(oFC.ParentID).startsWith(bREObjectPrefixChar)){
                    bREIDs.add(oFC.ParentID);
                }   
            }
        }
        
        if(bREIDs.size()>0){
            for(ID oID : bREIDs){
                oBRE = new BusinessRiskEscalations__c(ID = oID);
                oBRE.Actual_User_Lastmodifiedby__c = UserInfo.getUserId();
                oBRE.Actual_User_LastmodifiedDate__c = System.now();
                listBRE.add(oBRE);
            }
            
        }
        
        if(listBRE.size() >0){
            update listBRE;
        }
    }
    
    
    // OCT 2015 Release - Last Updated and Last Updated by fields population on Containment Action
    
    ContainmentAction__c oXA;
    List<ContainmentAction__c> listXA = new List<ContainmentAction__c>();
    List<ID> XAIDs = new List<ID>();
    
    public void updateXA_LastUpdatedActualUserAndDate(List<FeedComment> listFeedComment1){
        Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
        
         // Label  CLOCT15I2P15 = ContainmentAction__c
        string XAObjectPrefixChar = globalDescribe.get(System.Label.CLOCT15I2P15).getDescribe().getkeyprefix(); 
        system.debug( 'Test 1'+ XAObjectPrefixChar  );
        
        for(FeedComment oFC : listFeedComment1){
            if(oFC.ParentID != null){
                if(string.Valueof(oFC.ParentID).startsWith(XAObjectPrefixChar)){
                    XAIDs.add(oFC.ParentID);
                }   
            }
        }
        
        if(XAIDs.size()>0){
            for(ID oID : XAIDs){
                oXA = new ContainmentAction__c(ID = oID);
                oXA.ActualUserLastmodifiedby__c= UserInfo.getUserId();
                oXA.ActualUserLastmodifiedDate__c= System.now();
                listXA.add(oXA);
            }
            
        }
        
        if(listXA.size() >0){
            update listXA;
        }
    }

}