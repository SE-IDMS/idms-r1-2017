global class ContactFeatureContext {
    WebService String[] FeatureIDs;
    WebService String[] ContactIDs;

    //WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
    
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public ContactFeatureContext() {
    }

    /**
    * A constructor based on an Program @param a Program
    */
    public ContactFeatureContext(string[] contactIDs, string[] featureIDs) {
        this.FeatureIDs = featureIDs;
        this.ContactIDs = contactIDs;
    }
}