/*
    Author          : Bhuvana Subramaniyan 
    Description     : Test Class for VFC_AURValidateEditAccess.
*/
@isTest
private class VFC_AURValidateEditAccess_TEST
{
        static testMethod void valEditAccess()
        {
            Country__c ct = Utils_TestMethods.createCountry();
            ct.name = 'India';
            insert ct;
            User sysAdmin;
            sysAdmin = Utils_TestMethods.createStandardUser('testUesr');
            User user;
            system.runas(sysAdmin)
            {
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('user','SE - Sales Standard User');
            user = useruserandpermissionsetrecord.userrecord;
            database.insert(user);
            }
            Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
            insert acc1;
             AccountUpdateRequest__c aur = new AccountUpdateRequest__c(Account__c = acc1.id,
                                                                 AccountName__c = acc1.name,
                                                                 City__c='Test City',
                                                                 Street__c='Test Street',
                                                                 Country__c=ct.id,
                                                                 ZipCode__c='012345',
                                                                 ToBeDeleted__c=true);
                                                                 //Approver__c=UserInfo.getUserId());
            insert aur;
            ApexPages.StandardController controller=new ApexPages.StandardController(aur);
            VFC_AURValidateEditAccess valEditAcc = new VFC_AURValidateEditAccess(controller);
            valEditAcc.redirect();
            
             
            
        }
}