@isTest
public class IdmsUimsReconcUpdateUserBulkTest {
static testMethod void IdmsUimsReconcUpdateUser(){
         
         IdmsUimsReconcUpdateUserBulk idmsUimsReconcUpdateUserBulkcls=new IdmsUimsReconcUpdateUserBulk();   
         user userObj1 = new User(alias = 'user', email='test3127415' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                             timezonesidkey='Europe/London', username='test1user' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                             Company_Phone_Number__c='9986995000',FederationIdentifier ='188b80b1-c622-4f20-8b1d-3ae86af11c4c',IDMS_Registration_Source__c = 'Test',
                             IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                             IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432');
         insert userObj1;
         System.assert(userObj1!=null);
         
         IDMSUserReconciliationBatchHandler__c handler=new IDMSUserReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
         insert handler;  
         IDMSUserReconciliationBatchHandler__c handler1=new IDMSUserReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test1343',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
         insert handler1;  
         List<IDMSUserReconciliationBatchHandler__c> lstHandler=new List<IDMSUserReconciliationBatchHandler__c>();
         lstHandler.add(handler);
         String callFid = 'IDMSAdmin';
         Test.startTest();
         try{
             Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
             Map<String, User> mapUimsEmailIdmsUser = new Map<String, User>();
             mapUimsEmailIdmsUser.put(userObj1.email, userObj1);
             IdmsUimsReconcUpdateUserBulk.updateUimsUsers(lstHandler, 'batchId');
         }catch(exception e) {} 
         Test.stopTest();
         }
}