public class VFC_PRMPortalHomePage {
    
    Public Boolean IsSuperUser {get;set;}
    Public String userName{get;set;}
    string cookieLocation;
    
    public VFC_PRMPortalHomePage(){
        cookie isPrimaryContact;
        Id loggedInUserId = UserInfo.getUserId();
        User u = [SELECT Name,IsPrmSuperUser FROM User WHERE Id=:loggedInUserId];
        If(u.IsPrmSuperUser){
            IsSuperUser = True;
            
        }
        
        
        isPrimaryContact = new Cookie('isPrimaryContact',string.valueOf(u.IsPrmSuperUser),'/', -1, false);
        ApexPages.currentPage().setCookies(new Cookie[]{isPrimaryContact});
            
        system.debug('isPrimaryContact:' + isPrimaryContact);
    }
}