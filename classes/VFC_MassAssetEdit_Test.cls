@isTest
public class VFC_MassAssetEdit_Test{
    
    public static testMethod void TestOne() {
        Test.startTest();
        
       
         //controller.getSelected();
         // Creating Data 
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeid = Label.CLOCT13ACC08;
        insert objAccount;
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = objAccount .id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        Country__c country= Utils_TestMethods.createCountry(); 
        country.CountryCode__c= 'hkh';   
        insert country;   
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        objContact.Country__c= country.id;
        insert objContact;
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCase.SVMXC__Site__c = site1.id;
        insert objCase;
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = objAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        insert ip1;
        
        SVMXC__Case_Line__c cline = new SVMXC__Case_Line__c();        
        cline.SVMXC__Case__c = objCase.id;
        cline.SVMXC__Installed_Product__c = ip1.id;
        
        insert cline ;
        
         PageReference pageRef = Page.VFP_MassAssetEdit;
        Test.setCurrentPage(pageRef);
        List<SVMXC__Case_Line__c> clinelist = new List<SVMXC__Case_Line__c>();
        clinelist.add(cline );
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController((List<Sobject>)clinelist)  ;
        ssc.setSelected((List<Sobject>)clinelist) ;
        ApexPages.currentPage().getParameters().put('retURL', objCase.id);
        ApexPages.currentPage().getParameters().put('id', objCase.id);
        VFC_MassAssetEdit controller = new VFC_MassAssetEdit(ssc );
       controller.doSave();
       controller.doSaveAndDone();
       controller.doCancel();
        
        
        
        Test.stopTest();
    }
}