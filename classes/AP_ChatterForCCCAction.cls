public class AP_ChatterForCCCAction{
    public static void OwnerFollowTheCCCAction(Map<ID,CCCAction__c> CCCActionMap){
        //*********************************************************************************
        // Method Name      : OwnerFollowTheCCCAction
        // Purpose          : To make the ActionOwner to follow the Action Chatter automatically 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 21th June 2012
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Sep - 11 Release
        ///********************************************************************************/
        
        System.Debug('****** AP_ChatterForCCCAction OwnerFollowTheCCCAction Starts****');
        
        set<ID> CCCActionIDSet = CCCActionMap.keySet();
        List<CCCAction__c> CCCActionList = CCCActionMap.values();
               
        List<EntitySubscription> NewChatterFollow = new List<EntitySubscription>();
        
        for(CCCAction__c CurrentCCCAction:CCCActionList ){
            try{ 
                EntitySubscription OwnerChatterFollow = new EntitySubscription();
                OwnerChatterFollow.ParentId = CurrentCCCAction.Id;        
                OwnerChatterFollow.SubscriberId = CurrentCCCAction.OwnerID;  
                NewChatterFollow.add(OwnerChatterFollow);
            }
             catch(Exception e){
            }
        }         
        System.debug(NewChatterFollow);
        try{  
            List<DataBase.SaveResult> SR = DataBase.insert(NewChatterFollow, false );
        }
        catch(Exception e){
             System.debug('----- DML Exception: ' + e.getmessage());
        }
        System.Debug('****** AP_ChatterForCCCAction OwnerFollowTheCCCAction Ends ****');
    }
    public static void CreateFeedOnDueDate(Map<ID,CCCAction__c> TriggerNewMap, Map<ID,CCCAction__c> TriggerOldMap){
        //*********************************************************************************
        // Method Name      : CreateFeedOnDueDate
        // Purpose          : To add a Feed Entry on CCCAction when Duedate is reached 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 21th June 2012
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Sep - 11 Release
        ///********************************************************************************/
        
        List<FeedItem> lstCCCActionFeed = new List<FeedItem >();
        List<CCCAction__c> lstCCCAction = TriggerNewMap.values();
        
        for(CCCAction__c CurrentCCCAction: lstCCCAction ){
            if(TriggerOldMap!=null){
                if((TriggerOldMap.get(CurrentCCCAction.Id).TECH_DueDateReached__c != CurrentCCCAction.TECH_DueDateReached__c) && CurrentCCCAction.TECH_DueDateReached__c ){
                    lstCCCActionFeed.add(new FeedItem (Body=Label.CLSEP12CCC11, ParentId = CurrentCCCAction.Id));
                }
            }
            else{
                if(CurrentCCCAction.TECH_DueDateReached__c){
                    lstCCCActionFeed.add(new FeedItem (Body=Label.CLSEP12CCC11, ParentId = CurrentCCCAction.Id));
                }
            }
        }

        try{  
            List<DataBase.SaveResult> SR = DataBase.insert(lstCCCActionFeed );
        }
        catch(Exception e){
             System.debug('----- DML Exception: ' + e.getmessage());
        }
    }
    /*
    public static void CreateFeedOnActionClosure(Map<ID,CCCAction__c> TriggerNewMap, Map<ID,CCCAction__c> TriggerOldMap){
    */
        //*********************************************************************************
        // Method Name      : CreateFeedOnActionClosure
        // Purpose          : To add a Feed Entry on CCCAction when Closed 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 21th June 2012
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Sep - 11 Release
        ///********************************************************************************/
        
      /*
        List<FeedItem> lstCCCActionFeed = new List<FeedItem >();
        List<CCCAction__c> lstCCCAction = TriggerNewMap.values();
        
        for(CCCAction__c CurrentCCCAction: lstCCCAction ){
            if(TriggerOldMap!=null){
                if((TriggerOldMap.get(CurrentCCCAction.Id).Status__c != CurrentCCCAction.Status__c) && CurrentCCCAction.Status__c == Label.CLSEP12CCC07){
                    lstCCCActionFeed.add(new FeedItem (Body=Label.CLSEP12CCC12, ParentId = CurrentCCCAction.Id));
                }
            }
            else{
                if(CurrentCCCAction.Status__c == Label.CLSEP12CCC07){
                    lstCCCActionFeed.add(new FeedItem (Body=Label.CLSEP12CCC12, ParentId = CurrentCCCAction.Id));
                }
            }
        }

        try{  
            List<DataBase.SaveResult> SR = DataBase.insert(lstCCCActionFeed);
        }
        catch(Exception e){
             System.debug('----- DML Exception: ' + e.getmessage());
        }
    }
    */
    public static void updateCCCActionOwnerId(List<CCCAction__c> lstCCCAction){
        //*********************************************************************************
        // Method Name      : updateCCCActionOwnerId
        // Purpose          : To update the OwnerId field on CCC Action when Action Owner field is not blank 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 21th June 2012
        // Modified by      : Vimal Karunakaran
        // Date Modified    : 06-Aug 2013
        // Remarks          : For Sep - 11 Release, Oct 2013 Release
        ///********************************************************************************/
        
        
        for(CCCAction__c CurrentCCCAction: lstCCCAction ){
            try{
                if(CurrentCCCAction.ActionOwner__c != null){
                    CurrentCCCAction.OwnerId = CurrentCCCAction.ActionOwner__c ;
                }
                if(CurrentCCCAction.CCCTeam__c !=null){
                    List<CustomerCareTeam__c> lstCareTeam = new List<CustomerCareTeam__c>([Select Id, TeamMailBox__c from CustomerCareTeam__c where ID =:CurrentCCCAction.CCCTeam__c]);
                    if(lstCareTeam.size()>0){
                        if(String.isNotBlank(lstCareTeam[0].TeamMailBox__c)){
                            CurrentCCCAction.TECH_TeamMailBox__c =lstCareTeam[0].TeamMailBox__c;
                        }
                    }
                }
            }
            catch(Exception ex){
                System.debug('Error');
            }
        }
    }
}