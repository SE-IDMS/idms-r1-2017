@isTest 

private class FieloPRM_UTILS_FeaturesTest{

    static testMethod void unitTest(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'FieloPRM_AP_BadgeMemberTriggers';
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'ejemplolast';
            member.FieloEE__FirstName__c = 'ejemploFirst';
            member.FieloEE__Street__c = 'callealguna';
            
            insert member;
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level'; 
            insert badge;
            badge.F_PRM_Type__c = 'Program Level'; 
            update badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;
            
            badMem = [SELECT Id, FieloEE__Member2__c, FieloEE__Badge2__c, FieloEE__Badge2__r.F_PRM_Type__c FROM FieloEE__BadgeMember__c WHERE Id = :badMem.Id];
            System.debug('BADGEMEMBER: ' + badMem.FieloEE__Member2__c + ' ' + badMem.FieloEE__Badge2__c + ' ' + badMem.FieloEE__Badge2__r.F_PRM_Type__c);
            
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.F_PRM_FeatureCode__c = 'testt';
            insert feature;
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;
            
            FieloPRM_BadgeFeature__c ft = new FieloPRM_BadgeFeature__c();  
            ft.F_PRM_Feature__c = feature .id;
            ft.F_PRM_Badge__c = badge.id;
            insert ft;
            
            system.debug('con1 '+ con1);
            
         
            list<String> listIds = new list<String>();
            listIds.add(con1.PRMUIMSId__c);
            
            
            FieloPRM_REST_GetFeatByContact rest = new FieloPRM_REST_GetFeatByContact();
            FieloPRM_REST_GetFeatByContact.getFeaturesByContact(listIds );
            FieloPRM_UTILS_Features.getFeaturesByContact(listIds);
        }

    }

    static testMethod void unitTest2(){
        
        FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
        feature.F_PRM_FeatureCode__c= 'testt';
        insert feature;     

        FieloPRM_REST_GetFeatures rest = new FieloPRM_REST_GetFeatures();
        FieloPRM_REST_GetFeatures.getFeatures( );

    }
    
    static testMethod void unitTest3(){
        
       User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);

        
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'TestLaastN';
            member.FieloEE__FirstName__c = 'TestFiirstN';
            member.FieloEE__Street__c = 'test2';
                 
            insert member;
        
            FieloPRM_MemberUpdateHistory__c history = new FieloPRM_MemberUpdateHistory__c ();
            history.F_PRM_Type__c = 'Feature';
            history.F_PRM_Member__c = member.id;
            insert history;
        }
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.addParameter('fromTime', '19900202020202020');
        req.addParameter('toTime', '20160202020220202');
        
        req.requestURI = '/services/apexrest/RestUpdatedFeatureOnContacts';  
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        
        FieloPRM_REST_UpdatedFeatOnContacts callingClass = new FieloPRM_REST_UpdatedFeatOnContacts();
        FieloPRM_REST_UpdatedFeatOnContacts.getUpdatedFeatureOnContacts();
        
    }

    static testMethod void unitTest4(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'ejemplolast';
            member.FieloEE__FirstName__c = 'ejemploFirst';
            member.FieloEE__Street__c = 'callealguna';
            insert member;
            
            Contact conNew = [SELECT Id, PRMUIMSId__c FROM Contact WHERE FieloEE__Member__c =: member.Id];
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level'; 
            insert badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;
            
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.F_PRM_FeatureCode__c = 'testt';
            insert feature;           
           
            FieloPRM_BadgeFeature__c ft = new FieloPRM_BadgeFeature__c();  
            ft.F_PRM_Feature__c = feature.id;
            ft.F_PRM_Badge__c = badge.id;
            insert ft;
            
            list<String> listIds = new list<String>();
            listIds.add(conNew.PRMUIMSId__c);
        
            FieloPRM_REST_GetFeatByContact rest = new FieloPRM_REST_GetFeatByContact();
            FieloPRM_REST_GetFeatByContact.getFeaturesByContact(listIds);
        }

    }
    
}