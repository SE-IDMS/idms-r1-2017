global class VFC_AutoPopulateGMRForValidCR {
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Case caseRecord{get;set;}
    public Integer UnreadEmails{get;set;}
    public String s{get{if(UnreadEmails>1)return 's'; return null;}}
    public boolean renderPoller {get;set;}

    public VFC_AutoPopulateGMRForValidCR(ApexPages.StandardController controller){
        caseRecord=(Case)controller.getRecord();                        
        if(caseRecord.id!=null){            
            UnreadEmails= [SELECT Id, ParentID, Status FROM EmailMessage WHERE ParentID=:caseRecord.Id AND Status ='0'].size();
            pageParameters.put('UnreadEmails',UnreadEmails.format());  
            /* MODIFIED FOR BR-7931 - CHECKS IF THERE ARE ANY UNREAD EMAILS */
            if(UnreadEmails >0)          
                renderPoller = true;
            else
                renderPoller = false;
            /* END OF MODIFICATION */
        }
    }
    /* MODIFIED FOR BR-7931 - CHECKS IF THERE ARE ANY UNREAD EMAILS */
    public void checkConsoleValue()
    {
        renderPoller = false;
    }
    //RERENDERS UNREAD EMAILS SECTION 
    public void CheckReload()
    {
         if(caseRecord.id!=null && renderPoller==true)
         {            
            UnreadEmails= [SELECT Id, ParentID, Status FROM EmailMessage WHERE ParentID=:caseRecord.Id AND Status ='0'].size();
            pageParameters.put('UnreadEmails',UnreadEmails.format());  
            if(UnreadEmails >0)          
                renderPoller = true;
            else
                renderPoller = false;                 
         }
    }
    /* END OF MODIFICATION */
    @RemoteAction
    global static boolean remoteUnReadEmailUpdateOnCase(String strCaseId,String strUnReadEmails){
        Id caseId = strCaseId;
        Case objCurrentCase;
        boolean blnIsUpdateRequired=false;
        boolean blnReturn=false;
        objCurrentCase = new Case(Id=caseId);
        
        Integer intUnReadEmails= Integer.valueOf(strUnReadEmails);
        try{
            objCurrentCase.TECH_NumberOfUnreadEmails__c=intUnReadEmails;
            DataBase.SaveResult objCase_SR = DataBase.Update(objCurrentCase,false);
            if(objCase_SR.isSuccess()) {
                System.debug('TECH_NumberOfUnreadEmails__c Updated successfully');
                blnReturn=true;
            }
            else{
                System.debug('TECH_NumberOfUnreadEmails__c Updated failed');
                blnReturn=false;
            }
        }
        catch(Exception ex) {
            System.debug(ex.getTypeName()+'- '+ ex.getMessage());
            System.debug(ex.getTypeName()+'- Line = '+ ex.getLineNumber());
            System.debug(ex.getTypeName()+'- Stack trace = '+ ex.getStackTraceString());
            blnReturn=false;
        }
        return blnReturn;
    }
    
    /*
    @RemoteAction
    global static boolean remoteGMRFilteredSearch(String strCaseId,String strUnReadEmails, String strProductBU, String strCR,  boolean blnWSCallRequired){
        Id caseId = strCaseId;
        Case objCurrentCase;
        boolean blnIsUpdateRequired=false;
        WS_GMRSearch.resultFilteredDataBean results;
        boolean blnReturn=false;
        
        objCurrentCase = new Case(Id=caseId);
        
        Integer intUnReadEmails= Integer.valueOf(strUnReadEmails);
        
        objCurrentCase.TECH_NumberOfUnreadEmails__c=intUnReadEmails;
        blnIsUpdateRequired=true;
        
        
        if(blnWSCallRequired){
            try{
                WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
                WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
                WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

                GMRConnection.endpoint_x=Label.CL00376;
                GMRConnection.timeout_x=40000;
                GMRConnection.ClientCertName_x=Label.CL00616;

                Pagination.maxLimitePerPage=99;
                Pagination.pageNumber=1;
        
                filters.exactSearch=true;
                filters.onlyCatalogNumber=true;
                
                List<String> lstKeywords = new List<String>();
                lstKeywords.add(String.valueof(strCR));
                filters.keyWordList=lstKeywords;
                
                if(!Test.isRunningTest()){    
                    results=GMRConnection.globalFilteredSearch(filters,Pagination);  
                }
                else{
                    lstKeywords[0]='CR12345';
                    results = Utils_DummyDataForTest.FilteredSearch(lstKeywords[0]);
                }
                if(results!=null && results.gmrFilteredDataBeanList!=null && results.gmrFilteredDataBeanList.size()>0){
                    for(WS_GMRSearch.gmrFilteredDataBean objgmrFilteredDataBean: results.gmrFilteredDataBeanList){
                        if(objgmrFilteredDataBean.commercialReference !=null) { 
                            if(objgmrFilteredDataBean.commercialReference.trim() == lstKeywords[0].trim()) {
                                objCurrentCase.ProductBU__c             = objgmrFilteredDataBean.businessLine.label.UnEscapeXML();
                                objCurrentCase.ProductLine__c           = objgmrFilteredDataBean.productLine.label.UnEscapeXML();
                                objCurrentCase.ProductFamily__c         = objgmrFilteredDataBean.strategicProductFamily.label.UnEscapeXML();
                                objCurrentCase.Family__c                = objgmrFilteredDataBean.family.label.UnEscapeXML();
                                objCurrentCase.SubFamily__c             = objgmrFilteredDataBean.subFamily.label.UnEscapeXML();
                                objCurrentCase.ProductSuccession__c     = objgmrFilteredDataBean.productSuccession.label.UnEscapeXML();
                                objCurrentCase.ProductGroup__c          = objgmrFilteredDataBean.productGroup.label.UnEscapeXML(); 
                                objCurrentCase.CommercialReference__c   = objgmrFilteredDataBean.commercialReference.UnEscapeXML(); 
                                objCurrentCase.ProductDescription__c    = objgmrFilteredDataBean.description.UnEscapeXML();
                                
                                if(objgmrFilteredDataBean.businessLine.value !=null){
                                    objCurrentCase.TECH_GMRCode__c = objgmrFilteredDataBean.businessLine.value;
                                }
                                if(objgmrFilteredDataBean.productLine.value !=null){
                                    objCurrentCase.TECH_GMRCode__c += objgmrFilteredDataBean.productLine.value;
                                }
                                if(objgmrFilteredDataBean.strategicProductFamily.value !=null){
                                    objCurrentCase.TECH_GMRCode__c += objgmrFilteredDataBean.strategicProductFamily.value;
                                }
                                if(objgmrFilteredDataBean.family.value !=null){
                                    objCurrentCase.TECH_GMRCode__c += objgmrFilteredDataBean.family.value;
                                }
                                if(objgmrFilteredDataBean.subFamily.value !=null){
                                    objCurrentCase.TECH_GMRCode__c += objgmrFilteredDataBean.subFamily.value;
                                }
                                if(objgmrFilteredDataBean.productSuccession.value !=null){
                                    objCurrentCase.TECH_GMRCode__c += objgmrFilteredDataBean.productSuccession.value;
                                }
                                if(objgmrFilteredDataBean.productGroup.value !=null){
                                    objCurrentCase.TECH_GMRCode__c += objgmrFilteredDataBean.productGroup.value;
                                }

                                objCurrentCase.CheckedbyGMR__c          = true;
                                objCurrentCase.TECH_LaunchGMR__c        = false;
                                blnIsUpdateRequired                     = true;
                                break;
                            }
                        }
                    }
                }
                else{
                    if(strProductBU!=''){
                        objCurrentCase.ProductBU__c             = null;
                        objCurrentCase.ProductFamily__c         = null;
                        objCurrentCase.ProductLine__c           = null;
                        objCurrentCase.Family__c                = null;
                        objCurrentCase.ProductDescription__c    = null;
                        objCurrentCase.SubFamily__c             = null;
                        objCurrentCase.ProductSuccession__c     = null;
                        objCurrentCase.ProductGroup__c          = null;
                        objCurrentCase.CommercialReference__c   = null;
                        objCurrentCase.TECH_GMRCode__c          = null;
                        objCurrentCase.CheckedbyGMR__c          = true;
                        objCurrentCase.TECH_LaunchGMR__c        = false;
                        blnIsUpdateRequired                     = true;
                    }
                }
            }
            catch(Exception e) { 
                objCurrentCase.CheckedbyGMR__c = true;
                objCurrentCase.TECH_LaunchGMR__c = false;
            }
        }
        
        if(blnIsUpdateRequired) {
            objCurrentCase.CheckedbyGMR__c      = true;
            objCurrentCase.TECH_LaunchGMR__c    = false;
            DataBase.SaveResult objCase_SR = DataBase.Update(objCurrentCase,false);

            if(!objCase_SR.isSuccess()) {
                DataBase.Error DMLerror = objCase_SR.getErrors()[0];
                try{
                    Case errorCase = new Case(Id=caseId);
                    errorCase.CheckedbyGMR__c = true;
                    errorCase.TECH_LaunchGMR__c = false;
                    errorCase.TECH_ByPassVRForGMRPopulate__c=true;
                    DataBase.SaveResult errorCase_SR = DataBase.Update(errorCase,false);
                    return false;
                }
                catch(Exception ex) { 
                    System.debug(ex.getTypeName()+'- '+ ex.getMessage());
                    System.debug(ex.getTypeName()+'- Line = '+ ex.getLineNumber());
                    System.debug(ex.getTypeName()+'- Stack trace = '+ ex.getStackTraceString());
                    return false;
                }
            }
            blnReturn=true;
        }
        else{
            blnReturn=false;
        }
        return blnReturn;  
    }
    */
}