global without sharing class VFC_RegistrationComponentController {
    global String UserLanguage {get; set;}
    global String FormMode {get; set;}
    global String PhoneFormatValue {get; set;}
    global String FName {get; set;}
    global String LName {get; set;}
}