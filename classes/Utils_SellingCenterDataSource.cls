public with sharing class Utils_SellingCenterDataSource extends Utils_DataSource{
    
//*********************************************************************************
// Controller Name  : Utils_SellingCenterDataSource
// Purpose          : Data Source for Search Selling Center
// Created by       : Hari Krishna  Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For Sales April 2012 Release
///********************************************************************************/
 
     static String query1 = 'Select ID ,Name ,Country__c ,Country__r.Name,toLabel( Business__c )  From OPP_SellingCenter__c';
          //    Main Search Method
     
     Static String WhereClause ='';
     public override List<SelectOption> getColumns()
     {
         List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Field1__c',sObjectType.OPP_SellingCenter__c.fields.Name.getLabel())); // First Column
        Columns.add(new SelectOption('Field2__c',sObjectType.OPP_SellingCenter__c.fields.Country__c.getLabel())); 
        Columns.add(new SelectOption('Field3__c',sObjectType.OPP_SellingCenter__c.fields.Business__c.getLabel()));   
        
        return Columns;
        return null;
     }
     
     public override Result Search(List<String> keyWords, List<String> filters)
     {
        //    Initializing Variables      
        String searchText; 
        String business;
        String country;
        List<sObject> results = New List<sObject>();
        List<sObject> tempResults = New List<sObject>();
        Utils_Sorter sorter = new Utils_Sorter();
        List<Id> compId = New List<Id>();
        Utils_DataSource.Result returnedResult = new Utils_DataSource.Result();   
        returnedResult.recordList = new List<DataTemplate__c>();
        Map<Object, String> countries = New Map<Object, String>();
        Map<String,sObject> resultData = New Map<String,sObject>();
        Map<Object, String> pickList = New Map<Object, String>();
        Map<Integer,DataTemplate__c> resultMap = new Map<Integer,DataTemplate__c>();
        List<Object> countryId = New List<Object>();
        List<sObject> resultset = New List<sObject>();
         String final_query;
  

        if(!(filters[0].equalsIgnoreCase(Label.CL00355)))
            business = filters[0];
        if(!(filters[1].equalsIgnoreCase(Label.CL00355)))
            country = filters[1];

        if(keywords != null && keywords.size()>0)
            searchText = keyWords[0];
        else 
            searchText = '';
            
                 
        if(searchText != NULL && searchText!= '')
        {
            if(WhereClause.length()>0){
                
                WhereClause +=' AND Name LIKE \'%'+searchText+'%\' ';
            }
            else{
                
                 WhereClause +='  Name LIKE \'%'+searchText+'%\' ';
            }
        }
        if(business != NULL && business!= '')
        {
            if(WhereClause.length()>0){
                
                WhereClause +=' AND Business__c =  \''+business+'\' ';
            }
            else{
                
                 WhereClause +='  Business__c = \''+business+'\' ';
            }
        }
        if(country  != null  ){
            
           
            
            if(WhereClause.length()>0){
               
                WhereClause +=' AND Country__c = \''+country +'\' ';
            }
            else{
                 WhereClause +='  Country__c = \''+country +'\' ';
            }
            
        }
       final_query=query1 +' where '+WhereClause+' And Email__c != null';
         System.debug('#### final query: ' +  final_query );
          Utils_SDF_Methodology.log(' query: ',final_query); 
         results = Database.query(final_query);
        
        System.debug('#### results: ' +  results );
        
        
        //    Start of processing the Saerch Result to prepare the final result set
        tempResults.clear();
        for (sObject obj : results)
        { 
            OPP_SellingCenter__c records = (OPP_SellingCenter__c)obj;
            DataTemplate__c solcc = New DataTemplate__c ();
            solcc.put('Field1__c',records.Name);
            solcc.put('Field2__c',records.Country__r.Name);
            solcc.put('Field3__c',records.Business__c);
            solcc.put('Field4__c',records.Id);
            
            
            tempResults.add(solcc);              
        }
        System.debug('#### Results without Country Filter: ' +  tempResults);
        returnedResult.recordList.clear();
        
        returnedResult.recordList.addAll(tempResults);
        
        //    End of Processing and preparation of the final result Set
        
        returnedResult.recordList = (List<sObject>)sorter.getSortedList(returnedResult.recordList, 'Field1__c', true);
        System.debug('#### Final Result :' + returnedResult);
        
        resultset.addAll(returnedResult.recordList);
        returnedResult.numberOfRecord = resultset.size();

        if(returnedResult.numberOfRecord >100)
        {
            returnedResult.recordList.clear();
            for(Integer count=0; count< 100; count++)
            {
                returnedResult.recordList.add(resultset[count]);      
            }
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return returnedResult;
     }   
}