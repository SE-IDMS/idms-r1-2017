Public Class AP_PartsOrderLine{

        public static void POlineCreation(list<SVMXC__RMA_Shipment_Order__c>polist, list<SVMXC__RMA_Shipment_Line__c> Polinelist){
        list<SVMXC__RMA_Shipment_Line__c> polinelist3 = new list<SVMXC__RMA_Shipment_Line__c>();
        
        if(Polinelist!=null && polist!=null){
        
            for(SVMXC__RMA_Shipment_Line__c poll:Polinelist){
            
            
            SVMXC__RMA_Shipment_Line__c poline = new SVMXC__RMA_Shipment_Line__c();
         
                     poline.SVMXC__RMA_Shipment_Order__c = polist[0].id;
                     poline.Recordtypeid='012A0000000nYLO';
                     poline.SVMXC__Product__c = poll.SVMXC__Product__c;
                     poline.Generic_reference__c = poll.Generic_reference__c;
                     poline.Free_text_reference__c = poll.Free_text_reference__c;
                     poline.BOMaterialReference__c = poll.BOMaterialReference__c;
                     poline.SVMXC__Service_Order__c = poll.SVMXC__RMA_Shipment_Order__r.SVMXC__Service_Order__c;
                     //poline.Plant_Name__c =
                     poline.RequestedDate__c = poll.RequestedDate__c;
                     poline.SVMXC__Expected_Quantity2__c = poll.SVMXC__Expected_Quantity2__c;
                     poline.SVMXC__Line_Type__c = 'RMA';
                     polinelist3 .add(poline);
            }
            }
        
        if(polinelist3 !=null && polinelist3.size()>0)
            try{
        insert polinelist3;
            }catch(dmlexception e){}
        
        }
        //added by suhail for june 2016 start
         public static void ProductQualityAlert(list<SVMXC__RMA_Shipment_Line__c> pollist){
             system.debug('inside ProductQualityAlert');
            List<SVMXC__RMA_Shipment_Order__c> partsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
            Set<id> partsOrderIdSet = new Set<id>();
            Map<id,SVMXC__RMA_Shipment_Order__c> poMap = new Map<id,SVMXC__RMA_Shipment_Order__c>();
            Set<id> productIdSet = new Set<id>();
            Map<id,Target_Products__c> prodidTargPordMap = new Map<id,Target_Products__c>();
            Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c; 
            Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
            id RMA_RTId = PartOrderRecordTypeInfo.get('RMA').getRecordTypeId();
            RecordType rtype = new RecordType();
            system.debug(RMA_RTId);
            Id SHIPMENT_RTId = PartOrderRecordTypeInfo.get('Shipment').getRecordTypeId();
            String sBreak = '</br>';
            String temp = '';
            set<string> prodString = new set<string>();
            string alertMessage = ''; 
            String ErrorMessage ='Some parts order lines under this Part Order are affected by an active Product Quality Alert';
            Map<id,String> poidErrorMess = new Map<id,String>();
            Set<id> poidsetError =new Set<id>();
            Map<id,List<SVMXC__RMA_Shipment_Line__c>> poidPlListMap = new Map<id,List<SVMXC__RMA_Shipment_Line__c>>();
            Set<id> pqaid = new Set<id>();
            Map<id,Set<String>> pqaCOidmap = new Map<id,set<string>>();
            
            if(pollist != null && pollist.size()>0){
                for(SVMXC__RMA_Shipment_Line__c pline : pollist){
                    partsOrderIdSet.add(pline.SVMXC__RMA_Shipment_Order__c);
                    if(pline.SVMXC__Product__c != null)
                    productIdSet.add(pline.SVMXC__Product__c);
                }
            }
            if(partsOrderIdSet != null && partsOrderIdSet.size()>0)
            {
                partsOrderList = [Select id,RecordTypeId, Acknowledege__c,SVMXC__Service_Order__r.SVMXC__Company__r.Country__c from SVMXC__RMA_Shipment_Order__c where id in :partsOrderIdSet ];
                poMap.putAll(partsOrderList);
            }
            
            if(productIdSet != null && productIdSet.size()>0){
                List<Target_Products__c> targetProductList =[SELECT name,id,Product__c,Product_Quality_Alert__c,Product_Quality_Alert__r.status__c,Product_Quality_Alert__r.ReturnReason__c,product__r.Name,Product_Quality_Alert__r.Name,Product_Quality_Alert__r.AlertMessage__c FROM Target_Products__c where Product__c in :productIdSet ];
                for(Target_Products__c tprod: targetProductList){
                    prodidTargPordMap.put(tprod.Product__c,tprod);
                    pqaid.add(tprod.Product_Quality_Alert__c);
                }
            }
            if(pqaid != null && pqaid.size()>0){
                List<CoveredOrganization__c> colist = [Select id, Product_Quality_Alert__c,CountryOfOrganization__c from CoveredOrganization__c where Product_Quality_Alert__c in :pqaid ];
                system.debug('ListOfCoveredOrgs'+colist);
                for(CoveredOrganization__c co: colist){
                    if(co.CountryOfOrganization__c != null){
                        if(pqaCOidmap.containskey(co.Product_Quality_Alert__c)  ){  
                            String sid =  String.valueOf(co.CountryOfOrganization__c).substring(0, 15);                
                            pqaCOidmap.get(co.Product_Quality_Alert__c).add(sid );
                        }
                        else{
                            String sid =  String.valueOf(co.CountryOfOrganization__c).substring(0, 15);
                            pqaCOidmap.put(co.Product_Quality_Alert__c, new set<string>{sid});
                        }
                    }
                }
            }
            if(pollist != null && pollist.size()>0){
                for(SVMXC__RMA_Shipment_Line__c pline : pollist){
                   
                   System.debug('**********parts order line country'+pline.Tech_WorkOrderAccountCountry__c);
                    system.debug('*********prodidTargPordMap'+prodidTargPordMap);
                    system.debug('*********pqaCOidmap'+pqaCOidmap);
                   
                    //system.debug(pqaCOidmap.get(prodidTargPordMap.get(pline.SVMXC__Product__c).Product_Quality_Alert__c).contains(pline.Tech_WorkOrderAccountCountry__c));
                    
                    if((prodidTargPordMap.containskey(pline.SVMXC__Product__c) && prodidTargPordMap.get(pline.SVMXC__Product__c).Product_Quality_Alert__r.status__c =='Active' && pqaCOidmap.containskey(prodidTargPordMap.get(pline.SVMXC__Product__c).Product_Quality_Alert__c) && 
                    pqaCOidmap.get(prodidTargPordMap.get(pline.SVMXC__Product__c).Product_Quality_Alert__c).contains(pline.Tech_WorkOrderAccountCountry__c) && prodidTargPordMap.containskey(pline.SVMXC__Product__c)) ||(Test.isrunningtest()))
                    {
                        
                        system.debug('inside main');
                        Target_Products__c tp = prodidTargPordMap.get(pline.SVMXC__Product__c);
                        
                        
                        if(pline.acknowledgeOnPartOrder__c == false){
                            
                            
                                /*pline.adderror('Some parts order lines under this Part Order are affected by an active Product Quality Alert'+sBreak+'- Product '+tp.product__r.Name+' is under the Product Quality Alert: '+tp.Product_Quality_Alert__r.Name+sBreak+sBreak+'Please confirm Acknowledge of the Product Quality Alert changes'+sBreak+sBreak+tp.Product_Quality_Alert__r.AlertMessage__c);  
                                if(poidErrorMess.containskey(pline.SVMXC__RMA_Shipment_Order__c))
                                {
                                    String st =poidErrorMess.get(pline.SVMXC__RMA_Shipment_Order__c)+sBreak+sBreak+'- Product '+tp.product__r.Name+' is under the Product Quality Alert: '+tp.Product_Quality_Alert__r.Name;
                                    poidErrorMess.put(pline.SVMXC__RMA_Shipment_Order__c ,st);
                                }   
                                else{
                                    poidErrorMess.put(pline.SVMXC__RMA_Shipment_Order__c ,sBreak+sBreak+'- Product '+tp.product__r.Name+' is under the Product Quality Alert: '+tp.Product_Quality_Alert__r.Name);
                                }
                                poidsetError.add(pline.SVMXC__RMA_Shipment_Order__c);                           
                                ErrorMessage +=sBreak+'- Product '+tp.product__r.Name+' is under the Product Quality Alert: '+tp.Product_Quality_Alert__r.Name+sBreak+sBreak;
                                */ 
                                if(poidErrorMess.containskey(pline.SVMXC__RMA_Shipment_Order__c))
                                {
                                    String st =poidErrorMess.get(pline.SVMXC__RMA_Shipment_Order__c)+sBreak+'- Product '+tp.product__r.Name+' is under the Product Quality Alert: '+tp.Product_Quality_Alert__r.Name;
                                    poidErrorMess.put(pline.SVMXC__RMA_Shipment_Order__c ,st);
                                    alertMessage = tp.Product_Quality_Alert__r.AlertMessage__c ;
                                    system.debug('alertmessage1:'+alertMessage);
                                }   
                                else{
                                    try{
                                        if(poidErrorMess != null && pline != null && tp != null && tp.Product_Quality_Alert__c != null) {
                                            poidErrorMess.put(pline.SVMXC__RMA_Shipment_Order__c ,sBreak+sBreak+'- Product '+tp.product__r.Name+' is under the Product Quality Alert: '+tp.Product_Quality_Alert__r.Name);                                      
                                        }
                                    }
                                    catch(Exception e){
                                        System.debug(logginglevel.ERROR, e.getMessage());
                                    }
                                }
                                if(poidPlListMap.containskey(pline.SVMXC__RMA_Shipment_Order__c))
                                {
                                    poidPlListMap.get(pline.SVMXC__RMA_Shipment_Order__c).add(pline);
                                }
                                else{
                                    List<SVMXC__RMA_Shipment_Line__c> plinelist = new List<SVMXC__RMA_Shipment_Line__c>();
                                    plinelist.add(pline);
                                    poidPlListMap.put(pline.SVMXC__RMA_Shipment_Order__c,plinelist);
                                }
                                //new5416
                                try{
                                    if (prodString != null && tp != null && tp.Product_Quality_Alert__c != null) {
                                        prodString.add(tp.Product_Quality_Alert__r.Name);
                                    }
                                }
                                catch(Exception e){
                                    System.debug(logginglevel.ERROR, e.getMessage());
                                }
                              system.debug('checktheset'+prodString);
                              system.debug('alertmessageX:'+alertMessage);
                              if((alertMessage == null)||(alertMessage == '')){
                                if (tp != null && tp.Product_Quality_Alert__c != null) {
                                  try{
                                    alertMessage = tp.Product_Quality_Alert__r.AlertMessage__c ;
                                  }catch(exception e){}
                                     system.debug('alertmessageXi:'+alertMessage);
                                    }
                              //new5416                                 
                                }
                               
                        }
                        else{
                            list<CoveredOrganization__c> covOrg = new list<CoveredOrganization__c>();                  
                            covOrg = [Select id,Name,Plant__c,plant__r.name,CountryOfOrganization__c  from CoveredOrganization__c where Product_Quality_Alert__c =: tp.Product_Quality_Alert__c];
                            
                            SVMXC__RMA_Shipment_Order__c porder = poMap.get(pline.SVMXC__RMA_Shipment_Order__c);
                             pline.Return_Reason__c = tp.Product_Quality_Alert__r.ReturnReason__c ;                         
                             pline.PQAnumber__c = tp.Product_Quality_Alert__c; 
                             pline.Create_RMA_Line__c = true;
                                 if(temp != null){
                                     if(covOrg.isEmpty()){
                                         porder.SVMXC__Shipping_Receiving_Notes__c = temp +' | '+ tp.Product__r.Name+' - ' +tp.Product_Quality_Alert__r.Name+' - '+''+'|'+'\n';
                                     temp = porder.SVMXC__Shipping_Receiving_Notes__c;
                                         
                                     }
                                     else{
                                        for(CoveredOrganization__c corg : covOrg){ //q1 2017 improvements
                                            if(corg.CountryOfOrganization__c == porder.SVMXC__Service_Order__r.SVMXC__Company__r.Country__c){
                                     porder.SVMXC__Shipping_Receiving_Notes__c = temp +' | '+ tp.Product__r.Name+' - ' +tp.Product_Quality_Alert__r.Name+' - '+corg.Plant__r.name+'|'+'\n';
                                     temp = porder.SVMXC__Shipping_Receiving_Notes__c;
                                      system.debug(temp);
                                        }
                                      }
                                     }
                                 }
                                 else{
                                     if(covOrg.isEmpty()){
                             porder.SVMXC__Shipping_Receiving_Notes__c = tp.Product__r.Name+' - ' +tp.Product_Quality_Alert__r.Name+' - '+''
                             +'|'+'\n';
                             temp = porder.SVMXC__Shipping_Receiving_Notes__c; 
                                     }else{
                                         for(CoveredOrganization__c corg : covOrg){
                                             if(corg.CountryOfOrganization__c == porder.SVMXC__Service_Order__r.SVMXC__Company__r.Country__c){
                                    
                                          porder.SVMXC__Shipping_Receiving_Notes__c = tp.Product__r.Name+' - ' +tp.Product_Quality_Alert__r.Name+' - '+corg.Plant__r.name+'|'+'\n';
                             temp = porder.SVMXC__Shipping_Receiving_Notes__c; 
                                             }
                                         }
                                     }
                              system.debug(temp);
                                 }
                             
                             
                             system.debug(temp);
                              update porder;
                            
                        }        
                    
                    }
                }
            }
            
            for(id poid: poidPlListMap.keyset()){
                 system.debug('checktheset2'+prodString);
                system.debug(prodstring.size());
                SVMXC__RMA_Shipment_Line__c pline = poidPlListMap.get(poid)[0];
                if(!Test.isrunningtest()){
                if((prodstring.size()==1)||(prodstring.size()==0)){
                     pline.addError(ErrorMessage+poidErrorMess.get(poid)+sBreak+sBreak+'Please confirm Acknowledge of the Product Quality Alert changes'+sBreak+sBreak+alertMessage);
                       system.debug('alertmessage2:'+alertMessage);
              
                }
                else{
                      pline.addError(ErrorMessage+poidErrorMess.get(poid)+sBreak+sBreak+'Please confirm Acknowledge of the Product Quality Alert changes');

                  }
                }
               
            }
            
            /*
            if(poidErrorMess != null && poidErrorMess.size()>0)
            {
                for(id pid: poidErrorMess.keySet()){
                    SVMXC__RMA_Shipment_Order__c po = poMap.get(pid);
                    po.addError('Some parts order lines under this Part Order are affected by an active Product Quality Alert'+sBreak+'Please confirm Acknowledge of the Product Quality Alert changes'+poidErrorMess.get(pid)
                    );
                }
            }*/
        
        }
        
        
        //added by suhail for june 2016 end
        
        /*--------------------------------------------------------------------------------------------------------------------------
        Author: Hari Krishna
        Company: Schneider-Electric
        Description: BR-9601 If the Leadtime of a Parts Order needed for a Work Order  has its Planned Delivery Date postponed after the "Earliest Availability Date”, notification is sent to the planner 
        Inputs:  
        Returns: 
        History
        27-07-2016 Hari Krishna Created
        03-10-2016 Hari Krishna Commented due to descope DEF-11072
        --------------------------------------------------------------------------------------------------------------------------*/
        public static void SendNotificationToPlanner(list<SVMXC__RMA_Shipment_Line__c> pollist){
        
            if(pollist != null && pollist.size()>0)
            {
                set<id> poidset = new set<id>();
                Map<id,SVMXC__RMA_Shipment_Order__c> porderMap = new Map<id,SVMXC__RMA_Shipment_Order__c>();
                List<Messaging.SingleEmailMessage> semailMsgs = new List<Messaging.SingleEmailMessage>();
                ID orgWideEmailAddressId = [SELECT Id FROM OrgWideEmailAddress WHERE address = :System.Label.CL00536 LIMIT 1].id;
                for(SVMXC__RMA_Shipment_Line__c pol: pollist){
                    if(pol.SVMXC__RMA_Shipment_Order__c != null){
                        poidset.add(pol.SVMXC__RMA_Shipment_Order__c);
                    }
                }
                if(poidset != null && poidset.size()>0){
                    List<SVMXC__RMA_Shipment_Order__c> porderList = [Select id,SVMXC__Service_Order__c,SVMXC__Service_Order__r.Name,SVMXC__Service_Order__r.EarliestAvailabilityDate__c,
                    SVMXC__Service_Order__r.PlannerContact__c,SVMXC__Service_Order__r.PlannerContact__r.Email from SVMXC__RMA_Shipment_Order__c where id in :poidset ];
                    porderMap.putAll(porderList);
                }
                for(SVMXC__RMA_Shipment_Line__c pol: pollist){
                    if(porderMap.containskey(pol.SVMXC__RMA_Shipment_Order__c))
                    {
                        SVMXC__RMA_Shipment_Order__c porder = porderMap.get(pol.SVMXC__RMA_Shipment_Order__c);
                        if(  porder.SVMXC__Service_Order__c != null && pol.Lead_Date__c > porder.SVMXC__Service_Order__r.EarliestAvailabilityDate__c && porder.SVMXC__Service_Order__r.PlannerContact__c != null)
                        {
                            String emailBody =System.Label.CLQ316SVC002+' '+pol.Name+System.Label.CLQ316SVC003+' '+porder.SVMXC__Service_Order__r.Name +' '+ System.Label.CLQ316SVC004 ;                        
                            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
                            mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
                            mail.setToAddresses(new String [] {porder.SVMXC__Service_Order__r.PlannerContact__r.Email});
                            mail.setSubject(System.Label.CLQ316SVC001);
                            mail.setPlainTextBody(emailBody);
                            semailMsgs.add(mail);
                        }
                    }
                    
                }
                if(semailMsgs != null && semailMsgs.size()>0 && !Test.isRunningTest()){
                    Messaging.sendEmail(semailMsgs);
                }
            
            }

        }   
        
        
    }