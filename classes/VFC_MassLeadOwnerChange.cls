/*
05-July-2012    Srinivas Nallapati    Sep Mkt 12 Release    Br-1894 : Ablility to mass assign leads to external agents
*/
public with sharing class VFC_MassLeadOwnerChange{
    
    public boolean isLeadsSelected {get;set;}{isLeadsSelected = true;}
    set<id> selectedLeadids = new set<id>();
    
    public string selectedUserId {get;set;}
    public String selectedUserName {get;set;}
    
    public list<Lead> lstLeads {get;set;}
    
    public String userSearchKey {get;set;}
    
    public Boolean hasUpdateError {get;set;} {hasUpdateError=false;}
    
    public Lead led {get;set;}{led = new Lead();} 
    public List<LeadWrapper> lstResultLeadWraps {get;set;} {lstResultLeadWraps = new List<LeadWrapper>();}
    
    public VFC_MassLeadOwnerChange(ApexPages.StandardSetController controller) {
        lstLeads = new list<Lead>();
        lstLeads = controller.getSelected();
        if(lstLeads.size() > 0)
        {
            Map<id,Lead> tmap = new map<id,Lead>(lstLeads );
            lstLeads = [select name, status, Ownerid from lead where id in :tmap.keyset()];
        }else{
            isLeadsSelected = false; // No leads selected from list view
            
        }
   
    }//End of constructor
    
    public List<User> lstUser {get;set;} {lstUser = new List<user>();}
    public boolean showResults {get;set;}{showResults =false;} 
    //Method to search user
    public void searchUsers()
    {    
        showResults = true;
        system.debug('#######'+userSearchKey);
        User loggedInUser = new User();
        loggedInUser = [select id, ContactId, Contact.AccountId,Contact.Account.OwnerId, Name, ProfileId from User where id= :UserInfo.getUserId()];
        
        if(loggedInUser.ContactId != null)
        {
            lstUser = [select id, firstName,  LastName, ContactId, email,Tech_Email__C, name from user where name like :'%'+userSearchKey +'%' and (Contact.Accountid = :loggedInUser.Contact.AccountId OR id= :loggedInUser.Contact.Account.OwnerId)limit 20];
        }
        else
        {
            lstUser = [select id, firstName,  LastName, ContactId, email,Tech_Email__C, name from user where name like :'%'+userSearchKey +'%' limit 20]; 
        }
    }
    //End of method searchuser
  
    //Method to change the owner of the leads to the selected user
    public PageReference changeLeadOwner()
    {
        if(selectedUserId  == null || selectedUserId=='' || selectedUserId== ' ')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select new lead owner'));
            return null;
        }
        else
        {   
            map<id,Lead> mapUnchangedLeads = new map<id,Lead>();
            for(lead ld :lstLeads)
            {   
                mapUnchangedLeads.put(ld.id, ld.clone(true));
                ld.ownerId = selectedUserId ;
            }
            
            Database.Saveresult[] lstResult =  Database.update(lstLeads, false);
            
            for(Integer i = 0; i<lstLeads.size() ; i++){
          
                Database.Saveresult res = lstResult[i];
                  if(!res.isSuccess()){
                    LeadWrapper tmpLW =  new LeadWrapper(mapUnchangedLeads.get(lstLeads[i].id));
                    for(Database.Error er : res.getErrors()){
                      tmpLW.errorMessage = er.getMessage();
                    }
                    lstResultLeadWraps.add(tmpLW);
                    hasUpdateError = true;
                  }
            }
            
            lstLeads.clear();
            
            if(hasUpdateError)
                return Page.VFP_MassLeadOwnerChange_Result;
            else
                return new PageReference('/00Q/o');
            
        }//end of else
        

    }//End of chnageLeadOwner()
    
    public class LeadWrapper{
        public boolean isSelected {get;set;} {isSelected = false;}
        public Lead Ld {get;set;}
        public String errorMessage {get;set;}
        
        public LeadWrapper(Lead ld){
            this.Ld = ld;
        }
    }
}