@isTest
public class TestSubscriptionAfterUpdate {
    static testMethod void test(){
        
        Set<String> subSet = new Set<String>();      
        
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }

        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=country.Id, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');
        insert acc;

        Contact con = Utils_TestMethods.createContact(acc.Id, 'Contact');
        con.Country__c = country.Id;
        con.PRMUIMSID__c = 'Federated Id';
        insert con;

        Zuora__CustomerAccount__c ba = testObjectCreator.CreateBillingAccount('BillAcc_1','Acc_1', acc.Id);


        zqu__Quote__c zq = new zqu__Quote__c();
        zq.zqu__Account__c = acc.Id;
        zq.zqu__ZuoraAccountID__c = '2c92c0f84b07957f014b11cbaac6685e';
        zq.zqu__ZuoraSubscriptionID__c = '2c92c0f84b07957f014b2bbde6254c52';
        zq.zqu__Status__c = 'New';
        zq.zqu__ZuoraParentBillingAccountId__c = ba.Id;
        zq.Primary_User__c = con.Id;
        zq.zqu__InvoiceOwnerId__c = ba.Id;
        zq.zqu__InvoiceOwnerName__c = ba.Name;
        zq.zqu__SubscriptionType__c = 'Test';
        zq.zqu__ZuoraSubscriptionID__c = '123';
        zq.Type__c = 'Regular';
        insert zq;


        Zuora__Subscription__c subTest = new Zuora__Subscription__c();
        subTest.Zuora__Account__c = acc.Id;
        subTest.Zuora__CustomerAccount__c = ba.Id;
        subTest.Zuora__Zuora_Id__c = '123';
        insert subTest;

        Zuora__Subscription__c sub = testObjectCreator.CreateSubscription('Sub_1',acc.Id, ba.Id);

        sub.Zuora__ServiceActivationDate__c = date.today();

        try{
            update sub;
        }catch(Exception e1){
            subSet.add(sub.Id);
            try{
                Handler_SubscriptionAfterUpdate.updateSubsDate(subSet);
            }catch(Exception e2){
            }
        }

    }
}