//*********************************************************************************
//Class Name : Schedule_OSAProblemsAlert
// Purpose : A batch class to send alerts to Problem Stakeholders for Problems with 
//           Severity = 10 and any other Status other than Closed or Cancelled and 
//           also that have not been updated in the last 30 days
// Created by : Uttara P R - Global Delivery Team
// Date created : May 14, 2015
// Date modified : Jan 12. 2016
// Remarks : For Oct 15 Release, Mar 16 Release  
//********************************************************************************/ 

global class Schedule_OSAProblemsAlert implements Database.Batchable<sObject>,Schedulable {
    
    set<ID> setPrbIdsToProcess = new set<ID>();
    
    global Schedule_OSAProblemsAlert() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(!Test.isrunningtest()){
            return Database.getQueryLocator([SELECT Id, ActualUserLastmodifiedDate__c, Status__c, Severity__c FROM Problem__c WHERE ActualUserLastmodifiedDate__c <= : system.now() - Integer.valueOf(Label.CLOCT15I2P39) AND  (Status__c =: Label.CLOCT15I2P31 OR Status__c =: Label.CLOCT15I2P32 OR Status__c =: Label.CLOCT15I2P33) AND ((Severity__c =: Label.CLOCT14I2P22 AND (TECH_ReminderNoUpdateDate__c = null OR TECH_ReminderNoUpdateDate__c <= : system.now() - Integer.valueOf(Label.CLOCT15I2P40)))  OR (Severity__c !=: Label.CLOCT14I2P22 And (TECH_ReminderDate__c = null OR TECH_ReminderDate__c <= : system.now() - Integer.valueOf(Label.CLOCT15I2P40))))]);
            //CLOCT14I2P22 = 'Safety related'
            //CLOCT15I2P31 = 'New'  
            //CLOCT15I2P32 = 'Stand By'
            //CLOCT15I2P31 = 'In Progress'                                 
        }
        if(Test.isrunningtest()){
            return Database.getQueryLocator([SELECT Id FROM Problem__c LIMIT 1]);
        }
        return NULL;
    }
    
    global void execute(Database.BatchableContext BC, List<Problem__c> scope) {
        
        for(Problem__c Prb : scope) {
            setPrbIdsToProcess.add(Prb.Id);
        }
        
        AP1000_Problem.createAlertForPrb_Stakeholders(setPrbIdsToProcess);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    global void execute(SchedulableContext SC) {
        Schedule_OSAProblemsAlert P1 = new Schedule_OSAProblemsAlert();
        
        // label : CLAPR15I2P13 = 200
        ID batchprocessid = Database.executeBatch(P1,Integer.valueOf(System.Label.CLAPR15I2P13));           
    }
}