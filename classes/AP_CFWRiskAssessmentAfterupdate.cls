/* DESCRIPTION: UPDATES CERTIFICATION CHECKLIST RECORD WHEN RISK ASSESSMENT MOBILE FIELD IS CHANGED*/
public class AP_CFWRiskAssessmentAfterupdate
{
    public static void updateRiskAssessment(List<CFWRiskAssessment__c> riskAssessments)
    {
        List<Id> riskAssessmentIds = new List<Id>();
        List<CertificationChecklist__c> certChecklists = new List<CertificationChecklist__c>();
        Map<Id, CFWRiskAssessment__c> IdsRiskAss = new Map<Id, CFWRiskAssessment__c>();
        for(CFWRiskAssessment__c cr: riskAssessments)
        {
            riskAssessmentIds.add(cr.Id);
            IdsRiskAss.put(cr.Id, cr);
        }
        certChecklists = [select Id,CertificationRiskAssessment__c from CertificationChecklist__c where CertificationRiskAssessment__c in:riskAssessmentIds];
        if(!(certChecklists.isEmpty()))
        {
                List<Database.saveresult> sr = Database.update(certChecklists,false);
                for(Database.saveresult ds: sr)
                {
                    if(!(ds.isSuccess()))
                        IdsRiskAss.get(ds.getId()).addError(ds.getErrors()[0].getMessage());
                }
        }        
    }
}