public with sharing class VFC1000_ProblemSearch extends VFC_ControllerBase{
    
   
//*********************************************************************************
// Controller Name  : VFC1000_ProblemSearch
// Purpose          : Controller class for Problem Search
// Created by       : Global Delivery Team
// Date created     : 27th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/
        
     /*============================================================================
        V.A.R.I.A.B.L.E.S
    =============================================================================*/ 
    Map<String,String> ProductLineMap; // Map between Values and Labels for the product Line Picklist
    Map<String,String> ProductFamilyMap; // Map between Values and Labels for the Product Family Picklist
    Map<String,String> BusinessMap;  // Map between Values and Labels for the Business Picklist
    Map<String,String> FamilyMap;
    
    String AccountableOrganizationName='';
    Boolean blnIsDeployed=false;
    
        
    // current problem used for the input search
    public Problem__c problem{ get; set; }
    
    
    // collection of accounts matching the search criteria
    public List<Problem__c> problems {get; private set;}
    
    // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL10048);
    
    
    
    
    
    // indicates whether there are more records than the searchMaxSize.
    public Boolean hasNext {
        get {
            return (problems == null || (problems != null && problems.size() > searchMaxSize));
        }
        set;
    }
    // indicates whether Problem To Prevention is deployed for the selected Accountable Organization.
    public Boolean isAccountableOrganizationDeployed{
        get {
            return (blnIsDeployed);
        }
        set;
    }  
    
    public List<String> filters{get; set;}    
    
    public String title{ get; set; }
    
    public String CommercialReference{ get; set; }
    // Advanced I2P : Added Get and Set by Siddu Start
    
    public Boolean showAdvFields{ get; set; }
    public Boolean showSupplerSearch{ get; set; }
    public Boolean showMainSearch{ get; set; }
    public String recTypeName;
    public String recTypeId;
    public String Supplier{ get; set; }
    public String PartNumber{ get; set; }
    
    // Advanced I2P : Added Get and Set by Siddu End
    
        /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/    
    public VFC1000_ProblemSearch(ApexPages.StandardController controller) {
       
        problem = (Problem__c)controller.getRecord();
      // Advanced I2P : Added  by Hari Krishna Start  
       recTypeId =ApexPages.currentPage().getParameters().get('RecordType');
       
        
            
            if(recTypeId !=null && recTypeId.length()>0)
            {
                
                if(recTypeId == System.Label.CLI2PAPR120010)
                showAdvFields = true;
                else {showAdvFields = false;}
                
            }
        
        // Advanced I2P : Added  by Hari Krishna End  
        
    }
    
     /*============================================================================
        M.E.T.H.O.D.S
    =============================================================================*/  
    
    
    //*********************************************************************************
        // Method Name      : doSearchProblem
        // Purpose          : frame query string using getQuerySting and display problem search results on the screen
        // Created by       : Hari Krishna  Global Delivery Team
        // Date created     : 26th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/       
    
    public PageReference doSearchProblem(){
        
        //*************************code added by Mohit********************************************//
        // =======================SUKUMAR SALLA - MAY2013RELEASE - START=========================
        //list<BusinessRiskEscalationEntity__c> lstBRO=[select id,name,ProblemToPreventionDeployed__c from BusinessRiskEscalationEntity__c where id=:problem.AccountableOrganization__c];
         list<BusinessRiskEscalationEntity__c> lstBRO=[select id,name,ProblemToPreventionDeployed__c, Location_Type__c from BusinessRiskEscalationEntity__c where id=:problem.AccountableOrganization__c AND Location_Type__c!=:LABEL.CLMAY13I2P10];
         // =======================SUKUMAR SALLA - MAY2013RELEASE - END=========================
        //*************************code added by Mohit End********************************************//
        problems=null;        
        String s = null;
        // Advanced I2P : Modefied  by Hari Krishna Start  
        if(recTypeId == System.Label.CLI2PAPR120010 ){
             
            if(Supplier == null || Supplier.length()==0 ){
                
                 ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR,System.Label.CLI2PAPR120011);
                ApexPages.addMessage(errorMessage);
                
            }
            
            else{
            if(lstBRO.size()>0 && lstBRO[0].ProblemToPreventionDeployed__c==false)
            {
              ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR,Label.CLI2PDEC120007);
                ApexPages.addMessage(errorMessage);
            return null;
            }
           
                s = getQueryString();
               
                
                Utils_SDF_Methodology.log('START query: ', s);  
                Utils_SDF_Methodology.startTimer();
                try {            
                                problems = Database.query(s); 
                                                
                    
                }catch(Exception ex){
                    ApexPages.addMessages(ex);
                }
                
            }
            
        }
        else{
        try { 
        //*************************Code Added By Mohit for Dec12 Release*************************//
        if(problem.AccountableOrganization__c ==null)
        {
        ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR,Label.CLI2PDEC120006);
                ApexPages.addMessage(errorMessage);
                
        
        }else
        {
        //list<BusinessRiskEscalationEntity__c> lstBRO=[select id,name,ProblemToPreventionDeployed__c from BusinessRiskEscalationEntity__c where id=:problem.AccountableOrganization__c];
          
if(lstBRO.size()>0 && lstBRO[0].ProblemToPreventionDeployed__c==false)
{
ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR,Label.CLI2PDEC120007);
                ApexPages.addMessage(errorMessage);
                
}else
{   
//*************************Code Added By Mohit for Dec12 Release End*************************//

      s = getQueryString();
            
            
            Utils_SDF_Methodology.log('START query: ', s);  
            Utils_SDF_Methodology.startTimer();
                       
                    problems = Database.query(s);
                    
                    //Added by Pooja Gupta for May 2012 Monthy Release - Start
                    //Display error message when problem records exceed search maxsize
                    if(problems != null && problems.size() > searchMaxSize){
                        ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.INFO,System.Label.CLI2PMAY120001);
                        ApexPages.addMessage(errorMessage);
                    }
                    //Added by Pooja Gupta for May 2012 Monthy Release - End                 
         }   
        }
        }catch(Exception ex){
                ApexPages.addMessages(ex);
            }
        }
        
         // Advanced I2P : Modefied  by Hari Krishna End  

        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return null;
    }
    
    //*********************************************************************************
        // Method Name      : continueCreation
        // Purpose          : This will  allow the user to go to the Problem Create Standard page, after user done with search
        // Created by       : Hari Krishna  Global Delivery Team
        // Date created     : 26th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
    
    public PageReference continueCreation(){
        
                Schema.DescribeSObjectResult D = problem__c.sObjectType.getDescribe();
                PageReference newPbPage = new PageReference('/'+D.getKeyPrefix()+'/e?');
                newPbPage.getParameters().put('nooverride', '1');
  
                if(title!=null && title!='')
                newPbPage.getParameters().put(System.Label.CL10049,title );
                if(problem.AccountableOrganization__c!=null) {
                        newPbPage.getParameters().put(System.Label.CL10051,AccountableOrganizationName);
                        newPbPage.getParameters().put(System.Label.CL10050,problem.AccountableOrganization__c ); 
                                       
                } 
        newPbPage.getParameters().put('nooverride', '1');
        newPbPage.getParameters().put('RecordType', ApexPages.currentPage().getParameters().get('RecordType'));
        //for supplier record type
         // Advanced I2P : Added  by Siddu Start  
        if(recTypeId == System.Label.CLI2PAPR120010){
        //newPbPage.getParameters().put('CF00NK0000000HazF', UserInfo.getName());
        //newPbPage.getParameters().put('CF00NK0000000HazF_lkid', UserInfo.getUserId());
        
        //newPbPage.getParameters().put('CF00NA0000009exUG', UserInfo.getName());
        //newPbPage.getParameters().put('CF00NA0000009exUG_lkid', UserInfo.getUserId());
        
        newPbPage.getParameters().put(System.Label.CLI2PAPR120015,ApexPages.currentPage().getParameters().get(System.Label.CLI2PAPR120015));
        newPbPage.getParameters().put(System.Label.CLI2PAPR120016,ApexPages.currentPage().getParameters().get(System.Label.CLI2PAPR120016));
        newPbPage.getParameters().put(System.Label.CLI2PAPR120012, UserInfo.getName());
        newPbPage.getParameters().put(System.Label.CLI2PAPR120013, UserInfo.getUserId());
        
        
        }
         // Advanced I2P : Added  by Siddu End  
        newPbPage.setRedirect(true);
        return newPbPage;
    }
    
   
   //*********************************************************************************
        // Method Name      : getQueryString
        // Purpose          : This is used to frame the SOQL by using the user input
        // Created by       : Hari Krishna  Global Delivery Team
        // Date created     : 26th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
    
    public String getQueryString() {
         
        //AccountableOrganizationName=[SELECT Name FROM BusinessRiskEscalationEntity__c WHERE Id=:problem.AccountableOrganization__c].Name;
        
         // Advanced I2P : Added  by Siddu Start  
        if(problem.AccountableOrganization__c != null){
            //============ SUKUMAR SALLA - MAY2013RELEASE - START======================
            //List<BusinessRiskEscalationEntity__c> OrgList = new List<BusinessRiskEscalationEntity__c>([SELECT Name, ProblemToPreventionDeployed__c FROM BusinessRiskEscalationEntity__c WHERE Id=:problem.AccountableOrganization__c ]);
            List<BusinessRiskEscalationEntity__c> OrgList = new List<BusinessRiskEscalationEntity__c>([SELECT Name, ProblemToPreventionDeployed__c, Location_Type__c FROM BusinessRiskEscalationEntity__c WHERE Id=:problem.AccountableOrganization__c AND Location_Type__c!=:LABEL.CLMAY13I2P10]);
            // ============ SUKUMAR SALLA - MAY2013RELEASE - END ===========================
            if(OrgList.size()> 0){
                AccountableOrganizationName = OrgList[0].Name;
                blnIsDeployed =  OrgList[0].ProblemToPreventionDeployed__c ;
            }
            if(!blnIsDeployed){
                ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR,System.Label.CL10072);
                ApexPages.addMessage(errorMessage);
            }
        }
         // Advanced I2P : Added  by Siddu End  
         // Advanced I2P : Added  by Hari Krishna Start  
        if(recTypeId == System.Label.CLI2PAPR120010 ){
            blnIsDeployed = true;

        }
         // Advanced I2P : Added  by Hari Krishna End  
        
        String gmrcodestring='';               
               
            if(searchController.items1!=null && searchController.items1.size()>0 && searchController.level1!=null && searchController.level1!=Label.CL00355){                             
                gmrcodestring=searchController.level1;
            }          
            if(searchController.items2!=null && searchController.items2.size()>0 && searchController.level2!=null && searchController.level2!=Label.CL00355){
                 gmrcodestring=searchController.level2;                  
            }
            if(searchController.items3!=null && searchController.items3.size()>0 && searchController.level3!=null && searchController.level3!=Label.CL00355){
                 gmrcodestring=searchController.level3;                  
            }
            if(searchController.items4!=null && searchController.items4.size()>0 && searchController.level4!=null && searchController.level4!=Label.CL00355){
                 gmrcodestring=searchController.level4;                  
            }
                
        String QueryString='';
        String query_Fields;
        // Advanced I2P : Added  by Hari Start  (Added two Fields in the Query)
        query_Fields = 'Select Title__c , WhatIsTheProblem__c, Symptoms__c, Status__c, Sensitivity__c, Name, Id, CreatedDate,AccountableOrganization__r.SubEntity__c, AccountableOrganization__r.Entity__c, AccountableOrganization__r.Location__c, AccountableOrganization__c ,AccountableOrganization__r.Name ,PartnerAccount__r.Name  From Problem__c ';
        // Advanced I2P : Added  by Hari End  
        String outer_WhereClause='';
        if(problem.AccountableOrganization__c!=null){
        outer_WhereClause +=' Where  AccountableOrganization__r.Name LIKE \'%'+AccountableOrganizationName+'%\'';
        }
        if(title !=null && title!=''){    
                if(outer_WhereClause.length()>0 )  
                outer_WhereClause+=' AND Title__c LIKE \'%'+Utils_Methods.escapeForWhere(title)+'%\'';
                else{
                outer_WhereClause+='Where  Title__c LIKE \'%'+Utils_Methods.escapeForWhere(title)+'%\'';
                }
        }
         // Advanced I2P : Added  by Siddu Start  
        if(Supplier !=null && Supplier !=''){
                if(outer_WhereClause.length()>0 )       
                outer_WhereClause+=' AND PartnerAccount__r.Name LIKE \'%'+Utils_Methods.escapeForWhere(Supplier)+'%\'';
                 else{
                    outer_WhereClause+='Where  PartnerAccount__r.Name LIKE \'%'+Utils_Methods.escapeForWhere(Supplier)+'%\'';
                }
        }
        
        if(PartNumber !=null && PartNumber !=''){      
                outer_WhereClause+=' AND PartNumber__c LIKE \'%'+Utils_Methods.escapeForWhere(PartNumber)+'%\'';
        }
         // Advanced I2P : Added  by Siddu End  
        
        String inQueryString='ID in ( Select Problem__c  From CommercialReference__c where  ';
                
                String inner_WhereClause='';
                if(gmrcodestring!='')
                        inner_WhereClause='  GMRCode__c LIKE \''+gmrcodestring+'%\' ';
                
        
        if(CommercialReference !=null && CommercialReference!=''){
                
                if(inner_WhereClause!=null && inner_WhereClause !=''){
                        // Advanced I2P : Updated  by Hari Start  (Added like operator)
                        inner_WhereClause +=' AND CommercialReference__c   LIKE \'%'+Utils_Methods.escapeForWhere(CommercialReference)+'%\'';
                        // Advanced I2P : Updated  by Hari End
                }
                else{
                        // Advanced I2P : Updated  by Hari Start  (Added like operator)
                        inner_WhereClause +='  CommercialReference__c   LIKE \'%'+Utils_Methods.escapeForWhere(CommercialReference)+'%\'';
                        // Advanced I2P : Updated  by Hari End
                }
                
        }
        
        if(inner_WhereClause!=null && inner_WhereClause !=''){
                
                inner_WhereClause +=' ) ';
        }
         
        if(inner_WhereClause!=null && inner_WhereClause !=''){
                 QueryString +=query_Fields+outer_WhereClause+'  and  '+inQueryString+inner_WhereClause;
        } 
        
        else {
                
                 QueryString +=query_Fields+outer_WhereClause;
                
        }
       
        // Advanced I2P : Added  by Hari Start  
        if(recTypeId != null && recTypeId.length()>0){
             QueryString += ' And RecordTypeId =:recTypeId ';
        }
         // Advanced I2P : Added  by Hari End  
                QueryString += ' ORDER BY Name';
        QueryString += ' LIMIT ' + (Integer)(searchMaxSize+1);
       gmrcodestring=null;
        
        return QueryString; 
    }
    
    //*********************************************************************************
        // Method Name      : continueCreation
        // Purpose          : This will instantiate VCC05_DisplaySearchFields and display product filter in vf 
        // Created by       : Hari Krishna  Global Delivery Team
        // Date created     : 26th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
    
    public VCC05_DisplaySearchFields searchController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC05_DisplaySearchFields displaySearchFields;
                displaySearchFields = (VCC05_DisplaySearchFields)getcomponentControllerMap().get('searchComponent');
                
                if(displaySearchFields!= null)
                return displaySearchFields;
            } 
            return new VCC05_DisplaySearchFields();
        }
    }
    public PageReference componentOnchange(){
        if(problem.AccountableOrganization__c==null){
            searchController.init();
        }
        return null;
        
    }
    
    
    
 
    
    public class TestException extends Exception {}  

}