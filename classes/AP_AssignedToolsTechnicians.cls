Public class AP_AssignedToolsTechnicians{

     public static void updateAssignedToolsTechniciansStatus(Set<Id> woIdSet,String picValue){
        //*********************************************************************************
        // Method Name : updateAssignedToolsTechniciansStatus
        // Purpose : To populate Status of Assigned Tools Technicians to Cancelled which are other than Rejected.
        // Created by : Hari Krishna - Global Delivery Team
        // Date created : 12th Feb 2013
        // Modified by :
        // Date Modified :
        // Remarks : For May - 13 Release
        ///*********************************************************************************/
        List<AssignedToolsTechnicians__c> assignToolsTechList = new List<AssignedToolsTechnicians__c>();
        if(woIdSet != null && woIdSet.size() >0){
                try{
                    List<AssignedToolsTechnicians__c> asToolTech=[select id,PrimaryUser__c,Status__c,TechnicianEquipment__c,WorkOrder__c from AssignedToolsTechnicians__c where WorkOrder__c in :woIdSet];
                    if(asToolTech != null && asToolTech.size()>0){
                        for(AssignedToolsTechnicians__c att:asToolTech){
                            att.Status__c = picValue;
                            att.PrimaryUser__c = false;
                            assignToolsTechList.add(att);
                        }
                    }
                    if(assignToolsTechList != null && assignToolsTechList.size()>0){
                       update assignToolsTechList ;
                    }
                }
                catch(Exception ex){
                    System.debug('Exception on updateAssignedToolsTechniciansStatus '+ex);
                }
        }
        
        
        
        
    }
    public static void updateAssignedToolsTechniciansPrimaryUser(List<SVMXC__Service_Order__c> woList){
        //*********************************************************************************
        // Method Name : updateAssignedToolsTechniciansPrimaryUser
        // Purpose : when work order is update with new Team member , respective Assigned tools/Technician record was update with primary
        // Created by : Hari Krishna - Global Delivery Team
        // Date created : 12th Feb 2013
        // Modified by :
        // Date Modified :
        // Remarks : For May - 13 Release
        ///*********************************************************************************/
         Set<id> wIdset = new Set<id>();
         Map<id, List<AssignedToolsTechnicians__c>> widAtoolListMap= new Map<id, List<AssignedToolsTechnicians__c>>();
         List<AssignedToolsTechnicians__c> atttoupdatelist = new List<AssignedToolsTechnicians__c>();
        if(woList != null && woList.size()>0){
            try{
              
                for(SVMXC__Service_Order__c wo:woList){
                    wIdset.add(wo.id);
                    
                }
                if(wIdset != null && wIdset.size()>0){
                     System.debug('****'+wIdset);
                     List<AssignedToolsTechnicians__c> asToolTech=[select id,Status__c,PrimaryUser__c,TechnicianEquipment__c,WorkOrder__c from AssignedToolsTechnicians__c where WorkOrder__c in :wIdset  And Status__c <> 'Rejected'];
                      System.debug('asdf****'+asToolTech);
                      if(asToolTech != null && asToolTech.size()>0){
                            for(AssignedToolsTechnicians__c att:asToolTech){
                                if(!widAtoolListMap.containsKey(att.WorkOrder__c)){
                                    widAtoolListMap.put(att.WorkOrder__c, new List<AssignedToolsTechnicians__c>());
                                    widAtoolListMap.get(att.WorkOrder__c).add(att);
                                    
                                }
                                else{
                                    widAtoolListMap.get(att.WorkOrder__c).add(att);
                                }
                            }
                      }
                     
                }
                for(SVMXC__Service_Order__c wo:woList){
                    if(widAtoolListMap.containsKey(wo.id)){                 
                         for(AssignedToolsTechnicians__c att:widAtoolListMap.get(wo.id)){
                                                    
                            if(wo.SVMXC__Group_Member__c != null && att.TechnicianEquipment__c != null){
                                if(wo.SVMXC__Group_Member__c == att.TechnicianEquipment__c){                            
                                     if(att.Status__c !='Cancelled')
                                     att.PrimaryUser__c = true;
                                     atttoupdatelist.add(att);                           
                                }
                                else{                        
                                    att.PrimaryUser__c = false;
                                    atttoupdatelist.add(att);
                                }
                            }
                            if(wo.SVMXC__Group_Member__c == null && wo.SVMXC__Order_Status__c == System.label.CLDEC13SRV04){
                                    att.PrimaryUser__c = false;
                                    att.Status__c ='Cancelled';
                                    atttoupdatelist.add(att);
                            }
                            
                        }
                    }
                }
                update atttoupdatelist;
            }
            catch(Exception ex){
                System.debug('Exception on updateAssignedToolsTechniciansPrimaryUser '+ex);
            }
        
        }
    }
    public static void updateAssignedToolsTechniciansFieldUpdate(List<SVMXC__Service_Order__c> woList,String targetFieldApi,String SourceFieldApi){
        List<Event> eventtoupdatelist = new List<Event>();
        String SOQLQuery = '';
        Set<id> woIdSet = new Set<id>();
        Map<id, SVMXC__Service_Order__c> woidrecMap = new Map<id, SVMXC__Service_Order__c>();
        woidrecMap.putAll(woList);
        woIdSet = woidrecMap.keySet();
        Map<id, List<Event>> widEventListMap= new Map<id, List<Event>>();       
        List<Event> EventList = new List<Event>();
        SOQLQuery = ' Select '+SourceFieldApi+' ,WhatId   From Event  where WhatId in :woIdSet';
        EventList = Database.Query(SOQLQuery);
        
        if(EventList!= null && EventList.size()>0){ 
            for(Event eve:EventList){
                if(!widEventListMap.containsKey(eve.WhatId)){
                    widEventListMap.put(eve.WhatId, new List<Event>());
                    widEventListMap.get(eve.WhatId).add(eve);
                    
                }
                else{
                    widEventListMap.get(eve.WhatId).add(eve);
                }
            }
        }
        
        for(SVMXC__Service_Order__c wo:woList){
            if(widEventListMap.containsKey(wo.id)){
                
                for(Event eve:widEventListMap.get(wo.id)){
                        Sobject tobj = (Sobject)eve;
                        Sobject sobj = (Sobject)wo;
                        tobj.put(targetFieldApi , sobj.get(SourceFieldApi));
                        eventtoupdatelist.add((Event)tobj);
                }
                
            }
        }
        // Added for EUS# 046939 on 19-11-2014
        if(eventtoupdatelist!= null && eventtoupdatelist.size()>0){
           // update eventtoupdatelist;
            
            try{  
                   List<Database.SaveResult> srs = Database.update(eventtoupdatelist,false);
                   for (Database.SaveResult sr : srs) {
                        if (sr.isSuccess()) {
                            
                        }
                        else {
                             System.debug(' \n ----------------- start----------------- ');
                            // Operation failed, so get all errors                
                            for(Database.Error err : sr.getErrors()) {
                                System.debug('  \nThe following error has occurred.');                    
                                System.debug(' \n '+err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('  \n fields that affected this error: ' + err.getFields());
                            }
                            System.debug(' \n ----------------- end ----------------- ');
                        }
                        
                    }
            }
            catch(Exception ex){
                System.debug(' Exception occured on updateAssignedToolsTechniciansFieldUpdate  '+ ex.getMessage());
            }
            
           
        }
        
    }
    
    public static void CancelledAssignedToolsTechnicians(Set<string> eidSet)
    {
        //*********************************************************************************
        // Method Name : CancelledAssignedToolsTechnicians
        // Purpose : When a Event is Deleted when work order status is Scheduled delete the respective Assigned Tools Techinician based on  Event Id in ATT. 
        // Created by : Hari Krishna - Global Delivery Team
        // Date created : 1th Oct 2013
        // Modified by :
        // Date Modified :
        // Remarks : For Oct - 15 Release
        ///*********************************************************************************/
        List<AssignedToolsTechnicians__c> assignToolsTechList1 = new List<AssignedToolsTechnicians__c>();
        if(eidSet != null && eidSet.size()>0)
        {
            try
            {
                System.debug('***********************CancelledAssignedToolsTechnicians start**************************');
                List<AssignedToolsTechnicians__c> attList = [select id,Status__c from AssignedToolsTechnicians__c where EventID__c =: eidSet /*and  WorkOrder__r.SVMXC__Order_Status__c <> 'Customer Confirmed' */ ];
                if(attList != null && attList.size()>0)
                {
                    for(AssignedToolsTechnicians__c att:attList)
                    {
                        if(att.Status__c != 'Rejected')
                        {
                            att.Status__c = 'Cancelled';
                            System.debug('***********************Status == Cancelled**************************');
                            att.PrimaryUser__c = false;
                            assignToolsTechList1.add(att);
                        }
                    }
                }
                if(assignToolsTechList1 != null && assignToolsTechList1.size()>0)
                {
                    update assignToolsTechList1;
                }
                
                System.debug('***********************CancelledAssignedToolsTechnicians start**************************');
            }
            catch(Exception ex)
            {
                System.debug('Exception on CancelledAssignedToolsTechnicians '+ex);
            }
            //delete attList;
        }
    }
   // @future
   /* public static void CancelledATTAtCustomerConfirmANDAckFsr(Set<string> eidSet)
    {
        //*********************************************************************************
        // Method Name : DeleteAssignedToolsTechnicians
        // Purpose :Old Assigned WO line to secondary FSR with status “Cancelled” with tracked
        // Created by : Hari Krishna - Global Delivery Team
        // Date created : 04th DEC 2015
        // Modified by :
        // Date Modified :
        // Remarks : For Dec - 15 Release
        //*********************************************************************************
        List<AssignedToolsTechnicians__c> assignToolsTechList1 = new List<AssignedToolsTechnicians__c>();
        if(eidSet != null && eidSet.size()>0)
        {
            try
            {
                System.debug('***********************CancelledATTAtCustomerConfirmANDAckFsr start**************************');
                List<AssignedToolsTechnicians__c> attList = [select id,PrimaryUser__c,Status__c,WorkOrder__r.SVMXC__Order_Status__c from AssignedToolsTechnicians__c where EventID__c =: eidSet and  (WorkOrder__r.SVMXC__Order_Status__c = 'Customer Confirmed' OR WorkOrder__r.SVMXC__Order_Status__c = 'Acknowledge FSE') ];
                if(attList != null && attList.size()>0)
                {
                    for(AssignedToolsTechnicians__c att:attList)
                    {       
                        if(att.PrimaryUser__c == false )
                        {
                            att.Status__c = 'Cancelled';
                            System.debug('*********************** Status == Cancelled**************************');
                            //att.PrimaryUser__c = false;                           
                        }
                        else{
                            if(att.PrimaryUser__c == true && att.WorkOrder__r.SVMXC__Order_Status__c == 'Customer Confirmed' && att.Status__c != 'Rejected')
                            {
                                att.Status__c = 'Cancelled';
                                System.debug('*********************** Status == Cancelled**************************');
                            }
                            
                        } 
                         assignToolsTechList1.add(att);
                    }
                }
                if(assignToolsTechList1 != null && assignToolsTechList1.size()>0)
                {
                    update assignToolsTechList1;
                }
                System.debug('***********************CancelledATTAtCustomerConfirmANDAckFsr start**************************');
            }
            catch(Exception ex)
            {
                System.debug('Exception on CancelledATTAtCustomerConfirmANDAckFsr'+ex);
            }
            
        }
    }*/
}