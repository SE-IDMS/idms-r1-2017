@isTest
private class PRJ_planviewReportController_TEST
{
    static testMethod void unitTest()
    {
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'created','Buildings','Global');
        insert testDMTA1;
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.ParentFamily__c = 'Business';
        testProj.Parent__c = 'Buildings';
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.GeographicZoneSE__c = 'Global';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c = 'Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.ProjectClassification__c = 'Infrastructure';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        insert testProj;   
        
        
        PageReference pageRef = Page.PRJ_planviewReportPage;
        Test.setCurrentPage(pageRef);  

        ApexPages.currentPage().getparameters().put('fromDate','20/04/2015'); 
        ApexPages.currentPage().getparameters().put('toDate','21/04/2015'); 
        PRJ_planviewReportController controller = new PRJ_planviewReportController(new ApexPages.StandardController(testProj)); 

        controller.exportToExcel();
    }
}