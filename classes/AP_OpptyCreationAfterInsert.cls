/*Created by: Deepak Kumar
    Date:27/08/2013
    For the purpose of Creating Opportunity when Opportunity Notification is getting created by SFM Buttons.
    OCT13 Release
    */
    Public Class AP_OpptyCreationAfterInsert
    {   public static Savepoint sp;
        public static void CreatingOpportunity(Set<Id> optNotId)
        {   
            List<Opportunity>  OppListToCreate = new List<Opportunity>();
            List<OpportunityTeamMember>  OppTeamMemberList = new List<OpportunityTeamMember>();
            List<OpportunityShare> oppShares = new List<OpportunityShare>();
            Opportunity oppt = new Opportunity();
          //  sp = Database.setSavepoint();
            List<OpportunityNotification__c> opptyNotList = New List<OpportunityNotification__c>();
            
            set<id>  Oppidset = new set<id>();   
                    
            opptyNotList=[SELECT ID,OpportunityLink__c, OwnerId,Name,TechCreatedFromSC__c,Tech_CreatedFromIB__c, Associated_Contract__c,Associated_Contract__r.Name,AccountForConversion__r.id, AccountForConversion__r.Name, AccountForConversion__r.OwnerId, CountryDestination__r.id, CountryDestination__r.Name, LeadingBusinessBU__c, OpportunityDetector__r.id, CurrencyIsoCode, status__c, Amount__c, CloseDate__c, IncludedinForecast__c, OpportunityCategory__c, OpportunityScope__c, OpportunityType__c, Stage__c, Description__c, TECH_Locked__c, OpportunityOwner__r.id, WorkOrder__r.SVMXC__Contact__r.id, WorkOrder__r.Name,Case__c  FROM OpportunityNotification__c WHERE ID=:optNotId LIMIT 1] ;

             for(OpportunityNotification__c oppNotif :opptyNotList)
             {
                //if(oppNotif.Tech_CreatedFromIB__c == true || oppNotif.TechCreatedFromSC__c == true)
               // {
                    oppt.Name= oppNotif.Name;
                    oppt.OpportunityDetector__c = oppNotif.OpportunityDetector__r.id;
                    oppt.CurrencyIsoCode = oppNotif.CurrencyIsoCode;
                    oppt.AccountId = oppNotif.AccountForConversion__r.id;
                    oppt.TECH_CrossProcessConversionAmount__c = oppNotif.Amount__c;
                    oppt.CloseDate = oppNotif.CloseDate__c;
                    oppt.CountryOfDestination__c = oppNotif.CountryDestination__c;
                    oppt.IncludedinForecast__c = oppNotif.IncludedinForecast__c;
                    oppt.LeadingBusiness__c = oppNotif.LeadingBusinessBU__c;
                    oppt.OpportunityCategory__c = oppNotif.OpportunityCategory__c;
                    oppt.OpportunityScope__c = oppNotif.OpportunityScope__c;
                    oppt.OpptyType__c = oppNotif.OpportunityType__c;
                    oppt.StageName = oppNotif.Stage__c;
                    oppt.Description = oppNotif.Description__c;
                    oppt.TECH_CreatedFromServices__c = true;
                    oppt.BusinessMix__c = oppNotif.LeadingBusinessBU__c;
                    oppt.OpportunityNotification__c = oppNotif.Id;
                    oppt.SourceReference__c = oppNotif.Associated_Contract__r.Name;
                    oppt.ServiceContract__c = oppNotif.Associated_Contract__c;//BR-8005
                    
                    if(oppNotif.OpportunityOwner__r.id == null)
                    {
                        oppt.OwnerId = oppNotif.AccountForConversion__r.OwnerId;
                    }
                    else
                    {
                        oppt.OwnerId = oppNotif.OpportunityOwner__r.id;
                    }
                   /*
                    if(oppNotif.Case__c != null)
                    {
                        opportunity.OpportunitySource__c = Label.CLOCT13CCC10;
                    }
                    else if(oppNotif.WorkOrder__r.Name != null)
                    {
                        opportunity.Work_Order_Number__c = oppNotif.WorkOrder__r.Name;
                        opportunity.OpportunitySource__c = Label.CL00680;
                    }
                    else*/
                     if(oppNotif.Tech_CreatedFromIB__c)
                    oppt.OpportunitySource__c = Label.CLOCT13SRV31;
                    if(oppNotif.TechCreatedFromSC__c)
                    oppt.OpportunitySource__c = 'Service Contract';
                    //OppListToCreate.add(oppt);          
               //}
             }
             Id oppId;
             Id OwnerId;
             database.saveResult sv = Database.Insert(oppt, false);
            if(sv.issuccess())
            {  
      
                for(OpportunityNotification__c oppNotif :opptyNotList)
                { 
                      oppNotif.status__c = System.Label.CL00520;
                      OwnerId = oppNotif.OwnerId;
                      oppNotif.OpportunityLink__c = oppt.Id;//Added for April 14 Release
                     // oppNotif.recordTypeId ='012A0000000nYNf';
                      database.saveResult updt = Database.Update(oppNotif,false);
                        system.debug('33333'); 
                        oppId=sv.getId();
                        system.debug('oppId:'+oppId); 
                        system.debug('OwnerId:'+OwnerId); 
                
                
                OpportunityTeamMember opptm = New OpportunityTeamMember();  
                        opptm.OpportunityId=oppt.Id;
                        opptm.UserId = oppNotif.OwnerId;
                        opptm.TeamMemberRole = System.Label.CLOCT14SLS05;
                        system.debug('opptm:'+opptm); 
                        //opptm.OpportunityAccessLevel ='Read';
                        //OppTeamMemberList.add(opptm);
                        //system.debug('OppTeamMemberList:'+OppTeamMemberList);
                        insert opptm;
                
            List<OpportunityShare> shares = [select Id, OpportunityAccessLevel,  
             RowCause from OpportunityShare where OpportunityId =:oppt.Id and RowCause = 'Team'];

            // set all team members access to read/write
            for (OpportunityShare share : shares) { 
            share.OpportunityAccessLevel = 'Edit';
                }
            update shares;
                
                    
                    if(!updt.isSuccess())
                    {
                        Database.rollback(sp); //rolling back in case of any exception  
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,updt.getErrors()[0].getMessage()));                    
                        oppt.clear();
                    }
                }
                
            }
      
            
        }

  
        public static void CreatingIpOpportunity(list<InstalledProductOnOpportunityNotif__c> IpOppList) 
         {
            Set<Id> OptyNId = New Set<Id>();
            List<Opportunity>  OppList = new List<Opportunity>();
            
            for(InstalledProductOnOpportunityNotif__c ip :IpOppList)
            {
                    OptyNId.add(ip.OpportunityNotification__c);
            }
            // Hari added CurrencyIsoCode in query due to test failure
            OppList=[Select Id,Name,CurrencyIsoCode  From Opportunity Where OpportunityNotification__c =:OptyNId];

            InstalledProductOnOpportunity__c IPOppty = New InstalledProductOnOpportunity__c();

           if(OppList.size()>0)
           {
               for(Opportunity oppt :OppList)
               {
                  for(InstalledProductOnOpportunityNotif__c Ip :IpOppList)
                  {
                        IPOppty.Opportunity__c  = oppt.Id;
                        IPOppty.InstalledProduct__c  = Ip.InstalledProduct__c;
                        IPOppty.CurrencyIsoCode = oppt.CurrencyIsoCode;
                  }
               }
           }
           insert IPOppty;

         }
        
    }