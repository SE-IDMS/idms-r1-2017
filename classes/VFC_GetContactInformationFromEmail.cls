/*
Author          : Vimal Karunakaran - Global Delivery Team
Date Created    : 30/07/2013
Description     : Controller searches the Contacts and Personal Accounts with the given Email Address from CTI.
-----------------------------------------------------------------
                          MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : 
Modified Date   : 
Description     : 
-----------------------------------------------------------------
*/
public with sharing class VFC_GetContactInformationFromEmail {
    public String emailId{get;set;}{emailId='';}
    public List<List<Sobject>> searchResults{get;set;}{searchResults=new List<List<Sobject>>();}//To hold the SOSL search results
    public List<Contact> allContacts{get;set;}{allContacts=new List<Contact>();} //To store Contact records from search results
    public List<Contact> allBusinessContacts{get;set;}{allBusinessContacts=new List<Contact>();} //To store Contact records from search results
    public List<Contact> allPersonAccounts{get;set;}{allPersonAccounts=new List<Contact>();}//To store Account records from search results
    public Boolean bTestFlag = false;    //used in Test method on second invokation of the createEvents() method - ensures complete code coverage
    public list<AccounttoContactsWrapper>  lstAccWrap {get;set;} {lstAccWrap= new list<AccounttoContactsWrapper>();} // To store Search results in wraps of Account to related contacts list. to show on the page
    public Boolean blnDisplayBusinessContact{get;set;}{blnDisplayBusinessContact=true;}
    public Boolean blnDisplayPersonAccount{get;set;}{blnDisplayPersonAccount=true;}
    
    //Method to search the Contact and Person Accouts for the given email address
    public PageReference verifyEmailId(){   
        try{
            if(ApexPages.currentPage().getParameters().containsKey('emailId')
            || ApexPages.currentPage().getParameters().containsKey('email')){
                if(ApexPages.currentPage().getParameters().containsKey('emailId'))
                    emailId=ApexPages.currentPage().getParameters().get('emailId');
                else if(ApexPages.currentPage().getParameters().containsKey('email'))
                    emailId=ApexPages.currentPage().getParameters().get('email');
               
                if(Test.isRunningTest() && bTestFlag)
                    throw new TestException ();             //used during tests only to achieve coverage
                    
                if(emailId.length()>2){
                    System.debug('######'+emailId);
                    //search for Business Contacts and Person Accounts
                    searchResults=[find :emailId IN EMAIL FIELDS RETURNING Contact(id, IsPersonAccount, Name, FirstName, LastName, AccountId,Account.Name, ServiceContractType__c,ContactSupportLevel__c, Title,JobTitle__c,WorkPhone__c,MobilePhone, Email,AEmail__c,  InActive__c)];           
                 
                    allContacts = ((list<Contact>) searchResults[0]);
                   
                    for(Contact objContact: allContacts){
                        if(objContact.IsPersonAccount){
                            allPersonAccounts.add(objContact);
                        }
                        else{
                            allBusinessContacts.add(objContact);
                        }
                    }
                    String SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
                   
                    if(allContacts.size()==0){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CL10080);
                        ApexPages.addMessage(myMsg);
                        emailId = EncodingUtil.urlEncode(emailId, 'UTF-8');
                        //PageReference pg=new PageReference(SfUrl + Label.CLJAN13CCC01 +emailId); 
                        //DEF-3541  Redirect the Search to Contact Search
                        PageReference pg=new PageReference(SfUrl + Label.CLNOV13CCC16 + emailId);
                        pg.setRedirect(true);
                        return pg;
                    }
                    else if(allContacts.size()==1){ // if only one Person Account/Business Contaact found for the Email Id
                        if(allPersonAccounts.size()>0){
                            PageReference pg=new PageReference(SfUrl +'/'+allPersonAccounts[0].id);
                            pg.setRedirect(true);
                            return pg;
                        }
                        else if(allBusinessContacts.size()>0){
                            PageReference pg=new PageReference(SfUrl +'/'+allBusinessContacts[0].id);
                            pg.setRedirect(true);
                            return pg;
                        }
                        else{
                            return null;
                        }
                    }
                    else if(allContacts.size() > 1){
                        if(allPersonAccounts.size()==0){
                            blnDisplayPersonAccount=false;
                        }
                        if(allBusinessContacts.size()==0){
                            blnDisplayBusinessContact=false;
                        }
                        lstAccWrap =  buildWraps(allBusinessContacts ,allPersonAccounts);
                        return null;
                    }
                    else{
                        return null;
                    }
                }
                else{   //Throwing error if Email Id length is <= 2 charactors 
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CL10081);
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }
            else{   
                //Throwing error if Email Id is null
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CL10082);
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Following unexpected error occured. Please contact Adminstrator : '+e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }
    }//End of verifyemailId()
    
    //Wrapper to warp Account and related Contacts
    public class AccounttoContactsWrapper{
        public List<Contact> lstPersonAccount {get;set;}
        public list<Contact> lstBusinessContact  {get;set;} 
        public AccounttoContactsWrapper(List<Contact> lstPersonAccount, list<Contact> lstBusinessContact){
            this.lstPersonAccount = lstPersonAccount;
            this.lstBusinessContact = lstBusinessContact;
        }
    }
    
    //Method to build AccounttoContact wraps to show on the page
    public list<AccounttoContactsWrapper> buildWraps(list<Contact> lstBusinessContact, list<Contact> lstPersonAccount){
        list<AccounttoContactsWrapper>  lstWraps = new list<AccounttoContactsWrapper>();
        AccounttoContactsWrapper acw = new AccounttoContactsWrapper(lstPersonAccount, lstBusinessContact);
        lstWraps.add(acw);
        return lstWraps;
    }    
    
    public class TestException extends Exception {}
}//End of Class