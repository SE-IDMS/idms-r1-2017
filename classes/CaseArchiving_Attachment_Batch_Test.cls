@isTest
private class CaseArchiving_Attachment_Batch_Test{
    
    static testMethod void attachmentBatchtestmethod(){
      Account accounts = Utils_TestMethods.createAccount();
      Database.insert(accounts);
             
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      Database.insert(con);     

      Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'closed');
      Database.insert(caseobj);
      
      ArchiveCase__c AcaseObj = new  ArchiveCase__c();
      AcaseObj.Name='12345';
      Acaseobj.subject__c='Test Case please Ignore';
      Database.insert(AcaseObj);
        
      Attachment attachment = new Attachment(); 
      Blob b = Blob.valueOf('Test Data');   
      attachment.ParentId = caseobj.id;  
      attachment.Name = 'Test Attachment for Parent';  
      attachment.Body = b;  
      insert attachment;
      System.debug('Attachment'+attachment); 
        Test.startTest();
        CaseArchiving_Attachment_Batch caab = new CaseArchiving_Attachment_Batch(); 
        ID batchprocessid =Database.executeBatch(caab);
        Test.stopTest();
    
    }
    static testmethod void attachmentBatchScheduleTestmethod() {
        Test.startTest();
        String CRON_EXP = '0 0 0 1 1 ? 2025';  
        String jobId = System.schedule('Schdule casearchiving_Assignement_Batch', CRON_EXP, new casearchiving_Attachment_BatchSchedule() );
        Test.stopTest();
    }
}