public class VFC_SearchProductCatalog {
    public VCC_IBProductSearch GMRSearchClass{get;set;} // Controller for the search in Product functionality
    public WOGMRSearch WOImplementation{get;set;}

    public VFC_SearchProductCatalog(ApexPages.StandardController controller) {
       GMRSearchClass = new VCC_IBProductSearch();
       WOImplementation = new WOGMRSearch();
    }
    
    public PageReference requestForNewProduct()
    {
       PageReference requestForNewProduct = new PageReference('/'+SObjectType.SVMXC__Installed_Product__c.getKeyPrefix()+'/e'); 
       
       if(WOImplementation.locationInstalledAtId != null && WOImplementation.locationInstalledAtId <> ''){
         requestForNewProduct.getParameters().put(label.CLOCT13SRV52, WOImplementation.locationInstalledAtId);
         requestForNewProduct.getParameters().put(label.CLOCT13SRV53, WOImplementation.locationInstalledAtName);
         requestForNewProduct.getParameters().put('retURL', '/'+WOImplementation.locationId);
       }else if(WOImplementation.caseInstalledAtId != null && WOImplementation.caseInstalledAtId <> ''){
         requestForNewProduct.getParameters().put(label.CLOCT13SRV54, WOImplementation.caseInstalledAtId);
         requestForNewProduct.getParameters().put(label.CLOCT13SRV55, WOImplementation.caseInstalledAtName);
         requestForNewProduct.getParameters().put('retURL', '/'+WOImplementation.caseId);
       }
    
       // Location
       requestForNewProduct.getParameters().put(label.CLOCT13SRV56, WOImplementation.locationId);
       requestForNewProduct.getParameters().put(label.CLOCT13SRV57, WOImplementation.locationName);
       
       return requestForNewProduct;     
    } 
    
    public class WOGMRSearch implements Utils_IBProductSearchMethods
    {
       private String locationId;
       private String caseId;
       private String locationName;
       private String locationInstalledAtId;
       private String locationInstalledAtName;
       private String caseInstalledAtId;
       private String caseInstalledAtName;

        public Void Init(String ObjID, VCC_IBProductSearch Controller)
        {
           
           locationId = System.currentpagereference().getParameters().get('locationid');
           caseId = System.currentpagereference().getParameters().get('caseid');
           
           // From Location
           if(locationId != null && locationId <> ''){
             SVMXC__Site__c location = [select id, name, SVMXC__Account__r.id, SVMXC__Account__r.name from SVMXC__Site__c where id=:locationId limit 1];
             
             if(location != null){
               locationId = location.id;
               locationName = location.Name;
               locationInstalledAtId = location.SVMXC__Account__r.id;
               locationInstalledAtName = location.SVMXC__Account__r.name;
             }
           // Form Case
           }else if(caseId != null && caseId <> ''){
             Case caseObject = [select Account.id, Account.Name, AssetOwner__r.id, AssetOwner__r.Name from Case where id=:caseId limit 1];
             
             if(caseObject != null){
               if(caseObject.AssetOwner__r.id != null){
                 caseInstalledAtId = caseObject.AssetOwner__r.id;
                 caseInstalledAtName = caseObject.AssetOwner__r.Name;
               }else{
                 caseInstalledAtId = caseObject.Account.id;
                 caseInstalledAtName = caseObject.Account.Name;
               }
             }  
           // From Installed Product
           }else if(ObjID != null){    

             SVMXC__Installed_Product__c currentAsset = [Select Id, SVMXC__Product__c, brand2__r.Name, devicetype2__r.Name from SVMXC__Installed_Product__c where Id=:ObjID limit 1];
             Controller.RelatedObject = (SVMXC__Installed_Product__c )currentAsset ;

               // Init of the picklist
               if(!Controller.Cleared && !Controller.SearchHasBeenLaunched) {
                   List<String> filters = new List<String>();
                   filters.add(currentAsset.Brand2__r.Name);
                   filters.add(currentAsset.devicetype2__r.Name);  
                   Controller.filters = filters;
               }
           }            
       }
       
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
       {
           VCC_IBProductSearch GMRController = (VCC_IBProductSearch)ControllerBase;
       
           if(GMRController.RelatedObject !=null){
            
            if(GMRController.ActionType == 'simple')
            {
                SVMXC__Installed_Product__c currentAsset = (SVMXC__Installed_Product__c )GMRController.RelatedObject;
                DataTemplate__c data = (DataTemplate__c)obj;
                
                if(data.field6__c != null && data.field6__c <> ''){
                   Product2  product = [select name, brand2__c, devicetype2__c, categoryId__c from Product2 where id=:data.field6__c]; 
                   currentAsset.brand2__c = product.brand2__c;
                   currentAsset.devicetype2__c = product.devicetype2__c;
                   currentAsset.category__c = product.categoryId__c;
                   currentAsset.BrandToCreate__c = '';
                   currentAsset.DeviceTypeToCreate__c = '';
                   currentAsset.RangeToCreate__c = '';
                   currentAsset.SkuToCreate__c = '';
                   currentAsset.SVMXC__Product__c  = data.field6__c;
                   currentAsset.name = product.name;
                   
                   Database.Update(currentAsset );
                }
            }
 
           PageReference assetPage = new ApexPages.StandardController(GMRController.RelatedObject).view();
           return assetPage;   
         }else{
           PageReference assetCreationPage = new PageReference('/'+SObjectType.SVMXC__Installed_Product__c.getKeyPrefix()+'/e' );
           DataTemplate__c data = (DataTemplate__c)obj;
           Product2  product = [select id, name, brand2__r.id, brand2__r.name, devicetype2__r.id, devicetype2__r.name, categoryid__r.id, categoryid__r.name from Product2 where id=:data.field6__c]; 
           
           if(locationInstalledAtId != null && locationInstalledAtId <> ''){
             assetCreationPage.getParameters().put(label.CLOCT13SRV52, locationInstalledAtId);
             assetCreationPage.getParameters().put(label.CLOCT13SRV53, locationInstalledAtName);
             assetCreationPage.getParameters().put('retURL', '/'+locationId);
           }else if(caseInstalledAtId != null && caseInstalledAtId <> ''){
             assetCreationPage.getParameters().put(label.CLOCT13SRV54, caseInstalledAtId);
             assetCreationPage.getParameters().put(label.CLOCT13SRV55, caseInstalledAtName);
             assetCreationPage.getParameters().put('retURL', '/'+caseId);
           }
    
           // Location
           assetCreationPage.getParameters().put(label.CLOCT13SRV56, locationId);
           assetCreationPage.getParameters().put(label.CLOCT13SRV57, locationName);
           // Product
           assetCreationPage.getParameters().put(label.CLOCT13SRV58, product.id);
           assetCreationPage.getParameters().put(label.CLOCT13SRV59, product.name);
           // Brand
           assetCreationPage.getParameters().put(label.CLOCT13SRV60, product.brand2__r.id);
           assetCreationPage.getParameters().put(label.CLOCT13SRV61, product.brand2__r.name);
           // Device type
           assetCreationPage.getParameters().put(label.CLOCT13SRV62, product.devicetype2__r.id);
           assetCreationPage.getParameters().put(label.CLOCT13SRV63, product.devicetype2__r.name);
           // Range
           assetCreationPage.getParameters().put(label.CLOCT13SRV64, product.categoryid__r.id);
           assetCreationPage.getParameters().put(label.CLOCT13SRV65, product.categoryid__r.name);
           // Installed Product Name defaulted with Product Name 
           assetCreationPage.getParameters().put('Name', product.name);
           system.debug('assetCreationPage:'+assetCreationPage);
           return assetCreationPage;
         }
           
       }
       
       public PageReference Cancel(VCC_IBProductSearch Controller)
       {
           PageReference cancelPage;
           
           if(Controller.RelatedObject != null){
             cancelPage = new ApexPages.StandardController(Controller.RelatedObject).view();
           }else if(locationId != null){
             cancelPage= new PageReference('/'+locationId);
           }else if(caseId != null){
             cancelPage= new PageReference('/'+caseId);
           }else{
             cancelPage = new PageReference('/'+SObjectType.SVMXC__Installed_Product__c.getKeyPrefix()+'/e' );
           }
           return cancelPage ;
       }      
    }

}