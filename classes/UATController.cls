/*
    Author              : Gayathri Shivakumar (Schneider Electric)Champions_Name__c
    Date Created        : 13-Aug-2013
    Description         : Controller for UAT Class 
*/

public with sharing class UATController {

   
public UATController() {         

    }

public UATController(ApexPages.StandardController controller) {
     List<UAT_Results__c> TstRes = new List<UAT_Results__c>();
     
    }


 public string TestId{get;set;}
 public string Recid;
 
public string getRecid(){
Recid = ApexPages.currentPage().getParameters().get('Id');
return Recid;
}

public PageReference ReceiveRecord() {
    system.debug('<<<<<<<<< Test Id >>>>>>>' + TestId);
        return null;
    }

  
    public pagereference SetStatus() {
        AllRecord = 'Yes';
        return  null;
    }


public string Category {get;set;} 
public string Release {get;set;} 
public string AllRecord {get;set;}
public Integer recordLimit = 10;
public Integer offSetLimit = 0; 

// UAT Test Scenarios List

public List<User_Acceptance_Test__c> getUATData(){
     List<User_Acceptance_Test__c> uat = new List<User_Acceptance_Test__c>();
     if( AllRecord == 'Yes')
       uat = [Select Name,Action__c,Business_Requirement__r.Name,Category__c,Comments__c,Data_Inputs__c,Data_Outputs__c,Description__c,Key_Testing_Points__c,Status__c,Target_Release__c,Test_Cycle_Steps__c,Test_Scenario_Name__c,User_Role__c from User_Acceptance_Test__c where Category__c = :Category and Target_Release__c = :Release LIMIT :recordLimit OFFSET :offSetLimit];
     else
      uat = [Select Name,Action__c,Business_Requirement__r.Name,Category__c,Comments__c,Data_Inputs__c,Data_Outputs__c,Description__c,Key_Testing_Points__c,Status__c,Target_Release__c,Test_Cycle_Steps__c,Test_Scenario_Name__c,User_Role__c from User_Acceptance_Test__c where Category__c = :Category and Target_Release__c = :Release  and Status__c = :'Open' LIMIT :recordLimit OFFSET :offSetLimit];
      return uat;
      
     }
   // List of Applications
      
    public List<SelectOption> getFilterList() 
    {
    
    // Declarations

    List<User_Acceptance_Test__c> lstUAT = new List<User_Acceptance_Test__c>();
    lstUAT = [SELECT id, Category__c from  User_Acceptance_Test__c]; 
    Set<String> SetApplication = new Set<String>();
       
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','<Select an Application>'));     
     
      for(User_Acceptance_Test__c ct:lstUAT)
       {
       
          if(ct.Category__c != null)
            SetApplication.add(ct.Category__c);
        }
          
       for(String ct:SetApplication)
        {
            options.add(new SelectOption(ct,ct)); 
          
       }    
        options.sort();          
        return options;
    }
    
    
 public List<SelectOption> getFilterRelease() 
    {
    
    // Declarations

    List<User_Acceptance_Test__c> lstUAT = new List<User_Acceptance_Test__c>();
    lstUAT = [SELECT id, Target_Release__c  from  User_Acceptance_Test__c]; 
    Set<String> SetReleases = new Set<String>();
       
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','<Select a Release>'));     
     
      for(User_Acceptance_Test__c rel:lstUAT)
       {
       
          if(rel.Target_Release__c != null)
            SetReleases.add(rel.Target_Release__c);
        }
          
       for(String rel:SetReleases)
        {
            options.add(new SelectOption(rel,rel)); 
          
       }    
        options.sort();          
        return options;
    }
    
  public pagereference FilterChange()
    {
        AllRecord = 'No';
        return null;  
    }  
   
   User_Acceptance_Test__c DetTst{get;set;}
  
  public User_Acceptance_Test__c getDetTst()
  {
    return DetTst;
  }
   public  List<UAT_Results__c> TstRes{get;set;}
  public pagereference DisplayTestCaseDetail()
  {
    User_Acceptance_Test__c td = new User_Acceptance_Test__c();
    string tid = ApexPages.currentPage().getParameters().get('Id');
    system.debug('************' + tid);
    if (tid !=''){
        td = [Select id, Name,Action__c,Business_Requirement__r.Name,Business_Requirement__r.id,Category__c,Comments__c,Data_Inputs__c,Data_Outputs__c,Description__c,Key_Testing_Points__c,Status__c,Target_Release__c,Test_Cycle_Steps__c,Test_Scenario_Name__c,User_Role__c from User_Acceptance_Test__c where Name = :tid ];
        DetTst = td;
        }
    return null;    
  }
  public boolean displayPopup {get; set;}
  public boolean displayPopup2 {get; set;}
  public UAT_Results__c usrresult{get;set;}
  
  
    public void closePopup() {
       
        displayPopup = false;
    }
   
    public void showPopup() {       
        usrresult = new UAT_Results__c();      
        displayPopup = true;
    }
    
   public void closePopup2() {
       
        displayPopup2 = false;
    }
    
    
   public pagereference Save()
   {
    UAT_Results__c results = new UAT_Results__c(); 
     
  if(usrresult.Tested_As__c != null && usrresult.Test_Result__c !=null){
    
        results.User_Acceptance_Test__c = DetTst.id;
        results.Tested_As__c = usrresult.Tested_As__c;
        results.Test_Result__c = usrresult.Test_Result__c;
        results.Comments__c = usrresult.Comments__c;
        insert results; 
       }
    
    displayPopup = false;
    return null;
    }
    
    Public pagereference Next(){
         offSetLimit = offSetLimit + 10;
         return null;
     }
     
    Public pagereference Previous(){
         if(offSetLimit > 0)
             offSetLimit = offSetLimit - 10;
         return null;
     }
 // History pop up close    
  public PageReference Close() {
    displayPopup2 = false;
        return null;
    }

 // View History 
 
    public List<UAT_Results__c> viewres{get;set;}
    public PageReference ViewHistory() {
    
    viewres = new List<UAT_Results__c>(); 
    viewres = [Select Test_Cycle_Name__c,User_Acceptance_Test__r.Name,Comments__c,Tested_As__r.Name,Test_Result__c from UAT_Results__c where User_Acceptance_Test__r.Name = :Recid];
     displayPopup2 = true;
        return null;
    }

      
 }