@isTest
public class AP_ProcessFeedComment_Test{
    
    public static testMethod void CreateData(){
        set<ContainmentAction__c> setXA1 = new  set<ContainmentAction__c>();
        set<ID> setXAId = new set<ID>();
        List<User> lstUser = new List<User>();
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        User newUser = Utils_TestMethods.createStandardUser('TestUser');    
        newUser.UserRoleID = rol_ID ;
        system.debug(newUser.UserRoleID +'shiv@deep' + rol_ID);
        lstUser.add(newUser);

         User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');  
         newUser2.BypassVR__c =TRUE;
         newUser2.UserRoleID = rol_ID ;
         lstUser.add(newUser2);

        Insert lstUser;
         Country__c newCountry = Utils_TestMethods.createCountry();
         newCountry.Region__c=Label.CL00320;
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
           
         if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
         
         Account acct = Utils_TestMethods.createAccount();
         acct.RecordtypeID = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name != 'Supplier' limit 1].Id;
         Database.SaveResult AccountInsertResult = Database.insert(acct, false);
           
        if(!AccountInsertResult.isSuccess())
            Database.Error err = AccountInsertResult.getErrors()[0];
        
         system.runas(newUser2){
             BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
             Database.insert(brEntity);
             BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
             Database.insert(brSubEntity);
             BusinessRiskEscalationEntity__c oResolutionBREE = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();            
             oResolutionBREE.Location__c = ''; 
             Database.insert(oResolutionBREE,false);
             BusinessRiskEscalationEntity__c oOriginatingBREE = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocationReturnCenter();
             Database.insert(oOriginatingBREE);
             
             List<BusinessRiskEscalations__c> lsBRE = new List<BusinessRiskEscalations__c>();
             
             BusinessRiskEscalations__c BRE1 =  Utils_TestMethods.createBusinessRiskEscalation(acct.ID,newCountry.ID,newUser2.ID);
             
             BRE1.ResolutionOrganisation__c = oResolutionBREE.ID; 
             BRE1.OriginatingOrganisation__c =  oOriginatingBREE.ID; 
             BRE1.ResolutionLeader__c = newUser.ID;
             BRE1.BusinessRiskSponsor__c = newUser.ID;  
             lsBRE.add(BRE1);
            
             BusinessRiskEscalations__c BRE4 =  Utils_TestMethods.createBusinessRiskEscalation(acct.ID,newCountry.ID,newUser2.ID);
             BRE4.ResolutionLeader__c = newUser.ID;
             BRE4.BusinessRiskSponsor__c = newUser.ID;
             lsBRE.add(BRE4);
             insert lsBRE;
        
             FeedItem fi = new FeedItem(ParentId=lsBRE[0].ID, Body='Test');
             insert fi;
            
             FeedComment fc = new FeedComment(FeedItemId=fi.Id, CommentBody ='Test' );
             insert fc;
            /*
            FeedComment ofc = new FeedComment(ID = fc.Id, CommentBody ='TestTest' );
            update ofc;
            Error: Compile Error: DML operation UPDATE not allowed on FeedComment at line 75 column 13 
            */
         }
    }
    
    public static testMethod void CreateData1(){
        set<BusinessRiskEscalations__c> setBRE1 = new  set<BusinessRiskEscalations__c>();
        set<ID> setBREId = new set<ID>();
        List<User> lstUser = new List<User>();
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        User newUser = Utils_TestMethods.createStandardUser('TestUser');    
        newUser.UserRoleID = rol_ID ;
        system.debug(newUser.UserRoleID +'shiv@deep' + rol_ID);
        lstUser.add(newUser);

         User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');  
            newUser2.BypassVR__c =TRUE;
            newUser2.UserRoleID = rol_ID ;
            lstUser.add(newUser2);

        Insert lstUser;
         Country__c newCountry = Utils_TestMethods.createCountry();
              newCountry.Region__c=Label.CL00320;
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
           
         if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
         
         Account acct = Utils_TestMethods.createAccount();
         acct.RecordtypeID = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name != 'Supplier' limit 1].Id;
         Database.SaveResult AccountInsertResult = Database.insert(acct, false);
           
        if(!AccountInsertResult.isSuccess())
            Database.Error err = AccountInsertResult.getErrors()[0];
        
         system.runas(newUser2){
             BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
             Database.insert(brEntity);
             BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
             Database.insert(brSubEntity);
             BusinessRiskEscalationEntity__c AccOrg = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();            
             Database.insert(AccOrg,false);
            
             List<CS_Severity__c> lstCS = new List<CS_Severity__c>();
             CS_Severity__c CS1 = new CS_Severity__c(Name = 'Minor', Value__c = 1);
             lstCS.add(CS1);
             CS_Severity__c CS2 = new CS_Severity__c(Name = 'Safety related', Value__c = 10);
             lstCS.add(CS2);
             Database.insert(lstCS);
             
             Problem__c prob1 = Utils_TestMethods.createProblem(AccOrg.Id,12);
             prob1.ProblemLeader__c = newUser2.Id;
             prob1.ProductQualityProblem__c = false;
             prob1.RecordTypeid = Label.CLI2PAPR120014;
             prob1.Sensitivity__c = 'Public';
             prob1.Severity__c = 'Safety related';
             Database.insert(prob1);
             
             List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
             ContainmentAction__c XA1 = Utils_TestMethods.createContainmentAction(prob1.Id,AccOrg.Id);
             lstXA.add(XA1);
             ContainmentAction__c XA2 = Utils_TestMethods.createContainmentAction(prob1.Id,AccOrg.Id);
             lstXA.add(XA2);
             Database.insert(lstXA);
                     
             FeedItem fi = new FeedItem(ParentId=lstXA[0].ID, Body='Test');
             insert fi;
            
             FeedComment fc = new FeedComment(FeedItemId=fi.Id, CommentBody ='Test' );
             insert fc;
            /*
            FeedComment ofc = new FeedComment(ID = fc.Id, CommentBody ='TestTest' );
            update ofc;
            Error: Compile Error: DML operation UPDATE not allowed on FeedComment at line 75 column 13 
            */
         }
    }
}