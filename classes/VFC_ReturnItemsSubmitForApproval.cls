Public class VFC_ReturnItemsSubmitForApproval
{ 
  Public id XaId;
  Public boolean blnvisible{get;set;}
  Public boolean blnvisible1{get;set;}
  Public RMA__c ObjRMA{get;set;} 
  public string errormessage{get;set;}
  Public map<id,String> mapStatusApproval;
  list<ProcessInstance> ProcessInstance; 
  list<RMA_Product__c> lstRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> templstRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> templstRIs1 = new List<RMA_Product__c>();
  list<RMA_Product__c> xListRIs = new List<RMA_Product__c>();  
  // list<RMA_Product__c> lstOtherRIs = new List<RMA_Product__c>();
  //===============================================================
  list<RMA_Product__c> lstLocalRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> lstGlobalRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> templstGlobalRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> lstOtherRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> templstOtherRIs = new List<RMA_Product__c>();
  list<RMA_Product__c> tmplst = new List<RMA_Product__c>();
  // Map< RMA_Product__c, > MapRIToApproverLevels = new Map<Id, Set<Id>>();
  Map<string, List<RMA_Product__c>> mapAllRIs = new Map<string, List<RMA_Product__c>>();
  Map<string, List<RMA_Product__c>> mapLocalRIs = new Map<string, List<RMA_Product__c>>();
  Map<string, List<RMA_Product__c>> mapGlobalRIs = new Map<string, List<RMA_Product__c>>();
  Map<string, List<RMA_Product__c>> mapOtherRIs = new Map<string, List<RMA_Product__c>>();
  Boolean LocalFlag = False;
  Boolean GlobalFlag = False;

  //===============================================================

  Set<Id> RIidswithApprovalProcess = new set<Id>();
  Map<Id, Set<Id>> MapRIToApproverLevels = new Map<Id, Set<Id>>();
  Set<Id> RIidswithDirectApproval = new set<Id>();

  List<ReturnItemsApprovalMatrix__c> lstRIAM = new List<ReturnItemsApprovalMatrix__c>();

    public VFC_ReturnItemsSubmitForApproval(ApexPages.StandardController controller)
     {
     mapStatusApproval=new Map<id,String>();
     ObjRMA=new RMA__c();
     blnvisible=false;
     RMA__c rma=new RMA__c();
     rma= (RMA__c )controller.getRecord();
     
     ObjRMA=[select id,Status__c,TECH_CheckReturnAddressOnRI__c, Approver__c,Case__r.Account.Country__c, Case__r.SupportCategory__c, Case__r.Symptom__c, Case__r.SubSymptom__c from RMA__c where Id=:rma.id]; 
    lstRIs = [Select id, Tech_GMRCode__c,RMA__c,WarrantyStatus__c, CustomerRepairPONumber__c,AccountToBeInvoicedForTheRepair__c ,RMA__r.CustomerRepairPONumber__c  , RMA__r.Case__r.Account.Country__c, RMA__r.Case__r.SubSymptom__c,RMA__r.Case__r.Symptom__c,RMA__r.Case__r.SupportCategory__c,CustomerResolution__c, Name, ProductFamily__c,ApproverLevel1__c, ApproverLevel2__c, ApprovalStatus__c from RMA_Product__c where RMA__c =: rma.id AND ApprovalStatus__c=:null];
     //lstRIs = [Select id, RMA__c, CustomerRepairPONumber__c,AccountToBeInvoicedForTheRepair__c ,RMA__r.Case__r.Account.Country__c, RMA__r.Case__r.SubSymptom__c,RMA__r.Case__r.Symptom__c,RMA__r.Case__r.SupportCategory__c,CustomerResolution__c, Name, ProductFamily__c,ApproverLevel1__c, ApproverLevel2__c, ApprovalStatus__c from RMA_Product__c where RMA__c =: rma.id ];
     System.debug('### ObjRMA ###'+ ObjRMA);
     System.debug('### lstRIs ###'+lstRIs);
     
     // Get all Approval Matrix Records which are active.
     lstRIAM = [ Select id,Tech_GMRCode__c, AccountCountry__c, Category__c, CommercialReference__c, CustomerResolution__c, ProductFamily__c, Reason__c,SubReason__c,ApproverLevel1__c, ApproverLevel2__c, RuleActive__c, TECH_CustomerResolution__c from ReturnItemsApprovalMatrix__c where RuleActive__c=True];
     System.debug('### lstRIAM ###'+lstRIAM);
     System.debug('### lstRIAM SIZE ###'+lstRIAM.SIZE());
     
     // call method to get the list of returnitems which will undergo Approval Process.    
     //xListRIs = getReturnItems(lstRIs,lstRIAM);
      
      mapAllRIs = getReturnItems(lstRIs,lstRIAM);
      system.debug('### mapAllRIs ###' + mapAllRIs);
      if(mapAllRIs.size()>0)
      {
         for(string type: mapAllRIs.keyset())
         {
            if(type == Label.CLMAY13RR06)
            {
                lstOtherRIs = mapAllRIs.get(type);
            }
            else
            {
               if(xListRIs.size()>0 || xListRIs!=null)
               {
                  xListRIs.addall(mapAllRIs.get(type));
               }
               else
               {
                  xListRIs= mapAllRIs.get(type);
                }
           }
        }
        system.debug('### xListRIs ###' + xListRIs); 
        
    }
     
    
     // Get all ProcessInstances which are Pending
     ProcessInstance=[SELECT Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId FROM ProcessInstance where TargetObjectId in: RIidswithApprovalProcess and status='Pending'];    
     system.debug('### prcsinstance ###'+ ProcessInstance);  

     }


    Public Map<string, List<RMA_Product__c>> getReturnItems(List<RMA_Product__c> lstRIs,List<ReturnItemsApprovalMatrix__c> lstRIAM)
    {
      map<string, List<RMA_Product__c>> mapLocalRIs = new map<string, List<RMA_Product__c>>();
      for(RMA_Product__c tempRI: lstRIs)
      {
         for(ReturnItemsApprovalMatrix__c tempAM: lstRIAM)
         {
                // tempAM.CustomerResolution__c == tempRI.CustomerResolution__c &&
                // If in the approval Matrix, all values matches up with the RI item values and that exact match - Its a Local rule.
              system.debug('## tempAM ##'+ tempAM);
              system.debug('## tempRI ##'+ tempRI);
              System.debug('## Commercial Reference'+ tempAM.CommercialReference__c + '==' + tempRI.Name );
              System.debug('## CustomerResolution'+ tempAM.TECH_CustomerResolution__c + '==' + tempRI.CustomerResolution__c );
              System.debug('## GMR Code'+ tempAM.Tech_GMRCode__c + '==' + tempRI.Tech_GMRCode__c );
              System.debug('## Account Country'+ tempAM.AccountCountry__c + '==' + tempRI.RMA__r.Case__r.Account.Country__c );
              System.debug('## Category'+ tempAM.Category__c + '==' + tempRI.RMA__r.Case__r.SupportCategory__c );
              System.debug('## Reason ##'+ tempAM.Reason__c + '==' + tempRI.RMA__r.Case__r.Symptom__c );
              System.debug('## SubReason ##'+ tempAM.SubReason__c + '==' + tempRI.RMA__r.Case__r.SubSymptom__c );

              // -- sukumar salla -----
               List<String> CRs = tempAM.TECH_CustomerResolution__c.split(';');
               Set<String> setCRs = new Set<String>();
               setCRs.addall(CRs);
               system.debug('### check CR - Local Rule ##'+ setCRs.contains(tempRI.CustomerResolution__c));
              // --- end -------------

              If(  
                tempAM.CommercialReference__c == tempRI.Name && 
                setCRs.contains(tempRI.CustomerResolution__c) &&
                //tempAM.TECH_CustomerResolution__c.contains(tempRI.CustomerResolution__c) &&                
                //tempAM.ProductFamily__c == tempRI.ProductFamily__c &&
                tempAM.Tech_GMRCode__c!=null&&tempRI.Tech_GMRCode__c!=null&&
                (tempAM.Tech_GMRCode__c).substring(0, Integer.valueof(Label.CLOCT14I2P03)) == (tempRI.Tech_GMRCode__c).substring(0,Integer.valueof(Label.CLOCT14I2P03)) &&   // Label.CLOCT14I2P03 = 8 ; Oct2014 Release - Sukumar Salla
                tempAM.AccountCountry__c == tempRI.RMA__r.Case__r.Account.Country__c &&
                tempAM.Category__c == tempRI.RMA__r.Case__r.SupportCategory__c &&
                tempAM.Reason__c == tempRI.RMA__r.Case__r.Symptom__c &&
                tempAM.SubReason__c == tempRI.RMA__r.Case__r.SubSymptom__c
              )
                {
                   //system.debug('CommercialReference__c'+ tempAM.CommercialReference__c + == + tempRI.Name );
                    LocalFlag = True;
                    RIidswithApprovalProcess.add(tempRI.ID);
                    tempRI.ApproverLevel1__c = tempAM.ApproverLevel1__c;
                    tempRI.ApproverLevel2__c = tempAM.ApproverLevel2__c;
                    //tempRI.ApprovalStatus__c = Label.CLMAY13RR08;
                    lstLocalRIs.add(tempRI);
                    // CLMAY13RR05 - Local
                    if(mapLocalRIs.Containskey(Label.CLMAY13RR05))
                    {
                      mapLocalRIs.get(Label.CLMAY13RR05).add(tempRI);
                    }
                    else
                    {
                      mapLocalRIs.put(Label.CLMAY13RR05, new List<RMA_Product__c>());
                      mapLocalRIs.get(Label.CLMAY13RR05).add(tempRI);
                    }     

                }
         }
           
         If(LocalFlag==False)
         {
            templstGlobalRIs.add(tempRI);
            if(mapLocalRIs.Containskey(Label.CLMAY13RR04))
                {
                   mapLocalRIs.get(Label.CLMAY13RR04).add(tempRI);
                }
                else
                {
                  mapLocalRIs.put(Label.CLMAY13RR04, new List<RMA_Product__c>());
                  mapLocalRIs.get(Label.CLMAY13RR04).add(tempRI);
                } 
         }
         else
         {
            LocalFlag = False;
         } 
      }
      system.debug('### RIidswithApprovalProcess ###' + RIidswithApprovalProcess);
      system.debug('### templstGlobalRIs###' + templstGlobalRIs);
      system.debug('### mapLocalRIs ###' + mapLocalRIs);
      system.debug('mapLocalRIs.get(Label.CLMAY13RR04)'+ mapLocalRIs.get(Label.CLMAY13RR04));
      if(mapLocalRIs!=null && mapLocalRIs.get(Label.CLMAY13RR04)!=null)
      {
         
         map<string, List<RMA_Product__c>> tmpmapRIs = checkGlobalRIs(mapLocalRIs.get(Label.CLMAY13RR05),mapLocalRIs.get(Label.CLMAY13RR04), lstRIAM);
         mapLocalRIs.remove(Label.CLMAY13RR04);
         if(tmpmapRIs.size()>0)
         {
            for(string x:tmpmapRIs.keyset())
            {
                if(tmpmapRIs.containskey(x))
                {
                    if(mapLocalRIs.Containskey(x))
                    {
                      mapLocalRIs.get(x).addall(tmpmapRIs.get(x));
                    }
                    else
                    {
                      mapLocalRIs.put(x, new List<RMA_Product__c>());
                      mapLocalRIs.get(x).addall(tmpmapRIs.get(x));
                    }
                    
                }
            }
         }
        //lstLocalRIs.addall(tmplst);
        return mapLocalRIs;        
      }
      else
      {
        return mapLocalRIs;
      }
      
      
    }

  Public  map<string, List<RMA_Product__c>> checkGlobalRIs(List<RMA_Product__c>lstLocalRIs, List<RMA_Product__c> templstGlobalRIs, List<ReturnItemsApprovalMatrix__c> lstRIAM)
  {
    map<string, List<RMA_Product__c>> mapRIs = new map<string, List<RMA_Product__c>>();
    List<ReturnItemsApprovalMatrix__c> tempRIAMs = new List<ReturnItemsApprovalMatrix__c>();
    system.debug('### templstGlobalRIs ##'+templstGlobalRIs);
    for(RMA_Product__c tempRI: templstGlobalRIs)
    {
       system.debug('### tempRI##'+tempRI);
       for(ReturnItemsApprovalMatrix__c tempAM : lstRIAM)
       {
           // -- sukumar salla -----
               List<String> CRs = tempAM.TECH_CustomerResolution__c.split(';');
               Set<String> setCRs = new Set<String>();
               setCRs.addall(CRs);
               system.debug('### check CR - Global Rule ##'+ setCRs.contains(tempRI.CustomerResolution__c));
              // --- end -------------
       
          //==========================================================================
           system.debug('For Global RULE');
            system.debug('## tempAM ##'+ tempAM);
                    system.debug('## tempRI ##'+ tempRI);
                    System.debug('## Commercial Reference'+ tempAM.CommercialReference__c + '==' + tempRI.Name );
                System.debug('## CustomerResolution'+ tempAM.TECH_CustomerResolution__c + '==' + tempRI.CustomerResolution__c );
                   System.debug('## GMR Code'+ tempAM.Tech_GMRCode__c + '==' + tempRI.Tech_GMRCode__c );
                     System.debug('## Account Country'+ tempAM.AccountCountry__c + '==' + tempRI.RMA__r.Case__r.Account.Country__c );
                    System.debug('## Category'+ tempAM.Category__c + '==' + tempRI.RMA__r.Case__r.SupportCategory__c );
                    System.debug('## Reason ##'+ tempAM.Reason__c + '==' + tempRI.RMA__r.Case__r.Symptom__c );
                  System.debug('## SubReason ##'+ tempAM.SubReason__c + '==' + tempRI.RMA__r.Case__r.SubSymptom__c );
       
       
         //=========================================================================
       
          If((tempAM.CommercialReference__c == tempRI.Name || tempAM.CommercialReference__c == null || tempRI.Name==null ) && 
                (setCRs.contains(tempRI.CustomerResolution__c)|| tempAM.TECH_CustomerResolution__c == null || tempRI.CustomerResolution__c ==null) &&             
                  ( ( tempAM.Tech_GMRCode__c!=null && tempRI.Tech_GMRCode__c!=null &&(tempAM.Tech_GMRCode__c).substring(0,Integer.valueof(Label.CLOCT14I2P03)) == (tempRI.Tech_GMRCode__c).substring(0,Integer.valueof(Label.CLOCT14I2P03))  )|| tempAM.ProductFamily__c == null || tempRI.ProductFamily__c == null ) &&
                (tempAM.AccountCountry__c == tempRI.RMA__r.Case__r.Account.Country__c || tempAM.AccountCountry__c == null) &&
                (tempAM.Category__c == tempRI.RMA__r.Case__r.SupportCategory__c || tempAM.Category__c == null ||  tempRI.RMA__r.Case__r.SupportCategory__c == null) &&
                (tempAM.Reason__c == tempRI.RMA__r.Case__r.Symptom__c || tempAM.Reason__c == null || tempRI.RMA__r.Case__r.Symptom__c == null) &&
                (tempAM.SubReason__c == tempRI.RMA__r.Case__r.SubSymptom__c || tempAM.SubReason__c == null || tempRI.RMA__r.Case__r.SubSymptom__c == null )
                )
            {
                GlobalFlag = True;
                RIidswithApprovalProcess.add(tempRI.ID);
                tempRIAMs.add(tempAM);    
            }

       }
       // sukumar code - 12june2013 ====================================================
       String TECHGMRCodeRI = null;
       If (tempRI.Tech_GMRCode__c!=null) TECHGMRCodeRI = (tempRI.Tech_GMRCode__c).substring(0,Integer.valueof(Label.CLOCT14I2P03));
       String TECHGMRCodeRIAM;
       String KeywordStringRI = tempRI.Name + '~' + TECHGMRCodeRI + '~' + tempRI.RMA__r.Case__r.Account.Country__c +'~' + tempRI.RMA__r.Case__r.SupportCategory__c + '~' + tempRI.RMA__r.Case__r.Symptom__c + '~' + tempRI.RMA__r.Case__r.SubSymptom__c;
       system.debug('## KeywordStringRI ##'+ KeywordStringRI);
       map<ReturnItemsApprovalMatrix__c, Integer> mapCountMatch = new map<ReturnItemsApprovalMatrix__c, Integer>();
       if(GlobalFlag==True)
       {
           if(tempRIAMs.size()>1)
           {
               for (ReturnItemsApprovalMatrix__c RIAM : tempRIAMs)
               {
                    if( tempRI.CustomerResolution__c!=null)
                        {
                            If (RIAM.Tech_GMRCode__c!=null) 
                            {
                               TECHGMRCodeRIAM = (RIAM.Tech_GMRCode__c).substring(0,Integer.valueof(Label.CLOCT14I2P03));
                            }
                            else
                            {
                               TECHGMRCodeRIAM = null;
                            }
                            String KeywordStringRIAM = RIAM.CommercialReference__c+'~'+TECHGMRCodeRIAM+'~'+RIAM.AccountCountry__c+'~'+RIAM.Category__c +'~'+ RIAM.Reason__c+'~'+RIAM.SubReason__c;
                            system.debug('## KeywordStringRIAM ##'+ KeywordStringRIAM );
            
                            Integer Count=0;
                            // -- sreedhar jagannath -----
                            if (RIAM.CommercialReference__c!=null){
                            Count = KeywordStringRI.countmatches(RIAM.CommercialReference__c);
                            }
                            if (RIAM.Tech_GMRCode__c!=null){
                            Count+= KeywordStringRI.countmatches(TECHGMRCodeRIAM);
                            }
                            if (RIAM.AccountCountry__c!=null){        
                            Count+= KeywordStringRI.countmatches(RIAM.AccountCountry__c);
                            }
                            if (RIAM.Category__c!=null) {
                            Count+= KeywordStringRI.countmatches(RIAM.Category__c);
                            }
                            if (RIAM.Reason__c!=null){
                             Count+= KeywordStringRI.countmatches(RIAM.Reason__c);
                            }
                            if (RIAM.SubReason__c!=null){
                            Count+= KeywordStringRI.countmatches(RIAM.SubReason__c);    
                            }
                            // --- end -------------
                            mapCountMatch.put(RIAM, Count);
                            system.debug('## mapCountMatch ##'+ mapCountMatch);
                        }
                }
            list<Integer> counts = new List<Integer>();
            counts=mapCountMatch.values();
            Integer HigherCount;
            if (counts.size()>0)  
            HigherCount = counts[0];
            for( integer i=0; i<counts.size(); i++)
            {
                    if(counts[i] > HigherCount)
                    {
                        HigherCount = counts[i];
                    }
            }
            system.debug('## HigherCount ##'+ HigherCount);
            for(ReturnItemsApprovalMatrix__c R: mapCountMatch.Keyset())
            {
                if (HigherCount== mapCountMatch.get(R))
                {
                    tempRI.ApproverLevel1__c = R.ApproverLevel1__c;
                    tempRI.ApproverLevel2__c = R.ApproverLevel2__c;
                }
            }
       }
       else
       {
           tempRI.ApproverLevel1__c = tempRIAMs[0].ApproverLevel1__c;
           tempRI.ApproverLevel2__c = tempRIAMs[0].ApproverLevel2__c;
       }
        lstGlobalRIs.add(tempRI);
        if(mapRIs.Containskey(Label.CLMAY13RR04))
                {
                   mapRIs.get(Label.CLMAY13RR04).add(tempRI);
                }
                else
                {
                  mapRIs.put(Label.CLMAY13RR04, new List<RMA_Product__c>());
                  mapRIs.get(Label.CLMAY13RR04).add(tempRI);
                 } 
       }
       // sukumar code - end ============================================================
       
       system.debug('##@@ GlobalFlag@@##'+GlobalFlag);
       If(GlobalFlag==False)
         {
            // If GlobalFlag is False -> Its a Direct Approval.
            //lstOtherRIs.add(tempRI);
           
             tempRI.ApprovalStatus__c = Label.CLMAY13RR07;   // CLMAY13RR07 - Approved
             templstOtherRIs.add(tempRI);
              system.debug('##@@ templstOtherRIs @@##'+templstOtherRIs);
              //CLMAY13RR06 - Others
             if(mapRIs.Containskey(Label.CLMAY13RR06))
                {
                   mapRIs.get(Label.CLMAY13RR06).add(tempRI);

                }
                else
                {
                  mapRIs.put(Label.CLMAY13RR06, new List<RMA_Product__c>());
                  mapRIs.get(Label.CLMAY13RR06).add(tempRI);
                }     
         }
         else
         {
            GlobalFlag = False;
         }

    }
    
    system.debug('## mapRIs ##'+mapRIs);
    system.debug('## lstGlobalRIs##'+lstGlobalRIs);
    system.debug('## templstOtherRIs##'+templstOtherRIs);
          If(mapRIs.size()>0)
          {
            return mapRIs;
          }
          else
          {
            return null;
          }
  }  
  

  Public Pagereference processDetails()
  {
     Pagereference pr;
     Boolean ProcessFlag = False;
     system.debug('##ProcessInstance.size()##'+ProcessInstance.size());
     
     if (ObjRMA.Status__c == Label.CLMAY13RR13)
     {
     system.debug('$$ ProcessFlag $$'+ ProcessFlag );
              blnvisible=false;
              errormessage=Label.CLDEC12RR22;
              // CLDEC12RR30 - The record cannot be submitted for approval. Please contact your delegated admin
              pr = null;
              return pr;
     
     }
     If(ProcessInstance.size()==0 || ObjRMA.Status__c != Label.CLMAY13RR13 ) //submitted - CLMAY13RR13
        {
            if(ObjRMA.Status__c == Label.CLDEC12RR24 || ObjRMA.Status__c == Label.CLMAY13RR14 )
           // if(ObjRMA.Status__c == Label.CLDEC12RR24 )  // If status = validated or Rejected
                {
                    errormessage=Label.CLDEC12RR18;                 
                    pr = null;
                    return pr;
                }
                 
               if(lstRIs.Size()==0)
               {
                 errormessage=Label.CLDEC12RR20;
                  // Should have atleast one return Item.
                 pr = null;
                 return pr;
               }   

            // To check the RI with REP+TEX OR REP
            if(mapAllRIs.size()>0)
              {
              for(List<RMA_Product__c> lstRI: mapAllRIs.values())
              {
               for(RMA_Product__c xList: lstRI )
               {
                // If((xList.CustomerResolution__c==Label.CLMAY13RR10||xList.CustomerResolution__c==Label.CLMAY13RR11 )&&(xList.CustomerRepairPONumber__c==null || xList.AccountToBeInvoicedForTheRepair__c == null))
                // If((xList.CustomerResolution__c==Label.CLMAY13RR10||xList.CustomerResolution__c==Label.CLMAY13RR11 )&&(xList.CustomerRepairPONumber__c==null ))
                //If((xList.CustomerResolution__c==Label.CLMAY13RR10||xList.CustomerResolution__c==Label.CLMAY13RR11 )&&(xList.RMA__r.CustomerRepairPONumber__c==null ))
                if (xList.WarrantyStatus__c==null) //** Ram chilukuri added the below if condition as part of connectors release 2014 April.
                {
                    blnvisible=false;
                     errormessage= Label.CLAPR14RRC06;
                     
                     pr = null;
                     return pr;
                } //**              
                else If((xList.WarrantyStatus__c==Label.CLMAY13RR10 )&&(xList.RMA__r.CustomerRepairPONumber__c==null ))
                  {
                     blnvisible=false;
                     errormessage= Label.CLMAY13RR09;
                     
                     pr = null;
                     return pr;
                  }
               }
              }
            }

           // Check all the return Items and if atleast one returnitem status is also open then start the approval process.
           for( RMA_Product__c temp: xListRIs)
           {
             if ( temp.ApprovalStatus__c == null)   // If status = open
             {
                If(temp.ApproverLevel1__c!=null || temp.ApproverLevel2__c!=null)
                {
                      temp.ApprovalStatus__c = Label.CLMAY13RR08;
                      ProcessFlag = True;
                }                
             }
           }
           system.debug('##ProcessFlag##'+ ProcessFlag);
           If (ProcessFlag == True &&  !((lstOtherRIs.size()>0 || lstOtherRIs!= null ) && (xListRIs.size()==0 || xListRIs == null)) )
           {
            system.debug('#####ProcessFlag##'+ ProcessFlag);
            blnvisible=true;
            errormessage=Label.CLDEC12RR21;
            // CLDEC12RR21 - Once you submit this record for approval, you might not be able to edit it, or recall it from approval process depending on your setting.Continue?
            pr = null;
            return pr;
           }
           else
           {

             if(ProcessFlag == False &&  (lstOtherRIs.size()>0 || lstOtherRIs!= null) )
             {
                system.debug('$$ lstOtherRIs $$'+ lstOtherRIs);
                update lstOtherRIs;
                PageReference pr1 = new PageReference('/'+ObjRMA.id);
                pr1.setRedirect(true);
                return pr1;
                //pr = null;
                //return pr;
             }
             else 
             {
              system.debug('$$ ProcessFlag $$'+ ProcessFlag );
              blnvisible=false;
              errormessage=Label.CLDEC12RR30;
              // CLDEC12RR30 - The record cannot be submitted for approval. Please contact your delegated admin
              pr = null;
              return pr;
             }
             
           }
           
        }
         else
        {
            blnvisible=false;
            errormessage= Label.CLDEC12RR22;
            //This Return Request is already submitted for approval.
            pr = null;
        }
     return pr;
  }
   Public PageReference  doApprove()
   {   
        //Pagereference pr1;
        /*
         if(mapAllRIs.size()>0)
          {
            for(List<RMA_Product__c> lstRI: mapAllRIs.values())
            {
               for(RMA_Product__c xList: lstRI )
               {
               If((xList.CustomerResolution__c==Label.CLMAY13RR10||xList.CustomerResolution__c==Label.CLMAY13RR11 )&&(xList.CustomerRepairPONumber__c==null || xList.AccountToBeInvoicedForTheRepair__c == null))
               {
                  blnvisible=false;
                  errormessage= Label.CLMAY13RR09;
                  //This Return Request is already submitted for approval.
                   pr1 = null;
                   return pr1;
               }
               }
            }
          }
          */
            if(lstOtherRIs.size()>0 && xListRIs.size()>0)
            {
                Update lstOtherRIs;
                Update xListRIs;
            }
            else if (lstOtherRIs.size()>0 && xListRIs.size() == 0)
            {
                Update lstOtherRIs;
            }

            else if (lstOtherRIs.size()==0 && xListRIs.size()>0)
            {
               Update xListRIs;
            }

            
            if (xListRIs.size()>0)
            {
                for (RMA_Product__c tempRIs: xListRIs)
                {
                
                        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                        req1.setComments('Submitting request for approval.');
                        req1.setObjectId(tempRIs.id);
                        req1.setNextApproverIds(new Id[] {tempRIs.ApproverLevel1__c});
                        Approval.ProcessResult result = Approval.process(req1);
                    
                }
            }
            string strredirectURl='/'+ObjRMA.id;
            PageReference pr = new PageReference(strredirectURl);
           pr.setRedirect(true);
     return pr;
     
     
   }
     
   Public Pagereference  doCancel()
   {
   PageReference pr = new PageReference('/'+ObjRMA.id);
   pr.setRedirect(true);
     return pr;
   }
}