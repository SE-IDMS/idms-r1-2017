@isTest(seealldata=true)
//Created by VISHNU C
public class WS_Part_Order_TEST
{
    //added by suhail q3 release 2016 start
    public static testMethod void GCR(){
        User user1 = new User(alias = 'user', email='user' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'InstalledProductBeforeupdate;SVMX10;SVMX_InstalledProduct2;SVMX22;AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='InterfaceTestuser' + '@bridge-fo.com',FederationIdentifier = '12345',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000');       
       insert user1;
       system.runAs(user1)
       {  
            system.debug('>>>>>>>>>>>>>>>>>>>>>> user <<<<<<<<<<<<<<'+userinfo.getName());
        
                list<SVMXC__Service_Order__c> wolist = new list<SVMXC__Service_Order__c>();
                country__c country = [SELECT id,name,countrycode__C FROM country__C WHERE countrycode__C = 'US' ];
            Account accountSobj = Utils_TestMethods.createAccount();
            accountSobj.SEAccountID__c ='SEAccountID';
            accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
            accountSobj.Country__c = country.id ; 
            insert accountSobj;
            
            
            SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(accountSobj.id);
            workOrder.Work_Order_Category__c='On-site';                 
            workOrder.SVMXC__Order_Type__c='Maintenance';
            workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
            //workOrder.SVMXC__Order_Status__c = 'UnScheduled';
            workOrder.CustomerRequestedDate__c = Date.today();
            workOrder.Service_Business_Unit__c = 'Energy';
            workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
            workOrder.SendAlertNotification__c = true;
            workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
            workOrder.BackOfficeReference__c = '111111';
            workOrder.IsBillable__c = 'No';
            workOrder.Comments_to_Planner__c='testing';
            workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
            workOrder.BackOfficeSystem__c='BBO_AM';
            workOrder.TECH_TechnicaClosure__c='To Be Processed';
            workOrder.SVMXC__Order_Status__c ='Service Validated';
            
            insert workOrder;
                
                
                //SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c();
                //wolist = [SELECT id,name FROM SVMXC__Service_Order__c LIMIT 1 ];
                //workOrder = wolist[0];
                   
                WS_Part_Order.WorkOrder wou = new WS_Part_Order.WorkOrder();
                List<WS_Part_Order.PartsOrder> porulist = new List<WS_Part_Order.PartsOrder>();
                WS_Part_Order.PartsOrder poru = new WS_Part_Order.PartsOrder();
                WS_Part_Order.PartsOrderLines polu = new WS_Part_Order.PartsOrderLines();
                List<WS_Part_Order.PartsOrderLines> porlulist = new List<WS_Part_Order.PartsOrderLines>();
                WS_Part_Order.PartsOrders porsu = new WS_Part_Order.PartsOrders();
                
                wou.workOrderType = 'workOrderType';
                wou.workOrderSubType = 'workOrderSubType';
                wou.isBillable = 'Yes';
                wou.billingType = 'Yes';                     
                wou.salesOrderNumber = 'salesOrderNumber ';
                wou.WOBackOfficeReference = 'TEST-WOBOFFREF-1';
                wou.TECH_WOBackOfficeRecordID = 'TEST-WOBOFFREF-1';
                wou.WOGBackOfficeReference = 'TEST-WOGBOFFREF-1';
                wou.workOrderbFOID = workOrder.id;
                //add parts order here . start
                poru.partOrderStatus = 'Released';
                poru.workorderbFOID = workorder.id;
                poru.partOrderBOReference = '1234567890';
                // add parts order here . end
                //add parts order line here . start
                polu.BOMaterialReference = 'SURT1000';
                polu.expectedQuantity = 1;
                polu.freeTextReference = 'SURT1000';
                polu.LeadDate = date.newinstance(2016,7,7) ;
                polu.partOrderLineBOReference = '1234567890';
                polu.unitOfMeasure = 'KG';
                polu.vendor = 'SGL';
                // add parts order here line . end
                porlulist.add(polu);
                poru.PartsOrderLines = porlulist;
                porulist.add(poru);
                porsu.PartsOrders =porulist;
                wou.PartsOrders = porsu;
                //xml type end
                List<WS_Part_Order.WorkOrder>  wulist = new List<WS_Part_Order.WorkOrder>();                
                wulist.add(wou);
                
                
                
                Test.startTest();
                WS_Part_Order.bulkCreatePartOrders(wulist); 
                Test.stopTest();
            //}catch (exception e){}
        }   
    }
        public Static WS_Part_Order.WorkOrder getWOUnit(TestData td){
            
                SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
                //country 
                country__c country = [SELECT id,name,countrycode__C FROM country__C WHERE countrycode__C = 'US' ];
                //account
                Account AccountA = new Account();
                AccountA.Name='TestAccountA'; 
                AccountA.Country__c = country.id ; 
                AccountA.Street__c='New StreetA';
                AccountA.POBox__c='XXXA';
                AccountA.ZipCode__c = '12345';
                insert AccountA;
                //Work order
                SVMXC__Service_Order__c workOrder =  new SVMXC__Service_Order__c();
                workOrder.Work_Order_Category__c='On-site';  
              
                workOrder.SVMXC__Order_Type__c='Maintenance';
                workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                workOrder.CustomerRequestedDate__c = Date.today();
                workOrder.Service_Business_Unit__c = 'IT';
                workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
                workOrder.SendAlertNotification__c = true;
                workOrder.BackOfficeReference__c = 'TestExisting';
                workOrder.BackOfficeSystem__c = 'BBO_AM';
                workOrder.SVMXC__Company__c  =AccountA.id;            
                insert workOrder;           
        
        
                WS_Part_Order.WorkOrder wou = new WS_Part_Order.WorkOrder();
                List<WS_Part_Order.PartsOrder> porulist = new List<WS_Part_Order.PartsOrder>();
                WS_Part_Order.PartsOrder poru = new WS_Part_Order.PartsOrder();
                WS_Part_Order.PartsOrderLines polu = new WS_Part_Order.PartsOrderLines();
                List<WS_Part_Order.PartsOrderLines> porlulist = new List<WS_Part_Order.PartsOrderLines>();
                WS_Part_Order.PartsOrders porsu = new WS_Part_Order.PartsOrders();
                wou.workOrderType = 'workOrderType';
                wou.workOrderSubType = 'workOrderSubType';
                wou.isBillable = 'Yes';
                wou.billingType = 'Yes';                     
                wou.salesOrderNumber = 'salesOrderNumber ';
                wou.WOBackOfficeReference = 'TEST-WOBOFFREF-1';
                wou.TECH_WOBackOfficeRecordID = 'TEST-WOBOFFREF-1';
                wou.WOGBackOfficeReference = 'TEST-WOGBOFFREF-1';
                wou.workOrderbFOID = workorder.id;
                //add parts order here . start
                poru.partOrderStatus = 'Released';
                poru.workorderbFOID = workorder.id;
                poru.partOrderBOReference = '1234567890';
                // add parts order here . end
                //add parts order line here . start
                polu.BOMaterialReference = 'SURT1000';
                polu.expectedQuantity = 1;
                polu.freeTextReference = 'SURT1000';
                polu.LeadDate = date.newinstance(2016,7,7) ;
                polu.partOrderLineBOReference = '1234567890';
                polu.unitOfMeasure = 'KG';
                polu.vendor = 'SGL';
                // add parts order here line . end
                porlulist.add(polu);
                poru.PartsOrderLines = porlulist;
                porulist.add(poru);
                porsu.PartsOrders =porulist;
                wou.PartsOrders = porsu;

                return wou;
    }
    
    public class TestData{
        Country__c country;
        Account account;
        Contact contact;
        SVMXC__Installed_Product__c installedporduct;
        Product2 product ;
        SVMXC__Service_Order__c wo;
        SVMXC__Service_Order_Line__c Rwd1;
        SVMXC__Service_Order_Line__c Rwd2;
        
        
        public void createData(){
            
            list<Country__c> countrySobj=[select id,name from Country__c limit 1];          
            country = countrySobj[0];
            Account accountSobj = Utils_TestMethods.createAccount();
            accountSobj.SEAccountID__c ='SEAccountID';
            accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
            accountSobj.Country__c= countrySobj[0].id;
            insert accountSobj;
            account =  accountSobj;
            Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
            contactSobj.Country__c = countrySobj[0].id;
            contactSobj.SEContactID__c ='SEContactID1';
            contactSobj.FirstName = 'CFirstName';
            contactSobj.LastName= 'CLastName';
            contactSobj.LocalFirstName__c= 'CLFirstName';
            contactSobj.MidInit__c ='TMid';
            contactSobj.LocalMidInit__c='TLMid';
            contactSobj.LocalLastName__c='CLLastName';
            contactSobj.Email='test@test.com';
            contactSobj.mobilePhone ='9999999999';
            contactSobj.WorkPhone__c='9999999999';
           //insert contactSobj;
            contact = contactSobj;
            Product2 prod = Utils_TestMethods.createCommercialReference('One','Two','Three','Four','Five','Six',true,true,true);
            insert prod;
            product = prod;
            
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
            ip.SVMXC__Company__c = accountSobj.id;
            ip.Name = 'Test Installed Product1';
            ip.SVMXC__Status__c= 'new';
            ip.GoldenAssetId__c = 'TEST1';
            ip.BrandToCreate__c ='Test Brand';
            ip.CustomerSerialNumber__c = 'SerialNumber';
            ip.SVMXC__Product__c = prod.id;
            ip.SVMXC__Serial_Lot_Number__c = '1234'; 
            ip.DeviceTypeToCreate__c ='Test DeviceType';            
            //ip.SchneiderCommercialReference__c = 'SECREF';
            insert ip;
            List<SVMXC__Site__c> primaryLocs = [SELECT Id,SVMXC__Account__c, PrimaryLocation__c FROM SVMXC__Site__c WHERE SVMXC__Account__c  =: accountSobj.id AND PrimaryLocation__c=true ];
           if(primaryLocs != null && primaryLocs.size()>0)
            delete primaryLocs;
            installedporduct = ip;
            System.debug('\n clog Test Log:'+installedporduct);
            WorkOrderGroup__c workOrdergroup = new WorkOrderGroup__c();
            workOrdergroup.BackOfficeReference__c ='TestExisting';
            insert workOrdergroup;
            SVMXC__Service_Order__c workOrder =  new SVMXC__Service_Order__c();
            workOrder.Work_Order_Category__c='On-site';  
            //workorder.SVMXC__Contact__c = contactSobj.id;
            workorder.SVMXC__Component__c=ip.id;
            workOrder.SVMXC__Order_Type__c='Maintenance';
            workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
            workOrder.SVMXC__Order_Status__c = 'UnScheduled';
            workOrder.CustomerRequestedDate__c = Date.today();
            workOrder.Service_Business_Unit__c = 'IT';
            workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
            workOrder.SendAlertNotification__c = true;
            workOrder.BackOfficeReference__c = 'TestExisting';
            workOrder.BackOfficeSystem__c = 'BBO_AM';
            //workOrder.Comments_to_Planner__c='testing'; 
            workOrder.SVMXC__Company__c  =accountSobj.id;            
            insert workOrder;           
            wo = workOrder;            
            Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Order_Line__c; 
            Map<String,Schema.RecordTypeInfo> WDRecordType = dSobjres.getRecordTypeInfosByName(); 
            
            SVMXC__Service_Order_Line__c wd1 = new SVMXC__Service_Order_Line__c();
            wd1.recordtypeid = WDRecordType.get('Installed Products Serviced').getRecordTypeId(); 
            wd1.WDBackOfficeReference__c = 'BOVIS01';
            wd1.SVMXC__Product__c = prod.id;
            wd1.SVMXC__Service_Order__c= wo.id;           
                insert wd1;
            
                Rwd1 = wd1;
                SVMXC__Service_Order_Line__c wd2 = new SVMXC__Service_Order_Line__c();
                wd2.recordtypeid = WDRecordType.get('Installed Products Serviced').getRecordTypeId(); 
                wd2.WDBackOfficeReference__c = 'BOVIS02';
                wd2.Shipped_Serial_Number__c = 'VIS09032016';
                wd2.SVMXC__Service_Order__c= wo.id;
            
                insert wd2;            
                Rwd2 = wd2;
            
        }
    }
 
    
    //added by suhail q3 release 2016 end
    
    public static testMethod void PartOrderRelease() 
    {
        //User user = CreateUser();
       // insert user;
        system.runAs(CreateUser())
        {
            Country__c country = new country__c();
            country.Name = 'TestCountry';
            country.CountryCode__c = 'YY';
            insert country;
            Account acc = new account();
            acc = Utils_TestMethods.createAccount();
            acc.name = 'Test';
            acc.Country__c=country.id;
            acc.City__c = 'Chennai';

            insert acc;
            Contact Cnct=Utils_TestMethods.CreateContact(acc.Id,'TContact');
            insert Cnct;
            //Contact Cnct = new Contact(AccountId=acc.id,email='UTCont@mail.com',phone='1234567890',LastName='UTContact001',FirstName='Fname1');
            //insert Cnct;  //Inserting Contact
            WorkOrderGroup__c WOG = new WorkOrderGroup__c(WOGName__c='Test',Status__c='Open',Account__c=Acc.id,BackOfficeReference__c='123',CountryofBackOffice__c = 'Global', BackOfficeSystem__c = 'IT_ORA');
            insert WOG;
            SVMXC__Site__c Location = new SVMXC__Site__c(Name='Test',SVMXC__Location_Type__c='BUILD',SVMXC__Account__c=acc.id);
            insert Location;
            SVMXC__Service_Order__c wo =      new SVMXC__Service_Order__c(                                       
                                                         SVMXC__Company__c=Acc.Id,
                                                         SVMXC__Order_Status__c = 'New', 
                                                         BackOfficeReference__c='123',  
                                                         TECH_WOBackOfficeRecordID__c='123',
                                                         SVMXC__Order_Type__c = 'Preventive Maintenance',
                                                         WorkOrderSubType__c = 'Battery Maintenance',
                                                         SVMXC__Site__c = Location.id,
                                                         //SVMXC__Product__c = prod.id,
                                                         SVMXC__Contact__c = Cnct.Id, 
                                                         RescheduleReason__c='Weather Related',
                                                         Service_Business_Unit__c='Energy',
                                                         Parent_Work_Order__c = null,
                                                         Is_Billable__c = true,
                                                         Customer_Service_Request_Time__c= system.now(),
                                                         WorkOrderGroup__c = WOG.id,
                                                         CustomerTimeZone__c = 'Pacific/Kiritimati'
                                                         );
                                                   
            insert wo;
            Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c; 
            Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
            Id POId = PartOrderRecordTypeInfo.get('RMA').getRecordTypeId();
            SVMXC__RMA_Shipment_Order__c porder = new SVMXC__RMA_Shipment_Order__c();
            porder.SVMXC__Order_Status__c = 'Open';
            porder.RecordTypeId = POId;
            porder.SVMXC__Service_Order__c = wo.id;//cc
            porder.SVMXC__Company__c = acc.id;
            porder.SVMXC__Contact__c = Cnct.id;
            porder.Service_level__c = 'SDA';
            porder.SVMXC__Shipping_Receiving_Notes__c = 'Test';
            porder.Ship_to__c = acc.id;
            porder.Freight_terms__c = 'COL';
            insert porder;
            
             Schema.DescribeSObjectResult dSobjres1 = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c; 
            Map<String,Schema.RecordTypeInfo> PartOrderLineRecordTypeInfo = dSobjres1.getRecordTypeInfosByName();
            Id RMALineId = PartOrderLineRecordTypeInfo.get('RMA').getRecordTypeId();
            SVMXC__RMA_Shipment_Line__c poLine = new SVMXC__RMA_Shipment_Line__c(
                SVMXC__RMA_Shipment_Order__c = porder.Id,
                //RecordType = RMALineId,
                SVMXC__Expected_Quantity2__c = 5,
                RecordTypeId = RMALineId,
                CommercialReference__c = 'test2',
                RequestedDate__c = Date.Today(),
                SVMXC__Line_Type__c = 'RMA'
            );
            insert poLine;
            ws_part_order.getPartOrderDetails(porder.id,wo.id,'PartOrder.RELEASE');
        }
    }
    public Static User CreateUser(){
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP30;AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        UserRole uRole = [Select Id,Name,DeveloperName From UserRole where DeveloperName = 'CEO'];
        user.UserRoleId = uRole.Id;
        insert user;
        return user;
    }
}