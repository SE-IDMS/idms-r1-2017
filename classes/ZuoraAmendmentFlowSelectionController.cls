/********************************************************************
 * ZuoraAmendmentFlowSelectionController
 *
 * Author: Kevin Fabregue - Zuora Europe
 ********************************************************************/


public with sharing class ZuoraAmendmentFlowSelectionController {

  private final ApexPages.StandardController controller;
   
  private zqu__Quote__c quote;
     private String pageURL;
     private String quoteId;
     private String subscriptionId;

    public Boolean statusBool {get;set;}


    public string SelectedItem {get;set;}


  public ZuoraAmendmentFlowSelectionController(ApexPages.StandardController controller) {
      this.controller = controller;
      this.quoteId = '';
      this.subscriptionId = '';
      statusBool = false;
      SelectedItem = 'AddRemoveOption';
  }


  public PageReference onload() {
        
        System.debug('###### INFO: getPageURL() = ' + getPageURL());
        this.pageUrl = getPageURL();
        System.debug('###### INFO: ApexPages.currentPage().getParameters().get(\'Id\'); = ' + ApexPages.currentPage().getParameters().get('Id'));

        this.quoteId = ApexPages.currentPage().getParameters().get('Id');

        //appendMessage(ApexPages.Severity.WARNING, 'Work in progress.');

        if ( ( String.isNotBlank(this.quoteId) ) || (Test.isRunningTest() ) ) {
            List<zqu__Quote__c> quotes = new List<zqu__Quote__c>();

            quotes = [SELECT Id, BasePrp__c, Entity__c, zqu__Account__c, zqu__ExistSubscriptionID__c, Bill2Contact__c, Sold2Contact__c, zqu__Account__r.Z_ProfileDiscount__c, zqu__ZuoraAccountID__c, Add_onSKU__c, Add_onSKU10__c, Add_onSKU2__c, Add_onSKU3__c, Add_onSKU4__c, Add_onSKU5__c, Add_onSKU6__c, Add_onSKU7__c, Add_onSKU8__c, Add_onSKU9__c, TECH_ProfileDiscount__c, zqu__QuoteTemplate__c, zqu__Account__r.Tech_CountryCode__c, zqu__SubscriptionType__c FROM zqu__Quote__c WHERE Id = :this.quoteId];
          
            if (Test.isRunningTest() ) {
                quotes = [SELECT Id, BasePrp__c, Entity__c, zqu__Account__c, zqu__ExistSubscriptionID__c, zqu__Account__r.Z_ProfileDiscount__c, zqu__ZuoraAccountID__c, Add_onSKU__c, Add_onSKU10__c, Add_onSKU2__c, Add_onSKU3__c, Add_onSKU4__c, Add_onSKU5__c, Add_onSKU6__c, Add_onSKU7__c, Add_onSKU8__c, Add_onSKU9__c, TECH_ProfileDiscount__c, zqu__QuoteTemplate__c, zqu__Account__r.Tech_CountryCode__c, zqu__SubscriptionType__c FROM zqu__Quote__c WHERE zqu__Account__r.Name='Test Acc' AND zqu__Account__r.SEAccountID__c='123'];
                if (quotes.size() > 0) {
                  this.quoteId = quotes.get(0).Id;
                  //this.subscriptionId = quotes.get(0).zqu__ExistSubscriptionID__c;
              //System.debug('###### INFO: this.subscriptionId = ' + this.subscriptionId);
                }
            }

            Boolean quoteToUpdate = false;

            if (quotes.size() > 0) {
                this.quote = quotes.get(0);
                System.debug('###### INFO: this.quote = ' + this.quote);
                this.subscriptionId = quotes.get(0).zqu__ExistSubscriptionID__c;
                System.debug('###### INFO: this.subscriptionId = ' + this.subscriptionId);

                if(this.quote.Entity__c ==  null){
                  List<Zuora__Subscription__c> subList = [SELECT Id, Entity__c FROM Zuora__Subscription__c WHERE Zuora__Zuora_Id__c = :this.subscriptionId];
                  if(subList.size() > 0){
                    this.quote.Entity__c = subList.get(0).Entity__c;
                    quoteToUpdate = true;
                  }
                }

                if ( !(String.isBlank(this.quote.zqu__Account__r.Z_ProfileDiscount__c) || String.isBlank(this.quote.Entity__c)) ) {
                    String entityVal = this.quote.Entity__c;

                    if (String.isBlank(entityVal)) {
                        this.quote.TECH_ProfileDiscount__c = '';
                    } else {

                        String[] profiles = this.quote.zqu__Account__r.Z_ProfileDiscount__c.split(';');

                        for (String profile: profiles){
                            if (profile.contains(entityVal)){
                                profile = profile.substringAfter('-');

                                this.quote.TECH_ProfileDiscount__c = profile;
                            }
                        }
                    }
                    quoteToUpdate = true;
                }

                if(quote.Bill2Contact__c == null || quote.Bill2Contact__c == '' || quote.Sold2Contact__c == null || quote.Sold2Contact__c == ''){
                  List<Zuora__CustomerAccount__c> listZAccount = [SELECT Id, Zuora__BillToName__c, Zuora__SoldToName__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :quote.zqu__ZuoraAccountID__c];

                  if(listZAccount.size()>0){
                    Zuora__CustomerAccount__c zAccount = listZAccount.get(0);
                    quote.Bill2Contact__c = zAccount.Zuora__BillToName__c;
                    quote.Sold2Contact__c = zAccount.Zuora__SoldToName__c;
                    quoteToUpdate = true;
                  }

                }

                if(quoteToUpdate){
                  update this.quote;
                }

            } else {
              appendMessage(ApexPages.Severity.ERROR, 'Quote not found.');
            }
        } else {
          appendMessage(ApexPages.Severity.ERROR, 'Quote not found.');
        }
        
        return null;
    }


  public PageReference navigateBack() {
       String targetUrl = this.pageURL;
       
     if (targetUrl.contains('//c.')) {
         targetUrl = targetUrl.replace('//c.','//zqu.');
      }
      else {
            if (targetUrl.contains('--c.')) {
               targetUrl = targetUrl.replace('--c.','--zqu.');
            }
      }
      targetUrl = targetUrl.replace('ZuoraAmendmentFlowSelectionPage?','CreateQuote?billingAccountId=' + this.quote.zqu__ZuoraAccountID__c + '&crmAccountId=' + this.quote.zqu__Account__c + '&');
      targetUrl = targetUrl.replace('stepNumber=3','stepNumber=2');
      
      PageReference returnPage = new PageReference(targetUrl);
      
      System.debug('###### INFO: returnPage = ' + returnPage);
      
      //return null;
      return returnPage;
  }


  public PageReference next() {
    String targetUrl = this.pageURL;

    List<Zuora__SubscriptionProductCharge__c> spcList = new List<Zuora__SubscriptionProductCharge__c>();
    spcList = [SELECT Id, Name, Zuora__RatePlanName__c, Zuora__ProductSKU__c FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Subscription__r.Zuora__External_Id__c = :this.subscriptionId];
    System.debug('###### INFO: spcList = ' + spcList);

    Set<String> skuSet = new Set<String>();
    Set<String> ratePlanNamesSet = new Set<String>();
    for (Zuora__SubscriptionProductCharge__c spc : spcList) {
      skuSet.add(spc.Zuora__ProductSKU__c);
      ratePlanNamesSet.add(spc.Zuora__RatePlanName__c);
    }
    System.debug('###### INFO: skuSet = ' + skuSet);
    System.debug('###### INFO: ratePlanNamesSet = ' + ratePlanNamesSet);

    List<String> allowedTypes = new List<String> {'Standalone', 'Primary'};


    //TO DO: fill BasePrp__c
    //this.quote.BasePrp__c !=  prpToAddToQuote.zqu__ZuoraId__c
    String basePrp = '';
    List<zqu__ProductRatePlan__c> zprpList = new List<zqu__ProductRatePlan__c>();    
    zprpList = [SELECT Name, zqu__ZuoraId__c FROM zqu__ProductRatePlan__c WHERE Name IN :ratePlanNamesSet AND zqu__ZProduct__r.Type__c IN :allowedTypes AND zqu__ZProduct__r.zqu__SKU__c IN :skuSet];
    
    System.debug('###### INFO: zprpList = ' + zprpList);

    if (zprpList.size() > 0) {
      basePrp = zprpList.get(0).zqu__ZuoraId__c;
    }


    //TO DO: fill Add_onSKU__c 1 à 10
    List<zqu__ZProduct__c> zprodList = new List<zqu__ZProduct__c>();    
    zprodList = [SELECT Name, AddonSKUs__c FROM zqu__ZProduct__c WHERE Type__c IN :allowedTypes AND zqu__SKU__c IN :skuSet];

    System.debug('###### INFO: zprodList = ' + zprodList);
    
    String addonSKUs = '';
    if (zprodList.size() > 0) {
      addonSKUs = zprodList.get(0).AddonSKUs__c;
    }
    

    // Case 'Change Primary/Standalone product'
    if(SelectedItem == 'ChangeProduct' || SelectedItem == 'UpgradeProduct' || SelectedItem == 'DowngradeProduct') {
      this.quote.BasePrp__c = basePrp;
      this.quote.AmendOption__c = SelectedItem;
      update this.quote;

      targetUrl = targetUrl.replace('ZuoraAmendmentFlowSelectionPage','ZuoraProductSelectorRelaunch');
      PageReference nextPage = new PageReference(targetUrl);

      nextPage.getParameters().put('AmendOption', SelectedItem);

      System.debug('###### INFO: nextPage = ' + nextPage);
      return nextPage;
        
    // Case 'Add/Remove Option(s)'
    }
    else {
      ZuoraProductSelectorRelaunchController.updateQuoteAddOnSkus(this.quote, addonSKUs);
      this.quote.AmendOption__c = SelectedItem;
      /*if (! Test.isRunningTest() ) {
        this.quote.zqu__QuoteTemplate__c = ZuoraProductSelectorRelaunchController.findRelevantQuoteTemplateId(this.quote);
      }*/
      update this.quote;

      if (targetUrl.contains('//c.')) {
        targetUrl = targetUrl.replace('//c.','//zqu.');
      }
      else {
        if (targetUrl.contains('--c.')) {
          targetUrl = targetUrl.replace('--c.','--zqu.');
        }
      } 
      targetUrl = targetUrl.replace('ZuoraAmendmentFlowSelectionPage','SelectProducts');
      targetUrl = targetUrl.replace('stepNumber=3','stepNumber=4');
      PageReference nextPage = new PageReference(targetUrl);

      System.debug('###### INFO: nextPage = ' + nextPage);      

      nextPage.getParameters().put('AmendOption', SelectedItem);
      return nextPage;
    }
  }   


    
  public list<SelectOption> getItems(){
        list<SelectOption> options = new list<SelectOption>();

        options.add(new SelectOption('AddRemoveOption', 'Add/Remove Option(s)'));
        options.add(new SelectOption('ChangeProduct', 'Change Primary/Standalone product'));
        options.add(new SelectOption('UpgradeProduct', 'Upgrade Primary product'));
        options.add(new SelectOption('DowngradeProduct', 'Downgrade Primary product'));

        return options;
    }

    
    private String getPageURL() {
      if (Test.isRunningTest()) {
          String myString = 'abcd';
          return 'https://c.cs9.visual.force.com/apex/ZuoraBlockAccountCreationPage?crmAccountId=001K0000019An2HIAS&quoteType=Subscription&retUrl=%2Fapex%2Fzqu__quotelist&stepNumber=2&subscriptionId=1234&Id=001K0000019An2HIAS';
       } else {
          //String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
          //String urlVal = Apexpages.currentPage().getUrl();
          String urll = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + Apexpages.currentPage().getUrl();
          return urll;
      }
    }
   
   
    public static void appendMessage(ApexPages.Severity messageType, String message) {
      ApexPages.addMessage(new ApexPages.Message(messageType, message));
    }  
}