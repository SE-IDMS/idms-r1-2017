@isTest
global class MktoAccessTokenHttpCalloutMock implements HttpCalloutMock{

  global HttpResponse respond(HTTPRequest req){
    HttpResponse res = new HttpResponse();
    res.setStatus('OK');
    res.setStatusCode(200);
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{"access_token":"access_token"}');
    return res;
  }
}