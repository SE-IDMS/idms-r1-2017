/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 06/11/2012
    Description     : Class for triggers BL_BudgetLineBeforeInsert and BL_BudgetLineBeforeUpdate.
                      Creates a list of budget line records to be processed.
*/

public with sharing class BL_BudgetLineTriggers
{

    public static void budgetLineBeforeUpdate(Map<Id,PRJ_BudgetLine__c> newMap,Map<Id,PRJ_BudgetLine__c> oldMap)
    {
        System.Debug('****** budgetLineBeforeUpdate Start ****'); 
        List<PRJ_BudgetLine__c> budgetLineListToProcessCONDITION1 = new List<PRJ_BudgetLine__c>();
        
        for (ID budLineId:newMap.keyset())
        {
            PRJ_BudgetLine__c newBudLine = newMap.get(budLineId);
            PRJ_BudgetLine__c oldBudLine = oldMap.get(budLineId);
            
            if(newBudLine.Amount__c <> oldBudLine.Amount__c)
            {
                budgetLineListToProcessCONDITION1.add(newBudLine);                
            }

        }
        
        
        if (budgetLineListToProcessCONDITION1.size()>0) 
            BL_BudgetLineUtils.updateBudget(budgetLineListToProcessCONDITION1);
        System.Debug('****** budgetLineBeforeUpdate End ****'); 
    }
    
    public static void budgetLineBeforeInsert(List<PRJ_BudgetLine__c> newBudgetLines)
    {
        System.Debug('****** budgetLineBeforeInsert Start ****'); 
        List<PRJ_BudgetLine__c> budgetLineListToProcessCONDITION1 = new List<PRJ_BudgetLine__c>();
        
        for (PRJ_BudgetLine__c newBudLine:newBudgetLines)
        {                                  
            budgetLineListToProcessCONDITION1.add(newBudLine);                            
        }
        
        
        if (budgetLineListToProcessCONDITION1.size()>0) 
            BL_BudgetLineUtils.updateBudget(budgetLineListToProcessCONDITION1);
        System.Debug('****** budgetLineBeforeInsert End ****');         
        
    }
    

        
}