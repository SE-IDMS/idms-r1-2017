@isTest
private class AP_QuoteLinkError_TEST 
{
    // method to test the error thrown while activate 2nd Quote link
    /*static testMethod void AP_QuoteLinkError()
    {
  
        List<Opportunity> oppList = new List<Opportunity>();
        Country__c country=new Country__c(name='France',CountryCode__c='FR');
        insert country;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        oppList.add(opp1 );
        //insert opp1;
                
        Opportunity opp = new Opportunity(Name='Test Opp',  AccountId=acc.Id, StageName='0 - Closed', CloseDate=Date.today(),Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=country.id , IncludedInForecast__c='Yes',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Solutions');
        //insert opp;  
        oppList.add(opp ); 
        
        insert oppList; 
          
        //List<OPP_QuoteLink__c> lstQLK=new List<OPP_QuoteLink__c>();
        OPP_QuoteLink__c q1 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=true );
        insert q1;
        Test.startTest();
        OPP_QuoteLink__c q2 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=false );
        insert q2;
        
        q1.ActiveQuote__c=false;
        update q1;
        q2.ActiveQuote__c=true;
        update q2;
        Test.stopTest();
    }
    
    static testMethod void AP_QuoteLinkError_1()
    {
    Test.startTest();
        List<Opportunity> oppList = new List<Opportunity>();
        Country__c country=new Country__c(name='France',CountryCode__c='FR');
        insert country;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        //insert opp1;
        oppList.add(opp1);
                
        Opportunity opp = new Opportunity(Name='Test Opp',  AccountId=acc.Id, StageName='0 - Closed', CloseDate=Date.today(),Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=country.id , IncludedInForecast__c='Yes',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Solutions');
        //insert opp;
        oppList.add(opp);        
        
        insert oppList;    
        Test.stopTest();
        try
        {       
        OPP_QuoteLink__c q1 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=true );
        insert q1;
        OPP_QuoteLink__c q2 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=true );
        insert q2;
        }
        catch(Exception e)
        {
                System.debug('...Error ....'+e);
        }
        
    }
    static testMethod void AP_QuoteLinkError_2()
    {
        
       List<Opportunity> oppList = new List<Opportunity>();
        Country__c country=new Country__c(name='France',CountryCode__c='FR');
        insert country;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        //insert opp1;
        oppList.add(opp1);
        
                
        Opportunity opp = new Opportunity(Name='Test Opp',  AccountId=acc.Id, StageName='0 - Closed', CloseDate=Date.today(),Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=country.id , IncludedInForecast__c='Yes',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Solutions');
        //insert opp;    
        oppList.add(opp);
        Test.startTest();
        insert oppList;
        
        //List<OPP_QuoteLink__c> lstQLK=new List<OPP_QuoteLink__c>();
        OPP_QuoteLink__c q1 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=true );
        insert q1;
        OPP_QuoteLink__c q2 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=false );
        insert q2;
        Test.stopTest();
        try
        {
        q1.ActiveQuote__c=true;
        update q1;
        q2.ActiveQuote__c=true;
        update q2;
        }
        catch(Exception e)
        {
                System.debug('...Error ....'+e);
        }        
    }
 */   
    static testMethod void AP_QuoteLinkError_4()
    {
 /*   Test.startTest();
        List<Opportunity> oppList = new List<Opportunity>();
        Country__c country=new Country__c(name='France',CountryCode__c='FR');
        insert country;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        //insert opp1;
        oppList.add(opp1);
                
        Opportunity opp = new Opportunity(Name='Test Opp',  AccountId=acc.Id, StageName='0 - Closed', CloseDate=Date.today(),Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=country.id , IncludedInForecast__c='Yes',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Solutions');
        //insert opp;
        oppList.add(opp);        
        
        insert oppList;    
        Test.stopTest();
        try
        {       
        OPP_QuoteLink__c q1 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=true );
        insert q1;
        OPP_QuoteLink__c q2 = new OPP_QuoteLink__c(OpportunityName__c = oppList[1].Id,ValidityDate__c=date.today(),ActiveQuote__c=true );
        insert q2;
            List<OPP_QuoteLink__c> lstQLKs = new List<OPP_QuoteLink__c>();
            lstQLKs.add(q1);
            lstQLKs.add(q2);
            AP_QuoteLinkError.checkQuoteLink(lstQLKs);
        }
        
        catch(Exception e)
        {
                System.debug('...Error ....'+e);
        }
        
    }*/
        
        try
        {   
            AP_QuoteLinkError.checkQuoteLink(null);
        }
        catch(Exception e)
        {
                System.debug('...Error ....'+e);
        }
    }
}