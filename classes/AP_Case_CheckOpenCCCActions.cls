public class AP_Case_CheckOpenCCCActions{
    public static boolean isFollowupWFUpdated=false;
    public static boolean blnUpdateTestRequired =false;
    
    public static boolean checkForOpenTasks(Map<ID,Case> mapCases,boolean blnIsClosed, boolean blnIsErrorRequired ){
        System.Debug('******checkForOpenTasks method in CaseClosureValidation Class Start****');  
        //*********************************************************************************
        // Method Name      : checkForOpenTasks
        // Purpose          : To Check whether any Open Task or Event while Case Closure and Cancellation
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 20th Feb 2014
        // Modified by      :
        // Date Modified    :
        // Remarks          : For April - 14 Release, Moved this Method from AP10, to handle all scenarios together
        // Modified by      : Vimal K
        // Date Modified    : 20th Oct 2014
        // Remarks          : For Oct 14 Release, EUS#046154, Added Label CLOCT14CCC20 to the Status List of Tasks and Events
        ///********************************************************************************/
        
        Set<Case>       setCasesWthPndgActn     = new Set<Case>();   
        List<Task>      lstOpenTasks            = new List<Task>();
        List<Event>     lstOpenEvents           = new List<Event>();                
        List<String>    lstClosedTaskStatus     = new List<String>{System.Label.CL00327,System.Label.CL00328,System.Label.CLOCT14CCC20}; // Fixed EUS#046154
        List<String>    lstClosedEventStatus    = new List<String>{System.Label.CL00325,System.Label.CL00326,System.Label.CLOCT14CCC20}; // Fixed EUS#046154
        
        //Quering all the Task & Events which are not closed or deffered
        if(blnIsClosed){
            lstOpenTasks    = new List<Task>([Select Status,whatID from Task where whatID in:mapCases.keyset() and status Not In:(lstClosedTaskStatus) and IsRecurrence=false]);
            lstOpenEvents   = new List<Event>([Select Status__c,WhatID from Event where whatID in:mapCases.keyset() and status__c Not In:(lstClosedEventStatus) and IsRecurrence=false]);
        }
        else{
            lstOpenTasks    = new List<Task>([Select Status,whatID from Task where whatID in:mapCases.keyset() and  IsRecurrence=false]);
            lstOpenEvents   = new List<Event>([Select Status__c,WhatID from Event where whatID in:mapCases.keyset() and IsRecurrence=false]);
        }
        
        
        //If the there are any pending Tasks adding corresponding case to a List. 
        if(lstOpenTasks.size()>0){
            for(Task objTask:lstOpenTasks){
                 //Creating a new List of Cases having Pending Tasks.           
                 setCasesWthPndgActn.add(mapCases.get(objTask.whatID));
            }
        }
        
        //If there are any Pending Events adding corresponding case to a List.   
        if(lstOpenEvents.size()>0){
            for(Event objEvent:lstOpenEvents){
                //Adding Cases having pending Events to the list.           
                setCasesWthPndgActn.add(mapCases.get(objEvent.whatID));
            }
        }
        
        if(!blnIsErrorRequired){
            for(Case objCase: mapCases.values()){
                if(!setCasesWthPndgActn.contains(objCase))
                    objCase.TECH_ZeroPendingActions__c =true;
                else
                    objCase.TECH_ZeroPendingActions__c =false;
            }
        }
        
        
        // If there are some pending Actions throwing a validation Error  
        if(setCasesWthPndgActn.Size()>0){           
            for(Case objCaseWithPendingActions: setCasesWthPndgActn){
                if(!Test.isRunningTest() && blnIsErrorRequired){
                    objCaseWithPendingActions.addError(System.Label.CL00324);
                    return false;
                }
                else{
                    System.debug(System.Label.CL00324);
                    return false;
                }
            }
        }
        System.Debug('******checkForOpenTasks method in CaseClosure and Cancellation Validation Class END****');    
        return true; 
    }
    
    public static boolean checkForOpenCCCActions(Map<Id, Case> mapCases, boolean blnIsClosed, boolean blnIsErrorRequired){
    //*********************************************************************************
    // Method Name      : checkForOpenCCCActions
    // Purpose          : To Check whether any Open CCC Action while Case Closure or Cancel
    // Created by       : Vimal Karunakaran - Global Delivery Team
    // Date created     : 18th June 2012
    // Modified by      : Modified the return type to boolean, add Error Method handled with in the Class
    // Date Modified    : 20th Feb 2014
    // Remarks          : For Sep - 11 Release
    ///********************************************************************************/
       
        System.Debug('****AP_Case_CheckOpenCCCActions  checkForOpenCCCActions  Started****'); 
      
        Set<Id> caseActionId = new Set<Id>();   
        List<CCCAction__c> lstOpenCCCAction = new List<CCCAction__c>();
        if(blnIsClosed){
            lstOpenCCCAction = new List<CCCAction__c>([Select Id, Case__c from CCCAction__c where Case__c in: mapCases.keySet() AND Status__c != 'Closed']);
        }
        else{
            lstOpenCCCAction = new List<CCCAction__c>([Select Id, Case__c from CCCAction__c where Case__c in: mapCases.keySet()]);
        }
        
        for(CCCAction__c objAction:lstOpenCCCAction){
            caseActionId.add(objAction.Case__c);
        }
        
        
        if(!blnIsErrorRequired){
            for(Case objCase: mapCases.values()){
                if(!caseActionId.contains(objCase.Id))
                    objCase.TECH_ZeroPendingActions__c =true;
                else
                    objCase.TECH_ZeroPendingActions__c =false;
            }
        }
        
        if(caseActionId.size()>0){           
            for(Id CaseId:  caseActionId){
                Case objCaseWithOpenCCCActions = mapCases.get(CaseId);
                if(!Test.isRunningTest() && blnIsErrorRequired){
                    objCaseWithOpenCCCActions.addError(System.Label.CLSEP12CCC29);
                    return false;
                }
                else{
                    System.debug(System.Label.CLSEP12CCC29);
                    return false;
                }
            }
                
        }
        System.debug('****AP_Case_CheckOpenCCCActions  checkForOpenCCCActions  Ends****'); 
        return true;
    }
    
    
    public static boolean checkOpenTEXs(Map<id,Case> MapNewCase, boolean blnIsClosed, boolean blnIsErrorRequired){
        //<BR-3326>
        /*  Modified by     : Renjith
        Release version : Oct Release 2013, BR-3326    
        Description     : An agent should not close a case until all the associated TEXs have been Closed 
        
        <BR-4775>
        Modified By     : Roshan Srivastava
        Release Version : Feb 2014 Minor Release
        Description     : Correction of the 'Cancelled' status 
        ------------------------------------------------------------------------------------------------------ */
        List<TEX__c> ListTEX= new List<TEX__c>();
        if(blnIsClosed){
            ListTEX=[SELECT Id,Status__c,Case__c FROM TEX__c where Case__c in :MapNewCase.keyset() AND Status__c!='Closed' AND Status__c!='Cancelled'];//<BR-4775>
        }
        else{
            ListTEX=[SELECT Id,Status__c,Case__c FROM TEX__c where Case__c in :MapNewCase.keyset()];//<BR-4775>
        }
        
        System.debug('~~~checkOpenTEXs '+ListTEX.size());
        
        Set<id> CaseIds = new Set<id>();
        for(TEX__c tempTex :ListTEX)
            CaseIds.add(tempTex.Case__c);
        
        if(!blnIsErrorRequired){
            for(Case objCase: MapNewCase.values()){
                if(!CaseIds.contains(objCase.Id))
                    objCase.TECH_ZeroPendingActions__c =true;
                else
                    objCase.TECH_ZeroPendingActions__c =false;
            }
        }
        
        for(id caseId :CaseIds){
            Case temcase=MapNewCase.get(caseId);
            if(!Test.isRunningTest() && blnIsErrorRequired){
                temcase.addERROR(System.Label.CLOCT13RR10);
                return false;
            }
            else{
                System.debug('***** '+System.Label.CLOCT13RR10);
                return false;
            }
        }    
        return true;           
    }
     
    public static boolean checkOpenWON(Map<id,Case> CaseNewMap,boolean blnIsClosed, boolean blnIsErrorRequired){
    /* --------------------------------------------------------------------------------------------------------------
        Modified by     : Renjith
        Release version : Oct Release 2013, BR-2458    
        Description     : <BR-2458> - An agent should not close a case until all the associated Work Order Notifications are Closed or Cancelled  
    ----------------------------------------------------------------------------------------------------------------------------------------- */ 
    
        System.Debug('######## checkOpenWON - Started - ####');
        List<WorkOrderNotification__c> lstWON;
        if(blnIsClosed){
            List<String> lstCloseSts = System.Label.CLOCT13CCC07.split(',',0);
            lstWON = [Select Case__c from WorkOrderNotification__c WHERE Case__c in :CaseNewMap.keySet() and Work_Order_Status__c not in :lstCloseSts];
        }
        else{
            lstWON = [Select Case__c from WorkOrderNotification__c WHERE Case__c in :CaseNewMap.keySet()];
        }
    
        System.Debug('######## checkOpenWON - open WON:- '+ lstWon.size());
     
        Set<id> CaseIds = new Set<id>();
        for(WorkOrderNotification__c won :lstWON)
            CaseIds.add(won.Case__c);
        
        if(!blnIsErrorRequired){
            for(Case objCase: CaseNewMap.values()){
                if(!CaseIds.contains(objCase.Id))
                    objCase.TECH_ZeroPendingActions__c =true;
                else
                    objCase.TECH_ZeroPendingActions__c =false;
            }
        }
        
        for(id caseId :CaseIds){
            Case cCase = CaseNewMap.get(caseId) ;
            if(!Test.isRunningTest() && blnIsErrorRequired){  
                cCase.addERROR(System.Label.CLOCT13CCC13);   
                return false;
            }
            else{
                System.debug(System.Label.CLOCT13CCC13);
                return false;
            }
        }  
        
        System.Debug('######## checkOpenWON - Completed - ####');
        return true;
    }
    
    public static boolean checkOpenComplaintRequest(Map<id,Case> CaseNewMap,boolean blnIsClosed, boolean blnIsErrorRequired){
    /* --------------------------------------------------------------------------------------------------------------
        Modified by     : Renjith
        Release version : Dec Release 2013, BR-4393    
        Description     : <BR-4393> - Case cannot be closed if there is any Complaint/Request related to case with status as ‘Open’, ‘Re-open’, ‘solution found, execution pending’
 
    ----------------------------------------------------------------------------------------------------------------------------------------- */ 
    
        System.Debug('######## checkOpenComplaint/Request - Started - ####');
        List<ComplaintRequest__c> lstRequests;
        if(blnIsClosed){
            List<String> lstCloseSts = System.Label.CLDEC13CCC04.split(',',0);
            lstCloseSts.add(System.Label.CLOCT14CCC20);
            lstRequests = [Select Case__c from ComplaintRequest__c WHERE Case__c in :CaseNewMap.keySet() and Status__c not in :lstCloseSts];
        }
        else{
            lstRequests = [Select Case__c from ComplaintRequest__c WHERE Case__c in :CaseNewMap.keySet()];
        }
    
        System.Debug('######## checkOpenComplaint/Request  - open Requests:- '+ lstRequests.size());
     
        Set<id> CaseIds = new Set<id>();
        for(ComplaintRequest__c cReqst :lstRequests)
            CaseIds.add(cReqst.Case__c);
        
        if(!blnIsErrorRequired){
            for(Case objCase: CaseNewMap.values()){
                if(!CaseIds.contains(objCase.Id))
                    objCase.TECH_ZeroPendingActions__c =true;
                else
                    objCase.TECH_ZeroPendingActions__c =false;
            }
        }
        
        for(id caseId :CaseIds){
            Case cCase = CaseNewMap.get(caseId) ;
            if(!Test.isRunningTest() && blnIsErrorRequired){  
                cCase.addERROR(System.Label.CLDEC13CCC05);   
                return false;
            }
            else{
                System.debug(System.Label.CLDEC13CCC05);
                return false;
            }
        }
        
        System.debug('######## checkOpenComplaint/Request - Completed - ####');
        
        return true;
     
    }
    public static boolean checkForOpenRROnCaseClosure(Map<Id, Case> mapCancelledCases){
    //*********************************************************************************
    // Method Name      : checkForOpenRROnCaseClosure
    // Purpose          : To Check whether any Open Return Request while Case Closure or Cancel
    // Created by       : Vimal Karunakaran - Global Delivery Team
    // Date created     : 24th Feb 2014
    // Modified by      : 
    // Date Modified    : 
    // Remarks          : For April - 14 Release (BR-1825)
    ///********************************************************************************/
       
        System.Debug('****AP_Case_CheckOpenCCCActions  checkForOpenRROnCaseClosure  Started****'); 
      
        Set<Id> caseRRId = new Set<Id>();
        List<String> lstRRCancelledStatus =     new List<String>();
        lstRRCancelledStatus = System.Label.CLAPR14CCC07.split(',',0);
        List<RMA__c> lstOpenRR = new List<RMA__c>([Select Id, Case__c from RMA__c where Case__c in: mapCancelledCases.keySet()]);
        
        for(RMA__c objRR:lstOpenRR){
            caseRRId.add(objRR.Case__c);
        }
        
        
        if(caseRRId.size()>0){           
            for(Id CaseId:  caseRRId){
                if(mapCancelledCases.containsKey(CaseId)){
                    Case objCaseWithOpenRRs = mapCancelledCases.get(CaseId);
                    if(!Test.isRunningTest()){
                        objCaseWithOpenRRs.addError(System.Label.CLAPR14CCC05);
                        return false;
                    }
                    else
                        System.debug(System.Label.CLAPR14CCC05);
                }
            }
                
        }
        System.debug('****AP_Case_CheckOpenCCCActions  checkForOpenRROnCaseClosure  Ends****'); 
        return true;
    }
    public static boolean checkForOpenOpptyNotificationOnCaseClosure(Map<Id, Case> mapCancelledCases){
    //*********************************************************************************
    // Method Name      : checkForOpenOpptyNotificationOnCaseClosure
    // Purpose          : To Check whether any Open Opportunity Notification while Case Closure or Cancel
    // Created by       : Vimal Karunakaran - Global Delivery Team
    // Date created     : 24th Feb 2014
    // Modified by      : 
    // Date Modified    : 
    // Remarks          : For April - 14 Release (BR-1825)
    ///********************************************************************************/
       
        System.Debug('****AP_Case_CheckOpenCCCActions  checkForOpenOpptyNotificationOnCaseClosure  Started****'); 
      
        Set<Id> caseOpptyNotificationId = new Set<Id>();
        //List<String> lstOpptyNotificationCancelledStatus =    new List<String>();
        //lstOpptyNotificationCancelledStatus = System.Label.CLAPR14CCC07.split(',',0)
        List<OpportunityNotification__c> lstOpenOpptyNotification = new List<OpportunityNotification__c>([Select Id, Case__c from OpportunityNotification__c where Case__c in: mapCancelledCases.keySet()]);
        
        for(OpportunityNotification__c objOpptyNotification:lstOpenOpptyNotification){
            caseOpptyNotificationId.add(objOpptyNotification.Case__c);
        }
        
        
        if(caseOpptyNotificationId.size()>0){           
            for(Id CaseId:  caseOpptyNotificationId){
                if(mapCancelledCases.containsKey(CaseId)){
                    Case objCaseWithOpenOpptyNotifications = mapCancelledCases.get(CaseId);
                    if(!Test.isRunningTest()){
                        objCaseWithOpenOpptyNotifications.addError(System.Label.CLAPR14CCC08);
                        return false;
                    }
                    else
                        System.debug(System.Label.CLAPR14CCC08);
                }
            }
                
        }
        System.debug('****AP_Case_CheckOpenCCCActions  checkForOpenOpptyNotificationOnCaseClosure  Ends****'); 
        return true;
    }
    
    public static boolean checkForExternalRefOnCaseClosure(Map<Id, Case> mapCancelledCases){
    //*********************************************************************************
    // Method Name      : checkForExternalRefOnCaseClosure
    // Purpose          : To Check whether any Open Opportunity Notification while Case Closure or Cancel
    // Created by       : Vimal Karunakaran - Global Delivery Team
    // Date created     : 24th Feb 2014
    // Modified by      : 
    // Date Modified    : 
    // Remarks          : For April - 14 Release (BR-1825)
    ///********************************************************************************/
       
        System.Debug('****AP_Case_CheckOpenCCCActions  checkForExternalRefOnCaseClosure  Started****'); 
      
        Set<Id> caseExtnalRefId = new Set<Id>();
        //List<String> lstOpptyNotificationCancelledStatus =    new List<String>();
        //lstOpptyNotificationCancelledStatus = System.Label.CLAPR14CCC07.split(',',0)
        List<CSE_ExternalReferences__c> lstOpenExtnalRef = new List<CSE_ExternalReferences__c>([Select Id, Case__c from CSE_ExternalReferences__c where Case__c in: mapCancelledCases.keySet()]);
        
        for(CSE_ExternalReferences__c objExtnalRef:lstOpenExtnalRef){
            caseExtnalRefId.add(objExtnalRef.Case__c);
        }
        
        
        if(caseExtnalRefId.size()>0){           
            for(Id CaseId:  caseExtnalRefId){
                if(mapCancelledCases.containsKey(CaseId)){
                    Case objCaseWithExtnalRef = mapCancelledCases.get(CaseId);
                    if(!Test.isRunningTest()){
                        objCaseWithExtnalRef.addError(System.Label.CLAPR14CCC09);
                        return false;
                    }
                    else
                        System.debug(System.Label.CLAPR14CCC09);
                }
            }
                
        }
        System.debug('****AP_Case_CheckOpenCCCActions  checkForExternalRefOnCaseClosure  Ends****'); 
        return true;
    }
    
    public static void updateWFTriggeredField(Map<Id, Case> oldCaseMap, Map<Id, Case> newCaseMap){
        if(!isFollowupWFUpdated){
            for(Case objCase:newCaseMap.values()){
                if(oldCaseMap.get(objCase.Id).TECH_FollowupWFTriggered__c == TRUE && objCase.TECH_FollowupWFTriggered__c == TRUE && objCase.TECH_FollowupWFTriggered__c == oldCaseMap.get(objCase.Id).TECH_FollowupWFTriggered__c ){
                    objCase.TECH_FollowupWFTriggered__c=FALSE;
                }
            }
            isFollowupWFUpdated=TRUE;
        }
    }
   
}