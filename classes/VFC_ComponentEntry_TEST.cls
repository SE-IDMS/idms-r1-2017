@isTest
private class VFC_ComponentEntry_TEST 
{
    static testMethod void testComponentEntry() 
    { 
    Component__c cs = new Component__c();
        
    
    Release__c rs = new Release__c(Name = 'TestRelease',
    ReleaseStartDate__c = system.today(),
    ReleasePlannedEndDate__c= system.today(),
    RecordTypeId='012A0000000mDOc');
    Insert rs;
    
  
    ComponentAPINames__c  canobj = new ComponentAPINames__c(APIName__c = '<ClassName> Eg: VFC22_InquiraLink',
    Name = 'Apex Class');    
    Insert canobj;
    
    if(rs != null){
    cs = new Component__c(ComponentType__c = 'Apex Class',
    FieldName__c = 'VFC_TestClass',
    IsMigrate__c = True,
    ObjectName__c = 'Component__c',
    Owner__c = userinfo.getuserid(),
    Release__c = rs.Id,
    Track__c = 'CCC',
    Type__c = 'New');
    insert cs;
    }
    
    List<SelectOption> selectlist = new List<SelectOption>();
  
    VFC_ComponentEntry vfc = new VFC_ComponentEntry (new ApexPages.StandardController(cs));    
    vfc.component = 'Apex Class:0';
    vfc.selectedRec = '0';
    vfc.getname();
    vfc.getDispMessage(); 
    vfc.addrow(); 
    vfc.save();
    vfc.cancel();
    vfc.removerow(); 
    vfc.populateAPI();   
    
        
    VFC_GenerateXml vgd = new VFC_GenerateXml (new ApexPages.StandardController(rs));
    vgd.selectedvalue='CCC';
    vgd.queryXmlData();   
    
    List<Component__c> complist = new List<Component__c>();
    complist = [select Id,ComponentType__c from Component__c limit 1];
    
    ApexPages.StandardSetController controller = new ApexPages.StandardSetController(complist);
    controller.setSelected(complist);
    VFC_GenerateDeltaXML vdx = new VFC_GenerateDeltaXML(controller);
    
   }     
}