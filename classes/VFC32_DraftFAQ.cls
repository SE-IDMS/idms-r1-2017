/*
    Author          : Accenture Team
    Date Created    : 27/05/2011
    Description     : Controller for VFP32_DraftFAQ.This class passes corresponding case to Integration Methods class and returns the created FAQ Id on the page.
    
    Modified By     :Nikhil Agarwal
    Release         :May 2014 Release
    BR              :BR-4759,BR-4800,BR-4883
    Modification    :Redesign Draft FAQ form,File Attachment and Business Entity field
*/
public class VFC32_DraftFAQ 
{
    /* Variables Declaration */
    public Case cse{get;set;}{cse= new Case();}
    public DataTemplate__c objDataTemplate{get; set;}    
    //public String faqId;
    public string message{get;set;}
    public string faqMessage{get;set;}
    public Boolean IsDraftFAQSubmitted{get;set;}
    public String ProductFamily{get;set;}
    public String Answer{get;set;}
    public Attachment attach{get;set;}
    public String selectedBusinessEntity{get;set;}
    public String selectedBusinessSubEntity{get;set;}
    public String selectedLanguage{get;set;}
    public List<SelectOption> languageList{get;set;}
    public String Title{get;set;} 
    public string filename{get;set;}
    public blob filebody{get;set;}
    public list<attachment> attachedfiles=new list<attachment>();
    public boolean blnAttachedFileSection{get;set;}
    public list<string> lstfaqidandmessage=new list<string>();
    public list<attachment> lstattachmentid=new list<attachment>();
    public boolean isuploaddisabled{get;set;}
    list <attachment> lstdeleteattachment =new list<attachment>();
    
    /* Constructor */
    
    public VFC32_DraftFAQ(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC32_DraftFAQ******');
        blnAttachedFileSection=false;
        attach = new Attachment();
        isuploaddisabled=FALSE;
        IsDraftFAQSubmitted = FALSE;
        if(controller.getID()!=null)
            cse = [Select Id,CCCountry__c,TECH_FAQ__c, CustomerRequest__c,ProductFamily__c, ProductBU__c, Subject,Contact.Country__c, AnswerToCustomer__c ,Contact.Country__r.CountryCode__c,Contact.Country__r.Name, Account.Country__r.CountryCode__c, Account.Country__r.Name, Contact.CorrespLang__c from Case where Id=:controller.getID()];
        objDataTemplate = new DataTemplate__c();
        if(cse.contact !=null && cse.contact.Country__c !=null){
            objDataTemplate.Country__c = cse.contact.Country__c;
        }
        else if(cse.Account !=null && cse.Account.Country__c !=null){
            objDataTemplate.Country__c = cse.Account.Country__c;
        }
        else{
            objDataTemplate.Country__c = cse.CCCountry__c;
        }
        if(cse.Subject!=null &&cse.Subject!='' ){
            Title=cse.Subject;
        }
        if(cse.AnswerToCustomer__c!=null &&cse.AnswerToCustomer__c!='' ){
            Answer=cse.AnswerToCustomer__c;
        }
        languageList    = new List<SelectOption>();  
        Schema.DescribeFieldResult contactCorrespondingLanguageField= Contact.CorrespLang__c.getDescribe();
        List<Schema.PicklistEntry> languages= contactCorrespondingLanguageField.getPicklistValues();
        //display in alphabetical order
        List<String> sortResults=new List<String>();       
        for(Schema.PicklistEntry language: languages)
        {
            sortResults.add(language.getLabel()+':'+language.getValue());
        }
        sortResults.sort();        
        for(String result : sortResults)
        languageList.add(new SelectOption(result.split(':')[1],result.split(':')[0]));
        selectedLanguage=cse.Contact.CorrespLang__c;            
        ProductFamily = cse.ProductFamily__c;
            if( cse.TECH_FAQ__c!=null){
                IsDraftFAQSubmitted=TRUE;
                faqMessage = 'Draft FAQ is already submitted from this Case ' + cse.TECH_FAQ__c;
            }
        System.Debug('****** Initializing the Properties and Variables Ends for VFC32_DraftFAQ******');
    }
    /* This method redirects passes the Case to CaseIntegration Class & displays the returned FAQ Id to the user */
    public list<string> redirect()
    {        
        string target='';
        try
        {
            list<Country__c> CountryCode= new list<Country__c>();
            CountryCode=[Select CountryCode__c,ID from Country__c where id=:objDataTemplate.Country__c];
            if(selectedBusinessSubEntity!=null && selectedBusinessSubEntity!='')
                target=selectedBusinessSubEntity;
            else if(selectedBusinessEntity=='None')
                target='';
            else
                target=selectedBusinessEntity;
            if(cse!=null)
                lstfaqidandmessage = AP12_CaseIntegrationMethods.DraftInquiraFAQ(cse,selectedLanguage,CountryCode[0].CountryCode__c,target,Title,Answer,lstattachmentid);
            if(lstfaqidandmessage[0]!=null){
                cse.TECH_FAQ__c = lstfaqidandmessage[0];
                Database.update(cse,false);
                }  
        }
        catch(CalloutException exc)
        {   String strException=exc.getMessage();
            String strExceptionMessage ='';
            if(strException.containsIgnoreCase(System.Label.CLMAY13CCC49)){
                strExceptionMessage = System.Label.CLMAY14CCC19;
            }
            else if(strException.containsIgnoreCase(System.Label.CLMAY13CCC50)){
                strExceptionMessage = System.Label.CLMAY14CCC20;
            }
            else if(strException.containsIgnoreCase(System.Label.CLMAY13CCC51)){
                strExceptionMessage = System.Label.CLMAY14CCC21;
            }
            else{
                strExceptionMessage =Exc.getMessage();
            }
            message = strExceptionMessage;
            //message  = System.Label.CL00398;
            //system.debug('Exception while calling Draft FAQ WS '+ exc.getMessage() + ' for '+ cse.Id);
        }
        catch(Exception exc)
        {   
            message = exc.getmessage();
            //message  = System.Label.CL00407;
            //system.debug('Exception while calling Draft FAQ WS '+ exc.getMessage() + ' for '+ cse.Id);
        }
        finally
        {
        if(lstattachmentid!=null && lstattachmentid.size()>0)
        delete lstattachmentid[0];
        }
        
        if(lstfaqidandmessage.size()>0 && lstfaqidandmessage[0] != null)
            message = Label.CL00393 + ' ' +lstfaqidandmessage[0]+ ' ' +Label.CL00394;
        else if(lstfaqidandmessage.size()>0 && lstfaqidandmessage[1] != null )
            message=lstfaqidandmessage[1];
        else
            message= System.Label.CLJuly14CCC01;
            
        system.debug('****** End of redirection method in VFC32_DraftFAQ *******');
        return null;
    }
   
    //When user clicks upload button on Visualforce Page, perform upload/insert
    //BR-4800
    public ApexPages.Pagereference upload()
    {
        if(attachedfiles.size()==1){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.CLMAY14CCC27));
            return null;
        }
        else{
            blnAttachedFileSection=true;
            attach.name=filename;
            attach.body=filebody;
            attachment duplicateattachment=this.attach.clone(false,true,false,false);
            duplicateattachment.parentId=cse.Id;
            filebody=null;
            this.attach.body=filebody;
            Database.insert(duplicateattachment);
            lstattachmentid=[select id from attachment where id=:duplicateattachment.id];
            attachedfiles.add(attach);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,System.Label.CLMAY14CCC24));
            isuploaddisabled=TRUE;
            return null;
        }
    }
    public ApexPages.pagereference Removeattachedfile()
    {
        if(attachedfiles.size()==1){
            attachedfiles.clear();
            lstdeleteattachment=[select id,body,name from attachment where id=:lstattachmentid[0].id];
            delete lstdeleteattachment;
            lstattachmentid.clear();
            blnAttachedFileSection=false;
            isuploaddisabled=FALSE;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,System.Label.CLMAY14CCC26));
            return null;
        }   
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No files to be removed'));
            return null;
        }
    }
    
    public list<selectoption> getBEoptions(){
        List <selectoption> lstBEoptions = new list<selectoption>();
        // Find all the BE Picklist Values in the custom setting
        Map<String, CS_DraftFAQBusEntityValues__c> mapBusEntities = CS_DraftFAQBusEntityValues__c.getAll();
        for(CS_DraftFAQBusEntityValues__c obj:mapBusEntities.values())
        {
            lstBEoptions.add(new selectoption(obj.Value__c,obj.Label__c));
        }
        lstBEoptions.sort();
        return lstBEoptions;
    }
    
    public list<selectoption> getBSEoptions()
    {
        List <selectoption> lstBSEoptions = new list<selectoption>();
        List <CS_DraftFAQBusSubEntityValues__c> lstsortedBSE=[SELECT Business_Entity__c,Label__c,Name,Value__c FROM CS_DraftFAQBusSubEntityValues__c order by name];
        for(CS_DraftFAQBusSubEntityValues__c obj:lstsortedBSE)
        {
            if(obj.Business_Entity__c==selectedBusinessEntity)
            lstBSEoptions.add(new selectoption(obj.Value__c,obj.Label__c));
        }
        if(lstBSEoptions.size()==0)
        lstBSEoptions.add(new selectoption('','--None--'));
        return lstBSEoptions;
    }
    
}