/**
11-June-2012    Srinivas Nallapati  Sept Mkt Release    Initial Creation: test class for VFC_MassLeadOwnerChange
 */
@isTest
private class VFC_MassLeadOwnerChange_TEST {

    static testMethod void myUnitTest() {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
          User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
             user.LeadCleanup__c= true;
             user.BypassWF__c = False;
             Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
             Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();
             
             List<Lead> leads1;
              Country__c country= Utils_TestMethods.createCountry();
                insert country;
                Account Acc1 = Utils_TestMethods.createAccount();
                insert Acc1;
                MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                insert markCallBack;
                
                Contact con = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc1.id, email = 'textt@test.com', Country__c = country.id);
                insert con;
                
                leads1 = new List<Lead>();
                for(Integer i=0;i<20;i++)
                {
                    Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                   if(mapRecordType.get('Marcom Lead')!= null)    
                        lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                   if(Math.mod(i,2) ==0)
                       lead1.email = null;     
                   lead1.Contact__C = con.id; 
                   lead1.Account__C = Acc1.id;
                   leads1.add(lead1);   
                }
                
                Database.SaveResult[] leadsInsert1= Database.insert(leads1, true);
                //insert leads;

                
         system.runas(user)
         {
           
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController([select name, status, Ownerid from lead limit 1]);
            controller.setSelected([select name, status, Ownerid from lead limit 100]);
            
            Pagereference pg1 = Page.VFP_MassLeadOwnerChange;
            Test.setCurrentPage(pg1);
            
            VFC_MassLeadOwnerChange MLChangecontroller = new VFC_MassLeadOwnerChange(controller);
            
            MLChangecontroller.lstLeads = [select name, status, Ownerid from lead limit 100];
            
            MLChangecontroller.userSearchKey = '';
            MLChangecontroller.searchUsers();
            MLChangecontroller.changeLeadOwner();
            MLChangecontroller.selectedUserId = MLChangecontroller.lstUser[0].id;
            MLChangecontroller.changeLeadOwner();
            
            
         } //end of runas
           
             User usr2 = Utils_TestMethods.createStandardUser(profiles[0].Id,'tU92123');
             
             usr2.BypassWF__c = false;
             system.runAs(usr2)
             {
               ApexPages.StandardSetController controller = new ApexPages.StandardSetController([select name, status, Ownerid from lead limit 1]);
                controller.setSelected([select name, status, Ownerid from lead limit 100]);
                
                Pagereference pg1 = Page.VFP_MassLeadOwnerChange;
                Test.setCurrentPage(pg1);
                
                VFC_MassLeadOwnerChange MLChangecontroller = new VFC_MassLeadOwnerChange(controller);
                
                MLChangecontroller.lstLeads = leads1 ;
                
                MLChangecontroller.userSearchKey = '';
                MLChangecontroller.searchUsers();
                MLChangecontroller.changeLeadOwner();
                MLChangecontroller.selectedUserId = MLChangecontroller.lstUser[0].id;
                MLChangecontroller.changeLeadOwner();
             }  

      } //end of if 
    }//End of method
}