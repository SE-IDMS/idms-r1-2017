/**
 * An apex class that keeps updates of a portal user in sync with its corresponding contact.
   Guest users are never able to access this page.
 */
public class VFC_SRV_MYFsPersonalInformatinController {

    private User user;
    private boolean isEdit = false;
    Public String UltimateParent{get;set;}
    Public String SelectedCountry{get;set;}
    Public String cccPhoneNo{get;set;}

    public User getUser() {
        return user;
    }

    public VFC_SRV_MYFsPersonalInformatinController () {
        UltimateParent = Apexpages.currentpage().getparameters().get('Params');
        SelectedCountry = Apexpages.currentpage().getparameters().get('country');
        user = [SELECT id, email, username, usertype, communitynickname, timezonesidkey, languagelocalekey, firstname, lastname, phone, title,
                street, city, country, postalcode, state, localesidkey, mobilephone, extension, fax, contact.email
                FROM User
                WHERE id = :UserInfo.getUserId()];
        // guest users should never be able to access this page
        if (user.usertype == 'GUEST') {
            throw new NoAccessException();
        }
         cccPhoneNo = AP_SRV_myFSUtils.getCountryPhoneNumber(SelectedCountry);
    }

     public PageReference save() {
        if (user.contact != null) {
            setContactFields(user.contact, user);
        }

        try {
            update user;
            if (user.contact != null) {
                update user.contact;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Your settings have been successfully saved.')); 
            pagereference  PageRef = new pagereference('/myfieldservices/apex/VFP_SRV_MYFSSiteSelecitonPage?Params='+UltimateParent+'&'+'Country='+SelectedCountry);
            return PageRef;    
            }
            
            isEdit=false;
        } catch(DmlException e) {
            ApexPages.addMessages(e);
        }
       return null;
    }

    public pagereference cancel() {
       pagereference  PageRef = new pagereference('/myfieldservices/apex/VFP_SRV_MYFSSiteSelecitonPage?Params='+UltimateParent+'&'+'Country='+SelectedCountry);
       return PageRef;
    }

    public static void setContactFields(Contact c, User u) {
        c.title = u.title;
        c.firstname = u.firstname;
        c.lastname = u.lastname;
        c.email = u.email;
        c.phone = u.phone;
        c.mobilephone = u.mobilephone;
        c.fax = u.fax;
        c.mailingstreet = u.street;
        c.mailingcity = u.city;
        c.mailingstate = u.state;
        c.mailingpostalcode = u.postalcode;
        c.mailingcountry = u.country;
    }
}