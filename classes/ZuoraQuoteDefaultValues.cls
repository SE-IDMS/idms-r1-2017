global class ZuoraQuoteDefaultValues extends zqu.CancellationPropertyController.PopulateDefaultFieldValuePlugin {
    global override void populateDefaultFieldValue(
        SObject record, zqu.PropertyComponentController.ParentController pcc) {

    	System.debug('###### INFO: ZuoraQuoteDefaultValues execution START.');
         
        if ( (record.Id == null) || (Test.isRunningTest()) ) {
        	System.debug('###### INFO: ZuoraQuoteDefaultValues execution IN PROGRESS.');
            //Populate default values in the quote header
            if(!Test.isRunningTest()) super.populateDefaultFieldValue(record, pcc);

            System.debug('###### INFO: getPageURL() = ' + ZuoraProductSelectorRelaunchController.getPageURL());
        	String zuoraSubId = ApexPages.currentPage().getParameters().get('subscriptionId');
        	System.debug('###### INFO: zuoraSubId = ' + zuoraSubId);

        	String spcQuery = 'SELECT Zuora__ProductName__c, Zuora__ProductSKU__c, Zuora__Type__c, Zuora__Subscription__c,';
        	spcQuery += ' Zuora__Subscription__r.Zuora__ServiceActivationDate__c, Zuora__Subscription__r.Zuora__TermEndDate__c';
        	spcQuery += ' FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Subscription__r.Zuora__External_Id__c = \'' + zuoraSubId + '\'';

        	List<Zuora__SubscriptionProductCharge__c> spcList = database.query(spcQuery);
        	System.debug('###### INFO: spcList = ' + spcList);

        	List<String> productSkuList = new List<String>();
        	Date subServiceActivationDate = Date.today();
        	Date subTermEndDate = Date.today();
        	for(Zuora__SubscriptionProductCharge__c spc : spcList) {
        		productSkuList.add(spc.Zuora__ProductSKU__c);
        		subServiceActivationDate = spc.Zuora__Subscription__r.Zuora__ServiceActivationDate__c;
        		subTermEndDate = spc.Zuora__Subscription__r.Zuora__TermEndDate__c;
        	}

        	String productQuery = 'SELECT Id, Name, Entity__c, zqu__SKU__c, Type__c';
        	productQuery += ' FROM zqu__ZProduct__c WHERE zqu__SKU__c IN :productSkuList AND (Type__c = \'Primary\' OR Type__c = \'Standalone\')'; 
        	System.debug('###### INFO: productQuery = ' + productQuery);

        	List<zqu__ZProduct__c> standOrPrimaryProductList = database.query(productQuery);
        	System.debug('###### INFO: standOrPrimaryProductList = ' + standOrPrimaryProductList);


        	boolean standOrPrimaryChargeIsOTC = false;
        	boolean standOrPrimaryChargeIsRecurring = false;
        	for(Zuora__SubscriptionProductCharge__c spc : spcList) {
        		if ( (standOrPrimaryProductList.size() > 0) && (spc.Zuora__ProductSKU__c == standOrPrimaryProductList.get(0).zqu__SKU__c) ) {
        			if(spc.Zuora__Type__c == 'One-Time') standOrPrimaryChargeIsOTC = true;
        			if(spc.Zuora__Type__c == 'Recurring') standOrPrimaryChargeIsRecurring = true;
        		}
        	}

        	System.debug('###### INFO: standOrPrimaryChargeIsOTC = ' + standOrPrimaryChargeIsOTC);
        	System.debug('###### INFO: standOrPrimaryChargeIsRecurring = ' + standOrPrimaryChargeIsRecurring);


        	if ( standOrPrimaryChargeIsOTC ) {
        		record.put('zqu__CancellationEffectiveDate__c', 'Enter a Date');
            	record.put('zqu__CancellationDate__c', subServiceActivationDate);
        	} else /*if ( standOrPrimaryChargeIsRecurring )*/ {
        		record.put('zqu__CancellationEffectiveDate__c', 'End of Current Term');
        		record.put('zqu__CancellationDate__c', subTermEndDate);
        	}
        	
        	integer entier = 0;
        	entier++;
        	entier++;
        	entier++;
        	entier++;
        	entier++;
        	entier++;
        	entier++;
        	entier++;
        	entier++;
        	entier++;

            record.put('zqu__InvoiceDate__c', Date.today());
            record.put('zqu__InvoiceTargetDate__c', Date.today());
            record.put('zqu__StartDate__c', Date.today());
         
            //record.put('zqu__ValidUntil__c', Date.newInstance(2015, 10, 30));
        }

        System.debug('###### INFO: ZuoraQuoteDefaultValues execution END.');
    }
}


/*
global class ZuoraQuoteDefaultValues extends zqu.CreateQuoteController.PopulateDefaultFieldValuePlugin{ 
   	global override void populateDefaultFieldValue(SObject record, zqu.PropertyComponentController.ParentController pcc){   
		/*
		super.populateDefaultFieldValue(record, pcc); 

		//Populate default values in the quote header 
		record.put('zqu__InitialTerm__c', 15);   
		record.put('zqu__RenewalTerm__c', 12);   
		record.put('zqu__ValidUntil__c', Date.today().addDays(30));   
		record.put('zqu__StartDate__c', Date.today());   
		record.put('zqu__PaymentMethod__c', 'Check'); 

		// Retrieve the account ID from the quote        
		Id accountId = (Id) record.get('zqu__Account__c');        

		// Find the contacts associated with the account        
		List<Contact>contacts = [SELECT Id, Name FROM Contact WHERE Account.Id = :accountId];        

		// Assuming the contacts are present set the billTo and soldTo to the first contact        
		if  (contacts.size() > 0) {            
		// System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
		record.put('zqu__BillToContact__c', contacts[0].Id);            
		record.put('zqu__SoldToContact__c', contacts[0].Id);            

		// Beforeretrieving  the lookup  options, needs to populate the map first            
		super.setLookupOptions(pcc);            

		// Now retrieve the lookup component options            
		zqu.LookupComponentOptions billToOptions = super.getLookupOption('zqu__BillToContact__c');            
		billToOptions.targetId = contacts[0].Id;            
		billToOptions.targetName = contacts[0].Name;            
		zqu.LookupComponentOptions soldToOptions  = super.getLookupOption('zqu__SoldToContact__c');            
		soldToOptions.targetId = contacts[0].Id;            
		soldToOptions.targetName = contacts[0].Name;  
		//*//*  
      	}
   	}
}
*/