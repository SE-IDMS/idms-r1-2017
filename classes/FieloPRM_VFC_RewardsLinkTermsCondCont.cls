/************************************************************
* Developer: Fielo Team                                     *
* Type: VF Page Extension                                   *
* Page: FieloPRM_VFP_RewardsLinkTermsConditions             *
************************************************************/
public with sharing class FieloPRM_VFC_RewardsLinkTermsCondCont{

    public string linkTermsAndConditions {get;set;}
    
    private String languageMember = FieloEE.OrganizationUtil.getLanguage(); 

    public FieloPRM_VFC_RewardsLinkTermsCondCont() {
    
    }  
    
    public PageReference goToTermsConditions(){
        String memId = FieloEE.MemberUtil.getMemberId();
        FieloEE__Member__c member = [SELECT F_PRM_CountryCode__c FROM FieloEE__Member__c WHERE Id  =: memId];
        PageReference linkTermsAndConditions;
        if(member.F_PRM_CountryCode__c != null && languageMember != null){
            linkTermsAndConditions = new PageReference('/partners/Menu/P_' + member.F_PRM_CountryCode__c.toUpperCase() + '_' + languageMember.toUpperCase() + '_Rewards_TermsAndConditions');
        }else{
            linkTermsAndConditions = new PageReference('/partners');
        }
        linkTermsAndConditions.setRedirect(true);
        system.debug('### Rewards linkTermsAndConditions: ' + linkTermsAndConditions);
        return linkTermsAndConditions;
    } 
    
}