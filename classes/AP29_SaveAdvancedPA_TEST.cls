@istest

private class AP29_SaveAdvancedPA_TEST
{

    static testMethod void TEST_savePA()
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= [Select Id, Name from Country__c Limit 1].Id;
        insert contact1;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert case1;
    
        AP29_SaveAdvancedPA.AdvancedPA PAResult= new AP29_SaveAdvancedPA.AdvancedPA();

        AP29_SaveAdvancedPA.SaveAdvancedPA(PAResult, case1.ID, account1.ID);
    }
}