/*
    Author          : Accenture Team
    Date Created    : 26/05/2011
    Description     : Test class for AP11_UpdateQuoteLink class.
*/
@isTest
private class AP11_UpdateQuoteLink_TEST {
static testMethod void testUpdateQuoteLink() {
     Account accounts = Utils_TestMethods.createAccount();
     Database.insert(accounts); 
    
    Opportunity opportunity= Utils_TestMethods.createOpportunity(accounts.id);
    opportunity.OpptyPriorityLevel__c='standard'; 
    Database.insert(opportunity);        
         
    list<OPP_QuoteLink__c> quoteLink = new list<OPP_QuoteLink__c >();
    for(integer i=1;i<=200;i++)
    {
    OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
    quoteLink.add(newQuoteLink);
    }
    Database.insert(quoteLink); 
    
        Test.startTest();
        opportunity.OpptyPriorityLevel__c='High';    
        Database.update(opportunity); 
        Test.stopTest();     
        
    } 
 }