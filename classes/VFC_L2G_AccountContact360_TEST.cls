@isTest 
public class VFC_L2G_AccountContact360_TEST {
    
    @isTest static void testL2GAccount() {
        Country__c objAccountCountry = new Country__c();
        objAccountCountry.CountryCode__c = 'IN';
        insert objAccountCountry;
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objAccountCountry.ID;
        insert objAccount;
                
        Id accId = [select Id from Account limit 1].Id;
        PageReference pageRef = Page.VFP_L2G_Contact360;
        pageRef.getParameters().put('bFOId', accId);
        pageRef.getParameters().put('bFOType', 'Account');        
        Test.setCurrentPage(pageRef);
      
        VFC_L2G_AccountContact360 controller = new VFC_L2G_AccountContact360();     

    }
        
    @isTest static void testL2GContact() {
        Country__c objAccountCountry = new Country__c();
        objAccountCountry.CountryCode__c = 'GB';
        insert objAccountCountry;
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objAccountCountry.ID;
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContactForL2G');
        insert objContact;
               
        Id cttId = [select Id from Contact where AccountId != null limit 1].Id;
        PageReference pageRef = Page.VFP_L2G_Contact360;
        pageRef.getParameters().put('bFOId', cttId);
        pageRef.getParameters().put('bFOType', 'Contact');
        Test.setCurrentPage(pageRef);
      
        VFC_L2G_AccountContact360 controller = new VFC_L2G_AccountContact360();
        controller.getJsonSfdcObjectMap();        
        controller.getSfdcId18();
        controller.getCurrentTimestamp();
        String s = controller.name;
    }
    
    @IsTest static void testL2GCase() {
        Country__c objAccountCountry = new Country__c();
        objAccountCountry.CountryCode__c = 'US';
        insert objAccountCountry;
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objAccountCountry.ID;
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContactForL2G');
        insert objContact;
               
        Case objCaseForL2G = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'New');
        insert objCaseForL2G;
        
        Case cse = [select Id, ContactId from Case where ContactId != null limit 1];
        PageReference pageRef = Page.VFP_L2G_Contact360; 
        pageRef.getParameters().put('bFOId', cse.ContactId);
        pageRef.getParameters().put('bFOType', 'Contact');
        pageRef.getParameters().put('newid', cse.Id);
        pageRef.getParameters().put('newObjectType', 'Case');
        Test.setCurrentPage(pageRef);
        VFC_L2G_AccountContact360 controller = new VFC_L2G_AccountContact360();

    }
    
    @IsTest static void testL2GOppNotif() {

        Country__c objAccountCountry = new Country__c();
        objAccountCountry.CountryCode__c = 'IT';
        insert objAccountCountry;
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objAccountCountry.ID;
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContactForL2G');
        insert objContact;
               
        Case objCaseForL2G = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'New');
        insert objCaseForL2G;

        OpportunityNotification__c objCaseOppNotif = new OpportunityNotification__c(Name='Test Opp Notif', Case__c=objCaseForL2G.Id, Amount__c=100000, CurrencyIsoCode='EUR', Stage__c='3 - Identify & Qualify', OpportunityCategory__c='Simple', OpportunityType__c='Solutions');
        insert objCaseOppNotif;
        
        OpportunityNotification__c oppNotif = [select Id, Case__r.ContactId from OpportunityNotification__c where Case__c != null and Case__r.ContactId != null limit 1];
        PageReference pageRef = Page.VFP_L2G_Contact360;
        if(oppNotif!=null){
            pageRef.getParameters().put('bFOId', oppNotif.Case__r.ContactId);
            pageRef.getParameters().put('bFOType', 'Contact');
            pageRef.getParameters().put('newid', oppNotif.Id);
            pageRef.getParameters().put('newObjectType', 'OpportunityNotification');
        }
        Test.setCurrentPage(pageRef);
        VFC_L2G_AccountContact360 controller = new VFC_L2G_AccountContact360();
    }
    
    @isTest static void testCallout() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        VFC_L2G_AccountContact360.getContent('{searchstring:eric}');
        VFC_L2G_AccountContact360.getDocument('{searchstring:eric}');
        VFC_L2G_AccountContact360.getWelcome('{searchstring:eric}');
        VFC_L2G_AccountContact360.getPrediction('{searchstring:eric}');
        VFC_L2G_AccountContact360.postUpdateQuery('{searchstring:eric}');
        
    }
    
    @isTest static void testOtherFunc() {
    
        PageReference pageRef = Page.VFP_L2G_Contact360;
        pageRef.getParameters().put('bFOId', '');
        pageRef.getParameters().put('X-Salesforce-Forwarded-To', '');
        Test.setCurrentPage(pageRef);
        VFC_L2G_AccountContact360 controller = new VFC_L2G_AccountContact360();
        controller.getSfdcBaseHostname();
    }
}