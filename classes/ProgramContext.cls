global class ProgramContext {
    WebService String[] partnerProgramIDs;
    //WebService ProgramType progType;
    WebService ProgramType[] progTypes;
    WebService Integer BatchSize = 10000;
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    /**
    * A blank constructor
    */
    public ProgramContext() {
    }

    /**
    * A constructor based on an Program @param a Program
    */
    public ProgramContext(ProgramType[] types, List<String> programIDs) {
        this.partnerProgramIDs = programIDs;
        //this.progType = type;
        this.progTypes = types;
    }
}