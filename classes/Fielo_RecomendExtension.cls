/************************************************************
* Developer: Fielo                                          *
************************************************************/

public with sharing class Fielo_RecomendExtension {
    
    public String siteURL {get;set;}
    
    public Fielo_RecomendExtension(FieloEE.ProgramController controller){
        
        siteURL = '';
        List<FieloEE__Program__c> prog = [SELECT FieloEE__SiteURL__c FROM FieloEE__Program__c WHERE Name = 'Schneider Electric Rewards Program' LIMIT 1];
        
        if(!prog.isEmpty()){
            siteURL = prog[0].FieloEE__SiteURL__c;
        }
    
    }
    
    
}