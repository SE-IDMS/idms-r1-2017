/**
23-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for VFC_ShowContactRelatedDetails
 */
@isTest
private class VFC_ShowContactRelatedDetails_TEST {
      
      static testMethod void myUnitTest() {
        //Test converage for the myPage visualforce page

        PageReference pageRef = Page.VFP_ShowContactRelatedDetails;
        
        Test.setCurrentPageReference(pageRef);
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id,'test contact');
        
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(testcontact);
        VFC_ShowContactRelatedDetails myPageCon = new VFC_ShowContactRelatedDetails(sdtCon);
        
        insert testcontact;
        
        
        list<Case> lstCase = new list<case>();
        for(integer i=0; i<20;i++)
        {
            Case testCase = Utils_TestMethods.createCase(testAccount.Id,testcontact.Id,'Open');
            lstCase.add(testCase);
        }
        insert lstCase;
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(testcontact);
        VFC_ShowContactRelatedDetails myPageCon1 = new VFC_ShowContactRelatedDetails(sdtCon1);
       
      }
}