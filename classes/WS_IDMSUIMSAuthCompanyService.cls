/**
•   Created By: 
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: Below Class and Sub Class are generated from UIMS WSDL
**/  

//cloned from uimsv2ServiceImsSchneiderComNew
public class WS_IDMSUIMSAuthCompanyService{
    public class acceptCompanyMerge {
        public String callerFid;
        public String authentificationToken;
        public String federatedId;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] authentificationToken_type_info = new String[]{'authentificationToken','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','authentificationToken','federatedId'};
    }
    public class companyV3 {
        public String accountLifecycleCode;
        public String accountType;
        public String addInfoAddress;        
        public String addInfoAddressECS;
        public String annualSales;
        public String bdDomainExpertise;
        public String bfoId;
        public String businessType;
        public String businessUnit;
        public String cityECS;
        public String competitorFlag;
        public String coordLatitude;
        public String coordLongitude;
        public String countryCode;
        public String county;
        public String countyECS;
        public String currencyCode;
        public String customerClass;
        public String deletionFlag;
        public String employeeSize;
        public String exportControlCode;
        public String facsimileTelephoneNumber;
        public String federatedId;
        public String goldenId;
        public String headQuarter;
        public String itDomainExpertise;
        public String keyAccountTypeCode;
        public String languageCode;
        public String legalName;
        public String localityName;
        public String marketSegment;
        public String marketServed;
        public String nameECS;
        public String note;
        public String numberEmployees;
        public String organizationName;
        public Boolean partnerLocatorFlag;
        public String pmDomainExpertise;
        public String postOfficeBox;
        public String postOfficeBoxCode;
        public String postalAddress;
        public String postalCity;
        public String postalCode;
        public String postalCountry;
        public String postalCounty;
        public String postalStateProvince;
        public String preferredDistributor1;
        public String preferredDistributor2;
        public String preferredDistributor3;
        public Boolean publicVisibility;
        public String publisherGoldenId;
        public String pwDomainExpertise;
        public String relationshipLeaderCode;
        public String saccIdParent;
        public String saccIdUltimate;
        public String sdhVersion;
        public String seAccountOwner;
        public String securityDomainExpertise;
        public String shortName;
        public String shortNameECS;
        public String sourceSystemId;
        public String state;
        public String street;
        public String streetECS;
        public String taxIdentificationNumber;
        public String taxOffice;
        public String telephoneNumber;
        public String timeZone;
        public String webSite;
        private String[] accountLifecycleCode_type_info = new String[]{'accountLifecycleCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] accountType_type_info = new String[]{'accountType','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] addInfoAddressECS_type_info = new String[]{'addInfoAddressECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] addInfoAddress_type_info = new String[]{'addInfoAddress','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] annualSales_type_info = new String[]{'annualSales','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] bdDomainExpertise_type_info = new String[]{'bdDomainExpertise','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] bfoId_type_info = new String[]{'bfoId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] businessType_type_info = new String[]{'businessType','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] businessUnit_type_info = new String[]{'businessUnit','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] cityECS_type_info = new String[]{'cityECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] competitorFlag_type_info = new String[]{'competitorFlag','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] coordLatitude_type_info = new String[]{'coordLatitude','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] coordLongitude_type_info = new String[]{'coordLongitude','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] countryCode_type_info = new String[]{'countryCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] county_type_info = new String[]{'county','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] countyECS_type_info = new String[]{'countyECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] currencyCode_type_info = new String[]{'currencyCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] customerClass_type_info = new String[]{'customerClass','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] deletionFlag_type_info = new String[]{'deletionFlag','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] employeeSize_type_info = new String[]{'employeeSize','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] exportControlCode_type_info = new String[]{'exportControlCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] facsimileTelephoneNumber_type_info = new String[]{'facsimileTelephoneNumber','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] goldenId_type_info = new String[]{'goldenId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] headQuarter_type_info = new String[]{'headQuarter','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] itDomainExpertise_type_info = new String[]{'itDomainExpertise','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] keyAccountTypeCode_type_info = new String[]{'keyAccountTypeCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] languageCode_type_info = new String[]{'languageCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] legalName_type_info = new String[]{'legalName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] localityName_type_info = new String[]{'localityName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] marketSegment_type_info = new String[]{'marketSegment','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] marketServed_type_info = new String[]{'marketServed','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] nameECS_type_info = new String[]{'nameECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] note_type_info = new String[]{'note','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] numberEmployees_type_info = new String[]{'numberEmployees','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] organizationName_type_info = new String[]{'organizationName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] partnerLocatorFlag_type_info = new String[]{'partnerLocatorFlag','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] pmDomainExpertise_type_info = new String[]{'pmDomainExpertise','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postOfficeBox_type_info = new String[]{'postOfficeBox','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postOfficeBoxCode_type_info = new String[]{'postOfficeBoxCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalAddress_type_info = new String[]{'postalAddress','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalCity_type_info = new String[]{'postalCity','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalCode_type_info = new String[]{'postalCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalCountry_type_info = new String[]{'postalCountry','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalCounty_type_info = new String[]{'postalCounty','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalStateProvince_type_info = new String[]{'postalStateProvince','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] preferredDistributor1_type_info = new String[]{'preferredDistributor1','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] preferredDistributor2_type_info = new String[]{'preferredDistributor2','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] preferredDistributor3_type_info = new String[]{'preferredDistributor3','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] publicVisibility_type_info = new String[]{'publicVisibility','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] publisherGoldenId_type_info = new String[]{'publisherGoldenId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] pwDomainExpertise_type_info = new String[]{'pwDomainExpertise','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] relationshipLeaderCode_type_info = new String[]{'relationshipLeaderCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] saccIdParent_type_info = new String[]{'saccIdParent','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] saccIdUltimate_type_info = new String[]{'saccIdUltimate','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] sdhVersion_type_info = new String[]{'sdhVersion','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] seAccountOwner_type_info = new String[]{'seAccountOwner','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] securityDomainExpertise_type_info = new String[]{'securityDomainExpertise','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] shortName_type_info = new String[]{'shortName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] shortNameECS_type_info = new String[]{'shortNameECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] sourceSystemId_type_info = new String[]{'sourceSystemId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] state_type_info = new String[]{'state','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] street_type_info = new String[]{'street','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] streetECS_type_info = new String[]{'streetECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] taxIdentificationNumber_type_info = new String[]{'taxIdentificationNumber','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] taxOffice_type_info = new String[]{'taxOffice','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] telephoneNumber_type_info = new String[]{'telephoneNumber','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] timeZone_type_info = new String[]{'timeZone','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] webSite_type_info = new String[]{'webSite','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'accountLifecycleCode','accountType','addInfoAddress','addInfoAddressECS','annualSales','bdDomainExpertise','bfoId','businessType','businessUnit','cityECS','competitorFlag','coordLatitude','coordLongitude','countryCode','county','countyECS','currencyCode','customerClass','deletionFlag','employeeSize','exportControlCode','facsimileTelephoneNumber','federatedId','goldenId','headQuarter','itDomainExpertise','keyAccountTypeCode','languageCode','legalName','localityName','marketSegment','marketServed','nameECS','note','numberEmployees','organizationName','partnerLocatorFlag','pmDomainExpertise','postOfficeBox','postOfficeBoxCode','postalAddress','postalCity','postalCode','postalCountry','postalCounty','postalStateProvince','preferredDistributor1','preferredDistributor2','preferredDistributor3','publicVisibility','publisherGoldenId','pwDomainExpertise','relationshipLeaderCode','saccIdParent','saccIdUltimate','sdhVersion','seAccountOwner','securityDomainExpertise','shortName','shortNameECS','sourceSystemId','state','street','streetECS','taxIdentificationNumber','taxOffice','telephoneNumber','timeZone','webSite'};
    }
    public class updateCompany {
        public String callerFid;
        public String samlAssertion;
        public String federatedId;
        public WS_IDMSUIMSAuthCompanyService.companyV3 company;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] samlAssertion_type_info = new String[]{'samlAssertion','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] company_type_info = new String[]{'company','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','samlAssertion','federatedId','company'};
    }
    public class InvalidImsServiceMethodArgumentException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class searchCompany {
        public String callerFid;
        public String samlAssertion;
        public WS_IDMSUIMSAuthCompanyService.companyV3 template;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] samlAssertion_type_info = new String[]{'samlAssertion','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] template_type_info = new String[]{'template','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','samlAssertion','template'};
    }
    public class RequestedEntryNotExistsException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class rejectCompanyMergeResponse {
        public Boolean success;
        private String[] success_type_info = new String[]{'success','http://uimsv2.service.ims.schneider.com/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'success'};
    }
    public class getCompanyResponse {
        public WS_IDMSUIMSAuthCompanyService.companyV3 company;
        private String[] company_type_info = new String[]{'company','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'company'};
    }
    public class UnexpectedLdapResponseException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class LdapTemplateNotReadyException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class updateCompanyResponse {
        public Boolean success;
        private String[] success_type_info = new String[]{'success','http://uimsv2.service.ims.schneider.com/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'success'};
    }
    public class rejectCompanyMerge {
        public String callerFid;
        public String authentificationToken;
        public String federatedId;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] authentificationToken_type_info = new String[]{'authentificationToken','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','authentificationToken','federatedId'};
    }
    public class RequestedInternalUserException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class SecuredImsException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class getCompany {
        public String callerFid;
        public String samlAssertionOrToken;
        public String federatedId;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] samlAssertionOrToken_type_info = new String[]{'samlAssertionOrToken','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','samlAssertionOrToken','federatedId'};
    }
    public class removePrimaryContact {
        public String callerFid;
        public String samlAssertion;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] samlAssertion_type_info = new String[]{'samlAssertion','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','samlAssertion'};
    }
    public class removePrimaryContactResponse {
        public Boolean success;
        private String[] success_type_info = new String[]{'success','http://uimsv2.service.ims.schneider.com/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'success'};
    }
    public class createCompanyResponse {
        public String federatedId;
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'federatedId'};
    }
    public class createCompany {
        public String callerFid;
        public String samlAssertion;
        public WS_IDMSUIMSAuthCompanyService.companyV3 company;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] samlAssertion_type_info = new String[]{'samlAssertion','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] company_type_info = new String[]{'company','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','samlAssertion','company'};
    }
    public class acceptCompanyMergeResponse {
        public Boolean success;
        private String[] success_type_info = new String[]{'success','http://uimsv2.service.ims.schneider.com/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'success'};
    }
    public class IMSServiceSecurityCallNotAllowedException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class searchCompanyResponse {
        public WS_IDMSUIMSAuthCompanyService.companyV3[] companies;
        private String[] companies_type_info = new String[]{'companies','http://uimsv2.service.ims.schneider.com/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'companies'};
    }
}