/*
17-Feb-2014    Srinivas Nallapati  PRM Apr 14   Initial Creation

To Run this batch from console
  Batch_ProcessContactFeatures batch_pcf = new Batch_ProcessContactFeatures(); 
  batch_pcf.query = 'SELECT id,Contact__c, PartnerProgram__c, ProgramLevel__c, AccountAssignedProgram__c FROM ContactAssignedProgram__c WHERE Active__c= true AND LastModifiedDate =Last_N_Days:15';
    Database.executebatch(batch_pcf);
*/
global class Batch_ProcessContactFeatures implements Database.Batchable<sObject>,Queueable {
  

   public void execute(QueueableContext context) {
      Batch_ProcessContactFeatures bpcf = new Batch_ProcessContactFeatures();
      Database.executebatch(bpcf,50);       
   }
  public String query; 
  
  global Batch_ProcessContactFeatures() {
    
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    
    if(query != null)
      return Database.getQueryLocator(query);
    else
    {
      List<ProgramFeature__c> lstDeactivatedFeatures = [Select id, ProgramLevel__c FROM ProgramFeature__c WHERE FeatureStatus__c ='Inactive' AND DeactivatedDate__c=TODAY];
      set<id> progLevelIds = new set<id>();
      for(ProgramFeature__c pf : lstDeactivatedFeatures)
      {
        progLevelIds.add(pf.ProgramLevel__c);
      }

      query = 'SELECT id,Contact__c, PartnerProgram__c, ProgramLevel__c, AccountAssignedProgram__c FROM ContactAssignedProgram__c WHERE Active__c= true AND ProgramLevel__c IN :progLevelIds';
      return Database.getQueryLocator(query);
    }  
  }

  global void execute(Database.BatchableContext BC, List<ContactAssignedProgram__c> scope) {
     AP_ProcessFeatureAssignment.processContactAssignedFeatures(scope);
  }
  
  global void finish(Database.BatchableContext BC) { 
  
    Integer numFlexJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Holding' OR Status = 'Queued' OR Status = 'Preparing'];
    // Check to avoid Limit Exception in FlexQueue
    CS_PRM_ApexJobSettings__c cntRes = CS_PRM_ApexJobSettings__c.getInstance('MAX_JOB_COUNT');
    if( numFlexJobs < cntRes.NumberValue__c ){
        Id jobId = System.enqueueJob(new Batch_ProcessPartnerPermissionSets());
    }    
  }
    
}