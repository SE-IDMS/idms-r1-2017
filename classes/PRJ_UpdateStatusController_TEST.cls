@isTest
private class PRJ_UpdateStatusController_TEST
{
    
    static testMethod void test_PRJ_UpdateStatusController() 
    {
    
        //User runAsUser = Utils_TestMethods.createStandardDMTUser('tst1');
        
        //insert runAsUser;
        //System.runAs(runAsUser)
        //{ 
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            insert testProj; 
            
            
            Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
            insert testBud;            
           
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
            insert testDMTA2;   
            
            testProj.NextStep__c='Created';
            update testProj;
            
            PRJ_UpdateStatusController vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
                       
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
            
            DMTProgram__c dmtProg = new DMTProgram__c();
            dmtProg.BusinessConnectProgram__c = 'Global Customer Intimacy - Best in class key account management';
            dmtProg.Name = 'B2B eCommerce';
            dmtProg.BusinessOrgFamily__c = 'Business';
            dmtProg.Business_Organization__c = 'Buildings';
            dmtProg.SEOrganizationL1__c = 'India';
            dmtProg.SEOrganizationL2__c = '';
            insert dmtProg;
            
            DMTAuthorizationMasterData__c testD1 = new DMTAuthorizationMasterData__c();
            testD1.AuthorizedUser5__c = UserInfo.getUserId();
            testD1.AuthorizedUser6__c = UserInfo.getUserId();
            testD1.NextStep__c = 'Quoted';
            testD1.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testD1;
            
            testProj.NextStep__c = 'Valid';
            update testProj;
            
            vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
            
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
            
            
            test.startTest();
            
            DMTAuthorizationMasterData__c testDMTA5 = new DMTAuthorizationMasterData__c();
            testDMTA5.AuthorizedUser1__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser5__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA5.NextStep__c = 'Fundings Authorized';
            testDMTA5.Parent__c = testProj.Parent__c;
            testDMTA5.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA5.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA5; 
            
            testProj.NextStep__c = 'Quoted';
            update testProj;
            
            
            vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
            testProj.NextStep__c='Fundings Authorized';
            testProj.AuthorizeFunding__c = 'Yes';
            update testProj;
            
            vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
            
            test.stopTest();
            
    }
     static testMethod void test_PRJ_UpdateStatusController2() 
    {
           User u=[select id , name from user where   Profile.Name ='System Administrator' and IsActive =true  limit 1 ];
            

            System.runAs(u){
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            insert testProj; 
            
            PRJ_Stakeholder__c PrjStakeholder;
            PrjStakeholder=Utils_TestMethods_DMT.createDMTPRJStakeholder(testProj);
            PrjStakeholder.Exclude__c =false;
            insert PrjStakeholder;
            
            //PRJ_ProjectReq__c testProjs =[SELECT PreviousStep__c,EmailCC__c,EmailTo__c,BusinessTechnicalDomain__c FROM PRJ_ProjectReq__c where id =:testProj.id];
            PRJ_ProjectReq__c testProjs = new PRJ_ProjectReq__c(Id = testProj.id);
            
            Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
            insert testBud;            
            
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
            insert testDMTA2;   
            
            testProj.NextStep__c='Created';
            update testProj;
                        
            
            PRJ_UpdateStatusController vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
                       
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
            
            DMTAuthorizationMasterData__c testD1 = new DMTAuthorizationMasterData__c();
            testD1.AuthorizedUser5__c = UserInfo.getUserId();
            testD1.AuthorizedUser6__c = UserInfo.getUserId();
            testD1.NextStep__c = 'Quoted';
            testD1.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testD1;
            
            testProj.NextStep__c = 'Valid';
            test.startTest();
            update testProj;

            
            vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
            
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
            
            vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
            
            DMTAuthorizationMasterData__c testDMTA5 = new DMTAuthorizationMasterData__c();
            testDMTA5.AuthorizedUser1__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser5__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA5.NextStep__c = 'Fundings Authorized';
            testDMTA5.Parent__c = testProj.Parent__c;
            testDMTA5.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA5.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA5; 
           
            testProj.NextStep__c = 'Quoted';
            update testProj;
           
            vfCtrl.updateProjectRequest();
            vfCtrl.backToProjectRequest();
           
            //Srikant Modified for the July release
            //PRJ_ProjectReq__c testProjs =[SELECT PreviousStep__c,EmailCC__c,EmailTo__c,BusinessTechnicalDomain__c FROM PRJ_ProjectReq__c where id =:testProj.id];
            testProjs.PreviousStep__c =  'IPO IT Request Validate Proposals';
            System.debug('------&------'+testProjs.EmailTo__c+testProj.id);
            List<PRJ_ProjectReq__c> listPRJProject = new List<PRJ_ProjectReq__c>();
            List<PRJ_ProjectReq__c> listPRJProjects = new List<PRJ_ProjectReq__c>();            
            listPRJProject.add(testProj);
            listPRJProjects.add(testProj);
            listPRJProjects.add(testProjs);
            System.debug('------&8------'+listPRJProjects[0].Id );
            
            DMTFundingEntity__c PRJ_FundEntity;
            PRJ_FundEntity=Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012');
            insert PRJ_FundEntity;
            
            PRJ_FundEntity.ActiveForecast__c = true;
            update PRJ_FundEntity;
            //PRJ_ProjectUtils PRJProjectUtilsController = new PRJ_ProjectUtils();
            PRJ_ProjectUtils.sendStepChangeNotification(listPRJProjects);
            //PRJ_ProjectUtils.sendStepChangeNotification(listPRJProject);
            PRJ_ProjectUtils.verifyHFMCodeEnteredandSelectedInEnvelop(listPRJProject);
            PRJ_ProjectUtils.sendCreatedNotification(listPRJProject,'test');
            test.stopTest();
            
            }
            
           
            
    }
    
     static testMethod void test_PRJ_UpdateStatusController3() 
    {
        
        RecordType rt=[SELECT Id,Name FROM RecordType where SobjectType ='PRJ_ProjectReq__c' and Name ='Document Project Request'];
       List<String> stidList = new List<String>();
        string st = System.Label.DMT_AuthorizedUserIDForValid;
        list<String> stList = st.split(',');
        for(String s: stList){
            stidList.add(s);
        }
       
       // User createUser=Utils_TestMethods.createStandardUser('test');
        //insert createUser;
        User createUser=[select id, name from User where id=:stidList[0] ];
         
        /*
        try{
          
        }
        catch(Exception ex){
            System.debug('Debug RK-->>'+ex);
        }
        */
        
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(createUser.id,'Created','Business','Buildings','APAC');
        testDMTA1.AuthorizedUser1__c =createUser.id; 
        insert testDMTA1;
        PRJ_ProjectReq__c testProj = Utils_TestMethods.createPRJRequest();
        testProj.RecordTypeID=rt.id;
        testProj.BusGeographicZoneIPO__c='APAC';
        testProj.Parent__c = 'Buildings';
        testProj.WorkType__c  = 'Master Project';
        testProj.ParentFamily__c = 'Business';
        insert testProj;
        System.debug('Debug RK-->>> : '+testProj.WorkType__c  );
        Budget__c Budget = new Budget__c(ProjectReq__c =testProj.id,Active__c=true);
        Insert Budget ;
        PageReference pageRef = Page.PRJ_UpdateStatus;
        ApexPages.StandardController stdCon=new ApexPages.StandardController(testProj);
        PRJ_UpdateStatusController prjUpdateSCon=new PRJ_UpdateStatusController(stdCon);
        testProj.BusinessTechnicalDomain__c =  'Supply Chain';
        update testProj;
        System.RunAs(createUser){
            PRJ_UpdateStatusController createStage=new PRJ_UpdateStatusController(stdCon);
            createStage.oPR.NextStep__c = 'Created' ;
            createStage.updateProjectRequest();
            System.debug('Debug RK-->>> : '+testProj.NextStep__c);
            System.debug('Debug RK-->>> : '+testProj.WorkType__c  );
            PRJ_UpdateStatusControllerVerify.updateProjectRequest(testProj,null,false,'Valid');
        }
       // System.assertEquals(testProj.NextStep__c, 'Created');
       
        
        //Test.setCurrentPage(pageRef);
        //testProj.NextStep__c = 'Created';
        //testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        //update testProj;
    }
    
    static testMethod void test_PRJ_UpdateStatusController_4() 
    {
       
    // Added as part of April 2014 release
    
           User u=[select id , name from user where   Profile.Name ='System Administrator' and IsActive =true  limit 1 ];
            

            System.runAs(u){
            test.startTest();
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            insert testProj;
            
            Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
            insert testBud;            
           
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            testProj.Parent__c = 'Buildings';
            testProj.ExpectedStartDate__c = System.today();
            testProj.GoLiveTargetDate__c =System.today().addDays(2);           
           
            update testProj;     
            
            
                        
            DMTAuthorizationMasterData__c testDMTA11 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),System.Label.DMT_StatusCreated,testProj.Parent__c,testProj.BusGeographicZoneIPO__c);
            insert testDMTA11;           
            
            testProj.NextStep__c='Created';
            update testProj;
            PRJ_UpdateStatusController vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
                       
            vfCtrl.updateProjectRequest();
            
            // Valid Gate
            
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),System.Label.DMT_StatusValid,testProj.Parent__c,testProj.BusGeographicZoneIPO__c);
            insert testDMTA2;
            
            testProj.NextStep__c = 'Valid';
            testProj.substatus__c = 'working';
            testProj.AppGlobalOpservdecisiondate__c = System.today();
            testProj.AppServicesDecision__c = 'Yes';
            testProj.AppServicesDecision__c = 'Yes';     
            testProj.BusinessvalueEvaluationBVEScore__c = 100;
            testProj.InvestmentClassification__c = 'C1 - Address Business Continuity';       
            update testProj;
            
            PRJ_UpdateStatusController vfCtr0 = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
                       
            vfCtr0.updateProjectRequest();
            
            testProj.NextStep__c = 'Valid';
            testProj.substatus__c = 'complete';
            update testProj;
            PRJ_UpdateStatusController vfCtrl1 = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
                       
            vfCtrl1.updateProjectRequest();
            
            
            // Quoted Gate
            
            DMTAuthorizationMasterData__c testDMTA3 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),System.Label.DMT_StatusQuoted,testProj.BusinessTechnicalDomain__c,testProj.BusGeographicZoneIPO__c);
            insert testDMTA3;
            
            testProj.NextStep__c = 'Quoted';
            
                      
            update testProj;
            
            PRJ_UpdateStatusController vfCtr4 = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
                       
            vfCtr4.updateProjectRequest(); 
                
            test.stopTest();
            
            
            PRJ_ProjectReq__c testchr = Utils_TestMethods.createPRJRequest();                      
            insert testchr;
            
            Budget__c chrBud = Utils_TestMethods.createBudget(testchr,true);
            insert chrBud;
                   
           
            testchr.BusinessTechnicalDomain__c = 'Supply Chain';
            testchr.Parent__c = 'Buildings';
            testchr.ExpectedStartDate__c = System.today();
            testchr.GoLiveTargetDate__c =System.today().addDays(2);
            testchr.WorkType__c = 'Change Request';         
           
            update testchr;
            
            PRJ_UpdateStatusController vfCtrl3 = new PRJ_UpdateStatusController(new ApexPages.StandardController(testchr));
                       
            vfCtrl3.updateProjectRequest();
            
            testchr.NextStep__c='Created';
            update testchr;
                     
          } 
         
            
        }
    }