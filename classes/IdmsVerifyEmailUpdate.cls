/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is used to verify email and mobile update
**/
public class IdmsVerifyEmailUpdate {
    //To verify the email update
    public static boolean verifyEmail(String username,String newEmail){
        User userdetails=[select Id,username,email from user where username=:username limit 1];
        if(userdetails != null && userdetails.email!=newEmail)
        {
            return true;
        }
        return false;
    }
    
    //To verify the phone update
    public static boolean verifyPhone(String username,String mobile){
        User userdetails=[select Id,username,mobilephone from user where username=:username limit 1];
        if (userdetails != null && userdetails.mobilephone != mobile)
        {
            return true;
        }
        return false;
    }
    
}