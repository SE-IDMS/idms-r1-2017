/*
bp
Description :Test class VFC_AddLEOrderedProduct
*/

@isTest//(SeeAllData=true)
Public class VFC_AddLEOrderedProduct_TEST {
    
    public static testmethod void Test_AddLEOrderedProduct() {
  //  Test.startTest();
         // Create Country
       /* Country__c CountryObjLCR = Utils_TestMethods.createCountry();
       
        Insert CountryObjLCR;
        Role__c rolse= new Role__c();
        rolse.Role__c='CEO';
        insert rolse;*/
        country__c CountryObjLCR=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            CountryObjLCR=countries[0];
        else{               
            CountryObjLCR=new country__c(Name='TestCountry',CountryCode__c='IN');
            insert CountryObjLCR;    
        }
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCROwner');
        LCRTestUser.ManagerEmail__c='sukumar.salla@schneider-electric.com';
        LCRTestUser.Manager1Email__c='shivanand.rao@schneider-electric.com';
        LCRTestUser.Email='ram.chilukuri@schneider-electric.com';
      //  LCRTestUser.UserRole=rolse.id;
        Insert LCRTestUser ;
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
      //  LCRTestUserCq.UserRole=rolse.id;
      LCRTestUserCq.ManagerEmail__c='sukumar.salla@schneider-electric.com';
        LCRTestUserCq.Manager1Email__c='shivanand.rao@schneider-electric.com';
        LCRTestUserCq.Email='ram.chilukuri@schneider-electric.com';
        Insert LCRTestUserCq ;
        
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
       
        insert ContactObj;
        
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
        
        CaseObj.CommercialReference__c= 'Test Commercial Reference';
        CaseObj.ProductBU__c = 'Business';
        CaseObj.ProductLine__c = 'ProductLine';
        CaseObj.ProductFamily__c ='ProductFamily';
        CaseObj.Family__c ='Family';
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
        CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
         CaseObj.LevelOfExpertise__c ='Advanced';
        Insert CaseObj;
       /* 
        Case CaseObj1= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Closed');
        
        CaseObj1.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj1.Symptom__c =  'Installation/ Setup';
        CaseObj1.SubSymptom__c = 'Hardware';    
        CaseObj1.Quantity__c = 4;
        CaseObj1.AnswerToCustomer__c='Test-AnswerToCustomer';
        CaseObj1.CustomerRequest__c='Test-CustomerRequest';
        CaseObj1.Family__c = 'ADVANCED PANEL';
        CaseObj1.CommercialReference__c='xbtg5230';
        CaseObj1.ProductFamily__c='HMI SCHNEIDER BRAND';
        CaseObj1.TECH_GMRCode__c='02BF6DRG16NBX07632';
        Insert CaseObj1;*/
        
         // Create Organization (Complaint request)
         BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='SCH@schneider.com'
                                            );
        Insert lCROrganzObj; 
         BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST Repair',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity1',
                                            Location__c='LCR Location 1',
                                            Location_Type__c= 'LCR',
                                            Street__c = 'LCR Street1',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City1',
                                            RCZipCode__c = '500225',
                                            ContactEmail__c ='SCH1@schneider.com'
                                            );
        Insert lCROrganzObj1; 
        
        EntityStakeholder__c  LCROrgStakeHold= new EntityStakeholder__c(
                                User__c=LCRTestUser.id,
                                AdditionalInformation__c='TestLCR',
                                Department__c ='TestLCR',
                                BusinessRiskEscalationEntity__c =lCROrganzObj.id,
                                Role__c ='Complaint/Request Owner'
                                
                                ) ;
        insert LCROrgStakeHold; 
         EntityStakeholder__c  LCROrgStakeHold1= new EntityStakeholder__c(
                                User__c=LCRTestUser.id,
                                AdditionalInformation__c='TestLCR',
                                Department__c ='TestLCR',
                                BusinessRiskEscalationEntity__c =lCROrganzObj1.id,
                                Role__c ='CS&Q Manager'
                                
                                ) ;
        insert LCROrgStakeHold1; 
       //*
         
        //*/
        ComplaintRequest__c  CompRequestObj = new ComplaintRequest__c(
                Status__c='Solution found, execution pending', 
                Case__c  =CaseObj.id,
                Category__c ='4 - Post-Sales Tech Support',
                reason__c='Marketing Activity Response',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj.id,
                TECH_CROrderBusiness__c = 'TECH_CROrderBusiness__c',
                TECH_CROrderProductLine__c ='TECH_CROrderProductLine__c',
                TECH_CROrderProductFamily__c='TECH_CROrderProductFamily__c',
                FamilyOrdered__c ='FamilyOrdered__c',
                CommercialReferenceOrdered__c='CommercialReferenceOrdered__c'                
                );
                
        Insert CompRequestObj;  
       
        //String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        OPP_Product__c pdct = new OPP_Product__c(
                            Name='Name',
                            TECH_PM0CodeInGMR__c='GMR business__c',
                            IsActive__c =true
                            );
        insert pdct;
        
       // GMRSearchPage.Mode='ordered';
        Test.startTest();
        //global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
       List<Object> rs8 = VFC_AddLEOrderedProduct.remoteSearch('searchString','gmrcode1',true,true,true,true,true);
       List<Object> rs6 = VFC_AddLEOrderedProduct.remoteSearch('searchString','gmrcod',true,true,true,true,true);
       List<Object> rs4 = VFC_AddLEOrderedProduct.remoteSearch('searchString','gmrc',true,true,true,true,true);
       List<Object> rs2 = VFC_AddLEOrderedProduct.remoteSearch('searchString','gm',true,true,true,true,true);
       
       // global static List<OPP_Product__c> getOthers(String business) {
         List<OPP_Product__c> opp = VFC_AddLEOrderedProduct.getOthers('business');
         
         PageReference pageRef = Page.VFP_AddLEOrderedProduct;
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);
         pageRef.getParameters().put('Mode','ordered');
        // system.debug('Mode---->'+Mode);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(CompRequestObj);  
         VFC_AddLEOrderedProduct GMRSearchPage = new VFC_AddLEOrderedProduct(CaseStandardController );
         
        GMRSearchPage.jsonSelectString=jsonSelectString;
        GMRSearchPage.ChangeProduct = true;
        GMRSearchPage.performSelect();
        GMRSearchPage.performSelectFamily();
        GMRSearchPage.pageCancelFunction();
        GMRSearchPage.ChangeProduct = false;
        GMRSearchPage.performSelect();
        GMRSearchPage.performSelectFamily();
        GMRSearchPage.pageCancelFunction();
              Test.stopTest();
      }
      public static testmethod void Test_AddLEOrderedProduct1() {
  //  Test.startTest();
      // Create Country
      /*  Country__c CountryObjLCR = Utils_TestMethods.createCountry();
       
        Insert CountryObjLCR;
        Role__c rolse= new Role__c();
        rolse.Role__c='CEO';
        insert rolse;*/
        country__c CountryObjLCR=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            CountryObjLCR=countries[0];
        else{               
            CountryObjLCR=new country__c(Name='TestCountry',CountryCode__c='IN');
            insert CountryObjLCR;    
        }
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCROwner');
        LCRTestUser.ManagerEmail__c='sukumar.salla@schneider-electric.com';
        LCRTestUser.Manager1Email__c='shivanand.rao@schneider-electric.com';
        LCRTestUser.Email='ram.chilukuri@schneider-electric.com';
      //  LCRTestUser.UserRole=rolse.id;
        Insert LCRTestUser ;
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
      //  LCRTestUserCq.UserRole=rolse.id;
      LCRTestUserCq.ManagerEmail__c='sukumar.salla@schneider-electric.com';
        LCRTestUserCq.Manager1Email__c='shivanand.rao@schneider-electric.com';
        LCRTestUserCq.Email='ram.chilukuri@schneider-electric.com';
        Insert LCRTestUserCq ;
        
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
        insert ContactObj;
        
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
            CaseObj.CommercialReference__c= 'Test Commercial Reference';
            CaseObj.ProductBU__c = 'Business';
            CaseObj.ProductLine__c = 'ProductLine';
            CaseObj.ProductFamily__c ='ProductFamily';
            CaseObj.Family__c ='Family';
            CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
            CaseObj.Symptom__c =  'Installation/ Setup';
            CaseObj.SubSymptom__c = 'Hardware';    
            CaseObj.Quantity__c = 4;
            CaseObj.Family__c = 'ADVANCED PANEL';
            CaseObj.CommercialReference__c='xbtg5230';
            CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
            CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
            CaseObj.LevelOfExpertise__c ='Advanced';
        Insert CaseObj;
      
         BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='SCH@schneider.com'
                                            );
        Insert lCROrganzObj; 

        ComplaintRequest__c  CompRequestObj = new ComplaintRequest__c(
                Status__c='Solution found, execution pending', 
                Case__c  =CaseObj.id,
                Category__c ='4 - Post-Sales Tech Support',
                reason__c='Marketing Activity Response',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj.id,
                TECH_CRReceivedBusiness__c= 'TECH_CRReceivedBusiness__c',
                TECH_CRReceivedProductLine__c='TECH_CRReceivedProductLine__c',
                TECH_CRReceivedProductFamily__c='TECH_CRReceivedProductFamily__c',
                FamilyReceived__c='FamilyReceived__c',
                CommercialReferenceReceived__c='CommercialReferenceReceived__c'      
                 );
                
        Insert CompRequestObj;  
        
       Test.startTest();
         PageReference pageRef = Page.VFP_AddLEOrderedProduct;
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);
         pageRef.getParameters().put('Mode','received');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(CompRequestObj);  
         VFC_AddLEOrderedProduct GMRSearchPage = new VFC_AddLEOrderedProduct(CaseStandardController );
        
              Test.stopTest();
      }
    

    
}