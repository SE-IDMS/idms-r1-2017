/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
global class FieloPRM_Batch_MemberPropProcess implements Database.Batchable<sObject>, Schedulable {

    private static final CS_PRM_ApexJobSettings__c config= CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_MemberPropProcess');
  
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Set<string> setFieldsMemberProp =  Schema.SObjectType.MemberExternalProperties__c.fields.getMap().keySet();
        List<string> listFieldsMemberProp = new list<string>();
        listFieldsMemberProp.addAll(setFieldsMemberProp);
        String query = 'SELECT ' + String.join(listFieldsMemberProp, ',') + ' FROM MemberExternalProperties__c WHERE F_PRM_Status__c IN (\'TOPROCESS\', \'TODELETE\') ORDER BY CreatedDate';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(isRunning(BC.getJobId())) return;
        process(scope);
    }
  
    global void finish(Database.BatchableContext bc) {
        //schedule(bc.getJobId());
    }
  
    global void execute(SchedulableContext sc){
        Database.executeBatch(new FieloPRM_Batch_MemberPropProcess(), Integer.valueOf(config.F_PRM_BatchSize__c));
    }

    global static void runSync(List<MemberExternalProperties__c> meps){
        Boolean isGuest = UserInfo.getUserType() == 'Guest';
        Id memId;
        try{
            memId = getMemberId();
        }catch(Exception e){}
        List<MemberExternalProperties__c> thisUserMeps = new List<MemberExternalProperties__c>();
        List<MemberExternalProperties__c> syncMeps = new List<MemberExternalProperties__c>();
        for(MemberExternalProperties__c mep: meps){
            if(mep.F_PRM_Status__c != null && (mep.F_PRM_Status__c.equals('TOPROCESS') || mep.F_PRM_Status__c.equals('TODELETE'))){
                if(Trigger.isExecuting && Trigger.isUpdate){
                    Id mepId = mep.Id;
                    for(Id thisId: Trigger.oldMap.keySet()){
                        if(thisId == mepId){
                            MemberExternalProperties__c oldOne = (MemberExternalProperties__c)Trigger.oldMap.get(mepId);
                            MemberExternalProperties__c newOne = (MemberExternalProperties__c)Trigger.newMap.get(mepId);
                            if(oldOne.F_PRM_Status__c != newOne.F_PRM_Status__c || oldOne.F_PRM_IsSynchronic__c != newOne.F_PRM_IsSynchronic__c){
                                if(mep.F_PRM_IsSynchronic__c){
                                    syncMeps.add(mep);
                                }else if(isGuest || (memId != null && mep.Member__c == memId)){
                                    thisUserMeps.add(mep);
                                }
                            }
                        }
                    }
                }else{
                    if(mep.F_PRM_IsSynchronic__c){
                        syncMeps.add(mep);
                    }else if(isGuest || (memId != null && mep.Member__c == memId)){
                        thisUserMeps.add(mep);
                    }
                }
            }
        }
        if(syncMeps.size() > 0){
            process(syncMeps);
        }else if(thisUserMeps.size() > 0 && thisUserMeps.size() <= config.F_PRM_SyncAmount__c){
            process(thisUserMeps);
        }
    }

    global static void process(List<MemberExternalProperties__c> meps){
        List<MemberExternalProperties__c> toUpdate = new List<MemberExternalProperties__c>();
        List<MemberExternalProperties__c> toDelete = new List<MemberExternalProperties__c>();
        for(MemberExternalProperties__c mep: meps){
            if(mep.F_PRM_Status__c.equals('TOPROCESS')){
                toUpdate.add(mep);
            }else{
                toDelete.add(mep);
            }
        }
        if(toUpdate.size() > 0){
            FieloPRM_AP_MemberPropertiesTriggers.createBadgeMember(toUpdate);
        }
        if(toDelete.size() > 0){
            FieloPRM_AP_MemberPropertiesTriggers.deleteBadgeMember(toDelete);
        }
    }
    
    global static void schedule(Id jobId){
        List<MemberExternalProperties__c> toProcess = [SELECT Id FROM MemberExternalProperties__c WHERE F_PRM_Status__c IN ('TOPROCESS', 'TODELETE') LIMIT 1];
        if (!isRunning(jobId) && toProcess.size() > 0) {
            scheduleNow();
        }
    }

    global static Boolean isRunning(Id jobId){
        AsyncApexJob[] jobs = [SELECT Id 
                               FROM AsyncApexJob 
                               WHERE Status = 'Processing'
                               AND ApexClass.Name = 'FieloPRM_Batch_MemberPropProcess'
                               AND JobType = 'BatchApex'
                               AND Id != :jobId];
        return jobs.size() > 0;
    }
    
    public static void scheduleNow(){
        DateTime nowTime = datetime.now().addSeconds(65);
        String Seconds      = '0';
        String Minutes      = nowTime.minute() < 10 ? '0' + nowTime.minute() : String.valueOf(nowTime.minute());
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        System.schedule('FieloPRM_Batch_MemberPropProcess '+system.now() + ':' + system.now().millisecond(), CronExpression, new FieloPRM_Batch_MemberPropProcess());
    }  

    public static Id getMemberId(){
        Id memId;
        try{
            List<FieloEE__Member__c> members = [SELECT Id, FieloEE__Program__c FROM FieloEE__Member__c WHERE FieloEE__User__c =: UserInfo.getUserId()];
            if(members.size() == 1){
                memId = members[0].Id;
            }else{
                Id programId = FieloEE.ProgramUtil.getProgramByDomain().Id;
                for(FieloEE__Member__c m : members){
                    if(m.FieloEE__Program__c == programId){
                        memId = m.Id;
                        break;
                    }
                }
            }
        }catch(Exception e){
            return null;
        }
        return memId;
    }
}