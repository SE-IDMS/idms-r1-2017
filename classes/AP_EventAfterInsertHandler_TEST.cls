/*
    Author          : Pooja Suresh
    Description     : Test Class for AP_EventAfterInsertHandler.
    Release         : April 2014
*/
@isTest
private class AP_EventAfterInsertHandler_TEST
{
    static testMethod void testEvents() 
    {
        Country__c country= Utils_TestMethods.createCountry();
        insert country;            
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
    
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
        insert opp;
        
        Opportunity opp1 = Utils_TestMethods.createOpenOpportunity(acc.id);
        insert opp1;
        
        System.debug('------ First Activity Event After Insert Starts ------');
        Event opptyEvent= Utils_TestMethods.createEvent(opp.Id,null,'Not Started');
        Database.insert(opptyEvent, false);
        
        List<Opportunity> opptyInsert= [SELECT Tech_FirstActivity__c FROM Opportunity WHERE Id =:opp.id limit 1];
        System.debug('-------- oppty '+opptyInsert);
        System.assertEquals(opptyInsert[0].Tech_FirstActivity__c, Date.Today());
        
        System.debug('------ First Activity Event After Update Starts ------');
        opptyEvent.WhatId=opp1.Id;
        Database.update(opptyEvent, false);
        
        List<Opportunity> opptyUpdate= [SELECT Tech_FirstActivity__c FROM Opportunity WHERE Id =:opp.id limit 1];
        System.debug('-------- oppty '+opptyUpdate);
        System.assertEquals(opptyUpdate[0].Tech_FirstActivity__c, Date.Today());
    }

}