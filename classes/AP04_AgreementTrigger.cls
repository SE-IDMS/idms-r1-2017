/*
    Description: This class is used for defining the Business Logics for various functionalities like 
    Granting access to the Agreement Manager, Validating the Status Updation for the Agreement, Copy the 
    Product Line from Agreement to the Opportunity Product Line etc. All these operations are evoked through 
    triggers on the Agreement Object. Triggers invokes individual methods conditionally to do the tasks.    
    
    Author:Pooja Bhute
    Date: 01/02/2011.    
*/

public class AP04_AgreementTrigger {
    
    private static List<Opportunity> opportunities = new List<Opportunity>();   
    private static OPP_ProductLine__c newProductLine = new OPP_ProductLine__c();
    public static Boolean WS02TestClassFlag = True;  
    
    /* This method is called by the trigger 'OppAgreementGrantAgreementManagerAccess'.
       It will set the permission for the Agreement Manager as R/W for the Agreement Object.   
    */
              
    public static void grantAccess(OppAgreement__c[] agreements){ 
        System.Debug('****AP04_AgreementTrigger GrantAccess Started****');
        List<OppAgreement__Share> agreementShares = new List<OppAgreement__Share>();    
        Set<String> agreementSet = new Set<String>();
        
        if(WS02TestClassFlag == True){     
            for(OppAgreement__c agreement: agreements)
            {
                if(agreement.AgreementManager__c !=null)
                {
                    OppAgreement__Share AgreementManagerShare = new OppAgreement__Share();
                    AgreementManagerShare.ParentId = agreement.Id;
                    AgreementManagerShare.UserOrGroupId = agreement.AgreementManager__c;
                    AgreementManagerShare.AccessLevel = 'Edit';
                    AgreementManagerShare.RowCause = Schema.OppAgreement__Share.RowCause.Agreement_Manager_Access__c;
                    agreementShares.add(AgreementManagerShare);  
                    //agreementSet.add(AgreementManagerShare.Id);      
                   
                }
            }
            if(agreementShares.Size() > 0){
                if(agreementShares.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                    System.Debug('######## AP04 Error updating : '+ Label.CL00264);
                }
                else{
                    Database.SaveResult[] oppAgreementShareInsertResult = Database.insert(agreementShares, true);
                    
                    for(Database.SaveResult sr: oppAgreementShareInsertResult){
                        if(!sr.isSuccess()){
                            Database.Error err = sr.getErrors()[0];
                            System.Debug('######## AP04 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                        }    
                    }
                }
            }
        }        
        System.Debug('****AP04_AgreementTrigger GrantAccess End****');
    }
    
    /* This method is called by the trigger 'OppAgreementAfterUpdate'.
       It will revoke the access for the previous Agreement Manger and
       gives access to the new Agreement Manager as R/W for the Agreement Object.   
    */
    
    public static void grantAccessUpdate(OppAgreement__c[] newAgreements, OppAgreement__c[] oldAgreements){ 
    
        System.Debug('****AP04_AgreementTrigger GrantAccessUpdate Started****');
        List<OppAgreement__Share> newAgreementShares = new List<OppAgreement__Share>(); 
        List<String> oldAgreementManagers = new List<String>();   
        List<String> oldAgreementIds = new List<String>();
        Set<String> newAgreementSet = new Set<String>();
        Set<String> oldAgreementManagerSet = new Set<String>();
        Set<String> oldAgreementIdSet = new Set<String>();
        
        if(WS02TestClassFlag == True){  
            for(OppAgreement__c oldAgreement: oldAgreements)
            {
                if(!oldAgreementManagerSet.contains(oldAgreement.AgreementManager__c)){
                    oldAgreementManagers.add(oldAgreement.AgreementManager__c);
                    oldAgreementManagerSet.add(oldAgreement.AgreementManager__c);
                }
                
                if(!oldAgreementIdSet.contains(oldAgreement.Id)){
                    oldAgreementIds.add(oldAgreement.Id);
                    oldAgreementIdSet.add(oldAgreement.Id);
                }
            }
            
            if(oldAgreementManagers.Size() > 0){
                OppAgreement__Share[] agreementSharesToBeDeleted = [Select Id from OppAgreement__Share where UserOrGroupId in: oldAgreementManagers and ParentId in: oldAgreementIds];
                
                if(agreementSharesToBeDeleted.Size() > 0){
                    if(agreementSharesToBeDeleted.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                        System.Debug('######## AP04 Error Deleting: '+ Label.CL00264);
                    }
                    else{
                        Database.DeleteResult[] oldAgreementShareDeleteResult = Database.delete(agreementSharesToBeDeleted, true);
                        
                        for(Database.DeleteResult dr: oldAgreementShareDeleteResult){
                            if(!dr.isSuccess()){
                                Database.Error err = dr.getErrors()[0];
                                System.Debug('######## AP04 Error Deleting: '+err.getStatusCode()+' '+err.getMessage());
                            }    
                        }
                    }
                }
            }   
        
            for(OppAgreement__c newAgreement: newAgreements)
            {
                OppAgreement__Share newAgreementManagerShare = new OppAgreement__Share();
                newAgreementManagerShare.ParentId = newAgreement.Id;
                newAgreementManagerShare.UserOrGroupId = newAgreement.AgreementManager__c;
                newAgreementManagerShare.AccessLevel = 'Edit';
                newAgreementManagerShare.RowCause = Schema.OppAgreement__Share.RowCause.Agreement_Manager_Access__c;
                              
                newAgreementShares.add(newAgreementManagerShare);  
                      
                
            }
            if(newAgreementShares.Size() > 0){
                if(newAgreementShares.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                    System.Debug('######## AP04 Error Inserting : '+ Label.CL00264);
                }
                else{
                    Database.SaveResult[] oppAgreementShareInsertResult = Database.insert(newAgreementShares, true);
                    
                    for(Database.SaveResult sr: oppAgreementShareInsertResult){
                        if(!sr.isSuccess()){
                            Database.Error err = sr.getErrors()[0];
                            System.Debug('######## AP04 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                        }    
                    }
                      
                }
            }
        }        
        System.Debug('****AP04_AgreementTrigger GrantAccessUpdate End****');
    }     
     
    
    /*
        This method would validate the process of updation in the Stages in Agreement where
        the stages for the Agreement cannot be set as Deliver & Validate, if it has no Opportunity
        is associated before setting the Stages. Alert message displayed to the user once the condition
        is validated as true.
    */
    
    
    public static void ValidateAgrrementStatusUpdation(OppAgreement__c[] agreements)
    {  
        System.Debug('****AP04_AgreementTrigger ValidateAgrrementStatusUpdation Started****');    
        
        List<String> agreementIds = new List<String>();
        Boolean noError = False;
        List<Opportunity> opportunityDetails = new List<Opportunity>();
        
        for(OppAgreement__c agreement : agreements) {
            agreementIds.add(agreement.Id);
        }
        System.Debug('****Agreemant Ids: '+agreementIds);
        
        if(agreementIds.Size() > 0){
            opportunityDetails = [Select Id, AgreementReference__c from Opportunity where AgreementReference__c =: agreementIds];
        }
        
        if(opportunityDetails.Size() > 0) {
            for(OppAgreement__c agreement : agreements) {
                for(Opportunity oppDet: opportunityDetails) {
                    if(agreement.Id == oppDet.AgreementReference__c) {
                        noError = True;
                    }
                }
                if(noError == False && agreement.PhaseSalesStage__c == Label.CL00220) {
                    agreement.addError(Label.CL00244);
                }
                noError = False;
            }
        }
        else{
            for(OppAgreement__c agreement : agreements) {               
                if(agreement.PhaseSalesStage__c == Label.CL00220) {                    
                    agreement.addError(Label.CL00244);
                }
            }
        }     
        System.Debug('****AP04_AgreementTrigger ValidateAgrrementStatusUpdation End****');    
    }
    
    public static void populateAccountOwnerEmail(List<OppAgreement__c> oppAgrs)
    {
        System.Debug('****AP04_AgreementTrigger populateAccountOwnerEmail Starts****');    
        List<Id> accountOwnerIds = new List<Id>();
        Map<Id, String> userIdEmail = new Map<Id, String>();
        List<User> users = new List<User>();
        for(OppAgreement__c oppAgr: oppAgrs)
        {
            if(oppAgr.TECH_AccountOwnerId__c!=null)
            accountOwnerIds.add(oppAgr.TECH_AccountOwnerId__c);
        }
        users  = [select Email from User where Id in:accountOwnerIds];
        for(User usr: users)
        {
            userIdEmail.put(usr.Id, usr.email);
        }
        for(OppAgreement__c oppAgr: oppAgrs)
        {
            if(userIdEmail.containskey(oppAgr.TECH_AccountOwnerId__c))
            oppAgr.TECH_AccountOwnerEmail__c = userIdEmail.get(oppAgr.TECH_AccountOwnerId__c);                      
        }
        System.Debug('****AP04_AgreementTrigger populateAccountOwnerEmail Ends****');    
    }
    /*
        Method to validate only one Product Line for Agreement is Leading Product Line for Agreement.
    */
    /*
    public static void validateNumberofLeadindPLAs(List<ProductLineForAgreement__c> productLineAgreements){
        
        System.Debug('****AP04_AgreementTrigger validateNumberofLeadindPLAs Starts****');
        List<String> agreementIds = new List<String>();
        Map<String, String> updatedStatus = new Map<String, String>();        
        Set<String> uniqueAgreementSet = new Set<String>();        
        Boolean noUpdate = False;
                
        for(ProductLineForAgreement__c productLines: productLineAgreements){
            if(!uniqueAgreementSet.contains(productLines.Agreement__c)){
                agreementIds.add(productLines.Agreement__c);
                uniqueAgreementSet.add(productLines.Agreement__c);
            }      
        }        
          
        ProductLineForAgreement__c[] productLineAgreementDetails = [Select Id, Agreement__c, LeadingProductLine__c from ProductLineForAgreement__c where Agreement__c in: agreementIds];           
            
        for(ProductLineForAgreement__c prodLineAgreements: productLineAgreements){
            for(ProductLineForAgreement__c productDetails: productLineAgreementDetails){ 
                if(prodLineAgreements.Agreement__c == productDetails.Agreement__c){
                    noUpdate = True;
                }
            }
                       
            if(noUpdate == False && updatedStatus.get(prodLineAgreements.Agreement__c) != 'Done'){
                prodLineAgreements.LeadingProductLine__c = True;
                updatedStatus.put(prodLineAgreements.Agreement__c, 'Done');
            }
            else if(noUpdate == False && updatedStatus.get(prodLineAgreements.Agreement__c) == 'Done' && prodLineAgreements.LeadingProductLine__c == True){
                prodLineAgreements.addError(Label.CL00276);
            }
            else if(productLineAgreementDetails.Size() > 0 && noUpdate == True && prodLineAgreements.LeadingProductLine__c == True){
                prodLineAgreements.addError(Label.CL00276);
            }
            noUpdate = False;
        }
               
        
        System.Debug('****AP04_AgreementTrigger validateNumberofLeadindPLAs Ends****');
    }
    */
    /*
        Method to validate only one Product Line for Agreement is Leading Product Line for Agreement when Product Line for Agreement is Updated.
    */
    /*
    public static void validateNumberofUpdatedLeadindPLAs(List<ProductLineForAgreement__c> productLineAgreements){
        
        System.Debug('****AP04_AgreementTrigger validateNumberofUpdatedLeadindPLAs Starts****');
        List<String> agreementIds = new List<String>();              
        Set<String> uniqueAgreementSet = new Set<String>();        
                
        for(ProductLineForAgreement__c productLines: productLineAgreements){
            if(productLines.LeadingProductLine__c == True){
                if(!uniqueAgreementSet.contains(productLines.Agreement__c)){
                    agreementIds.add(productLines.Agreement__c);
                    uniqueAgreementSet.add(productLines.Agreement__c);
                }
            }               
        }   
        
        ProductLineForAgreement__c[] productLineAgreementDetails = [Select Id, Agreement__c, LeadingProductLine__c from ProductLineForAgreement__c where Agreement__c in: agreementIds and LeadingProductLine__c =: True];
        
        for(ProductLineForAgreement__c prodLineAgreements: productLineAgreements){
            for(ProductLineForAgreement__c productDetails: productLineAgreementDetails){ 
                if(prodLineAgreements.Agreement__c == productDetails.Agreement__c){
                    prodLineAgreements.LeadingProductLine__c.addError(Label.CL00276);
                }
            }
        }
        System.Debug('****AP04_AgreementTrigger validateNumberofUpdatedLeadindPLAs Ends****');
    }
     */
     /*   Method to Add Product Line And Product Family Translated Value to Tech Fields For PLA
     */
      /*public static void updateProductLineAndProductFamily(List<ProductLineForAgreement__c> plaList)
      {
       
       System.Debug('****AP04_AgreementTrigger UpdateProductLineAndProductFamily Starts****');
       List<String> productids = new List<String>();
       List<String> productlineids = new List<String>();
       List<ProductLineForAgreement__c> plineTranslate = new List<ProductLineForAgreement__c>(); 
       if(plaList.size() > 0)
       {
           for(ProductLineForAgreement__c pl : plaList)
           {
               productids.add(pl.SystemIdReference__c);
               productlineids.add(pl.Id);
           }
           
           plineTranslate = [Select p.Name, p.SystemIDReference__c,ToLabel(p.SystemIDReference__r.ProductFamily__c), 
           ToLabel(p.SystemIDReference__r.ProductLine__c) from ProductLineForAgreement__c p WHERE p.SystemIDReference__c IN : productids AND ID IN: productlineids];
           List<ProductLineForAgreement__c> updateProductLineProductFamily = new List<ProductLineForAgreement__c>();
           if(plineTranslate.size() > 0)
           {
                   
                   for(ProductLineForAgreement__c opl : plineTranslate)
                   {
                       opl.ProductLine__c = opl.SystemIDReference__r.ProductLine__c;
                       opl.ProductFamily__c = opl.SystemIDReference__r.ProductFamily__c;
                       updateProductLineProductFamily .add(opl);
                   }
                   
                   if(updateProductLineProductFamily .Size() > 0)
                   {
                        if(updateProductLineProductFamily.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                        {
                            System.Debug('######## AP04 Error Inserting : '+ Label.CL00264);
                        }
                        else
                        {
                            Database.SaveResult[] productLineProductFamilyUpdateResult = Database.update(UpdateProductLineProductFamily, true);
                            for(Database.SaveResult sr: productLineProductFamilyUpdateResult )
                            {
                                if(!sr.isSuccess()){
                                    Database.Error err = sr.getErrors()[0];
                                    System.Debug('######## AP04 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                                }    
                            }
                        }                         
                          
                        
                   }              
                       
               
            
           
        System.Debug('****AP04_AgreementTrigger UpdateProductLineAndProductFamily Ends ****');
        }
        
      
    }
  }  
  */
  
   
    /*December Release - Method to update all the opportunity attached to the OEM agreement on the change of phase/sales stage*/
    
    public static void updateOEMAgreementOpp(Map<Id,oppAgreement__c> agreementOppForUpdate){
        
        List<Opportunity> opportunityToUpdate = new List<Opportunity>();
        List<OPP_OpportunityCompetitor__c> OppcompetitorToUpsert = new List<OPP_OpportunityCompetitor__c>();
        //List<OPP_OpportunityCompetitor__c> OppcompetitorToUpdate = new List<OPP_OpportunityCompetitor__c>();
        Map<Id,Id> competitorToInsertMap = new Map<Id,Id>();
        Map<Id,Competitor__c> competitorMap;
        OPP_OpportunityCompetitor__c oppComp;
        Map<Integer, Id> updOpps = new Map<Integer, Id>();
        Integer noOfOpps = 1;
        Map<Id, Id> agrOppIds = new Map<Id, Id>();
        Map<Id, string> errorMsg = new Map<Id, String>();
        Map<Id,Opportunity> opportunityOfOEMAgr = new Map<Id,Opportunity>([select id, StageName, AgreementReference__c, SolutionCtr__c,
                                                                             RFQReceivedOn__c, CustomerSelected__c, Reason__c, OpptyType__c
                                                                             from opportunity 
                                                                             where AgreementReference__c=:agreementOppForUpdate.keySet()]);
        // Modefied by Hari Krishna 
        // Business Requirement: BR-2449 
        // Release : Dec 2012
        // <<-- Started -->>
        Set<Id> wcIdSet = new Set<id>();
           for(oppAgreement__c opAg:agreementOppForUpdate.values()){
            if(opAg.WinningCompetitor__c!=null)
            wcIdSet.add(opAg.WinningCompetitor__c);
        }
                                                                     
        
        Map<Id,OPP_OpportunityCompetitor__c> oppCompetitorMap = new Map<Id,OPP_OpportunityCompetitor__c>([select id,Winner__c,OpportunityName__c,TECH_CompetitorName__c from OPP_OpportunityCompetitor__c where OpportunityName__c in :opportunityOfOEMAgr.keySet() ]);
        // or TECH_CompetitorName__c in :wcIdSet]);
                                                                                                        
                                                                                                      

        competitorMap = new Map<Id,Competitor__c>([select id,Name,MultiBusiness__c,ParentCompany__c 
                                                        from Competitor__c
                                                        where id in :wcIdSet]);
        // <<-- Ended-->> 
        if(opportunityOfOEMAgr.size() > 0){
            for(Opportunity opp: opportunityOfOEMAgr.values())
            {
                //MODIFIED FOR BR-6716
                if((agreementOppForUpdate.get(opp.AgreementReference__c).PhaseSalesStage__c != Label.CL00272 && opp.StageName!=Label.CL00272) || (agreementOppForUpdate.get(opp.AgreementReference__c).PhaseSalesStage__c == Label.CL00272))
                {
                if(opp.OpptyType__c==agreementOppForUpdate.get(opp.AgreementReference__c).Opportunity_Type__c)
                {
                    opp.StageName=agreementOppForUpdate.get(opp.AgreementReference__c).PhaseSalesStage__c;
                    opp.SolutionCtr__c=agreementOppForUpdate.get(opp.AgreementReference__c).SolutionCenter__c;
                    opp.RFQReceivedOn__c=agreementOppForUpdate.get(opp.AgreementReference__c).RFPReceivedOn__c;
                    opp.CustomerSelected__c=agreementOppForUpdate.get(opp.AgreementReference__c).CustomerSelected__c;
                    opp.Reason__c=agreementOppForUpdate.get(opp.AgreementReference__c).Reason__c;
                    opp.Status__c = agreementOppForUpdate.get(opp.AgreementReference__c).Status__c;
                    opportunityToUpdate.add(opp);
                    updOpps.put(noOfOpps, opp.Id); 
                    agrOppIds.put(opp.Id, agreementOppForUpdate.get(opp.AgreementReference__c).Id); 
                    noOfOpps = noOfOpps+1;  
                    if(agreementOppForUpdate.get(opportunityOfOEMAgr.get(opp.Id).AgreementReference__c).WinningCompetitor__c!=NULL){
                        if(competitorMap.values().size()> 0 && (competitorMap.containsKey(agreementOppForUpdate.get(opportunityOfOEMAgr.get(opp.Id).AgreementReference__c).WinningCompetitor__c))){
                            System.debug('No Opp COMP'+competitorMap.values());
                            oppComp = new OPP_OpportunityCompetitor__c();
                            competitorToInsertMap.put(opp.Id,agreementOppForUpdate.get(opp.AgreementReference__c).WinningCompetitor__c);
                            oppComp.OpportunityName__c=opp.Id;
                            oppComp.TECH_CompetitorName__c=competitorToInsertMap.get(opp.Id);
                            oppComp.Winner__c=true;
                            oppComp.OtherName__c = agreementOppForUpdate.get(opp.AgreementReference__c).OtherName__c;
                            OppcompetitorToUpsert.add(oppComp);
                            System.debug('Opp Comp Insert '+OppcompetitorToUpsert);
                        }
                    }
                    if(oppCompetitorMap.values().size()>0){
                        for(OPP_OpportunityCompetitor__c oppCompt: oppCompetitorMap.values()){
                            if(oppCompt.TECH_CompetitorName__c==agreementOppForUpdate.get(opportunityOfOEMAgr.get(opp.Id).AgreementReference__c).WinningCompetitor__c
                                && oppCompt.OpportunityName__c==opp.Id){
                                oppCompt.Winner__c=true;
                                oppComp.OtherName__c = agreementOppForUpdate.get(opp.AgreementReference__c).OtherName__c;
                                integer count=+1;
                                System.debug('Count : '+count);
                                OppcompetitorToUpsert.add(oppCompt);
                            }
                            else if(oppCompt.OpportunityName__c==opp.Id){
                                oppCompt.Winner__c=false;
                                OppcompetitorToUpsert.add(oppCompt);
                            }
                        }
                        System.debug('Opp Compt To Update : '+OppcompetitorToUpsert);   
                    }
                }
                else{
                    agreementOppForUpdate.get(opp.AgreementReference__c).addError(Label.CL00711);
                }
                
                }
            }
        }

        if(OppcompetitorToUpsert.size()>0){
            if(OppcompetitorToUpsert.size()+Limits.getDMLRows() > Limits.getLimitDMLRows()){
                System.Debug('######## AP04 Error Updating : '+ Label.CL00264);
            }
            else{
                Database.upsertResult[] competitorInsert=Database.upsert(OppcompetitorToUpsert,false);
                for(Database.upsertResult comp: competitorInsert){
                     if(!comp.isSuccess())
                         System.Debug('######## AP04 Error Inserting : '+comp.getErrors()[0].getStatusCode()+' '+comp.getErrors()[0].getMessage());                    
                }
            }
        }
        if(opportunityToUpdate.size()>0){
            if(opportunityToUpdate.size()+Limits.getDMLRows() > Limits.getLimitDMLRows()){
                System.Debug('######## AP04 Error Updating : '+ Label.CL00264);
            }
            else{
                Database.SaveResult[] OpportunityUpdate=database.update(opportunityToUpdate,false);
                 Integer recs = 1;
                for(Database.SaveResult oppty: OpportunityUpdate){
                     if(!oppty.isSuccess())
                     {
                         errorMsg.put(updOpps.get(recs), oppty.getErrors()[0].getMessage().unescapeHtml3());
                         System.Debug('######## AP04 Error Updating : '+oppty.getErrors()[0].getStatusCode()+' '+oppty.getErrors()[0].getMessage());                    
                     }
                      recs = recs+1;
                }
            }
        }
        

        for(Id oppsId: errorMsg.keyset())
        {
             agreementOppForUpdate.get(agrOppIds.get((oppsId))).addError(errorMsg.get(oppsId));           
        }
        
    }
}