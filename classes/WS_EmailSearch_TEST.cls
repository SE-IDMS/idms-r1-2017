@isTest
private class WS_EmailSearch_TEST{
    static testMethod void testWS_EmailSearchMatchContacts(){
        String salesforceURL = URL.getSalesforceBaseUrl().toExternalForm();
        list<Account> newAccounts = new list<Account>();
        list<Contact> newContacts = new list<Contact>();
        list<ANI__c> newANIs = new list<ANI__c>();
        
        //Create Country
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        //Create StateProvince
        StateProvince__c stateProvince = Utils_TestMethods.createStateProvince(country.id);
        insert stateProvince;
        
        //Create Preferred Agent
        User agent1 = Utils_TestMethods.createStandardUser('Agent1');
        insert agent1;
        
        //Create Preferred Backup Agent
        User agent2 = Utils_TestMethods.createStandardUser('Agent2');
        insert agent2;
        
        //Create DAC Account
        Account account1 = Utils_TestMethods.createAccount();
        account1.Name = 'DAC Account 1';
        account1.Phone = '99999991';
        //account1.Email = 'dac.account1@schneider-electric.com';
        account1.Country__c = country.id;
        account1.StateProvince__c = stateProvince.id;
        account1.LeadingBusiness__c = 'Buildings';
        account1.ClassLevel1__c = 'Retailer';
        account1.Type = 'Prospect';
        account1.ServiceContractType__c = 'DAC';
        account1.City__c = 'bangalore';
        account1.ZipCode__c = '560066';
        account1.Street__c = 'EPIP';
        account1.POBox__c = '12345';
        account1.Pagent__c = agent1.Id;
        account1.PBAAgent__c = agent2.Id;
        
        newAccounts.add(account1); 
        System.debug('####### - DAC Account 1 - ' + account1 + '#######');
        
        
        //Create VIC Account 1
        Account account2 = Utils_TestMethods.createAccount();
        account2.Name = 'VIC Account 1';
        account2.Phone = '99999992';
        //account1.Email = 'vic.account1@schneider-electric.com';
        account2.Country__c = country.id;
        account2.StateProvince__c = stateProvince.id;
        account2.LeadingBusiness__c = 'Buildings';
        account2.ClassLevel1__c = 'Retailer';
        account2.Type = 'Prospect';
        account2.ServiceContractType__c = 'VIC';
        account2.City__c = 'bangalore';
        account2.ZipCode__c = '560066';
        account2.Street__c = 'EPIP';
        account2.POBox__c = '12345';
        account2.Pagent__c = agent1.Id;
        account2.PBAAgent__c = agent2.Id;
        newAccounts.add(account2); 
        System.debug('####### - VIC Account 1 - ' + account2 + '#######');
        
        
        //Create VIC Contact 1 - No. 99999991
        Contact contact1 = Utils_TestMethods.createContact(account2.id, 'VIC Contact 1' );
        contact1.firstname = 'VIC';
        contact1.LastName = 'Contact';
        contact1.WorkPhone__c = '99999991';
        contact1.Email = 'dac.account1@schneider-electric.com';
        contact1.accountid = account1.id;
        contact1.ServiceContractType__c = 'VIC';
        contact1.Pagent__c = agent1.Id;
        contact1.PBAAgent__c = agent2.Id;
        newContacts.add(contact1);
        System.debug('####### - VIC Contact 1 -' + contact1 + '#######');
        
        //Create VIC Contact 2 - No. 99999991
        Contact contact2 = Utils_TestMethods.createContact(account2.id, 'VIC Contact 2' );
        contact2.firstname = 'VIC';
        contact2.LastName = 'Contact';
        contact2.WorkPhone__c = '99999991';
        contact2.Email = 'dac.account1@schneider-electric.com';
        contact2.accountid = account2.id;
        contact2.Pagent__c = agent1.Id;
        contact2.PBAAgent__c = agent2.Id;
        newContacts.add(contact2);
        System.debug('####### - VIC Contact 2 -' + contact2 + '#######');
        
        //Create DAC Contact 1 - No. 99999992
        Contact contact3 = Utils_TestMethods.createContact(account1.id, 'DAC Contact 1' );
        contact3.firstname = 'DAC';
        contact3.LastName = 'Contact';
        contact3.WorkPhone__c = '99999992';
        contact3.Email = 'vic.account1@schneider-electric.com';
        contact3.accountid = account1.id;
        contact3.ServiceContractType__c = 'DAC';
        newContacts.add(contact3);
        System.debug('####### - DAC Contact 1 - ' + contact3 + '#######');
        
        //Create Standard Contact with VIC Account - No. 99999994
        Contact contact4 = Utils_TestMethods.createContact(account2.id, 'Standard Contact 1' );
        contact4.firstname = 'Stadard';
        contact4.LastName = 'Contact';
        contact4.WorkPhone__c = '99999994';
        contact4.Email = 'std.contact1@schneider-electric.com';
        contact4.accountid = account2.id;
        newContacts.add(contact4);
        System.debug('####### - Standard Contact 1 - ' + contact4 + '#######');
        
        //Create Standard Contact with DAC Account - No. 99999995
        Contact contact5 = Utils_TestMethods.createContact(account1.id, 'Standard Contact 2' );
        contact5.firstname = 'Stadard';
        contact5.LastName = 'Contact';
        contact5.WorkPhone__c = '99999995';
        contact5.Email = 'std.contact2@schneider-electric.com';
        contact5.accountid = account1.id;
        newContacts.add(contact5);
        System.debug('####### - Standard Contact 2 - ' + contact5 + '#######');
        
        insert newAccounts;
        insert newContacts;
        
        //Create open case for VIC Contact 1
        Case case1 = Utils_TestMethods.createCase(account2.Id, contact1.Id, 'Open');
        insert case1;
        
        test.startTest();
        WS_EmailSearch.Customer customer = new WS_EmailSearch.Customer();
        Id[] fixedSearchResults = null; 
        
        System.Debug('##################           Unit Test 1 - Begin          ##################');
        //SINGLE VIC CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = contact1.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        
        customer = WS_EmailSearch.getCustomerInfoFromEmail('dac.account1@schneider-electric.com');
        System.Debug('##################           Unit Test 1 - End            ##################');
        test.stoptest();
    }
}