/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :09-August-2012
    Modification Log    : 
    Description         : Test Method for Connect User Adoption Report
*/

@isTest
private class ConnectUsageReport_Test{
    static testMethod void ConnectUsageReportTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connectX');
      
       // user runAsUser= [select id from user where id='005A0000001pvmD'];
 
        System.runAs(runAsUser){
        
        
          //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.Connect_User_adoption;
       
       //In this case, the Visualforce page named 'Connect_User_adoption' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);

 //Instantiate and construct the controller class.   

        ConnectAdoptionController controller = new ConnectAdoptionController();
        controller.RFilter = 'Login History';
        controller.getReportFilter();
        controller.FilterChange();
        /* controller.AddqueryResults(); */
        controller.getUserLoginInfo();
        /* controller.getqueryResults(); */
        
        
        // Create Initiatives, Program, Global Milestone, Cascading Milestone and Entity Progress Status
        
        Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
        insert inti;
        Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;',GSC_Regions__c='APAC',Year__c = Label.ConnectYear, Power_Region__c='NA');
        insert cp;
        Project_NCP__c cpUpdate=[select id,name,Program_Name__c,Global_Functions__c from  Project_NCP__c where id=:cp.id];
        
        cpUpdate.Program_Name__c='cp-new';
        update cpUpdate;
        
        Project_NCP__c cpUpdnew=[select id,name, LastModifiedById,LastModifiedDate from  Project_NCP__c where id=:cp.id];
        system.debug('Program Updated ' + cpUpdnew.LastModifiedById + '  ' +  cpUpdnew.LastModifiedDate);
        
       
        Connect_Entity_Progress__c CEp1=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM');
        insert CEp1;
        
        Connect_Milestones__c ConMil = new Connect_Milestones__c(Program_Number__c=cp.id,Global_Business__c='ITB',Milestone_Name__c='tst',Roadblock_Decision_to_be_taken_Short__c='Test',Achievements_in_past_quarter_Summary__c='Tst');  
        Insert ConMil;  
        Connect_Milestones__c ConMil2 = new Connect_Milestones__c(Program_Number__c=cp.id,Global_Business__c='ITB',Milestone_Name__c='tst1',Roadblock_Decision_to_be_taken_Short__c='Test',Achievements_in_past_quarter_Summary__c='Tst');  
        Insert ConMil2;  
        
        Cascading_Milestone__c CM=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil2.id, Program_Number__c=cp.id,Program_Scope__c='Global Business',Entity__c='ITB');
        insert CM; 
        
         controller.RFilter = 'User Modified';
         controller.FilterChange();        
         controller.getModifiedUsers();
            
        
         controller.RFilter = 'User Not Logged In';
         controller.FilterChange();
         controller.getUserNotLoggedIn();
         
         controller.RFilter = 'Login History';
         controller.FilterChange();
         controller.getUserLoginInfo();
         controller.getQueryResults();
         

        
    
        }
     }
    }