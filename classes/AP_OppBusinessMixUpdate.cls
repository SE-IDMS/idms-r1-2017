/*    Author:             Pooja Gupta
      Date of Creation:   03-Jan-2013
      Description: Class utilized by triggers to Update the Opportunity BusinessMix on the opportunity.
*/


public class AP_OppBusinessMixUpdate
{
    
    Public static void UpdateOppBusinessMix(List<Opportunity> OppLst)
    {
          //Custom Settings 'CS_ProductLineBU' Map     
          Map<String,CS_ProductLineBU__c> MapDelBU = CS_ProductLineBU__c.getall();
          
          Set<ID> Oppset = New Set<ID>();
          
          for(Opportunity Opp1: OppLst)
          {
              Oppset.add(opp1.id);
          }
          
          List<OPP_ProductLine__c> PLLst = New List<OPP_ProductLine__c>([Select ID,ProductBU__c,Opportunity__c from OPP_ProductLine__c where Opportunity__c in :Oppset]);
           
          //Creating map of Product Lines to loop through the list
         // Map<ID,OPP_ProductLine__c> MapOpplist = New Map<ID,OPP_ProductLine__c>(PLLst);

          //Defining a map to hold unique Opportunity and Product line value set
          Map<ID,Set<String>> MapOppPL = New Map<ID,Set<String>>();
          
          
          
          For(Opp_ProductLine__c OppPl2: PLlst)
          {
              //Checking if GMR ProductLine value contains an equivalent value in the custom settings
              if(MapDelBU.get(OppPl2.ProductBU__c) != NULL)
              {
                  String DelST = MapDelBU.get(OppPl2.ProductBU__c).ProductBU__c;
                  
                  //If Map already contains Opp ID, then append to the list else add a new map set.                  
                  if(MapOppPL.containsKey(OppPl2.Opportunity__c))
                  {
                      MapOppPL.get(OppPl2.Opportunity__c).add(DelST);
                  }else
                  {
                      Set<String> st = new set<String>();
                      st.add(DelST);
                      MapOppPL.put(OppPl2.Opportunity__c, st);
                  }
                  
              }
             
          }
          
          List<Opportunity> ListOpp1 = New List<Opportunity>();
          
          For(ID i: MapOppPL.Keyset())
          {
              Opportunity op1 = New Opportunity(id = i);
              //op1.ID = i;     //To link Opportunity and Product Line Map
              
              Set<String> str = MapOppPL.get(i);
              List<String> lsst = new List<String>();
              lsst.addall(str);
              String s1 = String.Join( lsst , ';');
              
              op1.BusinessMix__c = s1;
             
              ListOpp1.add(op1);
          }    
               
        Update ListOpp1;
    }    
}