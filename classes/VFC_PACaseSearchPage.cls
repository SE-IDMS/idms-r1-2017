/**
    Author          : Nitin Khunal
    Date Created    : 20/07/2016
    Description     : Controller for VFP_PACaseSearchPage VF Page
*/
global without sharing class VFC_PACaseSearchPage{
    Public String strCaseTypeOptions{get;set;}
    public VFC_PACaseSearchPage() {
      Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
        getCaseType();
    }
    @RemoteAction
    Public static List<accountPickList> pickaccountValue(string strUserId){
        set<id> setAccountId = new set<Id>();
        set<id> setOtherAccountId = new set<Id>();
        list<Account> lstAccountId = new list<Account>();
        List<accountPickList> lstAccPick = new List<accountPickList>();
        //User UserObj =[select id,contactId, contact.accountId, contact.account.Name, contact.account.BillingCity, contact.account.BillingState, contact.account.BillingCountry, contact.account.StateProvince__r.StateProvinceCode__c from User where Id=:strUserId];
        User UserObj = PA_UtilityClass.getUser(strUserId);
        accountPickList strobjAll= new accountPickList();
        strobjAll.strAccountName = 'All';
        strobjAll.strAccountId = '';
        lstAccPick.add(strobjAll);
        if(UserObj.contactId != null && UserObj.contact.accountId != null){
            accountPickList strobj= new accountPickList();
            strobj.strAccountName = PA_UtilityClass.concatenateAddress(UserObj.contact.account.Name, UserObj.contact.account.BillingCity, UserObj.contact.account.StateProvince__r.StateProvinceCode__c, UserObj.contact.account.BillingCountry);
            strobj.strAccountId = UserObj.contact.accountId;
            lstAccPick.add(strobj);
            setAccountId.add(UserObj.contact.accountId);
        }
        /*for(AccountShare obj:[select id,AccountID from AccountShare where UserorGroupId=:UserInfo.getUserId()]){
            setOtherAccountId.add(obj.AccountID);
            
        }*/
        setOtherAccountId = PA_UtilityClass.getAccountId(false, true, 'Read', strUserId);
        //for(Account objAcc:[select id,Name, BillingCity, BillingState, BillingCountry, StateProvince__r.StateProvinceCode__c from Account where Id NOT In :setAccountId AND Id In :setOtherAccountId]){
        for(Account objAcc : PA_UtilityClass.getAccountRecord(setOtherAccountId, setAccountId)) {
            accountPickList strobj= new accountPickList();
            strobj.strAccountName = PA_UtilityClass.concatenateAddress(objAcc.Name, objAcc.BillingCity, objAcc.StateProvince__r.StateProvinceCode__c, objAcc.BillingCountry);
            strobj.strAccountId = objAcc.id;
            lstAccPick.add(strobj);
        }
        return lstAccPick;
    }
    
    @RemoteAction
    Public Static List<Case> getCases(String accId, String caseType, String searchString, String PACaseType) {
        Set<Id> setAccIds = new Set<Id>();
        Set<Id> setAllAccIds = new Set<Id>();
        Set<Id> setOrgIds = new Set<Id>();
        List<Case> lstCases = new List<Case>();
        Id userConid; 
        if(accId != '' && accId != null){
            setAllAccIds.add(accId);
            setAccIds.add(accId);
        }
        if(setAccIds.size() > 0) {
            While(!setAccIds.isEmpty()){
                List<AccountShare> lstAccount = [select AccountID from AccountShare where Account.ParentID IN: setAccIds AND UserorGroupId=:UserInfo.getUserId()];
                setAccIds = new Set<Id>();
                if(!lstAccount.isEmpty()){
                    for(AccountShare xAcc : lstAccount){
                        setAllAccIds.add(xAcc.AccountID);
                        setAccIds.add(xAcc.AccountID);
                    }
                }
            }
        } else
        {
            /*User UserObj =[select id,contactId, contact.accountId, contact.account.Name from User where Id=:UserInfo.getUserId()];
            for(AccountShare obj:[select id,AccountID from AccountShare where UserorGroupId=:UserInfo.getUserId()]){
                setAllAccIds.add(obj.AccountID);
            }
            setAllAccIds.add(UserObj.contact.accountId);*/
            setAllAccIds = PA_UtilityClass.getAccountId(true, true, 'Read', userinfo.getUserId());
        }
        /*if(!String.isBlank(System.Label.CLQ316PA021)) {
            String strOrg = System.Label.CLQ316PA021;
            if(strOrg.contains(',')) {
                List<String> parts = strOrg.split(',');
                for(String xOrgId : parts) {
                    setOrgIds.add(xOrgId);
                }
            }else
            {
                setOrgIds.add(strOrg);
            }
        }*/
        User UserObj =[select id,contactId, contact.accountId, contact.account.Name from User where Id=:UserInfo.getUserId()];
        if(UserObj!=null){
            userConid=UserObj.contactId;     
        }
        setOrgIds = PA_UtilityClass.getReportingOrganisation();
        system.debug('accId==='+accId);
        String query = 'SELECT CaseNumber,CreatedDate,PAGCS_Web_Status__c, CommercialReference_lk__r.Name, Contact.Name,CustomerRequestedDate__c,';
        query += ' AccountId, Account.Name, Account.City__c,  Account.StateProvince__r.StateProvinceCode__c, Account.TECH_SDH_CountryCode__c, Site__r.Name,Priority,Owner.Name,';
        query += ' Subject,Family_lk__r.Name,Id,LastModifiedDate,Status,CustomerRequest__c,ActionNeededFrom__c, PACaseType__c, CustomerMajorIssue__c, Alarm_date_and_time__c,';
        query += ' Severity__c, Account.BillingCity, Account.BillingState, Account.BillingCountry, AnswerToCustomer__c, Family__c, PAPlatform__c, ClosedDate, PACustomer_internal_case_number__c,';
        query += ' (select CreatedDate, CommentBody, CreatedBy.Name, Parent.casenumber from CaseComments where IsPublished = true ORDER BY CreatedDate DESC) FROM Case';
        if(!setAllAccIds.isEmpty()){
            query += ' Where AccountId IN: setAllAccIds';   
        }
        if(caseType == 'My' || caseType == 'MyOpen' || caseType == 'MyClose'){
            query += ' and contactId = \''+userConid+'\'';
        }
        if(caseType == 'AllOpen' || caseType == 'MyOpen'){
            query += ' and (Status = \'New\' OR Status = \'In Progress\')';
        }else
        if(caseType == 'AllClose' || caseType == 'MyClose'){
            query += ' and (Status = \'Closed\' OR Status = \'Answer Provided to Customer\' OR Status = \'Cancelled\')';
        }
        if(PACaseType == 'AllPA') {
            query += ' and (PACaseType__c = \''+system.label.CLQ316PA029+'\' OR PACaseType__c = \''+system.label.CLQ316PA040+'\' OR PACaseType__c = \''+system.label.CLQ316PA031+'\')';
        }else
        if(PACaseType == system.label.CLQ316PA029){
             query += ' and PACaseType__c = \''+system.label.CLQ316PA029+'\'';
        }else
        if(PACaseType == system.label.CLQ316PA040) {
            query += ' and PACaseType__c = \''+system.label.CLQ316PA040+'\'';
        }else
        if(PACaseType == system.label.CLQ316PA031) {
            query += ' and PACaseType__c = \''+system.label.CLQ316PA031+'\'';
        }
        if(searchString != '') {
            query += ' and (CaseNumber LIKE \'%' +String.escapeSingleQuotes(searchString) + '%\' OR Subject LIKE \'%' +String.escapeSingleQuotes(searchString) + '%\'';
            query += ' OR Account.Name LIKE \'%' +String.escapeSingleQuotes(searchString) + '%\' OR Contact.Name LIKE \'%' +String.escapeSingleQuotes(searchString) + '%\'';
            query += ' OR Status LIKE \'%' +String.escapeSingleQuotes(searchString) + '%\' OR Priority LIKE \'%' +String.escapeSingleQuotes(searchString) + '%\')';
        }
        query += ' and ReportingOrganization__c IN: setOrgIds';
        String target = 'FROM Case and';
        String replacement = 'FROM Case where';
        query = query.replace(target, replacement);
        system.debug('query==='+query);
        lstCases = Database.query(query);
        return lstCases;
    }
    
    public void getCaseType(){
       List<String> lstCaseTypeOptions= new List<String>();
       lstCaseTypeOptions.add('All');
       lstCaseTypeOptions.addAll(PA_UtilityClass.getCaseType());
       strCaseTypeOptions= JSON.serialize(lstCaseTypeOptions); 
    }
    
    public class accountPickList{
        public String strAccountName{get; set;}
        public String strAccountId {get;set;} 
        public accountPickList(){}   
    }
}