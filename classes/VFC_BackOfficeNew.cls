/*
    Author          : Ram Chilukuri
    Date Created    : 15/Arpil/2014, As part of May 20014 Return connector release.
    Description     : Class utilised by New Backoffice system on BackOfficeSystem__c object , it is related to Country and orgagnation objects .
*/
public with sharing class VFC_BackOfficeNew {

public BackOfficesystem__c NewBackOfficeSystem{get;set;} 

    public string relatedForg{get;set;}
    public BusinessRiskEscalationEntity__c frontorg;
    private Country__c bcountry;
    public VFC_BackOfficeNew(ApexPages.StandardController controller) 
    {
        string orgId = system.currentpagereference().getParameters().get('id1');
        string countryId = system.currentpagereference().getParameters().get('coid');
        system.debug('**** orgId '+ orgId );  
        system.debug('**** countryId '+ countryId );  
   
    }
    public void initializeComplaintRequest(BackOfficesystem__c aNewBackOfficeSystem)
    {   
          string relatedForg  ;    
          String orgId = system.currentpagereference().getParameters().get('id1');
          String countryId = system.currentpagereference().getParameters().get('coid');
            system.debug('**** orgId '+ orgId ) ;     
            if (orgId != '')            
            {
                frontorg=[select id , name from BusinessRiskEscalationEntity__c where id=:orgId ];
            }
            else 
            {
                bcountry=[select id , name from country__c where id=:countryId ];
            }
    }   
    public pagereference goToEditPage()
    {
        initializeComplaintRequest(NewBackOfficeSystem);
        PageReference aNewBackOfficeSystem = new PageReference('/'+SObjectType.BackOfficesystem__c.getKeyPrefix()+'/e' );
        aNewBackOfficeSystem.getParameters().put(Label.CL00690, Label.CL00691);
        
           if (frontorg!= null) {
                aNewBackOfficeSystem.getParameters().put(Label.CL00330, frontorg.ID);
                aNewBackOfficeSystem.getParameters().put(Label.CLAPR14RRC13,frontorg.id); //'CF00NJ0000001KHJn_lkid'
                aNewBackOfficeSystem.getParameters().put(Label.CLAPR14RRC14,frontorg.Name); //'CF00NJ0000001KHJn'
                aNewBackOfficeSystem.getParameters().put('RecordType',Label.CLAPR14RRC15);//'012J00000004kWg'
                system.debug('**** aNewBackOfficeSystem'+ aNewBackOfficeSystem) ;
            }
          else{
                aNewBackOfficeSystem.getParameters().put(Label.CL00330, bcountry.id);
                aNewBackOfficeSystem.getParameters().put(Label.CLAPR14RRC16,bcountry.id);//'CF00NJ0000001KQfY_lkid'
                aNewBackOfficeSystem.getParameters().put(Label.CLAPR14RRC17,bcountry.Name);  //'CF00NJ0000001KQfY'
                aNewBackOfficeSystem.getParameters().put('RecordType',Label.CLAPR14RRC18); //'012J00000004kWb'
            }
            return aNewBackOfficeSystem; 
    }
}