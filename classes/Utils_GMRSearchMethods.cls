// The strategy interface defines the three methods with different behavior according to the current context (Case, Work order ...)
public Interface Utils_GMRSearchMethods
{
   Void Init(String ObjID, VCC08_GMRSearch Controller); // Initialization of the component
   Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase); // Action performed when commandlink is clicked in the result table
   PageReference Cancel(VCC08_GMRSearch Controller); // Cancel method
}