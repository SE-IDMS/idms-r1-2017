public without sharing class VFC_CustomerPortalClsCaseView {
    Id CaseId;
    Id CaseCommentId; 
    
Id usid;
Id contid;
Id accid;   

Contact userContact = new Contact();    
Account userAcc = new Account();
User userr = null;

String ContactPhone = null;
String accountName = null;
    
    Case caseDetail;
    List<CaseComment> caseComments = null;
    CaseComment caseCommentDetail;
    List<Attachment> attachments;       

public String getContactPhone () {return ContactPhone ;}
public void setContactPhone (String Phone) {this.ContactPhone = ContactPhone ;}

public String getAccountName () {return accountName;}
public void setAccountName (String Phone) {this.accountName = accountName  ;}

public boolean getIsAuthenticated ()
{
  return (IsAuthenticated());
}

public boolean IsAuthenticated ()
{
  return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
}

public Contact getUserContact() {return userContact;}

public User getUserr() {return userr;}

    public Account getUserAcc() {return UserAcc;}
    
    
    public Id getCaseId(){return CaseId;}
    
    public VFC_CustomerPortalClsCaseView()
    {
      if (isAuthenticated())
      {
        CaseId = ApexPages.currentPage().getParameters().get('Id');
     
        if (CaseId != null){
            caseDetail = [SELECT CaseNumber, Subject, SubSymptom__c, SoftwareVersion__c, Status, SupportCategory__c, CustomerRequest__c, IsClosed FROM Case WHERE  Id =: CaseId];  
            
            caseComments = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
            attachments = [SELECT Id, CreatedById, Name, CreatedDate, BodyLength, ContentType, Description, IsPrivate, LastModifiedDate FROM Attachment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
            }
     
     
             usid = UserInfo.getUserId();
             if ( usid != null)
                {
                  contid = [Select ContactId from User where Id =: usid LIMIT 1][0].ContactId;
                  userr = [Select FirstName, LastName, Email from User where Id =: usid LIMIT 1][0];
                }
     
            if ( contid != null)
                {
                  accid = [Select AccountId from Contact where Id =: contid].AccountId;
                  userContact = [Select WorkPhone__c from Contact where Id =: contId][0];
                  userAcc = [Select Name from Account where Id =: Accid][0];
                  ContactPhone = userContact.WorkPhone__c;
                  accountName = userAcc.Name;
                }
                  
      }
     }   







// Return the parameter to the page
    public Case getCaseDetail() 
    {    
      if (caseDetail != null) 
      {
          if      (caseDetail.status.equalsIgnoreCase('New'))       {caseDetail.status = 'New';}
          else if (caseDetail.status.equalsIgnoreCase('Cancelled')) {caseDetail.status = 'Closed';}
          else if (caseDetail.status.equalsIgnoreCase('Closed'))    {caseDetail.status = 'Closed';}
          else caseDetail.status = 'In Progress';     
      }
      
      return caseDetail;   
    }

/*=======Return the comments List. ===================*/
    public List<CaseComment> getCaseComments() {    
         List<CaseComment> lcomm = new List<CaseComment>();
        
        if (CaseComments != null){
            for (CaseComment lcc : CaseComments){
                    if (lcc.IsPublished) lcomm.add(lcc);
            }
            return lcomm;
        }
        else return CaseComments; 
 
    }        
              
    public pageReference getCaseDetailView()
    {
      if (IsAuthenticated())
      {
      	getCaseDetail(); 
      }
      
      return null;
    
    }   
    
/*======= Return the Attachments List ===================*/
    public List<Attachment> getAttachments() {    
        List<Attachment> lattach = new List<Attachment>();
        
        if (Attachments != null){
            for (Attachment att : Attachments){
                    if (!(att.IsPrivate)) lattach.add(att);
            }
            return lattach;
        }
        else return Attachments;   
    }        
       
     
}