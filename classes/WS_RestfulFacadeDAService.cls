@RestResource(urlMapping='/da_rest_service/*')
global with sharing class WS_RestfulFacadeDAService {
    global static string strUsername;
    global static string strPassword;
    global static string strSourceIp;
    @HttpGet
    global static void doGet() {
        RestContext.response.addHeader('Content-Type', 'application/xml');
        String wsdlFile = System.Label.CLOCT15CCC22 + '/resource/SR_DelegatedAuthentication/DelegatedAuthentication/WSDL/DelegatedAuthentication.wsdl';
        RestContext.response.responseBody = Blob.valueOf(getBody(wsdlFile));
    }
 
 
    // Just checking that it's actually XML
    private static String parseXMLStr(String toParse) {
        DOM.Document doc = new DOM.Document();
       
        try {
            doc.load(toParse);    
            DOM.XMLNode root = doc.getRootElement();
            return walkThrough(root);
         
        } 
        catch (System.XMLException e) {  // invalid XML
            return e.getMessage();
        }
    }
    

    // Recursively walk through the XML
    /*
        14:22:49.043 (43847766)|USER_DEBUG|[96]|DEBUG|*** parseXMLStr=
        Element: Envelope namespace: http://schemas.xmlsoap.org/soap/envelope/
        Element: Body namespace: http://schemas.xmlsoap.org/soap/envelope/
        Element: Authenticate namespace: urn:authentication.soap.sforce.com
        Element: username namespace: urn:authentication.soap.sforce.com, text=OKM.Customer.invensys@bridge-fo.com.devoct
        Element: password namespace: urn:authentication.soap.sforce.com, text=5F91558D-0274-4423-B397-AA7065EA5074
        Element: sourceIp namespace: urn:authentication.soap.sforce.com, text=INVENSYS
    */
    // Reference: http://developer.force.com/cookbook/recipe/parsing-xml-using-the-apex-dom-parser
    private static String walkThrough(DOM.XMLNode node) {
        String result = '\n';
        if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
            return 'Comment (' +  node.getText() + ')';
        }
        if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
            return 'Text (' + node.getText() + ')';
        }
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            result += 'Element: ' + node.getName() + ' Namespace: ' + node.getNamespace();
            if (node.getText().trim() != '') {
                result += ', text=' + node.getText().trim();
            }
            if(node.getName().trim().equalsIgnoreCase('username')){
                strUsername = node.getText().trim();
            }
            else if(node.getName().trim().equalsIgnoreCase('password')){
                strPassword =  node.getText().trim();
            }
            else if(node.getName().trim().equalsIgnoreCase('sourceIp')){
                strSourceIp =  node.getText().trim();
            }
            if (node.getAttributeCount() > 0) { 
                for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
                    result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
                }  
            }
            for (Dom.XMLNode child: node.getChildElements()) {
                result += walkThrough(child);
            }
            return result;
        }
        return '';  //should never reach here     
    }    
 
    /*
    private Boolean isValid_DA_SOAPRequest(Dom.XMLNode node) {
        Boolean isValidSoapRequest = false;
        
        left as an exercise - Applied your logic here
                 
        return isValidSoapRequest;
    }
    */    
 
    private static String getBody(String url){
        Http h = new Http();
        HttpRequest webReq = new HttpRequest();
        HttpResponse res = new HttpResponse();
        webReq.setMethod('GET');
        webReq.setEndpoint(url);
        if(!Test.isRunningTest())
            res = h.send(webReq);
        else{
            String strWSDL= '<?xml version="1.0" encoding="UTF-8"?><definitions targetNamespace="urn:authentication.soap.sforce.com" xmlns="http://schemas.xmlsoap.org/wsdl/" xmlns:tns="urn:authentication.soap.sforce.com" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"></definitions>';
            res.setBody(strWSDL);           
        }
        res.setHeader('Content-Type', 'application/xml'); 
        return res.getbody();
    }   
 
 
    //1. Before you can access external servers from an endpoint or redirect endpoint using Apex or any other feature, you must add the remote site to a list of authorized remote sites in the Salesforce user interface. 
    //   Unauthorized endpoint, please check Setup->Security->Remote site settings - Add your site endpoint https://cmok-developer-edition.eu5.force.com
    //2.The return type of you WS MUST be void, so you can return any content type, in our case a valid SOAP response
    @HttpPost
    global static void doPost() {
        //RestRequest restRequest = RestContext.request;
        //String soapRequestBody= restRequest.requestBody ; 
        String soapRequestBody = RestContext.request.requestBody.toString();
        System.debug('*** soapRequestBody=' + soapRequestBody);
        System.debug('*** parseXMLStr=' + parseXMLStr(soapRequestBody));        
        RestContext.response.addHeader('Content-Type', 'application/xml');
        String urlSuccess = System.Label.CLOCT15CCC22 + '/resource/SR_DelegatedAuthentication/DelegatedAuthentication/Response/response_true.xml';
        String urlFailed  = System.Label.CLOCT15CCC22 + '/resource/SR_DelegatedAuthentication/DelegatedAuthentication/Response/response_False.xml';
        /*
        left as an exercise - Applied your logic here
        */
        System.debug('UserName: ' + strUsername);
        System.debug('Password: ' + strPassword);
        System.debug('SourceIp: ' + strSourceIp);
        WS_FacadeDelegatedAuthService.AuthenticationService objAuthenticationService = new WS_FacadeDelegatedAuthService.AuthenticationService();

        if(strUsername.containsIgnoreCase(System.Label.CLOCT15CCC38)){
            objAuthenticationService.endpoint_x = System.Label.CLOCT15CCC21;
        }
        else{
            objAuthenticationService.endpoint_x = System.Label.CLOCT15CCC20 ;    
        }
        objAuthenticationService.timeout_x= 120000;
        String retURL;
        if(!Test.isRunningTest() && objAuthenticationService.Authenticate(strUsername,strPassword,strSourceIp)){
            retURL = urlSuccess;
        }
        else{
            retURL = urlFailed;
        } 
        if(!Test.isRunningTest())
            RestContext.response.responseBody = Blob.valueOf(getBody(retURL));
        else{
            String strResponse= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><AuthenticateResult xmlns="urn:authentication.soap.sforce.com"><Authenticated>true</Authenticated></AuthenticateResult></soapenv:Body></soapenv:Envelope>';
            RestContext.response.responseBody = Blob.valueOf(strResponse);           
        }
    }
}