global class AP_AsyncEmailMessage
{
    public static boolean blnfutureCallEnabled=true;
    public static boolean blnRunTest=false;
    
    @future
    public static void updateCaseEmailDates(Id caseId)
    {
        // Retrieve all emails attached to this case ordered by date (DESC)
        EmailMessage[] emails = [SELECT Id, MessageDate, FromAddress, BccAddress, CcAddress, ToAddress, Parent.FirstLogicalResponseDateTime__c, Parent.SuppliedEmail, Parent.Contact.Email, Incoming, ParentId, CreatedById, Subject
                                    FROM EmailMessage
                                    WHERE ParentId =: caseId
                                    ORDER BY MessageDate DESC];
        
        System.debug('######### Number of Emails: ' + emails.size());
        
        // Outbound Email Dates
        Datetime FirstReplyToCustomerDate = null;
        Datetime LastReplyToCustomerDate = null;
        Datetime FirstLogicalResponseDateTime = null;

        Boolean  blnFirstLogicalResponseIdnetified = false;
        
        // Inbound Email Dates
        Datetime OldestCustomerEmailWithoutReplyDate = null;
        Datetime LastCustomerEmailDate = null;
        
        // Parsing all emails from the latest to the first one
        for (EmailMessage email : emails)
        {
            if (email.Incoming)
            {
                // Inbound Email - Email is sent by the customer
                System.debug('######### Inbound Email - Email is sent by the customer: '+email.Subject);
                if (LastCustomerEmailDate == null)
                {
                    // If LastCustomerEmailDate = null, this email is  the last email sent by the customer
                    LastCustomerEmailDate = email.MessageDate;
                }
                if (LastReplyToCustomerDate == null)
                {
                    // If LastReplyToCustomerDate is still empty, this agent did not answer this email yet
                    if (OldestCustomerEmailWithoutReplyDate == null || email.MessageDate < OldestCustomerEmailWithoutReplyDate)
                    {
                        // This email is the Oldest Customer Email Without Reply IF OldestCustomerEmailWithoutReplyDate is empty
                        // OR if this email date is older than OldestCustomerEmailWithoutReplyDate
                        OldestCustomerEmailWithoutReplyDate = email.MessageDate;
                    }
                }
            }
            else
            {
                // Outbound Email - Email is sent to the customer
                System.debug('######### Outbound Email - Email is sent to the customer: '+email.Subject);
                if (email.CreatedById != System.Label.CLOCT14CCC24)
                {
                    // this email is an outbound email BUT NOT an auto-response
                    // (Is outbound and created By System Interface User / 005A0000001nmj9IAA / CLOCT14CCC24)
                    System.debug('######### Outbound Email is not an auto response: '+email.Subject);
                    if (LastReplyToCustomerDate == null)
                    {
                        // If LastReplyToCustomerDate is empty, this email is the Last Reply Sent To The Customer
                        LastReplyToCustomerDate = email.MessageDate;
                    }
                    if (FirstReplyToCustomerDate == null || email.MessageDate < FirstReplyToCustomerDate)
                    {
                        // If FirstReplyToCustomerDate is empty OR this email date is prior to FirstReplyToCustomerDate
                        // Than this email is the earliest email sent to the customer
                        FirstReplyToCustomerDate = email.MessageDate;
                    }

                    if (email.Parent.FirstLogicalResponseDateTime__c == null){
                        String strOutgoingEmailAddress = email.ToAddress + ',' + email.CcAddress + ',' + email.BccAddress;
                        //HF-1298
                        //if(strOutgoingEmailAddress.toLowerCase().contains(email.Parent.Contact.Email.toLowerCase()) || strOutgoingEmailAddress.toLowerCase().contains(email.Parent.SuppliedEmail.toLowerCase())){
                        //if((email.Parent.Contact!=null && email.Parent.Contact.email.trim().length()>0 && strOutgoingEmailAddress.toLowerCase().contains(email.Parent.Contact.Email.toLowerCase())) || (email.Parent.SuppliedEmail.trim().length()>0 && strOutgoingEmailAddress.toLowerCase().contains(email.Parent.SuppliedEmail.toLowerCase()))){
                        if((email.Parent.Contact!=null && email.Parent.Contact.email!=null && email.Parent.Contact.email.trim().length()>0 && strOutgoingEmailAddress.toLowerCase().contains(email.Parent.Contact.Email.toLowerCase())) || (email.Parent.SuppliedEmail!=null && email.Parent.SuppliedEmail.trim().length()>0 && strOutgoingEmailAddress.toLowerCase().contains(email.Parent.SuppliedEmail.toLowerCase()))){
                            if (FirstLogicalResponseDateTime == null || email.MessageDate < FirstLogicalResponseDateTime){
                                blnFirstLogicalResponseIdnetified=true;
                                FirstLogicalResponseDateTime = email.MessageDate;
                            }
                        }
                    }

                }
            }
        }
        
        // Map the dates with date fields on case
        Case caseToUpdate = new Case(Id = caseId);
        if(blnFirstLogicalResponseIdnetified){
            caseToUpdate.FirstLogicalResponseDateTime__c = FirstLogicalResponseDateTime;
        }

        caseToUpdate.FirstReplyToCustomerDate__c = FirstReplyToCustomerDate;
        caseToUpdate.LastReplyToCustomerDate__c = LastReplyToCustomerDate;
        caseToUpdate.OldestCustomerEmailWithoutReplyDate__c = OldestCustomerEmailWithoutReplyDate;
        caseToUpdate.LastCustomerEmailDate__c = LastCustomerEmailDate;
        
        System.debug('######### First Logical Response calculated: '+ blnFirstLogicalResponseIdnetified);
        System.debug('######### FirstLogicalResponseDateTime: '+caseToUpdate.FirstLogicalResponseDateTime__c);
        System.debug('######### FirstReplyToCustomerDate: '+caseToUpdate.FirstReplyToCustomerDate__c);
        System.debug('######### LastReplyToCustomerDate: '+caseToUpdate.LastReplyToCustomerDate__c);
        System.debug('######### OldestCustomerEmailWithoutReplyDate: '+caseToUpdate.OldestCustomerEmailWithoutReplyDate__c);
        System.debug('######### LastCustomerEmailDate: '+caseToUpdate.LastCustomerEmailDate__c);
        
        try
        {
            update caseToUpdate;
            System.debug('######### UPDATE SUCCESSFULL');
        }
        catch(Exception ex)
        {
            createDebugLog(caseToUpdate.Id, ex);
        }
    }
    
    public static void createDebugLog(Id caseId, Exception ex)
    {
        System.debug('######### bug in trigger EmailMessage '+ex.getMessage());
        if(!Test.IsRunningTest() || blnRunTest){
            Database.DMLOptions DMLoption = new Database.DMLOptions(); 
            DMLoption.optAllOrNone = false;
            DMLoption.AllowFieldTruncation = true;
            DebugLog__c log = new DebugLog__c();
            log.StackTrace__c = ex.getStackTraceString();
            log.ExceptionType__c = ex.getTypeName();
            log.Description__c =  'Case Threshold Update issue \n\rCase Id: '+caseId+'\n\r'+ex.getMessage();
            log.OwnerId = System.Label.CLOCT14CCC24; // System Interface User
            DataBase.SaveResult logInsertSaveResult = database.insert(log, DMLoption);
        }
        //insert log;
    }
    
    public class MyCustomException extends Exception{}
}