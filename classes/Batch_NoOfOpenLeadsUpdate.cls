/* 
Srinivas Nallapati    June Mkt release    
How to run :   
        Batch_NoOfOpenLeadsUpdate updateContacts =new Batch_NoOfOpenLeadsUpdate();
        id batchprocessid = Database.executeBatch(updateContacts);
        system.debug(batchprocessid);
*/
global class Batch_NoOfOpenLeadsUpdate implements Database.Batchable<sObject>{
    public String queryString;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date dd = (system.today() -1);
        
        if(queryString == null)
        
            return Database.getQueryLocator('SELECT Contact__c, TECH_PreviousContactId__c, LeadContact__C, TECH_PreviousLeadContactId__c  FROM lead WHERE (Contact__c != null  or leadcontact__C != null) and (LastModifiedDate = YESTERDAY or LastModifiedDate = TODAY )'); 
        else
            return Database.getQueryLocator(queryString);
    }
    
    //// This method will be called by SFDC Job Batch with internal batch size to default of 200
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        set<id> Conids = new set<id>();
        set<id> LeadConIds = new set<id>();
        
        for(Lead ld :(Lead[]) scope){
            if(ld.Contact__c != null)
               Conids.add(ld.Contact__c );
            if(ld.TECH_PreviousContactId__c != null)
               Conids.add(ld.TECH_PreviousContactId__c);   
            
            if(ld.LeadContact__c != null)
               LeadConIds.add(ld.LeadContact__c);
            if(ld.TECH_PreviousLeadContactId__c != null)
               LeadConIds.add(ld.TECH_PreviousLeadContactId__c);  
        }
        system.debug('CONTACTID BATCH SIZE : '+ Conids.size());
        system.debug('LEACCONTACT BATCH SIZE : '+ LeadConIds.size());
        if(Conids.size() > 0)
            updateContactWithOpenLeadsCount('Contact', Conids);
        if(LeadConIds.size() > 0)
            updateContactWithOpenLeadsCount('LeadContact', LeadConIds);           
    
    
    //May 2013 Release -Start- BR-2821, BR-2859
    updateExistingLeadOwner(Conids);
    UpdateExistingLeadOwnerLeadContact(LeadConIds);
    //May 2013 Release -End- BR-2821, BR-2859
    }
    
    ////
    public void updateContactWithOpenLeadsCount(string sObjectTypeName, set<id> contactIds)
    {
        List<sObject> setCons = new List<sObject>();
        List<aggregateResult> results = null;
        
        if (sObjectTypeName == 'Contact')
            results = [SELECT COUNT(Id) OpenCount, Contact__C FROM lead  WHERE Contact__C IN :contactIds and (Status = :Label.CL00554 OR Status = :Label.CL00447) group by Contact__c];
        else if (sObjectTypeName == 'LeadContact')
            results = [SELECT COUNT(Id) OpenCount, LeadContact__C FROM lead  WHERE LeadContact__C IN :contactIds and (Status = :Label.CL00554 OR Status = :Label.CL00447) group by LeadContact__c];
        
        for (AggregateResult ar : results) {
           id Conid = sObjectTypeName == 'Contact' ? (id)ar.get('Contact__c') : (id)ar.get('LeadContact__c');
           
           if (sObjectTypeName == 'Contact')
              setCons.add(new Contact(id = Conid, TECH_NumberOfOpenLeads__c= Integer.valueOf(ar.get('OpenCount'))));
           else if (sObjectTypeName == 'LeadContact')
              setCons.add(new Lead (id = Conid,  TECH_NumberOfOpenLeads__c= Integer.valueOf(ar.get('OpenCount'))));
        }  
        system.debug('UPDATE BATCH SIZE : '+setCons.size());
        
        if(setCons.size() > 0) {            
            update setCons;
        }
    }
    
    
    //May 2013 Release -Start- BR-2821, BR-2859
    public void UpdateExistingLeadOwner( set<id> ContactIds)
    {
        List<Lead> lstLead1 = new list<Lead>();
        System.Debug('********** Batch debug Contact Query start**********');
        lstLead1 = [Select Contact__c,Account__c, Owner.name,Status, Category__c, LastModifiedDate from Lead where Contact__c in:ContactIds and Owner.name !=Null  ORDER BY LastModifiedDate DESC];
        
        List<Lead> lstClosedLeads = new list<Lead>();   
        Map<id,list<lead>> Mapleadlst = new Map<id,list<lead>>(); // Map of Contact id and List of related leads
        Map<id,set<string>> MapOwnerToCategory = New Map<id,set<string>>(); // Map of Owner Id and list of Category
               
        For(lead newlead:lstLead1)
        {
            String owdId = newlead.OwnerID;
            if(owdId.startsWith('005'))
            {
                if(Mapleadlst.containskey(newlead.Contact__c))
                    Mapleadlst.get(newlead.Contact__c).add(newlead);
                if(!Mapleadlst.containskey(newlead.Contact__c))
                    Mapleadlst.put(newlead.Contact__c, new Lead[] {newlead});
            }
        }
        
       
        system.debug('MapOwnerToCategory'+MapOwnerToCategory);
        List<Contact> ContactUpdate = New List<Contact>();
               
        //Passing all updated leads contact ids
        For(Id ContactId1: Mapleadlst.keyset())
        {        
            List<Lead> lstLds = new List<Lead>();
            String existingownerdetails='';     
            set<Id> leadowners=new Set<Id>(); // To hold unique owner ids
            for(Lead LeadMatch: Mapleadlst.get(ContactId1))
            { 
                    if(LeadMatch.status==Label.CL00554 || LeadMatch.Status ==Label.CL00447)
                    {
                        //To add to map 'MapOwnerToCategory' only those owner ids and category that are not null
                        if(LeadMatch.Category__c != Null)
                        {
                            if(MapOwnerToCategory.containskey(LeadMatch.OwnerId))
                                MapOwnerToCategory.get(LeadMatch.OwnerID).add(LeadMatch.Category__c);
                            else
                            {
                                set<string> tmpset = new set<string>();
                                tmpset.add(LeadMatch.Category__c);
                                MapOwnerToCategory.put(LeadMatch.OwnerID,tmpset);
                            }
                        }
                        if(!leadowners.contains(LeadMatch.OwnerId) && leadowners.size()<4)
                        {
                            leadowners.add(LeadMatch.OwnerId);
                            lstLds.add(LeadMatch);  
                        }
                    }    
            }
            for(Integer i=0;i<2 && i<lstLds.size(); i++)
            {
                Lead LeadMatch = lstLds[i];
                existingownerdetails += ','+ LeadMatch.Owner.name + ((MapOwnerToCategory.containsKey(LeadMatch.OwnerId)) ? (MapOwnerToCategory.get(LeadMatch.OwnerId)+'').replace('{','(').replace('}',')')+ ' ': '');
            
            }
            if(lstLds.size() > 2)
                existingownerdetails +=' ,and others';
               
                    
            existingownerdetails = existingownerdetails.replaceFirst(',','');
            leadowners.clear();                     
            ContactUpdate.add(new Contact(id = ContactId1,ExistingLeadOwner__c=existingownerdetails));
            existingownerdetails='';
         }   
                    
        Database.Saveresult[] lstResult =  Database.update(ContactUpdate, false);
        System.Debug('********** Batch debug end **********');
        
    }
    
    //May 2013 Release - Continued for LeadContact
    
    Public void UpdateExistingLeadOwnerLeadContact(Set<id> LeadContactIds)
    {
        List<Lead> lstLead1 = new list<Lead>();
        System.Debug('********** Batch debug Contact Query start**********');
        lstLead1 = [Select LeadContact__c,Account__c, Owner.name,Status, Category__c, LastModifiedDate from Lead where LeadContact__c in:LeadContactIds and Owner.name !=Null  ORDER BY LastModifiedDate DESC];
        Map<id,list<lead>> Mapleadlst = new Map<id,list<lead>>(); // Map of Contact id and List of related leads
        Map<id,set<string>> MapOwnerToCategory = New Map<id,set<string>>(); // Map of Owner Id and list of Category
               
        for(lead newlead:lstLead1)
        {
            String owdId = newlead.OwnerID;
            if(owdId.startsWith('005'))
            {
                if(Mapleadlst.containskey(newlead.LeadContact__c))
                    Mapleadlst.get(newlead.LeadContact__c).add(newlead);
                if(!Mapleadlst.containskey(newlead.LeadContact__c))
                    Mapleadlst.put(newlead.LeadContact__c, new Lead[] {newlead});
            }
        }   
        
        system.debug('MapOwnerToCategory'+MapOwnerToCategory);
        List<Lead> LeadContactUpdate = New List<Lead>();
               
        //Passing all updated leads contact ids
        For(Id ContactId1: Mapleadlst.keyset())
        {        
            List<Lead> lstLds = new List<Lead>();
            String existingownerdetails='';     
            set<Id> leadowners=new Set<Id>(); // To hold unique owner ids
            for(Lead LeadMatch: Mapleadlst.get(ContactId1))
            { 
                    if(LeadMatch.status==Label.CL00554 || LeadMatch.Status ==Label.CL00447)
                    {
                        //To add to map 'MapOwnerToCategory' only those owner ids and category that are not null
                        if(LeadMatch.Category__c != Null)
                        {
                                if(MapOwnerToCategory.containskey(LeadMatch.OwnerId))
                                    MapOwnerToCategory.get(LeadMatch.OwnerID).add(LeadMatch.Category__c);
                                else
                                {
                                    set<string> tmpset = new set<string>();
                                    tmpset.add(LeadMatch.Category__c);
                                    MapOwnerToCategory.put(LeadMatch.OwnerID,tmpset);
                                }
                        }
                        if(!leadowners.contains(LeadMatch.OwnerId) && leadowners.size()<4)
                        {
                            leadowners.add(LeadMatch.OwnerId);
                            lstLds.add(LeadMatch);  
                        }
                    }    
            }
            for(Integer i=0;i<2 && i<lstLds.size(); i++)
            {
                Lead LeadMatch = lstLds[i];
                existingownerdetails += ','+ LeadMatch.Owner.name + ((MapOwnerToCategory.containsKey(LeadMatch.OwnerId)) ? (MapOwnerToCategory.get(LeadMatch.OwnerId)+'').replace('{','(').replace('}',')')+ ' ': '');
            
            }
            if(lstLds.size() > 2)
                existingownerdetails +=' ,and others';
               
                    
            existingownerdetails = existingownerdetails.replaceFirst(',','');
            leadowners.clear();                     
            LeadContactUpdate.add(new Lead(id = ContactId1,ExistingLeadContactOwner__c=existingownerdetails));
            existingownerdetails='';
         }   
                    
        Database.Saveresult[] lstResult =  Database.update(LeadContactUpdate, false);
        System.Debug('********** Batch debug end **********');
        
    }      
    //May 2013 Release -End- BR-2821, BR-2859
    
    global void finish(Database.BatchableContext BC)
    {
    
    }           
       
}