/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Trigger Class                                       *
* Trigger: Fielo_OnContact                                  *
* Created Date: 19/08/2014                                  *
************************************************************/
public with sharing class Fielo_ContactTriggers{

    public static Boolean bypassAccountCreation = false;

    public static void createsAccount(List<Contact> triggerNew){
        
        Set<Id> setMembersId = new Set<Id>();
        
        List<FieloEE__Program__c> listProg = [SELECT Id FROM FieloEE__Program__c WHERE FieloEE__CustomPage__c = 'Fielo_PageCustom' LIMIT 1];
               
        if(!listProg.isEmpty()){
            
            for(Contact con: triggerNew){
                if(con.FieloEE__Member__c != null){
                    setMembersId.add(con.FieloEE__Member__c);
                }
            }
            
            if(!setMembersId.isEmpty()){
                
                Map<Id,FieloEE__Member__c> mapMembers = new Map<Id,FieloEE__Member__c>(
                    [SELECT Id, FieloEE__FirstName__c, FieloEE__LastName__c, F_CompanyName__c, FieloEE__Phone__c, FieloEE__MobilePhone__c, F_Country__c, 
                    F_CompanyPhoneNumber__c, F_Address1__c, F_NearLandmark__c, F_StateProvince__c, F_City__c, F_POBox__c, F_CorporateHeadquarters__c, 
                    F_BusinessType__c, FieloEE__Program__r.OwnerId, F_AreaOfFocus__c, F_Title__c, FieloEE__Email__c, F_JobTitle__c, F_JobFunction__c, 
                    FieloEE__Program__c FROM FieloEE__Member__c WHERE Id IN: setMembersId AND FieloEE__Program__c =: listProg[0].Id]
                );
                
                if(!mapMembers.isEmpty()){
                
                    Map<Id,Account> mapMemberAccount = new Map<Id,Account>();
                      
                    for(Contact con: triggerNew){    
                        if(!bypassAccountCreation){     
                            if(setMembersId.contains(con.FieloEE__Member__c)){
                                if(mapMembers.containsKey(con.FieloEE__Member__c)){                        
                                    Account acc = new Account(
                                        Name                     = mapMembers.get(con.FieloEE__Member__c).F_CompanyName__c,
                                        Phone                    = mapMembers.get(con.FieloEE__Member__c).F_CompanyPhoneNumber__c,
                                        Country__c               = mapMembers.get(con.FieloEE__Member__c).F_Country__c,
                                        Street__c                = mapMembers.get(con.FieloEE__Member__c).F_Address1__c,
                                        AdditionalAddress__c     = mapMembers.get(con.FieloEE__Member__c).F_NearLandmark__c,
                                        City__c                  = mapMembers.get(con.FieloEE__Member__c).F_City__c,
                                        POBox__c                 = mapMembers.get(con.FieloEE__Member__c).F_POBox__c,
                                        CorporateHeadquarters__c = mapMembers.get(con.FieloEE__Member__c).F_CorporateHeadquarters__c,
                                        ClassLevel1__c           = mapMembers.get(con.FieloEE__Member__c).F_BusinessType__c,
                                        ClassLevel2__c           = mapMembers.get(con.FieloEE__Member__c).F_AreaOfFocus__c,
                                        ZipCode__c               = mapMembers.get(con.FieloEE__Member__c).F_POBox__c,
                                        OwnerId                  = mapMembers.get(con.FieloEE__Member__c).FieloEE__Program__r.OwnerId
                                    );
                                    mapMemberAccount.put(con.FieloEE__Member__c,acc);
                                }
                            }
                        }
                    }

                    if(!mapMemberAccount.isEmpty()){
                        insert mapMemberAccount.values();
                    }
                    
                    for(Contact con: triggerNew){
                        if(setMembersId.contains(con.FieloEE__Member__c)){
                            if(mapMembers.containsKey(con.FieloEE__Member__c)){
                                con.Salutation  = mapMembers.get(con.FieloEE__Member__c).F_Title__c;
                                con.FirstName   = mapMembers.get(con.FieloEE__Member__c).FieloEE__FirstName__c;
                                con.LastName    = mapMembers.get(con.FieloEE__Member__c).FieloEE__LastName__c;
                                con.Email       = mapMembers.get(con.FieloEE__Member__c).FieloEE__Email__c;
                                con.JobTitle__c = mapMembers.get(con.FieloEE__Member__c).F_JobTitle__c;  
                                con.JobFunc__c  = mapMembers.get(con.FieloEE__Member__c).F_JobFunction__c;
                                con.Country__c  = mapMembers.get(con.FieloEE__Member__c).F_Country__c;
                                if(mapMemberAccount.containsKey(con.FieloEE__Member__c)){
                                    con.AccountId = mapMemberAccount.get(con.FieloEE__Member__c).Id;
                                }
                            }        
                        }
                    }
                }
            }
        }
    }
    
}