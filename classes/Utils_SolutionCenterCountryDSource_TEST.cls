@Istest
public with sharing class Utils_SolutionCenterCountryDSource_TEST {
    
//*********************************************************************************
// Controller Name  : Utils_SolutionCenterCountry_TEST
// Purpose          : Data Source for Search Solution Center
// Created by       : Hari Krishna  Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For Sales April 2012 Release
///********************************************************************************/
    
    static testMethod void SolutionCenterCountryDSource_TestMethod(){
        
        Country__c country1 = Utils_TestMethods.createCountry();
        insert country1;
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
              
        Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
        oppty.OwnerId = UserInfo.getUserId();      
        insert oppty;  
            
        SolutionCenter__c solcenter= Utils_TestMethods.createSolutionCenter();
        solcenter.Name = 'Test for VFP79_SearchSolutionCenter';
        solcenter.BusinessesServed__c ='BD';
        solcenter.TierCategory__c='A';
        insert  solcenter;
            
        SolutionCenterCountry__c  scc = Utils_TestMethods.createSolutionCenterCountry(solcenter,country1);
        insert scc;
        
        Utils_DataSource SolutionCenterCountryDSource = Utils_Factory.getDataSource('SOCC');
        List<SelectOption> Columns = SolutionCenterCountryDSource.getColumns();
        List<String> dummyList1 = new List<String>();
        List<String> dummyList2 = new List<String>();
        Integer counter = 0;
        
        for(Integer index=1;index<11;index++)
        {
            dummyList1.clear();
            dummyList2.clear();                
            counter = index;
            if(counter == 1)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country1.Id));
                dummyList2.add('A');
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 2)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country1.Id));
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 3)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 4)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                dummyList2.add('A');
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 5)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 6)
            {
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 7)
            {
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country1.Id));
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 8)
            {
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country1.Id));
                dummyList2.add('A');
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 9)
            {
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country1.Id));
                dummyList2.add('A');
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            if(counter == 10)
            {
                dummyList1.add('UNKNOWN');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country1.Id));
                dummyList2.add('A');
                Utils_DataSource.Result Results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
            }
            
        }
        
        
        Utils_DataSource.Result results = SolutionCenterCountryDSource.search(dummyList1, dummyList2);
    
        
        
        
        
    }

}