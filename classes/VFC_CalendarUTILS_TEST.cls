/**
    About
    -----
    Description: Test class for VFC_CalendarUTILS 
    Created for: SOW: IC001 ICON project
    Create date: FEB 2013

    Details
    -------
    This class contains methods that cover 
        - Get Record Types for Task, .. 
        - Describe schema, fields and assigned object,  
        - Append List to Map
        - Get Salesforce with current instance
        - Get Custom Settings
    
    Update History
    --------------
    Feb 2013 / IC001/ Created by Maroun Imad

    Issues / TODOs
    --------------
*/
@isTest
private class VFC_CalendarUTILS_TEST {


    static testMethod void describeMethods()
    {
        // We cannot check the actual values of field names as the config may have changed this in the workbench, but they should all return a string of some length
        //System.assert(VFC_CalendarUTILS.getFieldLabel('Account', 'DREAMID__c').length()>0); 
        System.assert(VFC_CalendarUTILS.getFieldLabel('Account', 'Name').length()>0);
        
        // This should product an empty string
        System.assert(VFC_CalendarUTILS.getFieldLabel('Product2', 'does not exit')=='');
        System.assert(VFC_CalendarUTILS.getFieldLabel('Product2', null)=='');
    }
    
    /**** Maps ****/
    static testMethod void testMapListAppend()
    {
        Map<Object,Object[]> objectMap = new Map<Object,Object[]>();
        
        // The first time add in Map the KEY and value in list 
        objectMap =  VFC_CalendarUTILS.mapListAppend(objectMap, 'KEY1', 'Value 1-1');
        objectMap =  VFC_CalendarUTILS.mapListAppend(objectMap, 'KEY2', 'Value 2-1');
        // If Key already exists in Map append to his list of values the parameter 
        objectMap =  VFC_CalendarUTILS.mapListAppend(objectMap, 'KEY1', 'Value 1-2');
        objectMap =  VFC_CalendarUTILS.mapListAppend(objectMap, 'KEY1', 'Value 1-3');
            
        Object[] theList = objectMap.get('KEY1'); 
        System.assert(objectMap.get('KEY2').size()>0); 
        System.assert(theList[0] == 'Value 1-1');
        System.assert(theList[1] == 'Value 1-2');
        System.assert(theList[2] == 'Value 1-3');
    }

    /**** Record Types ****/
    // Test the existing of record types for Exchnage Rate, Cpc Price Alert, ....
    static testMethod void testExistingRT() {
        //System.assert(!VFC_CalendarUTILS.empty(VFC_CalendarUTILS.getTaskRT()));
        //System.assert(!VFC_CalendarUTILS.empty(VFC_CalendarUTILS.getTaskDreamRT()));

    }
    
    /**** URL ****/
    static testMethod void testSfInstance() {
       String sfInst = VFC_CalendarUTILS.sfInstance;
       System.assert(sfInst != null && sfInst.length() > 0);
       String baseURL = VFC_CalendarUTILS.baseURL();
       String sfURL =  'https://' + sfInst + '.salesforce.com';
       System.assert(baseURL == sfURL);
    }
    
    /**** Lists ****/
    // Test convert List, Serialize, Deserialize, ..
    static testMethod void testList()
    {
        List<String> aList = new List<String>();
        String mutiSelectList =  '';
        
        for (Integer i=0; i < 3;  i++) {
            aList.add('value' + i);
            mutiSelectList= mutiSelectList + 'value'+i + ';';
        }
        mutiSelectList= mutiSelectList.removeEnd(';');
    
        Set<String> aSet = VFC_CalendarUTILS.toSet(aList);
        System.assert(aSet.size() == 3);
        String[] deserialize = VFC_CalendarUTILS.deserializeMultiSelect(mutiSelectList);
        System.assert(deserialize.size() == 3);
        String serializeListQotes = VFC_CalendarUTILS.serializeWithQuotes(aList);
        System.assert(serializeListQotes.length() > 0);
        String serializeSetQotes = VFC_CalendarUTILS.serializeWithQuotes(aSet);
        System.assert(serializeSetQotes.length() > 0);
        String serializeWithQotes = VFC_CalendarUTILS.serializeWithQuotes(mutiSelectList);
        System.assert(serializeWithQotes.length() > 0);
        
    }
    
    /**** Generic ****/
    // Test math, generic,  ..
    static testMethod void testGeneric()
    {
        Decimal d = 1;
        Integer i;
        System.assert(VFC_CalendarUTILS.empty(i));
        System.assert(!VFC_CalendarUTILS.empty(d));
        System.debug('*** round=' + VFC_CalendarUTILS.round(1.7,2));
    }
    
    /**** Custom Settings ****/
    // Test the existing values in custom setting  ..
    static testMethod void testExistingCS()
    {   
        Account acc = Utils_TestMethods.createAccount();
            insert acc;
        //String defaultValueTest = VFC_CalendarUTILS.getFieldDefaultValue('Picklist','AccType__c');
        Boolean accOwner = VFC_CalendarUTILS.isRecordOwner(UserInfo.getUserId());
        Boolean accRecordAccess = VFC_CalendarUTILS.hasRecordAccess(acc.Id);
        //Boolean accBT = VFC_CalendarUTILS.canTrigger('AP');
    }
    
    static testMethod void testOrder() {
        /* Lists */
        Account[] items = new Account[]{};
        items.add(new Account(Name='a'));
        items.add(new Account(Name='b'));
        items.add(new Account(Name='c'));
        VFC_CalendarUTILS.orderList(items, 'Name', 'asc');
        System.assert(items[0].Name=='a');
        VFC_CalendarUTILS.orderList(items, 'Name', 'desc');
        System.assert(items[0].Name=='c');
    }
    
    static testMethod void CalendarUTILS_testmethod() {
        //to cover the remaining methods
      
        String obj = 'Account';
        String fld = 'LastName';
        String fldEx = 'test';
        VFC_CalendarUTILS.getPickListVal(obj,fld,fldEx);
        
        Account[] items = new Account[]{};
        items.add(new Account(Name='a'));
        items.add(new Account(Name='b'));
        items.add(new Account(Name='c'));
        VFC_CalendarUTILS.getIdList(items,fld); 
        
        String url = 'testurl';
		VFC_CalendarUTILS.getURLParameters(url); 
        String url1 = 'testurltesturltesturltesturltesturl';
		VFC_CalendarUTILS.getURLParameters(url1); 
        
        Schema.SObjectField field;
        VFC_CalendarUTILS.getFieldDescribe(field);   
                
    }

}