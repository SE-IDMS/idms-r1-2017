/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 12/05/2011
    Description     : Test class for the class BUD_CloneBudgetController
                      
*/
@isTest
private class BUD_CloneBudgetController_TEST
{
    static testMethod void CloneBudgetUnitTest()
    {
        List<string> lstBudgetLineTypesAll = new List<string>();
        integer intNumBudLines = 0;
        string strUrl = '';
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tstCBud');
        
        insert runAsUser;
        System.runAs(runAsUser)
        {
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            insert testProj;         
            Budget__c testBud1 = new Budget__c(Payback__c = 0,Type__c = 'Actual',CurrencyIsoCode = 'EUR',ProjectReq__c = testProj.Id,Description__c = '1- Initial Budget');
            insert testBud1;

            PRJ_BudgetLine__c testBL1 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'Cashout CAPEX');
            insert testBL1;
            PRJ_BudgetLine__c testBL2 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'Cashout OPEX');
            insert testBL2;            
            PRJ_BudgetLine__c testBL3 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'Internal CAPEX');
            insert testBL3;            
            PRJ_BudgetLine__c testBL4 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'Internal OPEX');
            insert testBL4; 
            
            test.startTest();
                       
            PRJ_BudgetLine__c testBL5 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'Running Cost Impact');
            insert testBL5;            
            PRJ_BudgetLine__c testBL6 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'Small Enhancements');
            insert testBL6;            
            PRJ_BudgetLine__c testBL7 = Utils_TestMethods.createBudgetLine(testBud1,String.ValueOf(Integer.ValueOf(DateTime.Now().Year()) + 1),0,'test',0,'GD Costs');
            insert testBL7;
                                                             
            ApexPages.StandardController stdCtl = new ApexPages.StandardController(testBud1);
            BUD_CloneBudgetController budCtl = new BUD_CloneBudgetController(stdCtl);
            PageReference cloneBudgetPage = budCtl.CloneBudget();
            
            //for the cloned budget returned,check whether the number of budget lines has been pulled up
            Schema.DescribeFieldResult fieldResult = PRJ_BudgetLine__c.Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry ent : ple)
            {
                lstBudgetLineTypesAll.add(ent.getValue());
            }
        
            intNumBudLines = Integer.ValueOf(PRJ_Config__c.getInstance().NumberOfYearsForBudget__c * lstBudgetLineTypesAll.size());
            
            Test.setCurrentPage(cloneBudgetPage);
            strUrl = ApexPages.currentPage().getUrl().substring(1);
            List<PRJ_BudgetLine__c> lstClonedBudgetLines = [select Id from PRJ_BudgetLine__c where Budget__c =: strUrl];
            //System.AssertEquals(intNumBudLines,lstClonedBudgetLines.size());            
                        
            
        }
        test.stopTest();
                
    }
}