public with sharing class VFC_LeadsAscWithCase extends VFC_ControllerBase
{
    //Iniatialize variables
    public Case cse{get;private set;}
    public Contact con{get;private set;}
    public List<Lead> leads{get;set;}
    public List<DataTemplate__c> dataTemplates {get;set;}
    public List<SelectOption> columns{get;set;}
    public String GotoNewLeadUrl;
    //Constructor
    public VFC_LeadsAscWithCase(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC_LeadsAscWithCase******');  
        cse= (Case)controller.getRecord();
        leads = new List<Lead>();
        dataTemplates = new List<DataTemplate__c>();
        columns = new List<SelectOption>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC_LeadsAscWithCase******'); 
    }
    
      /* This method is called on the load of VFP_LeadsAscWithCase page. This method 
         queries the Converted & unconverted leads related to the corresponding contact 
         and displays the leads lists or a message if no Leads associated.
      */  
    
    public pagereference displayLeads()
    {
        System.Debug('****** Querying of Leads Begins ******'); 
        //Case caseObj =[Select id,ContactId from case where id =: cse.id ];  
        if(cse!=null)
        {
            leads = [select Tech_CampaignName__c, Timeframe__c,SubStatus__c, Status, SolutionInterest__c, PromoTitle__c, Priority__c, Name, CreatedDate, ClosedDate__c, Category__c,Owner.FirstName, Owner.LastName, OwnerID, ResponseDate__c, CampaignName__c,keycode__c, ConvertedDate, MarcomType__c from Lead where Case__c=:cse.Id order by CreatedDate desc];      
        }
       
            
       // Select l.Timeframe__c, l.SubStatus__c, l.Status, l.SolutionInterest__c, l.PromoTitle__c, l.Priority__c, l.Name, l.CreatedDate, l.ClosedDate__c, l.Category__c From Lead l
        columns.add(new SelectOption('Field1__c','Lead Name')); // First Column
        columns.add(new SelectOption('Field2__c',Schema.sObjectType.Lead.Fields.CreatedDate.label));
        columns.add(new SelectOption('Field3__c',Schema.sObjectType.Lead.Fields.Status.label));
        columns.add(new SelectOption('Field4__c',Schema.sObjectType.Lead.Fields.SubStatus__c.label));
        columns.add(new SelectOption('Field5__c',Schema.sObjectType.Lead.Fields.ClosedDate__c.label));
        columns.add(new SelectOption('Field6__c',Schema.sObjectType.Lead.Fields.Priority__c.label));
        columns.add(new SelectOption('Field7__c',Schema.sObjectType.Lead.Fields.Timeframe__c.label));
        columns.add(new SelectOption('Field8__c',Schema.sObjectType.Lead.Fields.Tech_CampaignName__c.label)); // DEF-1217
        columns.add(new SelectOption('Field9__c',Schema.sObjectType.Lead.Fields.SolutionInterest__c.label));
        columns.add(new SelectOption('Field10__c',System.Label.CLDEC12AC14)); // Last Column
       //BR-1911    Srinivas Nallapati
       
       for(Lead lead: leads)
        {
            DataTemplate__c dt = new DataTemplate__c();
            if(lead.ConvertedDate!=null){           
                dt.field1__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/apex/VFP44_LeadDetail?id='+lead.Id+'" onClick="openTab(\''+lead.Id+'\',\''+lead.Name+'\');return false">'+lead.Name+'</a>';                
            }   
            else{
                dt.field1__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/'+lead.Id+'" onClick="openTab(\''+lead.Id+'\',\''+lead.Name+'\');return false">'+lead.Name+'</a>';
            }
            
            dt.field2__c = String.valueOf(lead.CreatedDate);
            dt.field3__c = lead.Status;
            dt.field4__c = lead.SubStatus__c;
            dt.field5__c = String.valueOf(lead.ClosedDate__c);
            dt.field6__c = String.valueOf(lead.Priority__c);
            dt.field7__c = lead.Timeframe__c;
            dt.field8__c = lead.Tech_CampaignName__c;  //DEF-1217
            dt.field9__c = lead.SolutionInterest__c;
            dt.field10__c = lead.Owner.FirstName +' '+lead.Owner.LastName;  // BR-1911    Srinivas Nallapati
            
            /*string strOwnerID = lead.OwnerId; 
            if(strOwnerID.startsWith('005')) 
                dt.field8__c = lead.Owner.FirstName +' '+lead.Owner.LastName;
            else
                dt.field8__c =  mapQueueGroups.get(lead.OwnerId).name;
            */
                    
            dataTemplates.add(dt);
        }
        System.Debug('****** Querying of Leads Ends ******'); 
        return null;
        
    }
    
    //Added for BR-1858, New Lead page from the accociated leads section of Contact detail page. This method redirects the user to new lead page
    public PageReference gotoNewLead()
    {
        Cse = [Select id,CaseNumber,ContactId,CustomerRequest__c,AnswerToCustomer__c FROM Case Where id=:Cse.id];
        con= [select name,FirstName, LastName,Accountid, Account.name,Account.Country__r.Name,Account.Country__c,WorkPhone__c,MobilePhone,Email from Contact where id = :cse.contactid];
        PageReference pg = new PageReference('/00Q/e');
        pg.getParameters().put('CF00NA0000009eEic',con.Account.name);
        pg.getParameters().put('CF00NA0000009eEic_lkid',Con.Accountid);
        pg.getParameters().put('CF00NA0000009eEit',con.name);
        pg.getParameters().put('CF00NA0000009eEit_lkid',Con.id);
        pg.getParameters().put('CF00NA0000009eEiu',con.Account.Country__r.Name);
        pg.getParameters().put('CF00NA0000009eEiu_lkid',con.Account.Country__c);
        pg.getParameters().put('name_firstlea2',con.FirstName);
        pg.getParameters().put('name_lastlea2',con.LastName);
        pg.getParameters().put('lea3',con.Account.name);
        pg.getParameters().put('lea8',con.WorkPhone__c);
        pg.getParameters().put('lea9',con.MobilePhone);
        pg.getParameters().put('lea11',con.Email);
        pg.getParameters().put('00NA0000009eEjSMAU',cse.CustomerRequest__c);//Customer Request
        pg.getParameters().put('00NA0000009eEjeMAE',cse.AnswerToCustomer__c);//Suspected Solution
        pg.getParameters().put('CF00NJ00000029Gau',cse.CaseNumber);
               
        return pg;
    }
    
    // If we are in the console, return this page url
    public String getGotoNewLeadUrl() {
    
        Cse = [Select id,ContactId,CaseNumber,CustomerRequest__c,AnswerToCustomer__c FROM Case Where id=:Cse.id];
        con= [select name,FirstName, LastName,Accountid, Account.name,Account.Country__r.Name,Account.Country__c,WorkPhone__c,MobilePhone,Email from Contact where id =:cse.contactid];
        PageReference pg = new PageReference('/00Q/e');
        pg.getParameters().put(Label.CLMAR16CCC06,con.Account.name);
        pg.getParameters().put(Label.CLMAR16CCC07,Con.Accountid);
        
        pg.getParameters().put(Label.CLMAR16CCC08,con.name);
        pg.getParameters().put(Label.CLMAR16CCC09,Con.id);
        pg.getParameters().put(Label.CLMAR16CCC10,con.Account.Country__r.Name);
        pg.getParameters().put(Label.CLMAR16CCC11,con.Account.Country__c);
        pg.getParameters().put('00NA0000009yS3W','1');
        pg.getParameters().put(Label.CLMAR16CCC12,con.FirstName);
        pg.getParameters().put(Label.CLMAR16CCC13,con.LastName);
        pg.getParameters().put(Label.CLMAR16CCC14,con.Account.name);
        pg.getParameters().put(Label.CLMAR16CCC15,con.WorkPhone__c);
        pg.getParameters().put(Label.CLMAR16CCC16,con.MobilePhone);
        pg.getParameters().put(Label.CLMAR16CCC17,con.Email);
        
        pg.getParameters().put(Label.CLMAR16CCC18,cse.CustomerRequest__c);//Customer Request
        pg.getParameters().put(Label.CLMAR16CCC19,cse.AnswerToCustomer__c);//Suspected Solution
        pg.getParameters().put(Label.CLMAR16CCC20,cse.CaseNumber);
        pg.getParameters().put(Label.CLMAR16CCC21,cse.Id);
        pg.getParameters().put(Label.CLMAR16CCC22,cse.Id);
        return pg.getUrl();
    }
    
}//End of Class