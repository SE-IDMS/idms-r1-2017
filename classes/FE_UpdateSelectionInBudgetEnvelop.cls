/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/22/2012
    Description     : Controller class for the visualforce page FE_UpdateSelectionInBudgetEnvelop
                      Updates the value for the field 'Selected in Envelop?' after checking whether the logged-in user has the authority to do so.
                      
*/
public with sharing class FE_UpdateSelectionInBudgetEnvelop
{
    public ApexPages.StandardController controller;
    
    public DMTFundingEntity__c oFE{get;set;}
       
    public FE_UpdateSelectionInBudgetEnvelop(ApexPages.StandardController controller)
    {
        this.controller = controller;
        oFE = (DMTFundingEntity__c) controller.getRecord();
        mainInit(); 
    }    
    
    private void mainInit()
    {
        oFE = [Select Id,Parent__c,SelectedinEnvelop__c,ProjectReq__r.WorkType__c,ProjectReq__r.Parent__c,ProjectReq__r.NextStep__c,ProjectReq__r.BusGeographicZoneIPO__c,ProjectReq__r.OwnerId from DMTFundingEntity__c where Id = :oFE.Id];
    }
    
    public PageReference updateFundingEntity()
    {
        verifyUserCanChangeStatus();        
        return null;
    }
    
    public PageReference backToFundingEntity()
    {
        PageReference pageRef = new PageReference('/'+ oFE.Id);
        pageRef.setRedirect(true);
        return pageRef;        
    }
        
    private void verifyUserCanChangeStatus()
    {   
        /*
        Map<string,string> csMap = new Map<string,string>();
        Map<string,Id> grpMap = new Map<string,Id>();
        Map<Id,Set<ID>> grpmemberMap = new Map<Id,Set<Id>>();
                     
        //create csMap
        for(DMTAuthorization__c csAuth:DMTAuthorization__c.getAll().Values())
        {
            if(!csMap.containsKey(csAuth.BusinessOrganizationValue__c))
            {
                csMap.put(csAuth.BusinessOrganizationValue__c,csAuth.PublicGroupName__c);
                System.Debug('csMap -> Business Organization: ' + csAuth.BusinessOrganizationValue__c + ',Public Group: ' + csAuth.PublicGroupName__c);
            }
        }
         
        //create grpMap         
        for(Group grp:[Select Id,DeveloperName from Group where DeveloperName in :csMap.values()])
        {
            if(!grpMap.containsKey(grp.DeveloperName))
            {
                grpMap.put(grp.DeveloperName,grp.Id);
                System.Debug('grpMap -> Group Name: ' + grp.DeveloperName + ',Group ID: ' + grp.Id);
            }
        }
         
        //create grpmemberMap
        for(GroupMember grpmember:[Select Id,GroupId,UserOrGroupId from GroupMember where GroupId in :grpMap.values()])
        {
            Set<Id> setUserId = new Set<Id>();             
            if(grpmemberMap.containsKey(grpmember.GroupID))
            {
                setUserId = grpmembermap.get(grpmember.GroupID);
            }
            setUserId.add(grpmember.UserOrGroupId);
            grpmemberMap.put(grpmember.GroupID,setUserId);
            

        }     
        */ 
        
        //Modified for May, 2012 Release by Sreedevi Surendran (Schneider Electric)
        //begin
        //check if logged-in user can edit the status values
        System.Debug('Logged In User ID: ' + UserInfo.getUserId());
        boolean isAuthorized = false;
        boolean isDefault = false;
        System.Debug('Business Organization1: ' + oFE.ProjectReq__r.Parent__c);
        System.Debug('Status1: ' + oFE.ProjectReq__r.NextStep__c);
        System.Debug('IPO Regional Organization L11: ' + oFE.ProjectReq__r.BusGeographicZoneIPO__c);
        
        if(oFE.ProjectReq__r.WorkType__c == 'Master Project' || oFE.ProjectReq__r.WorkType__c == 'Sub Project') 
        {
            List<DMTAuthorizationMasterData__c> objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusCreated AND Parent__c =:oFE.Parent__c AND BusGeographicZoneIPO__c =:oFE.ProjectReq__r.BusGeographicZoneIPO__c limit 1];
            System.Debug('Business Organization(header): ' + oFE.Parent__c);
            System.Debug('Business Organization(funding entity): ' + oFE.ProjectReq__r.Parent__c);            
            System.Debug('Status2: ' + oFE.ProjectReq__r.NextStep__c);
            System.Debug('IPO Regional Organization L12: ' + oFE.ProjectReq__r.BusGeographicZoneIPO__c);

            Set<string> setUser = new Set<string>();
            
            if(objAuth.size() > 0)
            { 
                System.Debug('objAuth not null');
                SObject sobjectAuth =(SObject)objAuth[0];
                for(Integer i=1;i<11;i++)
                {
                    if(sobjectAuth.get('AuthorizedUser'+i+'__c') !=null)
                        setUser.add(sobjectAuth.get('AuthorizedUser'+i+'__c')+'');
                }
                if(setUser.contains(UserInfo.getUserId()))
                {
                    System.Debug('Found');
                    isAuthorized = true;
                    System.Debug('isAuthorized1: ' + isAuthorized);
                }                         
            }     
            else
            {
                System.Debug('objAuth = null');
                isDefault = true;
                System.Debug('isDefault1: ' + isDefault);
            }
        }
        else
        {
            isDefault = true;
            System.Debug('isDefault2: ' + isDefault);
        }

        System.Debug('isAuthorized2: ' + isAuthorized);
        System.Debug('isDefault3: ' + isDefault);  
          
        if(UserInfo.getProfileId()== System.Label.CL00110)
            isAuthorized = true;

        System.Debug('isAuthorized3: ' + isAuthorized);

        if(isAuthorized)
        {
            //call the method of the without sharing class   
            System.Debug('Here1'); 
            FE_UpdateSelectionInBudgetEnvelopVerify.updateFundingEntity(oFE);
        }                 
        else
        {  
            System.Debug('Here2');                                
            if(isDefault)
            {
                System.Debug('Here3');
                try
                {              
                    update oFE;
                    System.Debug('Here4');
                    if(Test.isRunningTest())
                        throw new noMessageException('Test Exception');
                }
                catch(DmlException dmlexp)
                {
                    for(integer i = 0;i<dmlexp.getNumDml();i++)
                        ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));                      
                }
                catch(Exception exp)
                {
                    ApexPages.addMessages(new noMessageException(exp.getMessage()));
                }
            }
            else
            {
                 ApexPages.addMessages(new noMessageException(System.Label.DMT_MessageNotAuthorizedToChangeStatus));          
            }            
        } 
        //end
                          
    }  
                      
    
    public class noMessageException extends Exception{}
    
}