/*
                Author          : Sreedevi Surendran (Schneider Electric)
                Date Created    : 03/08/2012
                Description     : Utils class for the class FE_FundingEntityTriggers.                      
                                  Deactivates the current active forecast funding entity record.
                                  Checks if a funding entity record with the same combination of PF-BO-SEL1-SEL2-BY-FD already exists for a project request.
                                  
            */
            public with sharing class FE_FundingEntityUtils
            {
                public static void deactivateActiveFundingEntity(List<DMTFundingEntity__c> fundingentityListToProcessCONDITION1,boolean isUpdate)
                {
                    Map<string,DMTFundingEntity__c> projectNewFundingEntityMap = new Map<string,DMTFundingEntity__c>();
                    Map<string,DMTFundingEntity__c> projectCurrFundingEntityMap = new Map<string,DMTFundingEntity__c>();
                    List<DMTFundingEntity__c> updateFundingEntityList = new List<DMTFundingEntity__c>();
                    Map<Id,string> projectRequestMap = new Map<Id,string>();
                    
                    /* Get the list (lstA) of current active forecast funding entity records for the list of project req IDs (from the fundingentityListToProcessCONDITION1 list) */
                    for(DMTFundingEntity__c oFE:fundingentityListToProcessCONDITION1)   
                    {
                        projectNewFundingEntityMap.put(oFE.ProjectReq__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c,oFE);
                        if(!projectRequestMap.containsKey(oFE.ProjectReq__c))
                        {
                            projectRequestMap.put(oFE.ProjectReq__c,null);
                            System.Debug('projectRequestMap ID: ' + oFE.ProjectReq__c);                    
                        }
                        System.Debug('projectNewFundingEntityMap PR: ' + oFE.ProjectReq__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c + ',New Funding Entity: ' + oFE.Id);
    
                    }         
                  
                    /* Loop thru the list and Make a map of all the Project IDs (as key) and current active forecast funding entity record */
                    for (DMTFundingEntity__c oFE:[select Id,ActiveForecast__c,ProjectReq__c,BudgetYear__c,ParentFamily__c,Parent__c,GeographicZoneSE__c,GeographicalSEOrganization__c,ForecastDate__c,HFMCode__c,ForecastQuarter__c,ForecastYear__c from DMTFundingEntity__c where ProjectReq__c in :projectRequestMap.keyset() and ActiveForecast__c = true])
                    {
                        if(projectNewFundingEntityMap.containsKey(oFE.ProjectReq__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c))
                        {
                            projectCurrFundingEntityMap.put(oFE.ProjectReq__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c,oFE);
                            System.Debug('projectCurrFundingEntityMap PR: ' + oFE.ProjectReq__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c + ',Curr Funding Entity: ' + oFE.Id);
                        }
                    }
    
                    /*
                    // Loop thru the map
                    //     for each current active forecast funding entity record (CAFE), set the active forecast flag = false
                    //     get the new funding entity record (NAFE) corresponding to the ProjectID from the newMap
                    // update NAFE list and CAFE list
                    */
                    for (string projIDfe:projectCurrFundingEntityMap.keyset())
                    {
                        DMTFundingEntity__c currFE = projectCurrFundingEntityMap.get(projIDfe);
                        DMTFundingEntity__c newFE = projectNewFundingEntityMap.get(projIDfe);
                        System.Debug('PR: ' + projIDfe + ',newFE:>> ' + newFE.Id);
                        System.Debug('PR: ' + projIDfe + ',currFE:>> ' + currFE.Id);
         
                        currFE.ActiveForecast__c = false;
                        System.Debug('false>>>>>>>>');
    
                       if(currFE.HFMCode__c!=newFE.HFMCode__c && currFE.ForecastQuarter__c == newFE.ForecastQuarter__c && currFE.ForecastYear__c == newFE.ForecastYear__c && currFE.BudgetYear__c == newFE.BudgetYear__c ){
                            currFE.ActiveForecast__c = true;
                            System.Debug('true>>>>>>>>');
                     }
                        updateFundingEntityList.add(currFE);
                    }
                    
                    try
                    {        
                        if(updateFundingEntityList.size() > 0)
                            update(updateFundingEntityList);
                            
                        if(Test.isRunningTest())
                            throw new TestException('Test Exception');
                    }
                    catch(Exception exp)
                    {
                        System.Debug('Update Failed');
                    }
                                    
                          
                    
                }
                
                public static void checkFundingEntityExists(List<DMTFundingEntity__c> feList)
                {
                    //check if a funding entity record with the same combinaton of PF-BO-SEL1-SEL2-BY-FD already exists
                    //map of project request record ids against funding entity records
                    Map<Id,DMTFundingEntity__c> feprjMap = new Map<Id,DMTFundingEntity__c>();
                    //Map of project ids against list of funding entity records
                    Map<Id,List<DMTFundingEntity__c>> prjfeMap = new Map<Id,List<DMTFundingEntity__c>>();
                    
                    for(DMTFundingEntity__c fe:feList)
                    {
                        if(!feprjMap.containsKey(fe.ProjectReq__c))
                            feprjMap.put(fe.ProjectReq__c,fe);
                    }
                    
                    
                    for(DMTFundingEntity__c oFE:[select Id,ProjectReq__c,BudgetYear__c,ParentFamily__c,Parent__c,GeographicZoneSE__c,GeographicalSEOrganization__c,ForecastQuarter__c,ForecastYear__c,HFMCode__c from DMTFundingEntity__c where ProjectReq__c in :feprjMap.keySet()])        
                    {
                        List<DMTFundingEntity__c> lstFe = new List<DMTFundingEntity__c>();
                        if(prjfeMap.containsKey(oFE.ProjectReq__c))
                        {
                            lstFe = prjfeMap.get(oFE.ProjectReq__c);
                        }
                        lstFe.add(oFE);
                        prjfeMap.put(oFE.ProjectReq__c,lstFe);
    
                    }
                    
                    
                    for(Id idPrj:feprjMap.keySet())
                    {
                        DMTFundingEntity__c fe = feprjMap.get(idPrj);
                        boolean isExist = false;
                        if(prjfeMap.containsKey(idPrj))
                        {
                            List<DMTFundingEntity__c> lstFeExisting = prjfeMap.get(idPrj);
                            string strLogicalKey1 = fe.ParentFamily__c + fe.Parent__c + fe.GeographicZoneSE__c + fe.GeographicalSEOrganization__c + fe.BudgetYear__c + fe.ForecastQuarter__c + fe.ForecastYear__c;
                            for(DMTFundingEntity__c feExisting:lstFeExisting)
                            {
                                string strLogicalKey2 = feExisting.ParentFamily__c + feExisting.Parent__c + feExisting.GeographicZoneSE__c + feExisting.GeographicalSEOrganization__c + feExisting.BudgetYear__c + feExisting.ForecastQuarter__c + feExisting.ForecastYear__c;
                                if(strLogicalKey2 <> null && strLogicalKey1 <> null)
                                {
                                    if(strLogicalKey1.equals(strLogicalKey2))
                                    {
                                        
                                        if(fe.HFMCode__c== null || feExisting.HFMCode__c==null || fe.HFMCode__c==feExisting.HFMCode__c){
                                        isExist = true;
                                        break;
                                        }
                                    }
                                }                    
                            }                
                        }
                        if(isExist)
                        {
                            fe.addError(System.Label.DMT_FundingEntityRecordExists);
                        }
                        
                    }
                    
                    
                }   
                
                
                public class TestException extends Exception{}
                
                // RamaKrishna Added Code for June Release Start
                
                public static void fundentityBeforeInsert(List<DMTFundingEntity__c> newFundingEntities)
                {
                    System.Debug('****** fund entity Before Insert Started ****');
                    System.Debug('****** Ram Test ****'+newFundingEntities[0].ParentFamily__c );
                    System.Debug('****** Ram Test ****'+newFundingEntities[0].Parent__c );
                    System.Debug('****** Ram Test ****'+newFundingEntities[0].GeographicZoneSE__c );
                    System.Debug('****** Ram Test ****'+newFundingEntities[0].ActiveForecast__c );
                    set<Id> prIdSet = new set<Id>();
                    list<DMTFundingEntity__c> feActualList = new list<DMTFundingEntity__c>();
                    Map<id, PRJ_ProjectReq__c> prIdMap = new Map<id, PRJ_ProjectReq__c>();
                    Set<id> existFeSet=new Set<id>();
                    list<PRJ_ProjectReq__c> ProjReqList=new list<PRJ_ProjectReq__c>();
                    
                    
                    for(DMTFundingEntity__c fund:newFundingEntities){
                        prIdSet.add(fund.ProjectReq__c);
                    }
                    if(prIdSet != null && prIdSet.size()>0){
                        ProjReqList =[select id, ParentFamily__c, Parent__c, GeographicZoneSE__c, GeographicalSEOrganization__c from PRJ_ProjectReq__c where id in: prIdSet];
                        for(DMTFundingEntity__c eFE: [select id,ProjectReq__c from DMTFundingEntity__c where ProjectReq__c in :prIdSet]){
                            existFeSet.add(eFE.ProjectReq__c);
                        }
                    }
                    if(ProjReqList != null)
                    {
                        prIdMap.putAll(ProjReqList);
                        
                        for(DMTFundingEntity__c fund:newFundingEntities){
                        
                            if(!existFeSet.contains(fund.ProjectReq__c)){
                                if(prIdMap.containsKey(fund.ProjectReq__c)){
                                    PRJ_ProjectReq__c projReq = prIdMap.get(fund.ProjectReq__c); 
                                    System.debug('asdf-------'+fund+projReq);                  
                                    //if(projReq.ParentFamily__c == fund.ParentFamily__c && projReq.Parent__c == fund.Parent__c && projReq.GeographicZoneSE__c == fund.GeographicZoneSE__c && projReq.GeographicalSEOrganization__c == fund.GeographicalSEOrganization__c && fund.ActiveForecast__c ==true){
                                      if(fund.ActiveForecast__c ==true){
                                        System.debug('asdf-------'+fund+projReq);
                                        feActualList.add(fund);
                                        //prIdActualSet.add(projReq.id);
                                    }
                                }
                            }
                            
                        }
                    }
                    if(feActualList != null && feActualList.size()>0){
                        
                        System.Debug('****** Funding Entity TotalCost Capex Started ****'); 
                        System.Debug('****** Debug  ****   '+feActualList.size()); 
                        UpdateTotalCostOnFundingEntity(feActualList);
                        
                    }
                    System.Debug('****** fund entity Before Insert End ****');
                }
                       
                public static void UpdateTotalCostOnFundingEntity(List<DMTFundingEntity__c> newFundingEntities)
                {
                    //Srikant Joshi added the code for June Release(28/06/2012) Start 
                    
                    Map<String,PRJ_BudgetLine__c> mapTypeToSobject = new Map<STring,PRJ_BudgetLine__c>();
                    
                    Map<Id,map<String,PRJ_BudgetLine__c>> mapPrToBudgetLine = new Map<Id,map<STring,PRJ_BudgetLine__c>>();
                    
                    Map<String, Decimal> isoCodeConvMap = new Map<String, Decimal>();
                    
                    List<DMT_FundingEntiity_BudgetLines__c> listLabels = DMT_FundingEntiity_BudgetLines__c.getAll().values();
                    
                    List<DMT_FundingEntiity_BudgetLines__c> listLabelYear0 = new List<DMT_FundingEntiity_BudgetLines__c>() ;
                    
                    List<DMT_FundingEntiity_BudgetLines__c> listLabelYear1 = new List<DMT_FundingEntiity_BudgetLines__c>();
                    
                    List<DMT_FundingEntiity_BudgetLines__c> listLabelYear2 = new List<DMT_FundingEntiity_BudgetLines__c>();
                    
                    List<DMT_FundingEntiity_BudgetLines__c> listLabelYear3 = new List<DMT_FundingEntiity_BudgetLines__c>();
                    
                    List<Id> listProjReqs = new List<Id>();
    
                    for(DMTFundingEntity__c fe : newFundingEntities){
                        listProjReqs.add(fe.ProjectReq__c);
                    }
    
                    for(PRJ_BudgetLine__c budgetLine : [select id,Budget__r.Active__c,CurrencyIsoCode,Amount__c,Type__c,Year__c,Budget__r.ProjectReq__c from PRJ_BudgetLine__c where Budget__r.ProjectReq__c in :listProjReqs ]){
                        if(budgetLine.Budget__r.Active__c){ // Only if the Costs Forecasts record is active.
                        mapTypeToSobject.put(budgetLine.Type__c+budgetLine.Year__c,budgetLine); // Ex:- mapTypeToSobject.put(Cashout Running Costs Impact+2011,Budget Line Record)
                        mapPrToBudgetLine.put(budgetLine.Budget__r.ProjectReq__c,mapTypeToSobject);
                        }
                    }
                            
                    for(DMT_FundingEntiity_BudgetLines__c listlabel :listLabels  ){
                        if(listlabel.name.contains('0'))
                            listLabelYear0.add(listlabel);//For all DMT Funding Entity fields in Custom Setting containing '0' Ex:- CAPEXY0__c.
                        if(listlabel.name.contains('1'))
                            listLabelYear1.add(listlabel);//For all DMT Funding Entity fields in Custom Setting containing '1' Ex:- CAPEXY1__c.
                        if(listlabel.name.contains('2'))
                            listLabelYear2.add(listlabel);//For all DMT Funding Entity fields in Custom Setting containing '2' Ex:- CAPEXY2__c.
                        if(listlabel.name.contains('3'))
                            listLabelYear3.add(listlabel);//For all DMT Funding Entity fields in Custom Setting containing '3' Ex:- CAPEXY3__c.
                    }
                    
    
                    for(DatedConversionRate dtCRList : [SELECT ConversionRate,IsoCode FROM DatedConversionRate where startDate<:System.Today().toStartOfMonth() and NextStartDate>=:System.Today()]){
                        isoCodeConvMap.put(dtCRList.IsoCode,dtCRList.ConversionRate);
                    }
                    system.debug('@@@@@@@@@@@12'+newFundingEntities.size());
                    for(DMTFundingEntity__c fe : newFundingEntities){
                        system.debug('**************'+fe.id);
                        system.debug('@@@@@@@@@@@12'+mapPrToBudgetLine.get(fe.ProjectReq__c));
                        if(mapPrToBudgetLine.get(fe.ProjectReq__c)!=null){
                        
                            Integer budgetYear = Integer.Valueof(fe.BudgetYear__c);
                            Decimal conversionFundEntity = isoCodeConvMap.get(fe.CurrencyIsoCode);
                            system.debug('@@@@@@@@@@@@@@@11'+listLabelYear0.size());
                            system.debug('@@@@@@@@@@@@@@@11'+listLabelYear0);
                            For(DMT_FundingEntiity_BudgetLines__c DMTlabel : listLabelYear0){
                                system.debug('@@@@@@@@@@@@@@@'+DMTlabel);
                                Decimal decamount0 ;
                                for(String budLineType : DMTlabel.Budget_Line__c.split(',')){// Iterate the Custom Setting field for each Value separated by comma, Ex:-Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services
                                system.debug('*************1'+budLineType);
                                system.debug('*************1'+budgetYear);
                                    if((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear))!=null){
                                        system.debug('*************2'+budLineType);
                                        system.debug('*************2'+budgetYear);
                                        if(decamount0==null){
                                        system.debug('*************3'+budLineType);
                                        system.debug('*************3'+budgetYear);
                                            decamount0 = ((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear)).CurrencyIsoCode));
                                        }
                                        else{
                                            system.debug('*************4'+budLineType);
                                            system.debug('*************4'+budgetYear);
                                            decamount0 = (decamount0+(mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear)).CurrencyIsoCode));
                                            }
                                    }
                                }
                                if(decamount0 !=null)
                                    fe.put(DMTlabel.Funding_Entity__c,decamount0); 
                            }
                            
                            For(DMT_FundingEntiity_BudgetLines__c DMTlabel : listLabelYear1){
                                Decimal decamount1 ;
                                for(String budLineType : DMTlabel.Budget_Line__c.split(',')){// Iterate the Custom Setting field for each Value separated by comma, Ex:-Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services
                                   if((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+1))!=null){
                                        if(decamount1==null)
                                            decamount1 = ((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+1)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+1)).CurrencyIsoCode));
                                        else
                                            decamount1 = (decamount1+(mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+1)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+1)).CurrencyIsoCode));
                                    }
                                }
                                if(decamount1!=null)
                                    fe.put(DMTlabel.Funding_Entity__c,decamount1); 
                            }
                   
                           For(DMT_FundingEntiity_BudgetLines__c DMTlabel : listLabelYear2){
                                Decimal decamount2 ;
                                for(String budLineType : DMTlabel.Budget_Line__c.split(',')){// Iterate the Custom Setting field for each Value separated by comma, Ex:-Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services
                                   if((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+2))!=null){
                                        if(decamount2==null)
                                            decamount2 = ((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+2)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+2)).CurrencyIsoCode));
                                        else
                                            decamount2 = (decamount2+(mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+2)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+2)).CurrencyIsoCode));
                                   }
                                }
                                if(decamount2!=null)
                                    fe.put(DMTlabel.Funding_Entity__c,decamount2); 
                           } 
                           For(DMT_FundingEntiity_BudgetLines__c DMTlabel : listLabelYear3){
                                Decimal decamount3 ;
                                for(String budLineType : DMTlabel.Budget_Line__c.split(',')){// Iterate the Custom Setting field for each Value separated by comma, Ex:-Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services
                                   if((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+3))!=null){
                                        if(decamount3==null)
                                            decamount3 = ((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+3)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+3)).CurrencyIsoCode));
                                        else
                                            decamount3 = (decamount3+(mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+3)).Amount__c)*(conversionFundEntity/isoCodeConvMap.get((mapPrToBudgetLine.get(fe.ProjectReq__c)).get(budLineType+String.Valueof(budgetYear+3)).CurrencyIsoCode));
                                   }
                                }
                                if(decamount3!=null)
                                    fe.put(DMTlabel.Funding_Entity__c,decamount3); 
                           }               
                        }
                    }
                }
                //Srikant Joshi added the code for June Release(28/06/2012) End
          
                // RamaKrishna Added Code for June Release End
                
                //This is added by Srikant for July Release Start
            public static void updateProjectRequest(List<DMTFundingEntity__c> fundingentityList){
                Try{            
                    List<Id> listProjRequestId = new List<Id>{};
                    
                    List<DMTFundingEntity__c> ListFeWithActiveSelected = new List<DMTFundingEntity__c>{};
                    
                    List<PRJ_ProjectReq__c> listProjReq  = new List<PRJ_ProjectReq__c>{};
    
                    Map<Id,List<DMTFundingEntity__c>> mapProjReqToFeWithActiveSelected = new Map<Id,List<DMTFundingEntity__c>>(); 
                    
                    Map<Id,PRJ_ProjectReq__c> mapIdTOProjReq = new Map<Id,PRJ_ProjectReq__c>();
                    
                    Map<Id,Id> maplistProjReq = new Map<Id,Id>(); 
                    
                    for(DMTFundingEntity__c fe : fundingentityList){
                        listProjRequestId.add(fe.ProjectReq__c);
                    }
                    
                    for(PRJ_ProjectReq__c ProReq : [select id,CurrencyIsoCode,(select id,CurrencyIsoCode,ProjectReq__c,TotalFY0__c,TotalFY1__c,TotalFY2__c,TotalFY3__c,BudgetYear__c from DMT_Funding_Entities__r Where ProjectReq__c in : listProjRequestId and ActiveForecast__c = True and SelectedinEnvelop__c = True and id not in :fundingentityList)from PRJ_ProjectReq__c ]){
                        mapProjReqToFeWithActiveSelected.put(ProReq.Id,ProReq.DMT_Funding_Entities__r); 
                        mapIdTOProjReq.put(ProReq.Id,ProReq);
                    }
                    
                    List<DatedConversionRate> dtCRList= [SELECT ConversionRate,IsoCode FROM DatedConversionRate where startDate<=:System.Today().toStartOfMonth() and NextStartDate>=:System.Today()];
     
                    
                    for(DMTFundingEntity__c fentity : fundingentityList){
                        
                        List<DMTFundingEntity__c> listFentityWithActiveSelected  = new List<DMTFundingEntity__c>{};
                        
                        PRJ_ProjectReq__c projReq =mapIdTOProjReq.get(fentity.ProjectReq__c);
                        listFentityWithActiveSelected =  mapProjReqToFeWithActiveSelected.get(fentity.ProjectReq__c);
                        if(fentity.ActiveForecast__c == True && fentity.SelectedinEnvelop__c == True)
                        listFentityWithActiveSelected.add(fentity);
                        projReq.TotalCostsFunded2012__c = 0.0;
                        projReq.TotalCostsFunded2013__c = 0.0;
                        projReq.TotalCostsFunded2014__c = 0.0;
                        projReq.TotalCostsFunded2015__c = 0.0;
                        projReq.TotalCostsFunded2016__c = 0.0;
                        projReq.TotalCostsFunded2017__c = 0.0;
                        Map<String, Decimal> isoCodeConvMap = new Map<String, Decimal>();
                        if(dtCRList!= null && dtCRList.size()>0){
                           for(DatedConversionRate dcr:dtCRList){
                            if(!isoCodeConvMap.containsKey(dcr.IsoCode))
                              {
                                if(dcr.ConversionRate !=null)
                                isoCodeConvMap.put(dcr.IsoCode,dcr.ConversionRate);
                              }
                           }
    }
                        if(ListFentityWithActiveSelected != null){
                            for(DMTFundingEntity__c fe:ListFentityWithActiveSelected){
                                String feIso=FE.CurrencyIsoCode;
                                String PrIso=projReq.CurrencyIsoCode;
                                Decimal decCurIso;
                                if(isoCodeConvMap.get(PRIso)!=null && isoCodeConvMap.get(FeIso)!=null ){
                                    decCurIso=((isoCodeConvMap.get(PRIso))/isoCodeConvMap.get(FeIso));
                                }else{
                                    decCurIso=1;
                                }
                                //System.debug('--------------Curr------'+feIso+PrIso+(isoCodeConvMap.get(feIso))+(isoCodeConvMap.get(PrIso))+'----'+decCurIso);
                                if(fe.BudgetYear__c =='2011' ){
                                    
                                    //projReq.TotalCostsFunded2011__c = projReq.TotalCostsFunded2011__c+(fe.TotalFY0__c)*decCurIso;
                                    projReq.TotalCostsFunded2012__c = projReq.TotalCostsFunded2012__c+(fe.TotalFY1__c)*decCurIso;
                                    projReq.TotalCostsFunded2013__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY2__c)*decCurIso;
                                    projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY3__c)*decCurIso;
                                }
                                if(fe.BudgetYear__c =='2012' ){
                                    
                                    projReq.TotalCostsFunded2012__c = projReq.TotalCostsFunded2012__c+(fe.TotalFY0__c)*decCurIso;
                                    projReq.TotalCostsFunded2013__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY1__c)*decCurIso;
                                    projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2014__c+(fe.TotalFY2__c)*decCurIso;
                                    projReq.TotalCostsFunded2015__c = projReq.TotalCostsFunded2015__c+(fe.TotalFY3__c)*decCurIso;
                                    //System.debug('---------*****'+projReq.TotalCostsFunded2012__c+projReq.TotalCostsFunded2013__c);
                                }
                                if(fe.BudgetYear__c =='2013' ){
                                    system.debug('-------Hello'+projReq.TotalCostsFunded2012__c+'----'+fe.TotalFY1__c+projReq.TotalCostsFunded2014__c+'-----'+fe.Id);
                                    projReq.TotalCostsFunded2013__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY0__c)*decCurIso;
                                    projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2014__c+(fe.TotalFY1__c)*decCurIso;
                                    //system.debug('-------Hello'+projReq.TotalCostsFunded2014__c+'----'+(fe.TotalFY1__c);
                                    projReq.TotalCostsFunded2015__c = projReq.TotalCostsFunded2015__c+(fe.TotalFY2__c)*decCurIso;
                                    projReq.TotalCostsFunded2016__c = projReq.TotalCostsFunded2016__c+(fe.TotalFY3__c)*decCurIso;
                                }
                                
                                if(fe.BudgetYear__c =='2014' ){
                                    system.debug('-------Hello'+projReq.TotalCostsFunded2012__c+'----'+projReq.TotalCostsFunded2014__c+'-----'+fe.Id);
                                    projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2014__c+(fe.TotalFY0__c)*decCurIso;
                                    system.debug('-------Hello'+projReq.TotalCostsFunded2014__c+'----'+fe.TotalFY1__c);
                                    projReq.TotalCostsFunded2015__c = projReq.TotalCostsFunded2015__c+(fe.TotalFY1__c)*decCurIso;
                                    projReq.TotalCostsFunded2016__c = projReq.TotalCostsFunded2016__c+(fe.TotalFY2__c)*decCurIso;
                                    projReq.TotalCostsFunded2017__c = projReq.TotalCostsFunded2017__c+(fe.TotalFY3__c)*decCurIso;
                                }
                            } 
                        }
                        if(!maplistProjReq.containsKey(projReq.Id)){
                            listProjReq.add(projReq);
                            maplistProjReq.put(projReq.Id,projReq.Id);
                        }
                    }
                    try {
                            update listProjReq;
                    } 
                    catch (DmlException e){
                    }      
                }
                catch (DmlException e){
                }
            //This is added by Srikant for July Release End
            }
            //This is added by Srikant for Dec Release Start
            //Calculate P&L if the Value is not changed and NOT null OR NOT 0 OR Calculating fields are changed.
            //Commented for July 2013 release by Priyanka Shetty -- START
            /*
            public static void updatePLFields(Map<Id,DMTFundingEntity__c> newMap,Map<Id,DMTFundingEntity__c> oldMap){
            
                Try{        
                    for(DMTFundingEntity__c fe : newMap.Values()){
                        for(DMT_FundingEntiity_BudgetLines__c csInstance : DMT_FundingEntiity_BudgetLines__c.getAll().values() ){
                            if(fe.get(csInstance.name) == null)
                                fe.put(csInstance.name,0);
                        }
                        if((newMap.get(fe.id).PLFundEstimatedY0__c==oldMap.get(fe.id).PLFundEstimatedY0__c && 
                           (newMap.get(fe.id).OPEXOTCY0__c!=oldMap.get(fe.id).OPEXOTCY0__c ||
                            newMap.get(fe.id).RunningCostsImpactY0__c!=oldMap.get(fe.id).RunningCostsImpactY0__c ||
                            newMap.get(fe.id).SmallEnhancementsY0__c!=oldMap.get(fe.id).SmallEnhancementsY0__c  )) || fe.PLFundEstimatedY0__c==null)
                                fe.PLFundEstimatedY0__c = fe.OPEXOTCY0__c + fe.RunningCostsImpactY0__c + fe.SmallEnhancementsY0__c;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                        if((newMap.get(fe.id).PLFundEstimatedY1__c==oldMap.get(fe.id).PLFundEstimatedY1__c &&
                            (newMap.get(fe.id).CAPEXY0__c!=oldMap.get(fe.id).CAPEXY0__c ||
                            newMap.get(fe.id).OPEXOTCY1__c!=oldMap.get(fe.id).OPEXOTCY1__c ||
                            newMap.get(fe.id).SmallEnhancementsY1__c!=oldMap.get(fe.id).SmallEnhancementsY1__c ||
                            newMap.get(fe.id).RunningCostsImpactY1__c!=oldMap.get(fe.id).RunningCostsImpactY1__c  )) || fe.PLFundEstimatedY1__c==null)
                                fe.PLFundEstimatedY1__c = fe.OPEXOTCY1__c + fe.RunningCostsImpactY1__c + fe.SmallEnhancementsY1__c + fe.CAPEXY0__c/3;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                        if((newMap.get(fe.id).PLFundEstimatedY2__c==oldMap.get(fe.id).PLFundEstimatedY2__c &&
                            (newMap.get(fe.id).CAPEXY0__c!=oldMap.get(fe.id).CAPEXY0__c ||
                            newMap.get(fe.id).CAPEXY1__c!=oldMap.get(fe.id).CAPEXY1__c ||
                            newMap.get(fe.id).OPEXOTCY2__c!=oldMap.get(fe.id).OPEXOTCY2__c ||
                            newMap.get(fe.id).SmallEnhancementsY2__c!=oldMap.get(fe.id).SmallEnhancementsY2__c ||
                            newMap.get(fe.id).RunningCostsImpactY2__c!=oldMap.get(fe.id).RunningCostsImpactY2__c  )) || fe.PLFundEstimatedY2__c==null)
                                fe.PLFundEstimatedY2__c = fe.OPEXOTCY2__c + fe.RunningCostsImpactY2__c + fe.SmallEnhancementsY2__c + fe.CAPEXY0__c/3 + fe.CAPEXY1__c/3;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                        if((newMap.get(fe.id).PLFundEstimatedY3__c==oldMap.get(fe.id).PLFundEstimatedY3__c &&
                            (newMap.get(fe.id).CAPEXY0__c!=oldMap.get(fe.id).CAPEXY0__c ||
                            newMap.get(fe.id).CAPEXY2__c!=oldMap.get(fe.id).CAPEXY2__c ||
                            newMap.get(fe.id).CAPEXY1__c!=oldMap.get(fe.id).CAPEXY1__c ||
                            newMap.get(fe.id).OPEXOTCY3__c!=oldMap.get(fe.id).OPEXOTCY3__c ||
                            newMap.get(fe.id).SmallEnhancementsY3__c!=oldMap.get(fe.id).SmallEnhancementsY3__c ||
                            newMap.get(fe.id).RunningCostsImpactY3__c!=oldMap.get(fe.id).RunningCostsImpactY3__c  )) || fe.PLFundEstimatedY3__c==null)
                                fe.PLFundEstimatedY3__c = fe.OPEXOTCY3__c + fe.RunningCostsImpactY3__c + fe.SmallEnhancementsY3__c + + fe.CAPEXY0__c/3 + fe.CAPEXY1__c/3 + fe.CAPEXY2__c/3;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                    }
                }
                Catch(Exception e){
                    System.debug('------'+e);
                }   
            
            }*/
            //END
        }