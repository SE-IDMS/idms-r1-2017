/**
 * This class contains unit tests for validating the behavior of Apex controller VFC03_OpptySearch.
 *
 * ~ Created by Adrian MODOLEA / Mohamed EL MOUSSAOUI
 * ~ SCHNEIDER SR.0 - RELEASE - November 4th 2010
 * 
**/
/*    Modified By ACCENTURE IDC
      Modifed for December Release Business Requirement BR-923
      Modified Date: 16/11/2011
*/

@isTest
private class VFC03_OpptySearch_TEST {

    static testMethod void testVFC03_OpptySearch() {
        System.debug('#### START test method for VFC03_OpptySearch ####');
        // create unit country
     Map<String,ID> profiles = new Map<String,ID>();
         List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
         for(Profile p : ps)
          {
             profiles.put(p.name, p.id);
          }     
    user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null and BypassVR__c = true limit 1];  
   
    system.runas(admin){
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User testU = Utils_TestMethods.createStandardUser('testU');
        testU.Email = 'testUser@schneider-electric.com';
        insert testU ;

        Account testAccount = Utils_TestMethods.createAccount(testU.Id);
        testAccount.Country__c=country.Id; //17th Apr 2014
        insert testAccount ;
        
        Opportunity testOppty=Utils_TestMethods.createOpportunity(testAccount.Id);
        insert testOppty;
        testOppty.CountryOfDestination__c = country.Id;
        update testOppty;
        
       /* Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
        testContact.Country__c = country.id; //17th-Apr-2014
       */
        
        Contact testContact = new Contact(
            FirstName='Test',
            LastName='testContact',
            AccountId=testAccount.Id,
            JobTitle__c='Z3',
            CorrespLang__c='EN',
            WorkPhone__c='1234567890',
            Country__c=country.Id
            );
         insert testContact;
        
        Case testCase = Utils_TestMethods.createCase(testAccount.Id, testContact.Id,'Open');
        insert testCase;
        testCase.PromotionSource__c = 'Advertisement';
        Update testCase;
        
        // start test
        PageReference vfPage = Page.VFP03_OpptySearch;
        Test.setCurrentPage(vfPage);
        
       // Add parameters to page URL  
        ApexPages.currentPage().getParameters().put('retURL', '/006');
        ApexPages.currentPage().getParameters().put('save_new', '1');   
                  
        Opportunity opportunity = new Opportunity();
        VFC03_OpptySearch vfCtrl = new VFC03_OpptySearch(new ApexPages.StandardController(opportunity));
        //Start of December Release Modification
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VCC06_DisplaySearchResults componentcontroller1 =vfCtrl.resultsController; 
        Test.startTest();
        //End of December Release Modification
        System.debug('vfCtrl.hasNext: ' + vfCtrl.hasNext);
        
        // 1/ test methods without setting the account fields
        vfCtrl.doSearchOpportunity();
        
        // 2/ test methods with all account fields
        opportunity.Name = 'Test Opp';
        //opportunity.Location__c = 'location';
        opportunity.CountryOfDestination__c = country.Id;
        opportunity.AccountID = testAccount.Id;
        vfCtrl.getQueryString(); // new for December Release 
        vfCtrl.doSearchOpportunity();
        // OpportunityRegistrationForm__c orf = Utils_TestMethods.CreateORF(country.Id);
       // insert orf;
       //===========================
       opportunity.Name = 'Test Opp for VFC03';
       opportunity.CountryOfDestination__c = country.Id;
       opportunity.AccountID = testAccount.Id;
       vfCtrl.getQueryString(); // new for December Release 
       vfCtrl.doSearchOpportunity();
       Account accountObj1 = Utils_TestMethods.createAccount();
       insert accountObj1;
        
    
   //PartnerProgram__c prog = new PartnerProgram__c( Name = 'Test Global Prg');
   //insert prog;
    //Country__c TestCountry= Utils_TestMethods.createCountry();
    //insert TestCountry;
    PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
    globalProgram.TECH_CountriesId__c = country.id;
    globalPRogram.recordtypeid = Label.CLMAY13PRM15;
    globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
    insert globalProgram;
    
    PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country.Id);
    insert countryProgram; 
    list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
    countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
    
    if(!countryPartnerProgram.isEmpty())
    {
    countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
    update countryPartnerProgram;
    }
   
   //user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];
  
   //system.runas(admin){
    Id profilesId = [select id from profile where name='SE - Channel Partner'].Id;
    //  Id profilesId = [select id from profile where name='SE - Sales Standard User'].Id;     
    Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
    insert PartnerAcc;
    
    Contact PartnerCon = new Contact(
            FirstName='Test',
            LastName='lastname',
            AccountId=PartnerAcc.Id,
            JobTitle__c='Z3',
            CorrespLang__c='EN',
            WorkPhone__c='1234567890',
            Country__c=country.Id
            );
            
            Insert PartnerCon;
    
   //PartnerProgram__c prog = new PartnerProgram__c();
   //prog.name = 'Test Prog';   
   //insert prog;
      
   PermissionSet orfPermissionSet = [Select ID,UserLicenseId from PermissionSet where Name='PRMRegularORF' Limit 1];
   User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                    CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                    Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
   insert u1;  

    //Assigning PRMORFConverter permission set to the user
                 
         PermissionSetAssignment newPermissionSetAssignment1 = new PermissionSetAssignment();
         newPermissionSetAssignment1.AssigneeId = u1.id;
         newPermissionSetAssignment1.PermissionSetId = orfPermissionSet.id;
         Insert newPermissionSetAssignment1; 
    //End


   PartnerAcc.TECH_AccountOwner__c=u1.Id;
   update PartnerAcc;
   Test.stopTest();
   system.runas(u1)
   {
      
   
   
        OpportunityRegistrationForm__c orf = new OpportunityRegistrationForm__c( Status__c='Pending', 
                                          AccountName__c= accountObj1.Name,
                                          AccountCountry__c = accountObj1.Country__c,
                                          Amount__c = 100000,
                                          ContactEmail__c='Test@test.com',
                                          OpportunityType__c='Standard',
                                          OpportunityScope__c='ENMVP - SECONDARY MV PRODUCTS',
                                          ExpectedOrderDate__c=System.Today(),
                                          ContactFirstName__c='Test',
                                          ContactLastName__c='User',
                                          ProjectName__c='Test Project',
                                          ProductType__c = 'ESSMS',
                                          PartnerProgram__c = countryPartnerProgram[0].id,
                                          Tech_CheckORFStatusOnSave__c='True');
    Insert orf;    
     ApexPages.currentPage().getParameters().put('oppRegFormId', orf.id); 
        ApexPages.currentPage().getParameters().put('PreviousPageURL','/apex/VFP02_AccountSearch?oppRegFormId='+ orf.id + '&Accname=' + orf.AccountName__c + '&AccCountry=' + orf.AccountCountry__c);
    }   
  // }    
       //========================
       
        sObject opp1 = new DataTemplate__c (field11__c ='test op',field16__c=testOppty.id);
        vfCtrl.PerformAction(opp1,vfCtrl.getThis());
        vfCtrl.doCancel();
                
        // Update in Test Class for September Release changes
        vfCtrl.goToNextStep();
        VCC05_DisplaySearchFields searchController = new VCC05_DisplaySearchFields();
        searchController.pickListType = 'OPP_REC';
        searchController.key = 'searchComponent';
        searchController.PageController = vfCtrl;
        searchController.init();
        searchController.level1 = System.Label.CL00355 ;
        searchController.level2 = System.Label.CL00355 ;
        vfCtrl.continueCreation();
        searchController.level1 = System.Label.CL00241;
        searchController.level2 = System.Label.CL00146;
        
        Opportunity opp = vfCtrl.getOpp();
        
        vfCtrl.continueCreation();
        
        //APRIL RELEASE 2012 CHANGES
        searchController.level1 = System.Label.CL00724;
        opp = vfCtrl.getOpp();        
        vfCtrl.continueCreation();
        
        searchController.level2 = System.Label.CL00147;
        vfCtrl.continueCreation();
        searchController.level2 = System.Label.CL00547;
        vfCtrl.continueCreation();
        vfCtrl.continueCreation();
        searchController.level1 = System.Label.CL00240;
        searchController.level2 = System.Label.CL00146;
        vfCtrl.continueCreation();
        searchController.level2 = System.Label.CL00147;
        //vfCtrl.continueCreation();
        
        vfCtrl.CaseID = testCase.Id;
        opp.accountID = testAccount.id;
        vfCtrl.cse=testCase;
       // vfCtrl.goToNextStep();
                

        //vfCtrl.continueCreation();
        
        System.debug('#### END test method for VFC03_OpptySearch ####');
    }
}
}