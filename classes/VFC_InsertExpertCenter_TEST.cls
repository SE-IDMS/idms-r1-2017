@isTest
private class VFC_InsertExpertCenter_TEST {

static testMethod void myUnitTest() {

    // Create Country
    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    // Create User
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    // Create Account
    Account accountObj = Utils_TestMethods.createAccount();
    //accountObj.Country__c=CountryObj.Id; 
    insert accountObj;
    
    // Create Contact to the Account
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    // Create Case
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.Quantity__c = 4;
    CaseObj.Family__c = 'ADVANCED PANEL';
    CaseObj.CommercialReference__c = 'xbtg5230';
    CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
    CaseObj.TECH_GMRCode__c='02BF6DRG16N';
    CaseObj.ProductLine__c = 'product line';
    Insert CaseObj;    
    
    // Create ROC
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             AssessmentType__c = 'Product Return to Expert Center',
                                             ProductDestinationCapability__c = 'Technical Expert Assessment - In-House'
                                             );
    Insert ROC;

    // Create Organization (Expert Center)
     BusinessRiskEscalationEntity__c RCorgEntity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
     Insert RCorgEntity; 
     BusinessRiskEscalationEntity__c RCorgSubEntity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
     Insert RCorgSubEntity; 
     BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg; 

     // Create Organization (Expert Center)
     BusinessRiskEscalationEntity__c RCorg1Entity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST1',
                                            Entity__c='RC Entity-2',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
     Insert RCorg1Entity ;
     BusinessRiskEscalationEntity__c RCorg1SubEntity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST1',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
     Insert RCorg1SubEntity ;
     BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST1',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg1 ; 


     RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Technical Expert Assessment - In-House',
                                 CommercialReference__c='xbtg5230',
                                 //CommercialReference__c='',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT;

    RMAShippingToAdmin__c RPDT1 = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Technical Expert Assessment - In-House',
                                 CommercialReference__c='xbtg5230',
                                 //CommercialReference__c='',
                                 ReturnCenter__c = RCorg1.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT1;
    
    List<RMAShippingToAdmin__c> lstRPDT = new List<RMAShippingToAdmin__c>();
    lstRPDT.add(RPDT);
    lstRPDT.add(RPDT1);
    
    ProductLineQualityContactMapping__c PLIS = new ProductLineQualityContactMapping__c();
    OPP_Product__c oppproduct = new OPP_Product__c(
                                Name =  'oppproduct',
                                BusinessUnit__c = 'business unit',
                                ProductLine__c = 'product line',
                                Family__c = 'ADVANCED PANEL',
                                ProductFamily__c='HMI SCHNEIDER BRAND'
    );
    insert oppproduct;
    PLIS.Product__c = oppproduct.Id;
    PLIS.AccountableOrganization__c = RCorg1SubEntity.Id;
    PLIS.QualityContact__c = RRTestUser.Id;
    insert PLIS;
    ProductLineQualityContactMapping__c PLIS1 = new ProductLineQualityContactMapping__c();
    PLIS1.Product__c = oppproduct.Id;
    PLIS1.AccountableOrganization__c = RCorg1SubEntity.Id;
    PLIS1.QualityContact__c = RRTestUser.Id;
    insert PLIS1;
    
    System.debug('!!!! PLIS ids : ' + PLIS.Id + ' ' + PLIS1.Id);

    TEX__c TEXObj = new TEX__c (
                    case__c = CaseObj.Id,
                    ExpertCenter__c = RCorg.Id,
                    FrontOfficeOrganization__c = RCOrgSubEntity.Id,
                    AffectedOffer__c = 'Affected Offer',
                    AssessmentType__c = 'Product Return to Expert Center',
                    Status__c = 'Expert Assessment to be started',
                    FrontOfficeTEXManager__c = RRTestUser.Id,
                    ForceManualSelection__c=True
                    );
    Insert TEXObj;
    
    System.debug('## TEXObj' + TEXObj);
        
    Test.SetCurrentPageReference(New PageReference('Page.VFC_InsertExpertCenter'));
    System.CurrentPageReference().getParameters().put('newid',TEXObj.Id);
    ApexPages.Standardcontroller SC = New ApexPages.StandardController(TEXObj);
    
    VFC_InsertExpertCenter IEC = new VFC_InsertExpertCenter(SC);
    IEC.doSearchExpertCenters();

    // To check Multiple Centers
     sObject newTEX = new DataTemplate__c (
                    
                    field1__c = 'Technical Expert Assessment - In-House',
                    field3__c = 'RC Street',
                    field4__c = 'RC City',
                    field5__c = '1234567',
                    field6__c = CountryObj.Id,
                    field7__c = CountryObj.Id,
                    field8__c = RPDT1.ReturnCenter__c,
                    field9__c = 'test@schneider-electric.com'            
     );
     IEC.PerformAction(newTEX,IEC.getThis());

    // To call Save Method
    TEXObj.ForceManualSelection__c=True;
    TEXObj.ExpertCenter__c=RCorg1.Id;

    ApexPages.Standardcontroller SC1 = New ApexPages.StandardController(TEXObj);
    System.CurrentPageReference().getParameters().put('newid',TEXObj.Id);
    VFC_InsertExpertCenter IEC1 = new VFC_InsertExpertCenter(SC1);
    
    IEC1.doSave();
    IEC1.doCancel();
    IEC1.displayExpertCenters(lstRPDT);
    System.assertEquals(IEC1.hasNext,false);
    String sList = 'Receive defective product';
    //IEC1.getAllECs(TEXObj,sList);
}
    
}