public class VFC_GenerateXml {
    private final release__c parOrd;
    public String compXMLResult {get;set;}   

    Map<string,string> CompMap = new Map<String,String>();
    
    public List<SelectOption> TrackOptions{get;set;}
    public string selectedvalue{get;set;}

       
    public VFC_GenerateXml(ApexPages.StandardController controller) {
    CompMap.put('Account Owner Sharing Rule','AccountOwnerSharingRule');
    CompMap.put('Account Criteria Based Sharing Rule','AccountCriteriaBasedSharingRule');
    //CompMap.put('Analytic Snapshot','ReportJob');
    CompMap.put('Analytic Snapshot','AnalyticSnapshot');
    CompMap.put('Account Owner Sharing Rule','AccountOwnerSharingRule');
    CompMap.put('Apex Class','ApexClass');
    //CompMap.put('Apex Sharing Reason','CustomShareRowCause');
    CompMap.put('Apex Sharing Reason','SharingReason');
    CompMap.put('Apex Trigger','ApexTrigger');
    //CompMap.put('App','TabSet');
    CompMap.put('App','CustomApplication');
    CompMap.put('Button or Link','WebLink');
    CompMap.put('Campaign Criteria Based Sharing Rule','CampaignCriteriaSharingRule');
    CompMap.put('Campaign Owner Sharing Rule','CampaignOwnerSharingRule');
    CompMap.put('Case Criteria Based Sharing Rule','CaseCriteriaSharingRule');
    CompMap.put('Case Owner Sharing Rule','CaseOwnerSharingRule');
    CompMap.put('Contact Criteria Based Sharing Rule','ContactCriteriaSharingRule');
    CompMap.put('Contact Owner Sharing Rule','ContactOwnerSharingRule');
    CompMap.put('Custom Data Type','CustomDataType');
    CompMap.put('Profile','Profile');
    CompMap.put('Custom Field','CustomField');
    //CompMap.put('Custom Label','ExternalString');
    CompMap.put('Custom Label','CustomLabel');
    //CompMap.put('Custom Object','CustomEntityDefinition');
    CompMap.put('Custom Object','CustomObject');
    //CompMap.put('Custom Object Criteria Based Sharing Rule','CustomObjectCriteriaSharingRule'); 
    CompMap.put('Custom Object Criteria Based Sharing Rule','SharingCriteriaRule');
    //CompMap.put('Custom Object Owner Sharing Rule','CustomObjectOwnerSharingRule');
    CompMap.put('Custom Object Owner Sharing Rule','SharingOwnerRule');
    //CompMap.put('Custom Report Type','CustomReportType');
    CompMap.put('CustomPageWebLink','CustomPageWebLink'); // added for DEC 2014 HotFIX
    CompMap.put('Custom Report Type','ReportType');
    CompMap.put('Custom Setting','CustomObject');
    CompMap.put('Dashboard','Dashboard');
    CompMap.put('Document','Document');
    CompMap.put('Email Template','EmailTemplate');
    CompMap.put('Field Set','FieldSet');
    //CompMap.put('Flow','InteractionDefinition'); For Oct 13 Release By Vimal
    CompMap.put('Flow','Flow'); //For Oct 13 Release By Vimal
    CompMap.put('Approval Process','ApprovalProcess');  //For Oct 13 Release By Vimal
    CompMap.put('Folder','Folder');
    CompMap.put('Group','Group');
    //CompMap.put('Home Page Component','PageComponent');
    CompMap.put('Home Page Component','HomePageComponent');
    //CompMap.put('Home Page Layout','CustomPage');
    CompMap.put('Home Page Layout','HomePageLayout');
    //CompMap.put('Language Translation','Translation');
    CompMap.put('Language Translation','CustomObjectTranslation');
    CompMap.put('Lead Criteria Based Sharing Rule','LeadCriteriaSharingRule');
    CompMap.put('Lead Owner Sharing Rule','LeadOwnerSharingRule');
    CompMap.put('Letterhead','Letterhead');// corrected for DEC 2014 HotFIX
    //CompMap.put('List View','Filter');
    CompMap.put('List View','ListView');
    CompMap.put('Opportunity Criteria Based Sharing Rule','OpportunityCriteriaSharingRule');
    CompMap.put('Opportunity Owner Sharing Rule','OpportunityOwnerSharingRule');
    CompMap.put('Page Layout','Layout');
    CompMap.put('Permission Set','PermissionSet');
      //CompMap.put('Queue','Queues');
      CompMap.put('Queue','Queue');
    CompMap.put('Record Type','RecordType');
    CompMap.put('Remote Site','RemoteProxy');
    CompMap.put('Report','Report');
    CompMap.put('Role','UserRole');
    CompMap.put('S-Control','Scontrol');
    CompMap.put('Static Resource','StaticResource');
    //CompMap.put('Tab','CustomTabDefinition');
    CompMap.put('Tab','CustomTab');
    CompMap.put('Validation Rule','ValidationRule');
    CompMap.put('Visualforce Component','ApexComponent');
    CompMap.put('Visualforce Page','ApexPage');
    //CompMap.put('Workflow Email Alert','ActionEmail');
    CompMap.put('Workflow Email Alert','WorkflowAlert');
    //CompMap.put('Workflow Field Update','ActionFieldUpdate');
    CompMap.put('Workflow Field Update','WorkflowFieldUpdate');
    //CompMap.put('Workflow Outbound Message','ActionOutboundMessage');
    CompMap.put('Workflow Outbound Message','WorkflowOutboundMessage');
    CompMap.put('Workflow Rule','WorkflowRule');
    CompMap.put('Workflow','Workflow');
    //CompMap.put('Workflow Task','ActionTask');
    CompMap.put('Workflow Task','WorkflowTask');
    //Srinivas  MAY13
    CompMap.put('Lookup filter','NamedFilter');
    CompMap.put('Connected App','ConnectedApp');
    CompMap.put('Custom Permission','CustomPermission');
  
    parOrd=(Release__c)controller.getrecord();  
    
    TrackOptions = new List<SelectOption>();
    Schema.DescribeFieldResult F = Component__c.Track__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        TrackOptions.add(new SelectOption('--None--','--None--'));
        //TrackOptions.add(new SelectOption('All','All'));
        for(Schema.PicklistEntry pe:P){
      //If(pe.getLabel() != 'Shared')
      TrackOptions.add(new SelectOption(pe.getValue(),pe.getLabel()));
        }   
    }
    
    public pagereference queryXmlData(){
        List<Component__c> lstComp = new List<Component__c>();
        string Querystr ='';
        compXMLResult ='';
        try{        
      //ueryString = queryString + ' where contract__r.accountId ='+'\''+ newValueChainPlayer.account__c+'\''+' AND contact__c ='+ '\''+newValueChainPlayer.contact__c+'\''+' and LegacyPIN__c ='+'\'' + newValueChainPlayer.LegacyPIN__c+'\'' ;
        
      If(selectedvalue  != '--None--'){
        String strSelectedTrack = selectedvalue ;
        String strComma = '\''+','+'\'';
        System.debug('strComma' + strComma );
        strSelectedTrack = strSelectedTrack.replace(',',strComma );
        
        strSelectedTrack = strSelectedTrack.replace('[','');
        strSelectedTrack = strSelectedTrack.replace(']','');
        strSelectedTrack = strSelectedTrack.replace(' ','');
        System.debug(strSelectedTrack);
        Querystr = 'Select FieldName__c,ComponentType__c from Component__c Where Release__c  =' + '\''+ parOrd.id +'\''+' AND Ismigrate__c = TRUE AND Track__c IN (' + '\'' + strSelectedTrack + '\'' + ') Order By ComponentType__c,FieldName__c' ;
        System.debug('Querystr :' + Querystr  );
        lstComp =Database.query(Querystr);    
        System.debug(lstComp.size());
          //lstComp = [Select FieldName__c,ComponentType__c from Component__c 
          //Where Release__c  =: parOrd.id AND Ismigrate__c =:TRUE AND Track__c IN: selectedvalue Order By ComponentType__c,FieldName__c];
      }
      
      Map<String, List<String>> packageComponentMap = new Map<String, List<String>>();
      If(lstComp != null && lstComp.size()>0){
        for(Component__c objComponent: lstComp){    
          if(packageComponentMap.containsKey(objComponent.ComponentType__c)){
            packageComponentMap.get(objComponent.ComponentType__c).add(objComponent.FieldName__c);
          }
          else{
            List<String> lstFieldName = new List<String>();
            lstFieldName.add(objComponent.FieldName__c);
            packageComponentMap.put(objComponent.ComponentType__c,lstFieldName);
          }
        }
      }
      else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,'No Records'));
        return null;
      }
      System.debug(packageComponentMap.size());
      System.debug('@@@@@'+packageComponentMap);
      if(packageComponentMap.size()>0){
        compXMLResult = GenarateXMLCls(packageComponentMap);
        //return null;      
      } 
        }        
        catch(DmlException ex){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,ex.getMessage()));
      return null;
        }
    return null;
    }
    
    public string GenarateXMLCls(Map<String, List<String>> pckComponentMap){
    XmlStreamWriter w = new XmlStreamWriter();     
    w.writeStartDocument('UTF-8','1.0');                  
    w.writeStartElement(null, 'Package',null);        
    w.writeNamespace('xmlns', 'http://soap.sforce.com/2006/04/metadata');
    for(String objComponent: pckComponentMap.Keyset()){              
      w.writeStartElement(null, 'types', null);    
      for(String valuefromlst :pckComponentMap.get(objComponent)){
        if(valuefromlst!='' && valuefromlst!=null){
          w.writeStartElement(null, 'members', null);
          w.writeCharacters(valuefromlst);
          w.writeEndElement(); // End Members
        }            
      }     
      w.writeStartElement(null, 'name', null);
      w.writeCharacters(CompMap.get(objComponent));
      w.writeEndElement(); // End name 
      w.writeEndElement(); //End Types    
    }
    w.writeStartElement(null, 'version', null);
    w.writeCharacters('35.0'); //update for DEC 2014 HOTFIX
    w.writeEndElement(); // End version 
    w.writeEndElement(); //End Package
               
    String xmlOutput = w.getXmlString();         
    w.close();        
    return xmlOutput;
  }  
}