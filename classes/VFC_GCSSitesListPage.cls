public  class VFC_GCSSitesListPage
{
    public List<GCSSite__c> mem {get;set;}
    public Integer count {get;set;}
    public List<GCSSite__c> TotalSites= new List<GCSSite__c>();
    public Integer index = integer.valueOf(system.label.CLOCT15CCC32);
    public Integer start =integer.valueOf(system.label.CLOCT15CCC33);
    public Boolean nextBool {get;set;}
    public Boolean prevBool {get;set;}
    Public User UserObj{get;set;}
    Public String FirstName {get;set;}
    Public String lastname{get;set;}
    Public String Company{get;set;}
 
    public VFC_GCSSitesListPage(Apexpages.standardcontroller ctrl){   
    
        User UserRec= [select id,firstname,lastname,CompanyName from user where id=:UserInfo.getUserId()];
        FirstName = UserRec.FirstName;
        LastName = UserRec.LastName;   
        Company = UserRec.CompanyName;
        prevBool = true;
        nextBool = true;
        if(!Test.isRunningTest()){
            UserObj =[select id,accountId from User where Id=:UserInfo.getUserId()] ;
            TotalSites= [SELECT Id,name,account__c,Active__c,LegacyIBSiteID__c FROM GCSSite__c where Account__c=:UserObj.AccountId and Active__c=TRUE ORDER BY Name];
        }
        else{
            UserObj =[select id,accountId from User where Id=:UserInfo.getUserId()] ;
            TotalSites= [SELECT Id,name,account__c,Active__c,LegacyIBSiteID__c FROM GCSSite__c Where Active__c=TRUE ORDER BY Name];
        }
        Count = TotalSites.size();
        system.debug('Count at start'+count);
        List<GCSSite__c> tempSiteList = new List<GCSSite__c>();
        if(!TotalSites.isEmpty()){
          if(index < count){
            for(Integer i = start; i<index; i++)
            {        
                tempSiteList.add(TotalSites.get(i));
            }
            mem = tempSiteList;
            prevBool = true;
           nextBool = false;
           }
           else{
             for(Integer i = start; i<Count; i++)
            {        
                tempSiteList.add(TotalSites.get(i));
            }
            mem = tempSiteList;
            prevBool = true;
           nextBool = true;
           }
        }
        
    }
    
    public void next()
    {
        
        index = index + 10;
        start = start + 10;
        if(mem!=null){
            mem.clear();
        }
        if(index > count)
        {   
            index = Math.Mod(count,10) + start;
            system.debug('Index is ' + index);
            nextBool = false;
            prevBool = true;
            
            List<GCSSite__c> tempSiteList= new List<GCSSite__c>();
            for(Integer i = start; i<index ; i++)
            {        
                tempSiteList.add(TotalSites.get(i));
            }
            
            mem = tempSiteList;  
            nextBool = True;
            prevBool = False;       
           // index = start + 5;    
        }
        else
        {   
            List<GCSSite__c> tempSiteList= new List<GCSSite__c>();
            for(Integer i = start; i<Count; i++)
            {        
                tempSiteList.add(TotalSites.get(i));
            }
            mem = tempSiteList;
            system.debug('index'+index);
            prevBool = false;
        }   
    }
    
    public void previous()
    {
        if(start > 10)
        {    
            index = index- 10;
            start = start- 10;
            List<GCSSite__c> tempSiteList= new List<GCSSite__c>();
            for(Integer i = start; i<index; i++)
            {        
                tempSiteList.add(TotalSites.get(i));
            }
            mem = tempSiteList; 
            prevBool = false;
            nextBool = false;
        }    
        else
        {
           
            index = Math.Mod(count,10) + start ;
            start = start - 10;
            List<GCSSite__c> tempSiteList= new List<GCSSite__c>();
            for(Integer i = start; i<index; i++)
            {        
                tempSiteList.add(TotalSites.get(i));
            }
            mem = tempSiteList; 
            prevBool = true;
            nextBool = false;        
        }   
    }
}