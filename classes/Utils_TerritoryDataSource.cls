/*
    Author          : Accenture On-shore Team 
    Date Created    : 06/02/2012
    Description     : Utility class for Territory Data Access extending abstract Data Source Class
*/

public class Utils_TerritoryDataSource extends Utils_DataSource
{
    //    Query Parameters - Main Query and Different cluases which will be used conditionally
    static String query1 = 'SELECT Id, Name, Business__c, StateProvince__c,StateProvince__r.Name ,City__c ,ZipCode__c FROM SE_Territory__c';
    static String query2 = ' where ';
    static String query3 = ' OR ';
    static String query4 = 'Name like \'%';
    static String query5 = '%\'';
    static String query6 = 'Business__c = \'';
    static String query7 = 'City__c = \'';
    static String query8 = 'ZipCode__c= \'';
    static String query9 = '\')';
    static String query10 = ' ORDER BY Name';
    static String query11 = '\'';
    static String query12 = ' AND ';
    static String query13 = 'Country__c = \'';
    static String query14 = '(';
    static String query15 = ')';
    static String query16 = 'StateProvince__c = \'';
    //    End of Query String cretaion
    
    
    // Define Columns Label and displayed field  
    public override List<SelectOption> getColumns()
    {
        List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Field1__c',sObjectType.SE_Territory__c.fields.Name.getLabel())); // Territory Name
        Columns.add(new SelectOption('Field2__c',Label.CL00351)); // Business
        Columns.add(new SelectOption('Field3__c',Label.CL10086)); // State Province
        Columns.add(new SelectOption('Field4__c','Zip Code')); // ZIP CODE
        Columns.add(new SelectOption('Field5__c','City')); // CITY
        
        
        return Columns;
    }

     //    Main Search Method
     public override Result Search(List<String> keyWords, List<String> filters)
     {
         System.Debug('****** Start of the search for VFC77_TerritorySearch******');
         
          //    Initializing Variables      
         String searchText; 
         String business;
         String stateprovinceID;
         
         String country;
         String squery;
         
         List<sObject> results = New List<sObject>();
         List<sObject> tempResults = New List<sObject>();
         Utils_Sorter sorter = new Utils_Sorter();
         List<Id> compId = New List<Id>();
         Utils_DataSource.Result returnedResult = new Utils_DataSource.Result();   
         returnedResult.recordList = new List<DataTemplate__c>();
         Map<Object, String> stateprovinces= New Map<Object, String>();
         Map<String,sObject> resultData = New Map<String,sObject>();
         Map<Object, String> pickList = New Map<Object, String>();
         Map<Integer,DataTemplate__c> resultMap = new Map<Integer,DataTemplate__c>();
         List<Object> countryId = New List<Object>();
         List<sObject> resultset = New List<sObject>();
         //    End of Variable Initialization
         
         //    Assigning the User input Search Filter and Search criteria to the Variables
         if(!(filters[0].equalsIgnoreCase(Label.CL00355)))
             {
             business = filters[0];
             }
         if(!(filters[1].equalsIgnoreCase(Label.CL00355)))
             {
             stateprovinceID = filters[1];
             
             }
         if(!(filters[2].equalsIgnoreCase(Label.CL00355)))
             {
             country = filters[2];
             }    
         if(keywords != null && keywords.size()>0)
             {
             searchText = keyWords[0];
             }
         else
             { 
             searchText = '';
             }
         //    End of Assignment
         System.Debug('****** Territory Search Parameters #####');
         System.debug('****** Search Text: ' + searchText);
         System.debug('****** Business: ' + business);
         System.debug('****** stateprovince : ' + stateprovinceID);
         System.debug('****** Country: ' + country);
        
        
        //squery = query1+query2+query14+query4+searchText+query5+query3+query7+searchText+query11+query3+query8+searchText+query11+query15+query12+query13+country+query11+query12+query6+business+query11+query12+query16+stateprovinceID+query11+query10;
       
        System.debug('#### squery : ' +  squery );
         //    Searching the Territory Table to get all the combination the List of Territory
         //    and all the Country they are associated to
         if(searchText != NULL && searchText!= '')
         {
            if(business != NULL)
            {
                if(stateprovinceID != NULL)
                    {
                    results = Database.query(query1+query2+query14+query4+searchText+query5+query3+query7+searchText+query11+query3+query8+searchText+query11+query15+query12+query13+country+query11+query12+query6+business+query11+query12+query16+stateprovinceID+query11+query10);   
                    }                 
                else
                    {
                    results = Database.query(query1+query2+query14+query4+searchText+query5+query3+query7+searchText+query11+query3+query8+searchText+query11+query15+query12+query13+country+query11+query12+query6+business+query11+query10);
                    }
            }
            else
            {
                if(stateprovinceID != NULL)
                    {
                    results = Database.query(query1+query2+query14+query4+searchText+query5+query3+query7+searchText+query11+query3+query8+searchText+query11+query15+query12+query13+country+query11+query12+query16+stateprovinceID+query11+query10);   
                    }                 
                else
                    {
                    results = Database.query(query1+query2+query14+query4+searchText+query5+query3+query7+searchText+query11+query3+query8+searchText+query11+query15+query12+query13+country+query11+query10);
                    }
            }
        }
        else
        {
            if(business != NULL)
            {
                if(stateprovinceID != NULL)
                    {
                    results = Database.query(query1+query2+query13+country+query11+query12+query6+business+query11+query12+query16+stateprovinceID+query11+query10);   
                    }                 
                else
                    {
                    results = Database.query(query1+query2+query13+country+query11+query12+query6+business+query11+query10);
                    }
            }
            else
            {
                if(stateprovinceID != NULL)
                    {
                    results = Database.query(query1+query2+query13+country+query11+query12+query16+stateprovinceID+query11+query10);   
                    }                 
                else
                    {
                    results = Database.query(query1+query2+query13+country+query11+query10);
                    }
            }
        }
        
        
        //    End of Search
        
        //    Creation of Map of Business Picklist Value and corresponding Label
        Schema.DescribeFieldResult businessPickVal = Schema.sObjectType.SE_Territory__c.fields.Business__c;
        for (Schema.PickListEntry PickVal : businessPickVal.getPicklistValues())
        {
            pickList.put((Object)PickVal.getValue(),PickVal.getLabel());
        }
        //    End of Picklist Map creation
        
        //    Creation of Map of SFDC ID of the StateProvince and StateProvince Name
        for(StateProvince__c stp : [Select Id, Name from StateProvince__c where Country__c =: country])
        {
                stateprovinces.put((Object)stp.Id, stp.Name);
        }    
        //    End of Country Map creation
        
         //    Start of processing the Search Result to prepare the final result set
        tempResults.clear();
        for (sObject obj : results)
        { 
            SE_Territory__c records = (SE_Territory__c)obj;
            DataTemplate__c terr = New DataTemplate__c ();
            terr.put('Field1__c',records.get('Name'));
            terr.put('Field2__c',pickList.get(records.get('Business__c')));
            terr.put('Field3__c',stateprovinces.get(records.get('StateProvince__c')));
            terr.put('Field4__c',records.get('ZipCode__c'));
            terr.put('Field5__c',records.get('City__c'));
            terr.put('Field6__c',records.ID);
            
            tempResults.add(terr);            
        }  
          
        System.debug('#### Results without Country Filter: ' +  tempResults);
        returnedResult.recordList.clear();
        
        returnedResult.recordList.addAll(tempResults);
        
        //    End of Processing and preparation of the final result Set
        returnedResult.recordList = (List<sObject>)sorter.getSortedList(returnedResult.recordList, 'Field1__c', true);
        System.debug('#### Final Result :' + returnedResult);
        
        resultset.addAll(returnedResult.recordList);
        returnedResult.numberOfRecord = resultset.size();

        if(returnedResult.numberOfRecord >100)
        {
            returnedResult.recordList.clear();
            for(Integer count=0; count< 100; count++)
            {
                returnedResult.recordList.add(resultset[count]);      
            }
        }
        
        return returnedResult;
               
    }
}