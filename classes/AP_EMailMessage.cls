/*
    Author      : Vimal K(GD India)
    Date Created: 06-March-2014
    Description : Apex Class which Calls the Utils Class for posting message to Case Owner when the Case receives a Reply on Email to Case
                  Method: postFeedForCaseOwner() (BR–2151)
*/

public class AP_EMailMessage{
    public static void postFeedForCaseOwner(EmailMessage objEmailMessage){
        
        if(objEmailMessage.Incoming){
            if(String.valueOf(objEmailMessage.ParentId).startsWith(Case.sObjectType.getDescribe().getKeyPrefix())){ //Not required as Parent for EMail Message is always Case
                List<Case> lstParentCaseForEmail = new List<Case>([Select Id, OwnerId from Case where Id =: objEmailMessage.ParentId LIMIT 1]);
                if(lstParentCaseForEmail.size()==1){
                    Case objParentCaseForEmail = lstParentCaseForEmail[0];
                    if(String.valueOf(objParentCaseForEmail.OwnerId).startsWith(User.sObjectType.getDescribe().getKeyPrefix())){
                        Utils_Methods.postFeed(objParentCaseForEmail.Id,objParentCaseForEmail.OwnerId,System.Label.CLAPR14CCC14);
                    }
                }
            }
        }
    }
}