/*
Created By: Akram GARGOURI (akram.gargouri@Edifixio.com)
Release: 2016 Q2
Sync standard login informations from LoginHistory to Partner Contact
*/

//  00eA0000000bVV0IAM;
global class Batch_PartnerActivityTracker implements Database.Batchable<sObject>,Schedulable {
    public static final Integer MAXIMUM_STEPS_IN_CHAIN = 2;
    // step=0 : Sync loginHistory to PRMSystem Contacts
    // Step=1 : Update PRMSystemIsActiveAccount__c on Account
    private Integer Step;

    global Batch_PartnerActivityTracker() {
        this.step=0;
    }
    
    global Batch_PartnerActivityTracker(Integer step) {
        this.step=step;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String soql;
        if(step==0){
            if(!Test.isRunningTest())
                soql = 'SELECT LoginTime,UserId FROM LoginHistory where LoginTime >= LAST_N_DAYS:'+Label.CLJUN16PRM093+' and userId in (select Id from User where ProfileId=\''+Label.CLAPR15PRM026+'\' ) order by LoginTime asc';
            else
                soql = 'SELECT LoginTime,UserId FROM LoginHistory where LoginTime >= LAST_N_DAYS:'+Label.CLJUN16PRM093+' and userId in (select Id from User where ProfileId=\''+Label.CLAPR15PRM026+'\' ) order by LoginTime asc LIMIT 50';
           }else if(step==1){
            Date desactivationDate = Date.today().addDays(-Integer.valueOf(Label.CLJUN16PRM099));
            System.debug('desactivationDate'+desactivationDate);
            soql = 'SELECT Id,PRMSystemIsActiveAccount__c from Account where isPartner=true and PRMSystemIsActiveAccount__c=true and Id in (SELECT accountId from Contact where PRMContact__c=true and PRMSystemLastLoginDate__c<=:desactivationDate) and Id not in (SELECT accountId from Contact where PRMContact__c=true and PRMSystemLastLoginDate__c>:desactivationDate) LIMIT 10000';
            soql = soql.replace(':desactivationDate', String.valueOf(desactivationDate));
            System.debug(soql);
        }else if(step==2){
            Date desactivationDate = Date.today().addDays(-Integer.valueOf(Label.CLJUN16PRM099));
            System.debug('desactivationDate'+desactivationDate);
            soql = 'SELECT Id,PRMSystemIsActiveAccount__c from Account where isPartner=true and PRMSystemIsActiveAccount__c=false and Id in (SELECT accountId from Contact where PRMContact__c=true and PRMSystemLastLoginDate__c>:desactivationDate) LIMIT 10000';
            soql= soql.replace(':desactivationDate', String.valueOf(desactivationDate));
            System.debug(soql);
        }
        System.debug('start step is: '+step);
        System.debug('soql is: '+soql);
        return Database.getQueryLocator(soql);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        if(scope.size()>0 && scope.get(0) instanceof LoginHistory){
            Map<Id,Date> lastLoginfDateByUserId = new Map<Id,Date>();
            List<SObject> sobjectList = new List<SObject>();
            for(LoginHistory login: (List<LoginHistory>)scope){
                     lastLoginfDateByUserId.put(login.UserId,Date.newinstance(login.LoginTime.year(), login.LoginTime.month(), login.LoginTime.day()));
            }
            //Update Contacts...
            List<Contact> myContactToUpdate = new List<Contact>();
            for(User u:[select Id,ContactId from User where Id in:lastLoginfDateByUserId.keySet() and ContactId!=null]){
                myContactToUpdate.add(new Contact(Id=u.ContactId,PRMSystemLastLoginDate__c=lastLoginfDateByUserId.get(u.Id)));
            }
            System.debug('myContactToUpdate: '+myContactToUpdate);
            update myContactToUpdate;
         }else if(scope.size()>0 && scope.get(0) instanceof Account){
            //step==1 Account check active
            List<Account> accList = (List<Account>)scope;
            for(Account acc: accList){
                acc.PRMSystemIsActiveAccount__c=!acc.PRMSystemIsActiveAccount__c;
            }
            System.debug('accList: '+accList);
            Database.update(accList,false);
         }

    } 
    
    global void finish(Database.BatchableContext BC) {
        System.debug('finish: step is: '+step);
        if(step<MAXIMUM_STEPS_IN_CHAIN && !Test.isRunningTest()){
            step++;
            Batch_PartnerActivityTracker partnerActivityTracke = new Batch_PartnerActivityTracker(step); 
            Database.executebatch(partnerActivityTracke);
        }
    }
    
    //Schedule method
    global void execute(SchedulableContext sc) 
    {
        Batch_PartnerActivityTracker partnerActivityTracke = new Batch_PartnerActivityTracker(); 
        Database.executebatch(partnerActivityTracke);
    }
}