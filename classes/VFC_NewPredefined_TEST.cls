@isTest
Public Class VFC_NewPredefined_TEST{
 Static TestMethod Void UnitTest1(){
        Account accountobj = Utils_TestMethods.createAccount();
        insert(accountobj);
        
        Country__c CountryObj =Utils_TestMethods.createCountry();
        insert CountryObj;
      
        contact con=Utils_TestMethods.createContact(accountobj.Id,'testContact');
        Con.Country__c = CountryObj.Id;
        Database.insert(con); 
      
        Case caseRecord = new Case(
                Status='New',
                AccountId=accountobj.Id,
                ContactId=con.Id,
                SupportCategory__c='6-General',
                Priority='Medium',
                origin='Phone',
                Subject = 'Community'
                );
        Insert caseRecord;         
            
        BusinessRiskEscalationEntity__c Organization = new BusinessRiskEscalationEntity__c();
        Organization.name = 'testOrganization';
        Organization.Entity__c ='TESTEntitity';
        Organization.SubEntity__c = 'SubEntity';
        Organization.Location_Type__c='plant';
        Organization.Location__c = 'India';
        insert Organization ;
              
        PointOfContact__c createPointOfContact =Utils_TestMethods.createPointOfContact(Organization.Id,CountryObj.Id,'TestPOC','Phone');
        createPointOfContact.IsActive__c =True;
        insert createPointOfContact;
       
        CustomerCareTeam__c CCTeam =Utils_TestMethods.CustomerCareTeam(CountryObj.Id);
        insert CCTeam ;
       
       CaseClassification__c CaseClassification = New CaseClassification__c();
       CaseClassification.Name ='CaseClassificationTest';
       CaseClassification.label__c='Documentation Missing or Incorrect - Other*'; 
       CaseClassification.Category__c='testCategory';
       CaseClassification.Reason__c='testReason';
       CaseClassification.SubReason__c='testSubreason';
       Insert CaseClassification ;
       
       TeamCaseJunction__c DefaultTeamJunction = New TeamCaseJunction__c(Default__C=True,CCTeam__c=CCTeam.Id,CaseClassification__c=CaseClassification.Id);
       Insert DefaultTeamJunction ;
       
       apexpages.currentpage().getparameters().Put('def_account_id',accountobj.Id);
       apexpages.currentpage().getparameters().Put('def_contact_id',con.Id);  
       
       VFC_NewPredefined Predefined = new VFC_NewPredefined();
       Predefined.SpocPhoneNoLocalStorage ='TestPOC';
       Predefined.SubscriptionIDLocalStorage = '123456';
       Predefined.CountryCodeLocalStorage ='Fr';
       Predefined.CtiSkillLocalStorage='Test';
       Predefined.pageaction();
    }
     Static TestMethod Void UnitTest2(){
        Account accountobj = Utils_TestMethods.createAccount();
        accountobj.TobeDeleted__c =true;
        accountobj.ReasonForDeletion__c='Created in error';
        insert(accountobj);
        
        Country__c CountryObj =Utils_TestMethods.createCountry();
        insert CountryObj;
        
        contact con=Utils_TestMethods.createContact(accountobj.Id,'testContact');
        Con.Country__c = CountryObj.Id;
        con.TobeDeleted__c =true;
        con.ReasonForDeletion__c='Created in error';
        Database.insert(con); 
      
        Case caseRecord = new Case(
                Status='New',
                AccountId=accountobj.Id,
                ContactId=con.Id,
                SupportCategory__c='6-General',
                Priority='Medium',
                origin='Phone',
                Subject = 'Community'
                );
        Insert caseRecord;         
        apexpages.currentpage().getparameters().Put('def_account_id',accountobj.Id);
        apexpages.currentpage().getparameters().Put('def_contact_id',con.Id);
        
       VFC_NewPredefined Predefined = new VFC_NewPredefined();
       Predefined.CtiSkillLocalStorage='';
       Predefined.pageaction();
       //Predefined.cancel();
      
    }        
    Static TestMethod Void UnitTest3(){
        Account accountobj = Utils_TestMethods.createAccount();
        accountobj.Inactive__c=true;
        insert(accountobj);
        
        contact con=Utils_TestMethods.createContact(accountobj.Id,'testContact');
        con.Inactive__c =true;
        Database.insert(con); 
      
        Case caseRecord = new Case(
                Status='New',
                AccountId=accountobj.Id,
                ContactId=con.Id,
                SupportCategory__c='6-General',
                Priority='Medium',
                origin='Phone',
                Subject = 'Community'
                );
        Insert caseRecord;         
        apexpages.currentpage().getparameters().Put('def_account_id',accountobj.Id);
        apexpages.currentpage().getparameters().Put('def_contact_id',con.Id);
        
        VFC_NewPredefined Predefined = new VFC_NewPredefined();
    }
       
}