@isTest
global class VFC_CaseHierarchy_TEST {
    static testMethod void testVFC_CaseHierarchy() {

         Country__c testCountry = Utils_TestMethods.createCountry();
         insert testCountry;

         Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
         testAccount.Name = 'Test';
         insert testAccount;

         Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
         testContact.Country__c = testCountry.Id;
         insert testContact;

         Case CaseObj= Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'Open');
         CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
         CaseObj.Symptom__c =  'Installation/ Setup';
         CaseObj.SubSymptom__c = 'Hardware';    
         CaseObj.Quantity__c = 4;
         CaseObj.Family__c = 'ADVANCED PANEL';
         CaseObj.CommercialReference__c='xbtg5230';
         Insert CaseObj;

         PageReference vfPage = Page.VFP_CaseHierarchy;
         VFC_CaseHierarchy.getCaseHierarchyNodes(CaseObj.id);

        }
    }