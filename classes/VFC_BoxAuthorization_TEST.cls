@isTest
private class VFC_BoxAuthorization_TEST{
    static testMethod void boxFolderLink() {
        // TO DO: implement unit test
        //Define Page
        PageReference pg = Page.VFP_BoxAuthorization;
        Test.setCurrentPage(pg);
        pg.getParameters().put('code', '12345');
        // define controller --panggil yg public class, page reference
        VFC_BoxAuthorization controller = new VFC_BoxAuthorization();
        Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
        //HttpResponse res = CalloutClass.getInfoFromExternalService();
        controller.boxConnect();
        String json = '{'+
                        '\"access_token\":\"ABC\",'+
                        '\"refresh_token\":\"+ABCcaksi+\",'+
                        '\"expires_in\":"60"'+
                        '}';
        controller.parseAuthJSON(json); 
        try{
            controller.getBoxToken();	
        }catch(Exception e){
            
        }
    }
    @isTest(SeeAllData=true)
    static void boxFolderLinkWithoutCode() {
    //static testMethod void boxFolderLinkWithoutCode() {
        // TO DO: implement unit test
        //Define Page
        PageReference pg = Page.VFP_BoxAuthorization;
        Test.setCurrentPage(pg);
        pg.getParameters().put('code', '');
        // define controller --panggil yg public class, page reference
        VFC_BoxAuthorization controller = new VFC_BoxAuthorization();
        Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
        //HttpResponse res = CalloutClass.getInfoFromExternalService();
        controller.boxConnect();
        String json = '{'+
                        '\"access_token\":\"ABC\",'+
                        '\"refresh_token\":\"+ABCcaksi+\",'+
                        '\"expires_in\":"60"'+
                        '}';
        controller.parseAuthJSON(json); 
        /*CS_ELLABoxAccessToken__c accessToken = new CS_ELLABoxAccessToken__c(
            Name = 'Admin',
            AccessToken__c = 'eJ1xZhMAGICb4PuFzmEg12nr0DsPYLxk',
            BoxAccessTokenExpires__c = system.now()+1,
            BoxRefreshTokenExpire__c = system.now()+1,
            RefreshToken__c = 'MKoEN4zSf2g0jlomVK70uJWBBpwy5pWteMLxDAvWCJxLbKrGgbAVYItoECzOa4TT'
        );
		insert accessToken;*/
        
        try{
            controller.getBoxToken();	
        }catch(Exception e){
            
        }
    }
    
    static testMethod void boxFolderLinkWithoutcustSet() {
        // TO DO: implement unit test
        //Define Page
        PageReference pg = Page.VFP_BoxAuthorization;
        Test.setCurrentPage(pg);
        pg.getParameters().put('code', '');
        // define controller --panggil yg public class, page reference
        VFC_BoxAuthorization controller = new VFC_BoxAuthorization();
        Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
        //HttpResponse res = CalloutClass.getInfoFromExternalService();
        
        controller.boxConnect();
        String json = '{'+
                        '\"access_token\":\"ABC\",'+
                        '\"refresh_token\":\"+ABCcaksi+\",'+
                        '\"expires_in\":"60"'+
                        '}';
        controller.parseAuthJSON(json); 
        try{
            controller.getBoxToken();	
        }catch(Exception e){
            
        }
    }
}