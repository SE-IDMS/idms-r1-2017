public with sharing class VFC_CFWNew {
        
   
    public VFC_CFWNew(ApexPages.StandardController stdcontroller) {
    
    
        }
    
  public pagereference CreateCertification()
   {  
   
     pagereference newpage = new pagereference('/' + CFWRiskAssessment__c.SobjectType.getDescribe().getKeyPrefix() + '/e?');
     newpage.getParameters().put('nooverride', '1');

     return newpage;
   
    }

}