/**
* This is test class for IDMSPasswordService class.
**/
@isTest

private class IDMSPasswordServiceTest{
    
    //This method tests set user password
    static testmethod void setPassworduims(){
        User userset2 = new User(alias = 'userst2', email='userst2' + '@accenture.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userst2' + '@accenture.com.devmajq3a',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',IDMSToken__c='123wel',IDMSSignatureGenerationDate__c =system.today(),
                                 IDMS_Registration_Source__c = 'UIMS',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',isActive=true, IDMS_VU_NEW__c = '1' ,IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userset2 ;
        
        IDMSPasswordService setPassword2 = new IDMSPasswordService();
        try{
            setPassword2.setPassworduser(String.valueof(userset2.id),String.valueof(userset2.FederationIdentifier),'Welcome@1234','UIMS','123wel');
        }
        catch(Exception e){}
        try{
            userset2.IDMSToken__c='123wel';
            userset2.IDMSSignatureGenerationDate__c    = system.today(); 
            update userset2;
            IDMSPasswordService setPassword3 = new IDMSPasswordService();
            setPassword3.setPassworduser(String.valueof(userset2.id),String.valueof(userset2.FederationIdentifier),'Welcome@1234','idms','123wel');
        }
        catch(Exception e){}
        
         try{
            userset2.IDMSToken__c='123wel';
            userset2.IDMSSignatureGenerationDate__c    = system.today(); 
            update userset2;
            IDMSPasswordService setPassword4 = new IDMSPasswordService();
            setPassword4.setPassworduser(null,'1234','Welcome@1234','idms','123wel');
        }
        catch(Exception e){}
        
         try{
            userset2.IDMSToken__c='123wel';
            userset2.IDMSSignatureGenerationDate__c    = system.today(); 
            update userset2;
            IDMSPasswordService setPassword5 = new IDMSPasswordService();
            setPassword5.setPassworduser(String.valueof(userset2.id),String.valueof(userset2.FederationIdentifier),'Welcome@1234','idms','123wel');
        }
        catch(Exception e){}
        
        
        
        
        user userset3 = [select id , isactive , FederationIdentifier,username from user where id =: userset2.id ];    
        try{
            userset3.isactive = false ;
            update userset3;
        }
        catch(Exception e){}
        IDMSPasswordService setPassword3 = new IDMSPasswordService();
        try{
            setPassword3.setPassworduser(String.valueof(userset3.id),String.valueof(userset3.FederationIdentifier),'Welcome@12345','UIMS','123wel');
        }
        catch(Exception e){}
        try{
            setPassword3.setPassworduser(String.valueof(userset3.id),String.valueof(userset3.FederationIdentifier),'Welcome@12345','idms','123wel');
        }
        catch(Exception e){}
        
        IDMSPasswordService setPassword4 = new IDMSPasswordService();
        try{
            setPassword4.setPassworduser('123',String.valueof(userset3.FederationIdentifier),'Welcome@12346','UIMS','123wel');
            
        }catch(exception e){}
        
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),'1233','Welcome@12347','idms','123wel');
        }catch(exception e){}
        
        try{
            setPassword4.setPassworduser(null,'1233','Welcome@1238','idms','123wel');
        }catch(exception e){}
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),null,'Welcome@1238','idms','123wel');
        }catch(exception e){}
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),null,'Welcome@1238',null,'123wel');
        }catch(exception e){}
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),null,'Welcome@1238','uims',null);
        }catch(exception e){}
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),null,'Welcome@1238','uims','1212121');
        }catch(exception e){}
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),null,null,'uims','1212121');
        }catch(exception e){}
        try{
            setPassword4.setPassworduser('12121212',null,'test123test','uims','1212121');
        }catch(exception e){}
        try{
            setPassword4.setPassworduser(String.valueof(userset3.id),null,'test','idms','1212121');}
        catch(Exception e){}
        try{setPassword4.setPassworduser(null,'123-121-12','test@123test1','idms','12121');}
        catch(Exception e){}
        
        
        user userset4 = [select id , isactive , FederationIdentifier,username from user where id =: userset2.id ];    
        userset4.isactive = true ;
        update userset4;
        
        try{
            system.setpassword(userset3.id,'Password@123');
            //Test.setMock(HttpCalloutMock.class, new IDMS_MockHttpResponseGen());
            
            setPassword4.LoginCheck(userset3.username,'Password@123');
        }
        catch(Exception e){}
    }
    
    //This method tests set user password 
    static testmethod void setPassworduims2(){
        User userset2 = new User(alias = 'userst2', email='userst2' + '@accenture.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userst2' + '@accenture.com.devmajq3a',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='',
                                 IDMS_Registration_Source__c = 'UIMS',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',isActive=true, IDMS_VU_NEW__c = '1' ,IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        // insert userset2 ;
        
        IDMSPasswordService setPassword2 = new IDMSPasswordService();
        try{
            String id1;
            String fedID;
            setPassword2.setPassworduser(id1,fedID,'Welcome@1234','UIMS','123wel');
        }
        catch(Exception e){}
        
        
    }
    //This method tests set user password with required values
    static testmethod void setPassworduims3(){
        User userset2 = new User(alias = 'userst2', email='userst2' + '@accenture.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userst2' + '@accenture.com.devmajq3a',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',IDMSToken__c='123wel',IDMSSignatureGenerationDate__c =system.today(),
                                 IDMS_Registration_Source__c = 'UIMS',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',isActive=true, IDMS_VU_NEW__c = '1' ,IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userset2 ;
        
        IDMSPasswordService setPassword2 = new IDMSPasswordService();
        
        try{
            setPassword2.setPassworduser(String.valueof(userset2.id),String.valueof(userset2.FederationIdentifier),'Welcome@1234','abc','123wel');
        }
        catch(Exception e){}
    }
    //This method tests set user password
    static testmethod void setPassworduims4(){
        User userset2 = new User(alias = 'userst2', email='userst2' + '@accenture.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userst2' + '@accenture.com.devmajq3a',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',IDMSToken__c='123wel',IDMSSignatureGenerationDate__c =system.today(),
                                 IDMS_Registration_Source__c = 'UIMS',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',isActive=false, IDMS_VU_NEW__c = '1' ,IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userset2 ;
        
        IDMSPasswordService setPassword2 = new IDMSPasswordService();
        
        try{
            setPassword2.setPassworduser(String.valueof(userset2.id),String.valueof(userset2.FederationIdentifier),'Welcome@1234','abc','123wel');
        }
        catch(Exception e){}
    }
    //This method tests update user password
    static testmethod void updatePasswordFederationTest() {
        User userfed = new User(alias = 'userfed', email='userfed' + '@accenture.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='userfed' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='3010-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userfed ;
        
        
        String fedid =[select FederationIdentifier from user where id=: userfed.id limit 1].FederationIdentifier ;
        IDMSPasswordService updatefed = new IDMSPasswordService();
        updatefed.updatePasswordFederation('','', '') ;    
        updatefed.updatePasswordFederation('',fedid, 'uims') ;
        updatefed.updatePasswordFederation('Welcome@123','', 'uims') ;
        updatefed.updatePasswordFederation('Welcome@123',fedid, '') ;
        updatefed.updatePasswordFederation('Welcome@123',fedid, 'idms') ;
        updatefed.updatePasswordFederation('Welcome@123',fedid , 'uims') ;
        updatefed.updatePasswordFederation('Welcome@',fedid , 'uims') ;
        updatefed.updatePasswordFederation('Welcome@','123', 'uims') ;
    }
    //This method tests reset user password with identity type email
    static testmethod void resetPasswordEmailTest(){
        
        String env43 = System.Label.CLQ316IDMS043 ;
        String env44 = System.Label.CLQ316IDMS044 ;
        User userrs = new User(alias = 'userpass', email='userpass' + '@accenture.com',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userpass' + env44 + env43 ,Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMSIdentityType__c= 'email',
                               IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        insert userrs ;
        
        Test.startTest();
        system.debug('userres --> ' + userrs.email + ' ' + userrs.username ) ;
        user newuser = [select username , email from user where id =: userrs.id];
        system.debug('usernew --> ' + newuser.email + ' ' + newuser.username ) ;
        
        IDMSPasswordService resetPassword1 = new IDMSPasswordService();
        User userRecord1 = new User();
        userRecord1.MobilePhone=null;
        userRecord1.email='userpass@bridge-fo.com';
        userRecord1.IDMS_Profile_update_source__c='sample';
        resetPassword1.resetPassworduser(userRecord1);
        
        User userrs1 = new User(alias = 'user23', email='user23' + '@accenture.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='user23' + env44 + env43,Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='3010-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMSIdentityType__c= 'mobile',
                                IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        insert userrs1 ;
        user newuser1 = [select username , email from user where id =: userrs1.id];
        system.debug('usernew 1--> ' + newuser1.email + ' ' + newuser1.username ) ;
        
        
        IDMSPasswordService resetPassword2 = new IDMSPasswordService();
        User userRecord2 = new User();
        userRecord2.MobilePhone=null;
        userRecord2.email='user23@bridge-fo.com';
        userRecord2.IDMS_Profile_update_source__c='idms';
        resetPassword2.resetPassworduser(userRecord2);
        
        User userRecord3 = new User();
        userRecord3.MobilePhone=null;
        userRecord3.email='';
        userRecord3.IDMS_Profile_update_source__c='';
        resetPassword2.resetPassworduser(userRecord3);
        
        User userRecord4 = new User();
        userRecord4.MobilePhone=null;
        userRecord4.email='userpass11@accenture.com';
        userRecord4.IDMS_Profile_update_source__c='sample';
        resetPassword1.resetPassworduser(userRecord4);
        
        Test.stopTest();
        
    }
    //This method tests reset user password with identity type mobile
    static testmethod void resetPasswordPhoneTest(){
        String env43 = System.Label.CLQ316IDMS043 ;
        String env44 = System.Label.CLQ316IDMS044 ;
        String sep2 = System.Label.CLSEP16IDMS2;
        User userph = new User(alias = 'userph', email='userph' + '@accenture.com',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='9986995000'+ sep2 ,Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',IDMSIdentityType__c= 'email',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999', mobilephone = '9986995000');
        insert userph ;
        user newuser2 = [select username , email , mobilephone from user where id =: userph.id];
        system.debug('usernew2 --> ' + newuser2.email + ' ' + newuser2.username + '' + newuser2.mobilephone) ;
        
        Test.startTest();
        system.debug('userres phone --> ' +userph .mobilephone+ ' ' + userph .username ) ;
        IDMSPasswordService resetPassword = new IDMSPasswordService();
        User userRecord = new User();
        userRecord.MobilePhone='9986995000';
        userRecord.IDMS_Profile_update_source__c='';
        resetPassword.resetPassworduser(userRecord);
        
        User userRecord1 = new User();
        userRecord1.MobilePhone='9986995000';
        userRecord1.IDMS_Profile_update_source__c='sample';
        resetPassword.resetPassworduser(userRecord1);
        
        Test.stopTest();
    }
    //This method tests reset user password with identity type phone
    static testmethod void resetPasswordPhoneTest1(){
        String env43 = System.Label.CLQ316IDMS043 ;
        String env44 = System.Label.CLQ316IDMS044 ;
        String sep2 = System.Label.CLSEP16IDMS2;
        
        User userph1 = new User(alias = 'userph1', email='userph1' + '@accenture.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='009986995001'+ sep2 ,Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995001',FederationIdentifier ='308-Test123',IDMSIdentityType__c= 'Mobile',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999', mobilephone = '009986995001');
        insert userph1 ;
        user newuser2 = [select username , email , mobilephone from user where id =: userph1.id];
        system.debug('usernew2 --> ' + newuser2.email + ' ' + newuser2.username + '' + newuser2.mobilephone) ;
        
        Test.startTest();
        system.debug('userres phone --> ' +userph1.mobilephone+ ' ' + userph1.username ) ;
        IDMSPasswordService resetPassword = new IDMSPasswordService();
        User userRecord = new User();
        userRecord.MobilePhone='9986995001';
        userRecord.IDMS_Profile_update_source__c='';
        resetPassword.resetPassworduser(userRecord);
        
        User userRecord1 = new User();
        userRecord1.MobilePhone='9986995001';
        userRecord1.IDMS_Profile_update_source__c='sample';
        resetPassword.resetPassworduser(userRecord1);
        
        Test.stopTest();
    }
    //This method tests update user password
    static testmethod void updatePasswordTest(){
        User userup = new User(alias = 'userup', email='userup' + '@accenture.com',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userup' + '@accenture.com.devmajq3a',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userup ;
        IDMSPasswordService updatepassword = new IDMSPasswordService();
        Test.startTest();
        system.runAs(userup){
            system.setpassword(userup.id,'Welcome#123');
            
            updatePassword.updatePassworduser('','','');
            IDMSPasswordService updatepassword1 = new IDMSPasswordService();
            updatePassword1.updatePassworduser('','','idms');
            IDMSPasswordService updatepassword2 = new IDMSPasswordService();
            updatePassword2.updatePassworduser('','Welcome@','');
            updatePassword2.updatePassworduser('Welcome#123','Welcome#123',null);
            IDMSPasswordService updatepassword3 = new IDMSPasswordService();
            updatePassword3.updatePassworduser('Welcome#123','Welcome#1233',null);
            IDMSPasswordService updatepassword4 = new IDMSPasswordService();
            updatePassword4.updatePassworduser('Welcome#123','test','idms');
            updatePassword4.updatePassworduser('Welcome#1234','Welcome#123','idms');
            updatePassword4.updatePassworduser('Welcome#123','Welcome#1234','idms');
            updatePassword4.updatePassworduser('Welcome#1234','Welcome#12344','UIMS');
            updatePassword4.updatePassworduser('Welcome#12344','Welcome#12345','uims');
            updatePassword4.updatePassworduser('Welcome#12345','Welcome#12388','app1');
            Test.stopTest();
        }
        
          
    }
    
     //This method tests update user password
    static testmethod void updatePasswordTestEx(){
        User userup1 = new User(alias = 'userup', email='userup1' + '@accenture.com',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userup1' + '@accenture.com.devmajq3a',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userup1 ;
        IDMSPasswordService updatepassword = new IDMSPasswordService();
        Test.startTest();
        system.runAs(userup1){
            updatePassword.updatePassworduser('Welcome#123','Welcome#123','app1');
            Test.stopTest();
        }
        
          
    }
   
    
}