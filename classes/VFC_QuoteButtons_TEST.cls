@isTest (SeeAllData=true)
private class VFC_QuoteButtons_TEST {
   static testMethod void testPublish2GoDigital_SOL_NoQuoteId() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_Publish2GoDigital;

      Account acc = makeAccount();
      Contact con = makeContact(acc);
      zqu__Quote__c newQuote = makeSOLQuote(acc, con);

      test.setCurrentPage(pageRef);

      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_QuoteButtons ctrl = new VFC_QuoteButtons(sc);
      ctrl.Publish2GoDigital();
      ctrl.back();

      test.stopTest();
   }

   static testMethod void testPublish2GoDigital_SOL() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_Publish2GoDigital;

      Account acc = makeAccount();
      Contact con = makeContact(acc);
      zqu__Quote__c newQuote = makeSOLQuote(acc, con);

      pageRef.getParameters().put('id', newQuote.Id);
      test.setCurrentPage(pageRef);

      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_QuoteButtons ctrl = new VFC_QuoteButtons(sc);
      ctrl.Publish2GoDigital();
      ctrl.back();

      test.stopTest();
   }

   static testMethod void testPublish2GoDigital_FIO_New() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_Publish2GoDigital;

      Account acc = makeAccount();
      Contact con = makeContact(acc);
      zqu__Quote__c newQuote = makeFIOQuote_New(acc, con);

      pageRef.getParameters().put('id', newQuote.Id);
      test.setCurrentPage(pageRef);

      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_QuoteButtons ctrl = new VFC_QuoteButtons(sc);
      ctrl.Publish2GoDigital();
      ctrl.back();

      test.stopTest();
   }

   static testMethod void testPublish2GoDigital_FIO_Internally_Approved() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_Publish2GoDigital;

      Account acc = makeAccount();
      Contact con = makeContact(acc);
      zqu__Quote__c newQuote = makeFIOQuote_Internally_Approved(acc, con);

      pageRef.getParameters().put('id', newQuote.Id);
      test.setCurrentPage(pageRef);

      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_QuoteButtons ctrl = new VFC_QuoteButtons(sc);
      ctrl.Publish2GoDigital();
      ctrl.back();

      test.stopTest();
   }

   static testMethod void testGenerateDocument() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_Generate_Document;

      Account acc = makeAccount();
      Contact con = makeContact(acc);
      zqu__Quote__c newQuote = makeFIOQuote_New(acc, con);

      pageRef.getParameters().put('id', newQuote.Id);
      pageRef.getParameters().put('format', 'pdf');
      test.setCurrentPage(pageRef);

      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_QuoteButtons ctrl = new VFC_QuoteButtons(sc);
      ctrl.GenerateDocument();

      test.stopTest();
   }

   /**
   * Insert a test account in the db
   */
   public static Account makeAccount() {
      Account testAcc = new Account();
  
      List<Country__c> countryList = [SELECT Id from Country__c WHERE Name = 'France'];
      Id countryId = countryList.get(0).Id;
  
      testAcc.Name = 'my test account';
      testAcc.Street__c = 'Test Strret';
      testAcc.ClassLevel1__c = 'SI';
      testAcc.City__c = 'TestCity';
      testAcc.ZipCode__c = '90003';
      testAcc.Country__c = countryId;

      insert testAcc;
      return testAcc;
   }

   /**
   * Insert a test contact associated with an account in the db
   */
   public static Contact makeContact(Account testAcc) {
      Contact testContact = new Contact();

      testContact.FirstName = 'firstname';
      testContact.LastName = 'lastname';
      testContact.Email = 'a.b@yahoo.com';

      testContact.AccountId = testAcc.Id;

      insert testContact;
      return testContact;
   }

   /**
   * Insert a test Quote for SOL in the db
   */
   public static zqu__Quote__c makeSOLQuote(Account testAcc, Contact testCon) {
      zqu__Quote__c testQuote = new zqu__Quote__c();

      testQuote.Name = 'my test quote';

      testQuote.zqu__Account__c = testAcc.Id;
      testQuote.QuoteContactName__c = testCon.Id;
      testQuote.Entity__c = 'SOL';
      testQuote.zqu__Currency__c = 'USD';
      testQuote.zqu__Hidden_Subscription_Name__c = 'Boo';
      testQuote.zqu__SubscriptionTermStartDate__c = Date.today();
      testQuote.zqu__InitialTerm__c = 12;
      testQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
      testQuote.zqu__Status__c = 'New';
      testQuote.ApprovalStatus__c = 'New';
      testQuote.Nb_MW__c = 12;

      insert testQuote;
      return testQuote;
    }

   /**
   * Insert a test Quote for FIO, with Approval Status equals to New to  in the db
   */
   public static zqu__Quote__c makeFIOQuote_New(Account testAcc, Contact testCon) {
      zqu__Quote__c testQuote = new zqu__Quote__c();

      testQuote.Name = 'my test quote';

      testQuote.zqu__Account__c = testAcc.Id;
      testQuote.QuoteContactName__c = testCon.Id;
      testQuote.Entity__c = 'FIO';
      testQuote.zqu__Currency__c = 'USD';
      testQuote.zqu__Hidden_Subscription_Name__c = 'Boo';
      testQuote.zqu__SubscriptionTermStartDate__c = Date.today();
      testQuote.zqu__InitialTerm__c = 12;
      testQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
      testQuote.zqu__Status__c = 'New';
      testQuote.ApprovalStatus__c = 'New';

      insert testQuote;
      return testQuote;
    }

   /**
   * Insert a test Quote for FIO, with Approval Status equals to Internally Approved to  in the db
   */
   public static zqu__Quote__c makeFIOQuote_Internally_Approved(Account testAcc, Contact testCon) {
      zqu__Quote__c testQuote = new zqu__Quote__c();

      testQuote.Name = 'my test quote';

      testQuote.zqu__Account__c = testAcc.Id;
      testQuote.QuoteContactName__c = testCon.Id;
      testQuote.Entity__c = 'FIO';
      testQuote.zqu__Currency__c = 'USD';
      testQuote.zqu__Hidden_Subscription_Name__c = 'Boo';
      testQuote.zqu__SubscriptionTermStartDate__c = Date.today();
      testQuote.zqu__InitialTerm__c = 12;
      testQuote.zqu__SubscriptionTermEndDate__c = Date.today().addMonths(12);
      testQuote.zqu__Status__c = 'New';
      testQuote.ApprovalStatus__c = 'Internally Approved';

      insert testQuote;
      return testQuote;
    }
}