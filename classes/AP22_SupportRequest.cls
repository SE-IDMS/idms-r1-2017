/*
    Author          : Accenture IDC Team 
    Date Created    : 28/07/2011
    Description     : Class utilised by triggers SupportRequestBeforeInsert, SupportRequestBeforeUpdate
                      supportRequestAfterInsert.
*/
public class AP22_SupportRequest
{
       /**Method Throws a Validation Error when not created by people from 
       the Sales team with edit rights on the linked Opportunity,
       */
       /*
    public static void validateUser(Map<Id,OPP_SupportRequest__c> supportRequest)
    {
         System.Debug('****** validateUser method Start****');
         List<opportunityTeamMember> opportunityTeam = New List<opportunityTeamMember>();
         Map<String,String> oppTeam = new Map<String,String>();
         Map<String,String> salesTeam = new Map<String,String>(); 
         Map<string,string> oppOwners= oppOwner(supportRequest.keyset());    
         Boolean flag=false;
         
         for(Profile prof : [Select Id, Name from Profile where Name Like :System.Label.CL00498])                
         {
             if(prof.Id == userinfo.getProfileId())
                 flag=true;
         }      
         
         if(!flag)
         {
             String Id =[select id from recordtype where name=:Label.Cl00538 and sObjectType='OPP_SupportRequest__c'].id;
             opportunityTeam = [select OpportunityID,userId from opportunityTeamMember where OpportunityID In :supportRequest.keyset() and opportunityAccessLevel =:Label.CL00203];     
         
             if(opportunityTeam.size()>0)
             {    
                 for(opportunityTeamMember team : opportunityTeam)
                 {
                     oppTeam.put(string.valueOf(team.OpportunityID)+string.valueOf(team.userID),team.OpportunityID);        
                     salesTeam.put(string.valueOf(team.OpportunityID)+string.valueOf(team.userID),team.userId);        
                 }
             }
         
             for(OPP_SupportRequest__c supportReq : supportRequest.values())
             {
             //Extra check so only transfer ownership can be created by opportunity owner
                 if(supportReq.recordTypeID!=Id)
                 {
                     for(string opportunity : oppTeam.keyset())             
                     {    
                         if(supportReq.Opportunity__c==oppTeam.get(opportunity))
                         { 
                                 if(salesTeam.get(opportunity).contains(userInfo.getUserId()))
                                 {
                                  flag=true;                                                                 
                                 }                     
                         }    
                     }
                 } 
                 for(string owner:oppOwners.keyset())
                 {
                     if(supportReq.opportunity__c==owner)                     
                     {
                         if(userinfo.getuserId()==oppOwners.get(supportReq.opportunity__c))
                         {
                             flag=true;
                         }
                     }
                 }
                
                                
                if(!flag)
                {
                    supportReq.addError(Label.Cl00530);                             
                }             
             }  
            System.Debug('****** validateUser method Start****');                  
        }
    }
    */
    public static Map<string,string> oppOwner(Set<Id> opportunityId)
    {
    list<opportunity> oppOwner=[select OwnerId from opportunity where id in:opportunityId];    
    Map<string,string> oppOwners = new map<string,string>();
         if(oppOwner.size()>0)
         {
             for(opportunity opp:oppOwner)
             {
                 oppOwners.put(opp.id,opp.ownerid);    
             }
         }         
         return oppOwners;  
    }
        
    public static void assignToOpportintyTeam(Map<Id,OPP_SupportRequest__c> supportRequest, String action)
    {
        system.Debug('######## START of Adding to the Sales Team and Opportunity Share method ########');
        List<opportunityTeamMember> oppTeamMem = new List<opportunityTeamMember>();
        set<id> solutionCenterId = new set<id>();
        set<id> opportunityID= new set<id>();
        List<opportunityShare> share = new List<opportunityShare>();
        Map<ID,string> solution = new map<ID,string>();
        Map<ID,ID> transferOpp = new Map<ID,ID>();
        
        for(OPP_SupportRequest__c srq: supportRequest.values())
        {
            solutionCenterId.add(srq.solutionCenter__c);
            opportunityID.add(srq.opportunity__c);
        }

        Map<string,string> oppOwners= oppOwner(opportunityID);         

        for(SolutionCenter__c soln: [select id,IsASolutionCenter__c from solutionCenter__c where id IN:solutionCenterId])
        {
           solution.put(soln.id,soln.IsASolutionCenter__c);
        }
        String reciD=[select id from recordtype where name=:Label.Cl00538 and sObjectType=:'OPP_SupportRequest__c'].id;    
        
        for(OPP_SupportRequest__c supReq : supportRequest.values())
        {
                /*Transfer oportunity ownership support request extra procesing*/
            if(supReq.recordTypeId!=NULL && (string.ValueOf(supReq.recordTypeId).contains(recId)||recid.contains(string.ValueOf(supReq.recordTypeId))))
            {
                transferOpp.put(supReq.opportunity__c,supReq.AssignedToUser__c);
            }
            //else
            
            opportunityTeamMember oppteam = new opportunityTeamMember();  
            opportunityShare Sharing = new opportunityShare();            
            oppteam.OpportunityID = supReq.opportunity__c; 
            sharing.OpportunityId=supReq.opportunity__c;
            
            if(supReq.AssignedToUser__c != NULL && action == 'assignToTeam')
            {
                oppteam.userId = supReq.AssignedToUser__c;// inserting the user in opportunity Team with read write access as Salesprson           
                sharing.UserOrGroupId= supReq.AssignedToUser__c;// inserting the user in opportunity sahre with read write access as Salesprson    
                sharing.OpportunityAccessLevel = Label.CL00203;
                
                if(supReq.SellingCenter__c!=null)
                {     
                    oppteam.TeamMemberRole = Label.CL00265;   // CL00265 - SalesPerson Role 
                }
                else if(supReq.SolutionCenter__c!=null && solution.get(supReq.SolutionCenter__c)==Label.CL00004) // If Support Center
                {                
                    oppteam.TeamMemberRole = Label.CL00495;//CL00495 - support center Manager Role
                }
                else if(supReq.SolutionCenter__c!=null && solution.get(supReq.SolutionCenter__c)==label.CL00005) // If Not Support Center
                {
                    oppteam.TeamMemberRole = Label.CL00494;// CL00494 - Technical Manager Role
                }
                else if(supReq.SolutionCenter__c!=null && solution.get(supReq.SolutionCenter__c)==NULL)// If Data has been loaded and IsSupportCenter has not been set
                {
                    oppteam.TeamMemberRole = Label.CL00494;// CL00494 - Technical Manager Role
                }
            }   
            
            if(supReq.PMTender__c != NULL && action == 'assignPMToTeam')
            {
                oppteam.userId = supReq.PMTender__c;
                sharing.UserOrGroupId= supReq.PMTender__c;  
                sharing.OpportunityAccessLevel = Label.CL00203;
                oppteam.TeamMemberRole = Label.CL00269; //CL00269 - PM Tender Role
            }
            
            /* Modified for BR-4172 */
            if(supReq.AppDevEngineer__c!= NULL && action == 'assignADEToTeam')
            {
                oppteam.userId = supReq.AppDevEngineer__c;
                sharing.UserOrGroupId= supReq.AppDevEngineer__c;  
                sharing.OpportunityAccessLevel = Label.CL00203;
                oppteam.TeamMemberRole = Label.CLOCT14SLS02; //ADE Tender Role
            }
            if(supReq.SolutionArchitect__c!= NULL && action == 'assignSAToTeam')
            {
                oppteam.userId = supReq.SolutionArchitect__c;
                sharing.UserOrGroupId= supReq.SolutionArchitect__c;  
                sharing.OpportunityAccessLevel = Label.CL00203;
                oppteam.TeamMemberRole = Label.CLOCT14SLS03; //SA Tender Role
            }            
            if(supReq.SupportUser1__c!= NULL && action == 'assignSu1ToTeam')
            {
                oppteam.userId = supReq.SupportUser1__c;
                sharing.UserOrGroupId= supReq.SupportUser1__c;  
                sharing.OpportunityAccessLevel = Label.CL00203;
                oppteam.TeamMemberRole = Label.CLOCT14SLS20; //Quotation Specialist Role
            }
            if(supReq.SupportUser2__c!= NULL && action == 'assignSu2ToTeam')
            {
                oppteam.userId = supReq.SupportUser2__c;
                sharing.UserOrGroupId= supReq.SupportUser2__c;  
                sharing.OpportunityAccessLevel = Label.CL00203;
                oppteam.TeamMemberRole = Label.CLOCT14SLS20; //Quotation Specialist Role
            }
            /* End of Modification for BR-4172 */
            
            for(string opp:oppOwners.keyset())
            {
                if(supReq.opportunity__c==opp)                     
                { 
                    if(supReq.AssignedToUser__c != NULL && supReq.AssignedToUser__c!=oppOwners.get(opp) && action == 'assignToTeam')   
                    {
                        if(!(supReq.recordTypeId!=NULL && (string.ValueOf(supReq.recordTypeId).contains(recId)||recid.contains(string.ValueOf(supReq.recordTypeId)))))
                            share.add(sharing);
                        oppTeamMem.add(oppteam);
                    }
                    if(supReq.PMTender__c != NULL && supReq.PMTender__c!=oppOwners.get(opp) && action == 'assignPMToTeam')   
                    {
                        share.add(sharing);
                        oppTeamMem.add(oppteam);
                    }
                    /* Modified for BR-4172 */
                    if(supReq.AppDevEngineer__c!= NULL && supReq.AppDevEngineer__c!=oppOwners.get(opp) && action == 'assignADEToTeam')   
                    {
                        share.add(sharing);
                        oppTeamMem.add(oppteam);
                    }
                    if(supReq.SolutionArchitect__c!= NULL && supReq.SolutionArchitect__c!=oppOwners.get(opp) && action == 'assignSAToTeam')   
                    {
                        share.add(sharing);
                        oppTeamMem.add(oppteam);
                    }  
                    if(supReq.SupportUser1__c!= NULL && supReq.SupportUser1__c!=oppOwners.get(opp) && action == 'assignSu1ToTeam')   
                    {
                        share.add(sharing);
                        oppTeamMem.add(oppteam);
                    }
                    if(supReq.SupportUser2__c!= NULL && supReq.SupportUser2__c!=oppOwners.get(opp) && action == 'assignSu2ToTeam')   
                    {
                        share.add(sharing);
                        oppTeamMem.add(oppteam);
                    }
                    /* End of Modification for BR-4172 */                  
                }
            }
           
        }
        //Added to handle transfer Opportunity ownership
        List<opportunity> updateOpportunity = new List<opportunity>();
        if(transferOpp!=null)
        {    
            List<opportunity> opportunity = [select ownerid,TECH_OwnerChangeFrmSupReq__c from opportunity where id in:transferOpp.keyset()];
            for(opportunity opp:opportunity)
            {
                opp.ownerId=transferOpp.get(opp.id);
                opp.TECH_OwnerChangeFrmSupReq__c=true;
                updateOpportunity.add(opp);
            }
        }
        if(!updateOpportunity.isEmpty())
        {
            if(updateOpportunity.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP22 Error Insert: '+ Label.CL00264);
            }
            else
            {
                Database.saveResult []svr = database.update(updateOpportunity);
                for(Database.SaveResult svrt:svr)
                {
                    if(!svrt.isSuccess())
                    {
                        System.Debug('######## AP22 Error Insert: '+svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());              
                    }
                }
            }             
        }
        
        if(!oppTeamMem.isEmpty())
        {
            if(oppTeamMem.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP22 Error Insert: '+ Label.CL00264);
            }
            else
            {
                Database.saveResult []svr = database.insert(oppTeamMem);
                for(Database.SaveResult svrt:svr)
                {
                    if(!svrt.isSuccess())
                    {
                        System.Debug('######## AP22 Error Insert: '+svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());              
                    }
                }
            }             
        }

        if(!share.isEMpty())
        {
            if(share.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP22 Error Insert: '+ Label.CL00264);
            }
            else
            {
                Database.saveResult []svr = database.insert(share);
                for(Database.SaveResult svrt:svr)
                {
                    if(!svrt.isSuccess())
                    {
                        System.Debug('######## AP22 Error Insert: '+svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());              
                    }
                }
            }             
        }
        System.Debug('######## START of Adding to the Sales Team and Opportunity Share method ########');
    }            
     
     public static void updateEmail(list<OPP_SupportRequest__c> supportRequest)
     {
        set<id> solutionCenterId = new set<id>();
        set<id> sellingCenterId = new set<id>();
        Map<ID,string> solution = new Map<ID,string>();
        Map<ID,string> selling = new map<ID,string>();
        for(OPP_SupportRequest__c srq: supportRequest)
        {
            if(srq.solutionCenter__c!=null)
            {
                solutionCenterId.add(srq.solutionCenter__c);            
            }
            if(srq.sellingCenter__c!=null)
            {
                sellingCenterId.add(srq.sellingCenter__c);            
            }
        }
         if(solutionCenterId.size()>0)
         {
               list<SolutionCenter__c> sol=[select EmailAddress__c from solutionCenter__c where id in:solutionCenterId];                
               for(SolutionCenter__c soln:sol)
               {
                    solution.put(soln.id,soln.EmailAddress__c);
               } 
         }
         if(sellingCenterId.size()>0)
         {
             list<OPP_SellingCenter__c> sel =[select Email__c from OPP_SellingCenter__c where id in:sellingCenterId];   
             for(opp_sellingCenter__c seln:sel)
             {
             selling.put(seln.id,seln.Email__c);
             }  
         }
         for(OPP_SupportRequest__c supReq:supportRequest)
         {
             if(supReq.SolutionCenter__c!=null )
             {
                supReq.SupportEmail__c= solution.get(supReq.SolutionCenter__c);                  
             }
             if(supReq.sellingCenter__c!=null)
             {
                 supReq.SupportEmail__c=selling.get(supReq.sellingCenter__C);
             }
         }
     }    
      //Populates Support Request's Agreement field with TECH_AgreementId__c
    Public static void updateAgreement(List<OPP_SupportRequest__c> suppRequests)
    {
        for(OPP_SupportRequest__c suppRequest: suppRequests)
        {
            suppRequest.Agreement__c = suppRequest.TECH_AgreementId__c;
        }            
    } 
    //AddedforBR-7964 - Additional information on the support request - March 2016 release 
   // populate Support Request Type of Offer into QL Type of Offer 
       Public static void qlTypeOfOfferPopulation(Map<Id,OPP_SupportRequest__c> suppRequestMap)
    {
        System.debug('>><<>qlTypeOfOfferPopulation start ><><');
        List<OPP_QuoteLink__c> qlLst = new List<OPP_QuoteLink__c>();
        List<OPP_QuoteLink__c> updateqlLst = new List<OPP_QuoteLink__c>();
        qlLst =[Select TypeOfOffer__c from OPP_QuoteLink__c where id in :suppRequestMap.keyset()];
        for(OPP_QuoteLink__c qlk: qlLst )
        {
            if(qlk.TypeOfOffer__c == null || qlk.TypeOfOffer__c == ''){
               qlk.TypeOfOffer__c = suppRequestMap.get(qlk.Id).TypeOfOffer__c ;
               updateqlLst.add(qlk);
            }
        }  
        update updateqlLst;     
        System.debug('>><<>qlTypeOfOfferPopulation end ><><');     
    }  
}