@isTest
public class VCC_OpptyPassesToPartner_TEST {
    static testMethod void myTest(){
        List<Profile> profiles = [select id from profile where name='System Administrator'];
            if(profiles.size()>0){
                User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                usr.BypassWF__c = false;
                usr.BypassVR__c = true;
                System.runas(usr){
                    
                      Country__c country = Utils_TestMethods.createCountry();
                      country.countrycode__c ='IN';
                      insert country;
                    
                      Account acc = Utils_TestMethods.createAccount();
                      acc.Country__c = country.id;
                      insert acc;
                      
                      Contact con = Utils_TestMethods.createContact(acc.id, 'Test Contact');
                      insert con;
                    
                      Opportunity opp = Utils_TestMethods.createOpportunity(acc.id);
                      insert opp;
                      
                      OPP_ValueChainPlayers__c opval = Utils_TestMethods.createValueChainPlayer(acc.id, con.id, opp.id);
                      opval.ContactRole__c = 'Primary Customer Contact';
                      insert opval;
                      
                      VCC_OpptyPassesToPartner op = new VCC_OpptyPassesToPartner();
                      op.setRecptId(usr.id);
                      
                      op.getRecptId();
                      op.setOpptyId(opp.id);
                      op.getOpptyId();
                      
                      
                 }
             }
      }
}