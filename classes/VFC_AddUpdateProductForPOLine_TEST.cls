@isTest(SeeAllData=true)
private class VFC_AddUpdateProductForPOLine_TEST {

    static testMethod void AddUpdateProductforPOLineTestMethod() {
    //Start of Test Data preparation
    
        SVMXC__RMA_Shipment_Order__c shipment = new SVMXC__RMA_Shipment_Order__c();
        insert shipment;
        
        Test.startTest();
        SVMXC__RMA_Shipment_Line__c line = new SVMXC__RMA_Shipment_Line__c();
        line.SVMXC__RMA_Shipment_Order__c = shipment.Id;
        line.CommercialReference__c = 'Commercial Reference test';
        insert line;
        Test.stopTest();

        ApexPages.StandardController ShipLineStandardController = new ApexPages.StandardController(line);   
        VFC_AddUpdateProductForPartOrderLine GMRSearchPage = new VFC_AddUpdateProductForPartOrderLine(ShipLineStandardController );
        
        
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        GMRSearchPage.jsonSelectString=jsonSelectString;
        PageReference pageRef=Page.VFP_AddUpdateProductForPartOrderLine;
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);  
        Test.setCurrentPage(pageRef);
        GMRSearchPage.performSelect();
        GMRSearchPage.performSelectFamily();
    }
    static testMethod void AddUpdateProductforPOLineTestErrorMethod() {
    //Start of Test Data preparation
    
        
        Test.startTest();
        SVMXC__RMA_Shipment_Line__c Errorline = new SVMXC__RMA_Shipment_Line__c();
        Errorline.CommercialReference__c = 'Commercial Reference test';
        Test.stopTest();

        ApexPages.StandardController ShipLineStandardController = new ApexPages.StandardController(Errorline);   
        VFC_AddUpdateProductForPartOrderLine GMRSearchPage = new VFC_AddUpdateProductForPartOrderLine(ShipLineStandardController );
        
        
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        GMRSearchPage.jsonSelectString=jsonSelectString;
        PageReference pageRef=Page.VFP_AddUpdateProductForPartOrderLine;
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);  
        Test.setCurrentPage(pageRef);
        GMRSearchPage.performSelect();
    }
}