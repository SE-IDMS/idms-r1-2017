/**
    Author          : Nitin Khunal
    Date Created    : 20/07/2016
    Description     : Test Class for VFC_PACaseSearchPage apex class
*/
@isTest
public class VFC_PACaseSearchPage_TEST {
    
    @testSetup static void testData() {
        List<Account> lstAccount = new List<Account>();
        List<Case> lstCase = new List<Case>();
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount1 = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount1.SEAccountID__c = '123456789';
        testAccount1.City__c = 'Bangalore';
        insert testAccount1;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        testAccount.parentid = testAccount1.Id;
        testAccount.City__c = 'Bangalore';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='1234';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
        BusinessRiskEscalationEntity__c objOrg1 = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        objOrg1.Name=System.Label.CLQ316PA025;
        insert(objOrg1);
        Case testCase1 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase1.ReportingOrganization__c = objOrg1.Id;
        testCase1.AnswerToCustomer__c = 'testing';
        lstCase.add(testCase1);
        Case testCase2 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'Closed');
        testCase2.ReportingOrganization__c = objOrg1.Id;
        testCase2.AnswerToCustomer__c = 'testing';
        lstCase.add(testCase2);
        insert lstCase;
    }
    
    @isTest static void testMethod1() {
        Account acc = [select id from Account limit 1];
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='1234567'; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='1234';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv';
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo.strAccountGoldenId = '123456789';
        manualShareInfo.strSharingAccessLevel = 'Edit';
        manualShareInfo.strWebUserName = 'testInv';
        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
        system.debug('userresult1==='+userresult1);
        system.runAs(new User(id = userresult.userId)) {
            Test.startTest();
            VFC_PACaseSearchPage controller = new VFC_PACaseSearchPage();
            List<VFC_PACaseSearchPage.accountPickList> accPickList = VFC_PACaseSearchPage.pickaccountValue(userresult.userId);
            //system.assertEquals(accPickList[1].strAccountName, 'TestAccount (Bangalore, null, FR)');
            List<Account> lstAccount = [select Id from Account where SEAccountID__c = '123456789'];
            List<Contact> lstContact = [select Id from Contact where SEContactID__c = '1234567'];
            List<Case> accCase = VFC_PACaseSearchPage.getCases(lstAccount[0].Id, 'All', 'Complaint', '');
            List<Case> accCase1 = VFC_PACaseSearchPage.getCases(lstAccount[0].Id, 'AllClose', 'Complaint','');
            BusinessRiskEscalationEntity__c objOrg1 = [select id from BusinessRiskEscalationEntity__c limit 1];
            Case testCase = Utils_TestMethods.createCase(lstAccount[0].Id, lstContact[0].Id, 'New');
            testCase.ReportingOrganization__c = objOrg1.Id;
            testCase.AnswerToCustomer__c = 'testing';
            insert testCase;
            List<Case> accCase2 = VFC_PACaseSearchPage.getCases(lstAccount[0].Id, 'MyOpen', 'Complaint','');
            //system.assertEquals(accCase2[0].Status, 'In Progress');
            Test.stopTest();
        }
    }
    
    @isTest static void testMethod2() {
        Account acc = [select id from Account limit 1];
        List<Case> accCase = VFC_PACaseSearchPage.getCases('', 'All', 'Complaint', 'AllPA');
        List<Case> accCase1 = VFC_PACaseSearchPage.getCases('', 'All', 'Complaint', system.label.CLQ316PA029);
        List<Case> accCase2 = VFC_PACaseSearchPage.getCases('', 'All', 'Complaint', system.label.CLQ316PA040);
        List<Case> accCase3 = VFC_PACaseSearchPage.getCases('', 'All', 'Complaint', system.label.CLQ316PA031);
        
    }
}