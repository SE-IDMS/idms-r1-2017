/**
    Author          : Nitin Khunal
    Date Created    : 21/09/2016
    Description     : Controller for VFP_PAMyOpenCasesReport VF Page
*/
Public without sharing class VFC_PAHomePage {
    Public VFC_PAHomePage () {
    Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
    }
    
    @RemoteAction
    Public static List<openCasesWrapperClass> myOpenCases () {
        Set<Id> setAccIds = new Set<Id>();
        Set<Id> setOrgIds = new Set<Id>();
        Set<Id> setBlank = new Set<Id>();
        Set<String> setCountry = new Set<String>();
        List<String> lstCountry = new List<String>();
        List<openCasesWrapperClass> lstCases = new List<openCasesWrapperClass>();
        List<Case> lstCase = new List<Case>();
        Map<String, List<openCasesWrapperClass>> mapOpenCasesSorted = new Map<String, List<openCasesWrapperClass>>();
        Map<String, openCasesWrapperClass> mapOpenCases = new Map<String, openCasesWrapperClass>();
        /*if(!String.isBlank(System.Label.CLQ316PA021)) {
            String strOrg = System.Label.CLQ316PA021;
            if(strOrg.contains(',')) {
                List<String> parts = strOrg.split(',');
                for(String xOrgId : parts) {
                    setOrgIds.add(xOrgId);
                }
            }else
            {
                setOrgIds.add(strOrg);
            }
        }*/
        setOrgIds = PA_UtilityClass.getReportingOrganisation();
        /*User UserObj =[select id,contactId, contact.accountId, contact.account.Name from User where Id=:UserInfo.getUserId()];
        for(AccountShare obj:[select id,AccountID from AccountShare where UserorGroupId=:UserInfo.getUserId()]){
            setAccIds.add(obj.AccountID);
        }
        setAccIds.add(UserObj.contact.accountId);*/
        setAccIds = PA_UtilityClass.getAccountId(true, true, 'Read', Userinfo.getUserId());
        //for(Account xAcc : [select id, Name, Country__r.Name, BillingCountry, BillingCity, BillingState, StateProvince__r.StateProvinceCode__c from Account where Id IN: setAccIds]) {
        for(Account xAcc : PA_UtilityClass.getAccountRecord(setAccIds, setBlank)) {
            openCasesWrapperClass oCWC = new openCasesWrapperClass();
            if(xAcc.Country__r != null) {
                oCWC.Country = xAcc.Country__r.Name;
            }else
            {
                oCWC.Country = '';
            }
            oCWC.AccountName = PA_UtilityClass.concatenateAddressWithoutCountry(xAcc.Name, xAcc.BillingCity, xAcc.StateProvince__r.StateProvinceCode__c);
            oCWC.AccountId = xAcc.Id;
            mapOpenCases.put(xAcc.Id, oCWC);
            lstCountry.add(oCWC.Country);
        }
        setCountry.addAll(lstCountry);
        lstCountry.clear();
        lstCountry.addAll(setCountry);
        lstCountry.sort();
        if(!Test.isRunningTest()) {
            lstCase = [select PACaseType__c, AccountId, Account.Name, Account.BillingCountry, Account.Country__r.Name, Status from Case Where AccountId IN: setAccIds and ReportingOrganization__c IN: setOrgIds];
        } else
        {
            lstCase = [select PACaseType__c, AccountId, Account.Name, Account.BillingCountry, Account.Country__r.Name, Status from Case Where AccountId IN: setAccIds];
        }
        for (Case xResult : lstCase) {
            openCasesWrapperClass oCWC = new openCasesWrapperClass();
            if(mapOpenCases.get(xResult.AccountId) != null) {
                oCWC = mapOpenCases.get(xResult.AccountId);
            }
            //if(xResult.Status == 'New' || xResult.Status == 'In Progress') {
            if(PA_UtilityClass.getOpenCaseOptionSet().contains(xResult.Status)) {
                if(xResult.PACaseType__c ==  system.label.CLQ316PA029) {
                    if(oCWC.totalOpenTECCases == null) {
                        oCWC.totalOpenTECCases = 1;
                    }else
                    {
                        oCWC.totalOpenTECCases += 1;
                    }
                    if(oCWC.totalOpenCases == null) {
                        oCWC.totalOpenCases = 1;
                    }else
                    {
                        oCWC.totalOpenCases += 1;
                    }
                }else
                if(xResult.PACaseType__c == system.label.CLQ316PA040) {
                    if(oCWC.totalOpenPERCases == null) {
                        oCWC.totalOpenPERCases = 1;
                    }else
                    {
                        oCWC.totalOpenPERCases += 1;
                    }
                    if(oCWC.totalOpenCases == null) {
                        oCWC.totalOpenCases = 1;
                    }else
                    {
                        oCWC.totalOpenCases += 1;
                    }
                }else
                if(xResult.PACaseType__c == system.label.CLQ316PA031) {
                    if(oCWC.totalOpenRWSCases == null) {
                        oCWC.totalOpenRWSCases = 1;
                    }else
                    {
                        oCWC.totalOpenRWSCases += 1;
                    }
                    if(oCWC.totalOpenCases == null) {
                        oCWC.totalOpenCases = 1;
                    }else
                    {
                        oCWC.totalOpenCases += 1;
                    }
                }
            }
            if(oCWC.totalOpenClosedCases == null) {
                oCWC.totalOpenClosedCases = 1;
            }else
            {
                oCWC.totalOpenClosedCases += 1;
            }
            mapOpenCases.put(xResult.AccountId, oCWC);
        }
        for(openCasesWrapperClass xCase : mapOpenCases.values()) {
            if(!mapOpenCasesSorted.containsKey(xCase.Country)) {
                mapOpenCasesSorted.put(xCase.Country, new List<openCasesWrapperClass>());
            }
            mapOpenCasesSorted.get(xCase.Country).add(xCase);
        }
        for(String xCountry : lstCountry) {
            if(mapOpenCasesSorted.get(xCountry) != null) {
                lstCases.addAll(mapOpenCasesSorted.get(xCountry));
            }
        }
        return lstCases;
    }
    
    Public class openCasesWrapperClass {
        Public String Country;
        Public String AccountName;
        Public String AccountId;
        Public Integer totalOpenCases;
        Public Integer totalOpenTECCases;
        Public Integer totalOpenPERCases;
        Public Integer totalOpenRWSCases;
        Public Integer totalOpenClosedCases;
    }
}