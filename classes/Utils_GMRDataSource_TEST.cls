@Istest

class Utils_GMRDataSource_TEST
{
    static testMethod void GMRDataSource_TestMethod()
    {
       // Utils_DataSource GMRDataSource = Utils_Factory.getDataSource('GMR');
        
        List<String> DummyList = new List<String>();
        List<String> KeyWord = new List<String>();
        KeyWord.add('Test');
        DummyList.add('Test');
        Long noOfRecords = 23;
        WS04_GMR.labelValue lblValue= new WS04_GMR.labelValue();
        lblValue.value = 'Test Description';
        lblValue.label = 'Test Description';
        
        WS04_GMR.labelValue lblValue1= new WS04_GMR.labelValue();
        lblValue1.value = '--';
        lblValue1.label = '--';
        
        WS04_GMR.returnDataBean returnDataBean = new WS04_GMR.returnDataBean();
        
        returnDataBean.errorNumber = noOfRecords;
        returnDataBean.numberOfPage = 5;
        returnDataBean.numberOfRecord = noOfRecords ;
        returnDataBean.returnCode = '20';
        returnDataBean.functionalErrorMessage = 'Test Message';
        returnDataBean.returnMessage = 'Test Message';
        
        WS04_GMR.gmrDataBean dataBean = new WS04_GMR.gmrDataBean();
        dataBean.commercialReference = 'Test Commercial REference';
        dataBean.description = 'Test Description';
        dataBean.businessLine = lblValue1;
        dataBean.productLine = lblValue;
        dataBean.strategicProductFamily = lblValue;
        dataBean.family = lblValue;
        dataBean.subFamily = lblValue;
        dataBean.productSuccession = lblValue;
        dataBean.productGroup = lblValue;
        list<WS04_GMR.gmrDataBean> lstDataBean = new list<WS04_GMR.gmrDataBean>();
        lstDataBean.add(dataBean);
                        
        Utils_GMRDataSource gmr = new Utils_GMRDataSource();
        gmr.Results = new WS04_GMR.resultDataBean();
        gmr.Results.returnDataBean = returnDataBean;
        gmr.Results.gmrDataBeanList = lstDataBean;
        
        List<SelectOption> Columns = gmr.getColumns();
        Utils_DataSource.Result Results = gmr.search(KeyWord , DummyList);
        DummyList.add('Test1');
        Results = gmr.search(KeyWord , DummyList);
        DummyList.add('This is Test 2');
        Results = gmr.search(KeyWord , DummyList);
        DummyList.add('This is Test3');
        Results = gmr.search(KeyWord , DummyList);
    }
}