@isTest
public class VFC_PRM_PropertiesRetroCalculation_TEST{
	public static testMethod void testMigration(){
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;
        user1.ByPassVR__c=true;
        System.runas(user1){
			FieloEE.MockUpFactory.setCustomProperties(false);
	        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
	            FieloEE__Member__c = false
	        );
	        insert deactivate;
	        PageReference pageRef = Page.VFP_PRM_PropertiesRetroCalculationPage;
	        Test.setCurrentPage(pageRef);
	        VFC_PRM_PropertiesRetroCalculation myController = new VFC_PRM_PropertiesRetroCalculation();
	        System.assertEquals(false,myController.migrationEnqueued);
	        System.assertEquals(false,myController.internalPropertiesEnqueued);
	        myController.refresh();

	        Country__c country = Utils_TestMethods.createCountry();
	        country.countrycode__c ='ASM';
	        insert country;
	        
	        Country__c country1 = Utils_TestMethods.createCountry();
	        country1.countrycode__c ='ABC';
	        insert country1;

	        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
	        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
	        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
	        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
	        newRecTypeMap.name = recCatalogActivityReq.Id;
	        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
	        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

	        insert newRecTypeMap;
	        
	        //creates a global partner program
	        PartnerPRogram__c gp1 = new PartnerPRogram__c();
	        gp1 = Utils_TestMethods.createPartnerProgram();
	        gp1.TECH_CountriesId__c = country.id;
	        gp1.recordtypeid = Label.CLMAY13PRM15;
	        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
	        insert gp1;

	        //creates a program level for the global program
	        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
	        insert prg1;
	        prg1.levelstatus__c = 'active';
	        update prg1;

	        //creates a country partner program
	        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
	        insert cp1;
	        cp1.ProgramStatus__c = 'active';
	        update cp1;

	        //creates a program level for the country partner program
	        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
	        insert prg2;


	        myController.migrationIdList =prg2.Id;
	        Test.startTest();
	        myController.retroCalculateMigration();
	        Test.stopTest();
	    }
    }


    public static testMethod void testInternalProperties(){

    	PageReference pageRef = Page.VFP_PRM_PropertiesRetroCalculationPage;
        Test.setCurrentPage(pageRef);
        VFC_PRM_PropertiesRetroCalculation myController = new VFC_PRM_PropertiesRetroCalculation();
        

    	User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;

        System.runAs(us){  
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = Utils_TestMethods.createCountry();
	        france.countrycode__c ='ASM';
	        insert france;

            Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName ='BFOProperties'].Id;
            ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,TableName__c='Contact',Type__c='BFOProperties',PRMCountryFieldName__c='PRMCountry__c',FieldName__c='PRMFirstName__c',SubType__c='ValidEmail',PropertyName__c='ValidEmail',MinimumLengthCheck__c =0);
            insert catalog;
            List<PRMBadgeBFOProperties__c> propList = new List<PRMBadgeBFOProperties__c>();
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Starts with',Value__c='Starts with',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Contains',Value__c='Contains',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Ends with',Value__c='Ends with',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            insert propList;

	    	myController.internalPropertiesList =propList[0].Id+','+propList[1].Id+','+propList[2].Id;
	    	myController.objectName='Contact';
	        Test.startTest();
	        myController.retroCalculateInternalProperties();
	        Test.stopTest();
	    }
    }
}