public without sharing class Utils_RecordSharing 
{
    /**
        Class to Share record with User/Group
    */
    
    //Wrapper class for recordshare
    public class RecordShare
    {        
        public String strUserId;
        public SObject sobjToObject;
        public String strToRecordId;
        public String strRowCause;
        public String strRecordAccessLevel;
    }   
    
    public static void createShareObjectRecordInSFDC(List<RecordShare> lstRecordShare)
    {
        List<SObject> lstSobj=new List<SObject>();
        for(RecordShare rs1:lstRecordShare)
        {
           rs1.sobjToObject.put('ParentId',rs1.strToRecordId);          
           rs1.sobjToObject.put('AccessLevel', rs1.strRecordAccessLevel);
           rs1.sobjToObject.put('UserOrGroupId', rs1.strUserId);
           rs1.sobjToObject.put('RowCause',rs1.strRowCause); 
           lstSobj.add(rs1.sobjToObject);
           
        }
        if(lstSobj.size()>0)
        {
            try
            {
                insert lstSobj;
            }
            catch(Exception e)
            {
                System.debug('...@...Exception in Utils_RecordSharing.createShareObjectRecordInSFDC()' );
                system.debug('....@@...'+e);
            }
        
        }
    }
}