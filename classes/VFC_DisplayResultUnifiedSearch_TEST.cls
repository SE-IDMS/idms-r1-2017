@isTest
public class VFC_DisplayResultUnifiedSearch_TEST {

    static testMethod void DisplayCRDetails2TestMethod() {
        
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG1234'; 
        
        Country__c country1=new Country__c(CountryCode__c='US', Name='United States');  
        insert country1;
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.Country__c = country1.Id;
        insert runAsUser;
        
        System.runAs(runAsUser){
            Test.startTest();
            string prefix='aa';
            User  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
            Database.insert(sampleUser);
            
            Country__c objCountry = Utils_TestMethods.createCountry();
            objCountry.Name='TCY'; 
            objCountry.CountryCode__c='TCY';
            Database.insert(objCountry);
            
            Account objAccount = Utils_TestMethods.createAccount();
            objAccount.Country__c = objCountry.Id;
            Database.insert(objAccount);
            
            Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
            Database.insert(objContact);
            
            Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
            objCase.CaseSymptom__c= 'Symptom';
            objCase.CaseSubSymptom__c= 'SubSymptom';
            objCase.CommercialReference__c = commRefTOTest;
            objCase.TECH_GMRCode__c = gmrCodeToTest;
            objCase.ProductBU__c = 'Business';
            objCase.ProductLine__c = 'ProductLine';
            objCase.ProductFamily__c ='ProductFamily';
            objCase.Family__c ='Family';
            Database.insert(objCase);
               
            BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
            Database.insert(breEntity);
            BusinessRiskEscalationEntity__c breSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
            Database.insert(breSubEntity);
            BusinessRiskEscalationEntity__c breLocationType = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
            Database.insert(breLocationType);
            
            List<CS_Severity__c> lstCS = new List<CS_Severity__c>();
            CS_Severity__c CS1 = new CS_Severity__c(Name = 'Minor', Value__c = 1);
            lstCS.add(CS1);
            CS_Severity__c CS2 = new CS_Severity__c(Name = 'Safety related', Value__c = 10);
            lstCS.add(CS2);
            Database.insert(lstCS);
             
            Problem__c prob = Utils_TestMethods.createProblem(breLocationType.Id,12);
            prob.ProblemLeader__c = sampleUser.id;
            prob.ProductQualityProblem__c = false;
            prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
            prob.RecordTypeid = Label.CLI2PAPR120014;
            prob.Sensitivity__c = 'Public';
            prob.Severity__c = 'Safety related';
            Database.insert(prob); 
            
            CommercialReference__c crs = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                    CommercialReference__c = commRefTOTest,Family__c = 'Family', 
                                                                    GMRCode__c = gmrCodeToTest,ProductLine__c = 'ProductLine', 
                                                                    ProductFamily__c ='ProductFamily');
            Database.insert(crs);
            CommercialReference__c crs1 = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                    CommercialReference__c = '',Family__c = 'Family', 
                                                                    GMRCode__c = gmrCodeToTest.substring(8),ProductLine__c = 'ProductLine', 
                                                                    ProductFamily__c ='ProductFamily');
            Database.insert(crs1);
            
            List<problem__c> problemList=new List<problem__c>();
            for (Integer i=0; i<10;i++) {
                problem__c pr1=new problem__c(Severity__c=Label.CLOCT14I2P22, RecordTypeid = Label.CLI2PAPR120014,Title__c='Test1 '+i, TECH_Symptoms__c = 'Symptom - SubSymptom');
                problemList.add(pr1);
            }
            for (Integer i=0; i<Integer.valueOf(Label.CLOCT14I2P21)+10;i++) {
                problem__c pr1=new problem__c(Severity__c='Significant / High', RecordTypeid = Label.CLI2PAPR120014,Title__c='Test2 '+i, TECH_Symptoms__c = 'Symptom - SubSymptom');
                problemList.add(pr1);
            }
            
            insert problemList;
            
            List<CommercialReference__c> commRefList= new List<CommercialReference__c>();
            for (Problem__c prb:problemList) {
                CommercialReference__c cr1=new CommercialReference__c(gmrcode__c=gmrCodeToTest, ProductLine__c = 'ProductLine', commercialreference__c=commRefTOTest, problem__c=prb.id);
                commRefList.add(cr1);
            }
            insert commRefList;
            
            BusinessRiskEscalationEntity__c br = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocationReturnCenter();
            br.RCCountry__c = country1.Id;
            br.Location__c = 'Location 1234';
            Database.insert(br,false);
         
            List<RMAShippingToAdmin__c> rmaList=new List<RMAShippingToAdmin__c>();
            for (Integer i=0; i<Integer.valueOf(Label.CLOCT14I2P23)+10;i++) {     
                 RMAShippingToAdmin__c rma1=new RMAShippingToAdmin__c(Ruleactive__c=True, TECH_GMRCode__c=gmrCodeToTest, CommercialReference__c=commRefTOTest,Country__c=country1.id, ReturnCenter__c=br.id);          
                 rmalist.add(rma1);
            }
            for (Integer i=0; i<2;i++) {     
                 RMAShippingToAdmin__c rma1=new RMAShippingToAdmin__c(Ruleactive__c=True, TECH_GMRCode__c=gmrCodeToTest,Country__c=country1.id, ReturnCenter__c=br.id);          
                 rmalist.add(rma1);
            }
            for (Integer i=0; i<2;i++) {     
                 RMAShippingToAdmin__c rma1=new RMAShippingToAdmin__c(Ruleactive__c=True, TECH_GMRCode__c=gmrCodeToTest,Country__c=country1.id, ReturnCenter__c=br.id);           
                 rmalist.add(rma1);
            } 
            insert rmalist;
            
            PageReference pageRef = Page.VFP_DisplayResultUnifiedSearch;
            pageRef.getParameters().put('gmrCode',gmrCodeToTest);
            pageRef.getParameters().put('commRef',commRefTOTest); 
            pageRef.getParameters().put('Mode',Label.CLOCT14I2P24);
            pageRef.getParameters().put('source','case');
            pageRef.getParameters().put('id',objCase.id);
            Test.setCurrentPage(pageRef);
            VFC_DisplayResultUnifiedSearch displayPage = new VFC_DisplayResultUnifiedSearch();
            
            displayPage.gotoReport();
            displayPage.goBack();
            displayPage.goBackToCase();
            displayPage.strhdn = prob.Id;
            displayPage.addProblemLinkedCase();
            displayPage.getNumberForSortRC(rmalist[0]);
            displayPage.sortRCs(rmaList);
           
            PageReference pageRef1 = Page.VFP_DisplayResultUnifiedSearch;
            pageRef1.getParameters().put('gmrCode',gmrCodeToTest); 
            pageRef1.getParameters().put('commRef',''); 
            pageRef1.getParameters().put('Mode',Label.CLOCT14I2P25);
            pageRef1.getParameters().put('source','SFRP');
            pageRef1.getParameters().put('id',objCase.id);
            Test.setCurrentPage(pageRef1);
            VFC_DisplayResultUnifiedSearch displayPage1 = new VFC_DisplayResultUnifiedSearch();
            
            displayPage1.gotoReport();
            displayPage1.goBack();
            displayPage1.goBackToCase();
            displayPage1.strhdn = prob.Id;
            displayPage1.addProblemLinkedCase();
            displayPage1.getNumberForSortRC(rmalist[0]);
            displayPage1.sortRCs(rmaList);
                        
            PageReference pageRef2 = Page.VFP_DisplayResultUnifiedSearch;
            pageRef2.getParameters().put('gmrCode',gmrCodeToTest); 
            pageRef2.getParameters().put('commRef',commRefTOTest); 
            pageRef2.getParameters().put('Mode',Label.CLOCT14I2P24);
            pageRef2.getParameters().put('source','');
            pageRef2.getParameters().put('id',objCase.id);
            Test.setCurrentPage(pageRef2);
            VFC_DisplayResultUnifiedSearch displayPage2 = new VFC_DisplayResultUnifiedSearch();
            
            List<problem__c> problemList2=new List<problem__c>();
            for (Integer i=0; i<Integer.valueOf(Label.CLOCT14I2P21)+2;i++) {
                problem__c pr1=new problem__c(Severity__c=Label.CLOCT14I2P22, Title__c='Test1 '+i);
                problemList2.add(pr1);
            }
            insert problemList2;
    
            PageReference pageRef3 = Page.VFP_DisplayResultUnifiedSearch;
            pageRef3.getParameters().put('gmrCode',gmrCodeToTest);  
            pageRef3.getParameters().put('Mode',Label.CLOCT14I2P24);
            pageRef3.getParameters().put('source','case');
            pageRef3.getParameters().put('id',objCase.id);
         
            Test.setCurrentPage(pageRef3);
            VFC_DisplayResultUnifiedSearch displayPage3 = new VFC_DisplayResultUnifiedSearch();
            System.assertEquals(displayPage3.gmrCode, gmrCodeToTest);
            System.assertEquals(displayPage3.mode, Label.CLOCT14I2P24);
    
            
         
            PageReference pageRef4 = Page.VFP_DisplayResultUnifiedSearch;
            Test.setCurrentPage(pageRef4);
            VFC_DisplayResultUnifiedSearch displayPage4 = new VFC_DisplayResultUnifiedSearch();
            Test.stopTest();
        }
    }
}