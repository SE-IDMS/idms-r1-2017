/*
    Author          : Shruti Karn
    Date Created    : 18/12/2012
    Description     : Test class for AP_ATT_UpdateTechnicia
*/
@isTest(seealldata=true)
public class AP_ATT_UpdateTechnician_TEST {

   static testMethod void testATT(){
    
        Profile profile = [Select id from Profile where name ='SE - SRV Standard User (technician)' limit 1];
        User ServiceUser =  Utils_TestMethods.createStandardUser('VFCWO');
        ServiceUser.profileId = profile.Id;
        insert ServiceUser;

        SVMXC__Service_Group__c ServiceGrp = Utils_TestMethods.createServiceGrp();
        insert ServiceGrp;
        SVMXC__Service_Group_Members__c newTech = Utils_TestMethods.createTech_Equip(ServiceUser.Id,ServiceGrp.Id);
        newTech.recordtypeid = Label.CLDEC12SRV39;
        insert newTech;
               
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        SVMXC__Service_Order__c testWorkOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
        testWorkOrder.SVMXC__Order_Status__c = 'Acknowledge FSE';
        testWorkOrder.TechPreviousOwner__c = userinfo.getUserId();
        insert testWorkOrder;
        
        AssignedToolsTechnicians__c newTools = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech.Id);
        try{
        insert newTools;
            
        }
            catch (exception e){}
        
       
        AssignedToolsTechnicians__c newTools1 = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech.Id);
         try{
        insert newTools1;
        AP_ATT_UpdateTechnician.updatePrimaryFSR(new list<AssignedToolsTechnicians__c>{newTools1});
        test.startTest();
        newTools1.status__c = 'Rejected';
             newTools1.PrimaryUser__c=true;
        update newTools1;
             
        test.stopTest();
        }
            catch (exception e){}
            
   
    }
    
    static testMethod void testATT1(){
    
        Profile profile = [Select id from Profile where name ='SE - SRV Standard User (technician)' limit 1];
        User ServiceUser =  Utils_TestMethods.createStandardUser('VFCWO');
        ServiceUser.profileId = profile.Id;
        insert ServiceUser;

        SVMXC__Service_Group__c ServiceGrp = Utils_TestMethods.createServiceGrp();
        insert ServiceGrp;
        SVMXC__Service_Group_Members__c newTech = Utils_TestMethods.createTech_Equip(ServiceUser.Id,ServiceGrp.Id);
        newTech.recordtypeid = Label.CLDEC12SRV39;
        insert newTech;
               
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        SVMXC__Service_Order__c testWorkOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
        testWorkOrder.SVMXC__Order_Status__c = 'Acknowledge FSE';
        testWorkOrder.TechPreviousOwner__c = userinfo.getUserId();
        insert testWorkOrder;
        
        AssignedToolsTechnicians__c newTools = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech.Id);
        try{
        insert newTools;
            
        }
            catch (exception e){}
        
       
        AssignedToolsTechnicians__c newTools1 = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech.Id);
         try{
            newTools1.status__c = 'Assigned';
        insert newTools1;
        test.startTest();
        newTools1.status__c = 'Accepted';
        update newTools1;
        test.stopTest();
        }
            catch (exception e){}
   
    }
    static testMethod void testATT2(){
    
        Profile profile = [Select id from Profile where name ='SE - SRV Standard User (technician)' limit 1];
        User ServiceUser =  Utils_TestMethods.createStandardUser('VFCWO');
        ServiceUser.profileId = profile.Id;
        insert ServiceUser;
        User ServiceUser2 =  Utils_TestMethods.createStandardUser('VFCWO');
        ServiceUser2.profileId = profile.Id;
        insert ServiceUser2;
        User user = new User(alias = 'user', email='user' + '@schneider-electric.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_ACC_PartnerAccountUpdate;AP_WorkOrderTechnicianNotification;SRV04;SVMX06;SVMX07;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){
        SVMXC__Service_Group__c ServiceGrp = Utils_TestMethods.createServiceGrp();
        insert ServiceGrp;
        SVMXC__Service_Group_Members__c newTech = Utils_TestMethods.createTech_Equip(ServiceUser.Id,ServiceGrp.Id);
        newTech.recordtypeid = Label.CLDEC12SRV39;
        insert newTech;  
        SVMXC__Service_Group_Members__c newTech2 = Utils_TestMethods.createTech_Equip(ServiceUser2.Id,ServiceGrp.Id);
        newTech2.recordtypeid = Label.CLDEC12SRV39;
        insert newTech2;
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        SVMXC__Service_Order__c testWorkOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
        testWorkOrder.AcceptedFSRIds__c = ServiceUser.id;
        testWorkOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
        testWorkOrder.TechPreviousOwner__c = userinfo.getUserId();
        insert testWorkOrder;        
        //AssignedToolsTechnicians__c newTool1 = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech.Id);
        AssignedToolsTechnicians__c newTool1 = new AssignedToolsTechnicians__c(WorkOrder__c=testWorkOrder.id,TechnicianEquipment__c=newTech.id);
        newTool1.status__c ='Assigned';
        newTool1.FSR_Salesforce_User__c =ServiceUser.id;
        newTool1.PrimaryUser__c = true;
       insert newTool1;
        AssignedToolsTechnicians__c newTool2 = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech2.Id);
        newTool2.status__c ='Assigned';
        newTool2.FSR_Salesforce_User__c =ServiceUser2.id;
       // insert newTool2;
        newTool1.Status__c ='Accepted';
        Utils_SDF_Methodology.removeFromRunOnce('AP_ATT_UpdateTechnician');
        Test.starttest();
        update newTool1;
        newTool2.Status__c ='Accepted';
       // update newTool2;
        newTool1.Status__c ='Rejected';
        Utils_SDF_Methodology.removeFromRunOnce('AP_ATT_UpdateTechnician');
       update newTool1;
        System.debug('\n hari log *********************'+newTool1);   
        
        
        map<id,AssignedToolsTechnicians__c> tmap= new map<id,AssignedToolsTechnicians__c>();
        tmap.putAll(new list<AssignedToolsTechnicians__c>{newTool1});
        AP_ATT_UpdateTechnician.updateTechnicianStatus2(tmap);
        
        Utils_SDF_Methodology.removeFromRunOnce('AP_ATT_UpdateTechnician'); 
        newTool1.Status__c ='Rejected';
        map<id,AssignedToolsTechnicians__c> tmap1= new map<id,AssignedToolsTechnicians__c>();
        tmap1.putAll(new list<AssignedToolsTechnicians__c>{newTool1});
        AP_ATT_UpdateTechnician.updateTechnicianStatus2(tmap1);
        newTool1.Status__c ='Rejected';
        AP_ATT_UpdateTechnician.updateTechnicianStatus2(tmap1);
        String cf='';
        String st='asdf';
        st=AP_ATT_UpdateTechnician.updateAcceptFSRIdsField(cf,st,'add');
        st=AP_ATT_UpdateTechnician.updateAcceptFSRIdsField(cf,st,'remove');
        update newTool1;
        Test.stoptest();
        }
        /*AssignedToolsTechnicians__c newTool2 = Utils_TestMethods.CreateAssignedTools_Technician(testWorkOrder.Id, newTech2.Id);
        newTool2.status__c ='Assigned';
        insert newTool2;
        newTool2.Status__c ='Accepted';
        update newTool2;*/
        
        /*String cf='';
        String st='asdf';
        st=AP_ATT_UpdateTechnician.updateAcceptFSRIdsField(cf,st,'add');
        st=AP_ATT_UpdateTechnician.updateAcceptFSRIdsField(cf,st,'remove');*/
    
    }
}