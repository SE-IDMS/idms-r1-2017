public class AP_PsAssignmentMethods {

    public static String getAssigneeId(PermissionSetAssignmentRequests__c psRequest) {
        return psRequest.Assignee__c != null ?psRequest.Assignee__c : psRequest.PendingAssigneeId__c;    
    }

    public static void deleteAssignment(List<PermissionSetAssignmentRequests__c> psAssignmentRequests ) {
        
        Set<Id> userIdKeySet = new Set<Id>();
        Set<Id> PsIdKeySet = new Set<Id>();
        Set<String> uniqueKeySet = new Set<String>();
        Set<Id> psAssignmentRequestsIds = new Set<Id>();
        
        for(PermissionSetAssignmentRequests__c psAssignementRequests:psAssignmentRequests ) {
            psAssignmentRequestsIds.add(psAssignementRequests.Id);
        }
        
        system.debug('psAssignmentRequests = ' + psAssignmentRequestsIds);
        
        /*
        List<PermissionSetAssignmentRequests__c> psAssignmentRequests = [SELECT Id, Assignee__c, PendingAssigneeId__c, PermissionSetGroup__c
                                                                         FROM PermissionSetAssignmentRequests__c 
                                                                         WHERE Id IN :psAssignmentRequestsIds];
        */
        
        
        system.debug('psAssignmentRequests  = ' + psAssignmentRequests );
        
        Map<Id, List<Id>> permissionSetGroupMap = new Map<Id, List<Id>>(); // Map User with related Permission Set Group
        Map<Id, List<PermissionSetGroup__c>> assignedPsGroupUserMap = new Map<Id, List<PermissionSetGroup__c>>(); // Map User with existing Permission Set Groups
        Map<Id, PermissionSetGroup__c> psGroupMap = new Map<Id, PermissionSetGroup__c>(); // Map Permission Set Group Id with Permission Set Group
        Set<Id> permissionSetGroupIdSet = new Set<Id>();
        
        Map<Id, Set<Id>> existingPsMemberMap = new Map<Id, Set<Id>>();
                
        for(PermissionSetAssignmentRequests__c psRequest:psAssignmentRequests) {

            String assigneeId = getAssigneeId(psRequest);
            userIdKeySet.add(assigneeId );
            permissionSetGroupIdSet.add(psRequest.PermissionSetGroup__c);
            
            system.debug('assigneeId = ' + assigneeId );
            
            if(permissionSetGroupMap.get(assigneeId ) == null) {
                permissionSetGroupMap.put(assigneeId , new List<ID>{psRequest.PermissionSetGroup__c});
                
                system.debug('assignee get null = ' + psRequest);
                
            }    
            else {
                permissionSetGroupMap.get(assigneeId ).add(psRequest.PermissionSetGroup__c);
                
                system.debug('assignee get not null= ' + psRequest);
            }        
        }

        system.debug('permissionSetGroupMap = ' + permissionSetGroupMap);

        if(!psAssignmentRequests.isEmpty()) {
            for(PermissionSetGroup__c permissionSetGroup:[SELECT Id, Name, RequiresValidation__c,  RichDescription__c, TECH_IsAssigned__c,
                                                           (SELECT Id, Name, PermissionSetId__c FROM PermissionSetGroupMembers__r),                               
                                                           MemberList__c, Type__c, Stream__c FROM PermissionSetGroup__c 
                                                           WHERE Id IN :permissionSetGroupIdSet]) { 
                
                psGroupMap.put(permissionSetGroup.Id, permissionSetGroup);                                      
            }
        }

        system.debug('psGroupMap = ' + psGroupMap);

        for(PermissionSetGroup__c permissionSetGroup:[SELECT Id, Name, RequiresValidation__c,  RichDescription__c, TECH_IsAssigned__c,
                                                      (SELECT Id, Name, PermissionSetId__c FROM PermissionSetGroupMembers__r),
                                                      (SELECT Id, PendingAssigneeId__c, Assignee__c FROM PSAssignmentRequests__r 
                                                       WHERE Status__c = 'Assigned' AND (PendingAssigneeId__c IN :userIdKeySet OR Assignee__C IN :userIdKeySet)
                                                       AND Id NOT IN :psAssignmentRequestsIds),                                
                                                       MemberList__c, Type__c, Stream__c FROM PermissionSetGroup__c 
                                                       WHERE Id IN (SELECT PermissionSetGroup__c FROM PermissionSetAssignmentRequests__c
                                                       WHERE Status__c = 'Assigned' AND (PendingAssigneeId__c IN :userIdKeySet OR Assignee__C IN :userIdKeySet))]) { 

            for(PermissionSetAssignmentRequests__c psAssignementRequest:permissionSetGroup.PSAssignmentRequests__r) {

                for(PsGroupMember__c psGroupMember:permissionSetGroup.PermissionSetGroupMembers__r) {
                
                    String assigneeId = getAssigneeId(psAssignementRequest);

                    if(existingPsMemberMap.get(assigneeId ) == null) {
                        existingPsMemberMap.put(assigneeId , 
                                                new Set<Id>{psGroupMember.PermissionSetId__c});
                    }            
                    else {
                        existingPsMemberMap.get(assigneeId).add(psGroupMember.PermissionSetId__c);                     
                    }
                }
            }
        }
        
        system.debug('existingPsMemberMap = ' + existingPsMemberMap);
        
        Set<String> permissionSetAssignmentKeyList = new Set<String>();
        
        Set<Id> userIdSet          = new Set<Id>();
        Set<Id> permissionSetIdSet = new Set<Id>();
        
        for(Id userId:permissionSetGroupMap.keyset()) {
            for(Id psGroupId:permissionSetGroupMap.get(userId)) {
                PermissionSetGroup__c psGroup = psGroupMap.get(psGroupId);
                
                system.debug('psGroup = ' + psGroup );
                
                if(psGroup != null && psGroup.PermissionSetGroupMembers__r != null) {

                
                    for(PsGroupMember__c psGroupMember:psGroup.PermissionSetGroupMembers__r) {
                        if(existingPsMemberMap.get(userId) == null || (existingPsMemberMap.get(userId) != null 
                            && !existingPsMemberMap.get(userId).contains(psGroupMember.PermissionSetId__c))) {
                            
                            permissionSetAssignmentKeyList.add(userId+'.'+psGroupMember.PermissionSetId__c);
                            
                            userIdSet.add(userId);
                            permissionSetIdSet.add(psGroupMember.PermissionSetId__c);
                        }
                    }                
                }
            }
        }
        
        system.debug('permissionSetAssignmentKeyList = ' + permissionSetAssignmentKeyList );
        
        deletePermissionSetAssignments(permissionSetAssignmentKeyList, permissionSetIdSet, userIdSet);
        
    }

    public static void assignPermissionSet(Set<Id> psAssignmentRequestsIds) {
        if(!System.isFuture() && !System.isBatch()) {
            assignPermissionSetAsync(psAssignmentRequestsIds);
        }
    }
    
    @future 
    public static void deletePermissionSetAssignments(Set<String> permissionSetAssignmentKeyList, Set<Id> permissionSetIdSet, Set<Id> userIdSet) {
        
        List<PermissionSetAssignment> deletePsAssignmentList = new List<PermissionSetAssignment>();
        
        for(PermissionSetAssignment psAssignment:[SELECT Id, PermissionSetId, AssigneeId 
                                                 FROM PermissionSetAssignment WHERE AssigneeId IN :userIdSet
                                                 AND PermissionSetId IN :permissionSetIdSet]) {
            
            if(permissionSetAssignmentKeyList.contains(psAssignment.AssigneeId+'.'+psAssignment.PermissionSetId)) {
                deletePsAssignmentList.add(psAssignment);
            }
        }
        
        system.debug('deletePsAssignmentList = ' + deletePsAssignmentList );
        
        if(!deletePsAssignmentList.isEmpty()) {
            delete deletePsAssignmentList;
        }    
    }
    
    @future
    public static void assignPermissionSetAsync(Set<Id> psAssignmentRequestsIds) {
        assignPermissionSetSync(psAssignmentRequestsIds);
    }

    public static void assignPermissionSet(List<PermissionSetAssignmentRequests__c> psAssignmentRequests) {

        List<PermissionSetAssignment> psAssignmentList = new List<PermissionSetAssignment>();
        Set<PermissionSetAssignment> psAssignmentSet = new Set<PermissionSetAssignment>();
        Map<Id, List<Id>> permissionSetGroupMap = new Map<Id, List<Id>>();
        
        for(PermissionSetAssignmentRequests__c psRequest:psAssignmentRequests) {
 
            psRequest.Status__c = 'Assigned';
            String assigneeId = psRequest.Assignee__c != null ? psRequest.Assignee__c : psRequest.PendingAssigneeId__c;
 
            if(!permissionSetGroupMap.containsKey(psRequest.PermissionSetGroup__c)) {
                permissionSetGroupMap.put(psRequest.PermissionSetGroup__c, new List<ID>{assigneeId});
            }    
            else {
                permissionSetGroupMap.get(psRequest.PermissionSetGroup__c).add(assigneeId);
            }        
        }
        
        system.debug('psAssignmentRequests = ' + psAssignmentRequests);
        system.debug('permissionSetGroupMap = ' + permissionSetGroupMap);
                
        if(!psAssignmentRequests.isEmpty()) {
            for(PermissionSetGroup__c permissionSetGroup:[SELECT Id, Name, RequiresValidation__c,  RichDescription__c, TECH_IsAssigned__c,
                                                           (SELECT Id, Name, PermissionSetId__c FROM PermissionSetGroupMembers__r),                               
                                                           MemberList__c, Type__c, Stream__c FROM PermissionSetGroup__c 
                                                           WHERE Id IN :permissionSetGroupMap.keySet()]) { 
                
                for(Id userId:permissionSetGroupMap.get(permissionSetGroup.Id)) {
                    for(PsGroupMember__c psMember:permissionSetGroup.PermissionSetGroupMembers__r) {
                        psAssignmentSet.add(new PermissionSetAssignment(PermissionSetId = psMember.PermissionSetId__c, AssigneeId = userId));
                    }                
                }                                       
            }
        }
        
        
       
        if(!psAssignmentSet.isEmpty()) {
            psAssignmentList.addAll(psAssignmentSet);
            List<Database.SaveResult> saveResultList = database.insert(psAssignmentList, false);   
            
            for(Database.SaveResult sr:saveResultList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted PermissionSetAssignment. ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('PermissionSetAssignment fields that affected this error: ' + err.getFields());
                    }
                }     
            }
        }
    
    }

    public static void assignPermissionSetSync(Set<Id> psAssignmentRequestsIds) {
        
        List<PermissionSetAssignmentRequests__c> psAssignmentRequests = [SELECT Id, PendingAssigneeId__c, Assignee__c, PermissionSetGroup__c
                                                                         FROM PermissionSetAssignmentRequests__c 
                                                                         WHERE Id IN :psAssignmentRequestsIds];
        assignPermissionSet(psAssignmentRequests); 
    }
}