/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
@isTest
public without sharing class FieloPRM_Batch_MemberPropProcessTest {
    public testMethod static void testUnit() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            
            Account acc = new Account(Name = 'acc');
            insert acc;

            FieloEE__Member__c member = new FieloEE__Member__c(FieloEE__LastName__c= 'Polo', 
                                                               FieloEE__FirstName__c = 'Marco', 
                                                               FieloEE__Street__c = 'test', 
                                                               FieloEE__Program__c = program.id, 
                                                               F_Account__c = acc.Id);
            insert member;

            ExternalPropertiesCatalog__c cat = new ExternalPropertiesCatalog__c(FieldName__c = 'Name');
            insert cat;

            CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberPropProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;


            MemberExternalProperties__c mep = new MemberExternalProperties__c(ExternalId__c ='BATCHTEST1', 
                                                                  FunctionalKey__c = 'BATCHTEST', 
                                                                  PRMUIMSId__c = 'BATCHTEST1', 
                                                                  Member__c = member.Id, 
                                                                  ExternalPropertiesCatalog__c = cat.Id,
                                                                  F_PRM_TryCount__c = 0,
                                                                  F_PRM_IsSynchronic__c = true);
            MemberExternalProperties__c mep2 = new MemberExternalProperties__c(ExternalId__c ='BATCHTEST2', 
                                                                  FunctionalKey__c = 'BATCHTEST2', 
                                                                  PRMUIMSId__c = 'BATCHTEST2', 
                                                                  Member__c = member.Id, 
                                                                  ExternalPropertiesCatalog__c = cat.Id,
                                                                  F_PRM_TryCount__c = 0,
                                                                  F_PRM_IsSynchronic__c = false);
            //insert mep;
            List<MemberExternalProperties__c> meps = new List<MemberExternalProperties__c>();
            meps.add(mep);
            meps.add(mep2);
            insert meps;

            test.starttest();

            Id batchJobId = Database.executeBatch(new FieloPRM_Batch_MemberPropProcess());
                        
            FieloPRM_Batch_MemberPropProcess bat = new FieloPRM_Batch_MemberPropProcess();
            bat.execute(null);
            FieloPRM_Batch_MemberPropProcess.schedule(null);
            meps.get(0).F_PRM_Status__c = 'TODELETE';
            update meps;

            test.stoptest();
        }
    }
    
    public testMethod static void testUnit2() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;

        CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberPropProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
        insert conf;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            test.starttest();

            FieloPRM_Batch_MemberPropProcess.scheduleNow();
            boolean b = FieloPRM_Batch_MemberPropProcess.isRunning(null);

            test.stoptest();
        }
    }
}