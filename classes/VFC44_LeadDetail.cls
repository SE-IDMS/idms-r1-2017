/*
    Author          : Accenture Team
    Date Created    : 18/7/2011 
    Description     : Controller extensions for VFC44_LeadDetail.This class displays the Lead Details, Child Leads & Lead History 
                      related lists.
    18/Apr/2012    Srinivas Nallapati   June MKT   Added  new function "goToCloseRelatedPage"  to redirect user to Close Duplicate page when user cliks on "Close Duplicates"                   
*/
public class VFC44_LeadDetail  extends VFC_ControllerBase
{
    //Iniatialize variables
    public Lead lead{get;private set;}
    public List<Lead> leads{get;set;}
    public List<DataTemplate__c> dataTemplates {get;set;}
    public List<SelectOption> columns{get;set;}
    
    //Constructor
    public VFC44_LeadDetail(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC44_LeadDetail******');
        lead = (Lead)controller.getRecord();
        leads = new List<Lead>();
        dataTemplates = new List<DataTemplate__c>();
        columns = new List<SelectOption>();
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00543));    
        System.Debug('****** Initializing the Properties and Variables Ends for VFC44_LeadDetail******');
    }
    
    
     /* This method queries the Child Leads related to the corresponding Lead 
      * and displays the leads lists or a message if there are no child Leads.
      */  
    
    public pagereference displayChildLeads()
    {
        System.Debug('****** Displaying Child Begins ******');  
        if(lead!=null)
        {
            leads = [select Name, ResponseDate__c, CampaignName__c,keycode__c, PromoTitle__c, ConvertedDate, MarcomType__c from Lead where ParentLead__c =:lead.Id];      
        }
        columns.add(new SelectOption('Field1__c',Schema.sObjectType.Lead.Fields.Name.label)); // First Column
        columns.add(new SelectOption('Field2__c',Schema.sObjectType.Lead.Fields.ResponseDate__c.label));
        columns.add(new SelectOption('Field3__c',Schema.sObjectType.Lead.Fields.Keycode__c.label));
        columns.add(new SelectOption('Field5__c',Schema.sObjectType.Lead.Fields.PromoTitle__c.label));
        columns.add(new SelectOption('Field6__c',Schema.sObjectType.Lead.Fields.CampaignName__c.label));
        columns.add(new SelectOption('Field7__c',Schema.sObjectType.Lead.Fields.MarcomType__c.label)); // Last Column
        for(Lead lead: leads)
        {
            DataTemplate__c dt = new DataTemplate__c();
            if(lead.ConvertedDate!=null)
               dt.field1__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/apex/VFP44_LeadDetail?id='+lead.Id+'" target="_blank">'+lead.Name+'</a>';
            else
                dt.field1__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+lead.Id+'" target="_blank">'+lead.Name+'</a>';
            dt.field2__c = String.valueOf(lead.ResponseDate__c);
            dt.field3__c = lead.Keycode__c;
            dt.field5__c = lead.PromoTitle__c;
            dt.field6__c = lead.CampaignName__c;
            dt.field7__c = lead.MarcomType__c;
            dataTemplates.add(dt);
        }
        System.Debug('****** Displaying Child Leads Ends ******'); 
        return null;        
    }
    
    public PageReference goToCloseRelatedPage(){
        PageReference pg = Page.VFP92_CloseDuplicateLeads;
        pg.getParameters().put('id', lead.id);
        return pg;
    }
}