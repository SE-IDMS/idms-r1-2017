//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This Rest class is used to update user AIL records using API
//*******************************************************************************************

@RestResource(urlMapping='/IDMSUpdateUserAIL/*')
global with sharing class IDMSUpdateUserAILRest {
    //This is Put method to call the service for update user AIL
    @HttpPut  
    global static IDMSResponseWrapperAIL  doPut(IDMSUserAIL__c UserAILRecord){
        IDMSUpdateUserAIL userAil           = new IDMSUpdateUserAIL(); 
        IDMSResponseWrapperAIL response     = userAil.updateUserAIL(UserAILRecord);
        
        //Handle errors with status code 
        RestResponse res = RestContext.response;
        if(response.Message.startsWith('Invalid Operation')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS005);
        }
        if(response.Message.startsWith('Invalid ACL')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS005);
        }
        if(response.Message.startsWith('Missing Mandatory')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS001);
        }         
        if(response.Message.startsWith('User not found')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS002);
        }
        if(response.Message.startsWith('Source and ACL')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS001);
        }
        return response;
    }
    
    
}