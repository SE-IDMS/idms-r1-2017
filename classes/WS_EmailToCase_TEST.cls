@isTest
private class WS_EmailToCase_TEST {
    static testMethod void createCaseTestMethod() {
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c = 'TST';
        insert country;
        
        List<Account> lstAccount = new List<Account>();
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = country.id;
        testAccount.Name = 'Test';
        insert testAccount;
        
        List<Contact> lstContact = new  List<Contact>();
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
        insert testContact;
        
        WS_EmailToCase.ExternalAttachment objExternalAttachment = new WS_EmailToCase.ExternalAttachment();
        objExternalAttachment.strFileName='MyFileName.txt';
        objExternalAttachment.Attchment = Blob.valueof('Test File For Attachment');
        
        List<WS_EmailToCase.ExternalAttachment> lstAttchment = new List<WS_EmailToCase.ExternalAttachment>();
        lstAttchment.add(objExternalAttachment);
        
        
        WS_EmailToCase.caseCreateRequest objCaseCreateRequest = new WS_EmailToCase.caseCreateRequest();
        objCaseCreateRequest.Subject='This is a Test Case';
        objCaseCreateRequest.Category='1 - Pre-Sales Support'; 
        objCaseCreateRequest.Reason='Product Selection';
        objCaseCreateRequest.SubReason='Catalog Selection';
        objCaseCreateRequest.Owner='SESA205823'; 
        objCaseCreateRequest.Customer_Request='This is a Test Case';
        objCaseCreateRequest.CountryCode='TST';
        objCaseCreateRequest.Case_Origin='Email';
        objCaseCreateRequest.Status='New';
        objCaseCreateRequest.CTI_Skill='';
        objCaseCreateRequest.EmailCreationDateTime='';
        objCaseCreateRequest.EmailOwnershipDateTime='';
        objCaseCreateRequest.Priority='Normal';
        objCaseCreateRequest.GenesysInteractionId='GENISD12345';
        objCaseCreateRequest.Attchments=lstAttchment;
        
        Test.startTest();
        WS_EmailToCase.caseResult objCaseResult = WS_EmailToCase.createCase(null);
        objCaseResult = WS_EmailToCase.createCase(objCaseCreateRequest);
        objCaseCreateRequest.ContactId = testContact.Id;
        objCaseResult = WS_EmailToCase.createCase(objCaseCreateRequest);
        objCaseCreateRequest.CaseId = objCaseResult.CaseId;
        objCaseCreateRequest.AnswerToCustomer = 'Answer Available for Customer';
        objCaseResult = WS_EmailToCase.createCase(objCaseCreateRequest);
        objCaseCreateRequest.Status = 'Closed';
        objCaseResult = WS_EmailToCase.createCase(objCaseCreateRequest);
        Test.stopTest();
    }
    static testMethod void closeCaseTestMethod(){
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c = 'TST';
        insert country;
        
        List<Account> lstAccount = new List<Account>();
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = country.id;
        testAccount.Name = 'Test';
        insert testAccount;
        
        List<Contact> lstContact = new  List<Contact>();
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
        insert testContact;
        Test.startTest();
        Case objCase = Utils_TestMethods.createCase(testAccount.id,testContact.id,'Open');
        insert objCase;
        WS_EmailToCase.ExternalAttachment objExternalAttachment = new WS_EmailToCase.ExternalAttachment();
        objExternalAttachment.strFileName='MyFileName.txt';
        objExternalAttachment.Attchment = Blob.valueof('Test File For Attachment');
        
        List<WS_EmailToCase.ExternalAttachment> lstAttchment = new List<WS_EmailToCase.ExternalAttachment>();
        lstAttchment.add(objExternalAttachment);
        
        WS_EmailToCase.closeCaseRequest objcloseCaseRequest = new WS_EmailToCase.closeCaseRequest();
        
        objcloseCaseRequest.Owner='SESA205823';
        objcloseCaseRequest.Attchments=lstAttchment;
    
        WS_EmailToCase.closeCaseResult objcloseCaseResult = WS_EmailToCase.closeCase(null);
        objcloseCaseResult = WS_EmailToCase.closeCase(objcloseCaseRequest);
        objcloseCaseRequest.CaseId='0034577777';
        objcloseCaseResult = WS_EmailToCase.closeCase(objcloseCaseRequest);
        objcloseCaseRequest.CaseId=objCase.Id;
        objcloseCaseResult = WS_EmailToCase.closeCase(objcloseCaseRequest);
        objcloseCaseRequest.AnswerToCustomer ='Closing the Case';
        objcloseCaseResult = WS_EmailToCase.closeCase(objcloseCaseRequest);
        
        Test.stopTest();
    }
    static testMethod void attachCaseTestMethod(){
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c = 'TST';
        insert country;
        
        List<Account> lstAccount = new List<Account>();
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = country.id;
        testAccount.Name = 'Test';
        insert testAccount;
        
        List<Contact> lstContact = new  List<Contact>();
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
        insert testContact;
        Test.startTest();
        Case objCase = Utils_TestMethods.createCase(testAccount.id,testContact.id,'Open');
        insert objCase;
       
        WS_EmailToCase.attachCaseResult objattachCaseResult = WS_EmailToCase.attachCase(objCase.Id);
        Test.stopTest();
    }
    static testMethod void attachContactTestMethod(){
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c = 'TST';
        insert country;
        
        List<Account> lstAccount = new List<Account>();
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = country.id;
        testAccount.Name = 'Test';
        insert testAccount;
        
        
        List<Contact> lstContact = new  List<Contact>();
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
        insert testContact;
        
        Test.startTest();
        WS_EmailToCase.attachContactResult objattachContactResult = WS_EmailToCase.attachContact(testContact.Id);
        Test.stopTest();
    }
}