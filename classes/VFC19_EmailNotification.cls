/*
    Author          : Karthik Shivprakash (ACN) 
    Date Created    : 18/02/2011
    Description     : Controller extension for VFP19_EmailNotification. 
                      This class renders the components in the VFP19_EmailNotification Visualforce pages.
*/

public with sharing class VFC19_EmailNotification {

    private final case caseRecord,caseData;
    
    public Boolean unReadMailFlag {get;set;}
    
    public List<Case> caseList = new List<Case>();
    
    public VFC19_EmailNotification(ApexPages.StandardController controller) {
        this.caseRecord = (Case)controller.getRecord();
        if(caseRecord!=null){
            caseData = [select id,TECH_NumberOfUnreadEmails__c from case where id=:caseRecord.Id Limit 1];
        }
        System.debug('Case Data'+caseData);
        if(caseData!=null){
            checkUnreadEmail();
        }
    }
    
    public void checkUnreadEmail(){
        if(caseData.TECH_NumberOfUnreadEmails__c>0){
            unReadMailFlag=true;
            System.debug('Inside the If'+caseData.TECH_NumberOfUnreadEmails__c);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CL00287));
        }
        else{
            System.debug('In the Else part'+caseData.TECH_NumberOfUnreadEmails__c);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.CL00288));
        }
    }
    
    public pageReference unReadMails(){
        if(caseData.TECH_NumberOfUnreadEmails__c>0){
                caseData.TECH_NumberOfUnreadEmails__c=0;
                caseList.add(caseData);
        }
        if(caseList.size()>0){
                Database.SaveResult[] lup = Database.update(CaseList,true);
                for(Database.SaveResult up : lup)
            {
                if(!up.isSuccess())
                {
                        Database.Error err = up.getErrors()[0];
                        System.Debug('######## VFC19 Error updating : '+err.getStatusCode()+' '+err.getMessage());
                }
            }
        }
        return new PageReference('/'+caseData.Id);
    }
    
       
    
}