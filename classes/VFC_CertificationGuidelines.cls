public with sharing class VFC_CertificationGuidelines {

    public CertificationGuidelines__c certGuide{get;set;}
    public string urlValue{get;set;}
    public Map<String, CSCFWGuidelines__c> CSGuidelines{get;set;}
    public boolean isEdit{get;set;}
    public boolean isSave{get;set;}
    private ApexPages.StandardController controller;
    public boolean submittedForApproval{get;set;}
    
    public VFC_CertificationGuidelines(ApexPages.StandardController controller) {
    
    certGuide = (CertificationGuidelines__c)controller.getRecord();
    this.controller =  controller;
    certGuide = database.query(queryAllFields());
    submittedForApproval = [select SubmittedforApproval__c from CFWRiskAssessment__c where Id=:certGuide.CertificationRiskAssessment__c].SubmittedforApproval__c;
    CSGuidelines = CSCFWGuidelines__c.getAll();
    isEdit = False;
    isSave = True;
    }
    
    
    //QUERY ALL CERTFICATION CHECKLIST FIELDS
    public String queryAllFields()
    {
        Schema.DescribeSObjectResult descRes =  CertificationGuidelines__c.sObjectType.getDescribe();
        List<Schema.SObjectField> tempFields = descRes.fields.getMap().values();
        List<Schema.DescribeFieldResult> fields  = new List<Schema.DescribeFieldResult>();
        for(Schema.SObjectField sof : tempFields)
        {
            fields.add(sof.getDescribe());
        } 
        String query = 'select ';
        for(Schema.DescribeFieldResult dfr : fields)
        {
            query = query + dfr.getName() + ',';
        }
        query = query.subString(0,query.length() - 1);
        query = query + ' from ';
        query = query + descRes.getName();
        query = query +  ' where id = \'';
        query = query + certGuide.id + '\'';
        system.debug('Build Query == ' + query);
        return query;
    }
    
    //REDIRECTS TO THE CORRESPONDING PAGE
    public pagereference urlPageRedirect()
    {
        if(urlValue.startsWith('012'))
            return new Pagereference('/apex/VFP_CFWDetailPage?RecordType='+urlValue+'&id='+certGuide.CertificationRiskAssessment__c);        
        else if(urlValue == 'VFP_CertificationChecklist')
        {
            //CHECKS IF CERTIFICATION CHECKLIST RECORD ALREADY EXISTS, IF NOT, CREATES THE RECORD
            List<CertificationChecklist__c> certAssessments = [select Id,CertificationRiskAssessment__c from CertificationChecklist__c where CertificationRiskAssessment__c=:certGuide.CertificationRiskAssessment__c];
            if(certAssessments!=null && certAssessments.size()>0)
                return new pagereference('/apex/'+urlValue+'?id='+certAssessments[0].Id);
            else
            {
                CertificationChecklist__c certChecklist = new CertificationChecklist__c();
                certChecklist.CertificationRiskAssessment__c = certGuide.CertificationRiskAssessment__c;
                insert certChecklist;
                return new pagereference('/apex/'+urlValue+'?id='+certChecklist.Id);
            } 
        }
        else if(urlValue == 'VFP_CFWGuidelinesPolicies')
        {
            return new pagereference('/apex/'+urlValue+'?id='+certGuide.Id);            
        }
        else
            return new pagereference('/apex/'+urlValue+'?id='+certGuide.CertificationRiskAssessment__c);
    }
    
    
 public pagereference SaveGuidelines()
   {
    
    controller.save();
    pagereference pg;  
    pg = new pagereference('/apex/VFP_CFWGuidelinesPolicies?id=' + certGuide.Id);
    pg.setredirect(true);
    return pg;
   
    }
    
    public pagereference EditGuidelines()
    {
     isEdit = True;
     isSave = False;
     return null;
    }
    
     public pagereference CancelPage()
    {
     isEdit = False;
     isSave = True;
     return null;
    }
}