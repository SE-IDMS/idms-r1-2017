/*
*    @Author : Avi (avidev9@gmail.com)
*    @Description : Unit test class for ApexCodeCoverageList_Con
*
**/
@isTest
private class VFC_ApexCodeCoverageList_Con_Test{
    static testMethod void testApexCodeCoverageList(){
        Test.setMock(HttpCalloutMock.class, new VFC_ApexCodeCoverageList_MockGenerator());
        System.assertNotEquals(NULL,VFC_ApexCodeCoverageList_Con.fetchOrgCoverage());
        System.assertNotEquals(NULL,VFC_ApexCodeCoverageList_Con.fetchCodeCoverage());
        System.assertNotEquals(NULL,VFC_ApexCodeCoverageList_Con.fetchClassOrTriggerById(
                                                                                [
                                                                                    SELECT 
                                                                                        Id 
                                                                                    FROM 
                                                                                        ApexClass 
                                                                                    WHERE 
                                                                                        Name='VFC_ApexCodeCoverageList_Con_Test'
                                                                                    ].Id)
                                                                                );
    }
}