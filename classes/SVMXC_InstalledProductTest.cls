@isTest(SeeAllData=true)
private class SVMXC_InstalledProductTest {

	static testMethod void testSyncOfIPToWDL() {
            Country__c ctry = new Country__c(Name = 'Test', CountryCode__c = 'TS');
            insert ctry;
            
            StateProvince__c state = new StateProvince__c(NAme = 'TestState', Country__c = ctry.Id, CountryCode__c = 'US', StateProvinceCode__c = 'TS');
            insert state;
            
            Account acc = Utils_TestMethods.createAccount();
            insert acc;
                    
            SVMXC__Site__c site = new SVMXC__Site__c(Name = 'TestBatchName',
                                                 SVMXC__City__c = 'Junction City',
                                                 SVMXC__State__c = 'Kansas',
                                                 SVMXC__Street__c = '704 S. Adams',
                                                 SVMXC__Zip__c = '66441',
                                                 LocationCountry__c = ctry.Id,
                                                 SVMXC__Location_Type__c='Building',
                                                 SVMXC__Account__c=acc.Id,
                                                 TimeZone__c='India');
            insert site;
            
            SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name = 'test');
            insert team;
            
            User testuser = new User();
            testUser.Email              = 'test@servicemax.com';
            testUser.Username           =  'svmxcuser@servicemax.com';
            testUser.LastName           = 'test';
            testUser.Alias              = 'test';
            testUser.ProfileId          = UserInfo.getProfileId();
            testUser.LanguageLocaleKey  = 'en_US';
            testUser.LocaleSidKey       = 'en_US';
            testUser.TimeZoneSidKey     = 'America/Chicago';
            testUser.EmailEncodingKey   = 'UTF-8';
            testUser.ManagerId = UserInfo.getUserId();
            
            insert testUser;
            
            Account acct = new Account(Name = 'TestAcct', Country__c = ctry.Id, Street__c = 'TestCity', StateProvince__c = state.Id, ZipCode__c = '666441',OwnerId=testUser.Id);
            insert acct;
           
            Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
            insert prd;
       
       		SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(
                SVMXC__Company__c = acct.Id,
                SVMXC__Site__c = site.Id,           
                Service_Business_Unit__c = 'BD',
                OwnerId=testUser.Id             
            );
       
       		insert wo;
       
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = site.Id, SVMXC__Company__c = acc.Id, 
            							RangeToCreate__c='test', WorkOrder__c = wo.Id,
            							SVMXC__Product__c=prd.Id, SKUToCreate__c='Test2', BrandToCreate__c='schneider-electric',
            							DeviceTypeToCreate__c='Circuit Breaker', OwnerId=testUser.Id, TECH_CreatedfromSFM__c =true);
            insert ip;
            
          	List<SVMXC__Service_Order_Line__c> lines = [SELECT Id FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Service_Order__c =: wo.Id];
          	
          	System.assertEquals(1, lines.size());
          	
          	SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = site.Id, SVMXC__Company__c = acc.Id, 
            							RangeToCreate__c='test', WorkOrder__c = wo.Id, SVMXC__Parent__c = ip.Id,
            							SVMXC__Product__c=prd.Id, SKUToCreate__c='Test2', BrandToCreate__c='schneider-electric',
            							DeviceTypeToCreate__c='Circuit Breaker', OwnerId=testUser.Id, TECH_CreatedfromSFM__c =true);
 
          	insert ip2;
        }
}