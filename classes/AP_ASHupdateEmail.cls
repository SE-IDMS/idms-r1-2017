public class AP_ASHupdateEmail{
    
    
    public static void updateEmail(set<id> anncskhids){
         list<AnnouncementStakeholder__c> annskh = new list<AnnouncementStakeholder__c >();
        
        for(AnnouncementStakeholder__c annc1 : [Select id,Email__c,UserName__c,Tech_Email__c,UserName__r.Email from AnnouncementStakeholder__c where id in: anncskhids]){
            if(annc1.Email__c == null && annc1.UserName__c != null){
                annc1.Tech_Email__c =  annc1.UserName__r.Email;
                annskh.add(annc1);
            }else if(annc1.Email__c != null && annc1.UserName__c == null){
                annc1.Tech_Email__c =  annc1.Email__c;
                annskh.add(annc1);
            }
        }
        if(annskh.size()>0){
            update annskh;
        }
        
        
    
    }
}