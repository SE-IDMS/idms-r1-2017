/**
16-April-2013
Shruti Karn  
PRM May13 Release    
Initial Creation: test class for VFC_UpdateCountryProgram
 */
@isTest
private class VFC_UpdateCountryProgram_TEST {
  
    
    static testMethod void myUnitTest() {
        
        Country__c country = Utils_TestMethods.createCountry();
        PartnerPRogram__c globalProgram = new PartnerPRogram__c();
        PartnerProgram__c countryPartnerProgram = new PartnerPRogram__c();
        //Country__c country = Utils_TestMethods.createCountry();
        //insert country;
        // Profile profile = [select id from profile where name='SE - Sales Manager'];
        // User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        // u.BypassVR__c = true;
        // u.profileid = profile.Id;
        // //u.country__c = country.countrycode__c;
        // insert u;

        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;   
        sysadmin2.BypassVR__c = true;
        sysadmin2.userroleid = '00EA0000000acDJ';
        
        User u;     
        System.runAs(sysadmin2){
            UserandPermissionSets uuserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Sales Manager');
            u = uuserandpermissionsetrecord.userrecord;
            u.ProgramOwner__c=true;
            u.ProgramAdministrator__c=true;
            Database.insert(u);

            List<PermissionSetAssignment> permissionsetassignmentlstforu=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:uuserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforu.add(new PermissionSetAssignment(AssigneeId=u.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforu.size()>0)
            Database.insert(permissionsetassignmentlstforu);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }

        
        system.runAs(u)
        {
            
            insert country;
            globalProgram = Utils_TestMethods.createPartnerProgram();
            globalProgram.TECH_CountriesId__c = country.id;
            globalPRogram.recordtypeid = Label.CLMAY13PRM15;
            globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
            insert globalProgram;
            PartnerProgramCountries__c ppc = new PartnerProgramCountries__c (Country__c=country.id,PartnerPRogram__c = globalPRogram.id);
            insert ppc;
            
        }
        u.country__c = country.countrycode__c;
        update u;
        
        system.runAs(u)
        {
            ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
            VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
            myPageCon1.selCountry = country.id;
            myPageCon1.goToCountryPRGEditPage();
            countryPartnerProgram = [Select id,programstatus__c,country__c from PartnerProgram__c where GlobalPartnerProgram__c = :globalProgram.Id limit 1];
            ApexPages.StandardController sdtCon2 = new ApexPages.StandardController(countryPartnerProgram );
            VFC_UpdateCountryProgram myPageCon2 = new VFC_UpdateCountryProgram(sdtCon2);
            myPageCon2.updateCountryStatus();
        }
        
        u.ProgramAdministrator__c = true;
        update u;
        
        system.runAs(u)
        {
            //countryPartnerProgram = [Select id,programstatus__c,country__c from PartnerProgram__c where GlobalPartnerProgram__c = :globalProgram.Id limit 1];
            ApexPages.StandardController sdtCon2 = new ApexPages.StandardController(countryPartnerProgram );
            VFC_UpdateCountryProgram myPageCon2 = new VFC_UpdateCountryProgram(sdtCon2);
            myPageCon2.updateCountryStatus();
            
            
            
        }
        
    
        User u2 = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u2.BypassVR__c = true;
        u2.userroleid = '00EA0000000acDJ';
        //u.country__c = country.countrycode__c;
        //insert u2; 
        system.runAs(u2)
        {
            countryPartnerProgram.country__c = null;
            countryPartnerProgram.programstatus__c = 'Active';
            update countryPartnerProgram;
        }
        
       // User u2 = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        //u2.BypassVR__c = false;
        //u.country__c = country.countrycode__c;
        //insert u2;
        
        
        system.runAs(u)
        {
            //countryPartnerProgram = [Select id,programstatus__c,country__c from PartnerProgram__c where GlobalPartnerProgram__c = :globalProgram.Id limit 1];
            ApexPages.StandardController sdtCon2 = new ApexPages.StandardController(countryPartnerProgram );
            VFC_UpdateCountryProgram myPageCon2 = new VFC_UpdateCountryProgram(sdtCon2);
            //try
            //{
                myPageCon2.updateCountryStatus();
            //}
           // catch(Exception e){}  
           VFC_UpdateCountryProgram.isTestException = true;
           myPageCon2.updateCountryStatus();                      
        }
    }
    static testMethod void myTest() {
         List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0) {
            User user1 = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            user1.ProgramAdministrator__c = true;
            user1.ProgramManager__c = true;
            user1.userroleid = '00EA0000000acDJ';
            insert user1;
            
            User user2 = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC97');
            user2.ProgramManager__c = true;
            user2.userroleid = '00EA0000000acDJ';
            insert user2;
            
            User user3 = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC98');
            user3.ProgramManager__c = true;
            user3.userroleid = '00EA0000000acDJ';
            insert user3;
                  
        Country__c country = Utils_TestMethods.createCountry();
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        
        // User user1 = new User(BypassWF__c = true, ProgramAdministrator__c = true, ProgramManager__c = true, profile = 'SystemAdministrator');
    
        // insert user1;
        user1.country__c = country.countrycode__c;
        update user1;
        system.runas(user1){
        
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__C = Label.CLMAY13PRM47;
        insert gp1;

        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        ApexPages.currentPage().getParameters().put('GlobalPrgId',gp1.Id);
        ApexPages.StandardController sdtCont1 = new ApexPages.StandardController(gp1);
        VFC_NewCountryProgram myPageCont1 = new VFC_NewCountryProgram(sdtCont1);
            
        //myPageCont1.goToCountryPRGEditPage();
        //cp1 = [Select id,name,programstatus__c,country__c from PartnerProgram__c where GlobalPartnerProgram__c = :gp1.Id limit 1];
        ApexPages.StandardController sdtCont2 = new ApexPages.StandardController(cp1 );
        VFC_UpdateCountryProgram myPageCont2 = new VFC_UpdateCountryProgram(sdtCont2);
        myPageCont2.updateCountryStatus();
        List< CS_StdFieldsToRemove__c> fields = new List< CS_StdFieldsToRemove__c >();
        CS_StdFieldsToRemove__c cs1 = new CS_StdFieldsToRemove__c();
        cs1.name = 'createdbyid';
        fields.add(cs1);
        CS_StdFieldsToRemove__c cs2 = new CS_StdFieldsToRemove__c();
        cs2.name = 'lastmodifiedbyid';
        fields.add(cs2);
        CS_StdFieldsToRemove__c cs3 = new CS_StdFieldsToRemove__c();
        cs3.name = 'ownerid';
        fields.add(cs3);
        CS_StdFieldsToRemove__c cs4 = new CS_StdFieldsToRemove__c();
        cs4.name = 'recordtypeid';
        fields.add(cs4);
        CS_StdFieldsToRemove__c cs5 = new CS_StdFieldsToRemove__c();
        cs5.name = 'user__c';
        fields.add(cs5);
        
        insert fields;
       
        Recordtype prgTeamRecordType = [Select id from RecordType where developername =  'PartnerProgramTeam' limit 1];
        
        TeamMember__c tm = new TeamMember__c();
        //if(mapRecordType.get('Partner Program Team') != null)
        tm.RecordTypeId = prgTeamRecordType.Id;
        tm.PartnerProgram__c = cp1.id;
        tm.User__c = user2.id;
        insert(tm);
        
        tm.User__c = user3.id;
        update(tm);
        
        delete(tm);
    }

    }
    }

    static testMethod void testProgramDecommission () {

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        User u1;
        System.runAs ( thisUser ) {
          UserRole uRole = [SELECT Id FROM UserRole WHERE Name='CEO' LIMIT 1];

          List<User> lstUsers = new List<User>();

          Id profilesId = [select id from profile where name='System Administrator'].Id;
          u1= new User(Username = 'testUserTwo@schneider-electric.com', LastName = 'User11', alias = 'patuser1',
                          CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                          Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US' , ProfileID = profilesId, UserRoleId=uRole.Id,
                          ProgramOwner__c=true, ProgramManager__c=true,ProgramApprover__c=true, ProgramAdministrator__c=true,
                          PartnerORFConverter__c=true,BypassVR__c=true );
          lstUsers.add(u1);
          insert lstUsers;

          System.debug('*** Partner User:' + u1);
        }
      
        System.runAs(u1) {
            Country__c country = Utils_TestMethods.createCountry();
            insert country;
            //Creation of Partner Program
        
            Recordtype globalProgramRecordType = [Select id from RecordType where developername =  'GlobalProgram' limit 1];
            PartnerProgram__c globalProgram = Utils_TestMethods.createPartnerProgram();
            globalProgram.recordtypeid=globalProgramRecordType.id;
            globalProgram.ProgramStatus__c = 'Active';
            insert globalProgram;
        
            //Program Level
        
            ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.id);
            insert newLevel;
            
            //Creation of Country Program       
            
            Recordtype countryProgramRecordType = [Select id from RecordType where developername =  'CountryPartnerProgram' limit 1];
            List<PartnerProgram__c> countryPrograms = new List<PartnerProgram__c>();
            PartnerProgram__c countryProgram = new PartnerProgram__c(Name='Test DecommissionProgram1',
                                                                     globalPartnerProgram__c = globalProgram.id, 
                                                                     country__c = country.id, 
                                                                     recordtypeid =countryProgramRecordType.Id,
                                                                     ProgramStatus__c='Active' );

            PartnerProgram__c countryProgram2 = new PartnerProgram__c(Name='Test DecommissionProgram2',
                                                                     globalPartnerProgram__c = globalProgram.id, 
                                                                     country__c = country.id, 
                                                                     recordtypeid =countryProgramRecordType.Id,
                                                                     ProgramStatus__c='Draft' );
            countryPrograms.add(countryProgram);
            countryPrograms.add(countryProgram2);
            insert countryPrograms;
            System.debug('*** Country Program:' + countryProgram);
        
            ProgramLevel__c CountryNewLevel = Utils_TestMethods.createCountryProgramLevel(countryProgram.id);
            CountryNewLevel.LevelStatus__c='Active';
            insert CountryNewLevel;
            System.debug('*** New Country Level:' + CountryNewLevel);

            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(countryProgram);
            PageReference PageRef1 = Page.VFP_UpdateCountryProgram;
            Test.setCurrentPage(PageRef1); 
            VFC_UpdateCountryProgram updCountryProgram = new VFC_UpdateCountryProgram(sdtCon1);
            updCountryProgram.DecommissionProgram();

            ApexPages.StandardController sdtCon2 = new ApexPages.StandardController(countryProgram2);
            PageReference PageRef2 = Page.VFP_UpdateCountryProgram;
            Test.setCurrentPage(PageRef2); 
            VFC_UpdateCountryProgram updCountryProgram2 = new VFC_UpdateCountryProgram(sdtCon2);
            updCountryProgram2.DecommissionProgram();
        }
    }
}