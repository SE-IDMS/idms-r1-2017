@isTest
private class VFC62_DisplayAccLocalAttributes_TEST {
    static testMethod void VFC62_DisplayAccLocalAttributesTest() {
        Profile profile = [select id from profile where name='System Administrator' LIMIT 1];
        User user = Utils_TestMethods.createStandardUser(profile.id, 'test6');
        database.insert(user);   
        
        Account account = Utils_TestMethods.createAccount();
        insert account;
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;

        LocalAttribute__c la = Utils_TestMethods.createLocalAttribute();
        insert la;
        
        LocalAttributeCountry__c laCountry= Utils_TestMethods.createLocalAttributeCountry(la.id, country.id);
        insert laCountry;        
        
        List<LocalAttribute__c> las = new List<LocalAttribute__c>(); 
        
        for(Integer i=0;i<110;i++)
        {
            LocalAttribute__c lao = Utils_TestMethods.createLocalAttribute();
            lao.Name = 'Test'+i;
            las.add(lao); 
        }
        Database.insert(las);
        
        List<String> keywords = New List<String>();
        keywords.add('Test LocalAttribute');
        List<String> filters = New List<String>();
        filters.add(country.id);
        filters.add('Quality and Control');
        filters.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
        Utils_DataSource dataSource = Utils_Factory.getDataSource('LA');
        Utils_DataSource.Result tempresult = dataSource.Search(keywords,filters);
        System.debug('#########tempsobject########');
        sObject tempsObject;
        if(tempresult.recordList!=null && tempresult.recordList.SIZE()>0)
            tempsObject = tempresult.recordList[0];
        PageReference pageRef;
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();    
        
        
        //End of Test Data preparation.
       
        ApexPages.StandardController la1 = new ApexPages.StandardController(new ACC_LocalAttribute__c());
        VFC62_DisplayAccLocalAttributes laSearch = new VFC62_DisplayAccLocalAttributes(la1);
        pageRef= Page.VFP62_DisplayAccLocalAttributes;  
        
        System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
        VCC06_DisplaySearchResults componentcontroller = laSearch.resultsController; 
        
        laSearch.searchText='';
        laSearch.level1 = Label.CL00355;
        laSearch.level2 = Label.CL00355;
        laSearch.search();

        laSearch.searchText='abcd';
        laSearch.level1 = Label.CL00355;
        laSearch.level2 = 'Quality and Control';
        laSearch.search();

        for(Integer index=0; index<1000; index++)
        {
          laSearch.searchText=laSearch.searchText+'1234567890';
        }
        laSearch.level1 = country.id ;
        laSearch.search(); 
        
        laSearch.searchText='Test LocalAttribute';
        laSearch.search(); 
        Test.setCurrentPageReference(pageRef);        
        laSearch.PerformAction(tempsObject, laSearch);

        pageRef.getParameters().put(System.Label.CL00330, account.id);
        Test.setCurrentPageReference(pageRef); 
        laSearch.PerformAction(tempsObject, laSearch);

        laSearch.PerformAction(tempsObject, laSearch);
        
        laSearch.searchText='Test';
        laSearch.search(); 
        laSearch.clear();
   }  
}