global class ApexLoggingService{
    /*
        A Virtual Class that Defines the 
        Structure of a Log
    */
    private static List<ApexDebugLog__c> logsToInsert;
    public static Boolean Batch_Logs = False;
    
    public virtual class Log{  
        public String Type;
        public String ApexClass;
        public String Method;

        public String RecordId;
        public String Message;
        public String StackTrace;
    }
    
    /*
        A Class that Extends the Virtual Class - Log
        to define a log of type - Error
    */
    public class Error extends Log {
        public Error(String cls, String routine, String recId, Exception ex) {
            this.Type = 'Error';
            this.ApexClass = cls;
            this.Method = routine;
            this.RecordId = recId;
            this.Message = ex.getMessage();
            this.StackTrace = ex.getStackTraceString();
        }
    }

    /*
        A Class that Extends the Virtual Class - Log
        to define a log of type - Information
    */
    public class Information extends Log {
        public Information(String cls, String routine, String recId, String msg) {
            this.Type = 'Information';
            this.ApexClass = cls;
            this.Method = routine;
            this.RecordId = recId;
            this.Message = msg;
            this.StackTrace = NULL;
        }
    }
    public static void createLog(String lType,String lApexCls,String lMethod,String lRecId,String lMessage,String lStackTrace){
        Log lg = new Log();
        lg.Type = lType;
        lg.ApexClass = lApexCls;
        lg.Method = lMethod;
        LG.RecordId = lRecId;
        lg.Message = lMessage;
        lg.StackTrace = lStackTrace;
        createLog(lg);
        
    }

    /*
        A Public Method that can be utilized by
        other Apex Classes to create a record into
        the Apex Debug Log object stating the Error
        or Information.
    */
    public static void createLog(Log logToCreate){
        try {
            ApexDebugLog__c apexDebuglog = new ApexDebugLog__c(
                Type__c         = logToCreate.Type,
                ApexClass__c   = logToCreate.ApexClass,
                Method__c       = logToCreate.Method,
                RecordId__c    = logToCreate.RecordId,
                Message__c      = logToCreate.Message,
                StackTrace__c  = logToCreate.StackTrace
            );
            if (Batch_Logs == True) {
                if (logsToInsert == null) logsToInsert = new List<ApexDebugLog__c>();
                logsToInsert.add(apexDebuglog);
            }
            else
                flush (new List<ApexDebugLog__c> { apexDebuglog } );
         }
        catch (DMLException ex){
            System.debug('Something fatal has occurred and hence failed to create a Log! Error:' + ex.getMessage());
        }
    }
    // Method to push a log to insert
    public static void flush(){
        if((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements())){
            if(logsToInsert.size()>0){
              Database.insert(logsToInsert, FALSE); 
            }
        }
        else{
            System.debug('The Governor Limits have already been exhausted and hence failed to create a Log!');        
        }       
    }
    
    // Method to push logs to insert
    public static void flush(List<ApexDebugLog__c> lLogs) {
        System.debug('**BatchValue :'+Batch_Logs);
        if((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements())) {
            if (logsToInsert.size() > 0){
              Database.insert(lLogs, FALSE);
            }
        }
        else{
            System.debug('The Governor Limits have already been exhausted and hence failed to create a Log!');
        }
    }

    /*
        A Public Method that can be utilized from
        JavaScript to create record(s) [aka Logs] into the
        Custom Object.
    */
    webService static void createLog(String log){
        try{
            /*
                Expects a JSON of the form - 
                {
                    "Type"       : "---",
                    "ApexClass"  : "---",
                    "Method"     : "---",
                    "RecordId"   : "---",
                    "Message"    : "---",
                    "StackTrace" : "---",
                }
            */

            /*Deserialize the same to create an instance of the virtual class - ApexDebugLog.Log*/
            ApexLoggingService.Log logToCreate = (ApexLoggingService.Log)JSON.deserialize(log, ApexLoggingService.Log.class);
            
            ApexLoggingService.createLog(logToCreate);
        }
        catch(Exception ex){
            System.debug('Something fatal has occurred and hence failed to create a Log! Error:' + ex.getMessage());
        }
    }
}