@isTest(SeeAllData=true)
private class AP_WorkOrderAfterUpdate_TEST 
{
     static testMethod void testWO()
     {   Account acc = Utils_TestMethods.createAccount();
         insert acc;
        
             
         
         WorkOrderNotification__c won = new WorkOrderNotification__c();//Id = workOrder.Work_Order_Notification__c);
                 won.WorkOrderReason__c='test';
                 won.InstalledAtAccount__c=acc.id;
                 //won.CustomerRequestedDate__c=Datetime.now();
                 won.ServicesBusinessUnit__c='energy';
                 insert won;
                 set<Id> wonid = New Set<Id>();
                 wonid.add(won.Id);
                 
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
                 workOrder.Work_Order_Category__c='On-site';                 
                 workOrder.SVMXC__Order_Type__c='Maintenance';
                 workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                 workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                 workOrder.CustomerRequestedDate__c = Date.today();
                 workOrder.Service_Business_Unit__c = 'Energy';
                 workOrder.SVMXC__Priority__c = 'Normal-Medium';
                 workOrder.CustomerTimeZone__c= 'on leave';
                 workOrder.SendAlertNotification__c = true;
                 workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                 workOrder.BackOfficeReference__c = '111111';
                 workOrder.Comments_to_Planner__c='testing';
                 workOrder.Work_Order_Notification__c=won.Id;
         insert workOrder;
         set<Id> woid = New Set<Id>();
         woid.add(workOrder.Id);
                 
                
               /* won.Work_Order_Status__c = workOrder.SVMXC__Order_Status__c;
                won.Scheduled_Date_Time__c = workOrder.SVMXC__Scheduled_Date_Time__c;
                won.CommentstoDispatcher__c = workOrder.Comments_to_Planner__c;
                update won;*/
        AP_WorkOrderAfterUpdate.UpdateWONStatus(wonid,woid);
        
         
     }
      static testMethod void testWO2()
     {
     
          Account acc = Utils_TestMethods.createAccount();
         insert acc;
         Contact contact1 = Utils_TestMethods.createContact(acc.Id,'TestContact');
         insert contact1;
         Case case1 = Utils_TestMethods.createCase(acc.Id, contact1.Id, 'Open');
          insert case1;
          
           SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
                 workOrder.Work_Order_Category__c='On-site';                 
                 workOrder.SVMXC__Order_Type__c='Maintenance';
                 workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                 workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                 workOrder.CustomerRequestedDate__c = Date.today();
                 workOrder.Service_Business_Unit__c = 'Energy';
                 workOrder.SVMXC__Priority__c = 'Normal-Medium';
                 workOrder.CustomerTimeZone__c= 'on leave';
                 workOrder.SendAlertNotification__c = true;
                 workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                 workOrder.BackOfficeReference__c = '111111';
                 workOrder.Comments_to_Planner__c='testing';
               workOrder.SVMXC__Case__c =case1.id;
               workOrder.SVMXC__Scheduled_Date_Time__c = system.now();   
             insert workOrder;
            workOrder.SVMXC__Scheduled_Date_Time__c = system.now().addDays(1);
            workOrder.SVMXC__Order_Status__c = 'Closed';
         Test.startTest();
            update workOrder;
         Test.stopTest();
            
         AP_WorkOrderAfterUpdate.UpdateCaseStatus(new List<SVMXC__Service_Order__c>{workOrder}, new Set<id>{case1.Id});  
     
     }
     static testMethod void myUnitTest33() {
    
    
    SVMXC__Service_Group__c st1= new SVMXC__Service_Group__c(Name='St1',SVMXC__Active__c=true,SVMXC__Group_Type__c='Internal',SVMXC__Group_Code__c='test213');
              Test.startTest();
   insert st1;


     SVMXC__Service_Group_Members__c tech1 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c=st1.id,recordtypeid = Label.CLDEC12SRV39);
     insert tech1;
     
     SVMXC__Service_Order__c wo2 = new SVMXC__Service_Order__c(SVMXC__Order_Status__c='Customer Confirmed',SVMXC__Group_Member__c=tech1.id,customerconfirmed__c = TRUE,CompliedWithSafetyRequirements__c='Yes');
     insert wo2;
     update wo2;
     
     
    SVMXC__Service_Order__c wo1 = new SVMXC__Service_Order__c(SVMXC__Order_Status__c='NEW',SVMXC__Group_Member__c=tech1.id,WOLevel__c='Accepted',CustomerTimeZone__c='GMT',CompliedWithSafetyRequirements__c='Yes');             
        insert wo1;
            wo1.SVMXC__Order_Status__c='Acknowledge FSE';
            
            update wo1;
         SVMXC__Service_Group__c st= new SVMXC__Service_Group__c(name='test123');
         insert st;
         SVMXC__Service_Group_Members__c tech=new SVMXC__Service_Group_Members__c(name='test22',SVMXC__Service_Group__c=st.id,SVMXC__Salesforce_User__c=userinfo.getUserId());
        insert tech;
         AssignedToolsTechnicians__c att= new AssignedToolsTechnicians__c(WorkOrder__c=wo1.id, Status__c = 'Accepted',TechnicianEquipment__c=tech.id);
         insert att;
         Test.stopTest();
    }
    
    
    static testMethod void myUnitTest22() {
    
    Test.StartTest();
        Account acc= new account(name='testacc');
        insert acc;
        Account ac2= new account(name='testacc11');
        insert ac2;
        contact con = new contact(Title='Name',LastName='ABCD',FirstName='text');
        insert con;
        contact con11 = new contact(Title='Namecon',LastName='Test',FirstName='texts');
        insert con11;
    
        
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(SVMXC__Order_Status__c='Service Complete',SendEmailToCustomer__c =TRUE,Work_Order_Category__c = 'Onsite',SVMXC__Company__c=acc.id,SVMXC__Contact__c=con.id);
        insert wo;
        wo.OwnerId='005A0000003tG8t';
        wo.SVMXC__Company__c=ac2.id;
        wo.SVMXC__Contact__c=con11.id;
        update wo;
        
        SVMXC__Service_Order__c wo1 = new SVMXC__Service_Order__c(SVMXC__Order_Type__c ='Commissioning & Installation',SVMXC__Order_Status__c='Service Complete');
        insert wo1;
        wo1.SVMXC__Order_Status__c='Service Validated';
        update wo1;
        Test.Stoptest();
        
    }
    
    static testMethod void myUnitTest1() {
    
    Test.startTest();
    Country__c  c1= [Select name,id,countrycode__C from country__C where countrycode__c =: 'IN'];
    Account a1= new account(name='test account',Country__c=c1.id);
    insert a1;
        
         Contact Cnct = new Contact(AccountId=a1.id,email='UTCont@mail.com',phone='1234567890',LastName='UTContact001',FirstName='Fname1');
        insert Cnct;   
        
        SVMXC__Service_Group__c st= new SVMXC__Service_Group__c(name= 'test sc');
        insert st;
        SVMXC__Service_Group_Members__c fsr= new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c=st.id,name='testfsr');
        //insert fsr;
        SVMXC__Site__c loc = new SVMXC__Site__c(name='testloc');
        insert loc;

        SVMXC__Installed_Product__c ip= new SVMXC__Installed_Product__c(Name='testip');
        insert ip;
        
        
        
        WorkOrderAssignmentRule__c woa= new WorkOrderAssignmentRule__c(BusinessUnit__c='EN',Country__c= c1.id,WorkOrderCategory__c=null,WorkOrderType__c=null,WorkOrderSource__c=null);
                 insert woa;

        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(SVMXC__Is_PM_Work_Order__c = false,SVMXC__Order_Status__c='New',SVMXC__Component__c = ip.id,Service_Business_Unit__c='EN',SVMXC__Company__c=a1.id,SVMXC__Site__c=loc.id,SVMXC__Contact__c= Cnct.id);
                insert wo;
                
                Role__c ro= new Role__c(Location__c=wo.SVMXC__Site__c);
                    insert ro;
                
                Test.StopTest();
                
                
        wo.SVMXC__Order_Status__c='Rescheduled';
        update wo;
        
    }
    

    static testMethod void myUnitTest() {
    
         Profile profile = [select id from profile where id = '00eA0000000awZt' ];     
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
      System.runAs(new User(Id = UserInfo.getUserId()))
        {
        
        List<SVMXC__Service_Order__c> wolist = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> wolist3 = new List<SVMXC__Service_Order__c>();
        ID TechRTID,EquipmentRTID;
         
        //Pricebook test data
        //Pricebook2 pb=[Select Name,id From Pricebook2 where Name='Standard Price Book' limit 1];
        Profile p1 = [select id from profile where name = 'System Administrator' limit 1];
        List<RecordType> RTylist=[select id,Name from Recordtype where sobjecttype='SVMXC__Service_Group_Members__c'];
        for( RecordType RTy:RTylist){
            if(Rty.Name=='Technician')
                TechRTID=RTy.id;
            if(Rty.Name=='Equipment')
                EquipmentRTID=RTy.id; }
        
       
        
        //Account test data
            Account Acc = new Account   (           Name='ACC001',                                   
                                                    POBox__c= 'POBox1',
                                                    POBoxZip__c='POBoxZip',
                                                    ZipCode__c='12345');
        insert Acc;
        
        Account ac1 = new Account   (
                                                    Name ='Account',
                                                    Street__c='ABC',
                                                    City__c='PQR ',
                                                    //Country__c = 'XYZ',
                                                    POBox__c= 'POBox2',
                                                    POBoxZip__c='POBoxZip2',
                                                    ZipCode__c='12345'
                                     ); 
           
         insert ac1;
         
                 
                                       
        //Contact test data
        
        Contact Cnct = new Contact(
                                                    AccountId=Acc.Id,
                                                    email='UTCont@mail.com',
                                                    phone='1234567890',
                                                    LastName='UTContact001',
                                                    FirstName='Fname1');
        insert Cnct;   
        
        
         //User test data
        
        User newUser = new User(alias = 'alias1', 
                                email = 'maxexpress@servicemax.com', 
                                emailencodingkey = 'UTF-8', 
                                lastname = 'ps.servicemax@gmail.com',
                                languagelocalekey = 'en_US',    
                                localesidkey = 'en_US', 
                                profileid = p1.id, 
                                timezonesidkey = 'America/Los_Angeles', 
                                username = 'svmx3@testorg.com'
                                //UserRoleId= ur.Id,  
                               );

        insert newUser;
        
         User newUser1 = new User(alias = 'alias1', 
                                email = 'maxexpress@servicemax.com', 
                                emailencodingkey = 'UTF-8', 
                                lastname = 'newuserTest1',
                                languagelocalekey = 'en_US', 
                                localesidkey = 'en_US', 
                                profileid = p1.id, 
                                timezonesidkey = 'America/Los_Angeles', 
                                username = 'svmx1@testorg.com'
                                //UserRoleId= ur.Id, 
                                );

        insert newUser1;
        
        //Case test data
        
        Case cs1 = new Case(
                            
                                                    AccountId=Acc.id,
                                                    ContactId=Cnct.id,
                                                    Status='New',
                                                    ProductBU__c='POWER', 
                                                    Origin='Email');
        insert cs1;
        
                Case cs2 = new Case(
                            
                                                    AccountId=Acc.id,
                                                    ContactId=Cnct.id,
                                                    Status='New',
                                                    ProductBU__c='INDUSTRY', 
                                                    Origin='Email');
       // insert cs2;
        
         //Product test data
         
                   Product2 prod =    new Product2( ProductCode='UTProd001',
                                                    Name='UTProd001',
                                                    Family='Oncology',
                                                    SVMXC__Product_Line__c='Desktop',
                                                    IsActive=true);
        insert prod;
        
                     Product2 prod1 = new Product2(  ProductCode='UTProd002',
                                                     Name='UTProd002',
                                                     Family='Oncology',
                                                     SVMXC__Product_Line__c='Desktop',
                                                     IsActive=true);
        insert prod1;
        
         //CustomerLocation test data
        
        CustomerLocation__c loc1=    new CustomerLocation__c(
                                                     AddressLine1__c='Add Lin 1',
                                                     AddressLine2__c='Add Line 2',
                                                     City__c='Bangalore',
                                                    // Country__c='India',
                                                     Health__c='Drug Test',
                                                     RelatedAccount__c=ac1.id,
                                                     ZipCode__c='1234'    );
                                                     
        //Location test data
         
         SVMXC__Site__c  loc = New SVMXC__Site__c(SVMXC__Street__c='Add Lin 1',
                                                     AddressLine2__c='Add Line 2',
                                                        SVMXC__City__c='Bangalore',
                                                    HealthAndSafety__c='Drug Test',
                                                     SVMXC__Account__c=ac1.id,
                                                        SVMXC__Zip__c='1234' 
                                                            ); 
         insert loc;
                
        
         //Service Team test data
        
        SVMXC__Service_Group__c st1= new             SVMXC__Service_Group__c(
                                                     Name='St1',
                                                     SVMXC__Active__c=true,
                                                     SVMXC__Group_Type__c='Internal',
                                                     SVMXC__Group_Code__c='test213');
             insert st1;
                                                     
        
 SVMXC__Territory__c ter1=              new          SVMXC__Territory__c(
                                                     //Owner=newUser1.id,
                                                     Name ='pter'   );  
            insert  ter1;                                    
 
 SVMXC__Territory__c ter2=              new          SVMXC__Territory__c(
                                                     Name ='pter2'  ); 
                                                     
         insert     ter2;                                                                                                           
                                                                                                                    
 /*SVMXC__Service_Group_Members__c tech1= new          SVMXC__Service_Group_Members__c(
                                                     SVMXC__Service_Group__c=st1.id,
                                                     Name='John',
                                                     SVMXC__Active__c=true,
                                                     PrimaryAddress__c='Home',
                                                     SVMXC__Country__c='Nepal',                                                      
                                                     RecordTypeid=TechRTID,
                                                     Manager__c=newUser1.id,
                                                     SVMXC__Email__c='ps.servicemax@schneider-electric.com',
                                                     SVMXC__Salesforce_User__c=newUser1.id );   
         insert tech1   ;                                                               
        */
        
        SVMXC__Service_Group_Members__c tech1 = Utils_TestMethods.createTech_Equip(newUser1.Id,st1.Id);
        tech1.recordtypeid = Label.CLDEC12SRV39;
            try{
        insert tech1;
            }
                catch(DMLexception e){} 
    WorkOrderNotification__c won1=   new            WorkOrderNotification__c(
                                                     WorkOrderReason__c='test',   
                                                     Work_Order_Status__c='open',
                                                     ContactLastName__c='WNContact',
                                                     ContactPhoneNumber__c='984456565',
                                                     Scheduled_Date_Time__c=system.now(), 
                                                     ServicesBusinessUnit__c='ITB',
                                                           Case__c=cs1.id );
        insert won1;
        

        
        CSE_ExternalReferences__c extref1= New CSE_ExternalReferences__c(
                                                    TECH_Account__c=ac1.id,
                                                    Case__c=cs1.id,
                                                    Type__c='Contract'
                                                    );
        insert  extref1;    

        CSE_ExternalReferences__c extref2= New CSE_ExternalReferences__c(
                                                    TECH_Account__c=ac1.id,
                                                    Case__c=cs1.id,
                                                    Type__c='Warranty'
                                                    );
        insert  extref2;        
        
       WorkOrderNotification__c won2=   new            WorkOrderNotification__c(
                                                       /* Location__c=loc.id,*/
                                                     WorkOrderReason__c='test',  
                                                     Work_Order_Status__c='open',                                                     
                                                     ContactLastName__c='WNContact',
                                                     ContactPhoneNumber__c='984456565',
                                                     WarrantyReference__c=null,
                                                     Scheduled_Date_Time__c=system.now(),
                                                    ServicesBusinessUnit__c='ITB',                                                   
                                                     Case__c=cs1.id );
        insert won2;                                            
        
    SVMXC__Service_Order__c WO =     new            SVMXC__Service_Order__c(                                        
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Open', 
                                                     BackOfficeReference__c='123',  
                                                     SVMXC__Product__c = prod.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                     SVMXC__Primary_Territory__c=ter1.id,
                                                     RescheduleReason__c='Weather Related',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     SVMXC__Group_Member__c=tech1.id,
                                                     Work_Order_Notification__c=won1.id,
                                                     Is_Billable__c = true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                     Customer_Service_Request_Time__c= system.now());
        
                                                     
                                                     
        Test.startTest();                                            
        insert WO;    
            
        update WO;
            test.stopTest(); 
        
       // test.startTest();
          SVMXC__Service_Order__c WO4 =      new SVMXC__Service_Order__c(                                       
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Waiting for PO', 
                                                     BackOfficeReference__c='123',  
                                                     SVMXC__Product__c = prod.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                     SVMXC__Primary_Territory__c=ter1.id,
                                                     SVMXC__Group_Member__c=tech1.id,
                                                     RescheduleReason__c='Weather Related',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     Work_Order_Notification__c=won1.id,
                                                     Is_Billable__c = true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                     SVMXC__Case__c = cs1.id,
                                                                                                     
                                                     Customer_Service_Request_Time__c= system.now());
                                               
        database.insert (WO4,false);
        
        
       // WO4.Parent_Work_Order__c = WO.id;
        //WO4.Work_Order_Notification__c = won2.id;
        WO4.SVMXC__Order_Status__c = 'Customer Confirmed';
        WO4.SVMXC__Group_Member__c = tech1.id;
        WO4.Customer_Service_Request_Time__c = system.today();
        WO4.CustomerRequestedTime__c = '0:00';
        WO4.CustomerRequestedDate__c = system.today();        
        WO4.SendEmailToThirdParty__c = false;
        WO4.Work_Order_Category__c = '3rd Party';
        WO4.SVMXC__Case__c = cs2.id; 
        WO4.Service_Business_Unit__c='IT';
        WO4.SVMXC__Site__c = loc.id;
        database.update (WO4,false);
        
        WO4.SVMXC__Order_Status__c = 'Unscheduled';
        WO4.SubStatus__c = 'To Be Rescheduled';
      database.update (WO4,false);
        
        WO4.SVMXC__Order_Status__c = 'Rescheduled';
        WO4.SVMXC__Primary_Territory__c=ter2.id;
      database.update (WO4,false);
        
               
        WO4.SVMXC__Order_Status__c = 'Service Complete';    
        WO4.SVMXC__Case__c = cs2.id;        
        database.update (WO4,false);
        
        /*WO4.SendEmailToThirdParty__c = false;
        WO4.Work_Order_Category__c = '3rd Party';
        WO4.SVMXC__Order_Status__c = 'Customer Confirmed';
        update WO4;*/
            
            
        
        
        
        }
     
    }
    
    
}