public Class PRM_ProcessPendingQueueItems {
    
    public class CodeCoverageException extends Exception {
        public String message = 'This exception is thrown to do code coverage';
    }

    public static Boolean throwExceptionInTest;
    
    public static void updatebFOIdInIMS(List<sObject> scope){
        List<PRMRequestQueue__c> prmRequestQueueLst = new List<PRMRequestQueue__c>();
        List<UIMSCompany__c> UIMSList;
        Boolean opSuccess = true;
        prmRequestQueueLst = (List<PRMRequestQueue__c>)scope;
        System.debug('**Scope variable before Try**'+prmRequestQueueLst);
        try{
            WS_UIMSAdminService_UpdateModel.updateReturnBean[] testAccount;
            WS_UIMSAdminService_UpdateModel.updateReturnBean[] testContact;
            
            //prmRequestQueue.addall(scope);
            WS_UIMSAdminService_Update.BFOIOServiceImplPort bfoIO = new WS_UIMSAdminService_Update.BFOIOServiceImplPort();
            bfoIO.endpoint_x   =     System.label.CLAPR15PRM165;//'https://ims2w-int.btsec.dev.schneider-electric.com/IMS-IntegrationFlow/BFOIOService';
            bfoIO.clientCertName_x = System.label.CLAPR15PRM018;
            bfoIO.timeout_x  =       Integer.valueOf(System.label.CLAPR15PRM021);
            /*   updateaccounts*/
            WS_UIMSAdminService_UpdateModel.accountUpdateRequest arg0Acc = new WS_UIMSAdminService_UpdateModel.accountUpdateRequest();
            WS_UIMSAdminService_UpdateModel.accountBean[] accounts = new WS_UIMSAdminService_UpdateModel.accountBean[]{};
            WS_UIMSAdminService_UpdateModel.userUpdateRequest arg0Con = new WS_UIMSAdminService_UpdateModel.userUpdateRequest();
            WS_UIMSAdminService_UpdateModel.contactBean[] contacts = new WS_UIMSAdminService_UpdateModel.contactBean[]{};
            for(PRMRequestQueue__c prmReqQue :  prmRequestQueueLst) {
                prmReqQue.put('ProcessingStatus__c', 'Processed');
                System.debug('** prmReqQue **'+prmReqQue);
                WS_UIMSAdminService_UpdateModel.accountBean accBean = new WS_UIMSAdminService_UpdateModel.accountBean();
                accBean.BFO_ID = prmReqQue.AccountId__c;
                accBean.ID   = prmReqQue.AccountId__r.PRMUIMSID__c;
                system.debug('**Bean**'+accBean);
                accounts.add(accBean);
                
                WS_UIMSAdminService_UpdateModel.contactBean ccBean = new WS_UIMSAdminService_UpdateModel.contactBean();
                ccBean.BFO_ID = prmReqQue.ContactId__c;
                ccBean.ID = prmReqQue.ContactId__r.PRMUIMSID__c;
                system.debug('**Contact Bean**'+ccBean);
                contacts.add(ccBean);

                // Webserive to update the Identity and Company in IMS
                UIMSList = new List<UIMSCompany__c> ([SELECT UserRegistrationInformation__c FROM UIMSCompany__c WHERE UserFederatedId__c = :prmReqQue.ContactId__r.PRMUIMSID__c ORDER BY LastModifiedDate DESC]);
                if(UIMSList != null && UIMSList.size() > 0 && String.isNotBlank(UIMSList[0].UserRegistrationInformation__c)){
                    AP_UserRegModel userRegModel = (AP_UserRegModel)JSON.deserialize(UIMSList[0].UserRegistrationInformation__c, AP_UserRegModel.class); 
                    opSuccess = AP_PRMUtils.UpdateContactInUIMS_Admin(userRegModel);
                    if(userRegModel.primayContact && userRegModel.companyChange != 'create')
                        opSuccess = AP_PRMUtils.UpdateAccountInUIMS_Admin(userRegModel);
                }
            }
            
            // Webservide to update the bFO ID on the Identity and Company in IMS    
            System.debug('*accounts*'+accounts);
            arg0Acc.accounts = accounts;
            system.debug('**arg0Acc.accounts**'+arg0Acc.accounts);
            testAccount =  bfoIO.updateAccounts(arg0Acc);
            System.debug('**AccountResponse**'+ testAccount);
            
            
            
            System.debug('*contacts*'+contacts);
            arg0Con.contacts = contacts;
            system.debug('**arg0Con.contacts**'+arg0Con.contacts);
            testContact =  bfoIO.updateContacts(arg0Con);
            System.debug('**Contact Response**'+ testContact);
            System.debug('opSuccess:' + opSuccess);
        } catch(Exception ex) {
            system.debug('Error on updatebFOIdInIMS'+ex.getMessage()+''+ex.getStacktracestring());
            System.debug('**Scope in Catch**'+prmRequestQueueLst);
            for(PRMRequestQueue__c prmReq : prmRequestQueueLst) {
                system.debug('**Iterations**'+prmReq);
                prmReq.ErrorInfo__c = ex.getMessage()+''+ex.getStacktracestring();
                prmReq.RunCounter__c = prmReq.RunCounter__c +1;
                prmReq.ProcessingStatus__c = 'Pending';
                //prmRequestQueueLstUpdate.add(prmReq);               
            }
            //Update prmRequestQueueLstUpdate;
        }
        finally{
            List<sObject> lQueueItems = prmRequestQueueLst;
            CS_PRM_ApexJobSettings__c mapApexJobSettings = CS_PRM_ApexJobSettings__c.getInstance('UPDATE_REQUEST_QUEUE');
            Integer batchSize = Integer.valueOf(mapApexJobSettings.NumberValue__c);
            if (AP_PRMUtils.CanAddNewApexJobToQueue()) {
                AP_PRMAppConstants.ProcessPendingQueueItems updRQ = AP_PRMAppConstants.ProcessPendingQueueItems.UPDATE_REQUEST_QUEUE;
                Batch_PRM_ProcessPendingQueueItems batch_ppsa = new Batch_PRM_ProcessPendingQueueItems(updRQ,lQueueItems,false);
                //Database.executebatch(batch_ppsa,batchSize);
            }
        }
    }
    
    public static Set<Id> CreatePRMRegistrationbFO(List<sObject> scope){
        Set<Id> ContactIds = new Set<Id>();
        UserRegResult regResult;
        List<UIMSCompany__c> lUcmp = (List<UIMSCompany__c>)scope;
        if(lUcmp != Null && lUcmp.size() > 0) {
            if (string.isBlank(lUcmp[0].UserRegistrationInformation__c)) return null;
            
            for (UIMSCompany__c uCmp : lUcmp){
                if (string.isBlank(uCmp.UserRegistrationInformation__c)) continue;

                System.debug('** UIMS **'+uCmp);
                AP_UserRegModel newUser = (AP_UserRegModel)JSON.deserialize(uCmp.UserRegistrationInformation__c, AP_UserRegModel.class); 
                System.debug('**UserRegModel Generated from UIMS Company**'+newUser);      
            
                if (newUser != Null) {    
                    if(newUser.privateRegCompleted != null && newUser.privateRegCompleted == true)  
                        AP_PRMUtils.UpdatePRMRegistrationInbFO(uCmp);
                    else{              
                        newUser.isPrimaryContact =  (newUser.accountId == null && newUser.skippedCompanyStep == false ? true : false);
                        //Boolean isPostRegistration = (uCmp.UIMSActivationStatus__c == null || uCmp.UIMSActivationStatus__c == false) ? false : true;
                        regResult = AP_PRMAccountRegistration.CreatePRMRegistrationbFO(newUser, new List<UIMSCompany__c> { uCmp }, true);
                    }
                }
                if(regResult != null && string.isNotBlank(regResult.ContactId))
                    ContactIds.add(regResult.ContactId);
            }
        }
        return ContactIds;
    }
    // POMP Permission
    public static void assignPermissionPOMPUser(List<sObject> scope ){
        
        List<Id> contIdLst = new List<Id>();
        Map<Id,PRMRequestQueue__c> mContQueueItems = new Map<Id,PRMRequestQueue__c>();
        List<PRMRequestQueue__c> prmRequestQueueLst = (List<PRMRequestQueue__c>)scope;
        System.debug('**Scope before Try assignPermissionPOMPUser**'+prmRequestQueueLst);
        try{
            if(throwExceptionInTest == False && Test.isRunningTest()){
             throw new CodeCoverageException('Exception to do the code coverage');
            }
            for(PRMRequestQueue__c prmReq : prmRequestQueueLst){
                contIdLst.add(prmReq.ContactId__c);
                mContQueueItems.put(prmReq.ContactId__c,prmReq);
                Boolean pompPermsnGranted = AP_PRMUtils.AssignPOMPOnlyPermissions(contIdLst,mContQueueItems);
            }

            /*List<ContactAssignedFeature__c> contAssigndFeatrLst = new List<ContactAssignedFeature__c>([Select Active__c, Contact__c,FeatureCatalog__c, LastModifiedDate from ContactAssignedFeature__c Where Contact__c IN :contIdLst]);            
             
            List<ContactAssignedFeature__c> activeCAF = new List<ContactAssignedFeature__c>();
            List<ContactAssignedFeature__c> inactiveCAF = new List<ContactAssignedFeature__c>();
        
            for(ContactAssignedFeature__c caf : contAssigndFeatrLst) {
                if(caf.Active__c)
                    activeCAF.add(caf);
                else
                    inactiveCAF.add(caf);   
            }

            if(activeCAF.size() > 0)
                AP_ContactAssignedFeature.createPermissionSetAssignments2(activeCAF);
            if(inactiveCAF.size() > 0)
                AP_ContactAssignedFeature.deletePermissionSetAssignments2(inactiveCAF);
            */
            
        } Catch (Exception ex) {
            System.debug('Exception in Retry assignPermissionPOMPUser'+ex.getMessage()+''+ex.getStacktracestring());
            /*List<PRMRequestQueue__c> prmRequestQueueLstUpdate = new List<PRMRequestQueue__c>();
            for(PRMRequestQueue__c prmReq : prmRequestQueueLst) {
                
                    prmReq.ErrorInfo__c = ex.getMessage()+''+ex.getStacktracestring();
                    prmReq.RunCounter__c = prmReq.RunCounter__c +1;
                    prmRequestQueueLstUpdate.add(prmReq);
                
            }
            Update prmRequestQueueLstUpdate;*/
        }
    }
    
    
    public Static void assignPermissionPartnerUser(List<sObject> scope) {
        System.debug('** scope**'+scope);
        List<Id> contIdLst = new List<Id>();
        Map<Id, PRMRequestQueue__c> mQueueItems = new Map<Id, PRMRequestQueue__c>();
        List<PRMRequestQueue__c> prmRequestQueueLst = (List<PRMRequestQueue__c>)scope;

        if (prmRequestQueueLst == null) return;

        System.debug('**Scope Before Try**'+prmRequestQueueLst);
        
        try {
            if(throwExceptionInTest == False && Test.isRunningTest()) {
                throw new CodeCoverageException('Exception to do the code coverage');
            }

            for(PRMRequestQueue__c prmReqQue : prmRequestQueueLst){
                Id contId = prmReqQue.Contactid__c;
                contIdLst.add(contId);
                mQueueItems.put (contId, prmReqQue);
            }
            System.debug('****'+contIdLst);
            AP_PRMUtils.AssignPortalPermissions(contIdLst, mQueueItems);
        
        } Catch (Exception ex) {
            // RequestQueueItems are logged / updated in the catch block of the chained method
            System.debug('Exception in Retry assignPermissionPOMPUser'+ex.getMessage()+''+ex.getStacktracestring());
            /*
            List<PRMRequestQueue__c> prmRequestQueueLstUpdate = new List<PRMRequestQueue__c>();
            System.debug('**Scope Before Try**'+prmRequestQueueLst);
            for(PRMRequestQueue__c prmReq : prmRequestQueueLst) {
                    prmReq.ErrorInfo__c = ex.getMessage()+''+ex.getStacktracestring();
                    prmReq.RunCounter__c = prmReq.RunCounter__c +1;
                    prmRequestQueueLstUpdate.add(prmReq);
                
            }
            System.debug('**Before Update**'+prmRequestQueueLstUpdate);
            Update prmRequestQueueLstUpdate;
            */
        }
    }
}