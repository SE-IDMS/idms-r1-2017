/********************************************************************
* Company: Fielo
* Developer: Lucas Caceres
* Created Date: 23/02/2015
* Description: 
********************************************************************/

public without sharing class FieloPRM_AP_BadgeMemberTriggers{
    
    //Filter trigger new
    public static List<FieloEE__BadgeMember__c> filterTriggerNew(List<FieloEE__BadgeMember__c> triggerNew){
        List<FieloEE__BadgeMember__c> triggerNewFiltered = new List<FieloEE__BadgeMember__c>();
        for(FieloEE__BadgeMember__c bmNew: triggerNew){
            if(bmNew.FieloCH__Status__c != 'Pending'){
                triggerNewFiltered.add(bmNew);
            }
        }
        return triggerNewFiltered;
    }
    
    /********************************************************************
    METHOD: generateExpirationDate
    MODE: before insert, before update
    DML: NO
    COMMENTS: Generate expiration date for the new badgemembers
    ********************************************************************/
    public static void generateExpirationDate(List<FieloEE__BadgeMember__c> triggerNew, Map<Id,FieloEE__BadgeMember__c> triggerOldMap){
        Set<Id> badgesIds = new Set<Id>();
        for(FieloEE__BadgeMember__c bdmNew: triggerNew){
            if(trigger.isInsert){
                if(bdmNew.FieloEE__Badge2__c != null){
                    badgesIds.add(bdmNew.FieloEE__Badge2__c);
                }
            }else if(trigger.isUpdate){
                FieloEE__BadgeMember__c bmOld = (FieloEE__BadgeMember__c)triggerOldMap.get(bdmNew.id);
                if((bmOld.FieloEE__Quantity__c == null && bdmNew.FieloEE__Quantity__c != null ) || bdmNew.FieloEE__Quantity__c > Integer.valueof(bmOld.FieloEE__Quantity__c)){
                    badgesIds.add(bdmNew.FieloEE__Badge2__c);
                }
            }
        }
        
        if(!badgesIds.isEmpty()){
            //create map of badgeId=>challengeReward
            Map<Id,FieloCH__ChallengeReward__c> badgeChRewardMap = new Map<Id,FieloCH__ChallengeReward__c>();
            for(FieloCH__ChallengeReward__c chr: [SELECT FieloCH__Badge__c, F_PRM_ExpirationNumber__c, F_PRM_ExpirationType__c FROM FieloCH__ChallengeReward__c WHERE FieloCH__Badge__c IN: badgesIds]){
                badgeChRewardMap.put(chr.FieloCH__Badge__c, chr);
            }
            
            if(!badgeChRewardMap.isEmpty()){
                for(FieloEE__BadgeMember__c bdm: triggerNew){
                    if(badgeChRewardMap.containsKey(bdm.FieloEE__Badge2__c)){
                        FieloCH__ChallengeReward__c chReward = badgeChRewardMap.get(bdm.FieloEE__Badge2__c);
                        if(chReward.F_PRM_ExpirationType__c != FieloPRM_UTILS_ChallengeReward.EXPIRATION_TYPE_NO_EXPIRATION){
                            bdm.F_PRM_ExpirationDate__c = FieloPRM_UTILS_ChallengeReward.calculateExpirationDate(chReward, Integer.valueof(chReward.F_PRM_ExpirationNumber__c));
                        }
                    }
                }
            }
        }
        
        for(FieloEE__BadgeMember__c bdm: triggerNew){
            if(bdm.F_PRM_ExpirationDate__c != null){
                bdm.F_PRM_ExpirationDateText__c = FieloPRM_UTILS_BadgeMember.getExpirationDateText(bdm.F_PRM_ExpirationDate__c);
            }
        }
    }
    
    /********************************************************************
    METHOD: badgesMembersSetActiveInactive
    MODE: before insert, before update
    DML: UPDATE FieloEE__BadgeMember__c
    COMMENTS: Checks the current active Badge Member by Member / Challenge
    ********************************************************************/
    public static void badgesMembersSetActiveInactive(List<FieloEE__BadgeMember__c> triggerNew, map<id,FieloEE__BadgeMember__c> triggerOldMap){
        set<Id> setBadgeMembersInTrigger = new set<Id>();
        set<Id> setMembers = new set<Id>();
        set<Id> setBadges = new set<Id>();
        
        for(FieloEE__BadgeMember__c bmNew: triggerNew){
            if(trigger.isInsert){
                setMembers.add(bmNew.FieloEE__Member2__c);
                setBadges.add(bmNew.FieloEE__Badge2__c);
            }else if(trigger.isUpdate){
                FieloEE__BadgeMember__c bmOld = (FieloEE__BadgeMember__c)triggerOldMap.get(bmNew.id);
                if((bmNew.FieloEE__Quantity__c != null && triggerOldMap.get(bmNew.id).FieloEE__Quantity__c == null) || (bmNew.FieloEE__Quantity__c > Integer.valueof(triggerOldMap.get(bmNew.id).FieloEE__Quantity__c)) || (bmNew.FieloCH__Status__c != 'Pending' && bmOld.FieloCH__Status__c == 'Pending')){
                    setMembers.add(bmNew.FieloEE__Member2__c);
                    setBadges.add(bmNew.FieloEE__Badge2__c);
                    setBadgeMembersInTrigger.add(bmNew.Id);
                }
            }
        }
        
        if(!setBadges.isEmpty()){
            map<Id, FieloEE__Badge__c> mapBadge = new map<Id, FieloEE__Badge__c>([SELECT Id, F_PRM_Challenge__c FROM FieloEE__Badge__c WHERE Id IN: setBadges AND RecordType.DeveloperName = 'F_PRM_ProgramLevel']);
            set<Id> setChallengeIds = new set<Id>();
            for(FieloEE__Badge__c b: mapBadge.values()){
                if(b.F_PRM_Challenge__c != null){
                    setChallengeIds.add(b.F_PRM_Challenge__c);
                }
            }
            
            map<Id, Decimal> mapBadgePosition = new map<Id, Decimal>();
            for(FieloCH__ChallengeReward__c cr: [SELECT Id, FieloCH__Badge__c, FieloCH__Challenge__c, FieloCH__Position__c FROM FieloCH__ChallengeReward__c WHERE FieloCH__Challenge__c IN: setChallengeIds]){
                mapBadgePosition.put(cr.FieloCH__Badge__c, cr.FieloCH__Position__c);
            }
            
            map<String, FieloEE__BadgeMember__c> mapMemberChallengeActiveBadge = new map<String, FieloEE__BadgeMember__c>();
            for(FieloEE__BadgeMember__c bm: [SELECT Id, F_PRM_Status__c, FieloEE__Member2__c, FieloEE__Badge2__c, FieloEE__Badge2__r.F_PRM_Challenge__c FROM FieloEE__BadgeMember__c WHERE Id NOT IN: setBadgeMembersInTrigger AND FieloEE__Member2__c IN: setMembers AND FieloEE__Badge2__r.F_PRM_Challenge__c IN: setChallengeIds AND F_PRM_Status__c = 'Active']){
                String key = bm.FieloEE__Member2__c + '-' + bm.FieloEE__Badge2__r.F_PRM_Challenge__c;
                mapMemberChallengeActiveBadge.put(key, bm);
            }
            
            List<FieloEE__BadgeMember__c> badgeMemberToInactivate = new List<FieloEE__BadgeMember__c>();
            for(FieloEE__BadgeMember__c bm: triggerNew){
                bm.F_PRM_Status__c = null;
                if(mapBadge.containsKey(bm.FieloEE__Badge2__c)){
                    bm.F_PRM_Status__c = 'Inactive';
                    if(mapBadgePosition.containsKey(bm.FieloEE__Badge2__c)){
                        if(mapBadge.get(bm.FieloEE__Badge2__c).F_PRM_Challenge__c != null && mapMemberChallengeActiveBadge.containsKey(bm.FieloEE__Member2__c + '-' + mapBadge.get(bm.FieloEE__Badge2__c).F_PRM_Challenge__c)){
                            FieloEE__BadgeMember__c actualActiveBM = mapMemberChallengeActiveBadge.get(bm.FieloEE__Member2__c + '-' + mapBadge.get(bm.FieloEE__Badge2__c).F_PRM_Challenge__c);
                            if(mapBadgePosition.containsKey(actualActiveBM.FieloEE__Badge2__c)){
                                if(mapBadgePosition.get(bm.FieloEE__Badge2__c) < mapBadgePosition.get(actualActiveBM.FieloEE__Badge2__c)){
                                    bm.F_PRM_Status__c = 'Active';
                                    actualActiveBM.F_PRM_Status__c = 'Inactive';
                                    badgeMemberToInactivate.add(actualActiveBM);
                                }
                            }else{
                                bm.F_PRM_Status__c = 'Active';
                            }
                        }else{
                            bm.F_PRM_Status__c = 'Active';
                        }
                    }
                }
            }
            
            //DML: UPDATE FieloEE__BadgeMember__c
            update badgeMemberToInactivate;
        }
    }
    
    /********************************************************************
    METHOD: memberFeatureCreation
    MODE: after insert, after update
    DML: INSERT FieloPRM_MemberFeature__c, INSERT FieloPRM_MemberFeatureDetail__c
    COMMENTS: Create Member Features
    ********************************************************************/
    public static void memberFeatureCreation(List<FieloEE__BadgeMember__c> triggerNew, map<id,FieloEE__BadgeMember__c> triggerOldMap){
        Id levelRecordTypeId = Schema.SObjectType.FieloPRM_MemberFeatureDetail__c.getRecordTypeInfosByName().get('Level').getRecordTypeId();
        Id levelFeatureRecordTypeId = Schema.SObjectType.FieloPRM_MemberFeature__c.getRecordTypeInfosByName().get('Automatic').getRecordTypeId();
        
        Set<Id> setBadges = new Set<Id>();
        Set<Id> setMembers = new Set<Id>();
        if(trigger.isUpdate){
            for(FieloEE__BadgeMember__c bmNew: triggerNew){
                FieloEE__BadgeMember__c bmOld = (FieloEE__BadgeMember__c)triggerOldMap.get(bmNew.id);
                if((bmNew.FieloEE__Badge2__c != bmOld.FieloEE__Badge2__c) || (bmNew.FieloCH__Status__c != 'Pending' && bmOld.FieloCH__Status__c == 'Pending')){
                    setBadges.add(bmNew.FieloEE__Badge2__c); 
                    setMembers.add(bmNew.FieloEE__Member2__c);
                }
            }
        }else{
            for(FieloEE__BadgeMember__c badgeMember: triggerNew){
                setBadges.add(badgeMember.FieloEE__Badge2__c); 
                setMembers.add(badgeMember.FieloEE__Member2__c);
            }
        }
        
        map<Id, FieloEE__Badge__c> mapBadge = new map<Id, FieloEE__Badge__c>([SELECT Id, (SELECT id, F_PRM_Feature__c, F_PRM_Badge__c FROM Badge_Features__r) FROM FieloEE__Badge__c WHERE Id IN: setBadges AND RecordType.DeveloperName = 'F_PRM_ProgramLevel']);
        
        if(!mapBadge.isEmpty()){
            map<id, FieloEE__Member__c> mapMember = new map<id, FieloEE__Member__c>([SELECT Id, PRMUIMSId__c FROM FieloEE__Member__c WHERE Id IN: setMembers]);
            
            map<String, FieloPRM_MemberFeature__c> mapKeyMemberFeatureInDB = new map<String, FieloPRM_MemberFeature__c>();
            for(FieloPRM_MemberFeature__c memberFeature: [SELECT Id, F_PRM_Member__c, F_PRM_Feature__c FROM FieloPRM_MemberFeature__c WHERE F_PRM_Member__c IN: setMembers]){
                String mapKey = string.valueof(memberFeature.F_PRM_Member__c) + '-' + string.valueof(memberFeature.F_PRM_Feature__c);
                mapKeyMemberFeatureInDB.put(mapKey, memberFeature);
            }
            
            list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailInsert = new list<FieloPRM_MemberFeatureDetail__c>();
            list<FieloPRM_MemberFeature__c> listMemberFeatureToInsert = new list<FieloPRM_MemberFeature__c>();
            map<String, List<Id>> mapMemberFeatureKeyListBadgesForNewMFDetail = new map<String, List<Id>>();
            
            for(FieloEE__BadgeMember__c badgeMember: triggerNew){
                if(mapBadge.containsKey(badgeMember.FieloEE__Badge2__c)){
                    for(FieloPRM_BadgeFeature__c badgeFeature: mapBadge.get(badgeMember.FieloEE__Badge2__c).Badge_Features__r){
                        
                        String mapKey = string.valueof(badgeMember.FieloEE__Member2__c) + '-' + string.valueof(badgeFeature.F_PRM_Feature__c);
                        if(!mapKeyMemberFeatureInDB.containsKey(mapKey)){
                            if(!mapMemberFeatureKeyListBadgesForNewMFDetail.containsKey(mapKey)){
                                //Member Feature creation
                                FieloPRM_MemberFeature__c memberFeatureNew = new FieloPRM_MemberFeature__c(
                                    F_PRM_Member__c = badgeMember.FieloEE__Member2__c,
                                    F_PRM_Feature__c = badgeFeature.F_PRM_Feature__c,
                                    PRMUIMSId__c = mapMember.containsKey(badgeMember.FieloEE__Member2__c) ? mapMember.get(badgeMember.FieloEE__Member2__c).PRMUIMSId__c : null,
                                    RecordTypeId = levelFeatureRecordTypeId,
                                    F_PRM_IsActive__c = false,
                                    F_PRM_IsSynchronic__c = false
                                );
                                listMemberFeatureToInsert.add(memberFeatureNew);
                                
                                mapMemberFeatureKeyListBadgesForNewMFDetail.put(mapKey, new List<Id>{badgeFeature.F_PRM_Badge__c});
                            }else{
                                mapMemberFeatureKeyListBadgesForNewMFDetail.get(mapKey).add(badgeFeature.F_PRM_Badge__c);
                            }
                        }else{
                            //Member Feature Detail creation
                            FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c(
                                F_PRM_MemberFeature__c = mapKeyMemberFeatureInDB.get(mapKey).Id,
                                F_PRM_Badge__c = badgeFeature.F_PRM_Badge__c,
                                recordTypeId = levelRecordTypeId,
                                F_PRM_isActive__c = true
                            );
                            listMemberFeatureDetailInsert.add(newMemberFeatureDetail);
                        }
                    }
                }
            }
            
            if(!listMemberFeatureToInsert.isEmpty()){
                //DML: INSERT FieloPRM_MemberFeature__c
                insert listMemberFeatureToInsert;
                
                for(FieloPRM_MemberFeature__c mf: listMemberFeatureToInsert){
                    String mapKey = String.ValueOf(mf.F_PRM_Member__c) + '-' + String.ValueOf(mf.F_PRM_Feature__c);
                    if(mapMemberFeatureKeyListBadgesForNewMFDetail.containsKey(mapKey)){
                        for(Id badgeId: mapMemberFeatureKeyListBadgesForNewMFDetail.get(mapKey)){
                            FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c(
                                F_PRM_MemberFeature__c = mf.Id,
                                F_PRM_Badge__c = badgeId,
                                recordTypeId = levelRecordTypeId,
                                F_PRM_isActive__c = true
                            );
                            listMemberFeatureDetailInsert.add(newMemberFeatureDetail);
                        }
                    }
                }
            }
            
            if(!listMemberFeatureDetailInsert.isEmpty()){
                //DML: INSERT FieloPRM_MemberFeatureDetail__c
                insert listMemberFeatureDetailInsert;
            }
        }
    }
    
    /********************************************************************
    METHOD: resetLevel
    MODE: after insert, after update
    DML: UPDATE FieloEE__BadgeMember__c
    COMMENTS: Inactivates Badge Member expired and activates the subsequent with higher position
    ********************************************************************/
    public static void resetLevel(List<FieloEE__BadgeMember__c> triggerNew, map<id, FieloEE__BadgeMember__c> triggerOldMap){
        Set<Id> setBadges = new Set<Id>();
        Set<Id> setMembers = new Set<Id>();
        for(FieloEE__BadgeMember__c badgeMember: triggerNew){
            if(trigger.isInsert){
                setBadges.add(badgeMember.FieloEE__Badge2__c);
                setMembers.add(badgeMember.FieloEE__Member2__c);
            }else if(trigger.isUpdate){
                if(badgeMember.FieloEE__Quantity__c != triggerOldMap.get(badgeMember.id).FieloEE__Quantity__c){
                    setBadges.add(badgeMember.FieloEE__Badge2__c);
                    setMembers.add(badgeMember.FieloEE__Member2__c);
                }
            }
        }
        
        if(!setBadges.isEmpty()){
            map<id,FieloEE__Badge__c> mapIdBadge = new map<id,FieloEE__Badge__c>([SELECT Id, F_PRM_TypeSelection__c, F_PRM_Challenge__c, F_PRM_Challenge__r.F_PRM_NumberOfLevels__c FROM FieloEE__Badge__c WHERE Id IN: setBadges]);
            list<FieloEE__BadgeMember__c> listBadgeMemberToPRocess = new list<FieloEE__BadgeMember__c>();
            for(FieloEE__BadgeMember__c badgeMember: triggerNew){
                if(mapIdBadge.containskey(badgeMember.FieloEE__Badge2__c)){
                    FieloEE__Badge__c badge = mapIdBadge.get(badgeMember.FieloEE__Badge2__c);
                    if(badge.F_PRM_TypeSelection__c == 'Program Level' && badge.F_PRM_Challenge__c != null && badge.F_PRM_Challenge__r.F_PRM_NumberOfLevels__c > 1){
                        listBadgeMemberToPRocess.add(badgeMember);
                    }
                }
            }
            if(listBadgeMemberToPRocess.size()>0){
                //ID jobID = System.enqueueJob(new FieloPRM_Queueable_ResetLevel(listBadgeMemberToPRocess));
                FieloPRM_Queueable_ResetLevel.listBadgeMember = listBadgeMemberToPRocess;
                //DML: UPDATE FieloEE__Badge__c
                FieloPRM_Queueable_ResetLevel.resetLevels();
            }
        }
    }
    
    /********************************************************************
    METHOD: serializationOnMember
    MODE: after insert, after update, before delete
    DML: UPDATE Account, UPDATE FieloEE__Member__c, DELETE FieloEE__MemberSegment__c, INSERT FieloEE__MemberSegment__c
    COMMENTS: Serialize the Badge Member on the Member and creates or deletes Member Segments
    ********************************************************************/
    public static void serializationOnMember(List<FieloEE__BadgeMember__c> triggerNewOld, map<id,FieloEE__BadgeMember__c> triggerOldMap){
        set<Id> setIdsMember = new set<Id>();
        map<Id, set<Id>> mapMemberIdDeletedBadges = new map<Id, set<Id>>();
        set<Id> setBadgeIdsDeleted = new set<Id>();
        set<Id> setBadgeMemberIdsDeleted = new set<Id>();
        
        for(FieloEE__BadgeMember__c bmNew: triggerNewOld){
            //INSERT
            if(trigger.isInsert){
                setIdsMember.add(bmNew.FieloEE__Member2__c);
            }
            //UPDATE
            if(trigger.isUpdate){
                FieloEE__BadgeMember__c bmOld = (FieloEE__BadgeMember__c)triggerOldMap.get(bmNew.id);
                if((bmNew.F_PRM_Status__c != bmOld.F_PRM_Status__c) || (bmNew.FieloCH__Status__c != 'Pending' && bmOld.FieloCH__Status__c == 'Pending')){
                    setIdsMember.add(bmNew.FieloEE__Member2__c);
                }
            }
            //DELETE
            if(trigger.isDelete){
                setBadgeMemberIdsDeleted.add(bmNew.Id);
                setIdsMember.add(bmNew.FieloEE__Member2__c);
                setBadgeIdsDeleted.add(bmNew.FieloEE__Badge2__c);
                if(mapMemberIdDeletedBadges.containsKey(bmNew.FieloEE__Member2__c)){
                    mapMemberIdDeletedBadges.get(bmNew.FieloEE__Member2__c).add(bmNew.FieloEE__Badge2__c);
                }else{
                    mapMemberIdDeletedBadges.put(bmNew.FieloEE__Member2__c, new set<Id>{bmNew.FieloEE__Badge2__c});
                }
            }
        }
        
        if(!setIdsMember.isEmpty()){
            set<String> setBadgeBFOPropertiesValidTypes = new set<String>();
            for(FieloPRM_ValidBadgeTypes__c typ: FieloPRM_ValidBadgeTypes__c.getAll().values()){
                setBadgeBFOPropertiesValidTypes.add(typ.FieloPRM_Type__c);    
            }
            
            List<FieloEE__Member__c> members = [SELECT id, F_PRM_ProgramLevels__c, F_PRM_SegmentationAttribute__c, F_Account__c, F_Account__r.PRMIsEnrolledAccount__c, 
                                               (SELECT F_PRM_Status__c, FieloEE__Badge2__r.Name, FieloEE__Badge2__r.F_PRM_Type__c, FieloEE__Badge2__r.F_PRM_BadgeAPIName__c, FieloEE__Badge2__r.F_PRM_Segment__c FROM FieloEE__BadgesMembers__r WHERE Id NOT IN: setBadgeMemberIdsDeleted), 
                                               (SELECT FieloEE__Segment2__c, FieloEE__Member2__c FROM FieloEE__MemberSegments__r)
                                               FROM FieloEE__Member__c WHERE id IN: setIdsMember];
            
            map<Id, FieloEE__Badge__c> deletedBadges = new map<Id, FieloEE__Badge__c>([SELECT Id, F_PRM_Segment__c FROM FieloEE__Badge__c WHERE Id IN: setBadgeIdsDeleted]);
            
            map<Id, Account> accountsToUpdate = new map<Id, Account>();
            List<FieloEE__MemberSegment__c> memberSegmentsToDelete = new List<FieloEE__MemberSegment__c>();
            List<FieloEE__MemberSegment__c> memberSegmentsToInsert = new List<FieloEE__MemberSegment__c>();
            
            for(FieloEE__Member__c member: members){
                member.F_PRM_ProgramLevels__c = '';
                member.F_PRM_SegmentationAttribute__c = '';
                
                set<Id> setMemberSegmentsToCheckDelete = new set<Id>();
                set<Id> setMemberSegmentsToCheckInsert = new set<Id>();
                
                for(FieloEE__BadgeMember__c bm: member.FieloEE__BadgesMembers__r){
                    if(bm.FieloEE__Badge2__r.F_PRM_Type__c == 'Program Level'){
                        if(bm.F_PRM_Status__c == 'Active'){
                            member.F_PRM_ProgramLevels__c += member.F_PRM_ProgramLevels__c == '' ? bm.FieloEE__Badge2__r.Name : ',' + bm.FieloEE__Badge2__r.Name;
                            if(bm.FieloEE__Badge2__r.F_PRM_Segment__c != null){
                                setMemberSegmentsToCheckInsert.add(bm.FieloEE__Badge2__r.F_PRM_Segment__c);
                            }
                        }else{
                            if(bm.FieloEE__Badge2__r.F_PRM_Segment__c != null){
                                setMemberSegmentsToCheckDelete.add(bm.FieloEE__Badge2__r.F_PRM_Segment__c);
                            }
                        }
                    }
                    if(bm.FieloEE__Badge2__r.F_PRM_Type__c != null && setBadgeBFOPropertiesValidTypes.contains(bm.FieloEE__Badge2__r.F_PRM_Type__c) && bm.FieloEE__Badge2__r.F_PRM_BadgeAPIName__c != null){
                        member.F_PRM_SegmentationAttribute__c += member.F_PRM_SegmentationAttribute__c == '' ? bm.FieloEE__Badge2__r.F_PRM_BadgeAPIName__c : ',' + bm.FieloEE__Badge2__r.F_PRM_BadgeAPIName__c;
                    }
                }
                
                if(mapMemberIdDeletedBadges.containsKey(member.Id)){
                    for(Id badgeId: mapMemberIdDeletedBadges.get(member.Id)){
                        setMemberSegmentsToCheckDelete.add(deletedBadges.get(badgeId).F_PRM_Segment__c);
                    }
                }
                
                set<Id> setExistingMemberSegments = new set<Id>();
                for(FieloEE__MemberSegment__c ms: member.FieloEE__MemberSegments__r){
                    setExistingMemberSegments.add(ms.FieloEE__Segment2__c);
                    if(setMemberSegmentsToCheckDelete.contains(ms.FieloEE__Segment2__c)){
                        memberSegmentsToDelete.add(ms);
                    }
                }
                
                for(Id idSegment: setMemberSegmentsToCheckInsert){
                    if(!setExistingMemberSegments.contains(idSegment)){
                        memberSegmentsToInsert.add(new FieloEE__MemberSegment__c(FieloEE__Member2__c = member.Id, FieloEE__Segment2__c = idSegment));
                    }
                }
                
                if(member.F_Account__c != null){
                    if(member.F_PRM_ProgramLevels__c == ''){
                        if(member.F_Account__r.PRMIsEnrolledAccount__c == true){
                            accountsToUpdate.put(member.F_Account__c, new Account(Id = member.F_Account__c, PRMIsEnrolledAccount__c = false));
                        }
                    }else{
                        if(member.F_Account__r.PRMIsEnrolledAccount__c == false){
                            accountsToUpdate.put(member.F_Account__c, new Account(Id = member.F_Account__c, PRMIsEnrolledAccount__c = true));
                        }
                    }
                }
            }
            
            //DML: UPDATE Account
            update accountsToUpdate.values();
            //DML: UPDATE FieloEE__Member__c
            update members;
            //DML: DELETE FieloEE__MemberSegment__c
            delete memberSegmentsToDelete;
            //DML: INSERT FieloEE__MemberSegment__c
            insert memberSegmentsToInsert;
        }
    }
    
    /********************************************************************
    METHOD: memberFeatureUpdateActiveInactive
    MODE: after update
    DML: UPDATE FieloPRM_MemberFeatureDetail__c
    COMMENTS: Inactivates or activates Member Feature details as the Status of the Badge Member
    ********************************************************************/
    public static void memberFeatureUpdateActiveInactive(List<FieloEE__BadgeMember__c> triggerNew, map<id,FieloEE__BadgeMember__c> triggerOldMap){
        Set<Id> setBadges = new Set<Id>();
        Set<Id> setMembers = new Set<Id>();
        map<String,FieloEE__BadgeMember__c> mapMembBadge = new map<String,FieloEE__BadgeMember__c>();
        
        for(FieloEE__BadgeMember__c bmNew: triggerNew){
            FieloEE__BadgeMember__c bmOld = (FieloEE__BadgeMember__c)triggerOldMap.get(bmNew.id);
            if((bmNew.F_PRM_Status__c != bmOld.F_PRM_Status__c) || (bmNew.FieloCH__Status__c != 'Pending' && bmOld.FieloCH__Status__c == 'Pending')){
                setBadges.add(bmNew.FieloEE__Badge2__c); 
                setMembers.add(bmNew.FieloEE__Member2__c);
                mapMembBadge.put(String.valueOf(bmNew.FieloEE__Member2__c) + String.valueOf(bmNew.FieloEE__Badge2__c), bmNew);
            }
        }
        
        if(!setMembers.isEmpty()){
            list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailToUpdate = new list<FieloPRM_MemberFeatureDetail__c>();
            for(FieloPRM_MemberFeatureDetail__c mfd: [SELECT id,F_PRM_MemberFeature__r.F_PRM_Member__c,F_PRM_Badge__c, F_PRM_isActive__c FROM FieloPRM_MemberFeatureDetail__c WHERE F_PRM_MemberFeature__r.F_PRM_Member__c in: setMembers AND F_PRM_Badge__c =: setBadges]){
                string memBadgeKey = String.valueOf(mfd.F_PRM_MemberFeature__r.F_PRM_Member__c) + String.valueOf(mfd.F_PRM_Badge__c);
                if(mapMembBadge.containskey(memBadgeKey)){
                    FieloEE__BadgeMember__c badgeMember = mapMembBadge.get(memBadgeKey);
                    if((badgeMember.F_PRM_Status__c == 'Inactive' || String.isBlank(badgeMember.F_PRM_Status__c) ) && triggerOldMap.get(badgeMember.id).F_PRM_Status__c == 'Active'){
                        mfd.F_PRM_isActive__c =  false;
                        listMemberFeatureDetailToUpdate.add(mfd);
                    }else if(badgeMember.F_PRM_Status__c == 'Active' && (triggerOldMap.get(badgeMember.id).F_PRM_Status__c == 'Inactive' || String.isBlank(triggerOldMap.get(badgeMember.id).F_PRM_Status__c) )){
                        mfd.F_PRM_isActive__c =  true;
                        listMemberFeatureDetailToUpdate.add(mfd);
                    }
                }
            }
            
            if(listMemberFeatureDetailToUpdate.size() > 0){
                //DML: UPDATE FieloPRM_MemberFeatureDetail__c
                update listMemberFeatureDetailToUpdate;
            }
        }
    }
    
    /********************************************************************
    METHOD: memberFeatureDeletion
    MODE: before delete
    DML: UPDATE FieloPRM_MemberFeatureDetail__c
    COMMENTS: Inactivates Member Feature details related to the Badges deleted
    ********************************************************************/
    public static void memberFeatureInactivation(List<FieloEE__BadgeMember__c> triggerOld){
        Set<Id> setBadges = new Set<Id>();
        Set<Id> setMembers = new Set<Id>();
        set<String> setMemBadgeKey = new set<String>();
        for(FieloEE__BadgeMember__c badgeMember: triggerOld){
            setBadges.add(badgeMember.FieloEE__Badge2__c); 
            setMembers.add(badgeMember.FieloEE__Member2__c);
            string memSegKey = String.valueOf(badgeMember.FieloEE__Member2__c) + String.valueOf(badgeMember.FieloEE__Badge2__c);
            setMemBadgeKey.add(memSegKey);
        }
        
        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetail = [SELECT id,F_PRM_MemberFeature__r.F_PRM_Member__c,F_PRM_Badge__c, F_PRM_isActive__c FROM FieloPRM_MemberFeatureDetail__c WHERE F_PRM_MemberFeature__r.F_PRM_Member__c IN: setMembers AND F_PRM_Badge__c IN: setBadges];
        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailToUpdate = new list<FieloPRM_MemberFeatureDetail__c>();
        for(FieloPRM_MemberFeatureDetail__c mfd: listMemberFeatureDetail){
            string memSegKey = String.valueOf(mfd.F_PRM_MemberFeature__r.F_PRM_Member__c) + String.valueOf(mfd.F_PRM_Badge__c);
            if(setMemBadgeKey.contains(memSegKey)){
                mfd.F_PRM_isActive__c =  false;
                listMemberFeatureDetailToUpdate.add(mfd);
            }
        }
        //DML: UPDATE FieloPRM_MemberFeatureDetail__c
        update listMemberFeatureDetailToUpdate;
    }
    
}