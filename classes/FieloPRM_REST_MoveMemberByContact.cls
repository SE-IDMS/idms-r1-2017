/********************************************************************************************************
    Author: Fielo Team
    Date: 11/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestMoveMemberByContact/*')
global class FieloPRM_REST_MoveMemberByContact{   

    /**
    * [postMoveMemberByContact ]
    * @method   postMoveMemberByContact
    * @Pre-conditions  
    * @Post-conditions 
    * @return   String    []
    */
    @HttpPost
    global static String postMoveMemberByContact(String contactIdFrom, String contactIdTo){    

        return FieloPRM_UTILS_Member.postMoveMemberByContact(contactIdFrom,contactIdTo);

    
    }   
}