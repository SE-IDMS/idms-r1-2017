/*
    Author          : Bhuvana Subramaniyan 
    Description     : Test Class for VFC_OpptyMessage.
*/
@isTest
private class VFC_OpptyMessage_TEST {

        static testMethod void bofFormTest() {

        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();  
        Country__c country =  Utils_TestMethods.createCountry();
        insert country;

        Account acc = Utils_TestMethods.createAccount();
        acc.Country__c=country.Id;
        insert acc;        

        Contact vcpContact = new Contact(
              FirstName='Test',
              LastName='lastname',
              AccountId=acc.Id,
              JobTitle__c='Z3',
              CorrespLang__c='EN',
              WorkPhone__c='1234567890',
              Country__c=country.Id
              );


        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName=System.Label.CLMAR16SLS05;
        opp2.OpptyType__c=System.Label.CL00240;
        opp2.TECH_AmountEUR__c=1000001;
        insert opp2;
        
        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.Amount__c = 110000;
            prodLines.add(pl1);
        }
        
        Database.SaveResult[] lsr = Database.Insert(prodLines); 
        OPP_ValueChainPlayers__c valChain = Utils_TestMethods.createValueChainPlayer(acc.Id, null, opp2.Id);
        valChain.AccountRole__c='Vendor';
        insert valChain;

        OPP_ValueChainPlayers__c valChain2 = Utils_TestMethods.createValueChainPlayer(null, vcpContact.Id, opp2.Id);
        valChain2.ContactRole__c=System.Label.CLMAY13PRM40;
        insert valChain2;

        ApexPages.StandardController controller = new ApexPages.StandardController(opp2);
        VFC_OpptyMessage opptyMsg = new VFC_OpptyMessage(controller);
        
        opp2.StageName=System.Label.CLMAR16SLS01;
        update opp2;
        opptyMsg = new VFC_OpptyMessage(controller);
        
        opp2.StageName=System.Label.CLMAR16SLS02;
        update opp2;
        opptyMsg = new VFC_OpptyMessage(controller);
        
        opp2.StageName=System.Label.CLMAR16SLS03;
        update opp2;
        opptyMsg = new VFC_OpptyMessage(controller);
        
        opp2.StageName=System.Label.CLMAR16SLS04;
        update opp2;
        opptyMsg = new VFC_OpptyMessage(controller);
        
        
        }
}