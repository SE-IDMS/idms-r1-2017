public with sharing class SyncObjectsTestUtil {
	
	/* generates a CORRECT set on contacts and accounts. With a ObjectSync__c records */
    public static ObjectSync__c createContactAccountBulk(String ownMapping , integer size){
	
		//CREACION ��
		list<Account> accounts = new list<Account>();
		string name = 'account';
		string site = 'site';
		string SicDesc = 'SicDesc';

		integer forsize = 100;
		if(size != null){
			forsize = size;
		}
		for(integer i = 0 ; i < forsize ; i++){
			accounts.add(new account(name=name + i, site=site+i, SicDesc=SicDesc+i));
		}
		insert accounts;

		list<contact> contacts = new list<Contact>();
		string firstname = 'FN test';
		string lastname = 'LN test';
		string title = 'Title test';
		for(integer i = 0 ; i < forsize - 1 ; i++){
			account ac =accounts.get(i);
			contacts.add(new contact(accountId = ac.Id, firstname=firstname + i, lastname=lastname+i, title=title+i));
		}
		insert contacts;


		ObjectSync__c obj = new ObjectSync__c();
		obj.FromObject_API_Name__c = 'Account';
		obj.ToObject_API_Name__c = 'Contact';
		obj.FromIdentifier__c = 'Id';
		obj.ToIdentifier__c = 'AccountId';
		if(ownMapping != null){
			obj.MappingFields_JSON__c = ownMapping; 
		}else{
			obj.MappingFields_JSON__c = '[{"fromApiField":"Name","toApiField":"FirstName","Nillable":false,"mappingEnabled":true},{"fromApiField":"SicDesc","toApiField":"Lastname","Nillable":false,"mappingEnabled":true}]'; 
		}

		insert obj;
    
    	return obj;
    }
    
}