/*
Author          : Global Delivery Team - Bangalore
Date Created    : 
Description     : Test class for AP_TeamCaseJunction class.
*/
@isTest 
private class AP_TeamCaseJunction_Test {

    static testMethod void TeamCaseJunction_Test() { 
    
        // Data creations
    
        CaseClassification__c  cc = Utils_TestMethods.createCaseClassification();
        insert cc;
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        CustomerCareTeam__c  cct=Utils_TestMethods.CustomerCareTeam(country.id);
        insert cct;
        TeamCaseJunction__c tcj = new TeamCaseJunction__c();
        tcj.CaseClassification__c = cc.id;
        tcj.CCTeam__c = cct.id;
        tcj.Default__c=false;
        insert tcj;
        tcj.Default__c=true;
        update tcj;
        
        CaseClassification__c  cc1 = Utils_TestMethods.createCaseClassificationWithOutSubReason();
        
        insert cc1;
        
        TeamCaseJunction__c tcj2 = new TeamCaseJunction__c();
        tcj2.CaseClassification__c = cc1.id;
        tcj2.CCTeam__c = cct.id;
        tcj2.Default__c=true;
        insert tcj2;
        tcj2.Default__c=false;
        update tcj2;
        
        
        
        
        
        
    }
    
    
}