public with sharing class ProgramSnapshotController {   
  
    
    public String p{get;set;} 
    public String pageid;   
    public String period{get;set;}
    public string year{get;set;}
    public string statusnew{get;set;}
    public List<SelectOption> Ryear;
    public boolean isStatus{get;set;}
     
   
    List<ProgramSnapshot> PRS = new List<ProgramSnapshot>();
    
  
    public string getPageid(){
        
        return Pageid;
    }
   
    public ProgramSnapshotController(ApexPages.StandardController controller) {
    pageid = ApexPages.CurrentPage().getParameters().get('Id');
    isStatus = True;
     
    
        
    }
    
    // Declarations

    List<Program_Progress_Snapshot__c> sp = new List<Program_Progress_Snapshot__c>();
      
    List<IPO_Strategic_Program__c> parentprg = new List<IPO_Strategic_Program__c>();
  
    public string programs; 
    public boolean isEdit{get;set;}
    public boolean isSave{get;set;}
    public boolean isAdd{get;set;}
    public boolean btnSave{get;set;}
    public String Id{get;set;}
    public string getprograms(){
        return programs;
    }
 
 
   
 public List<ProgramProgressSnapshot> getPrgProgressSnapShot(){
 
  List<ProgramProgressSnapshot> PPS = new List<ProgramProgressSnapshot>();
   
   sp = [SELECT Id, IPO_Program__r.Name, IPO_Program__c,Program_Progress_Snapshot__c.Current_Year__c, IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Q1_Program_Health_Indicator__c, Q2_Program_Health_Indicator__c, 
             Q3_Program_Health_Indicator__c, Q4_Program_Health_Indicator__c, Status_in_Quarter_Q1__c, Status_in_Quarter_Q2__c, Status_in_Quarter_Q3__c,
             Status_in_Quarter_Q4__c, Decision_Review_Outcomes_Q1__c, Decision_Review_Outcomes_Q2__c, Decision_Review_Outcomes_Q3__c, Decision_Review_Outcomes_Q4__c, 
             Roadblocks_Q1__c, Roadblocks_Q2__c, Roadblocks_Q3__c, Roadblocks_Q4__c, IPO_Program_Health_Q1__c, IPO_Program_Health_Q2__c, IPO_Program_Health_Q3__c,
             IPO_Program_Health_Q4__c FROM Program_Progress_Snapshot__c Where IPO_Program__c = :pageid  ORDER BY Current_Year__c DESC ];
       
              
   for(Program_Progress_Snapshot__c PP : sp){
   if(PP.IPO_Program__c != null ){
    if(PP.IPO_Program_Health_Q4__c != null)   
   PPS.add(new ProgramProgressSnapshot(PP.Id, 'Q4' + '-' + PP.Current_Year__c, PP.Q4_Program_Health_Indicator__c, PP.Status_in_Quarter_Q4__c, PP.IPO_Program__r.Name, PP.Current_Year__c, PP.IPO_Program__r.id,PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name));
       if(PP.IPO_Program_Health_Q3__c != null) 
   PPS.add(new ProgramProgressSnapshot(PP.Id, 'Q3' + '-' + PP.Current_Year__c, PP.Q3_Program_Health_Indicator__c, PP.Status_in_Quarter_Q3__c, PP.IPO_Program__r.Name, PP.Current_Year__c, PP.IPO_PRogram__r.Id,PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name));
       if(PP.IPO_Program_Health_Q2__c != null) 
   PPS.add(new ProgramProgressSnapshot(PP.Id, 'Q2' + '-' + PP.Current_Year__c, PP.Q2_Program_Health_Indicator__c, PP.Status_in_Quarter_Q2__c, PP.IPO_Program__r.Name, PP.Current_Year__c, PP.IPO_Program__r.id,PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name));
       if(PP.IPO_Program_Health_Q1__c != null) 
   PPS.add(new ProgramProgressSnapshot(PP.Id, 'Q1' + '-' + PP.Current_Year__c, PP.Q1_Program_Health_Indicator__c, PP.Status_in_Quarter_Q1__c, PP.IPO_Program__r.Name, PP.Current_Year__c, PP.IPO_Program__r.id,PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name));
   }
   }
    return PPS;
    }
    
public List<ProgramSnapshot> getProgramSnapshot(){
     List<ProgramSnapshot> PPS = new List<ProgramSnapshot>();
     
      sp = [SELECT Id, IPO_Program__r.Name, IPO_Program__c,Program_Progress_Snapshot__c.Current_Year__c, IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Q1_Program_Health_Indicator__c, Q2_Program_Health_Indicator__c, 
             Q3_Program_Health_Indicator__c, Q4_Program_Health_Indicator__c, Status_in_Quarter_Q1__c, Status_in_Quarter_Q2__c, Status_in_Quarter_Q3__c,
             Status_in_Quarter_Q4__c, Decision_Review_Outcomes_Q1__c, Decision_Review_Outcomes_Q2__c, Decision_Review_Outcomes_Q3__c, Decision_Review_Outcomes_Q4__c, 
             Roadblocks_Q1__c, Roadblocks_Q2__c, Roadblocks_Q3__c, Roadblocks_Q4__c, IPO_Program_Health_Q1__c, IPO_Program_Health_Q2__c, IPO_Program_Health_Q3__c,
             IPO_Program_Health_Q4__c FROM Program_Progress_Snapshot__c Where IPO_Program__c = :pageid  and Current_Year__c = :year  ORDER BY Current_Year__c DESC ]; 
       
       
           
      if (period == '0' || year == '0' || (sp.size()==0)){  
               isEdit = True;
               isSave = False;
               
               parentprg = [Select Name,IPO_Strategic_Program_Leader_1__r.Name from IPO_Strategic_Program__c Where id = :pageid];                              
               PPS.add(new ProgramSnapshot(null,parentprg[0].Name, parentprg[0].IPO_Strategic_Program_Leader_1__r.Name, '', '', '','', ''));          
               }
      else{      
             
        for(Program_Progress_Snapshot__c PP : sp){
        
        if(isAdd == True){
          
            if(Period.Contains('Q1') && ((PP.IPO_Program_Health_Q1__c != null) || (PP.Status_in_Quarter_Q1__c != null) || (PP.Roadblocks_Q1__c != null) || (PP.Decision_Review_Outcomes_Q1__c != null)) ) {             
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q1__c, PP.Status_in_Quarter_Q1__c, PP.Roadblocks_Q1__c, PP.Decision_Review_Outcomes_Q1__c));
                isEdit= False;
                isSave = True;
                btnSave = True;
                isStatus = False;
                
            }         
            
            else if(Period.Contains('Q1') && (PP.IPO_Program_Health_Q1__c == null) && (PP.Status_in_Quarter_Q1__c == null) && (PP.Roadblocks_Q1__c == null) && (PP.Decision_Review_Outcomes_Q1__c == null) ){                
                PPS.add(new ProgramSnapshot(PP.id,parentprg[0].Name, parentprg[0].IPO_Strategic_Program_Leader_1__r.Name, '', '', '','', ''));          
                isEdit= True;
                isSave = False;
              //  btnSave = False;
            }
            
            else if(Period.contains('Q2') && ((PP.IPO_Program_Health_Q2__c != null) || (PP.Status_in_Quarter_Q2__c != null) || (PP.Roadblocks_Q2__c != null) || (PP.Decision_Review_Outcomes_Q2__c != null)) ){               
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q2__c, PP.Status_in_Quarter_Q2__c, PP.Roadblocks_Q2__c, PP.Decision_Review_Outcomes_Q2__c));
                isEdit = False;
                isSave = True;
                btnSave = True;
                isStatus = False;
                
            }              
                
            else if(Period.contains('Q2') && (PP.IPO_Program_Health_Q2__c == null) && (PP.Status_in_Quarter_Q2__c == null) && (PP.Roadblocks_Q2__c == null) && (PP.Decision_Review_Outcomes_Q2__c == null) ){               
               PPS.add(new ProgramSnapshot(PP.id,parentprg[0].Name, parentprg[0].IPO_Strategic_Program_Leader_1__r.Name, '', '', '','', ''));          
               isEdit= True;
               isSave = False;
            }
             
            else if(Period.contains('Q3') && ((PP.IPO_Program_Health_Q3__c != null) || (PP.Status_in_Quarter_Q3__c != null) || (PP.Roadblocks_Q3__c != null) || (PP.Decision_Review_Outcomes_Q3__c != null)) ){
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q3__c, PP.Status_in_Quarter_Q3__c, PP.Roadblocks_Q3__c, PP.Decision_Review_Outcomes_Q3__c));
                isEdit = False;
                isSave = True;
                btnSave = True;
                isStatus = False;
                
            }
                             
            else if(Period.contains('Q3')&& (PP.IPO_Program_Health_Q3__c == null) && (PP.Status_in_Quarter_Q3__c == null) && (PP.Roadblocks_Q3__c == null) && (PP.Decision_Review_Outcomes_Q3__c == null) ){  
                PPS.add(new ProgramSnapshot(PP.id,parentprg[0].Name, parentprg[0].IPO_Strategic_Program_Leader_1__r.Name, '', '', '','', '')); 
                isEdit= True;
                isSave = False;
            }
            
            else if(Period.contains('Q4')&& ((PP.IPO_Program_Health_Q4__c != null) || (PP.Status_in_Quarter_Q4__c != null) || (PP.Roadblocks_Q4__c != null) || (PP.Decision_Review_Outcomes_Q4__c != null))){
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q4__c, PP.Status_in_Quarter_Q4__c, PP.Roadblocks_Q4__c, PP.Decision_Review_Outcomes_Q4__c));
                isEdit = False;
                isSave = True;
                btnSave = True;
                isStatus = False;
                
            }
              
            else if(Period.contains('Q4')&& (PP.IPO_Program_Health_Q4__c == null) && (PP.Status_in_Quarter_Q4__c == null) && (PP.Roadblocks_Q4__c == null) && (PP.Decision_Review_Outcomes_Q4__c == null)){  
                PPS.add(new ProgramSnapshot(PP.id,parentprg[0].Name, parentprg[0].IPO_Strategic_Program_Leader_1__r.Name, '', '', '','', '')); 
                isEdit= True;
                isSave = False;
            }
        }
        Else if(isAdd == False)
         {
         
          if(Period.Contains('Q1'))               
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q1__c, PP.Status_in_Quarter_Q1__c, PP.Roadblocks_Q1__c, PP.Decision_Review_Outcomes_Q1__c));          

            else if(Period.contains('Q2'))
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q2__c, PP.Status_in_Quarter_Q2__c, PP.Roadblocks_Q2__c, PP.Decision_Review_Outcomes_Q2__c));              

            else if(Period.contains('Q3'))
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q3__c, PP.Status_in_Quarter_Q3__c, PP.Roadblocks_Q3__c, PP.Decision_Review_Outcomes_Q3__c));              

            else if(Period.contains('Q4'))
                PPS.add(new ProgramSnapshot(PP.id,PP.IPO_Program__r.Name, PP.IPO_Program__r.IPO_Strategic_Program_Leader_1__r.Name, Period, PP.IPO_Program_Health_Q4__c, PP.Status_in_Quarter_Q4__c, PP.Roadblocks_Q4__c, PP.Decision_Review_Outcomes_Q4__c)); 
         }
       }  // For loop ends
         
      } // Main else ends
      if(!isAdd)
        statusnew = PPS[0].status;
       
        PRS = PPS; 
        return PPS; 
                                                    
    } 
    
public List<Milestone> getMilestoneSummary(){
 List<Milestone> MIL = new List<Milestone>();
 List<IPO_Strategic_Milestone__c> ml = new List<IPO_Strategic_Milestone__c>([SELECT Id, Name, IPO_Strategic_Milestone_Name__c, Due_Date__c, Progress_Summary_Q1__c, Progress_Summary_Q2__c, 
                             Progress_Summary_Q3__c, Progress_Summary_Q4__c, Status_in_Quarter_Q1__c, Status_in_Quarter_Q2__c, 
                             Status_in_Quarter_Q3__c, Status_in_Quarter_Q4__c, Roadblocks_Q1__c, Roadblocks_Q2__c, Roadblocks_Q3__c, 
                             Roadblocks_Q4__c, Decision_Review_Outcomes_Q1__c, Decision_Review_Outcomes_Q2__c, Decision_Review_Outcomes_Q3__c, 
                             Decision_Review_Outcomes_Q4__c, IPO_Strategic_Program_Name__c, Current_Status__c, Current_Roadblocks__c, 
                             Current_Progress_in_Quarter__c, Current_Decision_review_Outcomes__c FROM IPO_Strategic_Milestone__c 
                             WHERE IPO_Strategic_Program_Name__c = :pageid AND Year__c = :year ORDER BY Due_Date__c]);
                                                                                    
        for(IPO_Strategic_Milestone__c m : ml){
             
                  
            if(Period.Contains('Q1'))
                MIL.add(new Milestone(m.Id, m.Name, m.IPO_Strategic_Milestone_Name__c, m.Due_Date__c, m.Progress_Summary_Q1__c, m.Status_in_Quarter_Q1__c, m.Roadblocks_Q1__c, m.Decision_Review_Outcomes_Q1__c));
            else if(Period.Contains('Q2'))               
                MIL.add(new Milestone(m.Id, m.Name, m.IPO_Strategic_Milestone_Name__c, m.Due_Date__c, m.Progress_Summary_Q2__c, m.Status_in_Quarter_Q2__c, m.Roadblocks_Q2__c, m.Decision_Review_Outcomes_Q2__c));
            else if(Period.Contains('Q3'))
                MIL.add(new Milestone(m.Id, m.Name, m.IPO_Strategic_Milestone_Name__c, m.Due_Date__c, m.Progress_Summary_Q3__c, m.Status_in_Quarter_Q3__c, m.Roadblocks_Q3__c, m.Decision_Review_Outcomes_Q3__c));
            else if(Period.Contains('Q4'))            
                MIL.add(new Milestone(m.Id, m.Name, m.IPO_Strategic_Milestone_Name__c, m.Due_Date__c, m.Progress_Summary_Q4__c, m.Status_in_Quarter_Q4__c, m.Roadblocks_Q4__c, m.Decision_Review_Outcomes_Q4__c));
            }
         
         
         return MIL;    
    }  
  
   
// Edit the Program Progress Snapshot

public pagereference EditRecord(){
isAdd = False;
isEdit = True;
isSave = False;
btnSave = False;
if (period.contains('-'))
 year = period.right(4);
else
  period = period + '-' + year;
PageReference pageRef = new PageReference('/apex/ProgramProgressHistory');
return pageRef;
}

// View Program Progress Snapshot Record

public pagereference ViewRecord(){
isAdd = False;
isEdit = False;
isSave = True;
btnSave = True;
year = period.right(4);
PageReference pageRef = new PageReference('/apex/ProgramProgressHistory');
return pageRef;
}

public pagereference AddSnapshot(){
isEdit = True;
btnSave = True;
Period = '0';
isAdd = True; 
statusnew = 'None';
PageReference pageRef = new PageReference('/apex/ProgramProgressHistory');
ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Select a Quarter, Year and Status to save the new record.'));
return pageRef;
}
// Cancel Button - Return to Prgram page

public pagereference Cancel(){
 isEdit = False;
 isSave = true;
 pageReference pref = new pageReference('/' + pageid);                 
 return pref;   
  }
  
  
  // Save Program Progress Snapshot Changes
  
public pagereference Save(){
            List<Program_Progress_Snapshot__c> ExtA = new List<Program_Progress_Snapshot__c>();
            List<Program_Progress_Snapshot__c> NewPrgm = new List<Program_Progress_Snapshot__c>();
            List<IPO_Strategic_Milestone__c> ExtM = new List<IPO_Strategic_Milestone__c>();
            List<IPO_Strategic_Milestone__c> NewMile = new List<IPO_Strategic_Milestone__c>();    
            
            if((statusnew == 'None') && isAdd == True){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Select a Status to save the new record.'));
                isEdit = True;
                isSave = False;
                isAdd = True; 
                btnSave = True;
            }
            else{
             if(period.length() == 2)
                period = period + '-' + year;
            
                ExtA = [SELECT id from Program_Progress_Snapshot__c where Current_Year__c = :year and IPO_Program__c = :pageid];
                                  
                For(ProgramSnapshot P : PRS) {
                Program_Progress_Snapshot__c UpPrgm = new Program_Progress_Snapshot__c(); 
                
                if(isAdd)
                  P.Status = statusnew;
                                 
                UpPrgm.Current_Year__c = year;
            
                system.debug('Programs available are ' + ExtA.size());
            
            if(ExtA.size() == 0)
                UpPrgm.IPO_Program__c = pageid;
            else{
                UpPrgm.id = P.id; 
                system.debug('Programs Id ' + UpPrgm.id);
                }
                
            if(Period.Contains('Q1')){
                      
            UpPrgm.IPO_Program_Health_Q1__c = P.Status;
            UpPrgm.Status_in_Quarter_Q1__c = P.Progress;
            UpPrgm.Roadblocks_Q1__c = P.Roadblock;
            UpPrgm.Decision_Review_Outcomes_Q1__c = P.Decision;
            }
            else if(Period.Contains('Q2')){
                        
            UpPrgm.IPO_Program_Health_Q2__c = P.Status;
            UpPrgm.Status_in_Quarter_Q2__c = P.Progress;
            UpPrgm.Roadblocks_Q2__c = P.Roadblock;
            UpPrgm.Decision_Review_Outcomes_Q2__c = P.Decision;
            }
            else if(Period.Contains('Q3')){
                       
            UpPrgm.IPO_Program_Health_Q3__c = P.Status;
            UpPrgm.Status_in_Quarter_Q3__c = P.Progress;
            UpPrgm.Roadblocks_Q3__c = P.Roadblock;
            UpPrgm.Decision_Review_Outcomes_Q3__c = P.Decision;
            }
            else if(Period.Contains('Q4')){
            
            UpPrgm.IPO_Program_Health_Q4__c = P.Status;
            UpPrgm.Status_in_Quarter_Q4__c = P.Progress;
            UpPrgm.Roadblocks_Q4__c = P.Roadblock;
            UpPrgm.Decision_Review_Outcomes_Q4__c = P.Decision;
            }
            
            NewPrgm.add(UpPrgm);
           }
          try{
        
           if(ExtA.size() > 0)
            update NewPrgm;
           else             
             insert NewPrgm; 
                      
          }
          catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            }
                      
            isEdit = False;
            isSave = True;
            isAdd = False; 
            btnSave = True;
       }     
            return null;
        
    }
    
 // Get Program Progress Status
    
public List<SelectOption> getStatus() {
        List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('None','<Select a Status>'));
       //  options.add(new SelectOption('No Data','No Data'));
         options.add(new SelectOption('On Time Delivered','On Time Delivered'));
         options.add(new SelectOption('Slight Delay','Slight Delay'));
         options.add(new SelectOption('No Progress or Delayed over 1 Month','No Progress or Delayed over 1 Month'));
         
         return options;
     }
     
 // Quarter Selection for adding new records
 
 public List<SelectOption> getQuarter() {
        List<SelectOption> options = new List<SelectOption>();   
         options.add(new SelectOption('0','<Select a Quarter>'));      
         options.add(new SelectOption('Q1','Q1'));
         options.add(new SelectOption('Q2','Q2'));
         options.add(new SelectOption('Q3','Q3'));
         options.add(new SelectOption('Q4','Q4'));
         return options;
     }
     
 public List<SelectOption> getRyear() {
        List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('0','<Select a Year>'));
         options.add(new SelectOption('2013','2013'));
         options.add(new SelectOption('2014','2014'));
         return options;
     }
   
 public pagereference YearChange(){
  

  if(period == '0' || year == '0' || (statusnew == 'None')){
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Select a Quarter, Year and Status to save the new record.'));
    btnSave = True;
    }
  
  else{
       ApexPages.getMessages().clear();
       btnSave = False;
      }
 PageReference pageRef = new PageReference('/apex/ProgramProgressHistory');
 return pageRef;
 }  
     
//Wrapper Classes

public class ProgramProgressSnapshot{
    public string Id{get;set;}
    public String Period{get; set;}
    public String Status{get;set;}
    public String Summary{get;set;}
    public string Program{get;set;}
    public string Year{get;set;}
    public string ProgramId{get;set;}
    public string Leader{get;set;}
    public ProgramProgressSnapshot(string Id, string p, string s,string e, string prg, string y, string prgmid,string l){
        this.Id = id;
        this.Period =p; 
        this.Status=s;
        this.Summary=e;
        this.Program=prg;
        this.Year=y;
        this.ProgramId=prgmid;
        this.Leader = l;
    }
   } 
   
public class ProgramSnapshot{
        public String Id{get;set;}
        public String Program{get;set;}
        public String Leader{get;set;}
        public String Period{get;set;}
        public String Status{get;set;}
        public String Progress{get;set;}
        public String Roadblock{get;set;}
        public String Decision{get;set;}
        
         
        public ProgramSnapshot(id id,string p, string l, string pe, string s, string pr, string rd, string de){
                    
            this.id = id;
            this.Program=p;
            this.Leader=l;
            this.Period=pe;
            this.Status=s;
            this.Progress=pr;
            this.Roadblock=rd;
            this.Decision=de;       
        }
    }    
 public class Milestone{
        public String Id{get;set;}
        public String Identifier{get; set;}
        public String Name{get; set;}
        public Date Due_Date{get;set;}
        public String MilestoneStatus{get;set;}
        public String MilestoneProgress{get;set;}
        public String MilestoneRoadblock{get;set;}
        public String MilestoneOutcome{get;set;}        
        
    public Milestone(string id, string name, string n, Date d, string s, string p, string r, string o){
            this.Id=id;
            this.Identifier=name;
            this.Name=n; 
            this.Due_Date=d;
            this.MilestoneStatus=s;
            this.MilestoneProgress=p;
            this.MilestoneRoadblock=r;
            this.MilestoneOutcome=o;           
        }
    }
}