/**********************************************************************
 * 
 *
 * @url: /services/apexrest/rest
 * @data:
 *  {
        
    }
*************************************************************************/
@RestResource(urlMapping='/ProfileUpdate/*')
global without sharing class AP_IDMSProfileUpdate {
    /*
    **Method Name -updateProfile
    **
    **Purpose - Change the profile of the of Guest User at run time
    **
    */
    @HttpPatch
    global static String updateProfile(String  profileId, String userId) {
        
        update new User(Id=userId,ProfileId = profileId);
           
        return 'Profile Updated';
      }
     @HttpGet 
    global static void doProfileUpdate()
    {     
       try
       {
          String userId = ApexPages.currentPage().getParameters().get('userId');
          String profileId = ApexPages.currentPage().getParameters().get('profileId');
          update new User(Id=userId,ProfileId = profileId);
       }
       Catch(Exception e)
       {
         System.debug('::::user profile not update'+e);
       }
    }  
}