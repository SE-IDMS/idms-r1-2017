/*******************************************************************************
Created By : Shruti Karn
Created Date : 16-Jul-2013
Description  : For PRM October 2013 Release:
                1. Create and Edit Assessments
                BR # BR-3084, BR-3097, BR-3543, BR-3544, BR-3545, BR-3546, BR-3547,BR-3548, BR-3549, 
                BR-3550, BR-3551, BR-3552, BR-3553, BR-3554, BR-3555, BR-3556, BR-3557
/*******************************************************************************/
public class VCC_AssessmentController
{
    public String retURL{get;set;}
    public String SelectedAssment { get; set; }
    public sObject obj{get;set;}
    public String SelectedLB{get;set;}
    public List<SelectOption> AssmentOptions{get;set;}
    public list<String> lstQueryFieldValue{get;set;}
    public Map<id,OpportunityAssessmentAnswer__c> assessmentAnsMap{get;set;} 
    public List<QueAnsWrapper> AssmentData{get;set;}
    public Map<String,String> AssmentAnsOptnMap{get;set;}
    public Integer assessmentCount{get;set;}
    Map<id,list<OpportunityAssessmentResponse__c>> qIdAnsRes  =  new Map<id,list<OpportunityAssessmentResponse__c>>(); 
    map<String, String> mapQueryFieldValue = new map<String,String>();
    public Integer status{get;set;}
    public Boolean hasErrors{get;set;}
    public boolean isORF{set;} //Modified on 13-Jan-2015
    public String AssessmentStatus = 'submitted';
    
    /**************************************
    Constructor
    **************************************/
    public VCC_AssessmentController()
    {
        system.debug('****Constructor VCC_AssessmentController called*****');
        AssmentOptions= new List<SelectOption>();
        assessmentAnsMap =  new Map<id,OpportunityAssessmentAnswer__c>();
        AssmentData =  new List<QueAnsWrapper>();
        AssmentAnsOptnMap = new Map<String,String>();
        assessmentCount = 0;
        system.debug('**** End of Constructor VCC_AssessmentController call*****');
    }

    /******************************************************
    To create select option of all the Assessments 
    related to the selected object
    **********************************************************/
    public void getPopulateAssessment()
    {
        system.debug('****getPopulateAssessment() called*****');
        system.debug('obj :'+obj );
        AssmentOptions.clear();
        //String assessmentQuery = 'select id,Name from Assessment__c where Active__c = true and ';
        
        //Added by Sreenivas Sake for BR-4915
        String assessmentQuery = 'select Assessment__c,Assessment__r.Name from AssessementProgram__c where Assessment__r.Active__c = true and ';
        
        //Populate the selected Assessment on Edit
        if(obj != null && (obj.id != null || (obj.get('Assessment__c')!= null)))
        {    
            SelectedAssment = String.valueOF(obj.get('Assessment__c'));
            system.debug('SelectedAssment :'+SelectedAssment );
            //assessmentQuery = assessmentQuery + 'id ='+ '\''+SelectedAssment+'\'';
            
            //Added by Sreenivas Sake for BR-4915
            assessmentQuery = assessmentQuery + 'Assessment__c='+ '\''+SelectedAssment+'\'';
             
            prepareAssmentData();
            system.debug('SelectedAssmentafter :'+SelectedAssment );            
        }
                
        else if(lstQueryFieldValue!= null && !lstQueryFieldValue.isempty())
        {
            for(String str : lstQueryFieldValue)
            {
                if(str.contains(':'))
                    mapQueryFieldValue.put(str.split(':').get(0),str.split(':').get(1));
            }
            for(String field : mapQueryFieldValue.keySet())
                assessmentQuery = assessmentQuery + field +' ='+ '\'' +mapQueryFieldValue.get(field)+'\''  +' and ';
            assessmentQuery = assessmentQuery.removeEndIgnoreCase('and ');
        }
            system.debug('assessmentQuery :'+assessmentQuery );
            //list<Assessment__c> lstAssessment = new list<Assessment__c>();
            
            //Added by Sreenivas Sake for BR-4915
            list<AssessementProgram__c > lstAssessment = new list<AssessementProgram__c >();
            
            //lstAssessment = database.query(assessmentQuery);
            if((lstQueryFieldValue!=null || (SelectedAssment!= null && SelectedAssment.trim()!= null)) && SelectedAssment!=System.Label.CL00355 )
            {
                AssmentOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355)); 
                /*for (Assessment__c  a: database.query(assessmentQuery ))
                {
                    AssmentOptions.add(new SelectOption(a.id+'',a.Name));
                }*/
                
                 //Added by Sreenivas Sake
                for (AssessementProgram__c a: database.query(assessmentQuery ))
                {
                    AssmentOptions.add(new SelectOption(a.Assessment__c +'',a.Assessment__r.Name));
                }
                
                assessmentCount = AssmentOptions.size();
                if(AssmentOptions.size() ==2)
                { 
                    SelectedAssment = AssmentOptions[1].getValue();
                    populateQuestions();
                     
                }
                 
            }
            
            system.debug('SelectedAssment'+SelectedAssment);
        //}
        //else{AssmentOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355)); }
        
        system.debug('AssmentOptions:'+AssmentOptions.size());
        system.debug('****getPopulateAssessment() ends*****');
        
    }

    /****************************************************************************
        Calls prepareAssmentData from the Visualforce Page
    ****************************************************************************/
    public void populateQuestions()
    {
        system.debug('****populateQuestions() called*****');
        //if(SelectedAssment != System.Label.CL00355 )
            prepareAssmentData();
        system.debug('****populateQuestions() ends*****');
    }

    /****************************************************************************
        Created the list of Questions along with their answer and responses.
    ****************************************************************************/
    public void prepareAssmentData(){
        system.debug('****prepareAssmentData() called*****');
        system.debug('SelectedAssment in prepareAssmentData'+SelectedAssment);
        if(SelectedAssment != System.Label.CL00355)
        {
            AssmentData.clear();
            assessmentAnsMap.clear();
            AssmentAnsOptnMap.clear();
            AssmentAnsOptnMap.put(System.Label.CL00355,'');
            if(this.obj.id != null)
            {
                Set<id> questionIdSet=new Set<id>();
                List<OpportunityAssessmentResponse__c>  oppAssRes=[select Answer__c,AnswerOption__c,OpportunityAssessment__c,Question__c,QuestionText__c,ResponseText__c from OpportunityAssessmentResponse__c where PartnerAssessment__c  =:this.obj.id];
                for(OpportunityAssessmentResponse__c oar : oppAssRes)
                {
                    if(!qIdAnsRes.containsKey(oar.Question__c))
                        qIdAnsRes.put(oar.Question__c, new list<OpportunityAssessmentResponse__c>{( oar)});
                    else
                        qIdAnsRes.get(oar.Question__c).add( oar);
                }
            }
            AssmentData =  new List<QueAnsWrapper>();    
            Map<id,OpportunityAssessmentQuestion__c> oppAssQueMap =  new Map<id,OpportunityAssessmentQuestion__c>();
            Map<id,List<OpportunityAssessmentAnswer__c>> qidAnsRecListMap = new Map<id,List<OpportunityAssessmentAnswer__c>>();
            List<OpportunityAssessmentQuestion__c> oppAssQuestionsList=[select id , AnswerType__c ,Assessment__c,QuestionDescription__c, Required__c, Sequence__c, Weightage__c,ShowComments__c,CommentDescription__c
                                                                            from OpportunityAssessmentQuestion__c where Assessment__c =:SelectedAssment order by Sequence__c];
            System.debug('Assessment Questions:'+oppAssQuestionsList);
            if(oppAssQuestionsList!= null && oppAssQuestionsList.size()>0)
            {
                oppAssQueMap.putAll(oppAssQuestionsList);
                for(OpportunityAssessmentAnswer__c oPPAssAns:[select id, AnswerOption__c ,CommentDescription__c , Question__c ,Score__c, Sequence__c,ShowComments__c from OpportunityAssessmentAnswer__c where Question__c in :oppAssQueMap.keySet() order by Sequence__c asc])
                {
                    if(!qidAnsRecListMap.containsKey(oPPAssAns.Question__c))
                    {
                        qidAnsRecListMap.put(oPPAssAns.Question__c,new List<OpportunityAssessmentAnswer__c>());
                        qidAnsRecListMap.get(oPPAssAns.Question__c).add(oPPAssAns);
                    }
                    else
                    {
                        qidAnsRecListMap.get(oPPAssAns.Question__c).add(oPPAssAns);
                    }
                    assessmentAnsMap.put(oPPAssAns.id,oPPAssAns);
                    AssmentAnsOptnMap.put(oPPAssAns.id ,oPPAssAns.AnswerOption__c);
                }
                for(OpportunityAssessmentQuestion__c que:oppAssQuestionsList)
                {
                    QueAnsWrapper w = new QueAnsWrapper();
                    if(obj.id == null)
                        w.response = new OpportunityAssessmentResponse__c();
                    w.question = que;
                    //START: feb15 release
                    if(que.ShowComments__c && que.CommentDescription__c != null){
                       if(w.QuestionHelpText == null)
                            w.QuestionHelpText = que.CommentDescription__c; 
                       else
                            w.QuestionHelpText = w.QuestionHelpText + ', '+que.CommentDescription__c;
                    }               
                    //END: FEB15 Release
                    
                    system.debug('que.AnswerType__c:'+que.AnswerType__c);
                    if(que.AnswerType__c == Label.CLOCT13PRM11 || que.AnswerType__c == Label.CLOCT13PRM12 || que.AnswerType__c == Label.CLOCT13PRM29)
                    {
                        system.debug('in if:'+que.AnswerType__c);
                        system.debug('ques:'+que);
                        w.pickListOptions = new List<SelectOption>();
                        if(que.AnswerType__c == Label.CLOCT13PRM11)
                           w.pickListOptions.add(new SelectOption('',System.Label.CLMAR13PRM019)); 
                        system.debug('w.pickListOptions:'+w.pickListOptions);
                        //preparing Dropdown for the questions
                        if(qidAnsRecListMap.containsKey(que.id) )
                        {
                            for(OpportunityAssessmentAnswer__c opaa: qidAnsRecListMap.get(que.id)){
                                w.pickListOptions.add(new SelectOption(opaa.id,opaa.AnswerOption__c));
                                    
                                //feb15 release
                                    if(opaa.ShowComments__c && opaa.CommentDescription__c != null){
                                        if(w.AnswerHelpText == null)
                                            w.AnswerHelpText = opaa.AnswerOption__c+' = '+opaa.CommentDescription__c; 
                                        else
                                            w.AnswerHelpText = w.AnswerHelpText + ',  '+opaa.AnswerOption__c+' = '+opaa.CommentDescription__c;
                                    }  
                                
                            }   
                        }
                        
                    }
                    if(this.obj.id != null)
                    {
                        if(qIdAnsRes.Containskey(que.id))
                        {
                            for(OpportunityAssessmentResponse__c res: qIdAnsRes.get(que.id))
                            {
                                w.response =  res;
                                //if Answer Type is Multiselect picklist
                                if(que.answertype__c == Label.CLOCT13PRM12)
                                    w.lstSelectedAnswer.add(res.answer__c);
                                // if Answer Type is Picklist
                                if(que.answertype__c== Label.CLOCT13PRM11 && res.answer__c != null)
                                    w.selAnswer = res.answer__c;
               
                                if(que.answertype__c== Label.CLOCT13PRM29 && res.answer__c != null)
                                    w.selAnswer = res.answer__c;
                                 
                            }   
                        }
                    }
                   
                    AssmentData.add(w);
                }
            }

        }
        else
        {
            AssmentData.clear();
            
        }
        system.debug('****prepareAssmentData() ends*****');
    } 
    /*************************************************************
        URL where the page should redirect on Cancel.
    **************************************************************/
    public PageReference doCancel() 
    {
        system.debug('****doCancel() calls*****');
        if(UserInfo.getProfileId() == Label.CLOCT13PRM18)
            retURL = retURL+obj.id;
        PageReference pageRef = new PageReference(retURL);
        pageRef.setRedirect(true);
        return pageRef;
    }
    /*************************************************************
        Insert/update the Assessment
        Insert/Update the Response Records.
    **************************************************************/
    public PageReference doSave() 
    {
        system.debug('****doSave() calls*****');
        Savepoint sp;
        list<OpportunityAssessmentResponse__c> assmentResList = new List<OpportunityAssessmentResponse__c>();
        set<OpportunityAssessmentResponse__c> assmentResSet = new Set<OpportunityAssessmentResponse__c>();
        list<OpportunityAssessmentResponse__c> lstResponsetoDelete = new list<OpportunityAssessmentResponse__c>();
        hasErrors = false;
        status =0;
        
        try
        {
            sp = Database.setSavepoint(); // Added on 19-Jan-2015
            if(this.obj.id == null)
            {
                if(SelectedAssment != System.Label.CL00355 )
                {
                    obj.put('Assessment__c',SelectedAssment); 
                   // sp = Database.setSavepoint();
                    obj.put('TECH_Status__c', AssessmentStatus);
                    insert this.obj;
                    System.debug('obj inside try:'+obj);
                    
                }
                else
                {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select an Assessment','')); 
                    return null;
                }
            }
            
            if(AssmentData != null && AssmentData.size()>0)
            {
                for(QueAnsWrapper w:AssmentData )
                {
                    //Added by Akila for Oct 2014 Release
                    //Functionality : Answer mandatory for Questions marked Required.
                    System.debug('*** Question Required -'+w.question.id + ' -> '+w.question.required__c+ ' - '+w.response.responsetext__c );
                    System.debug('*** Assessment status ->'+AssessmentStatus);
                    if(AssessmentStatus != 'draft')
                    {
                        if(((w.question.answertype__c == Label.CLOCT13PRM10 && w.response.responsetext__c == '') || ((w.question.answertype__c == Label.CLOCT13PRM11 || w.question.answertype__c == Label.CLOCT13PRM29) && w.selAnswer == null) || (w.question.answertype__c == Label.CLOCT13PRM12 && w.lstSelectedAnswer.size()!=0)) && w.question.required__c == true )
                        {
                            System.debug('w.response.responsetext__c:'+w.response.responsetext__c);
                            hasErrors = True;
                            status = status + 1;
                            System.debug('*** Question id ->'+w.question.id);
                        }
                    }   
                    System.debug('w.response.responsetext__c:'+w.response.responsetext__c);
                    if(qIdAnsRes != null && !qIdAnsRes.isEmpty())
                    {
                        if(qIdAnsRes.containsKey(w.question.id))
                        {
                            for(OpportunityAssessmentResponse__c res : qIdAnsRes.get(w.question.id))
                            {
                                if(w.lstSelectedAnswer != null && !w.lstSelectedAnswer.isEmpty())
                                {
                                    lstResponsetoDelete.add(res);
                                    for(String s : w.lstSelectedAnswer)
                                    {
                                        OpportunityAssessmentResponse__c newResponse = new OpportunityAssessmentResponse__c();
                                        newResponse.answer__C = s;
                                        newResponse.question__c = res.question__c;
                                        newResponse.PartnerAssessment__c = this.obj.id;
                                        newResponse.recordTypeid = Label.CLOCT13PRM27;
                                        system.debug('newResponse:'+newResponse);
                                        assmentResSet.add(newResponse);
                                        system.debug('assmentResSet:'+assmentResSet);
                                    }
                                }
                                else if(w.selAnswer == System.Label.CL00355 && res.answer__c != null)
                                    res.answer__c = null;
                                else if(res.answer__c != w.selAnswer)
                                    res.answer__c = w.selAnswer;
                                if (res.responsetext__c != null && res.responsetext__c.trim()!=null 
                                && w.response.responsetext__c != null && w.response.responsetext__c.trim() != null
                                && res.responsetext__c.compareTo(w.response.responsetext__c) != 0)
                                {
                                    res.responsetext__c = w.response.responsetext__c;
                                }
                                else if(res.responsetext__c == null && w.response.responsetext__c != null && w.response.responsetext__c.trim() != null)
                                    res.responsetext__c = w.response.responsetext__c;
                                else if(res.responsetext__c != null && res.responsetext__c.trim()!=null && w.response.responsetext__c == null)
                                    res.responsetext__c = w.response.responsetext__c;
                                assmentResSet.add(res);
                            }
                        }
                    }
                    else
                    {
                        w.response.Question__c = w.question.id;
                        if(w.response.PartnerAssessment__c ==  null)
                        {
                            w.response.PartnerAssessment__c = this.obj.id;
                            w.response.recordTypeid = Label.CLOCT13PRM27;
                        }
                        if(w.question.answertype__c == Label.CLOCT13PRM11)
                        {
                            if(w.selAnswer != System.Label.CL00355)
                                w.response.Answer__c = w.selAnswer;
                            
                        }  
                        if(w.question.answertype__c == Label.CLOCT13PRM29)
                        {
                            if(w.selAnswer != System.Label.CL00355)
                                w.response.Answer__c = w.selAnswer;
                        }   
                        if(w.question.answertype__c == Label.CLOCT13PRM12)
                        {
                            if(w.lstSelectedAnswer != null && !w.lstSelectedAnswer.isEmpty())
                            for(String res : w.lstSelectedAnswer)
                            {
                                OpportunityAssessmentResponse__c AssmentResponse=new OpportunityAssessmentResponse__c();
                                AssmentResponse.question__c = w.question.id;
                                AssmentResponse.PartnerAssessment__c = this.obj.id;
                                AssmentResponse.answer__c = res;
                                AssmentResponse.recordTypeid = Label.CLOCT13PRM27;
                                system.debug('AssmentResponse:'+AssmentResponse);
                                assmentResSet.add(AssmentResponse);
                                system.debug('assmentResSet:'+assmentResSet);
                            }
                        }  
                        else if(w.response != null) //Modified for feb14 release, Fixed issue related to duplicate entry for multiselect answers
                            assmentResSet.add(w.response);
                    }
                }
            }
            System.debug('obj outside else:'+obj);
            //Added by Akila for Oct 2014 Release
            //Throws an Error Message if the Required Questions are not answered.
            System.debug('>>>>>>>>hasErrors'+status);
            if(status >0)
            {
               System.debug('>>>>>>>>hasErrors1'+status);
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLOCT14PRM02));
               return null;
            }
           
          else
           {  

            if(assmentResSet !=  null && assmentResSet.size()>0)
            {
                assmentResList.addAll(assmentResSet);
                upsert assmentResList ;
            }
            
            Decimal TotalScore = 0;
            for(OpportunityAssessmentResponse__c oasres:[select id,Score__c from  OpportunityAssessmentResponse__c where OpportunityAssessment__c  = :this.obj.id ]){
               
                if(oasres.Score__c != null){
                    TotalScore +=oasres.Score__c;
                }
            }
            System.debug('*** Total Scrore ->'+TotalScore);
            
            if(TotalScore  != null){
               //this.obj.AssessmentScore__c = TotalScore  ;
               obj.put('AssessmentScore__c',TotalScore); 
               obj.put('TECH_Status__c', AssessmentStatus); //13-Jan-2015
               update obj;
               System.debug('obj inside else:'+obj);
            }
            if(lstResponsetoDelete != null && !lstResponsetoDelete.isEmpty())
                delete lstResponsetoDelete;
            PageReference pageRef;
            if(UserInfo.getProfileId() == Label.CLOCT13PRM18)
                retURL = retURL+obj.id;
            if(retURL!= null)
            {
                //if(retURL.startsWith('/'))    
                    pageRef =  new PageReference(retURL);
                //else
                  //  pageRef =  new PageReference('/'+retURL);
            }
            else
                pageRef =  new PageReference('/'+this.obj.id);
            system.debug('****doSave() ends*****');
            pageRef.setRedirect(true);
            return pageRef;
        
          }
        }
        catch(Exception e)
        {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'An error occurred while saving the record. Please contact your system administrator.','')); 
            System.debug(e.getMessage()); 
            Database.rollback(sp);
            return null;
        }
    }

    /**********************************************************************
     Inner Class to hold Questions, its corresponding answers and responses.
    **********************************************************************/
    public class QueAnsWrapper
    {
        public OpportunityAssessmentQuestion__c question{get;set;}
        public List<SelectOption> pickListOptions{get;set;}
        public OpportunityAssessmentResponse__c response{get;set;}
        public OpportunityAssessmentAnswer__c answer{get;set;}
        public String selAnswer {get;set;}
        list<String> lstSelectedAnswer = new list<String>();
        public String AnswerHelpText {get;set;}     //Feb15
        public String QuestionHelpText {get;set;} //Feb15

        public String[] getlstSelectedAnswer()
        {
            return lstSelectedAnswer ;
        }

        public void setlstSelectedAnswer(String[] selString)
        {
            lstSelectedAnswer =selString;
        }
    }
    
    //------------------------------------------------------------------
    // Added on 13-Jan-2015
    public boolean getIsORF(){
        if(ApexPages.currentPage().getParameters().containsKey('ORFID'))
        {
          if(ApexPages.currentPage().getParameters().containsKey('ORFID'))
            retURL = ApexPages.currentPage().getParameters().get('retURL'); 
          return true; 
           
        }   
        else 
           return false;
    }
    public PageReference doSaveDraft() 
    {
        AssessmentStatus = 'draft';
        system.debug('****dosaveDraft() calls*****');
        if(doSave() == null)
            return null;
        PageReference pageRef = new PageReference(retURL);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public PageReference doSubmit() 
    {
        AssessmentStatus = 'submitted';
        if(doSave() == null)
            return null;
        PageReference pageRef = new PageReference(retURL);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    //-----------------------------------------------------------------
}