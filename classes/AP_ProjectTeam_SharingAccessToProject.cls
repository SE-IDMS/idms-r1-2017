/*Class utilised by trigger ProjectTeamTriggers for Project 
as part of Monalisa project oct 2015 release*/
public class AP_ProjectTeam_SharingAccessToProject
{
     //Method to grant Access to the Project Team member to the OPP_Projet__c
     public static void insertPrjTeamMember(Map<Id,OPP_ProjectTeam__c> newPrjTeamMap)
    {
       System.Debug('******insertPrjTeamMember method in SharingAccessToPRJTeam Class Start****');
        //Initialise variables
        List<OPP_Project__Share> prjSharelst= new List<OPP_Project__Share>();
        Set<ID> prjID =new Set<ID>();
        Map<String,OPP_ProjectTeam__c> prjTeam = new Map<String,OPP_ProjectTeam__c>();
        
        for(OPP_ProjectTeam__c prjt:newPrjTeamMap.values())
            prjID.add(prjt.Project__c);
        List<OPP_Project__Share> oppPrjSharecurr = [SELECT parentid,userOrGroupid  FROM OPP_Project__Share WHERE parentId IN : prjID  AND  RowCause='GrantAccessToOPP_ProjectTeam__c'];  

        for(OPP_ProjectTeam__c  PRJTeamMember  : newPRJTeamMap.values())        
            prjTeam.put(String.valueOf(PRJTeamMember.Project__c)+String.valueOf(PRJTeamMember.user__c),PRJTeamMember);         
    
        for(OPP_Project__Share  ops : oppPrjSharecurr)
                prjTeam.keyset().remove(string.valueOf(ops.parentID)+string.valueOf(ops.userOrGroupid));         
                
        for(OPP_ProjectTeam__c  pt  : prjTeam.values())
        {
            OPP_Project__Share  opppjTeamShare = new OPP_Project__Share();
            opppjTeamShare.UserOrGroupId  =  newPrjTeamMap.get(pt.Id).User__c;
            opppjTeamShare.ParentID  =  newPrjTeamMap.get(pt.ID).Project__c;
            opppjTeamShare.AccessLevel=label.CL00203;
            opppjTeamShare.RowCause=Schema.OPP_Project__Share.RowCause.GrantAccessToProjectTeam__c;
            prjSharelst.add(opppjTeamShare);
        }
                    system.debug('prjSharelst>>>>>'+prjSharelst);
        if(prjSharelst.Size() > 0)
        {
            if(prjSharelst.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## Error updating : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] lsr = database.insert(prjSharelst,false);
                for( Database.SaveResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share>>>>>>'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];
                        system.debug('err.getMessage()>>>>>>'+err.getMessage());                        
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  && err.getMessage().contains('AccessLevel')) 
                            system.debug('Success *Trivial Access*');                   
                        else
                            system.debug('failed * Event share*'); 
                    } 
                }
            }
        }               
        System.Debug('*** insertPrjTeamMember method in SharingAccessToPRJTeam Class End***');  
    }
/*Method to delete grant Access to the Project Team member to the OPP_Project__c*/
    public static void deletePrjTeamMember(Map<Id,OPP_ProjectTeam__c> deletePrjTeamMap)
    {        
        System.Debug('******deletePrjTeamMember  method in AP_ProjectTeam_SharingAccessToProject Class Start****');
        Map<String,id> prjTeamMap = new  Map<String,id>();
        for(OPP_ProjectTeam__c  prjTeamMember :  deletePrjTeamMap.values())
            prjTeamMap.put(String.valueOf(prjTeamMember.Project__c)+String.valueOf(prjTeamMember.User__c),prjTeamMember.Project__c); 

        List<OPP_Project__Share> prjTeamMemberDeletelst = new List<OPP_Project__Share>();
        List<OPP_Project__Share> deletePrjMemList = [SELECT UserOrGroupId,ParentId,RowCause FROM OPP_Project__Share
                                                    WHERE RowCause = 'GrantAccessToProjectTeam__c' and ParentId IN: prjTeamMap.values()];

        for(OPP_Project__Share  oppPrjShareObj: deletePrjMemList)
        {
            if(prjTeamMap.keyset().contains(string.valueOf(oppPrjShareObj.parentID)+string.valueOf(oppPrjShareObj.userOrGroupid)))
            {
                prjTeamMemberDeletelst.add(oppPrjShareObj);
            }
        }           
        system.debug('****'+ prjTeamMemberDeletelst);
        if(prjTeamMemberDeletelst.Size() > 0)
        {
            if(prjTeamMemberDeletelst.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## Error While deleting : '+ Label.CL00264);
            }
            else
            {
                Database.DeleteResult[] lsr = database.delete(prjTeamMemberDeletelst,false);
                for( Database.DeleteResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];               
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                        err.getMessage().contains('AccessLevel')){ 
                        system.debug('Success *Trivial Access*');
                    } 
                    else
                    {
                        system.debug('failed * Event share*'); 
                    } 
                    } 
                }
            }
        }         
        System.Debug('***deleteExistingSMETeamMember method in AP_ProjectTeam_SharingAccessToProject Class End***');  
    }


}