@isTest
private class WS_SiteConnector_TEST{
     static testMethod void siteCreation(){
        list<Account> lstAccounts = new list<Account>();
        list<WS_SiteConnector.GCSSite> lstGCSSites = new list<WS_SiteConnector.GCSSite>();
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        //Create StateProvince
        StateProvince__c stateProvince = Utils_TestMethods.createStateProvince(country.id);
        insert stateProvince;
        
        Account accObj1 = Utils_TestMethods.createAccount();
        accObj1.Name = 'Test Account';
        accObj1.Country__c = country.id;
        accObj1.StateProvince__c = stateProvince.id;
        accObj1.LeadingBusiness__c = 'Buildings';
        accObj1.SEAccountId__c='112233';
        lstAccounts.add(accObj1); 
        
        Account accObj2 = Utils_TestMethods.createAccount();
        accObj1.Name = 'Test Account2';
        accObj1.Country__c = country.id;
        accObj1.StateProvince__c = stateProvince.id;
        accObj1.LeadingBusiness__c = 'Buildings';
        accObj1.SEAccountId__c='223344';
        lstAccounts.add(accObj2); 
       
         Insert lstAccounts;
         GCSSite__c  siteObj = new GCSSite__c();
         siteObj.Account__c=accObj2.id;
         siteObj.LegacyIBSiteID__c='TestIBSiteID1';
         siteObj.Name='Testsitess';
         siteObj.Active__c=False;
         siteObj.LegacySiteId__c='TestLegacySiteID';
         insert siteObj;
             
      		
        WS_SiteConnector.GCSSite gcssite = new WS_SiteConnector.GCSSite();
        gcssite.LegacySiteId='TestLegacySiteID';
        gcssite.SiteName='Test Site Name';
        gcssite.IsSiteActive='Yes';
        gcssite.IBsiteId='TestIBSiteID';
        gcssite.AccountGoldenId='223344';
        lstGCSSites.add(gcssite);
        
        WS_SiteConnector.GCSSite gcssiteObj = new WS_SiteConnector.GCSSite();
        gcssiteObj.LegacySiteId='TestLegacySiteID1';
        gcssiteObj.SiteName='Test Site Name1';
        gcssiteObj.IsSiteActive='Yes';
        gcssiteObj.IBsiteId='TestIBSiteID1';
        gcssiteObj.AccountGoldenId='112233';
        lstGCSSites.add(gcssiteObj);
        
        WS_SiteConnector.SiteResult siteResult = new WS_SiteConnector.SiteResult();
        siteResult = WS_SiteConnector.CreateAndUpdateSites(lstGCSSites);
     
     
     }
    


}