@isTest
public class VFC_PRMHeaderFooter_TEST{
     public static testMethod void userHeaderFooter() {
         
         
         
         Country__c testCountry = new Country__c();
         testCountry.Name   = 'USA';
         testCountry.CountryCode__c = 'US';
         testCountry.InternationalPhoneCode__c = '91';
         testCountry.Region__c = 'APAC';
         INSERT testCountry;
            
         Account acc = Utils_TestMethods.createAccount();
         acc.RecordTypeId = System.label.CLAPR15PRM025;
         INSERT acc;
         
         Contact cont = Utils_TestMethods.createContact(acc.Id,'TestContact');
         cont.PRMContact__c = TRUE;
         cont.PRMCustomerClassificationLevel1__c= 'LC';
         cont.PRMCustomerClassificationLevel2__c= 'Lc1';
         cont.PRMCountry__c = testCountry.Id;
         INSERT cont;
         
         User usr = Utils_TestMethods.createStandardUser('TestUser');
         usr.contactId = cont.Id;
         usr.ProfileId = System.label.CLAPR15PRM026;
         INSERT usr;
         
         FieloEE.MockUpFactory.setCustomProperties(false);
         
         FieloEE__Member__c member = new FieloEE__Member__c();
         member.FieloEE__LastName__c = 'Test';
         member.FieloEE__FirstName__c = 'Member';
         member.FieloEE__Street__c = 'Calle Falsa';
         member.F_PRM_PrimaryChannel__c = 'TST';
         member.F_Country__c= testCountry.Id;
         member.F_Account__c = acc.Id;
         member.FieloEE__User__c = usr.Id;
         member.FieloEE__Program__c = System.label.CLAPR15PRM149;
         insert member;
         
         cont.FieloEE__Member__c = member.Id;
         update cont;
         
         System.runAs(usr){
             
             system.debug('contact:' + usr.contactId + 'Member:' + cont.FieloEE__Member__c + 'Program:' + cont.FieloEE__Member__r.FieloEE__Program__c);
             VFC_PRMHeaderFooter headerFooter = new VFC_PRMHeaderFooter();
         }
     }
}