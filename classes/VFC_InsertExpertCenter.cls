/**
  Author: Sukumar Salla (GD Team)
  Date Of Creation: 
  Description : Controller for the Expert Center Update in the Technical Expert Assessment Object on Click of Save Button.

**/

Public Class VFC_InsertExpertCenter  extends VFC_ControllerBase {
 
   
   
   public TEX__c objTEX{get;set;}             // Return Material Authorization Object
   public TEX__c  objNewTEX{get;set;}  
          public Boolean IsRMA{get; set;}
          Public Boolean showme{get; set;}
          
          // *********************************************
          Public Boolean showOthersBlock{get; set;}
          Public Boolean showResultsBlock{get; set;}
          Public Boolean isCheck{get; set;}
          Public Boolean IsError{get; set;}



          Public Boolean IsTEX {get; set;}
          //*********************************************
          //Oct 15 Rel
          Public Boolean IsErrorPL{get; set;}
          public List<ProductLineQualityContactMapping__c> listobjPL{get;set;}
          public ProductLineQualityContactMapping__c objPL{get;set;}
          //Oct 15 Rel Ends
          public list<selectOption> lstselectOption{get;set;}
          public list<selectOption> lstselectOption2{get;set;}
          Public string strCustomerResolutions{get;set;}
          Public String NewObjId;
          public string RIGMRCode;
          public string slist;
          Public string strCommercialReference;
          Public string strProductFamily;
          
          Public string strTEXAccCountry;
          list<ResolutionOptionAndCapabilities__c> lstROC;
          list<ResolutionOptionAndCapabilities__c> lstResROC;
          list<RMAShippingToAdmin__c> lstLocalRtnCenters;
         
		  list<RMAShippingToAdmin__c> lstExpCenters; 
          list<RMAShippingToAdmin__c> lstExpertCenters;
          
          PageReference pr1 = NULL;
          PageReference pr; 
   

   public ApexPages.StandardController controller;
   
   // ========== Initilization for Display Results ===============
   public List<SelectOption> columns{get;set;}
   public Boolean DisplayResults{get;set;}
   public List<DataTemplate__c> fetchedRecords{get;set;} 
   public Long numberOfRecord{get;set;}
   private Boolean showButton; 
   Public string SelectTypevalue{get;set;}  
   //current page parameters
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
   // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL00016);
   // indicates whether there are more records than the searchMaxSize.
      public Boolean hasNext {
        get {
            return ((numberOfRecord > searchMaxSize || numberOfRecord == 0)  && showButton );
            }
        set;
    }

   
  public VFC_InsertExpertCenter(ApexPages.StandardController stdController)
    {

        // ========= for search display ================
        columns = new List<SelectOption>();        
        DisplayResults = true;
        showButton = true; 
        SelectTypevalue='Single';
        numberOfRecord=0;
        IsError= False;
        IsErrorPL= False;
        IsTEX = False;
        showme=False;
        
        listobjPL= new List<ProductLineQualityContactMapping__c>();// As part of October 2015 Release BR-5320
        objPL=new ProductLineQualityContactMapping__c(); // As part of October 2015 Release BR-5320
        
        columns.add(new SelectOption('Field2__c','Expert Center'));// First Column
        columns.add(new SelectOption('Field1__c',sObjectType.RMAShippingToAdmin__c.fields.Capability__c.label));
        columns.add(new SelectOption('Field3__c',sObjectType.RMAShippingToAdmin__c.fields.RCStreet__c.label));
        columns.add(new SelectOption('Field4__c',sObjectType.RMAShippingToAdmin__c.fields.RCCity__c.label));
        columns.add(new SelectOption('Field5__c',sObjectType.RMAShippingToAdmin__c.fields.RCZipCode__c.label));
        columns.add(new SelectOption('Field6__c',sObjectType.RMAShippingToAdmin__c.fields.RCCountry__c.label));
        columns.add(new SelectOption('Field10__c',sObjectType.RMAShippingToAdmin__c.fields.Comments__c.label));
        fetchedRecords = new List<DataTemplate__c>();
        DataTemplate__c dt = new DataTemplate__C(); 
        dt.field1__c = ' ';  
        dt.field7__c=''; 
        fetchedRecords.add(dt);
        fetchedRecords.clear();
        
        //==============================================
       showOthersBlock = TRUE;
       this.objTEX = (TEX__c)stdController.getRecord();
       lstselectOption=new list<selectOption>();
       lstselectOption2=new list<selectOption>();
       isCheck =  false;
       objNewTEX = new TEX__c ();
       lstROC = new List <ResolutionOptionAndCapabilities__c>();
       lstExpertCenters = new List <RMAShippingToAdmin__c>();
       lstExpCenters = new List <RMAShippingToAdmin__c>();
       lstLocalRtnCenters = new List <RMAShippingToAdmin__c>();
      
        NewObjId = System.CurrentPageReference().getparameters().get('newid');
          // If NewId belongs to RMA
           //system.debug('## PREFIX ##' + RMASchemaReuslt.getKeyPrefix() );
                      
           objTEX = [Select id,TECH_ExpertCenterContactEmail__c,Case__r.TECH_GMRCode__c ,ExpertCenter__c,AssessmentType__c,
                     ForceManualSelection__c,ExpertCenterStreet__c,ExpertCenterCity__c,ExpertCenterZipCode__c,
                     ExpertCenterCountry__c,ExpertCenterAdditionalInformation__c,ExpertCenterCounty__c,ExpertCenterPOBox__c,
                     ExpertCenterPOBoxZipCode__c,ExpertCenterStateProvince__c,ExpertCenterPhone__c,ExpertCenterFax__c,
                     ExpertCenterContactName__c,ExpertCenterLocalStreet__c,ExpertCenterLocalAdditionalInfo__c,
                     ExpertCenterLocalCity__c,ExpertCenterLocalCounty__c,ExpertCenterContactEmail__c,Case__r.quantity__c,
                     Case__r.DateCode__c,Case__r.SerialNumber__c,Case__r.CommercialReference__c,  Case__r.Account.Country__c, 
                     Case__r.Family__c,Case__r.ProductFamily__c, Case__r.ProductLine__c from TEX__c where id=:NewObjId]; 
           system.debug('## 1. objTEX ##'+ objTEX );

            listobjPL= [SELECT ProductLine__c,QualityContact__r.name,QualityContact__r.IsActive, Id, BusinessUnit__c, AccountableOrganization__r.Name, AccountableOrganization__c from ProductLineQualityContactMapping__c where ProductLine__c=:objTEX.case__r.ProductLine__c]; // As part of October 2015 Release BR-5320
            system.debug('## listobjPL'+listobjPL);
            // Oct 15 Release -- Divya M 
             If ( listobjPL.size()>0 && listobjPL[0]!=null )
                {
                    objPL=listobjPL[0];
                    system.debug('**objPL** + objPL');
                }else{IsErrorPL= True;}
            //Ends
    }

     public PageReference doSearchExpertCenters()
     {
      // System.debug('## doSearchExpertCenters - ObjTEX.Other__c##'+objTEX.Other__c);         
       if (objTEX.ForceManualSelection__c==True)
       {
         showOthersBlock = False;
         //objTEX.Other__c=False;
       }
       getExpertCenter();
       return pr1;
     }

     Public Void getExpertCenter ()
    {
        if ( objTEX!=null)
        {
        // Hardcode the Capability - TEX
        
        lstROC = [Select id, AssessmentType__c , ProductCondition__c, CustomerResolution__c, ProductDestinationCapability__c from ResolutionOptionAndCapabilities__c where AssessmentType__c =: objTEX.AssessmentType__c];
        set <String> stCapabilites = new set <String> ();
        If (lstROC!=null && lstROC.size()>0)
           {
               for (ResolutionOptionAndCapabilities__c varROC :lstROC )
               {
                   stCapabilites.add(varROC.ProductDestinationCapability__c);
               }
             
                string slist='';
                for (String s: stCapabilites) 
                {
                    slist += '\'' + s + '\',';
                }
                slist = slist.substring (0,slist.length() -1);
                system.debug ( '## 4. stCapabilites  ## ' + slist );
        
         lstExpertCenters.clear();
         lstExpertCenters=getAllEcs( objTEX, slist);
         system.debug ('## lstExpertCenters ###'+ lstExpertCenters);
       //======================= Uniqeness Check on Display result =============================

         Map<String, RMAShippingToAdmin__c> keyObejctMap= new Map<String, RMAShippingToAdmin__c>();
                set <Id> LCids = new set <Id> ();
                Set <string> MasterKey = new set <string> ();
                for(RMAShippingToAdmin__c LCs: lstExpertCenters) 
                {
                    String key1;
                   
                    
                    key1 =LCs.ReturnCenter__c;
                    system.debug('key1'+ key1);
                    String strCAP= LCs.Capability__c;
                    list <STRING> lsCapabilites = new list <string>();
                    lsCapabilites = strCAP.split(';');
                    for (String s: lsCapabilites )
                            {
                                string tempKey = key1+s;
                                MasterKey.add(tempKey);
                                LCids.add(LCs.Id);
                                keyObejctMap.put(tempKey , LCs);
                            }
                     
                    // system.debug('key'+key);
                    // keyObejctMap.put(key , LCs);
                }
                for( String mk: MasterKey )
                {
                        if(keyObejctMap.containsKey(mk))
                        {
                            RMAShippingToAdmin__c exObj = keyObejctMap.get(mk);
                            if ( LCids.contains(exObj.id))
                            {
                                 lstExpCenters.add(keyObejctMap.get(mk));
                                 LCids.remove(exObj.id);
                            
                            }
                        }    
                 }
                        



       // call method to display Return Centers in the grid.
         system.debug ('## Calling displayExpertCenters Method ###');
         displayExpertCenters(lstExpCenters);
        }   
    }

  }


    public list<RMAShippingToAdmin__c> getAllECs( sObject objT, string slist)
    {        
             
                 TEX__c objTEX = (TEX__c) objT;
        	     System.debug('## objTEX : ' + objTEX);
                 strCommercialReference = objTEX.Case__r.CommercialReference__c;
                 System.debug('objRMA.case__r.Family__c'+objTEX.case__r.Family__c);

                 if (objTEX.case__r.Family__c!=null || objTEX.case__r.Family__c!='')
                 {
                    string newPF = objTEX.case__r.TECH_GMRCode__c ;
                    System.debug('## newPF' + newPF);
                    if (newPF!=null) 
                        {
                            strProductFamily = newPF.substring(0,Integer.valueOf(Label.CLOCT14I2P03)); //'8'
                        }
                    else
                        {
                            strProductFamily = null; 
                        } 
                 }
                 else
                        {
                            strProductFamily = null;
                        }
                 
                
                strTEXAccCountry = objTEX.case__r.Account.Country__c;
                 if (strTEXAccCountry==null)
                {
                   strTEXAccCountry='';
                }
                System.debug('strProductFamily'+strProductFamily);
              

             String strQuery = 'select id, Name, Comments__c,RCFax__c, RCContactName__c,ContactEmail__c, RCStreetLocalLang__c,RCLocalAdditionalAddress__c,RCLocalCity__c,RCLocalCounty__c,ReturnCenter__c,RCPhone__c,RCAdditionalAddress__c, TECH_ProductFamilyGMRCode__c, RCPOBox__c, RCPOBoxZip__c, RCCounty__c, RCStateProvince__c,ReturnCenter__r.RCStateProvince__c,Country__c,RCStreet__c,RCCity__c,RCZipCode__c, RCCountry__c,ReturnCenter__r.RCCountry__c,ReturnCenter__r.RCCountry__r.Name,ReturnCenter__r.Name,Capability__c,CommercialReference__c, ProductFamily__c,RCShippingAddress__c,Ruleactive__c from RMAShippingToAdmin__c where Capability__c Includes (' + slist + ') AND Ruleactive__c=True';
              if ( (strCommercialReference !='') && (strProductFamily !='' ))
               {
                System.debug('strCommercialReference '+strCommercialReference );
                strQuery = strQuery + ' AND ((((CommercialReference__c= \''+strCommercialReference+'\') OR ( ((CommercialReference__c=\'\') AND TECH_ProductFamilyGMRCode__c= \''+ strProductFamily +'\')))';              
               }
               else if ((strCommercialReference =='' ) && (strProductFamily =='' ))
               {
                  strQuery = strQuery + ' AND ((((CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\'))';
               }
             
               else if ((strCommercialReference =='' ) && (strProductFamily !='' ))
               {
                  strQuery = strQuery + ' AND (((CommercialReference__c=\'\') AND ( TECH_ProductFamilyGMRCode__c= \''+ strProductFamily +'\')';  
               }
                    strQuery = strQuery +' AND  (Country__c=\'\' OR   Country__c=\''+strTEXAccCountry +'\'))';
                strQuery =  strQuery + 'OR ( (Country__c=\''+strTEXAccCountry+'\') AND (CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\')))';
               system.debug('Local Rule'+ strQuery );
        	   system.debug('## database.query(strQuery)'+ database.query(strQuery) );
               return database.query(strQuery);
        }
        
  public PageReference displayExpertCenters(List <RMAShippingToAdmin__c> lstExpertCenters)
    { 
           fetchedRecords.clear();
           
           string newCR;
           string TechCR;
                
                
           system.debug('lstExpertCenters'+lstExpertCenters);
           If ( (lstExpertCenters!=null && lstExpertCenters.size()>1) )
               {
                   system.debug ('## SIZE ##'+ lstExpertCenters.size());
                   fetchedRecords.clear();
                   system.debug('==lstExpertCenters=='+lstExpertCenters);
              
                   for (RMAShippingToAdmin__c RC : lstExpertCenters )
                   {
                      set <String> stResolutions = new set <String> ();
                      numberOfRecord++;
                      if (RC!=null )
                        { 
                            system.debug ( '## RC  ## ' + RC  );
                            DataTemplate__c dt = new DataTemplate__c();                  
                            dt.field2__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+RC.ReturnCenter__c+'" target="_blank">'+RC.ReturnCenter__r.Name+'</a>';
                            dt.field1__c = RC.capability__c;
                            dt.field3__c = RC.RCStreet__c;
                            dt.field4__c = RC.RCCity__c;
                            dt.field5__c = RC.RCZipCode__c;
                            dt.field6__c = RC.RCCountry__c;
                            dt.field7__c = RC.ReturnCenter__r.RCCountry__c;
                            dt.field8__c = RC.ReturnCenter__c;
                            dt.field9__c = RC.ContactEmail__c;
                            dt.field10__c = RC.Comments__c;
                            fetchedRecords.add(dt);   
                        }
                   }
                }
    
            else if ( lstExpertCenters!=null && lstExpertCenters.size() == 1)
            {
                // It should directly create RI and update the shipping address
                objTEX.ExpertCenter__c=lstExpertCenters[0].ReturnCenter__c;
                objTEX.TECH_ExpertCenterContactEmail__c=lstExpertCenters[0].ContactEmail__c;  // Hot Fix - EUS039060 - Sukumar Salla - 05JUNE2014
                Upsert objTEX;

                pr1 = new PageReference('/'+ objTEX.id);
                system.debug('## pr1 ##'+ pr1);  
                pr1.setRedirect(true);         
            } 
            
            
        return pr1;
    }
    
    public VCC06_DisplaySearchResults resultsController 
        {
        set;
        get
            {
                if(getcomponentControllerMap()!=null)
                {
                    VCC06_DisplaySearchResults displaySearchResults;
                    displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                    system.debug('--------displaySearchResults -------'+displaySearchResults );                
                    if(displaySearchResults!= null)
                    return displaySearchResults;
                }  
                return new VCC06_DisplaySearchResults();
            }
        }

         public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
     {   
         System.debug('**Started***');
         VFC_InsertExpertCenter thisController = (VFC_InsertExpertCenter)controllerBase;
         DataTemplate__c dt= (DataTemplate__c)obj;
         System.debug('****************** Check Point*********');
        
        
        objTEX.ExpertCenter__c=dt.field8__c;
        objTEX.TECH_ExpertCenterContactEmail__c=dt.field9__c;      
        Upsert objTEX;
         

        PageReference pr = new PageReference('/'+ objTEX.id);
        pr.setRedirect(true);       
        return pr;
                
     }    
     
  Public Pagereference doSave()
     {
     pageReference pr;

         if(objTEX.ExpertCenter__c== null )
         {
            IsError=True;
            showme=True;
            objTEX.ForceManualSelection__c= true;     
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLOCT13RR55));
            pr = null;
          }
          else
          {
             objTEX.ForceManualSelection__c=True;
             system.debug('##ExpertCenter##'+ objTEX.ExpertCenter__c);
             BusinessRiskEscalationEntity__c EC = [Select id, ContactEmail__c from BusinessRiskEscalationEntity__c where id=: objTEX.ExpertCenter__c];
             system.debug('##ExpertCentercontactEmail##'+ EC.ContactEmail__c);
             objTEX.TECH_ExpertCenterContactEmail__c= EC.ContactEmail__c;
             system.debug('##TECHExpertCentercontactEmail##'+ objTEX.TECH_ExpertCenterContactEmail__c);
             Upsert objTEX;  

             pr = new PageReference('/'+ objTEX.Id);
             pr.setRedirect(true);
          } 
         system.debug ('## pagref ##'+ pr);
         return pr;
     }
     
      Public Pagereference doCancel()
      {
         string CaseId = objTEX.Case__c;
         delete objTEX;
         pr = new PageReference('/'+ CaseId );
        
         pr.setRedirect(true);
         return pr;

      }  
}