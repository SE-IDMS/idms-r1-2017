public class AP_PerformInsertForPlaftormAccounts implements Queueable{
   List<SFE_PlatformingScoring__c> psList=new List<SFE_PlatformingScoring__c>();
   public AP_PerformInsertForPlaftormAccounts(List<SFE_PlatformingScoring__c> pslst)
   {
        psList=pslst;
   }
   
    public void execute(QueueableContext context) {       
        List<Database.SaveResult> lsr=Database.insert(pslist,false);
        for(Database.SaveResult sr:lsr)
        {            
           if(!sr.isSuccess())
                  System.debug('>>>>Error>> '+sr+'>>>>>>'+sr.getErrors()[0].getMessage()); 
        }
    }
}