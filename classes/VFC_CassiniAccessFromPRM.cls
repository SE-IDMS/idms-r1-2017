public class VFC_CassiniAccessFromPRM {
    Public PageReference CassiniAccessFromPRM(){
        PageReference pf;
        Map <Id,PermissionSetAssignment> mapPSA = new Map <Id,PermissionSetAssignment>();
        System.debug(Userinfo.getUserID());
        List<PermissionSetAssignment>  lstPSA=[SELECT PermissionSetId  FROM PermissionSetAssignment WHERE AssigneeId=:Userinfo.getUserID()];
        for(PermissionSetAssignment psa :lstPSA ){
            mapPSA.put(PSA.PermissionSetId,PSA);
        }
        System.debug(mapPSA);
        if(mapPSA.containsKey(System.label.CLJUN16CSN118)){
            pf = new PageReference(Label.CLJUN16CSN155);
            pf.setRedirect(true);
            return pf;
        }
        else{
            pf = new PageReference(Label.CLJUN16CSN175);
            pf.setRedirect(true);
            return pf;
        }
    }   
}