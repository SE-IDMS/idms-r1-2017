public class VFC_ManageContactExternalInfo extends VFC_ExternalInfoDashboardController {
    
    public List<SelectOption> IMSApplications{get; set;}
    public String IMSApplicationID {get;set;}
    public String ReturnToTab { get; set; }
    public String Type;
    
    public VFC_ManageContactExternalInfo(){
        Type = ApexPages.currentPage().getParameters().get('type');
        List<AP_PRMUtils.ApplicationType> applicationLst = new List<AP_PRMUtils.ApplicationType>();
        IMSApplications = New List<SelectOption>();
        IMSApplications.add(new SelectOption('','--None--'));
        
        if(String.isBlank(DashboardInfo.ContactInfo.PRMFirstName__c))
            DashboardInfo.ContactInfo.PRMFirstName__c = DashboardInfo.ContactInfo.FirstName;
        if(String.isBlank(DashboardInfo.ContactInfo.PRMLastName__c))
            DashboardInfo.ContactInfo.PRMLastName__c = DashboardInfo.ContactInfo.LastName;
        if(String.isBlank(DashboardInfo.ContactInfo.PRMEmail__c))
            DashboardInfo.ContactInfo.PRMEmail__c = DashboardInfo.ContactInfo.Email;
        if(Type == 'reset'){
            if(String.isNotBlank(DashboardInfo.ContactInfo.PRMUIMSID__c))
                applicationLst = AP_PRMUtils.getApplications_Admin(DashboardInfo.ContactInfo.PRMUIMSID__c);
            
            System.debug('*** '+applicationLst.size());
            if(applicationLst != Null && applicationLst.size() > 0)
                for(AP_PRMUtils.ApplicationType applicationType : applicationLst)
                    IMSApplications.add(new SelectOption(applicationType.ApplicationId,applicationType.DisplayName));
        }
    }
    
    private PageReference GetFullUrl(String statusParam) {
        return this.GetFullUrl(statusParam, '');
    }

    private PageReference GetFullUrl(String statusParam, string pgName) {
        PageReference tPR = new PageReference('/apex/' + (String.isBlank(pgName) ? 'VFP_ManageContactExternalInfo' : pgName) + '?tab=' + this.ReturnToTab + '&id=' + RecordId + (String.isNotBlank(statusParam) ? '&status=' + statusParam : ''));
        tPR.setRedirect(true);
        return tPR;
    }

    public PageReference doSave(){
        Boolean recChanged = this.isDirty();
        PageReference pr = null;
        ValidationMessages.clear();
        if (recChanged) {
            try {
                if(this.DashboardInfo.ContactInfo.PRMContactRegistrationStatus__c != 'Registering'){
                    Boolean imsUPDStatus = this.UpdateContactInIMS();
                    if (imsUPDStatus != null && imsUPDStatus == true) {
                        UPDATE this.DashboardInfo.ContactInfo;
                        pr = this.GetFullUrl('success');
                    }
                 }
                 else if(this.DashboardInfo.ContactInfo.PRMContactRegistrationStatus__c == 'Registering'){
                     UPDATE this.DashboardInfo.ContactInfo;
                     pr = this.GetFullUrl('success');
                 }
                else {
                    ValidationMessages.add('Error while updating contact in IMS. ');
                    SaveStatus = 'valErrors';
                }
            }
            catch (Exception ex){
                ValidationMessages.add( 'Error while saving: ' + ex.getMessage() + ex.getStackTraceString());
                SaveStatus = 'valErrors';
            }
        }
        else{
            pr = this.GetFullUrl('nochange');
        }
        return pr;
    }
    
    // This is a helper method that is called when the company information updates have to be pushed to IMS
    private Boolean UpdateContactInIMS(){
        // Do the UIMS Update once the class is created.
        Contact thisContact = this.DashboardInfo.ContactInfo;
        AP_UserRegModel urModel = New AP_UserRegModel();
        urModel.federationId = thisContact.PRMUIMSID__c;
        urModel.email = thisContact.PRMEmail__c;
        urModel.firstName = thisContact.PRMFirstName__c;
        urModel.lastName = thisContact.PRMLastName__c;
        urModel.uimsCompanyId = thisContact.PRMUIMSID__c;
        urModel.jobTitle  = thisContact.PRMJobTitle__c;
        urModel.jobFunction = thisContact.PRMJobFunc__c;
        urModel.businessType = thisContact.PRMCustomerClassificationLevel1__c;
        urModel.areaOfFocus = thisContact.PRMCustomerClassificationLevel2__c;
        urModel.isPrimaryContact = thisContact.PRMPrimaryContact__c;
        
        urModel.contactID   = thisContact.Id;
        if(String.isNotBlank(thisContact.PRMWorkPhone__c) && thisContact.PRMWorkPhone__c != null) {
            urModel.phoneNumber = thisContact.PRMWorkPhone__c;
            urModel.phoneType = 'Work';
        }
        else if(String.isNotBlank(thisContact.PRMMobilePhone__c) && thisContact.PRMMobilePhone__c != null) {
            urModel.phoneNumber = thisContact.PRMMobilePhone__c;
            urModel.phoneType = 'Mobile';
        }
        urModel.companyCountry    = thisContact.PRMCountry__c;

        Boolean contactUpdated = AP_PRMUtils.UpdateContactInUIMS_Admin(urModel);
        return contactUpdated;
    }
    
    // This method is used to determine if any field values in the record have changed or not
    protected override Boolean IsDirty () {
        Boolean hasChanged = false;
        String qry = 'SELECT ' + ContactFields + ' FROM Contact WHERE Id = :RecordId LIMIT 1';
        System.debug('*** Query: ' + qry);

        Contact originalContact = Database.Query(qry);
        Contact thisContact = this.DashboardInfo.ContactInfo;
        List<String> lConFields = ContactFields.Split(',');

        for (String s : lConFields) {
            if(s !='FieloEE__Member__r.Name'){
                System.debug('*** Field Original New Value: ' + s + ',' + originalContact.get(s) + ',' + thisContact.get(s));
                if (originalContact.get(s) != thisContact.get(s)) {
                    System.debug('*** Field changed: ' + s);
                    hasChanged = True;
                    break;
                }
            }
        }
        System.debug('*** isDirty: ' + hasChanged);
        return hasChanged;
    }
    
    public PageReference ResetPassword(){
        try {
            String federatedId = DashboardInfo.ContactInfo.PRMUIMSID__c;
            List<User> lUsr = new List<User>([SELECT Id, federationIdentifier, email, Contact.PRMContact__c FROM USER WHERE IsActive = true AND federationIdentifier = :federatedId]);
            if (!lUsr.isEmpty()) {
                User usr = lUsr[0];
                federatedId = (usr != null ? usr.FederationIdentifier : '');
            }
            if(String.isBlank(federatedId)) {
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort uimsPort = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                uimsPort.timeout_x = 120000; 
                uimsPort.clientCertName_x = System.Label.CLAPR15PRM018;
                uimsPort.endpoint_x = System.Label.CLAPR15PRM017;
                uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType srchUsrResult = uimsPort.searchUser(System.Label.CLAPR15PRM016, DashboardInfo.ContactInfo.PRMEmail__c);
                System.debug('Search Result: ' + srchUsrResult);
                if (srchUsrResult != null ) federatedId = (string.isNotBlank(srchUsrResult.federatedId) ? srchUsrResult.federatedId : '');
            }
            if(string.isNotBlank(federatedId)){
                uimsv2ServiceImsSchneiderComAUM.accessElement application = new uimsv2ServiceImsSchneiderComAUM.accessElement();
                application.type_x = System.label.CLAPR15PRM020;//'APPLICATION';
                application.id = IMSApplicationID;    //System.label.CLAPR15PRM022;'prm';
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort abc = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                abc.endpoint_x = System.label.CLAPR15PRM017; //'https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager' (https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager%27);
                abc.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);//120000; 
                abc.clientCertName_x = System.label.CLAPR15PRM018;//'Certificate_IMS';
                String resetStatus = abc.resetPassword(System.label.CLAPR15PRM019, federatedId, application);
                System.debug('*** Password reset status:' + resetStatus);
                SaveStatus = 'success';
            }
            else {
                ValidationMessages.add(System.Label.CLJUL15PRM019); 
                SaveStatus = 'valErrors';
            }
        } 
        catch (Exception ex) {
            System.debug('Error:' + ex.getMessage() + ex.getStackTraceString());
            ValidationMessages.add('Error while performing password reset.' + ex.getMessage() + ex.getStackTraceString());
            SaveStatus = 'valErrors';
        }
        return null;
    }
    
    public PageReference DoCancelContact() {
        return this.GetFullUrl('');
    } 
}