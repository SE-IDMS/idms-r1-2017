/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: Test class of page controller FieloPRM_VFC_BatchExecutionCtrlTest
********************************************************************/

@isTest
public without sharing class FieloPRM_VFC_BatchExecutionCtrlTest {
    
    @testSetup
    static void setupTest() {
        FieloPRM_BatchManagementView__c bView = new FieloPRM_BatchManagementView__c();
        bView.name = 'view 1';
        insert bView;
        
        FieloPRM_BatchManagementConfiguration__c bConfiguration = new FieloPRM_BatchManagementConfiguration__c();
        bConfiguration.Name = 'conf 1';
        bConfiguration.F_PRM_BatchManagementView__c = bView.id;
        bConfiguration.F_PRM_BatchName__c = 'Batchtest1';
        insert bConfiguration;
    }
    
    @isTest
    static void unitTest1(){
        FieloPRM_BatchManagementView__c bView = [SELECT Id FROM FieloPRM_BatchManagementView__c LIMIT 1];
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(bView);
        FieloPRM_VFC_BatchExecutionController batch = new FieloPRM_VFC_BatchExecutionController(sdtCon);
        batch.executeb();
        batch.batchToexecute = '';
        batch.executeb();
        batch.batchSize = -1;
        batch.executeb();
    }
}