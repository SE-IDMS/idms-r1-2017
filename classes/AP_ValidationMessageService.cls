public class AP_ValidationMessageService {

    private static Map<String, String> errorMessageMap;
    public static boolean forTestCoverage = false;

    public static Map<String,String> ErrorMessages {
        get {
            if (errorMessageMap == null) {
                errorMessageMap = GetMessageMap();
            }
            return errorMessageMap;
        }
        private set;
    }


    // TODO: In future determine to load the messages when the message file is big
    private Static Map<String,String> GetMessageMap() {
        Map<String, String> messageMap = new Map<String, String>();

        try {
            String contentAsText;

            if (forTestCoverage == true) {
                StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'PRMMessages' LIMIT 1];
                contentAsText = sr.Body.toString();
            }
            else {
                PageReference errMsgFile = new PageReference('/resource/PRMMessages');
                Blob contentAsBlob = errMsgFile.getContent();
                contentAsText = contentAsBlob.toString();
            }

            System.debug('*** Raw Content:' + contentAsText);

            Map<String, Object> parameters = (Map<String, Object>) JSON.deserializeUntyped(contentAsText);

            System.debug('*** Deserialized:' + parameters);

            for (String a:parameters.Keyset()) {
                messageMap.put(a, (String) parameters.get(a));
            }
        }
        catch (Exception ex) {
            System.debug('*** Error loading resource:' + ex.getMessage() + ex.getStackTracestring());
        }
        return messageMap;
    } 
}