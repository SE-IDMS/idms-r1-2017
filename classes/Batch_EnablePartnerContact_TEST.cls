//Description: Test Class for Batch_EnablePartnerContact

@isTest
private class Batch_EnablePartnerContact_TEST {
    static TestMethod void testEnableContact(){
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        Account acc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id);
        insert acc;
        
        Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',SEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123',
                Email = 'test@test.com');
        insert c1;
        
        Contact c2 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = false, PRMUIMSSEContactId__c = '123456',
                Email = 'test@test.com');
        insert c2;
        
        Batch_EnablePartnerContact batchCont = new Batch_EnablePartnerContact();
        database.executeBatch(batchCont);
    }
}