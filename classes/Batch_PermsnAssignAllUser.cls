global class Batch_PermsnAssignAllUser implements Database.Batchable<SOBJECT>
{

    // Controller Name  : Batch_UpdatePermissionSetToUser
    // Purpose          : Controller class for Updating PermissionSet to user
    // Created by       : Global Delivery Team
    // Date created     : th 2-April 2012
    // Modified by      :
    // Date Modified    :
    // Remarks          : For October 2012 -  Release

global final String Query;
global Batch_PermsnAssignAllUser (String q)
{
    Query=q;
}

global Database.QueryLocator start(Database.BatchableContext BC)
{
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC , List<SOBJECT> scope)
{
    Map<ID,List<ID>> nwmp=new Map<ID,List<ID>>(); // This map is used to store User and their Associated PermissionSet Id.
    Set<ID> stUser=new Set<ID>();                 // Set For holding User Id
    List<Id> lst;                                 // List For Holding Permission SetId
    Set<ID> stpermissionSetId=new Set<ID>();       // Set For holding Permission Set Id
    List<User> lstfinalUser=new List<User>();     // List Holding the all users to be Updated
    List<User> lstUSR=new List<User>();          // List for querying User

    for(Sobject st : scope)
    {    User s=(User)st;
    
        stUser.add(s.id);
        
    }
    
    set<id> sttempUserId=new set<id>();
    
    List<PermissionSetAssignment> lstPermissionset= [SELECT AssigneeId,Id,PermissionSetId,PermissionSet.Label,SystemModstamp FROM PermissionSetAssignment where AssigneeId IN : stUser AND PermissionSet.IsOwnedByProfile =false] ;
    
    for(PermissionSetAssignment varPerAssignment: lstPermissionset)
    { 
      sttempUserId.add(varPerAssignment.AssigneeId);
      
    }
    list<string> lstPermissionSetlabel;
    for(user varUserId: (list<user>)scope)
    {
    lstPermissionSetlabel=new list<string>();
   
  string  strprmst='';
  
    
    for(PermissionSetAssignment varper:lstPermissionset)
    {
    if(varUserId.id== varper.AssigneeId)
    {  
       lstPermissionSetlabel.add(varper.PermissionSet.Label);
      //strprmst=strprmst+varper.PermissionSet.Label+',';
    }
    }
    lstPermissionSetlabel.sort();
    if(lstPermissionSetlabel.size()>0)
    {
    for(string str : lstPermissionSetlabel)
    {
    
    strprmst=strprmst+str+', ';
    
    
    
    }
    }
    if(strprmst.contains(','))
    {
      strprmst = strprmst.substring(0, strprmst.trim().length() - 1);
    }
    varUserId.Permission_sets_assigned__c=strprmst;
      lstfinalUser.add(varUserId);
    }
     
     update lstfinalUser;
    
  
}
    global void finish(Database.BatchableContext BC)
    {
                    //Send an email to the User after your batch completes
                    
     }
 }