/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 30/01/2012
    Description: Test class for AP38_EmailToCaseAutoAssignments
                 IF(ENABLE_ASSERT)System.assert can be disabled by editing the related custom setting record
*/   

@istest
private class AP42_TeamAssignmentMethods_TEST
{
     private static testMethod void uncheckDefault() {  
       
        Account anAccount = Utils_TestMethods.createAccount();
        insert anAccount;
        
        Contact aContact = Utils_TestMethods.createContact(anAccount.Id,'aTestContact');
        insert aContact;
        
        Country__c CCCountry = new Country__c(Name='Country',CountryCode__c='TC1');
        insert CCCountry;
        
        List<User> testUserList = new List<User>();
        
        User AgentLevel1_A_TEST =   Utils_TestMethods.createStandardUser('AgentA');
        Id ProfileId = UserInfo.getProfileId();
        //AgentLevel1_A_TEST.profileID = '00eA0000000aspm'; // Set the profile to Primary Agent
        AgentLevel1_A_TEST.profileID = ProfileId; // Set the profile to Current User
        
        testUserList.add(AgentLevel1_A_TEST);
        
        User AgentLevel1_B_TEST =   Utils_TestMethods.createStandardUser('AgentB');
        //AgentLevel1_B_TEST.profileID = '00eA0000000aspm'; // Set the profile to Primary Agent
        AgentLevel1_B_TEST.profileID = ProfileId ; // Set the profile to Current User
        testUserList.add(AgentLevel1_B_TEST);

        User AgentLevel1_C_TEST =   Utils_TestMethods.createStandardUser('AgentC');
        //AgentLevel1_C_TEST.profileID = '00eA0000000aspm'; // Set the profile to Primary Agent
        AgentLevel1_C_TEST.profileID = ProfileId; // Set the profile to Current User
        testUserList.add(AgentLevel1_C_TEST);
        
        insert testUserList ; 
        
        List<CustomerCareTeam__c> ccTeamList = new List<CustomerCareTeam__c>();
        List<TeamAgentMapping__c> teamMembers = new List<TeamAgentMapping__c>();
        
        CustomerCareTeam__c AgentAPrimaryTeam = new CustomerCareTeam__c(CCCountry__c=CCCountry.ID, Name='Agent A Primary Team',LevelOfSupport__c='Primary');
        ccTeamList.add(AgentAPrimaryTeam);

        CustomerCareTeam__c AgentBPrimaryTeam = new CustomerCareTeam__c(CCCountry__c=CCCountry.ID, Name='Agent B Primary Team',LevelOfSupport__c='Primary');
        ccTeamList.add(AgentBPrimaryTeam);
        
        CustomerCareTeam__c AgentCPrimaryTeam = new CustomerCareTeam__c(CCCountry__c=CCCountry.ID, Name='Agent C Primary Team',LevelOfSupport__c='Primary');
        ccTeamList.add(AgentCPrimaryTeam);
        
        insert ccTeamList;
        
        TeamAgentMapping__c AgentAPrimaryMembership = new TeamAgentMapping__c(DefaultTeam__c=true, CCTeam__c=AgentAPrimaryTeam.ID, CCAgent__c = AgentLevel1_A_TEST.ID);
        teamMembers.add(AgentAPrimaryMembership );

        TeamAgentMapping__c AgentBMembership = new TeamAgentMapping__c(DefaultTeam__c=false, CCTeam__c=AgentAPrimaryTeam.ID, CCAgent__c = AgentLevel1_B_TEST.ID);
        teamMembers.add(AgentBMembership );

        TeamAgentMapping__c AgentBPrimaryMembership = new TeamAgentMapping__c(DefaultTeam__c=true, CCTeam__c=AgentBPrimaryTeam.ID, CCAgent__c = AgentLevel1_B_TEST.ID);
        teamMembers.add(AgentBPrimaryMembership );
        
        insert teamMembers;
        
        // 1. Insert a new default team (AgentCPrimaryTeam) for Agent A
        System.debug('AP42.TEST.INFO - Test 1 - Insert a new default team (AgentCPrimaryTeam) for Agent A');
        TeamAgentMapping__c AgentAPrimaryMembership_teamC = new TeamAgentMapping__c(DefaultTeam__c=true, CCTeam__c=AgentCPrimaryTeam.ID, CCAgent__c = AgentLevel1_A_TEST.ID);
        insert AgentAPrimaryMembership_teamC;
        
        // 2. Insert a duplicated team for Agent A.
        System.debug('AP42.TEST.INFO - Test 2 - Insert a duplicated team for Agent A.');
        AP42_TeamAssignmentMethods.AP42_bypass = false;
        try {
            TeamAgentMapping__c AgentBPrimaryMembership_teamB = new TeamAgentMapping__c(DefaultTeam__c=true, CCTeam__c=AgentBPrimaryTeam.ID, CCAgent__c = AgentLevel1_B_TEST.ID);
            insert AgentBPrimaryMembership_teamB;
        }
        catch(Exception e) {
            System.debug('AP42.TEST.INFO - Exception when trying to insert duplicate record: ' + e.getMessage());
        }
        
        // 3. Try to assign agent C to both A and B team as primary at once
        AP42_TeamAssignmentMethods.AP42_bypass = false;
        System.debug('AP42.TEST.INFO - Test 3 - Try to assign agent C to both A and B team as primary at once.');
        try {
            List<TeamAgentMapping__c> duplicateTeams = new List<TeamAgentMapping__c>();
            TeamAgentMapping__c AgentCPrimaryMembership_t1 = new TeamAgentMapping__c(DefaultTeam__c=true, CCTeam__c=AgentBPrimaryTeam.ID, CCAgent__c = AgentLevel1_C_TEST.ID);
            TeamAgentMapping__c AgentCPrimaryMembership_t2 = new TeamAgentMapping__c(DefaultTeam__c=true, CCTeam__c=AgentAPrimaryTeam.ID, CCAgent__c = AgentLevel1_C_TEST.ID);            
            duplicateTeams.add(AgentCPrimaryMembership_t1);
            duplicateTeams.add(AgentCPrimaryMembership_t2);
            insert duplicateTeams;
        }
        catch(Exception e) {
            System.debug('AP42.TEST.INFO - Exception when trying to insert duplicate record: ' + e.getMessage());
        }
        
        // 4. Try to insert duplicate records
        AP42_TeamAssignmentMethods.AP42_bypass = false;
        System.debug('AP42.TEST.INFO - Test 3 - Try to assign agent C to both A and B team as primary at once.');
        try {
            List<TeamAgentMapping__c> duplicateTeams = new List<TeamAgentMapping__c>();
            TeamAgentMapping__c AgentCPrimaryMembership_t1 = new TeamAgentMapping__c(DefaultTeam__c=false, CCTeam__c=AgentAPrimaryTeam.ID, CCAgent__c = AgentLevel1_C_TEST.ID);
            TeamAgentMapping__c AgentCPrimaryMembership_t2 = new TeamAgentMapping__c(DefaultTeam__c=false, CCTeam__c=AgentAPrimaryTeam.ID, CCAgent__c = AgentLevel1_C_TEST.ID);            
            duplicateTeams.add(AgentCPrimaryMembership_t1);
            duplicateTeams.add(AgentCPrimaryMembership_t2);
            insert duplicateTeams;
        }
        catch(Exception e) {
            System.debug('AP42.TEST.INFO - Exception when trying to insert duplicate record: ' + e.getMessage());
        }
     }
}