/*
    Author          : Karthik (ACN) 
    Date Created    : 09/02/2011
    Description     : Test class for the apex class AP07_CaseStatusAge
*/

@isTest

private class AP07_CaseStatusAge_TEST{
    
/* @Method <This method caseStatusTestMethod is used test the AP07_CaseStatusAge class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Not throwing any Exception>
*/
    static testMethod void caseStatusTestMethod(){
        
        Country__c count = Utils_TestMethods.createCountry();
        insert count;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.country__c=count.id;
        insert account1;
        
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        insert contact1;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert case1;
        
        Test.startTest();        
        Case caseRecord9 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord9.Status='Open/ Waiting For Details - Customer';
        update caseRecord9;
       
        Case caseRecord1 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord1.Status='Open/ Waiting For Details - Internal';
        update caseRecord1;
        
        Case caseRecord2 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord2.Status='Closed/ Pending Action';
        update caseRecord2;
        
        Case caseRecord3 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord3.Status='Open/ Level 1 Escalation';
        update caseRecord3;
        
        Case caseRecord4 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord4.Status='Open/ Level 2 Escalation';
        caseRecord4.CommercialReference__c = 'testCommRef';
        caseRecord4.Family__c = 'testFamily';
        update caseRecord4;
        
        Case caseRecord5 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord5.Status='Open/ Level 3 Escalation';
        caseRecord5.CommercialReference__c = 'testCommRef';        
        caseRecord5.Family__c = 'testFamily';
        update caseRecord5;
        
        Case caseRecord6 = [Select Id,status,TECH_OpenAnswerAvailabletoL1AgeinHrs__c from case where id=:case1.Id];
        caseRecord6.Status='Open/ Answer Available To L1';
        update caseRecord6;
        
        Case caseRecord7 = [Select Id,status,TECH_OpenAnswerAvailabletoL2AgeinHrs__c from case where id=:case1.Id];
        caseRecord7.Status='Open/ Answer Available To L2';
        update caseRecord7;
        
        Case caseRecord8 = [Select Id,status,TECH_OpenLevel1EscalationAgeinHrs__c from case where id=:case1.Id];
        caseRecord8.Status='Open/ Level 1 Escalation';
        update caseRecord8;        
        Test.stopTest();
    }
}