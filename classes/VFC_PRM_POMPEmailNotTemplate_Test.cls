@isTest
private class VFC_PRM_POMPEmailNotTemplate_Test{

    static testmethod void testNotificationTemplate(){
    
        User u = Utils_TestMethods.createStandardUser('Mar16PRM');
        insert u;
        
        Country__c cont = Utils_TestMethods.createCountry();
        insert cont;
        
        Account acc = Utils_TestMethods.createAccount(u.id);
        insert acc;
        
        PartnerProgram__c prog =  Utils_TestMethods.createPartnerProgram();
        prog.ProgramStatus__c = 'Active';
        insert prog;
        
        
        PartnerProgram__c contProg = Utils_TestMethods.createCountryPartnerProgram(prog.Id,cont.Id);
        contProg.ProgramStatus__c = 'Active';
        insert contProg;
        
        Opportunity Optty = Utils_TestMethods.createOpportunity(acc.Id);
        insert Optty;
        
        OpportunityRegistrationForm__c OpRegForm = Utils_TestMethods.CreateORF(cont.Id);
        OpRegForm.PartnerProgram__c = contProg.Id;
        insert OpRegForm;
        
        VFC_PRM_POMPEmailNotificationTemplate notTemplate = new VFC_PRM_POMPEmailNotificationTemplate();
        notTemplate.OrfId = OpRegForm.Id;
        notTemplate.ObjType ='OpportunityRegistrationForm__c';
        notTemplate.EmailContentFor='ORF_SUBMITTED';
        notTemplate.RelatedId = u.id;
        notTemplate.RecptLanguage = u.LanguageLocaleKey;
        notTemplate.OpptyId = Optty.Id;
    }
}