/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 29 June 2012
    Description  : 1. checkContractStatus()
                       To check that Inactive/Draft Contract cannot have an Active package
                   2. checkDuplicate()
                       Checks for duplicate records
                   3. createProactiveCases()
                       Create proactive cases for active packages based on start and end package dates
                       
    Modified By : Shruti Karn
    Description : Added:
                   updateContractType() -  Update Contract Type based on the Package Level
    Modified By : Vimal K
    Description : Updated:
                   updateContractType() -  Update Contract Type based on the Package Level  for Eclipse Contract as well (BR-3466 )  
    Modified By : Pooja Suresh
    Description : Added:
                   createProactiveCases() - Create proactive cases for active packages
***************************************************************************************************************************/
public class AP_PackageTriggerUtils
{
    public static void checkContractStatus(list<Package__c> lstPackage)
    {
        System.Debug('AP_PackageTriggerUtils.checkContractStatus Method is called.');
        map<Id,Package__c> mapContractIdPKG = new map<Id,Package__c>();
        for(Package__c PKG : lstPackage)
        {
            mapContractIdPKG.put(PKG.Contract__c, PKG);
            
        }
        map<Id,Contract> mapContract = new map<Id,Contract>([Select id,status from Contract where id in : mapContractIdPKG.keySet() and (status =: System.LAbel.CLSEP12CCC24 
                                                                or status =: System.Label.CLSEP12CCC25) limit 10000]);
        for(Id contractId : mapContractIdPKG.keySet())
        {
            if(mapContract.keySet().contains(contractId ) && mapContractIdPKG.get(contractId).status__c == System.Label.CLSEP12CCC26)
            {
                mapContractIdPKG.get(contractId).addError(System.Label.CLSEP12CCC27);
            }
           
        }
        System.Debug('End of AP_PackageTriggerUtils.checkContractStatus Method');
    }
     
    public static void updateContractType(list<Package__c> lstNewPackage)
    {
        System.Debug('Start of AP_PackageTriggerUtils.updateContractType Method');
        set<Id> setPkgContractId = new set<Id>();
        for(Package__c pkg: lstNewPackage)
            if(!setPkgContractId.contains(pkg.contract__c))
                setPkgContractId.add(pkg.contract__c);
        
        list<Package__c> lstPackage = new list<Package__c>();
        lstPackage = [Select support_level__c , id, contract__c from Package__c where status__C = 'Active' and startdate__c<= today and enddate__c >= today and contract__c in : setPkgContractId order by support_level__c asc limit 50000];
        map<Id,String> mapPkgSupportLevel = new map<Id,String>();
        for(Package__c pkg : lstPackage)
        {
            if(!mapPkgSupportLevel.containsKey(pkg.contract__c) && pkg.support_level__c != null)
                mapPkgSupportLevel.put(pkg.contract__c , pkg.support_level__c);
        }

        list<Contract> lstContract = new list<Contract>();
        String contractQuery = 'Select id, contracttype__c, LeadingBusinessBU__c, WebAccess__c, ContractClassification__c, SupportContractLevel__c  from Contract where id in:setPkgContractId limit '+ Limits.getLimitDMLRows();
        lstContract = database.query(contractQuery);                  

        for(Contract contract : lstContract)
        {
          if(contract.LeadingBusinessBU__c == System.Label.CLDEC12CCC01 || contract.WebAccess__c == System.Label.CLOCT12CCC07) //BR-3466 
              if(mapPkgSupportLevel.containsKey(contract.Id))
                  
                  
                  contract.SupportContractLevel__c = CS_SupportContractLevel__c.getInstance(mapPkgSupportLevel.get(contract.Id)).SupportContractLevel__c;
              else
                  contract.SupportContractLevel__c = null;
        
        }

        update lstContract;
        System.Debug('End of AP_PackageTriggerUtils.updateContractType Method');
    }   
    
    public static void createProactiveCases(map<id,Package__c> mapOIdPkg,map<id,Package__c> mapNewPkg){

        System.Debug('AP_PackageTriggerUtils.createProactiveCases Method is called.');
        
        List<Case> lstProactiveCases = new List<Case>();
        Set<Id> setPkgContractId = new Set<Id>();
        List<Package__c> lstActivePackages = new List<Package__c>();
        String currMonth = System.now().format('MMMM');
        Integer year = Date.Today().Year();

        //Get contract ids of active packages
        for(Package__c pkg: mapNewPkg.values())            
            if(!setPkgContractId.contains(pkg.contract__c) && pkg.Status__c == System.Label.CLSEP12CCC26 && pkg.AutomaticProactiveCase__c){
                setPkgContractId.add(pkg.contract__c);
                lstActivePackages.add(pkg);
            }
        System.Debug('AP_PackageTriggerUtils.createProactiveCases lstActivePackages - '+lstActivePackages);

        //Get contract information of all contracts 
        Map<Id,Contract> mapContractDetails = new Map<Id,Contract>([SELECT Id, AccountId, ContactName__c, ContractNumber FROM Contract WHERE Id IN :setPkgContractId LIMIT :Limits.getLimitDMLRows()]);
        System.Debug('AP_PackageTriggerUtils.createProactiveCases mapContractDetails - '+mapContractDetails);

        //Create proactive case instances for all eligible packages
        for(Package__c pkg: lstActivePackages){

            if(mapContractDetails.containsKey(pkg.Contract__c)){

                //Get contract details of the currently referenced package
                Contract currPkgContract = mapContractDetails.get(pkg.Contract__c);

                //Create a proactive case 60 days after the Package Start Date
                if(pkg.TECH_TriggerFirstProactiveCase__c && pkg.TECH_TriggerFirstProactiveCase__c != mapOIdPkg.get(pkg.Id).TECH_TriggerFirstProactiveCase__c){
                    Case proCase1 = new Case();
                    proCase1.AccountId = currPkgContract.AccountId;
                    proCase1.ContactId = currPkgContract.ContactName__c;
                    proCase1.Team__c   = pkg.ProactiveCaseSupportTeam__c;
                    proCase1.Subject   = System.Label.CLQ316CCC01 + ' 01 - ' + currMonth + ' ' + year + ' ' + System.Label.CLQ316CCC02 + currPkgContract.ContractNumber;
                    proCase1.ProactiveCase__c = true;
                    proCase1.OwnerId   = System.Label.CLOCT13CCC15;                
                    lstProactiveCases.add(proCase1);
                    System.Debug('AP_PackageTriggerUtils.createProactiveCases inside loop Case 1 - '+proCase1);
                }

                //Create a proactive case 120 days before the Package End Date
                if(pkg.TECH_TriggerSecondProactiveCase__c && pkg.TECH_TriggerSecondProactiveCase__c != mapOIdPkg.get(pkg.Id).TECH_TriggerSecondProactiveCase__c){
                    Case proCase2 = new Case();
                    proCase2.AccountId = currPkgContract.AccountId;
                    proCase2.ContactId = currPkgContract.ContactName__c;
                    proCase2.Team__c   = pkg.ProactiveCaseSupportTeam__c;
                    proCase2.Subject   = System.Label.CLQ316CCC01 + ' 02 - ' + currMonth + ' ' + year + ' ' + System.Label.CLQ316CCC02 + currPkgContract.ContractNumber;
                    proCase2.OwnerId   = System.Label.CLOCT13CCC15;
                    proCase2.ProactiveCase__c = true;
                    lstProactiveCases.add(proCase2);
                    System.Debug('AP_PackageTriggerUtils.createProactiveCases inside loop Case 2 - '+proCase2);
                }
            }
        }

        System.Debug('AP_PackageTriggerUtils.createProactiveCases lstProactiveCases - '+lstProactiveCases);
        if(lstProactiveCases.size()>0){
            insert lstProactiveCases;
        }
        System.Debug('End of AP_PackageTriggerUtils.createProactiveCases Method');
    }

}