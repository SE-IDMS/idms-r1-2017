/*
    Author: Bhuvana Subramaniyan
    Purpose: Merged different functionalities of Account Update Request After Insert to a single class
*/
public class AP_AURAfterInsertHandler
{
  public void onBeforeInsert(List<AccountUpdateRequest__c> aurs, Map<Id, AccountUpdateRequest__c> aursMp)
  {
      //THIS METHOD UPDATES THE OWNER FIELD OF AUR WITH THE OWNER QUEUE OF THE CORRESPONDING ACCOUNT'S COUNTRY  
      Map<Id, Id> aurCountryIds = new Map<Id, Id>();
      List<Id> accountIds = new List<Id>();
      Map<Id, Id> countryUserIds  = new Map<Id, Id>();
      List<AccountUpdateRequest__c> accUpdReqs = new List<AccountUpdateRequest__c>();
      List<AccountUpdateRequest__c> accUpdReqStrategic = new List<AccountUpdateRequest__c>();
      
      for(AccountUpdateRequest__c aur: aurs)
      {
                if(aur.RecordTypeId==System.Label.CLMAR16ACC01  ||  Test.isRunningTest())    //CHECK FOR COUNTRY AUR RECORDTYPE ID
                {
                    accUpdReqs.add(aur);
                    aurCountryIds.put(aur.Account__c,aur.TECH_AccountCountry__c);
                } 
                if(aur.RecordTypeId==System.Label.CLMAR16ACC02  ||  Test.isRunningTest())    //CHECK FOR STRATEGIC AUR RECORDTYPE ID
                {
                    accUpdReqStrategic.add(aur);
                }                                               
           
      }

      //COUNTRY ACCOUNT UPDATE REQUEST  
      if(accUpdReqs.size() > 0)
      {
          //QUERIES THE COUNTRY FOR THE LIST OF OWNER IDS (QUEUES OF RESPECTIVE COUNTRY)
          List<Country__c> countries = [select OwnerId, DataSteward__c, Id from Country__c where Id in: aurCountryIds.values()];
          
          for(Country__c count: countries)
          {
              countryUserIds.put(count.Id, count.OwnerId);                       //count.DataSteward__c);
          }
           
          //UPDATES THE OWNER OF COUNTRY AUR 
          for(AccountUpdateRequest__c aur: accUpdReqs)
          {
              aur.OwnerId = countryUserIds.get(aur.TECH_AccountCountry__c);      //aur.Approver__c = countryUserIds.get(aur.TECH_AccountCountry__c);
          }
      }
      
      //STRATEGIC ACCOUNT UPDATE REQUEST
      if(accUpdReqStrategic.size() > 0)
      {
          //UPDATES THE OWNER OF STRATEGIC AUR 
          for(AccountUpdateRequest__c aur: accUpdReqStrategic)
          {
              aur.OwnerId = System.Label.CLMAR16ACC03;      
          }
                    
      }   
         
  }
}