@isTest
private class AP_AAP_AccountProgramUpdate_TEST
{
    public static testmethod void myUnitTest()
    {
        FeatureCatalog__c feature = new FeatureCatalog__c();
        feature.Description__c = 'Test Description';
        insert feature;

        FeatureCatalog__c feature1 = new FeatureCatalog__c();
        feature1.Description__c = 'Test Description';
        insert feature1;

        RequirementCatalog__c requirement = new RequirementCatalog__c();
        requirement.Description__c = 'Test Description';
        requirement.Applicableto__c = Label.CLMAY13PRM10;
        insert requirement;

        RequirementCatalog__c requirement1 = new RequirementCatalog__c();
        requirement1.Description__c = 'Test Description';
        requirement1.Applicableto__c = Label.CLMAY13PRM11;
        insert requirement1;

        FeatureRequirement__c feaReq = Utils_TestMethods.createFeatureRequirement(feature.Id, requirement.Id);
        Insert feaReq;

        FeatureRequirement__c feaReq1 = Utils_TestMethods.createFeatureRequirement(feature1.Id, requirement1.Id);
        Insert feaReq1;

        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='US';
        insert country;
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='FR';
        insert country1;

        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

        insert newRecTypeMap;
        
        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;

        Test.startTest();
        ProgramFeature__c pFeature= new ProgramFeature__c();
        pFeature.FeatureCatalog__c = feature.Id;
        pFeature.PartnerProgram__c = gp1.Id;
        pFeature.ProgramLevel__c = prg1.Id;
        pFeature.FeatureStatus__c = 'Active';
        insert pFeature;

        ProgramFeature__c pgFeature= new ProgramFeature__c();
        pgFeature.FeatureCatalog__c = feature1.Id;
        pgFeature.PartnerProgram__c = gp1.Id;
        pgFeature.ProgramLevel__c = prg1.Id;
        pgFeature.FeatureStatus__c = 'Active';
        insert pgFeature;
        
        ProgramFeature__c pFeature1 = new ProgramFeature__c();
        pFeature1.FeatureCatalog__c = feature.Id;
        pFeature1.PartnerProgram__c = cp1.Id;
        pFeature1.ProgramLevel__c = prg2.Id;
        pFeature1.FeatureStatus__c = 'Active';
        pFeature1.GlobalProgramFeature__c = pFeature.Id;
        insert pFeature1;

        ProgramFeature__c pFeature2 = new ProgramFeature__c();
        pFeature2.FeatureCatalog__c = feature1.Id;
        pFeature2.PartnerProgram__c = cp1.Id;
        pFeature2.ProgramLevel__c = prg2.Id;
        pFeature2.FeatureStatus__c = 'Active';
        pFeature2.GlobalProgramFeature__c = pgFeature.Id;
        insert pFeature2;

        ProgramRequirement__c pRequirement = new ProgramRequirement__c();
        pRequirement.RequirementCatalog__c = requirement.Id;
        pRequirement.PartnerProgram__c = gp1.Id;
        pRequirement.ProgramLevel__c = prg1.Id;
        pRequirement.Active__c = True;
        insert pRequirement;

        ProgramRequirement__c pRequirement1 = new ProgramRequirement__c();
        pRequirement1.RequirementCatalog__c = requirement1.Id;
        pRequirement1.PartnerProgram__c = cp1.Id;
        pRequirement1.ProgramLevel__c = prg2.Id;
        pRequirement1.GlobalProgramRequirement__c = pRequirement.Id;
        pRequirement1.Active__c = True;
        insert pRequirement1;

        FeatureRequirement__c newFeatureRequirement = Utils_TestMethods.createFeatureRequirement(feature1.Id , requirement1.Id);
        insert newFeatureRequirement;

        Account acc = Utils_TestMethods.createAccount();
        //acc.country__c = country.id;
        //acc.isPartner = true;
        acc.PRMAccount__c = True;
        acc.ClassLevel1__c = 'LC';
        acc.ClassLevel2__c = 'LC2';
        acc.PRMAccount__c = TRUE;
        Insert acc;

        
        acc.IsPartner = TRUE;
        update acc;
        
        Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
        contact.PRMContact__c = true;
        Insert contact;
        
        Assessment__c asm = new Assessment__c();
        asm = Utils_TestMethods.createAssessment();
        asm.AutomaticAssignment__c = true;
        asm.Name = 'Sample Assessment';
        asm.Active__c = true;
        asm.PartnerProgram__c = cp1.id;
        asm.ProgramLevel__c = prg2.id;
        insert asm;
        
        /*asm = Utils_TestMethods.createAssessment();
        asm.Name = 'Test';
        asm.AutomaticAssignment__c = false;
        asm.Name = 'Sample Assessment1';
        asm.PartnerProgram__c = cp1.id;
        asm.ProgramLevel__c = prg2.id;
        asm.specialization__c = sp2.id;
        insert asm;*/

        AssessementProgram__c assmentRec = new AssessementProgram__c();
        assmentRec.Assessment__c = asm.Id;
        assmentRec.PartnerProgram__c = cp1.Id;
        assmentRec.ProgramLevel__c = prg2.Id;
        assmentRec.Active__c = True;
        insert assmentRec;

        OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq;

        OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
        insert opassa;

        /*AccountAssignedFeature__c accAssFeature = new AccountAssignedFeature__c();
        //accAssFeature.Active__c = false;
        accAssFeature.Account__c = acc.Id;
        accAssFeature.featureCatalog__c = feature.id;
        //accAssFeature.AccountAssignedProgram__c = accPgm.Id;
        Insert accAssFeature;*/
        
        AccountAssignedFeature__c accAssFeature = Utils_TestMethods.createAccountFeature(feature.id, acc.Id);
        accAssFeature.Active__c = false;
        Insert accAssFeature;
        //accAssFeature.Active__c = false;
        //Update accAssFeature;
        //Test.stopTest();
        AccountAssignedFeature__c accAssFeature1 = Utils_TestMethods.createAccountFeature(feature1.id, acc.Id);
        accAssFeature1.Active__c = True;
        Insert accAssFeature1;

        AccountAssignedRequirement__c accAssReq = new AccountAssignedRequirement__c();
        accAssReq.RequirementStatus__c = 'Active';
        accAssReq.Account__c = acc.Id;
        accAssReq.RequirementCatalog__c= requirement.id;
        //accAssReq.AccountAssignedProgram__c = accPgm.Id;
        accAssReq.AccountAssignedFeature__c = accAssFeature.Id;
        Insert accAssReq;
        Test.stopTest();
        
        ACC_PartnerProgram__c accPgm = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
        //acc.TECH_UniqueAssignedId__c = 
        insert accPgm;
        accPgm.active__c = true;
        update accPgm;
        
        ContactAssignedProgram__c contactAssignedProgram = new ContactAssignedProgram__c();
        contactAssignedProgram.AccountAssignedProgram__c = accPgm.Id;
        contactAssignedProgram.Active__c = True;
        contactAssignedProgram.Contact__c = contact.Id;
        contactAssignedProgram.PartnerProgram__c = cp1.id;
        contactAssignedProgram.ProgramLevel__c = prg2.id;
        insert contactAssignedProgram;
        
        
        System.debug('Account_Test'+accPgm.Account__c);
        System.debug('ProgramLevel__C_Test'+accPgm.ProgramLevel__C);
        System.debug('accPgm_Test'+accPgm.Id);
        //Test.startTest();
        List<Account> accList = new List<Account>();
        accList.add(acc);
        List<PartnerProgram__c> partnerprgList = new List<PartnerProgram__c> ();
        partnerprgList.add(cp1);
        AP_AAP_AccountProgramUpdate.CreateAccountAssignedProgram(accList,partnerprgList);
        
    }
    
    public static testmethod void myUnitTest1()
    {
        FeatureCatalog__c feature = new FeatureCatalog__c();
        feature.Description__c = 'Test Description';
        insert feature;

        RequirementCatalog__c requirement = new RequirementCatalog__c();
        requirement.Description__c = 'Test Description';
        requirement.Applicableto__c = Label.CLMAY13PRM10;
        insert requirement;

        FeatureRequirement__c feaReq = Utils_TestMethods.createFeatureRequirement(feature.Id, requirement.Id);
        Insert feaReq;

        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='US';
        insert country;
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='FR';
        insert country1;

        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

        insert newRecTypeMap;
        
        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;

        Test.startTest();
        ProgramFeature__c pFeature= new ProgramFeature__c();
        pFeature.FeatureCatalog__c = feature.Id;
        pFeature.PartnerProgram__c = gp1.Id;
        pFeature.ProgramLevel__c = prg1.Id;
        pFeature.FeatureStatus__c = 'Active';
        insert pFeature;
        
        ProgramFeature__c pFeature1 = new ProgramFeature__c();
        pFeature1.FeatureCatalog__c = feature.Id;
        pFeature1.PartnerProgram__c = cp1.Id;
        pFeature1.ProgramLevel__c = prg2.Id;
        pFeature1.FeatureStatus__c = 'Active';
        pFeature1.GlobalProgramFeature__c = pFeature.Id;
        insert pFeature1;

        ProgramRequirement__c pRequirement = new ProgramRequirement__c();
        pRequirement.RequirementCatalog__c = requirement.Id;
        pRequirement.PartnerProgram__c = gp1.Id;
        pRequirement.ProgramLevel__c = prg1.Id;
        pRequirement.Active__c = True;
        insert pRequirement;

        ProgramRequirement__c pRequirement1 = new ProgramRequirement__c();
        pRequirement1.RequirementCatalog__c = requirement.Id;
        pRequirement1.PartnerProgram__c = cp1.Id;
        pRequirement1.ProgramLevel__c = prg2.Id;
        pRequirement1.GlobalProgramRequirement__c = pRequirement.Id;
        pRequirement1.Active__c = True;
        insert pRequirement1;

        Account acc = Utils_TestMethods.createAccount();
        //acc.country__c = country.id;
        Insert acc;

        acc.isPartner = true;
        acc.PRMAccount__c = True;
        acc.ClassLevel1__c = 'LC';
        acc.ClassLevel2__c = 'LC1';
        update acc;

        Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
        contact.PRMContact__c = true;
        Insert contact;
        
        /*Assessment__c asm = new Assessment__c();
        asm = Utils_TestMethods.createAssessment();
        asm.AutomaticAssignment__c = true;
        asm.Active__c = true;
        insert asm;*/
        
        Assessment__c asm = new Assessment__c();
        asm = Utils_TestMethods.createAssessment();
        asm.AutomaticAssignment__c = true;
        asm.Name = 'Sample Assessment1';
        asm.Active__c = true;
        asm.PartnerProgram__c = cp1.id;
        asm.ProgramLevel__c = prg2.id;
        insert asm;

        AssessementProgram__c assmentRec = new AssessementProgram__c();
        assmentRec.Assessment__c = asm.Id;
        assmentRec.PartnerProgram__c = cp1.Id;
        assmentRec.ProgramLevel__c = prg2.Id;
        assmentRec.Active__c = True;
        insert assmentRec;

        OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq;

        OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
        insert opassa;

        /*AccountAssignedFeature__c accAssFeature = new AccountAssignedFeature__c();
        accAssFeature.Active__c = true;
        accAssFeature.Account__c = acc.Id;
        accAssFeature.featureCatalog__c = feature.id;
        //accAssFeature.AccountAssignedProgram__c = accPgm.Id;
        Insert accAssFeature;*/
        
        //AccountAssignedFeature__c accAssFeature = Utils_TestMethods.createAccountFeature(feature.id, acc.Id);
        //accAssFeature.Active__c = false;
        //Insert accAssFeature;
        //accAssFeature.Active__c = false;
        //Update accAssFeature;

        AccountAssignedRequirement__c accAssReq = new AccountAssignedRequirement__c();
        accAssReq.RequirementStatus__c = 'Active';
        accAssReq.Account__c = acc.Id;
        accAssReq.RequirementCatalog__c= requirement.id;
        //accAssReq.AccountAssignedProgram__c = accPgm.Id;
        //accAssReq.AccountAssignedFeature__c = accAssFeature.Id;
        Insert accAssReq;

        Test.stopTest();
        ACC_PartnerProgram__c accPgm = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
        //acc.TECH_UniqueAssignedId__c = 
        insert accPgm;
        accPgm.active__c = true;
        update accPgm;

        System.debug('Account_Test'+accPgm.Account__c);
        System.debug('ProgramLevel__C_Test'+accPgm.ProgramLevel__C);
        System.debug('accPgm_Test'+accPgm.Id);
        
        List<Account> accList = new List<Account>();
        accList.add(acc);
        AP_AAP_AccountProgramUpdate.CreateAccountAssignedProgram(accList,null);
    }
}