public with sharing class VFC_NewAURRedirect{

    //Variables
    public Id accId{get; set;}
    public Account a{get; set;} {a = new Account();}
    public String aurRecType{get; set;} 
    public String retURL;
    
    public VFC_NewAURRedirect(ApexPages.StandardController controller) 
    {
        accId = ApexPages.currentPage().getParameters().get(Label.CLMAR16ACC18);    //Id of CurrentAccountName__c field
        aurRecType = ApexPages.currentPage().getParameters().get('RecordType');    //Id of AUR RecordType
        retURL = ApexPages.currentPage().getParameters().get('retURL');     //Get Id in retURL, in case of 'Save' button it is Account, in case of 'Save&New' it is last created AUR
              
        if(accId!=null)
            a=[Select Name,AccType__c,Phone,Fax,Street__c,AdditionalAddress__c,City__c,ZipCode__c,StateProvince__c,StateProvince__r.Name,Country__c,Country__r.Name,VATNumber__c,LeadingBusiness__c,ToBeDeleted__c,ReasonForDeletion__c,DuplicateWith__r.Name,CorporateHeadquarters__c,Inactive__c,InactiveReason__c,ParentId,Parent.Name,UltiParentAcc__c,UltiParentAcc__r.Name,ClassLevel1__c,ClassLevel2__c,MarketSegment__c,MarketSubSegment__c,PRMAccount__c from Account where id=:accId];  
        
    }

}