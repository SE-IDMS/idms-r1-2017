Public Class Utils_GMRDataSource extends Utils_DataSource
{
    WS04_GMR.criteriaInDataBean Criteria; // Search Criteria
    WS04_GMR.paginationInDataBean Pagination; // Pagination parameters
    public final static String GMR_ENDPOINT = Label.CL00376;
    public final static Long GMR_RECORD_PER_PAGE = 99; // Number of records per page
    public final static Long GMR_PAGE_NUMBER = 1; // Only the first page is required
    public final static Integer GMR_TIMEOUT = 40000; // Timeout in ms
    public final static String CLIENT_CERTIFICATE_NAME = Label.CL00616; // Name of HTTPS Certificate
    public WS04_GMR.resultDataBean Results;
    public WS_GMRSearch.resultFilteredDataBean FilteredResults;
    
    public override List<SelectOption> getColumns()
    {
        List<SelectOption> Columns = new List<SelectOption>();
        Columns.add(new SelectOption('Field8__c',Label.CL00374)); // First Column
        Columns.add(new SelectOption('Field9__c',Label.CL00375)); 
        Columns.add(new SelectOption('Field4__c',Label.CL00354));
        Columns.add(new SelectOption('Field3__c',Label.CL00353));
        Columns.add(new SelectOption('Field2__c',Label.CL00352));
        Columns.add(new SelectOption('Field1__c',Label.CL00351)); // Last Column
        return Columns;
    }
    public Utils_DataSource.Result FilteredSearch(List<String> KeyWords, List<String> Filters){
        // Instanciate the Web Service and its inner classes
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean WSFilters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        WSFilters.exactSearch=false;
        WSFilters.onlyCatalogNumber=false;
        WSFilters.excludeOld=false;
        WSFilters.excludeMarketingDocumentation=false;
        WSFilters.excludeSpareParts=false;
           
        // Set search keywords and filters    
        WSFilters.keyWordList = KeyWords;
        
        if(Filters.size()>=1 && Filters[0] != Label.CL00355)          
            WSFilters.businessLine = Filters[0];    
        if(Filters.size()>=2 && Filters[1] != Label.CL00355) 
            WSFilters.productLine = Filters[1].substring(2,4);
        if(Filters.size()>=3 && Filters[2] != Label.CL00355)
            WSFilters.strategicProductFamily = Filters[2].substring(4,6);
        if(Filters.size()>=4 && Filters[3] != Label.CL00355)
            WSFilters.Family = Filters[3].substring(6,8);

        // Web Service Call  
        if(!test.isRunningTest())    
            FilteredResults = GMRConnection.globalFilteredSearch(WSFilters,Pagination);
        // Set the output as a Utils_DataSource.Result 
        Utils_DataSource.Result ReturnedResult = new Utils_DataSource.Result(); 
        ReturnedResult.recordList = new List<DataTemplate__c>();
    
        ReturnedResult.errorCode = FilteredResults.returnDataBean.errorNumber; 
        ReturnedResult.numberOfPage = FilteredResults.returnDataBean.numberOfPage; 
        ReturnedResult.numberOfRecord = FilteredResults.returnDataBean.numberOfRecord; 
        ReturnedResult.returnCode = FilteredResults.returnDataBean.returnCode; 
        ReturnedResult.pageNumber = FilteredResults.returnDataBean.pageNumber; 
        ReturnedResult.functionalMessage = FilteredResults.returnDataBean.functionalErrorMessage;
        ReturnedResult.returnMessage = FilteredResults.returnDataBean.returnMessage ;

        if(FilteredResults.gmrFilteredDataBeanList != null)
        {
            For(WS_GMRSearch.gmrFilteredDataBean DBL:FilteredResults.gmrFilteredDataBeanList)
            {
                DataTemplate__c Product = new DataTemplate__c();
                                
                Product.Field1__c = SetFieldLabel(DBL.businessLine);
                Product.Field2__c = SetFieldLabel(DBL.productLine);
                Product.Field3__c = SetFieldLabel(DBL.strategicProductFamily);
                Product.Field4__c = SetFieldLabel(DBL.family); 
                Product.Field5__c = SetFieldLabel(DBL.subFamily);
                Product.Field6__c = SetFieldLabel(DBL.productSuccession);
                Product.Field7__c = SetFieldLabel(DBL.productGroup);
                Product.Field8__c = DBL.commercialReference;
                Product.Field9__c = DBL.description;
                
                Boolean AlreadyDisplayed = false;
                                 
                //for all 7 level's GMR Code
                if(DBL.businessLine.value != null){
                    Product.Field11__c = DBL.businessLine.value;
                }
                if(DBL.productLine.value != null){
                     Product.Field11__c += DBL.productLine.value;
                }
                if(DBL.strategicProductFamily.label != null){
                    Product.Field11__c += DBL.strategicProductFamily.value;
                }
                if(DBL.family.label != null){
                    Product.Field11__c += DBL.family.value;
                }
                if(DBL.subFamily.label != null){
                    Product.Field11__c += DBL.subFamily.value;
                }
                if(DBL.productSuccession.label != null){
                    Product.Field11__c += DBL.productSuccession.value;
                }
                if(DBL.productGroup.label != null){
                    Product.Field11__c += DBL.productGroup.value;
                }
                
                if(!AlreadyDisplayed){
                    ReturnedResult.recordList.add(Product);
                }
            }
        }
        
        System.debug('#### Result :' + ReturnedResult);
        return ReturnedResult;
    }
    public override Utils_DataSource.Result Search(List<String> KeyWords, List<String> Filters){
        // Instanciate the Web Service and its inner classes
        WS04_GMR.GMRSearchPort GMRConnection = new WS04_GMR.GMRSearchPort();    
        
        GMRConnection.endpoint_x =  GMR_ENDPOINT;
        GMRConnection.timeout_x = GMR_TIMEOUT;
        GMRConnection.ClientCertName_x = CLIENT_CERTIFICATE_NAME;
    
        WS04_GMR.criteriaInDataBean Criteria = new WS04_GMR.criteriaInDataBean();
        WS04_GMR.paginationInDataBean Pagination = new WS04_GMR.paginationInDataBean();
        
        // Set pagination parameters
        Pagination.maxLimitePerPage = GMR_RECORD_PER_PAGE;
        Pagination.pageNumber = GMR_PAGE_NUMBER;
        
        // Set search keywords and criteria    
        Criteria.keyWordList = KeyWords;
        
        if(Filters.size()>=1 && Filters[0] != Label.CL00355)          
            Criteria.businessLine = Filters[0];    
        if(Filters.size()>=2 && Filters[1] != Label.CL00355) 
            Criteria.productLine = Filters[1].substring(2,4);
        if(Filters.size()>=3 && Filters[2] != Label.CL00355)
            Criteria.strategicProductFamily = Filters[2].substring(4,6);
        if(Filters.size()>=4 && Filters[3] != Label.CL00355)
            Criteria.Family = Filters[3].substring(6,8);

        // Web Service Call  
        if(!test.isRunningTest())    
            Results = GMRConnection.globalSearch(Criteria,Pagination);
        // Set the output as a Utils_DataSource.Result 
        Utils_DataSource.Result ReturnedResult = new Utils_DataSource.Result(); 
        ReturnedResult.recordList = new List<DataTemplate__c>();
    
        ReturnedResult.errorCode = Results.returnDataBean.errorNumber; 
        ReturnedResult.numberOfPage = Results.returnDataBean.numberOfPage; 
        ReturnedResult.numberOfRecord = Results.returnDataBean.numberOfRecord; 
        ReturnedResult.returnCode = Results.returnDataBean.returnCode; 
        ReturnedResult.pageNumber = Results.returnDataBean.pageNumber; 
        ReturnedResult.functionalMessage = Results.returnDataBean.functionalErrorMessage;
        ReturnedResult.returnMessage = Results.returnDataBean.returnMessage ;

        if(Results.gmrDataBeanList != null)
        {
            For(WS04_GMR.gmrDataBean DBL:Results.gmrDataBeanList)
            {
                DataTemplate__c Product = new DataTemplate__c();
                                
                Product.Field1__c = SetField(DBL.businessLine);
                Product.Field2__c = SetField(DBL.productLine);
                Product.Field3__c = SetField(DBL.strategicProductFamily);
                Product.Field4__c = SetField(DBL.family); 
                Product.Field5__c = SetField(DBL.subFamily);
                Product.Field6__c = SetField(DBL.productSuccession);
                Product.Field7__c = SetField(DBL.productGroup);
                Product.Field8__c = DBL.commercialReference;
                Product.Field9__c = DBL.description;
                
                Boolean AlreadyDisplayed = false;
                  
                if(DBL.businessLine.value != null)
                {
                    Product.Field10__c = DBL.businessLine.value;
                }
                if(DBL.productLine.value != null)
                {
                     Product.Field10__c += DBL.productLine.value;
                }
                if(DBL.strategicProductFamily.label != null)
                {
                    Product.Field10__c += DBL.strategicProductFamily.value;
                }
                if(DBL.family.label != null)
                {
                    Product.Field10__c += DBL.family.value;
                }
                
                //for all 7 level's GMR Code
                if(DBL.businessLine.value != null)
                {
                    Product.Field11__c = DBL.businessLine.value;
                }
                if(DBL.productLine.value != null)
                {
                     Product.Field11__c += DBL.productLine.value;
                }
                if(DBL.strategicProductFamily.label != null)
                {
                    Product.Field11__c += DBL.strategicProductFamily.value;
                }
                if(DBL.family.label != null)
                {
                    Product.Field11__c += DBL.family.value;
                }
                if(DBL.subFamily.label != null)
                {
                    Product.Field11__c += DBL.subFamily.value;
                }
                if(DBL.productSuccession.label != null)
                {
                    Product.Field11__c += DBL.productSuccession.value;
                }
                if(DBL.productGroup.label != null)
                {
                    Product.Field11__c += DBL.productGroup.value;
                }
                
                if(!AlreadyDisplayed)
                {
                    ReturnedResult.recordList.add(Product);
                }
            }
        }
        
        System.debug('#### Result :' + ReturnedResult);
        return ReturnedResult;
    }
    
    public String SetField(WS04_GMR.labelValue Category)
    {
        if(Category.value=='--')
            return '--';
        else
            return Category.label;    
    }
    public String SetFieldLabel(WS_GMRSearch.labelValue Category){
        if(Category.value=='--')
            return '--';
        else
            return Category.label;    
    }
    
}