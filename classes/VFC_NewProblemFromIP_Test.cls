@isTest
private class VFC_NewProblemFromIP_Test {

    static testMethod void NewIP() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test2222');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
        
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                    accOrg1.Name='Test';
                    accOrg1.Entity__c='Test Entity-2'; 
                    insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
                    
            CSQ_Profile__c CP = new CSQ_Profile__c();
                CP.Name = runAsUser.Name;
                CP.CSQ_bFOUser__c = runAsUser.Id;
                CP.CSQ_DefaultOrganization__c = accOrg.Id;
                insert CP;
            
            EntityStakeholder__c OrgSH = Utils_TestMethods.createEntityStakeholder(accOrg.Id,runAsUser.Id,'CI Deployment Leader');
            insert OrgSH;
            
            CSQ_ImprovementProject__c IP1 = new CSQ_ImprovementProject__c();
                    IP1.CSQ_IPTitle__c = 'test1';
                    IP1.CSQ_DetectionOrganization__c = accOrg.Id;
                    IP1.CSQ_AccountableOrganization__c = accOrg.Id;
                    IP1.CSQ_PointsOfDetection__c = 'Customer';
                    IP1.CSQ_WhatIsTheProblem__c = 'what';
                    IP1.CSQ_WhyIsItaProblem__c = 'why';
                    IP1.CSQ_HowWasTheProblemDetected__c = 'how';
                    IP1.CSQ_WhenWasTheProblemDetected__c = 'when';
                    IP1.CSQ_WhoDetectedTheProblem__c = 'who';
                    IP1.CSQ_WhereWasTheProblemDetected__c = 'where';
                    IP1.CSQ_Severity__c = 'Minor';
                    IP1.CSQ_ProjectStartDate__c = System.TODAY();
                    insert IP1;
            List<CSQ_ImprovementProject__c> lstIP = new List<CSQ_ImprovementProject__c>();
            lstIP.add(IP1);
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';
            prob.RecordTypeid = Label.CLI2PAPR120014;
            prob.CSQ_Improvement_Project__c = IP1.Id;
            insert prob;
            
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstIP);
            PageReference pageRef = Page.VFP_NewProblemFromIP;
            ApexPages.currentPage().getParameters().put('id', IP1.id);
            VFC_NewProblemFromIP newProb = new VFC_NewProblemFromIP(controller);
            newProb.NewEditPage();
                    
        }
    }
}