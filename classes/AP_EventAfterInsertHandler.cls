public class AP_EventAfterInsertHandler
{
    //APRIL 2014 RELEASE BR-4616
    //The date when first event or Event was created is to be stored in "TECH_First Activity" field of Opportunity
    public static void insertOpptyEvents(Map<Id,Event> newOpptyEvents)
    {
        System.Debug('--------- newOpptyEvents '+newOpptyEvents);
        Map<Id, Date> whatIdCreatedDate = new Map<Id, Date>();
        List<Opportunity> updateOppties = new List<Opportunity>();
        set<Id> completedWhatIds = new set<Id>();
        
        System.Debug('--------- completedWhatIds '+completedWhatIds);
        
        for(Event oEvent: newOpptyEvents.values())
        {
            System.Debug('--------- oEvent '+oEvent);
            if(!(completedWhatIds.contains(oEvent.whatId)))  //checks if the completedWhatIds does not contain the same Opportunity 
            {
                    completedWhatIds.add(oEvent.whatId);                         //Stores the unique whatId of tasks
                    System.Debug('--------- completedWhatIds '+completedWhatIds);
                    whatIdCreatedDate.put(oEvent.whatId, oEvent.CreatedDate.date());     
                    System.Debug('--------- oEvent.whatId'+oEvent.whatId+'-------------oEvent.CreatedDate '+oEvent.CreatedDate.date());
            }
        }
        
        //Queries the List of leads which needs to be updated
        List<Opportunity> opptyLst = [select Id, TECH_FirstActivity__c from Opportunity where Id in: completedWhatIds Limit 50000];
        
        for(Opportunity opp: opptyLst)
        {
             //Updates the First Activity field of Opportunity with the created date of the Event
             if((opp.TECH_FirstActivity__c==NULL) && whatIdCreatedDate.containsKey(opp.Id))
                  opp.TECH_FirstActivity__c = date.today();
             System.Debug('--------- TECH_FirstActivity__c '+opp.TECH_FirstActivity__c);
             updateOppties.add(opp);
             
             System.Debug('--------- updateOppties '+updateOppties);
        }
        
        if(updateOppties.Size() > 0)
        {
            if(updateOppties.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())  //determine if there are too many Opportunities to update and avoid governor limits
            {
                System.Debug('######## AP25 Opportunity Update Error : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] opptyUpdate= Database.update(updateOppties, false);
                for(Database.SaveResult sr: opptyUpdate)
                {
                    System.Debug('--------- opptyUpdate '+opptyUpdate);
                    if(!sr.isSuccess())
                    {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) 
                        {
                            System.debug('######## AP25 Opportunity Update Error :');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            //System.debug('Fields that affected this error: ' + err.getFields());
                        }
                    }                   
                }
            }                         
        }
    }
}