public abstract class PRM_FeatureCustomPermissionSet implements FieloPRM_FeatureCustomLogic{
    
    public PRM_FeatureCustomPermissionSet (){
    }
    
    public abstract String getPermissionSetName();
    
    public void promote(Set<String> PRMUIMSIdList){
       System.debug('PRM_FeatureCustomPermissionSet promote');
       //get Users
       assignPermissionSet(PRMUIMSIdList,getPermissionSetName());
    }

    //@future removed in Q3 Release after the introduction of the F_PRM_IsSynchronic__c field
    
    private static void assignPermissionSet(Set<String> PRMUIMSIdList,String psetName){
       PermissionSet pset1 = [select id from PermissionSet where name =: psetName];
       List<PermissionSetAssignment> psetAssignList = new List<PermissionSetAssignment>();
       for(User user : [select Id,FederationIdentifier,Profile.Name from user where Contact.PRMUIMSId__c in: PRMUIMSIdList and Contact.PRMUIMSId__c!=null and FederationIdentifier!=null and Id not in (select AssigneeId from PermissionSetAssignment where PermissionSetId=:pset1.Id)]){
            System.debug('###Assign pset '+psetName+' to user with FederationIdentifier: ' + user.FederationIdentifier + '. ,Profile.Name:'+user.Profile.Name);
            PermissionSetAssignment psetAssign = new PermissionSetAssignment();
            psetAssign.PermissionSetId = pset1.id;
            psetAssign.AssigneeId = user.Id;
            psetAssignList.add(psetAssign);
       }
       Database.insert(psetAssignList,false);
       //Assign to a group
       List<GroupMember> lstGroupMem = new list<GroupMember>();
       for(User user : [select Id,FederationIdentifier,Profile.Name from user where Contact.PRMUIMSId__c in: PRMUIMSIdList and Contact.PRMUIMSId__c!=null and FederationIdentifier!=null and Id not in (select UserOrGroupId from GroupMember where GroupId=:Label.CLMAR13PRM026)]){
            System.debug('###Assign group '+Label.CLMAR13PRM026+' to user with FederationIdentifier: ' + user.FederationIdentifier + '. ,Profile.Name:'+user.Profile.Name);
            GroupMember newMember = new GroupMember();
            newMember.GroupId = Label.CLMAR13PRM026;
            newMember.UserOrGroupId =  user.Id;
            lstGroupMem.add(newMember);
       }
       Database.insert(lstGroupMem,false);
    }

    public void demote(Set<String> PRMUIMSIdList){
        System.debug('PRM_FeatureCustomPermissionSet demote');
        System.debug('PRMUIMSIdList: ' + PRMUIMSIdList);
        removePermissionSet(PRMUIMSIdList,getPermissionSetName());
    }

    //@future removed in Q3 Release after the introduction of the F_PRM_IsSynchronic__c field
    
    private static void removePermissionSet(Set<String> PRMUIMSIdList,String psetName){
        List<PermissionSetAssignment> psetAssignList = [select Id From PermissionSetAssignment where Assignee.FederationIdentifier in:PRMUIMSIdList and Assignee.FederationIdentifier!=null and PermissionSet.Name =: psetName];
        System.debug('###psetAssignList '+psetAssignList);
        Database.delete(psetAssignList,false);
    }

}