/************************************************************
* Developer: Denis Aranda                                   *
* Created Date: 19/08/2014                                  *
************************************************************/
public with sharing class Fielo_WelcomeController {
    
    public decimal pointsLeft {get;set;}
    public String currentYear {get;set;}
    public FieloEE__Member__c member {get; set;}
    
    public Fielo_WelcomeController(){
        String memId = FieloEE.MemberUtil.getMemberId();
        
        date hoy = date.today();
        currentYear = String.valueOf(hoy.year());
        
        member = [SELECT Id, Name, FieloEE__Name__c, FieloEE__Points__c, FieloEE__imageId__c FROM FieloEE__Member__c WHERE Id =: memId]; 
        
        List<FieloEE__Point__c> listPoints = [SELECT Id, Name, FieloEE__ExpirationDate__c, FieloEE__Points__c, FieloEE__PointsUsed__c 
                                    FROM FieloEE__Point__c WHERE FieloEE__Member__c =: memId AND FieloEE__ExpirationDate__c >=: hoy 
                                    AND FieloEE__ExpirationDate__c <=: date.newinstance(hoy.year(), 12, 31)];
        
        pointsLeft = 0;
        
        for(FieloEE__Point__c poi: listPoints){      
            if(poi.FieloEE__Points__c == null){
                pointsLeft += -poi.FieloEE__PointsUsed__c;                
            }else{    
                pointsLeft += poi.FieloEE__Points__c - poi.FieloEE__PointsUsed__c;                     
            }
        }
    }
    
    public pagereference doShopping(){
        return new PageReference('/apex/Fielo_ShoppingCart');
    }
    
}