@isTest(SeeAllData=true)
global class ZuoraQuoteDefaultValuesTest extends zqu.CancellationPropertyController.PopulateDefaultFieldValuePlugin {

	public static final String acctId = '12345678901234567890123456789012';
	public static final String subId = '23456789012345678901234567890123';

	@isTest
	private static void ZuoraQuoteDefaultValuesTestMethod() {

 		List<Country__c> countryList = [SELECT Id from Country__c WHERE Name = 'France'];

        Id countryId = countryList.get(0).Id;

        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=countryId, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');

        List<Account> accList = new List<Account>();
        accList.add(acc);
        insert accList;

        Zuora__CustomerAccount__c zBillingAccount1 = testObjectCreator.CreateBillingAccount('TestBA1', 'ContactName', acc.Id);

        zqu__Quote__c quote = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = '1234');
        List<zqu__Quote__c> quoteList = new List<zqu__Quote__c>();
        quoteList.add(quote);
        insert quoteList;
	  	

	  	ZuoraQuoteDefaultValues controller = new ZuoraQuoteDefaultValues();
	  	PageReference createQuote = Page.zqu__CreateQuote;
	  	
	  	createQuote.getParameters().put('quoteType', 'Cancel Subscription');
	  	createQuote.getParameters().put('stepNumber', '2');
	  	createQuote.getParameters().put('subscriptionId', '23456789012345678901234567890123');
	  	Test.setCurrentPage(Page.zqu__CreateQuote);

	  	Test.startTest();

 		controller.populateDefaultFieldValue(quote, new zqu.PropertyComponentController.ParentController());

	  	Test.stopTest();
	}
}