@isTest
Public Class VFC_SRV_MYFsContractsController_Test{
  Static TestMethod Void  UniTest(){
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  
  Account acc1 = new Account(Name = 'TestAccount',Country__c =Country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

  user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;
 // system.runAs(u) {
    test.starttest();
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
   
   Date startdate = date.Today().adddays(-40);
   Date  enddate = date.Today().adddays(+2);
   SVMXC__Installed_Product__c InstalledProduct = new SVMXC__Installed_Product__c();
   InstalledProduct.SVMXC__Company__c =acc1.Id;
   InstalledProduct.SVMXC__Site__c = location1.Id;
   InstalledProduct.UnderContract__c =False;
   InstalledProduct.SVMXC__Warranty_End_Date__c = enddate ;
   InstalledProduct.SVMXC__Warranty_End_Date__c = startdate ;
   Insert InstalledProduct ;
   
   //Date startdate = date.newInstance(2016, 4, 1);
   //Date  enddate = date.newInstance(2016, 6, 1);
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'PCV';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = startdate;
   Contract.SVMXC__end_Date__c = enddate;
   insert contract;
   
   VFC_SRV_MYFsContractsController ContratController= new VFC_SRV_MYFsContractsController();
   ContratController.Site=Location1.Id;
   ContratController.SelectedCountry = Country.CountryCode__c;
   ContratController.ParentContractId  = Contract.id; 
   ContratController.UltimateParentAccount  = acc1.id;
   ContratController.GetContracts();     
   
   Test.stoptest();
  //}
 }
 Static TestMethod Void  UniTest2(){
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  
   
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  
  Account acc1 = new Account(Name = 'TestAccount',Country__c =Country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

  user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;
 // system.runAs(u) {
    test.starttest();
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;
   system.debug('LocationTEST'+location1);

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
   
   Date startdate = date.Today().adddays(-40);
   Date  enddate = date.Today().adddays(+2);
   SVMXC__Installed_Product__c InstalledProduct = new SVMXC__Installed_Product__c();
   InstalledProduct.SVMXC__Company__c =acc1.Id;
   InstalledProduct.SVMXC__Site__c = location1.Id;
   InstalledProduct.UnderContract__c =False;
   InstalledProduct.SVMXC__Warranty_End_Date__c = enddate ;
   InstalledProduct.SVMXC__Warranty_End_Date__c = startdate ;
   Insert InstalledProduct ;
   
    
 
   
   SVMXC__Service_Plan__c ServicePlan = new SVMXC__Service_Plan__c();
   ServicePlan.name = 'TestServicePlan';
   Insert ServicePlan;
   
   SVMXC__Service__c AvailableServices = new SVMXC__Service__c();
   AvailableServices.Name= 'Preventive Maintanance Visti 5 *8';
   AvailableServices.SVMXC__Data_Type__c = 'Text';
   AvailableServices.SVMXC__Service_Type__c = 'Installation';
   Insert AvailableServices;
   
   SVMXC__Service_Offerings__c ServiceOffering = new SVMXC__Service_Offerings__c();
   ServiceOffering.SVMXC__Available_Services__c = AvailableServices.Id;
   ServiceOffering.SVMXC__Service_Plan__c = ServicePlan.id;
   Insert ServiceOffering;
   
   //Date startdate = date.newInstance(2016, 4, 1);
   //Date  enddate = date.newInstance(2016, 6, 1);
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'PCV';
   Contract.SVMXC__Service_Plan__c = ServicePlan.Id;
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = startdate;
   Contract.SVMXC__end_Date__c = enddate;
   insert contract;
  
   SVMXC__Service_Contract_Services__c ServiceFeature = new SVMXC__Service_Contract_Services__c();
   ServiceFeature.SVMXC__Service_Contract__c = contract.id;
   
   SVMXC__Service_Contract_Products__c CoveredInstalledProduct = new SVMXC__Service_Contract_Products__c();
   CoveredInstalledProduct.SVMXC__Service_Contract__c = contract.id;
   CoveredInstalledProduct.SVMXC__Installed_Product__c = InstalledProduct.id;
   CoveredInstalledProduct.AssetLocation__c = location1.id;
   insert CoveredInstalledProduct;
  
   
   VFC_SRV_MYFsContractsController ContratController= new VFC_SRV_MYFsContractsController();
   ContratController.Site='View All';
   ContratController.SelectedCountry = Country.CountryCode__c;
   ContratController.ParentContractId  = Contract.id; 
   ContratController.UltimateParentAccount  = acc1.id;   
   ContratController.GetContracts(); 
   Test.stoptest();
  }
// }
Static TestMethod Void  UniTest3(){
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  
   
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  
  Account acc1 = new Account(Name = 'TestAccount',Country__c =Country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

  user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;
 // system.runAs(u) {
    test.starttest();
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;
   system.debug('LocationTEST'+location1);

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
   
   Date startdate = date.Today().adddays(-40);
   Date  enddate = date.Today().adddays(+2);
   SVMXC__Installed_Product__c InstalledProduct = new SVMXC__Installed_Product__c();
   InstalledProduct.SVMXC__Company__c =acc1.Id;
   InstalledProduct.SVMXC__Site__c = location1.Id;
   InstalledProduct.UnderContract__c =False;
   InstalledProduct.SVMXC__Warranty_End_Date__c = enddate ;
   InstalledProduct.SVMXC__Warranty_End_Date__c = startdate ;
   Insert InstalledProduct ;
   
    
 
   
   SVMXC__Service_Plan__c ServicePlan = new SVMXC__Service_Plan__c();
   ServicePlan.name = 'TestServicePlan';
   Insert ServicePlan;
   
   SVMXC__Service__c AvailableServices = new SVMXC__Service__c();
   AvailableServices.Name= 'Preventive Maintanance Visti 5 *8';
   AvailableServices.SVMXC__Data_Type__c = 'Text';
   AvailableServices.SVMXC__Service_Type__c = 'Installation';
   Insert AvailableServices;
   
   SVMXC__Service_Offerings__c ServiceOffering = new SVMXC__Service_Offerings__c();
   ServiceOffering.SVMXC__Available_Services__c = AvailableServices.Id;
   ServiceOffering.SVMXC__Service_Plan__c = ServicePlan.id;
   Insert ServiceOffering;
   
   //Date startdate = date.newInstance(2016, 4, 1);
   //Date  enddate = date.newInstance(2016, 6, 1);
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'PCV';
   Contract.SVMXC__Service_Plan__c = ServicePlan.Id;
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = startdate;
   Contract.SVMXC__end_Date__c = enddate;
   insert contract;
   
   Date Contractstartdate = date.Today().adddays(-60);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ServiceLine = New SVMXC__Service_Contract__c();
   ServiceLine.SoldtoAccount__c = acc1.id;
   ServiceLine.ParentContract__c = contract.Id;
   ServiceLine.SVMXC__Service_Contract_Notes__c = 'Test';
   ServiceLine.name= 'TestContract';
   ServiceLine.Status__c= 'EXP';
   ServiceLine.LeadingBusinessBU__c= 'GC';
   ServiceLine.SVMXC__Start_Date__c = Contractstartdate ;
   ServiceLine.SVMXC__end_Date__c = Contractenddate ;
   insert ServiceLine;
  
   SVMXC__Service_Contract_Services__c ServiceFeature = new SVMXC__Service_Contract_Services__c();
   ServiceFeature.SVMXC__Service_Contract__c = contract.id;
   
   SVMXC__Service_Contract_Products__c CoveredInstalledProduct = new SVMXC__Service_Contract_Products__c();
   CoveredInstalledProduct.SVMXC__Service_Contract__c = ServiceLine.id;
   CoveredInstalledProduct.SVMXC__Installed_Product__c = InstalledProduct.id;
   CoveredInstalledProduct.AssetLocation__c = location1.id;
   insert CoveredInstalledProduct;
  
   
   VFC_SRV_MYFsContractsController ContratController= new VFC_SRV_MYFsContractsController();
   ContratController.Site='View All';
   ContratController.SelectedCountry = Country.CountryCode__c;
   ContratController.ParentContractId  = Contract.id; 
   ContratController.UltimateParentAccount  = acc1.id;   
   ContratController.GetContracts(); 
   Test.stoptest();
  }
 }