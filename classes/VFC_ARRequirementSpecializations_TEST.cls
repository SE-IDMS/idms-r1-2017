@isTest
private class VFC_ARRequirementSpecializations_TEST
{
 static testMethod void myUnitTest() {
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0) {
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
          //user.BypassWF__c = false;
            user.BypassVR__c = true;
            System.runAs(user){
            
            CertificationCatalog__c certcat = new CertificationCatalog__c();
            certcat.CertificationName__c = 'Test Certification Cataog';
            certcat.Active__c = true;
            insert certcat;
            
            SpecializationCatalog__c spcat = Utils_TestMethods.createSpecializationCatalog();
            spcat.Active__c = true;
            insert spcat;
            
            RequirementCatalog__c reqcat = Utils_TestMethods.createRequirementCatalog();
            reqcat.Name = 'Test Requirement Catalog';
            reqcat.Certification__c = certcat.id;
            insert reqcat;
            
            Specialization__c sp = Utils_TestMethods.createSpecialization(spcat.id);
            sp.Status__c = 'active';
            sp.SpecializationCatalog__c = spcat.id;
            insert sp;
            
            Specialization__c csp = Utils_TestMethods.createCountrySpecialization(sp.id, spcat.id);
            csp.Status__c = 'active';
            csp.GlobalSpecialization__c = sp.id;
            csp.SpecializationCatalog__c = spcat.id;
            insert csp;
            
            RequirementSpecialization__c reqsp = new RequirementSpecialization__c();
            reqsp.RequirementCatalog__c = reqcat.id;
            reqsp.Specialization__c = csp.id;
            insert reqsp;
            
            Apexpages.Standardcontroller cont = new Apexpages.Standardcontroller(reqcat);
            Pagereference pg4 = Page.VFP_AddRemoveRequirementSpecializations; 
            Test.setCurrentPage(pg4);
                    
            VFC_AddRemoveRequirementSpecializations vfcon = new VFC_AddRemoveRequirementSpecializations(cont);
            vfcon.updaterequirementspecialization();
          }
       }
    }
}