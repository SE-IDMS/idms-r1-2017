/*
    Author:             Pooja Gupta
    Date of Creation:   07-01-2013
    Description:  Test class for VFC_AccountQuestionController

*/


@isTest
private class VFC_AccountQuestionController_TEST
{
    static testmethod void testcontrollermethod()
    {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        QOTW__c qc = new QOTW__c(StartDate__c = system.date.today(),Enddate__c = system.date.today(),IsActive__c = true);
        insert qc;
        QuestionTemplate__c q = new QuestionTemplate__c(QuestionoftheWeek__c = qc.id,
        IncludeOther__c = true,
        Answers__c = 'test\ntest\n');
        insert q;
        CustomerQuickQuestion__c  cv = new CustomerQuickQuestion__c(QuestionTemplate__c = q.id,
        Account__c = objAccount.Id,
        Contact__c = objContact.Id);
        insert cv;
        
        VFC_AccountQuestionController qtp = new VFC_AccountQuestionController(new ApexPages.StandardController(objAccount));
        qtp.OtherAnswerSelected = True;
        qtp.OtherAnswer = 'Test';
        qtp.saveresponse();
        
        qtp.OtherAnswerSelected = False;
        qtp.OtherAnswer = 'Test';
        qtp.saveresponse();
        
        qtp.getAnswerChoices();
        qtp.getChoices();   
    }
}