/*
 Author:Siddharth Nagavarapu (sesa206167)
 Purpose:To update the contact owner with the account owner if the owner is not an SDH user.
 */
 public class AP_updateContactOwner{
    public static void updateContactOwner(Map<Id,List<Contact>> tobeUpdatedAccountContactMap){
        Map<Id,Account> accountMap=new Map<Id,Account>([select Id,OwnerId from Account where id in :tobeUpdatedAccountContactMap.keySet() and OwnerId!=:System.LABEL.CLNOV13ACC21]);
        for(Id accountId:accountMap.keySet())
        {
            for(Contact contactRecord:tobeUpdatedAccountContactMap.get(accountId))
            {
                contactRecord.OwnerId=accountMap.get(accountId).OwnerId;
            }
        }
    }
 }