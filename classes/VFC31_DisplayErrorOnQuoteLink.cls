/*
    Author          : Abhishek Yadav (ACN) 
    Date Created    : 25/05/2011
    Description     : Class utilised to display errors on Quotelink.
*/
public class VFC31_DisplayErrorOnQuoteLink {
public string quotelinkID; 
    public VFC31_DisplayErrorOnQuoteLink(ApexPages.StandardController controller) 
    {
       quotelinkID=controller.getID();     
    }
    
    public void displayError()
    {
    System.Debug('******displayError method in VFC31_DisplayErrorOnQuoteLink Class Start****');    
        if(quotelinkID!=null)
        {
        OPP_QuoteLink__c quotelink=[select TECH_IsSyncNeededForOpp__c,TECH_IsSynchronizationNeeded__c,QuoteStatus__c,TECH_IsBoundToBackOffice__c
        from OPP_QuoteLink__c where id=:quotelinkID];     
            if((quoteLink.QuoteStatus__c!=system.label.CL00397)&&(quoteLink.TECH_IsSynchronizationNeeded__c))
            {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,system.label.CL00396));             
            }
           if((quoteLink.TECH_IsSyncNeededForOpp__c)&&(quoteLink.TECH_IsBoundToBackOffice__c))
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,system.label.CL00395)); 
            }
        }
    System.Debug('******displayError method in VFC31_DisplayErrorOnQuoteLink Class End****');    
    }
}