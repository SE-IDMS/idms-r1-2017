/*
 Controller for VFP_AMPBubble.
 Copied from VFC07_SFEBubble 
*/
public class VFC_AMPBubble  {
        
    public AccountMasterProfile__c amp{get;set;}
    private String flashVars;
    public Boolean bTestFlag = false;                                            
    Id ampId;

    public VFC_AMPBubble(ApexPages.StandardController stdController) {    
        System.debug('<<<<<<********** Constructor*************>>>>>>'+stdController.getID()); 
        ampId=System.currentPageReference().getParameters().get( 'ampID' );                   
    }

    public String getBubbles() {
        String stringX = '';
        String stringY = '';
        Integer x = 0;
        Integer y = 0;
        Set<String> points=new Set<String>();
        String output='';
        boolean error=false;
        try{ 
            if(ampId != null){
                amp=[select Id,Score__c,MarketShare__c from AccountMasterProfile__c where Id=:ampId];
                if((amp.Score__c!= null)&&(amp.MarketShare__c != null)){
                    if(!points.contains(amp.Score__c.intValue().format()+','+amp.MarketShare__c.intValue().format()))
                    {
                        points.add(amp.Score__c.intValue().format()+','+amp.MarketShare__c .intValue().format());
                        stringX+= amp.Score__c.intValue().format()+',';
                        stringY+= amp.MarketShare__c .intValue().format()+',';
                    }                                                                                            
                }
                else
                    error=true;
               System.debug('points'+points);
               if (points.size()!=0) {
                   stringX = stringX.substring(0, stringX.length() - 1);
                   stringY = stringY.substring(0, stringY.length() - 1);   
               }
               output = 'xBubbles=' + stringX + '&yBubbles=' + stringY + '&rBubble=1';                                  
          }
          else
              error=true;
          if(error) 
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLQ316SLS023));
          if(Test.isRunningTest() && bTestFlag)
              throw new TestException ();       
        }catch (Exception e){
            ApexPages.addMessages(e);
        }
        return output;
    }
         
    public String getFlashVars(){
        String varsOutput = getBubbles();
        flashVars = '';
        flashVars+='xThreshT1=' + Integer.ValueOf(Label.CLQ316SLS012);
        flashVars+='&xThreshT2=' + Integer.ValueOf(Label.CLQ316SLS013);
        flashVars+='&yThreshT1=' + Integer.ValueOf(Label.CLQ316SLS010);
        flashVars+='&yThreshT2=' + Integer.ValueOf(Label.CLQ316SLS011);
        if (varsOutput != '') 
            flashVars+='&'+ varsOutput;
            flashVars+='&isUpdatable=false';
            System.debug('### Sending FlashVars to Bubble Chart ==> ' + this.flashVars);
        return flashVars;
    }
  
    public class TestException extends Exception {}
        
}