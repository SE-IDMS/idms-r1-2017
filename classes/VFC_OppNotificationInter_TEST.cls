@isTest(SeeAllData=true)
private class VFC_OppNotificationInter_TEST {
    public static testMethod void createOppNotificationTestMethod(){
        System.debug('#### VFP_OppNotificationInter_Test.createOppNotificationTestMethod Started ####');
        
        User caseCreationUser = Utils_TestMethods.createStandardUser('TestUser');
        caseCreationUser.BypassVR__c =true;
        caseCreationUser.UserBusinessUnit__c = 'EN';
        Insert caseCreationUser;
       
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

       Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
       insert objContact;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = objAccount.id;
        site.PrimaryLocation__c = false;
        site.TimeZone__c = 'Test';
        insert site;
        
        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
        
         RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
         RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          for(RecordType rt : rts) 
        {
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
               insert st;
           } 
        }
        for(RecordType rt : rtssg)
        {
           if(rt.DeveloperName == 'Technican')
            {
                sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = caseCreationUser.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '12345'
                                                            );
                insert sgm1;
            }
        }
        
        Country__c country = Utils_TestMethods.createCountry();
        country.Name = 'India';
        insert country;
        
         SVMXC__Service_Order__c workOrder =  New SVMXC__Service_Order__c();
         workOrder.SVMXC__Company__c = objAccount.Id;
         //workOrder.Name='Test';
         workOrder.Work_Order_Category__c='On-site';                 
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'New';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'EN';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
         workOrder.SendAlertNotification__c = False;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         workOrder.SVMXC__Site__c = site.Id;
        insert workOrder;
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(Name='test');
        insert sc;
         
      //  SVMXC__Service_Order__c workOrder = Utils_TestMethods.createWorkOrder(objAccount.Id);
       // workOrder.SVMXC__Group_Member__c = sgm1.Id;
       // insert workOrder;
         system.debug('workOrder:'+workOrder);
        
         
         OpportunityNotification__c  oppon = New OpportunityNotification__c();
         oppon.Name = 'test';
         oppon.AccountForConversion__c = objAccount.id;
         oppon.Location__c = site.Id;
         oppon.LeadingBusinessBU__c = 'EN';
         oppon.WorkOrder__c = workOrder.Id;
         oppon.OpportunityDetector__c = caseCreationUser.Id;
         insert oppon;
         
         Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCase.CCCountry__c = country.id;
        objCase.ProductLine__c = 'BDBMS';
        insert objCase;
       
        List<Case> lstCase = [Select id, Casenumber from Case where id = :objCase.id Limit 1];
        
        if (lstCase.size() > 0) {
         String caseNo = lstCase[0].casenumber;
         ID caseID = lstCase[0].id;
                
      //Pagereference pageReference = new pagereference('/apex/VFP_OppNotificationInter?CF00NA0000009eElI='+caseNo+'&CF00NA0000009eElI_lkid='+caseID+'&scontrolCaching=1&retURL=%2F'+caseid+'&RecordType=012K00000004f8V&ent=01IA0000002J6qn&save_new=1&sfdc.override=1'); 
       PageReference pageReference = Page.VFP_OppNotificationInter;                                                
        Test.setCurrentPageReference(pageReference);
            
        // pageReference.getParameters().put(Label.CLMAY13SRV17, workOrder.Name);
        pageReference.getParameters().put(Label.CLOCT13CCC03,caseNo);
        /*
        PageReference pageRef2 = ApexPages.currentPage();  
        
        System.Debug('******* Opened page *****');      
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(objCase );   
        VFC_OppNotificationInter oppNotificationInter = new VFC_OppNotificationInter(standardController);
       
        oppNotificationInter.Init();*/
        
     
        ApexPages.StandardController controller = new ApexPages.StandardController(oppon);
        VFC_OppNotificationInter newObj = new VFC_OppNotificationInter(controller);
        
        PageReference pageRef = Page.VFP_OppNotificationInter ;
        pageRef.getParameters().put(Label.CLMAY13SRV14, caseCreationUser.Id);
        Test.setCurrentPage(pageRef);
        newObj.Init();
        
        VFC_OppNotificationInter newObj1 = new VFC_OppNotificationInter(controller);
        
        PageReference pageRef1 = Page.VFP_OppNotificationInter ;
        pageRef1.getParameters().put(Label.CLMAY13SRV18,WorkOrder.Id);
        Test.setCurrentPage(pageRef1);
        newObj1.Init();   
        
     }   
 
   }
}