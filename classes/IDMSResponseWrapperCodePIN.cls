/**
•   Created By: Onkar
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is handling the code PIN Response from external system
**/ 
global with sharing class IDMSResponseWrapperCodePIN {
    public String Message;  
    public String Status; 
    
    //Constructor per default
    public IDMSResponseWrapperCodePIN (){}
    
    //Constructor of a code pin response
    public IDMSResponseWrapperCodePIN(string Status,string Message){
        this.Status     = Status;
        this.Message    = Message;
    }    
}