public with sharing class VFCServiceContractaccountCockpit{

    
    private ID mySerConId;
    public SVMXC__Service_Contract__c at = new SVMXC__Service_Contract__c();
    public String mData{get;set;}
    public Boolean fCompliancy{get;set;}
    public VFCServiceContractaccountCockpit(ApexPages.StandardController controller){
        mySerConId=controller.getId();
        //at = (SVMXC__Service_Contract__c)controller.getRecord();
        at = [select id, SoldtoAccount__c from SVMXC__Service_Contract__c where id=:mySerConId ];
        cockpit_hierarchy();
    }
    public Map<Id,String> parentSerConIdSet {get;set;}
    
    private void cockpit_hierarchy(){
        Boolean fTopIsOn=false;
        Integer fCount=0;
        parentSerConIdSet=new Map<Id,String>();
        mData='data.addRows([';
        parentSerConIdSet.put(mySerConId,null);
        List<Integer>IntegerListToNullify=new List<Integer>();
        List<String> ListStringTemp1=new List<String>();
        List<String> ListStringTemp2=new List<String>();
        Integer secureBarrier=0;
        Integer i,iTemp;
        Integer IntegerRowLine=0;
        String lastSentenceJS='';
        List<SVMXC__Service_Contract__c> mySerConList;
        //try
        {
            fCompliancy=true;
            //system.debug('aa=='+at.SoldtoAccount__c);
            List<Account> Accs = [Select Id,Name,OwnerId,Owner.Name,AnnualSales__c,AssociatedRelationshipSuite__c,(Select ofVisists__c, SoldtoAccount__c,SoldtoAccount__r.Name,Owner.Name,SVMXC__Activation_Notes__c, SVMXC__Active__c, SVMXC__All_Contacts_Covered__c, SVMXC__All_Products_Covered__c, SVMXC__All_Services_Covered__c, SVMXC__All_Sites_Covered__c, BackOfficeReference__c, BackOfficeSystem__c, BillingOption__c, BillingPlan__c, SVMXC__Billing_Schedule__c, SVMXC__Business_Hours__c, BusinessMix__c, SVMXC__Cancelation_Notes__c, SVMXC__Canceled_By__c, SVMXC__Canceled_On__c, ChannelType__c, SVMXC__Contact__c, ContractClassification__c, Name, SVMXC__Contract_Price2__c, SVMXC__Contract_Price__c, toLabel(ContractType__c), CountryofBackOffice__c, CreatedById, CreatedDate, Credits__c, CurrencyIsoCode, DONOTDELIVER__c, DONOTPRINT__c, SVMXC__Default_Group_Member__c, SVMXC__Default_Parts_Price_Book__c, SVMXC__Default_Service_Group__c, SVMXC__Default_Travel_Price__c, SVMXC__Default_Travel_Unit__c, IsDeleted, SVMXC__Discount__c, SVMXC__Discounted_Price2__c, SVMXC__Discounted_Price__c, SVMXC__End_Date__c, SVMXC__EndpointURL__c, SVMXC__Exchange_Type__c, InitialPrice__c, IsEvergreen__c, IsLocked, Jobonsitetriggerinvoice__c, LastActivityDate, LastModifiedById, LastModifiedDate, toLabel(LeadingBusinessBU__c), MarketSegment__c, MarketSubSegment__c, MayEdit, SVMXC__Minimum_Labor__c, SVMXC__Minimum_Travel__c, OpportunityScope__c, OpportunityType__c, OwnerExpirationNotice__c, OwnerId, ParentContract__c, SVMXC__Primary_Technician__c, Id, SVMXC__Renewal_Date__c, SVMXC__Renewal_Notes__c, SVMXC__Renewal_Number__c, SVMXC__Renewed_From__c, SVMXC__Round_Labor_To_Nearest__c, SVMXC__Round_Travel_To_Nearest__c, SVMXC__Labor_Rounding_Type__c, SVMXC__Travel_Rounding_Type__c, SVMXC__SESSION_ID__c, SVMXC__Service_Level__c, SVMXC__Sales_Rep__c, SVMXC__Select__c, SendCustomerRenewalNotification__c, SendWelcomePackage__c, SVMXC__Service_Contract_Notes__c, SVMXC__Service_Plan__c, SVMXC__Service_Pricebook__c, SVMXC__Start_Date__c, SupportAmount__c, SystemModstamp, TECH_ContactEmail__c, TECH_NotificationDate__c, SVMXC__Weeks_To_Renewal__c, SVMXC__Zone__c from R00N70000001hzYIEAY__r where SVMXC__Active__c = True) from Account where Id = :at.SoldtoAccount__c];
            system.debug('acs==='+Accs);
            Map<id,user> uidphoturlmap = new Map<id,user>();
            Set<id> owneridset = new Set<id>();
            for(Account a:Accs)
            {
                owneridset.add(a.ownerid);
                for(SVMXC__Service_Contract__c o:a.R00N70000001hzYIEAY__r) owneridset.add(o.ownerid);
            }
            List<user> userList = new List<user>();
            if(owneridset.size()>0) userList=[select SmallPhotoUrl from user where id in: owneridset];
            uidphoturlmap.putAll(userList);
            for(Account ac:Accs)
            {
                Boolean bIncludedInForecast = true;
                
                mData+='[{v:\''+ac.Id+'\',f:\'<div class="HAccount"><a title="'+System.Label.CL00122+'" href="/'+ac.Id+'">'+esc(ac.Name)+'</a></div><div class="HAmount"></div><div class="HOwner"><a title="'+System.Label.CLMAY13SRV08+'" href="/'+ac.OwnerId+'">'+esc(ac.owner.name)+'</a><img src="'+uidphoturlmap.get(ac.ownerid).SmallPhotoUrl+'" class="HPic"/></div><div class="CockpitFollow" id="follow1'+ac.id+'"></div>\'},\''+(ac.Id!=null?ac.Id:ac.Id)+'\',\''+esc(ac.Name)+'\'],';
         
                for(SVMXC__Service_Contract__c o:ac.R00N70000001hzYIEAY__r)
                {    
                   System.debug('**** asdf '+o);
                  System.debug('**** asdf '+o.Id);
                  System.debug('**** asdf '+o.Name);
                  System.debug('**** asdf '+o.SoldtoAccount__c);
                  System.debug('**** asdf '+o.SoldtoAccount__r.Name);
                  System.debug('**** asdf '+o.SVMXC__Contract_Price2__c);
                  System.debug('**** asdf '+UserInfo.getDefaultCurrency());
                  System.debug('**** asdf '+o.LeadingBusinessBU__c);
                  System.debug('**** asdf '+o.ContractType__c);
                  System.debug('**** asdf '+o.SVMXC__Start_Date__c);
                  System.debug('**** asdf '+o.SVMXC__End_Date__c);
                  System.debug('**** asdf '+o.SVMXC__Active__c);
                  System.debug('**** asdf '+o.owner.name);
                  System.debug('**** asdf '+o.ownerid);
                  System.debug('**** asdf '+o.ownerid);
                  System.debug('**** asdf '+ac.Id);
                  System.debug('**** asdf '+ac.Name);
                 
                  
                  
                    mData+='[{v:\''+o.Id+'\',f:\'<div class="HOppty"><a title="'+System.Label.CLMAY13SRV07+'" href="/'+o.Id+'">'+esc(o.Name)+'</a></div><div class="HAccount"><a title="'+System.Label.CL00122+'" href="/'+o.SoldtoAccount__c+'">'+esc(o.SoldtoAccount__r.Name)+'</a></div><div class="HAmount">'+(o.SVMXC__Contract_Price2__c!=null?((o.SVMXC__Contract_Price2__c/1000).intValue() +'k '+UserInfo.getDefaultCurrency()):' ')+'</div><div class="HLeadingBusiness">'+(o.LeadingBusinessBU__c!=null?((o.LeadingBusinessBU__c)):' ')+'</div><div class="HContracttype">'+(o.ContractType__c!=null?((o.ContractType__c)):' ')+'</div><div class="HStartDate">Start Date:'+o.SVMXC__Start_Date__c.format()+'</div><div class="HCloseDate">End Date:'+o.SVMXC__End_Date__c.format()+'</div><div><a class="HActions" title="'+(o.SVMXC__Active__c?esc('Active'):esc('InActive'))+'"><img id="pic'+o.id+'" src="'+esc(System.Label.CL00125)+'" class="'+(o.SVMXC__Active__c?'Hexclude':'Hinclude')+'"/></a></div><div class="HOwner"><a title="'+System.Label.CLMAY13SRV08+'" href="/'+o.OwnerId+'">'+esc(o.owner.name)+'</a><img src="'+uidphoturlmap.get(o.ownerid).SmallPhotoUrl+'" class="HPic"/></div><div class="CockpitFollow" id="follow1'+o.id+'"></div>\'},\''+ac.Id+'\',\''+esc(ac.Name)+'\'],';
                    lastSentenceJS+=bIncludedInForecast?'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:5px solid green;\');':'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:3px solid #888888;\');';
                    IntegerRowLine++;
                }
                lastSentenceJS+=bIncludedInForecast?'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:5px solid green;\');':'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:3px solid #888888;\');';
                IntegerRowLine++;
            }
            mData=mData.substring(0,mData.length()-1)+']);'+lastSentenceJS;
            
            system.debug('mData==='+mData);
        }
        //catch(Exception exc){ApexPages.addMessages(exc);}
    }

    private string esc(string s){return s!=null?String.escapeSingleQuotes(s):null;}
    public class BException extends Exception {} 
}