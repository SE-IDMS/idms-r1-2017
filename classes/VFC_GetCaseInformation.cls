/*-- ----------------------------------------------------------------------------------------------------------------------
    Author:Anil Budha
    Description: Get Case Information for Invensys (Enghouse).
--------------------------------------------------------------------------------------------------------------------------*/
global class VFC_GetCaseInformation{
    @RemoteAction
    global static List<Case> getCaseInfo(string strCseNo){
        List<Case>  lstCases;
        if(strCseNo!=null || strCseNo !=''){
            lstCases=[Select id,CaseNumber,ContactID,Contact.Name from Case where CaseNumber =:strCseNo]; 
        }
        return lstCases;  
    }    
    @RemoteAction
    global static List<CVCPInfo> getCVCPInfo(string strLegacyPIN){
        Set<Id> setContactIds = new Set<Id>();
        Set<Id> setContractIds = new Set<Id>();
        List<CVCPInfo> lstCVCPInfo = new List<CVCPInfo>();
        List<CTR_ValueChainPlayers__c>  lstCVCPS;
        if(!String.isBlank(System.Label.CLQ316PA045)) {
            String strContractIds = System.Label.CLQ316PA045;
            if(strContractIds.contains(',')) {
                List<String> parts = strContractIds.split(',');
                for(String conId : parts) {
                    setContractIds.add(conId);
                }
            }
            else{
                setContractIds.add(strContractIds);
            }
        }
        if(strLegacyPIN!=null || strLegacyPIN !=''){
            if(!Test.isRunningTest()) {
                lstCVCPS=[Select id,Name,LegacyPIN__c,Account__c,Contact__r.Name,contact__c from CTR_ValueChainPlayers__c where LegacyPIN__c =:strLegacyPIN and contract__r.ID IN : setContractIds];
            } else
            {
                lstCVCPS=[Select id,Name,LegacyPIN__c,Account__c,Contact__r.Name,contact__c from CTR_ValueChainPlayers__c where LegacyPIN__c =:strLegacyPIN];
            }
        }
        for(CTR_ValueChainPlayers__c CVCPObj : lstCVCPS){
            setContactIds.add(CVCPObj.Contact__c);
        }
        CVCPInfo objCVCPInfo = new CVCPInfo();
        if(setContactIds.size()==1){
           
           objCVCPInfo.contactId=lstCVCPS[0].contact__c;
           objCVCPInfo.accountId=lstCVCPS[0].Account__c;
           objCVCPInfo.cvcpCount=setContactIds.size();
        }
        else {
           objCVCPInfo.cvcpCount=setContactIds.size();
        }
        lstCVCPInfo.add(objCVCPInfo);
        System.debug(lstCVCPInfo);
        return lstCVCPInfo;
        
    }
    global class CVCPInfo{
        global Id contactId{get;set;}
        global Id accountId{get;set;}
        global Integer cvcpCount{get;set;}
        global string strError {get;set;}
    } 
}