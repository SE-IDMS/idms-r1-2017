/************************************************************
* Developer: Tomas Garcia E.                                *
* Type: Test Class                                          *
* Created Date: 14/04/2015                                  *
* Tested Class: FieloPRM_AP_CreateAccFieloTeamExt        *
************************************************************/

@isTest/*(SeeAllData=true)*/
public with sharing class FieloPRM_AP_CreateAccFieloTeamExtTest {
    
    public static testMethod void testUnit1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prmProgram = [SELECT Id FROM FieloEE__Program__c limit 1];
            
            prmProgram.Name = 'PRM Is On';
            
            update prmProgram;
            
            /*FieloEE__Program__c program = new FieloEE__Program__c (
                Name = 'PRM Is On', 
                FieloEE__SiteURL__c = 'https://www.test.com',
                FieloEE__RecentRewardsDays__c = 1
            );
            
            insert program;*/
            
            //account
             Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLPartnerLocatorFeatureGranted__c = true,
                City__c = 'test'
            );
            insert acc;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(acc);
            
            test.startTest();
            
            FieloPRM_AP_CreateAccFieloTeamExt thisExt = new FieloPRM_AP_CreateAccFieloTeamExt(sc);     
            thisExt.execute();
                  
            test.stopTest();
        }
    }
    

    
}