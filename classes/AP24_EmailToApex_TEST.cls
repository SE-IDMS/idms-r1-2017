@istest
private class AP24_EmailToApex_TEST {
   
    static testMethod void handleInboundEmail_TEST(){
        User systemInterfaceUser = new User(Id = System.Label.CLOCT14CCC24);
        System.runAs(systemInterfaceUser){
            String fromAddressKnownContactUSA   =   'contact-usa@hotmail.com';
      
            String customerCareUSAPOCEMailAddress   = 'poc.usacustomercare@bridge-fo.com';
      
            // create a new Email and envelope object
            
            Messaging.InboundEmail          objInboundEmail         =   new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope       objInboundEnvelope      =   new Messaging.InboundEnvelope();
            Messaging.InboundEmail.Header   objInboundEmailHeader   = new Messaging.InboundEmail.Header();
            Messaging.InboundEmail.Header   objInboundEmailHeader1   = new Messaging.InboundEmail.Header();
            objInboundEmail.headers=new Messaging.InboundEmail.Header[2];
            objInboundEmailHeader.name='Date';
            objInboundEmailHeader.value='Tue, 28 Apr 2009 14:08:37 -0700';
            objInboundEmail.headers[0]=objInboundEmailHeader;
            
            objInboundEmailHeader1.name='MessageId';
            objInboundEmailHeader1.value='4534JSDFJFSD343454';
            objInboundEmail.headers[1]=objInboundEmailHeader1;
            
            Country__c objPOCCountry = Utils_TestMethods.createCountry();
            objPOCCountry.Name='TestCountry2'; 
            objPOCCountry.CountryCode__c='IN';
            Database.insert(objPOCCountry);
            
            BusinessRiskEscalationEntity__c objPOCOrganization = Utils_TestMethods.createBRE('USAPOCEntity','USAPOCSubEntity','USAPOCLocation','Customer Care Center');
            Database.insert(objPOCOrganization);
            
            
            PointOfContact__c objUSAPOC = Utils_TestMethods.createPointOfContact(objPOCOrganization.Id, objPOCCountry.Id, customerCareUSAPOCEMailAddress,'Email');
            objUSAPOC.Priority1Routing__c = 'Account Preferred Team';
            Database.insert(objUSAPOC);
            
            AcknowledgmentTemplate__c  objACKTemplate = Utils_TestMethods.createAckTemplate(objUSAPOC.Id, 'ACKTEMPLATENAME_USA',objPOCCountry.Id,true,'EN');
            Database.insert(objACKTemplate);
            
            CustomerCareTeam__c AccountPreferredTeam = new CustomerCareTeam__c(CCCountry__c=objPOCCountry.ID, Name='Agent A Primary Team',LevelOfSupport__c='Primary');
            Database.insert(AccountPreferredTeam);
            
            Account objAccount = Utils_TestMethods.createAccount();
            objAccount.Preferred_CC_Team__c = AccountPreferredTeam.id;
            objAccount.Country__c=objPOCCountry.id;
            insert objAccount;
      

          
            Contact objUSAContact = Utils_TestMethods.createContact(objAccount.Id, 'TEST Contact EmailToApex 1');
            objUSAContact.Email = fromAddressKnownContactUSA;
            objUSAContact.CorrespLang__c = 'EN';
            insert objUSAContact;
      

            Test.startTest(); 
            
            // setup the data for the email
            objInboundEmail.Subject = 'Testinf Email To Case';
            objInboundEmail.fromname = 'Contact USA';
            objInboundEmail.fromAddress = fromAddressKnownContactUSA;
            objInboundEmail.ToAddresses = new List<String>();
            objInboundEmail.CCAddresses = new List<String>();
            objInboundEmail.plaintextbody =  Utils_TestMethods.generateRandomString(100);
      
            objInboundEmail.fromAddress = fromAddressKnownContactUSA;
            objInboundEmail.ToAddresses.add(customerCareUSAPOCEMailAddress);
        
                
            objInboundEnvelope.fromAddress = fromAddressKnownContactUSA;
            objInboundEnvelope.toAddress = customerCareUSAPOCEMailAddress;
     
            // add an attachment
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = blob.valueOf('my attachment text');
            attachment.fileName = 'textfile.txt';
            attachment.mimeTypeSubType = 'text/plain';
     
            Messaging.InboundEmail.BinaryAttachment attachment1 = new Messaging.InboundEmail.BinaryAttachment();
            attachment1.body = blob.valueOf('my attachment text');
            attachment1.mimeTypeSubType = 'text/plain';
            
            Messaging.InboundEmail.BinaryAttachment attachment2 = new Messaging.InboundEmail.BinaryAttachment();
            attachment2.body = blob.valueOf('my attachment text');
            attachment2.filename = 'myfile';
            attachment2.mimeTypeSubType = 'text/plain';
            
            objInboundEmail.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment,attachment1,attachment2 };
  
     
            // call the email service class and test it with the data in the testMethod
            AP24_EmailToApex objEmailToApex = new AP24_EmailToApex();
            objEmailToApex.isTest = True;
            objEmailToApex.handleInboundEmail(objInboundEmail, objInboundEnvelope);
            String strtext = AP24_EmailToApex.ConvertHTMLtoPlainText ('Test\r\n \t-'); 
            Test.stopTest();
        }
    }
     static testMethod void handleInboundEmailKeywordMatch_TEST(){
        User systemInterfaceUser = new User(Id = System.Label.CLOCT14CCC24);
        System.runAs(systemInterfaceUser){
            String fromAddressKnownContactUSA   =   'contact-usa@hotmail.com';
      
            String customerCareUSAPOCEMailAddress   = 'poc.usacustomercare@bridge-fo.com';
      
            // create a new Email and envelope object
            
            Messaging.InboundEmail          objInboundEmail         =   new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope       objInboundEnvelope      =   new Messaging.InboundEnvelope();
            Messaging.InboundEmail.Header   objInboundEmailHeader   = new Messaging.InboundEmail.Header();
            objInboundEmail.headers=new Messaging.InboundEmail.Header[1];
            objInboundEmailHeader.name='Date';
            objInboundEmailHeader.value='Tue, 28 Apr 2009 14:08:37 -0700';
            objInboundEmail.headers[0]=objInboundEmailHeader;
            
            //objInboundEmailHeader.name='MessageId';
            //objInboundEmailHeader.value='4534JSDFJFSD343454';
            //objInboundEmail.headers[1]=objInboundEmailHeader;
                        
            
            Country__c objPOCCountry = Utils_TestMethods.createCountry();
            objPOCCountry.Name='TestCountry2'; 
            objPOCCountry.CountryCode__c='IT';
            Database.insert(objPOCCountry);
            
            BusinessRiskEscalationEntity__c objPOCOrganization = Utils_TestMethods.createBRE('USAPOCEntity','USAPOCSubEntity','USAPOCLocation','Customer Care Center');
            Database.insert(objPOCOrganization);
            
            CaseClassification__c OrderAdminClassification = new CaseClassification__c(Category__c='3 - Order Admin');
            insert OrderAdminClassification;
        
            PointOfContact__c objUSAPOC = Utils_TestMethods.createPointOfContact(objPOCOrganization.Id, objPOCCountry.Id, customerCareUSAPOCEMailAddress,'Email');
            objUSAPOC.Priority2Routing__c = 'Keyword match';
            objUSAPOC.LevelOfExpertise__c ='Primary';
            Database.insert(objUSAPOC);
            
            EmailToCaseKeyword__c objKeywordMatch = new EmailToCaseKeyword__c(Name='TestKeyword', CaseCategory__c=OrderAdminClassification.ID,  Priority__c=1, CCCountry__c = objPOCCountry.ID, TotalMatch__c = 0,PointOfContact__c = objUSAPOC.Id);
            Database.insert(objKeywordMatch);
            
            AcknowledgmentTemplate__c  objACKTemplate = Utils_TestMethods.createAckTemplate(objUSAPOC.Id, 'ACKTEMPLATENAME_USA',objPOCCountry.Id,true,'EN');
            Database.insert(objACKTemplate);
            
            CustomerCareTeam__c AccountPreferredTeam = new CustomerCareTeam__c(CCCountry__c=objPOCCountry.ID, Name='Agent A Primary Team',LevelOfSupport__c='Primary');
            Database.insert(AccountPreferredTeam);
            
            Account objAccount = Utils_TestMethods.createAccount();
            objAccount.Preferred_CC_Team__c = AccountPreferredTeam.id;
            objAccount.Country__c=objPOCCountry.id;
            insert objAccount;
      

          
            Contact objUSAContact = Utils_TestMethods.createContact(objAccount.Id, 'TEST Contact EmailToApex 1');
            objUSAContact.Email = fromAddressKnownContactUSA;
            objUSAContact.CorrespLang__c = 'FR';
            insert objUSAContact;
      

            Test.startTest(); 
            
            // setup the data for the email
            objInboundEmail.Subject = 'My TestKeyword Testinf Email To Case';
            objInboundEmail.fromname = 'Contact USA';
            objInboundEmail.fromAddress = fromAddressKnownContactUSA;
            objInboundEmail.ToAddresses = new List<String>();
            objInboundEmail.CCAddresses = new List<String>();
            objInboundEmail.plaintextbody =  Utils_TestMethods.generateRandomString(100);
      
            
            // known contact 1: language FR
            // To email address: customer care center 1
            // should find custom setting 3
            objInboundEmail.fromAddress = fromAddressKnownContactUSA;
            objInboundEmail.CCAddresses.add(customerCareUSAPOCEMailAddress);
        
                
            //objInboundEnvelope.fromAddress = fromAddressKnownContactUSA;
            //objInboundEnvelope.toAddress = customerCareUSAPOCEMailAddress;
     
            // add an attachment
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = blob.valueOf('my attachment text');
            attachment.fileName = 'textfile.txt';
            attachment.mimeTypeSubType = 'text/plain';
     
            objInboundEmail.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
     
            // call the email service class and test it with the data in the testMethod
            AP24_EmailToApex objEmailToApex = new AP24_EmailToApex();
            objEmailToApex.isTest = True;
            objEmailToApex.handleInboundEmail(objInboundEmail, objInboundEnvelope);
            String strtext = AP24_EmailToApex.ConvertHTMLtoPlainText ('Test\r\n \t-'); 
            Test.stopTest();
        }
    }
    
    static testMethod void handleCCCAction_TEST(){
      
      Messaging.InboundEmail          objInboundEmail         =   new Messaging.InboundEmail() ;
      Messaging.InboundEnvelope       objInboundEnvelope      =   new Messaging.InboundEnvelope();
      Messaging.InboundEmail.Header   objInboundEmailHeader   = new Messaging.InboundEmail.Header();
      objInboundEmail.headers=new Messaging.InboundEmail.Header[1];
      objInboundEmailHeader.name='Date';
      objInboundEmailHeader.value='Tue, 28 Apr 2009 14:08:37 -0700';
      objInboundEmail.headers[0]=objInboundEmailHeader;

      Country__c objCountry = Utils_TestMethods.createCountry();
      insert objCountry;
      Account objAccount = Utils_TestMethods.createAccount();
      objAccount.Country__c = objcountry.id;      
      insert objAccount;

      Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
      objAccount.Country__c = objcountry.id;      
      insert objContact;

      Test.startTest();   
      
      List<Case> lstCase = new List<Case>(); 
      Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
      lstCase.add(objCase);
      
      Database.SaveResult[] dbInsertResultCase = Database.Insert(lstCase, false);
      System.debug('---->>Cases '+lstCase);
      if(dbInsertResultCase[0].IsSuccess()){
          System.debug('Case Inserted');
          Id caseId = dbInsertResultCase[0].getId();
          List<Case> lstCases = new List<Case>([Select Id, ThreadId__c from Case where Id =:caseId ]);
          System.debug('---->>lstCases '+lstCases);
          if(lstCases.size()>0){
              objInboundEmail.Subject = 'My Reply Email to Case ref:' + lstCases[0].ThreadId__c +':ref';
              objInboundEmail.fromname = 'Contact USA';
              objInboundEmail.fromAddress = 'vimal.karunakaran@schneider-electric.com';
              objInboundEmail.ToAddresses = new List<String>();
              objInboundEmail.CCAddresses = new List<String>();
              objInboundEmail.plaintextbody =  Utils_TestMethods.generateRandomString(100);
              objInboundEmail.fromAddress = 'vimal.karunakaran@schneider-electric.com';
              objInboundEmail.CCAddresses.add('cs.in@schneider-electric.com');
              AP24_EmailToApex objEmailToApex = new AP24_EmailToApex();
              objEmailToApex.isTest = True;
              objEmailToApex.handleInboundEmail(objInboundEmail, objInboundEnvelope);
          }
      }
      
      List<CCCAction__c> lstAction = new List<CCCAction__c>();

      CCCAction__c objAction1 = Utils_TestMethods.createCCCAction(objCase.Id);    
      lstAction.add(objAction1);
      
      Database.SaveResult[] dbInsertResult = Database.Insert(lstAction, false);
      if(dbInsertResult[0].IsSuccess()){
            System.debug('CCCAction Inserted');
            Id cccActionId = dbInsertResult[0].getId();
            List<CCCAction__c> lstCCCAction = new List<CCCAction__c>([Select Id, Status__c, ThreadId__c from CCCAction__c where Id =:cccActionId ]);
            System.debug('---->>LstCCCAction '+lstCCCAction);

            if(lstCCCAction.size()>0){
                  System.debug('SOQL Successful');
                  objInboundEmail.Subject = 'My Reply Email to CCC Action ref:' + lstCCCAction[0].ThreadId__c +':ref';
                  objInboundEmail.fromname = 'Contact USA';
                  objInboundEmail.fromAddress = 'vimal.karunakaran@schneider-electric.com';
                  objInboundEmail.ToAddresses = new List<String>();
                  objInboundEmail.CCAddresses = new List<String>();
                  objInboundEmail.plaintextbody =  Utils_TestMethods.generateRandomString(100);
                  objInboundEmail.fromAddress = 'vimal.karunakaran@schneider-electric.com';
                  objInboundEmail.CCAddresses.add('cs.in@schneider-electric.com');
                  AP24_EmailToApex objEmailToApex1 = new AP24_EmailToApex();
                  objEmailToApex1.isTest = True;
                  objEmailToApex1.handleInboundEmail(objInboundEmail, objInboundEnvelope);
            
                  System.debug('SOQL Successful');
                  objInboundEmail.Subject = 'My Reply Email to CCC Action ref:' + lstCCCAction[0].ThreadId__c +'+:ref';
                  objInboundEmail.fromname = 'Contact USA';
                  objInboundEmail.fromAddress = 'vimal.karunakaran@schneider-electric.com';
                  objInboundEmail.ToAddresses = new List<String>();
                  objInboundEmail.CCAddresses = new List<String>();
                  objInboundEmail.plaintextbody =  Utils_TestMethods.generateRandomString(100);
                  objInboundEmail.fromAddress = 'vimal.karunakaran@schneider-electric.com';
                  objInboundEmail.CCAddresses.add('cs.in@schneider-electric.com');
                  AP24_EmailToApex objEmailToApex2 = new AP24_EmailToApex();
                  objEmailToApex2.isTest = True;
                  objEmailToApex2.handleInboundEmail(objInboundEmail, objInboundEnvelope);
            }  
      
      }

      Test.stopTest();
         
    }
}