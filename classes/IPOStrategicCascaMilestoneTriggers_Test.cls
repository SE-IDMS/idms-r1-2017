@isTest 
private class IPOStrategicCascaMilestoneTriggers_Test{
    static testMethod void TestMethod1() {
    
    //+Ve case
   
         User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOcas2');
        System.runAs(runAsUser){
             IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
        
            
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
            IPO_Strategic_Deployment_Network__c C=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM');
            insert C;
            IPO_Strategic_Deployment_Network__c C1=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Business', Entity__c='ITB');
            insert C1;
            IPO_Strategic_Deployment_Network__c C2=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Operation Regions', Entity__c='NA');
            insert C2;
            IPO_Strategic_Milestone__c IPOmile1=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA');
            insert IPOmile1;  
            IPO_Strategic_Milestone__c IPOmile2=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA');
            insert IPOmile2; 
            IPO_Strategic_Milestone__c IPOmile3=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA');
            insert IPOmile3; 
            Test.startTest();
            IPO_Strategic_Cascading_Milestone__c IPOcas1= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile1.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Funtions',Entity__c='GM',Progress_Summary_Deployment_Leader_Q1__c='In Progress',Roadblocks_Q1__c='test',Progress_Summary_Deployment_Leader_Q4__c=null,Progress_Summary_Deployment_Leader_Q3__c='None',Progress_Summary_Deployment_Leader_Q2__c=null);
            insert IPOcas1;
            IPO_Strategic_Cascading_Milestone__c IPOcas2= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile2.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Business',Entity__c='ITB',Status_in_Quarter_Q2__c='test',Roadblocks_Q2__c='test',Progress_Summary_Deployment_Leader_Q2__c = 'In Progress',Progress_Summary_Deployment_Leader_Q1__c = null,Progress_Summary_Deployment_Leader_Q3__c = null,Progress_Summary_Deployment_Leader_Q4__c = null,Decision_Review_Outcomes_Q2__c='test');
            insert IPOcas2;
            IPO_Strategic_Cascading_Milestone__c IPOcas3= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile3.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Operation Regions',Entity__c='NA',Status_in_Quarter_Q2__c='test',Roadblocks_Q2__c='test',Progress_Summary_Deployment_Leader_Q2__c = 'In Progress',Decision_Review_Outcomes_Q2__c='test');
            insert IPOcas3;
            IPO_Strategic_Cascading_Milestone__c IPOcas4= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile3.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Operation Regions',Entity__c='NA',Status_in_Quarter_Q3__c='test',Roadblocks_Q3__c='test',Progress_Summary_Deployment_Leader_Q3__c = 'In Progress',Progress_Summary_Deployment_Leader_Q2__c = null,Progress_Summary_Deployment_Leader_Q4__c = null,Progress_Summary_Deployment_Leader_Q1__c = null,Decision_Review_Outcomes_Q3__c='test');
            insert IPOcas4;
            IPO_Strategic_Cascading_Milestone__c IPOcas5= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile3.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Operation Regions',Entity__c='NA',Status_in_Quarter_Q4__c='test',Roadblocks_Q4__c='test',Progress_Summary_Deployment_Leader_Q4__c = 'In Progress',Decision_Review_Outcomes_Q4__c='test');
            insert IPOcas5;
            
            
            IPOcas1.Scope__c='Global Business';
            IPOcas1.Entity__c='ITB';
            IPOcas1.Progress_Summary_Deployment_Leader_Q1__c = 'In Progress';
            IPOcas1.Status_in_Quarter_Q1__c='test1';
            IPOcas1.Roadblocks_Q1__c='test1';
            IPOcas1.Decision_Review_Outcomes_Q1__c='test1';
            update IPOcas1;
            
            IPOcas1.Progress_Summary_Deployment_Leader_Q2__c = 'In Progress';
            IPOcas1.Status_in_Quarter_Q2__c='test1';
            IPOcas1.Roadblocks_Q2__c='test1';
            IPOcas1.Decision_Review_Outcomes_Q2__c='test1';
            update IPOcas1;
            
            
            IPOcas1.Progress_Summary_Deployment_Leader_Q3__c = 'In Progress';
            IPOcas1.Status_in_Quarter_Q3__c='test1';
            IPOcas1.Roadblocks_Q3__c='test1';
            IPOcas1.Decision_Review_Outcomes_Q3__c='test1';
            update IPOcas1;
            
            IPOcas1.Progress_Summary_Deployment_Leader_Q4__c = 'In Progress';
            IPOcas1.Status_in_Quarter_Q4__c='test1';
            IPOcas1.Roadblocks_Q4__c='test1';
            IPOcas1.Decision_Review_Outcomes_Q4__c='test1';
            update IPOcas1;
            
            IPO_Strategic_Deployment_Network__c Ca=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Deployment_Leader__c =Label.Connect_Email,Scope__c='Global Functions', Entity__c='GM');
            insert Ca;
            IPO_Strategic_Deployment_Network__c Ca1=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Deployment_Leader__c =Label.Connect_Email,Scope__c='Global Business', Entity__c='ITB');
            insert Ca1;
            IPO_Strategic_Deployment_Network__c Ca2=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Deployment_Leader__c =Label.Connect_Email,Scope__c='Operation Regions', Entity__c='NA');
            insert Ca2;
            IPOcas1.Deployment_Leader__c=Label.Connect_Email;
            update IPOcas1;
            IPOcas2.Deployment_Leader__c=Label.Connect_Email;
            update IPOcas2;
            IPOcas2.Deployment_Leader__c=Label.Connect_Email;
            update IPOcas2;
            IPOcas1.Entity__c='HR';
            IPOcas2.Entity__c='power';
            IPOcas3.Entity__c='APAC';
            try{
            update IPOcas1;
            update IPOcas2;
            update IPOcas3;
            }catch(DmlException ta)
                {
                 ta.getMessage();
                }
            
            IPO_Strategic_Milestone__c IPOmile12=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile12');
            insert IPOmile12;
            IPO_Strategic_Cascading_Milestone__c IPOcas12= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile1.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Funtions',Entity__c='GM');
            try{
            insert IPOcas12;
            IPOcas12.Entity__c='GM';
            update IPOcas12;            
            }catch(DmlException a)
                {
                 a.getMessage();
                }
            Test.stopTest();
            }
            }
            }