/*
    Author          : Srinivas Nallapati
    Date Created    : 05/Mar/2012
    Description     : For scheduling weekly Problem digest and sending Weekly problem digest of public problems.
                        
*/
global class SCA01_ProblemWeeklyDigest implements Schedulable{
    
    //private String emailTo = System.Label.CLI2PMAR120005;
    //private String emailReplyTo = System.Label.CLI2PMAR120005;
    public Boolean bTestFlag = false;    //used in Test method on second invokation of the createEvents() method - ensures complete code coverage
    
    global void execute(SchedulableContext sc) {
        SendProblemDigest();
    }
    
    public void SendProblemDigest(){
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setToAddresses(emailTo.split(','));
            User usr = [select id, name from user where id =:System.Label.CLI2PMAR120010 limit 1];
            EmailTemplate et = [Select TemplateType, Subject, NamespacePrefix, Name, IsActive, Id, HtmlValue, FolderId, Body From EmailTemplate where name=:System.Label.CLI2PMAR120009 and iSactive = true limit 1];
            if(et!= null)
               mail.setTemplateId(et.id); 
            mail.setTargetObjectId(usr.id);
            mail.setSaveAsActivity(false);
    
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
            if(Test.isRunningTest() && bTestFlag)
                throw new TestException ();             //used during tests only to achieve coverage
            
        }Catch(Exception ex){
            System.debug('####### Following exception occured '+ex.getMessage());
        }                                                      
    }
    public class TestException extends Exception {}
}