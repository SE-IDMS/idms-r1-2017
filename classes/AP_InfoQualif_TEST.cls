@isTest
private class AP_InfoQualif_TEST
{
    private static Datetime current = Datetime.newInstance(2016, 1, 1, 1, 1, 1);

    @testSetup
    static void setup()
    {
        Country__c country = new Country__c(Name = 'fakeCountry', CountryCode__c = 'FR');
        insert country;

        Account fakeAccount1 = new Account(
            Name = 'fakeAccount1',
            Street__c = 'fakeStreet',
            ZipCode__c = '00000',
            Country__c = country.Id,
            ClassLevel1__c = 'VR'
        );
        insert fakeAccount1;

        InfoQualif__c iq = new InfoQualif__c(
            Account__c = fakeAccount1.Id,
            NameValidationDate__c = current,
            AddressValidationDate__c = current,
            PhoneValidationDate__c = current,
            SiretValidationDate__c = current,
            NafValidationDate__c = current,
            ClassLevel1ValidationDate__c = current,
            EmployeeSizeValidationDate__c = current,
            AnnualRevenueValidationDate__c = current,
            AnnualEquipmentAmountValidationDate__c = current,
            AnnualSchneiderAmountValidationDate__c = current,
            OpinionLeaderValidationDate__c = current,
            ContenderPurchaseValidationDate__c = current,
            RelationShipValidationDate__c = current,
            PriceSensitivityValidationDate__c = current,
            SchneiderPromoterValidationDate__c = current,
            ClassLevel2ValidationDate__c = current,
            ClassDetailValidationDate__c = current,
            CountCrewsValidationDate__c = current,
            CanalAchatValidationDate__c = current,
            PartElecValidationDate__c = current,
            TypeChantierValidationDate__c = current,
            PartRenovationValidationDate__c = current,
            EcoEnergieValidationDate__c = current,
            DecoValidationDate__c = current,
            CommandeValidationDate__c = current,
            RetraitValidationDate__c = current,
            DomaineValidationDate__c = current,
            LogementsValidationDate__c = current,
            TertiaireValidationDate__c = current,
            HotelValidationDate__c = current,
            SanteValidationDate__c = current,
            GammeValidationDate__c = current,
            AppareillageValidationDate__c = current,
            ConnectesValidationDate__c = current,
            TableauxComValidationDate__c = current,
            ComptageValidationDate__c = current,
            NombrePersonnesAvantVenteValidationDate__c = current,
            ProgrammePartenaireITBValidationDate__c = current,
            VenteAnnuelleServeursValidationDate__c = current,
            OnduleursValidationDate__c = current,
            RacksValidationDate__c = current,
            PDUsValidationDate__c = current,
            CommerciauxProduitsValidationDate__c = current,
            AutonomieVendreSchneiderValidationDate__c = current,
            PotentielCroissanceValidationDate__c = current,
            RisquePerteCAValidationDate__c = current,
            CompatibiliteOffreValidationDate__c = current,
            UltiParentAccValidationDate__c = current,
            PersonnelFixeValidationDate__c = current,
            NbChefsValidationDate__c = current,
            UniteDeporteeValidationDate__c = current,
            JDBValidationDate__c = current,
            MoyensTechniquesValidationDate__c = current,
            QualiteValidationDate__c = current,
            SegmentsValidationDate__c = current,
            TableauValidationDate__c = current,
            IndiceValidationDate__c = current,
            EnveloppesUniversellesValidationDate__c = current,
            CoffretsValidationDate__c = current,
            TGBT3200AValidationDate__c = current,
            PDMDistriValidationDate__c = current,
            PRMControleValidationDate__c = current,
            DOValidationDate__c = current,
            ChargesAffairesValidationDate__c = current,
            NbCableursValidationDate__c = current,
            MarketSegmentValidationDate__c = current,
            MarketSubSegmentValidationDate__c = current,
            SIAllianceValidationDate__c = current,
            PAMOneValidationDate__c = current,
            CiblageValidationDate__c = current,
            AdequationOffreValidationDate__c = current,
            MachinesValidationDate__c = current,
            DisAgencementValidationDate__c = current,
            DisDiffusionTvValidationDate__c = current,
            DisTypesClientsValidationDate__c = current,
            DisRexelTopValidationDate__c = current
        );
        insert iq;
    }

    @isTest
    static void checkCompletionCount()
    {
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = '';
        update me;

        Decimal completionCount = [SELECT CompletionCount2__c FROM InfoQualif__c LIMIT 1].CompletionCount2__c;

        System.assertEquals(16, completionCount);
    }

    @isTest
    static void checkCompletion()
    {
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = '';
        update me;

        Decimal completion = [SELECT Completion2__c FROM InfoQualif__c LIMIT 1].Completion2__c;

        System.assertEquals(100, completion);
    }

    @isTest
    static void checkLastValidationDate()
    {
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = '';
        update me;

        Datetime lastValidationDate = [SELECT LastValidationDate__c FROM InfoQualif__c LIMIT 1].LastValidationDate__c;

        System.debug(current);

        System.assertEquals(current, lastValidationDate);
    }

    @isTest
    static void checkElectricien()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'SC';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'PR';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(26, iq.CompletionCount2__c);

        iq.classLevel1Approb__c = 'Approuvé';
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(26, iq.CompletionCount2__c);

        acc.ClassLevel1__c = 'LC';
        update acc;

        iq.RetailClassDetail__c = 'Installateur major';
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(26, iq.CompletionCount2__c);

        me.UserBusinessUnit__c = 'EN';
        update me;

        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(16, iq.CompletionCount2__c);
    }

    @isTest
    static void checkPrescripteur()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];

        acc.ClassLevel1__c = 'SP';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'PR';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id  = :iq.Id];

        System.assertEquals(22, iq.CompletionCount2__c);

        iq.classLevel1Approb__c = 'Approuvé';
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(22, iq.CompletionCount2__c);

        acc.ClassLevel1__c = 'LC';
        update acc;

        iq.RetailClassDetail__c = 'CMIste (installateur)';
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(22, iq.CompletionCount2__c);

        me.UserBusinessUnit__c = 'EN';
        update me;

        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(16, iq.CompletionCount2__c);
    }

    @isTest
    static void checkItb()
    {
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'IT';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(27, iq.CompletionCount2__c);
    }

    @isTest
    static void checkDis()
    {
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'PR';
        update me;

        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.Name = 'Test';
        acc.ClassLevel1__c = 'WD';
        update acc;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(13, iq.CompletionCount2__c);

        acc.Name = 'REXEL';
        update acc;
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];
        System.assertEquals(14, iq.CompletionCount2__c);
    }

    @isTest
    static void checkPpebTableautiers()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'PB';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'BD';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(33, iq.CompletionCount2__c);
    }

    @isTest
    static void checkIndSystemIntegrators()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'SI';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'ID';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(22, iq.CompletionCount2__c);
    }

    @isTest
    static void checkIndTableautiers()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'PB';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'ID';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(22, iq.CompletionCount2__c);
    }

    @isTest
    static void checkIndOems()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'OM';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'ID';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        iq = [SELECT CompletionCount2__c FROM InfoQualif__c WHERE Id = :iq.Id];

        System.assertEquals(22, iq.CompletionCount2__c);
    }
}