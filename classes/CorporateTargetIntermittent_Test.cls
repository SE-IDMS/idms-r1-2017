/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 24-June-2013
    Modification Log    : 
    Description         : Test Method for Connect Intermittent Corporate Scorecard
*/

@isTest
private class CorporateTargetIntermittent_Test{
    static testMethod void CorporateTargetIntermittentTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connectM');     
       CorporateTargetIntermittent controller = new CorporateTargetIntermittent();
        System.runAs(runAsUser){
        controller.Redirect();
        }
       }
      }