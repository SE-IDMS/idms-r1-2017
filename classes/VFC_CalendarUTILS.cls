/**
    About
    -----
    Description: Shared utilities 
    Created for: SOW: 
    Create date: FEB 2013

    Details
    -------
    This class contains methods that cover
        - ByPass trigger 
        - Get Record Types for Task, .. 
        - Describe schema, fields and assigned object,  
        - Append List to Map
        - Get Salesforce with current instance
        - Get Custom Settings
    
    Update History
    --------------
    Feb 2013 / IC001/ Created by Maroun Imad
 
    Issues / TODOs
    --------------
*/

public class VFC_CalendarUTILS {
    
    public static final string bypassTrigger; //List of bypassed triggers
    
 


    /**
    * Check current user if is owner of record 
    * @param ownerId of any record from any object
    * @return boolean
    */
    public static boolean isRecordOwner(string ownerId){
        if (EMPTY(ownerId)) return false ;
        return (UserInfo.getUserId() == ownerId);

    }

    /**
    ** Check if current user has access to record
    ** @param recordId If of record in any object
    */
    public static boolean hasRecordAccess (String recordId) {
        UserRecordAccess a = [SELECT RecordId, HasReadAccess, HasEditAccess, HasAllAccess, MaxAccessLevel 
                              FROM UserRecordAccess
                              WHERE UserId = :UserInfo.getUserId()
                                AND RecordId = :recordId limit 1];
        if (VFC_CalendarUTILS.empty(a)) return false;
        return (a.HasEditAccess || a.HasAllAccess);
    } 
         
    /**
    * Method used for the class PAD
    * @param c object of type 
    * @return boolean
    */
    public static boolean canTrigger(string Name){
        return (bypassTrigger.indexof(';' + Name + ';') == -1);

    }
    
    /**** Describe Info ****/
    public static Map<String, Schema.SObjectType> globalDescibe_Map
    {
        get
        {
            if (globalDescibe_Map==null)
            {
                globalDescibe_Map = Schema.getGlobalDescribe();
            }
            return globalDescibe_Map;
        }
        set;
    }
    
    static map<String, Map<String, Schema.SObjectField>> objectField_Map = new map<String, Map<String, Schema.SObjectField>>(); // map: object API name >> <field API name, describe info>
    public static Map<String, Schema.SObjectField> getFieldMap(String typeName)
    {
        if (objectField_Map.containsKey(typeName))
        {
            return objectField_Map.get(typeName);
        } else if (globalDescibe_Map.containsKey(typeName))
        {
            Schema.SObjectType productSchema = VFC_CalendarUTILS.globalDescibe_Map.get(typeName);
            Map<String, Schema.SObjectField> fieldMap = productSchema.getDescribe().fields.getMap();
            objectField_Map.put(typeName,fieldMap);
            return fieldMap;
        }
        return null;
    }
    
    /**
    * Return List of Text fields in object
    * @param typeName object Name
    */
    static map<String, Map<String, Schema.SObjectField>> objectTextField_Map = new map<String, Map<String, Schema.SObjectField>>(); // map: object API name >> <field API name, describe info>
    public static Map<String, Schema.SObjectField> getTextFieldMap(String typeName)
    {
        if (objectTextField_Map.containsKey(typeName))
        {
            return objectTextField_Map.get(typeName);
        } else {
            Map<String, Schema.SObjectField> fieldsMap = getFieldMap(typeName);
            Map<String, Schema.SObjectField> textFieldsMap = new Map<String, Schema.SObjectField>();
            
            for (String field : fieldsMap.keySet()){
                Schema.DescribeFieldResult describeResult = fieldsMap.get(field).getDescribe();
                if(describeResult.getType()==Schema.DisplayType.TextArea || describeResult.getType()==Schema.DisplayType.String || describeResult.getName()=='Subject') {
                    if((!describeResult.isNameField() && describeResult.isAccessible()) || describeResult.getName()=='Subject') {
                        textFieldsMap.put(field, fieldsMap.get(field));
                    }
                }
            }
            objectTextField_Map.put(typeName,textFieldsMap);       
            return textFieldsMap;
        }
    }
        
    static map<Schema.SObjectField, Schema.DescribeFieldResult> objectFieldDescribe_Map = new map<Schema.SObjectField, Schema.DescribeFieldResult>(); // map: SObjectField >> DescribeFieldResult - used to reduce repeated describe calls in loops
    public static Schema.DescribeFieldResult getFieldDescribe(Schema.SObjectField field)
    {
        if (objectFieldDescribe_Map.containsKey(field))
        {
            return objectFieldDescribe_Map.get(field);
        } else if (field!=null)
        {
            Schema.DescribeFieldResult describeResult = field.getDescribe();
            objectFieldDescribe_Map.put(field,describeResult);
            return describeResult;
        }
        return null;
    }
    
    public static Schema.DescribeFieldResult getFieldDescribe(String typeName, String fieldName)
    {
        Map<String, Schema.SObjectField> fieldMap = getFieldMap(typeName);
        if (fieldMap.containsKey(fieldName)!=null) 
        {
            Schema.DescribeFieldResult fieldResult = getFieldDescribe(fieldMap.get(fieldName));
            if (fieldResult!=null) return fieldResult;
        }
        return null;
    }
    
    public static String getFieldLabel(String typeName, String fieldName)
    {
        Schema.DescribeFieldResult field = getFieldDescribe( typeName,  fieldName);
        if (field!=null) return field.getLabel();
        return '';
    }

    public static String getFieldDefaultValue(String typeName, String fieldName)
    {
        Schema.DescribeFieldResult field = getFieldDescribe( typeName,  fieldName);
        if (field!=null) return String.valueOf(field.getDefaultValueFormula());
        return '';
    }    
    
    /**** Maps ****/
    public static Map<Object,Object[]> mapListAppend(Map<Object,Object[]> m, Object key, Object value)
    {
        Object[] theList = new Object[]{};
        if (m.containsKey(key))
        {
            theList = m.get(key);
        }
        theList.add(value);
        m.put(key, theList);
        return m;
    }
    

    /**** Profiles ****/
    static String saProfile; // Profile for SA
    public static String getSAProfile()
    {
        if (VFC_CalendarUTILS.empty(saProfile)) 
        {
            saProfile = [Select Id from Profile where Name='ICON_SA'].Id;
        }
        return saProfile;
    }

    static String saManagerProfile; // Profile for SA Manager
    public static String getSAManagerProfile()
    {
        if (VFC_CalendarUTILS.empty(saManagerProfile)) 
        {
            saManagerProfile = [Select Id from Profile where Name='ICON_SA Manager'].Id;
        }
        return saManagerProfile;
    }
    
    static String corporateProfile; // Profile for CorporateProfile manager of SA and SA Manager
    public static String getCorporateProfile()
    {
        if (VFC_CalendarUTILS.empty(corporateProfile)) 
        {
            corporateProfile = [Select Id from Profile where Name='ICON_Corporate'].Id;
        }
        return corporateProfile;
    }

    static String dreamProfile; // Interface Dream profile
    public static String getDreamProfile()
    {
        if (VFC_CalendarUTILS.empty(dreamProfile)) 
        {
            dreamProfile = [Select Id from Profile where Name='ICON_Interface DREAM'].Id;
        }
        return dreamProfile;
    }

    static String lvAdminProfile; // Interface Dream profile
    public static String getLVAdminProfile()
    {
        if (VFC_CalendarUTILS.empty(lvAdminProfile)) 
        {
            lvAdminProfile = [Select Id from Profile where Name='ICON_LV Administrator'].Id;
        }
        return lvAdminProfile;
    }
    
    /**** Check connected user profile ****/
    public static Boolean isSA()
    {
        return (getSAProfile() ==  UserInfo.getProfileId());
    }

    public static Boolean isSAManager()
    {
        return (getSAManagerProfile() ==  UserInfo.getProfileId());
    }
    
    public static Boolean isCorporate()
    {
        return (getCorporateProfile() ==  UserInfo.getProfileId());
    }

    public static Boolean isInterfaceDream()
    {
        return (getDreamProfile() ==  UserInfo.getProfileId());
    }
    
    public static Boolean isLVAdmin()
    {
        return (getLVAdminProfile() ==  UserInfo.getProfileId());
    }
       
    /**** Record Types ****/
    static String taskRT; // RT for exchange rates set up by users for their own use
    public static String getTaskRT()
    {
        if (VFC_CalendarUTILS.empty(taskRT)) 
        {
            taskRT = [Select Id from RecordType where DeveloperName='TSK_To_Do' and sObjectType = 'TASK'].Id;
        }
        return taskRT;
    }

    static String taskDreamRT; // RT for exchange rates set up by users for their own use
    public static String getTaskDreamRT()
    {
        if (VFC_CalendarUTILS.empty(taskDreamRT)) 
        {
            taskDreamRT = [Select Id from RecordType where DeveloperName='DREAMContact' and sObjectType = 'TASK'].Id;
        }
        return taskDreamRT;
    }
    
    /**** URL ****/
    public static String sfInstance {
        public get {
            if (sfInstance == null) {
                //
                // Possible Scenarios:
                //
                // (1) ion--test1--nexus.cs0.visual.force.com  --- 5 parts, Instance is 2nd part
                // (2) na12.salesforce.com      --- 3 parts, Instance is 1st part
                // (3) ion.my.salesforce.com    --- 4 parts, Instance is not determinable
    
                // Split up the hostname using the period as a delimiter
                List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
                if (parts.size() == 3) sfInstance = parts[0];
                else if (parts.size() == 5) sfInstance = parts[1];
                else sfInstance = null;
            } return sfInstance;
        } private set;
    }
    
    // And you can then get the Salesforce base URL like this:
    public static String baseURL() 
    {
         return 'https://' + sfInstance + '.salesforce.com';
    }
   
    public static Map<String,String> getURLParameters(String url) 
    {
        Map<String,String> paramsMap = new Map<String,String>();
        if (empty(url)) return paramsMap;
        String[] params = url.substring(url.indexof('?')+1).split('&');
        for (String param  : params) {
            String[] paramValue = param.split('=');
            if (paramValue.size() < 2) paramsMap.put(paramValue[0], '');
            else paramsMap.put(paramValue[0], paramValue[1]); 
        }
        return paramsMap;
    }
    
    /**** Lists ****/
    //Convert a List to a Set
    public static Set<String> toSet(List<String> l)
    {
        Set<String> s = new Set<String>();
        s.addAll(l);
        return s;
    }

    // Return a set from an object list  of a field
    public static set<String> getIdList(List<Sobject> items, String fieldName)
    {
       Set<String> s = new Set<String>();
   
       for(Sobject ob : items)
       {
            s.add((string)ob.get(fieldName));
       }
       return s;         
    }  
        
    // Convert a mutiselect staring of values into a string list
    public static String[] deserializeMultiSelect(String mutiSelectList)
    {
        return mutiSelectList!=null ? mutiSelectList.split(';') : new String[]{};
    }
    
    // Convert a string list or set or mutiselect picklist string into a CSV set
    public static String serializeWithQuotes(String[] aList)
    {
        String csv = '';
        for (String s:aList)
        {
            csv += '\''+s+'\',';
        }
        csv = csv.removeEnd(',');
        return csv;
    }
    public static String serializeWithQuotes(String mutiSelectList)
    {
        String[] aList = mutiSelectList!=null ? mutiSelectList.split(';') : new String[]{};
        return serializeWithQuotes(aList);
    }
    public static String serializeWithQuotes(Set<String> aSet)
    {
        String[] aList = new String[]{};
        aList.addAll(aSet);
        return serializeWithQuotes(aList);
    }
       
    //Sort sObjects (alt do this in dynamic soql if using soql based pagination)
    public static void orderList(List<Sobject> items, String sortField, String theOrder)
    {
        theOrder = theOrder.toLowerCase();  
       List<Sobject> resultList = new List<Sobject>();
   
        //Create a map that can be used for sorting 
       Map<object, List<Sobject>> objectMap = new Map<object, List<Sobject>>();
       
       for(Sobject ob : items)
       {
            if(objectMap.get(ob.get(sortField)) == null)
            {
                objectMap.put(ob.get(sortField), new List<Sobject>()); 
            }
            objectMap.get(ob.get(sortField)).add(ob);
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
       
        for(object key : keys){ 
            resultList.addAll(objectMap.get(key)); 
        }
       
        //Apply the sorted values to the source list
        items.clear();
        if(theOrder == 'asc')
        {
            for(Sobject ob : resultList)
            {
                items.add(ob); 
            }
        }
        else if(theOrder == 'desc')
        {
            for(integer i = resultList.size()-1; i >= 0; i--)
            {
                items.add(resultList[i]);  
            }
        }
    }  
    
    /**** Math ****/
    public static Decimal round(decimal aNumber, integer places)
    {
        return aNumber.setScale(places,System.RoundingMode.HALF_UP);
    }
    
    /**** Generic shortcut methods ****/
    public static Boolean empty(Object o)
    {
        return o==null || String.valueOf(o)=='';
    }
    public static Boolean empty(Integer o)
    {
        return o==null || o==0;
    }
    public static Boolean empty(Decimal o)
    {
        return o==null || o==0;
    }

    
 
 
     
     
    /**  Returns List of picklist Values  **/
    
    public static List<String> getPickListVal(String obj, String fld, String fldEx) {
        
      System.debug ('*** Start to get Picklist Values on ' + obj + '/'+ fld);
      List<String> options = new List<String>();            
      // Get the object type of the SObject.
      SobjectType objType = Schema.getGlobalDescribe().get(obj);
      //Schema.sObjectType objType = sobj.getSObjectType(); 
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fld).getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a : values)
      { 
        if (a.getValue()!=fldEx){
         options.add(a.getValue());
        } 
      }
      System.debug ('***  options size=' + options.size());
      System.debug ('*** End to get Picklist Values ');
      return options;
    }
    
}