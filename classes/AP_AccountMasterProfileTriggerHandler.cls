//handler for Account Master profile triggers
Public class AP_AccountMasterProfileTriggerHandler
{
    public static void updateamp(List<accountMasterProfile__c> ampList){
        Map<id,account> accMap=new map<id,account>();
        List<CAPCountryWeight__c> countryWeightlist=[Select ClassificationLevel1__c,Country__c,CountryWeight__c,MarketSegment__c,QuestionSequence__c from CAPCountryWeight__c ORDER BY QuestionSequence__c ASC];
        List<CAPGlobalMasterData__c> globalWeightlist=[Select ClassificationLevel1__c,Weight__c,QuestionSequence__c from CAPGlobalMasterData__c ORDER BY QuestionSequence__c ASC];
        List<id> accidList=new List<id>();
        List<CAPCountryWeight__c> ampcountryWeightlist;
        List<CAPGlobalMasterData__c> ampglobalWeightlist;
        List<CAPGlobalMasterData__c> ampDefaultglobalWeightlist;
        Map<String,List<CAPCountryWeight__c>> countryWeightMap=new Map<String,List<CAPCountryWeight__c>>();
        Map<String,List<CAPGlobalMasterData__c>> globalWeightMap=new Map<String,List<CAPGlobalMasterData__c >>();
        
        //Country weight
        for(CAPCountryWeight__c cwt:countryWeightlist){            
            String ky=cwt.Country__c+'_'+cwt.ClassificationLevel1__c+'_'+cwt.MarketSegment__c;//marketsegment may or not have value
            if(countryWeightMap.containskey(ky))
                countryWeightMap.get(ky).add(cwt);
            else
                countryWeightMap.put(ky,new List<CAPCountryWeight__c>{(cwt)});
        }
        
        //Global weight
        for(CAPGlobalMasterData__c cwt:globalWeightlist){
            if(globalWeightMap.containskey(cwt.ClassificationLevel1__c))
                globalWeightMap.get(cwt.ClassificationLevel1__c).add(cwt);
            else
                globalWeightMap.put(cwt.ClassificationLevel1__c,new List<CAPGlobalMasterData__c >{(cwt)});
        }
        
        for(AccountMasterProfile__c amp:ampList)
            accidList.add(amp.account__c);
            
        for(Account a:[select id,AccountMasterProfileLastUpdated__c,TotalPAM__c,AccountMasterProfile__c,ClassLevel1__c,Country__c,MarketSegment__c from account where id in :accidList])
            accMap.put(a.id,a); 
                              
        for(AccountMasterProfile__c amp:ampList){            
            amp.Q1CriteriaWeight__c=0;
            amp.Q2CriteriaWeight__c=0;
            amp.Q3CriteriaWeight__c=0;
            amp.Q4CriteriaWeight__c=0;
            amp.Q5CriteriaWeight__c=0;
            amp.Q6CriteriaWeight__c=0;
            amp.Q7CriteriaWeight__c=0;
            amp.Q8CriteriaWeight__c=0;
            ampcountryWeightlist=new List<CAPCountryWeight__c>();
            ampglobalWeightlist=new List<CAPGlobalMasterData__c>();
            ampDefaultglobalWeightlist=new List<CAPGlobalMasterData__c>();
            
            String ky=accMap.get(amp.Account__c).Country__c+'_'+accMap.get(amp.Account__c).ClassLevel1__c+'_'+accMap.get(amp.Account__c).MarketSegment__c;//first preference with market segment key
            if(!countryWeightMap.ContainsKey(ky))
                 ky=accMap.get(amp.Account__c).Country__c+'_'+accMap.get(amp.Account__c).ClassLevel1__c+'_'+null; // without market segment fetch the value
            System.debug('*****accMap '+accMap+'>>>>>ky '+ky);
            if(countryWeightMap.ContainsKey(ky))
                ampcountryWeightlist=countryWeightMap.get(ky);

            if(globalWeightMap.ContainsKey(accMap.get(amp.Account__c).ClassLevel1__c))
                ampglobalWeightlist=globalWeightMap.get(accMap.get(amp.Account__c).ClassLevel1__c);// global weight based on classification
           if(globalWeightMap.ContainsKey('Default'))
                ampDefaultglobalWeightlist=globalWeightMap.get('Default'); //Default global weight
            if(ampcountryWeightlist.size()==8){// Country weight
                amp.Q1CriteriaWeight__c=ampcountryWeightlist[0].CountryWeight__c;
                amp.Q2CriteriaWeight__c=ampcountryWeightlist[1].CountryWeight__c;
                amp.Q3CriteriaWeight__c=ampcountryWeightlist[2].CountryWeight__c;
                amp.Q4CriteriaWeight__c=ampcountryWeightlist[3].CountryWeight__c;
                amp.Q5CriteriaWeight__c=ampcountryWeightlist[4].CountryWeight__c;
                amp.Q6CriteriaWeight__c=ampcountryWeightlist[5].CountryWeight__c;
                amp.Q7CriteriaWeight__c=ampcountryWeightlist[6].CountryWeight__c;
                amp.Q8CriteriaWeight__c=ampcountryWeightlist[7].CountryWeight__c;
            }
            else if(ampglobalWeightlist.size()==8){//Global weight based on classification
                amp.Q1CriteriaWeight__c=ampglobalWeightlist[0].Weight__c;
                amp.Q2CriteriaWeight__c=ampglobalWeightlist[1].Weight__c;
                amp.Q3CriteriaWeight__c=ampglobalWeightlist[2].Weight__c;
                amp.Q4CriteriaWeight__c=ampglobalWeightlist[3].Weight__c;
                amp.Q5CriteriaWeight__c=ampglobalWeightlist[4].Weight__c;
                amp.Q6CriteriaWeight__c=ampglobalWeightlist[5].Weight__c;
                amp.Q7CriteriaWeight__c=ampglobalWeightlist[6].Weight__c;
                amp.Q8CriteriaWeight__c=ampglobalWeightlist[7].Weight__c;
            }
            else if(ampDefaultglobalWeightlist.size()==8){ // Default weight
                amp.Q1CriteriaWeight__c=ampDefaultglobalWeightlist[0].Weight__c;
                amp.Q2CriteriaWeight__c=ampDefaultglobalWeightlist[1].Weight__c;
                amp.Q3CriteriaWeight__c=ampDefaultglobalWeightlist[2].Weight__c;
                amp.Q4CriteriaWeight__c=ampDefaultglobalWeightlist[3].Weight__c;
                amp.Q5CriteriaWeight__c=ampDefaultglobalWeightlist[4].Weight__c;
                amp.Q6CriteriaWeight__c=ampDefaultglobalWeightlist[5].Weight__c;
                amp.Q7CriteriaWeight__c=ampDefaultglobalWeightlist[6].Weight__c;
                amp.Q8CriteriaWeight__c=ampDefaultglobalWeightlist[7].Weight__c;
            }
            if(amp.Q1Rating__c == null || amp.Q2Rating__c == null || amp.Q3Rating__c == null || amp.Q4Rating__c == null || amp.Q5Rating__c == null || amp.Q6Rating__c == null || amp.Q7Rating__c == null || amp.Q8Rating__c == null )
                amp.score__c = null;
            else if(amp.Q1CriteriaWeight__c+amp.Q2CriteriaWeight__c+amp.Q3CriteriaWeight__c+amp.Q4CriteriaWeight__c+amp.Q5CriteriaWeight__c+amp.Q6CriteriaWeight__c+amp.Q7CriteriaWeight__c+amp.Q8CriteriaWeight__c!=0 && amp.Q1CriteriaWeight__c+amp.Q2CriteriaWeight__c+amp.Q3CriteriaWeight__c+amp.Q4CriteriaWeight__c+amp.Q5CriteriaWeight__c+amp.Q6CriteriaWeight__c+amp.Q7CriteriaWeight__c+amp.Q8CriteriaWeight__c!=null)
                amp.Score__c=(((amp.Q1CriteriaWeight__c*(Integer.ValueOf(amp.Q1Rating__c)-1)+amp.Q2CriteriaWeight__c*(Integer.ValueOf(amp.Q2Rating__c)-1)+amp.Q3CriteriaWeight__c*(Integer.ValueOf(amp.Q3Rating__c)-1)+amp.Q4CriteriaWeight__c*(Integer.ValueOf(amp.Q4Rating__c)-1)+amp.Q5CriteriaWeight__c*(Integer.ValueOf(amp.Q5Rating__c)-1)+amp.Q6CriteriaWeight__c*(Integer.ValueOf(amp.Q6Rating__c)-1)+amp.Q7CriteriaWeight__c*(Integer.ValueOf(amp.Q7Rating__c)-1)+amp.Q8CriteriaWeight__c*(Integer.ValueOf(amp.Q8Rating__c)-1))*100)/((amp.Q1CriteriaWeight__c+amp.Q2CriteriaWeight__c+amp.Q3CriteriaWeight__c+amp.Q4CriteriaWeight__c+amp.Q5CriteriaWeight__c+amp.Q6CriteriaWeight__c+amp.Q7CriteriaWeight__c+amp.Q8CriteriaWeight__c)*3)).round(System.RoundingMode.HALF_UP); 
            amp.TECH_UniqueAccount__c=amp.Account__c;
        }
    }
}