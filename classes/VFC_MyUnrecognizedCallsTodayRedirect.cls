public class VFC_MyUnrecognizedCallsTodayRedirect {
    //This method will redirect user to the My Unreconized Calls Today report
    public PageReference myUnrecognizedCallsTodayRedirect(){  
        //Redirect to My Calls Today report
        string Id= System.Label.CLOCT15CCC15;//'00Oq0000000K9r9';
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        PageReference referenceURL = new PageReference(baseURL + '/' +Id);//Define the My Calls Today Report Id in Custom Label
        referenceURL .setRedirect(true);
        return referenceURL ; 
    }
}