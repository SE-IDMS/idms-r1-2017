/********************************************************************************************************
    Author: Fielo Team
    Date: 11/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestMoveMembersByAccount/*')
global class FieloPRM_REST_MoveMembersByAccount{   

    /**
    * [postMoveMemberByContact ]
    * @method   postMoveMemberByContact
    * @Pre-conditions  
    * @Post-conditions 
    * @return   String    []
    */
    @HttpPost
    global static String postMoveMembersByAccount(String accountIdFrom, String accountIdTo){    

        return FieloPRM_UTILS_Member.postMoveMembersByAccount(accountIdFrom,accountIdTo);
   
    }   
}