/*
    Author          : Bhuvana Subramaniyan    
    Description     : Controller for Product Line Mass Edit page  
*/
public with sharing class  VFC_OppLineMassEdit extends VFC_ControllerBase
{
    public class PLContainer
    {
        public OPP_ProductLine__c PL {get; set;}
        public String UpdateStatus {get; set;}
        public String Link {get; set;}
        public String ErrorMessage {get; set;}
        
        public PLContainer(OPP_ProductLine__c PL)
        {
            this.PL = PL;
            this.UpdateStatus = '';
            this.ErrorMessage = '';
            this.Link ='/'+this.PL.id;  
        }    
    }
    
    public List<OPP_ProductLine__c> PLList {get; set;}
    public boolean displayPage {get;set;}
    
    public List<OPP_ProductLine__c> RetrievePLList(List<OPP_ProductLine__c> PLList)
    {
        List<ID> PLIdList = new List<ID>();
        List<OPP_ProductLine__c> RetrievedPLList = new List<OPP_ProductLine__c>();
        if(PLList.size() > 200)
        {
            this.DisplayPage = false;
        }
        else
        {
            this.DisplayPage = true;

            for(OPP_ProductLine__c PL : PLList)
            {
                PLIdList.add(PL.id);
            } 
            RetrievedPLList = [SELECT name, LineStatus__c, LineCloseDate__c, Opportunity__c, LineType__c, ParentLine__c,SynergyWith__c, SynergyLever__c,
            RevenueOverride__c, Probability__c,  IncludeinQuote__c,Amount__c, LineAmount__c,DeliveryDate__c,ConfigurationID__c,
            IncludedInForecast__c, Quantity__c,  TECH_CommercialReference__c,SalesOrderNumber__c, PurchaseOrderNumber__c, ProductBU__c, Specified__c
            FROM OPP_ProductLine__c WHERE ID IN: PLIdList];
            
        }
        return RetrievedPLList;
    }
    
    public Map<Id, PLContainer> PLId_PLWrap {get; set;}
    public List<PLContainer> PLWrapList {get; set;}
    
    public VFC_OppLineMassEdit (ApexPages.StandardSetController PLStandardSetController)
    {
        this.PLList = (List<OPP_ProductLine__c>) PLStandardSetController.getSelected();        
        this.PLWrapList = new List<PLContainer>();
        this.PLId_PLWrap = new Map<Id, PLContainer>();
        this.DisplayPage = false;

        this.PLList = RetrievePLList(this.PLList);
        for (OPP_ProductLine__c PL : this.PLList)
        {
            PLContainer PLWrap = new PLContainer(PL);
            this.PLWrapList.add(PLWrap);
            this.PLId_PLWrap.put(PL.id, PLWrap);
        }        
    }
    
    public void CustomSave()
    {
        List<OPP_ProductLine__c> PLListTmp = new List<OPP_ProductLine__c>();
        for (PLContainer PLWrap : this.PLWrapList ){
            PLWrap.UpdateStatus = '';
            PLListTmp.add(PLWrap.PL);
        }
        List<Database.SaveResult> SPList = database.update(PLListTmp , false);
        this.PLList = RetrievePLList(this.PLList);
        integer i = 0; // element # in the result list = PL # in the PLWrapList
        while(i<SPList.size())
        {
            PLContainer PLWrap = this.PLId_PLWrap.get(this.PLList[i].Id);
            if (SPList[i].getErrors().size()>0)
            {
                for(Database.Error err: SPList[i].getErrors())
                { 
                    PLWrap.ErrorMessage = err.getMessage();
                    system.debug('########## error: ' + err);
                    PLWrap.UpdateStatus = Label.CLOCT13SLS18;
                    PLWrap.PL = this.PLList[i];
                } 
            }
            else
            {
                PLWrap.UpdateStatus = Label.CL00267;
                PLWrap.ErrorMessage = Label.CLOCT13SLS19;
                PLWrap.PL = this.PLList[i];
            }         
            i++;
        }
    }    
}