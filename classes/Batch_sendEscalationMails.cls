global class Batch_sendEscalationMails implements Database.Batchable<sObject>{

   String query;
   String objectName;
   String objtlookup;
   String owner;
   String duedate;
   String accOrg;
   

   global Iterable<sObject> start(Database.BatchableContext BC){
      return new Iterator_sendEscalationMailsIterator();
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<sObject> currList=new List<sObject>();
        Schema.SObjectType currObjType=null;
        for (sObject currObj:scope){
            if (currObj!=null){
                if (currObjType==null) {
                    currObjType=currObj.getSObjectType();
                }
                if (currObj.getSObjectType()==currObjType) {
                    currList.add(currObj);

                } else {
                    setVars(currObjType);
                    AP_sendEscalationMailsBatch.createXAStakeholderonSH(currList.clone(),objtlookup,owner,duedate,accOrg);

                    currList=new List<sObject>();
                    currList.add(currObj);
                    currObjType=currObj.getSObjectType();
                }
            }
        }
            if (currList!=null && currList.size()>0){

                setVars(currObjType);
                    AP_sendEscalationMailsBatch.createXAStakeholderonSH(currList.clone(),objtlookup,owner,duedate,accOrg);
            }
    }
    
    global void setVars(Schema.SObjectType currObjType){
                    if (currObjType.getDescribe().getLocalName().equalsIgnoreCase('ComplaintRequest__c')) {
                        objtlookup='ComplaintRequest__c';
                        owner='IssueOwner__c';
                        duedate='TECH_DueDateForNotifications__c';
                        accOrg='AccountableOrganization__c';
                    } else if (currObjType.getDescribe().getLocalName().equalsIgnoreCase('ContainmentAction__c')) {
                        objtlookup='XA__c';
                        owner='Owner__c';
                        duedate='DueDate__c';
                        accOrg='AccountableOrganization__c';
                    } else if (currObjType.getDescribe().getLocalName().equalsIgnoreCase('CorrectiveAction__c')) {
                        objtlookup='CA__c';
                        owner='Owner__c';
                        duedate='DueDate__c';
                        accOrg='AccountableOrganization__c';
                    } else if (currObjType.getDescribe().getLocalName().equalsIgnoreCase('PreventiveAction__c')) {
                        objtlookup='PA__c';
                        owner='Owner__c';
                        duedate='DueDate__c';
                        accOrg='AccountableOrganization__c';
                    }
    }

   global void finish(Database.BatchableContext BC){
   }
}