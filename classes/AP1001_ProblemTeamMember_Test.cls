/*
    Author          : Global Delivery Team - Bangalore
    Date Created    : 26/05/2011
    Description     : Test class for AP1001_ProblemTeamMember class.
*/
@isTest
private class AP1001_ProblemTeamMember_Test {

    static testMethod void testProblemTeamMember() {
     
    User newUser1 = Utils_TestMethods.createStandardUser('TestTm1');    
    newUser1.Company_Phone_Number__c='9611320932';    
    Database.insert(newUser1);  
    
    User newUser2 = Utils_TestMethods.createStandardUser('TestTm2');  
    newUser2.Company_Phone_Number__c='9611320932';         
    Database.insert(newUser2); 
    
    BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
    breEntity.Name='Test Name123';
            breEntity.Entity__c='Test Entity123';
    Database.insert(breEntity);
    
    Problem__c prblm = Utils_TestMethods.createProblem(breEntity.id); 
 insert Prblm;
    
    ProblemTeamMember__c prbTm = Utils_TestMethods.createProblemTeamMember(Prblm.Id,newUser1.Id);
  insert prbTm;
    
    ProblemTeamMember__c prbTm1 = [Select User__c from ProblemTeamMember__c where Id =: prbTm.Id];    
    System.assertEquals(newuser1.id,prbTm1.User__c);
    
    prbTm1.User__c = newUser2.Id;
    Database.update(prbTm1);
    
    ProblemTeamMember__c prbTm2 = [Select User__c from ProblemTeamMember__c where Id =: prbTm.Id];    
   // System.assertEquals(newuser2.id,prbTm2.User__c);   
    
    Database.delete(prbTm2);
    
    }
}