/*
    Author          : Accenture IDC Team 
    Date Created    : 26/07/2011
    Description     : Class utilised by platform scoring completed Button.
*/
/*
Modification History:
---------------------
Date: 2/11/2011
Author: Siddharatha Nagavarapu(Global Delivery Team)
Purpose: UAT imporvement for the DEF- 111
*/    
public class VFC53_PlatformingScoringsCompleted {

public boolean flag{get;set;}
public boolean confirmFlag{get;set;}{confirmFlag=false;}
public String capId;
    public VFC53_PlatformingScoringsCompleted(ApexPages.StandardController controller) {        
    capId= controller.getId();
    }
/*
Method Name:checkAsignee
Modified by:Siddharatha Nagavarapu(Global Delivery)
purpose:This method checks if the user who is trying to access this page is the assigned user in the cap record , if yes then a conformation message is displayed
else an error is displayed requesting the user to get back to the record detail
*/
    public pagereference checkAsignee()
    {    
    system.debug('checkAsignee method Begins');
    SFE_IndivCAP__c cap= new SFE_IndivCAP__c();
        if(capId!=null)
        {
            cap= [select assignedTo__c,PlatformingScoringsCompleted__c from SFE_IndivCAP__c where id =:capId];
        }
        if(cap.assignedTo__c != userInfo.getUserId())
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00451)); 
            flag=true;
            return null;
        }
        //dec-11 release addition(start)
        else
        {            
            return displayConfirmationMessage();          //the existing code is pushed to a new function updateCap and the control is transferred to displayConformationMessage function 
        }    
       //dec-11 release addition(end)
    }

    /*
    Method Name: displayConfirmationMessage
    Purpose:The confirmation message should be dislayed to the user to submit the platform scorings, This method populates the message.    
    Author:Siddharatha Nagavarapu(Global Delivery Team)
    */
    public pageReference displayConfirmationMessage()
    {
    confirmflag=true;
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CL00638)); 
    return null;
    }  
    
   /*
    Method Name: updateCAP
    Purpose:The existing logic from this class is moved to this new function.This function updates the platform scorings field of the selected CAP record.
    Author:Siddharatha Nagavarapu(Global Delivery Team)
   */
   
    public pageReference updateCAP()
    {
            SFE_IndivCAP__c cap= [select assignedTo__c,PlatformingScoringsCompleted__c from SFE_IndivCAP__c where id =:capId];
            cap.PlatformingScoringsCompleted__c=true;                                                            
            Database.saveResult VFP53_PlatformingScoringsCompleted =  database.update(cap);            
                if(!VFP53_PlatformingScoringsCompleted.isSuccess())
                {
                Database.Error err =VFP53_PlatformingScoringsCompleted.getErrors()[0];  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,err+'')); 
                return null;                  
                }

              return new pagereference('/'+capId);  
    }


}