public with sharing class PRM_VFC_MigrationFromV1toV2 {
	public static String AUTOMATIC_EVENT_TYPE='Acct Assgnd Prgrm';
	public static String EXTERNAL_SYSTEM_MIGRATION_FUNCTIONNALKEY = 'BFOMigrationToProgramV2';
    //public String selectedCountry{get;set;}
    //Check get parameters
    private Id partnerProgramId;
    public List<ProgramLevelWrapper> programLevelWrapperList;
    public List<ProgramLevelWrapper> programLevelList {get {
    		if(programLevelWrapperList==null){
    			System.debug('These are the programLevelWrapperList of:'+partnerProgramId);
	    		programLevelWrapperList = new List<ProgramLevelWrapper>();
		    	if(partnerProgramId!=null){
	    			for(ProgramLevel__c prog: [select id,name,PartnerProgram__r.Name,PartnerProgram__r.FieloCHChallenge__c,PropertiesAndActivitiesCatalog__c,PropertiesAndActivitiesCatalog__r.PropertyName__c from ProgramLevel__c where PartnerProgram__c=:partnerProgramId order by Hierarchy__c]){
						programLevelWrapperList.add(new programLevelWrapper(prog));
					}
	    		}
    		}
    	 	return programLevelWrapperList;
    	}}

	public PRM_VFC_MigrationFromV1toV2() {
		try{
			partnerProgramId =!String.isEmpty(Apexpages.currentpage().getParameters().get('partnerProgramId'))?(Id)Apexpages.currentpage().getParameters().get('partnerProgramId'):null;
			System.debug('Ctor...'+partnerProgramId);
		}catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'An unexpected error has occurred: ' + ex.getMessage()));
        }
	}

	public PageReference  generateAllV2MigrationObjects(){
		Savepoint sp = Database.setSavepoint();
		try{
			List<ProgramLevel__c> programLevelToUpdate = new List<ProgramLevel__c>();
			List<ExternalPropertiesCatalog__c> extPropertiesCatToInsert = new List<ExternalPropertiesCatalog__c>();
			List<FieloPRM_EventCatalog__c> automaticEventToInsert = new List<FieloPRM_EventCatalog__c>();
			List<ProgramLevelWrapper> updateProgramList = new List<ProgramLevelWrapper>();
			List<FieloCH__Mission__c> missionToInsert = new List<FieloCH__Mission__c>();
			List<FieloCH__MissionCriteria__c> missionCriteriaToInsert = new List<FieloCH__MissionCriteria__c>();

			RecordType missionRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'WithObjective' and SobjectType ='FieloCH__Mission__c' limit 1];
        
			System.debug('These are the ProgramLevelWrapper...'+programLevelList);
			for(ProgramLevelWrapper prog: programLevelList){
				if(prog.isChecked){
					automaticEventToInsert.add(new FieloPRM_EventCatalog__c(F_PRM_isActive__c=true,Name=prog.generatedEventName,F_PRM_Type__c=AUTOMATIC_EVENT_TYPE,F_PRM_ValueText__c=prog.generatedEventName,RecordTypeId=Label.CLJUN16PRM102));
		        	extPropertiesCatToInsert.add(new ExternalPropertiesCatalog__c(RecordTypeId=(Id)System.label.CLJUN16PRM064,ExternalPropertiesCatalogType__r=new ExternalPropertiesCatalogType__c(  FunctionalKey__c=EXTERNAL_SYSTEM_MIGRATION_FUNCTIONNALKEY),PropertyName__c=prog.generatedEventName,Subtype__c=prog.progLevel.Id));
		        	programLevelToUpdate.add(prog.progLevel);
		        	updateProgramList.add(prog);
		        	FieloCH__Mission__c mission = new FieloCH__Mission__c(Name = prog.generatedEventName, FieloCH__Object__c = 'FieloEE__Event__c', FieloCH__ObjectiveType__c = 'Counter', 
            		FieloCH__Operator__c = 'greater or equal', FieloCH__ObjectiveValue__c = 1, RecordTypeId = missionRecordType.Id, F_PRM_MissionFilter__c  = 'P-WW');
        			missionToInsert.add(mission);
				}
			}
			if(extPropertiesCatToInsert.size()==0)
				return null;
			insert extPropertiesCatToInsert;
			insert automaticEventToInsert;
			insert missionToInsert;   
			List<PRMExternalPropertyEvent__c> propertyEventCatalogList = new List<PRMExternalPropertyEvent__c>();
			for(Integer i=0;i<automaticEventToInsert.size();i++){
				propertyEventCatalogList.add(new PRMExternalPropertyEvent__c(ExternalPropertiesCatalog__c=extPropertiesCatToInsert[i].Id,EventCatalog__c=automaticEventToInsert[i].Id));
				programLevelToUpdate[i].PropertiesAndActivitiesCatalog__c = extPropertiesCatToInsert[i].Id;
				updateProgramList[i].propertyCatalogId = extPropertiesCatToInsert[i].Id;
				updateProgramList[i].propertyCatalogName = extPropertiesCatToInsert[i].PropertyName__c;
				updateProgramList[i].isChecked = false;
				missionCriteriaToInsert.add(new FieloCH__MissionCriteria__c(FieloCH__FieldType__c='Text',FieloCH__Operator__c='contains',FieloCH__FieldName__c='F_PRM_ValueText__c',FieloCH__Mission__c=missionToInsert[i].Id,FieloCH__Values__c=extPropertiesCatToInsert[i].PropertyName__c));
				missionCriteriaToInsert.add(new FieloCH__MissionCriteria__c(FieloCH__FieldType__c='Text',FieloCH__Operator__c='contains',FieloCH__FieldName__c='FieloEE__Type__c',FieloCH__Mission__c=missionToInsert[i].Id,FieloCH__Values__c=AUTOMATIC_EVENT_TYPE));
			}
			insert propertyEventCatalogList;
			update programLevelToUpdate;
			insert missionCriteriaToInsert;
			
			//Map<Id ,ContactAssignedProgram__c> concernedContact = new Map<Id ,ContactAssignedProgram__c>([select Id,ProgramLevel__c,Contact__c from ContactAssignedProgram__c where ProgramLevel__c in:programLevelToUpdate]);
			//AP_PartnerContactAssignementMigration.createAndDeleteMemberProperties(null,concernedContact);
			             
		}catch(Exception ex){
			Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'An unexpected error has occurred: ' + ex.getMessage()));
        }
        return null;
	}


	public class ProgramLevelWrapper{
		public Boolean isChecked  {get;set;}
		public String programLevelName {get;set;}
		public String generatedEventName {get;set;}
		public Id challengeId {get;set;}
		public Id propertyCatalogId {get;set;}
		public String propertyCatalogName {get;set;}
		public ProgramLevel__c progLevel;

		public ProgramLevelWrapper(ProgramLevel__c prog){
			this.progLevel = prog;
			this.isChecked = prog.PropertiesAndActivitiesCatalog__c==null;
			this.programLevelName = prog.name;
			this.generatedEventName =prog.PartnerProgram__r.Name+' // '+prog.name;
			this.challengeId = prog.PartnerProgram__r.FieloCHChallenge__c;
			this.propertyCatalogId =prog.PropertiesAndActivitiesCatalog__c;
			this.propertyCatalogName = prog.PropertiesAndActivitiesCatalog__r.PropertyName__c;
		}
	}
}