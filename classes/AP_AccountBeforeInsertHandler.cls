/*
Author: Siddharth N(GD Solutions)
Purpose: Merged different functionalities of account before insert to a single class
*/
public with sharing class AP_AccountBeforeInsertHandler {
    private List<Account> lstPopulateAccountLeadingBU=new List<Account>();
    
    private Map<ID,List<Account>> mapParentAccountToChildAccount=new Map<ID,List<Account>>();
    private Map<String,Account> mapSEAccountIdtoAccount=new Map<String,Account>();
    
    private String userLeadingBU=[Select id,UserBusinessUnit__c from User where id=:UserInfo.getUserId() limit 1].UserBusinessUnit__c ;
    private Map<String,ID> accOwnForAll = new Map<String,ID>(); //Will hold the account owners of criteria where ClassLevel1, CountryCode and LeadingBU are non-null values
    private Map<String,ID> accOwnForNullLeadingBU = new Map<String,ID>();   //Will hold the account owners of criteria where LeadingBU are null values
    
    private List<Account> updateAcctCountryOrState= new List<Account>();
    private Set<String> countryCodeLst = new Set<String>();
    private Set<String> stateCodeLst = new Set<String>();
    
    public void OnBeforeInsert(List<Account> newAccounts,Map<Id,Account> newAccountsMap){
        // EXECUTE BEFORE INSERT LOGIC
    
        for(AccountOwnershipRule__c accOwnRul:[SELECT ClassLevel1__c, LeadingBusiness__c, TECH_CountryCode__c, AccountOwner__c, TECH_CriteriaCode__c from AccountOwnershipRule__c WHERE AccountOwner__c!=NULL])
        {
            if(String.isNotBlank(accOwnRul.LeadingBusiness__c))
                accOwnForAll.put(accOwnRul.TECH_CriteriaCode__c,accOwnRul.AccountOwner__c);
            else if(String.isBlank(accOwnRul.LeadingBusiness__c))
                accOwnForNullLeadingBU.put(accOwnRul.TECH_CriteriaCode__c,accOwnRul.AccountOwner__c);
        }
        
        
        for(Account accountRecord:newAccounts)
        {
            // ByPassing For PRM Record Type Accounts. To avoid the processing not required for it. 
            if(String.valueOf(accountRecord.RecordTypeId).StartsWithIgnoreCase(System.Label.CLAPR15PRM025))
                Continue;
                
            //populate the collections you would use.(the ones which require you to query account or any other object)
            //to populate the leading BU
            System.debug('--->>>>> accountRecord.LeadingBusiness__c --->>> '+accountRecord.LeadingBusiness__c);
            if(accountRecord.LeadingBusiness__c == null){               
                lstPopulateAccountLeadingBU.add(accountRecord);
            }
            //to populate the keyaccount type and targeted by on child
            if(accountRecord.ParentId != null && accountRecord.Type == null)
            {
                if(mapParentAccountToChildAccount.containsKey(accountRecord.ParentId))                                    
                    mapParentAccountToChildAccount.get(accountRecord.ParentId).add(accountRecord);                                 
                else                   
                    mapParentAccountToChildAccount.put(accountRecord.ParentId, new list<Account>{accountRecord});                                      
            } 
            //to check if the SEAccountId already exists     
            if(accountRecord.SEAccountID__c != null && accountRecord.SEAccountID__c.trim() != '')
            {
                if(mapSEAccountIdtoAccount.containsKey(accountRecord.SEAccountID__c))
                    accountRecord.addError(Label.CLMAY13ACC02);
                else
                    mapSEAccountIdtoAccount.put(accountRecord.SEAccountID__c,accountRecord);
            }


            //to prepopulate some values because salesforce doesn't give everything in workflows

            //populate tech owner Id
            accountRecord.Tech_AccountOwner__c=accountRecord.ownerID;

            //populate leading bu on account based on classification level 1, classification level 2, market segment
            
            //----BR-5861: Default Consumer account Leading Business (BU) based on User Business field of the User
            
            //For Consumer Account
            if(accountRecord.RecordTypeId==System.Label.CLDEC12ACC01)
            {            
                  //System.debug ('--- userLeadingBU :::: '+userLeadingBU);
                  if(userLeadingBU!='' && userLeadingBU!=null && CS_OpportunityLeadingBUMapping__c.getAll().containsKey(userLeadingBU))
                  {
                      accountRecord.LeadingBusiness__c = CS_OpportunityLeadingBUMapping__c.getValues(userLeadingBU).OpportunityLeadingBU__c;
                      System.debug ('--- accountRecord.LeadingBusiness__c for matched BU :::: '+accountRecord.LeadingBusiness__c);
                  }
                  else
                  {
                  // Default the consumer account leading BU to 'Partner' for all the User leading BU's that are not mapped in the custom setting CS_OpportunityLeadingBUMapping__c
                      accountRecord.LeadingBusiness__c = Label.CLAPR15ACC06;      
                      System.debug ('--- accountRecord.LeadingBusiness__c for Unmatched BU :::: '+accountRecord.LeadingBusiness__c);   
                  }
            }
            //----END BR-5861
            
            //For Business Accounts
            else if(accountRecord.RecordTypeId==System.Label.CLOCT13ACC08)         
            {            
                /*
                
                List<String> lstKey=new List<String>();
                if(accountRecord.ClassLevel1__c!=null)
                    lstKey.add(accountRecord.ClassLevel1__c);
                if(accountRecord.ClassLevel2__c!=null)
                    lstKey.add(accountRecord.ClassLevel2__c);
                if(accountRecord.MarketSegment__c!=null)
                    lstKey.add(accountRecord.MarketSegment__c);

                String accountSimplificationKey=String.join(lstKey,'_');                    
                System.debug ('--- accountRecord.accountSimplificationKey :::: '+accountSimplificationKey);

                
                if(CS_AccountSimplification__c.getAll().containsKey(accountSimplificationKey))              
                    accountRecord.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(accountSimplificationKey).LeadingBusiness__c;    
                
                 System.debug ('--- accountRecord.ClassLevel1__c:::: '+accountRecord.ClassLevel1__c); 
                 System.debug ('--- accountRecord.ClassLevel2__c:::: '+accountRecord.ClassLevel2__c); 
                 System.debug ('--- accountRecord.MarketSegment__c:::: '+accountRecord.MarketSegment__c); 
                 System.debug ('--- accountRecord.LeadingBusiness__c :::: '+accountRecord.LeadingBusiness__c); 
                 
                 */  
                 
                If(accountRecord.LeadingBusiness__c==NULL)
                {
                    if(CS_AccountSimplification__c.getValues(getKey(accountRecord.ClassLevel1__c, accountRecord.ClassLevel2__c, accountRecord.MarketSegment__c)) != null){
                       
                        accountRecord.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(getKey(accountRecord.ClassLevel1__c, accountRecord.ClassLevel2__c, accountRecord.MarketSegment__c)).LeadingBusiness__c;
                    }
                    else if(CS_AccountSimplification__c.getValues(getKey(accountRecord.ClassLevel1__c, null, accountRecord.MarketSegment__c)) != null)
                    {
                         accountRecord.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(getKey(accountRecord.ClassLevel1__c, null, accountRecord.MarketSegment__c)).LeadingBusiness__c;   
                    }
                    else if(CS_AccountSimplification__c.getValues(getKey(accountRecord.ClassLevel1__c, accountRecord.ClassLevel2__c , null)) != null){
                         accountRecord.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(getKey(accountRecord.ClassLevel1__c, accountRecord.ClassLevel2__c , null)).LeadingBusiness__c;
                    }
                }
            
                //----START BR-7029: Account Ownership Assignment Rule                         
                
                Id accountRecordOwnId;
                if(accountRecord.ReassignOwnership__c)
                {
                    // Generate criteria key from Account details where all conditional fields of are non-null. Obtain the owner for the generated key.
                    String accGeneratedKey = '';
                    if(accountRecord.ClassLevel1__c != '' && accountRecord.ClassLevel1__c != NULL)
                        accGeneratedKey+=accountRecord.ClassLevel1__c;
                    if(accountRecord.LeadingBusiness__c != '' && accountRecord.LeadingBusiness__c != NULL)
                        accGeneratedKey+=accountRecord.LeadingBusiness__c;
                    if(accountRecord.TECH_SDH_CountryCode__c != '' && accountRecord.TECH_SDH_CountryCode__c != NULL)
                        accGeneratedKey+=accountRecord.TECH_SDH_CountryCode__c;
                        //System.debug('Account Generated Key from account details with non-null values --- '+accGeneratedKey);
                    
                    if(accountRecordOwnId==null) 
                        accountRecordOwnId = accOwnForAll.get(accGeneratedKey);
                    
                    // Generate criteria key from Account details where the Leading BU field is null. Obtain the owner for the generated key if not obtained yet.
                    String accGeneratedKeyWithNullLeadingBU = '';
                    if(accountRecord.ClassLevel1__c != '' && accountRecord.ClassLevel1__c != NULL)
                        accGeneratedKeyWithNullLeadingBU+=accountRecord.ClassLevel1__c;
                    if(accountRecord.TECH_SDH_CountryCode__c != '' && accountRecord.TECH_SDH_CountryCode__c != NULL)
                        accGeneratedKeyWithNullLeadingBU+=accountRecord.TECH_SDH_CountryCode__c;
                        //System.debug('Account Generated Key from account details with null Leading BU value --- '+accGeneratedKeyWithNullLeadingBU);
                    
                    if(accountRecordOwnId==null) 
                        accountRecordOwnId = accOwnForNullLeadingBU.get(accGeneratedKeyWithNullLeadingBU);
                    
                    // Assign the owner only if variable 'accountRecordOwnId' is non-null. This is to ensure that blank is not assigned to Account OwnerId.
                    if(accountRecordOwnId!=NULL)    
                        accountRecord.OwnerId = accountRecordOwnId;
                        
                    // Reset the value of ReassignOwnership__c field
                    accountRecord.ReassignOwnership__c = false;
                }   
                
                //----END BR-7029: Account Ownership Assignment Rule                          
                                                 
            }
            
            //Back2Standard Q2 2016 - Populate custom Country, StateProvince, ZipCode fields of the physical address from standard Billing address
            if(String.isNotBlank(accountRecord.BillingCountryCode) || String.isNotBlank(accountRecord.BillingStateCode))
            {
                updateAcctCountryOrState.add(accountRecord);
                countryCodeLst.add(accountRecord.BillingCountryCode);
                stateCodeLst.add(accountRecord.BillingStateCode);
            }
            
            if(String.isNotBlank(accountrecord.BillingPostalCode))
                accountRecord.ZipCode__c = accountrecord.BillingPostalCode;    

            
        }

        //call your private functions accordingly.

        //update the parent account type
        updateAccountTypeandTargetedBy();
        //check for SEAccount Id
        checkSEAccountId();
        //populate custom state, country fields based on the standard Billing address fields
        if(updateAcctCountryOrState.size()>0)
             updateCustomAddrFields();
    }

    private void updateAccountTypeandTargetedBy()
    {
        if(!mapParentAccountToChildAccount.isEmpty())
        {           
            Integer remainingRecordsforLimit=Limits.getLimitQueryRows()-Limits.getQueryRows();
            for(Account parentAccountRecord : [select type,id,TargetedBy__c from Account where ID IN :mapParentAccountToChildAccount.keySet() and type!=null limit :remainingRecordsforLimit])
            {
                 for(Account accountRecord:mapParentAccountToChildAccount.get(parentAccountRecord.id))
                 {                    
                        
                            accountRecord.Type = parentAccountRecord.Type;     
                            accountRecord.TargetedBy__c= parentAccountRecord.TargetedBy__c;                    
                 }
            }
        }        
    }
    
   
    private void checkSEAccountId()
    {
        Integer remainingRecordsforLimit=Limits.getLimitQueryRows()-Limits.getQueryRows();      
        for(Account existingAccountrecord :[select id,name,SEAccountID__c from Account where SEAccountID__c IN :mapSEAccountIdtoAccount.keySet() limit :remainingRecordsforLimit])
        {           
            if(mapSEAccountIdtoAccount.containsKey(existingAccountrecord.SEAccountID__c))
                mapSEAccountIdtoAccount.get(existingAccountrecord.SEAccountID__c).addError(Label.CLMAY13ACC02);            
        }      
    }
    
    public String getKey(string CCL1, string CCL2, string MS)
    {
            String key='';
            if(CCL1 != null){
                key +=CCL1+'_';
            }
            if(CCL2 != null){
                key +=CCL2+'_';
            }
            else{key +='_';}
            if(MS  != null){
                key +=MS;
            }
            
            return key;
    }
    
    private void updateCustomAddrFields()
    {
        Map<String,Country__c> countryMap = new Map<String,Country__c>();
        Map<String,StateProvince__c> stateMap = new Map<String,StateProvince__c>();
        
        
        for(Country__c ct : [SELECT ID, CountryCode__c, Name FROM Country__c WHERE CountryCode__c IN :countryCodeLst])
        {
            countryMap.put(ct.CountryCode__c,ct);
        }
        
        for(StateProvince__c st: [SELECT ID, Name, StateProvinceCode__c, CountryCode__c FROM StateProvince__c WHERE CountryCode__c IN :countryCodeLst AND StateProvinceCode__c IN :stateCodeLst])
        {
            String ctyStateKey = st.CountryCode__c + st.StateProvinceCode__c;
            stateMap.put(ctyStateKey,st);
        }
        
        for(Account acc:updateAcctCountryOrState)
        {
            if(String.isNotBlank(acc.BillingCountryCode))
                acc.Country__c = countryMap.get(acc.BillingCountryCode).Id;
            if(String.isNotBlank(acc.BillingStateCode))    
                acc.StateProvince__c = stateMap.get(acc.BillingCountryCode+acc.BillingStateCode).Id;
            
        }
    }

}