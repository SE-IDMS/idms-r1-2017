/**************************************************************************************
    Author: Fielo Team
    Date: 22/06/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/
@isTest

public class FieloPRM_VFP_ContentFWidgetRLExtenTest{

    public static testMethod void test1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
           
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;
            

            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.id);
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.name = 'test';
            insert tag;
            
            FieloEE__Component__c component = new FieloEE__Component__c();
            component.FieloEE__Menu__c = thisMenu.id;
            component.FieloEE__Tag__c = tag.id;
            insert component;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c();
            contentFeed.FieloEE__IsActive__c = true;
            contentFeed.FieloEE__Component__c = component.id;
            contentFeed.FieloEE__CategoryItem__c = category.id;
            insert contentFeed;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c();
            //tagItem.F_PRM_Category__c = category.id;
            tagItem.FieloEE__News__c = contentFeed.id;
            tagItem.FieloEE__Tag__c = tag.id;
            insert tagItem;
            
            FieloEE__Component__c compo = new FieloEE__Component__c(); 
            compo.FieloEE__Menu__c = thisMenu.id;
            insert compo;
                      
            test.startTest();

            ApexPages.StandardController stdController = new ApexPages.StandardController(component);
            FieloPRM_VFP_ContentFWidgetRLExtension ext = new FieloPRM_VFP_ContentFWidgetRLExtension(stdController);

            ext.doNewCF();
            ApexPages.currentPage().getParameters().put('cfId',contentFeed.id);
            ext.doDelete();   
           
            test.stopTest();
        }
    }
 
}