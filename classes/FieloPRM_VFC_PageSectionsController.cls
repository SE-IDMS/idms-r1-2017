/********************************************************************
* Company: Fielo
* Created Date: 22/08/2016
* Description:
********************************************************************/

public class FieloPRM_VFC_PageSectionsController {
    
    public List<FieloEE__Section__c> componentsSection {get; set;}        
    public String fieldOptionalName {get; set;}
    public Map<Id,List<FieloEE__Banner__c>> bannersMap {get; set;}
    public Map<Id,List<FieloEE__News__c>> newsMap {get; set;}
    public Map<Id,List<String>> fieldSetsMap {get; set;}
    public transient Id contactId {get;set;}

    private static String docURL;
    public static String getDocURL(){
            if(docURL == null){
                String instance = FieloEE__PublicSettings__c.getOrgDefaults().FieloEE__Instance__c;
                if(String.isNotBlank(instance)){
                    docURL = '//c.' + instance + '.content.force.com';
                }else{
                    docURL = '';
                }
            }
            return docURL;
    }

    public static FieloPRM_FieldsML fieldsML{get{if(fieldsML == null) fieldsML = getFieldsML(); return fieldsML;} set;}    
    private static FieloPRM_FieldsML getFieldsML(){
        FieloPRM_FieldsML result = new FieloPRM_FieldsML();
        String language = FieloEE.OrganizationUtil.getLanguage();
        Set<String> bannerFields = Schema.SObjectType.FieloEE__Banner__c.fields.getMap().keySet();
        
        String fieldNameAttachment = 'AttachmentId_' + language + '__c';
        Boolean attachmentMLExist = bannerFields.contains(fieldNameAttachment.toLowerCase());
        if(attachmentMLExist)
            result.fieldAttachment = fieldNameAttachment;
        else
            result.fieldAttachment = 'FieloEE__AttachmentId__c';

        String fieldNameLink = 'Link_' + language + '__c';
        Boolean linkMLExist = bannerFields.contains(fieldNameLink.toLowerCase());
        if(linkMLExist)
            result.fieldLink = fieldNameLink;
        else
            result.fieldLink = 'FieloEE__Link__c';

        String fieldNameDescription = 'Description_' + language + '__c';
        Boolean descriptionMLExist = bannerFields.contains(fieldNameDescription.toLowerCase());
        if(descriptionMLExist)
            result.fieldDescription = fieldNameDescription;
        else
            result.fieldDescription = 'FieloEE__Description__c';

        String fieldNameOverlayHTML = 'OverlayHtml_' + language + '__c';
        Boolean overlayHTMLMLExist = bannerFields.contains(fieldNameOverlayHTML.toLowerCase());
        if(overlayHTMLMLExist)
            result.fieldOverlayHTML = fieldNameOverlayHTML;
        else
            result.fieldOverlayHTML = 'FieloEE__OverlayHtml__c';

        String fieldNameOverlayText = 'OverlayText_' + language + '__c';
        Boolean overlayTextMLExist = bannerFields.contains(fieldNameOverlayText.toLowerCase());
        if(overlayTextMLExist)
            result.fieldOverlayText = fieldNameOverlayText;
        else
            result.fieldOverlayText = 'FieloEE__OverlayText__c';
    
        return result;
    }

    public FieloPRM_VFC_PageSectionsController(){
        
        String lang = FieloEE.OrganizationUtil.getLanguage();
        
        //Multilanguage OptionalName field
        fieldOptionalName = FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__Component__c(), 'FieloEE__OptionalName__c', lang);

        Id sectionId = ApexPages.currentPage().getParameters().get('sectionId');    
        
        //If its the Default Case use only Standard Field
        String fields = 'FieloEE__Menu__r.FieloEE__Menu__c,FieloEE__OptionalName__c,FieloEE__Content__c,FieloEE__ContentHTML__c,FieloEE__NewsType__c,FieloEE__CSSClasses__c,FieloEE__CCS__c,FieloEE__Qcolumns__c,FieloEE__Qrows__c,FieloEE__FilterFormula__c,FieloEE__OrderBy__c,Id,FieloEE__PageName__c,FieloEE__Paging__c,FieloEE__Qant__c,FieloEE__Category__c,FieloEE__Delay__c,FieloEE__Layout__c,FieloEE__Display__c,FieloEE__Height__c,FieloEE__MenuRedirect__c,FieloEE__FieldSet__c,FieloEE__Filter__c,FieloEE__Tag__c,FieloEE__FieldSet2__c,FieloEE__Modal__c';
        for(Schema.FieldSetMember fsm : Schema.SObjectType.FieloEE__Component__c.fieldSets.FieloEE__ComponentQuery.getFields()){
            fields += ',' + fsm.getFieldPath();
        }

        // create a map ID-Lista de Section
        componentsSection = Database.query('SELECT Id, FieloEE__Type__c, FieloEE__Order__c, Name, FieloEE__Menu__c, FieloEE__Parent__c, FieloEE__CSSClasses__c, (SELECT ' + fields + ' ,RecordType.DeveloperName FROM FieloEE__Components__r ORDER BY FieloEE__Order__c asc) FROM FieloEE__Section__c WHERE FieloEE__Parent__c =: sectionId ORDER BY FieloEE__Order__c ASC');        
        bannersMap = new Map<Id,List<FieloEE__Banner__c>>();
        newsMap = new Map<Id,List<FieloEE__News__c>>();
        fieldSetsMap = new Map<Id,List<String>>();
        for(FieloEE__Section__c subSection : componentsSection){                        
            for(FieloEE__Component__c c : subSection.FieloEE__Components__r){
                if(c.RecordType.DeveloperName == 'Banner' || c.RecordType.DeveloperName == 'BannerWidget'){
                    bannersMap.put(c.Id, getBanners(c));
                }else if(c.RecordType.DeveloperName == 'News' || c.RecordType.DeveloperName == 'ContentWidget'){
                    system.debug('fieldset: ' + c.FieloEE__FieldSet2__c);
                    List<String> fieldSet = FieloEE.OrganizationUtil.getFieldSet(c.FieloEE__FieldSet2__c);
                    newsMap.put(c.Id, getNews(c, fieldSet));
                    fieldSetsMap.put(c.Id, fieldSet);
                }
            }
        }
        
        
         if(!Test.isRunningTest()){
            FieloEE__Member__c logdMember = [SELECT Id,FieloEE__User__r.contactId FROM FieloEE__Member__c
                                         WHERE Id =: FieloEE.MemberUtil.getMemberId()];
                                        
            contactId = logdMember.FieloEE__User__r.contactId;
         }   
    }

    private List<FieloEE__Banner__c> getBanners(FieloEE__Component__c comp){        
        Set<String> fieldset = new Set<String>{fieldsML.fieldAttachment, fieldsML.fieldDescription, fieldsML.fieldOverlayHTML, fieldsML.fieldOverlayText, fieldsML.fieldLink};
        if(comp.FieloEE__Qant__c == null) comp.FieloEE__Qant__c = 30;
        return FieloEE.BannerService.getBanners(fieldset, FieloEE.ProgramUtil.getProgramByDomain().Id, FieloEE.MemberUtil.getMemberId(), comp.Id, comp.FieloEE__Tag__c, comp.FieloEE__Category__c, comp.FieloEE__Qant__c.intValue(), null, null);
    }
    
    private List<FieloEE__News__c> getNews(FieloEE__Component__c comp, List<String> fieldSetList){
        Set<String> fieldSet = new Set<String>{'Name', 'Body__c' , 'Title__c', 'IsActive__c', 'PublishDate__c', 'Category__c','CreatedDate', 'AttachmentId__c','ExtractText__c'};
        fieldSet.addAll(fieldSetList);
        if(comp.FieloEE__Qant__c == null) comp.FieloEE__Qant__c = 30;
        return FieloEE.NewsService.getNews(fieldSet, FieloEE.ProgramUtil.getProgramByDomain().Id, FieloEE.MemberUtil.getMemberId(), comp.Id, comp.FieloEE__Tag__c, comp.FieloEE__Category__c, comp.FieloEE__Qant__c.intValue(), null, comp.FieloEE__OrderBy__c);
    }
    
}