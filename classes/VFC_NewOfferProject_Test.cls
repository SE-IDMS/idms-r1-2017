@istest
public class VFC_NewOfferProject_Test{
    static testmethod void testofferlaunch(){
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'newUser1', Email='newuser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
        
        RecordType rt = [select id, name from RecordType where name = 'Offer Launch - Product' and sObjectType='Offer_Lifecycle__c'];
        RecordType prt = [select id, name from RecordType where name = 'Offer Introduction Project' and sObjectType='Milestone1_Project__c'];
        //create offer
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c();
            olc.RecordTypeId=rt.id;
            olc.Name='Test';
            olc.Offer_Name__c='offer to Test';
            olc.Description__c='Offer Description';
            olc.Leading_Business_BU__c='Cloud Services';
            olc.Sales_Date__c= system.today() ;
            olc.Forecast_Unit__c='Opportunities';
            olc.Process_Type__c='PMP';
            olc.OfferScope__c='Country';
            olc.Launch_Owner__c=u1.id;
            olc.Marketing_Director__c=u1.id;
        insert olc;
        
        //create Country
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        insert c;
        
        //insert Projects
        Milestone1_Project__c pjct = new Milestone1_Project__c();
            pjct.RecordtypeId = prt.id;
            pjct.name='Test Project1';
            pjct.offer_launch__c = olc.id;
            pjct.country__c=c.id;
            pjct.Country_Announcement_Date__c=system.today();
            pjct.region__c='APAC';
            pjct.Country_Launch_Leader__c=u1.id;
        insert pjct;
        
        Apexpages.currentpage().getparameters().put('id',olc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(olc);
        VFC_NewOfferProject newpjct = new VFC_NewOfferProject();
        newpjct.redirecttoMilestone1Project(pjct);
        newpjct.projectEditPage();
        newpjct.offerCancel();
        
    }
    static testmethod void testofferlaunchsol(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'newUser1', Email='newuser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
        
        RecordType rt = [select id, name from RecordType where name = 'Offer Launch - Solution' and sObjectType='Offer_Lifecycle__c'];
        RecordType prt = [select id, name from RecordType where name = 'Offer Launch Solution Project' and sObjectType='Milestone1_Project__c'];
        //create offer
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c();
            olc.RecordTypeId=rt.id;
            olc.Name='Test';
            olc.OfferScope__c='Country';
            olc.Offer_Name__c='offer to Test';
            olc.Description__c='Offer Description';
            olc.Leading_Business_BU__c='Cloud Services';
            olc.Sales_Date__c= system.today() ;
            olc.Forecast_Unit__c='Opportunities';
            olc.Process_Type__c='PMP';
            olc.Launch_Owner__c=u1.id;
            olc.Marketing_Director__c=u1.id;
        insert olc;
        
        //create Country
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        insert c;
        
        //insert Projects
        Milestone1_Project__c pjct = new Milestone1_Project__c();
            pjct.RecordtypeId = prt.id;
            pjct.name='Test Project1';
            pjct.offer_launch__c = olc.id;
            pjct.country__c=c.id;
            pjct.Country_Announcement_Date__c=system.today();
            pjct.region__c='APAC';
            pjct.Country_Launch_Leader__c=u1.id;
        insert pjct;
        
        Apexpages.currentpage().getparameters().put('id',olc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(pjct);
        VFC_NewOfferProject newpjct = new VFC_NewOfferProject(controller);
        //newpjct(controller);
        newpjct.redirecttoMilestone1Project(pjct);
        newpjct.projectEditPage();
        newpjct.offerCancel();
        
    }
    static testmethod void testofferwithdraw(){
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'newUser1', Email='newuser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
         
        RecordType rt = [select id, name from RecordType where name = 'Offer Withdrawal - Product' and sObjectType='Offer_Lifecycle__c'];
        RecordType prt = [select id, name from RecordType where name = 'Offer Withdrawal Project' and sObjectType='Milestone1_Project__c'];
        //create offer
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c();
            olc.RecordTypeId=rt.id;
            olc.Name='Test';
            olc.OfferScope__c='Country';
            olc.Offer_Name__c='offer to Test';
            olc.Description__c='Offer Description';
            olc.Leading_Business_BU__c='Cloud Services';
            olc.Sales_Date__c= system.today() ;
            olc.Forecast_Unit__c='Opportunities';
            olc.Process_Type__c='PMP';
            olc.Withdrawal_Owner__c=u1.id;
            olc.Marketing_Director__c=u1.id;
        insert olc;
        
        //create Country
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        insert c;
        
        //insert Projects
        Milestone1_Project__c pjct = new Milestone1_Project__c();
            pjct.RecordtypeId = prt.id;
            pjct.name='Test Project1';
            pjct.offer_launch__c = olc.id;
            pjct.country__c=c.id;
            pjct.Country_Announcement_Date__c=system.today();
            pjct.region__c='APAC';
            pjct.Country_Withdrawal_Leader__c=u1.id;
        insert pjct;
        
         Milestone1_Project__c pjct1 = new Milestone1_Project__c();
            pjct1.RecordtypeId = prt.id;
            pjct1.name='Test Project121';
            pjct1.offer_launch__c = olc.id;
            pjct1.country__c=c.id;
            pjct1.Country_Announcement_Date__c=system.today();
            pjct1.region__c='APAC';
            pjct1.Country_Withdrawal_Leader__c=u1.id;
        insert pjct1;
        
        Delete pjct1;
        Apexpages.currentpage().getparameters().put('id',olc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(olc);
        VFC_NewOfferProject newpjct = new VFC_NewOfferProject();
        newpjct.redirecttoMilestone1Project(pjct);
        newpjct.projectEditPage();
        newpjct.offerCancel();
        
    }
    
    
    static testmethod void testofferwithdrawRegion(){
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u1 = new User(Alias = 'newUser1', Email='newuser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
         
        RecordType rt = [select id, name from RecordType where name = 'Offer Withdrawal - Product' and sObjectType='Offer_Lifecycle__c'];
        RecordType prt = [select id, name from RecordType where name = 'Offer Withdrawal Project' and sObjectType='Milestone1_Project__c'];
        //create offer
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c();
            olc.RecordTypeId=rt.id;
            olc.Name='Test';
            olc.OfferScope__c='Region';
            olc.Offer_Name__c='offer to Test';
            olc.Description__c='Offer Description';
            olc.Leading_Business_BU__c='Cloud Services';
            olc.Sales_Date__c= system.today() ;
            olc.Forecast_Unit__c='Opportunities';
            olc.Process_Type__c='PMP';
            olc.Withdrawal_Owner__c=u1.id;
            olc.Marketing_Director__c=u1.id;
        insert olc;
        
        //create Country
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        insert c;
        
        //insert Projects
        Milestone1_Project__c pjct = new Milestone1_Project__c();
            pjct.RecordtypeId = prt.id;
            pjct.name='Test Project1';
            pjct.offer_launch__c = olc.id;
            pjct.country__c=c.id;
            pjct.Country_Announcement_Date__c=system.today();
            pjct.region__c='APAC';
            pjct.Country_Withdrawal_Leader__c=u1.id;
        insert pjct;
        
         Milestone1_Project__c pjct1 = new Milestone1_Project__c();
            pjct1.RecordtypeId = prt.id;
            pjct1.name='Test Project121';
            pjct1.offer_launch__c = olc.id;
            pjct1.country__c=c.id;
            pjct1.Country_Announcement_Date__c=system.today();
            pjct1.region__c='APAC';
            pjct1.Country_Withdrawal_Leader__c=u1.id;
        insert pjct1;
        
        Delete pjct1;
        Apexpages.currentpage().getparameters().put('id',olc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(olc);
        VFC_NewOfferProject newpjct = new VFC_NewOfferProject();
        newpjct.redirecttoMilestone1Project(pjct);
        newpjct.projectEditPage();
        newpjct.offerCancel();
        
    }


}