@isTest
private class CaseArchiving_DeleteACases_Batch_TEST{
     static testMethod void deleteAcaseBatchTest(){
          Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
                 
          contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
          Database.insert(con);     
          Test.startTest();
          Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'closed');
          Database.insert(caseobj);
          
          caseobj.ArchivedStatus__c='Has Attachment';
          update caseobj;
          
          Case caseList= [Select id,casenumber from case where id=:caseobj.id limit 1];
          
          ArchiveCase__c acaseObj = new ArchiveCase__c();
          acaseobj.name=CaseList.CaseNumber;
          insert acaseobj;
          
          CaseArchiving_DeleteArchiveCases_Batch  cadb = new CaseArchiving_DeleteArchiveCases_Batch(); 
          ID batchprocessid =Database.executeBatch(cadb);
          Test.stopTest();    
    }
}