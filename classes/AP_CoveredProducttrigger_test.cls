/*     
@Author: Deepak
Created Date: 02-04-2014
Description: Test class for AP_CoveredProducttrigger .
**********
*/
@isTest(SeeAllData=true)
Public Class AP_CoveredProducttrigger_test
{
     static testMethod void Test() 
    {   
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.SVMXC__Company__c = testAccount.Id;
        insert sc;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = testAccount.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = testAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c = 'device1';
        insert ip1;
        
        SVMXC__Service__c sname = New SVMXC__Service__c();
        sname.Name='test1';
        insert sname;
        
        SVMXC__Service_Contract_Services__c scs = New SVMXC__Service_Contract_Services__c();
        scs.SVMXC__Service_Contract__c = sc.Id;
        scs.SVMXC__Service__c = sname.Id;
        insert scs;
        
        
        SVMXC__Service_Contract_Products__c cpList = New SVMXC__Service_Contract_Products__c();
        cpList.IncludedService__c = scs.Id;
        cpList.SVMXC__Service_Contract__c = sc.Id;
        cpList.SVMXC__Installed_Product__c = ip1.Id;
        insert cpList;
        List<SVMXC__Service_Contract_Products__c> cpList1 = New List<SVMXC__Service_Contract_Products__c>();
        cpList1.add(cpList);
        AP_CoveredProducttrigger.AddError(cpList1);
        
    }
}