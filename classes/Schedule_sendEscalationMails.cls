global class Schedule_sendEscalationMails implements Schedulable {
   global void execute(SchedulableContext sc) {
      /*Batch_sendEscalationMails b1 = new Batch_sendEscalationMails('ComplaintRequest__c');
      Batch_sendEscalationMails b2 = new Batch_sendEscalationMails('ContainmentAction__c');
      Batch_sendEscalationMails b3 = new Batch_sendEscalationMails('CorrectiveAction__c');
      Batch_sendEscalationMails b4 = new Batch_sendEscalationMails('PreventiveAction__c');
      database.executebatch(b1);
      database.executebatch(b2);
      database.executebatch(b3);
      database.executebatch(b4);
      */
      Database.executeBatch(new Batch_sendEscalationMails());
   }
}