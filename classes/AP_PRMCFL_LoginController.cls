public with sharing class AP_PRMCFL_LoginController {

    public PageReference redirect() {
      PageReference prmLoginPage = new pagereference(Label.CLOCT15CF031);
      prmLoginPage.setRedirect(true);
      return prmLoginPage;
    }

}