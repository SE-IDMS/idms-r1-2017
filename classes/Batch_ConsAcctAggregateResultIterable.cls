global class Batch_ConsAcctAggregateResultIterable implements Iterable<AggregateResult>{
    global Iterator<AggregateResult> Iterator(){ 
        return new Batch_ConsAcctAggregateResultIterator(); 
    } 
}