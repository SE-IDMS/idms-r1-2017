/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  07-Mar-2012
    Modification Log    :  Added Partner-Region on 09-May-2012
    Description         : Class on Connect Program to Validate Updates to Scope
 */

public class ConnectGlobalProgramTriggers
{

// Class to Check Milestone on Program Updation

    Public Static void ConnectGlobalProgramBeforeUpdate(List<Project_NCP__c> Prgnew,List<Project_NCP__c> Prgold )
      {
      
          System.Debug('****** ConnectGlobalProgramBeforeUpdate Start ****');
          
          List<Connect_Milestones__c> Mile = new List<Connect_Milestones__c>();
          
          Mile = [Select Name, Program__c, Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c, Partner_Region__c,Smart_Cities_Division__c from Connect_Milestones__c where Year__c = :Prgnew[0].Year__c ];
          
          
          // Check if a Scope with child Milestone is removed in the update action
          
          for(Project_NCP__c P : Prgnew){
              for(Connect_Milestones__c M: Mile){    
                if(P.Name == M.Program__c) { 
                
                // Check if Entiy in the Program is Null for any Scope (All Entities Removed)
                
                if(((P.Global_Functions__c == null)&& (M.Global_Functions__c !=null)) || ((P.Global_Business__c == null)&& (M.Global_Business__c !=null)) || ((P.Power_Region__c == null)&& (M.Power_Region__c !=null))  || ((P.GSC_Regions__c == null)&& (M.GSC_Regions__c !=null) || ((P.Partner_Region__c == null)&& (M.Partner_Region__c !=null)) || ((P.Smart_Cities_Division__c == null)&& (M.Smart_Cities_Division__c !=null))))
                     P.ValidateScopeonUpdate__c = True;
                
                // Check for Global Function Scope
                         
                    if (P.Global_Functions__c != prgold[0].Global_Functions__c){
                     if((M.Global_Functions__c !=null) && (P.Global_Functions__c != null)){
                      System.Debug('Milestone GF ' + M.Global_Functions__c);
                      System.Debug('Program GF ' + P.Global_Functions__c);
                        for(String GF : M.Global_Functions__c.split(';')){
                            if(!(P.Global_Functions__c.Contains(GF)))
                                P.ValidateScopeonUpdate__c = True;
                            } // GF For Ends
                           } // Milestone GF If Ends
                           } // Program GF If Ends
                           
                           
                  // Check for Global Business Scope
                         
                    if (P.Global_Business__c != prgold[0].Global_Business__c){
                     if((M.Global_Business__c !=null)&& (P.Global_Business__c != null)){
                     System.Debug('Milestone GB ' + M.Global_Business__c);
                      System.Debug('Program GB ' + P.Global_Business__c);
                        for(String GB : M.Global_Business__c.split(';')){
                            if(!(P.Global_Business__c.Contains(GB)))
                                P.ValidateScopeonUpdate__c = True;
                            } // GB For Ends
                           } // GB Milestone If Ends
                          } // GB Program If Ends 
                          
                   // Check for Power Regions
                   
                    if (P.Power_Region__c != prgold[0].Power_Region__c){
                     if((M.Power_Region__c !=null) && (P.Power_Region__c != null)){
                        for(String GB : M.Power_Region__c.split(';')){
                            if(!(P.Power_Region__c.Contains(GB)))
                                P.ValidateScopeonUpdate__c = True;
                            } // PR For Ends
                           } // PR Milestone If Ends
                          } // PR Program  If Ends
                           
                  // Check for GSC Region Scope
                         
                    if (P.GSC_Regions__c != prgold[0].GSC_Regions__c){
                     if((M.GSC_Regions__c != null)&& (P.GSC_Regions__c != null)) {
                        for(String GSC : M.GSC_Regions__c.split(';')){
                            if(!(P.GSC_Regions__c.Contains(GSC)))
                                P.ValidateScopeonUpdate__c = True;
                            } // GSC For Ends
                           } // GSC Milestone If Ends
                          } //GSC  Program If Ends 
                         
                         // Check for Smart cities Scope
                         
                    if (P.Smart_Cities_Division__c != prgold[0].Smart_Cities_Division__c){
                     if((M.Smart_Cities_Division__c != null)&& (P.Smart_Cities_Division__c != null)) {
                        for(String SCD : M.Smart_Cities_Division__c.split(';')){
                            if(!(P.Smart_Cities_Division__c.Contains(SCD)))
                                P.ValidateScopeonUpdate__c = True;
                            } // GSC For Ends
                           } // GSC Milestone If Ends
                          } //GSC  Program If Ends
                          
                          // Check for Partner Region Scope
                         
                    if (P.Partner_Region__c != prgold[0].Partner_Region__c){
                     if((M.Partner_Region__c != null)&& (P.Partner_Region__c != null)) {
                        for(String PAR : M.Partner_Region__c.split(';')){
                            if(!(P.Partner_Region__c.Contains(PAR)))
                                P.ValidateScopeonUpdate__c = True;
                            } // Partner Region For Ends
                           } // Partner Region Milestone If Ends
                          } //Partner Region  Program If Ends 
                          } // IF Ends
                          
                  } // Milestone For Ends   
                 }// Program For Ends      
             
       }
       
       // Class to Check Deployment Network on Program Updation
       
    Public Static void ConnectGlobalProgramBeforeUpdate_CheckDeploymentNetwork(List<Project_NCP__c> Prgnew,List<Project_NCP__c> Prgold )
      {
      
          System.Debug('****** ConnectGlobalProgramBeforeUpdate_CheckDeploymentNetwork Start ****');
          
          List<Team_Members__c> Team = New List<Team_Members__c>();
          
          Team = [Select Name, Team_Name__c, Entity__c, GSC_Region__c, Partner_Region__c,Smart_Cities_Division__c,Program_Name__c from Team_Members__c where Year__c = :Prgnew[0].Year__c];
          
          for(Project_NCP__c P : Prgnew){
              for(Team_Members__c T: Team){    
                if(P.Name == T.Program_Name__c) { 
                  
                   // Check if Entiy in the Program is Null for any Scope (All Entities Removed)
                
                if(((P.Global_Functions__c == null)&& (T.Team_Name__c == Label.Connect_Global_Functions)) || ((P.Global_Business__c == null)&& (T.Team_Name__c == Label.Connect_Global_Business)) || ((P.Power_Region__c == null)&& (T.Team_Name__c == Label.Connect_Power_Region))  || ((P.GSC_Regions__c == null)&& (T.GSC_Region__c !=null))  || ((P.Smart_Cities_Division__c == null)&& (T.Smart_Cities_Division__c !=null)))
                     P.ValidateScopeonUpdate__c = True;
                     
                       // Check for Every Scope
                         
                    
                       if((P.Global_Functions__c != null) && (T.Team_Name__c == Label.Connect_Global_Functions) && (!(P.Global_Functions__c.Contains(T.Entity__c))))
                          P.ValidateScopeonUpdate__c = True;
                      else if((P.Global_Business__c != null)&& (T.Team_Name__c == Label.Connect_Global_Business) && (!(P.Global_Business__c.Contains(T.Entity__c))))
                          P.ValidateScopeonUpdate__c = True;
                      else if((P.Power_Region__c != null) && (T.Team_Name__c == Label.Connect_Power_Region) && (!(P.Power_Region__c.Contains(T.Entity__c))))
                          P.ValidateScopeonUpdate__c = True;
                       else if((P.GSC_Regions__c != null)&& (T.Entity__c == Label.Connect_GSC) && (!(P.GSC_Regions__c.Contains(T.GSC_Region__c))))
                          P.ValidateScopeonUpdate__c = True;
                          else if((P.Smart_Cities_Division__c != null)&& (T.Entity__c == Label.Connect_Smart_Cities) && (!(P.Smart_Cities_Division__c.Contains(T.Smart_Cities_Division__c))))
                          P.ValidateScopeonUpdate__c = True;                    
                        else if((P.Partner_Region__c != null)&& (T.Entity__c == Label.Connect_Partner) && (!(P.Partner_Region__c.Contains(T.Partner_Region__c))))
                          P.ValidateScopeonUpdate__c = True;
                     
                    } // If Ends
                  } // For Ends
                 }// For Ends
                 
                 System.Debug('****** ConnectGlobalProgramBeforeUpdate_CheckDeploymentNetwork Stop ****');
                }
                
  Public Static void ConnectGlobalProgramNetworkValidationBeforeUpdate(List<Project_NCP__c> prgnew)
{

// Validate Deployment Network when the Scope is changed in the Program

System.Debug('****** ConnectGlobalProgramNetworkValidationBeforeUpdate Start ****'); 

// Lists

list<Team_Members__c> Team = [select Team_Member__r.FirstName, Team_Member__r.LastName, Team_Name__c, Entity__c, Program_Name__c, GSC_Region__c, Partner_Region__c,Smart_Cities_Division__c from Team_Members__c  ] ;
list <Project_NCP__c> Program = [Select Name, Program_Name__c, Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c,Partner_Region__c,Smart_Cities_Division__c from Project_NCP__c];

String GF_Team;
String GB_Team;
String PR_Team;
String GSC_Team;
String Partner_Team;
String Smart_Team;

Integer GFn = 0;
Integer GBs = 0;
Integer PRs = 0;
Integer GSC_R = 0;
Integer PAReg = 0;
Integer SCDs = 0;

For(Project_NCP__c Prg : prgnew){

    For(Team_Members__c TMs : Team){

    if(TMs.Team_Name__c == System.Label.Connect_Global_Functions && TMs.Program_Name__c == Prg.Name){
        GF_Team = GF_Team + Tms.Entity__c + ';';
          }

    // Concatenate the Scope of Selected Team Members for Global Business

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == Prg.Name){
        GB_Team = GB_Team + Tms.Entity__c + ';';
         }
         
     // Concatenate the Scope of Selected Team Members for Power Region

    if(TMs.Team_Name__c == System.Label.Connect_Power_Region && TMs.Program_Name__c == Prg.Name){
        PR_Team = PR_Team + Tms.Entity__c + ';';
         }
         
     // Concatenate the Scope of Selected Team Members for GSC

    if(TMs.Team_Name__c == System.Label.Connect_Global_Functions && TMs.Program_Name__c == Prg.Name){
        GSC_Team = GSC_Team + Tms.GSC_Region__c + ';';
         }
         
    // Concatenate the Scope of Selected Team Members for Partner Region

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == Prg.Name){
        Partner_Team = Partner_Team + Tms.Partner_Region__c + ';';
         }
         
          // Concatenate the Scope of Selected Team Members for Smart Cities Region

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == Prg.Name){
        Partner_Team = Smart_Team + Tms.Smart_Cities_Division__c + ';';
         }

    } // Team For Ends
    
    // Check Scope Changes for Global Functions
    if(Prg.Global_Functions__c != null)
     For(String GF : Prg.Global_Functions__c.Split(';')){
       if(GF_Team != null)
        if(!(GF_Team.Contains(GF)))
           GFn = 1;
         }
    
    // Check Scope Changes for Global Business
    if(Prg.Global_Business__c != null)
     For(String GB : Prg.Global_Business__c.Split(';')){
       if(GB_Team != null)
        if(!(GB_Team.Contains(GB)))
           GBs = 1;
         }
    // Check Scope Changes for Power Regions
    if(Prg.Power_Region__c != null)
     For(String PR : Prg.Power_Region__c.Split(';')){
     if(PR_Team != null)
        if(!(PR_Team.Contains(PR)))
           PRs = 1;
         }
         
    // Check Scope Changes for GSC Regions
    if(Prg.GSC_Regions__c != null)
     For(String GSC : Prg.GSC_Regions__c.Split(';')){
      if(GSC_Team != null)
        if(!(GSC_Team.Contains(GSC)))
           GSC_R = 1;
         }
         
          // Check Scope Changes for SCD Regions
    if(Prg.Smart_Cities_Division__c != null)
     For(String SCD : Prg.Smart_Cities_Division__c.Split(';')){
      if(Smart_Team != null)
        if(!(Smart_Team.Contains(SCD)))
           SCDs = 1;
         }
         
    // Check Scope Changes for Partner Regions
    if(Prg.Partner_Region__c != null)
     For(String PAR : Prg.Partner_Region__c.Split(';')){
      if(Partner_Team != null)
        if(!(Partner_Team.Contains(PAR)))
           PAReg = 1;
         }
 
     if ((GFn > 0) || (GBs > 0) || (PRs > 0) || (GSC_R > 0) || (PAReg > 0)|| (SCDs > 0))
        Prg.ProgramNetworkUpdate__c = True;

       } // For Ends
     }
 
         
Public Static void ValidateCorporateKPIConnectGlobalProgramBeforeUpdate(List<Project_NCP__c> Prgnew, List<Project_NCP__c> Prgold ){
System.Debug('Corporate KPI Validate Permission Starts ');
System.Debug('&&&&&' + Prgnew[0].User__c + ' ' + Prgold[0].User__c);
List<Id> lstId1 = new List<Id>();
List<Global_KPI_Targets__Share>  GlobalKPIShare = new List<Global_KPI_Targets__Share>();

// Add new Program Leaders to the Corporate KPI Target Sharing Permission

if((Prgnew[0].User__c != Prgold[0].User__c)){
 For(Global_KPI_Targets__c GKPI : [Select Id, Connect_Global_Program__c from Global_KPI_Targets__c where Connect_Global_Program__c = :Prgnew[0].id])
 {
  lstId1.add(GKPI.Id);
  Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + GKPI.Connect_Global_Program__c);
          
           gshare.ParentId = GKPI.id;           
           gshare.UserOrGroupId = Prgnew[0].User__c;          
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);           
          }
         Database.insert(GlobalKPIShare,false); 
         
         // Delete Share permission on Corporate KPI Target for removed program leaders
  
   for( Global_KPI_Targets__Share GlobalKPISharedeletePLdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId in :lstId1 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c and UserOrGroupId = :Prgold[0].User__c ]){
     if(GlobalKPISharedeletePLdelete.Parentid != null)              
      Database.delete(GlobalKPISharedeletePLdelete,false);
     }
   }        
if((Prgnew[0].Program_Leader_2__c != Prgold[0].Program_Leader_2__c)){
 For(Global_KPI_Targets__c GKPI : [Select Id, Connect_Global_Program__c from Global_KPI_Targets__c where Connect_Global_Program__c = :Prgnew[0].id])
 {
  lstId1.add(GKPI.Id);
  Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name 2' + GKPI.Connect_Global_Program__c + Prgnew[0].Program_Leader_2__c);
          
           gshare.ParentId = GKPI.id;
           gshare.UserOrGroupId = Prgnew[0].Program_Leader_2__c;          
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);           
          }
         Database.insert(GlobalKPIShare,false);     
  // Delete Share permission on Corporate KPI Target for removed program leaders
  
   for( Global_KPI_Targets__Share GlobalKPISharedeletePLdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId in :lstId1 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c and UserOrGroupId = :prgold[0].Program_Leader_2__c ]){
     if(GlobalKPISharedeletePLdelete.Parentid != null)              
      Database.delete(GlobalKPISharedeletePLdelete,false);
     }
   
  }
  // Add new Program Leader 3 to the Corporate KPI Target Sharing Permission
  
  if((Prgnew[0].Program_Leader_3__c != Prgold[0].Program_Leader_3__c)){
 For(Global_KPI_Targets__c GKPI : [Select Id, Connect_Global_Program__c from Global_KPI_Targets__c where Connect_Global_Program__c = :Prgnew[0].id])
 {
  lstId1.add(GKPI.Id);
  Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name 3' + GKPI.Connect_Global_Program__c + Prgnew[0].Program_Leader_3__c);
          
           gshare.ParentId = GKPI.id;
           gshare.UserOrGroupId = Prgnew[0].Program_Leader_3__c;          
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);           
          }
         Database.insert(GlobalKPIShare,false);     
  // Delete Share permission on Corporate KPI Target for removed program leaders
  
   for( Global_KPI_Targets__Share GlobalKPISharedeletePLdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId in :lstId1 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c and UserOrGroupId = :prgold[0].Program_Leader_3__c ]){
     if(GlobalKPISharedeletePLdelete.Parentid != null)              
      Database.delete(GlobalKPISharedeletePLdelete,false);
     }
   
  }  
  
 }   
}