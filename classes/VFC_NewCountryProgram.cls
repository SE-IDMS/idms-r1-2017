/*****************************************************************************************
    Author : Shruti Karn
    Description : For May 2013 Release to create a New Country Partner Program from Global Partner Program
             
    Updated By : Shruti Karn
                For PRM June 13 Release - 1. Br- 2624               
                                          2. BR # 2681
                                             
*****************************************************************************************/

public class VFC_NewCountryProgram
{
    public String GlobalProgramID{get;set;}
    public list<SelectOption> lstCountries {get;set;}
    public String selCountry{get;set;}
    map<Id,String> mapCountries = new map<Id,String>();
    public VFC_NewCountryProgram.VFC_NewCountryProgram(ApexPages.StandardController controller)
    {
    }
    
    public pagereference goToCountryPRGEditPage()
    {
          
          PartnerProgram__c countryPartnerPRG = new PartnerProgram__c();
          list<Country__c> lstCountry = new list<country__c>();
          list<PartnerProgram__c> lstExistingProgram = new list<PartnerProgram__c>();
          list<PartnerProgramCountries__c> lstProgramCountries = new list<PartnerProgramCountries__c>();
          lstCountries = new list<SelectOption>();
          String selCountryId;
          String selCountryName;
          
          String strFields = '';
          Savepoint sp;
          try 
          {
            GlobalProgramID = ApexPages.currentPage().getParameters().get('GlobalPrgId'); // Get the Case ID
            
            Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.PartnerProgram__c.fields.getMap();
                       
            if (mapFields != null)
            {
                for (Schema.SObjectField ft : mapFields.values())// loop through all field tokens (ft)
                { 
                     strFields += ft.getDescribe().getName()+',';
                   
                }
            }
            
            if (strFields.endsWith(','))
            {
                strFields = strFields.substring(0,strFields.lastIndexOf(','));
            }
         
            String globalPRGQuery = 'Select '+ strFields + ' FROM PartnerProgram__c WHERE id = :GlobalProgramID limit 1';
            PartnerProgram__c globalPRG = database.query(globalPRGQuery );
            if(globalPRG != null)
            {
                if(globalPRG.ProgramStatus__C != Label.CLMAY13PRM47)
                {
                    ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLMAY13PRM46);
                    ApexPages.addmessage(msg3); 
                    return null;
                }
            }
            User currentUser = [Select Country__c,ProgramAdministrator__c,ProgramOwner__c from User where id =:UserInfo.getUserId() limit 1];
            ID userID = UserInfo.getUserId(); //apr14
            System.Debug('**Selected Country: ' + selCountry);
            if(selCountry != null)
            {
                lstExistingProgram = [Select id from PartnerProgram__c where globalpartnerprogram__c = :GlobalProgramID and country__c =:selCountry  limit 1];
            }
            else
            {
                if(currentUser.Country__c != null)
                {
                    lstCountry = [Select id,Name,countrycode__c from Country__c where Countrycode__c =: currentUser.Country__c limit 1];
                }
                if(!lstCountry.isEmpty() && lstCountry.size() == 1 )
                {
                    lstExistingProgram = [Select id from PartnerProgram__c where globalpartnerprogram__c = :GlobalProgramID and country__c =:lstCountry[0].id  limit 1];
                }
            }
               
            //if(!lstExistingProgram.isEmpty() && currentUser.ProgramAdministrator__c && selCountry == null)
         /*   if(currentUser.ProgramAdministrator__c && selCountry == null)
            {
                set<Id> setCountryID = new set<ID>();
                list<PartnerProgramCountries__c> lstCountryProgramCountries = [Select id,name,country__c from PartnerProgramCountries__c where partnerprogram__r.globalpartnerprogram__c !=null and partnerprogram__r.globalpartnerprogram__c=:GlobalProgramID limit 1000];
                for(PartnerProgramCountries__c countryProgram : lstCountryProgramCountries)
                    setCountryID.add(countryProgram.country__c);
                lstProgramCountries = [Select id,name,country__c,country__r.name from PartnerProgramCountries__c where partnerprogram__c =:GlobalProgramID and  country__c not in:setCountryID limit 1000];
                for(PartnerProgramCountries__c country : lstProgramCountries)
                {
                    lstCountries.add(new SelectOption(country.country__c,country.country__r.name));
                    mapCountries.put(country.country__c,country.country__r.name);
                }
                if(lstProgramCountries.isempty())
                {
                    ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR,'No more Country Programs can be created');
                    ApexPages.addmessage(msg3); 
                }
                return null;
            }
        */
           
           System.debug('******* Prog Administrator - >'+currentUser.ProgramAdministrator__c );
           System.debug('******* Prog Owner - >'+currentUser.ProgramOwner__c );
           System.debug('*** GlobalProgramID:' + GlobalProgramID);
            //START: Release APR14
             if((currentUser.ProgramAdministrator__c || currentUser.ProgramOwner__c) && selCountry == null)
            {
                set<Id> setCountryID = new set<ID>();
                list<PartnerProgramCountries__c> lstCountryProgramCountries = new list<PartnerProgramCountries__c>(); //apr14
                
                // Get the list of existing Country / Cluster Programs
                if (currentUser.ProgramAdministrator__c)             
                    lstCountryProgramCountries = [Select id,name,country__c from PartnerProgramCountries__c 
                                                   where partnerprogram__r.globalpartnerprogram__c !=null and 
                                                         partnerprogram__r.globalpartnerprogram__c=:GlobalProgramID limit 1000];
                else
                    lstCountryProgramCountries = [Select id,name,country__c from PartnerProgramCountries__c 
                                                   where partnerprogram__r.globalpartnerprogram__c !=null and 
                                                         partnerprogram__r.globalpartnerprogram__c=:GlobalProgramID and
                                                         partnerprogram__r.ownerId = :userID limit 1000];  //Modified with OCT14 Release
                
                System.debug('********* List of Countries ->'+lstCountryProgramCountries.size()); 
                
                /*
                if(lstCountryProgramCountries.isEmpty())
                {   ApexPages.Message msg31 = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLAPR14PRM34);
                    ApexPages.addmessage(msg31); 
                    return null;
                }
                */

                for(PartnerProgramCountries__c countryProgram : lstCountryProgramCountries)
                    setCountryID.add(countryProgram.country__c);

                if (currentUser.ProgramAdministrator__c)
                    lstProgramCountries = [SELECT id,name,country__c,country__r.name FROM PartnerProgramCountries__c WHERE partnerprogram__c =:GlobalProgramID AND country__c NOT IN :setCountryID ORDER BY Country__r.Name LIMIT 1000];
                else
                    lstProgramCountries = [SELECT id,name,country__c,country__r.name FROM PartnerProgramCountries__c WHERE partnerprogram__c =:GlobalProgramID 
                                            AND PartnerProgram__r.ownerId = :userID AND Country__c NOT IN :setCountryID ORDER BY Country__r.Name LIMIT 1000]; //Modified with OCT14 Release

                if(lstProgramCountries.isempty()) {
                    ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLAPR14PRM34 + 'OR No more Country Programs can be created ');
                    ApexPages.addmessage(msg3); 
                    return null;
                }

                for(PartnerProgramCountries__c country : lstProgramCountries)
                {
                    lstCountries.add(new SelectOption(country.country__c,country.country__r.name));
                    mapCountries.put(country.country__c,country.country__r.name);
                }

                return null;
            }
               
           //END: Release APR14
           
            if(!lstExistingProgram.isEmpty() )
            {
                ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLMAY13PRM27);
                ApexPages.addmessage(msg2); 
                return null;
            }
            countryPartnerPRG = globalPRG.clone(false,true);
            system.debug('mapCountries:'+mapCountries);
            if((!lstCountry.isEmpty() && lstCountry.size() == 1) ||  selCountry != null)
            {
                //lstProgramCountries = [Select id from PartnerProgramCountries__c where country__r.countrycode__c =: lstCountry[0].countrycode__c and partnerprogram__c =:GlobalProgramID  limit 1];
                
                //if(!lstProgramCountries.isEmpty())
                if(selCountry != null)
                {
                    selCountryId = selCountry;
                    selCountryName = mapCountries.get(selCountry);
                }
                else
                {
                    selCountryId = lstCountry[0].Id;
                    selCountryName = lstCountry[0].Name;
                }
                system.debug('selCountryId :'+selCountryId );
                system.debug('selCountryName :'+selCountryName );
                
                if(globalPRG.TECH_CountriesId__c != null && globalPRG.TECH_CountriesId__c.contains(selCountryId))
                {
                    countryPartnerPRG.Country__c = selCountryId;
                    countryPartnerPRG.Name = countryPartnerPRG.Name + ' - ' +selCountryName;
                }
                else
                {
                    ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLMAY13PRM12);
                    ApexPages.addmessage(msg1); 
                    return null;
                }
            }
            else
            {
                ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLMAY13PRM13);
                ApexPages.addmessage(msg1); 
                return null;
            }
            
            //for June 2013 PRM Release . BR # 2681
            String PRGtype = ApexPages.currentPage().getParameters().get('type');

            if(PRGtype == Label.CLJUN13PRM07)
                countryPartnerPRG.recordtypeid = Label.CLMAY13PRM05;
            else if(PRGtype == Label.CLJUN13PRM08)
            {
                countryPartnerPRG.recordtypeid = Label.CLJUN13PRM09;
                countryPartnerPRG.TECH_CountriesId__c = selCountryId;
            }   
            countryPartnerPRG.GlobalPartnerProgram__c = GlobalProgramID;
            countryPartnerPRG.ProgramStatus__c = Label.CLMAY13PRM02;
            countryPartnerPRG.GlobalProgramOwner__c  = globalPRG.ownerId;
            countryPartnerPRG.ownerid = userinfo.getUserID();
            countryPartnerPRG.TECH_MigrationUniqueID__c = '';
            sp = Database.setSavepoint();
            insert countryPartnerPRG;
            //For PRM June 13 Release - Br- 2624
            PartnerProgramCountries__c partnerCountry = new PartnerProgramCountries__c();
            partnerCountry.PartnerProgram__C = countryPartnerPRG.Id;
            partnerCountry.Country__c = selCountryId;
            insert partnerCountry;
            
              
              
            list<PartnerProgramClassification__c> lstProgramClassification = new list<PartnerProgramClassification__c>();
            list<PartnerProgramClassification__c> lstNewProgramClassification = new list<PartnerProgramClassification__c>();
            lstProgramClassification = [Select id, ClassificationLevelCatalog__c from PartnerProgramClassification__c where PartnerProgram__c = :GlobalProgramID limit 1000];
            if(!lstProgramClassification.isEmpty())
                for(PartnerProgramClassification__c prgclassification : lstProgramClassification)
                {
                    PartnerProgramClassification__c newPrgClassification = new PartnerProgramClassification__c();
                    newPrgClassification.ClassificationLevelCatalog__c = prgclassification.ClassificationLevelCatalog__c;
                    newPrgClassification.PArtnerProgram__C = countryPartnerPRG.Id;
                    lstNewProgramClassification.add(newPrgClassification);
                }
            if(!lstNewProgramClassification.isEmpty())
                insert lstNewProgramClassification;
                
            list<PartnerProgramMarket__c> lstProgramMarket = new list<PartnerProgramMarket__c>();
            list<PartnerProgramMarket__c> lstNewProgramMarket = new list<PartnerProgramMarket__c>();
            lstProgramMarket = [Select id, MarketSegmentCatalog__c from PartnerProgramMarket__c where PartnerProgram__c = :GlobalProgramID limit 1000];
            if(!lstProgramMarket .isEmpty())
                for(PartnerProgramMarket__c prgMarket : lstProgramMarket )
                {
                    PartnerProgramMarket__c newPrgMarket = new PartnerProgramMarket__c();
                    newPrgMarket.MarketSegmentCatalog__c = prgMarket.MarketSegmentCatalog__c;
                    newPrgMarket.PArtnerProgram__C = countryPartnerPRG.Id;
                    lstNewProgramMarket.add(newPrgMarket);
                }
            if(!lstNewProgramMarket.isEmpty())
                insert lstNewProgramMarket;
            //Srinivas -- Added domains of expertise - Oct13 PRM Release
            list<ProgramDomainsOfExpertise__c> lstProgramDomains = new list<ProgramDomainsOfExpertise__c>();
            list<ProgramDomainsOfExpertise__c> lstNewProgramDomains = new list<ProgramDomainsOfExpertise__c>();
            lstProgramDomains = [Select id, DomainsOfExpertiseCatalog__c from ProgramDomainsOfExpertise__c where PartnerProgram__c = :GlobalProgramID limit 1000];
            if(!lstProgramDomains .isEmpty())
                for(ProgramDomainsOfExpertise__c prgDomain : lstProgramDomains )
                {
                    ProgramDomainsOfExpertise__c newPrgDomain = new ProgramDomainsOfExpertise__c();
                    newPrgDomain.DomainsOfExpertiseCatalog__c = prgDomain.DomainsOfExpertiseCatalog__c;
                    newPrgDomain.PArtnerProgram__C = countryPartnerPRG.Id;
                    lstNewProgramDomains.add(newPrgDomain);
                }
            if(!lstNewProgramDomains.isEmpty())
                insert lstNewProgramDomains;
            //End of Srinivas Oct13 PRM    

            //Srinivas -- Added Program Products - apr 14 PRM Release
            list<ProgramProduct__c> lstProgramProducts = new list<ProgramProduct__c>();
            list<ProgramProduct__c> lstNewProgramProducts = new list<ProgramProduct__c>();
            lstProgramProducts = [Select id, ProductLineCatalog__c from ProgramProduct__c where PartnerProgram__c = :GlobalProgramID limit 1000];
            if(!lstProgramProducts .isEmpty())
                for(ProgramProduct__c prgProd : lstProgramProducts )
                {
                    ProgramProduct__c newPrgProd = new ProgramProduct__c();
                    newPrgProd.ProductLineCatalog__c = prgProd.ProductLineCatalog__c;
                    newPrgProd.PArtnerProgram__C = countryPartnerPRG.Id;
                    lstNewProgramProducts.add(newPrgProd);
                }
            if(!lstNewProgramProducts.isEmpty())
                insert lstNewProgramProducts;
            //End of Srinivas Apr PRM 

            String error = createChildRecords(countryPartnerPRG , GlobalProgramID);
            if(error != null && error.trim() != '')
            {
                Database.rollback(sp);
                //ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR,error );
                //ApexPages.addmessage(msg2); 
                return null;
            }   
            system.debug('no error'); 
            PageReference countryPRGPage = new PageReference('/'+countryPartnerPRG.Id );
            return countryPRGPage ;
          }       
          catch(Exception e)
          {
                system.debug('Excpetion:'+e);
                Database.rollback(sp);
                //for (Integer i = 0; i < e.getNumDml(); i++) 
                //{ 
                    system.debug(e.getMessage());
                    String er = e.getMessage(); 
                    er =  er.substring(er.indexof('EXCEPTION') > 1 ? er.indexof('EXCEPTION')+10 : 0,  er.indexof('[]]') > 1 ? er.indexof('[]'): er.length()); 
                    //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, er,'')); 
                    System.debug(e.getMessage()); 
               // } 
                return null;
          }
    }
    
      public static String createChildRecords(PartnerProgram__c countryPartnerPRG , ID GlobalProgramID )
      {
        map<Id, Id> mapProgramLevel = new map<Id,Id>();
        list<Id> lstGlobalPRGID = new list<Id>();
                    
        /******************** Program Level*****************************************/
        list<ProgramLevel__c> lstGlobalProgramLevel = new list<ProgramLevel__c>();
        list<ProgramLevel__c> lstCountryProgramLevel = new list<ProgramLevel__c>();
        lstGlobalProgramLevel = [Select id, name, PartnerProgram__c, PointsRequired__c, Description__c, Hierarchy__c, ModifiablebyCountry__c, SEID__c FROM ProgramLevel__c where PartnerProgram__c=: GlobalProgramID and LevelStatus__c =: Label.CLMAY13PRM19  limit 10000];
           
        for(ProgramLevel__c globalPGMLevel: lstGlobalProgramLevel)
        {
            ProgramLevel__c newCountryPGMLevel = new ProgramLevel__c();
            newCountryPGMLevel.name = globalPGMLevel.name;
            newCountryPGMLevel.LevelStatus__c = Label.CLMAY13PRM02;
            newCountryPGMLevel.recordtypeid = Label.CLMAY13PRM03;
            newCountryPGMLevel.Description__c = globalPGMLevel.Description__c;
            newCountryPGMLevel.Hierarchy__c = globalPGMLevel.Hierarchy__c;
            newCountryPGMLevel.ModifiablebyCountry__c = globalPGMLevel.ModifiablebyCountry__c;
            newCountryPGMLevel.SEID__c = globalPGMLevel.SEID__c;
            newCountryPGMLevel.GlobalProgramLevel__c = globalPGMLevel.Id;
            newCountryPGMLevel.PartnerProgram__c = countryPartnerPRG.Id;
            newCountryPGMLevel.PointsRequired__c = globalPGMLevel.PointsRequired__c;
            
            lstCountryProgramLevel.add(newCountryPGMLevel);
        }
         
        try
        {
            insert lstCountryProgramLevel;
        }
        catch(Exception e)
        {
            system.debug('Exception in inserting Country Levels:'+e.getMessage());
            //countryPartnerPRG.addError(Label.CLMAY13PRM12);
            //return Label.CLMAY13PRM28;
            //return e.getMessage();
            String er = e.getMessage(); 
            return er.substring(er.indexof('EXCEPTION') > 1 ? er.indexof('EXCEPTION')+10 : 0,  er.indexof('[]]') > 1 ? er.indexof('[]'): er.length()); 
        }
        if(!lstCountryProgramLevel.isEmpty())
        {
            lstCountryProgramLevel = [Select id, GlobalProgramLevel__c from ProgramLevel__c where id in :lstCountryProgramLevel limit 10000];
            for(ProgramLevel__c prgLevel : lstCountryProgramLevel)
                mapProgramLevel.put(prgLevel.GlobalProgramLevel__c , prgLevel.id);
        }
         /******************* Program Level Brands: 03-May-2016: BR-9587 ***********************************/
          System.debug('**lstGlobalProgramLevel.isEmpty()**'+lstGlobalProgramLevel.isEmpty());
        try
        {
          if(!lstGlobalProgramLevel.isEmpty())
          {
              for(ProgramLevel__c globalPGMLevel: lstGlobalProgramLevel)
              {
                  
                  list <ProgramLevelBrand__c>   lstGlobalProgramLevelBrands= new list<ProgramLevelBrand__c>();
                  lstGlobalProgramLevelBrands=[SELECT  name,Brand__c FROM ProgramLevelBrand__c WHERE ProgramLevel__c =: globalPGMLevel.Id ];
                  list <ProgramLevelBrand__c>   lstCountryProgramLevelBrands= new list<ProgramLevelBrand__c>();
                  System.debug('**lstGlobalProgramLevelBrands.isEmpty()**'+lstGlobalProgramLevelBrands.isEmpty());
         
                  if(!lstGlobalProgramLevelBrands.isEmpty())
                  {      System.debug('**lstGlobalProgramLevelBrands**'+lstGlobalProgramLevelBrands);

                      FOR (ProgramLevelBrand__c  ProgLevelBrand : lstGlobalProgramLevelBrands)
                      {       ProgramLevelBrand__c newProgLevelBrand = new ProgramLevelBrand__c();
                              newProgLevelBrand.Brand__c=ProgLevelBrand.Brand__c;
                              newProgLevelBrand.name=ProgLevelBrand.name;
                              newProgLevelBrand.ProgramLevel__c=mapProgramLevel.get(globalPGMLevel.ID);
                              lstCountryProgramLevelBrands.add(newProgLevelBrand);
                      }
                           System.debug('**lstCountryProgramLevelBrands**'+lstCountryProgramLevelBrands);
                           insert lstCountryProgramLevelBrands;
                  }  
             }
          } 
       
        }
        catch(Exception e)
        {
            system.debug('Exception in inserting Programlevel Brands for Country/Cluster program:'+e.getMessage());
             String er = e.getMessage(); 
             return er.substring(er.indexof('EXCEPTION') > 1 ? er.indexof('EXCEPTION')+10 : 0,  er.indexof('[]]') > 1 ? er.indexof('[]'): er.length()); 
        }
    /*********Change made for BR-9587 Ends here****************/
          
        /******************** Program Feature*****************************************/
        list<ProgramFeature__c> lstGlobalPRGFeature = new list<ProgramFeature__c>();
        list<ProgramFeature__c> lstCountryPRGFeature = new list<ProgramFeature__c>();
        lstGlobalPRGFeature = [Select id, name ,PartnerProgram__c, FeatureCatalog__c, ProgramLevel__c from ProgramFeature__c where PartnerProgram__c =:GlobalProgramID and FeatureStatus__c =: Label.CLMAY13PRM09 and ProgramLevel__r.LevelStatus__c =:Label.CLMAY13PRM19 limit 10000];
        for(ProgramFeature__c globalPGMFeature : lstGlobalPRGFeature)
        {
            ProgramFeature__c newCountryPGMFTR = new ProgramFeature__c();
            newCountryPGMFTR.FeatureStatus__c = Label.CLMAY13PRM02;
            newCountryPGMFTR.recordtypeid = Label.CLMAY13PRM04;
            newCountryPGMFTR.FeatureCatalog__c = globalPGMFeature.FeatureCatalog__c;
            newCountryPGMFTR.ProgramLevel__c = mapProgramLevel.get(globalPGMFeature.ProgramLevel__c);
            newCountryPGMFTR.PartnerProgram__c = countryPartnerPRG.Id;
            newCountryPGMFTR.GlobalProgramFeature__c = globalPGMFeature.Id;
            lstCountryPRGFeature.add(newCountryPGMFTR);
            
        }
        
         /******************** Program Requirement*****************************************/
        list<ProgramRequirement__c> lstGlobalProgramReq = new list<ProgramRequirement__c>();
        list<ProgramRequirement__c> lstCountryProgramReq = new list<ProgramRequirement__c>();
        lstGlobalProgramReq = [Select id, name ,PartnerProgram__c, Certification__c, ProgramLevel__c, RequirementCatalog__c, RecordTypeId from ProgramRequirement__c 
                                where PartnerProgram__c =:GlobalProgramID and Active__c = true and ProgramLevel__r.LevelStatus__c =:Label.CLMAY13PRM19 limit 10000];
        for(ProgramRequirement__c globalPGMReq : lstGlobalProgramReq)
        {
            ProgramRequirement__c newCountryPGMREQ = new ProgramRequirement__c();
            newCountryPGMREQ.Active__c = false;
            newCountryPGMREQ.Certification__c = globalPGMReq.Certification__c;
            newCountryPGMREQ.ProgramLevel__c = mapProgramLevel.get(globalPGMReq.ProgramLevel__c);
            newCountryPGMREQ.RequirementCatalog__c = globalPGMReq.RequirementCatalog__c;
            //newCountryPGMREQ.Target__c = globalPGMReq.Target__c;
            newCountryPGMREQ.RecordTypeId = globalPGMReq.recordtypeid ;
            //newCountryPGMREQ.recordtypeid = Label.CLMAY13PRM14;
            newCountryPGMREQ.PartnerProgram__c = countryPartnerPRG.Id;
            newCountryPGMREQ.GlobalProgramRequirement__c = globalPGMReq.Id;
            lstCountryProgramReq.add(newCountryPGMREQ);
            
        }
        
        
        try
        {
            insert lstCountryPRGFeature;
            insert lstCountryProgramReq;
        }
        catch(Exception e)
        {
            system.debug('Exception in inserting Country Feature/Country Requirement:'+e.getMessage());
            //countryPartnerPRG .addError(Label.CLMAY13PRM12);
            //return null;
            //return Label.CLMAY13PRM28;
            String er = e.getMessage(); 
            return er.substring(er.indexof('EXCEPTION') > 1 ? er.indexof('EXCEPTION')+10 : 0,  er.indexof('[]]') > 1 ? er.indexof('[]'): er.length()); 
        }
        return '';
    }
}