@isTest(SeeAllData=false) 
global class VFC_LiveAgent_PrechatForm_Global_TEST{
    
    static testMethod void testinsertionVFC_LiveAgent_PrechatForm() {
        /* Create a profil of custom settings */
        CS_LiveAgent_PrechatFormSettings__c config = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'FR-Test',
            CountryCode__c = 'FR', 
            ChatAudience__c = 'Test',
            isRoutingMenuVisible__c = false
        );
        insert config;
        
        /* Create some routing rules */
        /*
        List<CS_LiveAgent_FormRoutingRule__c> listRoutings = new List<CS_LiveAgent_FormRoutingRule__c>();
        for(Integer i=0 ; i<1000 ; i++){
            if( i<10 || i>=990 ){
                CS_LiveAgent_FormRoutingRule__c routing = new CS_LiveAgent_FormRoutingRule__c(
                    Name = 'FR-Test-Product'+i,
                    CountryCode__c = 'FR', 
                    ChatAudience__c = 'Test',
                    RoutingLabel__c = 'Product'+i, 
                    ChatButtonID__c = 'Testbutton'
                );
                listRoutings.add(routing);
            } else {                
                CS_LiveAgent_FormRoutingRule__c routing = new CS_LiveAgent_FormRoutingRule__c(
                    Name = 'ZE-Test-Product'+i,
                    CountryCode__c = 'ZE', 
                    ChatAudience__c = 'Test',
                    RoutingLabel__c = 'AProduct'+i, 
                    ChatButtonID__c = 'Testbutton'
                );
                listRoutings.add(routing);
            }
        }
        insert listRoutings;
		*/
        
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_Global;
        Test.setCurrentPage(vfPage);
        
        // Test 1 : Pass an existing config name in parameter
        VFC_LiveAgent_PrechatForm_Global con = new VFC_LiveAgent_PrechatForm_Global();        
        con.configName = '';
        con.getConfig();
        system.assertEquals(TRUE, con.hasConfigNameButNotFound);
        
        con.configName = 'FR-Test';
        con.getConfig();
        con.getlst_configRoutingRules();
        system.assertEquals(TRUE, con.hasFoundConfig);
        
        // Test 2 : Pass an unknown config name in parameter with a default form. 
        config = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'Default',
            CountryCode__c = 'NA', 
            ChatAudience__c = 'NA'
        );
        insert config;
        
        con = new VFC_LiveAgent_PrechatForm_Global();
        con.configName = 'unknown';
        con.getConfig();
        system.assertEquals(TRUE, con.hasFoundConfig);
                
        con.configName = '';
        con.getConfig();
        system.assertEquals(TRUE, con.hasFoundConfig);
        system.assertEquals('Default', con.config.Name);
        
        con.saveConfigController();
        con.getlst_categories();
    }
    
    static testMethod void testinsertionVFC_LiveAgent_PrechatForm2() {
        /* Create a profil of custom settings */
        CS_LiveAgent_PrechatFormSettings__c config = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'FR-Test',
            CountryCode__c = 'FR', 
            ChatAudience__c = 'Test',
            IsRoutingMenuVisible__c = true
        );
        insert config;
        
        /* Create some routing rules */
        List<CS_LiveAgent_FormRoutingRule__c> listRoutings = new List<CS_LiveAgent_FormRoutingRule__c>();
        for(Integer i=0 ; i<100 ; i++){
            if( i<10 || i>=90 ){
                CS_LiveAgent_FormRoutingRule__c routing = new CS_LiveAgent_FormRoutingRule__c(
                    Name = 'FR-Test-'+i,
                    CountryCode__c = 'FR', 
                    ChatAudience__c = 'Test',
                    RoutingLabel__c = 'Product'+i, 
                    ChatButtonID__c = 'Testbutton'
                );
                listRoutings.add(routing);
            } else {                
                CS_LiveAgent_FormRoutingRule__c routing = new CS_LiveAgent_FormRoutingRule__c(
                    Name = 'FR-Test-'+i,
                    CountryCode__c = 'FR', 
                    ChatAudience__c = 'Test',
                    RoutingLabel__c = 'AProduct'+i, 
                    ChatButtonID__c = 'Testbutton',
                    Group__c = 'Group 1'
                );
                listRoutings.add(routing);
            }
        }
        insert listRoutings;
        
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_Global;
        Test.setCurrentPage(vfPage);
        
        VFC_LiveAgent_PrechatForm_Global con = new VFC_LiveAgent_PrechatForm_Global(); 
        con.configName = 'FR-Test';
        con.getConfig();
        con.getlst_configRoutingRules();
        system.assertEquals(TRUE, con.hasFoundConfig);
    }
    
    static testMethod void testinsertionVFC_LiveAgent_PrechatForm3() {
        /* Create a profil of custom settings */
        CS_LiveAgent_PrechatFormSettings__c config = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'FR-Test',
            CountryCode__c = 'FR', 
            ChatAudience__c = 'Test',
            IsRoutingMenuVisible__c = true,
            IsCategoryVisible__c = true,
            CategoryValues__c = 'value 1;value2;'
        );
        insert config;
        
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_Global;
        Test.setCurrentPage(vfPage);
        
        VFC_LiveAgent_PrechatForm_Global con = new VFC_LiveAgent_PrechatForm_Global(); 
        con.configName = 'FR-Test';
        con.getConfig();
        con.getlst_configRoutingRules();
        con.getlst_categories();
        system.assertEquals(TRUE, con.hasFoundConfig);
    }
}