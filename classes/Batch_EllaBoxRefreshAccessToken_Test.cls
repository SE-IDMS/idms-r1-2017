@isTest
                                
global class Batch_EllaBoxRefreshAccessToken_Test implements WebServiceMock {
    
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {} 
           
     @isTest(seeAllData=true) static void unitTest() {
        // The query used by the batch job.
        String query = 'select AccessToken__c,BoxAccessTokenExpires__c,BoxRefreshTokenExpire__c,RefreshToken__c from CS_ELLABoxAccessToken__c';
        
       /*CS_ELLABoxAccessToken__c cs = new CS_ELLABoxAccessToken__c(Name='Admin',AccessToken__c='DprxSfbropcMvXDOiftQOQYHe2ko516u',
       RefreshToken__c ='ub2Kfgp6mBqQjixI6RWcNVnf45z2CrGz3HJ729JUlahDC8GEvWuu9kS0x1qz6qMO' ,
       BoxAccessTokenExpires__c=datetime.now().addMinutes(-45),BoxRefreshTokenExpire__c=datetime.now().adddays(60) );
          
       insert cs;*/
       
        // This causes a fake response to be generated
        Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
        
        // Call the method that invokes a callout
        //String output = WebSvcCallout.callEchoString('Hello World!');
        

     //  Test.startTest();
       Batch_EllaBoxRefreshAccessToken c = new Batch_EllaBoxRefreshAccessToken();
       Database.executeBatch(c);
       Batch_ELLABoxRefreshAccessToken.boxResponseObject boxRes = new Batch_ELLABoxRefreshAccessToken.boxResponseObject('abc', 'abc');
       
       //Test.stopTest();

    }
    
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setBody('grant_type=refresh_token&refresh_token=ub2Kfgp6mBqQjixI6RWcNVnf45z2CrGz3HJ729JUlahDC8GEvWuu9kS0x1qz6qMO&client_id=v363zs5973gb9stitownjzq4jshn5w2m&client_secret=AsW2HfisiH5cE8Gj6mdQQ89N11bIZJa1');
            res.setStatusCode(200);
        return res;
    }
    static testMethod void myUnitTest_schedule() {
            Test.startTest();
                    String CRON_EXP = '0 00 * * * ?';
                   Schedule_EllaBoxRefreshAccessToken sch = new Schedule_EllaBoxRefreshAccessToken();
                   String jobId = system.schedule('test - ELLA Box Batch Class', CRON_EXP, sch);
            Test.stopTest();
    }

}