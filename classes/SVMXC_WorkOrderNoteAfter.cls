public class SVMXC_WorkOrderNoteAfter {

    public static void syncWorkOrderNotesToWO (List<WorkOrderNote__c> triggerNew, Map<Id, WorkOrderNote__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to sync Work Order Notes to WO - Written : May 15, 2015
        */
        
        Set<Id> workOrderNoteIds = new Set<Id>();
        Set<Id> workOrderIds = new Set<Id>();
         
        Map<Id, List<WorkOrderNote__c>> woWorkOrderNoteMap = new Map<Id, List<WorkOrderNote__c>>();
        
        for (WorkOrderNote__c won : triggerNew) {
            // organize incoming Work Order Notes into Map to query additional ones that are not part of the save transaction
            if (won.WorkOrder__c != null) {
                
                workOrderNoteIds.add(won.Id);       
                if (!workOrderIds.contains(won.WorkOrder__c))
                    workOrderIds.add(won.WorkOrder__c);
            }
        }
        
        if (!workOrderIds.isEmpty()) {
            
            Map<Id, User> userMap = new Map<Id, User>();
            
            // need to Query for any possible other Work Order Notes related to the WOs from this transaction
            List<WorkOrderNote__c> otherWorkOrderNotes = [SELECT DisplayOrder__c, FSRUser__c, Notes__c,
                                                            WorkOrder__c, LastModifiedDate FROM WorkOrderNote__c 
                                                            WHERE WorkOrder__c IN : workOrderIds
                                                            ORDER BY DisplayOrder__c ASC NULLS LAST, LastModifiedDate DESC];
                                                            
            if (!otherWorkOrderNotes.isEmpty()) {
                
                Set<Id> userIds = new Set<Id>();        
                
                // put queried Work Order Notes into the Map and pull all unique User Ids
                for (WorkOrderNote__c won2 : otherWorkOrderNotes) {
                    woWorkOrderNoteMap = addToMap(woWorkOrderNoteMap, won2);
                    if (!userIds.contains(won2.FSRUser__c))
                        userIds.add(won2.FSRUser__c);   
                }
                
                userMap = new Map<Id, User> ([SELECT Id, TimeZoneSidKey, FirstName, LastName FROM User WHERE Id IN: userIds]);
            }
            
            // organize consolidated Notes into Map
            Map<Id, String> woNotesMap = new Map<Id, String>();
            
            for (Id woId : woWorkOrderNoteMap.keySet()) {
                
                String workingNote = createNote(woWorkOrderNoteMap.get(woId), userMap);
                woNotesMap.put(woId, workingNote);  
            }
            
            List<SVMXC__Service_Order__c> workOrdersToUpdate = new List<SVMXC__Service_Order__c>();
            
            for (Id woId2 : woNotesMap.keySet()) {
                workOrdersToUpdate.add(new SVMXC__Service_Order__c(Id=woId2, FSRNotes__c = woNotesMap.get(woId2)));
            }
            
            if (!workOrdersToUpdate.isEmpty())
                update workOrdersToUpdate;
        }
    }
    
    public static String createNote (List<WorkOrderNote__c> incomingNotes, Map<Id, User> userMap) {
        
        String returnString='';
        
        for (WorkOrderNote__c won : incomingNotes) {
            returnString += '---------------------------------\n';
            System.debug('userMap ' + userMap);
            System.debug('won.FSRUser__c ' + won.FSRUser__c);
            returnString += userMap.get(won.FSRUser__c).FirstName + ' ' + userMap.get(won.FSRUser__c).LastName + '\n';          
            String strConvertedDate = won.LastModifiedDate.format('MM/dd/yyyy HH:mm:ss', userMap.get(won.FSRUser__c).TimeZoneSidKey);           
            returnString += strConvertedDate + ' (' + userMap.get(won.FSRUser__c).TimeZoneSidKey + ')\n';           
            returnString += won.Notes__c + '\n';
        }
        
        return returnString;
    
    }
    
    public static Map<Id, List<WorkOrderNote__c>> addToMap(Map<Id, List<WorkOrderNote__c>> incomingMap, 
                                                            WorkOrderNote__c incomingNote) {
        
        List<WorkOrderNote__c> workingWONList = new List<WorkOrderNote__c>();
        Map<Id, List<WorkOrderNote__c>> returnMap = new Map<Id, List<WorkOrderNote__c>>();
        returnMap = incomingMap;
        
        if (returnMap.containsKey(incomingNote.WorkOrder__c)) {
            
            // add additional note to the Map
            WorkOrderNote__c workingNote1 = new WorkOrderNote__c();
            workingNote1 = incomingNote;
            workingWONList = returnMap.get(incomingNote.WorkOrder__c);
            workingWONList.add(workingNote1);
            returnMap.put(incomingNote.WorkOrder__c, workingWONList);
                
        } else {
            
            // add new note for WO
            WorkOrderNote__c workingNote2 = new WorkOrderNote__c();
            workingNote2 = incomingNote;
            workingWONList.add(workingNote2);
            returnMap.put(incomingNote.WorkOrder__c, workingWONList);
        }
        
        return returnMap;
    }
}