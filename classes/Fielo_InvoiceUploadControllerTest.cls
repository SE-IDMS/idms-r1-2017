/************************************************************
* Developer: Marco Paulucci                                 *
* Type: Test Class                                          *
* Created Date: 22.09.2014                                  *
* Tested Class: Fielo_InvoiceUploadController               *
************************************************************/

@isTest
public with sharing class Fielo_InvoiceUploadControllerTest{
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
        insert deactivate;
        
        Account acct = new Account(Name = 'Polo Marco', Street__c = 'Calle Falsa 123', ZipCode__c = '666333');
        insert acct;
        
        Country__c cun = new Country__c(Name = 'United Arab Emirates', CountryCode__c = 'AE');
        insert cun;
        
        FieloEE__Member__c mem = new FieloEE__Member__c(FieloEE__LastName__c= 'Marco', FieloEE__FirstName__c = 'Polo', F_Country__c = cun.id, FieloEE__Email__c = 'test@test.test', F_CompanyName__c = 'TestCompany');
        insert mem;
        
        FieloEE.MemberUtil.setMemberId(mem.Id);
        
        Fielo_InvoiceUploadController TL = new Fielo_InvoiceUploadController();
        TL.doSubmit();
        
        Document doc = new Document(Name = TL.randomAttachmentName + '.test', FolderId = UserInfo.getUserId());
        insert doc;
        
        TL.doSubmit();
        
        Fielo_InvoiceUploadController JG = new Fielo_InvoiceUploadController();
        JG.isFirstTime = true;
        JG.doSubmit();
        
        Document docL = new Document(Name = JG.randomAttachmentName + '.test', FolderId = UserInfo.getUserId());
        insert docL;
        JG.doSubmit();

    }
}