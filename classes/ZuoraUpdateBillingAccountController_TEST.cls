/**
 * @author Benjamin LAMOTTE
 * @date Creation 23/09/2015
 * @description Test class for ZuoraUpdateBillingAccountController
 */
@isTest (SeeAllData=true)
private class ZuoraUpdateBillingAccountController_TEST {


    /**
     * @author Benjamin LAMOTTE
     * @date Creation 21/09/2015
     * @description Test method for all methods in the tested class
     */
    static testMethod void testAllMethods(){

        //Country__c accountCountry = Utils_TestMethods.createCountry();
        //insert accountCountry;

        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }

        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.CurrencyIsoCode = 'USD';
        //objAccount.country__c = accountCountry.id;
        objAccount.country__c = country.Id;
        insert objAccount;

        Zuora__CustomerAccount__c zBillingAccount1 = testObjectCreator.CreateBillingAccount('TestBA1', 'ContactName', objAccount.Id);
        zBillingAccount1.Zuora__BillToCountry__c = 'United States';
        zBillingAccount1.Zuora__BillToWorkEmail__c = 'jd@test.com';
        zBillingAccount1.Zuora__BillToWorkPhone__c = '010203040506';
        update zBillingAccount1;

        Zuora__CustomerAccount__c zBillingAccount2 = testObjectCreator.CreateBillingAccount('TestBA2', 'ContactName', objAccount.Id);
        //insert zBillingAccount2;

        Zuora__CustomerAccount__c zBillingAccount3 = testObjectCreator.CreateBillingAccount('TestBA3', 'ContactName', objAccount.Id);
        //insert zBillingAccount3;

        Test.startTest();

        //ZuoraUpdateBillingAccountController.fakeTest();

        ApexPages.StandardController stdController = new ApexPages.StandardController(zBillingAccount1);
        ZuoraUpdateBillingAccountController zuoraUpdBAController = new ZuoraUpdateBillingAccountController(stdController);
        ZuoraUpdateBillingAccountController newZuoraUpdBAController = new ZuoraUpdateBillingAccountController(stdController);


        zuoraUpdBAController.zuoraBillTo.Country = 'France';
        zuoraUpdBAController.zuoraSoldTo.Country = 'France';

        zuoraUpdBAController.updateBilligAccount();
        zuoraUpdBAController.getPaymentTermOptions();
        zuoraUpdBAController.zuoraBillingAccount.ImmediateInvoiceAccount__c = 'Yes';
        zuoraUpdBAController.getBatchOptions();
        zuoraUpdBAController.initPaymentMethod();
        zuoraUpdBAController.zuoraBillingAccount.ImmediateInvoiceAccount__c = 'No';
        zuoraUpdBAController.zuoraBillingAccount.InvoiceOwner__c = 'Y';
        zuoraUpdBAController.initBillingAccountName();
        zuoraUpdBAController.getBatchOptions();
        zuoraUpdBAController.initPaymentMethod();
        zuoraUpdBAController.getCountryOptions();
        zuoraUpdBAController.redirectToAccount();
        
        
        newZuoraUpdBAController.zuoraBillTo.Country = 'France';
        newZuoraUpdBAController.zuoraSoldTo.Country = 'France';
        
        newZuoraUpdBAController.zuoraBillingAccount.CompanyCode__c = 'SE FR';
        newZuoraUpdBAController.zuoraBillingAccount.CompanyLanguage__c = '';
        
        newZuoraUpdBAController.updateBilligAccount();
        newZuoraUpdBAController.getPaymentTermOptions();
        newZuoraUpdBAController.zuoraBillingAccount.ImmediateInvoiceAccount__c = 'Yes';
        newZuoraUpdBAController.getBatchOptions();
        newZuoraUpdBAController.initPaymentMethod();
        newZuoraUpdBAController.zuoraBillingAccount.ImmediateInvoiceAccount__c = 'No';
        newZuoraUpdBAController.zuoraBillingAccount.InvoiceOwner__c = 'Y';
        newZuoraUpdBAController.initBillingAccountName();
        newZuoraUpdBAController.getBatchOptions();
        newZuoraUpdBAController.initPaymentMethod();
        newZuoraUpdBAController.getCountryOptions();
        newZuoraUpdBAController.redirectToAccount();
        

        Test.stopTest();

        /*
        PageReference pageRef = Page.ZuoraUpdateBillingAccount;
        Test.setCurrentPage(pageRef);
        */


    }
}