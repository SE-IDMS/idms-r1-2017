global class AP_PRMAppConstants {
    global static final String CNTMODE_NEW = 'NEWCONTACT';
    global static final String CNTMODE_EXT = 'EXISTINGCONTACT'; 
    global static final String CNTMODE_UIMSEXT = 'EXISTINGUIMSCONTACT';
    global static final String ACCMODE_NEW = 'NEWACC';
    global static final String ACCMODE_EXT = 'EXTACC';
    global static final String APPL_PRTL = 'PRTNRPRTL';
    global static final String APPL_POMP = 'POMP';

    global static final String OPERATION_OK = 'success';
    global static final String OPERATION_FAIL = 'valErrors';
    
    global enum RegistrationType { 
        PartnerRegistration,
        MassRegistration 
    }
    
    global enum RegistrationValidationErrors { 
        ERR_EMAIL_BLANK,                                    // Chosen contact's email address is blank
        ERR_EMAIL_ALREADYEXISTS,                        // Chosen contact's email address already registered in SE Network
        ERR_EMAIL_UIMS_ALREADYEXISTS,

        ERR_NO_PRM_DEF_ACC,
        ERR_CHNL_NOTVALID,                              // Chosen Channel is not a valid channel
        ERR_CHNL_INFO_MISSING,
        ERR_PORTAL_NOT_DEPLOYED,
        ERR_POMP_REG_PORTAL_DEPLOYED,        // Portal is already deployed in the country for the selected channel
        ERR_PRM_CHNL_DEPLOYED,

        ERR_NOT_PRMACCOUNT, 
        ERR_ACC_INFO_REQ,                        // Chosen account is not enabled for PRM
        ERR_ACC_INVALID,
        ERR_ACC_SEL_CHNL_NOMATCH,
        ERR_ACC_OWNER_INACTIVE,
        ERR_ACC_STATE_NOTVALID,
        ERR_CNTRY_ACC_COUNTRY_NOTSAME,      // Chosen country & existing account's country do not match
        ERR_CNTRY_CHNL_INFO_MISSING,

        ERR_CONT_ID_REQUIRED,
        ERR_CNT_INVALID,

        MSG_GRANT_FULLPORTAL_ACCESS_SUCCESS,
        ERR_GRANT_FULLPORTAL_ACCESS,

        ERR_UNKNOWN,
        
        ERR_JOBTITLE_NOTVALID,
        ERR_JOBFUNC_NOTVALID,
        ERR_COUNTRY_NOTVALID,
        ERR_STATE_NOTVALID,
        ERR_EMAIL_PHONE_BLANK,
        ERR_REQFIELDS_BLANK,
        ERR_COMPANY_BLANK
    }
    
    global enum ManageInvoiceMessages {
        ERR_DUPLICATE,
        OK_NO_DUPLICATE,
        ERR_REG_RES_BLANK,
        ERR_APPROVEORREJECT,
        ERR_MANAGEPRODUCTS,
        OK_PRODUCTSUPDATED,
        OK_DEL_ATTACHMENT,
        ERR_DEL_ATTACHMENT,
        ERR_RETALER_BLANK,
        ERR_NO_PRODUCTS
    }
    
    global enum ProcessPendingQueueItems {
        REGISTRATION,
        UPDATEBFOIDINIMS,
        ASSIGN_PERMISSION_POMP,
        ASSIGN_PERMISSION_PARTNER,
        MEMBER_PROPERTIES,
        REGISTRATION_IMS,
        MASS_REGISTRATION,
        POMPV1FTR,
        PRIVATE_REGISTRATION,
        UPDATE_REG_HISTORY_TRACKING,
        UPDATE_REQUEST_QUEUE,
        UPDATE_MASS_REGISTRATION,
        GRANT_APPLICATION_ACCESS,
        ACCOUNT_TEAM
    }

    global enum ProcessRegistrationPool {
        CHANNELPOOL,
        COUNTRYPOOL,
        NOCOUNTRYNOCHANNELPOOL,
        ASSIGN_POOLPERMSET
    }
    
    global enum DebugLogType {
        Info,
        Error       
    }

    global enum ServiceEndpoints {
        PRM_UIMS_USRMGR_ADMIN,
        PRM_UIMS_CERT_NAME,
        PRM_UIMS_CALLERFID,
        PRM_UIMS_USRMGR_USER,
        PRM_UIMS_IDP_SSOINIT,
        PRM_UIMS_SSO_LOGINPOST,
        PRM_FIELO_PRG_ID,
        PRM_UIMS_CMPMGR_USER,
        PRM_UIMS_USER_SVC,
        PRM_UIMS_CMPMGR_ADMIN,
        PRM_UIMS_ADMIN_BACKEND_IO_SVC,
        PRM_UIMS_USRACCESS_SVC,
        PRM_UIMS_SLO_URL,
        PRM_UIMS_ADMIN_BACKEND_OUT_SVC,
        PRM_MKTO_LEADS,
        PRM_MKTO_OAUTH_CLIENTID,
        PRM_MKTO_OAUTH_CLIENTSECRET,
        PRM_MKTO_OAUTH_URL,
        PRM_MKTO_SVC_URL,
        PRM_MKTO_FORM_DOMAIN,
        PRM_MKTO_KEYCODE,
        PRM_MKTO_FORMID,
        PRM_MKTO_MUNCHKINID,
        PRM_PORTAL_UNAME_EXTSN,
        PRM_UIMS_CMP_SVC
    }
    
    global enum PRMTechnicalMerge {
        ACCOUNT,
        CONTACT
    }
    
    global enum REGISTRATIONFORMVIEWMODE {
        Simplified,
        Wizard
    }
    
    public static void method1(){
        System.debug('***********');
        System.debug('Method 1 ');
        System.debug('***********');
    }    
}