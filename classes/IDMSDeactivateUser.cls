/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016 
*   This class is to deactivate user. 
**/
public without sharing class IDMSDeactivateUser{
    //This method accepts user object and deactivate it.
    public static void DeactivateUser(id UserId){
        List<User> userToDeactivate = [select id, isactive from user where id = :Userid limit 1];
        if(userToDeactivate.size() > 0)
        {
            userToDeactivate[0].isActive = false;
            update userToDeactivate[0];
        }
    }
}