/*   Author: Nicolas PALITZYNE (Accenture)  
     Date Of Creation: 21/11/2011   
     Description : Class containing methods to update the RMA accordingly to the add of related products 
*/

public class AP34_RMAMailMerge
{
    // Inner class for the returned product fields of the RMA object
    public class innerReturnedProduct
    {
        RMA__c parentRMA;
        RMA_Product__c innerProduct;
        String innerProductID;
        
        // Constructor initializing attributes with the matching values
        public innerReturnedProduct(RMA_Product__c aReturnedProductsList, RMA__c aParentRMA)
        {
            if(aReturnedProductsList != null && aParentRMA != null)
            {
                innerProduct = aReturnedProductsList;
                parentRMA = aparentRMA; 
                innerProductID = aReturnedProductsList.ID;
            }     
        }
        
        // Constructor initializing attributes with the matching values
        public innerReturnedProduct(RMA__c aRMA, Integer index)
        {
            if(aRMA != null && index != null && index >0 && index <=5)
            {  
                parentRMA = aRMA;  
                innerProduct = new RMA_Product__c(); 
                 
                if(index == 1)
                {
                    innerProductID = aRMA.TECH_RP1_ID__c;
                    innerProduct.Name = aRMA.RP1_CommercialReference__c;
                    innerProduct.Quantity__c = aRMA.RP1_Quantity__c;
                    innerProduct.SerialNumber__c = aRMA.RP1_SerialNumber__c;
                    innerProduct.DateCode__c = aRMA.RP1_DateCode__c;                
                }
                else if(index == 2)
                {
                    innerProductID = aRMA.TECH_RP2_ID__c;
                    innerProduct.Name = aRMA.RP2_CommercialReference__c;
                    innerProduct.Quantity__c = aRMA.RP2_Quantity__c;
                    innerProduct.SerialNumber__c = aRMA.RP2_SerialNumber__c;
                    innerProduct.DateCode__c = aRMA.RP2_DateCode__c;                    
                }
                else if(index == 3)
                {
                    innerProductID = aRMA.TECH_RP3_ID__c;
                    innerProduct.Name = aRMA.RP3_CommercialReference__c;
                    innerProduct.Quantity__c = aRMA.RP3_Quantity__c;
                    innerProduct.SerialNumber__c = aRMA.RP3_SerialNumber__c;
                    innerProduct.DateCode__c = aRMA.RP3_DateCode__c;                    
                }
                else if(index == 4)
                { 
                    innerProductID = aRMA.TECH_RP4_ID__c;
                    innerProduct.Name = aRMA.RP4_CommercialReference__c;
                    innerProduct.Quantity__c = aRMA.RP4_Quantity__c;
                    innerProduct.SerialNumber__c = aRMA.RP4_SerialNumber__c;
                    innerProduct.DateCode__c = aRMA.RP4_DateCode__c;                   
                }
                else if(index == 5)
                {
                    innerProductID = aRMA.TECH_RP5_ID__c;
                    innerProduct.Name = aRMA.RP5_CommercialReference__c;
                    innerProduct.Quantity__c = aRMA.RP5_Quantity__c;
                    innerProduct.SerialNumber__c = aRMA.RP5_SerialNumber__c;
                    innerProduct.DateCode__c = aRMA.RP5_DateCode__c;                    
                }                                
            }         
        }        
        
        public void clear()
        {
            innerProduct = new RMA_Product__c();   
        }
                
        // Update the product with the index number in the RMA
        public void updateRMA(Integer index)
        {
            if(index != null)
            {
                if(index == 1)
                {
                    parentRMA = AP34_RMAMailMerge.updateRMAProduct1(innerProduct, parentRMA);
                    parentRMA.TECH_RP1_ID__c = innerProductID;
                }
                else if(index == 2)
                {
                    parentRMA = AP34_RMAMailMerge.updateRMAProduct2(innerProduct, parentRMA);
                    parentRMA.TECH_RP2_ID__c = innerProductID;                    
                }
                else if(index == 3)
                {
                    parentRMA = AP34_RMAMailMerge.updateRMAProduct3(innerProduct, parentRMA);
                    parentRMA.TECH_RP3_ID__c = innerProductID;                    
                }
                else if(index == 4)
                {
                    parentRMA = AP34_RMAMailMerge.updateRMAProduct4(innerProduct, parentRMA);
                    parentRMA.TECH_RP4_ID__c = innerProductID;                    
                }
                else if(index == 5)
                {
                    parentRMA = AP34_RMAMailMerge.updateRMAProduct5(innerProduct, parentRMA);
                    parentRMA.TECH_RP5_ID__c = innerProductID;                    
                }
            }           
        }
        
    }

    // Method called by the Returned Product After Insert Trigger
    Public static void addReturnedProducts(List<RMA_Product__c> aReturnedProductsList)
    {
        if(aReturnedProductsList!= null && aReturnedProductsList.size()>0)
        {
            // Retrieve the list of related RMAs and map them with their IDs
            Map<String,RMA__c> RMAIDRecordMap = buildRMAIdRecordMap(aReturnedProductsList);
             
            // Update the related RMA for every Returned Product in the lis
            List<RMA__c> RMAtoBeUpdatedList = new List<RMA__c>();
            
            For(RMA_Product__c ReturnedProducts:aReturnedProductsList)
            {
                // If the first product is not populated, enter the returned products, else try with following spots
                if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP1_ID__c == null)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct1(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));
                }     
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP2_ID__c == null)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct2(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));            
                } 
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP3_ID__c == null)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct3(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));             
                }   
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP4_ID__c == null)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct4(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));              
                }   
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP5_ID__c == null)
                {
                        RMAtoBeUpdatedList.add(updateRMAProduct5(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));          
                }                                                                      
            }
            
            // update the list of related RMAs
            List<Database.SaveResult> aDataBaseSaveResultList = database.update(RMAtoBeUpdatedList);
        }                                           
    }

    // Method called after update of Returned products in order to update the related RMA
    Public static void updateReturnedProducts(List<RMA_Product__c> aReturnedProductsList)
    {
        if(aReturnedProductsList!= null && aReturnedProductsList.size()>0)
        {    
            // Retrieve the list of related RMAs and map them with their IDs
            Map<String,RMA__c> RMAIDRecordMap = buildRMAIdRecordMap(aReturnedProductsList);
           
            // Update the related RMA for every Returned Product in the lis
            List<RMA__c> RMAtoBeUpdatedList = new List<RMA__c>();
            
            For(RMA_Product__c ReturnedProducts:aReturnedProductsList)
            {
                // If the first product is not populated, enter the returned products, else try with following spots
                if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP1_ID__c == ReturnedProducts.ID)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct1(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));
                }     
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP2_ID__c == ReturnedProducts.ID)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct2(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));           
                } 
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP3_ID__c == ReturnedProducts.ID)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct3(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));             
                }   
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP4_ID__c == ReturnedProducts.ID)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct4(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));           
                }   
                else if(RMAIDRecordMap.get(ReturnedProducts.RMA__c) != null &&  RMAIDRecordMap.get(ReturnedProducts.RMA__c).TECH_RP5_ID__c == ReturnedProducts.ID)
                {
                       RMAtoBeUpdatedList.add(updateRMAProduct5(ReturnedProducts,RMAIDRecordMap.get(ReturnedProducts.RMA__c)));            
                }                                                                      
            } 
            
            // update the list of related RMAs
            List<Database.SaveResult> aDataBaseSaveResultList = database.update(RMAtoBeUpdatedList);             
        }            
    }   

    // Method called upon the deletion of Returned Products
    Public static void removeReturnedProducts(List<RMA_Product__c> aReturnedProductsList)
    {
        // Retrieve the list of related RMAs and map them with their IDs
        Map<String,RMA__c> RMAIDRecordMap = buildRMAIdRecordMap(aReturnedProductsList);  
        
        System.debug('#### RMAIDRecordMap :' + RMAIDRecordMap );
        
        if(aReturnedProductsList!= null && aReturnedProductsList.size()>0)
        {       
            // Create the list of all RMAs to be updated
            Map<String, RMA__c> RMAtoBeUpdatedMap = buildRMAIdRecordMap(aReturnedProductsList);
            
            List<RMA__c> RMAtoBeUpdatedList = RMAtoBeUpdatedMap.values();
           
            System.debug('#### RMA to be updated:' + RMAtoBeUpdatedList);
           
            // Map the RMAs with their 5 inner products 
            Map<ID,List<innerReturnedProduct>> RMAInnerProductMap = new Map<ID,List<innerReturnedProduct>>();
            
            For(RMA__c aRMA:RMAtoBeUpdatedList)
            {
                List<innerReturnedProduct> anInnerproductList = new List<innerReturnedProduct>();
                
                For(Integer i=1; i<=5; i++)
                {
                     innerReturnedProduct anInnerProduct = new innerReturnedProduct(aRMA, i);
                     anInnerproductList.add(anInnerProduct);   
                }
                
                RMAInnerProductMap.put(aRMA.ID, anInnerproductList);
            }
            
            System.debug('#### RMA with their Inner Products Map:' + RMAInnerProductMap );
            
            // Map the RMAs with the related returned products which have been deleted
            Map<ID,List<innerReturnedProduct>> RMAdeletedProductMap = new Map<ID,List<innerReturnedProduct>>();
            
            For(RMA__c aRMA:RMAtoBeUpdatedList)
            {
                List<innerReturnedProduct> anInnerproductList = new List<innerReturnedProduct>();
            
                For(RMA_Product__c aReturnedProduct:aReturnedProductsList)
                {
                    if(aReturnedProduct.RMA__c == aRMA.ID && RMAIDRecordMap.get(aReturnedProduct.RMA__c) != null)
                    {
                         innerReturnedProduct anInnerProduct = new innerReturnedProduct(aReturnedProduct,RMAIDRecordMap.get(aReturnedProduct.RMA__c));
                         anInnerproductList.add(anInnerProduct );
                    }
                }
                
                System.debug('#### List of Inner Products: ' + anInnerproductList);
                
                RMAdeletedProductMap.put(aRMA.ID,anInnerproductList);                
            }   
            
            System.debug('#### RMAs with related deleted products Map values:' + RMAdeletedProductMap);            
            
            // Clear the list of RMA to be updated then populate with non-deleted products
            For(RMA__c aRMA:RMAtoBeUpdatedList)
            {
                For(Integer i=1; i<=5; i++)
                {
                    innerReturnedProduct emptyProduct = new innerReturnedProduct(new RMA_Product__c(), aRMA);
                    emptyProduct.updateRMA(i);
                }
                 
                System.debug('#### ********* RMA (before) ************ : ' + aRMA );
                                              
                if(RMAdeletedProductMap != null)
                { 
                    For(innerReturnedProduct anInnerProduct:RMAdeletedProductMap.get(aRMA.ID))
                    {
                         Integer index=1;
                            
                         System.debug('#### **** LOOP ' +index+ ' ****');    
                         
                         Boolean saveProduct = true;
                                                    
                         For(innerReturnedProduct anotherInnerProduct:RMAInnerProductMap.get(aRMA.ID))
                         {
                             System.debug('#### ID of the inner returned Product' + anotherInnerProduct.innerProductID );
                             System.debug('#### ID of the deleted returned Product' + anInnerProduct.innerProductID );
                            
                             if(anotherInnerProduct.innerProductID != anInnerProduct.innerProductID)
                             {
                                 System.debug('#### Inner product: ' + anInnerProduct);
                                 anotherInnerProduct.parentRMA = aRMA;
                                 anotherInnerProduct.updateRMA(index);
                                 System.debug('#### ********* RMA (after) ************ : ' + aRMA );
                                 index++;   
                                 System.debug('Index: ' + index );
                             }
                         }  
                         
                         System.debug('#### ********* RMA (Final) ************ : ' + aRMA );                                                             
                    }
                }
                
                List<DataBase.SaveResult> SaveResultList = DataBase.update(RMAtoBeUpdatedList);                    
            }     
        }          
    }      
    
    // Build the ID/Record for the RMA related to a list of Returned Products
    public static Map<String,RMA__c> buildRMAIdRecordMap(List<RMA_Product__c> aReturnedProductsList)
    {
        Map<String,RMA__c> result = new Map<String,RMA__c>();
        
        if(aReturnedProductsList!= null && aReturnedProductsList.size()>0)
        {
           // Retrieve the list of related RMAs and map them with their IDs
            Set<String> aSetOfRMAIDs = new Set<String>();
           
            For(RMA_Product__c aReturnedProduct:aReturnedProductsList)
            {
                aSetOfRMAIDs.add(aReturnedProduct.RMA__c);
            }
            
            List<RMA__c> aRMAList = [SELECT ID, RP1_CommercialReference__c,RP1_DateCode__c,TECH_RP1_ID__c,RP1_Quantity__c,RP1_SerialNumber__c,
                                                RP2_CommercialReference__c,RP2_DateCode__c,TECH_RP2_ID__c,RP2_Quantity__c,RP2_SerialNumber__c,
                                                RP3_CommercialReference__c,RP3_DateCode__c,TECH_RP3_ID__c,RP3_Quantity__c,RP3_SerialNumber__c,
                                                RP4_CommercialReference__c,RP4_DateCode__c,TECH_RP4_ID__c,RP4_Quantity__c,RP4_SerialNumber__c,
                                                RP5_CommercialReference__c,TECH_RP5_ID__c,RP5_Quantity__c,RP5_SerialNumber__c, RP5_DateCode__c  
                                                FROM RMA__c WHERE ID IN :aSetOfRMAIDs];
            
            // Build the map between ID and fetched records
            For(RMA__c aRMA:aRMAList)
            {
                result.put(aRMA.ID,aRMA);
            }  
        }   
        
        return result;  
    }
    
    // Method to update the first Product within the RMA
    public static RMA__c updateRMAProduct1(RMA_Product__c ReturnedProducts, RMA__c relatedRMA)
    {
        RMA__c result; 
        
        if(ReturnedProducts != null && relatedRMA != null)
        {
            result = relatedRMA; 
            result.TECH_RP1_ID__c = ReturnedProducts.ID;
            result.RP1_CommercialReference__c = ReturnedProducts.Name;
            result.RP1_Quantity__c = ReturnedProducts.Quantity__c;
            result.RP1_SerialNumber__c = ReturnedProducts.SerialNumber__c;
            result.RP1_DateCode__c = ReturnedProducts.DateCode__c;
        }
        
        return result;   
    }
    
    // Method to update the second Product within the RMA
    public static RMA__c updateRMAProduct2(RMA_Product__c ReturnedProducts, RMA__c relatedRMA)
    {
        RMA__c result; 
        
        if(ReturnedProducts != null && relatedRMA != null)
        {
            result = relatedRMA; 
            result.TECH_RP2_ID__c = ReturnedProducts.ID;
            result.RP2_CommercialReference__c = ReturnedProducts.Name;
            result.RP2_Quantity__c = ReturnedProducts.Quantity__c;
            result.RP2_SerialNumber__c = ReturnedProducts.SerialNumber__c;
            result.RP2_DateCode__c = ReturnedProducts.DateCode__c;
        }
        
        return result;   
    }   
    
    // Method to update the third Product within the RMA
    public static RMA__c updateRMAProduct3(RMA_Product__c ReturnedProducts, RMA__c relatedRMA)
    {
        RMA__c result; 
        
        if(ReturnedProducts != null && relatedRMA != null)
        {
            result = relatedRMA; 
            result.TECH_RP3_ID__c = ReturnedProducts.ID;
            result.RP3_CommercialReference__c = ReturnedProducts.Name;
            result.RP3_Quantity__c = ReturnedProducts.Quantity__c;
            result.RP3_SerialNumber__c = ReturnedProducts.SerialNumber__c;
            result.RP3_DateCode__c = ReturnedProducts.DateCode__c;
        }
        
        return result;   
    }    
    
    // Method to update the 4th Product within the RMA
    public static RMA__c updateRMAProduct4(RMA_Product__c ReturnedProducts, RMA__c relatedRMA)
    {
        RMA__c result; 
        
        if(ReturnedProducts != null && relatedRMA != null)
        {
            result = relatedRMA; 
            result.TECH_RP4_ID__c = ReturnedProducts.ID;
            result.RP4_CommercialReference__c = ReturnedProducts.Name;
            result.RP4_Quantity__c = ReturnedProducts.Quantity__c;
            result.RP4_SerialNumber__c = ReturnedProducts.SerialNumber__c;
            result.RP4_DateCode__c = ReturnedProducts.DateCode__c;
        }
        
        return result;   
    }   
    
    // Method to update the 4th Product within the RMA
    public static RMA__c updateRMAProduct5(RMA_Product__c ReturnedProducts, RMA__c relatedRMA)
    {
        RMA__c result; 
        
        if(ReturnedProducts != null && relatedRMA != null)
        {
            result = relatedRMA; 
            result.TECH_RP5_ID__c = ReturnedProducts.ID;
            result.RP5_CommercialReference__c = ReturnedProducts.Name;
            result.RP5_Quantity__c = ReturnedProducts.Quantity__c;
            result.RP5_SerialNumber__c = ReturnedProducts.SerialNumber__c;
            result.RP5_DateCode__c = ReturnedProducts.DateCode__c;
        }
        
        return result;   
    }   
    
    // Method used to replace empty fields by blank spaces
    public static void replaceNullbyDoubleBlank(String aField)
    {
         if(aField == null)
         {
             aField = '  ';
         }
    }   
    
    // To avoid empty fields to be replaced by underscores, they are replaced by blank spaces
    public static void discardNullRMAFields(List<RMA__c> aListOfRMA)
    {
         Map<String, Schema.SObjectField> RMAFieldMap = Schema.SObjectType.RMA__c.fields.getMap();
         
            for( Schema.SObjectField aRMAField:RMAFieldMap.values())
            {
                /*
                field = objFields.get(selectValue.getvalue());
                
                fieldDescribe = field.getdescribe();
                fieldType = fieldDescribe.getType(); 
                objFieldTypes.put(selectValue.getvalue(),fieldType);   
                */       
            }
         
    }   
}