public class VFC_CassiniHelpAndTraining{
    @RemoteAction
    public static list<ContentDocumentLink> chatterDocPOC(){
          Id pocId;
          List<PointOfContact__c> lstPOC =[Select Id from PointOfContact__c where Name =: System.Label.CLJUN16CSN154];
            pocId= lstPOC[0].Id;
            System.debug(pocId);
            if(lstPOC.size()>0){
                 list<ContentDocumentLink> lstContentDocumentLink =[Select contentDocument.Title,ContentDocument.LatestPublishedVersionId,ContentDocumentId, contentDocument.FileExtension from ContentDocumentLink where LinkedEntityId =:pocId ];
                 System.debug(pocId);     
                 return lstContentDocumentLink ;
             }
             else{
                   return null;
            }
    } 
        
}