global class Batch_PAMDataMigration implements Database.Batchable<sObject>{
    global Integer intLimitNumber;
    
    global Batch_PAMDataMigration(){}
    global Batch_PAMDataMigration(Integer intLmtNumber){
        if(intLmtNumber==0 || intLmtNumber==null)
            intLimitNumber=2000000;
        intLimitNumber=intLmtNumber;
    }


    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c,Product__c, CommercialReference__c,TECH_2014ReferentialUpdated__c FROM PAMandCompetition__c where TECH_2014ReferentialUpdated__c=false Order By CreatedDate DESC LIMIT :intLimitNumber]);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){      
        Map<Id,PAMandCompetition__c> mapPAMandCompetition =new Map<Id,PAMandCompetition__c>();    
        for(sObject objScope : scope){
            PAMandCompetition__c objPAMandCompetition =(PAMandCompetition__c)objScope;   
            mapPAMandCompetition.put(objPAMandCompetition.id,objPAMandCompetition);
        }
        updateReferntialDataOnPAMandCompetition(mapPAMandCompetition);
    }

    global void updateReferntialDataOnPAMandCompetition(Map<Id,PAMandCompetition__c> mapPAMandCompetition){
        List<PAMandCompetition__c> lstPAMandCompetitionToBeUpdate =new List<PAMandCompetition__c>();
        Map<Id,PAMandCompetition__c>mapPAMCompWithCR = new Map<Id,PAMandCompetition__c>();
        Map<Id,PAMandCompetition__c>mapPAMCompWithProductRef = new Map<Id,PAMandCompetition__c>();
        Map<Id,PAMandCompetition__c>mapPAMCompWithOutRef = new Map<Id,PAMandCompetition__c>();
        Map<Id,String>mapPAMCompUniqueRef = new Map<Id,String>();
        String strErrorId='';
        String strErrorMessage='';
        String strErrorType='';
        String strErrorStackTrace='';
        
        Set<String> CommRefSet = new Set<String>();
        Map<String,TECH_CommRefAllLevel__c> mapCommRefAllLevel = new Map<String,TECH_CommRefAllLevel__c>();
        Map<String,TECH_GMRMapping__c> mapGMRMapping = new Map<String,TECH_GMRMapping__c>();
        Set<Id> OldProductIdSet = new Set<Id>();
        Set<Id> UniqueAllLevelsSet = new Set<Id>();
        String strMapKey;
        for(PAMandCompetition__c objPAMComp: mapPAMandCompetition.values()){
            if(objPAMComp.CommercialReference__c !=null && objPAMComp.CommercialReference__c.trim() !=''){
            // Reference to Commercial Reference Table
            mapPAMCompWithCR.put(objPAMComp.Id,objPAMComp);
                CommRefSet.add(objPAMComp.CommercialReference__c);
            }
            /*
            else if(objPAMComp.Product__c !=null ){
                // Reference to Opp Product Table
                mapPAMCompWithProductRef.put(objPAMComp.Id,objPAMComp);
                OldProductIdSet.add(objPAMComp.Product__c);  
            }
            else{
                if(objPAMComp.Product__c == null && (objPAMComp.CommercialReference__c == null || objPAMComp.CommercialReference__c =='') && (objPAMComp.ProductBU__c !=null && objPAMComp.ProductBU__c.trim() !='')){
                //Data Migration Issue
                    if(objPAMComp.ProductBU__c.trim() !=''){
                        strMapKey = objPAMComp.ProductBU__c.toUpperCase();
                    }
                    if(objPAMComp.ProductLine__c.trim() !=''){
                        strMapKey += ',' + objPAMComp.ProductLine__c.toUpperCase();
                    }
                    if(objPAMComp.ProductFamily__c.trim() !=''){
                        strMapKey += ',' + objPAMComp.ProductFamily__c.toUpperCase();
                    }
                    if(objPAMComp.Family__c.trim() !=''){
                        strMapKey += ',' + objPAMComp.Family__c.toUpperCase();
                    }
                    UniqueAllLevelsSet.add(strMapKey);
                    mapPAMCompUniqueRef.put(objPAMComp.Id,strMapKey);
                    mapPAMCompWithOutRef.put(objPAMComp.Id,objPAMComp);
                }
            }
            */
        }
        if(mapPAMCompWithCR.size()>0){
            List<TECH_CommRefAllLevel__c> lstCommRefAllLevel = new List<TECH_CommRefAllLevel__c>([Select Id, Name, BusinessUnit__c, ProductLine__c, ProductFamily__c, Family__c, SubFamily__c, ProductSuccession__c, ProductGroup__c, NewGMRCode__c from TECH_CommRefAllLevel__c where Name IN:CommRefSet]);
            if(lstCommRefAllLevel.size()>0){
                for(TECH_CommRefAllLevel__c objCommRef: lstCommRefAllLevel ){
                    mapCommRefAllLevel.put(objCommRef.Name, objCommRef);
                }
            }
            if(mapCommRefAllLevel.size()>0){
                for(PAMandCompetition__c objPAMComp: mapPAMCompWithCR.values()){
                    if(mapCommRefAllLevel.containsKey(objPAMComp.CommercialReference__c)){
                        objPAMComp.ProductBU__c = mapCommRefAllLevel.get(objPAMComp.CommercialReference__c).BusinessUnit__c;
                        objPAMComp.ProductLine__c = mapCommRefAllLevel.get(objPAMComp.CommercialReference__c).ProductLine__c;
                        objPAMComp.ProductFamily__c = mapCommRefAllLevel.get(objPAMComp.CommercialReference__c).ProductFamily__c;
                        objPAMComp.Family__c = mapCommRefAllLevel.get(objPAMComp.CommercialReference__c).Family__c;
                        objPAMComp.TECH_2014ReferentialUpdated__c=true;
                        lstPAMandCompetitionToBeUpdate.add(objPAMComp);
                    }
                }
            }
        }
        /*
        if(mapPAMCompWithProductRef.size()>0){
            List<TECH_GMRMapping__c> lstGMRMapping = new List<TECH_GMRMapping__c>([Select Id, Name, OldProductID__c, NewBusinessUnit__c, NewProductLine__c, NewProductFamily__c, NewFamily__c, NewGMRCode__c from TECH_GMRMapping__c where OldProductID__c IN:OldProductIdSet ]);
            if(lstGMRMapping.size()>0){
                for(TECH_GMRMapping__c objGMRMapping: lstGMRMapping){
                    mapGMRMapping.put(objGMRMapping.OldProductID__c,objGMRMapping);
                }             
            }
            if(mapGMRMapping.size()>0){
                for(PAMandCompetition__c objPAMComp: mapPAMCompWithProductRef.values()){
                    if(mapGMRMapping.containsKey(objPAMComp.Product__c)){
                        objPAMComp.ProductBU__c = mapGMRMapping.get(objPAMComp.Product__c).NewBusinessUnit__c;
                        objPAMComp.ProductLine__c = mapGMRMapping.get(objPAMComp.Product__c).NewProductLine__c;
                        objPAMComp.ProductFamily__c = mapGMRMapping.get(objPAMComp.Product__c).NewProductFamily__c;
                        objPAMComp.Family__c = mapGMRMapping.get(objPAMComp.Product__c).NewFamily__c;
                        objPAMComp.TECH_2014ReferentialUpdated__c=true;
                        lstPAMandCompetitionToBeUpdate.add(objPAMComp);                      
                    }
                }
            }
        }
        
        if(mapPAMCompWithOutRef.size()>0){
            List<TECH_GMRMapping__c> lstGMRMapping = new List<TECH_GMRMapping__c>([Select Id, Name, OldProductID__c, NewBusinessUnit__c, NewProductLine__c, NewProductFamily__c, NewFamily__c, NewGMRCode__c from TECH_GMRMapping__c where  UniqueAllLevels__c IN:UniqueAllLevelsSet ]);
            mapGMRMapping=null;
            mapGMRMapping = new Map<String,TECH_GMRMapping__c>();
            if(lstGMRMapping.size()>0){
                for(TECH_GMRMapping__c objGMRMapping: lstGMRMapping){
                    mapGMRMapping.put(objGMRMapping.UniqueAllLevels__c,objGMRMapping);
                }             
            }
            if(mapGMRMapping.size()>0){
                for(PAMandCompetition__c objPAMComp: mapPAMCompWithOutRef.values()){
                    if(mapPAMCompUniqueRef.containsKey(objPAMComp.Id)){
                        if(mapGMRMapping.containsKey(mapPAMCompUniqueRef.get(objPAMComp.Id))){
                            objPAMComp.ProductBU__c = mapGMRMapping.get(mapPAMCompUniqueRef.get(objPAMComp.Id)).NewBusinessUnit__c;
                            objPAMComp.ProductLine__c = mapGMRMapping.get(mapPAMCompUniqueRef.get(objPAMComp.Id)).NewProductLine__c;
                            objPAMComp.ProductFamily__c = mapGMRMapping.get(mapPAMCompUniqueRef.get(objPAMComp.Id)).NewProductFamily__c;
                            objPAMComp.Family__c = mapGMRMapping.get(mapPAMCompUniqueRef.get(objPAMComp.Id)).NewFamily__c;
                            objPAMComp.Product__c = mapGMRMapping.get(mapPAMCompUniqueRef.get(objPAMComp.Id)).OldProductID__c;
                            objPAMComp.TECH_2014ReferentialUpdated__c=true;
                            lstPAMandCompetitionToBeUpdate.add(objPAMComp);
                        }
                    }
                }
            }
        }
        */
        try{
            Database.SaveResult[] OppLineUpdateResult = Database.update(lstPAMandCompetitionToBeUpdate, false);
            for(Database.SaveResult objSaveResult: OppLineUpdateResult){
                if(!objSaveResult.isSuccess()){
                    Database.Error err = objSaveResult.getErrors()[0];
                    strErrorId = objSaveResult.getId() + ':' + '\n';
                    strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                }
            }
        }    
        catch(Exception ex){
            strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
            strErrorType += ex.getTypeName()+'- '+ ex.getLineNumber() + '\n';
            strErrorStackTrace += ex.getTypeName()+'- '+ ex.getStackTraceString() + '\n';
        }
        finally{
            if(strErrorId.trim().length()>0 || Test.isRunningTest()){
                try{
                    DebugLog__c objDebugLog = new DebugLog__c(ExceptionType__c=strErrorType,Description__c=strErrorMessage,StackTrace__c=strErrorStackTrace, DebugLog__c= strErrorId );
                    Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                    DMLoption.optAllOrNone = false;
                    DMLoption.AllowFieldTruncation = true;
                    DataBase.SaveResult debugLog_SR = Database.insert(objDebugLog, DMLoption);
                }
                catch(Exception ex){
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC){
    }
}