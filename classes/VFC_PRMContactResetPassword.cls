public class VFC_PRMContactResetPassword{
    public Contact thisContact {get;set;}
    public Boolean isPermissions {get;set;}
    public Boolean isError {get;set;}
    public String thisContactId;
    public Boolean isSucess{get;set;}
    public VFC_PRMContactResetPassword(ApexPages.StandardController controller) {
        thisContactId = ApexPages.currentPage().getParameters().get('id'); 
    }

    
    public Void ResetPassword() {
        isError = True;
        try {
            
            
            List<User> lUsr = new List<User>([SELECT Id, federationIdentifier, email, Contact.PRMContact__c FROM USER WHERE IsActive = true AND ContactId =:thisContactId]);
            string error1 = 'Only PRM contact password can be reset';
            string error2 = 'you do not have the level of access to perform this action';
            User loggedInUser = [SELECT Id,ProfileId from User WHERE Id = : userinfo.getuserid()];
            if(!lUsr[0].Contact.PRMContact__c)
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLAPR15PRM420));
            if (!lUsr.isEmpty() && ApexPages.getMessages().Size() == 0) {
                User usr = lUsr[0];
                string federatedId = (usr != null ? usr.FederationIdentifier : '');
                
                uimsv2ServiceImsSchneiderComAUM.accessElement application = new uimsv2ServiceImsSchneiderComAUM.accessElement();
                application.type_x = System.label.CLAPR15PRM020;//'APPLICATION';
                application.id = System.label.CLAPR15PRM022;//'prm';
                
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort abc = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                abc.endpoint_x = System.label.CLAPR15PRM017; //'https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager';
                abc.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);//120000; 
                abc.clientCertName_x = System.label.CLAPR15PRM018;//'Certificate_IMS';
                String resetStatus = abc.resetPassword(System.label.CLAPR15PRM019, federatedId, application);
                System.debug('*** Password reset status:' + resetStatus);
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.CLAPR15PRM419));
                isSucess = True;
                isError = False;
            }
        } catch (Exception e) {
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
        }
    }
    public pagereference goToConDetailPage(){
    
        return new pagereference('/'+thisContactId);
    }
}