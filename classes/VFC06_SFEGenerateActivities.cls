/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 22/10/2010
    Description     : Controller extension that creates an event for each Platform Scoring record associated to the 'Individual Commercial Action Plan' record.
                        The total number of events created is determined by the quadrant specific frequency fields on the Individual Commercial Action Plan
                        multiplied by the number of Platform Scoring records associated to the Individual Commercial Action Plan.
                                                                        
*/
/*
Modification History:
---------------------
Date: 2/11/2011
Author: Siddharatha Nagavarapu(Global Delivery Team)
Purpose: UAT imporvement for the DEF- 119
*/
global with sharing class VFC06_SFEGenerateActivities implements Database.Batchable<sObject>{
    // global section
    public Boolean flag{get;set;}
    public SFE_IndivCAP__c iDivCAP;
    private final Integer PLATFORMACCOUNT_LIMIT = 50000;            // governor limit of 10000 records retrieved in SOQL
   // private Integer NEW_EVENT_LIMIT = 1000;                         // governor limit of 1000 records insert exists, we don't make final so when running Test we can alter this number for coverage purposes.
    private final Datetime START_DATETIME = Datetime.newInstance(Date.today().toStartOfMonth(), Time.newInstance(12, 0, 0, 0));
    private Integer DURATION = 60;
    private final Id RECORDTYPE_ID = Label.CLMAY13SLS01;
    public Boolean bTestFlag = false;                               // used in Test method on second invokation of the createEvents() method - ensures complete code coverage
     /* Modified by - Bhuvana Subramaniyan on 18/04/2011 for REQ-0016 - Platforming scoring improvement    
     */    
     private Integer intervalStrtEndDte = 0 ;    
     private Date startDate;    
     
     //End    
     
     //for the batch class
     private String queryString;     
     global Database.SaveResult[] lsr = new List<Database.SaveResult>();
     private String emailTo = System.Label.CL00081;
     private String emailReplyTo = System.Label.CL00086;
     private String emailSubject = System.Label.CL00082;
     
     
     // constructor section
    public VFC06_SFEGenerateActivities(ApexPages.StandardController stdController) {
        System.debug('Hey Iam in constructor method >>>>>>>>>>>');
        this.iDivCAP = (SFE_IndivCAP__c)stdController.getRecord();
        
        /* Modified by - Bhuvana Subramaniyan on 18/04/2011 for REQ-0016 - Platforming scoring improvement
        */
        
        iDivCAP = [select AssignedTo__c,AttractiAxisThreshold1__c,AttractiAxisThreshold2__c,Comments__c,Country__c,CriterWeightQu1__c,CriterWeightQu2__c,CriterWeightQu3__c,
                   CriterWeightQu4__c,CriterWeightQu5__c,CriterWeightQu6__c,OwnerId,CriterWeightQu7__c,CriterWeightQu8__c,CustPlatfViewAs__c,EndDate__c,G1VisitFreq__c,G2VisitFreq__c,
                   G3VisitFreq__c,MktShareThreshold1__c,MktShareThreshold2__c,NbrVisit__c,Q1VisitFreq__c,
                   Q2VisitFreq__c,Q3VisitFreq__c,S1VisitFreq__c,S2VisitFreq__c,S3VisitFreq__c,
                   StartDate__c,Status__c,PlatformingScoringsCompleted__c,TECH_IsEventsGenerated__c from  SFE_IndivCAP__c where Id=: this.iDivCAP.Id];
        //End
        
        if(this.iDivCAP != null && iDivCAP.AssignedTo__c != null){
            User user = [select Id, AvTimeVisit__c from user where Id = :iDivCAP.AssignedTo__c];
            if(user != null && user.AvTimeVisit__c != null)
                DURATION = user.AvTimeVisit__c.intValue() * 60;     // calculate time duration in minutes        
        }
        /* Modified by - Abhishek on 20/07/2011 for September Release 
        */
        flag=true;
        
    }
    
    // methods section
    public PageReference checkUserIsOwner(){
        System.debug('Hey Iam in checkUserIsOwner method >>>>>>>>>>>');
        // re-direct user back to the Individual Action Plan if they are not the owner
        PageReference pageRef = null;  
        //dec -11 release addition (start)
       boolean loopFlag=verifyAccess();
        //dec-11 release addition(end)
        /**Modified By Abhishek as part of september release  Req - (SLS_51)
        */                 
        if((UserInfo.getUserId() != iDivCAP.OwnerId) && !loopFlag)//added an OR condition to the existing code.
        {
            
            flag=false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL00439));
        }
        else if(!iDivCAP.PlatformingScoringsCompleted__c)
        {
            flag=false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL00438));
        }
        else if(iDivCAP.TECH_IsEventsGenerated__c)
        {
            flag=false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CL00437));
        }
        return pageRef;
    }
    
    public boolean verifyAccess()
    {
        boolean loopFlag=false;// a temp flag maitained
        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;//get the profile Name of the current User        
        List<String> profilePrefixList=System.Label.CL00632.split(',');//get the profile prefix list from the custom label
        for(String profilePrefix : profilePrefixList)//if the profile name does not start with the profile prefix
        {
            if((profileName+'').startsWith(profilePrefix))
            loopFlag=true;
        }
        return loopFlag;
    }
    public VFC06_SFEGenerateActivities(String queryString,SFE_IndivCAP__c iDivCAP1)
    {
        this.queryString=queryString;
        this.iDivCAP=iDivCAP1;
    }
    
    public PageReference createEvents(){
        if(!Test.isRunningTest())      
        database.executeBatch(new VFC06_SFEGenerateActivities('select Id, Name, CustomerProfile__c, PlatformedAccount__c from SFE_PlatformingScoring__c where IndivCAP__c = \''+iDivCAP.Id+'\'',iDivCAP));
        else
        database.executeBatch(new VFC06_SFEGenerateActivities('select Id, Name, CustomerProfile__c, PlatformedAccount__c from SFE_PlatformingScoring__c where IndivCAP__c = \''+iDivCAP.Id+'\' limit 1',iDivCAP));

        PageReference pageRef = null;        
        pageRef = new PageReference('/' + iDivCAP.Id);
        pageRef.setRedirect(true);
        iDivCAP.TECH_IsEventsGenerated__c=true;
         Id capRecordTypeId = [SELECT Id from RecordType where SobjectType='SFE_IndivCAP__c' and developerName=:System.Label.CLQ316SLS008 limit 1][0].Id;
        iDivCAP.RecordtypeId=capRecordTypeId ;
        // Jules Faligot - Sept Release
        iDivCAP.Status__c = 'Released';         // automaticaly set the status to 'released' after visit generation
        //End 
        database.update(iDivCAP);
        return pageRef;
    }
        
    // properties Section
    public Boolean bNoErrors {get{return !ApexPages.hasMessages();}}
    
    public class TestException extends Exception {}
    
    global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator(queryString);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Event> le = new List<Event>();
        if(iDivCAP.AssignedTo__c != null && iDivCAP.Id != null){
                for(Sobject sobj:scope)
                {
                SFE_PlatformingScoring__c ps=(SFE_PlatformingScoring__c)sobj;
                    Double j = 0; // j= number of visits
                    
                    System.debug( '...@@ 0 ...'+ps.CustomerProfile__c);
                    
                    if(ps.CustomerProfile__c == 'Q1'){
                        j = iDivCAP.Q1VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'Q2'){
                        j = iDivCAP.Q2VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'Q3'){
                        j = iDivCAP.Q3VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'S1'){
                        j = iDivCAP.S1VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'S2'){
                        j = iDivCAP.S2VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'S3'){
                        j = iDivCAP.S3VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'G1'){
                        j = iDivCAP.G1VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'G2'){
                        j = iDivCAP.G2VisitFreq__c;
                    }else if(ps.CustomerProfile__c == 'G3'){
                        j = iDivCAP.G3VisitFreq__c;
                    }
                    /* Modified by - Bhuvana Subramaniyan on 18/04/2011 for REQ-0016 - Platforming scoring improvement
                        Modified by - Anjali Gopinath on OCt 26,2012 . BR-2250 : Divide events equaly for the given duration.
                    */  
                    
                    if(iDivCAP.StartDate__c!=null & iDivCAP.EndDate__c!= null)
                    {                        
                        Date eventDate;
                        Date eventDateTemp;                     
                        Integer daysbtween = iDivCAP.StartDate__c.daysBetween(iDivCAP.EndDate__c);
                        System.debug('...daysbtween...='+daysbtween);

                        for(Integer i=0;i<j;i++)
                        {
                            if(i==0) //first Event
                            {
                                /*
                                if(iDivCAP.StartDate__c.day()==1) 
                                {
                                    eventDate = iDivCAP.StartDate__c;
                                }
                                else
                                {
                                    eventDate = (iDivCAP.StartDate__c).addMonths(1).toStartOfMonth();
                                }
                                eventDateTemp=eventDate;
                                */
                                eventDate = getEventStartDate(iDivCAP.StartDate__c);
                                System.debug('...eventDate 1...='+eventDate);
                                le.add(new Event(Subject = System.Label.CL00002,Platforming_Customer_Profile__c = ps.CustomerProfile__c ,StartDateTime = eventDate, WhatId = ps.PlatformedAccount__c, TECH_PlatformingScoringID__c = ps.Id, TECH_PlatformingScoringName__c = ps.Name, OwnerId = iDivCAP.AssignedTo__c, DurationInMinutes = DURATION, RecordTypeId = RECORDTYPE_ID, Status__c = System.Label.CL00003)); 
                            }
                            else //from 2nd Event
                            {                               
                                Integer cntEvntDay = Integer.valueOf(i * (daysbtween/j));
                                system.debug('....cntEvntDay....'+cntEvntDay);
                                eventDate=(iDivCAP.StartDate__c).addDays(cntEvntDay);
                                system.debug('....eventDate 00....'+eventDate);
                                /*
                                if(eventDate.day()!=1)
                                {
                                    eventDate=eventDate.toStartOfMonth();
                                }
                                if(eventDate<eventDateTemp)
                                {
                                    eventDate=eventDateTemp;
                                }
                                */
                                eventDate = getEventStartDate(eventDate);
                                
                                System.debug('...eventDate else...='+eventDate);
                                
                                le.add(new Event(Subject = System.Label.CL00002,Platforming_Customer_Profile__c = ps.CustomerProfile__c ,StartDateTime = eventDate, WhatId = ps.PlatformedAccount__c, TECH_PlatformingScoringID__c = ps.Id, TECH_PlatformingScoringName__c = ps.Name, OwnerId = iDivCAP.AssignedTo__c, DurationInMinutes = DURATION, RecordTypeId = RECORDTYPE_ID, Status__c = System.Label.CL00003,SalesEventType__c = Label.CL00002)); 
                                system.debug(le+'... Events List ... ');
                            }                           
                        }                       
                        
                    }
                    
                }
                //End
                if(!le.isEmpty()){
                    Database.SaveResult[] results=Database.insert(le, false);
                    system.debug(results + '... inserts results ... ');
                    for(Database.SaveResult sr:results)
                        if(!sr.isSuccess())
                            lsr.add(sr);      
                }                           
            }
        
    }
    
    public Date getEventStartDate(Date dtEventStartDate)
    {
        system.debug('....dtEventStartDate.....'+dtEventStartDate);
        Date dateTemp=Date.today();
        if(dtEventStartDate.day()==1) 
        {
            system.debug('...in if 1..');
            dateTemp = dtEventStartDate;
        }
        else
        {
            system.debug('...in else  ..');
            //dateTemp = (dtEventStartDate).addMonths(1).toStartOfMonth();
            dateTemp = dtEventStartDate.toStartOfMonth();
        }
        if(dateTemp < iDivCAP.StartDate__c)
        {
            system.debug('...in if 2..');
            dateTemp = (dtEventStartDate).addMonths(1).toStartOfMonth();
        }
        system.debug('...dateTemp...'+dateTemp);
        return dateTemp;
    }
    
    global void finish(Database.BatchableContext BC){
                        // process any errors   
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems from AsyncApexJob where Id = :BC.getJobId()];
    // send email with any errors 
    if(lsr.size()> 0 || a.NumberOfErrors > 0 || Test.isRunningTest()){
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(emailTo.split(','));
      mail.setReplyTo(emailReplyTo);
      mail.setSubject(emailSubject);
      String emailBody='System.Label.CL00084 + a.TotalJobItems' + '/' + a.NumberOfErrors + '. ' + System.Label.CL00085 + lsr.size()+'\n\n\nErrors\n';      
      for(Database.SaveResult sr:lsr)
      emailBody+=sr.getErrors()[0].getMessage()+'\n';
      
      mail.setPlainTextBody(emailBody);
      
      if(!Test.isRunningTest())                          // ensure we never actually send an email during a Test Run
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    }
    
}