public without sharing class ObjectSyncEmbeddedController {
  
    public ObjectSync__c ObjSync{get; set;}
    public  list<SyncObjects.MappingField> mappingsFields {get; set;}
    public  list<ObjectSyncWrapp > listAuxDesserialise {get; set;}
    
    
    public list<ObjectSyncWrapp> recordsToShow {get; set;}
    public integer recordsPerPage {get; set;}
    public integer actualPage {get; set;}
    public map<integer,list<ObjectSyncWrapp >> mapPaginator{get; set;}


    public ObjectSyncEmbeddedController (ApexPages.StandardController stdController){
    
        actualPage = 1;
        recordsPerPage = 10;
        mapPaginator = new map<integer,list<ObjectSyncWrapp>>();
        recordsToShow = new list<ObjectSyncWrapp >();
        
        listAuxDesserialise = new list<ObjectSyncWrapp >();

        if(!test.isRunningTest()){ // no se permiten addFields en test classes
            stdController.addFields(new List<String>{ 'FromIdentifier__c', 'FromObject_API_Name__c', 'ToIdentifier__c' , 'ToObject_API_Name__c' ,'Condition__c' , 'RoundTrip__c','MappingFields_JSON__c'});
        }  
        
        this.ObjSync= (ObjectSync__c )stdController.getRecord();
        
        mappingsFields = (list<SyncObjects.MappingField>)JSON.deserialize(objSync.MappingFields_JSON__c , list<SyncObjects.MappingField>.class);
        
        
        /****** From Describe *****/
                map<String,String> mapLabelaCompleteLabelFrom = new map<String,String>();

                // Get sObject
                Schema.SObjectType sobject_typeFrom = Schema.getGlobalDescribe().get( ObjSync.FromObject_API_Name__c );
                system.debug(sobject_typeFrom );
                
                // Get sObject Describe
                Schema.Describesobjectresult descSobjectResultObjFrom = sobject_typeFrom.getDescribe();  
                system.debug('aaa');
                          
                // Get Field Map
                Map<String,Schema.SObjectField> mapFieldsFrom = descSobjectResultObjFrom.fields.getMap(); 
                system.debug('aaa2');
                        
                if(descSobjectResultObjFrom  != null){
                    system.debug('aaa3');
                    for(String s : mapFieldsFrom.keySet()){
                        system.debug('aaa4');
                        mapLabelaCompleteLabelFrom.put(mapFieldsFrom.get(s).getDescribe().getName() ,mapFieldsFrom.get(s).getDescribe().getLabel() + ' (' + mapFieldsFrom.get(s).getDescribe().getName() + ')' );
                    }
                }
            
            /*****             *****/
            
             
            /****** Describe del To *****/
                         
            map<String,String> mapLabelaCompleteLabelTo = new map<String,String>();
    
            // Get sObject
            Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get( objSync.ToObject_API_Name__c);
            system.debug(sobject_type );
      
            // Get sObject Describe
            Schema.Describesobjectresult descSobjectResultObj = sobject_type.getDescribe();  
                      
            // Get Field Map
            Map<String,Schema.SObjectField> mapFields = descSobjectResultObj.fields.getMap(); 
                    
            if(descSobjectResultObj  != null){
                for(String s : mapFields.keySet()){                   
                    mapLabelaCompleteLabelTo .put(mapFields.get(s).getDescribe().getName() ,mapFields.get(s).getDescribe().getLabel() + ' (' + mapFields.get(s).getDescribe().getName() + ')' );
                }  
            }
            
           
            
            /******  *****/

        for(SyncObjects.MappingField sync : mappingsFields ){
            system.debug('aaa5 '+ mapLabelaCompleteLabelFrom.get(sync.FromApiField));
            system.debug('aaa6 '+ sync.FromApiField);
            system.debug('aaa7 '+ mapLabelaCompleteLabelFrom);

            ObjectSyncWrapp auxSync = new ObjectSyncWrapp (    mapLabelaCompleteLabelFrom.get(sync.FromApiField) , mapLabelaCompleteLabelTo.get(sync.toApiField) , sync.nillable ,  sync.mappingEnabled ); //'(' + fromVal +') ' +  sync.fromApiField

            listAuxDesserialise.add(auxSync );

        }
        system.debug('aaa8 ');
            
        if(!listAuxDesserialise.isEmpty()){
            system.debug('aaa9 ');
            integer recordsPorPageToLoop = recordsPerPage;
            integer pageNumber = 1;
            mapPaginator.put(1,new list<ObjectSyncWrapp>());
            
            system.debug('aaa10 ' + mapPaginator);
            
            for(integer i = 0 ; i < listAuxDesserialise.Size() ; i++){
                system.debug('aaa11 ' + mapPaginator);

                if(mapPaginator.get(pageNumber).Size() < 10){
                    list<ObjectSyncWrapp > aux = mapPaginator.get(pageNumber);
                    aux.add(listAuxDesserialise[i]);
                    mapPaginator.put(pageNumber,aux);
                }else{
                    pageNumber ++;
                    mapPaginator.put(pageNumber ,new list<ObjectSyncWrapp>());
                    if(mapPaginator.get(pageNumber).Size() < 11){
                        list<ObjectSyncWrapp > aux = mapPaginator.get(pageNumber);
                        aux.add(listAuxDesserialise[i]);
                        mapPaginator.put(pageNumber,aux);
                    }
                }
                
            }
            
            system.debug('aaa12 ' + mapPaginator);
            
            
            recordsToShow = mapPaginator.get(1);
        
        }
        
    }
    
    
    
    public void previous() {
        if( gethasPrevious() ){
            actualPage--;
        }
         recordsToShow = mapPaginator.get(actualPage);
    }
 
    public void next(){
        if( gethasNext() ){
            actualPage++;
        }
         recordsToShow = mapPaginator.get(actualPage);
    }
 
    public Boolean gethasNext(){
        if(!mapPaginator.containsKey(actualPage + 1)){
            return false;
        }
        return true;
    }
  
    public Boolean gethasPrevious(){
        if(!mapPaginator.containsKey(actualPage - 1)){
            return false;
        }
        return true;
    }
 
    public void first(){
        actualPage = 1;
        recordsToShow = mapPaginator.get(actualPage);
    }
 
    public void last() {
        Set<integer> indexes = mapPaginator.KeySET();
        integer aux = mapPaginator.Size();
        actualPage = aux ;
        recordsToShow = mapPaginator.get(aux  );
    }
    
    
    public class ObjectSyncWrapp {
        public String fromValue {get; set;}
        public String toValue {get; set;}
        public Boolean nilliable{get; set;}
        public Boolean enable {get; set;}
        
        public ObjectSyncWrapp(String fromValue ,String toValue ,Boolean nilliable,Boolean enable){
            this.fromValue = fromValue ;
            this.toValue = toValue ;
            this.nilliable = nilliable;
            this.enable = enable;
        
        } 
    
    }

}