/*
    Author          : Accenture IDC Team 
    Date Created    : 25/07/2011
    Description     : Utility class to populate Picklist Filter values for Competitor search
                      extending abstract Data Source Class
*/
/*
Modification History:
Author: Siddharatha Nagavarapu (GD)
Date Modified: 16/02/2012
Description: A new opportunity type is added 'Solutions Fast track' and the code is changed to support that.
*/
Public Class Utils_OppStageRecordTypePicklistManager implements Utils_PicklistManager
{
    Map<String,String> opptyStage;
    
    // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(sObjectType.Opportunity.fields.OpptyType__c.getLabel());
        Labels.add(sObjectType.Opportunity.fields.StageName.getLabel());
        return Labels;
    } 

    // Return All Picklist Values 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
       List<SelectOption> options = new  List<SelectOption>();
       If(S == Label.CL00355)
           return null;
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // PopulateOpportunity Type when Page Loads
       if(i==1)
       {
            // Populate Type Picklist Values
            Schema.DescribeFieldResult opptyType = Schema.sObjectType.Opportunity.fields.OpptyType__c;
            for (Schema.PickListEntry PickVal : opptyType.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }
       if(i==2)
       {
            // Populate Stage Picklist Values
            /*Schema.DescribeFieldResult stage = Schema.sObjectType.Opportunity.fields.StageName;
            for (Schema.PickListEntry PickVal : stage.getPicklistValues())
            {
                
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
                
            }*/
            if((s == System.Label.CL00241) ||(s == System.Label.CL00724))
            {
                options.add(new SelectOption(System.Label.CLOCT13SLS02, System.Label.CLOCT13SLS02));                
                options.add(new SelectOption(System.Label.CL00146, System.Label.CL00146));
                options.add(new SelectOption(System.Label.CL00147, System.Label.CL00147));
                options.add(new SelectOption(System.Label.CL00547, System.Label.CL00547));
            }
            if(s == System.Label.CL00240)
            {
                options.add(new SelectOption(System.Label.CLOCT13SLS02, System.Label.CLOCT13SLS02));
                options.add(new SelectOption(System.Label.CL00146, System.Label.CL00146));
                options.add(new SelectOption(System.Label.CL00147, System.Label.CL00147));
            }
       }  

       return  options;  
    }
}