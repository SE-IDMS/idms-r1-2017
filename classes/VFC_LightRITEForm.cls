/* 
    Description: The page displays the details related to Light RITE
    Author: Bhuvana Subramaniyan
*/
public with sharing class VFC_LightRITEForm 
{
    //Variables Declaration
    public IPOLightRITE__c lightRITE{get;set;}
    public Map<String, String> fieldsTextArea{get;set;}
    public Map<String, String> fieldsTextArea1{get;set;}
    public boolean renderPlatRules{get;set;}
    public Map<String, String> fieldAPIname{get;set;}
    public Map<String, String> mappingSMEValues{get;set;}
    private Map<String, CS_TextAreaMappingFields__c> csTextAreaFields= CS_TextAreaMappingFields__c.getAll();    
    private Map<String, CS_FieldNameAPIName__c> csFieldsAPIName= CS_FieldNameAPIName__c.getAll();
    public Map<String, String> fieldHelpText{get;set;}
    public Map<String, String> textMsg{get;set;}
    //Constructor
    public VFC_LightRITEForm(ApexPages.StandardController controller) 
    {
        lightRITE = (IPOLightRITE__c)controller.getRecord();
        if(lightRITE.Id!=null)
        {
            //Queries the Light RITE Record
            lightRITE = [select TypeOfContract__c, ExclusionOfDamagesPlan__c,PlatinumRulesInfringement__c,ExclusionOfDamages__c,ExclusionOfDamagesSME__c,DamageLimitedPlan__c,DamageLimited__c,DamageLimitedSME__c,LegalEntityPlan__c,LegalEntity__c,LegalEntitySME__c,ParentCompanyPlan__c,ParentCompany__c,ParentCompanySME__c,AccountName__c,AppplicableLawJurisdiction__c,AppplicableLawJurisdictionAction__c,AppplicableLawJurisdictionSME__c,BankGuarReqByClient__c,BankGuarReqByClientAction__c,BankGuarReqByClientSME__c,CCO__c,
            ClientLatePayment__c,ClientLatePaymentAction__c,ClientLatePaymentSME__c,ComingIntoForceOfContract__c,ComingIntoForceOfContractAction__c,ComingIntoForceOfContractSME__c,CommChartChangeOrder__c,CommChartChangeOrderAction__c,CommChartChangeOrderSME__c,ContractPrevailOtherContrt__c,ContractPrevailOtherContrtAction__c,ContractPrevailOtherContrtSME__c,CountryOfDestChanges__c,CountryOfDestChangesAction__c,CountryOfDestChangesSME__c,CountryOfDestination__c,CriticalMarketSegment__c,
            CriticalMarketSegmentPlan__c,CustomizedDesignInOffer__c,CustomizedDesignInOfferAction__c,CustomizedDesignInOfferSME__c,DamageToCustomerProperty__c,DamageToCustomerPropertyPlan__c,DownPaymentProgressPayments__c,DownPaymentProgressPaymentsAction__c,DownPaymentProgressPaymentsSME__c,EndUser__c,ExposureToCurrencyVariation__c,ExposureToCurrencyVariationAction__c,ExposureToCurrencyVariationSME__c,GeneralComments__c,LDForDelayPerfCapped__c,LDForDelayPerfCappedAction__c,LDForDelayPerfCappedSME__c,
            MajorOGSuppliersBackDev__c,MajorOGSuppliersBackDevPlan__c,MarketSegmentBU__c,OfferAmount__c,OfferMadeToNewCustomer__c,OfferMadeToNewCustomerPlan__c,OfferNewRenewedContract__c,OfferNewRenewedContractAction__c,OfferNewRenewedContractSME__c,OfferPartOfEndUserCoreProcess__c,OfferPartOfEndUserCoreProcessPlan__c,OGSuppBindingOffers__c,OGSuppBindingOffersAction__c,OGSuppBindingOffersSME__c,OutsideGrpSupplierScope__c,OwnershipOfIntellectualProp__c,OwnershipOfIntellectualPropAction__c,
            OwnershipOfIntellectualPropSME__c,PaymentTermsCreditInsurance__c,PaymentTermsCreditInsuranceAction__c,PaymentTermsCreditInsuranceSME__c,PrevLitigationOrBadDebt__c,PrevLitigationOrBadDebtPlan__c,ProjectDesc__c,ProjectName__c,ProjGrossMarginAboveMinGross__c,ProjGrossMarginAboveMinGrossAction__c,ProjGrossMarginAboveMinGrossSME__c,ProjInvExportOfGoods__c,ProjInvExportOfGoodsAction__c,ProjInvExportOfGoodsSME__c,ProjInvInnovativeFin__c,ProjInvInnovativeFinAction__c,ProjInvInnovativeFinSME__c,
            QuoteLink__c,ReqSchToCommitPerfComm__c,TotalCOGSInk__c, RiskProvision__c, IGCOGSink__c, PMhoursCOGSink__c,OGCOGSInk__c, MarketSegmentPick__c, LeadingBusiness__c,SampleField__c,ReqSchToCommitPerfCommAction__c,ReqSchToCommitPerfCommSME__c,SEContent__c,SELiableForThirdParty__c,SELiableForThirdPartyAction__c,SELiableForThirdPartySME__c,SEOFMajorSuppliers__c,SEOFMajorSuppliersAction__c,SEOFMajorSuppliersSME__c,TechnicalPerformanceGuarantee__c,TechnicalPerformanceGuaranteePlan__c, Name, Id from IPOLightRITE__c where Id=:lightRITE.Id];            
        }
        else
        {
            lightRITE = new IPOLightRITE__c();
            lightRITE.QuoteLink__c = system.currentPagereference().getParameters().get('quoteLink');
            lightRITE.MarketSegmentPick__c = system.currentPagereference().getParameters().get(Label.CLAPR14SLS57);
            lightRITE.LeadingBusiness__c = system.currentPagereference().getParameters().get(Label.CLAPR14SLS54);
        }
        //Map to hold field full name & Shortened Name AND Map to hold the picklist & its corresponding Text Area field        
        fieldsTextArea = new Map<String, String>();
        fieldsTextArea1 = new Map<String, String>();
        fieldAPIname = new Map<String, String>();
        mappingSMEValues = new Map<String, String>();
        fieldHelpText = new Map<String, String>();
        textMsg = new Map<String, String>();
        
        Map<String, Schema.SObjectField> lightRITEFields= Schema.getGlobalDescribe().get('IPOLightRITE__c').getDescribe().fields.getMap();
        for(String s: lightRITEFields.keyset())
        {
            if(lightRITEFields.get(s).getDescribe().getInlineHelpText()!=null)
            fieldHelpText.put(lightRITEFields.get(s).getDescribe().getLabel(), lightRITEFields.get(s).getDescribe().getInlineHelpText());
            else
            fieldHelpText.put(lightRITEFields.get(s).getDescribe().getLabel(), '-');
        }
        
        textMsg.put('ExclusionOfDamages__c', Label.CLAPR14SLS58);
        textMsg.put('DamageLimited__c', Label.CLAPR14SLS59);        
        textMsg.put('LegalEntity__c', ' ');        
        textMsg.put('ParentCompany__c', '');  
                      
        for(String s: csTextAreaFields.keyset())
        {
            fieldsTextArea.put(s, csTextAreaFields.get(s).MappingTextArea__c);
            fieldsTextArea1.put(s, csTextAreaFields.get(s).MappingTextArea1__c);                
            mappingSMEValues.put(s,csTextAreaFields.get(s).SMEValues__c);
        }     

        for(String s: csFieldsAPIName.keyset())
        {
            fieldAPIname.put(s, csFieldsAPIName.get(s).FieldFullName__c);
        }
        
    } 
    //Inserts or updates the Light RITE & displays the error messages if any
    public pagereference save()
    {
        Database.saveresult sr;
        Pagereference pg;
        if(lightRITE.Id!=null)
            sr = database.update(lightRITE, false);
        else
            sr = database.insert(lightRITE, false);
        if(!(sr.isSuccess()))
        {
            for (Database.Error e:sr.getErrors())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
        else
            pg = new pagereference('/' + sr.getId());
        return pg;
    }
}