public class VFC_CassiniContractDetails {
    
    public boolean pointsFlag {get;set;}
    
    public VFC_CassiniContractDetails(){
        pointsFlag=false;
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ List<Contract> lstContracts=[SELECT EndDate,Name,Points__c,StartDate,Id FROM Contract Where Id=:lstCVCPs[0].Contract__c LIMIT 1];
                if(!lstContracts.isEmpty()){ List<Package__c> lstPackageToAccessPoints=[SELECT Id FROM Package__c WHERE Contract__c = :lstContracts[0].Id AND PackageTemplate__r.PortalAccess__c = TRUE AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' ORDER BY LastModifiedDate DESC LIMIT 1];
                    if(!lstPackageToAccessPoints.isEmpty()) pointsFlag=true;
                }
            }
        }
    }
    
    @RemoteAction  
    public static List<Contract> getContractDetails(){
      List<Contract> lstContracts;
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){
                lstContracts=[SELECT EndDate,Name,Points__c,StartDate,Id FROM Contract Where Id=:lstCVCPs[0].Contract__c LIMIT 1];
            }
        }
        return lstContracts;
    }   
    
    @RemoteAction  
    public static List<PointsRegistrationHistory__c> getPointRegistrationHistory(){
      List<PointsRegistrationHistory__c> lstPRH;
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){
                lstPRH=[SELECT CreatedDate,CustomerOrderNumber__c,Description__c,Contract__c,Name,Points__c,TotalPoints__c FROM PointsRegistrationHistory__c Where Contract__c=:lstCVCPs[0].Contract__c ORDER BY CreatedDate DESC Limit 200];
            }
        }
        return lstPRH;
        
    }   
    
    @RemoteAction  
    public static List<Attachment> getContractRelatedDocuments(){
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        List <CTR_ValueChainPlayers__c> lstCVCPs=new List <CTR_ValueChainPlayers__c>();
        if(lstContactId[0].ContactId!=null){
            lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
        }
        List<Attachment> lstAttachments=new List<Attachment>();
        if(!lstCVCPs.isEmpty()) lstAttachments=[SELECT ContentType,Id,Name FROM Attachment WHERE ParentId = :lstCVCPs[0].Contract__c ORDER BY CreatedDate DESC NULLS FIRST Limit 5];
        return lstAttachments;
        
    }
    
    @RemoteAction  
    public static List<PackageTemplate__c> getPackageTemplates(){
    
        List<PackageTemplate__c> lstPackageTemplate;
        lstPackageTemplate=[SELECT Id,Name,OfferedPrice__c,Points__c FROM PackageTemplate__c WHERE Points__c !=null ORDER BY Points__c ASC NULLS LAST];
        /*List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){
                List<Contract> lstContracts=[SELECT Id,Points__c,EndDate FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' ORDER BY LastModifiedDate DESC LIMIT 1];
                if(!lstContracts.isEmpty()){
                    List<Package__c> lstPackage=[SELECT PackageTemplate__c FROM Package__c WHERE Contract__c = :lstContracts[0].Id ORDER BY LastModifiedDate DESC LIMIT 1];
                    if(!lstPackage.isEmpty()){
                        lstPackageTemplate=[SELECT Id,Name,OfferedPrice__c,Points__c FROM PackageTemplate__c WHERE Id=:lstPackage[0].PackageTemplate__c and Points__c !=null ORDER BY Points__c ASC NULLS LAST];
                    }
                }
            }
        }*/
    
        
        return lstPackageTemplate;
    }   
    
    @RemoteAction  
    public static Boolean putPointsRequest(String Points,String Price,String Reference,String Description,String ShortDescription){
        
        List<User> lstContactId=[SELECT Id,ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        System.debug(lstContactId);
        List <CTR_ValueChainPlayers__c> lstCVCPs=new List <CTR_ValueChainPlayers__c>();
        if(lstContactId[0].ContactId!=null){
            lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,Contract__r.Owner.Email,Contract__r.ContractNumber,Contract__r.Name,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            System.debug(lstCVCPs);
        }
        List<Contract> lstContract=new List<Contract>();
        if(!lstCVCPs.isEmpty()) {
            lstContract=[SELECT OwnerID FROM Contract WHERE Id =:lstCVCPs[0].Contract__c ORDER BY CreatedDate DESC Limit 1];
            System.debug(lstContract);
        }
        Boolean Flag=false;
        String strDescription='';
        String strShortDescription='';
        
        if(Description!='undefined' && Description!='' && Description!=null) 
            strDescription=Description;
        if(ShortDescription!='undefined' && ShortDescription!='' && ShortDescription!=null) 
            strShortDescription=ShortDescription;
        
        String subject;
        Messaging.reserveSingleEmailCapacity(2);
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        mail1.setTargetObjectId(lstContactId[0].Id);
        mail1.setSaveAsActivity(false);
        mail1.setReplyTo(Userinfo.GetUserEmail());
        mail1.setSenderDisplayName(Userinfo.GetName());
        
        if(!Test.IsRunningTest()){
              subject=Label.CLJUN16CSN119 +' - '+Userinfo.getName() +' - '+ Reference+ ' - '+Label.CLJUN16CSN120 +lstCVCPs[0].Contract__r.ContractNumber;  
              mail1.setSubject(subject);
        }   
        else
              mail1.setSubject('Test Subject1234512');
        
        mail1.setBccSender(false);
        mail1.setUseSignature(false);
        String strDesc1;
        if(!Test.IsRunningTest()){
            strDesc1=Label.CLJUN16CSN121+' <br><br> <b>'+ Label.CLJUN16CSN122 + '</b> ' + Userinfo.getName() +' <br> <b>'+Label.CLJUN16CSN123+'</b> '+ Reference +'<br><b> '+ Label.CLJUN16CSN124+' </b> ' +Points+ '<br><b> ' +Label.CLJUN16CSN125 +' </b>'+ Price+ '<br> <b> '+Label.CLJUN16CSN126 +' </b> '+strShortDescription+'<br><b> '+Label.CLJUN16CSN127+'</b> '+lstCVCPs[0].Contract__r.ContractNumber+'<br><b> '+Label.CLJUN16CSN128 +' </b>' +strDescription+'<br><br><br>'+ Label.CLJUN16CSN176 ;
            mail1.setHtmlBody(strDesc1);
        }
        else{
            mail1.setHtmlBody('Test HTML Body');
        }
        System.debug('mail1:'+mail1);
        Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
        if(!Test.IsRunningTest()){
            mail2.setTargetObjectId(lstContract[0].OwnerID);
        }
        else{
            mail2.setTargetObjectId(lstContactId[0].Id);   
        }
        mail2.setSaveAsActivity(false);
        mail2.setReplyTo(Userinfo.GetUserEmail());
        mail2.setSenderDisplayName(Userinfo.GetName());
        if(!Test.IsRunningTest())
            mail2.setSubject(subject);
        else
            mail2.setSubject('Test Subject1234512');
        
        mail2.setBccSender(false);
        mail2.setUseSignature(false);
        //String strDesc2=Label.CLJUN16CSN121+' </br></br> <b>'+ Label.CLJUN16CSN122 + '</b>' + Userinfo.getName() +' </br> <b>'+Label.CLJUN16CSN123+'</b> '+ Reference +'</br><b> '+ Label.CLJUN16CSN124+' </b> ' +Points+ '</br><b> ' +Label.CLJUN16CSN125 +' </b>'+ Price+ '</br> <b> '+Label.CLJUN16CSN126 +' </b> '+strShortDescription+'</br><b> '+Label.CLJUN16CSN127+'</b> '+lstCVCPs[0].Contract__r.ContractNumber+'</br><b> '+Label.CLJUN16CSN128 + strDescription+'</br></br> '+Label.CLJUN16CSN176;
        //strDesc2=Label.CLJUN16CSN121+' <br><br> <b>'+ Label.CLJUN16CSN122 + '</b>' + Userinfo.getName() +' <br> <b>'+Label.CLJUN16CSN123+'</b> '+ Reference +'<br><b> '+ Label.CLJUN16CSN124+' </b> ' +Points+ '<br><b> ' +Label.CLJUN16CSN125 +' </b>'+ Price+ '<br> <b> '+Label.CLJUN16CSN126 +' </b> '+strShortDescription+'<br><b> '+Label.CLJUN16CSN127+'</b> '+lstCVCPs[0].Contract__r.ContractNumber+'<br><b> '+Label.CLJUN16CSN128 + strDescription+'<br><br><br>'+ Label.CLJUN16CSN176 ;
        if(!Test.IsRunningTest()){
            mail2.setHtmlBody(strDesc1);
        }
        else{
            mail2.setHtmlBody('test Email HTML BODy');
        }
        System.debug('mail2:'+mail2);
        emails.add(mail1);
        emails.add(mail2);
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails);  
        Flag=true;
        return Flag;
    }   

}