@isTest(SeeAllData = true)
public class SVMXC_ApprovalsControllerTest {

	static testMethod void myUnitTest() {   
        
        adjustTimesheetSettings();
       
        User mgrUserObj = Utils_TestMethods.createStandardUser('TestMgr');
		mgrUserObj.DelegatedApproverId=UserInfo.getUserId();
        insert mgrUserObj;
        
        // insert user
        User localUser = Utils_TestMethods.createStandardUser('Test');
        localUser.ManagerId = mgrUserObj.Id;
		localUser.DelegatedApproverId= UserInfo.getUserId();
        insert localUser;
        
        SVMXC__Service_Group__c team2 = new SVMXC__Service_Group__c(Name = 'test team 2');
        
        insert team2;
        
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(
            SVMXC__Service_Group__c = team2.Id,
            SVMXC__Salesforce_User__c = localUser.Id);
        
        insert tech;
       
        Date todaysDate = Date.today();
        Datetime rightNow = System.now();
     
        SVMXC_Timesheet__c ts = new SVMXC_Timesheet__c(
                                Technician__c = tech.id,
                                Start_Date__c = system.today().addDays(-7).toStartOfWeek() + 1,
            					End_Date__c = system.today().toStartOfWeek() - 2);
        insert ts;
        
        DateTime startTime = DateTime.now();
        
        List<SVMXC_Time_Entry__c> teInserts = new List<SVMXC_Time_Entry__c>();
        
        SVMXC_Time_Entry__c te1a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addHours(8), End_Date_Time__c = startTime.addHours(10),
       										Timesheet__c = ts.Id, Technician__c = tech.Id, Activity__c = 'Time Off');      
        teInserts.add(te1a); 
        
        SVMXC_Time_Entry__c te2a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(1).addHours(8), End_Date_Time__c = startTime.addDays(1).addHours(10),
       										Timesheet__c = ts.Id, Technician__c = tech.Id, Activity__c = 'Time Off');      
        teInserts.add(te2a); 
        
        SVMXC_Time_Entry__c te3a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(2).addHours(8), End_Date_Time__c = startTime.addDays(2).addHours(10),
       										Timesheet__c = ts.Id, Technician__c = tech.Id, Activity__c = 'Time Off');      
        teInserts.add(te3a); 
        
        SVMXC_Time_Entry__c te4a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(3).addHours(8), End_Date_Time__c = startTime.addDays(3).addHours(10),
       										Timesheet__c = ts.Id, Technician__c = tech.Id, Activity__c = 'Time Off');      
        teInserts.add(te4a); 
        
        SVMXC_Time_Entry__c te5a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(4).addHours(8), End_Date_Time__c = startTime.addDays(4).addHours(10),
       										Timesheet__c = ts.Id, Technician__c = tech.Id, Activity__c = 'Time Off');      
        teInserts.add(te5a);
        
        insert teInserts;
        
        DateTime startingTime = DateTime.now();
		
		
        SVMXC_Time_Entry_Request__c newTER = new SVMXC_Time_Entry_Request__c();
        newTER.Technician__c = tech.Id;
        newTER.Start_Date__c = startingTime.addHours(-2);
        newTER.End_Date__c = startingTime.addHours(-1);	
        newTER.Activity__c = 'Time Off';
       	newTER.All_Day_Event__c=true;
        newTER.Travel_From_Hours__c = 1;
        newTER.Travel_To_Hours__c = 1;
        
       insert newTER;
      
       Test.startTest();
            
       System.runAs(localUser){  
       		      		
       		ts.Submit_for_Approval__c = true;
        	update ts;
           try{
        	newTER.Submitted_for_Approval__c = true;
        	update newTER;
           }
           catch (exception e){}
       }
       	
       System.runAs(mgrUserObj) {
       		
	       //Use the PageReference Apex class to instantiate a page
	       PageReference pageRef = Page.SVMXC_Approvals;
	       Test.setCurrentPage(pageRef);
	       
	       //Instantiate and construct the controller class.
	       SVMXC_ApprovalsController controller = new SVMXC_ApprovalsController();       
	       String nextPage = controller.Approve().getUrl();       
	       
	       SVMXC_ApprovalsController sac = new SVMXC_ApprovalsController();
	       List<SelectOption> YesNoList = controller.getYesNoList();
		   
		   controller.DisplayErrorsNWarnings();
		   controller.Approve();
	       
	       for(SVMXC_ApprovalsController.timesheet sacTS: sac.timesheetsList){
                sacTS.checked = true;
            }
            
            
            for(SVMXC_ApprovalsController.timeEntryRequest savTER: sac.timeEntryRequestsList){
                savTER.checked = false;
            }
            
            sac.Approve();
       }    
	       
       Test.stopTest();
       
    }
    private static void adjustTimesheetSettings() {
    	
    	Set<String> querySet = new Set<String>();
    	querySet.add('TIMESHEET002');
    	querySet.add('TIMESHEET003');
    	querySet.add('PRORIGINATIONDATE');
    	
    	List<SVMXC__ServiceMax_Config_Data__c> settings = [SELECT SVMXC__Internal_Value__c, SVMXC__Display_Value__c, 
    						SVMXC__Setting_ID__r.SVMXC__SettingID__c
							FROM SVMXC__ServiceMax_Config_Data__c 
							WHERE SVMXC__Setting_ID__r.SVMXC__SettingID__c IN : querySet];
		
		for (SVMXC__ServiceMax_Config_Data__c cd : settings) {
			if (cd.SVMXC__Setting_ID__r.SVMXC__SettingID__c == 'TIMESHEET002')
				cd.SVMXC__Internal_Value__c = '1';
			else if (cd.SVMXC__Setting_ID__r.SVMXC__SettingID__c == 'TIMESHEET003')
				cd.SVMXC__Internal_Value__c = 'false';
			else if (cd.SVMXC__Setting_ID__r.SVMXC__SettingID__c == 'PRORIGINATIONDATE')
				cd.SVMXC__Internal_Value__c = '2013-01-01';
		}
		
		update settings;
    	
    }
}