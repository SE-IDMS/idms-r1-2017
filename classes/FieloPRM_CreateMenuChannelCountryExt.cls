/********************************************************************
* Company: Fielo
* Created Date: 26/04/2015
* Description: 
********************************************************************/
public with sharing class FieloPRM_CreateMenuChannelCountryExt {
     
    public CountryChannels__c countrychannel{get;set;}
    public String selectedoption {get;set;}
    public id selectedMenu {get;set;}
    public String textHelp {get;set;}
    public map<string,String> mapIdHelp{get;set;}
    public List<FieloEE__Menu__c> listMenu {get;set;}
    public String categoryRecordTypeId {get;set;}
    public String menuRecordTypeId {get;set;}
    public map<id,FieloEE__Menu__c> mapMenu {get;set;}
    public FieloEE__Menu__c parentMenu{get;set;}
    public List<FieloPRM_MenuOptions__c> listMenuOptions {get;set;}
    public List<TemplateWrapper> listTemplateWrapper {get;set;}   
    public static Boolean isClone {get;set;}

    //OPTION OF THE MENU
    //
    //CreateTemplateStructureContent
    //CreateTemplateStructure
    //NewMenu
    //UseTemplate

    public FieloPRM_CreateMenuChannelCountryExt(ApexPages.StandardController stdController) {
        isClone = false;
        textHelp = '';
        listTemplateWrapper = new  List<TemplateWrapper>();
        listMenu = new List<FieloEE__Menu__c>();
        listMenuOptions = [SELECT id, name, F_PRM_Help__c, F_PRM_Label__c FROM FieloPRM_MenuOptions__c order by Name];
        mapIdHelp = new map<string,String>();
        for( FieloPRM_MenuOptions__c menu : listMenuOptions){
            mapIdHelp.put(menu.F_PRM_Label__c,menu.F_PRM_Help__c);    
        }
        
        categoryRecordTypeId = [SELECT id,DeveloperName FROM RecordType WHERE DeveloperName  = 'CategoryItem'].id;
        menuRecordTypeId = [SELECT id,DeveloperName FROM RecordType WHERE DeveloperName  = 'SubMenu'].id;
        parentMenu = [SELECT id FROM FieloEE__Menu__c WHERE Name  = 'My Partnership' LIMIT 1];
               
        selectedoption = null;
        this.countrychannel = (CountryChannels__c)stdController.getRecord();   
        system.debug('countrychannel: ' + countrychannel);
               
        String soqlCountryChannel = 'SELECT ';
        for(String fieldName : Schema.getGlobalDescribe().get('CountryChannels__c').getDescribe().fields.getMap().keySet()) {
            soqlCountryChannel += fieldName + ', ';
        }
        soqlCountryChannel = soqlCountryChannel.left(soqlCountryChannel.lastIndexOf(','));
        soqlCountryChannel += ', Country__r.Name ';
        soqlCountryChannel += ', Channel__r.Name ';
        soqlCountryChannel += ', PRMCountry__r.CountryCode__c ';
        soqlCountryChannel += ', Country__r.CountryCode__c ';
        soqlCountryChannel += ' FROM ' + 'CountryChannels__c' + ' WHERE Id = \'' + countrychannel.id + '\'  LIMIT 1';
        system.debug('soqlCountryChannel: ' + soqlCountryChannel);
        list<CountryChannels__c> listcountrychannelaux = (list<CountryChannels__c>) Database.query(soqlCountryChannel);
        
        system.debug('listcountrychannelaux: ' + listcountrychannelaux);
        
        countrychannel = listcountrychannelaux[0];
        
        system.debug('countrychannel: ' + countrychannel);
        
        mapMenu = new map<id,FieloEE__Menu__c>();
        listMenu = [SELECT Id,Name,FieloEE__ExternalName__c ,FieloEE__Description__c FROM FieloEE__Menu__c WHERE  F_PRM_Type__c = 'Template'];
        for(FieloEE__Menu__c menu : listMenu){
            mapMenu.put(menu.id, menu);
        
        }
         
    }

    public PageReference redirect(){
        if(countrychannel.F_PRM_Menu__c != null){
            return new PageReference('/'+ countrychannel.F_PRM_Menu__c );
        }else{
            return null;
        }
    }


    public void getTemplatesMenu(){
        if(mapIdHelp.containskey(selectedoption)){
            textHelp =  mapIdHelp.get(selectedoption);  
        }else{
            textHelp =  '';
        }
               
        if(listMenu.size() <1){
            system.debug('listMenu: ' + listMenu);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Not Template Found'));
        }

        listTemplateWrapper = new  List<TemplateWrapper>();
        for(FieloEE__Menu__c menu : listMenu ){

            TemplateWrapper temp = new TemplateWrapper();
            temp.check = false;
            temp.menu = menu;
            listTemplateWrapper.add(temp);      
        }
   
    }

    //OPTION OF THE MENU
    //
    //CreateTemplateStructureContent
    //CreateTemplateStructure
    //NewMenu
    //UseTemplate
    public PageReference nextStep(){

        system.debug('selectedoption ' + selectedoption );
        system.debug('selectedMenu ' + selectedMenu );

        if(selectedoption == 'NewMenu'){
            return createNewMenu();
        }else if(selectedoption == 'CreateTemplateStructure' && selectedMenu != null){
            return createNewMenuUseTemplate();
        }else if(selectedoption == 'CreateTemplateStructureContent' && selectedMenu != null){
            String resultS = FieloPRM_UTILS_MenuCloneMethods.createNewMenuUseTemplateContent(selectedMenu, countrychannel.Name, 'My Partnership', countrychannel);
            if(resultS.length() > 18 ){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,resultS);
                ApexPages.addMessage(myMsg);
                return null;
            }
            countrychannel.F_PRM_Menu__c  = resultS;
            update countrychannel;
            return new PageReference('/'+ countrychannel.F_PRM_Menu__c );
        }else if((selectedoption == 'CreateTemplateStructureContent' || selectedoption == 'CreateTemplateStructure')  && selectedMenu == null){
            system.debug('erro');
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please select a template.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        system.debug('final');

        return null;
    }   
    
    public PageReference createNewMenuUseTemplate(){
    
        System.debug('SIN ESTRUCTURA'  );
        
        isClone = true;
        Id clonedMenu = FieloEE.MenuUtil.cloneMenu(selectedMenu);
        isClone = false;
        
        FieloPRM_UTILS_MenuMethods.createEntitySubscriptions(new Set<String>{clonedMenu,selectedMenu});
               
        System.debug('clonedMenu ' + clonedMenu );
        
        String fieloProgramName = 'PRM Is On';
        
        String programId = [SELECT id FROM FieloEE__Program__c WHERE Name = : FieloProgramName].id;
        
        FieloEE__Category__c oldCategory;
        
        try {
            oldCategory = [SELECT id FROM FieloEE__Category__c WHERE F_PRM_Menu__c =: selectedMenu];    
        } catch(Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
        
        FieloEE__Category__c category = new FieloEE__Category__c();
        category.RecordTypeId = categoryRecordTypeId;
        if( countrychannel != null){
            if(countrychannel.PRMCountry__c != null){
                category.F_PRM_Country__c = countrychannel.PRMCountry__c;
            }
        }
        
        category.Name = countrychannel.PRMCountry__r.CountryCode__c + ' - ' + countrychannel.Sub_Channel__c;
        category.F_PRM_CategoryFilter__c = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        category.FieloEE__Program__c = programId;
        insert category;
        
        FieloEE__Menu__c menu = [SELECT id FROM FieloEE__Menu__c WHERE id =: clonedMenu];
        
        menu.FieloEE__AttachmentId__c = '';
     
        System.debug('menu ' + menu );
   
        System.debug('programId ' + programId );

        menu.FieloEE__Placement__c = 'Main';
        menu.FieloEE__Program__c = programId;
        menu.RecordTypeId = menuRecordTypeId;
        menu.F_DeveloperName__c = '';
        menu.F_PRM_Type__c = 'Page';
        menu.F_PRM_Status__c = 'Draft';
        if( countrychannel != null){
            if(countrychannel.PRMCountry__c != null){
                menu.F_PRM_Country__c = countrychannel.PRMCountry__c;
            }
        }
        menu.F_PRM_Category__c = category.id;
        menu.FieloEE__Menu__c = parentMenu.id;            
        menu.FieloEE__ExternalName__c = 'P-'+  countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        
        menu.FieloEE__ExternalName__c = FieloPRM_UTILS_MenuMethods.validateAndReplaceDuplicateExternalNames(new List<String>{menu.FieloEE__ExternalName__c})[0];
        
        menu.F_PRM_MenuFilter__c = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        if(menu.Name == ''){
            menu.Name = menu.F_PRM_MenuFilter__c;
        }
        
        menu.FieloEE__visibility__c = 'Private';
        system.debug('mapMenu: ' + mapMenu);
        system.debug('selectedMenu: ' + selectedMenu);

        if(countrychannel != null){
            menu.F_PRM_Scope__c = 'Local';
        }
        
        menu.Name = menu.F_PRM_MenuFilter__c;

        FieloEE__RedemptionRule__c newSegment = new FieloEE__RedemptionRule__c();
        newSegment.FieloEE__isActive__c = true ;
        newSegment.F_PRM_Country__c = countrychannel.PRMCountry__c;
        newSegment.F_PRM_SegmentFilter__c = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;

        newSegment.Name = countrychannel.PRMCountry__r.CountryCode__c +' - '+  countrychannel.Sub_Channel__c;   

        newSegment.RecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Manual' AND SObjectType = 'FieloEE__RedemptionRule__c'].id;
        newSegment.F_PRM_Type__c = 'Hybrid';
        newSegment.FieloEE__Program__c = programId;
        insert newSegment;
        
        list<FieloEE__RedemptionRuleCriteria__c> listSegmentCriteriasToInsert = new list<FieloEE__RedemptionRuleCriteria__c>();
         
        FieloEE__RedemptionRuleCriteria__c firstCriteria = new FieloEE__RedemptionRuleCriteria__c();
        firstCriteria.FieloEE__BooleanValue__c = false; 
        firstCriteria.FieloEE__FieldName__c = 'F_PRM_CountryCode__c';
        firstCriteria.FieloEE__FieldType__c = 'Text';
        firstCriteria.FieloEE__Operator__c = 'equals';
        firstCriteria.FieloEE__RedemptionRule__c = newSegment.id;
        firstCriteria.FieloEE__Values__c = countrychannel.F_PRM_CountryCode__c;
        listSegmentCriteriasToInsert.add(firstCriteria);
        
        FieloEE__RedemptionRuleCriteria__c secondCriteria = new FieloEE__RedemptionRuleCriteria__c();
        secondCriteria.FieloEE__BooleanValue__c = false; 
        secondCriteria.FieloEE__FieldName__c = 'F_PRM_Channels__c';
        secondCriteria.FieloEE__FieldType__c = 'Text';
        secondCriteria.FieloEE__Operator__c = 'contains';
        secondCriteria.FieloEE__RedemptionRule__c = newSegment.id;
        secondCriteria.FieloEE__Values__c = countrychannel.SubChannelCode__c; 
        listSegmentCriteriasToInsert.add(secondCriteria);
        
        insert listSegmentCriteriasToInsert;
                    
        menu.FieloEE__RedemptionRule__c = newSegment.Id;
            
        upsert menu;
        
        category.F_PRM_Menu__c = menu.id;
        update category;
        
        System.debug('menu ' + menu);        
        System.debug('category ' + category);

        list<FieloEE__Component__c> componentsRelatedList = [SELECT id, Name,FieloEE__Category__c,F_PRM_ComponentFilter__c FROM FieloEE__Component__c WHERE FieloEE__Menu__c =: menu.id];
        
        System.debug('componentsRelatedList  ' + componentsRelatedList );
        Set<id> setComponentsIds = new Set<id>();
        
        list<FieloEE__Component__c> componentsToUpdateList = new list<FieloEE__Component__c>();
        
        System.debug('componentsToUpdateList ' + componentsToUpdateList );
        
        if(!componentsRelatedList.isEmpty()){
            System.debug('entro for ' );
            for(FieloEE__Component__c com : componentsRelatedList){
                com.FieloEE__Category__c = category.id;
                com.FieloEE__OrderBy__c = '';
                com.F_PRM_Country__c = countrychannel.PRMCountry__c;
                com.F_PRM_ComponentFilter__c = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-'+ com.Name;
                setComponentsIds.add(com.id);
                componentsToUpdateList.add(com);
            }        
        }
        
        System.debug('before uodate compo ' + componentsToUpdateList);
        
        update componentsToUpdateList;
        
        list<FieloEE__Banner__c> directBannersList = [SELECT id, Name ,FieloEE__AttachmentId__c,FieloEE__Category__c,FieloEE__Component__c,FieloEE__Description__c   ,FieloEE__From__c   ,FieloEE__Image__c,FieloEE__isActive__c,FieloEE__Link__c,FieloEE__Order__c,FieloEE__OverlayHtml__c,OverlayHtml_EN__c,OverlayHtml_RU__c,FieloEE__OverlayText__c  ,FieloEE__Placement__c,FieloEE__To__c   ,FieloEE__RedemptionRule__c,F_PRM_ContentFilter__c FROM FieloEE__Banner__c WHERE FieloEE__Component__c IN: setComponentsIds];   
        delete directBannersList;
        list<FieloEE__News__c> directNewsList  = [SELECT id, Name, FieloEE__IsActive__c, FieloEE__AttachmentId__c,F_PRM_Parent__c,FieloEE__Body__c,Body_EN__c,Body_RU__c ,FieloEE__CategoryItem__c,FieloEE__Component__c,FieloEE__ExternalLink__c,FieloEE__ExternalName__c,FieloEE__ExtractText__c ,ExtractText_EN__c,ExtractText_RU__c,FieloEE__HasSegments__c,FieloEE__Image__c,F_PRM_LinkLabel__c   ,F_PRM_LinkLabel_EN__c,F_PRM_LinkLabel_RU__c,F_PRM_LinkNews__c,FieloEE__Order__c,FieloEE__PublishDate__c,FieloEE__QuantityComments__c,FieloEE__QuantityLikes__c,FieloEE__Title__c,Title_EN__c ,Title_RU__c ,FieloEE__Type__c,FieloEE__Category__c,FieloEE__RedemptionRule__c,F_PRM_ContentFilter__c FROM FieloEE__News__c WHERE FieloEE__Component__c IN: setComponentsIds];  
        delete directNewsList ;
        
        System.debug('componentsToUpdateList ' + componentsToUpdateList);

        countrychannel.F_PRM_Menu__c = menu.id;
        update countrychannel;
        
        System.debug('countrychannel' + countrychannel);
        
        System.debug('menu ' + menu);

        return new PageReference('/'+ menu.id );

    }

    public PageReference createNewMenu(){

        String fieloProgramName = 'PRM Is On';

        String programId = [SELECT id FROM FieloEE__Program__c WHERE Name = : FieloProgramName].id;
        
        FieloEE__Category__c category = new FieloEE__Category__c();
        category.RecordTypeId = categoryRecordTypeId;
        category.Name = countrychannel.PRMCountry__r.CountryCode__c + ' - ' + countrychannel.Sub_Channel__c;
        if( countrychannel != null){
            if(countrychannel.PRMCountry__c != null){
                category.F_PRM_Country__c = countrychannel.PRMCountry__c;
            }
        }
        category.F_PRM_CategoryFilter__c = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        category.FieloEE__Program__c = programId;
        insert category;

        FieloEE__Menu__c menu = new FieloEE__Menu__c();

        menu.FieloEE__Placement__c = 'Main';
        menu.FieloEE__Program__c = programId;
        menu.RecordTypeId = menuRecordTypeId;
        menu.F_PRM_Type__c = 'Page';
        menu.F_PRM_Scope__c = 'Local';
        menu.F_PRM_Status__c = 'Draft';
        if( countrychannel != null){
            if(countrychannel.PRMCountry__c != null){
                menu.F_PRM_Country__c = countrychannel.PRMCountry__c;
            }
        }
        menu.F_PRM_Category__c = category.id;
        menu.FieloEE__Menu__c = parentMenu.id; 
        menu.F_PRM_MenuFilter__c  = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        menu.Name = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        if(menu.Name == ''){
            menu.Name = 'P-'+ countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;
        }
        menu.FieloEE__visibility__c = 'Private';
        menu.F_PRM_Type__c = 'Page';
        menu.FieloEE__ExternalName__c = 'P_' + countrychannel.PRMCountry__r.CountryCode__c + '-'+ countrychannel.ChannelCode__c + '-' + countrychannel.SubChannelCode__c + '-' + countrychannel.Sub_Channel__c;     
        
        menu.FieloEE__ExternalName__c = FieloPRM_UTILS_MenuMethods.validateAndReplaceDuplicateExternalNames(new List<String>{menu.FieloEE__ExternalName__c})[0];  
        
        insert menu;
        
        FieloPRM_UTILS_MenuMethods.createEntitySubscriptions(new Set<String>{menu.Id});
        
        category.F_PRM_Menu__c = menu.id;
        update category;

        countrychannel.F_PRM_Menu__c = menu.id;
        
        update countrychannel;
        
        return new PageReference('/'+ menu.id );

    }

    public void auxmethod(){

    }

    public List<SelectOption> getItems() {

        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(' -- Select -- ',' -- Select --')); 
        for(FieloPRM_MenuOptions__c menu:  listMenuOptions){
            options.add(new SelectOption(menu.Name,menu.F_PRM_Label__c)); 
        }

        return options;
    }

    //OPTION OF THE MENU
    //
    //CreateTemplateStructureContent
    //CreateTemplateStructure
    //NewMenu
    //UseTemplate
    public List<SelectOption> getTemplates() {
        
        List<SelectOption> options = new List<SelectOption>(); 
        if(selectedoption != 'NewMenu'){
            for(FieloEE__Menu__c menu:  listMenu){
                options.add(new SelectOption(menu.id,menu.Name)); 
            }
        }          
        return options; 
    }

    public Class TemplateWrapper{
        public boolean check{get;set;}
        public FieloEE__Menu__c menu{get;set;}

        public TemplateWrapper(){
            menu = new FieloEE__Menu__c();
        }
    }

}