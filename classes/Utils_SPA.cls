/*
Author:Siddharth N(GD Solutions)
Purpose:Utility methods to be used for SPA related Stuff.
*/
public class Utils_SPA
{
   
    
    public static Account createAccount(){
        Account acc=Utils_TestMethods.createAccount(); 
        insert acc;
        Country__c country=new Country__c(Name='France',CountryCode__c='FR');
        insert country;
        LegacyAccount__c  legacyAccount=new LegacyAccount__c(Account__c=acc.id,LegacyAccountType__c='PR',LegacyName__c='HK_SAP');
        insert legacyAccount;
        return acc;
    }
    
    public static Opportunity createOpportunity(Id accid){
        Opportunity oppt=Utils_TestMethods.createOpenOpportunity(accid);
        return oppt;
    }
    
    public static SPARequest__c createSPARequest(String type,Id Opptyid)
    {
        SPARequest__c request=new SPARequest__c(ApprovalStatus__c='Draft',BackOfficeSystemID__c='HK_SAP',EndDate__c=System.Today()+10,StartDate__c=System.Today(),Opportunity__c=Opptyid,RequestedType__c=type);       
        return request;
    }    
            
    public static SPARequestLineItem__c createSPARequestLineItem(Id sparequestid,String requestedType)
    {
        SPARequestLineItem__c lineitem;
        if(requestedType=='Multiplier')
         lineitem=new SPARequestLineItem__c(SPARequest__c=sparequestid,CurrencyIsoCode='EUR',ListPrice__c=10000,LocalPricingGroup__c='LPG10',RequestedType__c=requestedType,RequestedValue__c=0.10,StandardDiscount__c=0.50,TargetQuantity__c=1000);
        else if(requestedType=='Discount')
         lineitem=new SPARequestLineItem__c(SPARequest__c=sparequestid,CurrencyIsoCode='EUR',ListPrice__c=10000,LocalPricingGroup__c='LPG20',RequestedType__c=requestedType,RequestedValue__c=0.40,StandardPrice__c=8000,TargetQuantity__c=1000);
        else if(requestedType=='Price')
         lineitem=new SPARequestLineItem__c(SPARequest__c=sparequestid,CurrencyIsoCode='EUR',ListPrice__c=10000,LocalPricingGroup__c='LPG30',RequestedType__c=requestedType,RequestedValue__c=800,StandardPrice__c=8000,TargetQuantity__c=1000);
        else if(requestedType=='Free of charge')
         lineitem=new SPARequestLineItem__c(SPARequest__c=sparequestid,CurrencyIsoCode='EUR',ListPrice__c=10000,LocalPricingGroup__c='LPG30',RequestedType__c=requestedType,RequestedValue__c=800,StandardPrice__c=8000,TargetQuantity__c=1000);
        else if(requestedType=='Advised Discount %')
         lineitem=new SPARequestLineItem__c(SPARequest__c=sparequestid,CurrencyIsoCode='EUR',ListPrice__c=10000,LocalPricingGroup__c='LPG30',RequestedType__c=requestedType,RequestedValue__c=0.40,StandardPrice__c=8000,TargetQuantity__c=1000);
        return lineitem;
    }

     public static SPAThresholdAmount__c createThresholdAmount(String BackofficeSystemId,String type)
    {
        SPAThresholdAmount__c thresholdamount=new SPAThresholdAmount__c(CurrencyIsoCode='EUR',ThresholdAmount0__c=2000,ThresholdAmount1__c=5000,ThresholdAmount2__c=10000,MinimumDealSize__c=500,BackOfficeSystemID__c=BackofficeSystemId,type__c=type);
        return thresholdamount;
    }  
    
      
    public static SPAThresholdDiscount__c createThresholdDiscount(String BackofficeSystemId,String type)
    {    
        SPAThresholdDiscount__c  thresholddiscount;    
        if(type=='Additional Discount')
            thresholddiscount=new SPAThresholdDiscount__c (ThresholdValue0__c=10,ThresholdValue1__c=20,ThresholdValue2__c=30,LocalPricingGroup__c='LPG20',BackOfficeSystemID__c=BackofficeSystemId,type__c=type);
        else if(type=='Final Multiplier')
            thresholddiscount=new SPAThresholdDiscount__c (ThresholdValue0__c=0.95,ThresholdValue1__c=0.90,ThresholdValue2__c=0.50,LocalPricingGroup__c='LPG10',BackOfficeSystemID__c=BackofficeSystemId,type__c=type);
        else if(type=='Compensation%')
            thresholddiscount=new SPAThresholdDiscount__c (ThresholdValue0__c=10,ThresholdValue1__c=20,ThresholdValue2__c=30,LocalPricingGroup__c='LPG30',BackOfficeSystemID__c=BackofficeSystemId,type__c=type);
     
        return thresholddiscount;        
    }
    public static SPASalesGroup__c createSPASalesGroup(String salesgroupname)
    {
        SPASalesGroup__c salesgroup=new SPASalesGroup__c(Name=salesgroupname);
        return salesgroup;
    }
    public static UserParameter__c createUserParameter(Id salesgroupId,Id UserId)
    {
        UserParameter__c param=new UserParameter__c(SPASalesGroup__c=salesgroupId,User__c=UserId);
        return param;
    }
    public static SPAQuota__c createQuota(String backofficeid,String salesgroupid)
    {
        
        SPAQuota__c quota=new SPAQuota__c(BackOfficeSystemID__c=backofficeid,CurrencyIsoCode='EUR',SPASalesGroup__c=salesgroupid,Year__c=String.valueOf(System.Today().year()));
        return quota;
    }
    public static List<SPAQuotaQuarter__c> createQuotaQuarters(Id QuotaId)
    {
        List<SPAQuotaQuarter__c> quotaQuarters=new List<SPAQuotaQuarter__c>();
        quotaQuarters.add(new SPAQuotaQuarter__c(Quarter__c='Q1',Amount__c=1000,SPAQuota__c=QuotaId));
        quotaQuarters.add(new SPAQuotaQuarter__c(Quarter__c='Q2',Amount__c=2000,SPAQuota__c=QuotaId));
        quotaQuarters.add(new SPAQuotaQuarter__c(Quarter__c='Q3',Amount__c=3000,SPAQuota__c=QuotaId));
        quotaQuarters.add(new SPAQuotaQuarter__c(Quarter__c='Q4',Amount__c=4000,SPAQuota__c=QuotaId));
        return quotaQuarters;
    }
    public static List<SPAPricingDeskMember__c> createPricingDeskMembers(String backofficeid,String localpricinggroup )
    {
       User chairmen=Utils_TestMethods.createStandardUser('chairmen');
       insert chairmen;       
       User localpricinggrouplead=Utils_TestMethods.createStandardUser('LPG');
       insert localpricinggrouplead;       
       User permanentmember=Utils_TestMethods.createStandardUser('pm');
       insert permanentmember;       
       List<SPAPricingDeskMember__c> members=new List<SPAPricingDeskMember__c>();
       members.add(new SPAPricingDeskMember__c(MemberType__c='Chairman',Member__c=chairmen.id,BackOfficeSystemID__c=backofficeid));
       members.add(new SPAPricingDeskMember__c(MemberType__c='Local Pricing Group Lead',Member__c=localpricinggrouplead.id,BackOfficeSystemID__c=backofficeid,LocalPricingGroup__c='LPG20'));
       members.add(new SPAPricingDeskMember__c(MemberType__c='Permanent Member',Member__c=chairmen.id,BackOfficeSystemID__c=backofficeid));
       return members;
    }
    
}