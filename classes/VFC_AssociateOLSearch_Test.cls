@isTest
private class VFC_AssociateOLSearch_Test {
    
    static testMethod void testmethod1(){    
       
        Country__c c = Utils_Testmethods.createCountry();
        INSERT c;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.Country__c = c.Id;
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        contact1.Country__c = c.Id;
        insert contact1;
        
        Opportunity opportunity1 = Utils_TestMethods.createOpportunity(account1.Id);
        insert opportunity1;
        
        OPP_Product__c testProduct = Utils_TestMethods.createProduct('Test','Test','Test','Test');
        testProduct.ProductLine__c = NULL;
        testProduct.BusinessUnit__c = 'Test';
        insert testProduct;
        
        OPP_ProductLine__c ProductLine1 = Utils_TestMethods.createProductLine(opportunity1.Id);
        ProductLine1.TECH_CommercialReference__c= 'Commercial\\sReference\\stest1';
        ProductLine1.ProductBU__c = 'Test';
        ProductLine1.ProductLine__c = 'Test';
        ProductLine1.Product__c=testProduct.id;
        insert ProductLine1;
        OPP_ProductLine__c ProductLine2 = Utils_TestMethods.createProductLine(opportunity1.Id);
        ProductLine2.TECH_CommercialReference__c= 'Commercial\\sReference\\stest2';
        ProductLine2.ProductBU__c = 'Test';
        ProductLine2.Product__c=testProduct.id;
        insert ProductLine2;
        
        OPP_ValueChainPlayers__c valcp1= Utils_TestMethods.createValueChainPlayer(account1.Id,contact1.Id,opportunity1.Id);
        insert valcp1;
        
        Competitor__c compt1 = Utils_TestMethods.createCompetitor();
        compt1.Active__c=TRUE;
        insert compt1; 
        
        OPP_OpportunityCompetitor__c oppComp = Utils_TestMethods.createOppCompetitor(compt1.Id,opportunity1.Id);
        insert oppComp;
        
        // for SourceType = COMPOL and Id = oppComp.id
        Pagereference pageref1 = Page.VFP_AssociateOLSearch;
        pageref1.getParameters().put('SourceType','COMPOL');
        pageref1.getParameters().put('Id',oppComp.id);
        Test.setCurrentPage(pageRef1);       
        VFC_AssociateOLSearch assOL1 = new VFC_AssociateOLSearch();
        assOL1.getprodBUS();
        assOL1.prodLine = 'Test';
        assOL1.selProdBU='Test';
        assOL1.searchRecords();
        assOL1.oppLinesList[0].selected = True;
        assOL1.Save();
        VFC_AssociateOLSearch assOL11 = new VFC_AssociateOLSearch();
        assOL1.cancel();
        assOL1.clearSearch();
                
        // for SourceType = VCPOL and Id = valcp1.id
        Pagereference pageref2 = Page.VFP_AssociateOLSearch;
        pageref2.getParameters().put('SourceType','VCPOL');
        pageref2.getParameters().put('Id',valcp1.id);
        Test.setCurrentPage(pageRef2);       
        VFC_AssociateOLSearch assOL2 = new VFC_AssociateOLSearch();
        assOL2.getprodBUS();
        assOL2.prodLine = 'Test';
        assOL2.selProdBU = 'Test';
        assOL2.searchRecords();
        assOL2.oppLinesList[0].selected = True;
        assOL2.Save();
        VFC_AssociateOLSearch assOL22 = new VFC_AssociateOLSearch();
        assOL2.cancel();
        assOL2.clearSearch();
    }
    
    static testMethod void testmethod2(){    
       
        // for empty selected lists of both COMPOL and VCPOL
        Country__c c = Utils_Testmethods.createCountry();
        INSERT c;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.Country__c = c.Id;
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        contact1.Country__c = c.Id;
        insert contact1;
        
        Opportunity opportunity1 = Utils_TestMethods.createOpportunity(account1.Id);
        insert opportunity1;
        
        OPP_Product__c testProduct = Utils_TestMethods.createProduct('Test','Test','Test','Test');
        testProduct.ProductLine__c = NULL;
        testProduct.BusinessUnit__c = 'Test';
        insert testProduct;
        
        OPP_ProductLine__c ProductLine1 = Utils_TestMethods.createProductLine(opportunity1.Id);
        ProductLine1.TECH_CommercialReference__c= 'Commercial\\sReference\\stest1';
        ProductLine1.ProductBU__c = 'Test';
        ProductLine1.ProductLine__c = 'Test';
        ProductLine1.Product__c=testProduct.id;
        insert ProductLine1;
        OPP_ProductLine__c ProductLine2 = Utils_TestMethods.createProductLine(opportunity1.Id);
        ProductLine2.TECH_CommercialReference__c= 'Commercial\\sReference\\stest2';
        ProductLine2.ProductBU__c = 'Test';
        ProductLine2.Product__c=testProduct.id;
        insert ProductLine2;
        
        OPP_ValueChainPlayers__c valcp1= Utils_TestMethods.createValueChainPlayer(account1.Id,contact1.Id,opportunity1.Id);
        insert valcp1;
        
        Competitor__c compt1 = Utils_TestMethods.createCompetitor();
        compt1.Active__c=TRUE;
        insert compt1; 
        
        OPP_OpportunityCompetitor__c oppComp = Utils_TestMethods.createOppCompetitor(compt1.Id,opportunity1.Id);
        insert oppComp;
        
        // for SourceType = COMPOL and Id = oppComp.id
        Pagereference pageref1 = Page.VFP_AssociateOLSearch;
        pageref1.getParameters().put('SourceType','COMPOL');
        pageref1.getParameters().put('Id',oppComp.id);
        Test.setCurrentPage(pageRef1);       
        VFC_AssociateOLSearch assOL1 = new VFC_AssociateOLSearch();
        assOL1.getprodBUS();
        assOL1.prodLine = 'Test';
        assOL1.selProdBU='Test';
        assOL1.searchRecords();
        //assOL1.oppLinesList[0].selected = True;
        assOL1.Save();
        //VFC_AssociateOLSearch assOL11 = new VFC_AssociateOLSearch();
        assOL1.cancel();
        assOL1.clearSearch();
                
        // for SourceType = VCPOL and Id = valcp1.id
        Pagereference pageref2 = Page.VFP_AssociateOLSearch;
        pageref2.getParameters().put('SourceType','VCPOL');
        pageref2.getParameters().put('Id',valcp1.id);
        Test.setCurrentPage(pageRef2);       
        VFC_AssociateOLSearch assOL2 = new VFC_AssociateOLSearch();
        assOL2.getprodBUS();
        assOL2.prodLine = 'Test';
        assOL2.selProdBU = 'Test';
        assOL2.searchRecords();
        //assOL2.oppLinesList[0].selected = True;
        assOL2.Save();
        //VFC_AssociateOLSearch assOL22 = new VFC_AssociateOLSearch();
        assOL2.cancel();
        assOL2.clearSearch();
    }
}