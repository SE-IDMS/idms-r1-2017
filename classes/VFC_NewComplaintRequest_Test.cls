/*
Hanu
Description :Test Class for NewComplaintRequest Button 
*/
@isTest
public class VFC_NewComplaintRequest_Test {

     static Testmethod  void Test_VFC_NewComplaintRequest() {
    
        Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCROwner');
        Insert LCRTestUser ;
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
        Insert LCRTestUserCq ;
        
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
        insert ContactObj;
        
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
        
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
        CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
        Insert CaseObj;
        
         // Create Organization (Complaint request)
         BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='SCH@schneider.com'
                                            );
        Insert lCROrganzObj; 
    
    
    system.debug('Test-------->'+lCROrganzObj);
       
         ComplaintRequest__c  CompRequestObj = new ComplaintRequest__c(
                    Status__c='Open',
                    Case__c  =CaseObj.id,
                    //CustomerImpact__c ='other',
                    Category__c ='1 - Pre-Sales Support',
                    reason__c='Marketing Activity Response',
                    AccountableOrganization__c =lCROrganzObj.id, 
                    ReportingEntity__c =lCROrganzObj.id
                    );
                    
         Insert CompRequestObj; 
          EntityStakeholder__c EntityStack1 = new EntityStakeholder__c();
                EntityStack1.Role__c = 'Complaint/Request creator';
                EntityStack1.User__c =UserInfo.getUserId();
                EntityStack1.BusinessRiskEscalationEntity__c =lCROrganzObj.Id;
                
                insert EntityStack1; 
          ComplaintRequest__c  CompRequestObj1 = new ComplaintRequest__c(
                    Status__c='Open',
                    //Case__c  =CaseObj.id,
                    //CustomerImpact__c ='other',
                    Category__c ='1 - Pre-Sales Support',
                    reason__c='Marketing Activity Response',
                    //AccountableOrganization__c =lCROrganzObj.id
                    ReportingEntity__c =lCROrganzObj.id
                   );
                    
         Insert CompRequestObj1;  
    
        ApexPages.StandardController sc = new ApexPages.standardController(CompRequestObj);
        //ApexPages.StandardSetController con = new ApexPages.StandardSetController(new List<ComplaintRequest__c>{CompRequestObj});
        VFC_NewComplaintRequest  LCRNew= new VFC_NewComplaintRequest(sc);
        ApexPages.CurrentPage().getParameters().put(Label.CL00330,CaseObj.id);
        LCRNew.initializeComplaintRequest(CompRequestObj);
        LCRNew.goToEditPage();
        CaseObj.SupportCategory__c = 'Troubleshooting*';
        update CaseObj;
        LCRNew.goToEditPage();
        CaseObj.Status = 'Closed';
        CaseObj.AnswerToCustomer__c = 'Hello!';
        update CaseObj;
        LCRNew.goToEditPage();
        LCRNew.doCancel();
        
        //caseId equals null
        
        ApexPages.StandardController a_sc = new ApexPages.standardController(CompRequestObj1);
        VFC_NewComplaintRequest  LCRNew1= new VFC_NewComplaintRequest(a_sc);
        ApexPages.CurrentPage().getParameters().put('Label.CL00330',CaseObj.id);
        LCRNew1.initializeComplaintRequest(CompRequestObj);
        LCRNew1.goToEditPage();
        LCRNew1.doCancel();
        
    
     }
}