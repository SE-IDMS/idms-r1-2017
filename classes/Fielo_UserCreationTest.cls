/************************************************************
* Developer: Denis Aranda                                   *
* Type: Test Class                                          *
* Created Date: 23.09.2014                                  *
* Tested Class: Fielo_UserCreation                          *
************************************************************/

@isTest
public with sharing class Fielo_UserCreationTest {
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
        insert deactivate;
        
        Country__c cou = new Country__c(Name = 'United Arab Emirates', CountryCode__c = 'EA', Region__c = 'NAM', InternationalPhoneCode__c = '1');
        Insert cou;
        
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BusinessAccount'];
        
        Account acct = new Account(Name = 'Polo Marco', Street__c = 'Calle Falsa 123', ZipCode__c = '666333', RecordTypeId = rt.Id);
        insert acct;
        
        Contact con = new Contact(firstname = 'Polo', lastname = 'Marco', AccountId = acct.Id, country__c = cou.Id, Email='email@test.com');
        insert con;
        
        FieloEE__Member__c mem = new FieloEE__Member__c(FieloEE__LastName__c= 'Marco', FieloEE__FirstName__c = 'Polo');
        insert mem;
        
        Profile pro = [SELECT Id, Name FROM Profile WHERE Name ='Fielo Member Site' LIMIT 1];
        
        Fielo_UserCreation fie = new Fielo_UserCreation();
        fie.getUser(mem, pro.Id, con.Id);
        
        con.FieloEE__Member__c = mem.Id;
        update con;
    }
}