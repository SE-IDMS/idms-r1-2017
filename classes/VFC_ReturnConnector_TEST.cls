/*
    Author          : Ram Chilukuri (SFDC) 
    Date Created    : 20/March/2013
    Description     : Test class for VFC_ReturnConnector  
*/
@isTest
private class VFC_ReturnConnector_TEST
 {
     static testMethod void testcontrollermethod(){
     
     Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;

    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;

    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;

    insert accountObj;

    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
     Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.CaseSymptom__c = '1';
    Insert CaseObj;
     
    CSE_ExternalReferences__c tempExtRef= new CSE_ExternalReferences__c();
            tempExtRef.Case__c=CaseObj.id;
            tempExtRef.Type__c='Return';
            tempExtRef.CountryOfBackOfficeSystem__c='Global';
            tempExtRef.BackOfficeSystem__c= 'ITB - InTouch';
            insert  tempExtRef;
            
             ITB_Asset__c tempITBAssets= new ITB_Asset__c();
        //tempITBAssets.Name='1-1003350021';
        tempITBAssets.Case__c=tempExtRef.Case__c;
        tempITBAssets.PrimaryFlag__c=true;
        insert tempITBAssets;
            
            Test.SetCurrentPageReference(New PageReference('Page.VFC_ReturnConnector'));
    ApexPages.Standardcontroller SC = New ApexPages.StandardController(tempExtRef);    
    VFC_ReturnConnector CRI = new VFC_ReturnConnector(SC);
    CRI.redirectToextRefereceExternalSystem();
   
             tempExtRef.IDinBackOfficeSystem__c='test';
            update tempExtRef;
            
            Test.SetCurrentPageReference(New PageReference('Page.VFC_ReturnConnector'));
    ApexPages.Standardcontroller SC1 = New ApexPages.StandardController(tempExtRef);    
    VFC_ReturnConnector CRI1 = new VFC_ReturnConnector(SC1);
    CRI1.redirectToextRefereceExternalSystem();
     tempExtRef.IDinBackOfficeSystem__c=null;
     update tempExtRef;
    CaseObj.CaseSymptom__c =null;
    update CaseObj;
   
         Test.SetCurrentPageReference(New PageReference('Page.VFC_ReturnConnector'));
    ApexPages.Standardcontroller SC2 = New ApexPages.StandardController(tempExtRef);    
    VFC_ReturnConnector CRI2 = new VFC_ReturnConnector(SC2);
    CRI2.redirectToextRefereceExternalSystem();
    
    CaseObj.CaseSymptom__c = '1';
    update CaseObj;
     tempITBAssets.Case__c=null;
   tempITBAssets.PrimaryFlag__c=false;
   update tempITBAssets;
     Test.SetCurrentPageReference(New PageReference('Page.VFC_ReturnConnector'));
    ApexPages.Standardcontroller SC3 = New ApexPages.StandardController(tempExtRef);    
    VFC_ReturnConnector CRI3 = new VFC_ReturnConnector(SC3);
    CRI3.redirectToextRefereceExternalSystem();
  
   }
}