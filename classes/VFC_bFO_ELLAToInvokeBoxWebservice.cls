/******
Release :April 2015
Comments: Method which call the BOX API Method from Button 
Modified : March Q1 
******/

global class  VFC_bFO_ELLAToInvokeBoxWebservice {

    public static string serviceResult{get;set;} 
    public static boolean isMessage{get;set;}
    public string OfferId;    
    public string ProjectId;
    
    public VFC_bFO_ELLAToInvokeBoxWebservice() {
        isMessage=true;
    }

   
     public void invokeOfferBoxWebservice() {
         system.debug('TEST-------->');
        try{
            //string  offerid;
           // if(ApexPages.currentPage().getParameters().get('id') != ''){
            //offerid = ApexPages.currentPage().getParameters().get('id');
            //}
            LIST<Offer_Lifecycle__c> listcoin = new LIST<Offer_Lifecycle__c>();
            serviceResult =label.CLAPR15ELLAbFO10;//'Please try after some times';
            
            if(offerId!=null) {
                system.debug('TEST-------->3445'+offerId);
                string jobApexClassId ='01p170000005BTE';
                
                boxResponseObject boxRes=new boxResponseObject();
                List<AsyncApexJob>  apexJob=[SELECT ApexClassId,ExtendedStatus,Id,JobItemsProcessed,JobType,NumberOfErrors,ParentJobId,Status,TotalJobItems FROM AsyncApexJob WHERE Status = 'Processing' and ApexClassId =:jobApexClassId];
                system.debug('TEST-------->31445'+offerId);
                if(apexJob.size() ==0) {
                    LIST<Offer_Lifecycle__c> coin_List =[select Id, BoxUserEmailId__c,Launch_Owner__c,Launch_Owner__r.email,Name,BoxSharedLink__c,BoxFolderId__c from Offer_Lifecycle__c where Id=: offerId];
              
                    
                        
                for( Offer_Lifecycle__c coibObj1:coin_List) {
                    system.debug('TEST-------->'+coibObj1);
                    if((coibObj1.BoxFolderId__c==null || coibObj1.BoxFolderId__c=='' ) && coibObj1.BoxUserEmailId__c!=null) {
                        system.debug('TEST-HANAN------->'+coibObj1);
                        /*****New folder and file creation IN BOX ******/
                        string folder_id=AP_ELLAToBoxConnect.folderStructureCreation(coibObj1.Name.replace('/', '') ,null,coibObj1.BoxUserEmailId__c); 
                        if(folder_id != null) {
                            boxRes = (boxResponseObject)JSON.deserialize(folder_id, boxResponseObject.class);
                            serviceResult =label.CLAPR15ELLAbFO11;//'Folder is  created successfully ';

                        }
                        if(boxRes.folderId!=null || boxRes.folderId!='') {
                            coibObj1.BoxFolderId__c=boxRes.folderId;
                            coibObj1.BoxSharedLink__c =boxRes.folderShareLink;
                            coibObj1.BoxCollaboration__c=boxRes.CollaborationId;
                            listcoin.add(coibObj1);
                        }
                    } else if(coibObj1.BoxUserEmailId__c==null) {
                        serviceResult=Label.CLAPR15ELLAbFO12;//'Please enter the Box User Email id';
                    }

                }
                    
                if(listcoin.size() >0) {

                    database.saveresult[] srlist = database.update(listcoin, false);
                    system.debug('HHHHddddH'+srlist);    
                }
                    
                } 
                else {
                    serviceResult=label.CLAPR15ELLAbFO13;//'Batch Job is running to create a folder in BOX . Please try after some times';
                
                }
            
            }
            //return serviceResult;
        }
        catch(exception exp) {
            serviceResult=exp.getmessage();
           //return serviceResult;
        }

    }  
    public  void invokeProjectBoxWebservice() {
        
        string folder_id;
        //string  ProjectId ;
        //if(ApexPages.currentPage().getParameters().get('id') != ''){
         //   ProjectId = ApexPages.currentPage().getParameters().get('id');
       // }
        LIST<Milestone1_Project__c> listProject = new LIST<Milestone1_Project__c>();
        Set<string>  setOfferId = new Set<string>();
        Set<string>  setCountryId = new Set<string>();
         Set<string>  setZoneId = new Set<string>();
        List<Milestone1_Project__c> projectCountry = new  List<Milestone1_Project__c> ();
        LIST<Milestone1_Project__c> projectList = new LIST<Milestone1_Project__c>();
        serviceResult =label.CLAPR15ELLAbFO10;//'Please try after some times';
        string jobApexClassId ='01p170000005BTE';           
        boxResponseObject boxProjectRes=new boxResponseObject();
        try {
        if(ProjectId!=null) {
            
            List<AsyncApexJob>  apexJob=[SELECT ApexClassId,ExtendedStatus,Id,JobItemsProcessed,JobType,NumberOfErrors,ParentJobId,Status,TotalJobItems FROM AsyncApexJob WHERE Status = 'Processing' and ApexClassId =:jobApexClassId];
            if(apexJob.size() ==0) {
                projectList =[select Id,Name, BoxUserEmailId__c,Zone__c,Country_Launch_Leader__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Id=: ProjectId];
              
              for(Milestone1_Project__c proOffObj:projectList) {
                    setOfferId.add(proOffObj.Offer_Launch__c);
                    setCountryId.add(proOffObj.Country__r.name);
                    setZoneId.add(proOffObj.Zone__c);
              }
            
            projectCountry=[select Id,Name, BoxUserEmailId__c,Zone__c,Country_Launch_Leader__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Offer_Launch__c=:setOfferId and (Country__r.name=:setCountryId AND Zone__c=:setZoneId) ];
             
             
                if(projectList.size() > 0 && projectCountry.size() ==1) {
                
                    /*****New folder and file creation IN BOX ******/
                    for(Milestone1_Project__c proObj:projectList) {
                    
                        if(proObj.Offer_Launch__r.BoxFolderId__c!=null && proObj.BoxFolderId__c==null && (proObj.Country__r.name!=null || proObj.Zone__c!=null) && proObj.BoxUserEmailId__c!=null)  {  
                            if(proObj.Country__r.name!=null) {
                                folder_id=AP_ELLAToBoxConnect.folderStructureCreation (proObj.Country__r.name,proObj.Offer_Launch__r.BoxFolderId__c,proObj.BoxUserEmailId__c); 
                            }
                            if(proObj.Zone__c!=null) {
                                folder_id=AP_ELLAToBoxConnect.folderStructureCreation (proObj.Zone__c,proObj.Offer_Launch__r.BoxFolderId__c,proObj.BoxUserEmailId__c);
                            }
                            
                            system.debug('TEST--->'+folder_id);
                            if(folder_id != null) {
                                boxProjectRes = (boxResponseObject)JSON.deserialize(folder_id, boxResponseObject.class);
                                if(boxProjectRes.folderId!=null || boxProjectRes.folderId!='') {
                                    serviceResult =label.CLAPR15ELLAbFO11;//'Folder is created successfully ';
                                    proObj.BoxFolderId__c=boxProjectRes.folderId;
                                    proObj.BoxSharedLink__c =boxProjectRes.folderShareLink;
                                     proObj.BoxCollaboration__c=boxProjectRes.CollaborationId;
                                    listProject.add(proObj);
                                }
                                

                            }

                            
                        } else if(proObj.Offer_Launch__r.BoxFolderId__c==null && proObj.Country__r.name!=null) {
                             
                            serviceResult=label.CLAPR15ELLAbFO14; //'Parent folder is not created in Box ';
               
                        } else if(proObj.BoxUserEmailId__c==null) {
                            serviceResult=Label.CLAPR15ELLAbFO12;//'Please enter the Box User Email id';
                        }

                        
                    }
                    
                    if(listProject.size() > 0) {
                    
                        database.saveresult[] srlist = database.update(listProject, false);
                               
                    }
                        
                }else {
                    system.debug('Test-->'+projectCountry);
                    Map<string,Milestone1_Project__c> mapProjt= new Map<string,Milestone1_Project__c>();
                    list<Milestone1_Project__c>  updateMProject = new list<Milestone1_Project__c>();
                    List<Milestone1_Project__c> lisIndroduction = new List<Milestone1_Project__c>();
                    for(Milestone1_Project__c mpObj:projectCountry) {
                    
                            if(mpObj.BoxFolderId__c!=null && mpObj.Country__r.name!=null) {
                               
                                    mapProjt.put(mpObj.Country__r.name,mpObj);
                               
                            }
                    }
                    
                    for(Milestone1_Project__c mpObj1:projectCountry) {
                        
                        if(mpObj1.BoxFolderId__c==null && mpObj1.Country__r.name!=null) {
                            if(mapProjt.containsKey(mpObj1.Country__r.name)) {
                                mpObj1.BoxFolderId__c=mapProjt.get(mpObj1.Country__r.name).BoxFolderId__c;
                                mpObj1.BoxSharedLink__c  =mapProjt.get(mpObj1.Country__r.name).BoxSharedLink__c ;
                                
                                updateMProject.add(mpObj1);
                            }
                           

                        }
                    
                    }
                    for(Milestone1_Project__c mpObj2:projectCountry) {
                         if(mpObj2.BoxUserEmailId__c!=null && mpObj2.BoxFolderId__c==null ) {
                            system.debug('Test-->'+mpObj2.BoxUserEmailId__c);
                            system.debug('Test-->1'+mapProjt);
                                if(mpObj2.BoxUserEmailId__c !=mapProjt.get(mpObj2.Country__r.name).BoxUserEmailId__c) {
                                    lisIndroduction.add(mpObj2);
                                }   
                            }
                    }
                    system.debug('Test-->'+updateMProject);
                    if(updateMProject.size() > 0) {
                        update updateMProject;
                        
                        if(lisIndroduction.size() > 0) {
                            for(Milestone1_Project__c mProjectObj:lisIndroduction) {
                                string collabora=AP_ELLAToBoxConnect.box_collaboraFolder(mProjectObj.Offer_Launch__r.BoxFolderId__c,mProjectObj.BoxUserEmailId__c);

                            }   
                        }
                        
                        serviceResult =label.CLAPR15ELLAbFO11;//'Folder is created successfully ';
                    }                   
                    
                }             
                               
            }else {
                serviceResult=label.CLAPR15ELLAbFO13;//'Batch JOb is running to create a Folder in BOX . Please try after some times';
            }
                
        }
       // return serviceResult;
        }catch(exception expBox) {
            serviceResult=expBox.getmessage();
            //return serviceResult;
        
        }

    }   
    
    public void addOfferBoxCollaborater(string strOfferId ) {
    
        LIST<Offer_Lifecycle__c> listOfferUpdate = new LIST<Offer_Lifecycle__c>();
        try {

            boxResponseObject boxRes=new boxResponseObject();
            string Collabora_id;
            LIST<Offer_Lifecycle__c> offerCalbList =[select Id, BoxCollaboration__c,BoxUserEmailId__c,Launch_Owner__c,Launch_Owner__r.email,Name,BoxSharedLink__c,BoxFolderId__c from Offer_Lifecycle__c where Id=: strOfferId and BoxFolderId__c!=null];

            if(offerCalbList.size() > 0)  {

                 for(Offer_Lifecycle__c offerObj:offerCalbList) {
          
                    if(offerObj.BoxFolderId__c!=null && offerObj.BoxUserEmailId__c!=null && offerObj.BoxCollaboration__c==null ) {
                    
                        /*****New folder creation IN BOX ******/
                        Collabora_id=AP_ELLAToBoxConnect.addBoxCollaboraFolder(offerObj.BoxFolderId__c,offerObj.BoxUserEmailId__c); 
                        if(Collabora_id != null) {
                            boxRes = (boxResponseObject)JSON.deserialize(Collabora_id, boxResponseObject.class);
                            serviceResult =label.CLAPR15ELLAbFO19;//added Collaborator 
                        }
                        
                        if(boxRes.CollaborationId!=null || boxRes.CollaborationId!='' ) {    
                            offerObj.BoxCollaboration__c=boxRes.CollaborationId;
                            //offerObj.BoxSharedLink__c =boxRes.folderShareLink;
                            listOfferUpdate.add(offerObj);
                        
                        }               
                                                
                        system.debug('HHHHddddH'+Collabora_id);    
                    }
                }
                
                if(listOfferUpdate.size() > 0 ) {
                    database.saveresult[] srlist = database.update(listOfferUpdate, false);                    
                }
            }
            
        
        
        }catch (exception exp) {
            serviceResult=exp.getmessage();
        
        }

    }   
    
    public void addProjectBoxCollaborater(string strCalbProjectId ) {
    
         try{
        
            LIST<Milestone1_Project__c> listColbProject = new LIST<Milestone1_Project__c>(); 
            string projectCollabora_id;        
            boxResponseObject boxProjectRes=new boxResponseObject();
            list<Milestone1_Project__c>  updateMProject = new list<Milestone1_Project__c>();
            LIST<Milestone1_Project__c> projectList =[select Id,Name, BoxUserEmailId__c,Country_Launch_Leader__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Id=: strCalbProjectId and BoxFolderId__c!=null];
        
            if(projectList.size() > 0) {
                 
                for(Milestone1_Project__c proObj:projectList) {
                    if(proObj.Offer_Launch__r.BoxFolderId__c!=null && proObj.BoxFolderId__c!=null && proObj.Country__r.name!=null && proObj.BoxUserEmailId__c!=null)  {  
                        projectCollabora_id=AP_ELLAToBoxConnect.addBoxCollaboraFolder(proObj.BoxFolderId__c,proObj.BoxUserEmailId__c); 
                        system.debug('--->'+projectCollabora_id);
                        if(projectCollabora_id != null) {
                            boxProjectRes= (boxResponseObject)JSON.deserialize(projectCollabora_id, boxResponseObject.class);
                            serviceResult =label.CLAPR15ELLAbFO19;//added Collaborator 
                           
                        }
                        
                         if(boxProjectRes.CollaborationId!=null || boxProjectRes.CollaborationId!='' ) {    
                            proObj.BoxCollaboration__c=boxProjectRes.CollaborationId;
                            //offerObj.BoxSharedLink__c =boxRes.folderShareLink;
                            updateMProject.add(proObj);
                        
                        }
                    }

                }
                 if(updateMProject.size() > 0 ) {
                    database.saveresult[] srlist = database.update(updateMProject, false);                    
                }

            }
        
        }catch (exception exp) {
            serviceResult=exp.getmessage();
        }
    
    
    }
    
    public Pagereference InvokeBoxAPICall() {
    
    // Collaborations
        string strSelected;
        string strOfferid;
        string strProjectid;
        if(ApexPages.currentPage().getParameters().get(label.CLAPR15ELLAbFO25) !='' &&  ApexPages.currentPage().getParameters().get('id')!='' ){
        
            strSelected= ApexPages.currentPage().getParameters().get(label.CLAPR15ELLAbFO25);
            system.debug('Sch-->'+strSelected);
            if(strSelected==label.CLAPR15ELLAbFO23) {//'CBROffer'
            
                OfferId=ApexPages.currentPage().getParameters().get(label.CLAPR15ELLAbFO24);//id
                system.debug(OfferId);
                addOfferBoxCollaborater(OfferId);
                
            
            }
            if(strSelected==label.CLAPR15ELLAbFO20) { //'CBRProject'
             system.debug('Sch-->'+strSelected);
                strProjectid=ApexPages.currentPage().getParameters().get(label.CLAPR15ELLAbFO24);
                system.debug('Sch-->'+strProjectid);
                //30/04/2015
                ProjectId=strProjectid;
                if(strProjectid!='' || strProjectid!=null) {
                    addProjectBoxCollaborater(strProjectid);
                    system.debug('Inside-->'+strProjectid);
                }
            }
            
            if(strSelected==label.CLAPR15ELLAbFO21) {//'fdrOffer'
            
                OfferId=ApexPages.currentPage().getParameters().get(label.CLAPR15ELLAbFO24);
                system.debug(OfferId);
                invokeOfferBoxWebservice();
                
            
            }
            if(strSelected==label.CLAPR15ELLAbFO22) {//'fdrProject'
            
                ProjectId=ApexPages.currentPage().getParameters().get(label.CLAPR15ELLAbFO24);
                invokeProjectBoxWebservice();
            }
            
            
            
        }
        return null;
    
    
    }
    
    
    
    public class boxResponseObject {
        string folderId{get;set;}
        string folderShareLink{get;set;}
        string CollaborationId{get;set;}  


    }
    
    Public Pagereference doCancel(){
    
        Pagereference pr ;
        if(ProjectId!=null ) {  
             pr = new PageReference('/'+ ProjectId);
             pr.setredirect(true);
             
         }
         if(OfferId!=null) {
            pr = new PageReference('/'+ OfferId);
            pr.setredirect(true);
            
         
         }    
        return pr;
    }      
      
}