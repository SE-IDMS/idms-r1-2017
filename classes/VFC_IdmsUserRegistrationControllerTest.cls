@isTest public class VFC_IdmsUserRegistrationControllerTest{


public static testMethod void idmsRegisterUserTest(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();


testUser.autoRun();
testUser.ucountry='IN';
testUser.displayPopup=true;
testUser.email=null;
testUser.save();
}
public static testMethod void idmsRegisterUserTest1(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();

testUser.ucountry='IN';
testUser.email='asdasd';
testUser.save();
}
public static testMethod void idmsRegisterUserTest2(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.email='asdasd@accenture.com';
testUser.firstName=null;
testUser.lastName='abc';
testUser.save();
}
public static testMethod void idmsRegisterUserTest3(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.email='asdasd@accenture.com';
testUser.firstName='abc';
testUser.lastName='abc';
testUser.phomenum='';
testUser.save();
}
public static testMethod void idmsRegisterUserTest4(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.firstName='abc';
testUser.lastName='abc';
testUser.email='asdasd@accenture.com';
testUser.phomenum='abc';
testUser.checked=false;
testUser.save();
}
public static testMethod void idmsRegisterUserTest5(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.firstName='abc';
testUser.lastName='abc';
testUser.email='asdasd@accenture.com';
testUser.phomenum='1234567890';
testUser.checked=false;
testUser.save();
}
public static testMethod void idmsRegisterUserTest6(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.firstName='abc';
testUser.lastName='abc';
testUser.email='asdasd@accenture.com';
testUser.phomenum='1234567890';
testUser.checked=false;
testUser.save();
}
public static testMethod void idmsRegisterUserTest7(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.firstName='abc';
testUser.lastName='abc';
testUser.email='tester321@test.com';
testUser.checked=true;
testUser.enterRecaptchcorrect=false;
testUser.phomenum='1234567890';

testUser.save();
}
public static testMethod void idmsRegisterUserTest8(){
IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
       termMap.name = 'test';
       termMap.url__c= 'test';
       insert termMap;
       
       IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'test';
       appMap.AppURL__c= 'test';
       insert appMap;
       
       IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
       CountryMap.name = 'IN';
       CountryMap.DefaultCurrencyIsoCode__c = 'INR';
       insert CountryMap;

PageReference mypage= Page.UserRegistration;
Test.setCurrentPage(mypage);

ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
ApexPages.currentpage().getparameters().put('app' ,'test');
ApexPages.currentpage().getparameters().put('context' ,'test');
VFC_IdmsUserRegistrationController testUser=new VFC_IdmsUserRegistrationController();
testUser.ucountry='IN';
testUser.firstName='abc';
testUser.lastName='abc';
testUser.checked=true;
testUser.email='asdas11d@accenture.com';
testUser.enterRecaptchcorrect=true;
testUser.phomenum='1234567890';
testUser.showCaptcha=true;
testUser.save();
testUser.showCaptcha=false;
testUser.save();
}
}