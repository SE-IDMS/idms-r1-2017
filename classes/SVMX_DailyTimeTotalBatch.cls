/*
* Author: Chris Hurd
* Date: 2013/05/27
* Description: This batch class is used to aggregate the time totals for the previous day 
* and place it in Technician Daily Total object.
* Also, Round decimal to the nearest quarter-
*/
global class SVMX_DailyTimeTotalBatch implements Database.Batchable<sObject>{
    
    String query= 'SELECT ID, Technician__r.SVMXC__Salesforce_User__c, OwnerId, Technician__r.SVMXC__Salesforce_User__r.TimeZoneSidKey FROM SVMXC_Timesheet__c WHERE Start_Date__c >= LAST_N_DAYS:60';
    
    
    global SVMX_DailyTimeTotalBatch()
    {
    
    }
    
    global SVMX_DailyTimeTotalBatch(String qval)
    {
        query = qval;
    }
    
    global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Map<Id, SVMXC_Timesheet__c> tsMap = new Map<Id, SVMXC_Timesheet__c>((List<SVMXC_Timesheet__c>) scope);
        Map<Id, String> tzOffsetMap = new Map<Id, String>();
        Set<Id> ownerIdSet = new Set<Id>();
        for (SVMXC_Timesheet__c ts : tsMap.values())
        {
            system.debug(ts);
            tzOffsetMap.put(ts.Technician__c, ts.Technician__r.SVMXC__Salesforce_User__r.TimeZoneSidKey);
            ownerIdSet.add(ts.OwnerId);
        }
        
        for (User u : [SELECT Id, TimeZoneSidKey FROM User WHERE Id IN :ownerIdSet])
        {
            tzOffsetMap.put(u.Id, u.TimeZoneSidKey);
        }
        
        system.debug(tzOffsetMap);
        
        for (List<Time_Sheet_Daily_Totals__c> tsdList : [SELECT Id FROM Time_Sheet_Daily_Totals__c WHERE Timesheet__c IN :tsMap.keySet()])
        {
           delete tsdList;
        }
        
        Map<Id, Map<String, Time_Sheet_Daily_Totals__c>> tsdMap = new Map<Id, Map<String, Time_Sheet_Daily_Totals__c>>();
        List<Time_Sheet_Daily_Totals__c> tsdList = new List<Time_Sheet_Daily_Totals__c>();
        
        for (SVMXC_Time_Entry__c tEntry : [SELECT Id, Start_Date_Time__c, Technician__c, Timesheet__c, OwnerID, Total_Time__c FROM SVMXC_Time_Entry__c WHERE Timesheet__c IN :tsMap.keySet() AND Activity__c != 'Time Off'])
        {
            if (! tsdMap.containsKey(tEntry.Technician__c))
            {
                tsdMap.put(tEntry.Technician__c, new Map<String, Time_Sheet_Daily_Totals__c>());
            }
            
            String strDate = null;
            
            if (tEntry.Technician__c != null)
            {
                strDate = tEntry.Start_Date_Time__c.addSeconds(TimeZone.getTimeZone(tzOffsetMap.get(tEntry.Technician__c)).getOffSet(tEntry.Start_Date_Time__c) / 1000).formatGmt('yyyyMMdd');
            }
            else
            {
                strDate = tEntry.Start_Date_Time__c.addSeconds(TimeZone.getTimeZone(tEntry.OwnerId).getOffSet(tEntry.Start_Date_Time__c) / 1000).formatGmt('yyyyMMdd');
            }
            
            Time_Sheet_Daily_Totals__c tsd = null;
            
            if (tsdMap.get(tEntry.Technician__c).containsKey(strDate))
            {
                tsd = tsdMap.get(tEntry.Technician__c).get(strDate);
            }
            else
            {
                tsd = new Time_Sheet_Daily_Totals__c();
                tsd.Technician__c = tEntry.Technician__c;
                tsd.Time_Entry_End_Date__c = strDate;
                tsd.Time_Entry_Start_Date__c = strDate;
                tsd.Timesheet__c = tEntry.Timesheet__c;
                tsd.Time_Entry_Total__c = 0;
                tsdMap.get(tEntry.Technician__c).put(strDate, tsd);
                tsdList.add(tsd);
            }
            
            tsd.Time_Entry_Total__c += tEntry.Total_Time__c;
        }
        
        for (Time_Sheet_Daily_Totals__c tsd : tsdList)
        {
            Decimal total = tsd.Time_Entry_Total__c;
            Long rounded = Math.mod((total * 100.0).round(), 25);
            total += (Decimal) (rounded > 0 ? (25 - rounded)/100.0 : 0);
            tsd.Time_Entry_Total__c = total;
        }
        
        insert tsdList;
        
    }
     
    global void finish(Database.BatchableContext BC)
    {
            
    }
    
    webservice static void executeBatch()
    {
        SVMX_DailyTimeTotalBatch batch = new SVMX_DailyTimeTotalBatch('SELECT ID, Technician__r.SVMXC__Salesforce_User__c, OwnerId, Technician__r.SVMXC__Salesforce_User__r.TimeZoneSidKey FROM SVMXC_Timesheet__c');
        database.executeBatch(batch, 10);
    }
    


}