@isTest
Public Class VFC_SRV_MyFSInternalUserLandingCtrl_TEST{
   Static TestMethod Void  Unitest(){
    Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
   ][0].Id;
  
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  Account acc1 = new Account(Name = 'TestAccount1',Country__c = Country.Id,Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '111');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname11');
  insert cnct;
  
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

 /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'EN', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;*/
  //system.runAs(u) {
  
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
  //  test.starttest();
 
    VFC_SRV_MyFSInternalUserLandingContrller Ctrl = new VFC_SRV_MyFSInternalUserLandingContrller();
    Apexpages.currentPage().getParameters().Put('accountId',acc1.Id);
    Ctrl.validateLoggedInUser();
    Ctrl.callpageredirect(Country.CountryCode__c,acc1.Id,location1.id);
   }
}