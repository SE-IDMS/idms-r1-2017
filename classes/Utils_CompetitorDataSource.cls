/*
    Author          : Accenture IDC Team 
    Date Created    : 25/07/2011
    Description     : Utility class for Competitor Data Access extending abstract Data Source Class
*/

public class Utils_CompetitorDataSource extends Utils_DataSource
{     
     //    Query Parameters - Main Query and Different cluases which will be used conditionally
        static String query1 = 'SELECT Id, Name, MultiBusiness__c, ParentCompany__c,(SELECT Id, Country__c, Country__r.Name FROM CompetitorCountry__r) FROM Competitor__c';
        static String query2 = ' where ';
        static String query3 = ' AND ';
        static String query4 = 'Name like \'%';
        static String query5 = '%\'';
        static String query6 = 'MultiBusiness__c INCLUDES (:business)';
        static String query7 = 'Id IN (select Competitor__c from CompetitorCountry__c where country__c = \'';
        static String query8 = '\')';
        static String query9 = ' ORDER BY Name';
     //    End of Query String cretaion
                          
     // Define Columns Label and displayed field  
     public override List<SelectOption> getColumns()
     {
        List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Field1__c',sObjectType.Competitor__c.fields.Name.getLabel())); // First Column
        Columns.add(new SelectOption('Field2__c',sObjectType.Competitor__c.fields.ParentCompany__c.getLabel())); 
        Columns.add(new SelectOption('Field3__c',Label.CL00351)); 
        Columns.add(new SelectOption('Field4__c',sObjectType.Country__c.getLabelPlural())); // Last Column
        return Columns;
     }
        
     //    Main Search Method
     public override Result Search(List<String> keyWords, List<String> filters)
     {
        //    Initializing Variables      
        String searchText; 
        String business;
        String country;
        List<sObject> results = New List<sObject>();
        List<sObject> tempResults = New List<sObject>();
        Utils_Sorter sorter = new Utils_Sorter();
        List<Id> compId = New List<Id>();
        Utils_DataSource.Result returnedResult = new Utils_DataSource.Result();   
        returnedResult.recordList = new List<DataTemplate__c>();
        Map<Object, String> countries = New Map<Object, String>();
        Map<String,sObject> resultData = New Map<String,sObject>();
        Map<Object, String> pickList = New Map<Object, String>();
        Map<Integer,DataTemplate__c> resultMap = new Map<Integer,DataTemplate__c>();
        List<Object> countryId = New List<Object>();
        List<sObject> resultset = New List<sObject>();
        //    End of Variable Initialization
        
        //    Assigning the User input Search Filter and Search criteria to the Variables
        if(!(filters[0].equalsIgnoreCase(Label.CL00355)))
            business = filters[0];
        if(!(filters[1].equalsIgnoreCase(Label.CL00355)))
            country = filters[1];

        if(keywords != null && keywords.size()>0)
            searchText = keyWords[0];
        else 
            searchText = '';
        //    End of Assignment
        
        System.debug('#### Competitor Search Parameters #####');
        System.debug('#### Search Text: ' + searchText);
        System.debug('#### Business: ' + business);
        System.debug('#### Country: ' + country);
                          
        //    Searching the Competitor Table to get all the combination the List of Competitors
        //    and all the Country they are associated to
        if(searchText != NULL && searchText!= '')
        {
            if(business != NULL)
            {
                if(country != NULL)
                    results = Database.query(query1+query2+query4+searchText+query5+query3+query6+query3+query7+country+query8+query9);                    
                else
                    results = Database.query(query1+query2+query4+searchText+query5+query3+query6+query9);
            }
            else
            {
                if(country != NULL)
                    results = Database.query(query1+query2+query4+searchText+query5+query3+query7+country+query8+query9);
                else
                    results = Database.query(query1+query2+query4+searchText+query5+query9);
            }
        }
        else
        {
            if(business != NULL)
            {
                if(country != NULL)
                    results = Database.query(query1+query2+query6+query3+query7+country+query8+query9);
                else
                    results = Database.query(query1+query2+query6+query9);
            }
            else
            {
                if(country != NULL)
                    results = Database.query(query1+query2+query7+country+query8+query9);
                else
                    results = Database.query(query1+query9);
            }
        }
        
        
        
        System.debug('#### results: ' +  results );
        //    End of Search
                
        //    Creation of Map of Business Picklist Value and corresponding Label
        Schema.DescribeFieldResult businessPickVal = Schema.sObjectType.Competitor__c.fields.MultiBusiness__c;
        for (Schema.PickListEntry PickVal : businessPickVal.getPicklistValues())
        {
            pickList.put((Object)PickVal.getValue(),PickVal.getLabel());
        }
        //    End of Picklist Map creation 
        
        //    Creation of Map of SFDC ID of the Country and Country Name
        for(Country__c cons : [Select Id, Name from Country__c])
        {
                countries.put((Object)cons.Id, cons.Name);
        }    
        //    End of Country Map creation       
        
        //    Start of processing the Saerch Result to prepare the final result set
        tempResults.clear();
        for (sObject obj : results)
        { 
            Competitor__c records = (Competitor__c)obj;
            DataTemplate__c comp = New DataTemplate__c ();
            comp.put('Field1__c',records.get('Name'));
            if(records.get('ParentCompany__c') != null)
                comp.put('Field2__c',records.get('ParentCompany__c'));
            if(records.get('MultiBusiness__c') != null)
            {
                System.debug('#### Competitor MultiBusiness__c:' + records.get('MultiBusiness__c'));
                
                List<String> BusinessValueList = ((String)records.get('MultiBusiness__c')).split(';',0);
                String MultiBusinessLabel='';
                
                For(Integer i=0; i<BusinessValueList.size(); i++)
                {
                   if(i>0)
                   {
                       MultiBusinessLabel += '; ';
                   }    
                   System.debug('#### Index = ' + i  + ' - Current MultiLabel ' + BusinessValueList[i] );

                   MultiBusinessLabel += pickList.get(BusinessValueList[i]);
                   
                }
                
                comp.put('Field3__c', MultiBusinessLabel);
                
                System.debug('#### Comp Field 3:' + comp.field3__c);
                
            }
            // Prevent a null relationship from being accessed  
            sObject[] countryOfComp = obj.getSObjects('CompetitorCountry__r');
            if (countryOfComp != null) 
            {
                for (sObject sCountries : countryOfComp)
                { 
                    if(comp.Field4__c == null)
                    {
                        if(sCountries.get('Country__c') != null)
                        {
                            comp.put('Field4__c', countries.get(sCountries.get('Country__c')));
                            comp.put('Field5__c', sCountries.get('Country__c'));
                        }
                    }
                    else
                    {
                        if(sCountries.get('Country__c') != null)
                        {
                            comp.put('Field4__c', comp.Field4__c+'; '+countries.get(sCountries.get('Country__c')));
                            comp.put('Field5__c', comp.Field5__c+'; '+sCountries.get('Country__c'));
                        }
                    } 
                }
            }
            tempResults.add(comp);              
        }
        System.debug('#### Results without Country Filter: ' +  tempResults);
        returnedResult.recordList.clear();
        
        returnedResult.recordList.addAll(tempResults);
        
        //    End of Processing and preparation of the final result Set
        
        returnedResult.recordList = (List<sObject>)sorter.getSortedList(returnedResult.recordList, 'Field1__c', true);
        System.debug('#### Final Result :' + returnedResult);
        
        resultset.addAll(returnedResult.recordList);
        returnedResult.numberOfRecord = resultset.size();

        if(returnedResult.numberOfRecord >100)
        {
            returnedResult.recordList.clear();
            for(Integer count=0; count< 100; count++)
            {
                returnedResult.recordList.add(resultset[count]);      
            }
        }
        
        return returnedResult;
     }
}