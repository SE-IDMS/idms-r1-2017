/**
13-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for VFC_ShowAccountRelatedDetails
 */
@isTest
private class VFC_ShowAccountRelatedDetails_TEST {

    static testMethod void myUnitTest() {
    
        //Test converage for the myPage visualforce page

        PageReference pageRef = Page.VFP_ShowAccountRelatedDetails;
        
        Test.setCurrentPageReference(pageRef);
        
        Account testAccount = Utils_TestMethods.createAccount();
        
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(testAccount );
        VFC_ShowAccountRelatedDetails myPageCon = new VFC_ShowAccountRelatedDetails(sdtCon);
        
        insert testAccount;
        
        list<contact> lstContact = new list<contact>();
        for(integer i=0; i<20;i++)
        {
            Contact testcontact = Utils_TestMethods.createContact(testAccount.Id,'test contact');
            lstContact.add(testContact);
        }
        insert lstContact;
        list<Case> lstCase = new list<case>();
        for(integer i=0; i<20;i++)
        {
            Case testCase = Utils_TestMethods.createCase(testAccount.Id,lstcontact[0].Id,'Open');
            lstCase.add(testCase);
        }
        insert lstCase;
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(testAccount );
        VFC_ShowAccountRelatedDetails myPageCon1 = new VFC_ShowAccountRelatedDetails(sdtCon1);
       
        
    }
    
        
}