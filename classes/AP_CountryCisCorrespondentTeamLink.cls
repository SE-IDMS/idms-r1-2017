/*
    Author          : Mohammad Naved 
    Date Created    : 30/05/2016
    Description     : Class to set country value in CIS Correspondents Team object.
*/    
public class AP_CountryCisCorrespondentTeamLink {

    public static void checkDuplicateCountry(list<CountryCISCorrespondentsTeamLink__c>lstCountryCISCorrespondentsTeamLink){
      set<String>setCountry= new set<String>();
      set<String>setCISTeam = new set<String>();
      list<CountryCISCorrespondentsTeamLink__c>lstTemp = new list<CountryCISCorrespondentsTeamLink__c>();
      for(CountryCISCorrespondentsTeamLink__c cis :  lstCountryCISCorrespondentsTeamLink){
          setCountry.add(cis.Country__c);
          setCISTeam.add(cis.CISCorrespondentsTeam__c);
          
          
      
      }
      for(CountryCISCorrespondentsTeamLink__c cis :[select id,
                                                    Country__r.Name,
                                                    CISCorrespondentsTeam__c,
                                                    CISCorrespondentsTeam__r.CIS_ScopeOfCountries__c
                                                    from  CountryCISCorrespondentsTeamLink__c  
                                                    where  Country__c IN :setCountry AND CISCorrespondentsTeam__c IN:setCISTeam]){
		system.debug('cis'+cis);                                                          
        lstTemp.add(cis);
      }
      if(lstTemp.size()>0){
          lstCountryCISCorrespondentsTeamLink[0].addError('Country Already Exists');
      
      }
 }

//Method call from trigger
public static void changeContryValue(list<CountryCISCorrespondentsTeamLink__c>lstCountryCISCorrespondentsTeamLink){
    map<id, String> mapCISCorrespondentsTeamId_Country  = new map<id, String>();
    set<Id>setCisCorrTeam= new set<Id>();
    for(CountryCISCorrespondentsTeamLink__c cis :  lstCountryCISCorrespondentsTeamLink){
        setCisCorrTeam.add(cis.CISCorrespondentsTeam__c);
    }
    for( CountryCISCorrespondentsTeamLink__c cis :[select id,
                                                    Country__r.Name,
                                                    CISCorrespondentsTeam__c,
                                                    CISCorrespondentsTeam__r.CIS_ScopeOfCountries__c
                                                    from  CountryCISCorrespondentsTeamLink__c  
                                                    where  CISCorrespondentsTeam__c =:setCisCorrTeam
                                                   
                                                  ] ){
                                                  
    
       if(mapCISCorrespondentsTeamId_Country.containsKey(cis.CISCorrespondentsTeam__c) ){
                
                string strCountry = mapCISCorrespondentsTeamId_Country.get(cis.CISCorrespondentsTeam__c);
               // if(!strCountry.contains( cis.Country__r.Name)){
                    strCountry = strCountry +';' +cis.Country__r.Name;
                    mapCISCorrespondentsTeamId_Country.put(cis.CISCorrespondentsTeam__c,strCountry);
               // }
            
            
        
       } else {
           
                mapCISCorrespondentsTeamId_Country.put(cis.CISCorrespondentsTeam__c,cis.Country__r.Name);
           
           
       }
    }
    list<CIS_CorrespondentsTeam__c>lstCIS_CorrespondentsTeam = new  list<CIS_CorrespondentsTeam__c>();
    for(Id objId:setCisCorrTeam){
        if(mapCISCorrespondentsTeamId_Country.containsKey(ObjId)){
        CIS_CorrespondentsTeam__c obj = new CIS_CorrespondentsTeam__c(id =objId,
                                                                     CIS_ScopeOfCountries__c= mapCISCorrespondentsTeamId_Country.get(objId));    
        lstCIS_CorrespondentsTeam.add(obj);
        }
        else{
            CIS_CorrespondentsTeam__c obj = new CIS_CorrespondentsTeam__c(id =objId,
                                                                     CIS_ScopeOfCountries__c= mapCISCorrespondentsTeamId_Country.get(objId));    
            lstCIS_CorrespondentsTeam.add(obj);
        
        }
    
    }
    if(!lstCIS_CorrespondentsTeam.isEmpty()) {
        update lstCIS_CorrespondentsTeam;
    }
    
    
}

}