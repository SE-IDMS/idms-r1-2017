//*********************************************************************************
// Class Name       : VFC1001_AddCMToProblem_TEST
// Purpose          : Test Class for VFC1001_AddCommercialReferencesToProblem
// Created by       : Global Delivery Team
// Date created     : 26th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


@isTest
private class VFC1001_AddCMToProblem_TEST {
    
    static testMethod void TestPblm() 
    {      
        BusinessRiskEscalationEntity__c BRE1=Utils_TestMethods.createBRE('sample entity','sample subentity','sample location','Plant');
        insert BRE1;
        
        Problem__c problem1 = Utils_TestMethods.createProblem(BRE1.id);
        insert problem1;
        
        string [] searchfilterList= new string[]{};
            
        for(Integer token=0;token<17;token++)
        {
            searchfilterList.add('a');          
        }
        
        VCC08_GMRSearch ProductforPblm1 = new VCC08_GMRSearch();
        ProductforPblm1.ActionType= 'multiple'; 
        VCC05_DisplaySearchFields DisplaySearchFieldsController = new VCC05_DisplaySearchFields(); 
            
        DisplaySearchFieldsController.pickListType = 'PM0_GMR';
        DisplaySearchFieldsController.SearchFilter = new String[4];
        DisplaySearchFieldsController.SearchFilter[0] = '00';
        DisplaySearchFieldsController.SearchFilter[1] = Label.CL00355;    
        DisplaySearchFieldsController.searchText = 'search\\stest';
        DisplaySearchFieldsController.key = 'searchComponent';
        DisplaySearchFieldsController.PageController = ProductforPblm1 ;
        
        DisplaySearchFieldsController.init();
        ProductforPblm1.searchController = DisplaySearchFieldsController;   
        DisplaySearchFieldsController.searchText = 'search\\stest';

        ProductforPblm1.search();
        ProductforPblm1.clear();
            
        ProductforPblm1.ObjID = problem1.ID;
        ProductforPblm1.Cleared = false;
        
        VCC06_DisplaySearchResults  controller =new VCC06_DisplaySearchResults();               
        VFC_ControllerBase basePageController = new VFC_ControllerBase();
        VCC_ControllerBase basecomponentcontroller = new VCC_ControllerBase();

        basePageController.getThis();
        basePageController.getComponentControllerMap();        
        basePageController.setComponentControllerMap('resultsComponent',controller );             
        basecomponentcontroller.pageController = basePageController ;
        basecomponentcontroller.key = 'resultsComponent';
        basecomponentcontroller.pageController = ProductforPblm1 ;
        ProductforPblm1.resultsController=controller;
        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        controller.columnList = dummydata.getColumns();
        
        Schema.sObjectType objType;
        Schema.DescribeSObjectResult objDescribe;
        Map<String, Schema.SObjectField> objFields;
        Map<String, Schema.DisplayType> ObjFieldTypes = new Map<String, Schema.DisplayType>();
        Schema.SObjectField field;
        Schema.DescribeFieldResult fieldDescribe;
        Schema.DisplayType fieldType;
        String[] dataType;
        List<String> Filters;    
        for(integer i=0;i < 4;i++){
        controller.columnList.add(new SelectOption('Field8__c','commercial Reference'));            
        }

        
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);
        List<DataTemplate__c> dataTemplates = (List<DataTemplate__c>)tempresult.recordList;
        Set<Object> exisRec = new Set<Object>();
        for(DataTemplate__c dt: dataTemplates )
        {
            exisRec.add(dt.Field1__c);
        }
        controller.existingRecords = exisRec;

            objType= DataTemplate__c.getSObjectType();
            objDescribe =objType.getdescribe();
            objFields =objDescribe.fields.getMap();
            for(SelectOption selectValue:controller.columnList)
            {
                field = objFields.get(selectValue.getvalue());
                fieldDescribe = field.getdescribe();
                fieldType = fieldDescribe.getType(); 
                objFieldTypes.put(selectValue.getvalue(),FieldType);          
            }
        
        PageReference pageRef= Page.VFP25_AddUpdateProductforCase;
        pageRef.getParameters().put('sortColumn','4');

        controller.setpageNumber(2);
        controller.recordsPerPage = 4;
        controller.getComponentController();

        controller.displayRecordList=tempresult.recordList; 

        controller.pageNumber = 5; 
        controller.numberOfPages = 2;
        controller.getpageNumber();
       
        controller.pageNumber = 0; 
        controller.getpageNumber();


        VCC06_DisplaySearchResults.DataDisplayWrapper[] wrapperclass = new VCC06_DisplaySearchResults.DataDisplayWrapper[]{};
        VCC06_DisplaySearchResults.DataDisplayWrapper wrapperclass1 = new VCC06_DisplaySearchResults.DataDisplayWrapper(tempresult.recordList[1],1,1,controller.columnList,basePageController,objFields,new List<String>(),controller.existingRecords,objFieldTypes);
        wrapperclass = controller.getDisplaySearchResults();
        
        wrapperclass1.selected=true;      
        
        Test.setCurrentPageReference(pageRef);
        system.debug('current pagereff'+system.currentPageReference().getparameters());
        controller.getrecordsDisplay();
        controller.first();
        controller.last();
        controller.previous();
        controller.next();
        controller.sortColumn();
        controller.getpageNumber();
        controller.pageNumberRenderer();       
        
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();  
        String jsonSelectFamily = Utils_WSDummyTestData.createDummyJSONString();
        ProductforPblm1.resultsController=controller;
        
        ApexPages.currentPage().getParameters().put(Label.CL10053,problem1.ID);
        ApexPages.currentPage().getParameters().put(Label.CL10054,problem1.Name);
        ApexPages.currentPage().getParameters().put('jsonSelectString','['+jsonSelectString+']');
        ApexPages.currentPage().getParameters().put('jsonSelectFamily','['+jsonSelectFamily+']');
        ApexPages.StandardController PblmStandardController = new ApexPages.StandardController(problem1);        

        VFC1001_AddCommercialReferencesToProblem PblmController = new VFC1001_AddCommercialReferencesToProblem(PblmStandardController);
        PblmController.jsonSelectString='['+jsonSelectString+']';
        PblmController.jsonSelectFamily='['+jsonSelectFamily+']';
        
        VFC1001_AddCommercialReferencesToProblem.remoteSearch('Test Commercial Reference','12',true,false,false,false,true);
        VFC1001_AddCommercialReferencesToProblem.remoteSearch('Test Commercial Reference','1234',true,false,false,false,true);
        VFC1001_AddCommercialReferencesToProblem.remoteSearch('Test Commercial Reference','123456',true,false,false,false,true);
        VFC1001_AddCommercialReferencesToProblem.remoteSearch('Test Commercial Reference','12345678',true,false,false,false,true);
      
        VFC1001_AddCommercialReferencesToProblem.getOthers('ANYBUSINESS');
        PblmController.performCheckbox();
      
      
        pageRef = Page.VFP1001_AddCommercialReferencesToProblem;
      
        pageRef.getParameters().put(Label.CL10053,problem1.ID);
        pageRef.getParameters().put(Label.CL10054,problem1.Name);
        ApexPages.currentPage().getParameters().put('jsonSelectString','['+jsonSelectString+']');
        ApexPages.currentPage().getParameters().put('jsonSelectFamily','['+jsonSelectFamily+']');
        
        Test.setCurrentPage(pageRef); 
        VFC1001_AddCommercialReferencesToProblem GMRSearchPage = new VFC1001_AddCommercialReferencesToProblem(PblmStandardController);
        GMRSearchPage.jsonSelectString='['+jsonSelectString+']';
        GMRSearchPage.jsonSelectFamily='['+jsonSelectFamily+']';
        GMRSearchPage.performCheckbox();
        GMRSearchPage.pageCancelFunction();
    }
}