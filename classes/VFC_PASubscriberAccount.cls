public with sharing class VFC_PASubscriberAccount { 
    
     //Subclass : Wrapper Class 
    public class Accountwrap {
        //Static Variables 
        public boolean blnCheck {get;set;}
        public User objUser {get;set;}
        public string strAccountId;
        public String strSubscribeId;
        public String strError;
        //Wrapper  Class Controller
        Public Accountwrap(Boolean Check,User obj,string strId,string SubId) {
            blnCheck =Check;
            objUser = obj;
            strAccountId = strId;
            strSubscribeId= SubId;
        }
        
    }
       
    //Method to bring the list of Account and Serialize Wrapper Object as JSON
    public  static String getlstAccount() {
        Id objectId;
        String strAccountId;
        Set<id>UserId= new set<ID>();
        list<Accountwrap>lstCheckedUser= new list<Accountwrap>();
        objectId =ApexPages.currentPage().getParameters().get('id');
        if(objectId!= null){
            Schema.SObjectType token = objectId.getSObjectType();
            Schema.DescribeSObjectResult dr = token.getDescribe();
            if(dr.getName() == 'Account') {
                strAccountId =objectId;
                for(Subscriber__c obj :[select id,Email__c,Name,User__r.FirstName,User__c,User__r.LastName,User__r.Email,User__r.contact.email from Subscriber__c where Account__c=:objectId ]){
                    User objU = new User(id = obj.User__c, FirstName = obj.User__r.FirstName,LastName=obj.User__r.LastName,Email =obj.Email__c);
                    Accountwrap  cu = new Accountwrap (true,objU,strAccountId,obj.id);
                    lstCheckedUser.add(cu);
                    UserId.add(obj.User__c);
                
                }
            
            }
        }
        
    
        system.debug('lstCheckedUser'+lstCheckedUser);
        return JSON.serialize(lstCheckedUser);
     }
     @RemoteAction
     public static String getClassInstances(string strJson, String accountid){
       system.debug(strJson);
       List<Subscriber__c>lstsubscriber = new List<Subscriber__c>();
       if(strJson !='[]' && strJson != null) {  
           List<Object> m = (List<Object>)JSON.deserializeUntyped(strJson);
            //system.debug(m.get(strAccountId));
            string strAccountId;
            
            lstsubscriber = new List<Subscriber__c>();
            for (Object o : m) {
                Map<String, Object> p = (Map<String, Object>) o;
                
                strAccountId =(String)p.get('strAccountId');
                Id subscribedId =(String)p.get('strSubscribeId');
                Map<String, Object> oy =(Map<String, Object>)p.get('objUser');
                string strId= (string)oy.get('Id');
                string strEMail= (string)oy.get('Email');
                
                Subscriber__c sb = new Subscriber__c();
                sb.Account__c= strAccountId;
                sb.User__c=strId;
                sb.Email__c=strEMail;
                if(subscribedId != null){
                   sb.id= subscribedId;
                }
                lstsubscriber.add(sb);
                


            }
            system.debug('lstsubscriber'+lstsubscriber);
            if(!lstsubscriber.IsEmpty()){
                upsert lstsubscriber;
            }
            system.debug('lstsubscriber:::'+lstsubscriber);
            List<Subscriber__c>lstsubscriberDelete = new List<Subscriber__c>([select id from Subscriber__c where Id NOT IN :lstsubscriber and Account__c=:strAccountId]);
            if(!lstsubscriberDelete .isEmpty()){
                 system.debug('Delete lstsubscriber:::'+lstsubscriber);
                delete lstsubscriberDelete ;
            }
        } else
        {
            lstsubscriber = new List<Subscriber__c>([select id from Subscriber__c where Account__c =:accountid]);
            if(!lstsubscriber.isEmpty()){
                delete lstsubscriber;
            }
            
        }
        return accountid;
        
     }
     @RemoteAction
     public static list<Accountwrap> subscriberResult(String strFilter, String strCaseId,String strJason){
         list<Accountwrap>lstCheckedUser = new list<Accountwrap>();
          Set<Id> UserIdSet = new Set<Id>();
         system.debug('strFilter'+strFilter);
         strFilter='%'+strFilter+'%';
         system.debug('strCaseId'+strCaseId);
         List<Object> m = (List<Object>)JSON.deserializeUntyped(strJason);
         Set<String>setUserId =new Set<String>();
         string strAccountId;
         for (Object o : m) {
            Map<String, Object> p = (Map<String, Object>) o;
                     system.debug('p++++'+p);
           
            Map<String, Object> oy =(Map<String, Object>)p.get('objUser');
            string strId= (string)oy.get('Id');
            setUserId.add(strId);
         }
         system.debug('strCaseId'+strCaseId);
         /*for(AccountShare u :[SELECT UserOrGroupId FROM AccountShare WHERE AccountId =: strCaseId AND (RowCause = 'Owner' OR RowCause = 'Manual')]){
            UserIdSet.add(u.UserOrGroupId);
         }*/
         UserIdSet = PA_UtilityClass.getAccountShareUserId(strCaseId);
         system.debug('UserIdSet'+UserIdSet);
         for(User objUser :[select id ,FirstName,LastName,Email,Contact.Email from User where Name Like :strFilter AND isActive = TRUE AND (User.Profile.UserLicense.Name ='Chatter free' OR User.Profile.UserLicense.Name = 'salesforce' OR ID IN :UserIdSet)]){
             User objU = new User(id = objUser.id, FirstName = objUser.FirstName,LastName=objUser.LastName);
              if(String.isBlank(objUser.ContactId)) {
                    objU.Email =objUser.Email;
                } else
                {
                    objU.Email =objUser.contact.email;
                }
            Accountwrap  cu = new Accountwrap (false,objU,strCaseId,null);
            lstCheckedUser.add(cu);
         }
         system.debug('lstCheckedUser::'+lstCheckedUser);
         if(lstCheckedUser.size() >0){
             
             
            if(lstCheckedUser.size()>=1){
                if(lstCheckedUser.size()==1 && setUserId.contains(lstCheckedUser[0].objUser.id)){
                     lstCheckedUser[0].strError='User already subscribe this case'; 
                     return lstCheckedUser;
                 }
                 for (Integer i = (lstCheckedUser.size()-1) ; i>= 0 ; i--){
                     if(setUserId.contains(lstCheckedUser[i].objUser.id)){
                            lstCheckedUser.remove(i);
                     }
                 }
                 system.debug('lstCheckedUser'+lstCheckedUser);
                 return lstCheckedUser;
             }
         
         }
        Accountwrap  cu = new Accountwrap (false,null,null,null); 
        cu.strError='No record Found'; 
        lstCheckedUser.add(cu);
        return lstCheckedUser;
     }
     public class ObjUser {
        
            public String Id {get;set;} 
            public String FirstName {get;set;} 
            public String LastName {get;set;} 
            public String Email {get;set;}
        }
    
}