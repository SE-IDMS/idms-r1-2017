@IsTest
public class AP_PRMEmailHandler_Test{
     public static testMethod void emailHandler() {
    // Create a new email and envelope object
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        // Set up your data if you need to
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True,
                         BypassWF__c = True);
        System.runAs(u){
        
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince;
        
        ClassificationLevelCatalog__c classLevel1 = new ClassificationLevelCatalog__c();
            classLevel1.Name  = 'EU';
            classLevel1.ClassificationLevelName__c = 'End User or Consumer';
            INSERT classLevel1;

            ClassificationLevelCatalog__c classLevel2 = new ClassificationLevelCatalog__c();
            classLevel2.Name  = 'EUB';
            classLevel2.ClassificationLevelName__c = 'Large Corporation';
            classLevel2.ParentClassificationLevel__c = classLevel1.Id;
            INSERT classLevel2;

            CountryChannels__c cntryChannels = new CountryChannels__c();
            cntryChannels.Channel__c     = classLevel1.Id;
            cntryChannels.SubChannel__c  = classLevel2.Id;
            cntryChannels.Active__c      = True;
            cntryChannels.Country__c     = testCountry.Id;
            cntryChannels.AllowPrimaryToManage__c = True;
            INSERT cntryChannels;
            
        Account newAcc = new Account();
            newAcc.RecordTypeId = System.Label.CLAPR15PRM025;   
            newAcc.Name  =  'NewAccount';
            newAcc.PRMCompanyName__c = 'NewCompanyName';
            newAcc.PRMStreet__c       = 'Elnath Street ';
            newAcc.PRMAdditionalAddress__c = 'Marathalli';
            newAcc.PRMCity__c              = 'Bangalore';
            newAcc.PRMStateProvince__c     = testStateProvince.Id;
            //newAcc.PRMStateProvince__r.Name
            //newAcc.PRMCountry__r.Name
            newAcc.PRMCountry__c         = testCountry.Id;
            newAcc.PRMCompanyPhone__c    = '987654321';
            newAcc.PRMWebsite__c         = 'Https://schneider-electric.com';
            newAcc.PRMZipCode__c         = '560103';
            newAcc.PRMCorporateHeadquarters__c = true;
            newAcc.PRMUIMSID__c = '123456';
            newAcc.PRMBusinessType__c = 'EU';
            newAcc.PRMAreaOfFocus__c  = 'EUB';
            newAcc.PRMAccount__c = True;
        INSERT newAcc;
        
        
        Contact newCon = new Contact();
        newCon.LastName = 'lastNameContact';
        newCon.PRMUIMSID__c = '123456';
        newCon.AccountId = newAcc.Id;
        newCon.PRMContact__c = True;
        newCon.PRMEmail__c = 'testmail@yopmail.com';
        newCon.PRMPrimaryContact__c = True;
        newCon.AccountId = newAcc.Id;
        INSERT newCon;
        
        // Create the email body
        email.fromAddress ='test@test.com';
        String contactEmail = 'jsmith@salesforce.com';
        //email.ccAddresses = new String[] {'Jon Smith <' + contactEmail + '>'};
        email.subject = 'ReceivedId: '+newCon.Id;
        
        AP_PRMEmailHandler EmailHandler = new AP_PRMEmailHandler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = EmailHandler.handleInboundEmail(email, env);
        Test.stopTest();
        
        //System.assertEquals (result.success, True);
        
       }
       }
}