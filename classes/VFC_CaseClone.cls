public class VFC_CaseClone{
    public Id CaseId;
    public Case caseRecord;
    public string strRelatedCaseNumber='';
    public list<Case> lstCaseClonefields=new list<Case>();
    
    // If the case is displayed in a service cloud console, the custom clone button sends the parameter isdtp=nv in the VFP_CaseClone page URL
    // the URL parameter isdtp=nv allows to hide the salesforce header and sidebar
    // The isdtp URL parameter will be stored in the isdtp class attribute
    public String isdtp='';
    
    public VFC_CaseClone(ApexPages.StandardController controller) {
        caseRecord=(Case)controller.getRecord();
        CaseId=caseRecord.Id;
        if(CaseId==null && System.currentPageReference().getParameters().containskey('CaseId'))
            CaseId=Apexpages.currentPage().getParameters().get('CaseId');
        if(System.currentPageReference().getParameters().containskey('RelatedCaseNumber'))
            strRelatedCaseNumber=Apexpages.currentPage().getParameters().get('RelatedCaseNumber');
        lstCaseClonefields=[select CaseNumber,Subject,SupportCategory__c,Symptom__c,OtherSymptom__c,AccountId, 
            ContactId,Origin,CommercialReference__c,CommercialReference_lk__c,CommercialReference_lk__r.Name, SVMXC__Component__c, SVMXC__Component__r.Name,ProductDescription__c,Family_lk__c,Family_lk__r.Name,
            CaseSymptom__c,CaseSubSymptom__c,CaseOtherSymptom__c from Case where id=:CaseId];
        
        // Retrieve the isdtp URL parameter if exists and store it
        if (Apexpages.currentPage().getParameters().containskey('isdtp'))
            isdtp=Apexpages.currentPage().getParameters().get('isdtp');
    }
      
    public PageReference CaseCloneLink(){
        PageReference pageRef = new PageReference('/500/e?');
        if(lstCaseClonefields.size()>0){
            if(lstCaseClonefields[0].Subject!=null && lstCaseClonefields[0].Subject!='')
                pageRef.getParameters().put(Label.CLOCT14CCC25,lstCaseClonefields[0].Subject);
            if(lstCaseClonefields[0].SupportCategory__c!=null && lstCaseClonefields[0].SupportCategory__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC26,lstCaseClonefields[0].SupportCategory__c);
            if(lstCaseClonefields[0].Symptom__c!=null && lstCaseClonefields[0].Symptom__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC27,lstCaseClonefields[0].Symptom__c);
            if(lstCaseClonefields[0].OtherSymptom__c!=null && lstCaseClonefields[0].OtherSymptom__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC28,lstCaseClonefields[0].OtherSymptom__c);
            if(lstCaseClonefields[0].AccountId!=null)
                pageRef.getParameters().put(Label.CLOCT14CCC29,lstCaseClonefields[0].AccountId); 
            if(lstCaseClonefields[0].ContactId!=null)
                pageRef.getParameters().put(Label.CLOCT14CCC30,lstCaseClonefields[0].ContactId);
            if(lstCaseClonefields[0].Origin!=null && lstCaseClonefields[0].Origin!='')
                pageRef.getParameters().put(Label.CLOCT14CCC31,lstCaseClonefields[0].Origin);
            if(lstCaseClonefields[0].CommercialReference_lk__c!=null){
                pageRef.getParameters().put(Label.CLOCT14CCC41,lstCaseClonefields[0].CommercialReference_lk__c);
                pageRef.getParameters().put(Label.CLOCT14CCC32,lstCaseClonefields[0].CommercialReference_lk__r.Name);
            }   
            if(lstCaseClonefields[0].SVMXC__Component__c!=null){
                pageRef.getParameters().put(Label.CLOCT14CCC42,lstCaseClonefields[0].SVMXC__Component__c);    
                pageRef.getParameters().put(Label.CLOCT14CCC33,lstCaseClonefields[0].SVMXC__Component__r.Name);    
            }
            if(lstCaseClonefields[0].ProductDescription__c!=null && lstCaseClonefields[0].ProductDescription__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC34,lstCaseClonefields[0].ProductDescription__c);
            if(lstCaseClonefields[0].Family_lk__c!=null){
                pageRef.getParameters().put(Label.CLOCT14CCC43,lstCaseClonefields[0].Family_lk__c); 
                pageRef.getParameters().put(Label.CLOCT14CCC35,lstCaseClonefields[0].Family_lk__r.Name); 
            }
            if(lstCaseClonefields[0].CaseSymptom__c!=null && lstCaseClonefields[0].CaseSymptom__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC36,lstCaseClonefields[0].CaseSymptom__c);
            if(lstCaseClonefields[0].CaseSubSymptom__c!=null && lstCaseClonefields[0].CaseSubSymptom__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC37,lstCaseClonefields[0].CaseSubSymptom__c);
            if(lstCaseClonefields[0].CaseOtherSymptom__c!=null && lstCaseClonefields[0].CaseOtherSymptom__c!='')
                pageRef.getParameters().put(Label.CLOCT14CCC38,lstCaseClonefields[0].CaseOtherSymptom__c);
            if(strRelatedCaseNumber!=null && strRelatedCaseNumber!='')
                pageRef.getParameters().put(Label.CLOCT14CCC39,strRelatedCaseNumber);
            pageRef.getParameters().put(Label.CLOCT14CCC40,'In Progress');
            pageRef.getParameters().put('retURL',CaseId);
            pageRef.getParameters().put('nooverride', '1');
            
            // Add the isdtp URL parameter if exists and to the clone page URL
            if (isdtp != null && isdtp.length()>0)
                pageRef.getParameters().put('isdtp',isdtp);
        }
        pageRef.setRedirect(true);
        return pageRef;
    }
}