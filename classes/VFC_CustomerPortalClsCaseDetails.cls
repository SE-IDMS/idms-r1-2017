public without sharing class VFC_CustomerPortalClsCaseDetails {

/*=== GLOBAL VARIABLES============*/
Id CaseId;
//Id CaseCommentId; 
    
Id usid;
Id contid;
Id accid;   

Contact userContact = new Contact();    
Account userAcc = new Account();
User userr = null;

String ContactPhone = null;
String accountName = null;

Case caseDetail;
List<CaseComment> caseComments = null;
CaseComment caseCommentDetail;
List<Attachment> attachments; 

public boolean isAttachListEmpty = true; 
public Boolean getIsAttachListEmpty(){return isAttachListEmpty;}
public void setIsAttachListEmpty(Boolean AttachListEmpty){this.isAttachListEmpty = AttachListEmpty;}

String productType = null;
String operatingSystem = null;
String SoftwareVersion = null;
String answerToCustomer = null;

public Boolean isCaseClosedd = true;
public Boolean getIsCaseClosedd(){return isCaseClosedd;}
public void setIsCaseClosedd(Boolean isCaseClosedd){this.isCaseClosedd = isCaseClosedd;}


/*=== GETTER AND SETTER METHODS============*/    
public Id getCaseId(){return CaseId;}

public boolean getIsAuthenticated ()
{
  return (IsAuthenticated());
}

public boolean IsAuthenticated ()
{
  return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
}

public void setCaseId(id caseId){this.CaseId = caseId;}
    
public void setCaseDetail(case c){this.CaseDetail = c;}    
public void setCaseComments(List<caseComment> c){this.caseComments = c;}
public void setattachments(LIST<attachment> a){this.attachments = a;}    
    
public Id getUsid(){return this.usid;}
public void setUsid(Id id){this.usid = id;}    
    
public Id getContId(){return contId;}    
public void setContid(Id id){this.usid = id;}    

public Id getAccId(){return AccId;}
public void setAccid(Id id){this.Accid = id;} 

public Contact getUserContact() {return userContact;}

public Account getUserAcc() {return UserAcc;}

public User getUserr() {return userr;}

public String getContactPhone () {return ContactPhone ;}
public void setContactPhone (String Phone) {this.ContactPhone = Phone ;}

public String getAccountName () {return accountName;}
public void setAccountName (String accountName) {this.accountName = accountName  ;}

public String getProductType(){return ProductType;}
public void setProductType(String ProductTypee){this.ProductType = ProductTypee;}

public String getoperatingSystem (){return operatingSystem ;}
public void setoperatingSystem (String swv){this.operatingSystem  = swv;}

public String getSoftwareVersion(){return SoftwareVersion;}
public void setSoftwareVersion(String swv){this.SoftwareVersion = swv;}

public String getAnswerToCustomer(){return AnswerToCustomer;}
public void setAnswerToCustomer(String swv){this.AnswerToCustomer = swv;}
    
/*=========THE CONSTRUCTOR============*/

public VFC_CustomerPortalClsCaseDetails(){
    
        CaseId = ApexPages.currentPage().getParameters().get('Id');
        
        if (CaseId != null){
            caseDetail = [SELECT CaseNumber, Subject, family__c, Product_Type__c, Operating_System__c, SoftwareVersion__c, Status, SupportCategory__c, CustomerRequest__c, AnswerToCustomer__c, IsClosed, ActionNeededFrom__c  FROM Case WHERE  Id =: CaseId];  
            
            caseComments = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
            
            attachments = [SELECT Id, CreatedById, Name, CreatedDate, BodyLength, ContentType, Description, IsPrivate, LastModifiedDate FROM Attachment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
            }
       
       
            if (caseDetail != null) {
                if (caseDetail.status.equalsIgnoreCase('Closed')){setIsCaseClosedd(false);}
                else setIsCaseClosedd(true);}
             
             if (Attachments != null){setIsAttachListEmpty(false);}
             
     
             usid = UserInfo.getUserId();
             if ( usid != null)
                {
                  contid = [Select ContactId from User where Id =: usid LIMIT 1][0].ContactId;
                  userr = [Select FirstName, LastName, name, Email from User where Id =: usid LIMIT 1][0];
                }
     
            if ( contid != null)
                {
                  accid = [Select AccountId from Contact where Id =: contid].AccountId;
                  userContact = [Select WorkPhone__c from Contact where Id =: contId][0];
                  userAcc = [Select Name from Account where Id =: Accid][0];
                  ContactPhone = userContact.WorkPhone__c;
                  accountName = userAcc.Name;
                }
     }   


//==========Return the parameter to the page=============
    public Case getCaseDetail() 
    {    
      if (caseDetail != null) 
      {
        //setProductType(caseDetail.family__c);
        setProductType(caseDetail.Product_Type__c);
        setOperatingSystem(caseDetail.Operating_System__c);
        setSoftwareVersion(caseDetail.SoftwareVersion__c);
        setAnswerToCustomer(caseDetail.AnswerToCustomer__c);
        
        if      (caseDetail.status.equalsIgnoreCase('New'))       {caseDetail.status = 'New';}
        else if (caseDetail.status.equalsIgnoreCase('Cancelled')) {caseDetail.status = 'Closed';}
        else if (caseDetail.status.equalsIgnoreCase('Closed'))    {caseDetail.status = 'Closed';}
        else if (caseDetail.ActionNeededFrom__c != null && caseDetail.ActionNeededFrom__c.equalsIgnoreCase('Customer')) {caseDetail.status = 'Waiting for Customer';} 
        else caseDetail.status = 'In Progress';     
      }
      
      return caseDetail; 
    }

/*=======Return the comments List. ===================*/
    public List<CaseComment> getCaseComments() {    
         List<CaseComment> lcomm = new List<CaseComment>();
        
        if (CaseComments != null){
            for (CaseComment lcc : CaseComments){
                    if (lcc.IsPublished) lcomm.add(lcc);
            }
            return lcomm;
        }
        else return CaseComments; 
    }        
                  
    public pageReference getCaseDetailView()
    {
      if (IsAuthenticated())
      {
      	getCaseDetail(); 
      }
      
      return null;
    
    }   
    
/*======= Return the Attachments List ===================*/
    public List<Attachment> getAttachments() {    
        List<Attachment> lattach = new List<Attachment>();
        
        if (Attachments != null){
            for (Attachment att : Attachments){
                    if (!(att.IsPrivate)) lattach.add(att);
            }
            
            return lattach;
        }
        else return Attachments;   
    }        
       
     
}