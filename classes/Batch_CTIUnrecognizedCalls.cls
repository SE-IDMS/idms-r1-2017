global class Batch_CTIUnrecognizedCalls implements Database.batchable<Sobject>{
    
    global Database.QueryLocator start (Database.BatchableContext bc){
        String Query ;
        //String strMonth='-3';
        String  DateStr= system.now().adddays(integer.valueof(system.label.CLOCT15CCC16)).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\''); 
        system.debug('DateStr'+DateStr);
        String Completed = System.label.CLOCT15CCC17;
        String Inbound = System.label.CLOCT15CCC18;
        String NoMatch = System.Label.CLOCT15CCC19;
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC04);
        if(!Test.isrunningtest()){
            Query ='select Id,ANI_Is_Entered__c,ANI__c,CTI_Recognition__c,createdDate  from Task where ANI_Is_Entered__c=false and CTI_Recognition__c=: '+ NoMatch + ' and createdDate >='+DateStr+' and Status =:'+Completed+' and CallType=:'+inbound;
        }
        else{
            Query ='select Id,ANI_Is_Entered__c,ANI__c,CTI_Recognition__c,createdDate  from Task where ANI_Is_Entered__c=false ';
        }
        system.debug('queryresult'+Database.getQueryLocator(Query));    
        return  Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext Bc,List<Sobject> Scope){
    DateTime DateStr=dateTime.now().adddays(integer.valueof(system.label.CLOCT15CCC35));
    system.debug('DateTime DateStr'+DateStr);
            system.debug('list of tasks from query'+Scope);
            Map<String,List<Task>> aniTaskMap = new Map<String,List<Task>>();
            set<String> ANISet= new set<String>();
            set<Id> ANIIdSet = new Set<Id>();
            list<Task> tasklisttoupdate=new list<Task>(); 
            list<Task> listtoupdate=new list<Task>(); 
            for(sObject genralisedObj: Scope){
                sObject sObj = genralisedObj;
                Task taskObj = (Task)genralisedObj;
                System.debug('taskObj'+taskObj);
                if(!aniTaskMap.containsKey(taskObj.ANI__c)){
                    List<Task> newTaskList = new List<Task>();
                    newTaskList.add(taskObj);
                    aniTaskMap.put(taskObj.Id,newTaskList);
                 }
                else{
                    aniTaskMap.get(taskObj.Id).add(taskObj);
                    System.debug('aniTaskMap'+aniTaskMap);
                }
                
                ANISet.add(taskObj.ANI__c);
                ANIIdSet.add(taskObj.Id);
            }
            List<Account> lstAccount=[select id,name,Phone,PhoneInternational__c,Fax,lastmodifieddate,systemmodstamp  from account WHERE lastmodifieddate >=:DateStr and (Phone =:ANISet or Fax=:ANISet or PhoneInternational__c=:ANISet)];
            system.debug('MatchingANIAccountList'+lstAccount.size());
            If(lstAccount!=null){
                for(Account acc :lstAccount){
                    for(Id ANIId :ANIIdSet){
                            if(ANISet.contains(acc.phone)|| ANISet.contains(acc.Fax)|| ANISet.contains(acc.PhoneInternational__c)){
                              tasklisttoupdate=aniTaskMap.get(ANIId);
                                if(acc.phone!=null){
                                  ANISet.remove(acc.phone);
                                }
                                if(acc.Fax!=null){
                                  ANISet.remove(acc.Fax);
                                }
                                if(acc.PhoneInternational__c!=null){
                                  ANISet.remove(acc.PhoneInternational__c);
                                }
                            }
                    } 
                }
            }
            List<Contact> lstContact=[select id,WorkPhone__c,WorkPhoneExt__c,Aphone__c,FaxInternational__c,phone,MobilePhone,HomePhone,OtherPhone,lastmodifieddate,systemmodstamp  from Contact WHERE lastmodifieddate >=:DateStr and (WorkPhone__c =:ANISet or WorkPhoneExt__c=:ANISet or Aphone__c=:ANISet or FaxInternational__c=:ANISet or phone=:ANISet or MobilePhone=:ANISet or HomePhone=:ANISet or OtherPhone=:ANISet)];
            system.debug('MatchingANIAccountList'+lstContact.size());
            if(lstContact!=null){
               for(Contact Con :lstContact){
                    for(Id ANIId :ANIIdSet){
                        if(ANISet.contains(Con.phone)|| ANISet.contains(Con.WorkPhone__c)|| ANISet.contains(Con.WorkPhoneExt__c)|| ANISet.contains(Con.FaxInternational__c)|| ANISet.contains(Con.MobilePhone)|| ANISet.contains(Con.HomePhone)||ANISet.contains(Con.OtherPhone)||ANISet.contains(Con.Aphone__c)){
                          tasklisttoupdate=aniTaskMap.get(ANIId);
                         /* if(Con.WorkPhone__c!=null){
                               ANISet.remove(Con.WorkPhone__c);
                          }
                          if(Con.WorkPhoneExt__c!=null){
                               ANISet.remove(Con.WorkPhoneExt__c);
                          }
                          if(Con.Aphone__c!=null){
                               ANISet.remove(Con.Aphone__c);
                          }
                          if(Con.FaxInternational__c!=null){
                               ANISet.remove(Con.FaxInternational__c);
                          }
                          if(Con.phone!=null){
                               ANISet.remove(Con.phone);
                          }
                          if(Con.MobilePhone!=null){
                               ANISet.remove(Con.MobilePhone);
                          }
                          if(Con.HomePhone!=null){
                               ANISet.remove(Con.HomePhone);
                          }
                          if(Con.OtherPhone!=null){
                           ANISet.remove(Con.OtherPhone);
                          }*/
                        }
                    } 
                }
            }
            System.debug('ANISet'+ANISet);
            List<ANI__c> lstANI=[select id,ANINumber__c,lastmodifieddate from ANI__c WHERE lastmodifieddate >=:DateStr and ANINumber__c =:ANISet];
            system.debug('MatchingANIAccountList'+lstANI.size());
            if(lstANI!=NULL){
                for(ANI__C ANI :lstANI){
                    for(Id ANIId :ANIIdSet){
                       if(ANISet.contains(ANI.ANINumber__c)){
                        tasklisttoupdate=aniTaskMap.get(ANIId);
                        If(ANI.ANINumber__c!=null){
                          ANISet.remove(ANI.ANINumber__c);
                        }
                       }
                    }
                }
            }
            System.debug('tasklisttoupdate'+tasklisttoupdate);
            for(task tsk: tasklisttoupdate ){
              tsk.ANI_Is_Entered__c=true;
              tsk.CTCRecognitionExecutionTime__c=System.Now();
              listtoupdate.add(tsk);
            }
            system.debug('ANISMatchTaskfromAccount'+listtoupdate.size());    
            system.debug('ANISMatchTaskfromAccount'+listtoupdate);    
            if(listtoupdate!=null)
            update listtoupdate;
       
    }   
    global void finish(Database.BatchableContext bc){
    
    }
}