/*
Author: Sid (bFO Solution) & Polyspot team
description: this class is a test class for the controller class VFC_DK_CreateContact  , this class is a exact duplicate except for controller names
for the class VFC_CreateContact_TEST

*/
@isTest
private class VFC_DK_CreateContact_TEST {
 static testMethod void testVFC_DK_ContactCreate() {
        Country__c country = Utils_TestMethods.createCountry();
        insert country;

        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;

        Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId());
        insert acc;

        Contact   con= Utils_TestMethods.createContact(acc.id,'TestContact');
        con.Country__c=country.id;
        insert con;

        Case CaseObj= Utils_TestMethods.createCase(acc.Id,Con.Id,'Open');
        insert CaseObj;

        PageReference vfPage1 = Page.VFP_DK_CreateContact;
        Test.setCurrentPage(vfPage1);
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLDEC12ACC01);
        System.currentPageReference().getParameters().put('Case',CaseObj.id);
        //for person account
        VFC_DK_CreateContact  vfCtrl1 = new VFC_DK_CreateContact();
        vfCtrl1.account.FirstName='conFirstName';
        vfCtrl1.account.LastName='conLastName';
        vfCtrl1.account.LocalFirstName__pc='conLocalFirstName';
        vfCtrl1.account.LocalLastName__pc='conLocalLastName';
        vfCtrl1.account.Street__c='Street';
        vfCtrl1.account.StreetLocalLang__c='StreetLocalLang';
        vfCtrl1.account.Country__c = country.Id;
        vfCtrl1.account.ZipCode__c='ZipCode';
        vfCtrl1.account.City__c='City';
        vfCtrl1.account.LocalCity__c='LocalCity';
        vfCtrl1.account.StateProvince__c=stateProv.Id;
        vfCtrl1.account.POBox__c='POBox';
        vfCtrl1.account.PersonEmail='abc@gmail.com';
        vfCtrl1.account.WorkPhone__pc='123456';
        vfCtrl1.account.WorkPhoneExt__pc='123456';
        vfCtrl1.account.PersonMobilePhone='123456';
        vfCtrl1.account.RecordTypeId=System.Label.CLDEC12ACC01;
        vfCtrl1.contact.FirstName='conFirstName';
        vfCtrl1.contact.LastName='conLastName';
        vfCtrl1.contact.LocalFirstName__c='conLocalFirstName';
        vfCtrl1.contact.LocalLastName__c='conLocalLastName';
        vfCtrl1.contact.Street__c='Street';
        vfCtrl1.contact.StreetLocalLang__c='StreetLocalLang';
        vfCtrl1.contact.Country__c = country.Id;
        vfCtrl1.contact.ZipCode__c='ZipCode';
        vfCtrl1.contact.City__c='City';
        vfCtrl1.contact.LocalCity__c='LocalCity';
        vfCtrl1.contact.StateProv__c=stateProv.Id;
        vfCtrl1.contact.POBox__c='POBox';
        vfCtrl1.contact.email='abc@gmail.com';
        vfCtrl1.contact.WorkPhone__c='123456';
        vfCtrl1.contact.WorkPhoneExt__c='123456';
        vfCtrl1.contact.MobilePhone='123456';
        vfCtrl1.saveContact();


        //Scenario when Case needs to be created
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLDEC12ACC01);
        System.currentPageReference().getParameters().put('Case','');
        //for person account
        VFC_DK_CreateContact  vfCtrl3 = new VFC_DK_CreateContact();
        vfCtrl3.account.FirstName='conFirstName';
        vfCtrl3.account.LastName='conLastName';
        vfCtrl3.account.LocalFirstName__pc='conLocalFirstName';
        vfCtrl3.account.LocalLastName__pc='conLocalLastName';
        vfCtrl3.account.Street__c='Street';
        vfCtrl3.account.StreetLocalLang__c='StreetLocalLang';
        vfCtrl3.account.Country__c = country.Id;
        vfCtrl3.account.ZipCode__c='ZipCode';
        vfCtrl3.account.City__c='City';
        vfCtrl3.account.LocalCity__c='LocalCity';
        vfCtrl3.account.StateProvince__c=stateProv.Id;
        vfCtrl3.account.POBox__c='POBox';
        vfCtrl3.account.PersonEmail='abc@gmail.com';
        vfCtrl3.account.WorkPhone__pc='123456';
        vfCtrl3.account.WorkPhoneExt__pc='123456';
        vfCtrl3.account.PersonMobilePhone='123456';
        vfCtrl3.account.RecordTypeId=System.Label.CLDEC12ACC01;
        vfCtrl3.contact.FirstName='conFirstName';
        vfCtrl3.contact.LastName='conLastName';
        vfCtrl3.contact.LocalFirstName__c='conLocalFirstName';
        vfCtrl3.contact.LocalLastName__c='conLocalLastName';
        vfCtrl3.contact.Street__c='Street';
        vfCtrl3.contact.StreetLocalLang__c='StreetLocalLang';
        vfCtrl3.contact.Country__c = country.Id;
        vfCtrl3.contact.ZipCode__c='ZipCode';
        vfCtrl3.contact.City__c='City';
        vfCtrl3.contact.LocalCity__c='LocalCity';
        vfCtrl3.contact.StateProv__c=stateProv.Id;
        vfCtrl3.contact.POBox__c='POBox';
        vfCtrl3.contact.email='abc@gmail.com';
        vfCtrl3.contact.WorkPhone__c='123456';
        vfCtrl3.contact.WorkPhoneExt__c='123456';
        vfCtrl3.contact.MobilePhone='123456';
        vfCtrl3.saveContact();

        //for Business account
        PageReference vfPage2 = Page.VFP_DK_CreateContact;
        Test.setCurrentPage(vfPage2);
        ApexPages.currentPage().getParameters().put('recType',System.Label.CLOCT13ACC08);
        System.currentPageReference().getParameters().put('Case',CaseObj.id);
        VFC_DK_CreateContact  vfCtrl2 = new VFC_DK_CreateContact();
        vfCtrl2.account.Name = 'TEST VFC_ContactSearch';
        vfCtrl2.account.AccountLocalName__c = 'Account Local Name';
        vfCtrl2.account.Street__c = 'Street';
        vfCtrl2.account.ZipCode__c = 'zipcode';
        vfCtrl2.account.City__c = 'city';
        vfCtrl2.account.StateProvince__c = stateProv.Id;
        vfCtrl2.account.Country__c = country.Id;
        vfCtrl2.account.POBox__c = 'pobox';
        vfCtrl2.account.POBoxZip__c = 'poboxzip';
        vfCtrl2.account.StreetLocalLang__c = 'streetlocal';
        vfCtrl2.account.ToBeDeleted__c = false;
        vfCtrl2.account.CorporateHeadquarters__c = true;
        vfCtrl2.account.LeadingBusiness__c = 'BD';
        vfCtrl2.account.MarketSegment__c = 'BDZ';
        vfCtrl2.account.RecordTypeId=System.Label.CLOCT13ACC08;

        vfCtrl2.ischecked=True;
        vfCtrl2.saveContact();

        vfCtrl2.ischecked=False;
        vfCtrl2.saveContact();
        vfCtrl2.cancelContact();

        vfCtrl2.contact.FirstName='conFirstName';
        vfCtrl2.contact.LastName='conLastName';
        vfCtrl2.contact.LocalFirstName__c='conLocalFirstName';
        vfCtrl2.contact.LocalLastName__c='conLocalLastName';
        vfCtrl2.contact.Street__c='Street';
        vfCtrl2.contact.StreetLocalLang__c='StreetLocalLang';
        vfCtrl2.contact.Country__c = country.Id;
        vfCtrl2.contact.ZipCode__c='ZipCode';
        vfCtrl2.contact.City__c='City';
        vfCtrl2.contact.LocalCity__c='LocalCity';
        vfCtrl2.contact.StateProv__c=stateProv.Id;
        vfCtrl2.contact.POBox__c='POBox';
        vfCtrl2.contact.email='abc@gmail.com';
        vfCtrl2.contact.WorkPhone__c='123456';
        vfCtrl2.contact.WorkPhoneExt__c='123456';
        vfCtrl2.contact.MobilePhone='123456';
        vfCtrl2.saveContact();

 }

}