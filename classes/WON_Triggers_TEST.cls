/*
    Author          : Shruti Karn
    Date Created    : 28/11/2012
    Description     : Test class for Work Order Notification triggers
*/
@isTest(SeeAllData=true)
private class WON_Triggers_TEST {

    static testMethod void testWON(){
     Account testAccount = Utils_TestMethods.createAccount();
     insert testAccount;
     
     Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
     insert testContact;
     
     Case testCase = Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'Open');
     insert testCase;
     
     Country__c testCountry = Utils_TestMethods.createCountry();
     insert testCountry;
     
     CustomerLocation__c testCustLocation = Utils_TestMethods.createCustomerLocation(testAccount.Id,testCountry.Id);
     insert testCustLocation;
     
     CSE_ExternalReferences__c extRef = Utils_TestMethods.createCaseExternalRef(testCase.ID);
     extRef.Type__c = 'Contract';
     insert extRef;
     
     CSE_ExternalReferences__c extRef2 = Utils_TestMethods.createCaseExternalRef(testCase.ID);
     extRef2.Type__c = 'Warranty';
     insert extRef2;
     
     WorkOrderNotification__c testWON = Utils_TestMethods.createWorkOrderNotification(testCase.Id, testCustLocation.Id);
     insert testWON;
     
     User testUser = Utils_TestMethods.createStandardUser('WON');
     insert testUser;
     
     //testCase.OwnerID = testUser.ID;
     //update testCase;
     update testWON;
     
     
    }
}