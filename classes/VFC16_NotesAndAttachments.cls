/*
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 28/12/2010
    Description     : Controller for C04_NotesAndAttachments Component.This class dynamically retrieves Notes & Attachment for the corresponding RITE object.
*/
public with sharing class VFC16_NotesAndAttachments 
{
    public string objectName{get;set;}
    public string objectId{get;set;}
    public List<NoteAndAttachment> notesAndAttachmentslst{get;set;}
    private List<Sobject> queryResult;
    private RIT_ProjectInformation__c projInfo = new RIT_ProjectInformation__c();
    private RIT_ProjectManagement__c projMgnt = new RIT_ProjectManagement__c();
    private RIT_FinancialEnv__c finEnv = new RIT_FinancialEnv__c();
    private RIT_Budget__c budget = new RIT_Budget__c();
    private RIT_CommCompEnv__c commAndComp= new RIT_CommCompEnv__c();
    private RIT_TechManScope__c techManScope= new RIT_TechManScope__c();
    private RIT_ContractualEnv__c contrEnv= new RIT_ContractualEnv__c();
    private String query;
    
    //This method queries the Notes and Attachments for the corresponding object name & object Id and returns it.
    public List<NoteAndAttachment> getNotesAndAttachments()
    {
        notesAndAttachmentslst = new List<NoteAndAttachment>();
        if(objectId!= null && objectName!= null)
        {
            query = 'select Id,(Select Id, IsNote, ParentId, Title, OwnerId, CreatedDate, CreatedById,  CreatedBy.Name,LastModifiedDate, LastModifiedById From NotesAndAttachments limit 5) from ' + objectName + ' WHERE ' + objectName + '.' + 
                          'Id='+'\'' + objectId + '\''  ; 
            
            // execute the query
            queryResult = Database.query(query) ;  
    
            for(Sobject obj_Sobject: queryResult )
            {
                if(objectName == 'RIT_ProjectInformation__c')
                {                
                    projInfo =(RIT_ProjectInformation__c)obj_Sobject;
                    notesAndAttachmentslst= projInfo.NotesAndAttachments;
                }
                else if(objectName == 'RIT_ProjectManagement__c')
                {                
                    projMgnt =(RIT_ProjectManagement__c)obj_Sobject;
                    notesAndAttachmentslst= projMgnt.NotesAndAttachments;
                }
                else if(objectName == 'RIT_FinancialEnv__c')
                {                
                    finEnv =(RIT_FinancialEnv__c)obj_Sobject;
                    notesAndAttachmentslst= finEnv.NotesAndAttachments;
                }
                else if(objectName == 'RIT_Budget__c')
                {                
                    budget =(RIT_Budget__c)obj_Sobject;
                    notesAndAttachmentslst= budget.NotesAndAttachments;
                }
                else if(objectName == 'RIT_ContractualEnv__c')
                {                
                    contrEnv=(RIT_ContractualEnv__c)obj_Sobject;
                    notesAndAttachmentslst= contrEnv.NotesAndAttachments;
                }
                else if(objectName == 'RIT_TechManScope__c')
                {                
                    techManScope=(RIT_TechManScope__c)obj_Sobject;
                    notesAndAttachmentslst= techManScope.NotesAndAttachments;
                }
                else if(objectName == 'RIT_CommCompEnv__c')
                {                
                    commAndComp=(RIT_CommCompEnv__c)obj_Sobject;
                    notesAndAttachmentslst= commAndComp.NotesAndAttachments;
                }
            }
        }
        
        if(notesAndAttachmentslst!=null && notesAndAttachmentslst.size()>0)
        {
            return notesAndAttachmentslst;
            
        }
        else
        {
           return null;        
        }
    }
}