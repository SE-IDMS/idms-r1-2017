/*
    Modified By: Renjith Jose
    Description: BR:5836,Ability to change the name of ORF Level Extension / Program Application assessment when performing cloning operation
    Release    : October 2014
   --------------------------------------------- 
*/

public with sharing class VFC_AssessmentClone{
    SObjectAllFieldCloner sCloner = new SObjectAllFieldCloner();
    public class ApplicationException extends Exception {}

    private string pageid;
    public Boolean hasError {get;set;} {hasError = false;}
    public String name{get;set;}
    public String clonedName{get;set;}
    public Set<String> exstNames = new Set<String>();
    //public List<Assessment__c> exstAssessment = new List<Assessment__c>([SELECT Id, Name FROM Assessment__c]);
    
   
    
    
    
    public VFC_AssessmentClone(ApexPages.StandardController controller){
        pageid = ApexPages.currentpage().getParameters().get('Id');
        main();
    }
    
    public void main() {
        Assessment__c originalAssessment = [SELECT Id, Name FROM Assessment__c WHERE Id = :pageid];
            name = originalAssessment.Name; 
    }
    
    public pagereference doCancel() {
        
        PageReference pageref = new PageReference('/'+pageid); 
        pageref.setRedirect(true); 
        return pageref;
                
    }
    
    
    public pagereference cloneAssessment() {
       
        Assessment__c cloned = new Assessment__c();
        System.debug('clonedName:'+clonedName);
        //Added by Akila for October 2014 Release, prompts for a name for the Cloned Assessment
        if(clonedName == null || clonedName == '' || (clonedName != null && clonedName.trim() == '')) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLOCT14PRM24));
            return null;
        }
        //else if(exstNames.contains(clonedName)) {
        Integer aCount = [SELECT COUNT() FROM Assessment__c WHERE Name = :clonedName];
        if (aCount > 0) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLOCT14PRM66));
            return null;    
        }
        else {
            try {
                Assessment__c originalAssessment = [SELECT Id, Name FROM Assessment__c WHERE Id = :pageid];
                name = originalAssessment.Name;
                System.debug('Name:'+name);
                List<sObject> originalSObjects = new List<sObject>{ ((sObject)originalAssessment) };
                
                // Clonging Assessments
                System.Debug('Original Assesments: ' + originalSObjects);
                List<Assessment__c> clonedAssessment = sCloner.cloneObjects(originalSobjects, (new Assessment__c()).getsObjectType());
                for (Assessment__c asmt : clonedAssessment) {
                    asmt.Active__c = false;
                    asmt.Name = clonedName; //October14 Release, BR:5836
                }

                System.Debug('Cloned Assesments Before INSERT: ' + clonedAssessment);
                INSERT clonedAssessment;                                     
                System.Debug('Cloned Assesments After INSERT: ' + clonedAssessment);

                // List of Questions to be cloned
                if(clonedAssessment != null && !clonedAssessment.isEmpty()) {
                    cloned = clonedAssessment.get(0);
                    List<Id> assessmentIdList = new List<Id>(new Map<Id, SObject>(originalSObjects).keySet());
                    System.Debug('Cloned Assessment Id: ' + cloned.Id);

                    List<OpportunityAssessmentQuestion__c> lstQues = new List<OpportunityAssessmentQuestion__c>([
                                    SELECT Id, Name FROM OpportunityAssessmentQuestion__c 
                                    WHERE Assessment__c IN :assessmentIdList]);

                    List<SObject> originalSObjects1 = new List<SObject>(((List<SObject>)lstQues));
                    System.Debug('Original Questions: ' + originalSObjects1);
                    // Cloned Questions
                    List<OpportunityAssessmentQuestion__c> clonedQuestions = sCloner.cloneObjects(originalSobjects1, (new OpportunityAssessmentQuestion__c()).getsObjectType(), true, true);

                    for(OpportunityAssessmentQuestion__c Ques : clonedQuestions){
                        Ques.Assessment__c = cloned.id;
                    }

                    System.Debug('Cloned Questions Before INSERT: ' + clonedQuestions);
                    INSERT clonedQuestions;
                    System.Debug('Cloned Questions After INSERT: ' + clonedQuestions);

                    Map<Id,Id> oldnewids = new Map<Id,Id>();
                    for(OpportunityAssessmentQuestion__c OAQ : clonedQuestions){
                        oldnewids.put(OAQ.TECH_ClonedFrom__c, OAQ.Id);
                    }
                    System.Debug('Old - New Question Map: ' + oldnewids);
                    
                    // List of Answers to be cloned
                    List<OpportunityAssessmentAnswer__c> lstAns = new List<OpportunityAssessmentAnswer__c>([
                                        SELECT id, Name, Question__c FROM OpportunityAssessmentAnswer__c 
                                        WHERE Question__c = :lstques]);
                    List<SObject> originalSObjects2 = new List<SObject>(((List<SObject>)lstAns));

                    // Cloned Answers.
                    System.Debug('Original Answers: ' + originalSObjects2);

                    List<OpportunityAssessmentAnswer__c> clonedAnswers = sCloner.cloneObjects(originalSObjects2, (new OpportunityAssessmentAnswer__c()).getSObjectType());

                    for (OpportunityAssessmentAnswer__c ans : clonedAnswers){
                        ans.Question__c = oldnewids.get(Ans.Question__c);
                    }

                    System.Debug('Cloned Answers Before INSERT: ' + clonedAnswers);
                    INSERT clonedAnswers;

                    System.Debug('Cloned Answers After INSERT: ' + clonedAnswers);
                }  
          
               //  PageReference redirect = new PageReference('/'+cloned.id+'/e?retURL=%2Fa'+cloned.id); //October14 Release, BR:5836
                //redirect.getparameters().put('Id',cloned.id);
                  PageReference redirect = new PageReference('/'+cloned.id); //October14 Release, BR:5836
                
                 redirect.setRedirect(true); 
                 return redirect;
               
                
            }
            
            catch (Exception ex) {
                System.Debug('Exception: ' + ex.getMessage() + ex.getStackTraceString());
                ApexPages.addMessage(new ApexPages.message(
                        ApexPages.severity.FATAL, 
                        System.Label.CLNOV13PRM003 + '<br />' + 
                        'Technical Information: ' + ex.getMessage() + '<br />' + ex.getStackTraceString()
                        )); 
                return null;
            }
        }
        
    }
}