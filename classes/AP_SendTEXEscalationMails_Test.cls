@istest

public class AP_SendTEXEscalationMails_Test{

    public static testMethod void AP_SendTEXEscalationMailsTest(){
    
        set<TEX__c> setTex1 = new  set<TEX__c>();
        set<ID> setTexSHId = new set<ID>();
        List<User> lstUser = new List<User>();
        
         Country__c newCountry = Utils_TestMethods.createCountry();
              newCountry.Region__c=Label.CL00320;
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
        if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
        // Create Account
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=newCountry.Id; 
    Database.SaveResult AccountInsertResult = Database.insert(accountObj, false);
    if(!AccountInsertResult.isSuccess())
            Database.Error err1 = AccountInsertResult.getErrors()[0];
    // Create Contact to the Account
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
     Database.SaveResult ContactInsertResult = Database.insert(ContactObj, false);
    if(!ContactInsertResult.isSuccess())
            Database.Error err2 = ContactInsertResult.getErrors()[0];
    // Create Case
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
     CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.Quantity__c = 4;
    CaseObj.Family__c = 'ADVANCED PANEL';
    CaseObj.CommercialReference__c='xbtg5230';
    CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
    CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
    Database.SaveResult CaseInsertResult = Database.insert(CaseObj, false);
    if(!CaseInsertResult.isSuccess())
            Database.Error err3 = CaseInsertResult.getErrors()[0];
        BusinessRiskEscalationEntity__c Org01 = new BusinessRiskEscalationEntity__c(SubEntity__c = '', Location__c = '', Entity__c = '@#$123TestEntity*&^',Location_Type__c = 'Country Front Office', ContactEmail__c = 'testContactEmail01@testSE.com');
        insert Org01;
        BusinessRiskEscalationEntity__c Org02 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '', Entity__c = '@#$123TestEntity*&^', Location_Type__c = 'Adaptation Center', ContactEmail__c = 'testContactEmail02@testSE.com');
        insert Org02;
        BusinessRiskEscalationEntity__c Org03 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '123TestSubEntityLocation', Entity__c = '@#$123TestEntity*&^' , Location_Type__c = 'Adaptation Center', ContactEmail__c = 'testContactEmail03@testSE.com');
        insert Org03;
        BusinessRiskEscalationEntity__c Org11 = new BusinessRiskEscalationEntity__c(SubEntity__c = '', Location__c = '', Entity__c = '@#$123TestEntity*&^1234', Location_Type__c = 'Country Front Office (verticalized)', ContactEmail__c = 'testContactEmail11@testSE.com');
        insert Org11;
        BusinessRiskEscalationEntity__c Org12 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '', Entity__c = '@#$123TestEntity*&^1234', Location_Type__c = 'Country Front Office (verticalized)', ContactEmail__c = 'testContactEmail12@testSE.com');
        insert Org12;
        BusinessRiskEscalationEntity__c Org13 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '123TestSubEntitylOCATION', Entity__c = '@#$123TestEntity*&^1234', Location_Type__c = 'Country Front Office (verticalized)', ContactEmail__c = 'testContactEmail13@testSE.com');
        insert Org13;
         
         List<EntityStakeholder__c> lsTexSH = new List<EntityStakeholder__c>();
         
         EntityStakeholder__c TexSH00 =  new EntityStakeholder__c(Role__c = 'CS&Q Manager', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org01.id ); lsTexSH.add(TexSH00);
         EntityStakeholder__c TexSH01 =  new EntityStakeholder__c(Role__c = 'TEX Escalation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org01.id ); lsTexSH.add(TexSH01);
         EntityStakeholder__c TexSH02 =  new EntityStakeholder__c(Role__c = 'I2P Process Advocate', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org02.id ); lsTexSH.add(TexSH02);
         EntityStakeholder__c TexSH03=  new EntityStakeholder__c(Role__c = 'TEX Creation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org02.id ); lsTexSH.add(TexSH03);
        
        EntityStakeholder__c TexSH10 =  new EntityStakeholder__c(Role__c = 'CS&Q Manager', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org03.id ); lsTexSH.add(TexSH10);
         EntityStakeholder__c TexSH11 =  new EntityStakeholder__c(Role__c = 'TEX Escalation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org03.id ); lsTexSH.add(TexSH11);
         EntityStakeholder__c TexSH12 =  new EntityStakeholder__c(Role__c = 'I2P Process Advocate', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org11.id ); lsTexSH.add(TexSH12);
         EntityStakeholder__c TexSH13 =  new EntityStakeholder__c(Role__c = 'TEX Creation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org11.id ); lsTexSH.add(TexSH13);
         
         EntityStakeholder__c TexSH20 =  new EntityStakeholder__c(Role__c = 'CS&Q Manager', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org12.id ); lsTexSH.add(TexSH20);
         EntityStakeholder__c TexSH21 =  new EntityStakeholder__c(Role__c = 'TEX Escalation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org12.id ); lsTexSH.add(TexSH21);
         EntityStakeholder__c TexSH22 =  new EntityStakeholder__c(Role__c = 'I2P Process Advocate', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org13.id ); lsTexSH.add(TexSH22);
         EntityStakeholder__c TexSH23 =  new EntityStakeholder__c(Role__c = 'TEX Creation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org13.id ); lsTexSH.add(TexSH23);
        
         insert lsTexSH;
        
   
        CS_TEXStakeholders__c cs1= new CS_TEXStakeholders__c(Name = 'cs1', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c =1 ,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs2= new CS_TEXStakeholders__c(Name = 'cs2', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 1,FromOrg__c = 'Expert Center',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs3= new CS_TEXStakeholders__c(Name = 'cs3', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c =1 ,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs4= new CS_TEXStakeholders__c(Name = 'cs4', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 1,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs5= new CS_TEXStakeholders__c(Name = 'cs5', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 1,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs6= new CS_TEXStakeholders__c(Name = 'cs6', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 3,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs7= new CS_TEXStakeholders__c(Name = 'cs7', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 3,FromOrg__c = 'Expert Center',Level__c = 'Sub-entity',Role__c='I2P Process Advocate');
        CS_TEXStakeholders__c cs8= new CS_TEXStakeholders__c(Name = 'cs8', EmailTemplate__c = 'TEX_SendEscalationMails_D8',EscalationDays__c = 8,FromOrg__c = 'FO Organization',Level__c = 'Entity',Role__c='I2P Process Advocate');
        list<CS_TEXStakeholders__c> lstCS = new list<CS_TEXStakeholders__c>{cs1, cs2, cs3, cs4, cs5, cs6, cs7, cs8};
        insert lstCS;
        
       

        TEX__c tex1 = new TEX__c( OpenDate__c = System.today(),Status__c = 'Expert Assessment to be started', PromiseDate__c = System.today(), case__C = CaseObj.id, OfferExpert__c = UserInfo.getUserId(), ExpertCenter__c = Org01.ID, FrontOfficeTEXManager__c = UserInfo.getUserId() , FrontOfficeOrganization__c = Org13.ID, LineofBusiness__c = Org12.ID);
        
        TEX__c tex2 = new TEX__c( OpenDate__c = System.today()-40,Status__c = 'Expert Assessment to be started', case__C = CaseObj.id, OfferExpert__c = UserInfo.getUserId(), ExpertCenter__c = Org02.ID, FrontOfficeTEXManager__c = UserInfo.getUserId() ,FrontOfficeOrganization__c = Org11.ID, LineofBusiness__c = Org03.ID);
         
         TEX__c tex3 = new TEX__c( OpenDate__c = System.today(),LastNotificationDay__c=3,Status__c = 'Expert Assessment to be started', PromiseDate__c = System.today(), case__C = CaseObj.id, OfferExpert__c = UserInfo.getUserId(), ExpertCenter__c = Org01.ID, FrontOfficeTEXManager__c = UserInfo.getUserId() , FrontOfficeOrganization__c = Org13.ID, LineofBusiness__c = Org12.ID);
       
        TEX__c tex4 = new TEX__c( OpenDate__c = System.today()-30,LastNotificationDay__c=8,Status__c = 'Expert Assessment to be started',PromiseDate__c = System.today(), case__C = CaseObj.id, OfferExpert__c = UserInfo.getUserId(), ExpertCenter__c = Org02.ID, FrontOfficeTEXManager__c = UserInfo.getUserId() ,FrontOfficeOrganization__c = Org11.ID, LineofBusiness__c = Org03.ID);
        
        TEX__c tex5 = new TEX__c( OpenDate__c = System.today()-30,LastNotificationDay__c=8,Status__c = 'Expert Assessment to be started',PromiseDate__c = System.today(), case__C = CaseObj.id, OfferExpert__c = UserInfo.getUserId(), FrontOfficeTEXManager__c = UserInfo.getUserId() ,FrontOfficeOrganization__c = Org11.ID, LineofBusiness__c = Org03.ID);
        list<TEX__c> lstTex = new List<TEX__c>{tex1,tex2,tex3,tex4,tex5};
        
        insert lstTex;
        
        list<TEX__c> testTEX=[select id,OpenDate__c,ExpertCenter__c,LastNotificationDay__c,Status__c,OfferExpert__c ,OfferExpert__r.email,OfferExpert__r.id,ExpertCenter__r.ContactEmail__c,ExpertCenter__r.SubEntity__c,ExpertCenter__r.id,FrontOfficeOrganization__c,LineofBusiness__c,EscalationDate__c,ExpertAssessmentCompleted__c,FrontOfficeTEXManager__r.Email,FrontOfficeTEXManager__r.id,FrontOfficeOrganization__r.SubEntity__c,FrontOfficeOrganization__r.id,ReminderDate__c,FrontOfficeOrganization__r.Entity__c,LineofBusiness__r.id,LineofBusiness__r.Entity__c,case__C,PromiseDate__c,FrontOfficeTEXManager__c from TEX__c limit 10];
        AP_SendTEXEscalationMails.createTEXStakeholder(new List<TEX__c>{testTEX[0]});
         AP_SendTEXEscalationMails.createTEXStakeholder(new List<TEX__c>{testTEX[1]});
         AP_SendTEXEscalationMails.createTEXStakeholder(new List<TEX__c>{testTEX[2]});
          AP_SendTEXEscalationMails.createTEXStakeholder(new List<TEX__c>{testTEX[3]});
          AP_SendTEXEscalationMails.createTEXStakeholder(new List<TEX__c>{testTEX[4]});
         
          AP_SendTEXEscalationMails.getEmailfromOrg('CS&Q Manager', Org13.id, Org13.Entity__c, '','Sub-entity');
     
       
      }  
       public static testMethod void AP_SendTEXEscalationMailsTest2(){
    
        set<TEX__c> setTex1 = new  set<TEX__c>();
        set<ID> setTexSHId = new set<ID>();
        List<User> lstUser = new List<User>();
        
         Country__c newCountry = Utils_TestMethods.createCountry();
              newCountry.Region__c=Label.CL00320;
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
        if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
        // Create Account
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=newCountry.Id; 
    Database.SaveResult AccountInsertResult = Database.insert(accountObj, false);
    if(!AccountInsertResult.isSuccess())
            Database.Error err1 = AccountInsertResult.getErrors()[0];
    // Create Contact to the Account
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
     Database.SaveResult ContactInsertResult = Database.insert(ContactObj, false);
    if(!ContactInsertResult.isSuccess())
            Database.Error err2 = ContactInsertResult.getErrors()[0];
    // Create Case
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
     CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.Quantity__c = 4;
    CaseObj.Family__c = 'ADVANCED PANEL';
    CaseObj.CommercialReference__c='xbtg5230';
    CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
    CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
    Database.SaveResult CaseInsertResult = Database.insert(CaseObj, false);
    if(!CaseInsertResult.isSuccess())
            Database.Error err3 = CaseInsertResult.getErrors()[0];
        BusinessRiskEscalationEntity__c Org01 = new BusinessRiskEscalationEntity__c(SubEntity__c = '', Location__c = '', Entity__c = '@#$123TestEntity*&^',Location_Type__c = 'Country Front Office', ContactEmail__c = 'testContactEmail01@testSE.com');
        insert Org01;
        BusinessRiskEscalationEntity__c Org02 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '', Entity__c = '@#$123TestEntity*&^', Location_Type__c = 'Adaptation Center', ContactEmail__c = 'testContactEmail02@testSE.com');
        insert Org02;
        BusinessRiskEscalationEntity__c Org03 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '123TestSubEntityLocation', Entity__c = '@#$123TestEntity*&^' , Location_Type__c = 'Adaptation Center', ContactEmail__c = 'testContactEmail03@testSE.com');
        insert Org03;
        BusinessRiskEscalationEntity__c Org11 = new BusinessRiskEscalationEntity__c(SubEntity__c = '', Location__c = '', Entity__c = '@#$123TestEntity*&^1234', Location_Type__c = 'Country Front Office (verticalized)', ContactEmail__c = 'testContactEmail11@testSE.com');
        insert Org11;
        BusinessRiskEscalationEntity__c Org12 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '', Entity__c = '@#$123TestEntity*&^1234', Location_Type__c = 'Country Front Office (verticalized)', ContactEmail__c = 'testContactEmail12@testSE.com');
        insert Org12;
        BusinessRiskEscalationEntity__c Org13 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '123TestSubEntitylOCATION', Entity__c = '@#$123TestEntity*&^1234', Location_Type__c = 'Country Front Office (verticalized)', ContactEmail__c = 'testContactEmail13@testSE.com');
        insert Org13;
         
         List<EntityStakeholder__c> lsTexSH = new List<EntityStakeholder__c>();
         
         EntityStakeholder__c TexSH00 =  new EntityStakeholder__c(Role__c = 'CS&Q Manager', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org01.id ); lsTexSH.add(TexSH00);
         EntityStakeholder__c TexSH01 =  new EntityStakeholder__c(Role__c = 'TEX Escalation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org01.id ); lsTexSH.add(TexSH01);
         EntityStakeholder__c TexSH02 =  new EntityStakeholder__c(Role__c = 'I2P Process Advocate', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org02.id ); lsTexSH.add(TexSH02);
         EntityStakeholder__c TexSH03=  new EntityStakeholder__c(Role__c = 'TEX Creation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org02.id ); lsTexSH.add(TexSH03);
        
        EntityStakeholder__c TexSH10 =  new EntityStakeholder__c(Role__c = 'CS&Q Manager', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org03.id ); lsTexSH.add(TexSH10);
         EntityStakeholder__c TexSH11 =  new EntityStakeholder__c(Role__c = 'TEX Escalation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org03.id ); lsTexSH.add(TexSH11);
         EntityStakeholder__c TexSH12 =  new EntityStakeholder__c(Role__c = 'I2P Process Advocate', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org11.id ); lsTexSH.add(TexSH12);
         EntityStakeholder__c TexSH13 =  new EntityStakeholder__c(Role__c = 'TEX Creation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org11.id ); lsTexSH.add(TexSH13);
         
         EntityStakeholder__c TexSH20 =  new EntityStakeholder__c(Role__c = 'CS&Q Manager', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org12.id ); lsTexSH.add(TexSH20);
         EntityStakeholder__c TexSH21 =  new EntityStakeholder__c(Role__c = 'TEX Escalation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org12.id ); lsTexSH.add(TexSH21);
         EntityStakeholder__c TexSH22 =  new EntityStakeholder__c(Role__c = 'I2P Process Advocate', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org13.id ); lsTexSH.add(TexSH22);
         EntityStakeholder__c TexSH23 =  new EntityStakeholder__c(Role__c = 'TEX Creation Informed', User__c = UserInfo.getUserId() , BusinessRiskEscalationEntity__c = Org13.id ); lsTexSH.add(TexSH23);
        
         insert lsTexSH;
        
   
        CS_TEXStakeholders__c cs1= new CS_TEXStakeholders__c(Name = 'cs1', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c =1 ,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs2= new CS_TEXStakeholders__c(Name = 'cs2', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 1,FromOrg__c = 'Expert Center',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs3= new CS_TEXStakeholders__c(Name = 'cs3', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c =1 ,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs4= new CS_TEXStakeholders__c(Name = 'cs4', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 1,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs5= new CS_TEXStakeholders__c(Name = 'cs5', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 1,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs6= new CS_TEXStakeholders__c(Name = 'cs6', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 3,FromOrg__c = '',Level__c = '',Role__c='Diagnostic Expert');
        CS_TEXStakeholders__c cs7= new CS_TEXStakeholders__c(Name = 'cs7', EmailTemplate__c = 'TEX_SendEscalationMails',EscalationDays__c = 3,FromOrg__c = 'Expert Center',Level__c = 'Sub-entity',Role__c='I2P Process Advocate');
        CS_TEXStakeholders__c cs8= new CS_TEXStakeholders__c(Name = 'cs8', EmailTemplate__c = 'TEX_SendEscalationMails_D8',EscalationDays__c = 8,FromOrg__c = 'FO Organization',Level__c = 'Entity',Role__c='I2P Process Advocate');
        list<CS_TEXStakeholders__c> lstCS = new list<CS_TEXStakeholders__c>{cs1, cs2, cs3, cs4, cs5, cs6, cs7, cs8};
        insert lstCS;
        
       

       
        
        TEX__c tex5 = new TEX__c( OpenDate__c = System.today(),LastNotificationDay__c=1,Status__c = 'Expert Assessment to be started',PromiseDate__c = System.today(), case__C = CaseObj.id, OfferExpert__c = UserInfo.getUserId(), FrontOfficeTEXManager__c = UserInfo.getUserId() ,FrontOfficeOrganization__c = Org11.ID, LineofBusiness__c = Org03.ID,ExpertCenter__c=Org02.ID);
        list<TEX__c> lstTex = new List<TEX__c>{tex5};
        
        insert lstTex;
        
        list<TEX__c> testTEX=[select id,OpenDate__c,ExpertCenter__c,ExpertCenter__r.Entity__c,LastNotificationDay__c,Status__c,OfferExpert__c ,OfferExpert__r.email,OfferExpert__r.id,ExpertCenter__r.ContactEmail__c,ExpertCenter__r.SubEntity__c,ExpertCenter__r.id,FrontOfficeOrganization__c,LineofBusiness__c,EscalationDate__c,ExpertAssessmentCompleted__c,FrontOfficeTEXManager__r.Email,FrontOfficeTEXManager__r.id,FrontOfficeOrganization__r.SubEntity__c,FrontOfficeOrganization__r.id,ReminderDate__c,FrontOfficeOrganization__r.Entity__c,LineofBusiness__r.id,LineofBusiness__r.Entity__c,LineofBusiness__r.SubEntity__c,case__C,PromiseDate__c,FrontOfficeTEXManager__c from TEX__c where id=:lstTex[0].id];
        AP_SendTEXEscalationMails.createTEXStakeholder(new List<TEX__c>{testTEX[0]});       
          AP_SendTEXEscalationMails.getEmailfromOrg('CS&Q Manager', Org13.id, Org13.Entity__c, '','Sub-entity');
     
       
      }  
    }