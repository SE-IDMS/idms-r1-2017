@IsTest
public class PRM_UserMigrationControllerTest{
    public static testMethod void test(){
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
        List<String> contactIds = new List<String>();

        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        thisUser.BypassVR__c = true;
        System.runAs ( sysAdmin ) {
            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;
            Account acc = Utils_TestMethods.createAccount();
            acc.PRMUIMSId__c ='theAccExtId_1';    
            acc.Country__c=france.Id;
            acc.Street__c='my Street';
            insert acc;
            
            Contact con = Utils_TestMethods.createContact(acc.Id,'myContactName');
            con.PRMUIMSID__c =  'theContExtId_1';
            con.email='prm.user@account-site.com';
            insert con;

            Contact con2 = Utils_TestMethods.createContact(acc.Id,'myContactName 2');
            con2.PRMUIMSID__c =  'theContExtId_2';
            con2.PRMEmail__c ='prm.user2@account-site.com';
            con2.email='prm.user2@account-site.com';
            insert con2;

            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            User u1 = new User(FirstName = con2.FirstName, ProfileId = ProfileFieloId, FederationIdentifier=con2.PRMUIMSId__c,LastName = con2.LastName, Email = con2.PRMEmail__c, Username = con2.PRMEmail__c + '.bfo.com',Alias ='teOo8_', CommunityNickname = con2.Name+'8_9', ContactId = con2.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert u1;


            PRM_UserMigration__c cusSetting = new PRM_UserMigration__c(); 
            cusSetting.LanguageLocaleKey__c = 'en_US' ;
            cusSetting.LocaleSidKey__c = 'fr_FR' ;
            cusSetting.TimeZoneSidKey__c = 'GMT';
            cusSetting.CurrencyIsoCode__c = 'EUR';
            cusSetting.name = System.Label.PRM_UserMigration;
            insert cusSetting;

            contactIds.add(con.Id);
            contactIds.add(con2.Id);
            System.assertNotEquals(null,PRM_UserMigrationController.migrateContacts(contactIds));
            User u = [select CurrencyIsoCode,LocaleSidKey from User where id =:u1.Id];
            System.assertEquals('EUR',u.CurrencyIsoCode);
            System.assertEquals('fr_FR',u.LocaleSidKey);
        }
        
        System.runAs(thisUser){ 
            System.assertNotEquals(null,PRM_UserMigrationController.createFieloMember(contactIds));
            System.assertNotEquals(null,PRM_UserMigrationController.addPermissionSet(contactIds));
            for(Contact c : [select Id from contact where FieloEE__Member__c!=null limit 4]){
                contactIds.add(c.Id);
            }
            contactIds.add('003A000001Ydq4jIABafafe');
            contactIds.add('');
        }
        System.runAs ( sysAdmin ) {
            System.assertNotEquals(null,PRM_UserMigrationController.migrateContacts(contactIds));
        }
        System.runAs(thisUser){ 
            System.assertNotEquals(null,PRM_UserMigrationController.createFieloMember(contactIds));
            System.assertNotEquals(null,PRM_UserMigrationController.addPermissionSet(contactIds));
        }   
        
        
    }
    

}