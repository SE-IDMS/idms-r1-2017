global class ProgramFeatureRequirementWrapper implements Comparable{

    public Id RecordId { get; set; }
    
    public String SortByFeauture{ get; set; }
    
    public String SortByRequirement{ get; set; }
    
    public ProgramFeature__c ProgramFeature {get;set;} 
    
    public ProgramRequirement__c ProgramRequirement { get; set; }

    public boolean ToBeDeleted { get; set; }
    public string UpdateStatus { get; set; }
        
    public ProgramFeatureRequirementWrapper() {
        ProgramFeature = new ProgramFeature__c();
        ProgramRequirement = new ProgramRequirement__c();
    }

    global Integer compareTo(Object compareTo) 
    {
        ProgramFeatureRequirementWrapper compareToOppy = (ProgramFeatureRequirementWrapper)compareTo;
        if(SortByFeauture!=null)
        {
            Integer returnValue = 0;
            if (SortByFeauture> compareToOppy.SortByFeauture)
            {
                returnValue = 1;
            }
            else if (SortByFeauture< compareToOppy.SortByFeauture)
            {
                returnValue = -1;
            }
            return returnValue;
        }
        else
        {
            Integer returnValue = 0;
            if (SortByRequirement > compareToOppy.SortByRequirement)
            {
                returnValue = 1;
            }
            else if (SortByRequirement < compareToOppy.SortByRequirement)
            {
                returnValue = -1;
            }
            return returnValue;
        }
        return null;
    }
}