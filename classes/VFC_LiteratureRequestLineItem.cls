public class VFC_LiteratureRequestLineItem extends VFC_ControllerBase {

    public litGMRSearch GMRsearchForlitLineItem{get;set;}
    public LiteratureRequest__c litRequest {get;set;}
    LiteratureRequestLineItem__c commRef ; // New Commecial Reference to be inserted.    
    Public String LineItemID;

    /*=======================================
    INNER CLASSES
    =======================================*/ 
    // GMR implementation for Cases
    public class litGMRSearch implements Utils_GMRSearchMethods
    {
        VFC_LiteratureRequestLineItem thisController;
        public Void Init(String ObjID, VCC08_GMRSearch Controller)
        {
            thisController = (VFC_LiteratureRequestLineItem)Controller.pageController;

        }

        // Cancel method returns the associated Literature request detail page
        public PageReference Cancel( VCC08_GMRSearch Controller)
        { 
            String url = '/'+thisController.litRequest.Id;
            PageReference pageRef = new PageReference(url);
            pageRef.setRedirect(true);
            return pageRef;
        }   
        
        public Pagereference returnLineItemEditPage()
        {
            // Create a new edit page for the Literature Request
            Pagereference LiteratureRequestLineitemEditPage;
              // Disable override
                     
            LiteratureRequestLineitemEditPage = new PageReference('/'+SObjectType.LiteratureRequestLineitem__c.getKeyPrefix()+'/e' );           
             // Disable override
            LiteratureRequestLineitemEditPage.getParameters().put(Label.CL00690, Label.CL00691);
            // Assign Commercial reference - Label.CLDEC12CCC001
            LiteratureRequestLineitemEditPage.getParameters().put(Label.CLDEC12CCC07, thisController.commRef.CommercialReference__c);               
           // Assign Parent Id(Literature request ID)
            LiteratureRequestLineitemEditPage.getParameters().put(Label.CLDEC12CCC06, thisController.litRequest.Name);
            // Assign Quantity
            LiteratureRequestLineitemEditPage.getParameters().put(Label.CLDEC12CCC05, thisController.litRequest.Quantity__c.format());
            return LiteratureRequestLineitemEditPage;
        }
        // Action performed when clicking on "Select" on the PM0 search results
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
        {

            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)controllerBase;
            DataTemplate__c Product = (DataTemplate__c)obj;
            
            
           

            if(GMRController.ActionNumber==1)
            {
                if(Product.field8__c != null)
                    thisController.commRef.CommercialReference__c = Product.field8__c;             
               
            }
           
                
            //if(thisController.litRequest.Id != null && system.currentpagereference().getparameters().get('LtrlnId') == null)
            //    thisController.commRef.LiteratureRequest__c =  thisController.litRequest.Id;                
            //else 
            //{
               // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,Label.CLDEC12CCC002));
               // return null;
           // }  
            
            try
            {
                
                If(system.currentpagereference().getparameters().get('LtrlnId') == null){
                thisController.commRef.Quantity__c = 1;
                insert thisController.commRef;
                String url = '/'+thisController.litRequest.Id;
                PageReference pageRef = new PageReference(url);
                pageRef.setRedirect(true);
                return pageRef;
                }
                else{
                Update thisController.commRef;
                String url = '/'+thisController.litRequest.Id;
                PageReference pageRef = new PageReference(url);
                pageRef.setRedirect(true);
                return pageRef;
                }
                /*
                Pagereference pageRef;
                pageRef = returnLineitemEditPage();
                return pageRef;*/
            } 
            catch(Exception e)
            {   
                String errorMessage=e.getMessage();               
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'An error occured while adding product:'+ e.getMessage()));                
                return null;
            }
            return null;
        }
    }
    
    
    
    public VFC_LiteratureRequestLineItem(ApexPages.StandardController controller)
    {
        LineItemID = system.currentpagereference().getparameters().get('LtrlnId');
        If(LineItemID != null && LineItemID != ''){
        commRef =[Select Id,LiteratureRequest__c,Commercialreference__c,Quantity__c from LiteratureRequestLineItem__c where Id=: LineItemID Limit 1];
        }else{
        commRef =  new LiteratureRequestLineItem__c();
        commRef = (LiteratureRequestLineItem__c)controller.getRecord();
        }
        //litRequestId = commRef.LiteratureRequest__c;
        if(commRef.LiteratureRequest__c != null)
            litRequest = [select id,name,Status__c,Quantity__c  from LiteratureRequest__c where id = : commRef.LiteratureRequest__c limit 1];
        GMRsearchForlitLineItem = new litGMRSearch();
    }
}