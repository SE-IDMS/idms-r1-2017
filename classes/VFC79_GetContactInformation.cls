global with sharing class VFC79_GetContactInformation{
    
    @RemoteAction
    global static List<CTR_ValueChainPlayers__c> verifyPIN(string strPin){
        List<CTR_ValueChainPlayers__c>  CVCPlist;
        if(strPin!=null || strPin !=''){
            CVCPlist=[Select id,LegacyPIN__c,Contact__c,Account__c,Points__c from CTR_ValueChainPlayers__c where LegacyPIN__c =:strPin]; 
        }
        return CVCPlist;  
    }
    @RemoteAction
    global static List<AccountContactSize > findPhone(string  phoneNo){
        list<AccountContactSize> lstAccountContactSize  = new list<AccountContactSize>();
        List<List<Sobject>> searchResults;
        List<Contact> allContacts=new List<Contact>(); //To store Contact records from search results
        List<Account> allAccounts=new List<Account>();//To store Account records from search results
        List<ANI__c> allANIs= new List<ANI__c>();//May 2013 Release - To store ANI records from search results
        List<Contact> allANIContacts= new List<Contact>(); //May 2013 Release - To store Contact records from ANI search results
        List<Contact> allANIPersonAccounts = new List<Contact>(); //Oct 2013 Release - To store Person records from ANI search results
        List<Account> allANIAccounts;
        System.debug('phoneNo:'+phoneNo);
        if(phoneNo.length()>2){
            system.debug('######'+phoneNo);
            if(phoneNo.startsWith('+')){
               phoneNo=+phoneNo.replace('+','');
            }
            //search for contacts and accounts
            system.debug('######'+phoneNo);
            searchResults=[find :phoneNo IN PHONE FIELDS RETURNING Contact(id,Name,Email,Phone,Title,IsPersonAccount, AccountId WHERE Inactive__c=False AND ToBeDeleted__c=False AND Account.ToBeDeleted__c=False), Account(id,Name,phone where IsPersonAccount=false AND ToBeDeleted__c=False), ANI__c(Contact__c, Account__c where contact__r.Inactive__c=false AND contact__r.ToBedeleted__c=false AND Account__r.ToBedeleted__c=false)];           

            //May Release 2013 - Additional logic for ANI object search
            Set<Contact> mergedContacts = new Set<Contact>();//Temporary set to merge Contacts and ANI Contacts
            Set<Account> mergedAccounts = new Set<Account>();//Temporary set to merge Accounts and ANI Accounts
            mergedContacts.addAll(((list<Contact>) searchResults[0]));
            mergedAccounts.addAll(((list<Account>) searchResults[1]));
            allANIs = ((list<ANI__c>) searchResults[2]);
          
            system.debug('allContacts ' + mergedContacts.size());
            system.debug('allAccounts ' + mergedAccounts.size());
            system.debug('allANIs ' + allANIs.size());
            if(allANIs.size() > 0){
                Set<Id> contactIds = new Set<Id>();//List of contact Ids from ANI SOSL search
                Set<Id> accountIds = new Set<Id>();//List of account Ids from ANI SOSL search
                for(ANI__c aniRecord : allANIs){
                    if(aniRecord.Contact__c != null)
                        contactIds.add(aniRecord.Contact__c);
                    if(aniRecord.Account__c != null)
                        accountIds.add(aniRecord.Account__c);
                }
                if(contactIds.size() > 0){
                    allANIContacts = getANIContacts(contactIds);//Get Contact collection of the ANI contacts 
                    mergedContacts.addAll(allANIContacts);
                }
                if(accountIds.size() > 0){
                    allANIPersonAccounts = getANIPersonAccounts(accountIds);//Get Person Account collection of the ANI accounts
                    mergedContacts.addAll(allANIPersonAccounts);
                }
                if(accountIds.size() > 0){
                    allANIAccounts = getANIAccounts(accountIds);//Get Account collection of the ANI accounts
                    mergedAccounts.addAll(allANIAccounts);
                }
            }
            allContacts.addAll(mergedContacts);
            allAccounts.addAll(mergedAccounts);
             
            AccountContactSize objAccountContactSize = new AccountContactSize();
            if(allContacts.size()> =1){
                objAccountContactSize.intContactId=allContacts.size();
                objAccountContactSize.contactid=allContacts[0].Id;
                objAccountContactSize.ContactName =allContacts[0].Name;
                list<Case>lstCase =[select Id,CaseNumber from case where contactid IN:allContacts];
                system.debug('VFC79_GetContactInformation.findPhone lstCase --> '+lstCase);
                if(lstCase.size()>=1){
                    objAccountContactSize.intCaseId =lstCase.size();
                    objAccountContactSize.caseId = lstCase[0].Id ; 
                    objAccountContactSize.CaseNumber = lstCase[0].CaseNumber ;  
                }
            }
            else if(allAccounts.size() >= 1){
                objAccountContactSize.intAccountId=allAccounts.size();
                objAccountContactSize.AccountId =allAccounts[0].Id;
                objAccountContactSize.AccountName =allAccounts[0].Name;
            } 
            else{
                objAccountContactSize.strError ='No Match Found';
            } 
            lstAccountContactSize.add(objAccountContactSize);
        }
        return lstAccountContactSize;  
    }
    
    //This method retrieves the Contact Details for the list of Contact IDs passed
    global static List<Contact> getANIContacts(Set<Id> contactIds){
        List<Contact> aniContacts = [SELECT
                id,
                Name,
                Email,
                Phone,
                Title,
                IsPersonAccount,
                AccountId
                FROM Contact WHERE Id IN :contactIds];
        return aniContacts;
    }
    
    //This method retrieves the Account Details for the list of Account IDs passed
    global static List<Account> getANIAccounts(Set<Id> accountIds){
        List<Account> aniAccounts = [SELECT
                id,
                Name,
                Phone
                FROM Account WHERE Id IN :accountIds and IsPersonAccount= false];
        return aniAccounts;
    }
    
    //This method retrieves the Account Details for the list of Account IDs passed
    global static List<Contact> getANIPersonAccounts(Set<Id> accountIds){
        List<Contact> aniPersonAccount = [SELECT
                id,
                Name,
                Email,
                Phone,
                Title,
                IsPersonAccount,
                AccountId
                FROM Contact WHERE AccountId IN :accountIds and IsPersonAccount= true];
        return aniPersonAccount;
    }
    global class AccountContactSize{
        global Id contactId{get;set;}
        global Id accountId{get;set;}
        global Id caseId{get;set;}
        global Integer intContactId{get;set;}
        global Integer intAccountId{get;set;}
        global Integer intCaseId{get;set;}
        global String CaseNumber{get;set;}
        global String AccountName{get;set;}
        global String ContactName{get;set;}
        global string strError {get;set;}
    }
    
}//End of Class/