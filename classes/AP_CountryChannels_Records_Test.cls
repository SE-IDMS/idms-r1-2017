@isTest
public class AP_CountryChannels_Records_Test{

    @testSetup static void testSetupMethodCountryChannelsRecords() {
        List<CS_PRM_UserRegistration__c> userRegCSLst = new List<CS_PRM_UserRegistration__c>();
        CS_PRM_UserRegistration__c userRegCS = new CS_PRM_UserRegistration__c();
            userRegCS.Name = 'email';
            userRegCS.FieldLabel__c = 'Contact: Email';
            userRegCS.MappedField__c = 'PRMEmail__c';
            userRegCS.AppliedTo__c = 1;
            userRegCS.FieldSortOrder__c = 1;
            userRegCS.MappedObject__c = 'Contact';
            userRegCS.AlwaysMandatory__c = true;
            userRegCS.HiddenDisabled__c = true;
        userRegCSLst.add(userRegCS);
        
        CS_PRM_UserRegistration__c userRegCS1 = new CS_PRM_UserRegistration__c();
            userRegCS1.Name = 'firstName';
            userRegCS1.FieldLabel__c = 'Contact: First Name';
            userRegCS1.MappedField__c = 'PRMFirstName__c';
            userRegCS1.AppliedTo__c = 1;
            userRegCS1.FieldSortOrder__c = 2;
            userRegCS1.MappedObject__c = 'Contact';
            userRegCS1.AlwaysMandatory__c = true;
            userRegCS1.HiddenDisabled__c = true;
        userRegCSLst.add(userRegCS1);
        
        INSERT userRegCSLst;
        
        CSPRMEmailNotification__c emailConfigCS = new CSPRMEmailNotification__c();
        emailConfigCS.Name = 'EM8';
        emailConfigCS.Configurable__c = true;
        emailConfigCS.AppliedTo__c = 1;
        emailConfigCS.Default__c = true;
        emailConfigCS.Description__c = 'When the Account has been approved by the PRM Local Admin, this email is sent to notify this user that he is the PRM Primary Contact for his account';
        emailConfigCS.EmailTemplate__c = '/00X12000001mqB0EAI?setupid=CommunicationTemplatesEmail';
        emailConfigCS.DisplayOrder__c  = 7;
        emailConfigCS.EmailID__c       = 'P-EM-REG-Account Approved';
        INSERT emailConfigCS;
        
        
        
        
        
        
    }
    static TestMethod void CountryChannels_Records(){
        Country__c country= Utils_TestMethods.createCountry();
        insert country;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLC1 = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Financier1', Active__c = true, ParentClassificationLevel__c = cLC.Id);
        insert cLC1;
        CountryChannels__c cCh = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        insert cCh;
        CountryChannels__c cCh1 = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        test.startTest();
        try{
            insert cCh1;
        }
        Catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains(System.Label.CLAPR15PRM164) ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        test.stopTest();
        
    }
    static TestMethod void CountryChannelsUpdate_Records(){
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        Country__c country1 = new Country__c(Name='TestCountry1', CountryCode__c='TCO');
        insert country1;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLC1 = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Financier1', Active__c = true, ParentClassificationLevel__c = cLC.Id);
        insert cLC1;
        ClassificationLevelCatalog__c cLC2 = new ClassificationLevelCatalog__c(Name = 'FI2', ClassificationLevelName__c = 'Financier2', Active__c = true, ParentClassificationLevel__c = cLC.Id);
        insert cLC2;
        CountryChannels__c cCh = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        insert cCh;
        CountryChannels__c cCh1 = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC2.Id);
        insert cCh1;
        CountryChannels__c cCh3 = new CountryChannels__c(Active__c = True,Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        insert cCh3;
        
        test.startTest();
            CountryChannels__c insertedTestChannels = [Select Id,Channel__c From CountryChannels__c Where SubChannel__c = :cLC2.Id];
            insertedTestChannels.Country__c = country1.id;
            update insertedTestChannels;
            insertedTestChannels.SubChannel__c = cLC1.Id;
            insertedTestChannels.Country__c = country.id;
            
        
            try{
                update insertedTestChannels;
            }
            Catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains(System.Label.CLAPR15PRM164) ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
        
          CountryChannels__c insertedTestChannels1 = [Select Id,Channel__c From CountryChannels__c Where id = :cCh3.Id];
           insertedTestChannels1.SubChannel__c = cLC2.Id;
            update insertedTestChannels1;
            
        test.stopTest();
    }
    
    static TestMethod void CountryChannelsUpdate3(){
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        Country__c country1 = new Country__c(Name='TestCountry1', CountryCode__c='TCO');
        insert country1;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLC1 = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Financier1', Active__c = true, ParentClassificationLevel__c = cLC.Id);
        insert cLC1;
        ClassificationLevelCatalog__c cLC2 = new ClassificationLevelCatalog__c(Name = 'FI2', ClassificationLevelName__c = 'Financier2', Active__c = true, ParentClassificationLevel__c = cLC.Id);
        insert cLC2;
        CountryChannels__c cCh = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        insert cCh;
        CountryChannels__c cCh1 = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC2.Id);
        insert cCh1;
        CountryChannels__c cCh3 = new CountryChannels__c(Active__c = True,Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        insert cCh3;
        
        
        Account acc = new Account();
        acc.Name = 'test acc';
        insert acc;
                
        Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            con.PRMCompanyInfoSkippedAtReg__c = false;
            con.PRMCountry__c = country.Id;
            con.PRMCustomerClassificationLevel1__c = 'FI';
            con.PRMCustomerClassificationLevel2__c = 'FI1';
            con.PRMFirstName__c = 'PRM test contact Fn'; 
            con.PRMLastName__c  = 'PRM test contact Fn';
        insert con;
        
        PRMConfigureEmailNotifications__c prmConfigureEmailNot = new PRMConfigureEmailNotifications__c();
            prmConfigureEmailNot.IsNotificationEnable__c = true;
            prmConfigureEmailNot.CountryChannel__c = cCh.Id;
            prmConfigureEmailNot.EmailName__c = 'EmailName';
        INSERT prmConfigureEmailNot;
        test.startTest();
        
            AP_CountryChannels_Records.GetConfiguredEmailNotifications(new List<Contact>{con});
            AP_CountryChannels_Records.getFieldResults('email');
        
        test.stopTest();
    }
}