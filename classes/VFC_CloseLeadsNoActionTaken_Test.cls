/**
25-Apr-2013    Srinivas Nallapati  May13 Mkt Release    Initial Creation: test class for 
 */
@isTest
private class VFC_CloseLeadsNoActionTaken_Test{
      static testMethod void myUnitTest() 
      {
         
         
          List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
          if(profiles.size()>0)
          {
             User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
             user.LeadCleanup__c= true;
             Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
             Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();
             
             List<Lead> leads;
              Country__c country= Utils_TestMethods.createCountry();
                insert country;
                Account Acc = Utils_TestMethods.createAccount();
                insert Acc;
                MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                insert markCallBack;
                
                Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
                insert con1;
                
                leads = new List<Lead>();
                for(Integer i=0;i<20;i++)
                {
                    Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                   if(mapRecordType.get('Marcom Lead')!= null)    
                        lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                   if(Math.mod(i,2) ==0)
                       lead1.email = null;     
                   lead1.Contact__C = con1.id; 
                   lead1.Account__C = Acc.id;
                   leads.add(lead1);   
                }
                Database.SaveResult[] leadsInsert= Database.insert(leads, true);
                //insert leads;
                
             system.runas(user)
             {
                 
               
                ApexPages.StandardSetController controller = new ApexPages.StandardSetController([select name, status, Ownerid from lead limit 1]);
                controller.setSelected([select name, status, Ownerid from lead limit 10]);
                
                Pagereference pg1 = Page.VFP_CloseLeadsNoActionTaken;
                Test.setCurrentPage(pg1);
                ApexPages.CurrentPage().getparameters().put('retUrl','/00Q/o');
                VFC_CloseLeadsNoActionTaken CLController = new VFC_CloseLeadsNoActionTaken(controller);
                CLController.closeSelected();
                
                CLController.lstLeads = new List<Lead>();
                CLController.closeSelected();
                
             }
            ApexPages.StandardSetController controlleru = new ApexPages.StandardSetController([select name, status, Ownerid from lead limit 1]);
            controlleru.setSelected([select name, status, Ownerid from lead limit 10]);
            
            Pagereference pgu = Page.VFP_CloseLeadsNoActionTaken;
            Test.setCurrentPage(pgu);
            ApexPages.CurrentPage().getparameters().put('retUrl','/00Q/o');
            VFC_CloseLeadsNoActionTaken CLControlleru = new VFC_CloseLeadsNoActionTaken(controlleru);
         
             User user2 = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsgFC92');
             system.runas(user2)
             {
               
                ApexPages.StandardSetController controller2 = new ApexPages.StandardSetController([select name, status, Ownerid from lead limit 1]);
                controller2.setSelected(leads);
                
                Pagereference pg1 = Page.VFP_CloseLeadsNoActionTaken;
                Test.setCurrentPage(pg1);
                ApexPages.CurrentPage().getparameters().put('retUrl','/00Q/o');
                VFC_CloseLeadsNoActionTaken CLController2 = new VFC_CloseLeadsNoActionTaken(controller2);
                CLController2.closeSelected();
             }
             user2.LeadCleanup__c = true;
             system.runas(user2)
             {
               
                ApexPages.StandardSetController controller3 = new ApexPages.StandardSetController([select name, status, Ownerid from lead limit 1]);
                controller3.setSelected(leads);
                
                Pagereference pg1 = Page.VFP_CloseLeadsNoActionTaken;
                Test.setCurrentPage(pg1);
                ApexPages.CurrentPage().getparameters().put('retUrl','/00Q/o');
                VFC_CloseLeadsNoActionTaken CLController3 = new VFC_CloseLeadsNoActionTaken(controller3);
                CLController3.closeSelected();
             }
             
                
          }
      }

}//end of test class