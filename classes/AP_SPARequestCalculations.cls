/*
Author:Siddharth N
Purpose: helper classes for triggers of SPARequest__c 
*/
public without sharing class AP_SPARequestCalculations
{
    public static Map<Id,List<SPARequest__c>> oppidsparequestmap=null;
    public static Map<Id,List<SPARequest__c>> spidsparequestmap=null;
    public static Map<Id,List<SPARequest__c>> accidsparequestmap=null;
    public static Map<Id,List<SPARequest__c>> channelidsparequestmap=null;
    /*
        Method: updateRelatedQuota
        Purpose: get the list of approved SPARequest and update the TotalRemainingQuota__c and TotalUsedUpQuota__c
    */
        public static void updateRelatedQuota(Set<Id> quotaIds)
        {        

            List<SPARequest__c> requests=[select currencyIsoCode,NetDiscountAmount__c,SPAQuotaQuarter__c,id from SPARequest__c where SPAQuotaQuarter__c in :quotaIds and ApprovalStatus__c='Approved' and Opportunity__r.Status__c!='Lost'];     

            Map<Id,List<SPARequest__c>> quotarequestsmap=new Map<Id,List<SPARequest__c>>();
            for(SPARequest__c request:requests)
            {
                if(quotarequestsmap.containsKey(request.SPAQuotaQuarter__c))
                {
                    quotarequestsmap.get(request.SPAQuotaQuarter__c).add(request);
                }
                else
                    quotarequestsmap.put(request.SPAQuotaQuarter__c,new List<SPARequest__c>{request});
            }        

            List<SPAQuotaQuarter__c> quotaquarters=[select CurrencyIsoCode,Id,Amount__c,QuotaRemaining__c,QuotaUsedUp__c,Quarter__c from SPAQuotaQuarter__c where id in :quotaIds];
            for(SPAQuotaQuarter__c quotaquarter:quotaquarters)
            {                     
                Decimal quotagranted=Utils_Methods.convertCurrencyToEuro(quotaquarter.currencyIsoCode,quotaquarter.Amount__c);
                Decimal usedup=0.00;            
                if(quotarequestsmap.ContainsKey(quotaquarter.id)){
                    for(SPARequest__c request:quotarequestsmap.get(quotaquarter.id))
                    {
                        usedup+=Utils_Methods.convertCurrencyToEuro(request.currencyIsoCode,request.NetDiscountAmount__c);
                    }
                }
                quotaquarter.QuotaUsedUp__c=Utils_Methods.convertCurrencyFromEuro(quotaquarter.currencyIsoCode,usedup);         
            }
            Database.update(quotaquarters,false);        

        }


    /*
    updateRelatedQuotaforLostOpportunity
    */
    public static void updateRelatedQuotaforLostOpportunity(Set<Id> opportunityIds)
    {        

        List<SPARequest__c> requests=[select currencyIsoCode,NetDiscountAmount__c,SPAQuotaQuarter__c,id from SPARequest__c where Opportunity__c in :opportunityIds and ApprovalStatus__c='Approved'];     
        
        Map<Id,List<SPARequest__c>> quotarequestsmap=new Map<Id,List<SPARequest__c>>();
        for(SPARequest__c request:requests)
        {
            if(quotarequestsmap.containsKey(request.SPAQuotaQuarter__c))
            {
                quotarequestsmap.get(request.SPAQuotaQuarter__c).add(request);
            }
            else
                quotarequestsmap.put(request.SPAQuotaQuarter__c,new List<SPARequest__c>{request});
        }    

        List<SPAQuotaQuarter__c> quotaquarters=[select CurrencyIsoCode,Id,Amount__c,QuotaRemaining__c,QuotaUsedUp__c,Quarter__c from SPAQuotaQuarter__c where id in :quotarequestsmap.keySet()];
        for(SPAQuotaQuarter__c quotaquarter:quotaquarters)
        {                     
            Decimal quotausedup=Utils_Methods.convertCurrencyToEuro(quotaquarter.currencyIsoCode,quotaquarter.QuotaUsedUp__c);
//            Decimal usedup=0.00;            
            if(quotarequestsmap.ContainsKey(quotaquarter.id)){
                for(SPARequest__c request:quotarequestsmap.get(quotaquarter.id))
                {
                    quotausedup-=Utils_Methods.convertCurrencyToEuro(request.currencyIsoCode,request.NetDiscountAmount__c);
                }
            }
            quotaquarter.QuotaUsedUp__c=Utils_Methods.convertCurrencyFromEuro(quotaquarter.currencyIsoCode,quotausedup);         
        }
        Database.update(quotaquarters,false);        

    }

    /*
        Method: getQuarterNo
        Purpose: get the quarter no based on the 
    */
        private static String getQuarterNo(Date myDate)
        {
            if(myDate!=null){
                Integer month=myDate.month();
                if(month>=1 && month<=3)
                    return 'Q1';
                if(month>=4 && month<=6)
                    return 'Q2';
                if(month>=7 && month<=9)
                    return 'Q3';
                if(month>=10 && month<=12)
                    return 'Q4';
            }

            return '';
        }

    /*
        Method: linkQuotaRecord
        Purpose: based on the value of backofficesystemid and spasalesgroup find the relavent quota record and update the 
        sparequest record.
    */
        public static void linkQuotaRecord(List<SPARequest__c> requests)
        {
            System.debug('*********Method linkQuotaRecord Starts*********');

            List<Id> salesgroupids=new List<Id>();
            List<String> backofficelst=new List<String>();
            Map<String,List<SPARequest__c>> uniquestringmap=new Map<String,List<SPARequest__c>>();
            for(SPARequest__c request:requests)
            {
                salesgroupids.add(request.SPASalesGroup__c);
                backofficelst.add(request.BackOfficeSystemID__c);
                String myKey=request.BackOfficeSystemID__c+':'+request.SPASalesGroup__c;
                if(uniquestringmap.containsKey(myKey))
                {
                    uniquestringmap.get(myKey).add(request);
                }                     
                else
                {
                    List<SPARequest__c> templst=new List<SPARequest__c>();
                    templst.add(request);
                    uniquestringmap.put(myKey,templst);
                }
            }
            if(salesgroupids.size()>0)
            {        
                for(SPAQuota__c quota:[select BackOfficeSystemID__c,Id,SPASalesGroup__c,(select id,Quarter__c from SPAQuotaQuarters__r) from SPAQuota__c where Year__c=:(System.today().year()+'') and BackOfficeSystemID__c in :backofficelst and SPASalesGroup__c in :salesgroupids])
                {
                    System.debug('quota'+quota.id);
                    String myKey=quota.BackOfficeSystemID__c+':'+quota.SPASalesGroup__c;
                    if(uniquestringmap.containsKey(myKey))
                    {
                        for(SPARequest__c request:uniquestringmap.get(myKey))
                        {
                            for(SPAQuotaQuarter__c quotaQuarter: quota.SPAQuotaQuarters__r)
                            {
                                System.debug(getQuarterNo(request.startDate__c)+'-----------'+quotaQuarter.Quarter__c);
                                if(getQuarterNo(request.startDate__c)==quotaQuarter.Quarter__c)
                                    request.SPAQuotaQuarter__c=quotaQuarter.id;                        
                            }

                        }
                    }            
                } 
            }
            System.debug('*********Method linkQuotaRecord Ends*********');               
        }
    /*
    */
    public static void populateBackofficeAccno(List<SPARequest__c> requests)
    {   
        //generate the opportunity Map
        AP_SPARequestCalculations.generateSPARequestOpportunityMap(requests);        
        
        //if the account Id is blank.
        if(oppidsparequestmap.size()>0)
        {
            List<Opportunity> oppts=[select AccountId from Opportunity where id in :oppidsparequestmap.keySet()];                        
            Map<Id,Id> oppidaccountMap=new Map<Id,Id>();
            for(Opportunity oppt:oppts)        
                oppidaccountMap.put(oppt.id,oppt.AccountId);        
            for(SPARequest__c sparequest:requests)
            {
                if(sparequest.Account__c==null && oppidaccountMap.containsKey(sparequest.Opportunity__c))           
                    sparequest.Account__c=oppidaccountMap.get(sparequest.Opportunity__c);
            }
        }
        //generate the account Map
        AP_SPARequestCalculations.generateSPARequestAccountMap(requests);
        if(accidsparequestmap.size()>0){        
            for(Account acct:[select id,(select LegacyName__c,LegacyNumber__c,Account__c from Accounts_source_Legacy_ID__r) from Account where id in :accidsparequestmap.keySet()]){        
                for(SPARequest__c sparequest:accidsparequestmap.get(acct.id))
                {
                    if(acct.Accounts_source_Legacy_ID__r.size()==1)
                    {
                        if(acct.Accounts_source_Legacy_ID__r[0].LegacyName__c==sparequest.BackOfficeSystemId__c)
                            sparequest.BackOfficeCustomerNumber__c=acct.Accounts_source_Legacy_ID__r[0].LegacyNumber__c; 
                        // For October 2014 release added below else condition to throw error if no record found
                        else if(sparequest.BackOfficeCustomerNumber__c==null || sparequest.BackOfficeCustomerNumber__c=='')
                            sparequest.addError(System.Label.CLSEP12SLS22);                           
                    }
                    else if(acct.Accounts_source_Legacy_ID__r.size()>1)
                    {
                        Integer iNumberOfLegacyAccounts = 0;
                        string strBackOfficeNumber = '';
                        for(LegacyAccount__c eachLegacyAccount:acct.Accounts_source_Legacy_ID__r)
                        {
                            If (eachLegacyAccount.LegacyName__c == sparequest.BackOfficeSystemID__c)
                            {
                                iNumberOfLegacyAccounts += 1;
                                strBackOfficeNumber = eachLegacyAccount.LegacyNumber__c;
                            }
                        }
                        if (iNumberOfLegacyAccounts == 1)
                        {
                            sparequest.BackOfficeCustomerNumber__c=strBackOfficeNumber;
                        }
                        else
                        {
                            sparequest.addError(System.Label.CLSEP12SLS21);
                        }
                    }
                    
                    else if(acct.Accounts_source_Legacy_ID__r.size()==0 && (sparequest.BackOfficeCustomerNumber__c==null || sparequest.BackOfficeCustomerNumber__c==''))
                        sparequest.addError(System.Label.CLSEP12SLS22);
                }            
            }
        }
    }

    public static void populateChannelLegacyNumber(List<SPARequest__c> requests)
    {
        //generate the channel Map
        AP_SPARequestCalculations.generateSPARequestChannelMap(requests);
        if(channelidsparequestmap.size()>0){        
            for(Account acct:[select id,(select LegacyName__c,LegacyNumber__c,Account__c from Accounts_source_Legacy_ID__r) from Account where id in :channelidsparequestmap.keySet()]){        
                for(SPARequest__c sparequest:channelidsparequestmap.get(acct.id))
                {
                    if(acct.Accounts_source_Legacy_ID__r.size()==1)
                    {
                        if(acct.Accounts_source_Legacy_ID__r[0].LegacyName__c==sparequest.BackOfficeSystemId__c)
                            sparequest.ChannelLegacyNumber__c=acct.Accounts_source_Legacy_ID__r[0].LegacyNumber__c; 
                        // For October 2014 release added below else condition to throw error if no record found
                         else if(sparequest.ChannelLegacyNumber__c==null || sparequest.ChannelLegacyNumber__c=='')
                            sparequest.addError(System.Label.CLJUN13SLS03);                           
                    }
                    else if(acct.Accounts_source_Legacy_ID__r.size()>1)
                    {
                        Integer iNumberOfLegacyAccounts = 0;
                        string strBackOfficeNumber = '';
                        for(LegacyAccount__c eachLegacyAccount:acct.Accounts_source_Legacy_ID__r)
                        {
                            If (eachLegacyAccount.LegacyName__c == sparequest.BackOfficeSystemID__c)
                            {
                                iNumberOfLegacyAccounts += 1;
                                strBackOfficeNumber = eachLegacyAccount.LegacyNumber__c;
                            }
                        }
                        if (iNumberOfLegacyAccounts == 1)
                        {
                            sparequest.ChannelLegacyNumber__c=strBackOfficeNumber;
                        } 
                        else
                        {
                            sparequest.addError(System.Label.CLJUN13SLS02);                  
                        }
                    }
                    else if(acct.Accounts_source_Legacy_ID__r.size()==0 && (sparequest.ChannelLegacyNumber__c==null || sparequest.ChannelLegacyNumber__c==''))
                        sparequest.addError(System.Label.CLJUN13SLS03);              
                }            
            }
        }
    }
    
    /*
    Method:populateSalesGroup
    Purpose:create a map of account,spa requests,
    */
    public static void populateSalesGroup(List<SPARequest__c> requests)
    {    
        //a map is not really required here but just maintaining it as a convention.lets hope they introduce something complex here.     
        AP_SPARequestCalculations.generateSPARequestSalesPersonMap(requests);              
        if(spidsparequestmap.size()>0){          
            List<UserParameter__c> userparams=[select id,SPASalesGroup__c,User__c from UserParameter__c where User__c in :spidsparequestmap.keySet() and SPASalesGroup__c!=null];
            Map<Id,Id> usersalesgroupmap=new Map<Id,Id>();
            for(UserParameter__c userparam:userparams)        
                usersalesgroupmap.put(userparam.User__c,userparam.SPASalesGroup__c);
            
            for(SPARequest__c spareq:requests){        
                if(usersalesgroupmap.containsKey(spareq.SalesPerson__c))
                    spareq.SPASalesGroup__c=usersalesgroupmap.get(spareq.SalesPerson__c);        
                else
                    spareq.addError(System.Label.CLSEP12SLS23);
            }        
        }
    }
    
    /*
   
    */    
    private static void generateSPARequestAccountMap(List<SPARequest__c> requests)
    {
        if(accidsparequestmap==null)
        {
            accidsparequestmap=new Map<Id,List<SPARequest__c>>();
            for(SPARequest__c request:requests)
            {
                System.debug('>>>>>>>'+request.BackOfficeCustomerNumber__c);
              // For October 2014 release added if condition for HK_SAP to autopopulate BackOfficeaccountnumber if it is null
               if(((request.BackOfficeCustomerNumber__c==null || request.BackOfficeCustomerNumber__c=='') && request.BackOfficeSystemID__c=='HK_SAP')||request.BackOfficeSystemID__c=='NL_SAP'){
                    if(request.Account__c!=null)
                    {
                        if(accidsparequestmap.containsKey(request.Account__c))
                            accidsparequestmap.get(request.Account__c).add(request);
                        else
                            accidsparequestmap.put(request.Account__c,new List<SPARequest__c>{request});
                    }
               }
            }    
        }
    }                    


    private static void generateSPARequestChannelMap(List<SPARequest__c> requests)
    {
        if(channelidsparequestmap==null){
            channelidsparequestmap=new Map<Id,List<SPARequest__c>>();
            for(SPARequest__c request:requests)
            {
                if(request.Channel__c!=null)
                {
                    if(channelidsparequestmap.containsKey(request.Channel__c))
                        channelidsparequestmap.get(request.Channel__c).add(request);
                    else
                        channelidsparequestmap.put(request.Channel__c,new List<SPARequest__c>{request});
                }
            }    
        }
    }                    


    private static void generateSPARequestOpportunityMap(List<SPARequest__c> requests)
    {
        if(oppidsparequestmap==null){
            oppidsparequestmap=new Map<Id,List<SPARequest__c>>();        
            for(SPARequest__c request:requests)
            {
                if(request.Opportunity__c!=null){
                    if(oppidsparequestmap.containsKey(request.Opportunity__c))
                        oppidsparequestmap.get(request.Opportunity__c).add(request);
                    else
                        oppidsparequestmap.put(request.Opportunity__c,new List<SPARequest__c>{request});
                }
            }
        }
    }  


    private static void generateSPARequestSalesPersonMap(List<SPARequest__c> requests)
    {
     if(spidsparequestmap==null){
        spidsparequestmap=new Map<Id,List<SPARequest__c>>();          
        for(SPARequest__c request:requests)
        {
            if(request.SalesPerson__c!=null){            
                if(spidsparequestmap.containsKey(request.SalesPerson__c))
                    spidsparequestmap.get(request.SalesPerson__c).add(request);
                else                                    
                    spidsparequestmap.put(request.SalesPerson__c,new List<SPARequest__c>{request});                
            }
        }
    }
}

    /*
    Method: addSalesPersonToShare
    Purpose: Add the salesperson to share of spa request
    */
    public static void addSalesPersonToShare(Map<Id,Id> requestsalespersonmap)
    {        
        List<SPARequest__Share> sharelist=new List<SPARequest__Share>();

        for(Id spareqid : requestsalespersonmap.keySet())
        {         
            SPARequest__Share tempshare=new SPARequest__Share();
            tempshare.AccessLevel='Edit';
            tempshare.ParentId=spareqid;
            tempshare.UserOrGroupId=requestsalespersonmap.get(spareqid);
            tempshare.RowCause=Schema.SPARequest__Share.RowCause.SharedWithOwnerAndApprover__c;
            sharelist.add(tempshare);
        }
        if(sharelist.size()>0)        
          Database.insert(sharelist,false);

  }
  
    
    /* Modified for BR-4953 */
    //Populates the current approver of SPA Request
    public static void populateCurrentApprover(List<SPARequest__c> spaReqs) 
    {
        for(SPARequest__c spaReq: spaReqs)
        {
            if(spaReq.TECH_ApprovalStep__c==1)
                spaReq.REP_CurrentApprover__c = spaReq.Approver1__c;
            else if(spaReq.TECH_ApprovalStep__c==2)
                spaReq.REP_CurrentApprover__c = spaReq.Approver2__c;
            else if(spaReq.TECH_ApprovalStep__c==3)
                spaReq.REP_CurrentApprover__c = spaReq.Approver3__c;
            else
                spaReq.REP_CurrentApprover__c = null;
        }
    }
    /* End of Modification for BR-4953 */
    
     /* Added for BR-4968 */
    //checks for the presence of the records in the Threshold Discount table for the SPA Requested Type
    public static void checkForThresholdDiscount(List<SPARequest__c> spaReqs) 
    {
        Integer thresholdDiscountRecCount;
        for(SPARequest__c spaReq: spaReqs)
        {
            thresholdDiscountRecCount=0;
            System.debug('####SPA Requested type>>'+spaReq.RequestedType__c);
            if(spaReq.RequestedType__c!=System.Label.CLOCT14SLS13)
                thresholdDiscountRecCount=[select count() from SPAThresholdDiscount__c where BackOfficeSystemID__c =:spaReq.BackOfficeSystemID__c and (Type__c=:System.Label.CLSEP12SLS40 or Type__c=:System.Label.CLSEP12SLS41)];
            else if(spaReq.RequestedType__c==System.Label.CLOCT14SLS13)
                thresholdDiscountRecCount=[select count() from SPAThresholdDiscount__c where BackOfficeSystemID__c=:spaReq.BackOfficeSystemID__c and Type__c=:System.Label.CLOCT14SLS14];
            if(thresholdDiscountRecCount==0)
               spaReq.addError(System.Label.CLOCT14SLS24);       
        }
    }
    
    //checks for the presence of the records in the Threshold Amount table for the SPA Requested Type
    public static void checkForThresholdAmount(List<SPARequest__c> spaReqs) 
    {
        Integer thresholdAmountRecCount;
        for(SPARequest__c spaReq: spaReqs)
        {
            thresholdAmountRecCount=0;
            System.debug('####SPA Requested type>>'+spaReq.RequestedType__c);
            if(spaReq.RequestedType__c!=System.Label.CLOCT14SLS11)
                thresholdAmountRecCount=[select count() from SPAThresholdAmount__c where BackOfficeSystemID__c =:spaReq.BackOfficeSystemID__c and Type__c=:System.Label.CLOCT14SLS12];
            else if(spaReq.RequestedType__c==System.Label.CLOCT14SLS11)
                thresholdAmountRecCount=[select count() from SPAThresholdAmount__c where BackOfficeSystemID__c=:spaReq.BackOfficeSystemID__c and Type__c=:System.Label.CLOCT14SLS11];
            if(thresholdAmountRecCount==0)
               spaReq.addError(System.Label.CLOCT14SLS25);    
        }
    }
    /* End of BR-4968 */
}