//public static final *type*  *ERROR/CONFIRM/WARNING/INFO* + *OPERATION_RESULT_NAME* 
public with sharing class SyncObjectsConstants {

	/* Used for SyncObjects.cls */
    public static final Integer LIMIT_ROWS_OPERATIONS = 1000;
	
	
	/* Used for SyncObjectsResult.cls*/
	public static final String ERROR_ROWS_LIMIT_EXCEDED = 'Too many rows. Run with batch please'; //when the rows exceeds the limit to run. To solve this, run SynbObjects with batch
	public static final String ERROR_INCORRECT_MAPPING = 'There was one or more incorrect mappings. Please fix the SyncObject record';
	public static final String ERROR_EMPTY_MAPPING = 'Empty Mapping';
	public static final String CONFIRM_SUCCESSFULLY = 'Operation successfully';
	public static final String CONFIRM_READY = 'Config OK';

}