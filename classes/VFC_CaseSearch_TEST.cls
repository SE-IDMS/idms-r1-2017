/*
Author          : Vimal Karunakaran Global Delivery Team - Bangalore
Date Created    : 
Description     : Test class for VFC_CaseSearch class.
*/
@isTest 
private class VFC_CaseSearch_TEST {
    static testMethod void testCaseSearchMethod() { 
        // creating Test Data
		String strCaseNumber;
        Account  objAccount = Utils_TestMethods.createAccount(userInfo.getUserId()); 
        insert objAccount;
        Contact   objContact = Utils_TestMethods.createContact(objAccount.id,'TestContact');
        insert objContact;
        Case objCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        insert objCase;
		List<Case> lstCase = new List<Case>([Select CaseNumber from Case where Id =:objCase.Id]);
		if(lstCase.size()==1){
			strCaseNumber =lstCase[0].CaseNumber;
		}
        
		PageReference pageRef = Page.VFP_CaseSearch;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('CaseNum', '3424234');
        	
        VFC_CaseSearch controller = new VFC_CaseSearch();
        
        controller.searchCaseNumber();
		controller.returnPage();
		ApexPages.currentPage().getParameters().put('errorURL', '/home/home.jsp');
		controller.returnPage();
		ApexPages.currentPage().getParameters().put('CaseNum', strCaseNumber);
		controller.searchCaseNumber();
    }   
}