/*
    test class for "AP_COLA_Announcement" class and trigger "AP_COLA_Announcement" on Announcement Object
*/

@istest

private class AP_COLA_Announcement_Test{
    static testmethod void myunittest(){
    Test.startTest();
    
         Offer_Lifecycle__c olc= new Offer_Lifecycle__c(); 
            olc.Offer_Name__c='Test Schneider';
            olc.Description__c ='Test Schneider Description';
            olc.Substitution_Strategy__c ='Test Schneider Substitution Strategy';
         Insert olc; 
         
         Announcement__c annc = new Announcement__c();
             annc.Name = 'Test Annc';
             annc.Offer_Lifecycle__c = olc.id;
             annc.OfferDescription__c = '';
             annc.SubstitutionStrategy__c = '';
         insert annc;
         
         Announcement__c annc1 = new Announcement__c();
             annc1.Name = 'Test Annc1';
             annc1.Offer_Lifecycle__c = olc.id;
             annc1.OfferDescription__c = '';
             annc1.SubstitutionStrategy__c = '';
         insert annc1;
         
         List<Announcement__c> ListAnnct = new List<Announcement__c>{annc,annc1};
         AP_COLA_Announcement colaAnnc = new AP_COLA_Announcement();
         //colaAnnc.UpdateOfferDescription(ListAnnct);
         
         
     Test.stopTest();         
    }
}