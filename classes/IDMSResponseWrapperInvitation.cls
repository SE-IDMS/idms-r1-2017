//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class is handling the invitation Response from external system
//
//*******************************************************************************************

global with sharing class IDMSResponseWrapperInvitation {
    public String Message;  
    public String Status; 
    
    //Constructor per default
    public IDMSResponseWrapperInvitation(){}
    
    //Constructor of invitation response
    public IDMSResponseWrapperInvitation(string Status,string Message){
        this.Status     = Status;
        this.Message    = Message;
    }
    
    
}