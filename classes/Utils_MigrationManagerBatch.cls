global class Utils_MigrationManagerBatch implements Database.Batchable<sObject>
{
/*
******************************************************
public String query;
public String field;
public String field1;
public String objectName;
public String debugs='';
public Map<String,String> oldNewMap=new Map<String,String>();
******************************************************
*/

global Database.querylocator start(Database.BatchableContext BC)
{
        List<Account> templist= new List<Account>();
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        templist.add(acc);
        String query='select id from account where id =\''+acc.id+'\'';
    return Database.getQueryLocator(query);
}

global Utils_MigrationManagerBatch()
{
}
/*
global Utils_MigrationManagerBatch(String q,String f,String o,Map<String,String> ons,String f1)
{
        string temptotest1='';
query=q;
field=f;
objectName=o;
oldNewMap=ons;
field1=f1;

//special case with , 
oldNewMap.put('MCCBs, Earth Leak&Swit','PWCCB');
oldNewMap.put('PWCCB - MCCBs, Earth Leak&Swit','PWCCB');
oldNewMap.put('PWCCB--MCCBs, Earth Leakage and Switches','PWCCB');
oldNewMap.put('MCCBs, Earth Leakage and Switches','PWCCB');
}         
*/



global void execute(Database.BatchableContext BC, List<sObject> scope)
{    
        string temptotest='';
/*
******************************************************            
Set<sObject> updateThis=new Set<sObject>();  
if(field1=='' || field1==null)
{
   for(sObject s : scope){
   if((s.get(field)+'')!=''){
      String fieldValue=s.get(field)+'';           
      debugs+='Field Value : '+fieldValue+'\n';
       if(!fieldValue.contains(';')){
       //this is not a multiselect           
           if(oldNewMap.containsKey(fieldValue)){
               String newValue=oldNewMap.get(fieldValue);
               s.put(field,newValue);
               System.debug('#batchEdits#'+field+'-----'+newValue+'-------');
          //     updateThis.add(s);
           }
       }
       else{    
       //multi select picklist
       String multiSelectString=s.get(field)+'';
       debugs+='\n'+multiSelectString+'\n';
       String newmultiSelectString='';
       List<String> picklistValues=multiSelectString.split(';');
       if(picklistValues.size()>0)
       {
           for(String ss:picklistValues)
           {
               if(oldNewMap.containsKey(ss)){
                   newmultiSelectString+=oldNewMap.get(ss)+';';
               }
               else{
                   newmultiSelectString+=ss+';';
               }
           }
       }
       newmultiSelectString.subString(0,newmultiSelectString.length()-1);
       s.put(field,newmultiSelectString);
       System.debug('#batchEdits#'+field+'-----'+newmultiSelectString+'-------'); 
  //     updateThis.add(s);           
       }
      } 
    }
}
else
{
   for(sObject s : scope){
   if((s.get(field)+'')!=''){
      String fieldValue=s.get(field)+'';
      String fieldValue1='';  
      if(s.get(field1)!=null)         
      fieldValue1=s.get(field1)+'';
      if(!fieldValue.contains(';')){
       //this is not a multiselect           
           if(oldNewMap.containsKey(fieldValue+':'+fieldValue1)){
               String newValue=oldNewMap.get(fieldValue+':'+fieldValue1);
               s.put(field,newValue.split(':')[0]);
               if(newValue.split(':').size()==2)
               s.put(field1,newValue.split(':')[1]);
               else
               s.put(field1,'');
            //   updateThis.add(s);
               System.debug('#batchEdits#'+field+'-----'+newValue+'-------');
           }
       }       
      } 
    }
}

update scope;
******************************************************        
*/
}

global void finish(Database.BatchableContext BC)
{
        string temptotest3='';
/*
******************************************************    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setToAddresses(new String[] {'siddharatha.nagavarapu@schneider-electric.com'});
    mail.setReplyTo('siddharatha.nagavarapu@schneider-electric.com');
    mail.setSenderDisplayName('Batch Processing');
    mail.setSubject('Batch Process Completed'+objectName);
    mail.setPlainTextBody('Batch Process has completed for  : '+objectName+'\n and the query\n'+query+'\n and the debug is \n'+debugs);

    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
******************************************************   
*/ 
}
}