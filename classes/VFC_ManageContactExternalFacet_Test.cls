@isTest
public class VFC_ManageContactExternalFacet_Test{
    @testSetup static void testSetupMethodPRM() {
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
        for(Profile p : ps)
        {
             profiles.put(p.name, p.id);
        }        
                  
        user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];
        admin.BypassVR__c =True;
        
        Test.startTest();
        ID  profilesId;
        system.runas(admin){
        Country__c TestCountry = new Country__c();
            TestCountry.Name   = 'India';
            TestCountry.CountryCode__c = 'IN';
            TestCountry.InternationalPhoneCode__c = '91';
            TestCountry.Region__c = 'APAC';
            INSERT TestCountry;
            
            StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            INSERT testStateProvince;
            
            Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', 
                                    Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMCountry__c=TestCountry.id,PRMBusinessType__c='FI',
                                    PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, StateProvince__c = testStateProvince.id,City__c = 'Bangalore');
            insert PartnerAcc;
          
            Contact PartnerCon = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  Country__c=TestCountry.Id,
                  PRMFirstName__c = 'Test',
                  PRMLastName__c = 'lastname',
                  PRMEmail__c = 'test.lastName@yopmail.com',
                  PRMWorkPhone__c = '999121212',
                  PRMMobilePhone__c = '999121212',
                  PRMPrimaryContact__c = true,
                  PRMContactRegistrationStatus__c = 'Validated',
                  PRMContact__c = true,
                  PRMUIMSID__c = '897dheuu37hsysh3sh3',
                  City__c = 'Bangalore'
                  );
                  
                  Insert PartnerCon;
            profilesId = [select id from profile where name='SE - Channel Partner (Community)'].Id; 
                
                  
            User   u1= new User(Username = 'testUserOne@schneider-electric.com'+Label.CLMAR13PRM05, LastName = 'User11', alias = 'tuser1',
                              CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                              Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                              PRMTemporarySamlToken__c = 'ASDHFTWEEXD121212', isActive = true,
                              LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId,ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true, FederationIdentifier = '897dheuu37hsysh3sh3');        
            insert u1;  

            PermissionSet fieloPermissionSet = [Select ID,UserLicenseId from PermissionSet where Name='FieloPRM_SE' Limit 1];
            PermissionSetAssignment newPermissionSetAssignment1 = new PermissionSetAssignment();
            newPermissionSetAssignment1.AssigneeId = u1.id;
            newPermissionSetAssignment1.PermissionSetId = fieloPermissionSet.id;
            Insert newPermissionSetAssignment1; 
            PartnerAcc.Owner = u1;
        }
    }
    static testMethod void  ManageContactExternalFacet_Test(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);

        System.runAs(u){
            PageReference pageRef = Page.VFP_ManageContactExternalFacet;
            Test.setCurrentPage(pageRef);
            Contact cont = new Contact();
            cont = [select Id,PRMContact__c,AccountId,PRMUIMSID__c,PRMWorkPhone__c from Contact Where FirstName = 'Test'];
            Apexpages.Standardcontroller stdController = new Apexpages.Standardcontroller(cont);
            ApexPages.currentPage().getParameters().put('Id',cont.id);
            VFC_ManageContactExternalFacet thisManageconExt = New VFC_ManageContactExternalFacet(stdController);
            Test.startTest();
            thisManageconExt.con.FirstName = 'Test1';
            thisManageconExt.editPage();
            thisManageconExt.detailPage();
            thisManageconExt.goToManConExtFacetPage();
            thisManageconExt.MoveToDetailPage();
            thisManageconExt.goToResetPasswordPage();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            thisManageconExt.ResetPassword();
            Test.stopTest();
        }
    }
    static testMethod void  ManageContactExternalFacet_Test1(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);

        System.runAs(u){
            PageReference pageRef = Page.VFP_ManageContactExternalFacet;
            Test.setCurrentPage(pageRef);
            Contact cont = new Contact();
            cont = [select Id,PRMContact__c,AccountId,PRMUIMSID__c,PRMWorkPhone__c,PRMMobilePhone__c from Contact Where FirstName = 'Test'];
            Apexpages.Standardcontroller stdController = new Apexpages.Standardcontroller(cont);
            ApexPages.currentPage().getParameters().put('Id',cont.id);
            VFC_ManageContactExternalFacet thisManageconExt = New VFC_ManageContactExternalFacet(stdController);
            Test.startTest();
            thisManageconExt.con.PRMWorkPhone__c = null;
            thisManageconExt.con.FirstName = 'Test1';
            thisManageconExt.con.PRMUIMSID__c = null;
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            thisManageconExt.ResetPassword();
            Test.stopTest();
        }
    }
    static testMethod void  ManageContactExternalFacet_Test2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);

        System.runAs(u){
            PageReference pageRef = Page.VFP_ManageContactExternalFacet;
            Test.setCurrentPage(pageRef);
            Contact cont = new Contact();
            cont = [select Id,PRMContact__c,AccountId,PRMUIMSID__c,PRMWorkPhone__c from Contact Where FirstName = 'Test'];
            Apexpages.Standardcontroller stdController = new Apexpages.Standardcontroller(cont);
            ApexPages.currentPage().getParameters().put('Id',cont.id);
            VFC_ManageContactExternalFacet thisManageconExt = New VFC_ManageContactExternalFacet(stdController);
            Test.startTest();
            thisManageconExt.con.FirstName = 'Test1';
            thisManageconExt.IMSApplicationID = System.Label.CLAPR15PRM022;
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            thisManageconExt.updateContactInfoInUIMS();
            Test.stopTest();
        }
    }
}