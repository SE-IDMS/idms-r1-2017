/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016 
*   This class generates random signature token and update the user.
*   randomly generated token is valid for 7 days.
**/

public class IDMSEmailEncryption {
    
    //Returns part of the URL to be sent in the email
    public Static String EmailEncryption(User usr){
        usr.IDMSToken__c                    = generateRandomString(20);
        usr.IDMSSignatureGenerationDate__c  = system.today();
        String signature                    = usr.IDMSToken__c;
        update usr;
        system.debug('signature: '+ signature);
        return signature;
    }
    
    //Returns part of the URL to be sent in the email
    public Static Boolean EmailDecryption(String Signature, String userId){
        List<User> users = [SELECT Id,IDMSToken__c,IDMSSignatureGenerationDate__c from User where Id = :userId limit 1];
        if(users.size() > 0){
            User usr = users[0];
            if(usr.IDMSSignatureGenerationDate__c != null && system.today() <= (usr.IDMSSignatureGenerationDate__c).addDays(7)){
                if (usr.IDMSToken__c == Signature){
                    usr.IDMSToken__c                    = '';
                    usr.IDMSSignatureGenerationDate__c  = null;
                    update usr;                
                    return TRUE;
                } else{
                    return FALSE;
                }
            }
        }
        return false;
    }
    //generate random signature token and return it.
    public static String generateRandomString(Integer len) {
        final String chars = Label.CLDEC16IDMS004;
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    } 
}