/*
    Author          : Karthik Shivprakash (ACN) 
    Date Created    : 04/02/2011
    Description     : This class is used to calculate the business hours difference on change of each status.
*/


public class AP07_CaseStatusAge{


/* @Method <This method updateCaseStatus is used to calculate the case age when case status
            changes from one to another.>
   @param <Taking Trigger.oldMap and Trigger.newMap as parameters>
   @return <void> - <Method not returning anything>
   @throws exception - <Method is not throwing any exception>
*/ 
    public static void updateCaseStatus(Map<Id,Case> OldCaseStatusMap, Map<Id,Case> NewCaseStatusMap){
        
        //Here we are using this variable to store the created date for the particular status.
        DateTime StartTime;
        //Using this variable to store the hour difference between the each status.
        Long BusinessTimeDiff;
        Double BusinessHourDiff,BusinessTimeDiffDouble,RoundUpHourDiff;
        
        //Creating the map for the status field, to handle the translation.
        /*Map<String,String> StatusMap = new Map<String,String>();
        Schema.DescribeFieldResult pickListValues = Schema.sObjectType.Case.fields.status;
        for(schema.Picklistentry nextPickVal: pickListValues.getPicklistValues()){
            StatusMap.put(nextPickVal.getValue(), nextPickVal.getLabel());
        }*/
        
        System.debug('Exact Business Hour calcuation starts here');
        
        /*BusinessHours businessHour = [select id from businesshours where IsDefault=true];
        
        if(businessHour!=null){*/
            for(case caseObj: OldCaseStatusMap.values()){
                /*StartTime = caseObj.CreatedDate;
                System.debug('Last status change value'+caseObj.LastStatusChangeDate__c);
                System.debug('System Now'+System.now());
                if(caseObj.LastStatusChangeDate__c!=null){
                    BusinessTimeDiff = BusinessHours.diff(businessHour.id, caseObj.LastStatusChangeDate__c, System.now());
                    BusinessTimeDiffDouble = BusinessTimeDiff;
                    System.debug('Business Hours when Last status change is not null'+BusinessTimeDiffDouble);
                    if(BusinessTimeDiff<3600000){
                        BusinessHourDiff=BusinessTimeDiffDouble/60000;
                        BusinessHourDiff=BusinessHourDiff/100;
                        System.debug('Inside if for min'+BusinessHourDiff);
                    }
                    else{
                        BusinessHourDiff = BusinessTimeDiffDouble/3600000;
                        RoundUpHourDiff = BusinessHourDiff;
                        BusinessHourDiff=roundingMinutes(RoundUpHourDiff,BusinessHourDiff);
                        System.debug('Inside else for hrs if last status is not null'+BusinessHourDiff);
                    }
                }
                else{
                    BusinessTimeDiff = BusinessHours.diff(businessHour.id, startTime, System.now());
                    System.debug('Business Hours when Last status change is null'+BusinessTimeDiff);
                    BusinessTimeDiffDouble = BusinessTimeDiff;
                    if(BusinessTimeDiffDouble<3600000){
                        BusinessHourDiff=BusinessTimeDiffDouble/60000;
                        BusinessHourDiff=BusinessHourDiff/100;
                    }
                    else{
                        BusinessHourDiff = BusinessTimeDiffDouble/3600000;
                        RoundUpHourDiff = BusinessHourDiff;
                        BusinessHourDiff=roundingMinutes(RoundUpHourDiff,BusinessHourDiff);
                        System.debug('Inside else for hrs'+BusinessHourDiff);
                    }
                }*/
                //System.debug('Status Map'+StatusMap);
                if(NewCaseStatusMap.containsKey(caseObj.Id)){ //HotFix for EUS# 022070, Checking the new Map for IDs, Issue not related to May 2013 Relelase
                    if(caseObj.Status=='Open'){
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAgeinHrsSatThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAgeSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAgeinHrsSunThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAgeSunThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                    }
                    else if(caseObj.Status=='Open/ Waiting For Details - Customer'){
                        System.debug('Before assigning'+BusinessHourDiff);
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAwaitingDetailsCustomerAge__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAwaitingDetailsCustomerAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAwaitingDtlsCstmrAgeSatThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAwaitingDtlsCustomerAgeSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAwaitingDtlsCstmrAgeSunThrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAwaitingDtlsCustomerAgeSunThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                    }
                    else if(caseObj.Status=='Open/ Waiting For Details - Internal'){
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAwaitingDetailsInternalAge__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAwaitingDetailsInternalAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAwaitingDtlsIntrnalAgeSatThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAwaitingDtlsInternalAgeSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAwaitingDtlsIntrnlAgeSunThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAwaitingDtlsInternalAgeSunThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                    }
                    else if(caseObj.Status=='Closed/ Pending Action'){
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                        NewCaseStatusMap.get(caseObj.Id).TECH_ClosedPendingActionAgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_ClosedPendingActionAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_ClosedPndngAcnAgeinHrsSatThrs__c=OldCaseStatusMap.get(caseObj.Id).REP_ClosedPendingActionAgeSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_ClosedPndngAcnAgeinHrsSunThrs__c=OldCaseStatusMap.get(caseObj.Id).REP_ClosedPendingActionAgeSunThrs__c;
                    }
                    else if(caseObj.Status=='Open/ Level 1 Escalation'){
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenLevel1EscalationAgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenLevel1EscalationAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenPrSprtCntrEsclnAgeinHrsSunThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenPrSptCtrEsclnAgeSunThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenPrSprtCntrEsclnAgeinHrsSatThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenPrSptCtrEsclnAgeSatThrs__c;
                    }
                    else if(caseObj.Status=='Open/ Level 2 Escalation'){
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenLevel2EscalationAgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenLevel2EscalationAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAdvSptCtrEsclnAgeinHrsSatThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAdvSptCtrEsclnAgeSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAdvSptCtrEsclnAgeinHrsSunThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAdvSptCtrEsclnAgeSunThrs__c;
                    }
                    else if(caseObj.Status=='Open/ Level 3 Escalation'){
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenLevel3EscalationAgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenLevel3EscalationAgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenExpSptCtrEsclnAgeinHrsSunThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenExpSptCtrEsclnAgeSunThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenExpSptCtrEsclnAgeinHrsSatThurs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenExpSptCtrEsclnAgeSatThrs__c;
                    }
                    else if(caseObj.Status=='Open/ Answer Available To L1'){
                        System.debug('While assigning'+BusinessHourDiff);
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAnswerAvailabletoL1AgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAnswerAvailabletoL1AgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAnsAvlblPrSptCtrAgeinHrsSatThrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAnsAvlbtoPrSptCtrSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAnsAvlblPrSptCtrAgeinHrsSunThrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAnsAvlbtoPrSptCtrSunThrs__c;
                    }
                    else if(caseObj.Status=='Open/ Answer Available To L2'){
                        NewCaseStatusMap.get(caseObj.Id).LastStatusChangeDate__c=System.now();
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAnswerAvailabletoL2AgeinHrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAnswerAvailabletoL2AgeinHrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAnsAvlblAdvSptCtrAgeHrsSatThrs__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAnsAvlbtoAdvSptCtrAgeSatThrs__c;
                        NewCaseStatusMap.get(caseObj.Id).TECH_OpenAnsAvlblAdvSptCtrAgeinHrsSunThr__c=OldCaseStatusMap.get(caseObj.Id).REP_OpenAnsAvlbtoAdvSptCtrAgeSunThrs__c;
                    }
                }
                
            }
       // }
        System.debug('Exact Business Hour calcuation Ends here');
    }
    
    /*public static Double roundingMinutes(Double RoundUpHourDiff,Double BusinessHoursDiff){
        Double BusinessHourExactDiff,RemainingMinutes;
        if(RoundUpHourDiff.intValue()!=BusinessHoursDiff){
            RemainingMinutes = BusinessHoursDiff - RoundUpHourDiff.intValue();
            System.debug('Difference between the minutes'+RemainingMinutes);
            if(RemainingMinutes>=0.60){
                BusinessHourExactDiff = (RemainingMinutes-0.60)+1+RoundUpHourDiff.intValue();
                System.debug('Inner If'+BusinessHourExactDiff);
            }
            else{
                if(BusinessHoursDiff!=null){
                    BusinessHourExactDiff=BusinessHoursDiff;
                }
            }
        }
        System.debug('Inside rounding Minutes'+BusinessHourExactDiff);
        return BusinessHourExactDiff;
    }*/
}