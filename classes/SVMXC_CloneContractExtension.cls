public class SVMXC_CloneContractExtension {

    SVMXC__Service_Contract__c pageContract;

    public String ContractIdPath {get; set;}
    public String successMsg {get; set;}
    public Boolean dealerTransaction {get; set;}
    public SVMXC__Service_Contract__c getpageContract() { return this.pageContract;}
    public void setpageContract (SVMXC__Service_Contract__c cont) {this.pageContract = cont;}
    public ApexPages.standardController controller {get; set;}
    
    private final SVMXC__Service_Contract__c c;
        
    public SVMXC_CloneContractExtension(ApexPages.StandardController stdcontroller) {
        controller = stdcontroller;
        this.c = (SVMXC__Service_Contract__c)controller.getRecord();
    }
            
    public PageReference returnToContract() {
        
        PageReference pageRef1 = new PageReference('/' + c.Id);
        pageRef1.setRedirect(true);
        return pageRef1;
    }
    
    public PageReference init() {
        
        // todo add rollback ability?
        SVMXC__Service_Contract__c c = [SELECT Id, SVMXC__Renewed_From__c, SVMXC__Start_Date__c, SVMXC__End_Date__c
                                         FROM SVMXC__Service_Contract__c WHERE Id =: c.Id LIMIT 1];
        
        if (c.SVMXC__Renewed_From__c == null) {
            
            throwExceptionMsg('This contract was not renewed from another contract ', 'warning');
            return null;    
        }
            
        try {
            
            // first clone service lines, but exclude ones that already exist under contract based on SKU           
            List <SVMXC__Service_Contract__c> existingLines = [SELECT SKUComRef__c FROM SVMXC__Service_Contract__c 
                                                                WHERE ParentContract__c =:c.Id];
                                                                
            Map<Id, Id> existingSKUs = new Map<Id, Id>();
            if (!existingLines.isEmpty()) {
                for (SVMXC__Service_Contract__c sc : existingLines) {
                    if (!existingSKUs.containsKey(sc.SKUComRef__c))
                        existingSKUs.put(sc.SKUComRef__c, sc.Id);
                }
            }
                        
            List <SVMXC__Service_Contract__c> linesToClone = [SELECT ofVisists__c, ValidationDate__c, Tech_Status__c, 
                Tech_StartDate__c, Tech_RenewalOpportunityCreat__c, Tech_EndDate__c, Tech_ContractNotification__c, 
                Tech_ContractCancel__c, TECH_UnitPrice__c, TECH_TopLevelActiveContract__c, TECH_SendCustomerRenewalNotification__c, 
                TECH_RenewalOpptyDate__c, TECH_OutboundEvent__c, TECH_NotificationDate__c, TECH_LastModifiedByBOuser__c, 
                TECH_IsBoundToBackOffice__c, TECH_CreatedByBOuser__c, TECH_ContactEmail__c, TECH_BOLastUpdateTimeStamp__c, 
                Synchronization_log__c, SupportAmount__c, Status__c, SoldtoAccount__c, SoldToContact__c, ServiceContractCounter__c, 
                SendWelcomePackage__c, SendCustomerRenewalNotification__c, SalesAreaData__c, SVMXC__Zone__c, 
                SVMXC__Weeks_To_Renewal__c, SVMXC__Travel_Rounding_Type__c, SVMXC__Start_Date__c, SVMXC__Service_Pricebook__c, 
                SVMXC__Service_Plan__c, SVMXC__Service_Level__c, SVMXC__Service_Contract_Notes__c, SVMXC__Select__c, 
                SVMXC__Sales_Rep__c, SVMXC__SESSION_ID__c, SVMXC__Round_Travel_To_Nearest__c, SVMXC__Round_Labor_To_Nearest__c, 
                SVMXC__Renewed_From__c, SVMXC__Renewal_Number__c, SVMXC__Renewal_Notes__c, SVMXC__Renewal_Date__c, 
                SVMXC__Primary_Technician__c, SVMXC__Minimum_Travel__c, SVMXC__Minimum_Labor__c, SVMXC__Labor_Rounding_Type__c, 
                SVMXC__Exchange_Type__c, SVMXC__EndpointURL__c, SVMXC__End_Date__c, SVMXC__Discounted_Price__c, 
                SVMXC__Discounted_Price2__c, SVMXC__Discount__c, SVMXC__Default_Travel_Unit__c, SVMXC__Default_Travel_Price__c, 
                SVMXC__Default_Service_Group__c, SVMXC__Default_Parts_Price_Book__c, SVMXC__Default_Group_Member__c, 
                SVMXC__Contract_Price__c, SVMXC__Contract_Price2__c, SVMXC__Contact__c, SVMXC__Company__c, SVMXC__Canceled_On__c, 
                SVMXC__Canceled_By__c, SVMXC__Cancelation_Notes__c, SVMXC__Business_Hours__c, SVMXC__Billing_Schedule__c, 
                SVMXC__All_Sites_Covered__c, SVMXC__All_Services_Covered__c, SVMXC__All_Products_Covered__c, 
                SVMXC__All_Contacts_Covered__c, SVMXC__Active__c, SVMXC__Activation_Notes__c, SKUComRef__c, SKUComRefDescription__c, 
                Requestcancellationby__c, RenewalOpportunityTriggered__c, RecordTypeId, ReasonForCancellation__c, Quantity__c, 
                PartsDiscount__c, Part_SLA_terms__c, ParentInstalledAtContact__c, ParentInstalledAtAccount__c, 
                ParentContract__c, PONumber__c, PODate__c, OwnerExpirationNotice__c, Opportunity_Name2__c, Opportunity_Lines__c, 
                OpportunityType__c, OpportunitySource__c, OpportunityScope__c, Next_Step__c, Nb_of_years__c, MayEdit, 
                MarketSubSegment__c, MarketSegment__c, Majoramendmentopportunity__c, MaininstalledatContact__c, LeadingBusinessBU__c, 
                LatestRenewalOpportunity__c, LatestOpportunity__c, LatestOpportunityNotif__c, Last_Synchronization__c,
                LabourDiscount__c, Jobonsitetriggerinvoice__c, IsLocked, IsEvergreen__c, Initial_start_date__c, InitialPrice__c, 
                InitialOpportunity__c, Final_Account_Site__c, Entitlement__c, DefaultInstalledAtAccount__c, 
                DONOTPRINT__c, DONOTDELIVER__c, CurrencyIsoCode, Credits__c, CountryofBackOffice__c, Count_Coverage__c,
                ContractType__c, ContractClassification__c, ContactInformation__c, Channel_Type__c, ChannelType__c, 
                Cancellation_Date__c, BusinessMix__c, BillingPlan__c, BillingOption__c, BillToContact__c, BillToAccount__c, 
                Back_Office_Contract_Ref_Num__c, BackOfficeSystem__c, BackOfficeReference__c, BOContractType__c, Amended_From__c, 
                Address__c, ActiveServiceLine__c, Account_Type__c FROM SVMXC__Service_Contract__c 
                WHERE ParentContract__c =: c.SVMXC__Renewed_From__c];
            
            if (!linesToClone.isEmpty()) {
            
                Set<Id> clonedFromServiceLineIds = new Set<Id>();
                // clone Service Lines
                List<SVMXC__Service_Contract__c> newServiceLines = new List<SVMXC__Service_Contract__c>();
                    
                    for (SVMXC__Service_Contract__c sl : linesToClone) {
                        
                        // need to create only lines that don't currently exist
                        if (!existingSKUs.containsKey(sl.SKUComRef__c)) {
                            SVMXC__Service_Contract__c clonedServiceLines = sl.clone(false, false, false, false);
                            clonedServiceLines.ParentContract__c = c.Id;
                            clonedServiceLines.cloned_from_Service_Line__c = sl.Id;
                            clonedServiceLines.SVMXC__Start_Date__c = c.SVMXC__Start_Date__c;
                            clonedServiceLines.SVMXC__End_Date__c = c.SVMXC__End_Date__c;
                            newServiceLines.add(clonedServiceLines);
                            
                            clonedFromServiceLineIds.add(sl.Id);
                        } 
                    }
                    
                insert newServiceLines;
                
                // need to build Map of new Service Lines lookup to old Lines to further build the IB, PM and Contract Services             
                Map<Id, Id> oldNewServiceLineMap = new Map<Id, Id>();
                
                for (SVMXC__Service_Contract__c sc2 : newServiceLines) {
                    oldNewServiceLineMap.put(sc2.cloned_from_Service_Line__c, sc2.Id);  
                }
            
                // clone services
                List<SVMXC__Service_Contract_Services__c> serviceLines = [SELECT SystemModstamp, StartDate__c, ShipTo__c, Service_Plan__c, 
                    SVMXC__Travel_Discount_Covered__c, SVMXC__Service__c, SVMXC__Service_Type__c, SVMXC__Service_Contract__c,
                    SVMXC__Parts_Discount_Not_Covered__c, SVMXC__Parts_Discount_Covered__c, SVMXC__Notes__c, SVMXC__Named_Expression__c, 
                    SVMXC__Line_Price__c, SVMXC__Labor_Discount_Not_Covered__c, SVMXC__Labor_Discount_Covered__c, SVMXC__Is_Billable__c,
                    SVMXC__Included_Units__c, SVMXC__Expense_Discount_Not_Covered__c, SVMXC__Expense_Discount_Covered__c,
                    SVMXC__Consumed_Units__c, ReasonForRejection__c, Quantity__c, NetValue__c, Name, MayEdit, Material__c, 
                    LastActivityDate, ItemNumber__c, ItemCategory__c, IsLocked, SVMXC__Travel_Discount_Not_Covered__c,
                    EndDate__c, CurrencyIsoCode, CountryofBackOffice__c, ContractName__c, BackOfficeSystem__c, 
                    BackOfficeReference__c, SVMXC__Service_Contract__r.SKUComRef__c
                    FROM SVMXC__Service_Contract_Services__c WHERE SVMXC__Service_Contract__c IN : clonedFromServiceLineIds];
                
                List<SVMXC__Service_Contract_Services__c> newServices = new List<SVMXC__Service_Contract_Services__c>();
                    
                if (!serviceLines.isEmpty()) {
                    
                    for (SVMXC__Service_Contract_Services__c sc : serviceLines) {
                        SVMXC__Service_Contract_Services__c clonedLines = sc.clone(false, false, false, false);
                        
                        clonedLines.Cloned_from_Service_Feature__c = sc.Id;
                        
                        if (oldNewServiceLineMap.containsKey(sc.SVMXC__Service_Contract__c))
                            clonedLines.SVMXC__Service_Contract__c = oldNewServiceLineMap.get(sc.SVMXC__Service_Contract__c);
                        else 
                            clonedLines.SVMXC__Service_Contract__c = existingSKUs.get(sc.SVMXC__Service_Contract__r.SKUComRef__c);
                        
                        newServices.add(clonedLines);
                    }
                    
                    insert newServices;
                }           
            
                // need to build Map of new Service lookup to old Lines to further build the IB record IncludedService lookup       
                Map<Id, Id> oldNewServicesMap = new Map<Id, Id>();
                
                for (SVMXC__Service_Contract_Services__c scs : newServices) {
                    oldNewServiceLineMap.put(scs.Cloned_from_Service_Feature__c, scs.Id);   
                }
            
                // clone Related Products records
                List <SVMXC__Service_Contract_Products__c> relatedProducts = [SELECT installedAtZipCode__c, installedAtStreet__c,  
                    installedAtStateProvinceCode__c, installedAtNameLocal__c, installedAtLocalCity__c, 
                    installedAtCountryCode__c, installedAtCity__c, installedAtAddAddressInfo__c, installedAtStreetLocal__c,
                    installedAtAddAddressInfoLocal__c, Tech_SetOfId__c, Tech_CreatedThroughSFM__c, 
                    TECH_isAddressSynchedWithAccount_c__c, SystemModstamp, Serial_Number__c, Serial_LotNumber__c, 
                    Schedule_PM_visit__c, SVMXC__Start_Date__c, SVMXC__Service_Contract__c, SVMXC__SLA_Terms__c, 
                    SVMXC__Product__c, SVMXC__Product_Line__c, SVMXC__Product_Family__c, SVMXC__Number_Of_PMs_Delivered__c, 
                    SVMXC__Notes__c, SVMXC__Line_Price__c, SVMXC__Is_Billable__c, SVMXC__Installed_Product__c, 
                    SVMXC__Exchange_Type__c, SVMXC__End_Date__c, ProductSKU__c, Name, MayEdit, IsLocked, IsDeleted, 
                    InstalledProductLocation__c, InstalledAtName__c, InstalledAtGoldenID__c, InstalledAtAccount__c, 
                    InstallAt__c, IncludedService__c, IncludedServiceFeatureName__c, Id, Golden_Asset_Id__c, CurrencyIsoCode, 
                    City__c, BackOfficeReference__c, AssetLocation__c, SVMXC__Service_Contract__r.SKUComRef__c
                    FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Service_Contract__c IN : clonedFromServiceLineIds];
                    
                if (!relatedProducts.isEmpty()) {
                    
                    List<SVMXC__Service_Contract_Products__c> newRelatedProducts = new List<SVMXC__Service_Contract_Products__c>();
                    
                    for (SVMXC__Service_Contract_Products__c rp : relatedProducts) {
                        SVMXC__Service_Contract_Products__c clonedRelatedProducts = rp.clone(false, false, false, false);
                        
                        clonedRelatedProducts.IncludedService__c = oldNewServiceLineMap.get(rp.IncludedService__c);
                        
                        if (oldNewServiceLineMap.containsKey(rp.SVMXC__Service_Contract__c))
                            clonedRelatedProducts.SVMXC__Service_Contract__c = oldNewServiceLineMap.get(rp.SVMXC__Service_Contract__c);
                        else 
                            clonedRelatedProducts.SVMXC__Service_Contract__c = existingSKUs.get(rp.SVMXC__Service_Contract__r.SKUComRef__c);
                        
                        clonedRelatedProducts.SVMXC__Start_Date__c = c.SVMXC__Start_Date__c;
                        clonedRelatedProducts.SVMXC__End_Date__c = c.SVMXC__End_Date__c;
                            
                        newRelatedProducts.add(clonedRelatedProducts);
                    }
                    
                    insert newRelatedProducts;
                }
                
                System.debug('##### checking for PM Plans');
                
                // clone PM Plans
                List<SVMXC__PM_Plan__c> pmPlans = [SELECT WOMapping__c, Tech_UniquePMplan__c, SystemModstamp, ScheduledPMVisits__c, 
                    SVMXC__Work_Order_Date__c, SVMXC__Work_Order_Assign_To__c, SVMXC__Work_Order_Assign_To_User__c, 
                    SVMXC__Work_Order_Assign_To_Queue__c, SVMXC__Time_and_Counters__c, SVMXC__Task_Template__c, 
                    SVMXC__Success_Email_ID__c,SVMXC__Status__c, SVMXC__Start_Date__c, SVMXC__Service_Contract__c, 
                    SVMXC__Select__c, SVMXC__Schedule_Type__c, SVMXC__SLA_Terms__c, SVMXC__Processed__c, 
                    SVMXC__PM_Plan_Template__c, SVMXC__Number_of_Work_orders__c, SVMXC__Number_of_Failures__c, 
                    SVMXC__Number_of_Cases__c, SVMXC__Next_PM_Date__c, SVMXC__Location__c, SVMXC__Last_PM_Date__c, 
                    SVMXC__Frequency__c, SVMXC__Frequency_Unit__c, SVMXC__Field_map_for_wo__c, SVMXC__Field_map_for_Case__c, 
                    SVMXC__Error_Email_ID__c, SVMXC__End_Date__c, SVMXC__Dispatch_Process__c, SVMXC__Description__c, 
                    SVMXC__Create_Work_Order__c, SVMXC__Create_Parts_Requests__c, SVMXC__Create_Case__c, SVMXC__Coverage_Type__c, 
                    SVMXC__Case_Assign_To__c, SVMXC__Case_Assign_To_User__c, SVMXC__Case_Assign_To_Queue__c, 
                    SVMXC__Canceled_On__c, SVMXC__Canceled_By__c, SVMXC__Cancelation_Notes__c, SVMXC__Activity_On_Success__c, 
                    SVMXC__Activity_On_Error__c, SVMXC__Activation_Notes__c, SVMXC__Account__c, OwnerId, Name, MayEdit, 
                    IsLocked, IsDeleted, Id, CurrencyIsoCode,
                    SVMXC__Service_Contract__r.SKUComRef__c
                    FROM SVMXC__PM_Plan__c WHERE SVMXC__Service_Contract__c IN : clonedFromServiceLineIds];
                    
                if (!pmPlans.isEmpty()) {
                    
                    List<SVMXC__PM_Plan__c> newPMs = new List<SVMXC__PM_Plan__c>();
                    
                    for (SVMXC__PM_Plan__c pm : pmPlans) {
                        SVMXC__PM_Plan__c clonedPM = pm.clone(false, false, false, false);
                        
                        if (oldNewServiceLineMap.containsKey(pm.SVMXC__Service_Contract__c))
                            clonedPM.SVMXC__Service_Contract__c = oldNewServiceLineMap.get(pm.SVMXC__Service_Contract__c);
                        else 
                            clonedPM.SVMXC__Service_Contract__c = existingSKUs.get(pm.SVMXC__Service_Contract__r.SKUComRef__c);
                        
                        newPMs.add(clonedPM);
                    }
                    
                    insert newPMs;
                }   
            }
            
            PageReference contractPage = new ApexPages.StandardController(c).view();
            contractPage.setRedirect(true);
            return contractPage;
            
        } catch (Exception e) {
            throwExceptionMsg('An unhandled exception occured creating a clone of the Contract ' + e, 'error');
        }
        
        return null;

    }
    
    public void throwExceptionMsg(String message, String severity) {
            
        if (severity.equalsIgnoreCase('warning')) {     
            System.debug('Throwing this warning message : ' + message); 
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.WARNING, message, message);
            ApexPages.addmessage(msg);
        } else { //assume error
            System.debug('Throwing this error message : ' + message);   
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.ERROR, message, message);
            ApexPages.addmessage(msg);
        }   
    }
}