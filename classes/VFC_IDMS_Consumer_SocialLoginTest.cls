/*
* Test for VFC_IDMS_Consumer_SocialLogin
* */
@isTest
private class VFC_IDMS_Consumer_SocialLoginTest{
    //Test1 : Create a page reference parameter with some data
    static testmethod void testUserLogin(){
        PageReference pRef = Page.UserLogin;
        pRef.getParameters().put('startURL', 'app=&');
        pRef.getParameters().put('app', '');
        Test.setCurrentPage(pRef);   
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
    }
    //Test2 : Create a page reference parameter with missing app value
    static testmethod void testUserLoginApp(){
        PageReference pRef = Page.UserLogin;
        pRef.getParameters().put('startURL', 'app=');
        pRef.getParameters().put('app', '');
        Test.setCurrentPage(pRef);   
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
    }
    //Test3 : Create a page reference parameter with blank
    static testmethod void testUserLogin1() {
        PageReference pRef1 = Page.UserLogin;
        pRef1.getParameters().put('startURL', '');
        pRef1.getParameters().put('app', 'Heroku');
        Test.setCurrentPage(pRef1);   
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
    }
    //Test4 : Creating page reference parameter with null and intialize variable null value
    static testmethod void testUserLogin2(){
        PageReference pRef2 = Page.UserLogin;
        pRef2.getParameters().put('startURL', '');
        pRef2.getParameters().put('app', 'Heroku');
        Test.setCurrentPage(pRef2);   
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        social.context = '' ;
        social.appName = '' ;
        social.url = '' ;
        
    }
    //Test4 : Creating page reference parameter with app value and intialize variable with null value
    static testmethod void testUserLogin5(){
        PageReference pRef3 = Page.UserLogin;
        pRef3.getParameters().put('startURL', 'app=test');
        pRef3.getParameters().put('app', 'Heroku');
        Test.setCurrentPage(pRef3);   
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        social.context = '' ;
        social.appName = '' ;
        social.url = '' ;
        
    }
    //Test5 : Creating custom setting value and assigning to parameter value
    static testmethod void testUserLogin003(){
       // IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS',AppName__c='testIDMS');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS1',AppName__c='testIDMS1',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
       // insert apidapname; 
        PageReference pRef2 = Page.UserLogin;
        pRef2.getParameters().put('startURL', 'app=testIDMS');
        pRef2.getParameters().put('app',null);
        Test.setCurrentPage(pRef2);   
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        social.context = '' ;
        social.appName = '' ;
        social.url = '' ;
        
    }
    //Test6 : Creating user record and assigning to parameter
    static testmethod void testUserLogin3(){
        
        PageReference pRef2 = Page.UserLogin;
        pRef2.getParameters().put('startURL',null);
        pRef2.getParameters().put('app',null);
        Test.setCurrentPage(pRef2);  
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        String ussuff=Label.CLJUN16IDMS71;
        User UserObjEmail = new User(alias = 'user', email='user90' + '@accenture.com', isActive=true,
                                     emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                     localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                     BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                     timezonesidkey='Europe/London', username='user90@accenture.com'+ussuff,Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                     IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                     IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IsIDMSUser__c = true,
                                     IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert UserObjEmail;
        system.setpassword(UserObjEmail.id,'Welcome#123');
        social.username='user90@accenture.com';
        social.password='Welcome#123';
        
    }
    // Test7 : Creating user record and assingning respective value 
    static testmethod void userloginTest() {
        String ussuff=Label.CLJUN16IDMS71;
        User UserObjEmail = new User(alias = 'userpass', email='user9' + '@accenture.com', isActive=false,
                                     emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                     localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                     BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                     timezonesidkey='Europe/London', username='user9@accenture.com' +ussuff,Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                     IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                     IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IsIDMSUser__c = true,
                                     IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert UserObjEmail;
        // system.setpassword(UserObjEmail.id,'Welcome@123');
        PageReference pRef = Page.UserLogin;
        pRef.getParameters().put('startURL', 'app=&');
        pRef.getParameters().put('app', '');
        Test.setCurrentPage(pRef);   
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        social.username = 'user9@bridge-fo.com';
        social.password = 'Welcome#123';
        social.UserLogin();
    }
    // Test8 : Validate Password SSHA
    static testmethod void validatePasswordSSHATest(){
        VFC_IDMS_Consumer_SocialLogin.validatePasswordSSHA('Welcome1','{SSHA}vdvcpqCZks5GB5gf9Y8lYsxg/1YtYjxEDHiY8g==');
    }
    //Test9 : Validate UIMS Password
    static testmethod void CheckPasswordUIMSTest(){
        VFC_IDMS_Consumer_SocialLogin.CheckPasswordUIMS('Welcome01','{SHA}zfWdtFHfJmTfZDzyznPVMdQEvCg=');
    }
    //Test10 : Calling reset password for user
    static testmethod void resetPasswordCreateUserTest(){
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        social.resetPasswordCreateUser('test@test.com','Heroku','idms') ;
    }
    //Test11 : Redirect Test to fb 
    static testmethod void redirectTestfb() {
        //IDMSCookieAppName__c cookie=new IDMSCookieAppName__c(Name='testIDMS123',AppName__c='testIDMS123');
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            10,   
            true        
        )});
        ApexPages.currentPage().getParameters().put('startURL','?test123&test1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_fb() ;
        }
        catch(Exception e){}
    }
    //Test12 : Redirect Test to fb 
    static testmethod void redirectTestfb00() {
       // IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS123',AppName__c='testIDMS123');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS123',AppName__c='testIDMS123',context__c='work',
                                                                                       AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
       // insert apidapname;
        //IDMSCookieAppName__c cookie=new IDMSCookieAppName__c(Name='testIDMS123',AppName__c='testIDMS123');
        //insert cookie;
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            10,   
            true        
        )});
        ApexPages.currentPage().getParameters().put('startURL','?testIDMS1443');
        ApexPages.currentPage().getParameters().put('app',null);
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_fb() ;
        }
        catch(Exception e){}
    }
    //Test13 : Redirect Test to fb 
    static testmethod void redirectTestfb2() {
       // IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS1',AppName__c='testIDMS1');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS1',AppName__c='testIDMS1',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
       // insert apidapname; 
        ApexPages.currentPage().getParameters().put('app','testIDMS1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_fb() ;
        }
        catch(Exception e){}
    }
    //Test14 : Redirect Test to google
    static testmethod void redirectTestGoogle() {
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            10,   
            true        
        )});
        ApexPages.currentPage().getParameters().put('startURL','?test123&test1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_Google() ;
        }
        catch(Exception e){}
    }
    //Test15 : Redirect Test to google
    static testmethod void redirectTestGoogle2() {
      //  IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS1',AppName__c='testIDMS1');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS1',AppName__c='testIDMS1',context__c='work',
                                                                                       AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',AppDescriptionFooter1__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
     //   insert apidapname;
        ApexPages.currentPage().getParameters().put('app','testIDMS1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_Google() ;
        }
        catch(Exception e){}
    }
    //Test16 : Redirect Test to linked in
    static testmethod void redirectTestLinkedIn() {
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            10,   
            true        
        )});
        ApexPages.currentPage().getParameters().put('startURL','?test123&test1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_LinkedIn() ;
        }
        catch(Exception e){}
    }
    //Test17 : Redirect Test to linked
    static testmethod void redirectTestLinkedIn2() {
       // IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS1',AppName__c='testIDMS1');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS1',AppName__c='testIDMS1',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',AppDescriptionFooter1__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
      //  insert apidapname;
        ApexPages.currentPage().getParameters().put('app','testIDMS');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_LinkedIn() ;
        }
        catch(Exception e){}
    }
    //Test18 : Redirect Test to twitter
    static testmethod void redirectTestTwitter() {
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            10,   
            true        
        )});
        ApexPages.currentPage().getParameters().put('startURL','?test123&test1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_Twitter() ;
        }
        catch(Exception e){}
    }
    //Test19 : Redirect Test to ping
    static testmethod void redirectTestPing(){
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            10,   
            true        
        )});
        ApexPages.currentPage().getParameters().put('startURL','?test123&test1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c();
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_ping() ;
        }
        catch(Exception e){}
    }
    //Test20 : Redirect Test to twitter
    static testmethod void redirectTestTwitter2() {
      //  IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS1',AppName__c='testIDMS1');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS1',AppName__c='testIDMS1',context__c='work',
                                                                                       AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',AppDescriptionFooter1__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
     //   insert apidapname;
        ApexPages.currentPage().getParameters().put('app','testIDMS1');
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c() ;
        cust_obj.name=  'IdmsKey';
        cust_obj.AppValue__c = 'dummy value';
        insert cust_obj ;
        
        VFC_IDMS_Consumer_SocialLogin social = new VFC_IDMS_Consumer_SocialLogin();
        try{
            social.redirect_Twitter() ;
        }
        catch(Exception e){}
    }
    //Test21 : Redirect Test to autologin
    static testmethod void autoLoginTest() {
        setcustomsetting();
        
        
        VFC_IDMS_Consumer_SocialLogin consumerSocialLogin=new VFC_IDMS_Consumer_SocialLogin();
        ApexPages.currentPage().getParameters().put('app','testIDMS1');
        ApexPages.currentPage().getParameters().put('IDToken1','123@gmail.com');
        ApexPages.currentPage().getParameters().put('IDToken2','Welcome1');
        ApexPages.currentPage().getParameters().put('startURL','?app=test123&test1');
        
        consumerSocialLogin.autoLogin();
    }
    //Test22 : Constructor for VFC_IDMS_Consumer_SocialLogin 
    static testmethod void constructorTest1() {
        setcustomsetting();
        
        ApexPages.currentPage().getParameters().put('startURL','?app=testIDMS1');
        VFC_IDMS_Consumer_SocialLogin consumerSocialLogin=new VFC_IDMS_Consumer_SocialLogin();
        
    }
    //Test23 : Constructor for VFC_IDMS_Consumer_SocialLogin and calling setcustomsetting
    static testmethod void constructorTest2() {
        
        setcustomsetting();
        
        ApexPages.currentPage().getParameters().put('app','testIDMS1&test1');
        
        ApexPages.currentPage().getParameters().put('startURL','?app=testIDMS1&test1');
        VFC_IDMS_Consumer_SocialLogin consumerSocialLogin=new VFC_IDMS_Consumer_SocialLogin();
        
    }
    //Test24 : Constructor for VFC_IDMS_Consumer_SocialLogin class and assign value to variable
    static testmethod void constructorTest3() {
        setcustomsetting();
        String rId = [select id from UserRole where  name = 'CEO' Limit 1].Id;
        String RecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'Consumer Account'].Id;
        Account personAcc = new Account(FirstName='test1231_19Sep16',LastName='Account', ZipCode__c='12345',RecordTypeId = RecordTypeId,personemail = 'Scenario2Exception@accenture.com');
        insert personAcc;
        
        String contId = [select PersonContactId FROM account where ID =: personAcc.Id].PersonContactId;
        system.debug('*******'+contId+' *******'+personAcc.PersonContactId);
        
        User userObj2 = new User(alias = 'user', email='test1231_19Sep16' + '@cognizant.com',
                                 emailencodingkey='UTF-8', lastname='Testlast1', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = '00e12000000Ruoi', BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='tej23416',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = 'Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432',ContactId=contId,IsIDMSUser__c=true,
                                 CommunityNickname='test 123',Username='test1231_19Sep16@cognizant.com'+Label.CLJUN16IDMS71);
        
        insert userObj2;
        
        UIMS_User__c uimswrk = new UIMS_User__c(email__c='test12311_19Sep16@cognizant.com',Cell__c='testcell',FirstName__c='test1',LastName__c='test2',
                                                CountryCode__c='US',LanguageCode__c='LT',Phone__c='3929439',JobTitle__c='Testtitle',
                                                JobFunction__c='testing',JobDescription__c='testdesc',Street__c='some',
                                                AddInfoAddress__c='testadd',LocalityName__c='mylocal',PostalCode__c='2358483',
                                                State__c='CA', User__c=userObj2.Id,County__c='IN',PostOfficeBox__c='boxtest',
                                                MiddleName__c='midtest',Salutation__c='Z003',Fax__c='3928483',FederatedID__c='85839212',
                                                CompanyId__c='84930',PrimaryContact__c='testprim',GoldenId__c='59492',AddInfoAddressECS__c='testecs',
                                                CountyECS__c='tstsce', GivenNameECS__c='testname',LocalityNameECS__c='localname',
                                                SnECS__c='testsn',StreetECS__c='tstst',Vnew__c='testnew',updateSource__c='UIMS',Password__c='{SHA}zfWdtFHfJmTfZDzyznPVMdQEvCg=');
        insert uimswrk ;
        
        
        
        
        
        ApexPages.currentPage().getParameters().put('app','testIDMS1');
        
        ApexPages.currentPage().getParameters().put('startURL','?app=testIDMS1&test1');
        VFC_IDMS_Consumer_SocialLogin consumerSocialLogin=new VFC_IDMS_Consumer_SocialLogin();
        consumerSocialLogin.username='test1231_19Sep16';
        consumerSocialLogin.password='Welcome01';
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='test1231_19Sep16@cognizant.com';
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='test12311_19Sep16@cognizant.com';
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='+test1231_19Sep16@cognizant.com';
        
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='test1231_19Sep16@cognizant.com'+Label.CLJUN16IDMS71;
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='1234567890';
        consumerSocialLogin.UserLogin();
        ApexPages.currentPage().getParameters().put('app','test1234');
        IDMSApplicationMapping__c iDMSApplicationMapping1= new IDMSApplicationMapping__c(Name='test1234',AppName__c='Idms1',context__c='@test',
                                                                                         AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping1;
        consumerSocialLogin.username='test1231_19Sep16@cognizant.com';
        consumerSocialLogin.UserLogin();
    }
    
    
    //Test25 : Constructor for VFC_IDMS_Consumer_SocialLogin class and assign value to parameter
    static testmethod void constructorTest001() {
        setcustomsetting();
        String rId = [select id from UserRole where  name = 'CEO' Limit 1].Id;
        String RecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'Consumer Account'].Id;
        Account personAcc = new Account(FirstName='test1231_19Sep16',LastName='Account', ZipCode__c='12345',RecordTypeId = RecordTypeId,personemail = 'Scenario2Exception@accenture.com');
        insert personAcc;
        
        String contId = [select PersonContactId FROM account where ID =: personAcc.Id].PersonContactId;
        system.debug('*******'+contId+' *******'+personAcc.PersonContactId);
        
        User userObj2 = new User(alias = 'user', email='test12311_19Sep16' + '@cognizant.com',
                                 emailencodingkey='UTF-8', lastname='Testlast1', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = '00e12000000Ruoi', BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='t1ej23416',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = 'Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432',ContactId=contId,IsIDMSUser__c=true,
                                 CommunityNickname='test 123',Username='test12311_19Sep16@cognizant.com'+Label.CLJUN16IDMS71);
        
        insert userObj2;
        
        UIMS_User__c uimswrk = new UIMS_User__c(email__c='test123111_19Sep16@cognizant.com',Cell__c='testcell',FirstName__c='test1',LastName__c='test2',
                                                CountryCode__c='US',LanguageCode__c='LT',Phone__c='3929439',JobTitle__c='Testtitle',
                                                JobFunction__c='testing',JobDescription__c='testdesc',Street__c='some',
                                                AddInfoAddress__c='testadd',LocalityName__c='mylocal',PostalCode__c='2358483',
                                                State__c='CA', User__c=userObj2.Id,County__c='IN',PostOfficeBox__c='boxtest',
                                                MiddleName__c='midtest',Salutation__c='Z003',Fax__c='3928483',FederatedID__c='815839212',
                                                CompanyId__c='84930',PrimaryContact__c='testprim',GoldenId__c='59492',AddInfoAddressECS__c='testecs',
                                                CountyECS__c='tstsce', GivenNameECS__c='testname',LocalityNameECS__c='localname',
                                                SnECS__c='testsn',StreetECS__c='tstst',Vnew__c='testnew',updateSource__c='UIMS',Password__c='{SHA}zfWdtFHfJmTfZDzyznPVMdQEvCg=');
        insert uimswrk ;
        
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='test123',AppName__c='Idms1',context__c='@work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        System.assert(iDMSApplicationMapping!=null);
        
        
        
        ApexPages.currentPage().getParameters().put('startURL','?app=test123&test111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111');
        VFC_IDMS_Consumer_SocialLogin consumerSocialLogin=new VFC_IDMS_Consumer_SocialLogin();
        consumerSocialLogin.username='test1231_19Sep16';
        consumerSocialLogin.password='Welcome01';
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='test12311_19Sep16@cognizant.com';
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='test123111_19Sep16@cognizant.com';
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='+test12311_19Sep16@cognizant.com';
        
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='test12311_19Sep16@cognizant.com'+Label.CLJUN16IDMS71;
        consumerSocialLogin.UserLogin();
        consumerSocialLogin.username='1234567890';
        consumerSocialLogin.UserLogin();
        
    }
    //Test26 : Login Test
    static testmethod void logintest001() {
       // IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS12',AppName__c='testIDMS1');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS12',AppName__c='testIDMS1',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',AppDescriptionFooter1__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
        //insert apidapname;
        System.assert(iDMSApplicationMapping!=null);
     //   System.assert(apidapname!=null);
        VFC_IDMS_Consumer_SocialLogin login=new VFC_IDMS_Consumer_SocialLogin();
        login.username='testuser001@test.com';
        login.username='Welcome01';
        login.UserLogin();
    }
    //Test27 : Login Test by setting cookie and creating custom setting
    static testmethod void logintest002() {
        
        ApexPages.currentPage().getParameters().put('startURL','?testIDMS1&test1');                                                                              
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS123',AppName__c='testIDMS123',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',AppDescriptionFooter1__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
      //  IDMSCookieAppName__c cookie=new IDMSCookieAppName__c(Name='testIDMS123',AppName__c='testIDMS123');
      //  insert cookie;
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(
            'brand',        
            'testIDMS123',      
            '.force.',         
            1000,   
            true        
        )});
        VFC_IDMS_Consumer_SocialLogin login2=new VFC_IDMS_Consumer_SocialLogin();
        login2.username='testuser001@test.com';
        login2.username='Welcome01';
        login2.UserLogin();
    }
    //Test28 : Update Uims records from VFC_IDMS_Consumer_SocialLogin
    static testmethod void updateUIMSRecordTest() {
        
        String rId = [select id from UserRole where  name = 'CEO' Limit 1].Id;
        String RecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'Consumer Account'].Id;
        Account personAcc = new Account(FirstName='Test1',LastName='Account', ZipCode__c='12345',RecordTypeId = RecordTypeId,personemail = 'Scenario2Exception@accenture.com');
        insert personAcc;
        
        String contId = [select PersonContactId FROM account where ID =: personAcc.Id].PersonContactId;
        system.debug('*******'+contId+' *******'+personAcc.PersonContactId);
        
        User userObj2 = new User(alias = 'user', email='test312741' + '@cognizant.com',
                                 emailencodingkey='UTF-8', lastname='Testlast1', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = '00e12000000Ruoi', BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='test1user1' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='tej23416',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = 'Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432',ContactId=contId);
        
        insert userObj2;
        
        UIMS_User__c uimswrk = new UIMS_User__c(email__c='tstuser122454' + '@accenture.com',Cell__c='testcell',FirstName__c='test1',LastName__c='test2',
                                                CountryCode__c='US',LanguageCode__c='LT',Phone__c='3929439',JobTitle__c='Testtitle',
                                                JobFunction__c='testing',JobDescription__c='testdesc',Street__c='some',
                                                AddInfoAddress__c='testadd',LocalityName__c='mylocal',PostalCode__c='2358483',
                                                State__c='CA', User__c=userObj2.Id,County__c='IN',PostOfficeBox__c='boxtest',
                                                MiddleName__c='midtest',Salutation__c='Z003',Fax__c='3928483',FederatedID__c='85839212',
                                                CompanyId__c='84930',PrimaryContact__c='testprim',GoldenId__c='59492',AddInfoAddressECS__c='testecs',
                                                CountyECS__c='tstsce', GivenNameECS__c='testname',LocalityNameECS__c='localname',
                                                SnECS__c='testsn',StreetECS__c='tstst',Vnew__c='testnew',updateSource__c='UIMS',Password__c='{SHA}zfWdtFHfJmTfZDzyznPVMdQEvCg=');
        insert uimswrk ;
        
        
        VFC_IDMS_Consumer_SocialLogin.updateUIMSRecord(uimswrk.Id,userObj2.Id);
    }
    //Test29 : Creating custom setting and assigning to page parameter
    static testmethod void setcustomsetting() {
       // IDMSAppIDAppName__c apidapname= new IDMSAppIDAppName__c(Name='testIDMS1',AppName__c='testIDMS1');
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='testIDMS1',AppName__c='testIDMS1',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com', AppDescription__c='test',AppLogo__c='test',AppMobileFooterImage__c='test',AppDescriptionFooter1__c='test',LinkedInDisplayed__c=true,GoogleDisplayed__c=true,FacebookDisplayed__c=true,TwitterDisplayed__c=true);
        insert iDMSApplicationMapping;
       // insert apidapname;
        System.assert(iDMSApplicationMapping!=null);
        
        
        ApexPages.currentPage().getParameters().put('app','testIDMS1');
        
        ApexPages.currentPage().getParameters().put('startURL','?app=test123&test1');
        VFC_IDMS_Consumer_SocialLogin consumerSocialLogin=new VFC_IDMS_Consumer_SocialLogin();
        
    }
    
}