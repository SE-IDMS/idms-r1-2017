/*
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 12/01/2011
    Description     : Controller extension for VFP17_ProjInfoTranslationValues.This class displays the translated value of Opportunity Business Unit in the Project Information.
*/
public with sharing class VFC17_ProjInfoTranslationValues 
{
    public String businessUnit{get;set;}
    public RIT_ProjectInformation__c projInfo;
    
    //Constructor
    public VFC17_ProjInfoTranslationValues(ApexPages.StandardController controller) 
    {
        projInfo = (RIT_ProjectInformation__c) controller.getRecord();
        
        if(projInfo!=null)
        {
            //Query the Quote Link Id of the Project Information
            RIT_ProjectInformation__c[] projInfo = [select QuoteLink__c from RIT_ProjectInformation__c where Id=:projInfo.Id limit 1];
            if(projInfo.size()>0)
            { 
                //Query the Opportunity Id of corresponding Quote Link
                OPP_QuoteLink__c[] quoteLink =[select OpportunityName__c from OPP_QuoteLink__c where Id=:projInfo[0].QuoteLink__c limit 1];
                if(quoteLink.size()>0)
                {
                    if(quoteLink[0].OpportunityName__c!=null)
                    {
                        //Query the Translated Value of the Business Unit for the corresponding Opportunity
                        businessUnit = [select ToLabel(LeadingBusiness__c) from Opportunity where Id=:quoteLink[0].OpportunityName__c limit 1].LeadingBusiness__c;
                    }
                }
             }
         }
     }    
}