@isTest
private class AP_ContactBeforeDeleteHandler_TEST
{
    @isTest static void testCheckContactDelete()
    {
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Country__c ct1 = Utils_TestMethods.createCountry();
        ct1.CountryCode__c = 'IN';
        insert ct1;
        Country__c ct2 = Utils_TestMethods.createCountry();
        ct2.CountryCode__c = 'US';
        insert ct2;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;
        Contact con1 = Utils_TestMethods.createContact(acc1.Id,'TestContact');
        insert con1;
        System.debug('---Check the update on contact country---');
        con1.OtherCountryCode='US';
        con1.OtherCountry='US';
        update con1;
        //Contact con2 = Utils_TestMethods.createContact(acc1.Id,'TestCont');
        //insert con2;
        
        Case case1 = Utils_TestMethods.createCase(acc1.Id, con1.Id, 'Open');
        insert case1;
        Lead lead1= Utils_TestMethods.createLeadWithCon(con1.Id,ct.Id,100);
        insert lead1;
        Contract ctr1= Utils_TestMethods.createContract(acc1.Id, con1.Id);
        insert ctr1;
        CTR_ValueChainPlayers__c vcp1 = Utils_TestMethods.createCntrctValuChnPlyr(ctr1.Id);
        insert vcp1;
        Opportunity opp1= Utils_TestMethods.createOpenOpportunity(acc1.Id);
        insert opp1;
        OPP_ValueChainPlayers__c valcp1= Utils_TestMethods.createValueChainPlayer(acc1.Id,con1.Id,opp1.Id);
        insert valcp1;
        CustomerSatisfactionSurvey__c css = new CustomerSatisfactionSurvey__c(GoldenCustomerSatisfactionSurveyId__c= 'CustomerSatisfactionSurveyId',Account__c = acc1.id, bFOContactId__c = con1.Id);
        insert css;
        Event evt1=Utils_TestMethods.createEvent(case1.Id,con1.Id,'Open');
        insert evt1;
        Task task1= Utils_TestMethods.createTask(case1.Id,con1.Id,'Open');
        
        test.startTest();
        insert task1;
        
        con1.ToBeDeleted__c=true;
        con1.ReasonForDeletion__c='Created in error';
        
        
        System.debug('-----Update Account Check-----');
        try
        {
            update con1;
            
        }
        catch(Exception e)
        {
            String excep;
            if(e.getMessage().contains(Label.CLAPR14ACC01))
            {
                excep = 'YES';
            }
            system.assertEquals(excep, 'YES');
        }
        
        System.debug('-----Delete Account Check-----');
        try
        {
            delete con1;             
        }
        catch(DmlException e)
        {
            System.debug('DmlException caught: ' + e.getMessage());
        }
        test.stopTest();
    }     
}