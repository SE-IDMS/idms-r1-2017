//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : class used to set user password when called from vf page
//
//*******************************************************************************************

public with sharing class VFC_IDMSSetPassword{
    
    public String password{get;set;}
    public String confirmPassword{get;set;} 
    public Id userId {get;set;}
    public String idmsOperation {get;set;}
    public Boolean setpwdFlag{get; set;}
    public Boolean resetpwdFlag {get; set;}
    public Boolean redirectToApp{get;set;}
    public String appUrl{get;set;}
    public String appidNew{get;set;}
    public boolean incorrectAppId {get;set;}
    public string appMobileFooterImg{get; set;}
    public string applicationId{get; set;}    
    public string signature=ApexPages.currentPage().getParameters().get('sig');
    public string registrationSource=ApexPages.currentPage().getParameters().get('App');
    public string context;
    public string language{get; set;}
    public Boolean errorMessage{get;set;}
    public String ErrorMsgCode {get;set;}
    public String PageErrorMsg {get;set;}
    public string appandse{get;set;}
    public boolean isLogopresent{get;set;}
    public string FirstName{get;set;}
    public string LastName{get;set;}
    public string appTabName{get;set;}
    public string appTabLogo{get; set;}
    //constructor used for redirecions and fetching values from custom setting
    public VFC_IDMSSetPassword(){
        
        incorrectAppId          = false;
        appidNew                = ApexPages.currentPage().getParameters().get('App');
        String userIDNullCheck  = 'ID';
        try{
            userId              = ApexPages.currentPage().getParameters().get('id'); 
        }catch(exception e){
            userIDNullCheck     = '';
        }
        
        IDMSApplicationMapping__c appMap;
        appMap                  = IDMSApplicationMapping__c.getInstance(appidNew);
        system.debug('appMAp--> ' + appMap) ; 
        appAndSE                        = Label.CLR117IDMS004;
        if(appMap != null){        
            appidNew            = (String)appMap.get('AppId__c');
            appMobileFooterImg  = (String)appMap.get('AppMobileFooterImage__c');
            applicationId       = (String)appMap.get('AppId__c');
           // language            = (String)appMap.get('Language__c');
            appAndSE            = (String)appMap.get('AppBrandOrLogo__c');
            system.debug('appMap--> ' + appidNew + ' ' +  appMobileFooterImg + ' ' +  applicationId + ' ' + language ) ;
        }else{
            system.debug('inside else');
            incorrectAppId      = true;
        } 
        
        if(string.isnotblank(appAndSe)){
        isLogoPresent=true;
        }
        else{
        appAndSE                        = Label.CLR117IDMS004;
        }
        
        if(String.isBlank(appidNew) || String.isBlank(userIDNullCheck) || String.isBlank(signature)){
            incorrectAppId      = true;
        }
        
        // added to show application specific TabName as in BFOID-523
        appTabName                      = Label.CLNOV16IDMS128;
        appTabName = (String)appMap.get('AppTabName__c');
                    if(String.isBlank(AppTabName)){
                        AppTabName     = Label.CLNOV16IDMS128;
                    }
          // added to show application specific TabLogo as in BFOID-523
          appTabLogo                      = Label.CLFEB17IDMS004;
          appTabLogo         = (String)appMap.get('AppTabLogo__c');
                    if(String.isBlank(AppTabLogo)){
                        AppTabLogo     = Label.CLFEB17IDMS004;
                    } 
        
        //Prashanth
        //Added as a part of New Password Policy
        List<User> objUser=new List<User>();
        if(String.isNotBlank(userId)){
            objUser=[Select Firstname,Lastname from User where Id=:userId];
        }
        if(!objUser.isEmpty()){
            FirstName= objUser[0].Firstname;
            Lastname= objUser[0].Lastname;
        }
    }
    
    // method used for setting user's password
    public PageReference setPassword(){
        IdmsPasswordPolicy policy=new IdmsPasswordPolicy();
        //String pwdPattern = '^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d$!@#$&-_=+{}()~*^\\s]{8,20}$';
        user u;
        If(userId != null){
            u = [select username from user where id=:userId limit 1];
        }
        // Server Validation 
        if(String.isBlank(password)){
            errorMessage = false;
            ErrorMsgCode = 'set.passwordNull';
            return null;
        }
        if(String.isBlank(confirmPassword)){
            errorMessage = false;
            ErrorMsgCode = 'set.confirmpasswordNull';
            return null;
        }
        //Prashanth
        //Modified as a part of new password policy
        //if(!Pattern.matches(Label.CLNOV16IDMS129, password) && String.isNotBlank(password) ){
        if(!policy.checkPolicyMetOrNotWithFNLN(password,FirstName,LastName) && String.isNotBlank(password) ){
               errorMessage = false;
               ErrorMsgCode = 'set.incorrectPassword';
               return null;
        }
        //if(!Pattern.matches(Label.CLNOV16IDMS129, confirmPassword) && String.isNotBlank(confirmPassword) ){
        if(!policy.checkPolicyMetOrNotWithFNLN(confirmPassword,FirstName,LastName) && String.isNotBlank(confirmPassword) ){
               errorMessage = false;
               ErrorMsgCode = 'set.incorrectPassword';
               return null;
        }
        if( String.isNotBlank(confirmPassword) != String.isNotBlank(password)){
               errorMessage = false;
               ErrorMsgCode = 'set.passwordMismatch';
               return null;
        }
        
        IDMSResponseWrapper idmsResponse    = new IDMSResponseWrapper() ; 
        IDMSPasswordService pwdService      = new IDMSPasswordService() ; 
        PageReference successPg             = new PageReference('/apex/IDMSSetPasswordSuccess'); 
        successPg.getParameters().put('app',applicationId) ; 
        IDMSPasswordService resp            = new IDMSPasswordService();
        if(!Test.isrunningTest()){ 
            if(resp.LoginCheck(u.username,password)){
                ErrorMsgCode = 'set.catchOldPwd';
                return null;
            }
        }
        
        system.debug('entering else-->');
        IDMSPasswordService pwdServices = new IDMSPasswordService();
        idmsResponse = pwdServices.setPassworduser(userId,null,confirmPassword, registrationSource,signature);    
        system.debug('result--> ' + idmsResponse  + ' userid ' ) ;
        
        if(idmsResponse != null ){
            if(idmsResponse .Status.equalsignorecase('Success')){
                system.debug('entering success') ;
                IDMSApplicationMapping__c appMap;
                appMap = IDMSApplicationMapping__c.getInstance(registrationSource);
                if(appMap != null){         
                    if(string.isnotblank((string)appMap.UrlRedirectAfterProfileUpdate__c)){
                        appurl = (string)appMap.UrlRedirectAfterProfileUpdate__c;
                    }
                    else{
                        appurl = '/identity/idp/login?app='+appMap.appid__c;
                    }
                }
                
                return successPg;
            } 
                        
            PageErrorMsg = idmsResponse.Message ; 
            if(PageErrorMsg.contains(Label.CLNOV16IDMS236)){
                errorMessage=false;
                ErrorMsgCode='set.catchOldPwd';
                return null; 
            } 
            errorMessage=false;
            ErrorMsgCode='set.catch';
            return null; 
        }else{
            system.debug('response--> ' + idmsResponse.Message) ;
            PageErrorMsg    = idmsResponse.Message ; 
            errorMessage    = false;
            ErrorMsgCode    = 'set.catch';
            return null; 
        } 
        return null  ; 
    }
    
    //used for redirecting user to login page
    public PageReference loginRedirect() {
        
        PageReference redirectpg = new PageReference('/apex/UserLogin');
        redirectpg.getParameters().put('app',applicationId) ; 
        return redirectpg ; 
        
    }
    
}