public with sharing class VFC_RequirementSpecializations {
    
    public list<RequirementSpecialization__c> lstReqSpe  {get;set;}
    public VFC_RequirementSpecializations(ApexPages.StandardController controller) {
        lstReqSpe = [Select Specialization__r.Name from RequirementSpecialization__c where RequirementCatalog__c=:controller.getId() order by Specialization__r.Name limit 1000]; 
        
    }

}