public class VFC_PRM_POMPEmailNotificationTemplate {
    public String RecptId {get; set;}
    public String OpptyId { get; set; }
    public String OrfId { get; set; }
    public String RecptLanguage { get; set; }
    public String EmailContentFor { get; set; }
    //public String AccountCountry {get;set;}
    public Id RelatedId { get; set; }
    public String ObjType {get; set;}
    public Id CountryId {get; set;}
    public Id Country {get; set;}
    public String SiteUrl { 
        get {
            return AP_PRMUtils.GetPartnerSiteDomain();        
        }
        private set;
    }
}