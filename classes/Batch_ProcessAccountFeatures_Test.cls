/*
------------------------------------
Name of Author : Atyam.D.N.V.Satyanarayana
Created Date   : 31-08-2016
Description    : Test Class for Batch_ProcessAccountFeatures,Batch_ProcessContactFeatures,Batch_ProcessPartnerPermissionSets
------------------------------------
*/
@isTest
public class Batch_ProcessAccountFeatures_Test{
    static testMethod void testMe(){
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
    User u = new User(Alias = 'standt', 
                     Email='test@schneider-electric.com', 
                     EmailEncodingKey='UTF-8',
                     FirstName ='Test First',      
                     LastName='Testing',
                     CommunityNickname='CommunityName123',      
                     LanguageLocaleKey='en_US', 
                     LocaleSidKey='en_US', 
                     ProfileId = p.Id,
                     TimeZoneSidKey='America/Los_Angeles', 
                     UserName='subhashish@test1.com',
                     userroleid = r.Id,
                     isActive = True,                       
                     BypassVR__c = True,
                     BypassTriggers__c='AP30');     
    
    System.runAs(u) {
        CS_PRM_ApexJobSettings__c jobSet = new CS_PRM_ApexJobSettings__c(Name='MAX_JOB_COUNT',NumberValue__c=100,InitialSync__c=True);
        insert jobSet;
        
        CS_PRM_ApexJobSettings__c jobSet1 = new CS_PRM_ApexJobSettings__c(Name='POMPV1FTR',NumberValue__c=1440,InitialSync__c=True);
        insert jobSet1;
        
        PartnerProgram__c ppm=new PartnerProgram__c();
        ppm.name='APC Value Added Tester';
        ppm.ProgramType__c='Business';
        ppm.ProgramStatus__c='Active';
        insert ppm;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram;
        
        PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, null);
        insert countryProgram; 
        list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
        countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
    
        if(!countryPartnerProgram.isEmpty())
        {
        countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
        update countryPartnerProgram;
        }
        
        ProgramLevel__c prgLevel2 = Utils_TestMethods.createCountryProgramLevel(countryPartnerProgram[0].id);
        insert prgLevel2;
        prgLevel2.levelstatus__c = 'active';              
        update prgLevel2;   
        
        ProgramFeature__c pf = new ProgramFeature__c();
        pf.FeatureStatus__c ='Inactive';
        pf.DeactivatedDate__c=System.now();
        pf.ProgramLevel__c=prgLevel2.Id;
        pf.PartnerProgram__c= countryPartnerProgram[0].id;
        insert pf;
        
        Id profilesId = System.label.CLMAR13PRM03;// [select id from profile where name='SE - Channel Partner (to be deleted)'].Id;
         
    
        Country__c Country = new Country__c (Name='TCY', CountryCode__c='US'); 
        insert country;
        String countryid = country.id;
        System.debug('****** Country ID:'+country.id);

        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
        insert PartnerAcc;
        Test.startTest();
        Contact PartnerCon = new Contact(
                FirstName='Test',
                LastName='lastname',
                AccountId=PartnerAcc.Id,
                JobTitle__c='Z3',
                CorrespLang__c='EN',            
                WorkPhone__c='1234567890'
                );
                System.debug('****** Country ID:'+countryid);
                PartnerCon.Country__c = countryid ; 
                
                Insert PartnerCon;   
        
        User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                        CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                        Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
        
        insert u1; 
        System.debug('*** User Profile ID->'+u1.ProfileID);   
          
        PartnerAcc.TECH_AccountOwner__c=u1.Id;
        update PartnerAcc;
        
        
            ACC_PartnerProgram__c acpp = new ACC_PartnerProgram__c();
            acpp.Active__c= true;
            acpp.ProgramLevel__c=prgLevel2.Id;
            acpp.PartnerProgram__c= countryPartnerProgram[0].id;
            acpp.Account__c= PartnerAcc.Id;  
            insert acpp;
            
            Country__c county = new Country__c();
                county.Name   = 'India';
                county.CountryCode__c = 'IN';
                county.InternationalPhoneCode__c = '91';
                county.Region__c = 'APAC';
                INSERT county;
            StateProvince__c stateProv = new StateProvince__c();
        stateProv.Name = 'Karnataka';
        stateProv.Country__c = county.Id;
        stateProv.CountryCode__c = county.CountryCode__c;
        stateProv.StateProvinceCode__c = '10';
        INSERT stateProv;
    
        Account acc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=county.id, PRMCompanyName__c='TestAccount',PRMCountry__c=county.id,PRMBusinessType__c='FI',
                                            PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true,StateProvince__c = stateProv.id,City__c='Bangalore');
        Insert acc;
        Contact con = new Contact(FirstName='Test',LastName='lastname', JobTitle__c='Z3',CorrespLang__c='EN',AccountId=acc.Id,
          WorkPhone__c='999121212',
          Country__c=county.Id,
          PRMFirstName__c = 'Test',
          PRMLastName__c = 'lastname',
          PRMEmail__c = 'test.lastName@yopmail.com',
          PRMWorkPhone__c = '999121212',
          PRMPrimaryContact__c = true
          );
          Insert con;
        FeatureCatalog__c FC = Utils_TestMethods.createFeatureCatalog();
        ContactAssignedFeature__c caf1 = new ContactAssignedFeature__c(Active__c=True,Contact__c=con.Id,FeatureCatalog__c=FC.Id);
        insert caf1;
        
        Batch_ProcessAccountFeatures bpaf = new Batch_ProcessAccountFeatures();
        Database.executeBatch(bpaf,500);
        Integer numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5){
            Batch_ProcessAccountFeatures bpaf1 = new Batch_ProcessAccountFeatures();
            DateTime sysTime = System.now().addMinutes(Integer.valueOf(jobSet1.NumberValue__c));
            String cronExp = sysTime.format('s m H d M \'?\' yyyy');     
            String jobId = System.schedule('Product2 test update', cronExp, bpaf1);
        }
        Test.stopTest();         
      }  
    }
}