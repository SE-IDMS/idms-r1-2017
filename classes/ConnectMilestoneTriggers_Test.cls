/*

Author: Srikant.Joshi
Date Created:08-March-2012
Description:

*/

@isTest 
private class ConnectMilestoneTriggers_Test{
    static testMethod void TestMethod1() {
    
    //+Ve case
   
         User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect9');
        System.runAs(runAsUser){
            Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            insert inti;
        
       
        Project_NCP__c globalProgram = new Project_NCP__c(Program__c=Inti.Id,name='Tst',Global_Business__c='Power;Building;Partner-Division;'+Label.Connect_Smart_Cities,Global_Functions__c='HR;GM;IPO;GSC-Regional',GSC_Regions__c='EMEAS;NA',Power_Region__c='EMEAS;NA',Partner_Region__c='test;test1',Smart_Cities_Division__c='test');
        Insert globalProgram;
        Connect_Milestones__c ConMil = new Connect_Milestones__c(Program_Number__c=globalProgram.id,Global_Business__c='Power;Partner-Division;'+Label.Connect_Smart_Cities,Milestone_Name__c='tst',Global_Functions__c='HR;GM;GSC-Regional',GSC_Regions__c='EMEAS',Power_Region__c='EMEAS',Smart_Cities_Division__c='test',Partner_Region__c='test',Roadblock_Decision_to_be_taken_Short__c='Test',Achievements_in_past_quarter_Summary__c='Tst');  
             Insert ConMil;  
             Connect_Milestones__c ConMil2 = new Connect_Milestones__c(Program_Number__c=globalProgram.id,Global_Business__c='Power',Milestone_Name__c='tst1',Global_Functions__c='HR;GM',Power_Region__c='EMEAS',Roadblock_Decision_to_be_taken_Short__c='Test',Achievements_in_past_quarter_Summary__c='Tst');  
             Insert ConMil2; 
             Connect_Milestones__c ConMil3 = new Connect_Milestones__c(Program_Number__c=globalProgram.id,Global_Business__c='Power',Milestone_Name__c='tst2',Global_Functions__c='HR;GM',Power_Region__c='EMEAS',Roadblock_Decision_to_be_taken_Short__c='Test',Achievements_in_past_quarter_Summary__c='Tst');  
             Insert ConMil3; 
          Project_NCP__c globalProgram1 = new Project_NCP__c(Program__c=Inti.Id,name='Tst1',Global_Business__c='Power;Building',Global_Functions__c='HR;GM;IPO;GSC-Regional',GSC_Regions__c='EMEAS;NA');
        Insert globalProgram1;
        Connect_Milestones__c ConMil1 = new Connect_Milestones__c(Program_Number__c=globalProgram1.id,Global_Business__c='ITB;Partner-Division',Milestone_Name__c='tst1',Global_Functions__c='HR',Power_Region__c='EMEAS',Partner_Region__c='test',Roadblock_Decision_to_be_taken_Short__c='Test',Achievements_in_past_quarter_Summary__c='Tst');  
        
       Cascading_Milestone__c CM=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil2.id,
Program_Number__c=globalProgram.id,Program_Scope__c='Global Functions',Entity__c='GM');
insert CM; 
Cascading_Milestone__c CM5=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil2.id,
Program_Number__c=globalProgram.id,Program_Scope__c='Global Functions',Entity__c='GM');
insert CM5; 

Cascading_Milestone__c CM1=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil1.id,
Program_Number__c=globalProgram1.id,Progress_Summary_Deployment_Leader_Q2__c = 'Completed',Program_Scope__c='Global Functions',Entity__c='GM');



Cascading_Milestone__c CM3=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil.id,Progress_Summary_Deployment_Leader_Q2__c = 'Completed',
Program_Number__c=globalProgram.id,Program_Scope__c='Global Functions',Entity__c='GSC-Regional',GSC_Region__c='EMEAS');
insert CM3; 

Cascading_Milestone__c CM4=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil.id,Progress_Summary_Deployment_Leader_Q2__c = 'Completed',
Program_Number__c=globalProgram.id,Program_Scope__c='Global Business',Entity__c='Partner-Division',Partner_Region__c='test');
insert CM4;  

Cascading_Milestone__c CM14=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=ConMil.id,Progress_Summary_Deployment_Leader_Q2__c = 'Completed',
Program_Number__c=globalProgram.id,Program_Scope__c='Global Business',Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test');
insert CM14;     

CM5.Progress_Summary_by_Champions__c='completed';
    ConMil3.Progress_Summary__c='Completed';
    update CM5;
    update ConMil3;  
                           
    ConMil.Global_Business__c='Power;Building;Partner-Division;'+Label.Connect_Smart_Cities;
    ConMil.Global_Functions__c='HR;GM;IPO;GSC-Regional';
    ConMil.GSC_Regions__c='EMEAS;NA';
    ConMil.Smart_Cities_Division__c = 'test';
    ConMil.Power_Region__c='EMEAS;NA';
    ConMil.Partner_Region__c='test;test1';
    ConMil.Progress_Summary_Q2__c='Completed';
    
   
    
    
     ConMil2.Global_Business__c='Power;Partner-Division';
    ConMil2.Partner_Region__c='test';
    ConMil2.Global_Functions__c='HR;GM;GSC-Regional';
    ConMil2.GSC_Regions__c='EMEAS';
    ConMil2.Power_Region__c='EMEAS;NA';
    

     Test.startTest(); 
       try{  
           
        update ConMil;
        update ConMil2;  
       
        
                   
         }
  catch(DmlException d)
  {
  d.getMessage();
  }
  ConMil.GSC_Regions__c='test3';
  ConMil.Partner_Region__c='test3';
  
  
  
  try{  
           
        update ConMil;  
        update CM1;
       
        
        update CM3;
        update CM4;
        
       
        update ConMil;   
         }
  catch(DmlException f)
  {
  f.getMessage();
  }
  ConMil.Global_Business__c='Power;Building;Partner-Division';
    ConMil.Global_Functions__c='HR;GM;IPO;GSC-Regional';
    ConMil.GSC_Regions__c='EMEAS;NA';
    ConMil.Power_Region__c='EMEAS;NA';
    ConMil.Partner_Region__c='test;test1';
        CM1.Entity__c='test2';
        CM3.GSC_Region__c='test2';
    CM4.Partner_Region__c='test2';
    ConMil.Progress_Summary_Q2__c = 'Completed';
    
    try{ 
        update CM3; 
        update CM4;  
        update ConMil;  
        update CM1;
       
        
            
         }
  catch(DmlException h)
  {
  h.getMessage();
  }
    //-VE case
    
   
        
     try{
        
        Insert ConMil1;  
        insert CM1;
        }catch(DmlException e)
        {
        e.getMessage();
        }
        ConMil1.Global_Functions__c='HR;GSC-Regional';
        ConMil1.GSC_Regions__c='test2';
        ConMil1.Power_Region__c='EMEAS;NA';
        
        try{
        update ConMil1;
        }catch(DmlException q)
        {
        q.getMessage();
        }
       Test.stopTest(); 
    }
  }
}