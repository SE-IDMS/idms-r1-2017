/*
    Author          : Siddharth Nagavarapu (GD) 
    Date Created    : 13/02/2012
    Description     : Test class for Controller VFC78_OpptyExecutiveSummary.  Verifies that required data is populated for displaying
                      the Opportunity Executive Summary.
                        - a Opportunity Record is created and a user,account,country,task and Opportunity Team Member is created.
*/
@isTest
private class VFC78_OpptyExecutiveSummary_TEST {
  public static testmethod void executiveSummaryTest() {
  
  //insert block
  
      User stdUser=Utils_testMethods.createStandardUser('test5');
      stdUser.currencyIsoCode='INR';
      stdUser.UserRoleId=System.Label.CLDEC13SLS41;
      insert stdUser;
      stdUser.isActive=False;
      update stdUser;
      stdUser.isActive=True;
      update stdUser;    
      Test.startTest();
      System.runAs(stdUser)
      {
      Account acc=Utils_testMethods.createAccount(stdUser.id);
      insert acc;
     Country__c cntry =new Country__c(Name='FRANCE',CountryCode__c='FR');
     insert cntry;
      Opportunity oppt=Utils_testMethods.createOpportunity(acc.id);
      oppt.amount=12345;
      oppt.currencyIsoCode='EUR';
      insert oppt;
      
      ApexPages.StandardController controller=new ApexPages.StandardController(oppt);
      VFC78_OpptyExecutiveSummary execSummary=new VFC78_OpptyExecutiveSummary(controller);
      
      execSummary.getactivities();
      execSummary.populateOpptAmount();
      execSummary.getSalesTeam();
      
      stdUser.currencyIsoCode='EUR';
      update stdUser;
      
        
      Task tsk=new Task(whatId=oppt.id,whoid=stdUser.id,subject='soc',activitydate=System.Today());
     // insert tsk;
      OpportunityTeamMember opptTMMember=Utils_TestMethods.createOppTeamMember(oppt.id,stdUser.id);
      insert opptTMMember;
    
    
  //Methods
  execSummary.getactivities();
  execSummary.populateOpptAmount();
  execSummary.getSalesTeam();
  }
  Test.stopTest();
 }
 
 public static testmethod void executiveSummaryTest2() {
  
  //insert block
  
      User stdUser=Utils_testMethods.createStandardUser('tes4t');
      stdUser.currencyIsoCode='INR';
      stdUser.UserRoleId=System.Label.CLDEC13SLS41;
      insert stdUser;
      Test.startTest();
      System.runAs(stdUser)
      {
      Account acc=Utils_testMethods.createAccount(stdUser.id);
      insert acc;
     Country__c cntry =new Country__c(Name='FRANCE',CountryCode__c='FR');
     insert cntry;
      Opportunity oppt=Utils_testMethods.createOpportunity(acc.id);
      oppt.amount=12345;
      oppt.currencyIsoCode='INR';
      insert oppt;
      
      ApexPages.StandardController controller=new ApexPages.StandardController(oppt);
      VFC78_OpptyExecutiveSummary execSummary=new VFC78_OpptyExecutiveSummary(controller);
      
      execSummary.getactivities();
      execSummary.populateOpptAmount();
      execSummary.getSalesTeam();
      
      stdUser.currencyIsoCode='EUR';
      update stdUser;
      
        
      Task tsk=new Task(whatId=oppt.id,whoid=stdUser.id,subject='soc',activitydate=System.Today());
     // insert tsk;
      OpportunityTeamMember opptTMMember=Utils_TestMethods.createOppTeamMember(oppt.id,stdUser.id);
      insert opptTMMember;
    
    
  //Methods
  execSummary.getactivities();
  execSummary.populateOpptAmount();
  execSummary.getSalesTeam();
  }
  Test.stopTest();
 }
}