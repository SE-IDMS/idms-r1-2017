global class WS_SiteConnector{

    //Apex object to be exposed in apex web service
   global class GCSSite{
        webservice String LegacySiteId;
        webservice String SiteName;
        webservice String IsSiteActive;
        webservice String IBsiteId;
        webservice String AccountGoldenId;
    }
    global class SiteResult{
        webservice String result;
        webservice String Error;
    }
    webservice static SiteResult CreateAndUpdateSites(List<GCSSite> gcsSites){
        SiteResult siteres = new SiteResult();
        set<string> LegacySiteIdSet = new set<string>();
        set<string> SiteGoldenIdSet = new set<string>();
        Map<String,id> accountGoldenIDMap = new Map<String,id>();
        List<GCSSite__c> lstSitesToBeUpdated = new List<GCSSite__c>();
        List<GCSSite__c> lstSitesToBeInserted = new List<GCSSite__c>();
        System.debug('gcsSites'+gcsSites);
        if(!gcsSites.isEmpty()){
            for(GCSSite site: gcsSites){
                LegacySiteIdSet.add(site.LegacySiteId);
                SiteGoldenIdSet.add(site.AccountGoldenId);
            }
        }
        List<Account> lstAccount = [SELECT SEAccountId__c,ID FROM Account WHERE SEAccountId__c =: SiteGoldenIdSet and ToBeDeleted__c=false];
        System.debug(lstAccount);
        for(Account acc:lstAccount){
            accountGoldenIDMap.put(acc.SEAccountId__c, acc.id);
        }
        
        
        //GCSSite maps to Sites object in bFO
        //Map GCSSite class object to Site custom object
        
        GCSSite__c SiteObj;
        Map<string, GCSSite__c> LegacySiteIdMap = new Map<string, GCSSite__c>();
        List<GCSSite__c> lstSites = [SELECT LegacySiteId__c, Name FROM GCSSite__c WHERE LegacySiteId__c IN: LegacySiteIdSet];
        for(GCSSite__c site:lstSites){
            LegacySiteIdMap.put(site.LegacySiteId__c,site);
        }
        for(GCSSite gcsSiteObj: gcsSites)
        {
           if(!LegacySiteIdMap.containsKey(gcsSiteObj.LegacySiteId)){
             
                SiteObj = new GCSSite__c();
                SiteObj.LegacySiteId__c  = gcsSiteObj.LegacySiteId;
                SiteObj.Name= gcsSiteObj.SiteName;
                SiteObj.Active__c = (gcsSiteObj.IsSiteActive =='yes'? true: false);
                SiteObj.LegacyIBSiteID__c = gcsSiteObj.IBsiteId;
                SiteObj.Account__c= accountGoldenIDMap.get(gcsSiteObj.AccountGoldenId);
                lstSitesToBeInserted.add(SiteObj);
            }
        }
        if(lstSitesToBeInserted.size() > 0){
            try{
                Database.saveResult[] siteUpdateResult = Database.insert(lstSitesToBeInserted,true);
                system.debug('Inserted Records'+siteUpdateResult);
                for(Database.saveResult result : siteUpdateResult){
                    if(!result.isSuccess()){
                       for (Database.Error err : result.getErrors()){
                            siteres.result=err.getStatusCode()+' '+err.getMessage();
                            
                            System.Debug('######## lstSitesToBeInsertedObject Insert Error : '+err.getStatusCode()+' '+err.getMessage());
                       }
                     }
                    else{
                    
                        siteres.result='Success';
                    }
                }
            }
            catch(Exception ex){
                System.Debug('######## WS_WebGCS Insert Error :' + ex );
                //siteres.result='Failed';
                siteres.result='Failed';
                siteres.Error = 'Golden ID not found';
            }   
        }
        
        for(GCSSite gcsSiteObj: gcsSites)
        {
         if(LegacySiteIdMap.containsKey(gcsSiteObj.LegacySiteId))
             {
                SiteObj = new GCSSite__c(id=LegacySiteIdMap.get(gcsSiteObj.LegacySiteId).ID);
                SiteObj.LegacySiteId__c  = gcsSiteObj.LegacySiteId;
                SiteObj.Name= gcsSiteObj.SiteName;
                SiteObj.Active__c = (gcsSiteObj.IsSiteActive =='yes'? true: false);
                SiteObj.LegacyIBSiteID__c = gcsSiteObj.IBsiteId;
                SiteObj.Account__c= accountGoldenIDMap.get(gcsSiteObj.AccountGoldenId);
                lstSitesToBeUpdated.add(SiteObj);
            }
        }
        if(lstSitesToBeUpdated.size() > 0){
            try{
                Database.saveResult[] siteUpdateResult = Database.update(lstSitesToBeUpdated,true);
                for(Database.saveResult result : siteUpdateResult){
                    if(!result.isSuccess()){
                       for (Database.Error err :result.getErrors()){
                            siteres.result=err.getStatusCode()+' '+err.getMessage();
                        }
                    }
                    else{
                     siteres.result='Sucess';
                    }
                }
               
            }
            catch(Exception ex){
                System.Debug('######## WS_WebGCS Update Error :' + ex );
                //siteres.result='Failed to update, please fill validated data';
                  siteres.result='Failed';
                siteres.Error = 'Golden ID not found';
            }   
        }
        return siteres;
    }
    }