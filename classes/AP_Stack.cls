/*******

Developer :Hanamanth

********/


Public class AP_Stack {

    public static List<Answertostack__c>  listCRSk = new List<Answertostack__c>();
    public static List<Answertostack__c>  listCRSk_ASC = new List<Answertostack__c>();
    public static List<Answertostack__c>  listCRSkInsert= new List<Answertostack__c>();
    public static boolean keepInvolved{get;set;}
    
     //for Delete the Stack
    public static List<Answertostack__c>  listDeletCRSk = new List<Answertostack__c>();
    public static integer intPostion;  
    
   
    public static void addtheOrgToStack(boolean keepInvold,string CurrtOrg,string newOrg,string  newDepart, set<id> compRequsSet,string oldDepart,string cREmail,Id isssueUsrId, Integer maxStackCounter) {
        integer targetPosition; 
        integer currentPosition; 
        string CRId;
        for(string str:compRequsSet) {
            CRId=str;
            break;
        
        }
        list<Answertostack__c> updateKpInv= new list<Answertostack__c>();
        listCRSk=[select id,name,ComplaintRequest__c,KeepInvolved__c,Organization__c,Department__c  from Answertostack__c where ComplaintRequest__c in :compRequsSet order by TECH_StackCounter__c DESC];
        listCRSk_ASC=[select id,name,ComplaintRequest__c,KeepInvolved__c,Organization__c,Department__c  from Answertostack__c where ComplaintRequest__c in :compRequsSet order by TECH_StackCounter__c ASC];
        
        //if(listCRSk_ASC.size > 0){
            
        //}
        
        targetPosition = getStackPosition(newOrg,newDepart,0);
        currentPosition = getStackPosition(CurrtOrg,oldDepart,0);
        
           system.debug('CR Inser1targetPosition---->'+targetPosition);
           system.debug('CR Inser1---->'+getHigherKeepInvolved_1(targetPosition));
           system.debug('CR Inser1---->'+newOrg);
           system.debug('CR Inser1---->'+CurrtOrg);
        if (targetPosition >= 0) { // If NEW organization is present in stack
             system.debug('CR Inser1targetPosition---->'+targetPosition);
            if (getHigherKeepInvolved_1(targetPosition) > 0) {
                if (targetPosition > 0 && targetPosition!=listCRSk_ASC.size()-1 )  {
                   
                    listCRSk_ASC[targetPosition].KeepInvolved__c = false; 
                    updateKpInv.add(listCRSk_ASC[targetPosition]);
                    
                    
                }   
            } // Clear the KI flag for the NEW organization, but only if it's not at the bottom of the stack (the first org must always be involved)
             
            else {
            system.debug('CR Inser1targetPosition---->'+targetPosition);
                if(targetPosition!=listCRSk_ASC.size()-1){
                    clearStackFromPosition(targetPosition);
                } 
            }
        }
        //&& getHigherKeepInvolved_1(targetPosition) >=0
        if(currentPosition >=0 ) {
            if (keepInvold) {

                listCRSk_ASC[currentPosition].KeepInvolved__c = true; 
                updateKpInv.add(listCRSk_ASC[currentPosition]);
            }
        }
        if(updateKpInv.size() > 0) {
            update updateKpInv; 
            //clearStackFromPosition(targetPosition);
        }
        /*if(CurrtOrg!= newOrg && keepInvold && listCRSk_ASC.size()= 1) && currentPosition < 0  ) {
        
        }*/
        if(keepInvold==false && listCRSk_ASC.size()==1 && listCRSk_ASC[0].Organization__c==newOrg ) {
            
        }else {
            if (CurrtOrg!= newOrg && (keepInvold || listCRSk_ASC.size() >= 1) && currentPosition < 0 ) {


                addToStack(CurrtOrg, keepInvold,CRId,oldDepart,cREmail,isssueUsrId ,maxStackCounter );

            }else if (CurrtOrg== newOrg  && newDepart!=oldDepart && currentPosition < 0) {
                addToStack(CurrtOrg, keepInvold,CRId,oldDepart,cREmail,isssueUsrId, maxStackCounter);

            }
        
        }
    
    }
    
   
    // Returns position of an organization in the stack, searching from the start position (default 0)
    public static  integer getStackPosition(string org,string Orgdepart,integer start){
    
        start = start;// OR 0;
        integer pos = -1;
        system.debug('HanuOrg----->'+org);
        system.debug('HanuOrgdepart----->'+Orgdepart);
        string Orgdepart1=Orgdepart;
        for (integer i = start; i < listCRSk_ASC.size() && pos < 0; i++) {
            Orgdepart=Orgdepart==''? Orgdepart=null:Orgdepart;
            Orgdepart1=Orgdepart1==null? Orgdepart1='':Orgdepart1;
            if (listCRSk_ASC[i].Organization__c == org && (Orgdepart==listCRSk_ASC[i].Department__c || Orgdepart1==listCRSk_ASC[i].Department__c)) {
                pos = i;
            }
             
        }
       return pos;
    }
   
    //Hanumanth
    public static  integer getHigherKeepInvolved_1(integer start){
    
        start = start; //|| 0;
        integer pos = -1;
        system.debug('start--------->'+listCRSk_ASC.size() );
          
          system.debug('start--------->'+start);
        for (integer i = start + 1; i < listCRSk_ASC.size() && pos < 0; i++) { 
            if (listCRSk_ASC[i].KeepInvolved__c) {
                pos = i;
            }
        }
          system.debug('start--------->'+start);
        return pos;
    }
    
  
    public static  void addToStack(string org,boolean keepInvolved1,string strCR,string strDepartment,string strEmailId,Id cRIsueUsrId, Integer maxStackCounterfromCr){
    
        Answertostack__c InsertStackNw = new Answertostack__c();
        InsertStackNw.ComplaintRequest__c=strCR;//crEH.ComplaintRequest__c;
        InsertStackNw.KeepInvolved__c =keepInvolved1;//crEH.KeepInvolved__c;//FromKeepInvolved__c;
        InsertStackNw.Department__c =strDepartment;
        InsertStackNw.IssueOwner__c =cRIsueUsrId;
        InsertStackNw.ContactEmailAccountableOrganization__c=strEmailId;
        InsertStackNw.Organization__c=org;//crEH.FromOrg__c;   
        System.debug('maxStackCounterfromCr'+maxStackCounterfromCr);
        if (maxStackCounterfromCr==null){ 
            InsertStackNw.TECH_StackCounter__c=Integer.ValueOf(label.CLOCT15I2P74);
        }
        else {       
            InsertStackNw.TECH_StackCounter__c=maxStackCounterfromCr+1;
        }
            
        listCRSkInsert.add(InsertStackNw);  
      
        system.debug('CR Inser1---->'+listCRSkInsert);
         //Insert Stack
        if(listCRSkInsert.size() > 0){ 
            system.debug('CR Inser---->'+listCRSkInsert);
            insert listCRSkInsert;
        }
    }  
   
  
    public static void clearStackFromPosition(integer start){
      
        //for Delete the Stack
        List<Answertostack__c>  listClearCRSk = new List<Answertostack__c>();
        //start = start; //|| 0;
        system.debug('Delete --->'+start);

        if(start < listCRSk_ASC.size()) {
            for(integer i=start;i<listCRSk_ASC.size();i++) {
                    system.debug('listCRSk_ASC[i]'+listCRSk_ASC[i]);
                    listClearCRSk.add(listCRSk_ASC[i]);
                                        
                
            }
        
        }
        system.debug('HAnu--------->'+listClearCRSk);
        if(listClearCRSk.size() > 0) {
         delete listClearCRSk;
        }    
   
    } 

}