@isTest
public class FieloPRM_AP_HeaderControllerTest{

    public static testmethod void unitTest(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'This is a Name with more than twenty';
            member.FieloEE__FirstName__c = 'Marco';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
            insert member;

            FieloEE.MemberUtil.setMemberId(member.Id);
            FieloPRM_AP_HeaderController controller = new FieloPRM_AP_HeaderController();
            FieloPRM_AP_HeaderController.getMemberByMemberOrContactId(null);
        }
    }
    
}