@isTest(seealldata=true)
public class SVMXC_WorkOrderAfter_Test {
    static testMethod void syncProductExpertiseReqToWO (){
    
             Account objAccount = Utils_TestMethods.createAccount();
            objAccount.name='testaccount';
            //RecordTypeid = Label.CLOCT13ACC08;
            insert objAccount;
            
            profile pf=[select name from profile where name='System Administrator'];
            
             User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = pf.id, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP30;AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
    
            Brand__c brand1_1 = new Brand__c();

            brand1_1.Name ='Brand 1-1';
            brand1_1.SDHBRANDID__c = 'Test_BrandSDHID';
            brand1_1.IsSchneiderBrand__c = true;
            insert brand1_1 ;
            
            DeviceType__c dt1_1 = new DeviceType__c();
            dt1_1.name = 'Device Type 1-1';
            dt1_1.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-1';
            insert dt1_1 ;
            
            
            
            Category__c c1_1 = new Category__c();
            c1_1.Name =  'Range 1-1';
            c1_1.CategoryType__c = 'RANGE';
            c1_1.SDHCategoryID__c = 'Test_RangeSDHID1-1';
            
            insert c1_1;
           
            
            product2 p2= new product2(Name='testprod',Brand2__c=brand1_1.id,DeviceType2__c=dt1_1.id,CategoryId__c=c1_1.id,TECH_IsEnriched__c=true);
            insert p2;
            product2 p3= new product2(Name='testprod23',Brand2__c=null,DeviceType2__c=null,CategoryId__c=null);
            insert p3;
            
             SVMXC__Skill__c skill= new SVMXC__Skill__c(name='testskill');
                insert skill;
                
                SVMXC_ApplicableProduct__c ap=new SVMXC_ApplicableProduct__c(Skill__c=skill.id,Product__c=p2.id);
                insert ap;

                SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(name='test123',SVMXC__Product__c=null);
                insert ip2;
                SVMXC__Installed_Product__c ip3 = new SVMXC__Installed_Product__c(name='test123',SVMXC__Product__c=p3.id);
                insert ip3;

        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
            ip.SVMXC__Company__c = objAccount.id;
            ip.Name = 'Test Intalled Product';
            ip.SVMXC__Status__c= 'Open';
            ip.GoldenAssetId__c = 'GoledenAssertId12345';
            ip.BrandToCreate__c ='Test Brand';
            ip.Brand2__c=brand1_1.id;
            ip.DeviceType2__c=dt1_1.id;
            ip.Category__c=c1_1.id;
            ip.SVMXC__Product__c=p2.id;
            ip.CustomerSerialNumber__c = 'SerialNumber';
            //ip.SchneiderCommercialReference__c = 'SECREF';
            insert ip;
         SVMXC__Service_Order__c workOrder =  new SVMXC__Service_Order__c();
            workOrder.Work_Order_Category__c='On-site';  
            workorder.SVMXC__Component__c=ip.id;
            workOrder.SVMXC__Order_Type__c='Maintenance';
            workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
            workOrder.SVMXC__Order_Status__c = 'UnScheduled';
            workOrder.CustomerRequestedDate__c = Date.today();
            workOrder.Service_Business_Unit__c = 'IT';
            workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
            workOrder.SendAlertNotification__c = true;
            workOrder.BackOfficeReference__c = '111111';
            workOrder.Comments_to_Planner__c='testing'; 
            workOrder.SVMXC__Company__c  =objAccount.id;
            //workOrder2.OwnerId='00512000006O70E';
            Test.starttest();
                insert workOrder;
                update workOrder;
            SVMXC__Service_Order__c workOrder2 =  new SVMXC__Service_Order__c();
            workOrder2.Work_Order_Category__c='On-site';  
            workorder2.SVMXC__Component__c=ip.id;
            workorder2.Parent_Work_Order__c=workOrder.id;
            workOrder2.SVMXC__Order_Type__c='Maintenance';
            workOrder2.SVMXC__Problem_Description__c = 'BLALBLALA';
            workOrder2.SVMXC__Order_Status__c = 'New';
            workOrder2.CustomerRequestedDate__c = Date.today();
            workOrder2.Service_Business_Unit__c = 'IT';
            workOrder2.SVMXC__Priority__c = 'Normal-Medium'; 
                insert workOrder2;
                workOrder2.OwnerId=user.id;//'00512000006O70E';
                update workOrder2;
                
           SVMXC__Skill__c skillrec= new SVMXC__Skill__c(Name='test1',SVMXC__Description__c='testrec',recordtypeid='012A0000000nwEZ');
               insert skillrec;
        Work_Order_Skills__c wos= new Work_Order_Skills__c(Skill__c=skillrec.id,Work_Order__c=workOrder2.id);
            try{
            insert wos; 
             Delete wos;
            }catch (exception e){}
           
                Test.stoptest();
    }
    
}