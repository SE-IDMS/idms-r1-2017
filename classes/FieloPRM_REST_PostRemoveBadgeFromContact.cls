/********************************************************************************************************
    Author: Fielo Team
    Date: 27/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestPostRemoveBadgeFromContact/*') 
global class FieloPRM_REST_PostRemoveBadgeFromContact{  

    /**
    * [postRemoveBadgeFromContact]
    * @method   postRemoveBadgeFromContact
    * @Pre-conditions  
    * @Post-conditions 
    * @param    []    []
    * @return   []    []
    */
    @HttpPost
    global static String postRemoveBadgeFromContact(String type, Map<String ,List<String>> conToBadges){    
        
        return FieloPRM_UTILS_BadgeMember.removeBadgeFromContact(Type, conToBadges);        
    }  
   
}