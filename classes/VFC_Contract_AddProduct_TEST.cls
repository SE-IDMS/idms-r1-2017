/**
31-July-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for VFC_Contract_AddProduct
 */
@isTest
private class VFC_Contract_AddProduct_TEST {

    static testMethod void myUnitTest() {
        
        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        insert PKGTemplate;
        
        ContractProductInformation__c CPI = Utils_TestMethods.createContractProdInfo(PKGTemplate.Id);
        /*CPI.CommercialReference__c = '';
        CPI.ProductSuccession__c = '';
        CPI.ProductDescription__c = '';
        CPI.TECH_PM0CodeInGMR__c = '';
        CPI.Family__c = '';
        CPI.ProductBU__c = '';
        CPI.ProductLine__c = '';
        CPI.ProductFamily__c= '';*/
        ApexPages.StandardController controller = new ApexPages.StandardController(CPI);
        VFC_Contract_AddProduct newObj = new VFC_Contract_AddProduct(controller);
        
        VCC08_GMRSearch newgmrSearch = new VCC08_GMRSearch();
        newgmrSearch.pageController = newObj ;
        
        
        newObj.GMRsearchForPkgTemplate.Init('',newgmrSearch);
        newObj.GMRsearchForPkgTemplate.Cancel(newgmrSearch);
        DataTemplate__c newDataTemplate = new DataTemplate__c();
        newDataTemplate.field1__c = 'Test1';
        newDataTemplate.field2__c = 'Test1';
        newDataTemplate.field3__c = 'Test1';
        newDataTemplate.field4__c = 'Test1';
        newDataTemplate.field5__c = 'Test1';
        newDataTemplate.field6__c = 'Test1';
        newDataTemplate.field7__c = 'Test1';
        newDataTemplate.field8__c = 'Test1';
        newDataTemplate.field9__c = 'Test1';
        newDataTemplate.field10__c = 'Test1';
        newDataTemplate.field11__c = 'Test1';
        newgmrSearch.ActionNumber = 1;
        newObj.GMRsearchForPkgTemplate.PerformAction(newDataTemplate,newgmrSearch);
        newgmrSearch.ActionNumber = 2;
        newObj.GMRsearchForPkgTemplate.PerformAction(newDataTemplate,newgmrSearch);
        CPI.PackageTemplate__c =null;
        newObj.GMRsearchForPkgTemplate.PerformAction(newDataTemplate,newgmrSearch);
    }
}