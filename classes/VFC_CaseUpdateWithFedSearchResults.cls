public class VFC_CaseUpdateWithFedSearchResults {

    private Case MyCase;
    
    public VFC_CaseUpdateWithFedSearchResults(ApexPages.StandardController stdController) {
        MyCase = (Case)stdController.getRecord();
    }
    
    public PageReference updateCase() {
        try {
            AP_CaseUpdateWithFedSearchResults.executeUpdate(MyCase);
            return new PageReference('/'+MyCase.Id);
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
        return null;
    }
}