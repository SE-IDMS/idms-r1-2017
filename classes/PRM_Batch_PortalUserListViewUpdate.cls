global class PRM_Batch_PortalUserListViewUpdate Implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {
  
    public String query;
    public static String CRON_EXP = '0 0 23 * * ? *'; // CRON JOB expression to run the batch job at 11 PM everyday
    
    public PRM_Batch_PortalUserListViewUpdate() {
    }
  
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query != null)
            return Database.getQueryLocator(query);
        else {
          query = 'SELECT Id, Name FROM Account LIMIT 1';
          return Database.getQueryLocator(query);
        }  
    }

    global void execute(Database.BatchableContext BC, List<Account> listAccount) {
        System.debug('*** Updating List Account *****');
        if (!Test.isRunningTest()) {
        AP_ParseListViewMetadata tObj = new AP_ParseListViewMetadata();
        tObj.doParse ();
        }
    }
  
    global void finish(Database.BatchableContext BC) {
        System.debug('*** Updating List Account Finished *****');
    }

    global static String ScheduleIt () {
        PRM_Batch_PortalUserListViewUpdate plv = new PRM_Batch_PortalUserListViewUpdate();
        return System.schedule('Extract Partner List View Access', CRON_EXP, plv);
    }
    
    // Schedule method
    global void execute(SchedulableContext sc) {
        PRM_Batch_PortalUserListViewUpdate batch_portalviewList = new PRM_Batch_PortalUserListViewUpdate ();
        Database.executebatch(batch_portalviewList);
    }
}