/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | AP_EUS_Test                                                                        |
|                       |                                                                                    |
|     - Object(s)       | End User Support                                                                   |
|     - Description     |   - Insert or Update Escalation History for KPIs calculation (Test method)         |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | 21-oct-2012                                                                        |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
private class AP_EUS_Test
{
   static TestMethod void testingUpdateEscalationHistory()
   {
      System.Debug('****** Testing the Method of Update Escalation History ****');
      Test.startTest();

      //********************************************
      //** STEP 0: DEFINE VARIABLES USED IN TESTS **
      //********************************************
      List<EUS_EndUserSupport__c>              lstEUS2Create        = new List<EUS_EndUserSupport__c>();
      set<Id>                                  setCreatedEUSID      = new set<Id>();
      List<EUS_EndUserSupport__c>              lstCreatedEUS        = new List<EUS_EndUserSupport__c>();
      List<EUS_EndUserSupport__c>              lstEUS2Update        = new List<EUS_EndUserSupport__c>();
      List<EndUserSupportEscalationHistory__c> lstEscalationHistory = new List<EndUserSupportEscalationHistory__c>();
      List<EndUserSupportEscalationHistory__c> lstTmpEscalationHist = new List<EndUserSupportEscalationHistory__c>();

      //******************************************************************************************
      //** STEP 1a: CREATE 2 EUS WITH RECORD TYPE = 'EUS Record Type' (1 NOT CLOSED + 1 CLOSED) **
      //******************************************************************************************
      //New EUS not "closed" when saving
      lstEUS2Create.add(new EUS_EndUserSupport__c(RecordTypeId       = System.Label.CLNOV13EUS01,   //EUS Record Type
                                                  Title__c           = 'TEST 1 - STATUS = NEW',
                                                  RequestType__c     = 'Question',
                                                  Environment__c     = 'Training',
                                                  Description__c     = 'TEST 1 - Question',
                                                  Capacity__c        = 'All Processes',
                                                  Category__c        = 'Other',
                                                  Criticality__c     = '3',                         //Gravity
                                                  DetectedBy__c      = UserInfo.getUserId(),
                                                  EscalationLevel__c = 'Level 3 - Data Administration',
                                                  Status__c          = 'New'
                                                ));
      //New EUS "closed" when saving
      lstEUS2Create.add(new EUS_EndUserSupport__c(RecordTypeId       = System.Label.CLNOV13EUS01,   //EUS Record Type
                                                  Title__c           = 'TEST 2 - STATUS = CLOSED',
                                                  RequestType__c     = 'Question',
                                                  Environment__c     = 'Training',
                                                  Description__c     = 'TEST 2 - Question',
                                                  Capacity__c        = 'All Processes',
                                                  Category__c        = 'Other',
                                                  Criticality__c     = '3',                         //Gravity
                                                  DetectedBy__c      = UserInfo.getUserId(),
                                                  EscalationLevel__c = 'Level 2 - Delegated Admin',
                                                  Status__c          = 'Closed',
                                                  Solution__c        = 'Solution is...'
                                                ));
      //****************************************************************************
      //** STEP 1b: CREATE 1 EUS WITH RECORD TYPE = 'Expertise - EUS Record Type' **
      //****************************************************************************
      lstEUS2Create.add(new EUS_EndUserSupport__c(RecordTypeId       = System.Label.CLNOV13EUS02,   //Expertise - EUS Record Type
                                                  Title__c           = 'EXPERTISE EUS',
                                                  RequestType__c     = 'salesforce.com Expertise',
                                                  Description__c     = 'Expertise EUS - Description',
                                                  Capacity__c        = 'All Processes',
                                                  Category__c        = 'Other',
                                                  Criticality__c     = '3',                         //Gravity
                                                  DetectedBy__c      = UserInfo.getUserId(),
                                                  EscalationLevel__c = 'Level 4 - salesforce.com Expertise',
                                                  ExpertiseType__c   = 'Technical',
                                                  SOWType__c         = 'Micro Project',
                                                  Status__c          = 'New'
                                                ));
      //****************************************************************************
      //** STEP 1c: CREATE 1 EUS WITH RECORD TYPE = 'Privilege - EUS Record Type' **
      //****************************************************************************
      lstEUS2Create.add(new EUS_EndUserSupport__c(RecordTypeId          = System.Label.CLNOV13EUS03,   //Privilege - EUS Record Type
                                                  Title__c              = 'PRIVILEGE EUS',
                                                  PrivilegeRequest__c   = 'Bypass Validation Rule (All);Bypass Workflow (All)',
                                                  DetectedBy__c         = UserInfo.getUserId(),
                                                  ExpectedStartDate__c  = System.today(),
                                                  ExpectedClosedDate__c = System.today(),
                                                  Environment__c        = 'Training',
                                                  EscalationLevel__c    = 'Level 3 - Solution Team',
                                                  Description__c        = 'Privilege EUS - Description',
                                                  ApprovalStatus__c     = 'Not Submitted',
                                                  Status__c             = 'New'
                                                ));

      //*************************************
      //** STEP 1d: INSERT LIST OF NEW EUS **
      //*************************************
      Database.SaveResult[] EUSInsertResult = Database.insert(lstEUS2Create, false);
      System.Debug('### EUSInsertResult = ' + EUSInsertResult);

      //*************************************************************************************************************
      //** STEP 2: CHECK THAT THERE IS 1 ESCALATION HISTORY RECORD ONLY FOR EUS WITH 'EUS Record Type' RECORD TYPE **
      //*************************************************************************************************************
      for(Database.SaveResult anItem : EUSInsertResult)
      {
         setCreatedEUSID.add(anItem.getId());
      }
      System.Debug('### setCreatedEUSID = ' + setCreatedEUSID);

      lstEscalationHistory = [SELECT Id,
                                     EndUserSupport__c,
                                     EndUserSupport__r.RecordTypeId,
                                     EndUserSupport__r.Status__c,
                                     EndUserSupport__r.EscalationLevel__c,
                                     From__c,
                                     FromDate__c,
                                     Status__c,
                                     To__c,
                                     ToDate__c,
                                     TrackingField__c
                              FROM   EndUserSupportEscalationHistory__c
                              WHERE  EndUserSupport__c in :setCreatedEUSID];

      System.AssertEquals(lstEscalationHistory.size(), 2);
      for(EndUserSupportEscalationHistory__c anItem : lstEscalationHistory)
      {
         System.Debug('### anItem = ' + anItem);

         System.AssertEquals(anItem.EndUserSupport__r.RecordTypeId, System.Label.CLNOV13EUS01);
         System.AssertEquals(anItem.Status__c, anItem.EndUserSupport__r.Status__c);
         System.AssertEquals(anItem.From__c, anItem.EndUserSupport__r.EscalationLevel__c);
         System.AssertNotEquals(anItem.FromDate__c, null);
         System.AssertEquals(anItem.TrackingField__c, System.Label.CLNOV13EUS04);
         
         if(anItem.Status__c == 'New')
         {
            System.AssertEquals(anItem.To__c, null);
            System.AssertEquals(anItem.ToDate__c, null);
         }
         else
         {
            System.AssertEquals(anItem.To__c, anItem.EndUserSupport__r.EscalationLevel__c);
            System.AssertNotEquals(anItem.ToDate__c, null);
         }
      }

      //*************************************************************************************
      //** STEP 3a: UPDATE ONLY ESCALATION LEVEL ON EUS WITH 'EUS Record Type' RECORD TYPE **
      //*************************************************************************************
      lstCreatedEUS = [SELECT Id,
                              Status__c,
                              EscalationLevel__c
                       FROM   EUS_EndUserSupport__c
                       WHERE  RecordTypeId = :System.Label.CLNOV13EUS01   //EUS Record Type
                       AND    Id in :setCreatedEUSID];

      for(EUS_EndUserSupport__c anItem : lstCreatedEUS)
      {
         lstEUS2Update.add(new EUS_EndUserSupport__c(Id                 = anItem.Id,
                                                     EscalationLevel__c = 'Level 3 - Solution Team'));
      }
      Database.SaveResult[] EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      //********************************************************************************************
      //** STEP 3b: CHECK - SHOULD HAVE 3 ESCALATIONS RECORDS (1 FOR CLOSED EUS + 2 FOR OPEN EUS) **
      //********************************************************************************************
      lstEscalationHistory.clear();
      lstEscalationHistory = [SELECT Id,
                                     EndUserSupport__c,
                                     EndUserSupport__r.RecordTypeId,
                                     EndUserSupport__r.Status__c,
                                     EndUserSupport__r.EscalationLevel__c,
                                     From__c,
                                     FromDate__c,
                                     Status__c,
                                     To__c,
                                     ToDate__c,
                                     TrackingField__c
                              FROM   EndUserSupportEscalationHistory__c
                              WHERE  EndUserSupport__r.RecordTypeId = :System.Label.CLNOV13EUS01   //EUS Record Type
                              AND    EndUserSupport__c in :setCreatedEUSID
                              ORDER BY EndUserSupport__c Asc, FromDate__c Asc];

      System.AssertEquals(lstEscalationHistory.size(), 3);

      //Closed EUS: should have 1 record
      for(EndUserSupportEscalationHistory__c anItem : lstEscalationHistory)
      {
         System.Debug('### anItem = ' + anItem);
         if(anItem.Status__c != 'New')
         {
            lstTmpEscalationHist.add(anItem);
         }
      }
      System.AssertEquals(lstTmpEscalationHist.size(), 1);
      System.AssertEquals(lstTmpEscalationHist.get(0).To__c, lstTmpEscalationHist.get(0).EndUserSupport__r.EscalationLevel__c);

      //Open EUS: should have 2 records
      lstTmpEscalationHist.clear();
      for(EndUserSupportEscalationHistory__c anItem : lstEscalationHistory)
      {
         System.Debug('### anItem = ' + anItem);
         if(anItem.Status__c == 'New')
         {
            lstTmpEscalationHist.add(anItem);
         }
      }
      System.AssertEquals(lstTmpEscalationHist.size(), 2);
      //1st record created
      System.AssertEquals(lstTmpEscalationHist.get(0).To__c, lstTmpEscalationHist.get(0).EndUserSupport__r.EscalationLevel__c);
      //2nd record created
      System.AssertEquals(lstTmpEscalationHist.get(1).From__c, lstTmpEscalationHist.get(1).EndUserSupport__r.EscalationLevel__c);
      System.AssertNotEquals(lstTmpEscalationHist.get(1).FromDate__c, null);
      System.AssertEquals(lstTmpEscalationHist.get(1).To__c, null);
      System.AssertEquals(lstTmpEscalationHist.get(1).ToDate__c, null);

      //******************************************************************************
      //** STEP 4a: UPDATE OPEN EUS TO CLOSED EUS WITHOUT CHANGING ESCALATION LEVEL **
      //******************************************************************************
      lstCreatedEUS.clear();
      lstEUS2Update.clear();
      EUSUpdateResult.clear();
      lstCreatedEUS = [SELECT Id,
                              Status__c,
                              EscalationLevel__c
                       FROM   EUS_EndUserSupport__c
                       WHERE  Id = :lstTmpEscalationHist.get(1).EndUserSupport__c];

      lstEUS2Update.add(new EUS_EndUserSupport__c(Id          = lstCreatedEUS.get(0).Id,
                                                  Status__c   = System.Label.CLNOV13EUS07,     //Under Champion's Validation
                                                  Solution__c = 'Solution found...'));

      EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      //********************************************************************************
      //** STEP 5a: UPDATE CLOSED EUS TO CLOSED EUS WITHOUT CHANGING ESCALATION LEVEL **
      //********************************************************************************
      lstEUS2Update.clear();
      EUSUpdateResult.clear();
      lstEUS2Update.add(new EUS_EndUserSupport__c(Id          = lstCreatedEUS.get(0).Id,
                                                  Status__c   = 'Closed'));

      EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      //******************************************************************************
      //** STEP 6a: UPDATE CLOSED EUS TO OPEN EUS WITHOUT CHANGING ESCALATION LEVEL **
      //******************************************************************************
      lstEUS2Update.clear();
      EUSUpdateResult.clear();
      lstEUS2Update.add(new EUS_EndUserSupport__c(Id          = lstCreatedEUS.get(0).Id,
                                                  Status__c   = 'New'));

      EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      //****************************************************************************
      //** STEP 7a: UPDATE OPEN EUS TO OPEN EUS WITHOUT CHANGING ESCALATION LEVEL **
      //****************************************************************************
      lstEUS2Update.clear();
      EUSUpdateResult.clear();
      lstEUS2Update.add(new EUS_EndUserSupport__c(Id          = lstCreatedEUS.get(0).Id,
                                                  Status__c   = 'Under Investigation'));

      EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      //************************************************************************
      //** STEP 8a: UPDATE OPEN EUS TO OPEN EUS AND CHANGING ESCALATION LEVEL **
      //************************************************************************
      lstEUS2Update.clear();
      EUSUpdateResult.clear();
      lstEUS2Update.add(new EUS_EndUserSupport__c(Id          = lstCreatedEUS.get(0).Id,
                                                  Status__c   = 'Under Correction',
                                                  EscalationLevel__c = 'Level 3 - Data Administration'));

      EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      //**************************************************************************
      //** STEP 9a: UPDATE OPEN EUS TO CLOSED EUS AND CHANGING ESCALATION LEVEL **
      //**************************************************************************
      lstEUS2Update.clear();
      EUSUpdateResult.clear();
      lstEUS2Update.add(new EUS_EndUserSupport__c(Id          = lstCreatedEUS.get(0).Id,
                                                  Status__c   = 'Closed',
                                                  EscalationLevel__c = 'Level 3 - Solution Team'));

      EUSUpdateResult = Database.update(lstEUS2Update, false);
      System.Debug('### EUSUpdateResult = ' + EUSUpdateResult);

      Test.stopTest();
   }
}