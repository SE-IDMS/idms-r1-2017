/*****************************************************************************************
    Author : Shruti Karn
    Description : For October 2013 Release to enable Partners to submit Assesssment via sites
                                       
*****************************************************************************************/
public class VFC_WebPartnerAssessment
{
    public PartnerAssessment__c prtAssessment{get;set;}
    public map<String,String> mapFieldValue{get;set;}
    public list<String> lstQueryFieldValue{get;set;}
    public String retURL{get;set;}
    public Boolean readOnly{get;set;}
    public String Company{get;set;}
    public String Name{get;set;}
    public VFC_WebPartnerAssessment(ApexPages.StandardController controller) {
    system.debug('****************VFC_WebPartnerAssessment called');
        String partnerPRGID;
        String programLevelID;
        String assessmentID;
        String specializationID;
        
        List<String> fieldList = new List<String>();
        mapFieldValue = new map<String,String>();
                
        fieldList.add('Assessment__c');
        fieldList.add('Assessment__r.Name');
        fieldList.add('Assessment__r.PartnerPRogram__c');
        fieldList.add('Assessment__r.ProgramLevel__c');
        fieldList.add('Assessment__r.Specialization__c');
        fieldList.add('AccountSpecialization__c');
        fieldList.add('AccountSpecialization__r.Name');
        fieldList.add('AccountSpecialization__r.Specialization__c');
        fieldList.add('AccountAssignedProgram__c');
        fieldList.add('AccountAssignedProgram__r.Name');
        fieldList.add('AccountAssignedProgram__r.PartnerProgram__c');
        fieldList.add('AccountAssignedProgram__r.ProgramLevel__c');
        fieldList.add('AccountAssignedProgram__r.PartnerProgram__r.Name');
        fieldList.add('AccountAssignedProgram__r.ProgramLevel__r.Name');
        fieldList.add('Company__c');
        fieldList.add('Name__c');
        fieldList.add('recordtypeid');
        if(!test.isRunningTest())
            controller.addFields(fieldList);
        prtAssessment = (PartnerAssessment__c)controller.getRecord();
        
            
        Id accountPrgRecordType = [Select id from RecordType where developername='PRMProgramApplication'and SobjectType ='PartnerAssessment__c' limit 1].Id;            
        //if(prtAssessment.recordtypeid == null)
            prtAssessment.recordtypeid = accountPrgRecordType;
        Name = ApexPages.currentPage().getParameters().get('Name');
        if(prtAssessment == null)
            prtAssessment = new PartnerAssessment__c();
        if(ApexPages.currentPage().getParameters().get('Company')!= null)
            prtAssessment.company__c =  ApexPages.currentPage().getParameters().get('Company');
        if(ApexPages.currentPage().getParameters().get('companyid')!= null)
            prtAssessment.companyid__c = ApexPages.currentPage().getParameters().get('companyid');
        if(ApexPages.currentPage().getParameters().get('name')!= null)
            prtAssessment.name__C = ApexPages.currentPage().getParameters().get('name');
        if(ApexPages.currentPage().getParameters().get('account')!= null)
            prtAssessment.account__c = ApexPages.currentPage().getParameters().get('account');
        if(ApexPages.currentPage().getParameters().get('SEAccountID')!= null)
            prtAssessment.seaccountid__c = ApexPages.currentPage().getParameters().get('SEAccountID');
        partnerPRGID = ApexPages.currentPage().getParameters().get('partnerPRGID');
        programLevelID = ApexPages.currentPage().getParameters().get('programLevelID');
        assessmentID = ApexPages.currentPage().getParameters().get('assessmentID');
        specializationID = ApexPages.currentPage().getParameters().get('specializationID');
        
        if(ApexPages.currentPage().getParameters().get('readOnly') != null)
            readOnly = Boolean.valueOF(ApexPages.currentPage().getParameters().get('readOnly'));
        else
            readOnly = false;

            if(assessmentID != null && prtAssessment.Id == null)
            {
                prtAssessment.Assessment__c = assessmentID ;
                retURL = '/apex/VFP_WebPartnerAssessment?readOnly='+true+'&assessmentID='+assessmentID+'&id=';
                
            }
            if(prtAssessment != null)
            {
                assessmentID = prtAssessment.Assessment__c;
                if(prtAssessment.Assessment__r.PartnerProgram__c != null)
                {
                    partnerPRGID = prtAssessment.Assessment__r.PartnerProgram__c ;
                    programLevelID = prtAssessment.Assessment__r.PRogramLevel__c ;
                }
                if(prtAssessment.Assessment__r.Specialization__c != null)
                    specializationID = prtAssessment.Assessment__r.Specialization__c ;    
            }
           
            if(partnerPRGID != null)
            {
                prtAssessment.recordtypeid = Label.CLOCT13PRM02;
                lstQueryFieldValue = new list<String>();
                lstQueryFieldValue.add('PartnerProgram__c:'+partnerPRGID );
                lstQueryFieldValue.add('ProgramLevel__c:'+programLevelID);
                retURL = '/apex/VFP_WebPartnerAssessment?readOnly='+true+'&assessmentID='+assessmentID+'&id=';
            }
            if(specializationID != null)
            {
                prtAssessment.recordtypeid = Label.CLOCT13PRM03;
                lstQueryFieldValue = new list<String>();
                lstQueryFieldValue.add('Specialization__c:'+specializationID);
                retURL = '/apex/VFP_WebPartnerAssessment?readOnly='+true+'&assessmentID='+assessmentID+'&id=';
            }
    
    system.debug('retURL :'+retURL );
     //   }
        system.debug('****************VFC_WebPartnerAssessment ends');
    }


}