/********************************************************************************************************
    Author: Fielo Team
    Date: 06/03/2015
    Description: REST API that receives a map with Contact external Id as key and a certification wrapper 
                 class as value.
                 If the certification doesn't exist (object Badge) it creates the Badge and then creates 
                 the Badge Member record for that Certification (Badge) and Contact Id.
                 If the Contact Id already has that Badge it doesn't create it.
                 It returns only errors in a map that has Contact external Id as key and a string with the
                 error message as value.
                 If no errors returns an empty map.
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestPostCertification/*') 
global class FieloPRM_REST_PostCertification{  

    /**
    * [postCertification creates certifications for the list of Contact external Ids]
    * @method   postCertification
    * @Pre-conditions  
    * @Post-conditions 
    * @param    Map<String,Lits<certificationWrapper>>  mapContactExtIdCertifications  [map with Contact external Id as key and a certification wrapper class as value]
    * @return   Map<String,String>                                                     [map that has Contact external Id as key and a string with the error message as value]
    */
    @HttpPost
    global static Map<String,list<String>> postCertification(Map<String,List<certificationWrapper>> mapContactExtIdCertifications){    
        
        return FieloPRM_UTILS_BadgeMember.createCertificationBadgeMembers(mapContactExtIdCertifications);        
    }  

    global class certificationWrapper{
        public string name;
        public string uniqueCode;
        public date dateFrom;
        public date dateTo;
    }   
    
}