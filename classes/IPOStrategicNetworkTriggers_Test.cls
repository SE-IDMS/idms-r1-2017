@isTest
private class IPOStrategicNetworkTriggers_Test {
static testMethod void testIPOStrategicNetworkTriggers() {
 User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOst6');
        System.runAs(runAsUser){
          IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM',Global_Business__c='ITB',Operation__c='NA');
            insert IPOp;
            IPO_Strategic_Deployment_Network__c C=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM');
            insert C;
            IPO_Strategic_Deployment_Network__c C1=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Business', Entity__c='ITB');
            insert C1;
            IPO_Strategic_Deployment_Network__c C2=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Operation Regions', Entity__c='NA');
            insert C2;
           C.Scope__c='Global Business';
            C.Entity__c='ITB';
                  
            Test.startTest();
            update c;
            
            Test.stopTest();
}
}
}