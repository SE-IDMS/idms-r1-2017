@isTest
private class WS_FacadeDelegatedAuthService_TEST {
    @isTest static void test_Authenticate() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WS_FacadeDelegatedAuthService_Mock());
        WS_FacadeDelegatedAuthService.AuthenticationService objAuthenticationService = new WS_FacadeDelegatedAuthService.AuthenticationService();
		Boolean blnResponse = objAuthenticationService.Authenticate('strUserName','strPassword','1.2.3.4');        
    }
}