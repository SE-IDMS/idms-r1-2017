/**

 */
@isTest(SeeAllData=true)
private class AP53_Campaign_Test {

    static testMethod void testAP53_Campaign() 
    {    
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {    
            AP53_Campaign ap = new AP53_Campaign();
            
            Campaign cm = new Campaign();
            cm.Name = 'TestCampaign '+system.now();
            cm.IsActive = true;
            cm.EndDate = system.today() + 10;
            cm.Global__c = true;
            cm.ProductFocus__c = 'All Products';
            cm.BUFocus__c = 'All Business Units';
            cm.CampaignLeader__c = UserInfo.getUserId();
            cm.Script__c = 'test script';
            
            insert cm;
                        
            system.debug(cm.id);
            List<CampaignCountry__c> lstcc = new List<CampaignCountry__c>();
            system.debug(cm.id);
            lstcc = [select name from CampaignCountry__c where Campaign__c = :cm.id];
            
            delete lstcc[0];
            
            cm.Global__c = false;
            update cm;
            
        
            ap.getAllCountryNames();
            
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
            user.UserPermissionsMarketingUser  = true;
                   
            system.runas(user)
            {
                
            }   
       }
    }
}//End of test class