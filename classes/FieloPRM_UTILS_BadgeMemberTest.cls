/********************************************************************
* Company: Fielo
* Developer: Denis Aranda 
* Created Date: 15/04/2015
* Description:
********************************************************************/
@isTest
public without sharing class FieloPRM_UTILS_BadgeMemberTest {

    static testMethod void test1(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'FieloPRM_MemberTrigger';
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'testNamePolo'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'testNameMarco';
            member.FieloEE__Street__c = 'test';
          
            insert member;
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_BadgeAPIName__c = 'asd'; 
            insert badge;

            badge.F_PRM_Type__c = 'Program Level'; 
            Update badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;

            FieloPRM_BadgeAccount__c badgeAcc = new FieloPRM_BadgeAccount__c ();  
            badgeAcc.F_PRM_Badge__c = badge.id;
            badgeAcc.F_PRM_Account__c = acc.id;
            
            insert badgeAcc;
    
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = '123';
            con1.FieloEE__Member__c = member.id;
            insert con1;
            
            delete [select id from FieloEE__MemberSegment__c];
            list<String> listIds = new list<String>();
            listIds.add(con1.id);
        
            //FieloPRM_REST_PostRemoveBadgeFromContact rest = new FieloPRM_REST_PostRemoveBadgeFromContact();
            Map<String ,List<String>> accToBadges = new Map<String ,List<String>>();
            List<String> aux = new List<String>();
            aux.add(badge.F_PRM_BadgeAPIName__c);
            accToBadges.put(con1.PRMUIMSID__c , aux);
            Test.startTest();
            Utils_SDF_Methodology.limits();
            FieloPRM_REST_PostRemoveBadgeFromContact.postRemoveBadgeFromContact('Program Level', accToBadges);
            Utils_SDF_Methodology.limits();
            Map<String ,List<String>> accToBadges2 = new Map<String ,List<String>>();
            list<String> listStringaux =  new list<String>();
            listStringaux.add('asd');
            accToBadges2.put('aa', listStringaux);
            //FieloPRM_REST_PostRemoveBadgeFromContact.postRemoveBadgeFromContact('Program Level', accToBadges2);
            Test.stopTest();
        }
    }
    
    static testMethod void test2(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
        
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo';
            member.FieloEE__FirstName__c = 'Marco'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            insert member;

            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level'; 
            badge.F_PRM_TypeSelection__c = 'Program Level';
            insert badge;

            badge.F_PRM_Type__c = 'Program Level'; 
            badge.F_PRM_TypeSelection__c = 'Program Level';
            Update badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;
            
            badMem.F_PRM_CompletionPercentage__c = 10;
            update badMem;
            
            FieloPRM_BadgeAccount__c badgeAcc = new FieloPRM_BadgeAccount__c ();  
            badgeAcc.F_PRM_Badge__c = badge.id;
            badgeAcc.F_PRM_Account__c = acc.id;
            insert badgeAcc;
    
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;

            integer currentYear = date.today().year();
            System.debug('currentYear: ' + currentYear);
            integer yearMinus = currentYear - 1;
            System.debug('yearMinus: ' + yearMinus);
            String currentMonth = string.ValueOf(date.today().month());
            String currentDay = string.ValueOf(date.today().day() + 1);
            System.debug('currentMonth: ' + currentMonth);
            System.debug('currentDay: ' + currentDay);
            if(integer.ValueOf(currentMonth) < 10){
                currentMonth = 0+currentMonth;
                System.debug('currentMonth1: ' + currentMonth);
            }
            if(integer.ValueOf(currentDay) < 10){
                currentDay = 0+currentDay;
                System.debug('currentDay1: ' + currentDay);
            }
    
            FieloPRM_UTILS_BadgeMember.getUpdatedProgramLevelOnContacts('20000101000000000', '30000101000000000');
                        
            FieloPRM_UTILS_BadgeMember.getUpdatedProgramLevelOnContacts(yearMinus + currentMonth + currentDay +'081010110', currentYear + currentMonth + currentDay + '081010110');
            FieloPRM_UTILS_BadgeMember.getUpdatedProgramLevelOnContacts('2015101008101','20151011081010110');
            Test.startTest();
            FieloPRM_UTILS_BadgeMember.getUpdatedProgramLevelOnContacts('20151010081010110', '0081010110');
            FieloPRM_UTILS_BadgeMember.getUpdatedProgramLevelOnContacts('20151010081010110','20151011081010110');
            Test.stopTest();

        }
        
    }
    
    static testMethod void test3(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo';
            member.FieloEE__FirstName__c = 'Marco'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
          
            insert member;
            
            FieloEE__Badge__c badge1 = new FieloEE__Badge__c();
            badge1.Name = 'test ProgramLevel1';
            badge1.F_PRM_Type__c = 'Program Level'; 
            badge1.F_PRM_TypeSelection__c = 'Program Level';

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
            badge2.Name = 'test ProgramLevel2';
            badge2.F_PRM_Type__c = 'Program Level'; 
            badge2.F_PRM_TypeSelection__c = 'Program Level';
            
            insert new List<FieloEE__Badge__c>{badge1,badge2};

            badge1.F_PRM_Type__c = 'Program Level';
            badge1.F_PRM_TypeSelection__c = 'Program Level'; 

            badge2.F_PRM_Type__c = 'Program Level';
            badge2.F_PRM_TypeSelection__c = 'Program Level'; 

            update new List<FieloEE__Badge__c>{badge1,badge2};
            
            FieloEE__BadgeMember__c badMem1 = new FieloEE__BadgeMember__c();
            badMem1.FieloEE__Member2__c = member.id;
            badMem1.FieloEE__Badge2__c = badge1.id;
            badMem1.F_PRM_Status__c = 'Active';

            FieloEE__BadgeMember__c badMem2 = new FieloEE__BadgeMember__c();
            badMem2.FieloEE__Member2__c = member.id;
            badMem2.FieloEE__Badge2__c = badge2.id;
            badMem2.F_PRM_Status__c = 'Active';

            insert new List<FieloEE__BadgeMember__c>{badMem1,badMem2};
                       
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;
                                
            list<String> listIds = new list<String>();
            listIds.add(con1.PRMUIMSId__c);
        
            FieloPRM_REST_GetProgLevelsByContact rest = new FieloPRM_REST_GetProgLevelsByContact();
            FieloPRM_REST_GetProgLevelsByContact.getProgramLevelsByContact(listIds);
        }
    }
    
    static testMethod void test4(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.PRMUIMSID__c = 'PRMUIMSIDTEST';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'LASTaCCtEST'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'fIRSTACCtEST';
            member.FieloEE__Street__c = 'testSreet';
          
            insert member;
            
            FieloEE__Badge__c badge1 = new FieloEE__Badge__c();
            badge1.Name = 'test ProgramLevel1';
            badge1.F_PRM_Type__c = 'Program Level'; 
            badge1.F_PRM_TypeSelection__c = 'Program Level';

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
            badge2.Name = 'test ProgramLevel2';
            badge2.F_PRM_Type__c = 'Program Level'; 
            badge2.F_PRM_TypeSelection__c = 'Program Level';
            
            insert new List<FieloEE__Badge__c>{badge1,badge2};

            badge1.F_PRM_Type__c = 'Program Level';
            badge1.F_PRM_TypeSelection__c = 'Program Level'; 

            badge2.F_PRM_Type__c = 'Program Level';
            badge2.F_PRM_TypeSelection__c = 'Program Level'; 

            update new List<FieloEE__Badge__c>{badge1,badge2};

            FieloPRM_BadgeAccount__c badgeAcc1 = new FieloPRM_BadgeAccount__c ();  
            badgeAcc1.F_PRM_Badge__c = badge1.id;
            badgeAcc1.F_PRM_Account__c = acc.id;

            FieloPRM_BadgeAccount__c badgeAcc2 = new FieloPRM_BadgeAccount__c ();  
            badgeAcc2.F_PRM_Badge__c = badge2.id;
            badgeAcc2.F_PRM_Account__c = acc.id;

            insert new List<FieloPRM_BadgeAccount__c>{badgeAcc1,badgeAcc2};
            
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;
            
            list<String> listIds = new list<String>(); 
            
            listIds.add(acc.PRMUIMSID__c);
        
            FieloPRM_REST_GetProgLevelsByAccount rest = new FieloPRM_REST_GetProgLevelsByAccount();
            FieloPRM_REST_GetProgLevelsByAccount.getProgramLevelsByAccount(listIds );
        }
    }

    static testMethod void test5(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'certificationFirstName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'certificationLastName';
            member.FieloEE__Street__c = 'test';
       
            insert member;
            
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Certification';
            badgeAux.F_PRM_TypeSelection__c = 'Certification';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;
              
            List<FieloPRM_REST_PostCertification.certificationWrapper> listWrapp = new List<FieloPRM_REST_PostCertification.certificationWrapper>();
            List<FieloPRM_REST_PostCertification.certificationWrapper> listWrapp2 = new List<FieloPRM_REST_PostCertification.certificationWrapper>();
            
            FieloPRM_REST_PostCertification.certificationWrapper wrapp = new FieloPRM_REST_PostCertification.certificationWrapper(); 
            wrapp.name = 'TestName';
            wrapp.uniqueCode = 'Test' ;
            wrapp.dateFrom = Date.Today();
            wrapp.dateTo = wrapp.dateFrom.addDays(1);
            
            listWrapp.add(wrapp);

            FieloPRM_REST_PostCertification.certificationWrapper wrapp2 = new FieloPRM_REST_PostCertification.certificationWrapper(); 
            wrapp2.name = 'TestName1';
            wrapp2.uniqueCode = 'Test1' ;
            wrapp2.dateFrom = Date.Today();
            wrapp2.dateTo = wrapp2.dateFrom.addDays(1);
            
            listWrapp2.add(wrapp2);
            
            Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>> mapContactExtIdCertifications = new Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>>();
            Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>> mapContactExtIdCertifications2 = new Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>>();
            mapContactExtIdCertifications.put('test' , listWrapp );
            mapContactExtIdCertifications.put('test' , listWrapp2 );
            
            FieloPRM_REST_PostCertification rest = new FieloPRM_REST_PostCertification();
            FieloPRM_REST_PostCertification.postCertification(mapContactExtIdCertifications );
            FieloPRM_REST_PostCertification.postCertification(mapContactExtIdCertifications2 );
        }
    }

    static testMethod void test6(){
        
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'certificationFirstName';
            member.FieloEE__FirstName__c = 'certificationLastName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
       
            
            insert member;
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            insert con1;
            
            Contact con2 = new Contact();
            con2.FirstName = 'conFirsst';
            con2.LastName = 'conLasst';
            con2.SEContactID__c = 'test2';
            con2.PRMUIMSId__c = 'test2';
            insert con2;
            
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Certification';
            badgeAux.F_PRM_TypeSelection__c = 'Certification';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;
            
                
            FieloEE__RedemptionRule__c segment1 = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
              
            FieloEE__MemberSegment__c memberse = new FieloEE__MemberSegment__c();
            memberse.FieloEE__Segment2__c = segment1.id;
            memberse.FieloEE__Member2__c = member.id;
    
            List<FieloPRM_REST_PostCertification.certificationWrapper> listWrapp = new List<FieloPRM_REST_PostCertification.certificationWrapper>();
            
            FieloPRM_REST_PostCertification.certificationWrapper wrapp = new FieloPRM_REST_PostCertification.certificationWrapper(); 
            wrapp.name = 'TestName';
            wrapp.uniqueCode = 'Test' ;
            wrapp.dateFrom = Date.Today();
            wrapp.dateTo = wrapp.dateFrom.addDays(1);
            
            listWrapp.add(wrapp);
            
            Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>> mapContactExtIdCertifications = new Map<String,List<FieloPRM_REST_PostCertification.certificationWrapper>>();
            mapContactExtIdCertifications.put('test' , listWrapp );
            mapContactExtIdCertifications.put('test2' , listWrapp );
            
            FieloPRM_REST_PostCertification rest = new FieloPRM_REST_PostCertification();
            FieloPRM_REST_PostCertification.postCertification(mapContactExtIdCertifications );
        }
      
    }  
    
    static testMethod void test7(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'trainingName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'trainingFirstName';
            member.FieloEE__Street__c = 'tsst';
            
            insert member;
         
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'Test123';
            con1.PRMUIMSId__c = 'test';
            insert con1;
            
            Contact con2 = new Contact();
            con2.FirstName = 'conFirsst';
            con2.LastName = 'conLasst';
            con2.SEContactID__c = 'test2';
            con2.PRMUIMSId__c = 'test2';
            insert con2;
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Certification';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;
              
            List<FieloPRM_REST_PostTraining.trainingWrapper> listWrapp = new List<FieloPRM_REST_PostTraining.trainingWrapper>();
            
            FieloPRM_REST_PostTraining.trainingWrapper wrapp = new FieloPRM_REST_PostTraining.trainingWrapper(); 
            wrapp.name = 'TestName';
            wrapp.uniqueCode = 'Test' ;
            wrapp.porcentage = 40;
            wrapp.dateFrom = Date.Today();
            wrapp.dateTo = wrapp.dateFrom.addDays(1);
            
            listWrapp.add(wrapp);
            
            Map<String,List<FieloPRM_REST_PostTraining.trainingWrapper>> mapContactExtIdTrainings = new Map<String,List<FieloPRM_REST_PostTraining.trainingWrapper>>();
            mapContactExtIdTrainings.put('Test123' , listWrapp );
            
            FieloPRM_REST_PostTraining rest = new FieloPRM_REST_PostTraining ();
            FieloPRM_REST_PostTraining.postTraining(mapContactExtIdTrainings);
        }
    }

    static testMethod void test8(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'MaarcoLast';
            member.FieloEE__FirstName__c = 'MaarcoFirst'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'tests';
          
            insert member;
            
            FieloPRM_ValidBadgeTypes__c validBadgeAux = new FieloPRM_ValidBadgeTypes__c(FieloPRM_ApiExtId__c = 'test', 
                    FieloPRM_Type__c = 'Program Level', name = 'test' , FieloPRM_CreateEvent__c = true ,FieloPRM_CreateBadgeMember__c = true);
            insert validBadgeAux;


            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_BadgeAPIName__c = 'asd'; 
            insert badge;

            badge.F_PRM_Type__c = 'Program Level'; 
            Update badge;
           

            FieloPRM_BadgeAccount__c badgeAcc = new FieloPRM_BadgeAccount__c ();  
            badgeAcc.F_PRM_Badge__c = badge.id;
            badgeAcc.F_PRM_Account__c = acc.id;
            
            insert badgeAcc;
                       
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.PRMUIMSID__c = '123';
            con1.SEContactID__c = 'test';
            update con1;
                     
            list<String> listIds = new list<String>();
            listIds.add(con1.id);
            
            Test.startTest();
        
            FieloPRM_REST_PostAddBadgeToContact rest = new FieloPRM_REST_PostAddBadgeToContact();
            Map<String ,List<String>> accToBadges = new Map<String ,List<String>>();
            List<String> aux = new List<String>();
            aux.add(badge.F_PRM_BadgeAPIName__c);
            
            accToBadges.put(con1.PRMUIMSID__c , aux);
            
            FieloPRM_REST_PostAddBadgeToContact.postAddBadgeToContact('Program Level', accToBadges);
                        
            FieloPRM_ValidBadgeTypes__c cusSetting = new FieloPRM_ValidBadgeTypes__c(); 
            cusSetting.FieloPRM_ApiExtId__c = 'asd' ;
            cusSetting.FieloPRM_Type__c = 'Test';
            cusSetting.name = 'test name';
            insert cusSetting;            
            
            FieloPRM_REST_PostAddBadgeToContact.postAddBadgeToContact('Program Level', accToBadges);
            
            cusSetting.FieloPRM_Type__c = 'Program Level';
            
            update cusSetting;
            
            FieloPRM_REST_PostAddBadgeToContact.postAddBadgeToContact('Program Level', accToBadges);
            
            Map<String ,List<String>> accToBadges2 = new Map<String ,List<String>>();
            list<String> listStringaux =  new list<String>();
            listStringaux.add('asd');
            accToBadges2.put('aa', listStringaux);
            FieloPRM_REST_PostAddBadgeToContact.postAddBadgeToContact('Program Level', accToBadges2);

            aux = accToBadges.get(con1.PRMUIMSID__c);
            aux.add('test');
            accToBadges.put(con1.PRMUIMSID__c , aux);
                
            try{
                FieloPRM_REST_PostAddBadgeToContact.postAddBadgeToContact('Program Level', accToBadges);
            }catch(Exception e){
            }
            
            Test.stopTest();
            
        }
    }
   
    static testMethod void test9(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            // startTest/stopTest block to run future method synchronously

            Test.startTest(); 
            
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'trainingName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'trainingFirstName';
            member.FieloEE__Street__c = 'tsst';
            
            insert member;
         
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            insert con1;
            
            Contact con2 = new Contact();
            con2.FirstName = 'conFirsst';
            con2.LastName = 'conLasst';
            con2.SEContactID__c = 'test2';
            con2.PRMUIMSId__c = 'test2';
            insert con2;
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Certification';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;   
            
            LIST<String> listBadgeMember = new LIST<String>();
            listBadgeMember.add(badMemAux.id);   

            FieloPRM_UTILS_BadgeMember.resetMissions(listBadgeMember);

            Test.stopTest();

        }

        // The future method will run after Test.stopTest();

    }

    static testMethod void test10(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;

        String fieldMission = '';

        Set<String> objectFields = Schema.SObjectType.FieloCH__ChallengeMission__c.fields.getMap().keySet();
        //dynamic
        if(objectFields.contains('fieloch__mission2__c')) {
            fieldMission = 'FieloCH__Mission2__';
        }else{
            fieldMission = 'FieloCH__Mission__';           
        }
        
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'trainingName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'trainingFirstName';
            member.FieloEE__Street__c = 'tsst';
            
            insert member;
         
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            insert con1;
            
            Contact con2 = new Contact();
            con2.FirstName = 'conFirsst';
            con2.LastName = 'conLasst';
            con2.SEContactID__c = 'test2';
            con2.PRMUIMSId__c = 'test2';
            insert con2;
            
            Test.startTest(); 

            Date fecha = system.today();

           /* FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(Name = 'Challenge TEST', FieloCH__Mode__c = 'Competition', FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', FieloCH__InitialDate__c = fecha.addDays(-5), FieloCH__FinalDate__c = fecha.addDays(5), FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, F_PRM_ChallengeFilter__c = 'Ch' + datetime.now());
            insert challenge;*/
            
            FieloCH__Challenge__c challenge = FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Certification';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;   
            
            LIST<String> listBadgeMember = new LIST<String>();
            listBadgeMember.add(badMemAux.id);   

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Average', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );

            FieloCH.ChallengeUtil.getMapRewardsByPosition(new fieloch__Challenge__c());

            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__Member__c = member.Id
            );
            insert challengeMember;

            FieloCH__ChallengeMission__c challengeMission = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__Order__c = 1
            );
            challengeMission.put(fieldMission + 'c', mission1.Id);
            insert challengeMission;

            FieloCH__ChallengeMissionMember__c challengeMissionMember = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );
            insert challengeMissionMember;
            
            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge);     

            challengeReward.FieloCH__Badge__c = badgeAux.Id;
            challengeReward.FieloCH__LogicalExpression__c = '(1 AND (2 AND 3))';
            
            update challengeReward;

            FieloPRM_UTILS_BadgeMember.resetMissions(listBadgeMember);
            
            FieloPRM_UTILS_BadgeMember.getExpirationDateText(date.today());

            Test.stopTest();

        }

        // The future method will run after Test.stopTest();

    }

}