@istest
public class VFC_Announcement_Test{
    static string regionlist_1;
    static Milestone1_Project__c plist1;
    static string setcount;
    static Announcement_Groups__c regionlist;
    static string s;
    static string group1;
    static string ctryldr;
    static string countries;
    static string Cntry;
    static string groupCountry1;
    static boolean check;
    static boolean check2;
    static string UserName1;
    static string grpcntry1;
    static string email1;
    static boolean check3;
    static string offeremail;
    static  list<VFC_Announcement.Innerclass> wrappervar;
    static  map<string,list<VFC_Announcement.innerclass2>> wrappervar2;
    static map<string,list<VFC_Announcement.innerclass3>> newsortwrapper3;
    static boolean isnullemail1;
    
    static testmethod void offerlaunch(){
        //User u1 = Utils_TestMethods.createStandardUser('TestUser'); 
        //Database.SaveResult UserInsertResult = Database.insert(u1, true); 
        /* Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         userROLE r = [Select id from userROLE where Name='CEO'];
        User u1 = new User(Alias = 'newUser1',userROLEId= r.id, Email='newuser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
        insert u1;*/
        
       user u1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
       
        system.debug('user id  is --->'+u1.id);
        Id rt = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();   
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c(RecordTypeId=rt,Name='Test',Offer_Name__c='offer to Test',Description__c='Offer Description',Leading_Business_BU__c='Cloud Services',
                    Sales_Date__c= system.today(),Forecast_Unit__c='Opportunities',Process_Type__c='PMP',Launch_Owner__c=u1.id,Marketing_Director__c=u1.id);
        insert olc;
        
        list<country__c> ctrylist = new list<country__c>();
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        ctrylist.add(c);
        country__c c1 = new country__c(name = 'Italy',countrycode__c = 'IT');
        ctrylist.add(c1);
        Insert ctrylist;
        list<Milestone1_Project__c> mpjcts = new list<Milestone1_Project__c>();
        Milestone1_Project__c pjct = new Milestone1_Project__c(name='Test Project1',offer_launch__c = olc.id,country__c=c.id,Country_Launch_Leader__c=u1.id,Country_Announcement_Date__c=system.today(),region__c='APAC');
        mpjcts.add(pjct);
        Milestone1_Project__c pjct1 = new Milestone1_Project__c(name='Test Project2',offer_launch__c = olc.id,country__c=c1.id,Country_Launch_Leader__c=u1.id,Country_Announcement_Date__c=system.today(),region__c='APAC');
        mpjcts.add(pjct1);
        Insert mpjcts; 
        //insert Announcement
        Announcement__c annc = new Announcement__c(Name = 'Test Announcement',Offer_Lifecycle__c=olc.id);
        insert annc;
        //Announcement Groups
        list<Announcement_Groups__c> annclist = new list<Announcement_Groups__c>();
        Announcement_Groups__c ag= new Announcement_Groups__c(Country__c=c.id,group_name__c ='Group-1');
        annclist.add(ag);
         Announcement_Groups__c ag1= new Announcement_Groups__c(Country__c=c1.id,group_name__c ='Group-2');
         annclist.add(ag1);
        Insert  annclist;
        // insert announcement Stakeholders
        list<AnnouncementStakeholder__c> anncSh = new list<AnnouncementStakeholder__c>();
        AnnouncementStakeholder__c ash1 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, Email__c ='xyz@abc.com' );
        anncSh.add(ash1);
        AnnouncementStakeholder__c ash2 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id,Email__c ='xyz123@abc.com');
        anncSh.add(ash2);
        AnnouncementStakeholder__c ash3 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, Email__c ='123xyz@abc.com');
        anncSh.add(ash3);
       AnnouncementStakeholder__c ash4 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, UserName__c =u1.id );
        anncSh.add(ash4);
        insert anncSh;
        //Custom Setting
        CS_Announcement__c cs = new CS_Announcement__c(Name='Test',EmailTemplate__c = 'Coin_bFO_Offer_AnnouncementEmail_Template',Offer_Record_Type__c ='Offer Launch - Product');
         insert cs;     
        Apexpages.currentpage().getparameters().put('id',olc.id);
         Apexpages.currentpage().getparameters().put('annid',annc.id);
        VFC_Announcement announce = new VFC_Announcement();
        announce.offeremail = 'newuser08977@testorg.com';
        VFC_Announcement.innerclass testin= new VFC_Announcement.innerclass(group1,check,countries);
        testin.groups='Group-1';
        testin.chk = true;
        testin.country='India,Italy';
        announce.wrappervar = new list<VFC_Announcement.innerclass>{testin};
        
        VFC_Announcement.innerclass2 testin1= new VFC_Announcement.innerclass2(Cntry,check2,ctryldr,groupCountry1);
        testin1.CntryLeader='Testing1';
        testin1.chk2= true;
        testin1.Country='India';
        testin1.groupCountry = 'Group-1India';
        //system.debug('wrappervar2 in test class--->'+testin1);
        announce.wrappervar2= new map<string,list<VFC_Announcement.innerclass2>>();
        announce.wrappervar2.put('Group-1',new list<VFC_Announcement.innerclass2>{testin1});
        
        VFC_Announcement.innerclass3 testin3= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin3.UserName = 'newuser2@testorg.com';
        testin3.grpcntry='Group-1India';
        testin3.email = '';
        testin3.chk3 = true;  testin3.isnullemail = true;
        VFC_Announcement.innerclass3 testin4= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin4.UserName = '';
        testin4.grpcntry='Group-1India';
        testin4.email = 'newuser3@testorg.com';
        testin4.chk3 = true; testin4.isnullemail = true;
        announce.newsortwrapper3= new map<string,list<VFC_Announcement.innerclass3>>();
        announce.newsortwrapper3.put('Group-1India',new list<VFC_Announcement.innerclass3>{testin3,testin4});
        
        announce.ContinueMethod();
        announce.cancelmethod();
        
    }
    static testmethod void offerWithdraw(){
       user u1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];  
        
        Id rt = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Withdrawal - Product').getRecordTypeId();   
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c(RecordTypeId=rt,Name='Test',Offer_Name__c='offer to Test',Description__c='Offer Description',Leading_Business_BU__c='Cloud Services',
                    Withdrawal_Owner__c=u1.id);
        insert olc;
        
        list<country__c> ctrylist = new list<country__c>();
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        ctrylist.add(c);
        country__c c1 = new country__c(name = 'Italy',countrycode__c = 'IT');
        ctrylist.add(c1);
        Insert ctrylist;
        list<Milestone1_Project__c> mpjcts = new list<Milestone1_Project__c>();
        Milestone1_Project__c pjct = new Milestone1_Project__c(name='Test Project1',offer_launch__c = olc.id,country__c=c.id,Country_Launch_Leader__c=u1.id,Country_Announcement_Date__c=system.today(),region__c='APAC');
        mpjcts.add(pjct);
        Milestone1_Project__c pjct1 = new Milestone1_Project__c(name='Test Project2',offer_launch__c = olc.id,country__c=c1.id,Country_Launch_Leader__c=u1.id,Country_Announcement_Date__c=system.today(),region__c='APAC');
        mpjcts.add(pjct1);
        Insert mpjcts; 
        //insert Announcement
        Announcement__c annc = new Announcement__c(Name = 'Test Announcement',Offer_Lifecycle__c=olc.id);
        insert annc;
        list<Announcement_Groups__c> annclist = new list<Announcement_Groups__c>();
        Announcement_Groups__c ag= new Announcement_Groups__c(Country__c=c.id,group_name__c ='Group-1');
        annclist.add(ag);
         Announcement_Groups__c ag1= new Announcement_Groups__c(Country__c=c1.id,group_name__c ='Group-2');
         annclist.add(ag1);
        Insert  annclist;
        // insert announcement Stakeholders
        list<AnnouncementStakeholder__c> anncSh = new list<AnnouncementStakeholder__c>();
        AnnouncementStakeholder__c ash1 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, Email__c ='xyz@abc.com' );
        anncSh.add(ash1);
        AnnouncementStakeholder__c ash2 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id,Email__c ='xyz123235@abc.com' );
        anncSh.add(ash2);
        AnnouncementStakeholder__c ash3 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id,Email__c ='adfadsfxyz@abc.com' );
        anncSh.add(ash3);
       AnnouncementStakeholder__c ash4 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, UserName__c =u1.id );
        anncSh.add(ash4);
        insert anncSh;
        CS_Announcement__c cs = new CS_Announcement__c(Name='Test',EmailTemplate__c = 'Coin_bFO_Offer_withdraw_AnnouncementEmail_Template',Offer_Record_Type__c ='Offer Withdrawal - Product');
         insert cs;     
        Apexpages.currentpage().getparameters().put('id',olc.id);
         Apexpages.currentpage().getparameters().put('annid',annc.id);
        VFC_Announcement announce = new VFC_Announcement();
        
        VFC_Announcement.innerclass testin= new VFC_Announcement.innerclass(group1,check,countries);
        testin.groups='Group-1';
        testin.chk = true;
        testin.country='India,Italy';
        announce.wrappervar = new list<VFC_Announcement.innerclass>{testin};
        
        VFC_Announcement.innerclass2 testin1= new VFC_Announcement.innerclass2(Cntry,check2,ctryldr,groupCountry1);
        testin1.CntryLeader='Testing1';
        testin1.chk2= true;
        testin1.Country='India';
        testin1.groupCountry = 'Group-1India';
        system.debug('wrappervar2 in test class--->'+testin1);
        announce.wrappervar2= new map<string,list<VFC_Announcement.innerclass2>>();
        announce.wrappervar2.put('Group-1',new list<VFC_Announcement.innerclass2>{testin1});
        VFC_Announcement.innerclass3 testin3= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin3.UserName = '';
        testin3.grpcntry='Group-1India';
        testin3.email = 'newuser2@testorg.com';
        testin3.chk3 = true;  testin3.isnullemail = true;
        VFC_Announcement.innerclass3 testin4= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin4.UserName = 'newuser3@testorg.com';
        testin4.grpcntry='Group-1India';
        testin4.email = '';
        testin4.chk3 = true; testin4.isnullemail = true;
        announce.newsortwrapper3= new map<string,list<VFC_Announcement.innerclass3>>();
        announce.newsortwrapper3.put('Group-1India',new list<VFC_Announcement.innerclass3>{testin3,testin4});
        
        announce.ContinueMethod();
        announce.cancelmethod();

    }
    // if no id is passed in the URL
    /*static testmethod void forerror(){
        
        //Apexpages.currentpage().getparameters().put('id','');
        VFC_Announcement announce = new VFC_Announcement();
        //announce.ContinueMethod();
    }*/
    //If nothing is selected
    static testmethod void forError2(){
        user u1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
        Id rt = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();   
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c(RecordTypeId=rt,Name='Test',Offer_Name__c='offer to Test',Description__c='Offer Description',Leading_Business_BU__c='Cloud Services',
                    Sales_Date__c= system.today(),Forecast_Unit__c='Opportunities',Process_Type__c='PMP',Launch_Owner__c=u1.id,Marketing_Director__c=u1.id);
        insert olc;
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        Insert c;
        Milestone1_Project__c pjct = new Milestone1_Project__c(name='Test Project1',offer_launch__c = olc.id,country__c=c.id,Country_Launch_Leader__c=u1.id,Country_Announcement_Date__c=system.today(),region__c='APAC');
        Insert pjct;
         CS_Announcement__c cs = new CS_Announcement__c(Name='Test',EmailTemplate__c = 'Coin_bFO_Offer_AnnouncementEmail_Template',Offer_Record_Type__c ='Offer Launch - Product');
         insert cs; 
        Apexpages.currentpage().getparameters().put('id',olc.id);
        ApexPages.currentPage().getParameters().put('accPriview','emailpreview');
        VFC_Announcement announce = new VFC_Announcement();
        announce.ContinueMethod();
        announce.cancelmethod();
        announce.AnncEmailPreview();
    }
    
    //If announcement stakeholders are selected more than 10 is selected
    static testmethod void forError3(){
        user u1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
        Id rt = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();   
        Offer_Lifecycle__c olc = new Offer_Lifecycle__c(RecordTypeId=rt,Name='Test',Offer_Name__c='offer to Test',Description__c='Offer Description',Leading_Business_BU__c='Cloud Services',
                    Sales_Date__c= system.today(),Forecast_Unit__c='Opportunities',Process_Type__c='PMP',Launch_Owner__c=u1.id,Marketing_Director__c=u1.id);
        list<country__c> ctrylist = new list<country__c>();
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        ctrylist.add(c);
        country__c c1 = new country__c(name = 'Italy',countrycode__c = 'IT');
        ctrylist.add(c1);
        Insert ctrylist;
        Milestone1_Project__c pjct = new Milestone1_Project__c(name='Test Project1',offer_launch__c = olc.id,country__c=c.id,Country_Launch_Leader__c=u1.id,Country_Announcement_Date__c=system.today(),region__c='APAC');
        Insert pjct;
         CS_Announcement__c cs = new CS_Announcement__c(Name='Test',EmailTemplate__c = 'Coin_bFO_Offer_AnnouncementEmail_Template',Offer_Record_Type__c ='Offer Launch - Product');
         insert cs; 
         //insert Announcement
        Announcement__c annc = new Announcement__c(Name = 'Test Announcement',Offer_Lifecycle__c=olc.id);
        insert annc;
         list<Announcement_Groups__c> annclist = new list<Announcement_Groups__c>();
        Announcement_Groups__c ag= new Announcement_Groups__c(Country__c=c.id,group_name__c ='Group-1');
        annclist.add(ag);
         Announcement_Groups__c ag1= new Announcement_Groups__c(Country__c=c1.id,group_name__c ='Group-2');
         annclist.add(ag1);
        Insert  annclist;
        // insert announcement Stakeholders
        list<AnnouncementStakeholder__c> anncSh = new list<AnnouncementStakeholder__c>();
        AnnouncementStakeholder__c ash1 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, Email__c ='xyz@abc.com' );
        anncSh.add(ash1);
        AnnouncementStakeholder__c ash2 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id,Email__c ='xyz123235@abc.com' );
        anncSh.add(ash2);
        AnnouncementStakeholder__c ash3 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id,Email__c ='adfadsfxyz@abc.com' );
        anncSh.add(ash3);
       AnnouncementStakeholder__c ash4 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, UserName__c =u1.id );
        anncSh.add(ash4);
        AnnouncementStakeholder__c ash5 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, Email__c ='xyz1231234134@abc.com' );
        anncSh.add(ash5);
        AnnouncementStakeholder__c ash6 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id,Email__c ='xyz1232351234123@abc.com' );
        anncSh.add(ash6);
        AnnouncementStakeholder__c ash7 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id,Email__c ='adfadsfxyz2341234@abc.com' );
        anncSh.add(ash7);
       AnnouncementStakeholder__c ash8 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, Email__c ='adfadsfxyz2341234asdfa@abc.com' );
        anncSh.add(ash8);
        AnnouncementStakeholder__c ash9= new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, Email__c ='xyzasdfasdfdfasd@abc.com' );
        anncSh.add(ash9);
        AnnouncementStakeholder__c ash10 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id,Email__c ='xyz123235adsfasd@abc.com' );
        anncSh.add(ash10);
        AnnouncementStakeholder__c ash11 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id,Email__c ='adfadsfxysdfadfadfaz@abc.com' );
        anncSh.add(ash11);
       AnnouncementStakeholder__c ash12 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, UserName__c =u1.id );
        anncSh.add(ash12);
        insert anncSh;
       Apexpages.currentpage().getparameters().put('id',olc.id);
         Apexpages.currentpage().getparameters().put('annid',annc.id);
        VFC_Announcement announce = new VFC_Announcement();
        
        VFC_Announcement.innerclass testin= new VFC_Announcement.innerclass(group1,check,countries);
        testin.groups='Group-1';
        testin.chk = true;
        testin.country='India,Italy';
        announce.wrappervar = new list<VFC_Announcement.innerclass>{testin};
        
        VFC_Announcement.innerclass2 testin1= new VFC_Announcement.innerclass2(Cntry,check2,ctryldr,groupCountry1);
        testin1.CntryLeader='Testing1';
        testin1.chk2= true;
        testin1.Country='India';
        testin1.groupCountry = 'Group-2Italy';
        VFC_Announcement.innerclass2 testin2= new VFC_Announcement.innerclass2(Cntry,check2,ctryldr,groupCountry1);
        testin1.CntryLeader='Testing1';
        testin1.chk2= true;
        testin1.Country='India';
        testin1.groupCountry = 'Group-1India';
        //system.debug('wrappervar2 in test class--->'+testin1);
        announce.wrappervar2= new map<string,list<VFC_Announcement.innerclass2>>();
        announce.wrappervar2.put('Group-1',new list<VFC_Announcement.innerclass2>{testin1});
        VFC_Announcement.innerclass3 testin3= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin3.UserName = '';
        testin3.grpcntry='Group-1India';
        testin3.email = 'newuser2@testorg.com';
        testin3.chk3 = true;  testin3.isnullemail = true;
        VFC_Announcement.innerclass3 testin4= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin4.UserName = 'newuser3@testorg.com';
        testin4.grpcntry='Group-1India';
        testin4.email = '';
        testin4.chk3 = true; testin4.isnullemail = true;
        VFC_Announcement.innerclass3 testin5= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin5.UserName = '';
        testin5.grpcntry='Group-1India';
        testin5.email = 'newuser2@testorg.com';
        testin5.chk3 = true;  testin5.isnullemail = true;
        VFC_Announcement.innerclass3 testin6= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin6.UserName = 'newuser3@testorg.com';
        testin6.grpcntry='Group-1India';
        testin6.email = '';
        testin6.chk3 = true; testin6.isnullemail = true;
        VFC_Announcement.innerclass3 testin7= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin7.UserName = '';
        testin7.grpcntry='Group-1India';
        testin7.email = 'newuser2@testorg.com';
        testin7.chk3 = true;  testin7.isnullemail = true;
        VFC_Announcement.innerclass3 testin8= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin8.UserName = 'newuser3@testorg.com';
        testin8.grpcntry='Group-1India';
        testin8.email = '';
        testin8.chk3 = true; testin8.isnullemail = true;
        VFC_Announcement.innerclass3 testin9= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin9.UserName = '';
        testin9.grpcntry='Group-1India';
        testin9.email = 'newuser2@testorg.com';
        testin9.chk3 = true;  testin9.isnullemail = true;
        VFC_Announcement.innerclass3 testin10= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin10.UserName = 'newuser3@testorg.com';
        testin10.grpcntry='Group-1India';
        testin10.email = '';
        testin10.chk3 = true; testin10.isnullemail = true;
        VFC_Announcement.innerclass3 testin11= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin11.UserName = '';
        testin11.grpcntry='Group-1India';
        testin11.email = 'newuser2@testorg.com';
        testin11.chk3 = true;  testin11.isnullemail = true;
        VFC_Announcement.innerclass3 testin12= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin12.UserName = 'newuser3@testorg.com';
        testin12.grpcntry='Group-1India';
        testin12.email = '';
        testin12.chk3 = true; testin12.isnullemail = true;
        VFC_Announcement.innerclass3 testin13= new VFC_Announcement.innerclass3(UserName1,grpcntry1,email1,check3,isnullemail1);
        testin13.UserName = '';
        testin13.grpcntry='Group-1India';
        testin13.email = 'newuser2@testorg.com';
        testin13.chk3 = true;  testin13.isnullemail = true;
        announce.newsortwrapper3= new map<string,list<VFC_Announcement.innerclass3>>();
        announce.newsortwrapper3.put('Group-1India',new list<VFC_Announcement.innerclass3>{testin3,testin4,testin5,testin6,testin7,testin8,testin9,testin10,testin11,testin12,testin13});
        
        announce.ContinueMethod();
        announce.cancelmethod();
    } 
    
}