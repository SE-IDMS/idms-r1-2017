@isTest 
private class VFC_CaseTransfer_TEST {
    static testMethod void testMassCaseTransferMethod() { 
        // creating Test Data
        String strCaseNumber;
        Account  objAccount = Utils_TestMethods.createAccount(userInfo.getUserId()); 
        insert objAccount;
        Contact   objContact = Utils_TestMethods.createContact(objAccount.id,'TestContact');
        insert objContact;
        Case objCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        insert objCase;
        
        List<Case> lstCase = new List<Case>();
        lstCase.add(objCase);
        
        PageReference pageRef = Page.VFP_CaseTransfer;
        Test.setCurrentPageReference(pageRef);         
        ApexPages.StandardSetController caseTransferSetController = new ApexPages.StandardSetController(lstCase);
        caseTransferSetController.setSelected(lstCase);      
        VFC_CaseTransfer CaseTransfer = new VFC_CaseTransfer(caseTransferSetController);
        VFC_CaseTransfer.updateRecords(JSON.serialize(lstCase),false,false);  
    }
    static testMethod void testSingleCaseTransferMethod() { 
        // creating Test Data
        String strCaseNumber;
        Account  objAccount = Utils_TestMethods.createAccount(userInfo.getUserId()); 
        insert objAccount;
        Contact   objContact = Utils_TestMethods.createContact(objAccount.id,'TestContact');
        insert objContact;
        Case objCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        insert objCase;
        
        PageReference pageRef = Page.VFP_CaseTransfer;
        Test.setCurrentPageReference(pageRef);         
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC_CaseTransfer CaseTransfer = new VFC_CaseTransfer(CaseStandardController);
    }   
}