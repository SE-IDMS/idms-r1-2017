public class VFC_CaseSearch {
    public string CaseNum;
    public String ErrorPage;
    public PageReference returnPage;
    public VFC_CaseSearch() {
        CaseNum = ApexPages.currentPage().getParameters().get('CaseNum');
        ErrorPage= ApexPages.currentPage().getParameters().get('errorURL');
    }
    public PageReference returnPage() {
        if(ErrorPage !=null && ErrorPage !='' && !(ErrorPage.containsignorecase('/apex/VFP_CaseSearch_HomePageComponent')))
            returnPage= new PageReference(ErrorPage);
        else
            returnPage= new PageReference('/home/home.jsp'); 
        return returnPage;
            
    }
    public PageReference searchCaseNumber() {
        if(CaseNum!=null || CaseNum !=''){
            List<Case> lstCase = new List<Case>([Select Id from Case where CaseNumber =:CaseNum]);
            if(lstCase.size()==1){
                returnPage= new PageReference('/'+lstCase[0].Id );
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, CaseNum + ' is not a valid Case Number'));
                returnPage = null;
            }
        }
        return returnPage;

    }
    
    
        
}