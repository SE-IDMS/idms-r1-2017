@isTest
public class VFC_updateProgramLevelBrand_Test{

  static testMethod void testMe(){
     User us = new user(id = userinfo.getuserId());
                us.BypassVR__c = true;
            update us;
            
      System.RunAs(us){
              PartnerProgram__c ppm=new PartnerProgram__c();
                    ppm.name='APC Value Added Tester';
                    ppm.ProgramType__c='Business';
                    insert ppm;

              ProgramLevel__c pml = new ProgramLevel__c();
                    pml.Description__c='This is a Test Description';
                    pml.ModifiablebyCountry__c='Yes';
                    pml.Name='TestRegistration';
                    pml.PartnerProgram__c=ppm.Id;
                    insert pml;
              Brand__c brd = new Brand__c();
                    brd.Name='APC';
                    brd.IsAssociatedSchneiderBrand__c = true;
                    insert brd;
              ProgramLevelBrand__c  plb =new ProgramLevelBrand__c ();
                    plb.ProgramLevel__c = pml.Id;
                    plb.Brand__c = brd.Id;
                    insert plb;
        Test.startTest();
        Test.SetCurrentPageReference(New PageReference('Page.VFC_updateProgramLevelBrand'));
        ApexPages.Standardcontroller sc2 = New ApexPages.StandardController(pml);
        //System.CurrentPageReference().getParameters().put('id',plb.Id); 
        VFC_updateProgramLevelBrand plb2 = new VFC_updateProgramLevelBrand(sc2); 
        plb2.hasError=false;
        plb2.save();
        VFC_updateProgramLevelBrand.throwExceptionInTest = False;
        plb2.save();
        Test.stopTest();
        
    }

}
}