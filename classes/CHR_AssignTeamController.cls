public with sharing class CHR_AssignTeamController {
    public ApexPages.StandardController controller{get;set;}
    public CHR_ChangeReq__c oChangeReq {get;set;}

    /*====================
    CONSTRUCTOR
    ========================*/
    public CHR_AssignTeamController(ApexPages.StandardController controller){
        this.controller = controller;
        oChangeReq = (CHR_ChangeReq__c )controller.getRecord();
    }
    
    public PageReference assignSolutionTeam(){
        
        if (oChangeReq.SolutionTeam__c <> null){            
             oChangeReq.OwnerId = [select Queue__c from REF_TeamRef__c where id=:oChangeReq.SolutionTeam__c].Queue__c;
            update oChangeReq;
        } else return null;
        
        return new PageReference('/'+controller.getId());
    }
    
    public PageReference assignDeliveryTeam(){
        if (oChangeReq.DeliveryTeam__c <> null){
             oChangeReq.OwnerId = [select Queue__c from REF_TeamRef__c where id=:oChangeReq.DeliveryTeam__c].Queue__c;
            update oChangeReq;
        } else return null;
        
        return new PageReference('/'+controller.getId());
    }
    
    public PageReference assignBusinessTeam(){
        if (oChangeReq.TeamReference__c <> null){
             oChangeReq.OwnerId = [select Queue__c from REF_TeamRef__c where id=:oChangeReq.TeamReference__c].Queue__c;
            update oChangeReq;
        } else return null;
        
        return new PageReference('/'+controller.getId());
    }
    
    
    public static testmethod void testAssignation(){
    User runAsUser = Utils_TestMethods.createAdminUser('tst1');
    test.startTest();
    insert runAsUser;
    System.runAs(runAsUser)
    {    
        REF_TeamRef__c testSolutionTeam = new REF_TeamRef__c();
        Group testGroup = new Group(Type = 'Queue', Name='TestGroup');
        insert testGroup;
        QueueSObject qs = new QueueSObject(QueueId = testGroup.Id, SObjectType = 'CHR_ChangeReq__c');
        insert qs;        
        testSolutionTeam.Name = 'Test Team';
        testSolutionTeam.Type__c = 'Solution Team';
        //testSolutionTeam.Queue__c = [select Id from Group where Type='Queue' limit 1].Id;
        testSolutionTeam.Queue__c = testGroup.Id;
        insert testSolutionTeam;
        
        REF_TeamRef__c testDeliveryTeam = new REF_TeamRef__c();
        testDeliveryTeam.Name = 'Test Team';
        testDeliveryTeam.Type__c = 'Delivery Team';
        //testDeliveryTeam.Queue__c = [select Id from Group where Type='Queue' limit 1].Id;
        testDeliveryTeam.Queue__c = testGroup.Id;
        insert testDeliveryTeam;
        
        REF_TeamRef__c testBusinessTeam = new REF_TeamRef__c();
        testBusinessTeam.Name = 'Test Team';
        testBusinessTeam.Type__c = 'Business Team';
        //testBusinessTeam.Queue__c = [select Id from Group where Type='Queue' limit 1].Id;
        testBusinessTeam.Queue__c = testGroup.Id;
        insert testBusinessTeam;
        
        //test.startTest();
        
        CHR_ChangeReq__c testCHR = new CHR_ChangeReq__c();
        testCHR.Name = 'test';
        testCHR.SolutionTeam__c = testSolutionTeam.Id;
        testCHR.DeliveryTeam__c  =testDeliveryTeam.Id;
        testCHR.TeamReference__c = testBusinessTeam.id;
        insert testCHR;
        
        ApexPages.StandardController ctl = new ApexPages.StandardController(testCHR);
        
        
        CHR_AssignTeamController teamCtl = new CHR_AssignTeamController(ctl);
        PageReference chrPage = teamCtl.assignDeliveryTeam();
        chrPage = teamCtl.assignSolutionTeam();
        chrPage = teamCtl.assignBusinessTeam();
        
        //added on June 14, 2011
        //start

        testCHR = new CHR_ChangeReq__c();
        testCHR.Name = 'test2';
        testCHR.SolutionTeam__c = null;
        testCHR.DeliveryTeam__c  = null;
        testCHR.TeamReference__c = null;
        insert testCHR;
        
        ctl = new ApexPages.StandardController(testCHR);
        
        
        teamCtl = new CHR_AssignTeamController(ctl);
        chrPage = teamCtl.assignDeliveryTeam();
        chrPage = teamCtl.assignSolutionTeam();
        chrPage = teamCtl.assignBusinessTeam();     
        
        //end
        
        
        


        
        
        
    }    
    test.stopTest();    
    }

}