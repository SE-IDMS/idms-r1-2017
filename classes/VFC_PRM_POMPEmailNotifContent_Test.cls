@isTest
private class VFC_PRM_POMPEmailNotifContent_Test{
    static testMethod void myUnitTest1() {
        Test.startTest();
        //Country__c newCountry = Utils_TestMethods.createCountry();
        //insert newCountry;
        
        //User newUser = Utils_TestMethods.createStandardUser('TestUser');
        //insert newUser;
        
        //OppAgreement__c objAgree = Utils_TestMethods.createFrameAgreement(newUser.Id);
        //insert objAgree;
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity  objOpp = Utils_TestMethods.createOpportunity(acc.Id);
        insert objOpp;
        
        CS_PRM_ApexJobSettings__c CSApexSet = new CS_PRM_ApexJobSettings__c(Name = 'Opportunity', QueryFields__c = 'Id,Name,SEReference__c,Account.Name,Account.Phone,AccountId, (SELECT Id,Contact__c,ContactRole__c,Contact__r.Name,Contact__r.WorkPhone__c,Contact__r.Email FROM Value_Chain_Players__r WHERE Contact__c!=null AND ContactRole__c= \'Primary Customer Contact\')');
        insert CSApexSet;
        
        
        VFC_PRM_POMPEmailNotificationContent  POMPEmailNotifi = new VFC_PRM_POMPEmailNotificationContent();
        POMPEmailNotifi.RecptId = null;
        POMPEmailNotifi.OpptyId = null;
        POMPEmailNotifi.OrfId = null;
        POMPEmailNotifi.RecptLanguage = null;
        POMPEmailNotifi.NotificationFor = null;
        POMPEmailNotifi.RecordId = objOpp.Id;
        POMPEmailNotifi.sObjectType = 'Opportunity';
        Opportunity opp = POMPEmailNotifi.OpportunityToUse;
        //Opportunity OrfToUse1 = POMPEmailNotifi.OrfToUse;
        //OPP_ValueChainPlayers__c VcpToUse1 = POMPEmailNotifi.VcpToUse;
        //VFC_PRM_POMPEmailNotificationContent.ObjectInstance(POMPEmailNotifi.sObjectType, POMPEmailNotifi.RecordId);
        Test.stopTest();
    }
    static testMethod void myUnitTest2() {
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0) {
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            user.BypassWF__c = false;
            user.BypassVR__c = true;
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
                         
            system.runas(user) {
                Test.startTest();
                Country__c country= new Country__c(Name = 'Canada', CountryCode__c = 'CA');
                insert country; 
        
                Account acc = Utils_TestMethods.createAccount();
                acc.Country__c = country.Id;
                acc.BillingCountry = 'CA';
                insert acc;
        
                PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
                //globalProgram.TECH_CountriesId__c = TestCountry.id;
                globalPRogram.recordtypeid = Label.CLMAY13PRM15;
                globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
                insert globalProgram;
        
                PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, null);
                countryProgram.ProgramStatus__c = 'Active';
                insert countryProgram;
        
                OpportunityRegistrationForm__c ORF1 = new OpportunityRegistrationForm__c( Status__c='Pending', 
                                          AccountName__c= acc.Name,
                                          AccountCountry__c = acc.Country__c,
                                          Amount__c = 100000,
                                          ContactEmail__c='Test@test.com',
                                          OpportunityType__c='Standard',
                                          OpportunityScope__c='ENMVP - SECONDARY MV PRODUCTS',
                                          ExpectedOrderDate__c=System.Today(),
                                          ContactFirstName__c='Test',
                                          ContactLastName__c='User',
                                          ProjectName__c='Test Project',
                                          ProductType__c = 'BDIBS',
                                          PartnerProgram__c = countryProgram.id,
                                          Tech_CheckORFStatusOnSave__c='True');
                Insert ORF1;
        
                CS_PRM_ApexJobSettings__c CSApexSet = new CS_PRM_ApexJobSettings__c(Name = 'OpportunityRegistrationForm__c', QueryFields__c = 'Name, ORFID__c, ProjectName__c, RecordTypeID, EndUserType__c, AccountName__c, AccountCity__c, ContactFirstName__c, ContactLastName__c, ProjectCity__c, Tech_LinkedOpportunity__r.SEReference__c,AdditionalComments__c,RejectReason__c');
                insert CSApexSet;
        
        
                VFC_PRM_POMPEmailNotificationContent  POMPEmailNotifi = new VFC_PRM_POMPEmailNotificationContent();
                POMPEmailNotifi.RecptId = null;
                POMPEmailNotifi.OpptyId = null;
                POMPEmailNotifi.OrfId = null;
                POMPEmailNotifi.RecptLanguage = null;
                POMPEmailNotifi.NotificationFor = null;
                POMPEmailNotifi.RecordId = ORF1.Id;
                POMPEmailNotifi.sObjectType = 'OpportunityRegistrationForm__c';
                //Opportunity opp = POMPEmailNotifi.OpportunityToUse;
                OpportunityRegistrationForm__c OrfToUse1 = POMPEmailNotifi.OrfToUse;
                //OPP_ValueChainPlayers__c VcpToUse1 = POMPEmailNotifi.VcpToUse;
                //VFC_PRM_POMPEmailNotificationContent.ObjectInstance(POMPEmailNotifi.sObjectType, POMPEmailNotifi.RecordId);
                Test.stopTest();
            }
        }
    }
}