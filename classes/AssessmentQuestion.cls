global class AssessmentQuestion
{

    // bFO ID of Assessment
    WebService string ID { get; set; }
    
    // Name of the Assessment
    WebService string ASSESSMENTID { get; set; }
    
    // Description of the Assessment
    WebService string QUESTIONDESCRIPTION { get; set; }

    // Maximum Score of the Assessment
    WebService string ANSWERTYPE { get; set; }

    // Program of the Assessment
    WebService string SEQUENCE { get; set; }

    // Program Level of the Assessment
    WebService string WEIGHTAGE { get; set; }


    // LastUpdatetdDate timestamp in bFO
    WebService Datetime SDH_VERSION { get; set; }
    
    public AssessmentQuestion() {
       
    }
}