public with sharing class VFC_BoxAuthorization {   
    public string MessageToShow {get;set;}
    public static boolean strBoxblean=false;
    //bfobox@gmail.com BOX CLIENT ID and SecretCode 
    private final string boxClientID =label.CLAPRIL15ELLAbFO02;//'d4jyna8orwx97jn44yshqniimnjhfejn';//label.CLAPRIL15ELLAbFO02;//
    private final string boxSecretCode =label.CLAPRIL15ELLAbFO03;//'VfUKaBPAGrOylHXncOLZcDVS5lewqv5x';//label.CLAPRIL15ELLAbFO03;
    
    //Pro
    //private final string boxClientID ='v363zs5973gb9stitownjzq4jshn5w2m';
    //private final string boxSecretCode ='AsW2HfisiH5cE8Gj6mdQQ89N11bIZJa1';
    private final string redirectURI = label.CLAPR15ELLAbFO16;//'https://cs22.salesforce.com/apex/VFP_BoxAuthorization';//'https://c.na9.visual.force.com/apex/boxconnect';
    private string codeFromBox;
    public boolean blnError {get;set;}
    
    
    public Boolean authState{get;set;}
  
    private static string refreshToken;
    private static string accessToken;
    private static Integer accessExpiresIn;
    public string UserNameEmail{get;set;}
    

    public VFC_BoxAuthorization(){
        MessageToShow = 'Start To Connecting...';
        authState=false;
        blnError=false;
        //UserNameEmail='bfoBoxIntegrationuser@gmail.com';
        codeFromBox = System.currentPageReference().getParameters().get(Label.CLAPR15ELLAbFO44);//'code');
        system.debug(' codeFromBox-->'+ codeFromBox);
      
    }
    //To get "authorization  Code"
    public PageReference boxConnect() {
    codeFromBox = System.currentPageReference().getParameters().get(Label.CLAPR15ELLAbFO44);//'code');
      
      if((UserNameEmail==null || UserNameEmail=='')) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,Label.CLAPR15ELLAbFO43);//'Please enter the User name it should not be Blank');
            ApexPages.addMessage(myMsg); 
            blnError=true;  
            return null; 
      
      }
      
      else if(UserNameEmail==label.CLAPR15ELLAbFO41) {
            if(codeFromBox == null || codeFromBox == ''){

                 PageReference pr = new PageReference(Label.CLAPR15ELLAbFO45+ 'response_type=code' + '&client_id=' + boxClientID +'&box_login='+UserNameEmail +'&redirect_uri=' + redirectURI );//'https://www.box.com/api/oauth2/authorize?'
                return pr ;
                }else {
                return null;
                }
              } else {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,label.CLAPR15ELLAbFO42);
                                    ApexPages.addMessage(myMsg); 
                blnError=true;  
            return null;        
              
              }
     
        
      
    }
   
    //To get "BOX Access Token"
    Public void getBoxToken(){
        
        codeFromBox = System.currentPageReference().getParameters().get(Label.CLAPR15ELLAbFO44);//'code');
        system.debug(' codeFromBox-->'+ codeFromBox);
        if(codeFromBox !=null){        
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            string endPointValue =Label.CLAPR15ELLAbFO46;//'https://www.box.com/api/oauth2/token';

            req.setEndpoint(endPointValue);
            req.setBody('Content-Type=' + EncodingUtil.urlEncode(Label.CLAPR15ELLAbFO47, 'UTF-8')+ '&charset=' + EncodingUtil.urlEncode('UTF-8', 'UTF-8')+'&grant_type=' + EncodingUtil.urlEncode('authorization_code', 'UTF-8')+ '&code=' + EncodingUtil.urlEncode(codeFromBox, 'UTF-8') + '&client_id=' + EncodingUtil.urlEncode(boxClientID, 'UTF-8')+ '&client_secret=' + EncodingUtil.urlEncode(boxSecretCode, 'UTF-8')+ '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8')); //'application/x-www-form-urlencoded',
            req.setMethod('POST');
            HttpResponse res = h.send(req);
            system.debug('Test---->'+res);
            parseAuthJSON(res.getBody());
            system.debug('accessToken'+accessToken);
            if(accessToken!=null && refreshToken !=null ) {
                authState=true;
            }else {
            authState=false;
            }
            
            List<CS_ELLABoxAccessToken__c> boxToken = CS_ELLABoxAccessToken__c.getall().values();
            if(boxToken.size() > 0) {
                CS_ELLABoxAccessToken__c boxupdate=boxToken[0];//[SELECT id, name,Refresh_Token__c,AccessToken__c FROM CS_ELLABoxAccessToken__c ];
                boxupdate.AccessToken__c=accessToken;
                boxupdate.RefreshToken__c=refreshToken;
                boxupdate.BoxAccessTokenExpires__c =Datetime.now().addSeconds(accessExpiresIn);
                boxupdate.BoxRefreshTokenExpire__c =Datetime.now().addDays(integer.valueof(label.CLAPR15ELLAbFO17));
                update boxupdate;
            } 
            else {
            
                CS_ELLABoxAccessToken__c boxTokenInsert=new CS_ELLABoxAccessToken__c();
                boxTokenInsert.AccessToken__c=accessToken;
                boxTokenInsert.RefreshToken__c=refreshToken;
                boxTokenInsert.Name ='Admin';
                boxTokenInsert.BoxAccessTokenExpires__c =Datetime.now().addSeconds(accessExpiresIn);
                boxTokenInsert.BoxRefreshTokenExpire__c =Datetime.now().addDays(integer.valueof(label.CLAPR15ELLAbFO17));
                insert boxTokenInsert;
            
            }
            
            MessageToShow ='Box Access Token'+accessToken;
            
        }


    }
    
    
   
    //TO pass the JSON Body to get Access Token 
    @TestVisible
    private void parseAuthJSON(string JSONValue){
        System.debug('Hbp&'+JSONValue);
        JSONParser parser = JSON.createParser(JSONValue);
        System.debug('&&&&&&&&'+parser );
        accessToken = '';
        refreshToken = '';
    
        while (parser.nextToken() != null) {
    
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME) {
    
            if(parser.getText() == Label.CLAPR15ELLAbFO48){//'access_token'){
                parser.nextToken();
                accessToken = parser.getText();
    
    
            }
            if(parser.getText() == Label.CLAPR15ELLAbFO49){//'refresh_token'){
                parser.nextToken();
                refreshToken = parser.getText();
    
            }
            if(parser.getText() == Label.CLAPR15ELLAbFO50){//'expires_in'){
                parser.nextToken();
                accessExpiresIn = integer.valueof(parser.getText());
    
            }
    
        }
    
        if(accessToken != '' && refreshToken != ''){
            break;
        }
        
    
        }
       
    }
  
}