Public class VFC_ContainmentActionSubmitForApproval
{ Public id XaId;
  Public boolean blnvisible{get;set;}
  Public ContainmentAction__c ObjCA{get;set;}
  Public list<EntityStakeholder__c> lstEntityStake;
  public string errormessage{get;set;}
  Public id containmentId{get;set;}
  Public map<id,String> mapStatusApproval;
  list<ProcessInstance> prcsinstance;
    public VFC_ContainmentActionSubmitForApproval(ApexPages.StandardController controller)
     {
     mapStatusApproval=new Map<id,String>();
     ObjCA=new ContainmentAction__c();
     lstEntityStake=new list<EntityStakeholder__c>();
     blnvisible=false;
     ContainmentAction__c cc=new ContainmentAction__c();
     cc= (ContainmentAction__c )controller.getRecord();
     ObjCA=[select id,Status__c,Tech_ProblemSeverity__c,AccountableOrganization__c,CountryPresident__c ,StatusofValidation__c from ContainmentAction__c where Id=:cc.id]; 
     lstEntityStake = [select id,Role__c,User__c,BusinessRiskEscalationEntity__c from EntityStakeholder__c where Role__c=:Label.CLSEP12I2P85  and BusinessRiskEscalationEntity__c=:ObjCA.AccountableOrganization__c and User__c!=null];
   prcsinstance=[SELECT Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId FROM ProcessInstance where TargetObjectId =:ObjCA.Id and status=:Label.CLSEP12I2P84];    
      
     }


   


   


  Public Pagereference processDetails()
  {
  If(prcsinstance.size()==0)
  {
  if(ObjCA.StatusofValidation__c==label.CLSEP12I2P111) 
  {
errormessage=Label.CLSEP12I2P110;

return null;
   }
  if(ObjCA.Tech_ProblemSeverity__c=='No')
  {
  // blnvisible=true;
         blnvisible=false;
         system.debug('MOHIT2');
         errormessage=Label.CLSEP12I2P101;
        // ApexPages.addmessage( new ApexPages.Message(ApexPages.Severity.Info, 'No Country President found ,Please select Country President'));
         return null;
  }
    If(ObjCA.Status__c =='Completed' )
    {
      if(lstEntityStake.size() >0)
      {
           blnvisible=true;
            ObjCA.CountryPresident__c=lstEntityStake[0].User__c;
            //update ObjCA;
            errormessage=Label.CLSEP12I2P100;
            //pagereference prCna= doApprove();
           // return prCna;   
           return null;
    }
    
    else
    {
        // blnvisible=true;
         blnvisible=false;
         system.debug('MOHIT2');
         errormessage=Label.CLSEP12I2P07;
        // ApexPages.addmessage( new ApexPages.Message(ApexPages.Severity.Info, 'No Country President found ,Please select Country President'));
         return null;
    
    }
    
    }
    else 
    {
     system.debug('MOHIT3');
     blnvisible=false;
     errormessage= Label.CLSEP12I2P06;
    // ApexPages.addmessage( new ApexPages.Message(ApexPages.Severity.Info, 'You cannot submit containment action record for approval since status of containment is not completed.'));
     return null;
    }
   }
   else
   {
     blnvisible=false;
     errormessage= Label.CLSEP12I2P102;
   return null;
   }

    
    
  }
   /* Public Pagereference doProcess()
    {
      
        Update ObjCA;
      
        PageReference prr= doApprove();
        return prr;
    }*/
   Public PageReference  doApprove()
   {   
   
  
            Update ObjCA;
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ObjCA.id);
            req1.setNextApproverIds(new Id[] {ObjCA.CountryPresident__c});
            Approval.ProcessResult result = Approval.process(req1);
            string strredirectURl='/'+ObjCA.id;
            PageReference pr = new PageReference(strredirectURl);
           pr.setRedirect(true);
     return pr;
     
     
   }
   Public Pagereference  doCancel()
   {
   PageReference pr = new PageReference('/'+ObjCA.id);
   pr.setRedirect(true);
     return pr;
    
  
   }
}