global with sharing class SynchronizeObjectsController{
    
    /**
    * @author Tomas Garcia E. 
    * @date 20.11.2014 
    * @description Utility Class. Controller to SynchronizeObjects Page. Allows to keep synchronized two objects.
    */

    public map<String,String> mapValues{get;set;}
    public map<String,String> mapValuesFrom{get;set;}
    public ObjectSync__c objSync {get;set;}
    public String esNew{get;set;}
    public list<MappingField> mappingsFields{get;set;}
    public List<SelectOption> paraFielsdNames{get;set;}
    public Boolean firstRun {get;set;}
    public String mappingSerialise {get;set;}
    public Boolean teniaRoundBeforeSave {get;set;}
    public Boolean blockearRoundTrip{get;set;}
    public Boolean checkUncheckAll{get;set;}
    
    
    public SynchronizeObjectsController(ApexPages.StandardController stdController){
       
        mappingsFields = new list<MappingField>();
        teniaRoundBeforeSave = false;
        blockearRoundTrip = false;
        checkUncheckAll = false;
        
        if(!test.isRunningTest()){ // no se permiten addFields en test classes
            stdController.addFields(new List<String>{ 'FromIdentifier__c', 'FromObject_API_Name__c', 'ToIdentifier__c' , 'ToObject_API_Name__c' ,'Condition__c' , 'RoundTrip__c','MappingFields_JSON__c'});
        }     
        
        //EDIT// 
        if(stdController.getId() != null){
        
            objSync = (ObjectSync__c )stdController.getRecord();
            esNew = 'false'; 
            firstRun = false; 
            
            list<ObjectSync__c> listRoundsTrips = [SELECT id FROM ObjectSync__c WHERE ToObject_API_Name__c  =: objSync.FromObject_API_Name__c   AND FromObject_API_Name__c  =: objSync.ToObject_API_Name__c ];
            
            if(listRoundsTrips.Size() > = 1 && objSync.RoundTrip__c == false ){
                blockearRoundTrip = true;
            }  
            
            if(objSync.RoundTrip__c == true){
                teniaRoundBeforeSave = true;
            } 
            
            if(blockearRoundTrip){               
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.label.DuplicatedSync ); 
                ApexPages.addMessage(myMsg);
            }           
            
            Set<String> setToNotRepeatRows = new Set<String>();   
            list<SyncObjects.MappingField> listAux = (list<SyncObjects.MappingField>)JSON.deserialize(objSync.MappingFields_JSON__c , list<SyncObjects.MappingField>.class);
            
            list<MappingField> listAuxDesserialise = new list<MappingField> ();
            
            
            for(SyncObjects.MappingField sync : listAux ){
            
                MappingField auxSync = new MappingField();
                auxSync .fromApiField = sync.fromApiField ; 
                auxSync .toApiField = sync.toApiField ;
                auxSync .nillable = sync.nillable;
                auxSync .enableSync = sync.mappingEnabled;
                
                listAuxDesserialise.add(auxSync );
            
            }
            
            
            /****** Describe del To *****/
                         
            List<String> listValues = new List<String>();
            map<String,String> mapLabelaApi = new map<String,String>();
            map<String,String> mapLabelaCompleteLabel = new map<String,String>();
            mapValues = new map<String,String>();
            mapValuesFrom = new map<String,String>();
    
            // Get sObject
            Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get( objSync.ToObject_API_Name__c);
            system.debug(sobject_type );
      
            // Get sObject Describe
            Schema.Describesobjectresult descSobjectResultObj = sobject_type.getDescribe();  
                      
            // Get Field Map
            Map<String,Schema.SObjectField> mapFields = descSobjectResultObj.fields.getMap(); 
            
            integer counterObjects =0;

            if(descSobjectResultObj  != null){
                for(String s : mapFields.keySet()){
                    listValues.add(mapFields.get(s).getDescribe().getName());
                    mapLabelaApi.put(mapFields.get(s).getDescribe().getLabel() , mapFields.get(s).getDescribe().getName() );
                    mapLabelaCompleteLabel.put(mapFields.get(s).getDescribe().getName() ,mapFields.get(s).getDescribe().getLabel() + ' (' + mapFields.get(s).getDescribe().getName() + ')' );
                    counterObjects++;
                    if(Test.isRunningTest() && counterObjects > 100){
                        break;
                    }
                }  
            }
            
           
            
            /******  *****/
            
            /****** From Describe *****/

                List<String> listValuesFrom = new List<String>();
                map<String,String> mapLabelaApiFrom = new map<String,String>();
                map<String,String> mapLabelaCompleteLabelFrom = new map<String,String>();
                mapValuesFrom = new map<String,String>();
        
                
                // Get sObject
                Schema.SObjectType sobject_typeFrom = Schema.getGlobalDescribe().get( objSync.FromObject_API_Name__c);
                system.debug(sobject_typeFrom );
                
                // Get sObject Describe
                Schema.Describesobjectresult descSobjectResultObjFrom = sobject_typeFrom.getDescribe();  
                          
                // Get Field Map
                Map<String,Schema.SObjectField> mapFieldsFrom = descSobjectResultObjFrom.fields.getMap(); 
                        
                if(descSobjectResultObjFrom  != null){
                    for(String s : mapFieldsFrom.keySet()){
                        listValuesFrom.add(mapFieldsFrom.get(s).getDescribe().getLabel());
                        mapLabelaApiFrom.put(mapFieldsFrom.get(s).getDescribe().getLabel() , mapFieldsFrom.get(s).getDescribe().getName() );
                        mapLabelaCompleteLabelFrom.put(mapFieldsFrom.get(s).getDescribe().getLabel() ,mapFieldsFrom.get(s).getDescribe().getLabel() + ' (' + mapFieldsFrom.get(s).getDescribe().getName() + ')' );
                    }
                }
            
            /*****             *****/
            
             for(MappingField objDes :listAuxDesserialise ){  // este bloque levanta las rows que YA tenian valor
                setToNotRepeatRows.add(objDes.toApiField );
                Schema.DescribeFieldResult toValue = mapFields.get(objDes.toApiField ).getDescribe();
                objDes.picklist = New list<SelectOption>();
                objDes.picklist.add(new SelectOption( 'none' , 'none' ));  
                
                for(String apiFrom :listValuesFrom){ // RECORRO CADA FROM VALUE LO VALIDO Y LO AGREGO AL PICKLIST
                        
                    Schema.DescribeFieldResult fromValue = mapFieldsFrom.get(mapLabelaApiFrom.get(apiFrom) ).getDescribe();  
                
                    if(appliesSync(fromValue , toValue ) == true){
                        objDes.picklist.add(new SelectOption( mapLabelaApiFrom.get(apiFrom ) , mapLabelaCompleteLabelFrom.get(apiFrom ) ));  
                    }
                } 
                
                objDes.toApiField = mapLabelaCompleteLabel.get(objDes.toApiField); 
                objDes.ToFieldIsRequired = !toValue.isNillable();
                
                
                
                mappingsFields.add(objDes);
            } 
        
            
            for(String api :listValues){ // este bloque levanta las rows que NO tienen valor

                Schema.DescribeFieldResult toValue = mapFields.get(api).getDescribe(); 
                if(validateToField(toValue)){
                    if( !setToNotRepeatRows.contains(api)){
                        
                        MappingField aux = new MappingField();
                        aux.fromApiField = 'none' ; 
                        aux.toApiField = mapLabelaCompleteLabel.get(api);
                        aux.nillable = false ;
                        aux.ToFieldIsRequired = !toValue.isNillable();
                        aux.picklist.add(new SelectOption( 'none' , 'none' )); 
                        
                        
                        for(String apiFrom :listValuesFrom){ //  AGREGO AL PICKLIST
                        
                            Schema.DescribeFieldResult fromValue = mapFieldsFrom.get(mapLabelaApiFrom.get(apiFrom) ).getDescribe();  
                        
                            if(appliesSync(fromValue , toValue ) == true){
                                aux.picklist.add(new SelectOption( mapLabelaApiFrom.get(apiFrom ) , mapLabelaCompleteLabelFrom.get(apiFrom ) ));  
                            }
                        }
                        
                        mappingsFields.add(aux);
                    }  
                }            
            }    
                       
            
            mappingsFields.sort();                         
            
        }else{           
        //NEW//
            objSync = new ObjectSync__c();
            system.debug('new');
            esNew = 'true';
            objSync = new ObjectSync__c ();
            objSync.FromIdentifier__c = null;
            objSync.FromObject_API_Name__c = 'Account';  
            objSync.ToIdentifier__c = null;       
            objSync.ToObject_API_Name__c = 'Account';   
            objSync.Condition__c = '';    
            objSync.RoundTrip__c = false;
        }
    }
    
    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description : Levanta todos los objetos del sistema para elegir el de y para. Works for from and To.
    * @return List<SelectOption>  
    */
    
   
    public List<SelectOption> getObjectsNames(){  // 
        List<Schema.SObjectType> listObjecs = Schema.getGlobalDescribe().Values();    
        List<SelectOption> options = new List<SelectOption>();
        List<String> listValues = new List<String>();
        map<String,String> mapLabelaApi = new map<String,String>();
        map<String,String> mapLabelaCompleteLabel = new map<String,String>();
        mapValues = new map<String,String>();
        
        Integer counterObjects =0;
        for(Schema.SObjectType f : listObjecs ){
            listValues.add(f.getDescribe().getlabel());
            mapLabelaApi.put(f.getDescribe().getlabel() , f.getDescribe().getName() );
            mapLabelaCompleteLabel.put(f.getDescribe().getlabel() ,f.getDescribe().getlabel() + ' (' + f.getDescribe().getName() + ')' );
            counterObjects++;
            if(Test.isRunningTest() && counterObjects > 100){
                break;
            }
        }
        
        listValues.sort();
   
        for(String api : listValues){
            options.add(new SelectOption( mapLabelaApi.get(api) , mapLabelaCompleteLabel .get(api) ));
        }
        
        return options;

    }
    
    
    /**
    * @author Tomas Garcia E. 
    * @date 20.11.2014 
    * @description: Levanta los valores para el identificador del DE.
    * @return List<SelectOption>  
    */
    
    public List<SelectOption> getDeFieldsNames(){  
    
        List<SelectOption> options = new List<SelectOption>();
        List<String> listValues = new List<String>();
        map<String,String> mapLabelaApi = new map<String,String>();
        map<String,String> mapLabelaCompleteLabel = new map<String,String>();

        // Get sObject
        Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get(objSync.FromObject_API_Name__c);
        system.debug(sobject_type );
        system.debug('objSync.FromObject_API_Name__c ' + objSync.FromObject_API_Name__c);
  
        // Get sObject Describe
        Schema.Describesobjectresult descSobjectResultObj = sobject_type.getDescribe();  
              
        // Get Field Map
        Map<String,Schema.SObjectField> mapFields = descSobjectResultObj.fields.getMap(); 
        
        Integer counterObjects = 0;    
        if(descSobjectResultObj  != null){
            for(String s : mapFields.keySet()){
                listValues.add(mapFields.get(s).getDescribe().getLabel());
                mapLabelaApi.put(mapFields.get(s).getDescribe().getLabel() , mapFields.get(s).getDescribe().getName() );
                mapLabelaCompleteLabel.put(mapFields.get(s).getDescribe().getLabel() ,mapFields.get(s).getDescribe().getLabel() + ' (' + mapFields.get(s).getDescribe().getName() + ')' );
                counterObjects++;
                if(Test.isRunningTest() && counterObjects > 100){
                    break;
                }
            }
        }
        
        listValues.sort();            
            
        for(String api :listValues){
            options.add(new SelectOption( mapLabelaApi.get(api) , mapLabelaCompleteLabel .get(api) ));
            mapValues.put(mapLabelaApi.get(api) + ' ' + mapLabelaCompleteLabel .get(api), '' );               
        } 
        return options;
    }
    
    
    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description: Levanta los valores para el identificador del PARA.
    * @return List<SelectOption>
    */
    
    public List<SelectOption> getParaFieldsNames(){   
        List<SelectOption> options = new List<SelectOption>();
        List<String> listValues = new List<String>();
        map<String,String> mapLabelaApi = new map<String,String>();
        map<String,String> mapLabelaCompleteLabel = new map<String,String>();

        // Get sObject
        Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get( objSync.ToObject_API_Name__c);
        system.debug(sobject_type );
  
        // Get sObject Describe
        Schema.Describesobjectresult descSobjectResultObj = sobject_type.getDescribe();  
              
        // Get Field Map
        Map<String,Schema.SObjectField> mapFields = descSobjectResultObj.fields.getMap(); 
        

        Integer counterObjects = 0;
        if(descSobjectResultObj  != null){
            for(String s : mapFields.keySet()){
                listValues.add(mapFields.get(s).getDescribe().getLabel());
                mapLabelaApi.put(mapFields.get(s).getDescribe().getLabel() , mapFields.get(s).getDescribe().getName() );
                mapLabelaCompleteLabel.put(mapFields.get(s).getDescribe().getLabel() ,mapFields.get(s).getDescribe().getLabel() + ' (' + mapFields.get(s).getDescribe().getName() + ')' );
                counterObjects++;
                if(Test.isRunningTest() && counterObjects > 100){
                    break;
                }
            }
        }
     
        listValues.sort();
        
        for(String api :listValues){
            options.add(new SelectOption( mapLabelaApi.get(api) , mapLabelaCompleteLabel .get(api) ));
            mapValues.put(mapLabelaApi.get(api) + ' ' + mapLabelaCompleteLabel .get(api), '' );
        } 
 
        return options;       
    }
    

    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description description of the method ///////////////////////////////////
    * @return Void  
    */
    
    public void generateMappingFields(){

        List<String> listValues = new List<String>();
        map<String,String> mapLabelaApi = new map<String,String>();
        map<String,String> mapLabelaCompleteLabel = new map<String,String>();
        mapValues = new map<String,String>();
        
        List<String> listValuesFrom = new List<String>();
        map<String,String> mapLabelaApiFrom = new map<String,String>();
        map<String,String> mapLabelaCompleteLabelFrom = new map<String,String>();
        mapValuesFrom = new map<String,String>();
        
        /////////* To part */////////
        // Get sObject
        Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get( objSync.ToObject_API_Name__c);
        system.debug(sobject_type );
  
        // Get sObject Describe
        Schema.Describesobjectresult descSobjectResultObj = sobject_type.getDescribe();  
                  
        // Get Field Map
        Map<String,Schema.SObjectField> mapFields = descSobjectResultObj.fields.getMap(); 
                
        Integer counterObjects = 0;                
        if(descSobjectResultObj  != null){
            for(String s : mapFields.keySet()){
                listValues.add(mapFields.get(s).getDescribe().getLabel());
                mapLabelaApi.put(mapFields.get(s).getDescribe().getLabel() , mapFields.get(s).getDescribe().getName() );
                mapLabelaCompleteLabel.put(mapFields.get(s).getDescribe().getLabel() ,mapFields.get(s).getDescribe().getLabel() + ' (' + mapFields.get(s).getDescribe().getName() + ')' );
                counterObjects++;
                if(Test.isRunningTest() && counterObjects > 100){
                    break;
                }
            }
        }
        
        /////////* From part */////////
        // Get sObject
        Schema.SObjectType sobject_typeFrom = Schema.getGlobalDescribe().get( objSync.FromObject_API_Name__c);
        system.debug(sobject_typeFrom );
  
        // Get sObject Describe
        Schema.Describesobjectresult descSobjectResultObjFrom = sobject_typeFrom.getDescribe();  
                  
        // Get Field Map
        Map<String,Schema.SObjectField> mapFieldsFrom = descSobjectResultObjFrom.fields.getMap(); 
                
        counterObjects = 0;
        if(descSobjectResultObjFrom  != null){
            for(String s : mapFieldsFrom.keySet()){
                listValuesFrom.add(mapFieldsFrom.get(s).getDescribe().getLabel());
                mapLabelaApiFrom.put(mapFieldsFrom.get(s).getDescribe().getLabel() , mapFieldsFrom.get(s).getDescribe().getName() );
                mapLabelaCompleteLabelFrom.put(mapFieldsFrom.get(s).getDescribe().getLabel() ,mapFieldsFrom.get(s).getDescribe().getLabel() + ' (' + mapFieldsFrom.get(s).getDescribe().getName() + ')' );
                counterObjects++;
                if(Test.isRunningTest() && counterObjects > 100){
                    break;
                }
            }
        }
            
        listValues.sort();
        mappingsFields = new list<MappingField>();
        
        
        for(String api :listValues){  // POR CADA TO VALUE CREO UNA ROW
            Schema.DescribeFieldResult toValue = mapFields.get(mapLabelaApi.get(api)).getDescribe(); 
            if(validateToField(toValue)){
                mapValues.put(mapLabelaApi.get(api) + ' ' + mapLabelaCompleteLabel .get(api), '' );
                
                MappingField aux = new MappingField();
                aux.fromApiField = 'none' ; 
                aux.toApiField = mapLabelaCompleteLabel.get(api);
                aux.nillable = false ; 
                aux.ToFieldIsRequired = !toValue.isNillable();
                aux.picklist.add(new SelectOption( 'none' , 'none' ));                

                for(String apiFrom :listValuesFrom){ // RECORRO CADA FROM VALUE LO VALIDO Y LO AGREGO AL PICKLIST
                
                    Schema.DescribeFieldResult fromValue = mapFieldsFrom.get(mapLabelaApiFrom.get(apiFrom) ).getDescribe();  
                
                    if(appliesSync(fromValue , toValue ) == true){
                        aux.picklist.add(new SelectOption( mapLabelaApiFrom.get(apiFrom ) , mapLabelaCompleteLabelFrom.get(apiFrom ) ));  
                        
                        if( mapLabelaApiFrom.get(apiFrom ) == mapLabelaApi.get(api) ){  // LABELS IGUALES
                            aux.enableSync = true;
                            aux.fromApiField = mapLabelaApiFrom.get(apiFrom ); 
                        }else if( apiFrom == api ){  // apis iguales
                            aux.enableSync = true;
                            aux.fromApiField = mapLabelaApiFrom.get(apiFrom ); 
                        }
                    
                    }
                    
                  
                }
                
                mappingsFields.add(aux);  
                
                mappingsFields.sort();
  
            }               
        }
    }
    
    
    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description: save button
    * @return PageReference 
    */
    
    public PageReference Save(){ 
    
        if(objSync.ToIdentifier__c == '' || objSync.ToIdentifier__c == 'none' ){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FieldRequiredTo);
            ApexPages.addMessage(myMsg);
        }
        
        if(objSync.FromIdentifier__c == '' || objSync.FromIdentifier__c == 'none' ){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FieldRequiredFrom);
            objSync.addError(System.Label.FieldRequiredFrom);
            ApexPages.addMessage(myMsg);
        }
        
        if(mappingsFields.isEmpty()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.AtLeast1mapping + '1');
            ApexPages.addMessage(myMsg);
        }
        
        if(ApexPages.hasMessages()){
            return null;
        } 
                    
    
        list<SyncObjects.MappingField > mappingsToUpsert = new list<SyncObjects.MappingField >();
       
        for( MappingField vals : mappingsFields){ // from sync.MappingField to local MappingField class
            if( vals.fromApiField != 'none'  ){
                System.debug('1');
                SyncObjects.MappingField aux = new SyncObjects.MappingField();                               
                aux.fromApiField = vals.fromApiField  ;
                String auxString =  vals.toApiField.substring(vals.toApiField.indexOf('(')+1,vals.toApiField.indexOf(')'));
                aux.toApiField = auxString ; 
                aux.nillable = vals.nillable ;
                aux.mappingEnabled = vals.enableSync;
  
                mappingsToUpsert.add(aux);         
            }
        }
    
        String JSONString = JSON.serialize(mappingsToUpsert);
        objSync.MappingFields_JSON__c = JSONString; 
        
        if(objSync.MappingFields_JSON__c == '[]' ){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.AtLeast1mapping + '2');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        if(ApexPages.hasMessages()){
            return null;
        }        
        
        objSync.Name = 'Syncronize: ' + objSync.FromObject_API_Name__c  + ' to ' + objSync.ToObject_API_Name__c; 
        Boolean uniqueName = true;
        
        list<ObjectSync__c> auxToUniqueValidation = [SELECT id, Name FROM ObjectSync__c  WHERE Name =: objSync.Name];
        if(auxToUniqueValidation.Size() >= 1 && esNew == 'true'){
            uniqueName = false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.SimilarMapping);  
            ApexPages.addMessage(myMsg );
            return null;
        }
                 
        upsert objSync;
        
        if(objSync.RoundTrip__c == true && teniaRoundBeforeSave == false ){
            system.debug('111 round ');
            ObjectSync__c roundTrip= new ObjectSync__c();
            roundTrip.FromIdentifier__c = objSync.ToIdentifier__c;
            roundTrip.FromObject_API_Name__c = objSync.ToObject_API_Name__c;
            roundTrip.ToIdentifier__c = objSync.FromIdentifier__c;
            roundTrip.ToObject_API_Name__c = objSync.FromObject_API_Name__c;
            //roundTrip.Condition__c = '';   desp chequear como seria esto
            roundTrip.RoundTrip__c = false;
            roundTrip.Name =  'Syncronize: ' + roundTrip.FromObject_API_Name__c  + ' to ' + roundTrip.ToObject_API_Name__c ;
            roundTrip.OriginalSync__c = objSync.id; 
            
            roundTrip.MappingFields_JSON__c = '[{"fromApiField":"Title","toApiField":"Description","Nillable":false}]';
            list<SyncObjects.MappingField> listAuxDeserialize = (list<SyncObjects.MappingField>)JSON.deserialize(objSync.MappingFields_JSON__c , list<SyncObjects.MappingField>.class);
            list<SyncObjects.MappingField> jsonPreSerialize = new list<SyncObjects.MappingField>();
            
            if( listAuxDeserialize.Size() >= 1){
                for(SyncObjects.MappingField mapF : listAuxDeserialize ){
 
                    SyncObjects.MappingField aux = new SyncObjects.MappingField();
                    aux.fromApiField = mapF.toApiField  ;
                    aux.toApiField = mapF.fromApiField ;
                    aux.nillable = mapF.nillable ;
                    aux.mappingEnabled = mapF.mappingEnabled ;

                    jsonPreSerialize.add(aux);
                }
                roundTrip.MappingFields_JSON__c =  JSON.serialize(jsonPreSerialize);
                
                list<ObjectSync__c> auxToUniqueValidationRoundTrip = [SELECT id, Name FROM ObjectSync__c  WHERE Name =: roundTrip.Name];
                if(auxToUniqueValidationRoundTrip.Size() >= 1 && esNew == 'true'){
                    uniqueName = false;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.label.DuplicatedSync);                   
                    ApexPages.addMessage(myMsg );
                    return null;
                }
            }
                       
            upsert roundTrip;
            
           
            
        /* Si le saco el roundtrip, el sistema borra la sincro inversa */
        }else if( esNew == 'false' && teniaRoundBeforeSave == true && objSync.RoundTrip__c == false ){ 
            system.debug('entro delete');
        
            DELETE  [SELECT id FROM ObjectSync__c WHERE OriginalSync__c =: objSync.id limit 1];

        }
        
        PageReference Pagetoredi = new PageReference('/'+ objSync.id ); 
        Pagetoredi.setRedirect(true);
        
        return Pagetoredi ;
    }
    
    
    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description: can be sincronizated to and from? Method 
    * @param Schema.DescribeFieldResult fromFieldDescribe
    * @param Schema.DescribeFieldResult toFieldDescribe
    * @return boolean 
    */
     
    private boolean appliesSync(Schema.DescribeFieldResult fromFieldDescribe, Schema.DescribeFieldResult toFieldDescribe){ 
        //restricciones en los dos campos
        boolean typesEquals = false;
        
        if( fromFieldDescribe.getType() == toFieldDescribe.getType() ){
            typesEquals = true;
        }     
       
        boolean bothAccessible = false;
        if( fromFieldDescribe.isAccessible() == true && toFieldDescribe.isAccessible() == true ){
            bothAccessible = true;
        }
        
        if( typesEquals == true && bothAccessible == true ){           
            return true;
        }else{               
            return false;
        }

    }
    
    
    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description can be sincronizated? Method.
    * @param Schema.DescribeFieldResult toFieldDescribe
    * @return boolean 
    */
    
    private boolean validateToField (Schema.DescribeFieldResult toFieldDescribe){   
     
        //restricciones en el PARA
        boolean notId = toFieldDescribe.getName() != 'Id';
        boolean notAutoNumber = !toFieldDescribe.isAutoNumber();
        boolean notFormula = !toFieldDescribe.isCalculated();
        boolean updeteables = toFieldDescribe.isUpdateable();
         
        return  notId && notAutoNumber && notFormula && updeteables;
    }
    
    
    /**
    * @author Tomas Garcia E.
    * @date 20.11.2014 
    * @description: Check all validations checkBoxes mapping or uncheck all.
    * @return Void  
    */
    
    public void checkAll(){       
        if(!mappingsFields.isEmpty()){
            for( MappingField mapp: mappingsFields ){
                if( checkUncheckAll == false){ 
                    mapp.enableSync = false;
                }else{
                    mapp.enableSync = true;
                }
            }    
         }     
    }
   
    
    /**
    * @author Tomas Garcia E. 
    * @date 20.11.2014 
    * @description .
    */
        
    global class MappingField  implements Comparable{
        public String fromApiField{get;set;}
        public String toApiField{get;set;}
        public boolean nillable{get;set;}
        public List<SelectOption> picklist{get;set;}
        public boolean enableSync{get;set;} 
        public boolean ToFieldIsRequired{get;set;} 
    
        public MappingField( ){
             picklist = new list<SelectOption>();
             enableSync = false;
        } 

        
        global Integer compareTo(Object compareTo) { // To order lists
            MappingField compareToMapp = (MappingField)compareTo;
            
            Integer returnValue = 0;
            
            if (toApiField > compareToMapp.toApiField) {
                returnValue = 1;
            } else if (toApiField < compareToMapp.toApiField) {
                returnValue = -1;
            }
            
            return returnValue;       
        }
        
    }   
}