public class SVMXC_TimesheetBefore {

    public static void ensureTechIsOwner (List<SVMXC_Timesheet__c> triggerNew, Map<Id, SVMXC_Timesheet__c> triggerOld, Boolean isInsert, Boolean isUpdate) {
        /*
            Method to ensure that the tech is the owner of the record (Written May 16 2014 / Updated Mar 13 2015)
          
        */
        
        Set<Id> techIds = new Set<Id>();
        
        if (isInsert) {
            for (SVMXC_Timesheet__c ts0: triggerNew) {
                if (ts0.Technician__c != null && !techIds.contains(ts0.Technician__c))
                    techIds.add(ts0.Technician__c);
            }
        } else if (isUpdate) {
            for (SVMXC_Timesheet__c ts1: triggerNew) {
                if (ts1.Technician__c != null && ts1.Technician__c != triggerOld.get(ts1.Id).Technician__c && !techIds.contains(ts1.Technician__c))
                    techIds.add(ts1.Technician__c);
            }
            
        }
        
        if (!techIds.isEmpty()) {
            Map <Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c> 
                        ([SELECT Id, SVMXC__Salesforce_User__c FROM SVMXC__Service_Group_Members__c 
                        WHERE Id IN: techIds AND SVMXC__Salesforce_User__c != null]);
            
            if (techMap.size() > 0) {
                                
                for (SVMXC_Timesheet__c ts1: triggerNew) {  
      
                    if (ts1.Technician__c != null && techMap.containsKey(ts1.Technician__c) 
                            && ts1.OwnerId != techMap.get(ts1.Technician__c).SVMXC__Salesforce_User__c)
                        ts1.OwnerId = techMap.get(ts1.Technician__c).SVMXC__Salesforce_User__c;
                }
            }
        }      
    }
}