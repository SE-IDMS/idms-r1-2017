public class VFC_AddMultipleProductsForLitReqLineItem extends VFC_ControllerBase {

    public literatureLineItemGMRSearch GMRSearchForlitLineItem{get;set;}
    public LiteratureRequest__c objLiteratureRequest {get;set;}
    LiteratureRequestLineItem__c objLiteratureRequestLineItem ; // New Commecial Reference to be inserted.   
    
    Public String LineItemID;
    public static boolean hasException=false;
    /*=======================================
    INNER CLASSES
    =======================================*/ 
    // GMR implementation for Cases
    public class literatureLineItemGMRSearch implements Utils_GMRSearchMethods
    {
        VFC_AddMultipleProductsForLitReqLineItem thisController;
        List<LiteratureRequestLineItem__c> lstLiteratureRequestLineItem {get;set;}{lstLiteratureRequestLineItem =new List<LiteratureRequestLineItem__c>();} 
        public Void Init(String ObjID, VCC08_GMRSearch Controller)
        {
            thisController = (VFC_AddMultipleProductsForLitReqLineItem)Controller.pageController;

        }

        // Cancel method returns the associated Literature request detail page
        public PageReference Cancel( VCC08_GMRSearch Controller)
        { 
            String url = '/'+thisController.objLiteratureRequest.Id;
            PageReference pageRef = new PageReference(url);
            pageRef.setRedirect(true);
            return pageRef;
        }   
        

        // Action performed when clicking on "Add" on the PM0 search results
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
        {

            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)controllerBase;
            DataTemplate__c Product = (DataTemplate__c)obj;
            
            if(thisController.objLiteratureRequest !=null)
            {
                //if multiple checkboxes are selected
                if(GMRController.ActionType == 'multiple')
                {
                   VCC06_DisplaySearchResults DisplayResultsController = (VCC06_DisplaySearchResults)GMRController.getcomponentControllerMap().get('resultComponent');
                   List<DataTemplate__c> SelectedProducts=new List<DataTemplate__c>();
                   if(DisplayResultsController != null)
                   SelectedProducts = (List<DataTemplate__c>)DisplayResultsController.getSelectedObjects();           
                   lstLiteratureRequestLineItem=VFC_AddMultipleProductsForLitReqLineItem.InsertLiteratureRequestLineItem(SelectedProducts,thisController.objLiteratureRequest);
                   System.debug('#ERROR STATUS#'+hasException+'#lstLiteratureRequestLineItem#'+lstLiteratureRequestLineItem);
                   if(hasException){
                       lstLiteratureRequestLineItem=null;
                       return null;
                   }
                }
            }    
             
           String url = '/'+thisController.objLiteratureRequest.Id;
           System.debug('##URL##:'+url);       
            PageReference pageRef = new PageReference(url);
            pageRef.setRedirect(true);
            return pageRef;
        }
    }
    
    
    
    public VFC_AddMultipleProductsForLitReqLineItem(ApexPages.StandardController controller)
    {
        LineItemID = system.currentpagereference().getparameters().get('LtrlnId');
        If(LineItemID != null && LineItemID != ''){
        objLiteratureRequestLineItem =[Select Id,LiteratureRequest__c,Commercialreference__c,Quantity__c from LiteratureRequestLineItem__c where Id=: LineItemID Limit 1];
        }else{
        objLiteratureRequestLineItem =  new LiteratureRequestLineItem__c();
        objLiteratureRequestLineItem = (LiteratureRequestLineItem__c)controller.getRecord();
        }
        //objLiteratureRequestId = objLiteratureRequestLineItem.LiteratureRequest__c;
        if(objLiteratureRequestLineItem.LiteratureRequest__c != null)
            objLiteratureRequest = [select id,name,Status__c,Quantity__c  from LiteratureRequest__c where id = : objLiteratureRequestLineItem.LiteratureRequest__c limit 1];
        GMRsearchForlitLineItem = new literatureLineItemGMRSearch();
    }
    public  static List<LiteratureRequestLineItem__c> InsertLiteratureRequestLineItem( List<DataTemplate__c> SelectedProducts,LiteratureRequest__c objLiteratureRequest){
        List<LiteratureRequestLineItem__c> selectedLiteratureLineItems=new List<LiteratureRequestLineItem__c>();
        for(DataTemplate__c template:SelectedProducts){
            LiteratureRequestLineItem__c objLiteratureRequestLineItem =new LiteratureRequestLineItem__c();
            objLiteratureRequestLineItem.CommercialReference__c=template.Field8__c;
            objLiteratureRequestLineItem.LiteratureRequest__c=objLiteratureRequest.Id;
            objLiteratureRequestLineItem.Quantity__c=1;
            selectedLiteratureLineItems.add(objLiteratureRequestLineItem);          
        }

        //insert into Literature Request Line Iten object               
        try{
            hasException=false;
            Database.insert(selectedLiteratureLineItems);
        }
        catch(Exception ex){
            hasException=true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,String.valueOf(ex)));
            System.debug('#DML ERRORS#'+ex);    
            return null;
        }
        if(selectedLiteratureLineItems.size()>0){                               
            return selectedLiteratureLineItems;
        }
        return null;
    }   
}