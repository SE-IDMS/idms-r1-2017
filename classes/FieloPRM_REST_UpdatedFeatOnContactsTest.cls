@isTest

private class FieloPRM_REST_UpdatedFeatOnContactsTest{

    static testMethod void unitTest(){
        
       User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);

        
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'TestLaastN';
            member.FieloEE__FirstName__c = 'TestFiirstN';
            member.FieloEE__Street__c = 'test2';
                 
            insert member;
        
            FieloPRM_MemberUpdateHistory__c history = new FieloPRM_MemberUpdateHistory__c ();
            history.F_PRM_Type__c = 'Feature';
            history.F_PRM_Member__c = member.id;
            insert history;
        }
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.addParameter('fromTime', '19900202020202020');
        req.addParameter('toTime', '20160202020220202');
        
        req.requestURI = '/services/apexrest/RestUpdatedFeatureOnContacts';  
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        
        FieloPRM_REST_UpdatedFeatOnContacts callingClass = new FieloPRM_REST_UpdatedFeatOnContacts();
        FieloPRM_REST_UpdatedFeatOnContacts.getUpdatedFeatureOnContacts();
        
        

    }

}