/**
12-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for ContractAfterInsert,   ContractAfterUpdate  and AP_ContractTriggerUtils
 */
@isTest
private class AP_ContractTriggerUtils_TEST {

    static testMethod void myUnitTest() {
        Account testAccount = new Account();
        testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        Contact testContact = new Contact();
        testContact = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        insert testContact;
        Contact testContact1 = new Contact();
        testContact1 = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        insert testContact1;
        
        Contract testContract = new Contract();
        testContract = Utils_TestMethods.createContract(testAccount.Id,null);
        insert testContract;
        
        testContract.contactname__c = testContact.Id;
        update testContract;
        testContract.contactname__c = testContact1.Id;
        update testContract;
        testContract.contactname__c = null;
        update testContract;
       // testContract.contactname__c = testContact.Id;
       // update testContract;
        
        Contract testContract1 = new Contract();
        testContract1 = Utils_TestMethods.createContract(testAccount.Id,testContact.Id);
        insert testContract1;
        testContract1.SupportContractLevel__c = 'Test1';
        testContract1.ContractType__c = 'Test1';
        update testContract1;
       
        testContract1.status = 'Activated';
        update testContract1;
        
        case testcase = Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'open');
        testcase.relatedcontract__c = testContract1.id;
        insert testcase;
        testContract1.SupportContractLevel__c = 'Test2';
        testContract1.ContractType__c = 'Test2';
        update testContract1;
        
    }

    static testMethod void createProactiveCaseForContracts_TEST() {
        Account testAccount = new Account();
        testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        List<Contact> lstContact = new List<Contact>();
        Contact testContact = new Contact();
        testContact = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        lstContact.add(testContact);
        
        
        Contact testContact1 = new Contact();
        testContact1 = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        lstContact.add(testContact1);
        
        CustomerCareTeam__c objCCTeam = Utils_TestMethods.createPrimaryCustomerCareTeamWithOutCountry();
        insert objCCTeam;

        
        insert lstContact;
        
        List<Contract> lstContract = new List<Contract>();
        Contract testContract = new Contract();
        testContract = Utils_TestMethods.createContract(testAccount.Id,null);
        testContract.AutomaticProactiveCase__c =true;
        testContract.ProactiveCaseSupportTeam__c = objCCTeam.id;
        lstContract.add(testContract);
        
        //insert testContract;
        
        
        Contract testContract1 = new Contract();
        testContract1 = Utils_TestMethods.createContract(testAccount.Id,testContact.Id);
        testContract1.StartDate = System.today();
        testContract1.ContractTerm = 12;
        testContract1.OwnerExpirationNotice ='30';
        lstContract.add(testContract1);
        insert lstContract;

        testContract1.SupportContractLevel__c = 'Test1';
        testContract1.ContractType__c = 'Test1';
        update testContract1;

        testContract.status = 'Activated';
        testContract.TECH_TriggerFirstProactiveCase__c = true;
        testContract.TECH_TriggerSecondProactiveCase__c = true;
        testContract1.status = 'Activated';
        update lstContract;


    }
    static testMethod void createPointsRegistrationHistory_TEST() {
        
        Account testAccount = new Account();
        testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        
        Contact testContact = new Contact();
        testContact = Utils_TestMethods.createContact(testAccount.Id,'Test Contact');
        insert testContact;
       
        
        Contract testContract = new Contract();
        testContract = Utils_TestMethods.createContract(testAccount.Id,testContact.Id);
        testContract.WebAccess__c= 'Cassini';
        insert testContract;
       
        testContract.status = 'Activated';
        update testContract;

        CTR_ValueChainPlayers__c objCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        insert objCVCP;
         
        Case objOpenCase = Utils_TestMethods.createCase(testAccount.Id, testContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        Test.startTest();
        AP_Case_CaseHandler.blnUpdateTestRequired=true;
        objOpenCase.Status='Closed';
        objOpenCase.TECH_DebitPoints__c=true;
        objOpenCase.CTRValueChainPlayer__c=objCVCP.id;
        objOpenCase.FreeOfCharge__c=false;
        objOpenCase.Points__c = 774; 
        System.debug('objOpenCase'+objOpenCase);
        update objOpenCase;
        System.debug('objOpenCase'+objOpenCase);
        Test.stopTest();
    }
}