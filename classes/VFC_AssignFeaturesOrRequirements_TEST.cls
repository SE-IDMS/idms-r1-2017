@isTest
private class VFC_AssignFeaturesOrRequirements_TEST
{
    public static testmethod void AssignFeaturesTestMethod()
    {
        FeatureCatalog__c feature = new FeatureCatalog__c();
        feature.Description__c = 'Test Description';
        insert feature;
        
        ApexPages.currentPage().getparameters().put('Id',feature.id);  
        ApexPages.StandardController featureController2 = new ApexPages.StandardController(feature);
        VFC_AssignFeaturesOrRequirements features2 = new VFC_AssignFeaturesOrRequirements(featureController2);
        features2.addNewRow();
        System.debug('Feature---------'+features2.queryUser);
        //features2.getProgramFeatures();

        RequirementCatalog__c requirement = new RequirementCatalog__c();
        requirement.Description__c = 'Test Description';
        insert requirement;
        
        ApexPages.currentPage().getparameters().put('Id',requirement.id);  
        ApexPages.StandardController reqController2 = new ApexPages.StandardController(requirement);
        VFC_AssignFeaturesOrRequirements requiements2 = new VFC_AssignFeaturesOrRequirements(reqController2);
        requiements2.addNewRow();
        //requiements2.getProgramRequirements();

        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;

        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;

        ProgramFeature__c pFeature= new ProgramFeature__c();
        pFeature.FeatureCatalog__c = feature.Id;
        pFeature.PartnerProgram__c = gp1.Id;
        pFeature.ProgramLevel__c = prg1.Id;
        pFeature.FeatureStatus__c = 'Active';
        insert pFeature;
        ApexPages.currentPage().getparameters().put('Id',feature.id);  
        ApexPages.StandardController featureController = new ApexPages.StandardController(feature);
        VFC_AssignFeaturesOrRequirements features = new VFC_AssignFeaturesOrRequirements(featureController);
        //features.getProgramFeatures();
        features.ProgramFeatureRequirements[0].ToBeDeleted = true;
        features.deleteRow();
        features.addNewRow();
        features.doSave();
        features.ClearUpdateStatus();
        features.doCancel();
        features.clearApexMessage();
        
        /*Account acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;

        Insert acc;
        acc.isPartner = true;
        acc.PRMAccount__c = True;
        update acc;

        Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
        Insert contact;

        ACC_PartnerProgram__c accAssPgm = Utils_TestMethods.createAccountProgram(prg2.Id, cp1.Id, acc.Id);
        Insert accAssPgm;*/

        ProgramFeature__c pFeature1 = new ProgramFeature__c();
        pFeature1.FeatureCatalog__c = feature.Id;
        pFeature1.PartnerProgram__c = gp1.Id;
        pFeature1.ProgramLevel__c = prg1.Id;
        pFeature1.FeatureStatus__c = 'Active';
        insert pFeature1;
        ApexPages.currentPage().getparameters().put('Id',feature.id);  
        ApexPages.StandardController featureController1 = new ApexPages.StandardController(feature);
        VFC_AssignFeaturesOrRequirements features1 = new VFC_AssignFeaturesOrRequirements(featureController);
        features1.deleteRow();
        features1.doSave();
        features1.doCancel();
        
        ProgramFeature__c pFeature3 = new ProgramFeature__c();
        pFeature3.FeatureCatalog__c = feature.Id;
        pFeature3.PartnerProgram__c = cp1.Id;
        pFeature3.ProgramLevel__c = prg2.Id;
        pFeature3.FeatureStatus__c = 'Active';
        insert pFeature3;
        ApexPages.currentPage().getparameters().put('Id',feature.id);  
        ApexPages.StandardController featureController3 = new ApexPages.StandardController(feature);
        VFC_AssignFeaturesOrRequirements features3 = new VFC_AssignFeaturesOrRequirements(featureController);
        features3.doSave();
        features3.doCancel();
        
        ProgramRequirement__c pRequirement = new ProgramRequirement__c();
        pRequirement.RequirementCatalog__c = requirement.Id;
        pRequirement.PartnerProgram__c = gp1.Id;
        pRequirement.ProgramLevel__c = prg1.Id;
        pRequirement.Active__c = True;
        insert pRequirement;
        ApexPages.currentPage().getparameters().put('Id',requirement.id);  
        ApexPages.StandardController reqController = new ApexPages.StandardController(requirement);
        VFC_AssignFeaturesOrRequirements requiements = new VFC_AssignFeaturesOrRequirements(reqController);
        //requiements.getProgramRequirements();
        requiements.ProgramFeatureRequirements[0].ToBeDeleted = true;
        requiements.deleteRow();
        requiements.addNewRow();
        requiements.doSave();
        requiements.doCancel();
        requiements.clearApexMessage();

        ProgramRequirement__c pRequirement1 = new ProgramRequirement__c();
        pRequirement1.RequirementCatalog__c = requirement.Id;
        pRequirement1.PartnerProgram__c = gp1.Id;
        pRequirement1.ProgramLevel__c = prg1.Id;
        pRequirement1.Active__c = True;
        insert pRequirement1;
        ApexPages.currentPage().getparameters().put('Id',requirement.id);  
        ApexPages.StandardController reqController1 = new ApexPages.StandardController(requirement);
        VFC_AssignFeaturesOrRequirements requiements1 = new VFC_AssignFeaturesOrRequirements(reqController);
        requiements1.deleteRow();
        requiements1.doSave();
        requiements1.doCancel();
        
        ProgramRequirement__c pRequirement3 = new ProgramRequirement__c();
        pRequirement3.RequirementCatalog__c = requirement.Id;
        pRequirement3.PartnerProgram__c = cp1.Id;
        pRequirement3.ProgramLevel__c = prg2.Id;
        pRequirement3.Active__c = True;
        insert pRequirement3;
        ApexPages.currentPage().getparameters().put('Id',requirement.id);  
        ApexPages.StandardController reqController3 = new ApexPages.StandardController(requirement);
        VFC_AssignFeaturesOrRequirements requiements3 = new VFC_AssignFeaturesOrRequirements(reqController);
        requiements3.doSave();
        requiements3.doCancel();
    }
}