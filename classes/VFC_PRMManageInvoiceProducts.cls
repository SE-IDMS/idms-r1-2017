global class VFC_PRMManageInvoiceProducts {
    public InvoiceAndProdDetails InvoiceAndProdInfo {get; set;}
    private transient Set<String> validationMessages;
    private transient Map<String, String> MessageList;
    public String ErrorInfoAddl { get; set; }
    public String OperationStatus { get; set; }
    public Id InvoiceId { get; set; }
    public String PRMClusterId { get; set; }
    public String DelAttachmentId { get; set; }
    public string retURL { get; set; }
    public string invCountries { get; set; }
    public transient string ErrMsgDetail {get;set;}
    public Account retailerAccount{get;set;}
    
    public Map<String, String> getMessageList(){
        if(MessageList == null && !Test.isRunningTest()){
            MessageList = AP_ValidationMessageService.ErrorMessages;
        }
        return MessageList;
    }
    
    public Set<String> getValidationMessages(){

        return ValidationMessages;
    }
    
    public void addValidationMessages(String errCode){
        if (ValidationMessages == null) ValidationMessages = new Set<String>();
        ValidationMessages.add(errCode);
    }
    
    public void addMessageMap(String errCode, String errValue){
        if (MessageList == null) MessageList = new Map<String, String>();
        MessageList.put(errCode, errValue);
    }
    
    public VFC_PRMManageInvoiceProducts() {
        OperationStatus = null;
        //ValidationMessages = new Set<String>();
        retailerAccount = new Account();
        List<String> lFldsLst = new List<String>();
        
        List<Schema.FieldSetMember> InvoiceFieldSetMembers = SObjectType.FieloPRM_Invoice__c.FieldSets.InvoiceInformation.getFields();
        for(Schema.FieldSetMember invFldSetMembers:InvoiceFieldSetMembers){
            lFldsLst.add(invFldSetMembers.getFieldPath());
        }
        String queryInvoice = ', F_PRM_Member__r.F_Account__c , F_PRM_Member__r.F_Country__c , F_PRM_Member__r.FieloEE__User__r.Contact.PRMFirstName__c ,F_PRM_Country__r.TECH_Countries__c, F_PRM_Member__r.FieloEE__User__r.Contact.PRMLastName__c , F_PRM_RetailerAccount__r.AdditionalAddress__c , F_PRM_RetailerAccount__r.LocalAdditionalAddress__c , F_PRM_RetailerAccount__r.AccountLocalName__c, F_PRM_RetailerAccount__r.ZipCode__c, F_PRM_RetailerAccount__r.StreetLocalLang__c, F_PRM_RetailerAccount__r.LocalCity__c, F_PRM_RetailerAccount__r.StateProvince__c, F_PRM_RetailerAccount__r.Country__c ';
        System.debug('*** The query string '+queryInvoice);
        InvoiceAndProdInfo = new InvoiceAndProdDetails();
        InvoiceAndProdInfo.InvoiceInfo = Database.query('SELECT Id,'+String.Join(lFldsLst,',')+queryInvoice+' FROM FieloPRM_Invoice__c WHERE Id = '+'\''+apexpages.currentpage().getparameters().get('id')+'\'');
        
        System.debug('*** '+retailerAccount);
        PRMClusterId = InvoiceAndProdInfo.InvoiceInfo.F_PRM_Country__c;
        InvoiceId = InvoiceAndProdInfo.InvoiceInfo.Id;
        System.debug('*** F_PRM_Country__c'+PRMClusterId);
        System.debug('*** '+InvoiceAndProdInfo.InvoiceInfo);
        //String invCountryCode = InvoiceAndProdInfo.InvoiceInfo.F_PRM_Country__r.TECH_Countries__c;
        invCountries = InvoiceAndProdInfo.InvoiceInfo.F_PRM_Country__r.TECH_Countries__c;
        
        InvoiceAndProdInfo.InvoiceAttachmentsList = [SELECT ContentType,Id,Name,ParentId, CreatedById,CreatedBy.Name, LastModifiedDate FROM Attachment WHERE ParentId = :InvoiceAndProdInfo.InvoiceInfo.ID];
        InvoiceAndProdInfo.InvoiceDetailsInfo = [SELECT Id, F_PRM_TotalPrice__c,F_PRM_PointsInstaller__c, F_PRM_Invoice__c, F_PRM_LoyaltyEligibleProduct__c, F_PRM_LoyaltyEligibleProduct__r.Name, F_PRM_LoyaltyEligibleProduct__r.F_PRM_NameDescription__c, F_PRM_Volume__c, F_PRM_UnitPrice__c FROM FieloPRM_InvoiceDetail__c WHERE F_PRM_Invoice__c =: InvoiceAndProdInfo.InvoiceInfo.Id];
        changeRetailerAccount();
        retURL = ApexPages.currentPage().getHeaders().get('Referer');   
        if(ApexPages.currentPage().getParameters().containsKey('detailPage') || string.isBlank(retURL))
                retURL = '/'+InvoiceId;     
        
        System.debug('*** Referer"'+retURL);
        ErrMsgDetail = JSON.serialize(AP_ValidationMessageService.ErrorMessages);
        system.debug('*** '+MessageList);
    }
    
    @RemoteAction
    global Static list<FieloPRM_LoyaltyEligibleProduct__c> productAdvancedSearch(SearchProductModel productSearch){
        List<FieloPRM_LoyaltyEligibleProduct__c> searchResults = new List<FieloPRM_LoyaltyEligibleProduct__c>();
        String query = Null;
        System.debug('*** Search Model->'+productSearch);
        try {
            List<FieloPRM_LoyaltyEligibleProduct__c> lProducts;
            if(productSearch != Null){
                query = 'SELECT Id,Name,F_PRM_NameDescription__c, F_PRM_Family__c, F_PRM_Range__c FROM FieloPRM_LoyaltyEligibleProduct__c WHERE ';
                
                if(String.isNotBlank(productSearch.sku)){
                    productSearch.sku = 'LIKE \'%'+productSearch.sku+'%\'';
                    query = AP_PRMUtils.getQuery(query, 'Name', productSearch.sku, 'AND');
                }
                if(String.isNotBlank(productSearch.productDesc)){
                    productSearch.productDesc = 'LIKE \'%'+productSearch.productDesc+'%\'';
                    query = AP_PRMUtils.getQuery(query, 'F_PRM_NameDescription__c', productSearch.productDesc, 'AND');
                }
                if(String.isNotBlank(productSearch.productFamily)){
                    productSearch.productFamily = 'LIKE \'%'+productSearch.productFamily+'%\'';
                    query = AP_PRMUtils.getQuery(query, 'F_PRM_Family__c', productSearch.productFamily, 'AND');
                }
                if(String.isNotBlank(productSearch.invCountry)){
                    productSearch.invCountry = '\''+productSearch.invCountry+'\'';
                    query = AP_PRMUtils.getQuery(query, 'F_PRM_Country__c = ', productSearch.invCountry, null);
                }
                if(query.endsWithIgnoreCase('AND ')) query = query.removeEndIgnoreCase('AND ');
                if(query.endsWithIgnoreCase('OR ')){
                    system.debug('*** The query before '+query);
                    query = query.removeEndIgnoreCase('OR ');
                    system.debug('*** The query after '+query);
                    query = query+' )';
                }
                if(query.endsWithIgnoreCase('WHERE')) query.removeEndIgnoreCase('WHERE');
                query = query+' Limit 100';
                System.debug('*** The Query ***'+query);
                lProducts = Database.Query(query);
                searchResults.addAll(lProducts);
            }
        }
        catch(Exception e){
            System.debug('*** Error Searching Accounts with the given account name *** '+e.getMessage());
        }
        return searchResults;
    }
    
    @RemoteAction
    global Static list<FieloPRM_InvoiceDetail__c> getInvoiceDetails(String InvoiceId){
        List<FieloPRM_InvoiceDetail__c> InvoiceDetails = [SELECT ID,F_PRM_Invoice__c,F_PRM_PointsInstaller__c, F_PRM_LoyaltyEligibleProduct__c, F_PRM_LoyaltyEligibleProduct__r.Name, F_PRM_Volume__c, F_PRM_UnitPrice__c,F_PRM_TotalPrice__c FROM FieloPRM_InvoiceDetail__c WHERE F_PRM_Invoice__c =: InvoiceId];
        return InvoiceDetails;
    }
    
    public PageReference checkDuplicateInvoice(){
        OperationStatus = null;
        List<FieloPRM_Invoice__c> triggerNew = new List<FieloPRM_Invoice__c>();
        triggerNew.add(InvoiceAndProdInfo.InvoiceInfo);
        FieloPRM_AP_PRMInvoiceTriggers.checksDuplicates(triggerNew, null, false);
        //System.debug('*** ValidationMessages'+ValidationMessages);
        System.debug('*** OperationStatus'+OperationStatus);
        for(ApexPages.Message message : ApexPages.getMessages()){
            OperationStatus = String.isNotBlank(OperationStatus) ? OperationStatus : String.isNotBlank(message.getSummary()) ? AP_PRMAppConstants.OPERATION_FAIL : OperationStatus;
            System.debug('*** Error'+message.getComponentLabel() + ' : ' + message.getSummary());
            
            addValidationMessages('ERR_DUPLICATE');
        }
        OperationStatus = ValidationMessages == null ? AP_PRMAppConstants.OPERATION_OK : OperationStatus;
        System.debug('*** OperationStatus'+OperationStatus);
        if(OperationStatus == AP_PRMAppConstants.OPERATION_OK )
            addValidationMessages(AP_PRMAppConstants.ManageInvoiceMessages.OK_NO_DUPLICATE.name());
        return null;
    }
    public PageReference changeRetailerAccount(){
        if(string.IsNotBlank(InvoiceAndProdInfo.InvoiceInfo.F_PRM_RetailerAccount__c))
            retailerAccount = [SELECT Id,AccountLocalName__c, StreetLocalLang__c, LocalAdditionalAddress__c, ZipCode__c, LocalCity__c, Country__c, StateProvince__c FROM Account WHERE Id=: InvoiceAndProdInfo.InvoiceInfo.F_PRM_RetailerAccount__c];
        system.debug('*** retailerAccount'+retailerAccount);
        return null;
    }
    @RemoteAction
    global Static String saveInvoiceProducts(List<InvoiceProductDetails> updatedProducts, InvoiceDetails Updateinvoice){
        Savepoint sp = Database.setSavepoint();
        List<FieloPRM_InvoiceDetail__c> upsertInvoiceDetails = new List<FieloPRM_InvoiceDetail__c>();
        List<FieloPRM_InvoiceDetail__c> toDeleteInvoiceDetails = new List<FieloPRM_InvoiceDetail__c>();
        system.debug('*** updatedProducts'+updatedProducts);
        system.debug('*** updatedProducts'+Updateinvoice);
        string statusMessage;
        try{
            //OperationStatus = '';
            
            FieloPRM_Invoice__c toUpdateInvoice = new FieloPRM_Invoice__c(Id = Updateinvoice.InvoiceId);
            if(String.isNotBlank(Updateinvoice.RetailerAccountId) && Updateinvoice.RetailerAccountId != '000000000000000') toUpdateInvoice.F_PRM_RetailerAccount__c = Updateinvoice.RetailerAccountId;
            toUpdateInvoice.Name = Updateinvoice.InvoiceName;
            toUpdateInvoice.F_PRM_InvoiceDate__c = date.valueOf(Updateinvoice.InvoiceDate);
            
            for(InvoiceProductDetails products:updatedProducts){
                if(String.isNotBlank(products.InvoiceProdId) && products.tobeDeleted == 'true'){
                    FieloPRM_InvoiceDetail__c InvoiceDetails = new FieloPRM_InvoiceDetail__c();
                    InvoiceDetails.Id = products.InvoiceProdId;
                    toDeleteInvoiceDetails.add(InvoiceDetails);
                    
                }
                else{
                    FieloPRM_InvoiceDetail__c InvoiceDetails = new FieloPRM_InvoiceDetail__c(F_PRM_Invoice__c = products.InvoiceId);
                    InvoiceDetails.Id = String.isNotBlank(products.InvoiceProdId) ? products.InvoiceProdId : null;
                    InvoiceDetails.F_PRM_LoyaltyEligibleProduct__c = products.skuProductId;
                    InvoiceDetails.F_PRM_Volume__c = products.quantity;
                    InvoiceDetails.F_PRM_UnitPrice__c = products.price;
                    upsertInvoiceDetails.add(InvoiceDetails);
                }
            }
            if(toUpdateInvoice != null && String.isNotBlank(toUpdateInvoice.Id)){
                update toUpdateInvoice;
                if(!upsertInvoiceDetails.isEmpty()){
                    upsert upsertInvoiceDetails;
                    //OperationStatus = AP_PRMAppConstants.OPERATION_OK; 
                }
                if(!toDeleteInvoiceDetails.isEmpty())
                    delete toDeleteInvoiceDetails;
            }
            statusMessage = AP_PRMAppConstants.ManageInvoiceMessages.OK_PRODUCTSUPDATED.name();
        }
        catch(DmlException dmlEx){
            statusMessage = dmlEx.getDmlMessage(0);
            Database.rollback(sp);
        }
        catch(exception ex){
            System.debug('*** '+ex.getMessage());
            statusMessage = ex.getMessage();
            //if(statusMessage.contains('VALIDATION_EXCEPTION'))
            //statusMessage = statusMessage.substringAfter('VALIDATION_EXCEPTION,');
            Database.rollback(sp);
        }
        return statusMessage;
    }
    
    public PageReference ApproveOrReject(){
        PageReference pg;
        //ValidationMessages.clear();
        try{
            System.debug('*** '+OperationStatus);
            System.debug('*** '+InvoiceAndProdInfo.InvoiceInfo.F_PRM_InvoiceRejectionReason__c);
            System.debug('*** '+InvoiceAndProdInfo.InvoiceInfo.Name);
            System.debug('*** '+InvoiceAndProdInfo.InvoiceInfo.F_PRM_RetailerAccount__c);
            if(OperationStatus == 'Approve'){
                InvoiceAndProdInfo.InvoiceInfo.F_PRM_Status__c = 'Approved';
                update InvoiceAndProdInfo.InvoiceInfo;
                pg = new PageReference('/'+InvoiceId);
            }
            else if(OperationStatus == 'Reject' && String.isNotBlank(InvoiceAndProdInfo.InvoiceInfo.F_PRM_InvoiceRejectionReason__c)){
                InvoiceAndProdInfo.InvoiceInfo.F_PRM_Status__c = 'Rejected';
                update InvoiceAndProdInfo.InvoiceInfo;
                pg = new PageReference('/'+InvoiceId);
            }
            
            return pg;           
            
        }
        catch(DmlException dmlEx){
            addValidationMessages(AP_PRMAppConstants.ManageInvoiceMessages.ERR_APPROVEORREJECT.name());
            OperationStatus = AP_PRMAppConstants.OPERATION_FAIL;
            addMessageMap(AP_PRMAppConstants.ManageInvoiceMessages.ERR_APPROVEORREJECT.name(), dmlEx.getDmlMessage(0));
            return null;
        }
        catch(exception ex){
            System.debug('*** '+ex.getMessage());
            addValidationMessages(AP_PRMAppConstants.ManageInvoiceMessages.ERR_APPROVEORREJECT.name());
            OperationStatus = AP_PRMAppConstants.OPERATION_FAIL;
            addMessageMap(AP_PRMAppConstants.ManageInvoiceMessages.ERR_APPROVEORREJECT.name(), ex.getMessage());
            return null;
        }
    }
    
    public PageReference doCancel(){
        System.debug('*** return URL ->'+retURL);
        PageReference pg = new PageReference(retURL);
        return pg;          
    }
    
    public PageReference deleteAttachment(){
        System.debug('*** DelAttachmentId'+DelAttachmentId);
        //ValidationMessages.clear();
        OperationStatus = null;
        PageReference pg;
        Attachment delAtt = new Attachment();
        try{
            for(attachment att:InvoiceAndProdInfo.InvoiceAttachmentsList){
                if(att.id == DelAttachmentId)
                    delAtt = att;
            }
            if(delAtt != null){
                delete delAtt;
                addValidationMessages(AP_PRMAppConstants.ManageInvoiceMessages.OK_DEL_ATTACHMENT.name());
                OperationStatus = AP_PRMAppConstants.OPERATION_OK;
                DelAttachmentId = null;
                pg = new PageReference('/apex/VFP_PRMManageInvoiceProducts?id='+InvoiceId);
                pg.setRedirect(true);
            }
        }
        catch(exception ex){
            System.debug('*** '+ex.getMessage());
            addValidationMessages(AP_PRMAppConstants.ManageInvoiceMessages.ERR_DEL_ATTACHMENT.name());
            OperationStatus = AP_PRMAppConstants.OPERATION_FAIL;
            DelAttachmentId = null;
        }
        return null;
    }
    
    public class InvoiceAndProdDetails{
        public FieloPRM_Invoice__c InvoiceInfo {get; set;}
        public List<Attachment> InvoiceAttachmentsList {get; set;}
        public List<FieloPRM_InvoiceDetail__c> InvoiceDetailsInfo {get;set;}
        public InvoiceAndProdDetails(){
        }
    }
    
    global class SearchProductModel{
        public String sku;
        public String Condition1;
        
        public String productDesc; 
        public String Condition2;
        
        public String productFamily;
        public String Condition3;
        
        public String invCountry;
    }
    
    global class InvoiceProductDetails{
        public String InvoiceId;
        public String InvoiceProdId;
        public String skuProductId;
        public Double quantity;
        public Double price;
        public String tobeDeleted; 
    }
    global class InvoiceDetails{
        public String InvoiceId;
        public String InvoiceName;
        public String InvoiceDate;
        public String RetailerAccountId; 
    }
}