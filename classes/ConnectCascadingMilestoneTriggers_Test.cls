/*
    Author          : Kiran Kareddy (Schneider Electric)
    Date Created    : 08-Feb-2012
    Description     : Test Class for Connect CascadingMilestone Triggers
*/
@isTest
private class ConnectCascadingMilestoneTriggers_Test {
static testMethod void testConnectCascadingMilestone() {
User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect3');
        System.runAs(runAsUser){
            Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            insert inti;
Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division;'+Label.Connect_Smart_Cities,GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',Smart_Cities_Division__c='test');
insert cp;

Team_Members__c C=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Functions', Entity__c='GM');
insert C;


Team_Members__c C1=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Business', Entity__c='ITB');
insert C1;

Team_Members__c C2=new Team_Members__c(Program__c=cp.id,Team_Name__c='Power Region', Entity__c='NA');
insert C2;

Team_Members__c C3=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Business', Entity__c='Partner-Division',Partner_Region__c='test');
insert C3;

Team_Members__c C4=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Functions', Entity__c='GSC-Regional',GSC_Region__c='test1');
insert C4;
Team_Members__c C14=new Team_Members__c(Program__c=cp.id,Team_Name__c='Global Business', Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test');
insert C14;


Connect_Milestones__c mile=new Connect_Milestones__c(Program_Number__c=cp.id,Milestone_Name__c='mile1',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division;'+Label.Connect_Smart_Cities,GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',Smart_Cities_Division__c='test');
insert mile;

Cascading_Milestone__c CM=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm',Milestone_Name__c=mile.id,
Program_Number__c=cp.id,Program_Scope__c='Global Functions',Entity__c='GM',Progress_Summary_by_Champions__c='Completed');
insert CM;
Cascading_Milestone__c CM4= [SELECT Champion_Name__c,Deployment_User_Name__c FROM Cascading_Milestone__c WHERE Id =:CM.Id];
System.assertEquals(C.Champion_Name__c,CM4.Champion_Name__c);
System.assertEquals(C.Team_Member__r.id,CM4.Deployment_User_Name__c);
Cascading_Milestone__c CM1=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm1',Milestone_Name__c=mile.id,
Program_Number__c=cp.id,Program_Scope__c='Global Business',    Entity__c='ITB',Progress_Summary_Deployment_Leader_Q2__c='Completed');
insert CM1;
Cascading_Milestone__c CM5= [SELECT Champion_Name__c,Deployment_User_Name__c FROM Cascading_Milestone__c WHERE Id =:CM1.Id];
System.assertEquals(C1.Champion_Name__c,CM5.Champion_Name__c);
System.assertEquals(C1.Team_Member__r.id,CM5.Deployment_User_Name__c);
Cascading_Milestone__c CM2=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm2',Milestone_Name__c=mile.id,
Program_Number__c=cp.id,Program_Scope__c='Power Region',Entity__c='NA',Progress_Summary_Deployment_Leader_Q3__c='Completed');
insert CM2;
Cascading_Milestone__c CM11=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm11',Milestone_Name__c=mile.id,
Program_Number__c=cp.id,Program_Scope__c='Global Functions',Entity__c='GSC-Regional',GSC_Region__c='test1'  );
insert CM11;
Cascading_Milestone__c CM12=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm12',Milestone_Name__c=mile.id,
Program_Number__c=cp.id,Program_Scope__c='Global Business',Entity__c='Partner-Division',Partner_Region__c='test',Progress_Summary_Deployment_Leader_Q4__c='Completed'  );
insert CM12;
Cascading_Milestone__c CM14=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm14',Milestone_Name__c=mile.id,
Program_Number__c=cp.id,Program_Scope__c='Global Business',Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Progress_Summary_Deployment_Leader_Q4__c='Completed'  );
insert CM14;
Cascading_Milestone__c CM6= [SELECT Champion_Name__c,Deployment_User_Name__c FROM Cascading_Milestone__c WHERE Id =:CM2.Id];
System.assertEquals(C2.Champion_Name__c,CM6.Champion_Name__c);
System.assertEquals(C2.Team_Member__r.id,CM6.Deployment_User_Name__c);
Test.startTest();

CM.Program_Scope__c='Global Business';
CM.Entity__c='ITB';


update CM;



CM1.Entity__c='Power';
try
{
update CM1;
Cascading_Milestone__c CM8= [SELECT ValidateScopeonInsertUpdate__c FROM Cascading_Milestone__c WHERE Id =:CM1.Id];
System.assertEquals(false ,CM8.ValidateScopeonInsertUpdate__c);
}
catch(DmlException d)
{
d.getMessage();
}

Team_Members__c TM=new Team_Members__c(Program__c=cp.id,Team_Member__c=Label.Connect_Email,Team_Name__c='Global Functions', Entity__c='GM');
insert TM;
Team_Members__c TM1=new Team_Members__c(Program__c=cp.id,Team_Member__c=Label.Connect_Email,Team_Name__c='Global Business', Entity__c='ITB');
insert TM1;
Team_Members__c TM2=new Team_Members__c(Program__c=cp.id,Team_Member__c=Label.Connect_Email,Team_Name__c='Power Region', Entity__c='NA');
insert TM2;
Team_Members__c TM3=new Team_Members__c(Program__c=cp.id,Team_Member__c=Label.Connect_Email,Team_Name__c='Global Business', Entity__c='Partner-Division',Partner_Region__c='test');
insert TM3;
Team_Members__c TM4=new Team_Members__c(Program__c=cp.id,Team_Member__c=Label.Connect_Email,Team_Name__c='Global Functions', Entity__c='GSC-Regional',GSC_Region__c='test1');
insert TM4;
Team_Members__c TM10=new Team_Members__c(Program__c=cp.id,Team_Member__c=Label.Connect_Email,Team_Name__c='Global Business', Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test');
insert TM10;

CM.Deployment_User_Name__c=Label.Connect_Email;
update CM;
CM2.Deployment_User_Name__c=Label.Connect_Email;
update CM2;
CM11.Deployment_User_Name__c=Label.Connect_Email;
update CM11;
CM12.Deployment_User_Name__c=Label.Connect_Email;
update CM12;
CM14.Deployment_User_Name__c=Label.Connect_Email;
update CM14;


//_VE case

Connect_Milestones__c mile1=new Connect_Milestones__c(Program_Number__c=cp.id,Milestone_Name__c='mile11');

insert mile1;

Cascading_Milestone__c CM10=new Cascading_Milestone__c(Cascading_Milestone_Name__c='cm1',Milestone_Name__c=mile1.id,
Program_Number__c=cp.id,Program_Scope__c='Global Functions',Entity__c='GM');
try{
insert CM10;
CM10.Entity__c='HR';
update CM10;
}
catch(DmlException a)
{
a.getMessage();
}
Test.stopTest();

}
}
}