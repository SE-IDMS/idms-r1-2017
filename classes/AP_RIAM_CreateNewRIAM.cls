public class AP_RIAM_CreateNewRIAM {
   
    public static void createNewRIAM(List<ReturnItemsApprovalMatrix__c> RIAMs) {
    
    set <String> strCountry = new set <String> ();
    set <String> strCR = new set <String> ();
    set <String> strPF = new set <String> ();
    set <String> strRC = new set <String> ();
    set <String> strCuR = new set <String> ();
    set <String> strC = new set <String> ();
    set <String> strR = new set <String> ();
    set <String> strSR = new set <String> ();

    Map<String,ReturnItemsApprovalMatrix__c> keyObejctMap= new Map<String,ReturnItemsApprovalMatrix__c>();
    
    for(ReturnItemsApprovalMatrix__c riam : RIAMs)
    {       
        if(riam.AccountCountry__c!= null)
        strCountry.add(riam.AccountCountry__c);
        
        if (riam.CommercialReference__c!=null)
        strCR.add(riam.CommercialReference__c);
        
        if(riam.ProductFamily__c!=null)
        strPF.add(riam.ProductFamily__c);
        
        if(riam.Category__c!=null)
        strC.add(riam.Category__c);
    
        if(riam.Reason__c!=null)
        strR.add(riam.Reason__c);
    
        if(riam.SubReason__c!=null)
        strSR.add(riam.SubReason__c);
        
        
        system.debug ('## strCountry ##'+ strCountry);
        system.debug ('## strCR ##'+ strCR );
        system.debug ('## strPF ##'+ strPF);
    

    }
    
    List<ReturnItemsApprovalMatrix__c> existingRecords =[ SELECT AccountCountry__c, TECH_CustomerResolution__c,CustomerResolution__c,CommercialReference__c, ProductFamily__c, Category__c, Reason__c, SubReason__c from ReturnItemsApprovalMatrix__c where (AccountCountry__c in :strCountry) OR (CommercialReference__c in :strCR)OR (ProductFamily__c in :strPF) OR (Category__c in :strC) OR (Reason__c in :strR) OR (SubReason__c in :strSR)];
    
    system.debug ('## existingRecords ##'+ existingRecords);
    
    if(existingRecords!=null &&  existingRecords.size()>0)
    {
        
        for(ReturnItemsApprovalMatrix__c obj: existingRecords ){
            String key='';
            if(obj.AccountCountry__c != null)
            key +=obj.AccountCountry__c;
            if(obj.CommercialReference__c!=null)
            key +=obj.CommercialReference__c;
            if(obj.ProductFamily__c!=null)
            key +=obj.ProductFamily__c;
            if(obj.Category__c!=null)
            key +=obj.Category__c;
            if(obj.Reason__c!=null)
            key +=obj.Reason__c;
            if(obj.SubReason__c!=null)
            key +=obj.SubReason__c;

            
            system.debug ('## key1 ##'+ key );
            
            keyObejctMap.put(key , obj);
            
            system.debug ('## keyObejctMap ##'+ keyObejctMap);
        }
        
        for(ReturnItemsApprovalMatrix__c rtnAM: RIAMs)
        {
            String key='';
            if(rtnAM.AccountCountry__c != null)
            key +=rtnAM.AccountCountry__c;
            if(rtnAM.CommercialReference__c!=null)
            key +=rtnAM.CommercialReference__c;
            if(rtnAM.ProductFamily__c!=null)
            key +=rtnAM.ProductFamily__c;
            if(rtnAM.Category__c!=null)
            key +=rtnAM.Category__c;
            if(rtnAM.Reason__c!=null)
            key +=rtnAM.Reason__c;
            if(rtnAM.SubReason__c!=null)
            key +=rtnAM.SubReason__c;
            
             
            String CurrentRecId=rtnAM.Id;
            
            system.debug ('## key2 ##'+ key );
            
            if(keyObejctMap.containsKey(key)){

                system.debug ('## key Found ##');
                ReturnItemsApprovalMatrix__c exObj = keyObejctMap.get(key);
                if (CurrentRecId!=exObj.Id)
                {
                      // forth field validation 
                String strCRs= exObj.TECH_CustomerResolution__c;
                system.debug ('## strCRs ##'+strCRs);
                Set <string> stCustomerResolutions = new set <string> ();
                list <STRING> lstCustomerResolutions = new list <string>();
                lstCustomerResolutions = strCRs.split(';');
                for (String s: lstCustomerResolutions )
                {
                    stCustomerResolutions.add(s);
                }
                
                String strrtnCR = rtnAM.TECH_CustomerResolution__c;
                system.debug ('## strrtnCR ##'+strrtnCR );
                list <STRING> lsrtnCRs = new list <string>();
                lsrtnCRs = strrtnCR.split(';');
                for (String s: lsrtnCRs )
                {
                   if(stCustomerResolutions.contains(s))
                        {
                            rtnAM.AddError(System.Label.CLDEC12RR07);
                        }
                else {}
                }                   
                }
               
           } 
        }
         
    }
    }

}