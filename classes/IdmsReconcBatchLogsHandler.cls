public class IdmsReconcBatchLogsHandler {
    
    //Create batch logs for both success and failed users
    public static IDMSReconciliationBatchLog__c batchLogForCreateUser(User idmsUsrUpdate, IdmsUsersResyncflowServiceIms.createContactReturnBean responseUims, string batchId){
        IDMSReconciliationBatchLog__c idmsBatchLg = new IDMSReconciliationBatchLog__c();
        if(idmsUsrUpdate.FederationIdentifier!=null){
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS050;
        }else{
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS053;
        }
        idmsBatchLg.Operation__c= Label.CLNOV16IDMS051;
        idmsBatchLg.IdmsIdentityType__c = Label.CLNOV16IDMS052;
        idmsBatchLg.BatchUimsHttpResponse__c = 'FederatedId:'+responseUims.ID+'\n'+'Email Id:'+responseUims.email+'\n'+'Message:'+responseUims.message+'\n'+'Return Code:'+responseUims.returnCode;
        idmsBatchLg.IdmsUser__c = idmsUsrUpdate.Id;
        idmsBatchLg.IdmsBatchId__c = batchId;      
        system.debug('****Idms Batch log return Create User*****'+idmsBatchLg);
        return idmsBatchLg;
    }
    
    //Create batch logs for update users
    public static IDMSReconciliationBatchLog__c batchLogsForUpdateUser(User idmsUsrUpdate, IdmsUsersResyncflowServiceIms.contactUpdateReturnBean responseUims, string batchId){
        IDMSReconciliationBatchLog__c idmsBatchLg = new IDMSReconciliationBatchLog__c();
        if(responseUims.returnCode== 0){
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS050;
        }else{
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS053;
        }
        idmsBatchLg.Operation__c= Label.CLNOV16IDMS056;
        idmsBatchLg.IdmsIdentityType__c = Label.CLNOV16IDMS052;
        idmsBatchLg.BatchUimsHttpResponse__c = 'FederatedId:'+responseUims.federatedId+'\n'+'Has been updated:'+responseUims.hasBeenUpdated+'\n'+'Message:'+responseUims.message+'\n'+'Return Code:'+responseUims.returnCode;
        idmsBatchLg.IdmsUser__c = idmsUsrUpdate.Id;
        idmsBatchLg.IdmsBatchId__c = batchId;      
        system.debug('****Idms Batch log return Update User*****'+idmsBatchLg);
        return idmsBatchLg;
    } 
    
    //Create batch logs for successful company creation
    public static IDMSReconciliationBatchLog__c batchLogForCreateCompany(User processIdmsUser, IdmsUsersResyncflowServiceIms.createReturnBean responseUims, string batchId){
        IDMSReconciliationBatchLog__c idmsBatchLg = new IDMSReconciliationBatchLog__c();
        if(processIdmsUser.IDMSCompanyFederationIdentifier__c!=null){
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS050;
        }else{
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS053;
        }
        idmsBatchLg.Operation__c= Label.CLNOV16IDMS051;
        idmsBatchLg.IdmsIdentityType__c = 'Company';
        idmsBatchLg.BatchUimsHttpResponse__c = 'Company Fed Id:'+responseUims.ID+'\n'+'\n'+'Message:'+responseUims.message+'\n'+'Return Code:'+responseUims.returnCode;
        idmsBatchLg.IdmsUser__c = processIdmsUser.Id;
        idmsBatchLg.IdmsBatchId__c = batchId;      
        system.debug('****Idms Batch log return Create Company*****'+idmsBatchLg);
        return idmsBatchLg;
    }
    
    //ToDo: to verify
    //Create batch logs to fail while creating company.
    public static IDMSReconciliationBatchLog__c batchLogForFailedCreateCompany(String idmsUserId, IDMSCompanyReconciliationBatchHandler__c responseUims, string batchId){
        
        IDMSReconciliationBatchLog__c idmsBatchLg = new IDMSReconciliationBatchLog__c();
        idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS053;
        idmsBatchLg.Operation__c= Label.CLNOV16IDMS051;
        idmsBatchLg.IdmsIdentityType__c = Label.CLNOV16IDMS059;
        idmsBatchLg.BatchUimsHttpResponse__c = 'Company FederatedId:'+responseUims.IdmsUser__r.IDMSCompanyFederationIdentifier__c+'\n'+'Message:'+responseUims.HttpMessage__c+'\n'+'Return Code:'+'14';
        idmsBatchLg.IdmsUser__c = idmsUserId;
        idmsBatchLg.IdmsBatchId__c = batchId;      
        system.debug('****Idms Batch log return Create Company*****'+idmsBatchLg);
        return idmsBatchLg;
    }
    
        //Create batch logs for update users
    public static IDMSReconciliationBatchLog__c batchLogsForUpdateCompany(User idmsUsrUpdate, IdmsUsersResyncflowServiceIms.accountUpdateReturnBean responseUims, string batchId){
        IDMSReconciliationBatchLog__c idmsBatchLg = new IDMSReconciliationBatchLog__c();
        if(responseUims.returnCode== 0){
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS050;
        }else{
            idmsBatchLg.BatchRecordStatus__c = Label.CLNOV16IDMS053;
        }
        idmsBatchLg.Operation__c= Label.CLNOV16IDMS056;
        idmsBatchLg.IdmsIdentityType__c = Label.CLNOV16IDMS059;
        idmsBatchLg.BatchUimsHttpResponse__c = 'FederatedId:'+responseUims.federatedId+'\n'+'Has been updated:'+responseUims.hasBeenUpdated+'\n'+'Message:'+responseUims.message+'\n'+'Return Code:'+responseUims.returnCode;
        idmsBatchLg.IdmsUser__c = idmsUsrUpdate.Id;
        idmsBatchLg.IdmsBatchId__c = batchId;      
        system.debug('****Idms Batch log return Update Company*****'+idmsBatchLg);
        return idmsBatchLg;
    } 
    
}