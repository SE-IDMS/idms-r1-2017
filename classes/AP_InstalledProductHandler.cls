public without sharing class AP_InstalledProductHandler{
public static List<SVMXC__Installed_Product__c> calculateProductInfo(Map<String,List<SVMXC__Installed_Product__c>> mapInstalledProductsWithMissingProductInfo){
   //query for products for information
   List<SVMXC__Installed_Product__c> finallistofAssetsTobeUpdated=new List<SVMXC__Installed_Product__c>();
   for(Product2 productrecord:[select id,ExtProductId__c from Product2 where ExtProductId__c in :mapInstalledProductsWithMissingProductInfo.keySet()])
   {
    for(SVMXC__Installed_Product__c assetRecord:mapInstalledProductsWithMissingProductInfo.get(productrecord.ExtProductId__c))
    {
        finallistofAssetsTobeUpdated.add(new SVMXC__Installed_Product__c(id=assetRecord.id,SVMXC__Product__c=productrecord.id));
    }       
   }
   return finallistofAssetsTobeUpdated;   
}
}