/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 12-July-2012
    Modification Log    : 
    Description         : Initiative Health Status Report
*/

public with sharing class ConnectInitiativeHealthStatusController {

    public ConnectInitiativeHealthStatusController(ApexPages.StandardController controller) {
    
    Quarter = 'Q1';
    RenPDF = '';
    Year = ApexPages.CurrentPage().getParameters().get('Year');
    if(Year == null)
       Year = '2013';

    }
 // Declaration Section

 public string Quarter {get; set;}
 public string Year {get; set;}
 public string RenPDF{get; set;}
 public string PE_Health;
 public string SE_Health;
 public string TSC_Health;
 public string EL_Health; 
 public string EI_Health;
 public string EW_Health;
 public string GCI_Health;
 public string GE_Health;
 public string RE_Health;
 public string OM_Health;
 public string OE_Health;
 public string GC_Health;
 public string TL;

 public String LongDate = ' ';

 


public List<SelectOption> getFilterList() {
       List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('2012','2012'));  
       options.add(new SelectOption('2013','2013')); 
       options.add(new SelectOption('2014','2014'));        
       return options;
   }
    
public List<SelectOption> getFilterQuarter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('Q1','Q1'));  
       options.add(new SelectOption('Q2','Q2'));     
       options.add(new SelectOption('Q3','Q3'));  
       options.add(new SelectOption('Q4','Q4'));       
       return options;
  }
  

 public string getYear(){
 return Year ;
 }
  
 public string getQuarter(){
 return Quarter ;
 }
    
 public pagereference RenderPDF(){
 renPDF = 'PDF';
 return null ;
 }
    
 public pagereference QuarterChange(){
     
     return null;
    
    }
    
  public pagereference YearChange(){
     if(Year == '2013' || Year == '2014'){
     PageReference pageRef = new PageReference('/apex/Connect_Initiative_Health_Status2013?Year=2013');
     return (pageRef);
     }
     else{
       PageReference pageRef2012 = new PageReference('/apex/Connect_Initiative_Health_Status_Report?Year=2012');
       return (pageRef2012); 
       }  
    }

Public void TrafficLight(){

List<Initiatives__c> Init = new List<Initiatives__c>();

Init = [Select Name, Initiative_Health_Status__c, Initiative_Health_Status_by_Sponsors__c, Q2_Initiative_Health__c,Q2_Initiative_Health_Status__c,Q3_Initiative_Health__c,Q3_Initiative_Health_Status__c,Q4_Initiative_Health__c,Q4_Initiative_Health_Status__c from Initiatives__c where Year__c = :Year];

for(Initiatives__c CIn:Init){

//Q1 Health

if(Quarter == 'Q1'){
 if(CIn.Initiative_Health_Status_by_Sponsors__c == Label.ConnectStatusNoData)
     TL = '/resource/Connect_Grey';
  Else
    if(CIn.Initiative_Health_Status_by_Sponsors__c == Label.ConnectStatusSlightDelay)
      TL = '/resource/Connect_Yellow';
  Else
    if(CIn.Initiative_Health_Status_by_Sponsors__c == Label.ConnectStatusOnTimeDelivered)
        TL = '/resource/Connect_Green';
  Else
     TL = '/resource/Connect_Red';
     }
     //Q2 Health
     
  if((Quarter == 'Q2')){
         if(CIn.Q2_Initiative_Health_Status__c == Label.ConnectStatusNoData)
            TL = '/resource/Connect_Grey';
        Else
         if(CIn.Q2_Initiative_Health_Status__c == Label.ConnectStatusSlightDelay)
            TL = '/resource/Connect_Yellow';
        Else
         if(CIn.Q2_Initiative_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
            TL = '/resource/Connect_Green';
        Else
          TL = '/resource/Connect_Red';
        }
        
// Q3 Health
        
    if((Quarter == 'Q3')){
         if(CIn.Q3_Initiative_Health_Status__c == Label.ConnectStatusNoData)
            TL = '/resource/Connect_Grey';
        Else
         if(CIn.Q3_Initiative_Health_Status__c == Label.ConnectStatusSlightDelay)
            TL = '/resource/Connect_Yellow';
        Else
         if(CIn.Q3_Initiative_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
            TL = '/resource/Connect_Green';
        Else
          TL = '/resource/Connect_Red';
        }
    // Q4 Health
        
    if((Quarter == 'Q4')){
         if(CIn.Q4_Initiative_Health_Status__c == Label.ConnectStatusNoData)
            TL = '/resource/Connect_Grey';
        Else
         if(CIn.Q4_Initiative_Health_Status__c == Label.ConnectStatusSlightDelay)
            TL = '/resource/Connect_Yellow';
        Else
         if(CIn.Q4_Initiative_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
            TL = '/resource/Connect_Green';
        Else
          TL = '/resource/Connect_Red';
        }
        
        // Check for the Initiative Names
        
        if((CIn.Name == Label.ConnectInitEL))
            EL_Health = TL;
         Else If(CIn.Name == Label.ConnectInitEI)
             EI_Health = TL;
         Else if(CIn.Name == Label.ConnectInitEW)
             EW_Health = TL;
         Else if((CIn.Name == Label.ConnectInitGCI))
           GCI_Health = TL;
         Else if((CIn.Name == Label.ConnectInitGC))
          GC_Health = TL;
         Else if((CIn.Name == Label.ConnectInitGE))
          GE_Health = TL;
         Else if((CIn.Name == Label.ConnectInitOE))
           OE_Health = TL;
         Else if((CIn.Name == Label.ConnectInitOM))
            OM_Health = TL;
         Else if((CIn.Name == Label.ConnectInitPE))
            PE_Health = TL;
         Else if((CIn.Name == Label.ConnectInitRE))
            RE_Health = TL;
         Else if((CIn.Name == Label.ConnectInitSE))
            SE_Health = TL;    
         Else if((CIn.Name == Label.ConnectInitTSC))
            TSC_Health = TL;
        
         
      }
     
  }
 public string getEL_Health(){
   TrafficLight();
   return EL_Health;
   }
   
public string getEI_Health(){
   TrafficLight();
   return EI_Health;
   }
public string getEW_Health(){
   TrafficLight();
   return EW_Health;
   }

public string getGCI_Health(){
   TrafficLight();
   return GCI_Health;
   }
   
public string getGC_Health(){
   TrafficLight();
   return GC_Health;
   }
   
public string getGE_Health(){
   TrafficLight();
   return GE_Health;
   }
  
public string getOE_Health(){
   TrafficLight();
   return OE_Health;
   } 
   
public string getOM_Health(){
   TrafficLight();
   return OM_Health;
   } 
   
public string getPE_Health(){
   TrafficLight();
   return PE_Health;
   }  
public string getRE_Health(){
   TrafficLight();
   return RE_Health;
   } 
   
public string getSE_Health(){
   TrafficLight();
   return SE_Health;
   }  
   
public string getTSC_Health(){
   TrafficLight();
   return TSC_Health;
   }


   // Init cLongDate with the current Datetime in long date format    
  public String getLongDate() {
    Datetime cDT = System.now();
    LongDate = cDT.format('EEEE, MMMM d, yyyy');
    return LongDate;
  }


}