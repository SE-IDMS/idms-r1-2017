/*
    Author          : Hari Krishna
    Date Created    : 
    Description     : Utility class for SolutionCenterCountry Access extending abstract Data Source Class
*/

public class Utils_SolutionCenterCountryDataSource extends Utils_DataSource
{     
     //    Query Parameters - Main Query and Different cluases which will be used conditionally
        static String query1 = 'Select ID ,Country__c ,toLabel( Solution_Center__r.BusinessesServed__c), toLabel( Solution_Center__r.TierCategory__c) ,Solution_Center__c, Country__r.Name , Solution_Center__r.name,Solution_Center__r.Inactive__c From SolutionCenterCountry__c';
        
        Static String WhereClause ='';
       
     //    End of Query String cretaion
                          
     // Define Columns Label and displayed field  
     public override List<SelectOption> getColumns()
     {
        List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Field1__c',sObjectType.SolutionCenterCountry__c.fields.Solution_Center__c.getLabel())); // First Column
        Columns.add(new SelectOption('Field2__c',sObjectType.SolutionCenterCountry__c.fields.Country__c.getLabel()));       
        Columns.add(new SelectOption('Field3__c','Business Unit'));  
        Columns.add(new SelectOption('Field5__c','Tier Category')); 
        //Columns.add(new SelectOption('Field6__c',sObjectType.SolutionCenter__c.fields.Inactive__c.getLabel())); 
        //Columns.add(new SelectOption('Field4__c','SolutionCenter_ID'));     
        return Columns;
     }
        
     //    Main Search Method
     public override Result Search(List<String> keyWords, List<String> filters)
     {
        //    Initializing Variables      
        String searchText; 
        String business;
        String country;
        String tier;
        List<sObject> results = New List<sObject>();
        List<sObject> tempResults = New List<sObject>();
        Utils_Sorter sorter = new Utils_Sorter();
        List<Id> compId = New List<Id>();
        Utils_DataSource.Result returnedResult = new Utils_DataSource.Result();   
        returnedResult.recordList = new List<DataTemplate__c>();
        Map<Object, String> countries = New Map<Object, String>();
        Map<String,sObject> resultData = New Map<String,sObject>();
        Map<Object, String> pickList = New Map<Object, String>();
        Map<Integer,DataTemplate__c> resultMap = new Map<Integer,DataTemplate__c>();
        List<Object> countryId = New List<Object>();
        List<sObject> resultset = New List<sObject>();
         String final_query;
        //    End of Variable Initialization
        
        //    Assigning the User input Search Filter and Search criteria to the Variables
       
        if(!(filters[0].equalsIgnoreCase(Label.CL00355)))
            business = filters[0];
        if(!(filters[1].equalsIgnoreCase(Label.CL00355)))
            country = filters[1];
        if(!(filters[2].equalsIgnoreCase(Label.CL00355)))
            tier = filters[2];

        if(keywords != null && keywords.size()>0)
            searchText = keyWords[0];
        else 
            searchText = '';
          
        //    End of Assignment
      
        System.debug('#### Competitor Search Parameters #####');
        System.debug('#### Search Text: ' + searchText);
        System.debug('#### Business: ' + business);
        System.debug('#### Country: ' + country);
        System.debug('#### Tier Category: ' + tier);
                  
        //    Searching the Solution Center Country to get all the combination the List of Competitors
        //    and all the Country they are associated to
          
        
            
            
        if(searchText != NULL && searchText!= '')
        {
            if(WhereClause.length()>0){
                
                WhereClause +=' AND Solution_Center__r.Inactive__c = FALSE ';
            }
            else{
                
                 WhereClause +=' Solution_Center__r.Inactive__c = FALSE ';
            }
            
            if(WhereClause.length()>0){
                
                WhereClause +=' AND Solution_Center__r.name LIKE \'%'+searchText+'%\' ';
            }
            else{
                
                 WhereClause +='  Solution_Center__r.name LIKE \'%'+searchText+'%\' ';
            }
        }
        if(business != NULL && business!= '')
        {
            if(WhereClause.length()>0){
                
                WhereClause +=' AND Solution_Center__r.BusinessesServed__c includes ( \''+business+'\') ';
            }
            else{
                
                 WhereClause +='  Solution_Center__r.BusinessesServed__c includes ( \''+business+'\') ';
            }
        }
        if(country  != null  ){
            
           
            
            if(WhereClause.length()>0){
               
                WhereClause +=' AND Country__c = \''+country +'\' ';
            }
            else{
                 WhereClause +='  Country__c = \''+country +'\' ';
            }
            
        }
        if(tier != NULL && tier!= '')
        {
            if(WhereClause.length()>0){
                
                WhereClause +=' AND Solution_Center__r.TierCategory__c = \''+tier+'\' ';
            }
            else{
                
                 WhereClause +='  Solution_Center__r.TierCategory__c = \''+tier+'\' ';
            }
        }
       final_query=query1 +' where '+WhereClause;
          System.debug('**************************** final query *********************'+final_query);
          Utils_SDF_Methodology.log(' query: ',final_query); 
       results = Database.query(final_query);
        
        
        
        System.debug('#### results: ' +  results );
        
        
        //    Start of processing the Search Result to prepare the final result set
        tempResults.clear();
        for (sObject obj : results)
        { 
            SolutionCenterCountry__c records = (SolutionCenterCountry__c)obj;
            DataTemplate__c solcc = New DataTemplate__c ();
            solcc.put('Field1__c',records.Solution_Center__r.name);
            solcc.put('Field2__c',records.Country__r.Name);
            solcc.put('Field3__c',records.Solution_Center__r.BusinessesServed__c);
            solcc.put('Field4__c',records.Solution_Center__c);            
            solcc.put('Field5__c',records.Solution_Center__r.TierCategory__c);
            /*if(records.Solution_Center__r.Inactive__c == true)
                solcc.put('Field6__c','Yes');
            else
                solcc.put('Field6__c','No');
            */
            tempResults.add(solcc);              
        }
        System.debug('#### Results without Country Filter: ' +  tempResults);
        returnedResult.recordList.clear();
        
        returnedResult.recordList.addAll(tempResults);
        
        //    End of Processing and preparation of the final result Set
        
        returnedResult.recordList = (List<sObject>)sorter.getSortedList(returnedResult.recordList, 'Field1__c', true);
        System.debug('#### Final Result :' + returnedResult);
        
        resultset.addAll(returnedResult.recordList);
        returnedResult.numberOfRecord = resultset.size();

        if(returnedResult.numberOfRecord >100)
        {
            returnedResult.recordList.clear();
            for(Integer count=0; count< 100; count++)
            {
                returnedResult.recordList.add(resultset[count]);      
            }
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        
        return returnedResult;
     }
}