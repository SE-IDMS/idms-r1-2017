public class VFC_ManageAccountExternalInfo extends VFC_ExternalInfoDashboardController {
    
    private final String multiFldChkEveryValue = 'PLMarketServed__c,PLDomainsOfExpertise__c';
    private boolean isPrefill;
    private String targetApplication;
    public String accCountryid { get; set; } //added by pradeep
    public String SaveAction { get; set; }
    public String ReturnToTab { get; set; }
    public String NewPrimaryContactId { get; set; }
    public String SelectedBusinessType { get; set; }
    public String SelectedAreaOfFocus { get; set; }

    public List<SelectOption> PrimaryContactList { get; set; }
    public List<SelectOption> BusinessTypeList { get; set; }
    public List<SelectOption> AreaOfFocusList { get; set; }

    public List<AssessmentResponse> ResponseList { get; set; }
    
    public Boolean Prefill {
        get { return isPrefill; }
        private set;
    }

    public Boolean IsDefaultAccount {
        get {
            if (String.isBlank(this.DashboardInfo.AccountInfo.PRMCompanyName__c)) return false;
            else
                return this.DashboardInfo.AccountInfo.PRMCompanyName__c.ContainsIgnoreCase(System.Label.CLJUL15PRM027);
        }
        private set;
    }

    public VFC_ManageAccountExternalInfo() {
        targetApplication = ApexPages.currentPage().getParameters().get('app');
        if (ApexPages.currentPage().getParameters().containsKey('prefill')) {
            this.isPrefill = true;
            Account tA = DashboardInfo.AccountInfo;

            tA.PRMAccount__c = true;
            Map<String, CS_PRM_AccountFieldsMapping__c> fldMap =  CS_PRM_AccountFieldsMapping__c.getAll();
            for (String fldKey : fldMap.keySet()) {
                System.debug('*** Field:' + fldKey);
                System.debug('*** Field Value:' + tA.get(fldMap.get(fldKey).AccountSourceField__c));

                if (tA.get(fldKey) == null)
                    tA.put(fldKey, tA.get(fldMap.get(fldKey).AccountSourceField__c));
            }
        }

        ResponseList = new List<AssessmentResponse>();
        List<OpportunityAssessmentResponse__c> lResponses = new List<OpportunityAssessmentResponse__c>([
                SELECT Question__c,QuestionText__c, AnswerOption__c, ResponseText__c,Question__r.AnswerType__c,Answer__c
                FROM OpportunityAssessmentResponse__c 
                WHERE OpportunityAssessment__r.Assessment__r.RecordType.DeveloperName = 'PRMUserRegistration'
                AND Account__c = :RecordId]);
        
        System.debug('***' +lResponses);
        System.debug('***' +lResponses.size());
        System.debug('***' +RecordId);
        String multiPicklistResponse;
        String multiPicklistQuestionText;
        String multiPicklistAnswerType;

        this.PrimaryContactList = new List<SelectOption>();
        this.PrimaryContactList.add(new SelectOption('','--None--'));

        this.AreaOfFocusList = new List<SelectOption>();
        this.AreaOfFocusList.add(new SelectOption('', '--None--'));

        this.SelectedBusinessType = this.DashboardInfo.AccountInfo.PRMBusinessType__c;
        this.SelectedAreaOfFocus = this.DashboardInfo.AccountInfo.PRMAreaOfFocus__c;
        accCountryid =this.DashboardInfo.AccountInfo.PRMCountry__c; //added by pradeep

        for ( EligiblePrimaryContact ppc : this.PPCEligible) {
            this.PrimaryContactList.add (new SelectOption(ppc.Id, ppc.Name));
        }
        this.PopulateAvailableChannels();
        this.PopulateAvailableSubChannels();

        Map<Id,List<OpportunityAssessmentResponse__c>> mapQuestionResponses = new Map<Id,List<OpportunityAssessmentResponse__c>>();
        for(OpportunityAssessmentResponse__c response : lResponses){
            if(mapQuestionResponses.containsKey(response.Question__c))
                mapQuestionResponses.get(response.Question__c).add(response);
            else
                mapQuestionResponses.put(response.Question__c,new List<OpportunityAssessmentResponse__c>{response});
        }
        for (Id questionId:mapQuestionResponses.keySet()){
            for (OpportunityAssessmentResponse__c response : mapQuestionResponses.get(questionId)) {
                //Picklist Or Radio Button
                AssessmentResponse tResponse;
                if(response.Question__r.AnswerType__c == Label.CLOCT13PRM11 || response.Question__r.AnswerType__c == Label.CLOCT13PRM29){
                    tResponse = new AssessmentResponse(response.QuestionText__c, response.AnswerOption__c, response.Question__r.AnswerType__c);
                    ResponseList.add(tResponse);
                }
                // Test
                else if(response.Question__r.AnswerType__c == 'Text'){
                    tResponse = new AssessmentResponse(response.QuestionText__c, response.ResponseText__c, response.Question__r.AnswerType__c);
                    ResponseList.add(tResponse);
                }
                // MultiPicklist
                else if(response.Question__r.AnswerType__c == Label.CLOCT13PRM12){
                    multiPicklistResponse = String.isblank(multiPicklistResponse) ? response.AnswerOption__c : multiPicklistResponse + ', ' + response.AnswerOption__c;
                    multiPicklistQuestionText = response.QuestionText__c;
                    multiPicklistAnswerType = response.Question__r.AnswerType__c;
                }
            }
            if(String.isNotBlank(multiPicklistResponse)){
                ResponseList.add( new AssessmentResponse(multiPicklistQuestionText, multiPicklistResponse, multiPicklistAnswerType));
                multiPicklistResponse = null;
                multiPicklistQuestionText = null;
                multiPicklistAnswerType = null;
            }
        }
        if(TabName == 'pgmmbmrship'){
            Set<ID> BadgeAccountSet = new Set<ID>();
            List<FieloPRM_BadgeAccount__c> bdgAccountList = [SELECT ID, F_PRM_Account__c, F_PRM_Badge__c, CreatedDate, F_PRM_Badge__r.Name, 
                                                            F_PRM_Badge__r.F_PRM_Challenge__c, F_PRM_Badge__r.F_PRM_ProgramLevel__c,
                                                            F_PRM_Badge__r.F_PRM_ProgramLevel__r.Name, F_PRM_Badge__r.F_PRM_ProgramLevel__r.PartnerProgram__c, 
                                                            F_PRM_Badge__r.F_PRM_ProgramLevel__r.PartnerProgram__r.Name, F_PRM_Badge__r.F_PRM_Challenge__r.Name 
                                                            FROM FieloPRM_BadgeAccount__c WHERE F_PRM_Account__c =: RecordId 
                                                            AND F_PRM_Badge__r.F_PRM_TypeSelection__c =:'Program Level'
                                                            ];
                                                            // 
                                                            system.debug('****bdgAccountList ***'+bdgAccountList );
            for(FieloPRM_BadgeAccount__c bdgAccount:bdgAccountList){
                String ProgramOrBadge = String.isNotBlank(bdgAccount.F_PRM_Badge__r.F_PRM_ProgramLevel__c) ? bdgAccount.F_PRM_Badge__r.F_PRM_ProgramLevel__r.PartnerProgram__r.Name : bdgAccount.F_PRM_Badge__r.F_PRM_Challenge__r.Name;
                ProgramMembership pgm = new ProgramMembership(bdgAccount, ProgramOrBadge, null);
                DashboardInfo.ProgramMembershipList.add(pgm);
                if(String.isNotBlank(bdgAccount.F_PRM_Badge__r.F_PRM_ProgramLevel__c))
                    BadgeAccountSet.add(bdgAccount.F_PRM_Badge__r.F_PRM_ProgramLevel__c);
            }
            List<ACC_PartnerProgram__c> AccAsignProList = [SELECT ID,CreatedDate,PartnerProgram__r.Name,Account__c,Active__c,Eligiblefor__c,PartnerProgram__c,PartnerProgramName__c,ProgramLevel__c,ProgramLevel__r.Name,TECH_UniqueAssignedId__c FROM ACC_PartnerProgram__c WHERE Account__c =:RecordId];
            for(ACC_PartnerProgram__c AccAsignPro:AccAsignProList){
                if(!BadgeAccountSet.contains(AccAsignPro.ProgramLevel__c)){
                    String ProgramOrBadge = AccAsignPro.PartnerProgram__r.Name;
                    ProgramMembership pgm = new ProgramMembership(null, ProgramOrBadge, AccAsignPro);
                    DashboardInfo.ProgramMembershipList.add(pgm);
                }
                    
            }
        }
    }
    
    // This is method is called when clicked on Save button. Would determin if the record isDirty and commit the changes
    // only when isDirty returns true
    public override PageReference Save(){
        Boolean recChanged = this.isDirty();
        PageReference pr = null;
        ValidationMessages.clear();
        if (recChanged) {
            this.ValidateInputData();
            system.debug('***'+ValidationMessages);
            if(ValidationMessages.isEmpty()){
                try {
                    if (SaveAction == 'CMPINFO') // Update IMS when company information is updated and not locator information
                        this.UpdateAccountInIMS();
                    UPDATE this.DashboardInfo.AccountInfo;
                    pr = this.GetFullUrl('success');
                } 
                catch (Exception ex){
                    ValidationMessages.add( 'Error while saving: ' + ex.getMessage() + ex.getStackTraceString());
                    SaveStatus = 'valErrors';
                }
            }
            else
                SaveStatus = 'valErrors';
        }
        else{
            pr = this.GetFullUrl('nochange');
        }
        return pr;
    }

    protected override void ValidateInputData() {
        Account thisAcc = this.DashboardInfo.AccountInfo;

        if (String.isBlank(thisAcc.PRMCountry__c) || String.isBlank(thisAcc.PRMBusinessType__c) || String.isBlank(thisAcc.PRMAreaOfFocus__c))
            ValidationMessages.add('Country / Business Type / Area of Focus cannot be left blank');
        else {
            String accCntryCode = thisAcc.PRMCountry__r.CountryCode__c;
            List<CountryChannels__c> lChnl = new List<CountryChannels__c>([SELECT Id,Name FROM CountryChannels__c
                                                    WHERE (PRMCountry__r.Country__r.CountryCode__c = :accCntryCode OR PRMCountry__r.MemberCountry1__r.CountryCode__c = :accCntryCode OR PRMCountry__r.MemberCountry2__r.CountryCode__c = :accCntryCode OR PRMCountry__r.MemberCountry3__r.CountryCode__c = :accCntryCode OR PRMCountry__r.MemberCountry4__r.CountryCode__c = :accCntryCode OR PRMCountry__r.MemberCountry5__r.CountryCode__c = :accCntryCode) 
                                                    AND ChannelCode__c = :thisAcc.PRMBusinessType__c 
                                                    AND SubChannelCode__c = :thisAcc.PRMAreaOfFocus__c 
                                                    AND PRMCountry__r.CountryPortalEnabled__c = true
                                                    AND Active__c = true]);
            if(lChnl.isEmpty() && targetApplication != 'POMP')
                ValidationMessages.add('No active channel found for selected country, business type and area of focus');    
            
            
        }

        if(thisAcc.PRMAccountRegistrationStatus__c == 'Declined' && String.isBlank(thisAcc.PRMReasonForDecline__c))
            ValidationMessages.add('Reason for Declining Registration is Mandatory for this status');

        if(thisAcc.PRMReasonForDecline__c == 'Other' && String.isBlank(thisAcc.PRMAdditionalComments__c))
            ValidationMessages.add('Additional Comments is Mandatory for this Reason for Declining Registration');
    }

    // This method is used to determine if any field values in the record have changed or not
    protected override Boolean IsDirty () {
        Boolean hasChanged = false;
        String qry = 'SELECT ' + AccountFields + ' FROM Account WHERE Id = :RecordId LIMIT 1';
        System.debug('*** Query: ' + qry);

        Account originalAccount = Database.Query(qry);
        Account thisAccount = this.DashboardInfo.AccountInfo;
        List<String> lAccFields = AccountFields.Split(',');

        for (String s : lAccFields) {
            System.debug('*** Field Original New Value: ' + s + ',' + originalAccount.get(s) + ',' + thisAccount.get(s));
            if (!multiFldChkEveryValue.ContainsIgnoreCase(s) && originalAccount.get(s) != thisAccount.get(s)) {
                System.debug('*** Field changed: ' + s);
                hasChanged = True;
                break;
            }
            else if (multiFldChkEveryValue.ContainsIgnoreCase(s) && HasMultiPicklistValueChanged(originalAccount.get(s), thisAccount.get(s), ';')) {
                System.debug('*** Field changed: ' + s);
                hasChanged = true;
                break;
            }
        }
        System.debug('*** isDirty: ' + hasChanged);
        return hasChanged;
    }

    // This method is called from VFP_PRMChangePrimaryContact
    public PageReference UpdatePrimaryContact () {
        Boolean isUpdatedinUIMS;
        System.debug('NewPrimaryContactId:' + NewPrimaryContactId);
        System.debug('PrimaryContact:' + PrimaryPartnerContact);
        try {
            List<Contact> newPrimaryContact = ([SELECT Id, PRMPrimaryContact__c, PRMUIMSID__c FROM Contact WHERE Id = :NewPrimaryContactId]);
            Contact toPrimaryCon;
            List<Contact> toUpdateList = new List<Contact>();
            if(NewPrimaryContactId != PrimaryPartnerContact.Id ) {
                toPrimaryCon  = newPrimaryContact [0];
                toPrimaryCon.PRMPrimaryContact__c = true;
                PrimaryPartnerContact.PRMPrimaryContact__c = false;

                toUpdateList.add(toPrimaryCon);
                toUpdateList.add(PrimaryPartnerContact);
                
                if(!toUpdateList.isEmpty()) {
                    system.debug('**** the size is ***' + toUpdateList.Size());
                    isUpdatedinUIMS = AP_PRMUtils.UpdatePrimaryContact_Admin(PrimaryPartnerContact, toPrimaryCon);
                    if(isUpdatedinUIMS){
                        UPDATE toUpdateList;
                        PrimaryPartnerContact = toPrimaryCon;
                        NewPrimaryContactId = toPrimaryCon.Id;
                        SaveStatus = 'success';
                    }
                    else {
                        ValidationMessages.add('Failed to process primary partner contact change request in IMS');
                        SaveStatus = 'valErrors';
                    }
                }
            }
            //else
            //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select a Non Primary Contact'));
        }
        catch(Exception ex){
            ValidationMessages.add( 'Error while updating Primary Contact: ' + ex.getMessage() + ex.getStackTraceString());
            SaveStatus = 'valErrors';
        }
        return null;
    }

    public PageReference UpdateChannelInfo () {
        try {
            String sUIMSId;
            List<Account> lAcc = new List<Account>([SELECT Id, PRMUIMSID__c FROM Account WHERE Id = :RecordId]); 
            if (!lAcc.isEmpty())
                sUIMSId = lAcc[0].PRMUIMSID__c;

            Account tAccount = new Account(Id = RecordId, PRMBusinessType__c = this.SelectedBusinessType, PRMAreaOfFocus__c = this.SelectedAreaOfFocus);

            this.DashboardInfo.AccountInfo.PRMBusinessType__c = this.SelectedBusinessType;
            this.DashboardInfo.AccountInfo.PRMAreaOfFocus__c = this.SelectedAreaOfFocus;

            Boolean imsUPDStatus = this.UpdateAccountInIMS();
            if (imsUPDStatus != null && imsUPDStatus == true && String.isNotBlank(sUIMSId)) {
                UPDATE tAccount;
                SaveStatus = 'success';                
            }
            else {
                ValidationMessages.add ('Error while updating account in IMS. ');
                SaveStatus = 'valErrors';                
            }
        }
        Catch (Exception ex) {
            ValidationMessages.add ('Error while updating account' + ex.getMessage() + ex.getStackTraceString());
            SaveStatus = 'valErrors';
        }
        return null;
    }

    public PageReference UpdateLocatorListing() {
        PageReference pr = null;
        try {
            Account plAcc = new Account(Id = RecordId, PLShowInPartnerLocator__c = SaveAction);
            UPDATE plAcc;

            SaveStatus = 'success';
            pr = this.GetFullUrl('success', 'VFP_ManageLocatorListing');
        }
        catch (Exception ex) {
            SaveStatus = 'valErrors';
            ValidationMessages.add ('Error while updating locator listing status. ' + ex.getMessage() + ex.getStackTraceString());
        }
        return pr;
    }

    // This method is called when clicked on 'Cancel' button and uses 'ReturnToTab' to return to view mode of
    // respective tab
    public PageReference DoCancel () {
        return this.GetFullUrl('');
    }
    
    public PageReference DoCancelLocator () {
        return this.GetFullUrl('', 'VFP_ManageLocatorListing');
    }

    public override void PopulateAvailableChannels () {
        String cntryId = this.DashboardInfo.AccountInfo.PRMCountry__c;
        Set<String> tChannels = new Set<String>();
        List<PRMCountry__c> lCntry = new List<PRMCountry__c>([SELECT Id,SupportedLanguage1__c,SupportedLanguage2__c,SupportedLanguage3__c,Country__c,MemberCountry1__c,MemberCountry2__c,MemberCountry3__c,MemberCountry4__c,MemberCountry5__c,
                                                        (SELECT Id, ChannelCode__c,Channel_Name__c, SubChannelCode__c,Sub_Channel__c, Country__c FROM CountryChannels__r WHERE Active__c = true)
                                                        FROM PRMCountry__c WHERE CountryPortalEnabled__c = True 
                                                        AND (Country__c = :cntryId OR MemberCountry1__c = :cntryId OR MemberCountry2__c = :cntryId OR MemberCountry3__c = :cntryId OR MemberCountry4__c = :cntryId OR MemberCountry5__c = :cntryId)]);

        System.debug('**Country Channel List**' + lCntry);
        this.BusinessTypeList = new List<SelectOption>();
        this.BusinessTypeList.add(new SelectOption('', '--None--'));

        if (lCntry != null && !lCntry.isEmpty()) { 
            // Map<String, String> mBT = new Map<String, String>();
            PRMCountry__c tCntry = lCntry[0];
            List<CountryChannels__c> lChannels = tCntry.CountryChannels__r;
            if (lChannels != null && !lChannels.isEmpty()) {
                for (CountryChannels__c tChnl : lChannels) {
                    if (!tChannels.contains(tChnl.ChannelCode__c))
                        tChannels.add(tChnl.ChannelCode__c);
                }
            }
            Schema.DescribeFieldResult btResult = PRMLovs__c.BT__c.getDescribe();
            List<Schema.PicklistEntry> btPicklistValues = btResult.getPicklistValues();

            for( Schema.PicklistEntry pValue : btPicklistValues){
                if (tChannels.Contains(pValue.getValue()))
                    this.BusinessTypeList.add(new SelectOption(pValue.getValue(), pValue.getLabel())); 
            }
        }
    }

    public override PageReference PopulateAvailableSubChannels () {
        String cntryId = this.DashboardInfo.AccountInfo.PRMCountry__c;
        System.debug('*** Account Country: ' + cntryId);
        System.debug('*** Selected Business Type: ' + this.SelectedBusinessType);

        List<PRMCountry__c> lCntry = new List<PRMCountry__c>([SELECT Id,SupportedLanguage1__c,SupportedLanguage2__c,SupportedLanguage3__c,Country__c,MemberCountry1__c,MemberCountry2__c,MemberCountry3__c,MemberCountry4__c,MemberCountry5__c,
                                                                    (SELECT Id, ChannelCode__c,Channel_Name__c, SubChannelCode__c,Sub_Channel__c, Country__c FROM CountryChannels__r WHERE  ChannelCode__c = :this.SelectedBusinessType AND Active__c = true)
                                                                FROM PRMCountry__c WHERE CountryPortalEnabled__c=True 
                                                                AND (Country__c = :cntryId OR MemberCountry1__c = :cntryId OR MemberCountry2__c = :cntryId OR MemberCountry3__c = :cntryId OR MemberCountry4__c = :cntryId OR MemberCountry5__c = :cntryId)]);

        this.AreaOfFocusList.Clear();
        this.AreaOfFocusList.add(new SelectOption('','--None--'));
        
        Map<String, String> mAF = new Map<String, String>();
        Schema.DescribeFieldResult afResult = PRMLovs__c.AF__c.getDescribe();
        List<Schema.PicklistEntry> afPicklistValues = afResult.getPicklistValues();

        for( Schema.PicklistEntry pValue : afPicklistValues){
            mAF.put(pValue.getValue(), pValue.getLabel()); 
        }

        PRMCountry__c tCntry = lCntry[0];
        List<CountryChannels__c> lChannels = tCntry.CountryChannels__r;
        System.debug('*** Sub Channels:' + lChannels);

        Set<String> sSubChannels = new Set<String>();
        if (lChannels != null && !lChannels.isEmpty()) {
            for (CountryChannels__c tChnl : lChannels) {
                if (!sSubChannels.contains(tChnl.SubChannelCode__c))
                    sSubChannels.add(tChnl.SubChannelCode__c);
            }
            
            for (String s : sSubChannels) {
                if (mAF.containsKey(s))
                    this.AreaOfFocusList.add(new SelectOption(s, mAF.get(s)));
            }
        }
        System.debug('*** Sub Channel Options:' + this.AreaOfFocusList);
        return null;
    }

    // This is a helper method that is called when the company information updates have to be pushed to IMS
    private Boolean UpdateAccountInIMS (){
        // Do the UIMS Update once the class is created.
        Account thisAccount = this.DashboardInfo.AccountInfo;
        AP_UserRegModel urModel = New AP_UserRegModel();
        urModel.accountId = thisAccount.Id;
        urModel.companyFederatedId = thisAccount.PRMUIMSID__c;
        urModel.companyName = thisAccount.PRMCompanyName__c;
        urModel.businessType = thisAccount.PRMBusinessType__c;
        urModel.areaOfFocus = thisAccount.PRMAreaOfFocus__c;
        urModel.marketServed = thisAccount.PRMMarketServed__c;
        urModel.employeeSize = thisAccount.PRMEmployeeSize__c;
        urModel.companyAddress1 = thisAccount.PRMStreet__c;
        urModel.companyCity = thisAccount.PRMCity__c;
        urModel.companyZipcode = thisAccount.PRMZipCode__c;
        urModel.companyCurrencyCode = thisAccount.PRMCurrencyIsoCode__c;
        urModel.includeCompanyInfoInPublicSearch = thisAccount.PRMIncludeCompanyInfoInSearchResults__c;
        urModel.companyCountry = thisAccount.PRMCountry__c;
        urModel.companyStateProvince = thisAccount.PRMStateProvince__c;
        
        urModel.companyPhone   = thisAccount.PRMCompanyPhone__c;
        urModel.companyWebsite = thisAccount.PRMWebsite__c;
        urModel.taxId          = thisAccount.PRMTaxId__c;
        urModel.annualSales    = thisAccount.PRMAnnualSales__c;
        urModel.companyHeaderQuarters    =thisAccount.PRMCorporateHeadquarters__c;
        urModel.companyAddress2 = thisAccount.PRMAdditionalAddress__c;

        Boolean accountUpdated = AP_PRMUtils.UpdateAccountInUIMS_Admin(urModel);
        return accountUpdated;
    }

    // This is an helper method that is used to check if any multi picklist value has changed or not
    private Boolean HasMultiPicklistValueChanged(Object oldValue, Object newValue, string splitChar) {
        Boolean valueChanged = false;
        String tOldVal = (String)oldValue;
        String tNewVal = (String)newValue;
        System.debug('>>>>tOldVal'+tOldVal+'>>>>tNewVal'+tNewVal);
        if((string.isBlank(tOldVal) && string.isBlank(tNewVal)))
            valueChanged = false;
        else if ((string.isBlank(tOldVal) && string.isNotBlank(tNewVal)) || (string.isNotBlank(tOldVal) && string.isBlank(tNewVal))) 
            valueChanged = true;
        else {
            System.debug('*** Checking picklist values');
            System.debug('*** Old value: ' + tOldVal);
            
            List<String> lValues = tNewVal.Split(splitChar);
            for (String val : lValues) {
                System.debug('*** New Picklist value:' + val);
                if (!tOldVal.ContainsIgnoreCase(val)) {
                    valueChanged = true;
                    break;
                }
            } 
        }
        System.debug('*** Picklist value has changed: ' + valueChanged);
        return valueChanged;
    }

    private PageReference GetFullUrl(String statusParam) {
        return this.GetFullUrl(statusParam, '');
    } 

    private PageReference GetFullUrl(String statusParam, string pgName) {
        PageReference tPR = new PageReference('/apex/' + (String.isBlank(pgName) ? 'VFP_ManageAccountExternalInfo' : pgName) + '?tab=' + this.ReturnToTab + '&id=' + RecordId + (String.isNotBlank(statusParam) ? '&status=' + statusParam : ''));
        tPR.setRedirect(true);
        return tPR;
    } 

    public class AssessmentResponse{
        public String QuestionText { get; set; }
        public String ResponseText { get; set; }
        public String AnswerType { get; set; }
        
        public AssessmentResponse(String questionText, String responseText, String answerType){
            this.QuestionText = questionText;
            this.ResponseText = responseText;
            this.AnswerType = answerType;
        }
    }
}