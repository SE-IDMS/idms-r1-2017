/*
    Author          : Accenture Team
    Date Created    : 18/07/2011 
    Description     : Controller extensions for VFP46_LeadsAscWithAcc.This class Access the
                      corresponding Account record via the standard Account controller method
    18/Apr/2012    Srinivas Nallapati    updated the SOQL   added "order by Status asc" 
    06/Aug/2012    Srinivas Nallapati    Updated for modifying the columns DEF-0661
    30/Oct/2012    Srinivas Nallapati    New Lead from the Leads related list on Account BR-1858
    30/Oct/2012    Srinivas Nallapati    Removed the Catrgory column, replace it with Campaign Name  BR-1911
*/
public with sharing class VFC46_LeadsAscWithAcc extends VFC_ControllerBase
{
    //Iniatialize variables
    public Account acc{get;private set;}
    public List<Lead> leads{get;set;}
    public List<DataTemplate__c> dataTemplates {get;set;}
    public List<SelectOption> columns{get;set;}
    public String gotoNewLead;
    
    //Constructor
    public VFC46_LeadsAscWithAcc(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC46_LeadsAscWithAcc******'); 
        acc= (Account)controller.getRecord();
        leads = new List<Lead>();
        dataTemplates = new List<DataTemplate__c>();
        columns = new List<SelectOption>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC46_LeadsAscWithAcc******'); 
    }
    
    /* This method is called on the load of VFP46_LeadsAscWithAcc page. This method 
       queries the Converted & unconverted leads related to the corresponding account 
       and displays the leads lists or a message if no Leads associated.
    */  
    
    public pagereference displayLeads()
    {
        System.Debug('****** Querying of Leads Begins ******'); 
        if(acc!=null){ 
            leads = [select Tech_CampaignName__c, Timeframe__c,SubStatus__c, Status, SolutionInterest__c, PromoTitle__c, Priority__c, Name, CreatedDate, ClosedDate__c, Category__c,Owner.FirstName,Owner.LastName, OwnerID, ResponseDate__c, CampaignName__c,keycode__c, ConvertedDate, MarcomType__c from Lead where Account__c =:acc.Id AND RecordTypeId !=: System.Label.CLJUL14MKT01 order by CreatedDate desc];      
        }
        
        columns.add(new SelectOption('Field1__c','Lead Name')); // First Column
        columns.add(new SelectOption('Field2__c',Schema.sObjectType.Lead.Fields.CreatedDate.label));
        columns.add(new SelectOption('Field3__c',Schema.sObjectType.Lead.Fields.Status.label));
        columns.add(new SelectOption('Field4__c',Schema.sObjectType.Lead.Fields.SubStatus__c.label));
        columns.add(new SelectOption('Field5__c',Schema.sObjectType.Lead.Fields.ClosedDate__c.label));
        columns.add(new SelectOption('Field6__c',Schema.sObjectType.Lead.Fields.Priority__c.label));
        columns.add(new SelectOption('Field7__c',Schema.sObjectType.Lead.Fields.Timeframe__c.label));
        columns.add(new SelectOption('Field8__c',Schema.sObjectType.Lead.Fields.Tech_CampaignName__c.label)); // DEF-1217
        columns.add(new SelectOption('Field9__c',Schema.sObjectType.Lead.Fields.SolutionInterest__c.label));
        columns.add(new SelectOption('Field10__c',System.Label.CLDEC12AC14)); // Last Column
        //BR-1911    Srinivas Nallapati
        for(Lead lead: leads)
        {
            DataTemplate__c dt = new DataTemplate__c();
            if(lead.ConvertedDate!=null)
                dt.field1__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/apex/VFP44_LeadDetail?id='+lead.Id+'" onClick="openTab(\''+lead.Id+'\',\''+lead.Name+'\');return false">'+lead.Name+'</a>';
            else
            dt.field1__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+lead.Id+'" onClick="openTab(\''+lead.Id+'\',\''+lead.Name+'\');return false">'+lead.Name+'</a>';
            
            dt.field2__c = String.valueOf(lead.CreatedDate);
            dt.field3__c = lead.Status;
            dt.field4__c = lead.SubStatus__c;
            dt.field5__c = String.valueOf(lead.ClosedDate__c);
            dt.field6__c = String.valueOf(lead.Priority__c);
            dt.field7__c = lead.Timeframe__c;
            dt.field8__c = lead.Tech_CampaignName__c; // DEF-1217
            dt.field9__c = lead.SolutionInterest__c;
            dt.field10__c = lead.owner.firstName + ' '+lead.owner.lastName;  // BR-1911    Srinivas Nallapati 
          
            dataTemplates.add(dt);
        }
        System.Debug('****** Querying of Leads Ends ******'); 
        return null;
    }
    
    //Added for BR-1858, New Lead page from the accociated leads section of Account detail page. This method redirects the user to new lead page
    public String getgotoNewLead()
    {
        acc = [select FirstName, LastName, Phone , AccountLocalName__c,Street__c,City__c,ZipCode__c, StateProvince__r.name, StateProvince__c ,County__c,isPersonAccount, name,Country__c, Country__r.Name from Account where id = :acc.id];
        PageReference pg = new PageReference('/00Q/e');
        pg.getParameters().put('CF00NA0000009eEic',acc.name);
        pg.getParameters().put('CF00NA0000009eEic_lkid',acc.id);
        pg.getParameters().put('CF00NA0000009eEiu',acc.Country__r.Name);
        pg.getParameters().put('CF00NA0000009eEiu_lkid',acc.Country__c);
        pg.getParameters().put('00NA0000009yS3W','1');
        
        if(!acc.isPersonAccount)   // if Not a Person Account     
        {
            pg.getParameters().put('lea3',acc.name);
        }
        else
        {
            Contact perCon = [select MobilePhone, Email, LocalFirstName__c, LocalLastName__c from Contact Where AccountId = :acc.id limit 1];
            pg.getParameters().put('name_firstlea2',acc.FirstName);
            pg.getParameters().put('name_lastlea2',acc.LastName);

            pg.getParameters().put('lea8',acc.Phone );

            pg.getParameters().put('lea9',perCon.MobilePhone);  // from contact
            pg.getParameters().put('lea11',perCon.Email);  // from contact
            pg.getParameters().put('00NA0000009x5DU',perCon.LocalFirstName__c);  // from contact
            pg.getParameters().put('00NA0000009x5DV',perCon.LocalLastName__c);   // from contact

            pg.getParameters().put('00NA0000009wkIx',acc.AccountLocalName__c);  // from Account

            pg.getParameters().put('00NA0000009eEjc',acc.Street__c);
            pg.getParameters().put('00NA0000009eEio',acc.City__c);
            pg.getParameters().put('00NA0000009eEjo',acc.ZipCode__c);

            pg.getParameters().put('CF00NA0000009eEjb',acc.StateProvince__r.name);
            pg.getParameters().put('CF00NA0000009eEjb_lkid',acc.StateProvince__c);

            pg.getParameters().put('00NA0000009eEiv',acc.County__c);
        }    
        return pg.getUrl();
    }
}