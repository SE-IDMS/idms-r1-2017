@isTest
public class Schedule_BREAlertOpen_Test{

    public static testMethod void CreateData(){
    Test.startTest();
        set<BusinessRiskEscalations__c> setBRE1 = new  set<BusinessRiskEscalations__c>();
        set<ID> setBREId = new set<ID>();
        List<User> lstUser = new List<User>();
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        User newUser = Utils_TestMethods.createStandardUser('TestUser');    
         newUser.UserRoleID = rol_ID ;
         system.debug(newUser.UserRoleID +'shiv@deep' + rol_ID);
         lstUser.add(newUser);

         User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');  
            newUser2.BypassVR__c =TRUE;
            newUser2.UserRoleID = rol_ID ;
            lstUser.add(newUser2);

        //Insert lstUser;
        
         Country__c newCountry = Utils_TestMethods.createCountry();
              newCountry.Region__c=Label.CL00320;
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
           
         if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
              Account acct1 = Utils_TestMethods.createAccount();
              acct1.name='TestAccount1';
              acct1.Account_VIPFlag__c='VIP1';
         acct1.RecordtypeID = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name != 'Supplier' limit 1].Id;
        // Database.SaveResult AccountInsertResult = Database.insert(acct1, false);
        insert acct1;
         
         Account acct = Utils_TestMethods.createAccount();
         acct.RecordtypeID = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name != 'Supplier' limit 1].Id;
         acct.UltiParentAcc__c=acct1.id;
         acct.Account_VIPFlag__c='VIP1';
         insert acct;
       /*  Database.SaveResult AccountInsertResult1 = Database.insert(acct, false);
          system.debug('RAM-1000-------------->'+AccountInsertResult);
        if(!AccountInsertResult.isSuccess())
            Database.Error err = AccountInsertResult.getErrors()[0];*/
        
        // system.runas(newUser2){
            // list<BusinessRiskEscalationEntity__c> lstBREE1 = new list<BusinessRiskEscalationEntity__c>();
             BusinessRiskEscalationEntity__c oResolutionBREE1 = new BusinessRiskEscalationEntity__c();
             oResolutionBREE1.Entity__c  =  'TESTBREE';              
             oResolutionBREE1.Location_Type__c = 'Country Front Office';
             insert oResolutionBREE1;
             EntityStakeholder__c stakeholder1 = Utils_TestMethods.createEntityStakeholder(oResolutionBREE1.Id,userInfo.getuserid(),Label.CLOCT14I2P07);
                insert stakeholder1;
            BusinessRiskEscalationEntity__c oResolutionBREE = new BusinessRiskEscalationEntity__c();
             oResolutionBREE.Entity__c = 'TESTBREE';
             oResolutionBREE.SubEntity__c =  'BRESubEnty';              
             oResolutionBREE.Location_Type__c = 'Country Front Office';
            insert oResolutionBREE;
            EntityStakeholder__c stakeholder3 = Utils_TestMethods.createEntityStakeholder(oResolutionBREE.Id,userInfo.getuserid(),Label.CLAPR15I2P09);
                insert stakeholder3; 
            BusinessRiskEscalationEntity__c oOriginatingBREE = new BusinessRiskEscalationEntity__c();
             oOriginatingBREE.SubEntity__c = 'BRESubEnty'; 
             oOriginatingBREE.Location__c = 'BRELocation';
             oOriginatingBREE.Entity__c = 'TESTBREE'; 
             oOriginatingBREE.Location_Type__c = 'Country Front Office';
             insert oOriginatingBREE;            

             EntityStakeholder__c stakeholder4 = Utils_TestMethods.createEntityStakeholder(oOriginatingBREE.Id,userInfo.getuserid(),Label.CL00317);
          insert stakeholder4; 
          EntityStakeholder__c stakeholder5 = Utils_TestMethods.createEntityStakeholder(oOriginatingBREE.Id,userInfo.getuserid(),Label.CL10035);
          insert stakeholder5;
             List<BusinessRiskEscalations__c> lsBRE = new List<BusinessRiskEscalations__c>();
             
             BusinessRiskEscalations__c BRE1 =  Utils_TestMethods.createBusinessRiskEscalation(acct.ID,newCountry.ID,newUser2.ID);
             
             BRE1.ResolutionOrganisation__c = oResolutionBREE.ID; 
             BRE1.OriginatingOrganisation__c =  oOriginatingBREE.ID; 
             BRE1.ResolutionLeader__c =userInfo.getuserid(); //newUser.ID;
             BRE1.BusinessRiskSponsor__c =userInfo.getuserid(); //newUser.ID;
             BRE1.CountryQualityManager__c=userInfo.getuserid(); 
             BRE1.TECH_ReminderOpenDate__c=system.now()-8;           
             lsBRE.add(BRE1);
            
             BusinessRiskEscalations__c BRE4 =  Utils_TestMethods.createBusinessRiskEscalation(acct.ID,newCountry.ID,newUser2.ID);
             BRE4.ResolutionOrganisation__c = oResolutionBREE.ID; 
             BRE4.OriginatingOrganisation__c =  oOriginatingBREE.ID; 
             BRE4.ResolutionLeader__c = userInfo.getuserid();
             BRE4.BusinessRiskSponsor__c = userInfo.getuserid();
             BRE4.CountryQualityManager__c=userInfo.getuserid(); 
              BRE4.TECH_ReminderOpenDate__c=system.now()-9; 
             lsBRE.add(BRE4);
             insert lsBRE;

            system.debug('RAM--------------->'+lsBRE);

             List<BusinessRiskEscalations__c> lsBREnew = [select id,GSAAffected__c ,AffectedCustomer__c,CountryQualityManager__c,TECH_OriginatingEntity__c,TECH_OriginatingSubEntity__c,TECH_OriginatingLocation__c,  BusinessRiskSponsor__c, ResolutionLeader__c from BusinessRiskEscalations__c];
             system.debug('#### lsBREnew'+lsBREnew);
             
             for(BusinessRiskEscalations__c oBRE11 :lsBREnew){
                 system.debug('oBRE11.ID'+oBRE11.ID);
                 setBREId.add(oBRE11.ID);
                 
             }
              system.debug('setBREId'+setBREId);
       // }    
        
          Schedule_BusinessRiskEscalationAlertOpen batch_BREA = new Schedule_BusinessRiskEscalationAlertOpen(); 
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_BREA,1000);
        /*
        ProvideChatterDetails PCD = new ProvideChatterDetails();
        system.debug('Shiv deep setBREId'+ setBREId);
        PCD.createAlertForBRE_Stackholders(setBREId);
        PCD.returnObjectSet(setBREId);
        */
    Test.stopTest();
    }
    
    static testMethod void myUnitTest_schedule() {
            Test.startTest();
                    String CRON_EXP = '0 0 0 1 1 ? 2025';  
                    String jobId = System.schedule('Schduel Schedule_BusinessRiskEscalationAlertOpen', CRON_EXP, new Schedule_BusinessRiskEscalationAlertOpen() );

            Test.stopTest();
    }
}