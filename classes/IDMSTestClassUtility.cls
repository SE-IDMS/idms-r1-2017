/*
* Prepare test utility for IdmsUserRest
* */
@isTest
public Class IDMSTestClassUtility{
    
    static
    {
        //ProfileMap Record
        
        List<IDMS_Profile_Mapping__c  > LstProfileMap = new List<IDMS_Profile_Mapping__c  >();
        LstProfileMap.add(new IDMS_Profile_Mapping__c  (name = '@Home',Profile_Name__c = 'IDMS_@home'));
        LstProfileMap.add(new IDMS_Profile_Mapping__c  (name = 'Home',Profile_Name__c = 'IDMS_@home'));
        LstProfileMap.add(new IDMS_Profile_Mapping__c  (name = '@Work',Profile_Name__c = 'SE - IDMS - Standard B2B User'));
        LstProfileMap.add(new IDMS_Profile_Mapping__c  (name = 'Work',Profile_Name__c = 'SE - IDMS - Standard B2B User'));
        
        insert LstProfileMap ;
        
        
        
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        
        //IDMS_field_Mapping
        
        List<IDMS_field_Mapping__c> LstFiedMap = new List<IDMS_field_Mapping__c>();
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'AdditionalAddress__pc',userfield__c='IDMS_AdditionalAddress__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'AdditionalAddress__c',userfield__c='IDMS_AdditionalAddress__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'City__c',userfield__c='City'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'City__pc',userfield__c='City'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'CorrespLang__pc',userfield__c='IDMS_PreferredLanguage__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Country__pc',userfield__c='Country'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Country__c',userfield__c='Country'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'County__pc',userfield__c='IDMS_County__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'County__c',userfield__c='IDMS_County__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'CurrencyIsoCode',userfield__c='DefaultCurrencyIsoCode'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Fax',userfield__c='Fax'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'IDMS_Account_UserExternalId__c',userfield__c='username'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'IDMS_Contact_UserExternalId__pc',userfield__c='username'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalCity__c',userfield__c='City'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalCounty__c',userfield__c='IDMS_County__c'));                
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalAdditionalAddress__c',userfield__c='IDMS_AdditionalAddress__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalFirstName__pc',userfield__c='Firstname'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'LocalLastName__pc',userfield__c='Lastname'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'MarcomPrefEmail__pc',userfield__c='IDMS_Email_opt_in__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'PersonEmail',userfield__c='Email'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'WorkPhone__pc',userfield__c='Phone'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'PersonMobilePhone',userfield__c='MobilePhone'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'POBox__c',userfield__c='IDMS_POBox__c'));        
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'POBox__pc',userfield__c='IDMS_POBox__c'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Street__c',userfield__c='Street'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'StreetLocalLang__c',userfield__c='Street'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'Street__pc',userfield__c='Street'));
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'ZipCode__c',userfield__c='PostalCode'));      
        LstFiedMap.add(new IDMS_field_Mapping__c(name = 'ZipCode__pc',userfield__c='PostalCode'));      
        
        if(LstFiedMap != null && LstFiedMap.size() > 0)
            insert LstFiedMap;
        
        
        //create Country__C record
        Country__C cntry = new Country__C();
        cntry.CountryCode__c = 'IN';
        cntry.Region__c = 'APAC';
        cntry.Name = 'India';
        insert cntry ;   
        // create status code mapping
        List<IDMSStatusCodeMapping__c > LstStatusCodeMap = new List<IDMSStatusCodeMapping__c >();
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Account and User with matching email',httpCode__c=200)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Error in creating user',httpCode__c=500));   
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User Id not same in searched user',httpCode__c=500)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User not found based on user Id',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'New username already present',httpCode__c=500)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Field(s) not in correct format',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User created successfully',httpCode__c=200)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User Exists in UIMS',httpCode__c=500)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Fed Id should contain',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Update source not found',httpCode__c=500)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Person account not present',httpCode__c=500)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Fields with incorrect length',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'user profile update succesfully',httpCode__c=200)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User Id should start with 005',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Missing mandatory Federation Id or Id',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User not found based on Fed Id',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'User Already exists',httpCode__c=409)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Required Fields Missing',httpCode__c=400)); 
        LstStatusCodeMap.add(new IDMSStatusCodeMapping__c (name = 'Both Email and Phone can',httpCode__c=400)); 
        insert LstStatusCodeMap;
        
        
        
        
    }
    //Test 1: getting record type object type
    public static RecordType getRecordType(String SObjectApiName , String recordTypeDevName)
    {
        RecordType recType = [Select  DeveloperName,Name, Id From RecordType where SobjectType = :SObjectApiName and DeveloperName = :recordTypeDevName];
        return recType;
    }
    //Test 2: getting prfile id from profile name
    public static Id getProfileId(String ProfileName)
    {
        Profile pf = [Select  Id From Profile where name = :ProfileName];            
        return pf.id;
    }
    //Test 3: creating a user with all the details
    public static user createUser(String Context,string email,string firstname,string lastname,string preferredLang,String RegistrationSource){
        
        User u = new user();
        u.firstname = firstname;
        u.lastname = lastname;
        u.IDMS_User_Context__c = Context;
        u.Country= 'IN';
        u.state = '01';
        u.email = email;
        u.IDMS_PreferredLanguage__c= preferredLang;
        u.phone ='444444444444';
        u.MobilePhone = '55555555555';
        u.fax='33333';
        u.IDMS_county__c = 'test';
        u.DefaultCurrencyIsoCode = 'USD';
        u.IDMS_Email_opt_in__c = 'Y';
        u.IDMS_POBox__c = '1111';
        u.Street = 'teststreet';
        u.PostalCode = '110088';
        u.IDMS_Registration_Source__c = RegistrationSource;
        return u;
    }
    //Test 4: creating a user for identity type phone with all the details
    public static user createUserPhone(String Context,string mobilePhone,string firstname,string lastname,string preferredLang,String RegistrationSource){
        
        User u = new user();
        u.firstname = firstname;
        u.lastname = lastname;
        u.IDMS_User_Context__c = Context;
        u.Country= 'IN';
        u.state = '01';
        //u.email = email;
        u.IDMS_PreferredLanguage__c= preferredLang;
        u.phone ='444444444444';
        u.MobilePhone = mobilePhone;
        u.fax='33333';
        u.IDMS_county__c = 'test';
        u.DefaultCurrencyIsoCode = 'USD';
        u.IDMS_Email_opt_in__c = 'Y';
        u.IDMS_POBox__c = '1111';
        u.Street = 'teststreet';
        u.PostalCode = '110088';
        u.IDMS_Registration_Source__c = RegistrationSource;
        u.CompanyName='testcompany';
        u.Company_City__c='testcity';
        u.Company_Country__c='India';
        u.IDMSClassLevel1__c='OM';
        return u;
    }
    
    //Test 5: it returns the complete user object
    public static user returnUserObj(){
        user UserObj=new user();
        
        UserObj.MobilePhone='+366810236979';
        UserObj.Phone='124-456-7890';
        UserObj.FirstName='test12';
        UserObj.LastName='test21';
        UserObj.IDMS_Email_opt_in__c='Y';
        UserObj.IDMS_User_Context__c='@Work';
        UserObj.Country='IN';
        UserObj.IDMS_PreferredLanguage__c='fr';
        UserObj.DefaultCurrencyIsoCode='XOF';
        UserObj.Street='Delhi';
        UserObj.City='Delhi';
        UserObj.PostalCode='1100092';
        UserObj.State='';
        UserObj.IDMS_County__c='test';
        UserObj.IDMS_POBox__c='110092';
        UserObj.IDMS_AdditionalAddress__c='testing';
        UserObj.CompanyName='';
        UserObj.Company_Address1__c='';
        UserObj.Company_City__c='';
        UserObj.Company_Postal_Code__c='';
        UserObj.Company_State__c='';
        UserObj.IDMSCompanyPoBox__c='122001';
        UserObj.Company_Country__c='IN';
        UserObj.Company_Address2__c='Tower-B,Unitech Infospace';
        UserObj.IDMSClassLevel1__c='';
        UserObj.IDMSClassLevel2__c='';
        UserObj.IDMSWorkPhone__c='';
        UserObj.Job_Title__c='';
        UserObj.Job_Function__c='';
        UserObj.IDMSJobDescription__c='';
        UserObj.IDMSCompanyMarketServed__c='';
        UserObj.IDMSCompanyNbrEmployees__c='';
        UserObj.IDMSCompanyHeadquarters__c=true;
        UserObj.IDMSTaxIdentificationNumber__c='';
        UserObj.IDMSMiddleName__c='';
        UserObj.Company_Website__c='www.accenture.com';
        UserObj.IDMSSalutation__c='Z003';
        UserObj.Department='Security';
        UserObj.IDMSSuffix__c='';
        UserObj.Fax='123456789';
        UserObj.IDMSCompanyFederationIdentifier__c='';
        UserObj.IDMSDelegatedIdp__c='';
        UserObj.IDMSIdentityType__c='';
        UserObj.IDMS_Registration_Source__c='Heroku';
        return UserObj;
        
    }
    
    //Test 6: it creates ifwuser
    public static user createIFWUser(String Context,string email,string firstname,string lastname,string preferredLang,String RegistrationSource){
        
        User u = new user();
        u.firstname = firstname;
        u.lastname = lastname;
        u.IDMS_User_Context__c = Context;
        u.Country= 'IN';
        u.email = email;
        u.IDMS_PreferredLanguage__c= preferredLang;
        u.phone ='444444444444';
        u.MobilePhone = '5555555555555';
        u.fax='33333';
        u.IDMS_county__c = 'test';
        u.DefaultCurrencyIsoCode = 'USD';
        u.IDMS_Email_opt_in__c = 'Y';
        u.IDMS_POBox__c = '1111';
        u.Street = 'teststreet';
        u.PostalCode = '110088';
        u.IDMS_Registration_Source__c = RegistrationSource;
        u.username = 'IFWUSer@accenture.com.devmj';
        u.alias = 'IFWU';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = Label.CLJUN16IDMS09;
        u.EmailEncodingKey = 'ISO-8859-1';
        u.Profileid = IDMSTestClassUtility.getProfileId('SE - Interface - IFW');
        u.LanguageLocaleKey = Label.CLJUN16IDMS09;
        u.userroleid = [SELECT Id FROM UserRole WHERE Name = 'CEO' limit 1].id;
        insert u;
        return u;
    }
    
    
    //Test 7: it creates Account
    public static void CreateAccount(String email, string firstname, string lastname,string SupplierAccountname, Boolean isPerson){
        Account Acc = new Account();
        if(isPerson){
            RecordType rt = getRecordType('Account','PersonAccount');
            Acc.recordtypeid = rt.id;
            Acc.firstname = firstname;
            Acc.lastname = lastname;
            Acc.personemail = email;
            
        }
        else{
            RecordType rt = getRecordType('Account','Supplier');
            Acc.Name = SupplierAccountname;
            Acc.recordtypeid = rt.id;
        }
        
        insert Acc;
    }
    //Test 8: it returns the  Account
    public static Account returnAccount(String email, string firstname, string lastname,string SupplierAccountname, Boolean isPerson){
        Account Acc = new Account();
        if(isPerson){
            RecordType rt = getRecordType('Account','PersonAccount');
            Acc.recordtypeid = rt.id;
            Acc.firstname = firstname;
            Acc.lastname = lastname;
            Acc.personemail = email;
            
        }
        else{
            RecordType rt = getRecordType('Account','Supplier');
            Acc.Name = SupplierAccountname;
            Acc.recordtypeid = rt.id;
        }
        return Acc;
    }
    
    
    
}