/*
    Description: Batch Class updates data based on the parameters passed
*/
global class Batch_ReferentialDataUpdates implements Database.Batchable<sObject>
{
    //Variables Declaration
    String query;
    //String whereCondition;
    String obj;
    String updateField;
    String updateValue;
    //Constructor
    global Batch_ReferentialDataUpdates(String obj,String query, String updateField, String updateValue)
    {
        this.obj=obj;
        //this.whereCondition=whereCondition;
        this.updateField=updateField;
        this.updateValue=updateValue;
        //this.query = 'SELECT Id,'+updateField+' from Account Where '+whereCondition;
        this.query=query;
    }
    //START method
    global Database.querylocator start(Database.BatchableContext BC)
    {
        System.debug('**** QUERY >>>>>'+query);
        return Database.getQueryLocator(query);
    }
    //EXECUTE method
    global void execute(Database.BatchableContext BC, List<sObject> scope){
         System.debug('###### excute method entered#######');
         System.debug(' updateValue ::: '+updateValue);
         // To update Boolean Fields
         if( updateValue.toLowerCase() == 'true' || updateValue.toLowerCase() == 'false' ){
             Boolean updateValueBool = Boolean.ValueOf(updateValue);
             for(sobject s : scope){
                 s.put(updateField,updateValueBool); 
             }
         }
         // To update String Fields
         else{
             for(sobject s : scope){
                 s.put(updateField,updateValue); 
             }
         
         }
         
         update scope;
         System.debug('###### excute method ended#######');
    }
    //FINISH method
    global void finish(Database.BatchableContext BC){
      System.debug('###### finish method called #######');
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems,ExtendedStatus from AsyncApexJob where Id = :BC.getJobId()];
      //send email with  total jobs and errors 
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(System.Label.CLDEC14SLS01.split(','));
      mail.setSubject('Batch to update '+updateField+' of '+obj+' with '+updateValue);
      mail.setPlainTextBody('Status>>>>>> '+a.Status+ '\r\n\r\n'+'JobItemsProcessed>>>>>> '+a.JobItemsProcessed+ '\r\n\r\n'+'ToTALJOBITEMS>>>>>> ' + a.TotalJobItems + '\r\n\r\n'+'NUMBEROFERRORS>>>>>> ' + a.NumberOfErrors+'\r\n\r\n'+'QUERY>>>>>> '+query+'\r\n\r\n'+'UPDATE CONDITION>>>>>> '+updateField+'='+updateValue+'\r\n\r\n'+'Short Description Of The First Error>>>>>>'+a.ExtendedStatus);  
      if(!Test.isRunningTest())                          
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

     }
}