/*
    Author          : Yannick Tisserand ~ yannick.tisserand@accenture.com 
    Date Created    : 02/08/2011
    Description     : Send Work Order localized email notifications
*/
/***************************************************************************************************************
Date Modified       Modified By                         Comments
----------------------------------------------------------------------------------------------------------------
27-03-2013          Jyotiranjan Singhlal               Added Logic for EMail Opt out for Contacts
****************************************************************************************************************/
public class AP28_WorkOrderLocalizedNotification
{
   /* Type of notification:
    * 1 - Site Authorization
    * 2 - Customer confirmation 
    */ 
    public static void sendEmailNotification(List<ID> workOrderIds, integer typeOfNotif)
    {   
        String language;
        String email;
        String email2;
        //String emailOptIn;
        ID targetObjectId;
        String accOwnerEmail ='';
        
        List<SVMXC__Service_Order__c> workOrders = [select id, Name, SVMXC__Contact__r.Id, SVMXC__Contact__r.MarcomPrefEmail__c,
        SVMXC__Contact__r.Email, SVMXC__Contact__r.CorrespLang__c, SVMXC__Group_Member__r.ThirdPartyContact__r.id, 
        SVMXC__Group_Member__r.ThirdPartyContact__r.Email, SVMXC__Group_Member__r.ThirdPartyContact__r.CorrespLang__c,
        SVMXC__Group_Member__r.ThirdPartyContact__r.MarcomPrefEmail__c,SVMXC__Company__r.Id,SVMXC__Company__r.Owner.Email from SVMXC__Service_Order__c where id in :workOrderIds];   
            
        for(SVMXC__Service_Order__c workOrder:workOrders )
        {
            ServiceNotificationsSetting__c customSetting = new ServiceNotificationsSetting__c();
            //ServiceNotificationsSetting__c customSetting2 = new ServiceNotificationsSetting__c();
            
            if(typeOfNotif == 1)
            {
              //emailOptIn = workOrder.SVMXC__Group_Member__r.ThirdPartyContact__r.MarcomPrefEmail__c;
              //if(emailOptIn == 'Y')
              //{
                  language = workOrder.SVMXC__Group_Member__r.ThirdPartyContact__r.CorrespLang__c;
                  email = workOrder.SVMXC__Group_Member__r.ThirdPartyContact__r.Email;
                  targetObjectId = workOrder.SVMXC__Group_Member__r.ThirdPartyContact__r.id;
                  accOwnerEmail = workOrder.SVMXC__Company__r.Owner.Email;
                  
             // }
            }
            else if(typeOfNotif == 2)
            {
              //emailOptIn = workOrder.SVMXC__Contact__r.MarcomPrefEmail__c;
            //  if(emailOptIn == 'Y')
             // {
                  language = workOrder.SVMXC__Contact__r.CorrespLang__c; 
                  email = workOrder.SVMXC__Contact__r.Email;
                  targetObjectId = workOrder.SVMXC__Contact__r.Id;
                  accOwnerEmail = workOrder.SVMXC__Company__r.Owner.Email;
                  
             // }
            }
            /*if(typeOfNotif == 3)
            {
                //language = workOrder.SVMXC__Contact__r.CorrespLang__c; 
                //User u1:[SELECT Id ,Email FROM User WHERE Id in :woOwnerid
                email2 = workOrder.SVMXC__Company__r.Owner.Email;
                targetObjectId = workOrder.SVMXC__Company__r.Id;
            }*/
             
            if(email != null)
            {  
              // Request Custom settings base on correspondance language 
              List<ServiceNotificationsSetting__c> contactSettings = getCustomSettings(language, typeOfNotif);   
                        
              if(contactSettings.size() == 1)
              {    
                customSetting = contactSettings[0];
              }
              else
              {
                // if there is not template in the contact's language, we send the notif in English by default
                List<ServiceNotificationsSetting__c> defaultSettings = getCustomSettings('EN', typeOfNotif); 
                customSetting = defaultSettings[0];
              }
    
              if(customSetting != null)
              { 
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();   
                
                System.debug('##### - SENDER '+customSetting.Sender__c);
                
                if(customSetting.Sender__c != null && customSetting.Sender__c != '')
                {
                  ID orgWideEmailAddressId = [SELECT Id FROM OrgWideEmailAddress WHERE address = :customSetting.Sender__c LIMIT 1].id;
                  
                  System.debug('##### - orgWideEmailAddressId '+orgWideEmailAddressId);
                 
                  if(orgWideEmailAddressId != null)
                  {
                    mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
                    mail.setReplyTo(customSetting.Sender__c);
                  }   
                }
                 
                //mail.setSenderDisplayName(System.Label.CL00537);
                mail.setTemplateID([SELECT Id FROM EmailTemplate WHERE DeveloperName =:customSetting.EmailTemplate__c LIMIT 1].Id); 
                mail.setTargetObjectId(targetObjectId);
                System.debug('\n CLog :'+accOwnerEmail);
                //mail.setCcAddresses(new String[]{accOwnerEmail});
                //mail.setToAddresses(new String[] { email });
                
                mail.setWhatID(workOrder.ID);
                   
                try
                {
                  System.debug('##### - Sending Services notification to '+email);
                  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
                catch(Exception e)
                {
                  System.debug('----- Send Services localized email has failed');
                }
               }
            }
            else
            {
                 System.debug('##### - No email for sending notification (type '+typeOfNotif+') on Work Order '+workOrder.Name);
            }
            //Added for Account Owner Notification
            
            /*if(email2 != null)
            {
                List<ServiceNotificationsSetting__c> accountowneremailSettings = getCustomSettings('EN', 2);
                customSetting2 = accountowneremailSettings[0];
                
                if(customSetting2 != null)
                { 
                    Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();   
                    
                    System.debug('##### - SENDER '+customSetting2.Sender__c);
                    
                    if(customSetting2.Sender__c != null && customSetting2.Sender__c != '')
                    {
                        ID orgWideEmailAddressId = [SELECT Id FROM OrgWideEmailAddress WHERE address = :customSetting2.Sender__c LIMIT 1].id;
                        
                        System.debug('##### - orgWideEmailAddressId '+orgWideEmailAddressId);
                        
                        if(orgWideEmailAddressId != null)
                        {
                            mail2.setOrgWideEmailAddressId(orgWideEmailAddressId);
                            mail2.setReplyTo(customSetting2.Sender__c);
                        }   
                    }
                
                    //mail.setSenderDisplayName(System.Label.CL00537);
                    mail2.setTemplateID([SELECT Id FROM EmailTemplate WHERE DeveloperName =:customSetting2.EmailTemplate__c LIMIT 1].Id); 
                    mail2.setTargetObjectId(targetObjectId);
                    mail2.setWhatID(workOrder.ID);
                    
                    try
                    {
                        System.debug('##### - Sending Services notification to '+email2);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail2 });
                    }
                    catch(Exception e)
                    {
                        System.debug('----- Send Services localized email has failed');
                    }
                }
            }
            else
            {
                System.debug('##### - No email for sending notification (type '+typeOfNotif+') on Work Order '+workOrder.Name);
            } */
            //End for Account Owner Notification  
        }
                  
    }
    
    public static List<ServiceNotificationsSetting__c> getCustomSettings(String language, Integer typeOfNotif)
    {   
       List<ServiceNotificationsSetting__c> customSettings = [SELECT Id, EmailTemplate__c, Sender__c FROM ServiceNotificationsSetting__c WHERE LanguageCode__c = :language and type__c = :typeOfNotif];  
       
       return customSettings;
    }
    
}