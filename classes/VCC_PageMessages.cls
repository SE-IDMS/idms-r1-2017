public with sharing class VCC_PageMessages {
    public String CustomPageMessages_ClosableErrorsRenderPlaceHolder {
        get { 
            if(CustomPageMessages_ClosableErrorsRenderPlaceHolder == null) CustomPageMessages_ClosableErrorsRenderPlaceHolder = ''; 
            return CustomPageMessages_ClosableErrorsRenderPlaceHolder; 
        }
        private set;
    } 
 
    public List<AP_PageMessages.PageMessage> getPageMessages() {
        return AP_PageMessages.getMessages();
    }
}