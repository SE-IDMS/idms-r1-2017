/*
    Author          : Accenture Team 
    Date Created    : 06/08/2011
    Description     : Test class for VCC09
*/
@isTest
private class VCC09_HistoryRelatedLists_TEST
{
    static testMethod void testHistoryRel() 
    {
            Lead lead;
            Opportunity opp1;            
             List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
             if(profiles.size()>0)
            {
                  User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
                  user.byPassVR__C = true;
                  user.byPassWF__C = false;
                  system.runas(user)
                 {        
                          
                    Account acc = Utils_TestMethods.createAccount();
                    Database.DMLOptions objDMLOptions = new Database.DMLOptions();
                    objDMLOptions.DuplicateRuleHeader.AllowSave = true;
                    Database.SaveResult accountSaveResult = Database.insert(acc,objDMLOptions); 
                    //insert acc;
                    
                    Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
                    insert opp;
                    
                    opp1 = Utils_TestMethods.createOpenOpportunity(acc.Id);
                    insert opp1;
                    
                    Country__c country= Utils_TestMethods.createCountry();
                    insert country;            
                    
                    MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                    insert markCallBack;
                    
                    lead = Utils_TestMethods.createLead(opp.Id, country.Id,100);
                    insert lead;
        
                    lead = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead.Id limit 1];            
                    system.assertEquals(100,lead.CallBackLimit__c);
                    
                    List<Task> tasks= new List<Task>();
                    for(Integer i=0;i<10;i++)
                    {
                        Task task = Utils_TestMethods.createOpenActivity(null,lead.Id,  'Not Started', Date.Today().addDays(2));
                        tasks.add(task);
                    }
                    Database.SaveResult[] tasksInsert= Database.insert(tasks, true);
                                  
                    lead.Status = Label.CL00447;
                    lead.SubStatus__c = Label.CL00448;
                    update lead;
                    
                    lead.Opportunity__c = null;
                    update lead;
                    
                    lead.Opportunity__c = opp1.Id;
                    update lead;
                    test.startTest();
                    lead = [select Status, subStatus__c,CallBackLimit__c,Country__c, Priority__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id =: lead.Id limit 1];
                    system.assertEquals('3 - Closed - Existing Opportunity',lead.Status);
                    system.assertEquals('Existing Opportunity',lead.SubStatus__c);            
                                
                    lead.status = 'New';
                    update lead;
                    lead = [select Status, subStatus__c,CallBackLimit__c,Country__c, Priority__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id =: lead.Id limit 1];
                    system.assertEquals('New',lead.Status);
                    
                    VCC09_HistoryRelatedLists histRelLists1 = new VCC09_HistoryRelatedLists();
                    VCC09_HistoryRelatedLists.objectHistoryLine[] obj = new VCC09_HistoryRelatedLists.objectHistoryLine[]{};
                    VCC09_HistoryRelatedLists.objectHistoryLine wrapperclass1 = new VCC09_HistoryRelatedLists.objectHistoryLine();        
                    
                    //inserts QuoteLink
                    OPP_QuoteLink__c quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
                    insert quoteLink;        
                    quoteLink = [select id from OPP_QuoteLink__c where Id=:quoteLink.Id];
                    
                    //inserts Budget
                    RIT_Budget__c budget= RIT_Utils_TestMethods.createbudget(quoteLink.Id);
                    insert budget;        
                    
                    VCC09_HistoryRelatedLists histRelLists2 = new VCC09_HistoryRelatedLists();                
                    histRelLists2.sObjType = budget;
                    histRelLists2.recordLimit = 100;
                    histRelLists2.getObjectHistory();
                    test.stopTest();
                }
               
          }  
    }
/*********************/


  static testMethod void testHistoryRel_2() 
    {
            Lead lead;
            Opportunity opp1;            
             List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
             if(profiles.size()>0)
            {
                  User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
                  user.byPassVR__C = true;
                  user.byPassWF__C = false;
                   Account acc = Utils_TestMethods.createAccount();
                   Database.DMLOptions objDMLOptions = new Database.DMLOptions();
                   objDMLOptions.DuplicateRuleHeader.AllowSave = true;
                   Database.SaveResult accountSaveResult = Database.insert(acc,objDMLOptions); 
                   //insert acc;
                    
                    Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
                    insert opp;
                    
                    opp1 = Utils_TestMethods.createOpenOpportunity(acc.Id);
                    insert opp1;
                    
                    Country__c country= Utils_TestMethods.createCountry();
                    insert country;            
                    
                    MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                    insert markCallBack;
                    
                    lead = Utils_TestMethods.createLead(opp.Id, country.Id,100);
                    insert lead;
        
                    lead = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead.Id limit 1];            
                    system.assertEquals(100,lead.CallBackLimit__c);
                    
                    List<Task> tasks= new List<Task>();
                    for(Integer i=0;i<10;i++)
                    {
                        Task task = Utils_TestMethods.createOpenActivity(null,lead.Id,  'Not Started', Date.Today().addDays(2));
                        tasks.add(task);
                    }
                    Database.SaveResult[] tasksInsert= Database.insert(tasks, true);
                                  
                    lead.Status = Label.CL00447;
                    lead.SubStatus__c = Label.CL00448;
                    update lead;
                    
                    lead.Opportunity__c = null;
                    update lead;
                
                LeadHistory leadHist1 = Utils_TestMethods.createLeadHistory(lead.Id,'Created');
                insert leadHist1;
                LeadHistory leadHist = Utils_TestMethods.createLeadHistory(lead.Id,'Opportunity__c');
                insert leadHist;            
                LeadHistory leadHist2 = Utils_TestMethods.createLeadHistory(lead.Id,'status');
                insert leadHist2;            

                //system.runas(user)
                //{   
                    test.startTest(); 
                    VCC09_HistoryRelatedLists histRelLists = new VCC09_HistoryRelatedLists();                
                    histRelLists.sObjType = lead;
                    histRelLists.recordLimit = 100;
                    histRelLists.getObjectHistory();
                    
                    VCC09_HistoryRelatedLists histRelLists1 = new VCC09_HistoryRelatedLists();                
                    histRelLists1.sObjType = lead;
                    histRelLists1.getObjectHistory();
                    
                    
                    VCC09_HistoryRelatedLists.returnFieldLabel('Name');
                    test.stopTest(); 
                //}
          }  
    }

}