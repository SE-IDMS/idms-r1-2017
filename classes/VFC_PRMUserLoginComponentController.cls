global class VFC_PRMUserLoginComponentController {

    global transient Boolean AuthenticationFailed { get; set; }
    global transient string ReferredFrom { get; set; }
    global transient AP_IMSSSOInitParams.SSOInitParams InitParams { get; set; }
    global transient String ErrorMsgCode { get; set; }
    global transient string tokenValue {get;set;}
    global transient string UserLanguage {get;set;}
    global transient String Country { get; set; }
    global String RefferedValue { get; set; }
    global transient String supportEmail {get;set;}
    global transient Map<String,String> URLParams  = ApexPages.currentPage().getParameters();
    global String referredFromUrl{get{ return ApexPages.currentPage().getHeaders().get('Referer');} set;}
    global transient String currentURL = Apexpages.currentPage().getUrl();
    global String gotoParam {get{ return ApexPages.currentPage().getParameters().get('goto');}}
    global String sunQueryStringParam {get {return ApexPages.currentPage().getParameters().get('SunQueryParamsString');}}
    global transient String IDToken = ApexPages.currentPage().getParameters().get('IDToken1');
    global transient String languageURL = ApexPages.currentPage().getParameters().get('language');
    global transient String countryURL = ApexPages.currentPage().getParameters().get('country');
    
    global transient string loginParameters = '';
    global transient String countryValue = '';
    global transient String tUserLanguage ='';
    global transient String langValue = '';
    global transient String socialLoginType{get; set;}
    global transient String socialMediaType {get;set;}

  
    global class ResetPasswordResult {
        global Boolean Success { get; set; }
        global String ErrorMsg { get; set; }
    }
    
    global VFC_PRMUserLoginComponentController() {
        AP_PRMUtils.LanguageCountryCount LanguageCntryCount = new AP_PRMUtils.LanguageCountryCount();
        RefferedValue = ApexPages.currentPage().getParameters().get('val');
        System.debug('RefferedValue:' + RefferedValue);
        Boolean isLangSet = false;
        String pCode='';
        String languageURL1 ='';
        String countryURL1 ='';
        List<VFC_PRMUserRegistrationController.PicklistEntry> lstLanguageEntry = new List<VFC_PRMUserRegistrationController.PicklistEntry>();
        map<String, List<VFC_PRMUserRegistrationController.PicklistEntry>> mapCountryLng = new map<String, List<VFC_PRMUserRegistrationController.PicklistEntry>>();
        List<PRMCountry__c> lstPRMCountry = new List<PRMCountry__c>();
        
        System.debug('*** Login Post:' + gotoParam);
        System.debug('*** Referrred from:' + referredFromUrl);
        
       languageURL1 = ApexPages.currentPage().getParameters().get('language');
       countryURL1 = ApexPages.currentPage().getParameters().get('country');
        Cookie tlangCookie = ApexPages.currentPage().getCookies().get('language');
        Cookie CountryCookie = ApexPages.currentPage().getCookies().get('country');

        String langValue='';
        
        system.debug('URLS:' + languageURL1 +countryURL1 );
         
        if(CountryCookie != null){
            CountryValue = CountryCookie.getValue();
            pCode = '%'+ CountryValue +'%';
            system.debug('getCountry:' + CountryCookie.getValue());
        }
        
        if(tlangCookie != null){
            langValue = tlangCookie.getValue();
            system.debug('getLang:' + tlangCookie.getValue());
        }
        
        if (!String.isEmpty(URLParams.get('goto')) && !String.isEmpty(URLParams.get('SunQueryParamsString'))) {
            Cookie gotoCookie= new Cookie('gotoCookie', URLParams.get('goto'),null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{gotoCookie});
            Cookie SunQueryParamsStringCookie= new Cookie('SunQueryParamsStringCookie', URLParams.get('SunQueryParamsString'),null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{SunQueryParamsStringCookie});
            InitParams = new AP_IMSSSOInitParams.SSOInitParams();
            InitParams.GotoParam = URLParams.get('goto');
            InitParams.QueryParam = URLParams.get('SunQueryParamsString');
        }
        else{
            Cookie gotoCookie= ApexPages.currentPage().getCookies().get('gotoCookie');
            Cookie SunQueryParamsStringCookie= ApexPages.currentPage().getCookies().get('SunQueryParamsStringCookie');
            if(gotoCookie != null && SunQueryParamsStringCookie != null){
                InitParams = new AP_IMSSSOInitParams.SSOInitParams();
                InitParams.GotoParam = gotoCookie.getValue();
                InitParams.QueryParam = SunQueryParamsStringCookie.getValue();
            }
        }
        Boolean isAuthFailed = false;
        String tmpIDToken;

        if(!String.isBlank(languageURL)){
            tUserLanguage = languageURL;
                    
        }
        else{
            if(langValue != null )
                tUserLanguage = langValue ;
        }           
        
        if(!String.isBlank(countryURL)){
            if(String.isBlank(CountryValue)){
                CountryValue = countryURL;
                pCode = '%'+ CountryValue +'%';
            }
            Cookie tCountryCookie = new Cookie('country', countryURL, null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{tCountryCookie});   
        }
        
        for(String s: URLParams.keyset()){
            if(s == 'language'){
                 if(String.isNotBlank(URLParams.get(s))){
                    tUserLanguage = URLParams.get(s) == 'id' ? 'in' : URLParams.get(s);
                    loginParameters += '&Language='+tUserLanguage ;
                }
                else{
                    tUserLanguage = tlangCookie != null ? (tlangCookie.getValue() == 'id' ? 'in' : tlangCookie.getValue() ): 'en_US';
                    loginParameters += '&language='+tUserLanguage;
                }
            }
            else if(s == 'country'){
                if(String.isNotBlank(URLParams.get(s))){
                    CountryValue = URLParams.get(s);
                    loginParameters += '&Country='+CountryValue;
                }
                else{
                    CountryValue = CountryCookie != null ? CountryCookie.getValue(): null;
                    loginParameters += '&country='+CountryValue;
                }
            }
            else{
                loginParameters += '&'+s+'='+URLParams.get(s);
                
            }
        }        
        
        if(String.isNotBlank(CountryValue)){
            LanguageCntryCount  = AP_PRMUtils.updateLanguageAndCountry(tUserLanguage,CountryValue,langValue);
            system.debug('languageAndCountry:' + LanguageCntryCount );
            
            Cookie tcountryCookie= new Cookie('country', CountryValue,null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{tcountryCookie});

            Cookie languageCookie= new Cookie('language', LanguageCntryCount.languageCountrymap.get(CountryValue),null, 31536000, false);
            ApexPages.currentPage().setCookies(new Cookie[]{languageCookie});

            if(String.isNotBlank(LanguageCntryCount.socialLogin)){
                System.debug('>>>>LanguageCntryCount.socialLogin:'+LanguageCntryCount.socialLogin);
                socialLoginType = LanguageCntryCount.socialLogin;
                socialMediaType = LanguageCntryCount.socialMediaType;

            }
            if(String.isNotBlank(LanguageCntryCount.supportEmail))
                supportEmail = LanguageCntryCount.supportEmail;
            
            UserLanguage = LanguageCntryCount.languageCountrymap.get(CountryValue);
        }
        if (String.isNotBlank(referredFromUrl)) {
            String decodedReferredFrom = EncodingUtil.urlDecode(referredFromUrl, 'UTF-8');
            this.ReferredFrom = decodedReferredFrom;
            if (decodedReferredFrom.indexOf('?') > 0){
                String params = decodedReferredFrom.substringAfter('?');
                String[] aParams = params.split('&', 0);
                for (String s : aParams) {
                    if (s.startsWith('errorMessage=') == true) {
                        String[] aErrMsg = s.split('=');
                        if (aErrMsg != null && aErrMsg.size() > 1 && 
                                (aErrMsg[1] == 'auth.failed' || aErrMsg[1] == 'usernot.active' || aErrMsg[1] == 'userhasnoprofile.org')) {
                            isAuthFailed = true;
                            ErrorMsgCode = aErrMsg[1];
                            ApexPages.currentPage().getParameters().put('authfail', String.valueOf(isAuthFailed));
                        }
                    }
                    else if(s.startsWith('IDToken1') == true) {
                        //ApexPages.currentPage().getParameters().put('IDToken1', String.valueOf(s));   
                        tokenValue = String.valueOf(s).substringAfter('IDToken1=');
                        tokenValue = EncodingUtil.URLDecode(tokenValue,'UTF-8');
                        System.debug('>>>>IDToken1:'+tokenValue);
                        System.debug('*** setting token ');
                    }
                    
                    
                    System.debug('Param:' + s);                 
                }
                
            }
        }
        this.AuthenticationFailed = isAuthFailed;
        System.debug('Authentication Failed: ' + isAuthFailed);
        System.debug('Token: ' + tokenValue);
        ApexPages.currentPage().getParameters().put('IDToken1', String.valueOf(tokenValue));            
    }

    
    global String getIsSSOParamSet() {

        if (String.isBlank(referredFromUrl) || (String.isNotBlank(referredFromUrl) && !referredFromUrl.contains('ims'))){
            System.debug('*** referredFromUrl->'+referredFromUrl);
            System.debug('***currentURL :' + currentURL );
            System.debug('*** Custom Base URL->'+site.getBaseCustomUrl());
            //System.debug('Bool:'+currentURL.toLowerCase().contains('/menu/pomplogin'));
            if(currentURL.contains('/login')){           
                String baseURL;
                if(String.isBlank(site.getBaseCustomUrl()))
                    baseURL = site.getBaseUrl();
                else
                    baseURL = site.getBaseCustomUrl();
                    
                System.debug('**** baseURL ->'+ baseURL);
                Cookie POMPCookie = ApexPages.currentPage().getCookies().get('app');
                if(POMPCookie != null){
                    Cookie appCookie = new Cookie('app', '', null, 0, false);
                    ApexPages.currentPage().setCookies(new Cookie[]{appCookie});
                }
                
                if(String.isNotBlank(IDToken))
                    loginParameters += '&IDToken1='+IDToken;
                if(String.isNotBlank(CountryValue))
                    loginParameters+= '&Country='+CountryValue;
                if(String.isNotBlank(tUserLanguage))
                    loginParameters+= '&Language='+tUserLanguage;
                        
                system.debug('loginParameters:' + loginParameters);
                if(String.isNotBlank(referredFromUrl) && !referredFromUrl.containsIgnoreCase('/menu/registration') && referredFromUrl.startsWith(baseURL) && !referredFromUrl.containsIgnoreCase('/apex/vfp_partnerslogout?newemail') && !referredFromUrl.containsIgnoreCase('/Menu/Logout?type=pr')){
                    if(referredFromUrl.containsIgnoreCase('IDToken1')){
                        pageReference referredURL = new PageReference(referredFromUrl);
                        referredURL.getParameters().remove('IDToken1');
                        referredFromUrl = referredURL.getURL();
                    }
                    return System.Label.CLAPR15PRM114 + '&RelayState=/partners/apex/VFP_PRMLoginSuccess?referredFromUrl='+referredFromUrl+loginParameters;
                }
                else
                    return System.Label.CLAPR15PRM114+'&RelayState=/partners/apex/VFP_PRMLoginSuccess'+loginParameters;
            }  
            else if(currentURL.contains('/pomplogin')){
                Cookie appCookie = new Cookie('app', 'POMP', null, -1, false);
                ApexPages.currentPage().setCookies(new Cookie[]{appCookie});
                System.debug('>>>>starturl:'+URLParams.get('StartURL'));
                if(string.isNotBlank(URLParams.get('StartURL'))){
                    return System.Label.CLMAR16PRM013 + loginParameters +'&RelayState=/pomp/apex/VFP_NewPompHomePage?PompStartUrl='+URLParams.get('StartURL');
                }
                else
                    return System.Label.CLMAR16PRM013 + loginParameters +'&RelayState=/pomp/apex/VFP_NewPompHomePage';
            }
            else
                return '';
        }     
        else return '';
    }

    @RemoteAction
    global static ResetPasswordResult ResetPassword(string emailAddress) {
        User usr;
        String bfoUName = emailAddress + Label.CLMAR13PRM05;
        ResetPasswordResult result = new ResetPasswordResult();
        string federatedId='';
        try {
            result.Success = false;
            List<User> lUsr = new List<User>([SELECT Id, federationIdentifier, email FROM USER WHERE Username = :bfoUName AND IsActive = true LIMIT 1]);
            if (!lUsr.isEmpty()) {
                usr = lUsr[0];
                federatedId = (usr != null ? usr.FederationIdentifier : '');
            }
            else {
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort uimsPort = new
                                        uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                uimsPort.timeout_x = 120000;
                uimsPort.clientCertName_x = System.Label.CLAPR15PRM018;
                uimsPort.endpoint_x = System.Label.CLAPR15PRM017;
                uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType srchUsrResult = uimsPort.searchUser(System.Label.CLAPR15PRM016, emailAddress);
                System.debug('Search Result: ' + srchUsrResult);
                federatedId = srchUsrResult.federatedId;
            }
            
            if(String.isNotBlank(federatedId)) {
                
                uimsv2ServiceImsSchneiderComAUM.accessElement application = new uimsv2ServiceImsSchneiderComAUM.accessElement();
                application.type_x = System.label.CLAPR15PRM020;//'APPLICATION';
                application.id = System.label.CLAPR15PRM022;//'prm';
                
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort abc = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                abc.endpoint_x = System.label.CLAPR15PRM017; //'https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager';
                abc.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);//120000; 
                abc.clientCertName_x = System.label.CLAPR15PRM018;//'Certificate_IMS';
                String resetStatus = abc.resetPassword(System.label.CLAPR15PRM019, federatedId, application);
                System.debug('*** Password reset status:' + resetStatus);
                result.Success = true;
            } else {
                result.ErrorMsg = 'Invalid email address';
            }

        } catch (Exception e) {
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
        }
        return result;
    }
}