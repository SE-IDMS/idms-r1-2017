@isTest
private class PRJ_updateFundAuthController_TEST
{
    static testMethod void test_PRJ_updateFundAuthController() 
    {
       
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
            
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.ParentFamily__c = 'Business';
        testProj.Parent__c = 'Buildings';
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.GeographicZoneSE__c = 'Global';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c = 'Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.ProjectClassification__c = 'Infrastructure';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        insert testProj;    
        
        Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
        insert testBud;            
        
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
        insert testDMTA2;  
                
        testProj.NextStep__c='Created';
        update testProj;
        
        
        DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
        testDMTA3.AuthorizedUser1__c = UserInfo.getUserId();
        testDMTA3.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA3.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA3.NextStep__c = 'Fundings Authorized';
        testDMTA3.Parent__c = testProj.Parent__c;
        testDMTA3.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA3.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testDMTA3; 
            
            
        testProj.NextStep__c = 'Project Open';
        testProj.AuthorizeFunding__c = 'Yes';
        testProj.ProjectClosureDate__c = system.today();
        testProj.EffectiveGoLiveDate__c = system.today();
        update testProj;
        
        PRJ_updateFundAuthController vfCtrl = new PRJ_updateFundAuthController(new ApexPages.StandardController(testProj));
        vfCtrl.callFundAuth();
    }
    
}