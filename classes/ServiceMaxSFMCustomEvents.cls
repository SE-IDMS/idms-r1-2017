global class ServiceMaxSFMCustomEvents {
	
	webservice static SVMXC.SFM_WrapperDef.SFM_PageData populateDeinstalledDates(SVMXC.SFM_WrapperDef.SFM_TargetRecord request)
    {
        SVMXC.SFM_ServicesDef servicesDef = new SVMXC.SFM_ServicesDef();
        map<String,Schema.SObjectType> allObjMap = Schema.getGlobalDescribe();  
        Sobject header = servicesDef.SFM_Page_GetHeaderRecord(request,allObjMap);
        map<String,List<Sobject>> detailListMap = servicesDef.SFM_Page_GetDetailRecords(request,allObjMap);
        
        Swap_Asset__c sAsset = (Swap_Asset__c) header;
        SVMXC__Installed_Product__c ip = null;
        if (sAsset.DeinstalledInstalledProduct__c != null)
        {
        	ip = [SELECT Id, DecomissioningDate__c FROM SVMXC__Installed_Product__c WHERE ID = :sAsset.DeinstalledInstalledProduct__c LIMIT 1];
        }
        
        if (ip != null && ip.DecomissioningDate__c != null)
        {
        	sAsset.DecommissionedDate__c = ip.DecomissioningDate__c;
        }
        else
        {
        	sAsset.DecommissionedDate__c = system.today();
        }
        
        SVMXC.SFM_WrapperDef.SFM_PageData pageData = new SVMXC.SFM_WrapperDef.SFM_PageData();
        pageData =  servicesDef.SFM_Page_BuildResponse(request,header,detailListMap);
        system.debug('*****'+pageData); 
        return pageData;
        
    }
    
    webservice static SVMXC.SFM_WrapperDef.SFM_PageData populateNewinstalledDates(SVMXC.SFM_WrapperDef.SFM_TargetRecord request)
    {
        SVMXC.SFM_ServicesDef servicesDef = new SVMXC.SFM_ServicesDef();
        map<String,Schema.SObjectType> allObjMap = Schema.getGlobalDescribe();  
        Sobject header = servicesDef.SFM_Page_GetHeaderRecord(request,allObjMap);
        map<String,List<Sobject>> detailListMap = servicesDef.SFM_Page_GetDetailRecords(request,allObjMap);
        
        Swap_Asset__c sAsset = (Swap_Asset__c) header;
        SVMXC__Installed_Product__c ip = null;
        if (sAsset.InstalledProduct__c != null)
        {
            ip = [SELECT Id, CommissioningDateInstallDate__c FROM SVMXC__Installed_Product__c WHERE ID = :sAsset.InstalledProduct__c LIMIT 1];
        }
        
        if (ip != null && ip.CommissioningDateInstallDate__c != null)
        {
            sAsset.CommissioningDateInstallDate__c = ip.CommissioningDateInstallDate__c;
        }
        else
        {
            sAsset.CommissioningDateInstallDate__c = system.today();
        }
        
        SVMXC.SFM_WrapperDef.SFM_PageData pageData = new SVMXC.SFM_WrapperDef.SFM_PageData();
        pageData =  servicesDef.SFM_Page_BuildResponse(request,header,detailListMap);
        system.debug('*****'+pageData); 
        return pageData;
        
    }

}