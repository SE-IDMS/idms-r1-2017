@isTest

private class FieloPRM_REST_MoveMemberByContactTest{

    static testMethod void unitTest(){
    
         
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'TestLastN';
            member.FieloEE__FirstName__c = 'TestFirstN';
            member.FieloEE__Street__c = 'testds';
            member.F_Account__c = acc.id;
                 
            insert member;
            
            FieloEE__Member__c membertO = new FieloEE__Member__c();
            membertO .FieloEE__LastName__c= 'testttt';
            membertO .FieloEE__FirstName__c = 'teeessttt';
            membertO .FieloEE__Street__c = 'tetstd';
            membertO .F_Account__c = accTo.id;
                 
            insert membertO ;     
            
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
   
            Contact con2 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: membertO.id limit 1];

 
            FieloPRM_REST_MoveMemberByContact rest = new FieloPRM_REST_MoveMemberByContact();
            FieloPRM_REST_MoveMemberByContact.postMoveMemberByContact( con1.id , con2.id );
        }

    }
    
}