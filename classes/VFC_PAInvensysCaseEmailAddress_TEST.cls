@isTest
public class VFC_PAInvensysCaseEmailAddress_TEST {
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.City__c = 'Bangalore';
        testAccount.SEAccountID__c = '12345678';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        Case objCase = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id,'testing');
        insert objCase;
        List<User> lstUser = new List<User>();
        for(integer i = 0; i < 5; i++) {
            User u = Utils_TestMethods.createStandardUser('testU'+i);
            lstUser.add(u);
        }
        insert lstUser;
        List<Subscriber__c> lstSubscribe = new List<Subscriber__c>();
        Subscriber__c testSubsrcibe = new Subscriber__c(Account__c = testAccount.Id, Case__c = objCase.Id, Email__c = 'test@test.com', User__c = lstUser[0].Id);
        lstSubscribe.add(testSubsrcibe);
        Subscriber__c testSubsrcibe1 = new Subscriber__c(Account__c = testAccount.Id, Email__c = 'test1@test1.com', User__c = lstUser[1].Id);
        lstSubscribe.add(testSubsrcibe1);
        Subscriber__c testSubsrcibe2 = new Subscriber__c(Account__c = testAccount.Id, Case__c = objCase.Id, Email__c = 'test@test2.com', User__c = lstUser[2].Id);
        lstSubscribe.add(testSubsrcibe2);
        Subscriber__c testSubsrcibe3 = new Subscriber__c(Account__c = testAccount.Id, Email__c = 'test1@test3.com', User__c = lstUser[3].Id);
        lstSubscribe.add(testSubsrcibe3);
        insert lstSubscribe;
    }
    
    @isTest static void testMethod1() {
        Case caseObj = [select id from case limit 1];
        Account accObj = [select id from account limit 1];
        apexpages.currentpage().getparameters().put('id', caseObj.Id);
        ApexPages.currentPage().getParameters().put('AccountId', accObj.Id);
        VFC_PAInvensysCaseEmailAddress controller = new VFC_PAInvensysCaseEmailAddress();
        controller.emailCaseAccount();
    }
}