/*
17-Feb-2014    Srinivas Nallapati  PRM Apr 14   Initial Creation

To Run this batch from console
  Batch_ProcessAccountFeatures batch_paf = new Batch_ProcessAccountFeatures(); 
  batch_paf.query = 'SELECT id,Account__c, PartnerProgram__c, ProgramLevel__c FROM ACC_PartnerProgram__c WHERE Active__c= true AND LastModifiedDate =Last_N_Days:15';
    Database.executebatch(batch_paf);

*/
global class Batch_ProcessAccountFeatures implements Database.Batchable<sObject>,Schedulable {
  
  public String query;
  Map<String,CS_PRM_ApexJobSettings__c> mapApexJobSettings = CS_PRM_ApexJobSettings__c.getall(); 
  
  global Batch_ProcessAccountFeatures() {
    
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    
    if(query != null)
      return Database.getQueryLocator(query);
    else
    {
      List<ProgramFeature__c> lstDeactivatedFeatures = [Select id, ProgramLevel__c FROM ProgramFeature__c WHERE FeatureStatus__c ='Inactive' AND DeactivatedDate__c=TODAY];
      set<id> progLevelIds = new set<id>();
      for(ProgramFeature__c pf : lstDeactivatedFeatures)
      {
        progLevelIds.add(pf.ProgramLevel__c);
      }

      query = 'SELECT id,Account__c, PartnerProgram__c, ProgramLevel__c FROM ACC_PartnerProgram__c WHERE Active__c= true AND ProgramLevel__c IN :progLevelIds';
      return Database.getQueryLocator(query);
    }  
    
  }

  global void execute(Database.BatchableContext BC, List<ACC_PartnerProgram__c> scope) {    
    AP_ProcessFeatureAssignment.processAccountAssignedFeatures(scope);
  }
  
  global void finish(Database.BatchableContext BC) {
    Integer numFlexJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Holding' OR Status = 'Queued' OR Status = 'Preparing'];
    // Check to avoid Limit Exception in FlexQueue
    CS_PRM_ApexJobSettings__c cntRes = CS_PRM_ApexJobSettings__c.getInstance('MAX_JOB_COUNT');
    if( numFlexJobs < cntRes.NumberValue__c ){
      Id jobKey =System.enqueueJob(new Batch_ProcessContactFeatures());
    }
  }

  //Schedule method
  global void execute(SchedulableContext sc) {
    String batchQueryFilter = '%POMPV1FTR%';
    List<CronTrigger> crnTrigger = new List<CronTrigger>([SELECT State, CronJobDetail.Name FROM CronTrigger WHERE CronJobDetail.Name LIKE :batchQueryFilter AND (State = 'Waiting' OR State = 'Acquired' OR State = 'Queued' OR State = 'Holding') LIMIT 100]);
    if(crnTrigger.isEmpty()){
        System.debug('**Batch Start Method called**');
        Datetime sysTime = System.now().addMinutes(Integer.valueOf(mapApexJobSettings.get('POMPV1FTR').NumberValue__c));
        String cronExp = sysTime.format('s m H d M \'?\' yyyy');
        Batch_ProcessAccountFeatures batch_paf = new Batch_ProcessAccountFeatures(); 
        System.schedule('PRM Job - POMPV1FTR '+'-'+sysTime, cronExp, batch_paf);
    }
   }
}