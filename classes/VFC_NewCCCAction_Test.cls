@isTest
private class VFC_NewCCCAction_Test{
    public static testMethod void testNewCCCAction(){
        System.debug('#### VFC_NewCCCAction_Test.testNewCCCAction Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        
        CCCAction__c objAction = new CCCAction__c();
        ApexPages.StandardController CCCActionController = new ApexPages.StandardController(objAction);
        VFC_NewCCCAction controller = new VFC_NewCCCAction(CCCActionController);
          
        PageReference pageRefWithCaseId = Page.VFP_NewCCCAction;
        pageRefWithCaseId.getParameters().put(Label.CLSEP12CCC03, objCaseForAction.Id);
        Test.setCurrentPage(pageRefWithCaseId);
        controller.goToCCCActionEditPage();
        
        PageReference pageRefWithretURL = Page.VFP_NewCCCAction;
        pageRefWithCaseId.getParameters().put(Label.CL00330, '/'+ objCaseForAction.Id);
        Test.setCurrentPage(pageRefWithretURL);
        controller.goToCCCActionEditPage();
        
        PageReference pageRef = Page.VFP_NewCCCAction;
        Test.setCurrentPage(pageRef);
        controller.goToCCCActionEditPage();
        
        
        
        System.debug('#### VFC_NewCCCAction_Test.testNewCCCAction Ends ####');
    }
}