/********************************************************************************************************************
    Created By : Nicolas PALITZYNE
    Created Date : 28 November 2016
    Description : This clas stores the mapping methods for the account conversion (related to the Digital and PRM Account
                  Topic.
    
    Release    : November Release 2016, BR-8145, CCC Feature
********************************************************************************************************************/
public class AP_AccountConvMapping {

    public static Account prmToBusinessAccount(Account aPrmAccount) {
    
        Account businessAccount       = aPrmAccount;
        
        businessAccount.Street__c                  = aPrmAccount.PRMStreet__c;
        businessAccount.AdditionalAddress__c       = aPrmAccount.PRMAdditionalAddress__c;
        businessAccount.ClassLevel2__c             = aPrmAccount.PRMAreaOfFocus__c;
        businessAccount.ClassLevel1__c             = aPrmAccount.PRMBusinessType__c;
        businessAccount.City__c                    = aPrmAccount.PRMCity__c;
        businessAccount.Name                       = aPrmAccount.PRMCompanyName__c != null ? aPrmAccount.PRMCompanyName__c : businessAccount.Name;
        businessAccount.Phone                      = aPrmAccount.PRMCompanyPhone__c;    
        businessAccount.Website                    = aPrmAccount.PRMWebsite__c;
        businessAccount.Country__c                 = aPrmAccount.PRMCountry__c;
        businessAccount.CurrencyIsoCode            = aPrmAccount.PRMCurrencyIsoCode__c;
        businessAccount.EmployeeSize__c            = aPrmAccount.PRMEmployeeSize__c;
        businessAccount.CorporateHeadquarters__c   = aPrmAccount.PRMCorporateHeadquarters__c;                        
        businessAccount.MarketServed__c            = aPrmAccount.PRMMarketServed__c;
        businessAccount.AccType__c                 = aPrmAccount.PRMAccType__c;
        businessAccount.StateProvince__c           = aPrmAccount.PRMStateProvince__c;
        businessAccount.VATNumber__c               = aPrmAccount.PRMTaxId__c;
        businessAccount.AnnualSales__c             = aPrmAccount.PRMAnnualSales__c != null ? Decimal.valueOf(aPrmAccount.PRMAnnualSales__c) : null;
        businessAccount.ZipCode__c                 = aPrmAccount.PRMZipCode__c;                                                        
        
        return businessAccount;
    }
}