@isTest
private class AP_LocalizedFSB_Test {
    
    static testMethod void Method1() {
        
        OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
        FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;
            
        BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                accOrg1.Operations__c = True;
                insert  accOrg1;
        BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                accOrg2.Name='Test';
                accOrg2.Entity__c='Test Entity-2'; 
                accOrg2.SubEntity__c='Test Sub-Entity 2';
                accOrg2.Operations__c = True;
                insert  accOrg2;
        BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                accOrg.Name='Test';
                accOrg.Entity__c='Test Entity-2';  
                accOrg.SubEntity__c='Test Sub-Entity 2';
                accOrg.Location__c='Test Location 2';
                accOrg.Location_Type__c='Design Center';
                accOrg.Operations__c = True;
                insert  accOrg;
                
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        insert runAsUser;
        
        EntityStakeholder__c OrgSH1 = Utils_TestMethods.createEntityStakeholder(accOrg.Id,runAsUser.Id,'Regional FSB Leader');
        insert OrgSH1;
        
        LocalizedFSB__c locFSB1 = new LocalizedFSB__c();
        locFSB1.Organization__c = accOrg.Id;
        locFSB1.FieldServiceBulletin__c = FSB.Id;
        insert locFSB1;
    }
    
    static testMethod void Method2() {
           
        OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
        FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;
            
        BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                accOrg1.Operations__c = True;
                insert accOrg1;
                
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        insert runAsUser;
        
        EntityStakeholder__c OrgSH2 = Utils_TestMethods.createEntityStakeholder(accOrg1.Id,runAsUser.Id,'CS&Q Manager');
        insert OrgSH2;  
        
        LocalizedFSB__c locFSB = new LocalizedFSB__c();
        locFSB.Organization__c = accOrg1.Id;
        locFSB.FieldServiceBulletin__c = FSB.Id;
        insert locFSB;
    }
}