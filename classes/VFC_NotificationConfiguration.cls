global class VFC_NotificationConfiguration{     

        public transient wrapperClass wrapperClassVariable{get;set;}
        public transient list<SetupAlerts__c> lsetupAlert {get;set;}
        transient list<TrackSetupAlets__c> lexistingAlerts;
        transient Map<Id,list<Id>> ackAlerts = new Map<Id,list<Id>> ();
        public transient Integer NumberOfAlerts{get;set;}
        transient user u;
        transient String CookieCountry;
        transient String value;
        public transient String referer{get;set;}
        
        public VFC_NotificationConfiguration(){
            Cookie country;
            Cookie alertCookie;
            NumberOfAlerts = 0;
            wrapperClassVariable = new wrapperClass();
            lsetupAlert = new list<SetupAlerts__c>();
            u = [select Id,toLabel(Country__c) from user WHERE Id = :userInfo.getUserId()];
            system.debug('User:' + u);
            country = ApexPages.currentPage().getCookies().get('country');
            referer = ApexPages.currentPage().getHeaders().get('referer');
            
            if(country != null)
                CookieCountry = country.getValue();
            system.debug('country:' + CookieCountry);
            
            
            alertCookie = ApexPages.currentPage().getCookies().get('alertCookie');
            if(alertCookie == null){
                alertCookie = new Cookie('alertCookie', 'false','/partners', -1, false);
                ApexPages.currentPage().setCookies(new Cookie[]{alertCookie});
            }
            
            system.debug('alertCookie:' + alertCookie);
            main();
        }
        
        public void main(){
            Cookie repeatAlert;
            
            list<Id> existingAlerts = new list<Id> ();
            list<TrackSetupAlets__c> existingList = new list<TrackSetupAlets__c> ([Select Id,SetupAlerts__c,User__c from TrackSetupAlets__c WHERE User__c = :u.id]);
            
            for(TrackSetupAlets__c ts :existingList){
                existingAlerts.add(ts.SetupAlerts__c);
            }
            system.debug('Existing Alerts:' + existingAlerts);
            
            lsetupAlert = [select Id,recordType.DeveloperName,Content__c,toLabel(Display__c),FromDate__c,PRMCountryClusterSetup__c,PRMCountryClusterSetup__r.Country__r.countryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.countryCode__c,PRMCountryClusterSetup__r.MemberCountry2__r.countryCode__c,PRMCountryClusterSetup__r.MemberCountry3__r.countryCode__c,PRMCountryClusterSetup__r.MemberCountry4__r.countryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.countryCode__c,ToDate__c from SetupAlerts__c WHERE FromDate__c <= :System.now() AND ToDate__c >= :System.now() AND Active__c = true AND Id NOT IN :existingAlerts AND recordType.DeveloperName NOT IN ('Notification','UserNotification') ORDER BY PRMCountryClusterSetup__c ASC NULLS FIRST]; 
            system.debug('list Alerts:' + lsetupAlert );
            
            for(SetupAlerts__c s:lsetupAlert ){
                system.debug('PRMCountryClusterSetup__r.Country__r.countryCode__c:' + s.PRMCountryClusterSetup__r.Country__r.countryCode__c);
                
                     if(s.PRMCountryClusterSetup__c == null){
                        wrapperClassVariable.display = s.Display__c;
                        wrapperClassVariable.content = s.Content__c;
                        wrapperClassVariable.Id = s.Id;
                        }
                     else if(s.PRMCountryClusterSetup__c != null &&(s.PRMCountryClusterSetup__r.Country__r.countryCode__c == CookieCountry ||
                        s.PRMCountryClusterSetup__r.MemberCountry1__r.countryCode__c == CookieCountry ||
                        s.PRMCountryClusterSetup__r.MemberCountry2__r.countryCode__c == CookieCountry ||
                        s.PRMCountryClusterSetup__r.MemberCountry3__r.countryCode__c == CookieCountry ||
                        s.PRMCountryClusterSetup__r.MemberCountry4__r.countryCode__c == CookieCountry ||
                        s.PRMCountryClusterSetup__r.MemberCountry5__r.countryCode__c == CookieCountry )){
                        wrapperClassVariable.display = s.Display__c;
                        wrapperClassVariable.content = s.Content__c;
                        wrapperClassVariable.Id = s.Id;
                        }
                    if(wrapperClassVariable.Id != null)
                        break;
                }
                
            if(wrapperClassVariable.Id != null){
                if(wrapperClassVariable.display == 'Repetitively On Every Login Until Acknowledged'){
                    repeatAlert = new Cookie('repeatAlert','true', '/partners', -1, false);
                    ApexPages.currentPage().setCookies(new Cookie[]{repeatAlert});
                }
                NumberOfAlerts = 1;   
            } 
        }
        @RemoteAction
        global static Boolean closePopup(Id setupAlertId,string setupAlertdisplay,boolean alertAcknowledged){
            system.debug('Id:' + setupAlertId);
            system.debug('display:' + setupAlertdisplay);
            TrackSetupAlets__c trackAlerts = new TrackSetupAlets__c();
            if(setupAlertdisplay== 'Show Only Once'){
                trackAlerts.SetupAlerts__c = setupAlertId;
                trackAlerts.User__c = userInfo.getUserId();
                try{
                    insert trackAlerts;
                    return true;
                }
                catch(exception e){return false;}
            }
            else if(setupAlertdisplay== 'Repetitively On Every Login Until Acknowledged'){
                if(alertAcknowledged){
                    trackAlerts.SetupAlerts__c = setupAlertId;
                    trackAlerts.User__c = userInfo.getUserId();
                    try{
                        insert trackAlerts;
                        return true;
                    }
                    catch(exception e){return false;}
                }
                else 
                    return true;
            }
            else{return true;}
        }
        public class wrapperClass{
            public String display{get;set;}
            public String content{get;set;}
            public String Id{get;set;}
            
            public void wrapperClass(){
                display='';
                content='';
                Id='';
            }
        }
    }