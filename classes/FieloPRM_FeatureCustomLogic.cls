public interface FieloPRM_FeatureCustomLogic { 

    void promote(set<String> PRMUIMSIdList);
    void demote(set<String> PRMUIMSIdList);

}