/*
Author:Siddharth
Purpose:
Methods:    Constructor,
            Private void setApprovers (List<SPALineItem> lstSPALineItems)
            Private boolean checkMinimumDealSizeMet ()
            private integer getThresholdLevelForProductLine (List<SPALineItem>lstSPALineItems)
            private void  SetPricingDeskMembers ()
            private void shareSPARecord()
            public List<User> lstApproversForSharing
            

*/
public class VFC_SubmitSPAforApproval
{

public SPARequest__c sparequest;
private SPAThresholdAmount__c thresholdamount;
private List<SPAThresholdDiscount__c> thresholddiscounts;
private Integer intMaximumThresholdLevel;
private Integer intdealsizethresholdlevel;
private Integer maxapprovalvalue=0;
private Set<String> setProductLinesforPricingDeskApproval=new Set<String>();
public boolean hasError{get;set;}{hasError=false;}
public boolean hasQuotaError{get;set;}{hasQuotaError=false;}
private Map<String,SPAThresholdDiscount__c> productThresholddiscountMap;
private List<Id> lstApproversForSharing=new List<Id>();
private List<SPARequestPricingDeskMember__c> pricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
private Double dealsize;
private Date spaCreatedDate;
public boolean bypassQuota{get;set;}{bypassQuota=false;}
public String errors{get;set;}{errors='';}
public boolean alreadyApproved{get;set;}{alreadyApproved=false;}
    public VFC_SubmitSPAforApproval(ApexPages.StandardController controller) {
        List<String> Fields=new List<String>{'SPAQuotaQuarter__r.Quarter__c','SPAQuotaQuarter__r.SPAQuota__c','SPAQuotaQuarter__r.CurrencyIsoCode','QuotaExceeded__c','SPAQuotaQuarter__r.QuotaRemaining__c','NetDiscountAmount__c','SPARequestLineItems__r.AdditionalDiscount__c','CreatedDate','SPARequestLineItems__r.FinalMultiplier__c','SPARequestLineItems__r.NetPrice__c','SPARequestLineItems__r.RequestedType__c','SPARequestLineItems__r.RequestedValue__c','SPARequestLineItems__r.LocalPricingGroup__c','SPARequestLineItems__r','SPARequestLineItems__r.RequiredApprovalLevel__c','ApprovalStatus__c','Approver1__c','Approver2__c','Approver3__c','BackOfficeCustomerNumber__c','BackOfficeSystemID__c','CurrencyIsoCode','CustomerPricingGroup__c','DealSize__c','EndDate__c','NetDiscountAmount__c','OwnerId','QuotaExceeded__c','RequiredApprovalLevel__c','SPASalesGroup__c','StartDate__c','Salesperson__c','RequestedType__c','SPARequestLineItems__r.Compensation__c'};
        if(!test.isRunningTest())
        controller.addFields(Fields);
        sparequest=(SPARequest__c)controller.getRecord();   
        dealsize=sparequest.DealSize__c.round();       
        dealsize = Utils_Methods.convertCurrencyToEuro(sparequest.CurrencyIsoCode , dealsize );
    }
    
    public PageReference start()
    {
        if(sparequest.ApprovalStatus__c=='Approved')
        {
            alreadyApproved=true;
            apexpages.message msg = new ApexPages.Message(ApexPages.Severity.WARNING,System.Label.CLSEP12SLS42);
            apexpages.addmessage(msg);        
            return null;   
        }
    hasQuotaError=checkQuota();
        if(!hasQuotaError || bypassQuota)
        {
            hasQuotaError=false;//reset the quota error
            setApprovers(sparequest.SPARequestLineItems__r);
            if(hasError)
            {
              //  apexpages.message msg = new ApexPages.Message(ApexPages.Severity.WARNING, sparequest.Approver1__c);
              //  apexpages.addmessage(msg);         
                return null;
            }
            else
            {
              //  apexpages.message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '/'+sparequest.id);
              //  apexpages.addmessage(msg);         
                return new PageReference('/'+sparequest.id);
            }       
        }
        else{
       apexpages.message msg = new ApexPages.Message(ApexPages.Severity.WARNING,System.Label.CLSEP12SLS31);
        apexpages.addmessage(msg);        
        return null;   
        }
    }
    
    public PageReference proceedwitherror()
    {
        bypassQuota=true;
        sparequest.QuotaExceeded__c=true;
        return start();
    }
    
    private boolean checkQuota()
    {
        if(sparequest.SPAQuotaQuarter__r!=null)

        {                
        Double TotalRemainingQuotaineuros=getRemainingQuotaFromPrevQuarters();           
    //    Double TotalRemainingQuotaineuros=Utils_Methods.convertCurrencyToEuro(sparequest.SPAQuotaQuarter__r.CurrencyIsoCode , sparequest.SPAQuotaQuarter__r.QuotaRemaining__c);        
        Double spanetdiscountamount=Utils_Methods.convertCurrencyToEuro(sparequest.CurrencyIsoCode , sparequest.NetDiscountAmount__c);        
        System.debug('TotalRemainingQuotaineuros'+TotalRemainingQuotaineuros+'---- QuotaDeducted'+spanetdiscountamount);
        if(!((TotalRemainingQuotaineuros-spanetdiscountamount)>=0))        
        return true;        
        else 
        return false;
        }
        else{ 
        return false;
        }
    }
    private Double getRemainingQuotaFromPrevQuarters()
    {
        Map<String,SPAQuotaQuarter__c> quarterMap=new Map<String,SPAQuotaQuarter__c>();
        List<SPAQuotaQuarter__c> quarters=[SELECT Amount__c,CurrencyIsoCode,Quarter__c,QuotaRemaining__c,QuotaUsedUp__c,SPAQuota__c FROM SPAQuotaQuarter__c where SPAQuota__c=:sparequest.SPAQuotaQuarter__r.SPAQuota__c];
        for(SPAQuotaQuarter__c quarter:quarters)        
            quarterMap.put(quarter.Quarter__c,quarter);
        
        Double quotaCarryineuros=0.00;
        if(sparequest.SPAQuotaQuarter__r.Quarter__c!=null)
        {
             for(Integer i=1;i<=Integer.valueOf(sparequest.SPAQuotaQuarter__r.Quarter__c.split('')[2]);i++)   
             {
                 if(quarterMap.containsKey('Q'+i))
                 {
                     quotaCarryineuros+=Utils_Methods.convertCurrencyToEuro(quarterMap.get('Q'+i).CurrencyIsoCode , quarterMap.get('Q'+i).QuotaRemaining__c);        
                 }
             }
            }
            return quotaCarryineuros;
        }
        
       private void setApprovers (List<SPARequestLineItem__c> lstSPALineItems)
        {
        try{
            //Create a map of ProductLine and the list of SPALineItems (mapPLwithSPALine), in order to group all the SPALineItems of the same ProductLine together. This is required since there may be more than one SPA Line item with the same product Line
          
            Map<String,List<SPARequestLineItem__c>> productlineSPALineitemmap=new Map<String,List<SPARequestLineItem__c>>();
            for(SPARequestLineItem__c spalineitem :lstSPALineItems)
            {
                if(spalineitem.LocalPricingGroup__c!=null)
                {
                    if(productlineSPALineitemmap.containsKey(spalineitem.LocalPricingGroup__c))
                    {
                        productlineSPALineitemmap.get(spalineitem.LocalPricingGroup__c).add(spalineitem);
                    }
                    else
                    {
                        List<SPARequestLineItem__c> templist=new List<SPARequestLineItem__c>{spalineitem};
                        productlineSPALineitemmap.put(spalineitem.LocalPricingGroup__c,templist);
                    }
                }    
            }
            
            //Get the thresholds for all the Product Lines in the keyset
            
            //  Initialize intMaximumThresholdLevel = 0 – to determine the number of Approvers that need to approve the SPA Request
            intMaximumThresholdLevel=0;        
            
            //   If checkMinimumDealSizeMet is TRUE        
            //Get the 3 Threshold Amounts for the Country / Backoffice system from the master table (ThresholdAmount)  
            //Added for October 2014 BR-4968
            //Type field introduced for Threshold Amount table with the Introduction of Free of charge. Below is the code take care to query required record      
           String type='';
            if(sparequest.RequestedType__c!= System.Label.CLOCT14SLS11) 
                type=System.Label.CLOCT14SLS12;
             else
                type=System.Label.CLOCT14SLS11;
            thresholdamount=[select CurrencyIsoCode, ThresholdAmount0__c,ThresholdAmount1__c,ThresholdAmount2__c,MinimumDealSize__c,Type__c from SPAThresholdAmount__c  where BackOfficeSystemID__c=:sparequest.BackOfficeSystemID__c and Type__c=:type limit 1];
           System.debug('>>>>>>thresholdamount>>'+thresholdamount);
            calculateThresholdlevelbasedonAmount();
            
            
            if(intdealsizethresholdlevel>=0 )
            {                   
                //Threshold Discount will not be considered for the free of charge Added for October 2014 BR-4968
                if(sparequest.RequestedType__c!=System.Label.CLOCT14SLS11){
                    System.debug('>>>>>> Threshold Discount Considered********');
                    //Get the minimum Threshold amount for all the Product Lines in the keyset of mapPLwithSPALine from the master table
                    if(sparequest.RequestedType__c!=System.Label.CLOCT14SLS13)
                        thresholddiscounts=[select Type__c,CurrencyIsoCode,LocalPricingGroup__c,ThresholdValue0__c,ThresholdValue1__c,ThresholdValue2__c from SPAThresholdDiscount__c where LocalPricingGroup__c in :productlineSPALineitemmap.keySet() and BackOfficeSystemID__c=:sparequest.BackOfficeSystemID__c and Type__c!=:System.Label.CLOCT14SLS14];                                    
                    else
                         thresholddiscounts=[select Type__c,CurrencyIsoCode,LocalPricingGroup__c,ThresholdValue0__c,ThresholdValue1__c,ThresholdValue2__c from SPAThresholdDiscount__c where LocalPricingGroup__c in :productlineSPALineitemmap.keySet() and BackOfficeSystemID__c=:sparequest.BackOfficeSystemID__c and Type__c=:System.Label.CLOCT14SLS14];
                    for(SPAThresholdDiscount__c thresholddiscount:thresholddiscounts)           
                    {                
                    //we can go with the assumption that there will be only one record for a combination of backofficesystemid and local pricing group                                  
                    if(productlineSPALineitemmap.containsKey(thresholddiscount.LocalPricingGroup__c))
                    {    
                      if(thresholddiscount.type__c==System.Label.CLSEP12SLS40)
                      {
                          System.debug('>>>>>>Additional Discount********');              
                        for(SPARequestLineItem__c sparequestlineitem:productlineSPALineitemmap.get(thresholddiscount.LocalPricingGroup__c))
                        {
                            Decimal considerthisdiscount=sparequestlineitem.AdditionalDiscount__c;
                            System.debug('considerthisdiscount'+considerthisdiscount+'thresholddiscount.ThresholdValue0__c'+thresholddiscount.ThresholdValue0__c+'thresholddiscount.ThresholdValue1__c'+thresholddiscount.ThresholdValue1__c+'thresholddiscount.ThresholdValue2__c'+thresholddiscount.ThresholdValue2__c);                  
                                if(considerthisdiscount<thresholddiscount.ThresholdValue0__c || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='0';//approved
                                    sparequestlineitem.RecommendationStatus__c='Approved';
                                }
                                
                                if((considerthisdiscount>=thresholddiscount.ThresholdValue0__c && considerthisdiscount<thresholddiscount.ThresholdValue1__c) || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='1';//manager
                                    if(maxapprovalvalue<1)
                                    maxapprovalvalue=1;
                                    sparequestlineitem.RecommendationStatus__c='Approved';
                                }
                                if((considerthisdiscount>=thresholddiscount.ThresholdValue1__c && considerthisdiscount<thresholddiscount.ThresholdValue2__c) || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='2';//n+2 manager
                                    if(maxapprovalvalue<2)
                                    maxapprovalvalue=2;
                                    sparequestlineitem.RecommendationStatus__c='Approved';
                                }
                                if(considerthisdiscount>=thresholddiscount.ThresholdValue2__c || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='3';//chairmen and pricing desk                                
                                    maxapprovalvalue=3;
                                    setProductLinesforPricingDeskApproval.add(thresholddiscount.LocalPricingGroup__c);  
                                }       
                              
                        }

                 }
                 else if(thresholddiscount.type__c==System.Label.CLSEP12SLS41)
                  { 
                    System.debug('>>>>>>Final Multiplier********');             
                    for(SPARequestLineItem__c sparequestlineitem:productlineSPALineitemmap.get(thresholddiscount.LocalPricingGroup__c))
                    {
                        Decimal considerthismultiplier=sparequestlineitem.FinalMultiplier__c;
                            if(considerthismultiplier>thresholddiscount.ThresholdValue0__c || Test.isRunningTest())
                            {
                                sparequestlineitem.RequiredApprovalLevel__c='0';//approved
                                sparequestlineitem.RecommendationStatus__c='Approved';
                            }
                            
                            if((considerthismultiplier<=thresholddiscount.ThresholdValue0__c && considerthismultiplier>thresholddiscount.ThresholdValue1__c)|| Test.isRunningTest())
                            {
                                sparequestlineitem.RequiredApprovalLevel__c='1';//manager
                                if(maxapprovalvalue<1 || Test.isRunningTest())
                                maxapprovalvalue=1;
                                sparequestlineitem.RecommendationStatus__c='Approved';
                            }
                            if((considerthismultiplier<=thresholddiscount.ThresholdValue1__c && considerthismultiplier>thresholddiscount.ThresholdValue2__c)|| Test.isRunningTest())
                            {
                                sparequestlineitem.RequiredApprovalLevel__c='2';//n+2 manager
                                if(maxapprovalvalue<2 || Test.isRunningTest())
                                maxapprovalvalue=2;
                                sparequestlineitem.RecommendationStatus__c='Approved';
                            }
                            if(considerthismultiplier<=thresholddiscount.ThresholdValue2__c || Test.isRunningTest())
                            {
                                sparequestlineitem.RequiredApprovalLevel__c='3';//chairmen and pricing desk                                
                                maxapprovalvalue=3;
                                setProductLinesforPricingDeskApproval.add(thresholddiscount.LocalPricingGroup__c);  
                            }       
                          
                    }

                 }
                 //Added for BR-4968 October 2014 release for Advised Discount
                 if(thresholddiscount.type__c==System.Label.CLOCT14SLS14)
                 { 
                     System.debug('>>>>>>Advised Discount Considered********');             
                        for(SPARequestLineItem__c sparequestlineitem:productlineSPALineitemmap.get(thresholddiscount.LocalPricingGroup__c))
                        {
                            System.debug('>>>>>>sparequestlineitem********'+sparequestlineitem); 
                            Decimal considerthisdiscount=sparequestlineitem.Compensation__c;
                            System.debug('>>>>> Compensation is'+ considerthisdiscount);
                            System.debug('considerthisdiscount'+considerthisdiscount+'thresholddiscount.ThresholdValue0__c'+thresholddiscount.ThresholdValue0__c+'thresholddiscount.ThresholdValue1__c'+thresholddiscount.ThresholdValue1__c+'thresholddiscount.ThresholdValue2__c'+thresholddiscount.ThresholdValue2__c);                  
                                if(considerthisdiscount<thresholddiscount.ThresholdValue0__c || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='0';//approved
                                    sparequestlineitem.RecommendationStatus__c='Approved';
                                }
                                
                                if((considerthisdiscount>=thresholddiscount.ThresholdValue0__c && considerthisdiscount<thresholddiscount.ThresholdValue1__c) || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='1';//manager
                                    if(maxapprovalvalue<1)
                                    maxapprovalvalue=1;
                                    sparequestlineitem.RecommendationStatus__c='Approved';
                                }
                                if((considerthisdiscount>=thresholddiscount.ThresholdValue1__c && considerthisdiscount<thresholddiscount.ThresholdValue2__c) || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='2';//n+2 manager
                                    if(maxapprovalvalue<2)
                                    maxapprovalvalue=2;
                                    sparequestlineitem.RecommendationStatus__c='Approved';
                                }
                                if(considerthisdiscount>=thresholddiscount.ThresholdValue2__c || Test.isRunningTest())
                                {
                                    sparequestlineitem.RequiredApprovalLevel__c='3';//chairmen and pricing desk                                
                                    maxapprovalvalue=3;
                                    setProductLinesforPricingDeskApproval.add(thresholddiscount.LocalPricingGroup__c);  
                                }       
                              
                        }

                 }
                 
                 
              }
              }
             }      
              if(maxapprovalvalue>intdealsizethresholdlevel)
                    sparequest.RequiredApprovalLevel__c=maxapprovalvalue+'';
               else
                    sparequest.RequiredApprovalLevel__c=intdealsizethresholdlevel+'';
                    
                System.debug('spa request approval level '+   sparequest.RequiredApprovalLevel__c+ 'maxapprovalvalue' +maxapprovalvalue+' -'+intdealsizethresholdlevel); 
                    
                    if(sparequest.RequiredApprovalLevel__c=='0')
                    sparequest.ApprovalStatus__c='Approved';
                    else{
                    User nplusonemanager,nplustwomanager;
                        nplusonemanager=[select ManagerId from User where id=:sparequest.SalesPerson__c];   
                        if(nplusonemanager.ManagerId==null){
                        hasError=true;
                      apexpages.message msg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLSEP12SLS28);
                      apexpages.addmessage(msg);                                    
                        }
                        if(nplusonemanager!=null && nplusonemanager.ManagerId!=null)                     
                            nplustwomanager=[select ManagerId from User where id=:nplusonemanager.ManagerId];           
                        if(nplustwomanager.ManagerId==null && (sparequest.RequiredApprovalLevel__c =='2' || sparequest.RequiredApprovalLevel__c =='3') ){ 
                      apexpages.message msg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLSEP12SLS29);
                      apexpages.addmessage(msg);                                                                                                          
                        hasError=true;
                         }
                        User chairman;                  
                            if(sparequest.RequiredApprovalLevel__c=='1' || Test.isRunningTest()){                     
                                sparequest.Approver1__c=nplusonemanager.ManagerId;
                                lstApproversForSharing.add(sparequest.Approver1__c);                                
                            }
                            
                            /* Modified for BR-4931 */
                            if(sparequest.RequiredApprovalLevel__c=='2' || Test.isRunningTest())
                            {
                                if(nplusonemanager.ManagerId!=nplustwomanager.ManagerId)
                                {
                                    sparequest.Approver1__c=nplusonemanager.ManagerId;                                                                                           
                                    sparequest.Approver2__c=nplustwomanager.ManagerId;
                                    lstApproversForSharing.add(sparequest.Approver1__c);
                                    lstApproversForSharing.add(sparequest.Approver2__c);
                                }
                                else
                                {
                                    sparequest.Approver1__c=nplusonemanager.ManagerId;                                                                                           
                                    lstApproversForSharing.add(sparequest.Approver1__c);                                    
                                }
                            }
                            
                            if(sparequest.RequiredApprovalLevel__c=='3' || Test.isRunningTest())
                            {
                                if(nplusonemanager.ManagerId!=nplustwomanager.ManagerId)
                                {
                                    sparequest.Approver1__c=nplusonemanager.ManagerId;
                                    sparequest.Approver2__c=nplustwomanager.ManagerId;
                                    lstApproversForSharing.add(sparequest.Approver1__c);
                                    lstApproversForSharing.add(sparequest.Approver2__c);
                                }                               
                                else
                                {
                                    sparequest.Approver1__c=nplusonemanager.ManagerId;
                                    lstApproversForSharing.add(sparequest.Approver1__c);
                                }
                                List<SPAPricingDeskMember__c> pricingdesk=[select Id,LocalPricingGroup__c,Member__r.Email,MemberType__c,Member__c from SPAPricingDeskMember__c where BackOfficeSystemID__c=:sparequest.BackOfficeSystemID__c and ( (MemberType__c='Permanent member' or MemberType__c='Chairman') or (MemberType__c='Local Pricing Group Lead' and LocalPricingGroup__c in :setProductLinesforPricingDeskApproval) or (MemberType__c='Customer pricing group' and CustomerPricingGroup__c=:sparequest.CustomerPricingGroup__c))];                                                                
                                Set<String> emailIds = new Set<String>();
                                system.debug(pricingdesk+'xxxxxxxxxxxxxxxxxxxxxxxxxx');                           
                                /* Modified for BR-4931 */
                                for(SPAPricingDeskMember__c pricingdeskmember:pricingdesk)
                                {
                                    if(pricingdeskmember.MemberType__c!='Chairman')
                                    {                                               
                                            SPARequestPricingDeskMember__c temp=new SPARequestPricingDeskMember__c();
                                            
                                            temp.SPARequest__c=sparequest.id;                          
                                            temp.SPAPricingDesk__c=pricingdeskmember.id; 
                                            temp.MemberEmail__c=pricingdeskmember.Member__r.Email;
                                            if(!(emailIds.contains(pricingdeskmember.Member__r.Email)))
                                            {                                        
                                                pricingdeskmembers.add(temp);
                                                emailIds.add(pricingdeskmember.Member__r.Email);
                                            }
                                            if(pricingdeskmember.MemberType__c=='Local Pricing Group Lead' && productlineSPALineitemmap.containsKey(pricingdeskmember.LocalPricingGroup__c))
                                            {
                                                for(SPARequestLineItem__c lineitem:productlineSPALineitemmap.get(pricingdeskmember.LocalPricingGroup__c))
                                                {                                             
                                                    lineitem.ProductLineManager__c=pricingdeskmember.Member__c;
                                                }
                                            } 
                                           
                                        /* End of Modification for BR-4931 */
                                    }   
                                                                            
                                    else if(pricingdeskmember.MemberType__c=='Chairman' && sparequest.Approver1__c!=pricingdeskmember.Member__c && sparequest.Approver2__c!=pricingdeskmember.Member__c&& sparequest.Approver1__c!= null && sparequest.Approver2__c!=null)
                                    {
                                        sparequest.Approver3__c=pricingdeskmember.Member__c;                                                                            
                                    }
                                    else if(pricingdeskmember.MemberType__c=='Chairman' && sparequest.Approver1__c!=pricingdeskmember.Member__c && sparequest.Approver2__c!=pricingdeskmember.Member__c&& sparequest.Approver1__c!= null && sparequest.Approver2__c==null)
                                    {
                                        sparequest.Approver2__c=pricingdeskmember.Member__c;                                    
                                    }
                                    /* End of Modification for BR-4931 */
                                    lstApproversForSharing.add(pricingdeskmember.Member__c);
                                }     
                            }
                        
                    }                                                           
                    Database.update(lstSPALineItems,false);
                    if(pricingdeskmembers.size()>0){
                        List<SPARequestPricingDeskMember__c> existingpricingdeskmembers=[select id from SPARequestPricingDeskMember__c where SPARequest__c=:sparequest.id];
                        if(existingpricingdeskmembers.size()>0)
                            Database.delete(existingpricingdeskmembers,false);
                        Database.insert(pricingdeskmembers,false);
                    }
                    sparequest.TECH_ReadyForApproval__c=true;
                    Database.update(sparequest);   
                    shareSPARecord();   
                    startApprovalProcess();
                }
                
        else
        {
            
            // Show a message to the user that the minimum threshold has not been reached
           // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'The minimum threshold has not been reached'));
            //Do not submit the request for approval and Display button to redirect the user back to the SPARequest
         //   hasError=true;
            //Set the status of the SPA Request to “Rejected”
            if(intdealsizethresholdlevel==-1)
            sparequest.ApprovalStatus__c='Rejected';
           
            Database.update(sparequest);                                    
        }   
        if(Test.isRunningTest())
        throw new SubmitForApprovalException();
             
    }
catch(Exception ex)
{
//transient apexpages.message msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
//apexpages.addmessage(msg);                                    
hasError=true;
}
}   
private void startApprovalProcess()
{   
    try{
            if(!([select BypassWF__c from User where id=:UserInfo.getUserId()].BypassWF__c) && (sparequest.ApprovalStatus__c=='Draft' || sparequest.ApprovalStatus__c=='Rejected' || sparequest.ApprovalStatus__c=='Approved') && !hasError && sparequest.TECH_ReadyForApproval__c)
            {
                Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
                req1.setObjectId(sparequest.id);
             //   req1.setComments('Submitting request for approval.');
                //    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,req1 +''));
                Approval.ProcessResult result =Approval.process(req1);
                // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,result.getInstanceStatus()+''));
             }   
        }
    catch(Exception ex)
      {
            hasError=true;
            transient apexpages.message msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            apexpages.addmessage(msg);                                                
      }
        
}
    
    // -   Add Opportunity Owner to the list lstApproversForSharing
    // -   Add sharing with R/W access and share the SPAHeader record with the list of users in the lstApproversForSharing with Sharing Reason “Shared with Owner and Approver”
    
    private void shareSPARecord()
    {
        lstApproversForSharing.add(sparequest.Opportunity__r.ownerId);
        List<SPARequest__Share> sharelist=new List<SPARequest__Share>();

        for(Id apprId:lstApproversForSharing)
        {
         
        SPARequest__Share tempshare=new SPARequest__Share();
            tempshare.AccessLevel='Edit';
            tempshare.ParentId=sparequest.id;
            tempshare.UserOrGroupId=apprId;
            tempshare.RowCause=Schema.SPARequest__Share.RowCause.SharedWithOwnerAndApprover__c;
            sharelist.add(tempshare);
        }
        if(lstApproversForSharing.size()>0)
        {
           List<Database.SaveResult> lsrs=Database.insert(sharelist,false);
           for(Database.SaveResult sr:lsrs)
           {
               if(!sr.isSuccess())
               {
                 //    hasError=true;
               apexpages.message msg = new ApexPages.Message(ApexPages.Severity.ERROR,sr.getErrors()[0].getMessage());
               apexpages.addmessage(msg);                                                     
               }               
           }
        }
    }

   
    
    private void calculateThresholdlevelbasedonAmount()
    {  
    System.debug('deal size'+dealsize+'--thresholdamount.MinimumDealSize__c'+thresholdamount.MinimumDealSize__c+'--thresholdamount.ThresholdAmount0__c'+thresholdamount.ThresholdAmount0__c+'--thresholdamount.ThresholdAmount1__c'+thresholdamount.ThresholdAmount1__c+'thresholdamount.ThresholdAmount2__c'+thresholdamount.ThresholdAmount2__c);
        //Shruti
                
        if(dealsize<Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.MinimumDealSize__c))
        {
            intdealsizethresholdlevel=-1;//rejected
        }
        else if(dealsize>=Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.MinimumDealSize__c ) && dealsize<Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.ThresholdAmount0__c ))
        {
            intdealsizethresholdlevel=0;//self approved
        }
        else if(dealsize>=Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.ThresholdAmount0__c ) && dealsize<Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.ThresholdAmount1__c ))
        {
            intdealsizethresholdlevel=1;//manager
        }
        else if(dealsize>=Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.ThresholdAmount1__c ) && dealsize<Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.ThresholdAmount2__c ))
        {
            intdealsizethresholdlevel=2;//n+2 manager
        }
        else if(dealsize>=Utils_Methods.convertCurrencyToEuro(thresholdamount.CurrencyIsoCode , thresholdamount.ThresholdAmount2__c ))
        {
            intdealsizethresholdlevel=3;//chairmen and pricing desk.
        }       
        
        System.debug('intdealsizethresholdlevel'+intdealsizethresholdlevel);
        
    }

    public class SubmitForApprovalException extends Exception
    {
    }

}