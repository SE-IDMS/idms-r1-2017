/**
25-April-2013
Shruti Karn  
PRM May13 Release    
Initial Creation: test class for    
    1. ProgramFeatureAfterInsert, ProgramFeatureAfterUpdate, AP_PRF_CountryFeatureUpdate
    2. ProgramLevelAfterInsert, ProgramLevelAfterUpdate, AP_PRL_CountryFeatureUpdate
    3. ProgramRequirementAfterInsert, ProgramRequirementAfterUpdate, AP_PGR_CountryRequirementUpdate
    4. PartnerProgramAfterUpdate, PartnerProgramBeforeUpdate, AP_PartnerPRG_CountryProgramMethods

 */
@isTest
private class PartnerProgram_TEST {
    static testMethod void myUnitTest() {
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
       // u.ProgramAdministrator__c = true;
        insert u;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.TECH_CountriesId__c = country.id;
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        insert globalProgram;
        
        CS_GlobalPRGtoCountryPRGFields__c newSetting = new CS_GlobalPRGtoCountryPRGFields__c();
        newSetting.name = 'programtype__c';
        insert newSetting;
        
        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype programActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ProgramRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
        
        insert newRecTypeMap;
        
        Recordtype levelRecordType = [Select id from RecordType where developername =  'GlobalProgramLevel' limit 1];
        Recordtype featureRecordType = [Select id from RecordType where developername =  'GlobalFeature' limit 1];
        Recordtype featurecountryRecordType = [Select id from RecordType where developername =  'CountryFeature' limit 1];
        
        ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
        VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
        system.runAs(u)
        {
            
            myPageCon1.goToCountryPRGEditPage();
            globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
            update globalProgram;
           
            ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel.recordtypeid = levelRecordType.Id;
            insert newLevel;
                        
            FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
            feature.Enabled__c = 'Contact';
            feature.Visibility__c = 'Contact';
            insert feature;
            
            RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.ApplicableTo__c = 'Contact';
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req;
            
            FeatureRequirement__c  newFeatureReq = Utils_TestMethods.createFeatureRequirement(feature.ID ,req.Id);
            insert newFeatureReq;
            
            ProgramFeature__c newFeature = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature.FeatureStatus__c = Label.CLMAY13PRM09;
            newFeature.recordtypeid = featureRecordType.Id;
            //newFeature.recordtypeid = Label.CLMAY13PRM06 ;
            insert newFeature;
                      
            
            ProgramRequirement__c newRequirement = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel.Id, req.ID);
            newRequirement.Active__c = true;
            newRequirement.recordtypeid  = programActivityReq.Id;
            insert newRequirement;
            
                      
          /*  ProgramRequirement__c newRequirement2 = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel.Id, req.ID);
            newRequirement2.Active__c = false;
            newRequirement2.recordtypeid  = programActivityReq.Id;
            insert newRequirement2;
          */
            myPageCon1.goToCountryPRGEditPage();
            
          /*  newRequirement2.active__C = true;
            update newRequirement2;
                       
            ProgramFeature__c newFeature2 = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature2.FeatureStatus__c = 'Inactive';
            newFeature2.recordtypeid = featureRecordType.Id;           

            insert newFeature2;
           
            newFeature2.FeatureStatus__c =Label.CLMAY13PRM09;
            update newFeature2;
          */  
            newLevel.ModifiablebyCountry__c = 'Yes';
            update newLevel;
            
            newRequirement.Active__c = false;
            update newRequirement;
            newRequirement.Active__c = true;
            update newRequirement;
            test.startTest();
        
            ProgramLevel__c newLevel2 = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel2.recordtypeid = levelRecordType.Id;
            newLevel2.LevelStatus__c ='Inactive';
            insert newLevel2;
            
            newLevel2.LevelStatus__c = Label.CLMAY13PRM19;
            update newLevel2;
            
            ProgramLevel__c newLevel3 = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel3.LevelStatus__c =Label.CLMAY13PRM19;
            newLevel3.recordtypeid = levelRecordType.Id;
            insert newLevel3;
            
            newLevel2.LevelStatus__c = Label.CLMAY13PRM19;
            update newLevel2;
            
            list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
            countryPartnerProgram = [Select id,programstatus__c,country__c from PartnerProgram__c where Id = :globalProgram.Id limit 1];
            if(!countryPartnerProgram.isEmpty())
            {
                countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
                countryPartnerProgram[0].recordtypeid = Label.CLMAY13PRM05;
                update countryPartnerProgram;
                list<ProgramLevel__c> lstProgramLevel = [Select id,levelstatus__c from ProgramLevel__c where partnerprogram__c = :countryPartnerProgram[0].Id limit 10];
        
                for(PRogramLevel__c countryLevel : lstProgramLevel )
                {
                    countryLevel.levelstatus__C = Label.CLMAY13PRM19;
                }
                update lstProgramLevel ;
                
                list<ProgramRequirement__c> lstProgramReq = [Select id, active__c,programlevel__c from ProgramRequirement__c where partnerprogram__c = :countryPartnerProgram[0].Id limit 10];
                for(ProgramRequirement__c countryReq : lstProgramReq )
                {
                    countryReq.active__C = true;
                    
                }
                update lstProgramReq;
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = countryPartnerProgram[0].country__c;
                
                insert acc;
                
                Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
                contact1.PRMContact__c = true;
                insert contact1;
                //ACC_PartnerProgram__c accPRogram = Utils_TestMethods.createAccountProgram(lstProgramLevel[0].Id , countryPartnerProgram[0].Id,acc.Id);
                //try
                //{
               //     insert accPRogram;
               // }
               // catch(Exception e){}
               // acc.IsPartner  = true;
               // update acc;
              //  insert accPRogram;     
                ProgramFeature__c countryFeature1= Utils_TestMethods.createProgramFeature(countryPartnerProgram[0].Id , lstProgramLevel[0].Id, feature.Id);
                countryFeature1.recordtypeid = featurecountryRecordType.id;
                countryFeature1.FeatureStatus__c =Label.CLMAY13PRM09;
                insert countryFeature1;
                list<ProgramFeature__c> lstProgramFeature = [Select id, featurestatus__c,programlevel__c from ProgramFeature__c where partnerprogram__c = :countryPartnerProgram[0].Id limit 10];
                for(ProgramFeature__c countryFeature : lstProgramFeature )
                {
                    countryFeature.FeatureStatus__C = Label.CLMAY13PRM09;
                    
                }
                update lstProgramFeature ;
                         
                
              /*  accPRogram.ProgramLevel__C = lstProgramLevel[1].Id;
                update accPRogram; 
                accPRogram.active__c = false;
                update accPRogram;*/
            }
            test.stopTest();
        }
     
    }
    
     static testMethod void myUnitTest2() {
                         
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
       // u.ProgramAdministrator__c = true;
        insert u;
        
        User user2 = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        user2.BypassVR__c = true;
        user2.country__c = country.countrycode__c;
        insert user2;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.TECH_CountriesId__c = country.id;
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        insert globalProgram;
        
        CS_GlobalPRGtoCountryPRGFields__c newSetting = new CS_GlobalPRGtoCountryPRGFields__c();
        newSetting.name = 'programtype__c';
        insert newSetting;
        
        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype programActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ProgramRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
        
        insert newRecTypeMap;
        
        
        ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
        VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
        system.runAs(u)
        {
            
            myPageCon1.goToCountryPRGEditPage();
            globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
            update globalProgram;
            
            Recordtype levelRecordType = [Select id from RecordType where developername =  'GlobalProgramLevel' limit 1];
            Recordtype featureRecordType = [Select id from RecordType where developername =  'GlobalFeature' limit 1];
            
            ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel.recordtypeid = levelRecordType.Id;
            insert newLevel;
                        
            FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
            feature.Enabled__c = 'Contact';
            feature.Visibility__c = 'Contact';
            insert feature;
            
            RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.ApplicableTo__c = 'Contact';
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req;
            
            FeatureRequirement__c  newFeatureReq = Utils_TestMethods.createFeatureRequirement(feature.ID ,req.Id);
            insert newFeatureReq;
            
            ProgramFeature__c newFeature = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature.FeatureStatus__c = Label.CLMAY13PRM09;
            newFeature.recordtypeid = featureRecordType.Id;
            insert newFeature;
                      
            
            ProgramRequirement__c newRequirement = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel.Id, req.ID);
            newRequirement.Active__c = true;
            newRequirement.recordtypeid  = programActivityReq.Id;
            insert newRequirement;
            
            myPageCon1.goToCountryPRGEditPage();
                     
          /*  ProgramFeature__c newFeature2 = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature2.FeatureStatus__c = 'Inactive';
            newFeature2.recordtypeid = featureRecordType.Id;           

            insert newFeature2;
           
            newFeature2.FeatureStatus__c =Label.CLMAY13PRM09;
            update newFeature2;
         */  
            newLevel.ModifiablebyCountry__c = 'Yes';
            update newLevel;
            
            newRequirement.Active__c = false;
            update newRequirement;
            test.StartTest();
            list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
            countryPartnerProgram = [Select id,programstatus__c,country__c from PartnerProgram__c where GlobalPartnerProgram__c = :globalProgram.Id limit 1];
            if(!countryPartnerProgram.isEmpty())
            {
                countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
                countryPartnerProgram[0].recordtypeid = Label.CLMAY13PRM05;
                update countryPartnerProgram[0];
                list<ProgramLevel__c> lstProgramLevel = [Select id,levelstatus__c from ProgramLevel__c where partnerprogram__c = :countryPartnerProgram[0].Id limit 10];
        
                for(PRogramLevel__c countryLevel : lstProgramLevel )
                {
                    countryLevel.levelstatus__C = Label.CLMAY13PRM19;
                }
                update lstProgramLevel ;
                
                list<ProgramRequirement__c> lstProgramReq = [Select id, active__c,programlevel__c from ProgramRequirement__c where partnerprogram__c = :countryPartnerProgram[0].Id limit 10];
                for(ProgramRequirement__c countryReq : lstProgramReq )
                {
                    countryReq.active__C = true;
                    
                }
                update lstProgramReq;
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = countryPartnerProgram[0].country__c;
                
                insert acc;
                
                Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
                contact1.PRMContact__c = true;
                insert contact1;
                ACC_PartnerProgram__c accPRogram = Utils_TestMethods.createAccountProgram(lstProgramLevel[0].Id , countryPartnerProgram[0].Id,acc.Id);
                try
                {
                    insert accPRogram;
                }
                catch(Exception e){}
                acc.IsPartner  = true;
                acc.PRMAccount__c = True;
                update acc;
                insert accPRogram;     
               
                list<ProgramFeature__c> lstProgramFeature = [Select id, featurestatus__c,programlevel__c from ProgramFeature__c where partnerprogram__c = :countryPartnerProgram[0].Id limit 10];
                for(ProgramFeature__c countryFeature : lstProgramFeature )
                {
                    countryFeature.FeatureStatus__C = Label.CLMAY13PRM09;
                    
                }
                update lstProgramFeature ;
                
                globalProgram.programtype__C = 'Industry';
                update globalPRogram;
                
                try
                {
                    countryPartnerProgram[0].ownerid = user2.id;
                    update countryPartnerProgram[0];
                    globalPRogram.ownerid = user2.id;
                    update globalPRogram;
                
                }
                catch(Exception e){}
                
                try
                {
                    globalPRogram.ownerid = user2.id;
                    update globalPRogram;
                
                }
                catch(Exception e){}
            }
           test.stopTest();
        }
      
        
    }
    
}