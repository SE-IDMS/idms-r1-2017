/*
    Author          : Ankit (ACN) 
    Date Created    : 28/05/2011
    Description     : Test class for the apex class  VFC32_DraftFAQ.
*/
@isTest
private class VFC32_DraftFAQ_TEST
{
    
/* @Method <This method CaseToggleSpamTestMethod is used test the  VFC32_DraftFAQ class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Exception>
*/
    static testMethod void DraftFAQTestMethod()
    {          
    
       // Country__c country = Utils_TestMethods.createCountry();
        //insert country;
        Account accounts = Utils_TestMethods.createAccount();
        //accounts.country__c=country.id;
        insert accounts;
        
        contact contact =Utils_TestMethods.createContact(accounts.Id,'TestContact');
       // contact.country__c=country.id;
        contact.CorrespLang__c='french';
        database.insert(contact);           
  
       Case cases =Utils_TestMethods.createCase(accounts.id,contact.id,'closed');    
       cases.answerToCustomer__c='please close the case';
       database.insert(cases);
     
            ApexPages.StandardController sc1= new ApexPages.StandardController(cases);            
            VFC32_DraftFAQ  draftFaq = new  VFC32_DraftFAQ (sc1);
            //draftFaq.faqId='faq';
            draftFaq.selectedBusinessEntity='None';
            draftFaq.redirect();
            draftFaq.selectedBusinessEntity='APS';
            draftFaq.selectedBusinessSubEntity='Industry';
            draftFaq.redirect();
            draftfaq.filename='test';
            draftfaq.filebody=Blob.valueOf('Unit Test Attachment Body'); 
            draftFaq.upload();
            draftFaq.upload();
            draftFaq.removeattachedfile();
            draftFaq.removeattachedfile();
            draftFaq.getBEOptions();
            draftFaq.getBSEOptions();
            
             
        }  
   }