@istest
private class VFC83_newLiteratureRequest_Test{
    static testmethod void VFC83_newLiteratureRequestTest(){
     
    User testU = Utils_TestMethods.createStandardUser('testU');
    insert testU ;
    
    Country__c cont = Utils_TestMethods.CreateCountry();
    Insert cont;
    
    StateProvince__c stat = Utils_TestMethods.createStateProvince(cont.Id);
    Insert stat;

    Account testAccount = Utils_TestMethods.createAccount(testU.Id);
    insert testAccount ;
    testAccount.StateProvince__c=stat.id;
    testAccount.Country__c=cont.id;
    testAccount.Street__c='Test Street';
    testAccount.City__c='Test City';
    testAccount.POBox__c='009009';
    testAccount.ZipCode__c='009009';
    testAccount.POBoxZip__c='009009';    
    Update  testAccount;
    
    Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
    insert testContact;
    testContact.Country__c=cont.id;
    testContact.StateProv__c = stat.id;
    testContact.Street__c='Test Street';
    testContact.City__c='Test City';    
    testContact.POBoxZip__c='009009'; 
    Update testContact;
    
    Case testCase = Utils_TestMethods.createCase(testAccount.Id, testContact.Id,'Open');
    insert testCase; 
    
    LiteratureRequest__c ltr = Utils_TestMethods.createLiteratureRequest(testAccount.Id,testContact.Id,testCase.Id);
    Insert ltr;
    
    DataTemplate__c GMRProduct = new DataTemplate__c();
       GMRProduct.Field1__c='Test1';
       GMRProduct.Field2__c='Test2';
       GMRProduct.Field3__c='Test3';
       GMRProduct.Field4__c='Test4';
       GMRProduct.Field5__c='Test5';
       GMRProduct.Field6__c='Test6';
       GMRProduct.Field7__c='Test7';
       GMRProduct.Field8__c='Test8';
       GMRProduct.Field9__c='Test9';
       GMRProduct.Field10__c='Test10';
       GMRProduct.Field11__c='Test11';
    
    LiteratureRequest__c  lt = new LiteratureRequest__c();
    
    PageReference vfPage = Page.VFP83_newLiteratureRequest;
    Test.setCurrentPage(vfPage);
    
   
    VFC83_newLiteratureRequest vfCtrl = new VFC83_newLiteratureRequest(new ApexPages.StandardController(lt));
    VFC83_newLiteratureRequest.newLiteratureRequestGMRSearch  InnerclassObj = new VFC83_newLiteratureRequest.newLiteratureRequestGMRSearch();
    
    ApexPages.StandardController cc1 = new ApexPages.StandardController(testCase);
   
    //VCC08_GMRSearch forCase = new VCC08_GMRSearch(cc1);    
    //InnerclassObj.Init('Test',forCase);   
   //  VFC_ControllerBase ctrl = new VFC_ControllerBase();
   //  InnerclassObj.PerformAction(GMRProduct,ctrl);
   // InnerclassObj.Cancel(controller);   
     
      
     ApexPages.StandardController LitController = new ApexPages.StandardController(ltr);
     VFC83_newLiteratureRequest LiteratureController = new VFC83_newLiteratureRequest(LitController );
     LiteratureController.returnEditPage();
     LiteratureController.checkCommercialReference();
     LiteratureController.saveCommercialReference(GMRProduct);   
    
    }
}