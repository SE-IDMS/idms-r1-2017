// Added by Uttara - Notes to bFO - Q2 2016 Release - Send Notifications to FSB Team Memebers

public class VFC_FSBTeamExtendedEmail {
    
    public String fsbid {get; set;}
    public Set<String> stUsers = new Set<String>();
    public Set<String> stEmailIds = new Set<String>();
    public String objId;
    public String temp;
        
    public VFC_FSBTeamExtendedEmail(ApexPages.StandardSetController controller) {
        
        fsbid = ApexPages.currentPage().getParameters().get('id');
        System.debug('!!!! fsbid : ' + fsbid);
    } 
    
    public PageReference sendEmailtoExtendedTeam() {
        
        System.debug('!!!! sendEmailtoExtendedTeam method entered');
        for(FSBTeam__c team : [SELECT User__c, Email__c, FieldServiceBulletin__c FROM FSBTeam__c WHERE FieldServiceBulletin__c =: fsbid]) {
            temp = team.Id;
            if(team.User__c != null)
                stUsers.add(team.User__c);
            else
                stEmailIds.add(team.Email__c);
        }
        
        System.debug('!!!! stUsers : ' + stUsers);
        
        objId=(new List<String> (stUsers).get(0));
        
        System.debug('!!!! objId : ' + objId);
        
        if(stUsers.size() == 1) {
            for(User u : [SELECT Email FROM User WHERE Id IN: stUsers]) {
                stEmailIds.add(u.Email);
            }
        } 
        else {
            for(User u : [SELECT Email FROM User WHERE Id IN: stUsers AND Id !=: objId]) {
                stEmailIds.add(u.Email);
            }             
        }
        
        
        System.debug('!!!! stEmailIds : ' + stEmailIds);
        
        //Sending Notification
        
        if(!stEmailIds.isEmpty()) {
            List<Messaging.SingleEmailMessage> lstMails =  new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage  mail = new Messaging.SingleEmailMessage(); 
            
            EmailTemplate Template = [SELECT Id, Name FROM EmailTemplate WHERE Name =: 'FSB_EmailToFSBTeam'];
            
            mail.setTargetObjectId(objId);
            mail.setTargetObjectId(objId);
            mail.setWhatId(temp);
            mail.setTemplateId(Template.Id);
            mail.setToAddresses(new List<String> (stEmailIds));
            mail.setSaveAsActivity(false); 
            
            lstMails.add(mail);
            if(!lstMails.isEmpty())
                Messaging.sendEmail(lstMails);
        }
       
        PageReference fsbPage = new PageReference('/' + fsbid);
        System.debug('!!!! fsbPage : ' + fsbPage);
        return fsbPage;
    }
}