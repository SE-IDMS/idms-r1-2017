/*
Created By: Deepak Kumar
Created Date: 16-07-2013
Description: This class has been used to update ShipToAccount and BillToAccount in WorkOrderBefore insert before update trigger.
*/

Public Class AP_WOBeforInsertUpdateClass{
   
   public static void Ship_BillToAccountupdate(SVMXC__Service_Order__c[] wolist)
   {
   
set<id> AccId = New Set<Id>();
Map<Id,Account> accmap = New Map<Id,Account>();

    for(SVMXC__Service_Order__c wo:wolist)
    {
         if(wo.SoldToAccount__c !=null && wo.BillToAccount__c==null)
         {
         AccId.add(wo.SoldToAccount__c);
         }
    }
List<Account> acclist = [Select Id,Name from Account Where Id in:AccId];    
  
    if(acclist!=null && acclist.size()>0)
     {
     accmap.putall(acclist);
     }
     
    for(SVMXC__Service_Order__c wo1:wolist)  
    {
         if(accmap.containskey(wo1.SoldToAccount__c))
         {
             wo1.BillToAccount__c = accmap.get(wo1.SoldToAccount__c).Id;
             //wo1.ShipToAccount__c =accmap.get(wo1.SoldToAccount__c).Id;
         }
    
    }
   }     

}