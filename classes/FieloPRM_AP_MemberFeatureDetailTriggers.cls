/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 14/09/2016
* Description: 
********************************************************************/

public without sharing class FieloPRM_AP_MemberFeatureDetailTriggers{
    
    /********************************************************************
    METHOD: memberFeatureActivatesInactivates
    MODE: after insert, after update, after delete
    DML: UPDATE FieloPRM_MemberFeature__c
    COMMENTS: Inactivates or activates Member Feature
    ********************************************************************/
    public static void memberFeatureActivatesInactivates(List<FieloPRM_MemberFeatureDetail__c> triggerNewOld, map<Id, FieloPRM_MemberFeatureDetail__c> triggerOldMap){
        set<Id> setMemberFeatureIds = new set<Id>();
        
        system.debug('TOKEN trigger.isInsert: ' + trigger.isInsert);
        system.debug('TOKEN trigger.isUpdate: ' + trigger.isUpdate);
        system.debug('TOKEN trigger.isDelete: ' + trigger.isDelete);
        
        if(trigger.isInsert){
            for(FieloPRM_MemberFeatureDetail__c mfd: triggerNewOld){
                setMemberFeatureIds.add(mfd.F_PRM_MemberFeature__c);
            }
        }else if(trigger.isUpdate){
            for(FieloPRM_MemberFeatureDetail__c mfd: triggerNewOld){
                if(mfd.F_PRM_isActive__c != triggerOldMap.get(mfd.Id).F_PRM_isActive__c){
                    setMemberFeatureIds.add(mfd.F_PRM_MemberFeature__c);
                }
            }
        }else if(trigger.isDelete){
            for(FieloPRM_MemberFeatureDetail__c mfd: triggerNewOld){
                setMemberFeatureIds.add(mfd.F_PRM_MemberFeature__c);
            }
        }
        
        if(!setMemberFeatureIds.isEmpty()){
            List<FieloPRM_MemberFeature__c> listMFtoUpdate = new List<FieloPRM_MemberFeature__c>();
            for(FieloPRM_MemberFeature__c mf: [SELECT Id, F_PRM_IsActive__c, (SELECT Id FROM Member_Feature_Details__r WHERE F_PRM_isActive__c = true LIMIT 1) FROM FieloPRM_MemberFeature__c WHERE Id IN: setMemberFeatureIds]){
                
                system.debug('TOKEN mf.Id: ' + mf.Id);
                system.debug('TOKEN mf.F_PRM_IsActive__c: ' + mf.F_PRM_IsActive__c);
                system.debug('TOKEN mf.Member_Feature_Details__r: ' + mf.Member_Feature_Details__r);
                
                if(mf.Member_Feature_Details__r.isEmpty()){
                    if(mf.F_PRM_IsActive__c){
                        mf.F_PRM_IsActive__c = false;
                        listMFtoUpdate.add(mf);
                    }
                }else{
                    if(!mf.F_PRM_IsActive__c){
                        mf.F_PRM_IsActive__c = true;
                        listMFtoUpdate.add(mf);
                    }
                }
            }
            
            system.debug('TOKEN listMFtoUpdate.SIZE: ' + listMFtoUpdate.size());
            system.debug('TOKEN listMFtoUpdate: ' + listMFtoUpdate);
            
            if(!listMFtoUpdate.isEmpty()){
                update listMFtoUpdate;
            }
        }
        
    }
    
}