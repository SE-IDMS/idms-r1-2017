public class VFC_NewFrameOpportunity
{
    public Id oppAgreeId{get;set;}
    public String URL{get;set;}
    
    public VFC_NewFrameOpportunity(ApexPages.StandardController controller)
    {     
        oppAgreeId = ApexPages.currentPage().getParameters().get('oppAgreeId');  
        system.debug('oppAgreeId = '+oppAgreeId );
    }
    
    public Pagereference populateFrameOppURL()
    {
        system.debug('oppAgreeId = '+oppAgreeId ); 
        
        URL = WS03_NewFrameOpportunity.Redirect(oppAgreeId);
        system.debug('the URL is : ' + URL);
        PageReference reRend = new PageReference(URL);
        return reRend;    
          
     }   
}