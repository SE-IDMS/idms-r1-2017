/**************************************************************************************
    Author: Fielo Team (Elena J. Schwarzböck)
    Date: 26/03/2015
    Description: 
    Related Components: Fielo_PRM_CL_008_News
***************************************************************************************/
public without sharing class FieloPRM_AP_NewsController{

    public static final string NEWS  = 'News';
    
    public map<id,FieloEE__Tag__c> mapIdTag{get;set;}
    public FieloEE__Component__c componentObj {get;set;}
    public String fieldTagName {get;set;} 
    private List<FieloEE__TagItem__c> listTagItems{get;set;} 
    
    private map<string,Schema.SObjectField> tagSchemaMaps {get;set;} 
    private String languageMember = FieloEE.OrganizationUtil.getLanguage();    

    public FieloPRM_AP_NewsController(){
        tagSchemaMaps = Schema.SObjectType.FieloEE__Tag__c.fields.getMap();

        String idMenu = ApexPages.currentPage().getParameters().get('idMenu');
        
        if(fieldTagName == null){
            fieldTagName = 'Name';
            String fieldName = 'Name_' + languageMember + '__c';
            if(tagSchemaMaps.containskey(fieldName)){
                fieldTagName = fieldName;
            }
        }
    
        List<FieloEE__Category__c> listCatNews = [SELECT Id FROM FieloEE__Category__c WHERE Name =: NEWS AND RecordType.DeveloperName = 'CategoryItem' LIMIT 1];
        
                
        if(!listCatNews.isEmpty()){
            
            List<FieloEE__News__c> listNews = FieloEE.NewsService.getNews(new Set<String>{'Id'},FieloEE.ProgramUtil.getProgramByDomain().Id,FieloEE.MemberUtil.getMemberId(),null,null,listCatNews[0].Id,null,null,null);
               
            String soql2 = 'SELECT Id, FieloEE__Tag__c, FieloEE__Tag__r.' +  fieldTagName  + ' FROM FieloEE__TagItem__c WHERE FieloEE__News__c IN: listNews ORDER BY FieloEE__Tag__r.FieloEE__Order__c';             
            listTagItems = (List<FieloEE__TagItem__c>) Database.query(soql2);
            
            mapIdTag = new map<id,FieloEE__Tag__c>();
            for(FieloEE__TagItem__c tagItem: listTagItems){
                if(!mapIdTag.containskey(tagItem.FieloEE__Tag__c)){
                    mapIdTag.put(tagItem.FieloEE__Tag__c,tagItem.FieloEE__Tag__r);
                }
            }
        }
    }   
}