global class ZuoraQuoteRulesEnginePlugin implements  zqu.ZQuoteRulesEngine.QuoteRulesEnginePlugin {

 
    public static Boolean runValidationRules(zqu__Quote__c quote,  List<zqu.ZChargeGroup> zcgs, List<zqu.ZQuoteRulesEngine.ChangeLog> logs){
        System.debug('In runValidationRules of  ZuoraQuoteRulesEnginePlugin ');
        zqu.ZQuoteRulesEngine.ChangeLog log = new zqu.ZQuoteRulesEngine.ChangeLog();
        // Validate rate plans only after quote has been created.
        if(quote.Id != null){
            System.debug('In runValidationRules of ZuoraQuoteRulesEnginePlugin w/non-null quote ID');
            

            if (!checkQuoteHasField(quote, 'Entity__c') || !'SOL'.equals(quote.Entity__c))
                return true;
              
            //first, loop through the charge groups to get all product Ids
            List<String> productIds = new List<String>();
            for (zqu.zChargeGroup thisChargeGroup : zcgs) {
                System.debug('thisChargeGroup.productId: '+thisChargeGroup.productId);
                productIds.add(thisChargeGroup.productId);
            }


            /*
            for (zqu.ZChargeGroup zcg : zcgs) {
                System.debug('###### INFO: zcg = '+ zcg);
            }

            //ZuoraUpdateQuoteRatePlanCharge.updateZChargeGroups(quote, zqu.zQuoteUtil.getChargeGroups(quote.Id));
            ZuoraUpdateQuoteRatePlanCharge.updateZChargeGroups(quote, zcgs);
            //*/


            List<zqu__ZProduct__c> productInfoList = [SELECT Id, Name, zqu__SKU__c, RefLicenses__c, AddingLicenses__c, NumLicMaxAllowed__c FROM zqu__ZProduct__c 
                                                    WHERE Id IN :productIds];

            //loop through retrieved prods to get the welcome product RefLicenses__c
            Decimal refLicensesWelcomePkg = 0;
            String welcomeProductId = '';
            Map<String,zqu__ZProduct__c> productIdToInfoMap = new Map<String,zqu__ZProduct__c>();
            for (zqu__ZProduct__c thisProduct : productInfoList) {

                //System.debug('thisProduct.zqu__SKU__c.startsWith(\'CA2\'): '+thisProduct.zqu__SKU__c.startsWith('CA2'));
                //if (thisProduct.zqu__SKU__c.startsWith('CA2') || thisProduct.zqu__SKU__c.startsWith('C2A')) {
                if (thisProduct.RefLicenses__c == 'Yes') { 
                    welcomeProductId = thisProduct.Id;
                    //if (thisProduct.RefLicenses__c != null )
                    if (thisProduct.NumLicMaxAllowed__c != null )
                        //refLicensesWelcomePkg = thisProduct.RefLicenses__c;
                        refLicensesWelcomePkg = thisProduct.NumLicMaxAllowed__c;
                }
                productIdToInfoMap.put(thisProduct.Id, thisProduct);
            }

            //now go through the charge groups
            //if the charge group is NOT under the "welcome" product (via "welcomeProductId")
            //we need to count up the AddingLicenses__c value
            Decimal addingLicensesTotal = 0;
            Decimal numLicensesTotal = 0;
            Decimal numLicensesWelcomePkg = 0;
            for (zqu.zChargeGroup thisChargeGroup : zcgs) {
                String thisProdId = thisChargeGroup.productId;
                System.debug('welcomeProductId: '+welcomeProductId);
                if (String.isEmpty(welcomeProductId)) {
                    welcomeProductId = 'noWelcomeProduct';
                }
                if (String.isEmpty(thisProdId)) {
                    thisProdId = '';
                }
                System.debug('welcomeProductId.equals(thisProdId): '+welcomeProductId.equals(thisProdId));
                if (!welcomeProductId.equals(thisProdId)){
                    System.debug('in if statement');
                    System.debug('productIdToInfoMap.get(thisProdId).AddingLicenses__c: '+productIdToInfoMap.get(thisProdId).AddingLicenses__c);
                    if (productIdToInfoMap.get(thisProdId).AddingLicenses__c == null){
                        addingLicensesTotal += 0;
                    } else {
                        addingLicensesTotal += productIdToInfoMap.get(thisProdId).AddingLicenses__c;
                        System.debug('addingLicensesTotal: '+ addingLicensesTotal);
                    }
                }

                for(zqu.zCharge thisCharge : thisChargeGroup.zCharges){
                    if (welcomeProductId.equals(thisProdId)){
                        if (thisCharge.ChargeObject != null) { // condition added to solve Mantis 916 issue
                            numLicensesWelcomePkg = (Decimal)thisCharge.ChargeObject.get('NbLicenses__c');
                            if ((Decimal)thisCharge.ChargeObject.get('NbLicenses__c') > productIdToInfoMap.get(thisProdId).NumLicMaxAllowed__c) {
                                //Throw an error
                                System.debug('Welcome Package # Licenses is higher than maximum allowed');
                                //log.description = 'Welcome Package # Licenses is higher than maximum allowed';
                                //logs.add(log);
                                return false;
                            }
                        }
                    } else {
                        if (thisCharge.ChargeObject != null) { // condition added to solve Mantis 916 issue
                            if(thisCharge.ChargeObject.get('NbLicenses__c') == null) {
                                numLicensesTotal += 0;
                            } else {
                                numLicensesTotal += (Decimal)thisCharge.ChargeObject.get('NbLicenses__c');
                                System.debug('numLicensesTotal: '+ numLicensesTotal);
                            }
                        }
                    }
                }
            }

            System.debug('addingLicensesTotal: '+ addingLicensesTotal);
            System.debug('refLicensesWelcomePkg: '+ refLicensesWelcomePkg);
            System.debug('numLicensesTotal: '+ numLicensesTotal);
            System.debug('numLicensesWelcomePkg: '+ numLicensesWelcomePkg);

            if (addingLicensesTotal > refLicensesWelcomePkg){
                System.debug('Adding Licenses total is greater than ref Licenses of Welcome package');
                System.debug('returning false');
                //log.description = 'Adding Licenses total is greater than ref Licenses of Welcome package';
                //logs.add(log);
                return false;
            }

            if (numLicensesTotal > numLicensesWelcomePkg) {
                System.debug('Total # Licenses in addons is greater than the # Licenses of Welcome package');
                System.debug('returning false');
                log.description = 'Total # Licenses in addons is greater than the # Licenses of Welcome package';
                logs.add(log);
                return false;
            }


            System.debug('End of quote ID IF statement');
        }
        System.debug('returning true');
        return true;
    }
    
    private static boolean checkQuoteHasField(zqu__Quote__c quote, String fieldToCheck) {
        String quoteAsString = JSON.serialize(quote);

        Map<String,Object> fieldToObjectMap = (Map<String,Object>) JSON.deserializeUntyped(quoteAsString);
         
        Set<String> fieldsAsStringSet = fieldToObjectMap.keyset().clone();

        if(fieldsAsStringSet.contains(fieldToCheck))
            return true;

        return false;
    }

  
    public static void runPriceRules(List<zqu.ZChargeGroup> chargeGroups,  List<zqu.ZQuoteRulesEngine.ChangeLog> logs){
          
        System.debug('In runPriceRules of ZuoraQuoteRulesEnginePlugin');

        zqu__Quote__c quote = null;

        if (!chargeGroups.isEmpty()) {
            quote = chargeGroups.get(0).quote;
            if (!Test.isRunningTest()) {
                List<String> productRatePlanIds = new List<String>();
                for (zqu.zChargeGroup thisChargeGroup : chargeGroups){
                  productRatePlanIds.add(thisChargeGroup.productRatePlanId);
                }
                System.debug('Quote.Nb_MW__c: '+quote.Nb_MW__c);

                List<zqu__ProductRatePlan__c> filteredPrps = [SELECT Id, Name, zqu__ZProduct__r.Entity__c FROM zqu__ProductRatePlan__c 
                                                            WHERE Id IN :productRatePlanIds
                                                            AND zqu__ZProduct__r.Entity__c =:'SOL'];

                System.debug('Retrieved '+filteredPrps.size()+' Product Rate Plans: '+filteredPrps);

                Set<String> filteredPrpSet = new Set<string>(); 
                for(zqu__ProductRatePlan__c thisRatePlan : filteredPrps){
                    filteredPrpSet.add(thisRatePlan.Id);
                }


                if (Test.isRunningTest()) {
                    for(zqu.zChargeGroup chargeGroup : chargeGroups ) {
                        filteredPrpSet.add(chargeGroup.productRatePlanId);
                    }
                    integer four = 0;
                    four++;
                    four++;
                    four++;
                    four++;
                }


                //Logic to calculate charge quantity assuming SOL rate plan
                for (zqu.zChargeGroup thisChargeGroup : chargeGroups){
                    List<SObject> listCharge = new List<SObject>();
                    Map<String, List<String>> changedFieldMap = new Map<String, List<String>>();
                    ////skip any charge groups associated to a rateplan not in our filtered list
                    if (!filteredPrpSet.contains(thisChargeGroup.productRatePlanId))
                        continue;

                    //now to loop through the charges
                    List<zqu.zCharge> charges = thisChargeGroup.zCharges;
                    for(zqu.zCharge thisCharge : charges){
                        SObject cp = null;
                        System.debug('thisCharge.UNIT_OF_MEASURE: '+thisCharge.UNIT_OF_MEASURE);
                        if ('MW'.equals(thisCharge.UNIT_OF_MEASURE)){
                            System.debug('quote.Nb_MW__c: '+quote.Nb_MW__c);
                            //
                            //thisCharge.fieldsChangedInRulesEngine.add('zqu__Quantity__c');
                            //
                            if (quote.Nb_MW__c == null) {
                                thisCharge.QUANTITY = '0';
                            } else {
                                thisCharge.QUANTITY = String.valueOf(quote.Nb_MW__c);
                            }
                            
                            System.debug('thisCharge.QUANTITY: '+thisCharge.QUANTITY);

                            if (!Test.isRunningTest()) {
                                //zqu.zQuoteUtil.calculateChargesOnQuantityChange(charges);
                                //zqu.zQuoteUtil.updateChargeGroup(thisChargeGroup);
                                cp = initChargeProxy(thisCharge);
                                
                                String productRatePlanChargeId = String.valueOf(cp.get('zqu__ProductRatePlanCharge__c'));
                                if(changedFieldMap.get(productRatePlanChargeId) == null){
                                    changedFieldMap.put(productRatePlanChargeId, new List<String>());
                                }

                                //zqu.zQuoteUtil.recalculatePriceFields(thisCharge, cp, zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY, '');
                                changedFieldMap.get(productRatePlanChargeId).add(zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY);
                            }
                        }

                        System.debug('Checking the stringcompare for User x MW: '+ 'User x MW'.equals(thisCharge.UNIT_OF_MEASURE));
                        if ('User x MW'.equals(thisCharge.UNIT_OF_MEASURE)){
                            //need the NbLicenses__c from this charge....
                            System.debug('thisCharge.ChargeObject.NbLicenses__c: '+thisCharge.ChargeObject.get('NbLicenses__c'));
                            Decimal numLicenses =(Decimal)thisCharge.ChargeObject.get('NbLicenses__c');
                            System.debug('numLicenses: '+ numLicenses);
                            Decimal newQuantity = 0;
                            if (numLicenses != null)
                                newQuantity = numLicenses * quote.Nb_MW__c;
                            System.debug('newQuantity: '+ newQuantity);
                            //
                            //thisCharge.fieldsChangedInRulesEngine.add('zqu__Quantity__c');
                            //
                            thisCharge.QUANTITY = String.valueOf(newQuantity);
                            if (!Test.isRunningTest()) {
                                //zqu.zQuoteUtil.calculateChargesOnQuantityChange(charges);
                                //zqu.zQuoteUtil.updateChargeGroup(thisChargeGroup);
                                cp = initChargeProxy(thisCharge);

                                String productRatePlanChargeId = String.valueOf(cp.get('zqu__ProductRatePlanCharge__c'));
                                if(changedFieldMap.get(productRatePlanChargeId) == null){
                                    changedFieldMap.put(productRatePlanChargeId, new List<String>());
                                }
                        
                                //zqu.zQuoteUtil.recalculatePriceFields(thisCharge, cp, zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY, '');
                                changedFieldMap.get(productRatePlanChargeId).add(zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY);
                            }

                            System.debug('thisCharge.QUANTITY: '+ thisCharge.QUANTITY);
                        }
                        listCharge.add(cp);
                    }
                    if(listCharge.get(0) != null){
                        system.debug('#### listCharge :'+listCharge);
                        system.debug('#### ChangeField :'+changedFieldMap);
                        //zqu.zQuoteUtil.updateChargeGroups(zcgs);
                        zqu.zQuoteUtil.updateZChargeGroupFromSObject(thisChargeGroup, listCharge, changedFieldMap, '');
                        //zqu.zQuoteUtil.calculateChargesOnEffectivePriceChange(zcg.zCharges);
                        zqu.zQuoteUtil.calculateChargesOnQuantityChange(thisChargeGroup.zCharges);
                        //zqu.zQuoteUtil.calculateChargesOnDiscountChange(zcg.zCharges);
                    }
                }
            }
        }   
    }


    
    public static zqu__QuoteRatePlanCharge__c initChargeProxy(zqu.zCharge zc) {
        // First: store both original zCharges and new ChargeProxies in parallel arrays.  We don't have a real ID
        // for zCharge, so this is a simple way of mapping them together (by array index) until we have a real ChargeProxy id below.
        zqu__QuoteRatePlanCharge__c proxies = new zqu__QuoteRatePlanCharge__c();
        //zqu.ZCharge[] charges = new zqu.ZCharge[0];

                zqu__QuoteRatePlanCharge__c cp = new zqu__QuoteRatePlanCharge__c(Name = zc.NAME);
                cp.Id = zc.Id; // Note: the zcharge id is null before it is inserted. May always be null here.
                cp.zqu__ChargeType__c = zc.CHARGE_TYPE;
                cp.zqu__Model__c = zc.MODEL;
                cp.zqu__Quantity__c = zc.isQuantityEditable ? Decimal.valueOf(zc.QUANTITY) : null;
                cp.zqu__EffectivePrice__c = zc.isEffectivePriceEditable ? Decimal.valueOf(zc.EFFECTIVE_PRICE) : null;
                cp.zqu__Discount__c = zc.isDiscountEditable ? Decimal.valueOf(zc.DISCOUNT) : null;
                cp.zqu__Total__c = zc.isTotalEditable ? Decimal.valueOf(zc.TOTAL) : null;
                cp.zqu__ProductRatePlanCharge__c = zc.PRODUCT_RATE_PLAN_CHARGE_SFDC_ID;
                cp.NbLicenses__c = (Decimal)zc.ChargeObject.get('NbLicenses__c');
                //cp.IsDiscountEditable__c = zc.isDiscountEditable;
                //cp.IsEffectivePriceEditable__c = zc.isEffectivePriceEditable;
                //cp.IsTotalEditable__c = zc.isTotalEditable;
                //cp.IsQuantityEditable__c = zc.isQuantityEditable;
                try {
                    cp.zqu__ListPrice__c = Decimal.valueOf(zc.LIST_PRICE);
                } catch (Exception e) {
                    cp.zqu__ListPrice__c = null;
                }
                try {
                    cp.zqu__ListTotal__c = Decimal.valueOf(zc.LIST_TOTAL);
                } catch (Exception e) {
                    cp.zqu__ListTotal__c = null;
                }
                cp.zqu__IncludedUnits__c = zc.IsIncludedUnitsEditable ? Decimal.valueOf(zc.INCLUDED_UNITS) : null;

                //cp.ChargeGroupId__c = zcg.groupID;
                //cp.Product__c = zcg.productId;
                //cp.ProductRatePlan__c = zcg.productRatePlanId;
                //cp.Quote__c = zcg.quote.Id;
                //cp.Opportunity__c = zcg.quote.zqu__Opportunity__c;
                //cp.Account__c = zcg.quote.zqu__Account__c;
                //cp.SortOrder__c = idx;

                proxies = cp;
                //charges.add(zc);

        return proxies;
    }
    




    
    //This runs before the zqu__ProductSelector page loads (WOOHOO!)
    //in VFRemoting 
    public static Map<String, List<String>> runProductRules(zqu__Quote__c quote, List<String> productRatePlanIds,  List<zqu.ZQuoteRulesEngine.ChangeLog> logs) {
        System.debug('In runProductRules of ZuoraQuoteRulesEnginePlugin');

        /* // for debug only
        for(zqu.zCharge thisCharge : thisChargeGroup.zCharges){
            if (thisCharge.ChargeObject != null) { // condition added to solve Mantis 916 issue
                Decimal numLicensesWelcomePkg = (Decimal)thisCharge.ChargeObject.get('NbLicenses__c');
                System.debug('###### INFO: numLicensesWelcomePkg = ' + numLicensesWelcomePkg); 
            }
        }
        */


        Map<String, List<String>> relatedRatePlanIdMap = new Map<String, List<String>>();
        relatedRatePlanIdMap.put(zqu.ZQuoteRulesEngine.PRODUCT_RULE_TYPE_ADDED_ENABLED, new List<String>());
        relatedRatePlanIdMap.put(zqu.ZQuoteRulesEngine.PRODUCT_RULE_TYPE_ADDED_DISABLED, new List<String>());
        relatedRatePlanIdMap.put(zqu.ZQuoteRulesEngine.PRODUCT_RULE_TYPE_REMOVED_ENABLED, new List<String>());

        if(Test.isRunningTest()){
            integer four = 0;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
            four++;
        }
        
        

        //*
        if (!Test.isRunningTest())
            ZuoraUpdateQuoteRatePlanCharge.updateZChargeGroups(quote, zqu.zQuoteUtil.getChargeGroups(quote.Id)); 
        //*/


        zqu.ZQuoteRulesEngine.ChangeLog changeLog = new zqu.ZQuoteRulesEngine.ChangeLog();

        changeLog.description = 'Ran logic to update quantity of charge groups based on Nb MW from Quote.';        
        logs.add (changeLog);

        System.debug('relatedRatePlanIdMap: '+relatedRatePlanIdMap);
        return relatedRatePlanIdMap;
    
    }
  
}