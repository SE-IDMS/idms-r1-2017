@isTest
public class AP_bFO_ELLAToInvokeOPSWebservice_TEST{
    /*public static testMethod void checkWithdrawalPushTest() {
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        insert offerRec;
        
        //Withdrawal Creation
        list<WithdrawalReference__c> withdrawal = new list<WithdrawalReference__c>();
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'XBTG5230'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2195M1271'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2220V21'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = '19203403V509001R'));
        insert withdrawal;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Withdrawal'));
        AP_bFO_ELLAToInvokeOPSWebservice.invokeOPSWebservice(String.valueOf(offerRec.Id));  
        
        Test.StopTest();  
    }*/
    public static testMethod void WithdrawalJobStatusInOPS() {
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        insert offerRec;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Status'));
        AP_bFO_ELLAToInvokeOPSWebservice.checkWithdrawalJobStatusInOPS(String.valueOf(offerRec.Id), 'Withdrawal');
        
        Test.StopTest();
    }
    public static testMethod void SubstitutionJobStatusInOPS() {
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        insert offerRec;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Status'));
        AP_bFO_ELLAToInvokeOPSWebservice.checkWithdrawalJobStatusInOPS(String.valueOf(offerRec.Id), 'Substitution');
        
        Test.StopTest();
    }
}