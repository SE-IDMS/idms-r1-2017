/**
* This is test class for IDMSPasswordRest class. 
**/

@isTest

private class IDMSPasswordRestTest{
    //This method tests set user password  
    static testMethod void setPasswordRest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/IDMSPassword';  
        req.httpMethod = 'Post';
        RestContext.request= req;
        RestContext.response= res;
        User userset = new User(alias = 'userset', email='userset' + '@accenture.com', 
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='userset' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userset ;
        Test.startTest();
        IDMSPasswordRest.doPost(userset.id,userset.FederationIdentifier ,'Welcome@123','idms','123456');
        IDMSPasswordRest.doPost('1234',null ,'Welcome@123','idms','123456');
        IDMSPasswordRest.doPost(null,'12345','Welcome@123','idms','123456');
        Test.stopTest(); 
        
    }
    //This method tests update user password.
    static testMethod void updatePasswordRest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/IDMSPassword';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        
        User userup = new User(alias = 'userup', email='userup' + '@accenture.com', 
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userup' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-update123',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert userup;
        Test.startTest();
        System.runAs(userup){
            system.setpassword(userup.id,'Welcome@123');
            IDMSPasswordRest.doPut('Welcome@123','Welcome@1234','idms');
            IDMSPasswordRest.doPut('Welcome@1234','Welcome@123','idms');
            IDMSPasswordRest.doPut('Welcome@1234','','idms');
        }
        IDMSPasswordRest.doPut('abc@test1234','','idms');
        Test.stopTest() ; 
    } 
    
    //This method tests password recovery of user. 
    static testMethod void recoveryPasswordRest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/IDMSPasswordRecovery';  
        req.httpMethod = 'Post';
        RestContext.request= req;
        RestContext.response= res;
        
        User userrec = new User(alias = 'userrec', email='userrec' + '@accenture.com', 
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='userrec' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-update123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMS_Profile_update_source__c='idms');
        
        insert userrec;
        
        User userRecord = new User(); 
        userRecord.email = userrec.email;
        userRecord.IDMS_Profile_update_source__c  = userrec.IDMS_Profile_update_source__c ; 
        User userRecord1 = new User(); 
        userRecord1.email = 'abc@abc.com';
        userRecord1.IDMS_Profile_update_source__c  = 'sample' ; 
        
        Test.startTest();
        IDMSPasswordRecoveryRest.doPost(userRecord);
        IDMSPasswordRecoveryRest.doPost(userRecord1);
        Test.stopTest(); 
        
    }
    //This method tests update user password. 
    static testmethod void updatePasswordFederationRest(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/IDMSPasswordRecovery';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        
        User userfed = new User(alias = 'userfed', email='userfed' + '@accenture.com', 
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='userfed' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-fed123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMS_Profile_update_source__c='idms');
        
        insert userfed;
        Test.startTest();
        IDMSPasswordRecoveryRest.doPut('Welcome@123',userfed.FederationIdentifier,'idms');
        IDMSPasswordRecoveryRest.doPut('abc@7854','78y934','idms');
        Test.stopTest(); 
        
    }
    
}