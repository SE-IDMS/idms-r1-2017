public with sharing class Handler_SubscriptionBeforeInsert{
    
    public static void UpdateReferenceQuote(List<Zuora__Subscription__c> newListSubs){
        
        Map<String,Zuora__Subscription__c> mapRefQIdTozSub = new Map<String,Zuora__Subscription__c>();
        
        for(Zuora__Subscription__c zSub:newListSubs){
            mapRefQIdTozSub.put(zSub.Zuora__Zuora_Id__c,zSub);
        }
        
        //Retrieve linked Quote with zqu__ZuoraSubscriptionID__c
        Map<Id,zqu__Quote__c> mapQuote = new Map<Id,zqu__Quote__c>([Select Id,ApprovalStatus__c,ReferenceQuote__c,Type__c,zqu__ZuoraSubscriptionID__c from zqu__Quote__c where zqu__ZuoraSubscriptionID__c IN :mapRefQIdTozSub.keySet() and Type__c = 'Regular']);
        list<zqu__Quote__c> listQuoteToUpdate;
        Map<Id,Id> mapIdRefQuotetoCurrentQuote = new Map<Id,Id>();

        if(mapQuote.size()>0){
            listQuoteToUpdate = new list<zqu__Quote__c>();
            for(zqu__Quote__c zq:mapQuote.values()){
                // If quote has no reference quote
                if(String.isBlank(zq.ReferenceQuote__c)){
                    zq.ApprovalStatus__c = 'Closed';
                    listQuoteToUpdate.add(zq);
                }else{
                    mapIdRefQuotetoCurrentQuote.put(zq.ReferenceQuote__c,zq.Id);
                }
            }
        }

        //Retrieve Reference Quotes
        Map<Id,zqu__Quote__c> mapRefQuote = new Map<Id, zqu__Quote__c>([Select Id,NbQuotesDone__c,IsLocked from zqu__Quote__c where Id IN :mapIdRefQuotetoCurrentQuote.keySet()]);

        if(mapRefQuote.size()>0){

            //Update Reference quotes
            for(zqu__Quote__c refQ:mapRefQuote.values()){
                //If zSub Version = 1
                if(mapRefQIdTozSub.get(mapQuote.get(mapIdRefQuotetoCurrentQuote.get(refQ.Id)).zqu__ZuoraSubscriptionID__c).Zuora__Version__c == 1){
                    //Add 1 to the RefQuote Number of quote already done
                    
                    refQ.NbQuotesDone__c = refQ.NbQuotesDone__c + 1;
                    listQuoteToUpdate.add(refQ);
                }
            }
        }

        System.debug('## listQuoteToUpdate : '+listQuoteToUpdate);
        if(listQuoteToUpdate != null){
            update listQuoteToUpdate;
        }

    }
}