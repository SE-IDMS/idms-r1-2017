Public class VFC_ReturnRequestSubmitForApproval
{ Public id XaId;
  Public boolean blnvisible{get;set;}
  Public RMA__c ObjRMA{get;set;}
  public string errormessage{get;set;}
  Public id containmentId{get;set;}
  Public map<id,String> mapStatusApproval;
  list<ProcessInstance> prcsinstance;
  list<RMA_Product__c> lstRIs = new List<RMA_Product__c>();
    public VFC_ReturnRequestSubmitForApproval(ApexPages.StandardController controller)
     {
     mapStatusApproval=new Map<id,String>();
     ObjRMA=new RMA__c();
     blnvisible=false;
     RMA__c rma=new RMA__c();
     rma= (RMA__c )controller.getRecord();
     
     ObjRMA=[select id,Status__c,TECH_CheckReturnAddressOnRI__c, Approver__c from RMA__c where Id=:rma.id]; 
      lstRIs = [Select id, RMA__c from RMA_Product__c where RMA__c =: rma.id ];
    // lstEntityStake = [select id,Role__c,User__c,BusinessRiskEscalationEntity__c from EntityStakeholder__c where Role__c=:Label.CLSEP12I2P85  and BusinessRiskEscalationEntity__c=:ObjCA.AccountableOrganization__c and User__c!=null];
   prcsinstance=[SELECT Id,IsDeleted,LastModifiedById,LastModifiedDate,Status,SystemModstamp,TargetObjectId FROM ProcessInstance where TargetObjectId =:ObjRMA.Id and status='Pending'];    
      
     }

  Public Pagereference processDetails()
  {
  
     Pagereference pr;
        If(prcsinstance.size()==0)
        {
        System.debug('Status-- >'+ObjRMA.Status__c);
        System.debug('Status-- >'+ObjRMA.Status__c);
            if(ObjRMA.Status__c == Label.CLDEC12RR24)  // If status = validated
                {
                
                    errormessage=Label.CLDEC12RR18;
                     // This customer containment action has already been approved,you can't submit it for approval.
                    pr = null;
                    return pr;
                }
                if(lstRIs.Size()==0)
             {
                 errormessage=Label.CLDEC12RR20;
                // Should have atleast one return Item.
                pr = null;
                return pr;
             }
            if(ObjRMA.TECH_CheckReturnAddressOnRI__c == null)
                {
                  errormessage=Label.CLDEC12RR19;
                  pr = null;
                  return pr;
                }
             
             
             if ( ObjRMA.Status__c == Label.CLDEC12RR23)   // If status = open
             {
                 blnvisible=true;
                 errormessage=Label.CLDEC12RR21;
                 pr = null;
                 return pr;
             }
             else
             {
             blnvisible=false;
                 errormessage=Label.CLDEC12RR30;
                 pr = null;
                 return pr;
             }
        
        }
        else
        {
            blnvisible=false;
            errormessage= Label.CLDEC12RR22;
            //This Return Request is already submitted for approval.
            pr = null;
        }
        
        return pr;
}
   Public PageReference  doApprove()
   {   
            Update ObjRMA;
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ObjRMA.id);
            req1.setNextApproverIds(new Id[] {ObjRMA.Approver__c});
            Approval.ProcessResult result = Approval.process(req1);
            string strredirectURl='/'+ObjRMA.id;
            PageReference pr = new PageReference(strredirectURl);
           pr.setRedirect(true);
     return pr;
     
     
   }
   Public Pagereference  doCancel()
   {
   PageReference pr = new PageReference('/'+ObjRMA.id);
   pr.setRedirect(true);
     return pr;
    
  
   }
}