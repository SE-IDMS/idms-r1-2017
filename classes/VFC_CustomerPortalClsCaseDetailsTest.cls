@isTest
public class VFC_CustomerPortalClsCaseDetailsTest {

public static testMethod void VFC_CustomerPortalClsCaseDetails() {
    
//Set up all conditions for testing.
    PageReference pageRef = Page.VFP_CustomerPortalClsCaseDetails;
     //ApexPages.VFP_CustomerPortalCls pageRef = new ApexPages.VFP_CustomerPortalCls; 
    Test.setCurrentPage(pageRef);
        
    VFC_CustomerPortalClsCaseDetails controller = new VFC_CustomerPortalClsCaseDetails(); 
    PageReference nextPage = controller.getCaseDetailView();
    
    // Verify that page fails without parameters
       System.assertEquals(null, nextPage);         
        
    account a = new account(name = 'name');
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345');
    
    controller.setusid(UserInfo.getUserId());
    User userr = [Select FirstName, LastName, Email from User where Id =: controller.getUsid() LIMIT 1][0];
    
    //Id accid = [Select AccountId from Contact where Id =: controller.getContid()].AccountId;
    //Contact con = [Select WorkPhone__c from Contact where Id =: c.id];
    //Account acc = [Select Name from Account where Id =: a.id];
    String s2 = controller.getContactPhone();
    String s1 = controller.getAccountName();
    
    
    
    controller.setcontid(c.id);
    controller.setaccid(a.id);

    case case1 = new case(ContactId=c.id, subject='case1', status='New');    
    insert(case1);
    CaseComment comt1 = new CaseComment(ParentId=case1.id, CommentBody='My Comment body1', IsPublished=true);
    CaseComment comt2 = new CaseComment(ParentId=case1.id, CommentBody='My Comment body2', IsPublished=false);

    insert(comt1); //insert(comt2);
    
    

    //System.assertEquals(UserInfo.getUserId(), controller.getusid());
    System.assertEquals(c.id, controller.getcontid());
    System.assertEquals(a.id, controller.getaccid());

    controller.setCaseId(case1.id);
    
//    case caseDetails1 = [SELECT CaseNumber, Subject, SubSymptom__c, SoftwareVersion__c, Status, SupportCategory__c, CustomerRequest__c, AnswerToCustomer__c, IsClosed FROM Case WHERE  Id =: case1.id];
    case caseDetails1 = [SELECT CaseNumber, Subject, family__c, Operating_System__c, SoftwareVersion__c, Status, SupportCategory__c, Product_Type__c, CustomerRequest__c, AnswerToCustomer__c, IsClosed, ActionNeededFrom__c FROM Case WHERE  Id =: Case1.Id]; 

    List<caseComment> caseComments1 = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC];
    List<Attachment> attachments1 = [SELECT Id, CreatedById, Name, CreatedDate, BodyLength, ContentType, Description, IsPrivate, LastModifiedDate FROM Attachment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC];

    casecomment cc = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC][0]; 
    List<caseComment> caseCommentsOK = new List<caseComment>();
    caseCommentsOK.add(cc);

    
    // Testing Begin Here    
    Test.starttest(); 

    controller.getIsAttachListEmpty();
    controller.setIsAttachListEmpty(true);
    controller.getIsCaseClosedd();
    controller.setIsCaseClosedd(false);
    controller.getCaseId();
    controller.getUserContact();
    controller.getUserAcc();
    controller.getUserr();    
    controller.setContactPhone('+33 170787878');
    controller.setAccountName('Schneider');    
    controller.getProductType();
    controller.getOperatingSystem();
    controller.getSoftwareVersion();
    controller.getAnswerToCustomer();        
    controller.setCaseDetail(caseDetails1);
    controller.setCaseComments(caseComments1);   
    controller.setAttachments(attachments1);
    
    System.assertEquals(controller.getCaseDetail(), caseDetails1);
    System.assertEquals(controller.getCaseComments(), caseComments1);
    System.assertEquals(controller.getCaseComments(), caseCommentsOK);
    System.assertEquals(controller.getAttachments(), Attachments1);       
    
    Test.stoptest(); 
    
}


}