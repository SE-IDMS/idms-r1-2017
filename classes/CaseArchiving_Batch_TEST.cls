@isTest
private class CaseArchiving_Batch_TEST{
    
    static testmethod void attachmentBatchScheduleTestmethod() {
        Test.startTest();
        String CRON_EXP = '0 0 0 1 1 ? 2025';  
        String jobId = System.schedule('Schdule casearchiving_Batch', CRON_EXP, new casearchiving_BatchSchedule() );
        Test.stopTest();
    }
    
    static testMethod void archvieBatchtestmethod(){
    
        List<CS_ForFieldMapping__c>  cslist = new List<CS_ForFieldMapping__c>();
        List<CS_NewFieldMapping__c> newcslist = new List<CS_NewFieldMapping__c>();
        
        CS_ForFieldMapping__c obj1= new CS_ForFieldMapping__c();
        obj1.name='Test_1';
        obj1.SourceObject__c ='Case';
        obj1.TargetObject__c = 'ArchiveCase__c';
        obj1.SourceField__c = 'AnswerToCustomer__c';
        obj1.TargetField__c = 'AnswerToCustomer__c';
        cslist.add(obj1);
        
        CS_ForFieldMapping__c obj2= new CS_ForFieldMapping__c();
        obj2.name='Test_2';
        obj2.SourceObject__c ='Case';
        obj2.TargetObject__c = 'ArchiveCase__c';
        obj2.SourceField__c = 'AssetId';
        obj2.TargetField__c = 'Asset__c';
        cslist.add(obj2);
        
        CS_ForFieldMapping__c obj3= new CS_ForFieldMapping__c();
        obj3.name='Test_3';
        obj3.SourceObject__c ='Case';
        obj3.TargetObject__c = 'ArchiveCase__c';
        obj3.SourceField__c = 'AssetOwner__c';
        obj3.TargetField__c = 'AssetOwner__c';
        cslist.add(obj3);
        
        CS_ForFieldMapping__c obj4= new CS_ForFieldMapping__c();
        obj4.name='Test_4';
        obj4.SourceObject__c ='Case';
        obj4.TargetObject__c = 'ArchiveCase__c';
        obj4.SourceField__c = 'CallbackPhone__c';
        obj4.TargetField__c = 'CallbackPhone__c';
        cslist.add(obj4);
        
        CS_ForFieldMapping__c obj5= new CS_ForFieldMapping__c();
        obj5.name='Test_5';
        obj5.SourceObject__c ='Case';
        obj5.TargetObject__c = 'ArchiveCase__c';
        obj5.SourceField__c = 'CampaignKeycode__c';
        obj5.TargetField__c = 'CampaignKeycode__c';
        cslist.add(obj5);
        
        CS_ForFieldMapping__c obj6= new CS_ForFieldMapping__c();
        obj6.name='Test_6';
        obj6.SourceObject__c ='Case';
        obj6.TargetObject__c = 'ArchiveCase__c';
        obj6.SourceField__c = 'CaseNumber';
        obj6.TargetField__c = 'Name';
        cslist.add(obj6);
       
        CS_ForFieldMapping__c obj7= new CS_ForFieldMapping__c();
        obj7.name='Test_7';
        obj7.SourceObject__c ='Case';
        obj7.TargetObject__c = 'ArchiveCase__c';
        obj7.SourceField__c = 'OwnerId';
        obj7.TargetField__c = 'OwnerId';
        cslist.add(obj7);
            
        CS_ForFieldMapping__c obj11= new CS_ForFieldMapping__c();
        obj11.name='Test_11';
        obj11.SourceObject__c ='CaseStage__c';
        obj11.TargetObject__c = 'ArchiveCaseStage__c';
        obj11.SourceField__c = 'BusinessHours__c';
        obj11.TargetField__c = 'BusinessHours__c';
        cslist.add(obj11);
        
        CS_ForFieldMapping__c obj12= new CS_ForFieldMapping__c();
        obj12.name='Test_12';
        obj12.SourceObject__c ='CaseStage__c';
        obj12.TargetObject__c = 'ArchiveCaseStage__c';
        obj12.SourceField__c = 'Name';
        obj12.TargetField__c = 'Name';
        cslist.add(obj12);
        
        CS_ForFieldMapping__c obj13= new CS_ForFieldMapping__c();
        obj13.name='Test_13';
        obj13.SourceObject__c ='InquiraFAQ__c';
        obj13.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj13.SourceField__c = 'InfoCenterURL__c';
        obj13.TargetField__c = 'InfoCenterURL__c';
        cslist.add(obj13);
        
        CS_ForFieldMapping__c obj14= new CS_ForFieldMapping__c();
        obj14.name='Test_14';
        obj14.SourceObject__c ='InquiraFAQ__c';
        obj14.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj14.SourceField__c = 'Name';
        obj14.TargetField__c = 'Name';
        cslist.add(obj14);
        
        CS_ForFieldMapping__c obj15= new CS_ForFieldMapping__c();
        obj15.name='Test_15';
        obj15.SourceObject__c ='InquiraFAQ__c';
        obj15.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj15.SourceField__c = 'URL__c';
        obj15.TargetField__c = 'URL__c';
        cslist.add(obj15);
        
        CS_ForFieldMapping__c obj16= new CS_ForFieldMapping__c();
        obj16.name='Test_16';
        obj16.SourceObject__c ='CaseComment';
        obj16.TargetObject__c = 'ArchiveCaseStage__c';
        obj16.SourceField__c = 'CommentBody';
        obj16.TargetField__c = 'CommentBody__c';
        cslist.add(obj16);
        
        CS_ForFieldMapping__c obj17= new CS_ForFieldMapping__c();
        obj17.name='Test_17';
        obj17.SourceObject__c ='CaseComment';
        obj17.TargetObject__c = 'ArchiveCaseStage__c';
        obj17.SourceField__c = 'IsPublished';
        obj17.TargetField__c = 'IsPublished__c';
        cslist.add(obj17);
        
        CS_ForFieldMapping__c obj18= new CS_ForFieldMapping__c();
        obj18.name='Test_18';
        obj18.SourceObject__c ='CaseComment';
        obj18.TargetObject__c = 'ArchiveCaseStage__c';
        obj18.SourceField__c = 'CreatedById';
        obj18.TargetField__c = 'CreatedBy__c';
        cslist.add(obj18);
        
        
        CS_ForFieldMapping__c obj19= new CS_ForFieldMapping__c();
        obj19.name='Test_19';
        obj19.SourceObject__c ='EmailMessage';
        obj19.TargetObject__c = 'ArchiveEmailMessage__c';
        obj19.SourceField__c = 'FromName';
        obj19.TargetField__c = 'FromName__c';
        cslist.add(obj19);
        
        CS_ForFieldMapping__c obj20= new CS_ForFieldMapping__c();
        obj20.name='Test_20';
        obj20.SourceObject__c ='EmailMessage';
        obj20.TargetObject__c = 'ArchiveEmailMessage__c';
        obj20.SourceField__c = 'Status';
        obj20.TargetField__c = 'Status__c';
        cslist.add(obj20);
        
        
        CS_ForFieldMapping__c obj21= new CS_ForFieldMapping__c();
        obj21.name='Test_21';
        obj21.SourceObject__c ='EmailMessage';
        obj21.TargetObject__c = 'ArchiveEmailMessage__c';
        obj21.SourceField__c = 'Subject';
        obj21.TargetField__c = 'Subject__c';
        cslist.add(obj21);
        
        CS_ForFieldMapping__c obj22= new CS_ForFieldMapping__c();
        obj22.name='Test_22';
        obj22.SourceObject__c ='SVMXC__Case_Line__c';
        obj22.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj22.SourceField__c = 'SVMXC__Entitled_Exchange_Type__c';
        obj22.TargetField__c = 'SVMXC_Entitled_Exchange_Type__c';
        cslist.add(obj22);
        
        CS_ForFieldMapping__c obj23= new CS_ForFieldMapping__c();
        obj23.name='Test_23';
        obj23.SourceObject__c ='SVMXC__Case_Line__c';
        obj23.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj23.SourceField__c = 'SVMXC__Entitlement_History__c';
        obj23.TargetField__c = 'SVMXC_Entitlement_History__c';
        cslist.add(obj23);
        
        CS_ForFieldMapping__c obj24= new CS_ForFieldMapping__c();
        obj24.name='Test_24';
        obj24.SourceObject__c ='SVMXC__Case_Line__c';
        obj24.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj24.SourceField__c = 'SVMXC__Line_Status__c';
        obj24.TargetField__c = 'SVMXC_Line_Status__c';
        cslist.add(obj24);
        
        CS_ForFieldMapping__c obj30= new CS_ForFieldMapping__c();
        obj30.name='Test_30';
        obj30.SourceObject__c ='CSE_ExternalReferences__c';
        obj30.TargetObject__c = 'ArchiveCSE_ExternalReferences__c';
        obj30.SourceField__c = 'BackOfficeSystem__c';
        obj30.TargetField__c = 'BackOfficeSystem__c';
        cslist.add(obj30);
        
        insert cslist;
        
        CS_NewFieldMapping__c nobj1 = new CS_NewFieldMapping__c();
        nobj1.name = 'Test_25';
        nobj1.SourceObject__c = 'Case';
        nobj1.TargetObject__c = 'ArchiveCase__c';
        nobj1.SourceField__c = 'ACC_ClassLevel1__c';
        nobj1.TargetField__c = 'ACC_ClassLevel1__c';
        newcslist.add(nobj1);
        
        CS_NewFieldMapping__c nobj2 = new CS_NewFieldMapping__c();
        nobj2.name = 'Test_26';
        nobj2.SourceObject__c = 'Case';
        nobj2.TargetObject__c = 'ArchiveCase__c';
        nobj2.SourceField__c = 'ACC_PreferredAgent__c';
        nobj2.TargetField__c = 'ACC_PreferredAgent__c';
        newcslist.add(nobj2);
        
        CS_NewFieldMapping__c nobj3 = new CS_NewFieldMapping__c();
        nobj3.name = 'Test_27';
        nobj3.SourceObject__c = 'Case';
        nobj3.TargetObject__c = 'ArchiveCase__c';
        nobj3.SourceField__c = 'TECH_GMRFamily__c';
        nobj3.TargetField__c = 'TECH_GMRFamily__c';
        newcslist.add(nobj3);
        
        CS_NewFieldMapping__c nobj4 = new CS_NewFieldMapping__c();
        nobj4.name = 'Test_28';
        nobj4.SourceObject__c = 'Case';
        nobj4.TargetObject__c = 'ArchiveCase__c';
        nobj4.SourceField__c = 'ActualAge__c';
        nobj4.TargetField__c = 'ActualAge__c';
        nobj4.Flag__c = true;
        newcslist.add(nobj4);
        
        CS_NewFieldMapping__c nobj5 = new CS_NewFieldMapping__c();
        nobj5.name = 'Test_29';
        nobj5.SourceObject__c = 'Case';
        nobj5.TargetObject__c = 'ArchiveCase__c';
        nobj5.SourceField__c = 'PCRExternalReferenceCount__c';
        nobj5.TargetField__c = 'PCRExternalReferenceCount__c';
        nobj5.Flag__c = true;
        newcslist.add(nobj5);
        
        insert newcslist;
        
        CS_Severity__c sevobj = new CS_Severity__c();
        sevobj.value__c=7;
        sevobj.name='test CS';
        insert sevobj;


        Account accountObj = Utils_TestMethods.createAccount();
        Database.insert(accountObj);
             
        contact contactObj=Utils_TestMethods.createContact(accountObj.Id,'testContact');
        Database.insert(contactObj);     
        
        Country__c countryObj = Utils_TestMethods.createCountry();
        Database.insert(countryObj);
        
        OPP_Product__c objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        Database.insert(objProduct1);
        
        Case caseObj=Utils_TestMethods.createCase(accountObj.id,contactObj.id,'closed');
        caseObj.Family_lk__c = objProduct1.Id;
        Database.insert(caseObj);
        
        //Creating System user
        User TestUser = Utils_TestMethods.createStandardUser('testing');
        TestUser.BypassTriggers__c = 'AP01;AP03;AP10;AP16;AP11;AP1000;AP1000_1;AP1000_2;AP1000_3;AP1001;AP1002;AP1003;AP1004;AP43;AP_CustomerSafetyIssue;AP_Case_CheckOpenCCCActions;AP_Case_CreateCaseActions;AP_ChatterForCCCAction;AP_ContractTriggerUtils;AP_TemplateTriggerUtils;AP_UpdateCCCActionAge;SVMX06;SVMX07;SVMX05;AP54;AP_Opp_UpdateIncludeInForecastField;AP_Opp_UpdateParentCloseDate;AP_WorkOrderTechnicianNotification;AP_OppBusinessMixUpdate;AP_Case_CaseHandlerCaseAfterUpdate;AP_Case_CaseHandlerCaseBeforeInsert;AP_Case_CaseHandlerCaseBeforeUpdate;AP_InvolvedOrganization;AP_IO_AI;AP_ITB_Assets_PrimaryAssetCheck;AP_NewRIAM;AP_RI_FieldUpdate;AP_RR_FieldUpdate;SVMX08;AP_CustomerSafetyIssue_After;AP_OpportunityTeamMember;SVMX09;AP_ORFNotification;SRV01;AP_SendOpportunityNotification;SVMX_InstalledProduct;SVMX_InstalledProduct2;AP_LCR;AP_LCR1;AP08;AP_CaseRelatedProduct_Handler;AP_Case_MajorIssue;AP_TechnicalExpertAssessment;AP_RI_RMAMailMerge;AP_CIS_ISRHandler;AP_CIS_InterventionCostHandler;SVMX10;SVMX22;SRV03;SRV_APR_001;SRV_APR_002;SRV04;SRV07;SVMX11;SVMX12;SVMX14;SVMX16;SVMX18;SVMX19;SVMX20;SVMX21;AP_ProblemComplaintRequestLink;AP_TEXConfirmCheck;ProblemCaseLinkAfterDelete;ProblemCaseLinkAfterInsert;RMAAtferUpdate;AP_LCR2;CRAfterDelete;CRAfterInsert;RMAAtferDelete;RMAAtferInsert;TEX_AI;AP_CareTeamBeforeUpdateHandler;SVMX01;AP_Case_CaseHandler;CaseAfterInsert;CaseAfterUpdate;CaseBeforeInsert;CaseBeforeUpdate;AP_CRComments;ITB_AssetsAfterUpdate';
        insert TestUser;
        System.runAs(TestUser){
       
        /*
       
        
        Lead lead1 = Utils_TestMethods.createLead(null, countryObj.Id,100);
        insert lead1;
        
        CustomerLocation__c custlocobj=  Utils_TestMethods.createCustomerLocation(accountObj.id,countryObj.Id);
        Database.insert(custlocobj);
        
        
        CCCAction__c cccactionobj= Utils_TestMethods.createCCCAction(caseObj.Id);
        Database.insert(cccactionobj);
       
       
        ITB_Asset__c itbassetobj= Utils_TestMethods.createITBAsset(caseObj.Id);
        Database.insert(itbassetobj);
       
        LiteratureRequest__c literaturereqObj=Utils_TestMethods.createLiteratureRequest(accountObj.Id, contactObj.id,caseObj.Id);
        Database.insert(literaturereqObj);
        
        Opportunity oppobj=Utils_TestMethods.createOpportunity(accountObj.id);
        oppobj.ReferenceCase__c=caseObj.id;
        Database.insert(oppobj);
        
        EmailMessage[] emailObj = new EmailMessage[0];
         emailObj.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = True, ToAddress= 'anil.budha@non.schneider-electric.com', Subject = 'Test email', TextBody = '23456 ', ParentId =caseObj.id )); 
        Database.insert(emailObj);
        
        CSE_RelatedProduct__c cseRelProdObj = new CSE_RelatedProduct__c();
        cseRelProdObj.Case__c=caseObj.id;
        Database.insert(cseRelProdObj); 
        
        
        */
        
        
        
        Test.startTest();
        
        Lead lead1 = Utils_TestMethods.createLead(null, countryObj.Id,100);
        insert lead1;
        
        CaseComment cseComentObj = new CaseComment(ParentId = caseObj.id,IsPublished = false);
        Database.insert(cseComentObj);
             
        CaseStage__c casestageobj = new casestage__c();
        casestageobj.case__c=caseObj.id;
        Database.insert(casestageobj);
         
        CSE_L2L3InternalComments__c internalobj = new CSE_L2L3InternalComments__c();
        internalobj.Case__c=caseObj.id;
        Database.insert(internalobj);
        
        PS_OLH__Chat__c chatObj= new PS_OLH__Chat__c();
        chatObj.PS_OLH__Case__c=caseObj.id;
        chatObj.PS_OLH__Account__c=accountObj.Id;
        chatObj.PS_OLH__Contact__c=contactObj.id;
        Database.insert(chatObj);
        
        PS_OLH__Mail__c mailObj=new PS_OLH__Mail__c();
        mailObj.PS_OLH__Account__c=accountObj.Id;
        mailObj.PS_OLH__Contact__c=contactObj.id;
        mailObj.PS_OLH__Lead__c=lead1.id;
        mailObj.PS_OLH__Case__c=caseObj.id;
        Database.insert(mailObj);
        
        /*
        InquiraFAQ__c inquiraobj=Utils_TestMethods.createFAQ(caseObj.Id);
        Database.insert(inquiraobj);
        */
            
        RMA__c rmaObj = new RMA__c();
        rmaObj.AccountName__c=accountObj.Id;
        rmaObj.ContactName__c=contactObj.Id;
        rmaObj.Case__c=caseObj.Id;
        rmaObj.ProductCondition__c='TestPC';
        rmaObj.CustomerResolution__c='TestRes';
        rmaObj.AccountCity__c='TestCity';
        rmaObj.AccountZipCode__c='1111';
        rmaObj.AccountCountry__c=countryObj.Id;
        Database.insert(rmaObj);
        
        
        
        CustomerSafetyIssue__c saftyobj =  new CustomerSafetyIssue__c();
        saftyobj.AffectedCustomer__c=accountObj.Id;
        saftyobj.CountryOfOccurrence__c=countryObj.id;
        saftyobj.RelatedbFOCase__c=caseObj.id;
        Database.insert(saftyobj);
       
        SVMXC__Site__c  loc = New SVMXC__Site__c(SVMXC__Street__c='Add Lin 1',
                    AddressLine2__c='Add Line 2',
                    SVMXC__City__c='Bangalore',
                    HealthAndSafety__c='Drug Test',
                    SVMXC__Account__c=accountObj.id,
                    SVMXC__Zip__c='1234'); 
        Database.insert(loc);           
        WorkOrderNotification__c won1=   new WorkOrderNotification__c(
            Location__c=loc.id,
            InstalledAtAccount__c = accountObj.id,
            Work_Order_Status__c='open',
            ContactLastName__c='WNContact',
            ContactPhoneNumber__c='984456565',
            Scheduled_Date_Time__c=system.now(), 
            ServicesBusinessUnit__c='ITB',
            Case__c=caseObj.id );
        insert won1;
        
        CSE_RelatedProduct__c cseRelProdObj = new CSE_RelatedProduct__c();
        cseRelProdObj.Case__c=caseObj.id;
        Database.insert(cseRelProdObj); 
        
        Group testGroup2 = new Group(Name='CIS Team 2',Email='testEmail2@schneider-electric.com', type='Queue');
        insert testGroup2;
        
        List<QueueSObject> queuesObjectList = new  List<QueueSObject>{
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_CorrespondentsTeam__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_ISR__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_CountryListPrice__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_FieldServiceRepresentative__c'),
                    new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_ServiceCenter__c')};

        System.runAs (new User(Id = UserInfo.getUserId())) {
            insert queuesObjectList;
        } 
        
        Country__c countryInstance1 = new Country__c();
        countryInstance1.Name = 'India';
        countryInstance1.CountryCode__c ='IN';
        insert countryInstance1;

        CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance2 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance2.Name ='CIS Team 2';
        cisCorrespondentTeamInstance2.OwnerId = testGroup2.Id; 
        cisCorrespondentTeamInstance2.CIS_FieldsOfActivity__c='ED: Medium Voltage';
        cisCorrespondentTeamInstance2.CIS_ScopeOfCountries__c='India';
        insert cisCorrespondentTeamInstance2;
        
        CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR';
            isrInstance.CIS_FieldOfActivity__c='ED: Low Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance1.Id;
            isrInstance.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrInstance.Case__c = caseObj.id;
        insert isrInstance;
        
        Literaturerequest__c ltr = Utils_TestMethods.createLiteratureRequest(accountObj.Id, contactObj.Id, caseObj.id);
        insert ltr;
        
        Opportunity opportunity = Utils_TestMethods.createOpportunity(accountObj.id);
        opportunity.OpptyPriorityLevel__c='standard';
        opportunity.OpptyType__c = 'Standard'; 
        opportunity.ReferenceCase__c = caseObj.id;
        insert opportunity;
        
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(accountObj.id);
        workOrder.SVMXC__Contact__c = contactObj.id;    
        Database.insert(workOrder);
        
        OpportunityNotification__c opportunityNotif = Utils_TestMethods.createOpportunityNotification(workOrder.id);
        opportunityNotif.Name = 'Test Opp';
        opportunityNotif.case__c=caseObj.id;
        opportunityNotif.OpportunityScope__c ='BDSL3';
        opportunityNotif.AccountForConversion__c =accountObj.id;
        opportunityNotif.CountryDestination__c=countryObj.id;
        opportunityNotif.RejectionReason__c='null';
        opportunityNotif.OpportunityOwner__c=null;
        opportunityNotif.Amount__c=null;
        Database.insert(opportunityNotif);
        
        ITB_Asset__c objAsset = Utils_TestMethods.createITBAsset(caseObj.id);
        objAsset.Address__c  = 'TestAddress';
        objAsset.AssetComments__c  = 'TestComments';
        objAsset.City__c  = 'TestCity';
        objAsset.CommercialReference__c  = 'BK500';
        objAsset.LegacyAccountID__c  = '1-242343';
        objAsset.LegacyAccountName__c  = 'TestAccount';
        objAsset.LegacyAssetId__c  = '1-22222';
        objAsset.LegacyAssetNumber__c  = '1-22222';
        objAsset.LegacyName__c  = 'IT_SB';
        objAsset.SerialNumber__c  = 'SFSEDFSDFDFDS';
        objAsset.StateProvince__c  = 'TESTSTATE';
        objAsset.TECH_AssetRefrenceLinkID__c  = '34545345345';
        objAsset.PrimaryFlag__c  = TRUE;
        objAsset.ZipCode__c ='342342';
        insert objAsset;
        
        
    /*
        WorkOrderNotification__c workordernotiobj=Utils_TestMethods.createWorkOrderNotification(caseObj.id,custlocobj.id);
        Database.insert(workordernotiobj);

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
                    
            
        BusinessRiskEscalations__c bresObj = Utils_TestMethods.createBusinessRiskEscalation(accountObj.Id,countryObj.id,u.Id);
        Database.Insert(bresObj); 
        
         BusinessRiskEscalationCaseLink__c breobj= new BusinessRiskEscalationCaseLink__c();
         breobj.BusinessRisk__c=bresObj.id;
         breobj.Case__c=caseObj.id;
         insert breobj;
         
        Product2 prod = new Product2( ProductCode='UTProd001',
                Name='UTProd001',
                Family='Oncology',
                SVMXC__Product_Line__c='Desktop',
                IsActive=true);
        insert prod;
        Profile p1 = [select id from profile where name = 'System Administrator' limit 1];
        User newUser1 = new User(alias = 'alias1', 
                                email = 'maxexpress@servicemax.com', 
                                emailencodingkey = 'UTF-8', 
                                lastname = 'newuserTest1',
                                languagelocalekey = 'en_US', 
                                localesidkey = 'en_US', 
                                profileid = p1.id, 
                                timezonesidkey = 'America/Los_Angeles', 
                                username = 'svmx1@testorg.com'
                                //UserRoleId= ur.Id, 
                                );

        insert newUser1;
        SVMXC__Territory__c ter1=new  SVMXC__Territory__c(Name ='pter');  
        insert  ter1;  
        SVMXC__Service_Group__c st1= new SVMXC__Service_Group__c(
                     Name='St1',
                     SVMXC__Active__c=true,
                     SVMXC__Group_Type__c='Internal',
                     SVMXC__Group_Code__c='test213');
            insert st1;
        
        SVMXC__Service_Group_Members__c tech1 = Utils_TestMethods.createTech_Equip(newUser1.Id,st1.Id);
            tech1.recordtypeid = Label.CLDEC12SRV39;
        insert tech1;
        SVMXC__Site__c  loc = New SVMXC__Site__c(SVMXC__Street__c='Add Lin 1',
                    AddressLine2__c='Add Line 2',
                    SVMXC__City__c='Bangalore',
                    HealthAndSafety__c='Drug Test',
                    SVMXC__Account__c=accountObj.id,
                    SVMXC__Zip__c='1234'); 
        Database.insert(loc);           
        WorkOrderNotification__c won1=   new WorkOrderNotification__c(
            Location__c=loc.id,
            Work_Order_Status__c='open',
            ContactLastName__c='WNContact',
            ContactPhoneNumber__c='984456565',
            Scheduled_Date_Time__c=system.now(), 
            ServicesBusinessUnit__c='ITB',
            Case__c=caseObj.id );
        insert won1;
        
        
        SVMXC__Service_Order__c WO = new SVMXC__Service_Order__c(                                        
            SVMXC__Company__c=accountObj.Id,
            SVMXC__Order_Status__c = 'Open', 
            BackOfficeReference__c='123',  
            SVMXC__Product__c = prod.id,
            SVMXC__Case__c=caseObj.id,
            SVMXC__Contact__c = contactObj.Id, 
            SVMXC__Primary_Territory__c=ter1.id,
            RescheduleReason__c='Weather Related',
            Service_Business_Unit__c='Energy',
            Parent_Work_Order__c = null,
            SVMXC__Group_Member__c=tech1.id,
            Work_Order_Notification__c=won1.id,
            Is_Billable__c = true,
            SVMXC__Locked_By_DC__c=true,
            Customer_Service_Request_Time__c= system.now());
        insert WO;
        
        
        
        LiveChatTranscript transcriptObj = new LiveChatTranscript();
        transcriptObj.caseID=caseObj.id;
        transcriptObj.LeadID=lead1.id;
        Database.insert(transcriptObj);
       
        Problem__c problemobj = Utils_TestMethods.createProblem(lCROrganzObj.id);
        Database.insert(problemobj);
        
        ProblemCaseLink__c problemcaseobj = new ProblemCaseLink__c();
        problemcaseobj.case__c = caseObj.id;
        problemcaseobj.problem__c = problemobj.id;
        database.insert(problemcaseobj);

    */
   
        
        CaseArchiving_Batch capb = new CaseArchiving_Batch(); 
        ID batchprocessid =Database.executeBatch(capb);
        Test.stopTest();
        }
    
    }
    static testMethod void archvieBatchtestmethodOne(){
    
         List<CS_ForFieldMapping__c>  cslist = new List<CS_ForFieldMapping__c>();
         List<CS_NewFieldMapping__c> newcslist = new List<CS_NewFieldMapping__c>();
    
        CS_ForFieldMapping__c obj1= new CS_ForFieldMapping__c();
        obj1.name='Test_1';
        obj1.SourceObject__c ='Case';
        obj1.TargetObject__c = 'ArchiveCase__c';
        obj1.SourceField__c = 'AnswerToCustomer__c';
        obj1.TargetField__c = 'AnswerToCustomer__c';
        cslist.add(obj1);
        
        CS_ForFieldMapping__c obj2= new CS_ForFieldMapping__c();
        obj2.name='Test_2';
        obj2.SourceObject__c ='Case';
        obj2.TargetObject__c = 'ArchiveCase__c';
        obj2.SourceField__c = 'AssetId';
        obj2.TargetField__c = 'Asset__c';
        cslist.add(obj2);
        
        CS_ForFieldMapping__c obj3= new CS_ForFieldMapping__c();
        obj3.name='Test_3';
        obj3.SourceObject__c ='Case';
        obj3.TargetObject__c = 'ArchiveCase__c';
        obj3.SourceField__c = 'AssetOwner__c';
        obj3.TargetField__c = 'AssetOwner__c';
        cslist.add(obj3);
        
        CS_ForFieldMapping__c obj4= new CS_ForFieldMapping__c();
        obj4.name='Test_4';
        obj4.SourceObject__c ='Case';
        obj4.TargetObject__c = 'ArchiveCase__c';
        obj4.SourceField__c = 'CallbackPhone__c';
        obj4.TargetField__c = 'CallbackPhone__c';
        cslist.add(obj4);
        
        CS_ForFieldMapping__c obj5= new CS_ForFieldMapping__c();
        obj5.name='Test_5';
        obj5.SourceObject__c ='Case';
        obj5.TargetObject__c = 'ArchiveCase__c';
        obj5.SourceField__c = 'CampaignKeycode__c';
        obj5.TargetField__c = 'CampaignKeycode__c';
        cslist.add(obj5);
        
        CS_ForFieldMapping__c obj6= new CS_ForFieldMapping__c();
        obj6.name='Test_6';
        obj6.SourceObject__c ='Case';
        obj6.TargetObject__c = 'ArchiveCase__c';
        obj6.SourceField__c = 'CaseNumber';
        obj6.TargetField__c = 'Name';
        cslist.add(obj6);
       
        CS_ForFieldMapping__c obj7= new CS_ForFieldMapping__c();
        obj7.name='Test_7';
        obj7.SourceObject__c ='Case';
        obj7.TargetObject__c = 'ArchiveCase__c';
        obj7.SourceField__c = 'OwnerId';
        obj7.TargetField__c = 'OwnerId';
        cslist.add(obj7);
            
        CS_ForFieldMapping__c obj11= new CS_ForFieldMapping__c();
        obj11.name='Test_11';
        obj11.SourceObject__c ='CaseStage__c';
        obj11.TargetObject__c = 'ArchiveCaseStage__c';
        obj11.SourceField__c = 'BusinessHours__c';
        obj11.TargetField__c = 'BusinessHours__c';
        cslist.add(obj11);
        
        CS_ForFieldMapping__c obj12= new CS_ForFieldMapping__c();
        obj12.name='Test_12';
        obj12.SourceObject__c ='CaseStage__c';
        obj12.TargetObject__c = 'ArchiveCaseStage__c';
        obj12.SourceField__c = 'Name';
        obj12.TargetField__c = 'Name';
        cslist.add(obj12);
        
        CS_ForFieldMapping__c obj13= new CS_ForFieldMapping__c();
        obj13.name='Test_13';
        obj13.SourceObject__c ='InquiraFAQ__c';
        obj13.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj13.SourceField__c = 'InfoCenterURL__c';
        obj13.TargetField__c = 'InfoCenterURL__c';
        cslist.add(obj13);
        
        CS_ForFieldMapping__c obj14= new CS_ForFieldMapping__c();
        obj14.name='Test_14';
        obj14.SourceObject__c ='InquiraFAQ__c';
        obj14.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj14.SourceField__c = 'Name';
        obj14.TargetField__c = 'Name';
        cslist.add(obj14);
        
        CS_ForFieldMapping__c obj15= new CS_ForFieldMapping__c();
        obj15.name='Test_15';
        obj15.SourceObject__c ='InquiraFAQ__c';
        obj15.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj15.SourceField__c = 'URL__c';
        obj15.TargetField__c = 'URL__c';
        cslist.add(obj15);
        
        CS_ForFieldMapping__c obj16= new CS_ForFieldMapping__c();
        obj16.name='Test_16';
        obj16.SourceObject__c ='CaseComment';
        obj16.TargetObject__c = 'ArchiveCaseStage__c';
        obj16.SourceField__c = 'CommentBody';
        obj16.TargetField__c = 'CommentBody__c';
        cslist.add(obj16);
        
        CS_ForFieldMapping__c obj17= new CS_ForFieldMapping__c();
        obj17.name='Test_17';
        obj17.SourceObject__c ='CaseComment';
        obj17.TargetObject__c = 'ArchiveCaseStage__c';
        obj17.SourceField__c = 'IsPublished';
        obj17.TargetField__c = 'IsPublished__c';
        cslist.add(obj17);
        
        CS_ForFieldMapping__c obj18= new CS_ForFieldMapping__c();
        obj18.name='Test_18';
        obj18.SourceObject__c ='CaseComment';
        obj18.TargetObject__c = 'ArchiveCaseStage__c';
        obj18.SourceField__c = 'CreatedById';
        obj18.TargetField__c = 'CreatedBy__c';
        cslist.add(obj18);
        
        
        CS_ForFieldMapping__c obj19= new CS_ForFieldMapping__c();
        obj19.name='Test_19';
        obj19.SourceObject__c ='EmailMessage';
        obj19.TargetObject__c = 'ArchiveEmailMessage__c';
        obj19.SourceField__c = 'FromName';
        obj19.TargetField__c = 'FromName__c';
        cslist.add(obj19);
        
        CS_ForFieldMapping__c obj20= new CS_ForFieldMapping__c();
        obj20.name='Test_20';
        obj20.SourceObject__c ='EmailMessage';
        obj20.TargetObject__c = 'ArchiveEmailMessage__c';
        obj20.SourceField__c = 'Status';
        obj20.TargetField__c = 'Status__c';
        cslist.add(obj20);
        
        
        CS_ForFieldMapping__c obj21= new CS_ForFieldMapping__c();
        obj21.name='Test_21';
        obj21.SourceObject__c ='EmailMessage';
        obj21.TargetObject__c = 'ArchiveEmailMessage__c';
        obj21.SourceField__c = 'Subject';
        obj21.TargetField__c = 'Subject__c';
        cslist.add(obj21);
        
        CS_ForFieldMapping__c obj22= new CS_ForFieldMapping__c();
        obj22.name='Test_22';
        obj22.SourceObject__c ='SVMXC__Case_Line__c';
        obj22.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj22.SourceField__c = 'SVMXC__Entitled_Exchange_Type__c';
        obj22.TargetField__c = 'SVMXC_Entitled_Exchange_Type__c';
        cslist.add(obj22);
        
        CS_ForFieldMapping__c obj23= new CS_ForFieldMapping__c();
        obj23.name='Test_23';
        obj23.SourceObject__c ='SVMXC__Case_Line__c';
        obj23.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj23.SourceField__c = 'SVMXC__Entitlement_History__c';
        obj23.TargetField__c = 'SVMXC_Entitlement_History__c';
        cslist.add(obj23);
        
        CS_ForFieldMapping__c obj24= new CS_ForFieldMapping__c();
        obj24.name='Test_24';
        obj24.SourceObject__c ='SVMXC__Case_Line__c';
        obj24.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj24.SourceField__c = 'SVMXC__Line_Status__c';
        obj24.TargetField__c = 'SVMXC_Line_Status__c';
        cslist.add(obj24);
        
        CS_ForFieldMapping__c obj30= new CS_ForFieldMapping__c();
        obj30.name='Test_30';
        obj30.SourceObject__c ='CSE_ExternalReferences__c';
        obj30.TargetObject__c = 'ArchiveCSE_ExternalReferences__c';
        obj30.SourceField__c = 'BackOfficeSystem__c';
        obj30.TargetField__c = 'BackOfficeSystem__c';
        cslist.add(obj30);
        
        insert cslist;
        
        CS_NewFieldMapping__c nobj1 = new CS_NewFieldMapping__c();
        nobj1.name = 'Test_25';
        nobj1.SourceObject__c = 'Case';
        nobj1.TargetObject__c = 'ArchiveCase__c';
        nobj1.SourceField__c = 'ACC_ClassLevel1__c';
        nobj1.TargetField__c = 'ACC_ClassLevel1__c';
        newcslist.add(nobj1);
        
        CS_NewFieldMapping__c nobj2 = new CS_NewFieldMapping__c();
        nobj2.name = 'Test_26';
        nobj2.SourceObject__c = 'Case';
        nobj2.TargetObject__c = 'ArchiveCase__c';
        nobj2.SourceField__c = 'ACC_PreferredAgent__c';
        nobj2.TargetField__c = 'ACC_PreferredAgent__c';
        newcslist.add(nobj2);
        
        CS_NewFieldMapping__c nobj3 = new CS_NewFieldMapping__c();
        nobj3.name = 'Test_27';
        nobj3.SourceObject__c = 'Case';
        nobj3.TargetObject__c = 'ArchiveCase__c';
        nobj3.SourceField__c = 'TECH_GMRFamily__c';
        nobj3.TargetField__c = 'TECH_GMRFamily__c';
        newcslist.add(nobj3);
        
        CS_NewFieldMapping__c nobj4 = new CS_NewFieldMapping__c();
        nobj4.name = 'Test_28';
        nobj4.SourceObject__c = 'Case';
        nobj4.TargetObject__c = 'ArchiveCase__c';
        nobj4.SourceField__c = 'ActualAge__c';
        nobj4.TargetField__c = 'ActualAge__c';
        nobj4.Flag__c = true;
        newcslist.add(nobj4);
        
        CS_NewFieldMapping__c nobj5 = new CS_NewFieldMapping__c();
        nobj5.name = 'Test_29';
        nobj5.SourceObject__c = 'Case';
        nobj5.TargetObject__c = 'ArchiveCase__c';
        nobj5.SourceField__c = 'PCRExternalReferenceCount__c';
        nobj5.TargetField__c = 'PCRExternalReferenceCount__c';
        nobj5.Flag__c = true;
        newcslist.add(nobj5);
        
        insert newcslist;
        
        //Creating System user
        User TestUser = Utils_TestMethods.createStandardUser('testing');
        TestUser.BypassTriggers__c = 'AP01;AP03;AP10;AP16;AP11;AP1000;AP1000_1;AP1000_2;AP1000_3;AP1001;AP1002;AP1003;AP1004;AP43;AP_CustomerSafetyIssue;AP_Case_CheckOpenCCCActions;AP_Case_CreateCaseActions;AP_ChatterForCCCAction;AP_ContractTriggerUtils;AP_TemplateTriggerUtils;AP_UpdateCCCActionAge;SVMX06;SVMX07;SVMX05;AP54;AP_Opp_UpdateIncludeInForecastField;AP_Opp_UpdateParentCloseDate;AP_WorkOrderTechnicianNotification;AP_OppBusinessMixUpdate;AP_Case_CaseHandlerCaseAfterUpdate;AP_Case_CaseHandlerCaseBeforeInsert;AP_Case_CaseHandlerCaseBeforeUpdate;AP_InvolvedOrganization;AP_IO_AI;AP_ITB_Assets_PrimaryAssetCheck;AP_NewRIAM;AP_RI_FieldUpdate;AP_RR_FieldUpdate;SVMX08;AP_CustomerSafetyIssue_After;AP_OpportunityTeamMember;SVMX09;AP_ORFNotification;SRV01;AP_SendOpportunityNotification;SVMX_InstalledProduct;SVMX_InstalledProduct2;AP_LCR;AP_LCR1;AP08;AP_CaseRelatedProduct_Handler;AP_Case_MajorIssue;AP_TechnicalExpertAssessment;AP_RI_RMAMailMerge;AP_CIS_ISRHandler;AP_CIS_InterventionCostHandler;SVMX10;SVMX22;SRV03;SRV_APR_001;SRV_APR_002;SRV04;SRV07;SVMX11;SVMX12;SVMX14;SVMX16;SVMX18;SVMX19;SVMX20;SVMX21;AP_ProblemComplaintRequestLink;AP_TEXConfirmCheck;ProblemCaseLinkAfterDelete;ProblemCaseLinkAfterInsert;RMAAtferUpdate;AP_LCR2;CRAfterDelete;CRAfterInsert;RMAAtferDelete;RMAAtferInsert;TEX_AI;AP_CareTeamBeforeUpdateHandler;SVMX01;AP_Case_CaseHandler;CaseAfterInsert;CaseAfterUpdate;CaseBeforeInsert;CaseBeforeUpdate;AP_CRComments;ITB_AssetsAfterUpdate';
        insert TestUser;
        System.runAs(TestUser){
        
         Account accountObj = Utils_TestMethods.createAccount();
        Database.insert(accountObj);
             
        contact contactObj=Utils_TestMethods.createContact(accountObj.Id,'testContact');
        Database.insert(contactObj);     
        
        Country__c countryObj = Utils_TestMethods.createCountry();
        Database.insert(countryObj);
        Test.StartTest();
         /*    
        OPP_Product__c objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        Database.insert(objProduct1);
        */
        CustomerLocation__c custlocobj=  Utils_TestMethods.createCustomerLocation(accountObj.id,countryObj.Id);
        Database.insert(custlocobj);
        
        
        
        Case caseObj=Utils_TestMethods.createCase(accountObj.id,contactObj.id,'closed');
        //caseObj.Family_lk__c = objProduct1.Id;
        Database.insert(caseObj);
        
        ExpertInternalComment__c expertObj = new ExpertInternalComment__c();
        expertObj.case__c = caseObj.id;
        Database.insert(expertObj);
        
        
        TEX__c texObj = new TEX__c();
        texObj.case__c = caseObj.id;
        Database.insert(texObj);
        
        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
            Name='LCR TEST SCH',
            Entity__c='LCR Entity-1',
            SubEntity__c='LCR Sub-Entity',
            Location__c='LCR Location',
            Location_Type__c= 'Design Center',
            Street__c = 'LCR Street',
            RCPOBox__c = '123456',
            RCCountry__c = countryObj.Id,
            RCCity__c = 'LCR City',
            RCZipCode__c = '500055',
            ContactEmail__c ='SCH@schneider.com'
            );
        Insert lCROrganzObj; 
        
        BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(
            Name='LCR TEST Repair',
            Entity__c='LCR Entity-1',
            SubEntity__c='LCR Sub-Entity1',
            Location__c='LCR Location 1',
            Location_Type__c= 'LCR',
            Street__c = 'LCR Street1',
            RCPOBox__c = '123456',
            RCCountry__c = countryObj.Id,
            RCCity__c = 'LCR City1',
            RCZipCode__c = '500225',
            ContactEmail__c ='SCH1@schneider.com'
            );
            Insert lCROrganzObj1;
        
        ComplaintRequest__c  complaintObj = new ComplaintRequest__c(
            Status__c='Open',
            Case__c  =caseObj.id,
            Category__c ='1 - Pre-Sales Support',
            reason__c='Marketing Activity Response',
            AccountableOrganization__c =lCROrganzObj.id, 
            ReportingEntity__c =lCROrganzObj1.id
            );
        Database.Insert(complaintObj); 
        
        SVMXC__Case_Line__c svmaxobj = new SVMXC__Case_Line__c();
        svmaxobj.SVMXC__Case__c=caseObj.id;
        Database.Insert(svmaxobj);
        
        SVMXC__Entitlement_History__c entitleobj = new  SVMXC__Entitlement_History__c();
        entitleobj.SVMXC__Case__c=caseObj.id;
        Database.Insert(entitleobj);
        
        CSE_ExternalReferences__c cseerobj = new CSE_ExternalReferences__c();
        cseerobj.Case__c=caseObj.id;
        cseerobj.TECH_Account__c=accountObj.Id;
        Database.Insert(cseerobj);
        
        CaseStakeholders__c cseStakeHolderObj = new CaseStakeholders__c();
        cseStakeHolderObj.case__c=caseObj.id;
        cseStakeHolderObj.AccountableOrganization__c=lCROrganzObj1.id;
        Database.Insert(cseStakeHolderObj);
        
        
        CaseArchiving_Batch capb1 = new CaseArchiving_Batch(); 
        ID batchprocessid =Database.executeBatch(capb1);
        Test.StopTest();
        }
        
    }    
    
}