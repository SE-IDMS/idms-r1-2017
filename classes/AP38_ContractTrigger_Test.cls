@isTest
private class AP38_ContractTrigger_Test{

  static testMethod void testContractTrigger(){
    Account Acc = Utils_TestMethods.createAccount();
    insert Acc;
    
    Contract contract = new Contract(Name='Contract Name', AccountId=Acc.id, SupportAmount__c=123, StartDate=date.newInstance(2012, 3, 30), ContractTerm=12, status='draft');
    insert contract;
    update contract;
    List<CS_SerContOppMapping__c> scmList = new List<CS_SerContOppMapping__c>();
    
    CS_SerContOppMapping__c scmone = new CS_SerContOppMapping__c(); 
    scmone.SourceFieldApi__c ='SVMXC__Service_Contract__c.Name;SVMXC__Service_Contract__c.OpportunityScope__c;Account.Name';
    scmone.TargetFieldApi__c ='Name';   
    scmone.name='testone';
    scmList.add(scmone);
    CS_SerContOppMapping__c scmtwo = new CS_SerContOppMapping__c(); 
    scmtwo.SourceFieldApi__c ='60';
    scmtwo.TargetFieldApi__c ='Probability';    
    scmtwo.name='testtwo';
    scmtwo.Type__c = 'Integer';
    scmList.add(scmtwo);
    
    CS_SerContOppMapping__c scmthree = new CS_SerContOppMapping__c();   
    scmthree.SourceFieldApi__c ='3 - Identify & Qualify';
    scmthree.TargetFieldApi__c ='StageName';    
    scmthree.name='testthree';
    scmthree.Type__c = 'String';
    scmList.add(scmthree);
    insert scmList;
    
    
    AP38_ContractTrigger.createNewContractOpportunity(contract,Acc);
    
    SVMXC__Service_Contract__c sc= new SVMXC__Service_Contract__c();
    
    sc.Name = 'Test';
    sc.LeadingBusinessBU__c = 'PW';
    sc.OpportunityType__c ='Standard';
    sc.BillingPlan__c= 'Annual';
    sc.OpportunityScope__c = 'BDADS';
    sc.SVMXC__Company__c =Acc.id;
    insert sc;
    AP38_ContractTrigger.createNewServiceContractOpportunity(sc,Acc);
  }

}