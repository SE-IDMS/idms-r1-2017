@isTest
private class VFC_EscalationDeEscalationForCase_TEST{
    static testMethod void EscalationToExpertForCase_TestMethod() {
        
        String strPrimaryQueueId  = Label.CLOCT13CCC15;
        String strAdvancedQueueId = Label.CLOCT13CCC16;
        String strExpertQueueId   = Label.CLOCT13CCC17;
        
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        objOpenCase.OwnerId = strAdvancedQueueId;
        Database.update(objOpenCase);
        
        PageReference pageRef = Page.VFP_EscalationDeEscalationForCase;
        pageRef.getParameters().put('escalation','1');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_EscalationDeEscalationForCase EscalationCasePage = new VFC_EscalationDeEscalationForCase(CaseStandardController );
        
        Test.startTest();
        EscalationCasePage.performAction();
        EscalationCasePage.returnToCase();
        Test.stopTest();
    }
    static testMethod void DeEscalationToPrimaryTeamForCase_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        String strPrimaryQueueId  = Label.CLOCT13CCC15;
        String strAdvancedQueueId = Label.CLOCT13CCC16;
        String strExpertQueueId   = Label.CLOCT13CCC17;
    
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name = 'Adv Country';
        objCountry.Name = 'ACT';
        Database.insert(objCountry);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
        
        CustomerCareTeam__c objCCTeam = Utils_TestMethods.CustomerCareTeam(objCountry.Id);
        Database.insert(objCCTeam);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.Team__c =objCCTeam.Id;
        Database.insert(objOpenCase);
        
        objOpenCase.OwnerId = strAdvancedQueueId;
        Database.update(objOpenCase);
        
        
        PageReference pageRef = Page.VFP_EscalationDeEscalationForCase;
        pageRef.getParameters().put('escalation','0');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_EscalationDeEscalationForCase DeEscalationCasePage = new VFC_EscalationDeEscalationForCase(CaseStandardController );
        Test.startTest();
        DeEscalationCasePage.performAction();
        DeEscalationCasePage.returnToCase();
        Test.stopTest();
    }
    static testMethod void DeEscalationToAdvancedTeamForCase_TestMethod(){
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        String strPrimaryQueueId  = Label.CLOCT13CCC15;
        String strAdvancedQueueId = Label.CLOCT13CCC16;
        String strExpertQueueId   = Label.CLOCT13CCC17;
        
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name = 'Primary Country';
        objCountry.Name = 'PCT';
        Database.insert(objCountry);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
        
        CustomerCareTeam__c objCCTeam = Utils_TestMethods.CustomerCareTeam(objCountry.Id);
        Database.insert(objCCTeam);
        
        CustomerCareTeam__c objAdvancedCCTeam = Utils_TestMethods.CustomerCareTeam(objCountry.Id);
        objAdvancedCCTeam.LevelOfSupport__c = 'Advanced';
        Database.insert(objAdvancedCCTeam);
        
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.Team__c =objCCTeam.Id;
        Database.insert(objOpenCase);
        
        objOpenCase.Team__c = objAdvancedCCTeam.Id;
        Database.update(objOpenCase);
        Test.startTest();
        objOpenCase.OwnerId = strExpertQueueId;
        Database.update(objOpenCase);
        
        PageReference pageRef = Page.VFP_EscalationDeEscalationForCase;
        pageRef.getParameters().put('escalation','0');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_EscalationDeEscalationForCase DeEscalationCasePage = new VFC_EscalationDeEscalationForCase(CaseStandardController );
        
        DeEscalationCasePage.performAction();
        DeEscalationCasePage.returnToCase();
        Test.stopTest();
    }
    static testMethod void EscalationToAdvancedFailure_TestMethod() {
        // This is to cover the Error Scenario -Esclate from Primary to Advanced
        String strPrimaryQueueId  = Label.CLOCT13CCC15;
        String strAdvancedQueueId = Label.CLOCT13CCC16;
        String strExpertQueueId   = Label.CLOCT13CCC17;
        
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        PageReference pageRef = Page.VFP_EscalationDeEscalationForCase;
        pageRef.getParameters().put('escalation','1');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_EscalationDeEscalationForCase EscalationCasePage = new VFC_EscalationDeEscalationForCase(CaseStandardController );
        
        Test.startTest();
        EscalationCasePage.performAction();
        EscalationCasePage.returnToCase();
        Test.stopTest();
    }
    static testMethod void DeEscalationToPrimaryNoTeamForCase_TestMethod(){
        // This is to cover the Error Scenario - DeEscalate, when Last Primary Team doesn't exist
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        String strPrimaryQueueId  = Label.CLOCT13CCC15;
        String strAdvancedQueueId = Label.CLOCT13CCC16;
        String strExpertQueueId   = Label.CLOCT13CCC17;
    
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        objOpenCase.OwnerId = strAdvancedQueueId;
        Database.update(objOpenCase);
        
        
        PageReference pageRef = Page.VFP_EscalationDeEscalationForCase;
        pageRef.getParameters().put('escalation','0');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_EscalationDeEscalationForCase DeEscalationCasePage = new VFC_EscalationDeEscalationForCase(CaseStandardController );
        
        Test.startTest();
        DeEscalationCasePage.performAction();
        DeEscalationCasePage.returnToCase();
        Test.stopTest();
    }
    static testMethod void DeEscalationToAdvancedNoTeamForCase_TestMethod(){
        // This is to cover the Error Scenario - DeEscalate, when Last Advanced Team doesn't exist
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        String strPrimaryQueueId  = Label.CLOCT13CCC15;
        String strAdvancedQueueId = Label.CLOCT13CCC16;
        String strExpertQueueId   = Label.CLOCT13CCC17;
    
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        objOpenCase.OwnerId = strAdvancedQueueId;
        Database.update(objOpenCase);
        
        Test.startTest();
        objOpenCase.OwnerId = strExpertQueueId;
        Database.update(objOpenCase);
        
        PageReference pageRef = Page.VFP_EscalationDeEscalationForCase;
        pageRef.getParameters().put('escalation','0');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_EscalationDeEscalationForCase DeEscalationCasePage = new VFC_EscalationDeEscalationForCase(CaseStandardController );
        
        DeEscalationCasePage.performAction();
        DeEscalationCasePage.returnToCase();
        Test.stopTest();
    }
}