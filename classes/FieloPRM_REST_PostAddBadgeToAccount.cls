/********************************************************************************************************
    Author: Fielo Team
    Date: 27/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestPostAddBadgeToAccount/*') 
global class FieloPRM_REST_PostAddBadgeToAccount{  

    /**
    * [postAddBadgeToAccount]
    * @method   postAddBadgeToAccount
    * @Pre-conditions  
    * @Post-conditions 
    * @param    []    []
    * @return   []    []
    */
    @HttpPost
    global static String postAddBadgeToAccount(String type, Map<String ,List<String>> accToBadges){    
        
        return FieloPRM_UTILS_BadgeAccount.addBadgeToAccount(type, accToBadges);        
    }  
   
}