public class AP1001_ProblemTeamMember{
    public static void addProblemTeamMemberToProblemShare(List<ProblemTeamMember__c> prblmTeamList){
        //*********************************************************************************
        // Method Name      : addProblemTeamMemberToProblemShare
        // Purpose          : To add Team Member to Problem Share Object 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 25th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        System.Debug('****AP1001_ProblemTeamMember addProblemTeamMemberToProblemShare  Started****');
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        
        for(ProblemTeamMember__c prblmTeam: prblmTeamList){
            prblmShareList.add(Utils_P2PMethods.createProblemShare(prblmTeam.Problem__c,prblmTeam.User__c,Label.CL10059));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareList);
        System.Debug('****AP1001_ProblemTeamMember addProblemTeamMemberToProblemShare  Finished****');
    }
    public static void updateProblemTeamMemberToProblemShare(Map<Id,ProblemTeamMember__c> prblmTeamOldMap,Map<Id,ProblemTeamMember__c> prblmTeamNewMap){
        //*********************************************************************************
        // Method Name      : updateProblemTeamMemberToProblemShare
        // Purpose          : To Update Sharing for Problem Team Member when Problem Team Member Record is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 19th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1001_ProblemTeamMember updateProblemTeamMemberToProblemShare Started****');
        
        deleteProblemTeamMemberFromProblemShare(prblmTeamOldMap);
         
        List<Problem__Share> prblmShareListForInsert = new List<Problem__Share>();
        List<ProblemTeamMember__c> prblmTeamList =prblmTeamNewMap.values();
        for(ProblemTeamMember__c prblmTeam: prblmTeamList){
            prblmShareListForInsert.add(Utils_P2PMethods.createProblemShare(prblmTeam.Problem__c,prblmTeam.User__c,Label.CL10059));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareListForInsert);
        
        System.Debug('****AP1001_ProblemTeamMember updateProblemTeamMemberToProblemShare Finished****');
    }
    public static void deleteProblemTeamMemberFromProblemShare(Map<Id,ProblemTeamMember__c> prblmTeamOldMap){
        //*********************************************************************************
        // Method Name      : deleteProblemTeamMemberFromProblemShare
        // Purpose          : Delete Sharing for Problem Team Member when Problem Team Member Record is Deleted 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 26th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1001_ProblemTeamMember deleteProblemTeamMemberFromProblemShare Started****');
        Set<Id> prblmId = new Set<id>();
        Set<Id> prblmTeamUserId = new Set<id>();
        List<ProblemTeamMember__c> oldPrblmTeamList = prblmTeamOldMap.values();
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        Map <String, Problem__Share> prblmShareMap = new Map<String, Problem__Share>();
        List<Problem__Share> prblmShareListForDelete = new List<Problem__Share>();
        for(ProblemTeamMember__c prblmTeam: oldPrblmTeamList){
            prblmId.add(prblmTeam.Problem__c);
            prblmTeamUserId.add(prblmTeam.User__c);
        }
        System.Debug('ParentId :' + prblmId);
        System.Debug('UserOrGroupId:'+ prblmTeamUserId);
        prblmShareList = [Select Id, ParentId, UserOrGroupId, AccessLevel,RowCause from  Problem__Share where ParentId IN:prblmId 
                                                                          AND UserOrGroupId IN: prblmTeamUserId AND RowCause =: Label.CL10059 ];
        for (Problem__Share prblmShare:prblmShareList ){
            prblmShareMap.put(String.valueOf(prblmShare.ParentId) + String.valueOf(prblmShare.UserOrGroupId) ,prblmShare);
        }
        //Delete the record from Problem Share only, if the Team Member is not present in any other role in same Problem
        //The Map "otherPrblmTeamMap" will get all the other Team Members associated with the Problem for this validation. 
		Map<String,ProblemTeamMember__c> otherPrblmTeamMap= new Map<String,ProblemTeamMember__c>();
		for (ProblemTeamMember__c  otherPrblmTeam : [Select Id, Problem__c, User__c from  ProblemTeamMember__c where Problem__c IN:prblmId 
                                                                                                        AND Id NOT IN: prblmTeamOldMap.KeySet()]) {
			otherPrblmTeamMap.put(String.valueOf(otherPrblmTeam.Problem__c) + String.valueOf(otherPrblmTeam.User__c),otherPrblmTeam);
		}
		
        for(ProblemTeamMember__c prblmTeam: oldPrblmTeamList){
            if(prblmShareMap.containsKey(String.valueOf(prblmTeam.Problem__c) + String.valueOf(prblmTeam.User__c))){
                if(!(otherPrblmTeamMap.containsKey(String.valueOf(prblmTeam.Problem__c) + String.valueOf(prblmTeam.User__c)))){
                	prblmShareListForDelete.add(prblmShareMap.get(String.valueOf(prblmTeam.Problem__c) + String.valueOf(prblmTeam.User__c)));
                }
            }
        }
        
        Utils_P2PMethods.deletePrblmShare(prblmShareListForDelete);

        System.Debug('****AP1001_ProblemTeamMember deleteProblemTeamMemberFromProblemShare Finished****');
    }
}