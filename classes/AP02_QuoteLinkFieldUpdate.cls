public without sharing class AP02_QuoteLinkFieldUpdate {
    public static void performCurrentUserUpdates(List<OPP_QuoteLink__c> qls){
        for(OPP_QuoteLink__c ql : qls){
            // set current approver
            if(ql.TECH_ApprovalStep__c == 10)
                ql.REP_CurrentApprover__c = ql.Approver10__c;
            else if(ql.TECH_ApprovalStep__c == 9)
                ql.REP_CurrentApprover__c = ql.Approver9__c;
            else if(ql.TECH_ApprovalStep__c == 8)
                ql.REP_CurrentApprover__c = ql.Approver8__c;
            else if(ql.TECH_ApprovalStep__c == 7)
                ql.REP_CurrentApprover__c = ql.Approver7__c;
            else if(ql.TECH_ApprovalStep__c == 6)
                ql.REP_CurrentApprover__c = ql.Approver6__c;
            else if(ql.TECH_ApprovalStep__c == 5)
                ql.REP_CurrentApprover__c = ql.Approver5__c;
            else if(ql.TECH_ApprovalStep__c == 4)
                ql.REP_CurrentApprover__c = ql.Approver4__c;
            else if(ql.TECH_ApprovalStep__c == 3)
                ql.REP_CurrentApprover__c = ql.Approver3__c;
            else if(ql.TECH_ApprovalStep__c == 2)
                ql.REP_CurrentApprover__c = ql.Approver2__c;
            else if(ql.TECH_ApprovalStep__c == 1)
                ql.REP_CurrentApprover__c = ql.Approver1__c;
            else
                ql.REP_CurrentApprover__c = null;   
        }
    }
    
    public static void performLastUserUpdates(List<OPP_QuoteLink__c> qls){
        for(OPP_QuoteLink__c ql : qls){
            // set last approver
            if(ql.Approver10__c != null)
                ql.REP_LastApprover__c = ql.Approver10__c;
            else if(ql.Approver9__c != null)
                ql.REP_LastApprover__c = ql.Approver9__c;
            else if(ql.Approver8__c != null)
                ql.REP_LastApprover__c = ql.Approver8__c;
            else if(ql.Approver7__c != null)
                ql.REP_LastApprover__c = ql.Approver7__c;
            else if(ql.Approver6__c != null)
                ql.REP_LastApprover__c = ql.Approver6__c;
            else if(ql.Approver5__c != null)
                ql.REP_LastApprover__c = ql.Approver5__c;
            else if(ql.Approver4__c != null)
                ql.REP_LastApprover__c = ql.Approver4__c;
            else if(ql.Approver3__c != null)
                ql.REP_LastApprover__c = ql.Approver3__c;
            else if(ql.Approver2__c != null)
                ql.REP_LastApprover__c = ql.Approver2__c;
            else if(ql.Approver1__c != null)
                ql.REP_LastApprover__c = ql.Approver1__c;
            else
                ql.REP_LastApprover__c = null;
        }
    }
    
    public static void performOppUpdates(List<OPP_QuoteLink__c> qls){
        List<Opportunity> updateList = new List<Opportunity>();
        Set<Opportunity> OpportunitySet = new Set<Opportunity>();
        for(OPP_QuoteLink__c ql : qls){
            Opportunity o = new Opportunity(Id = ql.OpportunityName__c, TECH_QuoteApproval__c = true);
            OpportunitySet.add(o);
        }
        // update opportunities
        updateList.addAll(OpportunitySet);
        if(!updateList.isEmpty()){
            Database.update(updateList); 
        }
    }
    
    //Populates Quote Link's Agreement field with TECH_AgreementId__c
    Public static void updateAgreement(List<OPP_QuoteLink__c> quoteLinks)
    {
        for(OPP_QuoteLink__c quoteLink: quoteLinks)
        {
            quoteLink.Agreement__c = quoteLink.TECH_AgreementId__c;
        }            
    }
}