/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 25-sep-2013
    Description     : Acid Test Controller - IPO Strategic Program
*/




public with sharing class IPOAcidTestController {


 public IPOAcidTestController(ApexPages.StandardController controller) {
 pageid = ApexPages.currentPage().getParameters().get('Id');
   if(pageid == null){
    isEdit = True;
    isView = True;
    isSave = True;
    }
   else if (pageid != null){
    isEdit = False;
    isView = True;
    isSave = True;
    }

    }


        
//Declarations

 public string Answer{get;set;}
 public boolean isEdit{get;set;}
 public boolean isView{get;set;}
 public boolean isSave{get;set;}
 public string pageid{get;set;}
 public string Rem{get;set;}
 List<AcidTestAnswer> SelAns = new List<AcidTestAnswer>();


public List<SelectOption> getItems() {
List<SelectOption> options = new List<SelectOption>();
 options.add(new SelectOption('Not Answered','Not Answered'));
 options.add(new SelectOption('Yes','Yes'));
  options.add(new SelectOption('Partial','Partial'));
 options.add(new SelectOption('No','No'));
 return options;
  }
  
Public List<AcidTestAnswer> getQuestionAnswer(){

pageid = ApexPages.currentPage().getParameters().get('Id');
List<AcidTestAnswer> QA = new List<AcidTestAnswer>();
List<IPOActidTestQuestions__c> ActQ = new List<IPOActidTestQuestions__c>();
List<IPOAcidTestAnswers__c> ActA = new List<IPOAcidTestAnswers__c>();

ActQ = [Select id, Qno__c, Question__c from IPOActidTestQuestions__c order by Qno__c];

ActA = [Select IPOAcidTestQuestions__c, IPO_Program__c, Answer__c,Remarks__c,AnswerIndicator__c from IPOAcidTestAnswers__c where IPO_Program__c = :pageid ];

  
integer Ansfound;

// Look for Questions and the Respective Answers

For(IPOActidTestQuestions__c Q: ActQ){
 Ansfound = 0;
 For(IPOAcidTestAnswers__c A: ActA){
   string aind;
   if(Q.id == A.IPOAcidTestQuestions__c){
      if(A.AnswerIndicator__c.Contains('Green'))
          aind = '/resource/Connect_Green';
      else if(A.AnswerIndicator__c.Contains('Grey'))
          aind = '/resource/Connect_Grey';
      else if(A.AnswerIndicator__c.Contains('Red'))
          aind = '/resource/Connect_Red';
      else if(A.AnswerIndicator__c.Contains('Yellow'))
          aind = '/resource/Connect_Yellow';
          
      QA.add(new AcidTestAnswer(A.IPOAcidTestQuestions__c,Q.Qno__c,Q.Question__c, A.IPO_Program__c, A.Answer__c,A.Remarks__c,aind));
      Ansfound = Ansfound + 1;
      }
     }
   if(Ansfound == 0)
      QA.add(new AcidTestAnswer(Q.id,Q.Qno__c, Q.Question__c, '', 'Not Answered','','/resource/Connect_Grey'));
   SelAns = QA;
       
  
  }
return QA;
}

public void change()
         {
           isEdit = True;
           isView = False;
           isSave = False;
          }
        
public pagereference cancel()
  {
           isEdit = False;
           isView = True;
           isSave = True;
           return null;
          }
          
 public pagereference save()
{

List<IPOAcidTestAnswers__c> ExtA = new List<IPOAcidTestAnswers__c>();
List<IPOAcidTestAnswers__c> NewAns = new List<IPOAcidTestAnswers__c>();

ExtA = [Select IPOAcidTestQuestions__c,IPO_Program__c, Answer__c,Remarks__c from IPOAcidTestAnswers__c where IPO_Program__c = :pageid];

For(IPOAcidTestAnswers__c DelAns: ExtA)
 Delete DelAns;
 
// Insert new Answers

For(AcidTestAnswer A:SelAns) {
IPOAcidTestAnswers__c InsAns = new IPOAcidTestAnswers__c();

InsAns.IPOAcidTestQuestions__c = A.ActQname;
insAns.IPO_Program__c = pageid;
InsAns.Answer__c = A.ActAns;
InsAns.Remarks__c = A.Remarks;  
NewAns.add(InsAns);
 }


insert NewAns;

    
  isEdit = False;
  isView = True;
  isSave = True;
  return null;
 }   
 
Public Class AcidTestAnswer{
public string ActQname{get;set;}
public decimal ActQuestid{get;set;}
public string ActQuest{get;set;}
public string ActProgid{get;set;}
public string ActAns{get;set;}
public string Remarks{get;set;}
public string indicator{get;set;}

public AcidTestAnswer(string n,decimal id, string ques, string pid, string ans, string rm,string ind){
this.ActQname = n;
this.ActQuestid = id;
this.ActQuest = ques;
this.ActProgid = pid;
this.ActAns = ans;
this.Remarks = rm;
this.indicator = ind;
}
}

}