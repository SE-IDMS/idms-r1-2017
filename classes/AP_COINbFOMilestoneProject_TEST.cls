/*****************************
Test class AP_COINbFOMilestoneProject,Trigger and AP_COIN_bFO_OfferLifeCycleForecast

******************************/

@isTest
    public class AP_COINbFOMilestoneProject_TEST {
            static string cl;
            static String optr;
            static String vls;
            static list<VFC_COLA_bFO_OfferlifeCycleReportChart.reportFilter> columnFilters ;
        public static testMethod void mileProjectUnitTest() {
            Test.startTest();
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u1 = new User(Alias = 'newUser1', Email='newuser1@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
        
           Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c; 
           Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
           
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            Id offerlaunchid = [Select Id From RecordType  Where SobjectType = 'Offer_Lifecycle__c' and DeveloperName = 'Offer_Launch_Product'][0].Id;
            Id offerwithdrawalid = [Select Id From RecordType  Where SobjectType = 'Offer_Lifecycle__c' and DeveloperName = 'Offer_Withdrawal_Product'][0].Id;
            Id offersolutionlaunchid = [Select Id From RecordType  Where SobjectType = 'Offer_Lifecycle__c' and DeveloperName = 'Offer_Launch_Solution'][0].Id;
            
            Id projectofferlaunchid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Introduction_Project'][0].Id;
            Id projectofferwithdrawalid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Withdrawal_Project'][0].Id;
            Id projectoffersolutionlaunchid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Launch_Solution_Project'][0].Id;
    
            Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
            inOffLif.Offer_Name__c='Test Schneider';
            inOffLif.Leading_Business_BU__c ='IT';
            inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Kilo Euro';
            inOffLif.Description__c='test desc';
            inOffLif.Launch_Master_Assets_Status__c='Red';
            inOffLif.Stage__c ='0 - Define';
            inOffLif.Sales_Date__c = System.today();
            inOffLif.BD_Status__c = 'Draft' ;
            inOffLif.RecordTypeId = offerlaunchid;
            inOffLif.Launch_Owner__c = UserInfo.getUserId();
            Insert inOffLif; 
            
            //=== testing for "VFC_COLA_bFO_OfferlifeCycleReportChart" class ======
            ApexPages.StandardController controller = new ApexPages.StandardController(inOffLif);   
            VFC_COLA_bFO_OfferlifeCycleReportChart reportchart = new VFC_COLA_bFO_OfferlifeCycleReportChart(controller);// VFC_COLA_bFO_OfferlifeCycleReportChart);
            
            VFC_COLA_bFO_OfferlifeCycleReportChart.reportFilter rprt = new VFC_COLA_bFO_OfferlifeCycleReportChart.reportFilter(cl, optr, vls);
            rprt.column ='abc';
            rprt.operator ='xyz';
            rprt.value ='sdghf';
            reportchart.columnFilters = new list<VFC_COLA_bFO_OfferlifeCycleReportChart.reportFilter>{rprt};
            //==== end of the code for testing "VFC_COLA_bFO_OfferlifeCycleReportChart" ======
            
            Announcement__c annc = new Announcement__c();
            annc.Name = 'test1234';
            annc.Offer_Lifecycle__c = inOffLif.id;
            annc.OfferDescription__c = '';
            insert annc;
             Test.stopTest();        
            inOffLif.Description__c='test description';
            inOffLif.Sales_Date__c = system.today().addDays(-25);
            inOffLif.BD_Status__c = 'Published';
            inOffLif.Launch_Owner__c = u1.Id;
            update inOffLif;
            
            Offer_Lifecycle__c inOffLif1= new Offer_Lifecycle__c(); 
            inOffLif1.Offer_Name__c='Test_Schneider_1';
            inOffLif1.Leading_Business_BU__c ='IT';
            inOffLif1.Offer_Classification__c ='Major';
            inOffLif1.Forecast_Unit__c='Kilo Euro';
            inOffLif1.Launch_Master_Assets_Status__c='Red';
            inOffLif1.Stage__c ='0 - Define';
            inOffLif1.End_of_Services_Date__c = System.today();
            inOffLif1.BD_Status__c = 'Published' ;
            inOffLif1.RecordTypeId = offerwithdrawalid;
            inOffLif1.Forecast_Stage__c = 'Validation';
            inOffLif1.Withdrawal_Owner__c = UserInfo.getUserId();
            Insert inOffLif1; 
            
            inOffLif1.Forecast_Stage__c = 'Validated';
            inOffLif1.End_of_Services_Date__c = system.today().addDays(-25);
            inOffLif1.Withdrawal_Owner__c = u1.id;
            update inOffLif1; 
            
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            mileProject.RecordTypeId=MPRecordTypeId;
            mileProject.Name ='By Country Project ';
            mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.Country__c=cotryObj.id;
            mileProject.Country_Announcement_Date__c=Date.today();
            mileProject.TECH_CalculateSendEmailCountrySellDate__c = true;
            mileProject.send_activation_reminder_email__c = true;
            
            insert mileProject ;
           
            Milestone1_Project__c mileProject1= new Milestone1_Project__c(); 
            mileProject1.RecordTypeId=projectofferwithdrawalid;
            mileProject1.Name ='By Country Project2 ';
            mileProject1.Offer_Launch__c=inOffLif.id;
            mileProject1.Country__c=cotryObj.id;
            mileProject1.Country_Announcement_Date__c=Date.today();
            mileProject1.TECH_CalculateSendEmailCountrySellDate__c = false;
            //insert mileProject1 ;
            
            Milestone1_Project__c mileProject2= new Milestone1_Project__c(); 
            mileProject2.RecordTypeId=projectoffersolutionlaunchid;
            mileProject2.Name ='By Country Project1 ';
            mileProject2.Offer_Launch__c=inOffLif.id;
            mileProject2.Country__c=cotryObj.id;
            mileProject2.Country_Announcement_Date__c=Date.today();
            
           // insert mileProject2 ;
           list<Milestone1_Project__c> listmp = new list<Milestone1_Project__c>{mileProject,mileProject1,mileProject2};
            set<string> setOfferPrtId = new set<string>();
            for(Milestone1_Project__c mp : listmp){
                setOfferPrtId.add(mp.id);
            }
            
            Map<Id,Milestone1_Project__c> MapoffProj = new Map<Id,Milestone1_Project__c>();
            for(Milestone1_Project__c mp : listmp){
                MapoffProj.put(mp.id,mp);
            }
            List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
             List<OfferLifeCycleForecast__c> ListoffForecastUpdate= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_YEAR.size(); i++ ) {
                offForecast = new OfferLifeCycleForecast__c();
                offForecast.project__c=mileProject.id;
                offForecast.month__c=monthName(i);
                offForecast.ForcastYear__c='2014';
                offForecast.Actual__c =1000;
                offForecast.BU__c =2000;
                offForecast.Country2__c='France';
                offForecast.Country__c=4000;
                if(i <=3) {
                offForecast.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast.add(offForecast);
            
            
            }
            insert  ListoffForecast;
            
            
            for(OfferLifeCycleForecast__c offerObj:ListoffForecast) {
                offerObj.Actual__c =2000;
                offerObj.BU__c =3000;
                offerObj.Country__c=2000;
                ListoffForecastUpdate.add(offerObj);
            }
            AP_COIN_bFO_OfferLifeCycleForecast.isOfferExt=false; 
            update ListoffForecastUpdate;
            
             List<OfferLifeCycleForecast__c> ListoffForecast1= new List<OfferLifeCycleForecast__c>();
             List<OfferLifeCycleForecast__c> ListoffForecastUpdate1= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast1 = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_YEAR.size(); i++ ) {
                offForecast1 = new OfferLifeCycleForecast__c();
                offForecast1.project__c=mileProject.id;
                offForecast1.month__c=monthName(i);
                offForecast1.ForcastYear__c='2014';
                offForecast1.Actual__c =0;
                offForecast1.BU__c =0;
                offForecast1.Country2__c='France';
                offForecast1.Country__c=0;
                if(i <=3) {
                offForecast1.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast1.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast1.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast1.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast1.add(offForecast1);
            
            
            }
            insert  ListoffForecast1;
            
            
            for(OfferLifeCycleForecast__c offerObj:ListoffForecast1) {
                offerObj.Actual__c =2000;
                offerObj.BU__c =3000;
                offerObj.Country__c=4000;
                ListoffForecastUpdate1.add(offerObj);
            }
            AP_COIN_bFO_OfferLifeCycleForecast.isOfferExt=false; 
            update ListoffForecastUpdate1;
            
            mileProject.isForecastResetConfirm__c=true;
            mileProject.Country_Announcement_Date__c=Date.today()+1;
             
            //update mileProject ;
            
            
            delete mileProject ;
            Milestone1_Exception mp = new Milestone1_Exception();
            mp.Milestone1_Exception1();
          /*  OfferLifeCycleForecast__c offForecastTotal = new OfferLifeCycleForecast__c();
                offForecastTotal.project__c=mileProject.id;
                offForecastTotal.month__c='Total by Year';
                offForecastTotal.ForcastYear__c='2014';
                offForecastTotal.Actual__c =1000;
                offForecastTotal.BU__c =2000;
                offForecastTotal.Country2__c='France';
                offForecastTotal.Country__c=4000;
                
                offForecastTotal.ForecastQuarter__c='Q1';
              AP_COIN_bFO_OfferLifeCycleForecast.isOfferExt=false;  
            insert offForecastTotal; */
            //ApexPages.currentPage().getParameters().put('Id', mileProject.Id);
           // ApexPages.StandardController controller = new ApexPages.StandardController(mileProject);       
            //VFC_OfferLifeCycleForecastCreation coForecast = new VFC_OfferLifeCycleForecastCreation(controller);   
            
        
        
        }
    private static final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December','Total by Year'};


    private static String monthName(Integer monthNum) {

        return(MONTHS_OF_YEAR[monthNum - 1]);

    }
    
    
    
    }