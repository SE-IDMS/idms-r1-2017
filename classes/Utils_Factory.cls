/*
    Author          : Nicolas Palitzyne ~ nicolas.palitzyne@accenture.com 
    Date Created    : 28/04/2011
    Description     : Utility class to create Data source objects
*/

public class Utils_Factory
{
    public static Utils_DataSource getDataSource(String arg0)
    {
         if(arg0 == 'GMR' )
            return new Utils_GMRDataSource();
         else if(arg0 == 'Inquira')
             return new Utils_InquiraDataSource();      
         else if(arg0.equalsIgnoreCase('COMP'))//For Opportunity Competitor Search
             return new Utils_CompetitorDataSource();
         /*Start of sales  2012 April release update*/
          else if(arg0.equalsIgnoreCase('SOCC'))//For Solution Center Country Search
             return new Utils_SolutionCenterCountryDataSource();   
         /*End of sales  2012 April release update*/  
         /*Start of sales  2012 April release update*/
          else if(arg0.equalsIgnoreCase('SECS'))//For Selling Center  Search
             return new Utils_SellingCenterDataSource();   
         /*End of sales  2012 April release update*/  
         /*Start of december release update*/
         else if(arg0.equalsIgnoreCase('LA'))
             return new Utils_LocalAttributeDataSource();        
          /*End of december release update*/
         /*Start of April release 2012 update*/
         else if(arg0.equalsIgnoreCase('TERR'))
             return new Utils_TerritoryDataSource();        
          /*End of April release 2012 update*/ 
          else if(arg0 == 'IB_PRODUCT')
             return new Utils_IBProductDataSource();
          else
             return null;
    }
    
    public static Utils_PicklistManager getPickListManager(String arg1)
    {
         if(arg1 == 'PM0' )
            return new Utils_PM0PicklistManager();
         if(arg1 == 'PM0_GMR' )
            return new Utils_PM0PicklistManager_GMR();   
         else if(arg1.equalsIgnoreCase('COMP'))//For Competitor Search
             return new Utils_CompetitorPicklistManager();//For Opportunity Competitor Search
         /*Start of sales  2012 April release update*/
         else if(arg1.equalsIgnoreCase('SOCC'))//For Solution Center Country Search
             return new Utils_SolutionCenterPicklistManager();   
         /*End of sales  2012 April release update*/
         /*Start of sales  2012 April release update*/
         else if(arg1.equalsIgnoreCase('SECS'))//For Solution Center Country Search
             return new Utils_SellingCenterPicklistManager();   
         /*End of sales  2012 April release update*/
         else if(arg1.equalsIgnoreCase('OPP_REC'))//For Opportunity Record Type and Stage Picklist Display         
             return new Utils_OppStageRecordTypePicklistManager();
         /*Start of december release update*/
         else if(arg1.equalsIgnoreCase('LA'))//FOR LOCAL ATTRIBUTE SEARCH
             return new Utils_LocalAttributePicklistManager();    
         /*End of december release update*/
         /*Start of April release 2012 update*/
         else if(arg1.equalsIgnoreCase('TERR'))//FOR Territory SEARCH
             return new Utils_TerritoryPicklistManager();    
         /*End of April release 2012 update*/          
         else if(arg1 == 'IB_PRODUCT')
             return new Utils_IBProductPicklistManager();
         else
             return null;
    }
}