@IsTest
public class FieloPRM_C_ChatComponentController_TEST {
    public static testMethod void testRemote(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;

        System.runAs(us){    
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;
            
            FieloEE__Program__c prog = [SELECT Id,FieloEE__SiteURL__c,FieloEE__CustomPage__c FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'RahlIsMyLastName';
            member.FieloEE__FirstName__c = 'Marco';
            member.FieloEE__Street__c = 'test';
            member.FieloEE__Program__c = prog.Id;
            insert member;
            
            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;

            Account acc1 = new Account(name='name',Street__c='Street',ZipCode__c='75020',PRMZipCode__c='75020',Country__c=france.id,PRMCountry__c=france.id,PRMUIMSID__c='ABCD');
            insert acc1;

            Contact cont = Utils_TestMethods.createContact(acc1.Id,'RahlIsMyLastName');
            cont.FieloEE__Member__c = member.Id;
            insert cont;

            PRMCountry__c prmCountry = new PRMCountry__c(Country__c=france.Id,PLDataPool__c='en_US2',CountryPortalEnabled__c=true,PartnerLocatorSubmitURL__c='http://www.google.{countryCode}');
            insert prmCountry;
            
            PRMChatComponentConfiguration__c chat = new PRMChatComponentConfiguration__c(CountryCluster__c=prmCountry.Id,IsActive__c=true,ReactiveChatButtonId__c='TEST');
            insert chat;

            Test.startTest();
            FieloPRM_C_ChatComponentController controller  = new FieloPRM_C_ChatComponentController();
            controller.memberId = member.Id;
            FieloPRM_C_ChatComponentController.getChatConfiguration(member.Id,null);
            member.F_PRM_Country__c = prmCountry.Id;
            update member;
            FieloPRM_C_ChatComponentController.getChatConfiguration(member.Id,null);
            Test.stopTest();
        }
    }
}