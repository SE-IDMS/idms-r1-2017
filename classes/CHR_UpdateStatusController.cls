/*    
      Author          : Srikant (Schneider Electric)    
      Date Created    : 04/21/2013   
      Description     : Controller class for the visualforce page CHR_UpdateStatus                      
                        Updates the value for the field 'Status' after checking whether the logged-in user has the authority to do so.
*/                      

public with sharing class CHR_UpdateStatusController
{
    public Change_Request__c oPR{get;set;}
    
    public ApexPages.StandardController controller;
    
    public CHR_UpdateStatusController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        oPR = (Change_Request__c) controller.getRecord();  
        oPR = [Select Id,Parent__c,NextStep__c,Platform__c,ScoreResultCalculated__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,ParentFamily__c,AppServicesDecision__c,AppGlobalOpservdecisiondate__c,AuthorizeFunding__c,BCINumber__c,RecordType.DeveloperName from Change_Request__c where Id = :oPR.Id];
         
    }
    
    public PageReference updateProjectRequest()
    {
        List<DMTAuthorizationMasterData__c> objAuth = new List<DMTAuthorizationMasterData__c>();
        Set<string> setUser = new Set<string>();
        try
        {   if(oPR.nextstep__c == 'Created' || oPR.nextstep__c == 'Fundings Authorized')
                objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusProjectOpen AND Platform__c =:oPR.Platform__c limit 1];    
            if(oPR.nextstep__c == 'Valid')
                objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusValid AND Sub_Domain__c  =:oPR.BusinessTechnicalDomain__c  limit 1];                
            if(objAuth.size() > 0)
            { 
                System.Debug('Data present in table'+objAuth);
                SObject sobjectAuth =(SObject)objAuth[0];
                for(Integer i=1;i<11;i++)
                {
                    if(sobjectAuth.get('AuthorizedUser'+i+'__c') !=null)
                        setUser.add(sobjectAuth.get('AuthorizedUser'+i+'__c')+'');
                }                            
            }
            if(setUser.contains(UserInfo.getUserId()))
                {
                    System.debug('Authorized User');         
                    PRJ_UpdateStatusControllerVerify.updateStatus(null,oPR,'strStatusOnLoad');
                }    
            else
                {
                    if(oPR.nextstep__c == 'Created' || oPR.nextstep__c == 'Fundings Authorized')
                        ApexPages.addMessages(new noMessageException('Only Platform Owner has the ability to change Status Value to ' +oPR.nextstep__c ));
                    else if(oPR.nextstep__c == 'Valid')
                        ApexPages.addMessages(new noMessageException('Only Sub Domain Owner has the ability to change Status Value to '+oPR.nextstep__c));
                    else
                    update oPR;

                }  
            
        }
      catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));
                      
        }
        catch(Exception exp)
        {
            ApexPages.addMessages(new noMessageException(exp.getMessage()));
        }
        
        return null;
    }
    
     public PageReference backToProjectRequest()
    {
        PageReference pageRef = new PageReference('/'+ oPR.Id);
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public class noMessageException extends Exception{}
    
}