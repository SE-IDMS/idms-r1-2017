@isTest(SeeAllData=true)
public class AP_WOFSRNotification_Test {
    static testMethod void myUnitTest() {
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];        
        
        //User creation
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,BypassVR__c=true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        insert user;
        
        User user1 = new User(alias = 'user1', email='user1' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,BypassVR__c=true,
        timezonesidkey='Europe/London', username='user1' + '@bridge-fo.com');
        insert user1;
        
        User user2 = new User(alias = 'user2', email='user2' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing12', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,BypassVR__c=true,
        timezonesidkey='Europe/London', username='user2' + '@bridge-fo.com');
        insert user2;
        
         //Create Country
         Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
         SVMXC__Service_Group_Members__c sgm2;
        SVMXC__Service_Group_Members__c sgm3;
                
        RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
        RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          
         for(RecordType rt : rts) 
        {
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
                insert st;
           } 
        }
        for(RecordType rt : rtssg)
        {
           if(rt.DeveloperName == 'Technican')
            {
                sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '12345'
                                                            );
                insert sgm1;
                
                sgm2 = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user1.Id,
                                                            Send_Notification__c = 'SMS',
                                                            SVMXC__Email__c = 'sgm2.test@company.com',
                                                            SVMXC__Phone__c = '123789'
                                                            );
                insert sgm2;
                
                sgm3 = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user2.Id,
                                                            Send_Notification__c = 'SMS and Email',
                                                            SVMXC__Email__c = 'sgm3.test@company.com',
                                                            SVMXC__Phone__c = '123489'
                                                            );
                insert sgm3;
            }
        }
   
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder.Work_Order_Category__c='On-site';                 
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'Energy';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
        // workOrder.Customer_Location__c = Cust_Location.Id;
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         insert workOrder;
         
         SVMXC__Service_Order__c workOrder1 =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder1.Work_Order_Category__c='On-site';                 
         workOrder1.SVMXC__Order_Type__c='Maintenance';
         workOrder1.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder1.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder1.CustomerRequestedDate__c = Date.today();
         workOrder1.Service_Business_Unit__c = 'Energy';
         workOrder1.SVMXC__Priority__c = 'Normal-Medium';
        // workOrder1.Customer_Location__c = Cust_Location.Id;
         workOrder1.SendAlertNotification__c = true;
         workOrder1.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         database.insert (workOrder1,false);
         AssignedToolsTechnicians__c technician1 = Utils_TestMethods.CreateAssignedTools_Technician(WorkOrder.id, sgm1.id);
         technician1.Status__c = Label.CLDEC13SRV06;
         try{
         	//insert technician1;
        	database.insert (technician1,false);
         }
         catch(Exception ex){
         	
         }
         AssignedToolsTechnicians__c technician2 = Utils_TestMethods.CreateAssignedTools_Technician(workOrder1.id, sgm2.id);
         technician2.Status__c = Label.CLDEC13SRV06;
         database.insert (technician2,false);
        
        AP_WOFSRNotification.getPickList();
        AP_WOFSRNotification.NotifyTechnicians(workOrder.id);
         AP_WOFSRNotification.NotifyTechnicians(workOrder1.id);
       
    }
}