Public class VFC_NewCCCAction{
    
    /*=======================================
      CONSTRUCTOR
    =======================================*/   
    
    // VFC_NewCCCAction standard controller extension
    public VFC_NewCCCAction(ApexPages.StandardController controller){
        System.debug('VFC_NewCCCAction.Constructor.INFO - Constructor is called.');
        System.debug('VFC_NewCCCAction.Constructor.INFO - End of constructor.'); 
    }
    /*=======================================
      Action Method, Called from VF Page
     =======================================*/   
    
    public pagereference goToCCCActionEditPage(){
        System.Debug('VFCNewCCCAction goToCCCActionEditPage - Method is called.');
        // Create a new edit page for the CCC Action 
        PageReference cccActionNewEditPage = new PageReference('/'+SObjectType.CCCAction__c.getKeyPrefix()+'/e' );

        try {
            String retURL = system.currentpagereference().getParameters().get('retURL'); // Get return URL paramater
            String caseName = System.currentpagereference().getParameters().get(Label.CLSEP12CCC02); // Get the Case Name
            String CaseId   = System.currentpagereference().getParameters().get(Label.CLSEP12CCC03); // Get the Case ID
            
            System.debug('### caseName ###: ' +  caseName);
            System.debug('### CaseId ###:' + CaseId);
            
            // Set the return URL to the edit page with the value from the current visual force page
            if(retURL  != null) {
                cccActionNewEditPage.getParameters().put(Label.CL00330, retURL);  
                System.Debug('VFCNewCCCAction.goToCCCActionEditPage.SUCCESS - Return URL (retURL) =' +retURL);
            }
            else if(CaseId !=null && CaseId !='' ){
                cccActionNewEditPage.getParameters().put(Label.CL00330, '/'+ CaseId);  
                System.Debug('VFCNewCCCAction.goToCCCActionEditPage.WARNING - Return URL is null, Return to Case, if Case Id is not null');
            }
            else{
                cccActionNewEditPage.getParameters().put(Label.CL00330, '/'+SObjectType.CCCAction__c.getKeyPrefix()+'/o');  
                System.Debug('VFCNewCCCAction.goToCCCActionEditPage.WARNING - Return URL and Case Id are null, Return to CCC Action');
            }
            
            
            // Set the the contact ID for the next page with the ID get through the URL for the current page
            if(CaseId != null){
                cccActionNewEditPage.getParameters().put(Label.CLSEP12CCC02, caseName);  
                cccActionNewEditPage.getParameters().put(Label.CLSEP12CCC03, CaseId);
            }
            else {
                System.Debug('VFCNewCCCAction.goToCCCActionEditPage.WARNING - CaseId is null.');
            }
            
            // Pass the User Information value through the URL
            cccActionNewEditPage.getParameters().put(Label.CLSEP12CCC04,UserInfo.getFirstName() + ' ' + UserInfo.getLastName());             
            cccActionNewEditPage.getParameters().put(Label.CLSEP12CCC05,UserInfo.getUserId());
            
            //PrePopulate Due Date with SystemDate + 15 minutes
            DateTime dueDate = System.now();
            cccActionNewEditPage.getParameters().put(Label.CLSEP12CCC14,dueDate.addMinutes(Integer.valueOf(Label.CLSEP12CCC13)).format());
            
            
            // Disable override
            cccActionNewEditPage.getParameters().put(Label.CL00690, Label.CL00691); 
            System.Debug('VFCNewCCCAction.goToCCCActionEditPage.INFO - Override disabled.');
            
        }
        catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                System.debug('VFC_NewCCCAction.goToCCCActionEditPage.'+e.getTypeName()+' - '+ e.getMessage());
                System.debug('VFC_NewCCCAction.goToCCCActionEditPage.'+e.getTypeName()+' - Line = '+ e.getLineNumber());
                System.debug('VFC_NewCCCAction.goToCCCActionEditPage.'+e.getTypeName()+' - Stack trace = '+ e.getStackTraceString());
        }
        System.Debug('VFC_NewCCCAction End of Method.');
        return cccActionNewEditPage;
    }
}