@isTest
public class VFC_ViewGroupPermissions_TEST {
    
    static testMethod void testObjectGroupPermission() {
 
        User thisUser = [SELECT Id FROM User WHERE ProfileId = '00eA0000000uVHP' AND IsActive = true LIMIT 1];

        System.runAs (thisUser) {
            Test.startTest();

            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController psGroupController = new ApexPages.StandardController(permissionSetGroupList.get(0));
            VFC_ViewGroupPermissions viewGroupPermissionController = new VFC_ViewGroupPermissions(psGroupController);        
            
            Test.stopTest();   
        }    
    }    
}