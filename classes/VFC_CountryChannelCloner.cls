public class VFC_CountryChannelCloner {
    public CountryChannels__c conCha;
    public String selectedCountry{get;set;}
    
    public List<SelectOption> getactiveCountryClusters() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        List<PRMCountry__c> prmCntry = new List<PRMCountry__c>([SELECT Id,Name, Country__c, Country__r.CountryCode__c, Country__r.Name, CountryPortalEnabled__c FROM PRMCountry__c WHERE CountryPortalEnabled__c = true]);
        for(PRMCountry__c pCntry : prmCntry){
          options.add(new SelectOption(pCntry.Country__r.CountryCode__c,pCntry.Country__r.Name));
        }    
        return options;
    }
    
    public VFC_CountryChannelCloner(ApexPages.StandardController controller) {
        String[] countryChannelFields = new String[] {'Active__c','F_PRM_Menu__c','AllowPrimaryToManage__c','Channel__c','SubChannel__c','Country__c',
                                                              'PRMCountry__c','PRMCountry__r.CountryCode__c','CompleteUserRegistrationonFirstLogin__c','PredictiveCompanyMatchEnabled__c',
                                                              'F_PRM_ReviewLocatorListing__c','AccountOwner__c','ChannelCode__c','SubChannelCode__c','DefaultLandingPage__c'};
        List<String> conChanFields = new List<String>(countryChannelFields);
        if(!test.isRunningTest()) {
            Controller.addFields(conChanFields);
      conCha = (CountryChannels__c)controller.getRecord();
    } else 
      conCha = [SELECT Active__c,AllowPrimaryToManage__c,Channel__c,SubChannel__c,Country__c,CompleteUserRegistrationonFirstLogin__c,PRMCountry__c,
						PRMCountry__r.CountryCode__c,PredictiveCompanyMatchEnabled__c,F_PRM_ReviewLocatorListing__c,AccountOwner__c,ChannelCode__c,
						SubChannelCode__c,DefaultLandingPage__c FROM CountryChannels__c];
    }
    
    public pagereference cloneAll(){
        system.debug('>>>>'+concha.PRMCountry__r.CountryCode__c+'>>>>'+conCha.Country__c);
        if(string.isBlank(conCha.Country__c) && conCha.PRMCountry__r.CountryCode__c == 'WW' && string.isBlank(selectedCountry)){
            PageReference pageRef = new PageReference('/apex/VFP_GlobalChannelClone?Id='+conCha.Id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        else{
        Savepoint sp = Database.setSavepoint();
        try{
            List<PRMAccountOwnerAssignment__c> accOwnerToCloneList = new List<PRMAccountOwnerAssignment__c>();
            List<Country__c> cntry = new List<Country__c>();
            List<PRMCountry__c> prmCountry = new List<PRMCountry__c>([SELECT Id, Name, CountryCode__c FROM PRMCountry__c WHERE CountryCode__c = :selectedCountry]);
            // Clone Country Channels Start.
            List<CountryChannels__c> conChObjectList = new List<sObject>();
            conchObjectList.add(conCha);
            Id CountryChannelId = conCha.Id;
            
            Schema.SObjectType  conChObjectType = CountryChannelId.getSObjectType();
            SObjectAllFieldCloner soAll = new SObjectAllFieldCloner();
            List<CountryChannels__c> conChListCloned = soAll.cloneObjects(conchObjectList,conChObjectType,False,True);
            String subChannel;
            if(string.isNotBlank(selectedCountry)){
                cntry = [SELECT Id, Name, CountryCode__c FROM Country__c WHERE CountryCode__c = :selectedCountry];
            }
            for(CountryChannels__c conch:conChListCloned){
                subChannel = conch.SubChannel__c;
                conch.TechIsCloned__c = True;
                conch.Active__c = False;
                if(string.isNotBlank(conCha.PRMCountry__c) && conCha.PRMCountry__r.CountryCode__c != 'WW')
                    conch.SubChannel__c = Null;
                else{
                    conch.DefaultLandingPage__c = 'Partnership';
                    conch.CurrencyIsoCode = Null;
                    conch.Country__c = cntry[0].Id;
                    conch.PRMCountry__c = prmCountry[0].Id;
                    conch.F_PRM_Menu__c = null;
                    conch.PredictiveCompanyMatchEnabled__c = false;
                    conch.F_PRM_ReviewLocatorListing__c = false;
                    conch.AccountOwner__c = null;
                }
            }
            insert conchListCloned;
            //Clone Country Channels End.
            
            //Clone CountryChannelFormFields Start
            List<CountryChannelUserRegistration__c> conChaFieldsList = [Select Id from CountryChannelUserRegistration__c where CountryChannels__c =:CountryChannelId];
            Schema.SObjectType  conChFieldsObjectType = conChaFieldsList[0].getSObjectType();
            List<CountryChannelUserRegistration__c> conchFieldsListCloned = soAll.cloneObjects(conChaFieldsList,conChFieldsObjectType,False,True);
            
            for(CountryChannelUserRegistration__c conchFields:conchFieldsListCloned){
                conchFields.CountryChannels__c = conchListCloned[0].id;
                if((conCha.PRMCountry__r.CountryCode__c == 'WW') && conchFields.UserRegFieldName__c == 'companyCurrency')
                    conchFields.DefaultValue__c = '';
                    
            }
            insert conchFieldsListCloned;
            //Clone CountryChannelFormFields end.
            
            //Clone accountOwner assignment Start.
            List<PRMAccountOwnerAssignment__c> accOwnerList = [SELECT Id FROM PRMAccountOwnerAssignment__c WHERE Classification__c = :conChListCloned[0].ChannelCode__c AND SubClassification__c = :subChannel AND Country__c = :conChListCloned[0].Country__c];
            System.debug('***The list values are***' +accOwnerList);
            if(!accOwnerList.isEmpty()){
                accOwnerToCloneList.add(accOwnerList[0]);
                Schema.SObjectType  accOwnerObjectType = accOwnerToCloneList[0].getSObjectType();
                List<PRMAccountOwnerAssignment__c> accOwnerCloned = soAll.cloneObjects(accOwnerToCloneList,accOwnerObjectType,False,True);
                for(integer i=0;i<accOwnerCloned.Size();i++){
                    accOwnerCloned[i].SubClassification__c = Null;
                    accOwnerCloned[i].TECH_LookupKey__c = Null;
                    accOwnerCloned[i].CountryChannel__c = conchListCloned[0].id;
                }
                insert accOwnerCloned;
            }
            if(string.isNotBlank(selectedCountry)){
                System.debug('>>>>pagereference:');
                return new pagereference('/apex/VFP_NewCountryChannel?Id='+conchListCloned[0].Id);
            }
            else{
            return new pagereference('/'+conchListCloned[0].Id);
            }
        }
        Catch(exception e){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            Database.rollback(sp);
            return Null;
        }
        }
    }
}