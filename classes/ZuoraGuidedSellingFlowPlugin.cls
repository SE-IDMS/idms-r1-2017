global class ZuoraGuidedSellingFlowPlugin implements zqu.SelectProductComponentOptions.IGuidedSellingFlowPlugin{  

    public List<Id> getFlows(Id quoteId){
        List <Id> flowIds = new List<Id>();
        
        String quoteType = ApexPages.currentPage().getParameters().get('quoteType');
        String quoteAmendOption = ApexPages.currentPage().getParameters().get('AmendOption');
        
        List<zqu__Quote__c> quoteList = [SELECT Name, zqu__SubscriptionType__c, Entity__c, Type__c, AmendOption__c FROM zqu__Quote__c WHERE Id = :quoteId];
        zqu__Quote__c quote = quoteList.get(0);
        
        List<Zuora_GS_Flow__c> csFlowsList = [SELECT Flow__c FROM Zuora_GS_Flow__c 
                                    WHERE Subscription_Type__c = :quote.zqu__SubscriptionType__c AND Amendment_Type__c = :quote.AmendOption__c  
                                    AND Entity__c = :quote.Entity__c AND Quote_Type__c = :quote.Type__c];
                                    
        Set<String> flowNames = new Set<String>();
        for(Zuora_GS_Flow__c csFlow : csFlowsList){
            flowNames.add(csFlow.Flow__c);
        }

        List<zqu__GuidedSellingFlow__c > flowsList = [SELECT Id FROM zqu__GuidedSellingFlow__c WHERE Name IN :flowNames];
        if(flowsList.isEmpty()) return flowIds;

        // If flows were found, add their ids to the result list
        for(zqu__GuidedSellingFlow__c flow : flowsList) {
            flowIds.add(flow.Id);
        }
        return flowIds;
    }
}