/*****
Cooments :tO update the user to report List 
To Update report list by dialy 
CLJUN16ELLA05 =ELLA - Business Unit User
CLJUN16ELLA06 =ELLA - Country User
CLJUN16ELLA07 =ELLA - View Access
******/

global class Batch_ELLARepUserAddRemove implements Database.Batchable<SObject> ,Schedulable{
    
    set<string>  listActiveLRS = new set<string>();
    
    Set<String> usersWithELLABUAccess = new Set<String>();
    Set<String> usersWithELLACouryAccess = new Set<String>();
    Set<String> usersWithELLAViewAccess = new Set<String>();
    Set<String> usersTESTCLassUser = new Set<String>();
    public string useridTest; 
    public Batch_ELLARepUserAddRemove(Set<String> SettestUsr) {
      usersTESTCLassUser=SettestUsr;
    
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC) {     
          
        String usrwithEllaAcces;
        if(!Test.isRunningTest()) {
            usrwithEllaAcces = 'SELECT Id,Permission_sets_assigned__c,IsActive FROM User'; 
       
        } else {
                usrwithEllaAcces = 'SELECT Id,Permission_sets_assigned__c,IsActive FROM User where id In:usersTESTCLassUser';
             }
        return Database.getQueryLocator(usrwithEllaAcces);
        
    }   
    
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        
        set<id> existingLReportSholder= new set<id>();
        //set<id> exingLReportSholderNotActive= new set<id>();
        map<id,id> exingLReportSholderNotActive= new map<id,id>();
        set<id> listOfuserToAddLRS= new set<id>();
        
        //set<id> listOfuserToUpdateLRS= new set<id>();
        
        map<id,id> listOfuserToUpdateLRS= new map<id,id>();
        List<LaunchReportStakeholders__c> newLReportSholder= new List<LaunchReportStakeholders__c>();
        
        
        //to remove from list
        set<id> listOfuserToRemoveLRS= new set<id>();
        
        //remove frozen user from list
        set<id> setUpdatefrozenUsrToLR = new set<id>();
        set<id> listfzToAddLRS = new set<id>();
        
        //User List id 
        set<id> listUserSet = new set<id>();
        //ELLA - Business Unit User//0PSA0000000LcQE
        
        //ReMove Inactive user From LRS
         map<id,id> mapInActiveUser = new map<id,id>();
        
        //List of permission set Users which are already assigned
        //ELLA - Business Unit User
        for (PermissionSetAssignment psaObj : [SELECT AssigneeId,PermissionSetId FROM PermissionSetAssignment WHERE (PermissionSetId =:label.CLJUN16ELLA05 OR PermissionSetId =:label.CLJUN16ELLA06 OR PermissionSetId =:label.CLJUN16ELLA07)]) { 
            if(psaObj.PermissionSetId==label.CLJUN16ELLA05) {
                usersWithELLABUAccess.add(psaObj.AssigneeId);   
            }

            if(psaObj.PermissionSetId==label.CLJUN16ELLA06) {
                usersWithELLACouryAccess.add(psaObj.AssigneeId);
            }
            if(psaObj.PermissionSetId==label.CLJUN16ELLA07) {
                usersWithELLAViewAccess.add(psaObj.AssigneeId);
            }
                  
        }
        
        //already assigned
        for (SObject usrObj1:scope) {
            
            User usrObj= (User)usrObj1;
            listUserSet.add(usrObj.id);
            //to update is inactive
                if(!usersWithELLABUAccess.contains(usrObj.id) && !usersWithELLACouryAccess.contains(usrObj.id) && !usersWithELLAViewAccess.contains(usrObj.id) ) {
                    listOfuserToRemoveLRS.add(usrObj.id); 
                }           
        }
            
    //}
        
    Map<id,id>  mapExistingLReportSholder = new Map<id,id> (); 
    
        for (LaunchReportStakeholders__c lrsObj : [SELECT Id,CountryReport__c,BUReport__c,IsActive__c,User__c FROM LaunchReportStakeholders__c where IsUnsubscribe__c=false]) {          
            if(lrsObj.IsActive__c) {
                existingLReportSholder.add(lrsObj.User__c);
                mapExistingLReportSholder.put(lrsObj.User__c,lrsObj.Id);
            } else {
                exingLReportSholderNotActive.put(lrsObj.User__c,lrsObj.Id);
                
            }
            
        }   
        
        for(SObject usrObj:scope) {
            User usr= (User)usrObj;
            if(usr.IsActive) {
                if(!existingLReportSholder.contains(usr.id)) {
                    listOfuserToAddLRS.add(usr.id);
                    
                } 
                if(existingLReportSholder.contains(usr.id)) {
                    listfzToAddLRS.add(usr.id);
                    
                }
                
                if(exingLReportSholderNotActive.containskey(usr.id)) {
                    listOfuserToUpdateLRS.put(usr.id,exingLReportSholderNotActive.get(usr.id));
                }
            }else {
                if(mapExistingLReportSholder.containskey(usr.id) && mapExistingLReportSholder.size() >0) {
                    mapInActiveUser.put(usr.id,mapExistingLReportSholder.get(usr.id));
                }   
            }               
            
        }
    
    
        system.debug('listfzToAddLRS'+listOfuserToAddLRS);
        set<id> setuserToLR = new set<id>();  
        //to update existing user
        set<id> setUpdateuserToLR = new set<id>();  
        //List<UserLogin> usersLoglist = [SELECT IsFrozen,UserId FROM UserLogin WHERE UserId IN :listOfuserToAddLRS and IsFrozen=false];    
        if(listOfuserToAddLRS.size() > 0) {
            for(UserLogin luObj:[SELECT IsFrozen,UserId FROM UserLogin WHERE UserId IN :listOfuserToAddLRS and IsFrozen=false]) {
                    setuserToLR.add(luObj.UserId);
                
            }
           
        }
        // and IsFrozen=false
         if(listOfuserToUpdateLRS.size() > 0  || listfzToAddLRS.size() > 0) {
            for(UserLogin luObj:[SELECT IsFrozen,UserId FROM UserLogin WHERE UserId IN :listOfuserToUpdateLRS.keyset() OR UserId IN :listfzToAddLRS ]) {
                    system.debug('listfzToAddLRSluObj'+luObj.IsFrozen);
                    if(!luObj.IsFrozen && listOfuserToUpdateLRS.containskey(luObj.UserId)) {
                        setUpdateuserToLR.add(luObj.UserId);
                    }
                    system.debug('list Inside'+listfzToAddLRS);
                    if(luObj.IsFrozen && listfzToAddLRS.contains(luObj.UserId) ) {
                        setUpdatefrozenUsrToLR.add(luObj.UserId);   
                    } 
                
            }
        
        }
        system.debug('setUpdatefrozenUsrToLR'+setUpdatefrozenUsrToLR);
        LisT<LaunchReportStakeholders__c> LaunchReportUpdate = new list<LaunchReportStakeholders__c>();
        //to remove from existing user from report list
           system.debug('LaunchReportUpdate  4'+LaunchReportUpdate);
        if(listOfuserToRemoveLRS.size() > 0 && mapExistingLReportSholder.size() > 0) {
            for(string str:listOfuserToRemoveLRS) {
                if(mapExistingLReportSholder.containskey(str)) {
                        LaunchReportStakeholders__c updatLRSFasle= new LaunchReportStakeholders__c(id=mapExistingLReportSholder.get(str),IsActive__c=false); 
                        LaunchReportUpdate.add(updatLRSFasle);
                }
            }   
            
        }
        system.debug('LaunchReportUpdate  3'+LaunchReportUpdate);
        //to update
        // LisT<LaunchReportStakeholders__c> LaunchReportUpdate = new list<LaunchReportStakeholders__c>();
        if(setUpdateuserToLR.size() > 0) {
            for(string strUsrLRS:setUpdateuserToLR) {
                LaunchReportStakeholders__c updatLRS= new LaunchReportStakeholders__c(id=listOfuserToUpdateLRS.get(strUsrLRS),IsActive__c=true); 
                LaunchReportUpdate.add(updatLRS);
            }
            
            
        }
        
        system.debug('LaunchReportUpdate  2'+LaunchReportUpdate);
        if(setUpdatefrozenUsrToLR.size() > 0) {
            for(string strFzUsrLRS:setUpdatefrozenUsrToLR) {
                LaunchReportStakeholders__c updatFzLRS= new LaunchReportStakeholders__c(id=mapExistingLReportSholder.get(strFzUsrLRS),IsActive__c=false); 
                LaunchReportUpdate.add(updatFzLRS);
            }
            
            
        }
         system.debug('LaunchReportUpdate  1'+LaunchReportUpdate);
        //Inactive user Update in LCR
         if(mapInActiveUser.size() > 0) {
            for(string InAcuser:mapInActiveUser.keyset()) {
                LaunchReportStakeholders__c updatInActLRS= new LaunchReportStakeholders__c(id=mapInActiveUser.get(InAcuser),IsActive__c=false); 
                LaunchReportUpdate.add(updatInActLRS);
            }
            
            
        }
        system.debug('LaunchReportUpdate  yes'+LaunchReportUpdate);
        if(LaunchReportUpdate.size() > 0) {
            
            Database.SaveResult[] lsr = Database.update(LaunchReportUpdate,false);
            
        }
        
        system.debug('TEST-q--->'+setuserToLR.size());
        LaunchReportStakeholders__c LaunchReport = new LaunchReportStakeholders__c();
        if(setuserToLR.size() > 0) {        
            for(string strset:setuserToLR) {
                
               if(usersWithELLABUAccess.contains(strset)) {
                    LaunchReport = new LaunchReportStakeholders__c();
                    LaunchReport.BUReport__c=true;
                    LaunchReport.User__c=strset;
                    LaunchReport.IsActive__c=true;
                    newLReportSholder.add(LaunchReport);
                   
               } else if (usersWithELLACouryAccess.contains(strset)) {
                    LaunchReport = new LaunchReportStakeholders__c();
                    LaunchReport.CountryReport__c=true;
                    LaunchReport.User__c=strset;
                    LaunchReport.IsActive__c=true;
                    newLReportSholder.add(LaunchReport);
                   
               } else if(usersWithELLAViewAccess.contains(strset)) {
                               
                   LaunchReport = new LaunchReportStakeholders__c();
                    LaunchReport.BUReport__c=true;
                    LaunchReport.User__c=strset;
                    LaunchReport.IsActive__c=true;
                    newLReportSholder.add(LaunchReport);
                   
               }    
                
            }
        
        }
        system.debug('TEST---->'+newLReportSholder.size());
        if(newLReportSholder.size() > 0) {
            try {      
            //insert newLReportSholder; 
                Database.SaveResult[] lsr = Database.insert(newLReportSholder,false);
            }catch(Exception exp) {            
            }  
        }
        
    }
    
    
    global void finish(Database.BatchableContext info) {
        //add user user
    }
    
    //schedule method
    
     global void execute(SchedulableContext sc) {
     
        Set<string> Setid = new  Set<string>();     
        Batch_ELLARepUserAddRemove eruar = new Batch_ELLARepUserAddRemove(Setid);

        //label : CLMAR16ELLAbFO12 = 200
        ID batchprocessid = Database.executeBatch(eruar,Integer.valueOf(System.Label.CLMAR16ELLAbFO12));           
    }
    
    
}