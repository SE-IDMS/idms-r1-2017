public class Helper_SendingErrorEmail {

    public static void sendNotificationEmail(String type, String exMessage) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
        String[] toAddresses = Label.ZuoraErrorEmail.split(';');
        
        mail.setToAddresses(toAddresses);
        
        if (type == 'Contact') {            
            mail.setSubject('Company Name | SALESFORCE - ZUORA: Contact Update error.');
        } else if (type == 'Quote') {
            mail.setSubject('Company Name | SALESFORCE - ZUORA: Quote Update error.');
        } else if (type == 'Subscription') {
            mail.setSubject('Company Name | SALESFORCE - ZUORA: Subscription Update error.');
        }
        
        String textBody = '';
        if (exMessage != null) {
            textBody += 'Exception: ' + exMessage + '\r\n\r\n';  
        }
                            
        mail.setPlainTextBody(textBody);
        try {            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            System.Debug('###### INFO: Notification email sent to: ' + toAddresses);
        } catch(System.EmailException ex) {
            System.Debug('###### ERROR: ' + ex.getMessage());
        }
    }

}