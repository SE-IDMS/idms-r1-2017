public class VFC_EllaOPSConnector{
    public String result {get;set;}
    public static AP_ELLA_TO_OPS_WebServcieWithdrawal.responseMes resMes = new AP_ELLA_TO_OPS_WebServcieWithdrawal.responseMes();
    public String callFor = '';
    public Boolean statusCall;
    public Offer_Lifecycle__c offerRec;
    public VFC_EllaOPSConnector(ApexPages.StandardController stdController){
        this.offerRec = (Offer_Lifecycle__c)stdController.getRecord();
        statusCall = false;
    }
    public static Offer_Lifecycle__c getOfferRec(String OfferId){
        return [Select id,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c,name,SubstitutionOPSJobId__c,OneSubstitutionStatus__c,OPSStatus__c, Withdrawal_Notice_Status__c,WithdrawalOPSJobId__c from Offer_Lifecycle__c where Id =: OfferId limit 1];
    }
    public PageReference callOPSService(){
        system.debug('OfferRecord '+offerRec);
        id profileId =label.CLMAR16ELLAbFO15; //[Select Id,Name from Profile where name = 'System Administrator'].id;
        String recordId = apexpages.currentpage().getparameters().get(Label.CLJUNE15ELLABFO01);  // Id

        callFor = apexpages.currentpage().getparameters().get(Label.CLJUNE15ELLABFO02); // callFor
        String statusinUrl = apexpages.currentpage().getparameters().get(Label.CLJUNE15ELLABFO03); // statusCall
        if(statusinUrl != null && statusinUrl != '')
            statusCall = Boolean.valueOf(statusinUrl);
        system.debug('Call from : '+callFor+ ' Status call : '+statusCall);
        offerRec = getOfferRec(recordId);
         set<Id> userAccSet = new set<Id>();  
         userAccSet = new set<Id>{offerRec.ownerid,offerRec.Segment_Marketing_VP__c,offerRec.Launch_Area_Manager__c,offerRec.Marketing_Director__c,offerRec.Withdrawal_Owner__c};
        system.debug('1212121212'+offerRec);
        if(userAccSet.contains(Userinfo.getuserid()) || userinfo.getProfileid()==profileId ) {
        if(callFor != null && callFor != ''){
            if(callFor == Label.CLMAY15ELLABFO04){ // Withdrawal
                if(!statusCall){
                    if(offerRec.Withdrawal_Notice_Status__c == Label.CLJUNE15ELLABFO04) // Published
                        
                        if(offerRec.WithdrawalOPSJobId__c != null && offerRec.OPSStatus__c == null){
                            //result = Label.CLJUNE15ELLABFO06; // Offer is already synced,so you can't Sync Data to OPS
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLJUNE15ELLABFO06));
                        }
                        else 
                            callWithdrawalPushService();
                    else{
                        //result = Label.CLJUNE15ELLABFO07; // Offer is not validated, you can't Sync Data to OPS
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLJUNE15ELLABFO07));
                    }
                }
                else
                    if(offerRec.OPSStatus__c != null && String.valueOf(offerRec.OPSStatus__c).toLowerCase() == Label.CLJUNE15ELLABFO05.toLowerCase() ){ // Done
                        //result = Label.CLMAY15ELLABFO06;
                        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLMAY15ELLABFO06));
                        callWithdrawalStatusService();
                    }
                    else
                        callWithdrawalStatusService(); 
            }else if(callFor == Label.CLMAY15ELLABFO05){ // Substitution
                if(!statusCall){
                    if(offerRec.Withdrawal_Notice_Status__c == Label.CLJUNE15ELLABFO04) // Published
                        if(offerRec.SubstitutionOPSJobId__c != null && offerRec.OneSubstitutionStatus__c == null){
                            //result = Label.CLJUNE15ELLABFO06; // Offer is already synced,so you can't Sync Data to OPS
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLJUNE15ELLABFO06));
                        }/*else if(offerRec.SubstitutionOPSJobId__c != null && offerRec.OneSubstitutionStatus__c.toLowerCase() == Label.CLJUNE15ELLABFO05.toLowerCase()){
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLJUNE15ELLABFO21)); //Substitution Cannot be Synced multiply times
                        }*/
                        else 
                            callSubstitutionPushService();
                    else{
                        //result = Label.CLJUNE15ELLABFO07; // Offer is not validated, you can't Sync Data to OPS
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLJUNE15ELLABFO07));
                    }
                } 
                else // this is for to check the Status
                    if(offerRec.OneSubstitutionStatus__c != null && String.valueOf(offerRec.OneSubstitutionStatus__c).toLowerCase() == Label.CLJUNE15ELLABFO05.toLowerCase() ){ // done
                        //result = Label.CLMAY15ELLABFO07;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, Label.CLMAY15ELLABFO07));
                    }
                    else
                        callSubstitutionStatusService(); 
            }
        }
        if(resMes != null && (resMes.status != null && resMes.status != '')){
            if(resMes.status == Label.CLJUNE15ELLABFO16){  // Success
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, resMes.message));
            }else if(resMes.status == Label.CLJUNE15ELLABFO12){  // Error
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, resMes.message));
            }else if(resMes.status == Label.CLJUNE15ELLABFO20){ //Warning
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, resMes.message));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Current Job Status is "'+resMes.status+'"'));
            }
        }
        }else {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));
            
        }
        return null;
    } 
    public void callWithdrawalPushService(){
        resMes = AP_bFO_ELLAToInvokeOPSWebservice.invokeOPSWebservice(offerRec.Id);
    }
    public void callSubstitutionPushService(){
        resMes = AP_bFO_ELLAToInvokeOPSWebservice.invokeSubstitutionOPSWebservice(offerRec.Id);
    }
    public void callWithdrawalStatusService(){
        resMes = AP_bFO_ELLAToInvokeOPSWebservice.checkWithdrawalJobStatusInOPS(offerRec.Id, Label.CLMAY15ELLABFO04);
    }
    public void callSubstitutionStatusService(){
        resMes = AP_bFO_ELLAToInvokeOPSWebservice.checkWithdrawalJobStatusInOPS(offerRec.Id, Label.CLMAY15ELLABFO05);
    }  
    public PageReference returnMethod(){
        PageReference pg = new PageReference('/' + offerRec.id);
        return pg; 
    }
}