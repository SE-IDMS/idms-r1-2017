public with sharing class VFC_ProgramProducts {
    
    public list<ProgramProduct__c> lstProgramProducts {get;set;}
   
    public VFC_ProgramProducts(ApexPages.StandardController controller) {
        lstProgramProducts = [Select ProductLineCatalog__r.Name, ProductLineCatalog__r.ProductName__c,PartnerProgram__c FROM ProgramProduct__c where PartnerProgram__c=:controller.getId() ORDER BY ProductLineCatalog__r.ProductName__c ];
    }

}