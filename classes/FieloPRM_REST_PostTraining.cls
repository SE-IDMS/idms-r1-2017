/********************************************************************************************************
    Author: Fielo Team
    Date: 06/03/2015
    Description: REST API that receives a map with Contact external Id as key and a training wrapper 
                 class as value.
                 If the training doesn't exist (object Badge) it creates the Badge and then creates 
                 the Badge Member record for that Training (Badge) and Contact Id.
                 If the Contact Id already has that Badge it doesn't create it.
                 It returns only errors in a map that has Contact external Id as key and a string with the
                 error message as value.
                 If no errors returns an empty map.
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestPostTraining/*') 
global class FieloPRM_REST_PostTraining{  

    /**
    * [postTraining creates training for the list of Contact external Ids]
    * @method   postTraining
    * @Pre-conditions  
    * @Post-conditions 
    * @param    Map<String,Lits<trainingWrapper>>  mapContactExtIdTrainings    [map with Contact external Id as key and a training wrapper class as value]
    * @return   Map<String,String>                                             [map that has Contact external Id as key and a string with the error message as value]
    */
    @HttpPost
    global static Map<String,list<String>> postTraining(Map<String,List<trainingWrapper>> mapContactExtIdTrainings){
        
        return FieloPRM_UTILS_BadgeMember.createTrainingBadgeMembers(mapContactExtIdTrainings);     
    }  
    
    global class trainingWrapper{
        public string name;
        public string uniqueCode;
        public decimal porcentage;
        public Date dateFrom;
        public Date dateTo;
    }  
     
}