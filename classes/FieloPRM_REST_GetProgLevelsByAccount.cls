/********************************************************************************************************
    Author: Fielo Team
    Date: 06/03/2015
    Description: REST API that receives a list of salesforce Account Ids and returns a map with salesforce 
                 Account id as key and a list of Program Levels (badge external id) as value.
                 If an Account is not found it's not included in the return map.
                 If an Account is found but has no Features an empty list is return as value.
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestGetProgramLevelsByAccount/*')
global class FieloPRM_REST_GetProgLevelsByAccount{  

    /**
    * [getProgramLevelsByAccount returns program levels for a list of Account Ids]
    * @method   getProgramLevelsByAccount
    * @Pre-conditions  
    * @Post-conditions 
    * @param    List<String>               listAccountIds  [list of salesforce Account Ids]
    * @return   Map<String,List<String>>                   [map with salesforce Account id as key and a list of Program Levels (badge external id) as valu]
    */
    @HttpPost
    global static Map<String, map<String,String>> getProgramLevelsByAccount(List<String> listAccountPRMUIMSIds){
    
        return FieloPRM_UTILS_BadgeMember.getProgramLevelsByAccount(listAccountPRMUIMSIds);     
    }  
     
}