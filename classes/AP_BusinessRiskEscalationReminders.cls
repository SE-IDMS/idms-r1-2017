public class AP_BusinessRiskEscalationReminders{
//The class created as pert of October 2015 release.
//Created by Ram Chilukuri , BR-2417
//If the BRE opens more than 16 days this class sennds reminder mails to respective stakeholders.

  public void createBusinessRiskStakeholder(List<BusinessRiskEscalations__c> brisk)
 {
   // List<BusinessRiskEscalations__c> brisk=[select id, GSAAffected__c ,AffectedCustomer__c,CountryQualityManager__c,TECH_OriginatingEntity__c,TECH_OriginatingSubEntity__c,TECH_OriginatingLocation__c, ResolutionLeader__c, BusinessRiskSponsor__c from BusinessRiskEscalations__c where id in : setofIDsOpen];
    map<string,string>mapBreIdNOrgCntry=new map<string,string>(); // BRE id and Orgination country name.
    map<string,set<string>> mapBreIdAllUserids=new map<string,set<string>>();   // All User ids.
    list<InvolvedOrganization__c> listInvolvedOrgs=new list<InvolvedOrganization__c>();
    list<BusinessRiskEscalationEntity__c> businessRiskEntityList=new list<BusinessRiskEscalationEntity__c>();
    map<string,set<string>>mapBreIdInvOrgEntity=new map<string,set<string>>(); // BRE id and Involved Orgination Entitiy name.
    map<string,set<string>>mapBreIdInvOrgName=new map<string,set<string>>(); // BRE id and Involved Orgination name.
    map<string,set<string>>mapBreIdOrgnatingCounty=new map<string,set<string>>(); // BRE id and Orginating country org name.
    map<string,BusinessRiskEscalationEntity__c>mapOrgidNname=new map<string,BusinessRiskEscalationEntity__c>(); // Organization Name and organiztion.
     map<string,List<EntityStakeholder__c>> entityStakeholderList = new map<string,List<EntityStakeholder__c>>();
    set<string> allOrgNames=new set<string>();
        for(BusinessRiskEscalations__c BussRisk :bRisk){
            mapBreIdNOrgCntry.put(BussRisk.id,BussRisk.TECH_OriginatingEntity__c+' - '+BussRisk.TECH_OriginatingSubEntity__c+' - '+BussRisk.TECH_OriginatingLocation__c);
            if(mapBreIdAllUserids.containskey(BussRisk.id) )
                    {
                        mapBreIdAllUserids.get(BussRisk.id).add(BussRisk.ResolutionLeader__c);
                        mapBreIdAllUserids.get(BussRisk.id).add(BussRisk.BusinessRiskSponsor__c);
                        mapBreIdAllUserids.get(BussRisk.id).add(BussRisk.CountryQualityManager__c);
                    }
                   else
                    {
                        mapBreIdAllUserids.put(BussRisk.id, new Set <string>());
                        mapBreIdAllUserids.get(BussRisk.id).add(BussRisk.ResolutionLeader__c);
                        mapBreIdAllUserids.get(BussRisk.id).add(BussRisk.BusinessRiskSponsor__c);
                        mapBreIdAllUserids.get(BussRisk.id).add(BussRisk.CountryQualityManager__c);
                    }
        }
    listInvolvedOrgs=[select id, Organization__r.name,Tech_Entity__c,Tech_SubEntity__c,BusinessRiskEscalation__c from InvolvedOrganization__c where BusinessRiskEscalation__c in: mapBreIdNOrgCntry.keyset() ];
    if(listInvolvedOrgs.size()>0){
        for(InvolvedOrganization__c inOrg:listInvolvedOrgs){
        
            if(mapBreIdInvOrgEntity.containskey(inOrg.BusinessRiskEscalation__c) )
                    {
                        mapBreIdInvOrgEntity.get(inOrg.BusinessRiskEscalation__c).add(inOrg.Tech_Entity__c);
                    }
                   else
                    {
                        mapBreIdInvOrgEntity.put(inOrg.BusinessRiskEscalation__c, new Set <string>());
                        mapBreIdInvOrgEntity.get(inOrg.BusinessRiskEscalation__c).add(inOrg.Tech_Entity__c);
                    }
                        if(mapBreIdInvOrgName.containskey(inOrg.BusinessRiskEscalation__c) )
                    {
                        mapBreIdInvOrgName.get(inOrg.BusinessRiskEscalation__c).add(inOrg.Organization__r.name);
                    }
                   else
                    {
                        mapBreIdInvOrgName.put(inOrg.BusinessRiskEscalation__c, new Set <string>());
                        mapBreIdInvOrgName.get(inOrg.BusinessRiskEscalation__c).add(inOrg.Organization__r.name);
                    }
            
            }
        }
        allOrgNames.addAll(mapBreIdNOrgCntry.values());
         If( mapBreIdInvOrgEntity.Size()>0 || mapBreIdInvOrgEntity !=null)
             {
             allOrgNames.addAll(getOrganizationIdsSet(mapBreIdInvOrgEntity));
            }
        //allOrgNames.addAll(mapBreIdInvOrgEntity.values());
        If( mapBreIdInvOrgName.Size()>0 || mapBreIdInvOrgName !=null)
             {
                allOrgNames.addAll(getOrganizationIdsSet(mapBreIdInvOrgName));
              }
              mapBreIdOrgnatingCounty=getOriginatingEntityCountry(mapBreIdInvOrgEntity,brisk);
              system.debug('*********mapBreIdOrgnatingCounty'+mapBreIdOrgnatingCounty);
               If( mapBreIdOrgnatingCounty.Size()>0 || mapBreIdOrgnatingCounty !=null)
                {
                allOrgNames.addAll(getOrganizationIdsSet(mapBreIdOrgnatingCounty));
              }
        //allOrgNames.addAll(mapBreIdInvOrgName.values());
        
    businessRiskEntityList = [select Id, Name, Entity__c, SubEntity__c, Location__c, Location_Type__c, Country__c, TECH_SearchESC__c    
                                                        from BusinessRiskEscalationEntity__c where Name In: allOrgNames AND Location_Type__c!=:Label.CLMAY13I2P10];
    for(BusinessRiskEscalationEntity__c orgname:businessRiskEntityList)
        {
            mapOrgidNname.put(orgname.name,orgname);
        }
        system.debug('*********mapOrgidNname'+mapOrgidNname);
        if(mapOrgidNname.size()>0){
        Set<String> tempSet = new Set<String>();
        tempSet.addAll(mapOrgidNname.keyset());
            entityStakeholderList = getOrgStakeholders(tempSet);
            }
            if(entityStakeholderList.size()>0){
            for(BusinessRiskEscalations__c BussRisk :bRisk){
            Set<String> tsthuser = new Set<String>();
             if(mapBreIdNOrgCntry.containskey(BussRisk.id)){
                if(entityStakeholderList.containskey(mapBreIdNOrgCntry.get(BussRisk.id))){
               for(EntityStakeholder__c tstr:entityStakeholderList.get(mapBreIdNOrgCntry.get(BussRisk.id))){
               if(tstr.Role__c ==Label.CL00317){//Country President
                tsthuser.add(tstr.User__c);
                }
                }}
            if(mapBreIdInvOrgName.containskey(BussRisk.id)){
               for( string tOrgName:mapBreIdInvOrgName.get(BussRisk.id)){
                if(entityStakeholderList.containskey(tOrgName)){
                    for(EntityStakeholder__c tstr:entityStakeholderList.get(tOrgName)){
                        if(tstr.Role__c ==Label.CLAPR15I2P09 || tstr.Role__c ==Label.CLOCT14I2P07 || tstr.Role__c ==Label.CLSEP12I2P18 )
                            {// CLAPR15I2P09-Organization Manager, CLOCT14I2P07-CS&Q Manager,CLSEP12I2P18-I2P Process Advocate
                            tsthuser.add(tstr.User__c);
                            }
                        }
                }}}
                //
                 if(mapBreIdOrgnatingCounty.containskey(BussRisk.id)){
                    for( string tOrgName:mapBreIdOrgnatingCounty.get(BussRisk.id)){
                        if(entityStakeholderList.containskey(tOrgName)){
                            for(EntityStakeholder__c tstr:entityStakeholderList.get(tOrgName)){
                                if(tstr.Role__c ==Label.CL10035)
                                {// CL10035-Business VP
                                    tsthuser.add(tstr.User__c);
                                }
                            }
                    }}}
                //
                if(mapBreIdInvOrgEntity.containskey(BussRisk.id)){
                for( string tOrgName:mapBreIdInvOrgEntity.get(BussRisk.id)){
                    if(entityStakeholderList.containskey(tOrgName)){
                    for(EntityStakeholder__c tstr:entityStakeholderList.get(tOrgName)){
                            if(tstr.Role__c ==Label.CLOCT14I2P07 || tstr.Role__c ==Label.CLSEP12I2P18 )
                                {//CLOCT14I2P07-CS&Q Manager,CLSEP12I2P18-I2P Process Advocate
                                    tsthuser.add(tstr.User__c);
                                }
                            if(BussRisk.GSAAffected__c=='Yes' && (tstr.Role__c ==Label.CLSEP12I2P16 || tstr.Role__c ==Label.CLAPR15I2P09))
                                {//CLSEP12I2P16-GSA CS&Q Manager ,CLAPR15I2P09-Organization Manager,
                                    tsthuser.add(tstr.User__c);
                                }
                        }}}}
                if(mapBreIdAllUserids.containskey(BussRisk.id) )
                    {
                        mapBreIdAllUserids.get(BussRisk.id).addAll(tsthuser);
                        
                    }
                   else
                    {
                        mapBreIdAllUserids.put(BussRisk.id, new Set <string>(tsthuser));
                        //mapBreIdAllUserids.get(BussRisk.id).addAll(tsthuser);
                    }
                system.debug('*********tsthuserBefore'+tsthuser);
                tsthuser.clear();
                system.debug('*********tsthuserAfter'+tsthuser);
                BussRisk.TECH_ReminderOpenDate__c=system.now();
            }}}
            map<string,set<string>> mapaccountusers=accountStakeholders(brisk);
            system.debug('*********mapaccountusers'+mapaccountusers);
            system.debug('*********mapBreIdAllUserids'+mapBreIdAllUserids);
            for(string strmap:mapaccountusers.keyset()) {
                if(mapBreIdAllUserids.containsKey(strmap)) {
                        
                    mapBreIdAllUserids.get(strmap).addALL(mapaccountusers.get(strmap));
                }else {
                     mapBreIdAllUserids.put(strmap, new Set <string>());
                        mapBreIdAllUserids.get(strmap).addAll(mapaccountusers.get(strmap));
                }
            }
        //mapBreIdAllUserids.putall(mapaccountusers);
        system.debug('*********mapBreIdAllUserids1'+mapBreIdAllUserids);
        if(mapBreIdAllUserids.size()>0){
        creataStakeholder(mapBreIdAllUserids);}
        system.debug('*********bRisk'+bRisk);
        update bRisk;
 }
 Public static void  creataStakeholder(Map<string,set<string>> MapBreUserids){
        set<string> allUserIds=new set<string>();
        id usrid;
        list<Business_Risk_Escalation_Alert__c> listBREesclsnReminer=new list<Business_Risk_Escalation_Alert__c>();
        
        allUserIds=getOrganizationIdsSet(MapBreUserids);
        system.debug('RAM---->'+MapBreUserids);
        map<id,user> luser=new map<id,user>([SELECT id,Email from user where id IN :allUserIds]);
         //luser=[SELECT id,Email from user where id IN :allUserIds];
        system.debug('**********luser***'+luser);
            for(string breid:MapBreUserids.keyset()){
                for(string userid:MapBreUserids.get(breid)){
                    Business_Risk_Escalation_Alert__c tempStakeholder = new Business_Risk_Escalation_Alert__c();
                    //usrid=Id.valueOf(userid);
                   tempStakeholder.Business_Risk_Escalation__c = breid;
                   tempStakeholder.EventType__c = 'Reminder Open';
                   tempStakeholder.User_Email__c = luser.get(userid).Email;
                    listBREesclsnReminer.add(tempStakeholder); 
                }}
        if(listBREesclsnReminer.size()>0){
        system.debug('**********listBREesclsnReminer***'+listBREesclsnReminer);
        insert listBREesclsnReminer;
        }
       }
 public static set<string> getOrganizationIdsSet(Map<string,set<string>> MapofInvIdOrgs)
        {
            list<set<string>> myList=MapofInvIdOrgs.values();
            system.debug('myList---->'+myList);
            Set<string> FinalSet = new Set<string>();
            for(set<string> tset: myList)
            {
                FinalSet.addAll(tset);
            }
            return FinalSet;
         } 
Public Static map<string,set<string>> accountStakeholders(List<BusinessRiskEscalations__c> breobjList){
        
        Map<id,BusinessRiskEscalations__c> breMap=new Map<id,BusinessRiskEscalations__c>();
        map<string,string> affectedCustomerId = new map<string,string>();
       // Set<Id> ultimateParentId = new Set<Id>();
        Map<id,id> childparentidmap=new Map<id,id>();
        set<Id> parentidset=new set<Id>();
        
        List<AccountTeammember> atmList=new List<AccountTeammember>(); 
        Map<id,AccountTeammember> aidatrecmap=new Map<id,AccountTeammember>();
        map<string,set<string>> mapBreidNAccoutUsers=new map<string,set<string>>();
        map<string,string>mapBreidUPALBU=new map<string,string>();
        map<string,string>mapUPALBUOrgid=new map<string,string>();
        map<id,Account> affectedCustomerMap=new map<id,Account>();
        map<string,List<EntityStakeholder__c>> entityStakeholderList = new map<string,List<EntityStakeholder__c>>();
        if(breobjList!=null && breobjList.size()>0){
            
            breMap.putAll(breobjList);
            for(BusinessRiskEscalations__c buRisk : breobjList){
              affectedCustomerId.put(buRisk.id,buRisk.AffectedCustomer__c);   
            }
            if(affectedCustomerId.size() >0){
                affectedCustomerMap = new map<id,Account>( [Select Id,Name,UltiParentAcc__c,UltiParentAcc__r.LeadingBusiness__c,UltiParentAcc__r.OwnerId, OwnerId from Account where Id In: affectedCustomerId.values()]);              
            }
            //To fetch Parent Account Ids
            for(BusinessRiskEscalations__c breid:breobjList){
              Account acct= affectedCustomerMap.get(affectedCustomerId.get(breid.id));
               if(mapBreidNAccoutUsers.containskey(breid.id) )
                    {
                        mapBreidNAccoutUsers.get(breid.id).add(acct.OwnerId);
                    }
                   else
                    {
                        mapBreidNAccoutUsers.put(breid.id, new Set <string>());
                        mapBreidNAccoutUsers.get(breid.id).add(acct.OwnerId);
                    }
                if(acct.UltiParentAcc__c != null){
                    //ultimateParentId.add(acct.UltiParentAcc__c);
                    childparentidmap.put(acct.id,acct.UltiParentAcc__c);
                    if(breid.GSAAffected__c=='Yes'){
                    mapBreidUPALBU.put(breid.id,acct.UltiParentAcc__r.LeadingBusiness__c);}
                    if(mapBreidNAccoutUsers.containskey(breid.id) )
                    {
                        mapBreidNAccoutUsers.get(breid.id).add(acct.UltiParentAcc__r.OwnerId);
                    }
                   else
                    {
                        mapBreidNAccoutUsers.put(breid.id, new Set <string>());
                        mapBreidNAccoutUsers.get(breid.id).add(acct.UltiParentAcc__r.OwnerId);
                    }
                   }
                   //else{childparentidmap.put(acct.id,acct.id);}
              }
              
              if(childparentidmap.values()!=null && childparentidmap.values().size()>0)
              {
                for(Id s:childparentidmap.values()){
                    parentidset.add(s);
                }
              }
              if(parentidset!=null && parentidset.size()>0){
                
                atmList = [Select UserId,AccountID from AccountTeammember where AccountID IN : parentidset and TEAMMEMBERROLE =: 'ESP'];
                if(atmList!=null && atmList.size()>0){
                    for(AccountTeammember accteammenb:atmList){
                        aidatrecmap.put(accteammenb.AccountID,accteammenb);
                    }
                    
                }
                
              }
              for(BusinessRiskEscalations__c bre:breobjList){
                if(childparentidmap.containsKey(bre.AffectedCustomer__c)&& bre.GSAAffected__c=='Yes'){ 
                    if(aidatrecmap.containsKey(childparentidmap.get(bre.AffectedCustomer__c))){
                        AccountTeammember accteammember=aidatrecmap.get(childparentidmap.get(bre.AffectedCustomer__c));
                        if(accteammember.UserId != null)
                            {
                                if(mapBreidNAccoutUsers.containskey(bre.id) )
                                {
                                    mapBreidNAccoutUsers.get(bre.id).add(accteammember.UserId);
                                }
                                else
                                {
                                    mapBreidNAccoutUsers.put(bre.id, new Set <string>());
                                    mapBreidNAccoutUsers.get(bre.id).add(accteammember.UserId);
                                }
                            }
                        }           
                        }          
                    }       
                if(mapBreidUPALBU.size()>0){
                set<string>  setStrg = new set<string>();
                for(string strset:mapBreidUPALBU.values()) {
                    setStrg.add(strset);
                }
                    mapUPALBUOrgid=getEnitityOrgNames(setStrg);
                    if(mapUPALBUOrgid.size()>0){
                        Set<String> tempSet1 = new Set<String>();
                        tempSet1.addAll(mapUPALBUOrgid.values());
                        entityStakeholderList = getOrgStakeholders(tempSet1);
                        if(entityStakeholderList.size()>0)
                            {
                                for(string tbreid:mapBreidUPALBU.keyset())
                                {
                                    for(EntityStakeholder__c tshkeholder:entityStakeholderList.get(mapUPALBUOrgid.get(mapBreidUPALBU.get(tbreid))))
                                    {
                                        if(tshkeholder.Role__c==label.CLOCT14I2P16){
                                            if(mapBreidNAccoutUsers.containskey(tbreid) )
                                            {
                                                mapBreidNAccoutUsers.get(tbreid).add(tshkeholder.User__c);
                                            }
                                            else
                                            {
                                            mapBreidNAccoutUsers.put(tbreid, new Set <string>());
                                            mapBreidNAccoutUsers.get(tbreid).add(tshkeholder.User__c);
                                            }
                                        
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        system.debug('*************mapBreidNAccoutUsers'+mapBreidNAccoutUsers);
        return mapBreidNAccoutUsers;
      }
public Static map<string,string> getEnitityOrgNames(set<string> lBUEntityName)
 {
    map<string, string> mapLBUEnityNOrgName = new map<string, string>();
   for(BusinessRiskEscalationEntity__c tempEntity: [Select id, Name ,BusinessUnit__c from BusinessRiskEscalationEntity__c where BusinessUnit__c in: lBUEntityName])
   {
        mapLBUEnityNOrgName.put(tempEntity.BusinessUnit__c,tempEntity.Name);
   } 
system.debug('*** mapLBUEnityNOrgName'+ mapLBUEnityNOrgName);   
 return mapLBUEnityNOrgName;
 }
public Static map<string,List<EntityStakeholder__c>> getOrgStakeholders(SET<string> lBUEntityName)
 {system.debug('*** lBUEntityName'+ lBUEntityName); 
   map<string,List<EntityStakeholder__c>> entityStakeholderList = new map<string,List<EntityStakeholder__c>>();
  for(EntityStakeholder__c stakholder: [Select Id,Name,BusinessRiskEscalationEntity__r.name,Role__c,User__c 
                                                        from EntityStakeholder__c where BusinessRiskEscalationEntity__r.name In: lBUEntityName])
    {
        if(entityStakeholderList.containskey(stakholder.BusinessRiskEscalationEntity__r.name) )
                {
                    entityStakeholderList.get(stakholder.BusinessRiskEscalationEntity__r.name).add(stakholder);
                }
            else
                {
                    entityStakeholderList.put(stakholder.BusinessRiskEscalationEntity__r.name, new list<EntityStakeholder__c>());
                    entityStakeholderList.get(stakholder.BusinessRiskEscalationEntity__r.name).add(stakholder);
                }
    }
system.debug('*** entityStakeholderList'+ entityStakeholderList);   
 return entityStakeholderList;


 }
public Static map<string,set<string>> getOriginatingEntityCountry(map<string,set<string>> mapBreIdInvOrgEntity ,List<BusinessRiskEscalations__c> brisk)
 {system.debug('*** mapBreIdInvOrgEntity'+ mapBreIdInvOrgEntity);
    map<string,set<string>> mapBreIdOriginatingEntityCountry=new map<string,set<string>>();
 set<string> allOrgNames=new set<string>();
 allOrgNames.addAll(getOrganizationIdsSet(mapBreIdInvOrgEntity));
   map<string,list<BusinessRiskEscalationEntity__c>> OrgEntityCoountry = new map<string,list<BusinessRiskEscalationEntity__c>>();
   if(allOrgNames.size()>0){
  for(BusinessRiskEscalationEntity__c tOrgname: [select Id, Name, Entity__c, SubEntity__c, Location__c, Location_Type__c, Country__c, TECH_SearchESC__c    
                                                        from BusinessRiskEscalationEntity__c where Entity__c In: allOrgNames AND Location__c!=null AND Location__c!='' AND Location_Type__c=:Label.CL10057])
    {
        if(OrgEntityCoountry.containskey(tOrgname.Entity__c) )
                {
                    OrgEntityCoountry.get(tOrgname.Entity__c).add(tOrgname);
                }
            else
                {
                    OrgEntityCoountry.put(tOrgname.Entity__c, new list<BusinessRiskEscalationEntity__c>());
                    OrgEntityCoountry.get(tOrgname.Entity__c).add(tOrgname);
                }
    }
system.debug('*** OrgEntityCoountry'+ OrgEntityCoountry); 

for(BusinessRiskEscalations__c BussRisk :bRisk){
    if(mapBreIdInvOrgEntity.containskey(BussRisk.id)){
       for(string torgs:mapBreIdInvOrgEntity.get(BussRisk.id)){
            if(OrgEntityCoountry.containskey(torgs)){
                for(BusinessRiskEscalationEntity__c torg:OrgEntityCoountry.get(torgs)){
                   if(BussRisk.TECH_OriginatingLocation__c==torg.Location__c){
                   
                        if(mapBreIdOriginatingEntityCountry.containskey(BussRisk.id) )
                        {
                            mapBreIdOriginatingEntityCountry.get(BussRisk.id).add(torg.name);
                        }
                    else
                        {
                            mapBreIdOriginatingEntityCountry.put(BussRisk.id, new set<string>());
                            mapBreIdOriginatingEntityCountry.get(BussRisk.id).add(torg.name);
                        }                       
                   }
                }      
            }      
       }
    } 
} system.debug('*** mapBreIdOriginatingEntityCountry'+ mapBreIdOriginatingEntityCountry); 

}  
return mapBreIdOriginatingEntityCountry; 
}   
}