public class AP_AssessmentHandler{
      
    public static void checkDuplicateAssessments(List<Assessment__c> lstAssessment,Map<Id,Assessment__c> exslstAssessment) {
        Map<Id,Recordtype> prgorfRecordType = new Map<Id,RecordType>([SELECT Id FROM RecordType 
                                        WHERE DeveloperName IN ('PRMProgramLevelORF_Extension','PRMProgramApplication','PRMSpecialtyApplication')]);
        
        Map<String, Assessment__c> asmtMap = new Map<String, Assessment__c>();
        Set<id> prgIds = new Set<id>(); //Feb15
        Set<id> levelIds = new Set<id>();//Feb15
        Map<String, Assessment__c> mapNewAssessment = new Map<string,Assessment__c>();
        Set<id> set1stLevelPrg = new Set<id>();
        Set<Id> AsmtId = new Set<id>();
                
        //Feb15
        for (Assessment__c asmt : lstAssessment) {
            set1stLevelPrg.add(asmt.PartnerProgram__c);
            AsmtId.add(asmt.id);
        
        }
        
        List<ProgramLevel__c> listPrgLevel = [Select id,PartnerProgram__c from ProgramLevel__c where PartnerProgram__c = :set1stLevelPrg and Hierarchy__c = '1' ];
        map<id,id> mapPrgLevel = new Map<id,id>();
        for(ProgramLevel__c aLevel :listPrgLevel){
            mapPrgLevel.put(aLevel.PartnerProgram__c,aLevel.id);            
        }
        
        //End Feb15
        
      
            for (Assessment__c asmt : lstAssessment) {
                if ((asmt.Name != null && (exslstAssessment == null || (!exslstAssessment.isEmpty() && asmt.Name != exslstAssessment.get(asmt.Id).Name)))) {
               // if ((asmt.Name != null && exslstAssessment == null) || (asmt.Name != null && !exslstAssessment.isEmpty() && asmt.Name != exslstAssessment.get(asmt.Id).Name)) {         
                    if (asmtMap.containsKey(asmt.Name)) {
                        asmt.Name.addError(Label.CLOCT14PRM66);
                    } 
                    else {
                        asmtMap.put(asmt.Name, asmt);
                    }
                }
                //-------------------------------------------------------------------
                //Feb15
                if(asmt.RecordTypeId == System.label.CLOCT14PRM14 && asmt.Active__c == true){
                
                    String asmKey;
                    if(asmt.ProgramLevel__c == null && mapPrgLevel.containsKey(asmt.PartnerProgram__c))
                    {
                        asmKey = String.valueOf(asmt.PartnerProgram__c) + ':' + String.ValueOf(mapPrgLevel.get(asmt.PartnerProgram__c));
                        levelIds.add(mapPrgLevel.get(asmt.PartnerProgram__c));
                    }   
                    else{
                        asmKey = String.valueOf(asmt.PartnerProgram__c) + ':' + String.valueOf(asmt.ProgramLevel__c) ;
                        levelIds.add(asmt.ProgramLevel__c);
                    }   
                        
                    System.debug('**** Program Key ->'+ asmKey);
                    
                    if (mapNewAssessment.containsKey(asmKey)) 
                        asmt.addError(System.Label.CLFEB15PRM19);
                    else
                        mapNewAssessment.put(asmKey,asmt);

                    prgIds.add(asmt.PartnerProgram__c);
                    
                    //------------------------------------------------          
                    
                }    
                
            }
            for (Assessment__c assessment : [SELECT Id, Name, RecordTypeId FROM Assessment__c WHERE Name IN :asmtMap.KeySet() AND RecordTypeId = :prgorfRecordType.keyset()]) {
                if(asmtMap.containsKey(assessment.Name)){
                    Assessment__c newAssessment = asmtMap.get(assessment.Name);
                    newAssessment.Name.addError(Label.CLOCT14PRM66);
                }   
            }
            
            //FEB15: Checking of Duplicate Level
            //Find the Existing assessment
            System.debug('*** Exist Prg Id-> '+prgIds);
            System.debug('*** Exist Prg Level-> '+levelIds);
            Set<string> setEAssessment = new Set<String>();
                List<Assessment__c> lstExistAssessment = new List<Assessment__c>();
            lstExistAssessment = [Select id,ProgramLevel__c, Partnerprogram__c from Assessment__c 
                                where (ProgramLevel__c = :levelIds OR ProgramLevel__c = null) and Partnerprogram__c = :prgIds and id != :AsmtId and Active__c = true ];
            
            System.debug('*** Exist -> '+lstExistAssessment.size());               
            for(Assessment__c aAssessment :lstExistAssessment){
                String asmNewKey;
                if(aAssessment.ProgramLevel__c == null && mapPrgLevel.containsKey(aAssessment.PartnerProgram__c))
                    asmNewKey = aAssessment.PartnerProgram__c + ':' + mapPrgLevel.get(aAssessment.PartnerProgram__c);
                else
                    asmNewKey = aAssessment.PartnerProgram__c + ':' + aAssessment.ProgramLevel__c ;
                    
                System.debug('**** Program Key exist ->'+   asmNewKey); 
                    
                setEAssessment.Add(asmNewKey);
            }
            
            
            
            for (String asmt : mapNewAssessment.keySet()) {          
                if(setEAssessment.contains(asmt))                    
                    mapNewAssessment.get(asmt).addError(System.Label.CLFEB15PRM19);          
            }
            
            //End Feb15
            
            
    }
    
    // Added for updating Global Program
    public static void SetGlobalProgramApprovalFlag (List<SObject> lSObjects) {
        Schema.SObjectType inType;
        Map<Id, SObject> sAsmIds = new Map<Id, SObject>();
        Map<Id, List<SObject>> sAsmQIds = new Map<Id, List<SObject>>();
        List<PartnerProgram__c> lPrograms = new List<PartnerProgram__c>();
        Map<Id,Integer> AsmtQtnMap = new Map<Id,Integer>();
        RecordType prgorfRecordType = [SELECT Id FROM RecordType 
                                        WHERE DeveloperName = 'PRMProgramApplication' LIMIT 1];
        
        if (lSObjects != null && lSObjects.size() > 0)
            inType = lSObjects[0].getSObjectType();

        if (inType == Assessment__c.SObjectType) {
            sAsmIds = new Map<Id, SObject>(lSObjects);
        } 
        else if (inType == OpportunityAssessmentQuestion__c.SObjectType) {
            for (SObject s : lSObjects) {
                OpportunityAssessmentQuestion__c tmp = (OpportunityAssessmentQuestion__c)s;
                List<SObject> lObj = new List<SObject>();
                if (sAsmQIds.containsKey(tmp.Assessment__c))
                    lObj = sAsmQIds.get(tmp.Assessment__c);

                lObj.add(s);
                sAsmQIds.put(tmp.Assessment__c, lObj);
            }
            //List<Assessment__c> lAsmnts = new List<Assessment__c>([SELECT Id, PartnerProgram__c Total FROM Assessment__c WHERE Id IN :sAsmQIds.keySet()]);
        }
        Set<Id> lAssessmentIds = (inType == Assessment__c.SObjectType ? sAsmIds.keySet() : sAsmQIds.keySet());
        List<AggregateResult> lAggrResults = [SELECT Id,PartnerProgram__c, RecordTypeId, COUNT(Name) Total FROM Assessment__c WHERE RecordTypeId = :prgorfRecordType.Id  AND Id IN :lAssessmentIds  GROUP BY PartnerProgram__c,Id,RecordTypeId];
        List<AggregateResult> lAggrResults1 = [SELECT Assessment__c, Assessment__r.RecordTypeId, COUNT(Name) QTotal FROM OpportunityAssessmentQuestion__c WHERE Assessment__c IN :lAssessmentIds AND Assessment__r.RecordTypeId = :prgorfRecordType.Id AND Required__c = True AND Assessment__r.Active__c = true GROUP BY Assessment__c,Assessment__r.RecordTypeId];
        for(AggregateResult ar1 : lAggrResults1)
            AsmtQtnMap.put(String.valueOf(ar1.get('Assessment__c')),Integer.valueOf(ar1.get('QTotal'))); 
        
        System.debug('Map:'+AsmtQtnMap);  
        System.debug('Active Assessment Count:' + lAggrResults);
        if (lAggrResults != null && lAggrResults.size() > 0) {
            for (AggregateResult ar : lAggrResults) {
              //  if(AsmtQtnMap.containsKey(ar.Id)) { 
                    PartnerProgram__c tPrg = new PartnerProgram__c( 
                                                Id = String.valueOf(ar.get('PartnerProgram__c')),
                                                Tech_Assessment__c = (AsmtQtnMap.containsKey(ar.Id)) ? true : false);
                lPrograms.add(tPrg);
             //  }
             
            }
            
                        if (lPrograms.size() > 0)
                Database.Update(lPrograms);
        }
    }
    
    //-------------------------------------------------------------------------------------------------------
    //Added by Sreenivas Sake for BR-4915
    public static void checkProgramLevel(List<AssessementProgram__c> lstAssessment){
    
        Set<String> newPrgIds = new Set<String>();
        Set<Id> prgIds = new Set<Id>();
        Set<Id> levelIds = new Set<Id>();
        
        //Added by Sreenivas Sake for BR-4915
        Set<Id> assessId = new Set<Id>();
        Map<Id, Id> activeValueMap = new Map<Id, Id>();
        
        Set<String> newUniqueAssessment = new Set<String>();
        Recordtype prgorfRecordType = [SELECT Id FROM RecordType 
                                        WHERE DeveloperName =  'PRMProgramLevelORF_Extension' LIMIT 1];
        
        //Added by Sreenivas Sake for BR-4915 
                for (AssessementProgram__c asm : lstAssessment) {
            assessId.add(asm.Assessment__c);
        }
        List<Assessment__c> activeList = [SELECT Id, RecordTypeId FROM Assessment__c WHERE ID in :assessId and Active__c = true];

        for (Assessment__c activeTrue: activeList) {
            activeValueMap.put(activeTrue.Id, activeTrue.RecordTypeId);
        }
        
        //for (Assessment__c asm : lstAssessment) {
        
        //Added by Sreenivas Sake for BR-4915
        for (AssessementProgram__c asm : lstAssessment) 
        {
            String asmKey = asm.PartnerProgram__c + ':' + asm.ProgramLevel__c + ':' + activeValueMap.containsKey(asm.Assessment__c);
            //String asmKey = asm.PartnerProgram__c + ':' + asm.ProgramLevel__c + ':' + asm.Active__c;
            //if (asm.RecordTypeId == prgorfRecordType.Id && asm.Active__c == true) {
            if (activeValueMap.get(asm.Assessment__c) == prgorfRecordType.Id && activeValueMap.containsKey(asm.Assessment__c)) 
            {
                if (!newUniqueAssessment.contains(asmKey))
                    newUniqueAssessment.add (asmKey);
                else
                    asm.addError(System.Label.CLNOV13PRM001);

                newPrgIds.add(asm.PartnerProgram__c + ':' + asm.ProgramLevel__c); 
            }
            prgIds.add(asm.PartnerProgram__c);
            levelIds.add(asm.ProgramLevel__c);               

        }
        
        Set<String> existingORFAssessments = new Set<String>();
       
        /*List<Assessment__c> lstOldAssessment = new List<Assessment__c>([
                        SELECT Id, Name, PartnerProgram__c, ProgramLevel__c 
                        FROM Assessment__c WHERE Active__c = true AND Id NOT IN :trigger.new AND PartnerProgram__c = :prgIds
                        AND ProgramLevel__c = :levelIds AND RecordTypeId = :prgorfRecordType .id]);*/
                        
                        //Added by Sreenivas Sake for BR-4915 
        List<AssessementProgram__c> lstOldAssessment = new List<AssessementProgram__c>([
                        SELECT Assessment__c, PartnerProgram__c, ProgramLevel__c 
                        FROM AssessementProgram__c WHERE Assessment__r.Active__c = true AND Id NOT IN :trigger.new AND PartnerProgram__c = :prgIds
                        AND ProgramLevel__c = :levelIds AND Assessment__r.RecordTypeId = :prgorfRecordType.id]);

       //for(Assessment__c asm1 : lstOldAssessment){
       for(AssessementProgram__c asm1 : lstOldAssessment){
            existingORFAssessments.add(asm1.PartnerProgram__c + ':' + asm1.ProgramLevel__c);
       }

        //for(Assessment__c tAsm : lstAssessment){
            //if(existingORFAssessments.contains(tAsm.PartnerProgram__c + ':' + tAsm.ProgramLevel__c) && tAsm.Active__c == true){

            //Added by Sreenivas Sake for BR-4915

            for(AssessementProgram__c tAsm : lstAssessment){
            if(existingORFAssessments.contains(tAsm.PartnerProgram__c + ':' + tAsm.ProgramLevel__c) && activeValueMap.containsKey(tAsm.Assessment__c)){
                tAsm.addError(System.Label.CLNOV13PRM001);
            }
        }
    }
}