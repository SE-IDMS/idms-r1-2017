public class VFC_OpenBackOfficeContract 
{
    private final ID ServiceContractId;
    private SVMXC__Service_Contract__c ServiceContract;
    public final String SOLDTO = System.Label.CLAPR15SRV44;
    public static String Url{get;set;}

    public VFC_OpenBackOfficeContract(ApexPages.StandardController controller) 
    {
      ServiceContractId = Controller.getID();
      Url = '';   
    }
    public PageReference doRedirect()
    {
        string parameters ='';
        ServiceContract = [select BackOfficeSystem__c,Name,CountryofBackOffice__c, SoldtoAccount__c, BackOfficeReference__c from SVMXC__Service_Contract__c where id=:ServiceContractId];
       
        if(ServiceContract.BackOfficeSystem__c==null)
        {          
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.CLAPR15SRV12));          
            return null;
        }
        else if(ServiceContract.CountryofBackOffice__c==null)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.CLAPR15SRV12));          
            return null;
        }
        if(ServiceContract.BackOfficeSystem__c == Label.CLAPR15SRV54)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'This Feature is not supported for Oracle BackOffice System'));          
            return null;
        }
        
        //Session ID
        parameters += Label.CLDEC12SRV16+'='+UserInfo.getSessionId(); 
        
        //User ID
        parameters += '&'+ 'uid='+UserInfo.getUserId();  //uid label need to be created !
        
        //BO System
        //parameters += '&'+Label.CLAPR15SRV06+'='+ServiceContract.BackOfficeSystem__c;
        parameters += '&'+ Label.CLDEC12SRV36 +'='+ServiceContract.BackOfficeSystem__c;
        
        //Contract Number
        parameters += '&'+Label.CLAPR15SRV09+'='+ServiceContract.BackOfficeReference__c;
        
        //SAP Code transaction
        parameters += '&'+Label.CLAPR15SRV08+'='+Label.CLAPR15SRV07;
        
        //Country of BO
        parameters += '&'+Label.CLAPR15SRV10+'='+ServiceContract.CountryofBackOffice__c;
        
        //Lang
        parameters += '&'+Label.CLAPR15SRV11+'='+userinfo.getlanguage();
        
        //Sold To Account
        parameters += '&'+SOLDTO +'='+ServiceContract.SoldToAccount__c;
        
        //SFDC Server URL
        parameters += '&'+Label.CLDEC12SRV35+'='+System.URL.getSalesforceBaseURL().getHost();
        
        PageReference  pref = new PageReference(Label.CLAPR15SRV05+'&'+parameters);
        pref.setRedirect(true);
        return pref;
    }

}