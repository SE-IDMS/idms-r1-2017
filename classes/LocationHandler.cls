public without sharing class LocationHandler {

    private static boolean debug = Boolean.valueOf(Label.CLAPR14SRV20);
    
    public static void alignAddressesWithAccounts(List<SVMXC__Site__c> aList) {
        
        debug('START LocationHandler.alignAddressesWithAccounts');
        if (aList != null) {
            debug('Nb of locations: '+aList.size());
                        
            for ( SVMXC__Site__c site: (List<SVMXC__Site__c>) aList) { 
                debug('Current location:'+site);
                
                site.SVMXC__Street__c = site.SVMXC__Account__r.Street__c;
                site.SVMXC__City__c = site.SVMXC__Account__r.City__c;
                if(site.SVMXC__Account__r.StateProvince__c != null)
                    site.StateProvince__c = site.SVMXC__Account__r.StateProvince__c;
                site.SVMXC__Zip__c = site.SVMXC__Account__r.ZipCode__c;
                if(site.SVMXC__Account__r.Country__c != null)
                    site.LocationCountry__c = site.SVMXC__Account__r.Country__c; 
                site.SVMXC__Latitude__c =  site.SVMXC__Account__r.AutomaticGeoLocation__Latitude__s;
                site.SVMXC__Longitude__c =  site.SVMXC__Account__r.AutomaticGeoLocation__Longitude__s;          
                debug('Updated location:'+site);
                
            }
             
            try {
                Database.SaveResult[] results = Database.Update(aList, false);
                
                for(Database.SaveResult result: results) {
                    debug('Yassine Result'+result);
                    if (result.isSuccess()) {
                        debug('Successfully updated location: '+result.getId());
                    }
                    else {
                        for (Database.Error err : result.getErrors()) {
                            
                            debug('Error updating location: '+ result.getId()+'\n\tCode: '+err.getStatusCode()+'\n\t'+'Msg: '+err.getMessage()+'\n\tFields: '+err.getFields());
                        }
                    }
                }
            }
            catch (DmlException dmle) {
                debug('Exception / Error: '+dmle.getMessage());
            }
        }
        else {
            debug('aList is null');
        }
    }
    
    public static void calculateTimeZone(List<SVMXC__Site__c> aList){
    
      debug('START LocationHandler.calculateTimeZone');
      Map<ID,String> accountTimezone = new Map<ID,String>();
      String uniqueTimeZone = '';
      
      if (aList != null) {
        debug('Nb of locations: '+aList.size());
        
        for ( SVMXC__Site__c site: (List<SVMXC__Site__c>) aList) { 
          debug('Current location:'+site); 
          CS_CountryTimeZone__c countryTimeZoneCS = CS_CountryTimeZone__c.getInstance(site.LocationCountry__r.CountryCode__c);
          
          if(countryTimeZoneCS != null && countryTimeZoneCS.TimeZoneCode__c != ''){
            debug('retrieving TimeZone directly from the Custom setting: '+site.LocationCountry__r.CountryCode__c);
            site.TimeZone__c = countryTimeZoneCS.TimeZoneCode__c;
            site.ValidTimeZone__c = True;
          }else{
            debug('Calling Google TimeZone API - Country: '+site.LocationCountry__r.CountryCode__c);
            callGoogleTimeZoneApi(site);
          }
          debug('Updated location:'+site);
          accountTimezone.put(site.SVMXC__Account__r.id, site.timezone__c); 
        }
        
        
        try {
                Database.SaveResult[] results = Database.Update(aList, false);
                
                for(Database.SaveResult result: results) {
                    if (result.isSuccess()) {
                        debug('Successfully updated location: '+result.getId());
                    }
                    else {
                        for (Database.Error err : result.getErrors()) {
                            debug('Error updating location: '+ result.getId()+'\n\tCode: '+err.getStatusCode()+'\n\t'+'Msg: '+err.getMessage()+'\n\tFields: '+err.getFields());
                        }
                    }
                }
        }catch (DmlException dmle) {
                debug('Exception / Error: '+dmle.getMessage());
        }
        
        alignSubLocationTimeZone(accountTimezone);       
     }else{
            debug('aList is null');
     }
   
    }
    
     private static void callGoogleTimeZoneApi(SVMXC__Site__c site) { 
        String GOOGLE_API_URL = Label.CLAPR14SRV15;
        String GOOGLE_PRIVATE_KEY = Label.CLAPR14SRV17;
        String GOOGLE_CLIENT = Label.CLAPR15SRV50;
        //long timestamp = 1331161200;
        Long timestamp = (datetime.now().getTime()/1000);
    
        if(site.SVMXC__Latitude__c != null && site.SVMXC__Longitude__c != null && site.SVMXC__Latitude__c != 0 && site.SVMXC__Longitude__c != 0){
            String baseUrl = GOOGLE_API_URL+'?sensor=true&location='+site.SVMXC__Latitude__c+','+site.SVMXC__Longitude__c+'&timestamp='+timestamp +'&client='+GOOGLE_CLIENT ;
            URL url1 = new URL(baseURL);
            String path =url1.getPath();
            String query=url1.getQuery();
            String input=path+'?'+query;
            
            debug('@unid path '+ path);
            debug('@unid query '+ query);
            debug('@unid input '+ input);
            
            String privateKey= GOOGLE_PRIVATE_KEY;
            privateKey = privateKey.replace('-', '+');
            privateKey = privateKey.replace('_', '/');
            
            Blob decodePK = EncodingUtil.base64Decode(privateKey);
            String algorithmName = 'hmacSHA1';
            Blob hmacData = Crypto.generateMac(algorithmName, Blob.valueOf(input), decodePK);
            String encodePK = EncodingUtil.base64Encode(hmacData);
            encodePK = encodePK.replace('+', '-');
            encodePK = encodePK.replace('/', '_');              
            String signature =EncodingUtil.urlEncode(encodePK, 'UTF-8');
            signature = signature.replace('+', '-');
            signature = signature.replace('/', '_');  
            baseUrl +='&signature='+signature;
            
            //this is the final URL that  can be pasted to browser
            debug('@unid baseURL ='+ baseURL);
            
            HttpRequest req = new HttpRequest(); 
    
                   
            string url = baseUrl;
            req.setEndpoint( url );
            req.setMethod('GET');
            
            
            try {
                Http http = new Http();
                HttpResponse response = null;
                
                if (Test.isRunningTest()){
                    response = new HttpResponse();
                    response.setStatus('200');
                    response.setBody('{"dstOffset" : 0,"rawOffset" : 3600,"status" : "OK","timeZoneId" : "Europe/Paris","timeZoneName" : "Central European Standard Time"}');  
                    response.setStatusCode(200);
                }else{
                    response = http.send(req);
                }
                
                if (response.getStatusCode() != 200 ) { 
                    debug('Code!= 200: '+dumpResponse(response));
                } else {
                    String code = '';
                    JSONParser parser = JSON.createParser(response.getBody());
                    while (parser.nextToken() != null) {
        
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                            (parser.getText() == 'timeZoneId')) {
                            parser.nextToken();
                                if(parser.getText() != null && parser.getText() != ''){
                                    site.TimeZone__c = parser.getText();    
                                    site.ValidTimeZone__c = True;
                                }
                        }
                    }
                } 
            } catch(System.Exception e) {
               debug('Exception / Error when calling Google Api: '+e.getMessage());
            } 
       }else{
         debug('Could not call Google API, latitude and/or longitude are empty');
       }       
    }
    
    public static void alignSubLocationTimeZone(Map<ID,String> accountTimezone){
      debug('START LocationHandler.alignSubLocationTimeZone');
      Set<ID> AccountIds = accountTimezone.keySet();
      debug('Query: '+'SELECT Id, Name, TimeZone__c, SVMXC__Account__r.id FROM SVMXC__Site__c WHERE SVMXC__Parent__c is not null and SVMXC__Account__c in '+AccountIds); 
      List<SVMXC__Site__c> aList = [SELECT Id, Name, TimeZone__c, SVMXC__Account__r.id FROM SVMXC__Site__c WHERE SVMXC__Parent__c <> '' and SVMXC__Account__c in :AccountIds];
      //List<SVMXC__Site__c> subLocationToUpdate = new List<SVMXC__Site__c>();
      
      if (aList != null) {
        debug('Nb of locations: '+aList.size());
        String tmpTimezone = '';
        
        for ( SVMXC__Site__c site: (List<SVMXC__Site__c>) aList) { 
          debug('Current location:'+site); 
          tmpTimezone = accountTimezone.get(site.SVMXC__Account__r.id);
            
          if(tmpTimezone != null && tmpTimezone != ''){
            site.TimeZone__c = tmpTimezone;
            site.ValidTimeZone__c = True;
          }  
          
          //subLocationToUpdate.add(site);
          debug('Updated location:'+site);
        }
        
        try {
          Database.SaveResult[] results = Database.Update(aList, false);
                
          for(Database.SaveResult result: results) {
          
            if (result.isSuccess()) {
               debug('Successfully updated location: '+result.getId());
            }else{
               for (Database.Error err : result.getErrors()) {
                 debug('Error updating location: '+ result.getId()+'\n\tCode: '+err.getStatusCode()+'\n\t'+'Msg: '+err.getMessage()+'\n\tFields: '+err.getFields());
               }
            }
          }
        }catch (DmlException dmle){
          debug('Exception / Error: '+dmle.getMessage());
        }
      }
      
    }
    
    @future (callout=true)
    public static void calculateTimeZoneAsynchronously(List<ID> locationIds) {
    
      debug('START LocationHandler.calculateTimeZoneAsynchronously');
      
       List<SVMXC__Site__c > aList = [SELECT Id, Name, SVMXC__Longitude__c, SVMXC__Latitude__c, TimeZone__c, SVMXC__Account__r.id, LocationCountry__r.CountryCode__c, ValidTimeZone__c FROM SVMXC__Site__c WHERE Id IN :locationIds];
      
      if (aList != null) {
        calculateTimeZone(aList);
      }
    }
    
    private static void debug(String aMsg) {
        if (debug) {
            System.debug('### '+aMsg);
        }
    }
    
    private static String dumpResponse(HttpResponse response) { 
      List<String> error = new List<String>();
        error.add('TIMEZONE ERROR: Could not parse or resolve timezone'); 
        error.add('STATUS:'+response.getStatus());
        error.add('STATUS_CODE:'+response.getStatusCode());
        error.add('BODY: '+response.getBody());
        
        return String.join(error, '\n');
    }

}