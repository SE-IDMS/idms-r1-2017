public class VFC_ReferentialUpdate{
    public String toSObjectName{get;set;}
    public Map<String, String> reRunErrorRecordsOnlyMap{get;set;} //Key: toObject Name, Values: true: Rerun Error Records Only, false: Run All (NOT Error Records), DONT RUN OR NULL: User has NOT selected any option.
    public Map<String, List<ReferentialDataMapping__c>> refMap{get;set;} //Key: toObject name, Value: Records of ReferentialDataMappings for that toObject
    public List<AsyncApexJob> jobList{get;set;} //List of Last 20 Jobs for Batch.
    public String toObjSelected{get;set;} //Name of which to Object is selected
    public Map<String, String> toObjectLastJobId{get;set;} //Last Job Ids for each To Object
    
    public VFC_ReferentialUpdate(){
        
        refMap=new Map<String, List<ReferentialDataMapping__c>>();
        toObjectLastJobId=new Map<String, String>();
        reRunErrorRecordsOnlyMap=new Map<String, String>();
        List<ReferentialDataMapping__c> refDataRecs = [select id, Active__c, FromObject__c, ToObject__c, ToField__c, FromField__c, WhereClauseFromField__c, WhereClauseToField__c from ReferentialDataMapping__c where Active__c=true];
        Set<String> toObjectsSet=new Set<String>(); //To handle Duplicates
        for (ReferentialDataMapping__c refDataRec: refDataRecs){
                if (refDataRec.toObject__c!=null && refDataRec.fromObject__c!=null){
                    //For To Object
                    if (!toObjectsSet.contains(refDataRec.toObject__c)){
                        toObjectLastJobId.put(refDataRec.toObject__c, '');
                        reRunErrorRecordsOnlyMap.put(refDataRec.toObject__c, 'DONT_RUN');
                        toObjectsSet.add(refDataRec.toObject__c);
                    }
                    if( !refMap.keySet().contains(refDataRec.toObject__c) ){
                        refMap.put(refDataRec.toObject__c, new List<ReferentialDataMapping__c>());
                    }
                    List<ReferentialDataMapping__c> refDataMappingForToFromObject=refMap.get(refDataRec.toObject__c);
                    refDataMappingForToFromObject.add(refDataRec);
                }       
        }
    }
    
    public String batchStatus{get;set;}
    public void executeBatch(){
        try {
            Boolean reRunErrorRecordsOnly;
            for (String toObjInMap: reRunErrorRecordsOnlyMap.keySet()){
                if (reRunErrorRecordsOnlyMap.get(toObjInMap)==null) {
                    reRunErrorRecordsOnlyMap.put(toObjInMap, 'DONT_RUN');
                }
            }
            
            String selectedOption = reRunErrorRecordsOnlyMap.get(toObjSelected);
            if (selectedOption!='DONT_RUN'){
                if (selectedOption=='true') {
                    reRunErrorRecordsOnly=true;
                } else if (selectedOption=='false') {
                    reRunErrorRecordsOnly=false;
                }
                Batch_ReferentialUpdateFromVFP batchRef = new Batch_ReferentialUpdateFromVFP(reRunErrorRecordsOnly, toObjSelected);
                ID batchId = Database.executeBatch(batchRef);
                batchStatus='Batch Started, Batch ID: '+batchId;
                toObjectLastJobId.put(toObjSelected, batchId);
                refreshAll();
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLDEC14REF04)); //To be converted to Custom Label.
                batchStatus='';
            }
        } catch (Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            batchStatus='';
        }
    }
    
    
    //This method will be called from ActionPoller
    public PageReference refreshAll(){
        jobList = [SELECT Id,
                             Status,
                             NumberOfErrors,
                             JobItemsProcessed,
                             TotalJobItems,
                             CompletedDate,
                             ExtendedStatus,
                             CreatedDate,
                             //ApexClass.name,
                             //CreatedBy.Email,
                             CreatedBy.Name,
                             jobtype
                             FROM AsyncApexJob 
                      WHERE ApexClass.name='Batch_ReferentialUpdateFromVFP' 
                        AND jobtype='BatchApex'
                      ORDER BY CreatedDate DESC limit 20];
        return null;
    }
    
    public Boolean displayFromTable{get;set;}
    
    public List<ReferentialDataMapping__c> toDisplayInFrom{get;set;}
    
    public void displayFromObjects(){
        displayFromTable=true;
        toDisplayInFrom=new List<ReferentialDataMapping__c>();
            for (ReferentialDataMapping__c currRefDataToDisp: refMap.get(toObjSelected)) {
                toDisplayInFrom.add(currRefDataToDisp);
            }
    }

}