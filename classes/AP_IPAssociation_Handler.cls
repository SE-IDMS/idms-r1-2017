Public class AP_IPAssociation_Handler{
 /**
     * The purpose of this method attachIPForWorkDetails &attachIPForWorkOrders is
     * to Link Install Product in the WD and Main Install Product at the WO Level.
     */ 
    public static void attachIPForWorkOrders (List<SVMXC__Service_Order__c> aListOfWorkOrder) {
        if (aListOfWorkOrder != null) {
        Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>();
            Set<String> serialNumbers = new Set<String>();
            String prodSNSeparator = '@#@#@';
            // First we retrieve the set of serial numbers that we have
            for (SVMXC__Service_Order__c currentWorkOrder : aListOfWorkOrder) {
                if (currentWorkOrder.Shipped_Serial_Number__c != null) {
                    serialNumbers.add(currentWorkOrder.Shipped_Serial_Number__c);
                    woMap.put(currentWorkOrder.Id,currentWorkOrder);
                }
            }
            //myDebug('Set of serial numbers: '+serialNumbers);
            
            Map<String, SVMXC__Installed_Product__c> ipMapSNProd = new Map<String, SVMXC__Installed_Product__c>();
            
            for (SVMXC__Installed_Product__c currentIP : [SELECT id, name, SVMXC__Serial_Lot_Number__c, SVMXC__Product__c from SVMXC__Installed_Product__c where SVMXC__Serial_Lot_Number__c in :serialNumbers and SVMXC__Product__c != null]) {
                //myDebug('currentIP: '+currentIP);
                ipMapSNProd.put(currentIP.SVMXC__Product__c + prodSNSeparator + currentIP.SVMXC__Serial_Lot_Number__c, currentIP);
            }
           // myDebug('Map of IP (Product'+prodSNSeparator+'SerialNumber): '+ipMapSNProd);
            
            for (SVMXC__Service_Order__c currentWorkOrder : aListOfWorkOrder) {
                if (currentWorkOrder.Shipped_Serial_Number__c != null && currentWorkOrder.SVMXC__Product__c != null && ipMapSNProd.containskey(currentWorkOrder.SVMXC__Product__c+prodSNSeparator+currentWorkOrder.Shipped_Serial_Number__c) && ipMapSNProd.get(currentWorkOrder.SVMXC__Product__c+prodSNSeparator+currentWorkOrder.Shipped_Serial_Number__c) != null) {
                currentWorkOrder.SVMXC__Component__c = ipMapSNProd.get(currentWorkOrder.SVMXC__Product__c+prodSNSeparator+currentWorkOrder.Shipped_Serial_Number__c).Id;
                }
            }
           // myDebug('List of work orders: '+aListOfWorkOrder);
            //update aListOfWorkOrder;
            try{
            Database.SaveResult[] updateResults = Database.update(aListOfWorkOrder, false);
                  System.debug('Update Fail Records start');
                  if (updateResults != null){
                        for (Database.SaveResult result : updateResults) {
                            if (!result.isSuccess()) {
                                Database.Error[] errs = result.getErrors();
                                for(Database.Error err : errs){
                                    System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                                }
                            }
                        }
                    }
                 
              System.debug('Update Fail Records end');  
            
            }catch(dmlexception ex){
   
       System.debug('Line Number' + string.valueOf(ex.getLineNumber()));      
       System.debug('exception Message' + ex.getMessage());     
       System.debug('Stack Trace'+ ex.getStackTraceString());      
       System.debug('Type Name' + ex.getTypeName());
            }
            attachIPForWorkDetails (aListOfWorkOrder,woMap);
        }
    }
    private static void attachIPForWorkDetails (List<SVMXC__Service_Order__c> aListOfWorkOrder,Map<Id, SVMXC__Service_Order__c> woMap) {
        Set<Id> workOrders = new Set<Id>();
        workOrders = woMap.keyset();
        String WorkDetailRT = System.Label.CLAPR15SRV60;
        List<SVMXC__Service_Order_Line__c> workOrderDetails = [select id, SVMXC__Service_Order__c,name, Shipped_Serial_Number__c, SVMXC__Product__c , SVMXC__Serial_Number__c from SVMXC__Service_Order_Line__c where Shipped_Serial_Number__c != null AND SVMXC__Product__c  != null AND RecordTypeId =:WorkDetailRT AND SVMXC__Service_Order__c In :workOrders];
                if (workOrderDetails != null) {
            
            Set<String> wdserialNumbers = new Set<String>();// added for WD Check.
            String prodSNSeparator = '@#@#@';
            // First we retrieve the set of serial numbers that we have
            for (SVMXC__Service_Order_Line__c currentWorkOrderDetails : workOrderDetails) {
                if (currentWorkOrderDetails.Shipped_Serial_Number__c != null) {
                    wdserialNumbers.add(currentWorkOrderDetails.Shipped_Serial_Number__c);
                }
            }
           // myDebug('Set of serial numbers: '+wdserialNumbers);
            
            Map<String, SVMXC__Installed_Product__c> ipMapSNProd = new Map<String, SVMXC__Installed_Product__c>();

            
            for (SVMXC__Installed_Product__c currentIP : [SELECT id, name, SVMXC__Serial_Lot_Number__c, SVMXC__Product__c from SVMXC__Installed_Product__c where SVMXC__Serial_Lot_Number__c in :wdserialNumbers and SVMXC__Product__c != null]) {
                //myDebug('currentIP: '+currentIP);
                ipMapSNProd.put(currentIP.SVMXC__Product__c + prodSNSeparator + currentIP.SVMXC__Serial_Lot_Number__c, currentIP);

            }
           // myDebug('Map of IP (Product'+prodSNSeparator+'SerialNumber): '+ipMapSNProd);
            
            for (SVMXC__Service_Order_Line__c currentWorkOrderLine : workOrderDetails) {
                if (currentWorkOrderLine.Shipped_Serial_Number__c != null && currentWorkOrderLine.SVMXC__Product__c  != null && ipMapSNProd.containskey(currentWorkOrderLine.SVMXC__Product__c + prodSNSeparator+currentWorkOrderLine.Shipped_Serial_Number__c) && ipMapSNProd.get(currentWorkOrderLine.SVMXC__Product__c +prodSNSeparator+currentWorkOrderLine.Shipped_Serial_Number__c) != null) {
                    
                    currentWorkOrderLine.SVMXC__Serial_Number__c = ipMapSNProd.get(currentWorkOrderLine.SVMXC__Product__c +prodSNSeparator+currentWorkOrderLine.Shipped_Serial_Number__c).Id;
                }
            }
            //myDebug('List of work order details: '+workOrderDetails);
           // update workOrderDetails;
            try{
            Database.SaveResult[] updateResults = Database.update(workOrderDetails, false);
                  System.debug('WD Update Fail Records start');
                  if (updateResults != null){
                        for (Database.SaveResult result : updateResults) {
                            if (!result.isSuccess()) {
                                Database.Error[] errs = result.getErrors();
                                for(Database.Error err : errs){
                                    System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                                }
                            }
                        }
                    }
                 
              System.debug('WD Update Fail Records end');  
            
            }catch(dmlexception ex){
   
       System.debug('Line Number' + string.valueOf(ex.getLineNumber()));      
       System.debug('exception Message' + ex.getMessage());     
       System.debug('Stack Trace'+ ex.getStackTraceString());      
       System.debug('Type Name' + ex.getTypeName());
   
   }
}
    
}
}