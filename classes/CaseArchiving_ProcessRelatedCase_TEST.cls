@isTest
private class CaseArchiving_ProcessRelatedCase_TEST{

    static testMethod void relatedCaseTestMethod(){
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts);
             
        contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
        Database.insert(con);     
    
        Case case1=Utils_TestMethods.createCase(accounts.id,con.id,'closed');
        case1.Subject += 'swap'; 
        Database.insert(case1);
        System.debug('Case1->'+case1);
        
        
        Case case2=Utils_TestMethods.createCase(accounts.id,con.id,'closed');
        case2.relatedSupportCase__c=case1.id;
        case2.Subject += 'swap2';
        Database.insert(case2);
        System.debug('Case2->'+case2);
        System.debug('Case2 related case number='+case2.relatedSupportCase__r.caseNumber);
        
        List<Case> cseList = [Select Id,CaseNumber from Case];
        System.debug('Caselist-> '+cseList);
        
        
        ArchiveCase__c AcaseObj = new  ArchiveCase__c();
        String name1 = (String)cseList[0].CaseNumber;
        AcaseObj.Name= name1;
        Acaseobj.subject__c='Test Case please Ignore';
        Database.insert(AcaseObj);
        System.debug('Archive Case1->'+AcaseObj);
        
        ArchiveCase__c AcaseObject = new  ArchiveCase__c();
        String name2 = (String)cseList[1].CaseNumber;
        AcaseObject.Name= name2;
        AcaseObject.subject__c='Test Case please Ignore This ArchiveCase';
        //AcaseObject.relatedSupportCase__c = AcaseObj.id;
        Database.insert(AcaseObject);
        System.debug('Archive Case2->'+AcaseObject);
        
        ArchiveCase__c AcaseObj1 = new  ArchiveCase__c();
        AcaseObj1.Name= '12345';
        Acaseobj1.subject__c='Test Case please Ignore';
        Database.insert(AcaseObj1);
        System.debug('Archive Case3->'+AcaseObj1);
        
        ArchiveCase__c AcaseObject1 = new  ArchiveCase__c();
        AcaseObject1.Name='123456';
        AcaseObject1.relatedSupportCase__c = AcaseObj1.Id;
        AcaseObject1.subject__c='Test Case please Ignore This ArchiveCase';
        Database.insert(AcaseObject1);
        System.debug('Archive Case4->'+AcaseObject1);
        
        Test.startTest();
        CaseArchiving_ProcessRelatedCase caprc = new CaseArchiving_ProcessRelatedCase(); 
        ID batchprocessid =Database.executeBatch(caprc);
        Test.stopTest();
        
       
        

    }
    static testmethod void ProcessRelatedCaseScheduleTestmethod() {
        Test.startTest();
        String CRON_EXP = '0 0 0 1 1 ? 2025';  
        String jobId = System.schedule('Schdule casearchiving_processrelatedcase', CRON_EXP, new CaseArchiving_ProcessRelatedCaseSchedule() );
        Test.stopTest();
    }

}