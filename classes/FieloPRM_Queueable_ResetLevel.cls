/********************************************************************
* Company: Fielo
* Created Date: 09/07/2016
* Description:
********************************************************************/
public with sharing class FieloPRM_Queueable_ResetLevel implements Queueable {
 
    public static List<FieloEE__BadgeMember__c> listBadgeMember {get;set;}

    public FieloPRM_Queueable_ResetLevel(list<FieloEE__BadgeMember__c> listID){
        listBadgeMember = listId;
    }

    public void execute(QueueableContext context) {
        resetLevels();
    }

    public static void resetLevels(){
        set<String> setIdMembers = new set<String>();
        set<String> setIdBadges = new set<String>();

        FieloCH.ChallengeUtil.getMapRewardsByPosition(new FieloCH__Challenge__c());

        for(FieloEE__BadgeMember__c badgeMember : listBadgeMember){
            setIdMembers.add(badgeMember.FieloEE__Member2__c);
            setIdBadges.add(badgeMember.FieloEE__Badge2__c);
        }
        
        list<FieloCH__ChallengeReward__c> listChallengeRewards = [SELECT ID, FieloCH__Badge__c, FieloCH__Challenge__c , FieloCH__LogicalExpression__c, F_PRM_ExpirationType__c 
                FROM FieloCH__ChallengeReward__c WHERE FieloCH__Badge__c in : setIdBadges];

        set<id> setIdChallenge = new set<id>();
        map<id,FieloCH__ChallengeReward__c> mapBadgeChallenge = new map<id,FieloCH__ChallengeReward__c>();
        map<id,set<integer>> mapChallengeMissions = new map<id,set<integer>>();
        for(FieloCH__ChallengeReward__c chReward : listChallengeRewards){
            setIdChallenge.add(chReward.FieloCH__Challenge__c);
            mapBadgeChallenge.put(chReward.FieloCH__Badge__c , chReward);
            String conditionString = chReward.FieloCH__LogicalExpression__c ;
            
            if(chReward.F_PRM_ExpirationType__c != null){
                conditionString = conditionString.replace(' ','');
                List<String> resultList = conditionString.split('[^0-9]+');
                resultList.remove(0);
                set<integer> setMissionsAux = new set<integer>();
                for(String r: resultList){
                    setMissionsAux.add(Integer.valueOf(r));
                }    
                mapChallengeMissions.put(chReward.FieloCH__Challenge__c, setMissionsAux);
            }
        }

        Set<Id> setChallengeMissions = new Set<Id>();
        list<FieloCH__ChallengeMission__c> listChallengeMission;
        try{
            listChallengeMission = Database.query('SELECT Id, FieloCH__Mission2__c, FieloCH__Order__c, FieloCH__MaxValue__c, FieloCH__Summary__c, FieloCH__Status__c, FieloCH__Counter__c, FieloCH__Challenge2__c FROM FieloCH__ChallengeMission__c WHERE FieloCH__Challenge2__c IN: setIdChallenge');
        }catch(Exception e){
            listChallengeMission = Database.query('SELECT Id, FieloCH__Mission__c, FieloCH__Order__c, FieloCH__MaxValue__c, FieloCH__Summary__c, FieloCH__Status__c, FieloCH__Counter__c, FieloCH__Challenge2__c FROM FieloCH__ChallengeMission__c WHERE FieloCH__Challenge2__c IN: setIdChallenge');
        }

        for(FieloCH__ChallengeMission__c chMission : listChallengeMission){
            if(mapChallengeMissions.containskey(chMission.FieloCH__Challenge2__c)){
                set<integer> setMissionsAux = mapChallengeMissions.get(chMission.FieloCH__Challenge2__c);
                if(setMissionsAux.contains(integer.valueof(chMission.FieloCH__Order__c))){                   
                    setChallengeMissions.add(chMission.Id);                    
                }
            }
        }

        list<FieloCH__ChallengeMissionMember__c> listChallengeMissionMember = [SELECT Id, FieloCH__ChallengeMission__c, FieloCH__MaxValue__c, FieloCH__Summary__c, FieloCH__Status__c, FieloCH__Counter__c FROM FieloCH__ChallengeMissionMember__c WHERE FieloCH__ChallengeMission__c IN: setChallengeMissions AND FieloCH__ChallengeMember__r.FieloCH__Member__c in : setIdMembers];

        for(FieloCH__ChallengeMissionMember__c chMissionMember: listChallengeMissionMember){
            chMissionMember.FieloCH__MaxValue__c = 0;
            chMissionMember.FieloCH__Summary__c = 0;
            chMissionMember.FieloCH__Status__c =  null;
            chMissionMember.FieloCH__Counter__c = 0;
        }
        update listChallengeMissionMember;
    }
   
}