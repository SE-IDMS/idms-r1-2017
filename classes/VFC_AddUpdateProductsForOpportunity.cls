global class VFC_AddUpdateProductsForOpportunity {
  public String searchText{get;set;}
  @RemoteAction
  global static List<Sobject> retrieveCurrencyInformation(String idParameter)
  {
    String idType =Id.valueOf(idParameter).getSObjectType()+'';        
    return Database.query('select toLabel(CurrencyISOCode) from '+idType+' where id=\''+idParameter+'\'');
  }

  @RemoteAction
  global static List<Object> performSave(String jsonSelectStringArray,String idParameter,String mode){    
    // while creating parent line - populate Opp Id, currency, Line Closed date
    // while creating child line - populate Parent Line, Line Closed Date, Line Type, Opp Id, Currency
    Date closeDate=null;
    Id OpportunityId=null;
    String CurrencyISOCode=null;
    Id ParentLineId=null;    
    String lineType=null;
    String recordTypeId=null;
    String idType =Id.valueOf(idParameter).getSObjectType()+'';
    String decimalSeperator=null;
    String groupingSeperator=null;               
    Integer multiplicationFactor=1;

    List<SaveResultWrapper> saveResult=new List<SaveResultWrapper>();
    if(mode!='change')
    {
      if(idType=='OPP_ProductLine__c')
      {
        for(OPP_ProductLine__c prodline:[select Opportunity__c,Opportunity__r.CurrencyISOCode,Opportunity__r.CloseDate from OPP_ProductLine__c where id=:idParameter limit 1])
        {
          parentLineId=prodline.id;          
          opportunityId=prodline.Opportunity__c;
          closeDate=prodline.Opportunity__r.CloseDate;
          CurrencyISOCode=prodline.Opportunity__r.CurrencyISOCode;
          lineType='Child Line'; 
          recordTypeId=System.Label.CLOCT13SLS22;
        }
      }
      else if(idType=='Opportunity')
      {
        for(Opportunity opportunityrecord:[select id,CurrencyISOCode,CloseDate from Opportunity where id=:idParameter limit 1])
        {         
          opportunityId=opportunityrecord.id;    
          closeDate=opportunityrecord.CloseDate;
          CurrencyISOCode=opportunityrecord.CurrencyISOCode;
          lineType='Parent Line';
          recordTypeId=System.Label.CLOCT13SLS21;
        }
      }
    }

      List<OPP_ProductLine__c> tobeInsertedProductLines=new List<OPP_ProductLine__c>();
      Map<String,List<OPP_ProductLine__c>> gmrCodeProductLineMap=new Map<String,List<OPP_ProductLine__c>>();          

      List<OPP_ProductLine__c> productLineList=new List<OPP_ProductLine__c>();

      for(Object obj:(List<Object>)JSON.deserializeUntyped(jsonSelectStringArray))
      {      
        Map<String,Object> gmrwrapper=(Map<String, Object>)obj;            
        OPP_ProductLine__c tempOpportunityLine=null;

        if(mode=='change')
          tempOpportunityLine=new OPP_ProductLine__c(Id=idParameter);    
        else
          tempOpportunityLine=new OPP_ProductLine__c(); 
          
          tempOpportunityLine.ProductDescription__c= null;           
          tempOpportunityLine.TECH_CommercialReference__c = null;

        String gmrcode=''; 
        if(gmrwrapper.get('gmrcode')!=null && gmrwrapper.get('gmrcode')!='null')    
          gmrcode=gmrwrapper.get('gmrcode')+'';        
        else if(gmrwrapper.get('jsonkey')!=null && gmrwrapper.get('jsonkey')!='null')
        {
          WS_GMRSearch.gmrFilteredDataBean DBL= (WS_GMRSearch.gmrFilteredDataBean) JSON.deserialize(gmrwrapper.get('jsonkey')+'', WS_GMRSearch.gmrFilteredDataBean.class);                  
          if(DBL.businessLine.value != null)       
            gmrcode = DBL.businessLine.value;        
          if(DBL.productLine.value != null)       
           gmrcode += DBL.productLine.value;        
         if(DBL.strategicProductFamily.label != null)       
          gmrcode += DBL.strategicProductFamily.value;        
        if(DBL.family.label != null)       
          gmrcode += DBL.family.value;            
            tempOpportunityLine.ProductDescription__c= DBL.description;           
            tempOpportunityLine.TECH_CommercialReference__c = DBL.commercialReference; 
      }
      if(mode!='change'){
        tempOpportunityLine.Quantity__c=Integer.valueOf(gmrwrapper.get('quantity')+'');
        //tempOpportunityLine.Amount__c=Decimal.valueOf(gmrwrapper.get('unitprice')+''); 
         //April 2014 release START
        try{
            decimalSeperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).DecimalSeperator__c;
            groupingSeperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).GroupingSeperator__c;
            if(decimalSeperator==null)
                decimalSeperator='';
            if(groupingSeperator==null)
                groupingSeperator='';
        }
        catch( Exception ex ){
            System.debug('exception'); 
            decimalSeperator=CS_CurrencyDecimalSeperator__c.getValues('en_US').DecimalSeperator__c;
            groupingSeperator=CS_CurrencyDecimalSeperator__c.getValues('en_US').GroupingSeperator__c;                
        }
        String amt=(String)gmrwrapper.get('unitprice');
        if(decimalSeperator==','){
            System.debug('Comma Seperator>>>>>>>');
                amt=amt.replace(groupingSeperator,'');
                amt=amt.replace('.','^');//to throw error for dot if its not grouping seperator
                amt=amt.replace(',','.');
        }
        else if (decimalSeperator=='.'){
            System.debug('Dot Seperator>>>>');
            amt=amt.replace(groupingSeperator,'');
        }
        System.debug('amtafter>>>>>'+amt);
        multiplicationFactor=1;
        if(amt.contains('K')||amt.contains('k'))
        {
            amt=amt.replace('K','').replace('k','');
            multiplicationFactor=1000;
        }
        else if(amt.contains('M')||amt.contains('m'))
        {
            amt=amt.replace('M','').replace('m','');
            multiplicationFactor=1000000;
        }
        else if(amt.contains('B')||amt.contains('b'))
        {
            amt=amt.replace('B','').replace('b','');
            multiplicationFactor=1000000000;
        }
        tempOpportunityLine.Amount__c= (Decimal.valueOf(amt)*multiplicationFactor).setScale(2,RoundingMode.HALF_UP); 
        //April 2014 release END          
        tempOpportunityLine.Opportunity__c= OpportunityId;                
        tempOpportunityLine.ParentLine__c=ParentLineId;
        tempOpportunityLine.CurrencyISOCode=CurrencyISOCode;
        tempOpportunityLine.LineClosedate__c=closeDate;
        tempOpportunityLine.LineType__c=lineType;
        tempOpportunityLine.RecordTypeId=recordTypeId;
        
         if(mode == 'addProdFromAgreement')
            tempOpportunityLine.TECH_IsCreatedFromAgr__c=true;   
      }
      if(gmrCodeProductLineMap.containsKey(gmrcode))
        gmrCodeProductLineMap.get(gmrcode).add(tempOpportunityLine);
      else
        gmrCodeProductLineMap.put(gmrcode,new List<OPP_ProductLine__c>{tempOpportunityLine});    

      productLineList.add(tempOpportunityLine);
    }

    for(OPP_Product__c product:[select BusinessUnit__c,Family__c,TECH_PM0CodeInGMR__c,ProductFamily__c,ProductLine__c from OPP_Product__c where IsActive__c=true and TECH_PM0CodeInGMR__c in :gmrCodeProductLineMap.keySet()])
    {
      for(OPP_ProductLine__c simpleProductLine:gmrCodeProductLineMap.get(product.TECH_PM0CodeInGMR__c))
      {        
        simpleProductLine.ProductBU__c=product.BusinessUnit__c;
        simpleProductLine.ProductLine__c=product.ProductLine__c;        
        simpleProductLine.ProductFamily__c=product.ProductFamily__c;
        simpleProductLine.Family__c=product.Family__c;  
        simpleProductLine.Product__c=product.id;             
      }
    }

    try{
      for(Database.UpsertResult result:Database.upsert(productLineList,true))
        saveResult.add(new SaveResultWrapper(result.id,null,result.success));
      return saveResult;
    }  
    catch (System.DmlException e) {
      for (Integer i = 0; i < e.getNumDml(); i++) {        
        saveResult.add(new SaveResultWrapper(null,e.getDmlMessage(i),false)); 
      }
      return saveResult;  
    }
    catch(Exception ex)  
    { 
      for(Integer i=0;i<productLineList.size();i++)   
        saveResult.add(new SaveResultWrapper(null,ex.getMessage(),false));    
      return saveResult;
    }  
  }
  class SaveResultWrapper{
    Id objectId;
    String message;
    Boolean success;
    SaveResultWrapper(Id objectId,String message,Boolean success){
      this.objectId=objectId;
      this.message=message;
      this.success=success;
    }
  }

  public VFC_AddUpdateProductsForOpportunity(ApexPages.StandardSetController controller) { 

  }

  public pagereference cancel()
  {
      if(system.currentPageReference().getParameters().get('agreementId')!=null && system.currentPageReference().getParameters().get('mode')=='addProdFromAgreement')
          return new pagereference('/apex/VFP_MassUpdateOLsAgr?&ID='+system.currentPageReference().getParameters().get('agreementId'));      
      else if(system.currentPageReference().getParameters().get('agreementId')!=null && system.currentPageReference().getParameters().get('mode')=='change')
          return new pagereference('/apex/VFP_MassUpdateOLsAgr?selectedopplne='+system.currentPageReference().getParameters().get('id')+'&ID='+system.currentPageReference().getParameters().get('agreementId'));
      else
          return new pagereference(Site.getPathPrefix()+'/'+system.currentPageReference().getParameters().get('id'));
  }
  @RemoteAction
  global static List<OPP_Product__c> getNextLevel(String gmrPrefix) {
    String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+gmrPrefix+'__\' ORDER BY Name ASC';
    return Database.query(query);       
  }

  @RemoteAction
  global static Object getRecordDetails(String idParameter) {
    for(OPP_ProductLine__c productlinerecord:[select Quantity__c,Amount__c,TECH_CommercialReference__c,Product__r.TECH_PM0CodeInGMR__c,Product__r.Name from OPP_ProductLine__c where id=:idParameter limit 1])
    {
      String gmrcode=productlinerecord.Product__r.TECH_PM0CodeInGMR__c; 
      Set<String> gmrcodes=new Set<String>{'__'};
      if(gmrcode!=null)
      gmrcodes.addAll(new Set<String>{'__',gmrcode.left(2)+'__',gmrcode.left(4)+'__',gmrcode.left(6)+'__'});
      gmrcodes.removeAll(new List<String>{'',null});      
      Map<String,Set<OPP_Product__c>> gmrcodetoproducts=new Map<String,Set<OPP_Product__c>>();
      for(OPP_Product__c product:[select Name,TECH_PM0CodeInGMR__c from OPP_Product__c where TECH_PM0CodeInGMR__c like :gmrcodes and IsActive__c=true order by TECH_PM0CodeInGMR__c])
      {        
        String key=product.TECH_PM0CodeInGMR__c.left(product.TECH_PM0CodeInGMR__c.length()-2);
        if(gmrcodetoproducts.containsKey(key))
          gmrcodetoproducts.get(key).add(product);
        else
          gmrcodetoproducts.put(key,new Set<OPP_Product__c>{product});
      }      
      return new RecordDetailsWrapper(productlinerecord,gmrcodetoproducts);
    } 
    return null;       
  }

  class RecordDetailsWrapper{
    OPP_ProductLine__c productline;
    Map<String,Set<OPP_Product__c>> gmrcodetoproducts;    
    RecordDetailsWrapper(OPP_ProductLine__c productline,Map<String,Set<OPP_Product__c>> gmrcodetoproducts){
      this.productline=productline;
      this.gmrcodetoproducts=gmrcodetoproducts;
    }
  }

  @RemoteAction
  global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeDiscontinuedProducts)
  {
    WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
    WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
    WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

    GMRConnection.endpoint_x=Label.CL00376;
    GMRConnection.timeout_x=40000;
    GMRConnection.ClientCertName_x=Label.CL00616;

    Pagination.maxLimitePerPage=99;
    Pagination.pageNumber=1;
    
    filters.exactSearch=exactSearch;
    filters.onlyCatalogNumber=searchincommercialrefs; 
    filters.excludeDiscontinuedProducts=excludeDiscontinuedProducts;
    filters.excludeMarketingDocumentation=FALSE;
    filters.excludeOld=FALSE;
    filters.excludeSpareParts=FALSE; 
    


filters.keyWordList=searchString.split(' '); //prepare the search string
if(gmrcode != null)
{
  if(gmrcode.length()==2)
  {
    filters.businessLine = gmrcode;            
  }
  else if(gmrcode.length()==4)
  {
   filters.businessLine = gmrcode.substring(0,2);            
   filters.productLine =  gmrcode.substring(2,4);            
 }
 else if(gmrcode.length()==6)
 {
   filters.businessLine = gmrcode.substring(0,2);            
   filters.productLine = gmrcode.substring(2,4);
   filters.strategicProductFamily = gmrcode.substring(4,6);
 }
 else if(gmrcode.length()==8)
 {
   filters.businessLine = gmrcode.substring(0,2);            
   filters.productLine = gmrcode.substring(2,4);
   filters.strategicProductFamily = gmrcode.substring(4,6); 
   filters.Family = gmrcode.substring(6,8);
 }
}
  //if(!Test.isRunningTest()){    
    WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
    return results.gmrFilteredDataBeanList;    
  /*}
  else{
    return null;
  }*/
}


}