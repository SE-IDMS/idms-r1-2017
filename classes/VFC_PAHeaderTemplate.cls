/**
    Author          : Nitin Khunal
    Date Created    : 04/10/2016
    Description     : Controller for VFP_PAHeaderTemplate VF Page
*/
public class VFC_PAHeaderTemplate {
    Public String contactName{get; set;}
    Public String accountName{get; set;}
    Public String address{get; set;}
    Public VFC_PAHeaderTemplate() {
        User userObj = new User();
        userObj = [select Contact.Name, Contact.Account.Name, Contact.Account.Country__r.Name, Contact.Account.BillingCity, Contact.Account.StateProvince__r.StateProvinceCode__c from User where Id =: Userinfo.getUserId()];
        contactName = userObj.Contact.Name;
        accountName = userObj.Contact.Account.Name;
        address = PA_UtilityClass.concatenateAddressWithoutAccount(userObj.Contact.Account.Country__r.Name, userObj.Contact.Account.BillingCity, userObj.Contact.Account.StateProvince__r.StateProvinceCode__c);
    }
}