public class IDMSValidateReCaptcha {
    
    public Static Final String secretKey = Label.CLNOV16IDMS191;
    public Static Final String googleApi = Label.CLNOV16IDMS192;
    
    Public Static boolean validateReCaptcha(String response){
        system.debug('validation process');
        Http h = new Http();        
        HttpRequest req = new HttpRequest();
        String url = googleApi + 'secret=' + secretKey + '&response=' + response;
        system.debug('url: '+url);
        req.setEndpoint(url);
        req.setMethod('GET');
        
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        String result = res.getBody();
        
        String[] results = result.split(',');
        if(results.size() > 0){
            if(results[0].contains('true')){
                return true;
            }
        }
        
        return false;
    }
    
}