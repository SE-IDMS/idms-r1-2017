/*
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016  
* This class provides methods that allow end to end IDMS user activation 
* */
public without sharing class IDMSActivateuser{
    IDMSCaptureError Errorcls;
    
    //This method is used to activate user in IDMS. In case of mobile registration, it is returning the code pin confirmation handler
    public IDMSConfirmPinResponse Activateuser(String id,String FederationIdentifier,string Registration_Source){
        
        IDMSConfirmPinResponse response;
        Errorcls = new IDMSCaptureError(); 
        list <user> users = new list <user>();
        
        try{
            if (Registration_Source == null ||Registration_Source == '' ||( Registration_Source.startswith(' ') &&  Registration_Source.endswith(' '))) {
                return new IDMSConfirmPinResponse(Label.CLJUN16IDMS76,Label.CLQ316IDMS001,null, null); 
            }
            else if(string.isnotblank(Id)){
                users = [Select id,name,username,IDMS_User_Context__c,IDMS_VU_NEW__c,IsActive,FederationIdentifier from user where Id=:Id limit 1];
                if (users.size() == 0 ){
                    return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS40,null,null);
                }
            }   
            else if(string.isnotblank(FederationIdentifier) && (id == null || id == ''))
            {
                users = [Select IDMS_VU_NEW__c, id,name,username,IsActive,IDMS_User_Context__c,FederationIdentifier from user where FederationIdentifier =:FederationIdentifier limit 1];
                if (users.size() == 0 ){
                    return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS39,null,null);
                }   
            }  
            else {
                return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS42,null,null);
            }
            system.debug('users--> ' + users[0]);
            
            if(users.size()>0){
                users[0].IsActive = true;
            }
            else{
                return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS40,'null',null);
            }
            
            Database.SaveResult SaveResult = Database.update(users[0],false);
            if (SaveResult.isSuccess()) {                                      
                response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS94,Label.CLQ316IDMS002,users[0].FederationIdentifier,users[0].id);   
            }    
            else{
                response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS003,users[0].FederationIdentifier,users[0].id);
            }
            
            if(users != null && users.size()>0 && users[0].IsActive == true){
                if(!Registration_Source.equalsignorecase('UIMS')){   
                    system.debug('Entering uims method--> ' + users[0]) ;
                    IDMSEncryption encryption   = new IDMSEncryption();
                    String samlAssrestion       = encryption.ecryptedToken(String.valueOf(users[0].FederationIdentifier),String.valueOf(users[0].IDMS_VU_NEW__c));
                    system.debug('String.valueOf(users[0].FederationIdentifier)-->  ' + String.valueOf(users[0].FederationIdentifier) ) ;
                    system.debug('saml Assertion--> '  +  samlAssrestion) ; 
                    IDMSActivateuser.ActivateUIMSIdentity(String.valueOf(users[0].FederationIdentifier) , samlAssrestion); 
                }   
            } 
            if(Test.isRunningTest()){
                Integer x =100/0;
            }  
        }
        catch(Exception e){
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            Errorcls.IDMScreateLog('ActivateUser','IDMSActivateUser','Activate User',users[0].username,ExceptionMessage ,Registration_Source,Datetime.now(),
                                   users[0].IDMS_User_Context__c,FederationIdentifier,null,false,null);                
            response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS003,users[0].FederationIdentifier,users[0].id); 
        }
        return response;    
    }
    
    //This method is used to activate the user in UIMS. Consequently, the user is being activated end to end cross Idp.
    @future(callout = true) 
    public static void ActivateUIMSIdentity(String fedId , String authToken ){
        system.debug('---in activate identity method--');
        system.debug('authToken --> ' + authToken  ) ; 
        system.debug('fedID--> ' + fedId) ;  
        if(!Test.isRunningTest()  ){
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort uimsNoPassword   = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            uimsNoPassword.clientCertName_x                                                 = System.Label.CLJUN16IDMS103;
            uimsNoPassword.endpoint_x                                                       = label.CLQ316IDMS130;
            string callerFid                                                                = Label.CLJUN16IDMS104;                  
            Boolean noPasswordResponse                                                      =  uimsNoPassword.activateIdentityNoPassword(callerFid, authToken);
            system.debug('--- noPasswordResponse  -- ' + noPasswordResponse  ) ;
        }
    }
    
}