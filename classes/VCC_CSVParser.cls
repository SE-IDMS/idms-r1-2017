public with sharing class VCC_CSVParser {
    public Blob uploadedFile{get;set;}
    public String nameFile{get;set;}
    public StringListWrapper csvList{get;set;}
    public boolean isFileReady{get;set;}{isFileReady=false;}
    public VCC_CSVParser(){}
    public VCC_CSVParser(ApexPages.StandardSetController controller) {

    }
    public Pagereference cancel(){
        if(ApexPages.currentPage().getParameters().get('Id')!= null)
            return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
        else
            return null;
    }
    public void upload(){
    //March 2016 release uploaded file null check is added
        System.debug('>>>>>Enetered Upload func>>>>');
        if(uploadedFile!=null){
            if(Test.isRunningTest())
                List<List<String>> test=parseCSV(uploadedFile.toString(),false);
            else
                csvList.stringList=parseCSV(uploadedFile.toString(),false);
            uploadedFile=null; //save heap size
            isFileReady=true; 
       }  
       else
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Choose File'));    
    }
/* 
    Code Source: http://wiki.developerforce.com/page/Code_Samples#Parse_a_CSV_with_APEX
*/
    private List<List<String>> parseCSV(String contents,Boolean skipHeaders) 
    {
        List<String> fieldList = new List<string>();
        List<List<String>> allFields = new List<List<String>>();
        System.debug('>><<<>><<Header values'+contents.split('\r')[0]);
        //START - Added for March 2016 Jan release - DEF-9442
        //Europeans has seperator semi colon. Inorder to make 
        //global seperator replacing all semicolons with commas
        if(contents.split('\r')[0].countMatches(';')>0)
            contents = contents.replaceAll(';',',');
        // END - Added for March 2016 Jan release - DEF-9442
        System.debug('>><><><contents '+contents );
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try 
        {
            //lines = contents.split('\n'); //correction: this only accomodates windows files
            lines = contents.split('\r'); // using carriage return accomodates windows, unix, and mac files
            //http://www.maxi-pedia.com/Line+termination+line+feed+versus+carriage+return+0d0a
        } 
        catch (System.ListException e) 
        {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line: lines) 
        {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');  
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field: fields) 
            {                
                if (field.startsWith('"') && field.endsWith('"')) 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } 
                else if (field.startsWith('"')) 
                {
                    makeCompositeField = true;
                    compositeField = field;
                } 
                else if (field.endsWith('"')) 
                {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } 
                else if (makeCompositeField) 
                {
                    compositeField +=  ',' + field;
                } 
                else 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        if(allFields.size() > 0)
            fieldList = allFields[0];
        System.debug('>>>>>>>allFields'+allFields);
        return allFields;       
    }
}