//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class provide a sending email method for invitation process 
//
//*******************************************************************************************

public class IdmsSendInvitationFuture {
    
    //this method sends asynchrounously email to invited user in the conext of invitation process
    @future
    public static void SendEmail(Id InviteId , String email) {
        IDMS_Send_Invitation__c sendInvite = [select Id, Email__c, IDMS_Registration_Source__c, IDMS_User_Context__c from IDMS_Send_Invitation__c where ID =:  InviteId];
        String appname = sendInvite.IDMS_Registration_Source__c;
        system.debug('Application name:'+appname);
        String appId;
        //code for getting app id from app name.        
        IDMSAppMapIdName__c appMap;
        appMap = IDMSAppMapIdName__c.getInstance(appname);
        if(appMap!=null){
            appId = (String)appMap.get('AppId__c');
        }
        
        String signature=IDMSInvitationEmailEncryption.EmailEncryption(sendInvite);
        String PrefilledRegUrl = Label.CLQ316IDMS073;
        // Reserve email capacity for the current Apex transaction to ensure 
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        // Assign the email addresses receivevd from application.
        String[] toAddresses = new String[] {email};
        mail.setToAddresses(toAddresses);
        // Specify the name used as the display name.
        mail.setSenderDisplayName('IDMS Invitation');
        // Specify the subject line for your email address.
        mail.setSubject('IDMS Invitation');
        // Specify the text content of the email.
        mail.setPlainTextBody('Your invitation: ' + InviteId +' has been created.');
        mail.setHtmlBody('To view your invitation <a href='+PrefilledRegUrl+'?InvitationId='+InviteId +'&app='+appId+'&sig='+signature+'>click here.</a>');
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        system.debug('email has been sent:'+mail );
        
    }
}