global class CTIUnrecognizedBatch_Schedule implements Schedulable {
    global void execute(SchedulableContext SC) {
        Batch_CTIUnrecognizedCalls  batchObj = new Batch_CTIUnrecognizedCalls(); 
        Database.executeBatch(batchObj,200);
    }
}