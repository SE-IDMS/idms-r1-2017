@isTest
private class AP_CONCreateWithSEContactID_TEST
{
    static testMethod void testCONCreateWithSEContactID()  
    {
        
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        
        
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;

        Contact con1=new Contact(FirstName='Anjali',LastName='Gopi',WorkPhone__c='1234',AccountId=acc1.id,SEContactID__c='abc1234');
        insert con1;

        Contact con2=new Contact(FirstName='Anjali',LastName='Gopi',WorkPhone__c='1234',AccountId=acc1.id,SEContactID__c='abc1234');    

        try
        {
            insert con2;
        }
        catch(Exception e)
        {
            System.debug(e.getmessage());
        }


    }
}