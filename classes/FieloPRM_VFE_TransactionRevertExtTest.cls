/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: Test class of revert Fielo Transaction page controller
********************************************************************/

@isTest
public without sharing class FieloPRM_VFE_TransactionRevertExtTest {
    
    @testSetup
    static void setupTest() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;
            
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c = 'test',
                F_Country__c = country.Id
            );
            Insert mem;
               
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id
            );
            
            insert invoice;

            FieloPRM_LoyaltyEligibleProduct__c loyaltyEligibleProduct = new FieloPRM_LoyaltyEligibleProduct__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_PointsInstaller__c = 1                        
            );
            insert loyaltyEligibleProduct;
            
            FieloPRM_InvoiceDetail__c invoiceDetail = new FieloPRM_InvoiceDetail__c(
                F_PRM_Invoice__c = invoice.Id,
                F_PRM_UnitPrice__c = 100,
                F_PRM_Volume__c = 1,
                F_PRM_LoyaltyEligibleProduct__c = loyaltyEligibleProduct.Id
            );
            
            insert invoiceDetail;
            
            invoice.F_PRM_Status__c = 'Approved';
            
            update invoice;
        }
    }
    
    @isTest
    static void unitTest1(){
        //Test page controller without transaction
        FieloPRM_VFE_TransactionRevertExtension ctrl = new FieloPRM_VFE_TransactionRevertExtension(new ApexPages.StandardController(new FieloEE__Transaction__c()));
        ctrl.doRevert();
        //Test page controller with a transaction
        FieloEE__Transaction__c trx = [SELECT Id FROM FieloEE__Transaction__c LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(trx);
        FieloPRM_VFE_TransactionRevertExtension ctrl2 = new FieloPRM_VFE_TransactionRevertExtension(sc);
        ctrl2.doRevert();
        

    }
}