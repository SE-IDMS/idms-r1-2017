/*
25-Apr-2013    Srinivas Nallapati    Test Call for Program Change Request
*/
@isTest
public class AP_PRMChangeRequest_Test{

    private static testMethod void PRMTest1(){
        
        Schema.DescribeSObjectResult PPDescribe= PartnerProgram__C.sObjectType.getDescribe();
        Map<string, Schema.RecordTypeInfo> mapRecordType = PPDescribe.getRecordTypeInfosByName();       
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
        insert u;
        
        //PartnerPRogram__c globalProgram  = new PartnerPRogram__c();
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        if(mapRecordType.get('Global Program') != null)
            globalProgram .RecordTypeId = mapRecordType.get('Global Program').getRecordTypeId();
        
        globalProgram.TECH_CountriesId__c = country.id;
        insert globalProgram;
        
        system.runAs(u)
        {
            ProgramChangeRequest__c prc1 = new ProgramChangeRequest__c();
            prc1.PartnerProgram__c = globalProgram.id;
            prc1.ChangeRequestType__c = 'Program Access Request';
            insert prc1;
        }//end of runAs
    }

}