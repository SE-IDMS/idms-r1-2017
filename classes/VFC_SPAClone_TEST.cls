@isTest
public class VFC_SPAClone_TEST
{
    static testMethod void nospalineItems() 
    {
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        LegacyAccount__c  legacyAccount1=new LegacyAccount__c(Account__c=acc.id,LegacyAccountType__c='PR',LegacyName__c='NL_SAP');
        insert legacyAccount1;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Account__c=null;
        sparequest.Opportunity__c=opp.id;
        insert sparequest;
        
        test.starttest();
        try{ 
            ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);        
            VFC_SPAClone controllerinstance=new VFC_SPAClone (controller);
            Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosMultipleProducts'));
            controllerinstance.proceedClone();
            controllerinstance.spa.Account__c=null;
            controllerinstance.spa.BackOfficeCustomerNumber__c=null;
            controllerinstance.spa.Channel__c=acc.id;
            controllerinstance.spa.ChannelLegacyNumber__c=null;
            controllerinstance.continueClone(); 
        }
        catch(Exception e){
            Boolean expectedExceptionThrown = true;
            system.assertEquals(expectedExceptionThrown,true);      
        }
        test.stopTest(); 
    }
    static testMethod void morespalineitemstocloneerror() 
    {
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        List<SPARequestLineItem__c> lineList=new List<SPARequestLineItem__c>();
        for(Integer i=0;i<=(Integer.valueOf(System.Label.CLOCT14SLS68)+1);i++){
            SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount'); 
            lineList.add(lineitem);
        }
        insert lineList; 
        test.starttest();
        try{ 
            ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);        
            VFC_SPAClone controllerinstance=new VFC_SPAClone (controller);
            Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosMultipleProducts'));
            controllerinstance.proceedClone();
            controllerinstance.continueClone(); 
        }
        catch(Exception e){
            Boolean expectedExceptionThrown = true;
            system.assertEquals(expectedExceptionThrown,true);      
        }
        test.stopTest(); 
    }
    static testMethod void clonespaoneLegacyAcc() 
    {
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP',System.Label.CLOCT14SLS11);
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest(System.Label.CLOCT14SLS11,opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,System.Label.CLOCT14SLS11); 
        lineitem.recordtypeId=System.Label.CLOCT14SLS52;
        insert  lineitem; 
        test.starttest(); 
        try{
            ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);        
            VFC_SPAClone controllerinstance=new VFC_SPAClone (controller);
            Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosMultipleProducts'));
            controllerinstance.proceedClone();
            controllerinstance.continueClone(); 
       }
       catch(Exception e){
            Boolean expectedExceptionThrown = true;
            system.assertEquals(expectedExceptionThrown,true);      
        }
        test.stopTest(); 
    }
        static testMethod void clonespaMultiplelegacyacc() 
    {
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        test.starttest();
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        try{
            insert sparequest;
            SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount'); 
            insert  lineitem; 
            LegacyAccount__c  legacyAccount1=new LegacyAccount__c(Account__c=acc.id,LegacyAccountType__c='PR',LegacyName__c='HK_SAP');
            insert legacyAccount1;
            LegacyAccount__c  legacyAccount2=new LegacyAccount__c(Account__c=acc.id,LegacyAccountType__c='PR',LegacyName__c='HK_SAP');
            insert legacyAccount2;
            ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);        
            VFC_SPAClone controllerinstance=new VFC_SPAClone (controller);
            Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosMultipleProducts'));
            controllerinstance.proceedClone();
            controllerinstance.continueClone(); 
       }  
        catch(Exception e){
            Boolean expectedExceptionThrown = true;
            system.assertEquals(expectedExceptionThrown,true);      
        }      
        test.stopTest(); 
    }
    static testMethod void clonespanolegacyacc() 
    {
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        test.starttest();
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        try{
            insert sparequest;
            SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount'); 
            insert  lineitem; 
            LegacyAccount__c  legacyAccount=[select id from  LegacyAccount__c  where Account__c=:acc.id];
            delete legacyAccount;
            ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);        
            VFC_SPAClone controllerinstance=new VFC_SPAClone (controller);
            Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosMultipleProducts'));
            controllerinstance.proceedClone();
            controllerinstance.continueClone(); 
       }  
        catch(Exception e){
            Boolean expectedExceptionThrown = true;
            system.assertEquals(expectedExceptionThrown,true);      
        }      
        test.stopTest(); 
    }
    static testMethod void clonespanoaccountForClonedSpa() 
    {
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        test.starttest();
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.Account__c=null;
        try{
            insert sparequest;
            SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount'); 
            insert  lineitem; 
            LegacyAccount__c  legacyAccount=[select id from  LegacyAccount__c  where Account__c=:acc.id];
            legacyAccount.LegacyName__c='NL_SAP';
            update legacyAccount;
            //sparequest.Account__c=null;
            ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);        
            VFC_SPAClone controllerinstance=new VFC_SPAClone (controller);
            Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('NegMultipleProducts'));
            controllerinstance.proceedClone();
            controllerinstance.continueClone(); 
       }  
        catch(Exception e){
            Boolean expectedExceptionThrown = true;
            system.assertEquals(expectedExceptionThrown,true);      
        }      
        test.stopTest(); 
    }
}