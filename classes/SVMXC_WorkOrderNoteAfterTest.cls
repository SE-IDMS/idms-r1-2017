@isTest(SeeAllData=true)
private class SVMXC_WorkOrderNoteAfterTest {

	static testMethod void testNoteInsert() {

        Account acc = Utils_TestMethods.createAccount();
       	insert acc;
     
        SVMXC__Service_Order__c wo =  Utils_TestMethods.createWorkOrder(acc.id);
        wo.SVMXC__Order_Type__c='Maintenance';
        wo.CustomerRequestedDate__c = Date.today();
        wo.BackOfficeReference__c = '111111';
        wo.CustomerTimeZone__c = 'Europe/Paris';
        insert wo;
        
        List<WorkOrderNote__c> insertWONs = new List<WorkOrderNote__c>();

		WorkOrderNote__c firstNote = new WorkOrderNote__c();
		firstNote.Notes__c = 'More Notes Last';
		firstNote.DisplayOrder__c = 6;
		firstNote.WorkOrder__c = wo.Id;
		firstNote.FSRUser__c = UserInfo.getUserId();
		
		insertWONs.add(firstNote);
		
		WorkOrderNote__c secondNote = new WorkOrderNote__c();
		secondNote.Notes__c = 'More Notes First';
		secondNote.DisplayOrder__c = 1;
		secondNote.WorkOrder__c = wo.Id;
		secondNote.FSRUser__c = UserInfo.getUserId();
		
		insertWONs.add(secondNote);
		
		insert insertWONs;
		
		SVMXC__Service_Order__c wo2 = [SELECT FSRNotes__c FROM SVMXC__Service_Order__c WHERE Id =: wo.Id];
		
		System.assertNotEquals(null, wo2.FSRNotes__c);
	}
}