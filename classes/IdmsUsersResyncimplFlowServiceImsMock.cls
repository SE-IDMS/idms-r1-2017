@isTest
global class IdmsUsersResyncimplFlowServiceImsMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           if(request instanceof IdmsUsersResyncflowServiceIms.createAccounts) {
           system.debug('INside create account:');
           IdmsUsersResyncflowServiceIms.createReturnBean createReturnBean1 = new IdmsUsersResyncflowServiceIms.createReturnBean();
           createReturnBean1.ID = '188b80b1-c622-4f20-8b1d-3ae86af11c4d' ;
           createReturnBean1.message= 'Test Mock1' ;
           createReturnBean1.returnCode = 1 ;
           IdmsUsersResyncflowServiceIms.createReturnBean createReturnBean2 = new IdmsUsersResyncflowServiceIms.createReturnBean();
           createReturnBean2.ID = '' ;
           createReturnBean2.message= 'TestMock2' ;
           createReturnBean2.returnCode = 2 ;
          
           IdmsUsersResyncflowServiceIms.createReturnBean createReturnBean3 = new IdmsUsersResyncflowServiceIms.createReturnBean();
           createReturnBean3.ID = '188b80b1-c622-4f20-8b1d-3ae86af11c4a' ;
           createReturnBean3.message= 'Test Mock3' ;
           createReturnBean3.returnCode = 3 ;
           
           IdmsUsersResyncflowServiceIms.createReturnBean[] arrayCreateBean = new List<IdmsUsersResyncflowServiceIms.createReturnBean>();
           arrayCreateBean.add(createReturnBean1) ;
           arrayCreateBean.add(createReturnBean2) ;
           arrayCreateBean.add(createReturnBean3) ; 
           IdmsUsersResyncflowServiceIms.createAccountsResponse createAccountResponse = new IdmsUsersResyncflowServiceIms.createAccountsResponse();
           createAccountResponse.return_x = arrayCreateBean;
           system.debug('Create account response:'+createAccountResponse.return_x);
           //return createAccountResponse ; 
           response.put('response_x',createAccountResponse);
          }
          if(request instanceof IdmsUsersResyncflowServiceIms.createContacts) {
            IdmsUsersResyncflowServiceIms.createContactsResponse contactResponse = new IdmsUsersResyncflowServiceIms.createContactsResponse();
            IdmsUsersResyncflowServiceIms.createContactReturnBean contactBean1 = new IdmsUsersResyncflowServiceIms.createContactReturnBean();
            contactBean1.email = 'test3127415' + '@accenture.com';
            contactBean1.message= 'OK';
            contactBean1.ID= '188b80b1-c622-4f20-8b1d-3ae86af11c4d';
            contactBean1.returnCode= '0';
            IdmsUsersResyncflowServiceIms.createContactReturnBean contactBean2 = new IdmsUsersResyncflowServiceIms.createContactReturnBean();
            contactBean2.email = 'test31274199' + '@accenture.com';
            contactBean2.message= 'OK';
            contactBean2.ID= '';
            contactBean2.returnCode= '0';
            IdmsUsersResyncflowServiceIms.createContactReturnBean contactBean3 = new IdmsUsersResyncflowServiceIms.createContactReturnBean();
            contactBean3.email = 'test3@mock.com';
            contactBean3.message= 'OK';
            contactBean3.ID= '188b80b1-c622-4f20-8b1d-3ae86af11c4c';
            contactBean3.returnCode= '0';
            
            IdmsUsersResyncflowServiceIms.createContactReturnBean[] arrayContactBean = new List<IdmsUsersResyncflowServiceIms.createContactReturnBean>();
            arrayContactBean.add(contactBean1);
            arrayContactBean.add(contactBean2);
            arrayContactBean.add(contactBean3);
           
            contactResponse.return_x = arrayContactBean ;
            response.put('response_x',contactResponse);
            //return contactResponse;
            
          }
          if(request instanceOf IdmsUsersResyncflowServiceIms.updateAccounts) {
           system.debug('Entering inside updateAccount' + request); 
           //Update Request Result
           IdmsUsersResyncflowServiceIms.updateAccountsResponse updateResponse = new IdmsUsersResyncflowServiceIms.updateAccountsResponse();
           IdmsUsersResyncflowServiceIms.accountUpdateReturnBean accountUpdate1 = new  IdmsUsersResyncflowServiceIms.accountUpdateReturnBean() ; 
           accountUpdate1.federatedId = '188b80b1-c622-4f20-8b1d-3ae86af11c4d';
           accountUpdate1.hasBeenUpdated = true; 
           accountUpdate1.message = 'OK';
           accountUpdate1.returnCode = 1 ;
           IdmsUsersResyncflowServiceIms.accountUpdateReturnBean accountUpdate2 = new  IdmsUsersResyncflowServiceIms.accountUpdateReturnBean() ; 
           accountUpdate2.federatedId = '188b80b1-c622-4f20-8b1d-3ae86af11c4c';
           accountUpdate2.hasBeenUpdated = true; 
           accountUpdate2.message = 'OK';
           accountUpdate2.returnCode = 2 ;
           IdmsUsersResyncflowServiceIms.accountUpdateReturnBean accountUpdate3 = new  IdmsUsersResyncflowServiceIms.accountUpdateReturnBean() ; 
           accountUpdate3.federatedId = '';
           accountUpdate3.hasBeenUpdated = true; 
           accountUpdate3.message = 'OK';
           accountUpdate3.returnCode = 3 ;
           
           IdmsUsersResyncflowServiceIms.accountUpdateReturnBean[] accountUpdatedRes = new List<IdmsUsersResyncflowServiceIms.accountUpdateReturnBean>();
           accountUpdatedRes.add(accountUpdate1);
           accountUpdatedRes.add(accountUpdate2);    
           accountUpdatedRes.add(accountUpdate3);    
           IdmsUsersResyncflowServiceIms.accounts_element accountElement = new  IdmsUsersResyncflowServiceIms.accounts_element(); 
           accountElement.accountUpdatedRes = accountUpdatedRes ; 
           
           IdmsUsersResyncflowServiceIms.accountsUpdateReturnBean updateBean = new IdmsUsersResyncflowServiceIms.accountsUpdateReturnBean() ; 
           updateBean.accounts = accountElement; 
           updateResponse.return_x = updateBean; 
           //updateResponse.return_x = accountUpdatedRes ; 
           
           system.debug('system.debug .. ' + updateResponse + ' -- ' + accountUpdatedRes); 
           response.put('response_x',updateResponse);
           
          }
          if(request instanceOf  IdmsUsersResyncflowServiceIms.updateContacts){
           IdmsUsersResyncflowServiceIms.contactUpdateReturnBean contactUpdate1 = new IdmsUsersResyncflowServiceIms.contactUpdateReturnBean();
           contactUpdate1.federatedId = '188b80b1-c622-4f20-8b1d-3ae86af11c4d';
           contactUpdate1.hasBeenUpdated = true; 
           contactUpdate1.message = 'OK';
           contactUpdate1.returnCode = 0 ;
           
           IdmsUsersResyncflowServiceIms.contactUpdateReturnBean contactUpdate2 = new IdmsUsersResyncflowServiceIms.contactUpdateReturnBean();
           contactUpdate2.federatedId = '188b80b1-c622-4f20-8b1d-3ae86af11c4c';
           contactUpdate2.hasBeenUpdated = true; 
           contactUpdate2.message = 'OK';
           contactUpdate2.returnCode = 0 ;
           
           IdmsUsersResyncflowServiceIms.contactUpdateReturnBean contactUpdate3 = new IdmsUsersResyncflowServiceIms.contactUpdateReturnBean();
           contactUpdate3.federatedId = '';
           contactUpdate3.hasBeenUpdated = true; 
           contactUpdate3.message = 'OK';
           contactUpdate3.returnCode = 0 ;
           
           IdmsUsersResyncflowServiceIms.contactUpdateReturnBean[] arrayContactupdate = new List<IdmsUsersResyncflowServiceIms.contactUpdateReturnBean>();
           arrayContactupdate.add(contactUpdate1);
           arrayContactupdate.add(contactUpdate2);
           
           IdmsUsersResyncflowServiceIms.contacts_element contactElement = new IdmsUsersResyncflowServiceIms.contacts_element(); 
           contactElement.contactUpdatedRes = arrayContactupdate ; 
           
           IdmsUsersResyncflowServiceIms.contactsUpdateReturnBean  contactUpdateReturnBean = new IdmsUsersResyncflowServiceIms.contactsUpdateReturnBean(); 
           contactUpdateReturnBean.contacts = contactElement; 
           
           IdmsUsersResyncflowServiceIms.updateContactsResponse updateContactRespn = new IdmsUsersResyncflowServiceIms.updateContactsResponse();
           updateContactRespn.return_x = contactUpdateReturnBean ; 
           response.put('response_x',updateContactRespn);
          }
          
   }
}