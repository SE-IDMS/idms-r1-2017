/*
    Author          : Accenture IDC Team 
    Date Created    : 25/07/2011
    Description     : Utility class to populate Picklist Filter values for Competitor search
                      extending abstract Data Source Class
*/

Public Class Utils_CompetitorPicklistManager implements Utils_PicklistManager
{
    // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(Label.CL00351);
        Labels.add(sObjectType.Country__c.getLabel());
        return Labels;
    } 

    // Return All Picklist Values 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
       List<SelectOption> options = new  List<SelectOption>();
       
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Business fields and Country when Page Loads
       if(i==1)
       {
            // Populate Business Picklist Values
            Schema.DescribeFieldResult business = Schema.sObjectType.Competitor__c.fields.MultiBusiness__c;
            for (Schema.PickListEntry PickVal : business.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }
       if(i==2)
       {
            // Populate Country Picklist Values
            for(Country__c countries: [Select Id, Name from Country__c order by Name asc])
            {
                options.add(new SelectOption(countries.Id,countries.Name));
            }
       }  

       return  options;  
    }
}