/*
Created By: Deepak Kumar
Created Date: 16-07-2013
Description: This class has been used to update manager of a salesforce user to technician
               It also update the SESAId with FedrationId of Schneider User.
*/


public class AP_Technician_UpdatesalesforceUser 
{
    
    private static Boolean isDebugEnabled = false;
    static {
        try {
            isDebugEnabled = Boolean.valueOf(Label.CLAPR14SRV20);
        }
        catch(Exception e) {
            System.debug('#### Error getting debug enabled status from Custom Label CLAPR14SRV20: '+e.getMessage());
        }        
    }
    
    private static void myDebug(String aMsg) {
        if (isDebugEnabled) {
            System.debug('#### '+aMsg);
        }
    }
    

    public static void updatewithSalesforceUserManager(SVMXC__Service_Group_Members__c[] lstsgm)
    {
        myDebug('#### Start updatewithSalesforceUserManager with: '+lstsgm);
        Map<Id,user> techmap = New Map<Id,user>();
        set<id> uid = new set<id>();
        list<user> ulist = New list<user>();
        
        for(SVMXC__Service_Group_Members__c tech:lstsgm)
        {
            myDebug('#### Current tech: '+tech);
            if(tech.SVMXC__Salesforce_User__c !=null)
            {
                myDebug('#### tech.SVMXC__Salesforce_User__c: '+tech.SVMXC__Salesforce_User__c);
                uid.add(tech.SVMXC__Salesforce_User__c);
            }
    
        }
        myDebug('#### List of users: '+uid);
        List<Country__c> cn = New List<Country__c>();
        Map<String, String> countryMap = new Map<String, String>();
        
        ulist = [SELECT Id,Username,UserBusinessUnit__c,ManagerId,FederationIdentifier,Country__c,Country from User where Id in :uid];
        myDebug('#### User list from DB: '+ulist);
        cn = [SELECT CountryCode__c,Id,Name FROM Country__c];
        /**
         * 2015-03-25 - Nabil ZEGHACHE
         * Added a map with country codes as keys and country names as values to be able to set the ServiceMax country (picklist) on the FSR record
         */
        for (Country__c currentCountry : [SELECT CountryCode__c,Id,Name FROM Country__c limit 5000]) {
            countryMap.put(currentCountry.CountryCode__c, currentCountry.Name);
        }
        myDebug('#### Countries from DB:' + cn);
        
        
        if(ulist !=null && ulist.size()>0)
        {   
            myDebug('#### User list from DB not null and not empty ==> initializing tech map');
            techmap.putall(ulist);
            myDebug('#### techmap: '+techmap);
        }
        for(SVMXC__Service_Group_Members__c tech1 :lstsgm)
        {
            myDebug('#### Current tech: '+tech1);
            if(techmap.containskey(tech1.SVMXC__Salesforce_User__c))
            {
                myDebug('#### Tech map contains the user ==> setting manager, sesa and BU');
                myDebug('#### tech1: '+tech1);
                tech1.Manager__c =  techmap.get(tech1.SVMXC__Salesforce_User__c).ManagerId;
                tech1.SESAID__c =  techmap.get(tech1.SVMXC__Salesforce_User__c).FederationIdentifier;
                // Hari Update for DEF-2886 
                tech1.Business_Unit__c =techmap.get(tech1.SVMXC__Salesforce_User__c).UserBusinessUnit__c;
                // 2015-03-25 - Nabil ZEGHACHE - Assigning the ServiceMax country (picklist) using the country map
                if (
                    techmap.get(tech1.SVMXC__Salesforce_User__c) != null 
                    && techmap.get(tech1.SVMXC__Salesforce_User__c).Country != null
                    && countryMap.get(techmap.get(tech1.SVMXC__Salesforce_User__c).Country) != null
                ) {
                    myDebug('#### Found the country: '+techmap.get(tech1.SVMXC__Salesforce_User__c).Country);
                    tech1.SVMXC__Country__c = countryMap.get(techmap.get(tech1.SVMXC__Salesforce_User__c).Country);
                }
                myDebug('#### tech1: '+tech1);
            }
            
/* NZE - 2015-03-25 - This part below is not only useless, it also creates CPU usage limits issues and assigns wrong countries to the FSRs
            if(ulist.size()>0)
            {
                myDebug('#### Looping through the users');
                for(User u:ulist)
                { 
                    myDebug('#### Current user: '+u);
                    string s = u.Country;
                    myDebug('#### Current user country: '+s);
                    for(Country__c c :cn)
                    {
                        myDebug('#### Looping through the Countries. Country Code: '+c.CountryCode__c);
                        if(c.CountryCode__c ==s)
                        { 
                            myDebug('#### Country code equals user\'s country');
                            tech1.SVMXC__Country__c = c.Name;
                            myDebug('#### tech1: '+tech1);
                        }
                    }
                }
            }
*/
            
        }
    
    }
}