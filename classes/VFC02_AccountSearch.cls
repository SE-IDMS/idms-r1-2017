/**
 * Apex Controller for Visualforce Page VFP02_AccountSearch
 * ~ Created by Adrian MODOLEA / Mohamed EL MOUSSAOUI
 * ~ SCHNEIDER SR.0 - RELEASE - October 18th 2010
**/
/*    Modified By ACCENTURE IDC
      Modifed for December Release Business Requirement BR-924
      Modified Date: 10/11/2011

20-Nov-2012 Srinivas Nallapati  A&C Dec12 release : changed the class to 'with sharing'
*/

public without sharing class VFC02_AccountSearch extends VFC_ControllerBase
{
    /*============================================================================
        V.A.R.I.A.B.L.E.S
    =============================================================================*/     
    // current account used for the input search
    public Account account{ get; private set; }
    
    // current page parameters
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
    
    // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL00016);
    
    // collection of accounts matching the search criteria
    //public List<Account> accounts {get; private set;}

    // indicates whether there are more records than the searchMaxSize.
    public Boolean hasNext {
        get {
            return ((numberOfRecord > searchMaxSize || numberOfRecord == 0) && showButton);
        }
        set;
    }      
    public Boolean ShowOrfCancelButton { get; set; }
    public Boolean ShowCancelButton { get; set; }
    //Start of December Release Modification
    public Boolean DisplayResults{get;set;}
    Public string SelectTypevalue{get;set;}
    public List<SelectOption> columns{get;set;}   
    public List<DataTemplate__c> fetchedRecords{get;set;} 
    public Long numberOfRecord{get;set;}
    private Boolean showButton;   
    public Boolean openInConsole { get; set; }
    
    //START EUS: 011868  
    private String recordTypeId;
    // END EUS: 011868
    private final RecordType r; //RecordType sObject
    //public RecordType SupplierRec;
    //public RecordType CustomerRec;
    public RecordType ConsumerRec;
    
    private String loggedinUserProfileId;

    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );
                                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            
            return new VCC06_DisplaySearchResults();
        }
         
    }
    //End of December Release Modification
   /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/
    public VFC02_AccountSearch(ApexPages.StandardController controller) {
//        this.r = (RecordType)controller.getRecord();
        system.debug('--------openInConsole in Constructor -------'+openInConsole ); 
        account = (Account)controller.getRecord();

        // APR 14
        loggedinUserProfileId = UserInfo.getProfileId();
        // APR 14
        // START EUS: 011868

        System.Debug('** Record Type: **' + this.r);
        /* start - Sept 2012 release */
        
        String accName = ApexPages.currentPage().getParameters().get('Accname');
        String accCountry = ApexPages.currentPage().getParameters().get('AccCountry');
        String Oppregid= ApexPages.CurrentPage().getParameters().get('oppRegFormId');
        
        String accCity = ApexPages.currentPage().getParameters().get('accCity');
        String accZipCode = ApexPages.currentPage().getParameters().get('accZipCode');
        String accStreet = ApexPages.CurrentPage().getParameters().get('accStreet');
        String fromPRM= ApexPages.CurrentPage().getParameters().get('fromPRM');
        
        this.recordTypeId = ApexPages.CurrentPage().getParameters().get('RecordType');
        
        //SupplierRec = [SELECT r.Id, r.Name, r.SobjectType from RecordType r Where SobjectType='Account' And IsActive=true AND r.Name = :System.Label.CLSEP12SLS48];
        //CustomerRec = [SELECT r.Id, r.Name, r.SobjectType from RecordType r Where SobjectType='Account' And IsActive=true AND r.Name = :System.Label.CLSEP12SLS49];
        ConsumerRec = [SELECT r.Id, r.Name, r.SobjectType from RecordType r Where SobjectType='Account' And IsActive=true AND r.Name = :System.Label.CLDEC12AC04];
        /*
        if (this.recordTypeId == null) {
            //Change for EUS 013792
            RecordType r1 = [SELECT r.Id, r.Name, r.SobjectType from RecordType r Where SobjectType='Account' And IsActive=true AND r.Name = 'Customer'];
            //End of Change for EUS 013792
            this.recordTypeId = r1.Id;
        }
        */
        System.Debug('**RecordType**' + this.recordTypeId);
        // END EUS: 011868
        
        if(accCity!='null'  )
            account.city__c=ApexPages.currentPage().getParameters().get('accCity');
        if(accZipCode!='null'  )
            account.ZipCode__c=ApexPages.currentPage().getParameters().get('accZipCode');
        if(accStreet!='null'  )
            account.street__c=ApexPages.currentPage().getParameters().get('accStreet'); 
        
         if(accName!='null'  )
            account.name=ApexPages.currentPage().getParameters().get('Accname');
        if(accCountry!='null')   
            account.Country__c=ApexPages.currentPage().getParameters().get('AccCountry');
        If ( Oppregid!=null)
        {ShowOrfCancelButton = TRUE;
         ShowCancelButton = FALSE;
         system.debug('**ShowOrfCancelButton'+ ShowOrfCancelButton);
          system.debug('**ShowCancelButton'+ ShowCancelButton);
        }
        else
        {ShowOrfCancelButton = FALSE;
         ShowCancelButton = TRUE;
        }    
        /* end -  Sept 2012 release */
        
        /*Modified By Accenture IDC for December Release*/
        columns = new List<SelectOption>();
        showButton = true;        
        DisplayResults = true;
        if(!(fromPRM != null && fromPRM == 'true'))
        {
            try
            {
                String UserCountryCode = [SELECT Country__c from User WHERE id =:UserInfo.getUserId()].Country__c ;
                Account.Country__c = [SELECT ID FROM Country__c WHERE CountryCode__c=:UserCountryCode LIMIT 1].ID;
                        
                System.debug('##### Account: ' + account);
            }
            catch(Exception e)
            {
                System.debug('---- No default Country found');
            }
        }    
        //Start of December Release Modification
        numberOfRecord=0;
        columns.add(new SelectOption('Field10__c',sObjectType.account.fields.name.label)); // First Column
        columns.add(new SelectOption('Field9__c',sObjectType.account.fields.AccountLocalName__c.label));
        columns.add(new SelectOption('Field8__c',sObjectType.account.fields.Street__c.label));
        columns.add(new SelectOption('Field7__c',sObjectType.account.fields.ZipCode__c.label));
        columns.add(new SelectOption('Field6__c',sObjectType.account.fields.City__c.label));
        columns.add(new SelectOption('Field5__c',sObjectType.account.fields.StateProvince__c.label)); 
        columns.add(new SelectOption('Field4__c',sObjectType.account.fields.Country__c.label));
        columns.add(new SelectOption('Field3__c',sObjectType.account.fields.POBox__c.label));
        columns.add(new SelectOption('Field2__c',sObjectType.account.fields.POBoxZip__c.label));// Last Column
        columns.add(new SelectOption('Field1__c',Label.CL00049));
        columns.add(new SelectOption('Field12__c',sObjectType.account.fields.StreetLocalLang__c.label));
        columns.add(new SelectOption('FieldCheckbox1__c',sObjectType.account.fields.ToBeDeleted__c.label));
        columns.add(new SelectOption('FieldCheckbox2__c',sObjectType.account.fields.CorporateHeadquarters__c.label));
        columns.add(new SelectOption('Field11__c','Select'));
        fetchedRecords = new List<DataTemplate__c>();
        DataTemplate__c dt = new DataTemplate__C(); 
        dt.field3__c = ' '; 
      //  dt.FieldCheckbox1__c = FALSE;
        fetchedRecords.add(dt);
        //End of December Release Modification
        
        /* start - Sept 2012 release */
        if (Oppregid!=null )
        {
           SelectTypevalue='Single';
        }
        else
        {
           SelectTypevalue='none';
        }
        /* End - Sept 2012 release */
    }


    // APR 14
    public PageReference doCreate() {
        if(loggedinUserProfileId.startsWith(System.Label.CLAPR14PRM74)) {
            PageReference newAccPage2 = new PageReference('/001/e');
            // do not longer override the creation action   
            newAccPage2.getParameters().put('nooverride', '1');
            newAccPage2.setRedirect(true);

            return newAccPage2; 
        } 
        else
            return null;
    }
    //

    /*============================================================================
        M.E.T.H.O.D.S
    =============================================================================*/         
    // returns the search string to be used in SOSL query
    public String getSearchString() {
        String strSearch = 'FIND {*'+Utils_Methods.escapeForSOSL(account.Name)+'*} IN NAME FIELDS ';
        strSearch += ' RETURNING Account(Id,Name,AccountLocalName__c,Street__c,StreetLocalLang__c,ZipCode__c,City__c,StateProvince__c,Country__c,POBox__c,POBoxZip__c,ToBeDeleted__c,CorporateHeadquarters__c,OwnerId';
        List<String> whereConditions = new List<String>();
        if(account.AccountLocalName__c!=null && account.AccountLocalName__c!='')
            whereConditions.add('AccountLocalName__c=\''+Utils_Methods.escapeForWhere(account.AccountLocalName__c)+'\'');
        if(account.Street__c!=null && account.Street__c!='')
            whereConditions.add('Street__c=\''+Utils_Methods.escapeForWhere(account.Street__c)+'\'');
        if(account.StreetLocalLang__c!=null && account.StreetLocalLang__c!='')
            whereConditions.add('StreetLocalLang__c=\''+Utils_Methods.escapeForWhere(account.StreetLocalLang__c)+'\'');
        if(account.ZipCode__c!=null && account.ZipCode__c!='')
            whereConditions.add('ZipCode__c=\''+Utils_Methods.escapeForWhere(account.ZipCode__c)+'\'');
        if(account.City__c!=null && account.City__c!='')
            whereConditions.add('City__c=\''+Utils_Methods.escapeForWhere(account.City__c)+'\'');
        if(account.StateProvince__c!=null)
            whereConditions.add('StateProvince__c=\''+account.StateProvince__c+'\'');
        if(account.Country__c!=null) 
            whereConditions.add('Country__c=\''+account.Country__c+'\'');
        if(account.POBox__c!=null && account.POBox__c!='')
            whereConditions.add('POBox__c=\''+Utils_Methods.escapeForWhere(account.POBox__c)+'\'');
        if(account.POBoxZip__c!=null && account.POBoxZip__c!='')
            whereConditions.add('POBoxZip__c=\''+Utils_Methods.escapeForWhere(account.POBoxZip__c)+'\'');
        

        //DEC 12 AC UAT Srinivas
        
        list<String> notAvailabelRIds = new list<String>();
        list<String> AvailabelRIds = new list<String>();
        
        Schema.DescribeSObjectResult AccDes = Schema.SObjectType.Account;
        Map<Id,Schema.RecordTypeInfo> rtMapById = AccDes.getRecordTypeInfosById();
        for(id i : rtMapById.keyset())
        {
            if(rtMapById.get(i).isAvailable())
               AvailabelRIds.add(i);
            else
               notAvailabelRIds.add(i);   
        }
        String recordTypeNotEquals;
        String recordTypeEquals;
        if(AvailabelRIds.size() > 0)
        {
            recordTypeEquals = '( ';
            for(Integer  i=0; i< AvailabelRIds.size(); i++)
            {
                recordTypeEquals =recordTypeEquals + ' RecordTypeId =\'' + Utils_Methods.escapeForWhere(AvailabelRIds.get(i)) + '\'';
                if(i<AvailabelRIds.size() - 1)
                  recordTypeEquals =recordTypeEquals + ' OR ';
            }
            recordTypeEquals =recordTypeEquals + ' OR RecordTypeId = null';
                
            recordTypeEquals += ') ';
        }    
        system.debug(recordTypeEquals);
        if(recordTypeEquals != null)
        {
            whereConditions.add(recordTypeEquals);
        }
        else
        {
            recordTypeEquals = ' RecordTypeId = null';
            whereConditions.add(recordTypeEquals);
        }
        
        //
        // START EUS: 011868 , EUS 013792
        //if(this.recordTypeId != SupplierRec.id)
        //    whereConditions.add('(RecordTypeId =\'' + Utils_Methods.escapeForWhere(CustomerRec.id) + '\''+' OR RecordTypeId =\'' + Utils_Methods.escapeForWhere(ConsumerRec.id) + '\''+ ' OR RecordTypeId = null)');
        //else
         //    whereConditions.add('RecordTypeId =\'' + Utils_Methods.escapeForWhere(recordTypeId ) + '\'');
         // END EUS: 011868
            
        if(whereConditions.size() >0 ) {
            strSearch += ' WHERE ';
            for(String condition : whereConditions) {
                strSearch += condition + ' AND ';   
            }
            // delete last 'AND'
            strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
        }
        
        strSearch += ' ORDER BY Name';
        //strSearch += ' LIMIT ' + (Integer)(searchMaxSize+1); // As no Limits required, we are using pagination to allow user to navigate all the records.
        strSearch += ')';
        return strSearch;   
    }
    
    public String getQueryString() {
        String strSearch = 'SELECT Id,Name,AccountLocalName__c,Street__c,StreetLocalLang__c,ZipCode__c,City__c,StateProvince__c,Country__c,POBox__c,POBoxZip__c,OwnerId,Account.Owner.Name,Country__r.Name,StateProvince__r.Name,ToBeDeleted__c,CorporateHeadquarters__c FROM Account';
        List<String> whereConditions = new List<String>();
        String whereClause='';
        //whereConditions.add('Name LIKE \'%'+Utils_Methods.escapeForWhere(account.Name)+'%\'');
         whereClause +='Name LIKE \'%'+Utils_Methods.escapeForWhere(account.Name)+'%\'';
        if(account.AccountLocalName__c!=null && account.AccountLocalName__c!='')
        {
            if(whereClause != null && whereClause.length()>0){
                 whereClause +=' OR AccountLocalName__c LIKE \'%'+Utils_Methods.escapeForWhere(account.AccountLocalName__c)+'%\'';
            }
            else{whereConditions.add('AccountLocalName__c LIKE \'%'+Utils_Methods.escapeForWhere(account.AccountLocalName__c)+'%\'');}
        }
        
        //whereConditions.add('AccountLocalName__c LIKE \'%'+Utils_Methods.escapeForWhere(account.AccountLocalName__c)+'%\'');
        if(account.Street__c!=null && account.Street__c!='')
            whereConditions.add('Street__c=\''+Utils_Methods.escapeForWhere(account.Street__c)+'\'');
        if(account.StreetLocalLang__c!=null && account.StreetLocalLang__c!='')
            whereConditions.add('StreetLocalLang__c=\''+Utils_Methods.escapeForWhere(account.StreetLocalLang__c)+'\'');
        if(account.ZipCode__c!=null && account.ZipCode__c!='')
            whereConditions.add('ZipCode__c=\''+Utils_Methods.escapeForWhere(account.ZipCode__c)+'\'');
        if(account.City__c!=null && account.City__c!='')
            whereConditions.add('City__c=\''+Utils_Methods.escapeForWhere(account.City__c)+'\'');
        if(account.StateProvince__c!=null)
            whereConditions.add('StateProvince__c=\''+account.StateProvince__c+'\'');
        if(account.Country__c!=null) 
            whereConditions.add(' Country__c=\''+account.Country__c+'\'');
        if(account.POBox__c!=null && account.POBox__c!='')
            whereConditions.add('POBox__c=\''+Utils_Methods.escapeForWhere(account.POBox__c)+'\'');
        if(account.POBoxZip__c!=null && account.POBoxZip__c!='')
            whereConditions.add('POBoxZip__c=\''+Utils_Methods.escapeForWhere(account.POBoxZip__c)+'\'');
        //DEC 12 AC UAT Srinivas
        
        list<String> notAvailabelRIds = new list<String>();
        list<String> AvailabelRIds = new list<String>();
        
        Schema.DescribeSObjectResult AccDes = Schema.SObjectType.Account;
        Map<Id,Schema.RecordTypeInfo> rtMapById = AccDes.getRecordTypeInfosById();
        for(id i : rtMapById.keyset())
        {
            if(rtMapById.get(i).isAvailable())
               AvailabelRIds.add(i);
            else
               notAvailabelRIds.add(i);   
        }
        String recordTypeNotEquals;
        String recordTypeEquals;
        if(AvailabelRIds.size() > 0)
        {
            recordTypeEquals = '( ';
            for(Integer  i=0; i< AvailabelRIds.size(); i++)
            {
                recordTypeEquals =recordTypeEquals + ' RecordTypeId =\'' + Utils_Methods.escapeForWhere(AvailabelRIds.get(i)) + '\'';
                if(i<AvailabelRIds.size() - 1)
                  recordTypeEquals =recordTypeEquals + ' OR ';
            }
            recordTypeEquals =recordTypeEquals + ' OR RecordTypeId = null';
                
            recordTypeEquals += ') ';
        }    
        system.debug(recordTypeEquals);
        if(recordTypeEquals != null)
        {
            whereConditions.add(recordTypeEquals);
        }
        else
        {
            recordTypeEquals = ' RecordTypeId = null';
            whereConditions.add(recordTypeEquals);
        }
        
        //    
        // START EUS: 011868
        //if(this.recordTypeId != SupplierRec.id)
        //    whereConditions.add('(RecordTypeId =\'' + Utils_Methods.escapeForWhere(CustomerRec.id) + '\''+' OR RecordTypeId =\'' + Utils_Methods.escapeForWhere(ConsumerRec.id) + '\''+ 'OR RecordTypeId = null)');
        //else
        //     whereConditions.add('RecordTypeId =\'' + Utils_Methods.escapeForWhere(recordTypeId ) + '\'');
        // END EUS: 011868

        if(whereConditions.size() > 0 || whereClause.length()>0) {
            strSearch += ' WHERE ';
            if(whereClause.length()>0){
                strSearch +=' ( '+whereClause+' ) '+' AND ';
            }
            for(String condition : whereConditions) {
                strSearch += condition + ' AND ';   
            }
            // delete last 'AND'
            strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
        }
        strSearch += ' ORDER BY Name';
        //strSearch += ' LIMIT ' + (Integer)(searchMaxSize+1); // As no Limits required, we are using pagination to allow user to navigate all the records.
        return strSearch;   
    }
            
    // execute the SOSL query
    public PageReference doSearchAccount(){
        //Boolean soqlDebugMode = ApexPages.currentPage().getParameters().containsKey('soql');
        
        Boolean soqlDebugMode = True; // parameter to force the soql mode instead of the sosl mode. this change allow to perform a search on a string with special characters        if (System.Label.CL00546=='1')
        // check for the account name length
        if(account.Name == null || account.Name.trim().length() < 3)
        {
            account.Name.addError(System.Label.CL00028);
            return null;
        }
        String s = null;
        if(soqlDebugMode) {
            s = getQueryString();
            Utils_SDF_Methodology.log('START query: ', s);          
        } else {
            s = getSearchString();
            Utils_SDF_Methodology.log('START search: ', s);
        }
        
        Utils_SDF_Methodology.startTimer();
        try {
            
        //Start of December Release Modification
        fetchedrecords.clear(); 
        
        if(soqlDebugMode)
        {
        numberOfRecord = 0;
        for(Account a : Database.query(s))
        {
            numberOfRecord++;
            if (a!=null && fetchedRecords.size()<100)
                fetchedRecords.add(dataCreation(a));
        }
        }
        else
        {
        numberOfRecord = 0;
        for(Account a : (List<Account>)search.query(s).get(0))
        {
            numberOfRecord++;            
            if (a!=null && fetchedRecords.size()<100)
                fetchedRecords.add(dataCreation(a));
        }
        }
        if(numberOfRecord == 0)
            showButton = False;
        else
            showButton = True;
        if( numberOfRecord  > 100)
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349)); 
        if(numberOfRecord==0)
        {
            DataTemplate__c dt = new DataTemplate__c();
            dt.field3__c = ' ';   
            fetchedRecords.add(dt);
        }
        //End of December Release Modification     
            if(Test.isRunningTest())
                throw new TestException();
            }
            catch(Exception ex){
            ApexPages.addMessages(ex);
            }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return null;
    }
    
/******************************************************************/
/*          START: SEP 2012 REL. ORF TO OPP                        */
/******************************************************************/    

public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
     {   
         System.debug('**Started***');
         VFC02_AccountSearch thisController = (VFC02_AccountSearch)controllerBase;
         DataTemplate__c dt= (DataTemplate__c)obj;
         System.debug('******#######'+dt.field11__c);
         System.debug('****'+ApexPages.currentPage().getParameters().get('oppRegFormId'));
        // OpportunityRegistrationForm__c opregForm=[select id, AccountbFO__c from OpportunityRegistrationForm__c where id=:ApexPages.currentPage().getParameters().get('oppRegFormId')];
         OpportunityRegistrationForm__c opregForm = new OpportunityRegistrationForm__c(Id = ApexPages.currentPage().getParameters().get('oppRegFormId'));                                        
         opregForm.AccountbFO__c = dt.field11__c;
         update opregForm;
          PageReference pr = new PageReference('/apex/VFP_ConvertORFToOpportunity?id='+opregForm.id);
          pr.getParameters().put('PreviousPageURL', ApexPages.currentPage().getParameters().get('PreviousPageURL')); 
         pr.setRedirect(true);
         return pr;
                
     }
     
     
 /*  Overridding the standard Cancel Button  */  
Public Pagereference doCancel()
{
PageReference pr = new PageReference('/'+ ApexPages.currentPage().getParameters().get('oppRegFormId'));
   pr.setRedirect(true);
        return pr;
}     
/******************************************************************/
/*          END: SEP 2012 REL. ORF TO OPP                         */
/******************************************************************/


    
    private DataTemplate__c dataCreation(Account accn)
    {         
        // Site.getPathPrefix - added as part of Community Changes in July 2015
        DataTemplate__c dt = new DataTemplate__c();
        
        dt.field2__c = accn.POBoxZip__c;  
        dt.field3__c = accn.POBox__c;       
        dt.field6__c = accn.City__c;  
        dt.field7__c = accn.ZipCode__c;
        dt.field8__c = accn.Street__c;    
        dt.field9__c = accn.AccountLocalName__c;
        if(openInConsole)
        {
            dt.field1__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.OwnerId+'" onClick="openTab(\''+accn.OwnerId+'\',\''+accn.Owner.Name+'\');return false">'+accn.Owner.Name+'</a>';
            dt.field4__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.Country__c+'" onClick="openTab(\''+accn.Country__c+'\',\''+accn.Country__r.Name+'\');return false">'+accn.Country__r.Name+'</a>';
            if(accn.StateProvince__c!=null)         
                dt.field5__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.StateProvince__c+'" onClick="openTab(\''+accn.StateProvince__c+'\',\''+accn.StateProvince__r.Name+'\');return false">'+accn.StateProvince__r.Name+'</a>';
            dt.field10__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.Id+'" onClick="openTab(\''+accn.Id+'\',\''+accn.Name+'\');return false" >'+accn.Name+'</a>';
        }
        else
        {
            dt.field1__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.OwnerId+'" target="_blank">'+accn.Owner.Name+'</a>';
            dt.field4__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.Country__c+'" target="_blank">'+accn.Country__r.Name+'</a>';
            if(accn.StateProvince__c!=null)         
                dt.field5__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.StateProvince__c+'" target="_blank">'+accn.StateProvince__r.Name+'</a>';
            dt.field10__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + SITE.getPathPrefix() +'/'+accn.Id+'" target="_blank">'+accn.Name+'</a>';
        }    
        dt.field11__c =accn.Id;
        dt.field12__c = accn.StreetLocalLang__c;
        dt.fieldCheckbox1__c = accn.ToBeDeleted__c;
        dt.fieldCheckbox2__c = accn.CorporateHeadquarters__c;
        
        return dt;
    }
    
    
    
    // returns the standard account new page with pre-filled information from the input search         
    public PageReference continueCreation()
    { 
        PageReference newAccPage = new PageReference('/001/e');
        
        // add parameters from the initial context (except <save_new>)
        for(String param : pageParameters.keySet())
            newAccPage.getParameters().put(param, pageParameters.get(param));
        if(newAccPage.getParameters().containsKey('save_new'))
            newAccPage.getParameters().remove('save_new');      

        /********************************************************************************/
        /*          START: OCT 2013 REL.POP FIRST & LAST NAME FOR CONSUMER ACCOUNTS     */
        /********************************************************************************/    
                
        if(recordTypeId == ConsumerRec.id)
        {
            if(account.name!=null && account.name.split(' ')!=null && account.name.split(' ').size()>1) 
            {
                newAccPage.getParameters().put('name_firstacc2', account.name.split(' ')[0]);
                newAccPage.getParameters().put('name_lastacc2', account.name.substringAfter(account.name.split(' ')[0]));
            }
            else
                newAccPage.getParameters().put('name_lastacc2', account.name);            
                
            if(account.AccountLocalName__c!=null && account.AccountLocalName__c.split(' ')!=null && account.AccountLocalName__c.split(' ').size()>1) 
            {
                newAccPage.getParameters().put(Label.CLOCT13ACC42, account.AccountLocalName__c.split(' ')[0]);
                newAccPage.getParameters().put(Label.CLOCT13ACC43, account.AccountLocalName__c.substringAfter(account.AccountLocalName__c.split(' ')[0]));
            }
            else
                newAccPage.getParameters().put(Label.CLOCT13ACC43, account.AccountLocalName__c);            
        }
        /********************************************************************************/
        /*          END: OCT 2013 REL.POP FIRST & LAST NAME FOR CONSUMER ACCOUNTS     */
        /********************************************************************************/    
                                    
        // add parameters from the input search form
        newAccPage.getParameters().put('acc2', account.Name);
        if(account.AccountLocalName__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00422, account.AccountLocalName__c);
            
        if(account.Street__c!=null){
            newAccPage.getParameters().put('acc17street', account.Street__c);    
            //newAccPage.getParameters().put(System.Label.CL00036, account.Street__c); --->> Commented for B2S
        }                       
        if(account.StreetLocalLang__c!=null) 
            newAccPage.getParameters().put(System.Label.CLOCT13ACC04, account.StreetLocalLang__c);    
        if(account.ZipCode__c!=null){
            newAccPage.getParameters().put('acc17zip', account.ZipCode__c);    
            //newAccPage.getParameters().put(System.Label.CL00038, account.ZipCode__c);        --->> Commented for B2S 
        }              
        if(account.City__c!=null){          
            newAccPage.getParameters().put('acc17city', account.City__c);    
            //newAccPage.getParameters().put(System.Label.CL00039, account.City__c); --->>  Commented for B2S
        }                   
        if(account.StateProvince__c!=null) {
            newAccPage.getParameters().put('acc17state', [SELECT Name,StateProvinceCode__c   FROM StateProvince__c  WHERE Id=:account.StateProvince__c].StateProvinceCode__c);    
            // newAccPage.getParameters().put(System.Label.CL00040, [SELECT Name FROM StateProvince__c WHERE Id=:account.StateProvince__c].Name);    --->> Commented for B2S
            // newAccPage.getParameters().put(System.Label.CL00041, account.StateProvince__c);           --->> Commented for B2S
        } 
        if(account.Country__c!=null) {
            newAccPage.getParameters().put('acc17country', [SELECT Name,CountryCode__c FROM Country__c WHERE Id=:account.Country__c].CountryCode__c);    
             //newAccPage.getParameters().put(System.Label.CL00042, [SELECT Name FROM Country__c WHERE Id=:account.Country__c].Name);    --->> Commented for B2S
             //newAccPage.getParameters().put(System.Label.CL00043, account.Country__c);     --->> Commented for B2S
             // Q3 2016 BR-9867, Currency will be taken from the country
             newAccPage.getParameters().put(System.Label.CLQ316ACC001, [SELECT CurrencyISOCode FROM Country__c WHERE Id=:account.Country__c].CurrencyISOCode);     
        }               
        if(account.POBox__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00044, account.POBox__c); 
        if(account.POBoxZip__c!=null) 
            newAccPage.getParameters().put(System.Label.CL00045, account.POBoxZip__c); 
        if(account.LeadingBusiness__c!=null) 
            newAccPage.getParameters().put(System.Label.CLOCT13ACC05, account.LeadingBusiness__c);         
        if(account.MarketSegment__c!=null) 
            newAccPage.getParameters().put(System.Label.CLOCT13ACC06, account.MarketSegment__c);
        else if(account.ClassLevel1__c!=null) 
        //Please note that the ELSE IF clause has been added because of the known salesforce issue, where the Pagereference.getParameters() set is case insensitive
        // So two fields with the same field ID (with different case) overwrite each other
        //In this case, both Market Segment and Customer CLassification Level 1 fields have the same ID. The only difference is the case between the 2 IDs.
        //Refer: Salesforce help for more details: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_pages_pagereference.htm
           newAccPage.getParameters().put(System.Label.CLOCT13ACC07, account.ClassLevel1__c); 
        
              
        
        // do not longer override the creation action   
        newAccPage.getParameters().put('nooverride', '1');
        
        //Add parameters for defaulting values for Consumer(person account) recordtype
        /*this.recordTypeId = ApexPages.CurrentPage().getParameters().get('RecordType');
        if(recordTypeId == ConsumerRec.id)
        {
            User usr = [select name, LanguageLocaleKey from User where id = :UserInfo.getUserId()];
            if(usr.LanguageLocaleKey != null)
                newAccPage.getParameters().put(System.Label.CLDEC12AC08, usr.LanguageLocaleKey.substring(0,2) );
        }*/
       /******************************************************************
            START: SEP 2012 REL. Acc Creation
        ******************************************************************   
       if(ApexPages.currentPage().getParameters().get('oppRegFormId')!=null)
       {
       OpportunityRegistrationForm__c opregForm = [Select AccountAddress__c,AccountStateProvince__c,AccountCity__c,AccountZipcode__c,ProjectAddress__c,MarketSegment__c, MarketSubSegment__c,TECH_LeadingBusiness__c from OpportunityRegistrationForm__c where Id =: ApexPages.currentPage().getParameters().get('oppRegFormId')];
      
       newAccPage.getParameters().put('saveURL', ApexPages.currentPage().getParameters().get('saveURL')); 
       newAccPage.getParameters().put(System.Label.CL00036, opregForm.AccountAddress__c);
       newAccPage.getParameters().put(System.Label.CLSEP12PRM12, opregForm.ProjectAddress__c);
       newAccPage.getParameters().put(System.Label.CL00040, [SELECT Name FROM StateProvince__c WHERE Id=:opregForm.AccountStateProvince__c].Name);
       newAccPage.getParameters().put(System.Label.CL00041, opregForm.AccountStateProvince__c);
       newAccPage.getParameters().put(System.Label.CL00039, opregForm.AccountCity__c);
       newAccPage.getParameters().put(System.Label.CL00038, opregForm.AccountZipcode__c);
       //newAccPage.getParameters().put(System.Label.CLSEP12PRM13, 'EU'); // Classification Leve1 - EU
       newAccPage.getParameters().put(System.Label.CLSEP12PRM10, opregForm.MarketSubSegment__c);
       newAccPage.getParameters().put(System.Label.CLSEP12PRM11, opregForm.TECH_LeadingBusiness__c);
        }
        ******************************************************************
                END: SEP 2012 REL. Acc Creation
        ******************************************************************/

       /******************************************************************
            START: SEP 2012 REL. ORF TO OPP
        *****************************************************************/ 
        
       if(ApexPages.currentPage().getParameters().get('oppRegFormId')!=null)
       {
       string url='';
       OpportunityRegistrationForm__c opregForm = new OpportunityRegistrationForm__c(Id = ApexPages.currentPage().getParameters().get('oppRegFormId'));
       //string oppRegFromId = ApexPages.currentPage().getParameters().get('oppRegFormId');
       url ='/apex/VFP_ConvertORFToOpportunity?Id='+ opRegForm.Id+ '&skipAccCheck=true'+ '&skipConCheck=true';
            newAccPage = new PageReference(url);
            newAccPage.setRedirect(true); 
        }
        
        /******************************************************************
                END: SEP 2012 REL. ORF TO OPP
        ******************************************************************/    
     
        newAccPage.setRedirect(true);
        return newAccPage;
    }  
    
    public class TestException extends Exception {}               
}