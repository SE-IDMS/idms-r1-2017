@RestResource(urlMapping='/IDMSPartners/*')
global with sharing class IDMSGetCookie {
    
    private static final String AUTHORIZE_ENDPOINT = '/services/oauth2/authorize';  
    
    @HttpGet
    global static void doGet() {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String redirectUrl = '';
        String brand = '';
        String brandBefore = req.requestUri.remove('/IDMSPartners/');
        
        if (brandBefore.contains(AUTHORIZE_ENDPOINT)) {
            brand = brandBefore.remove(AUTHORIZE_ENDPOINT);
            redirectUrl = AUTHORIZE_ENDPOINT;
            String bodyString = '<script>document.cookie = "apex__brand=' + brand + ';Path=/;"; ';
            bodyString += 'window.location = "' + redirectUrl + '?';
            for (String parameterName :  req.params.keySet() ) {
                bodyString += parameterName + '=' + EncodingUtil.urlEncode(req.params.get(parameterName), 'UTF-8') + '&';
            }
            bodyString += '";</script>';
            res.addHeader('Content-Type', 'text/html');  
            res.statusCode = 200; 
            res.responseBody = Blob.valueOf(bodyString);
        } else {
            res.statusCode = 403; 
        }
        
    }
    
}