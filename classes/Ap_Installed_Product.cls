/*
Created By-Deepak
For April Services Release-14
Purpose-Asset Category Modification

*/
public class Ap_Installed_Product
{ 
    
    public Static List<SVMXC__Installed_Product__c> AssetCategory(List<SVMXC__Installed_Product__c> iplist) 
    { 
    system.debug('332:'+system.label.CLAPR14SRV08);
    Map<id,List<SVMXC__Service_Order__c>> ipidwoListMap = new Map<id, List<SVMXC__Service_Order__c>>();
     Map<id,SVMXC__Service_Contract_Products__c> ipidSCPActiveMap = new Map<id, SVMXC__Service_Contract_Products__c>();
     
     //MayRelease changes
    Map<Id,SVMXC__Installed_Product__c> newmap = New Map<Id,SVMXC__Installed_Product__c>();
    Map<Id,String> oldmap = New Map<Id,String>();
    List<SVMXC__Installed_Product__c> tempList = New List<SVMXC__Installed_Product__c>();
    //Id oldid ;
    //Id newid ;
    List<SVMXC__Installed_Product__c> returnList = New List<SVMXC__Installed_Product__c>();
    
     
    system.debug('iplist:'+iplist);
        Set<Id> ipid = new Set<Id>();
        Set<Id> accid = new Set<Id>();
         if(iplist.size()>0)
        {  
            for(SVMXC__Installed_Product__c ip :iplist)
            {
                ipid.add(ip.Id);
                accid.add(ip.SVMXC__Company__c);
                oldmap.put(ip.Id,ip.AssetCategory2__c);
            }
           // tempList = iplist;
            
        }
        
        List<Account>  acclist = New List<Account>();
        List<SVMXC__Service_Order__c> wolist = New List<SVMXC__Service_Order__c>();
        if(ipid !=null)
        {
            wolist=[Select SVMXC__Component__c,Id,Name From SVMXC__Service_Order__c Where SVMXC__Component__c =:ipid];
            
             string accCLass = system.label.CLAPR14SRV08;
            acclist =[Select Id,ClassLevel1__c, Name From Account Where Id=:accid and ClassLevel1__c=:accCLass];
            
            for(SVMXC__Service_Order__c so:wolist){
            
                if(ipidwoListMap.containsKey(so.SVMXC__Component__c))
                {
                    ipidwoListMap.get(so.SVMXC__Component__c).add(so);
                }
                else{
                    ipidwoListMap.put(so.SVMXC__Component__c, new List<SVMXC__Service_Order__c>());
                    ipidwoListMap.get(so.SVMXC__Component__c).add(so);
                }
            
            }
            List<SVMXC__Service_Contract_Products__c> cprodlist =[select id,SVMXC__Installed_Product__c, SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.SVMXC__Active__c from SVMXC__Service_Contract_Products__c where SVMXC__Installed_Product__c in :ipid and SVMXC__Service_Contract__r.SVMXC__Active__c = true ];
            
            for(SVMXC__Service_Contract_Products__c cp:cprodlist){
            
                if(cp.SVMXC__Installed_Product__c!= null && cp.SVMXC__Service_Contract__r.SVMXC__Active__c)
                ipidSCPActiveMap.put(cp.SVMXC__Installed_Product__c,cp);
            }
            system.debug('wolis:t'+wolist);
        }
        
        if(iplist.size()>0)
        {  
        
            for(SVMXC__Installed_Product__c ip :iplist)
            {
               system.debug('***Class Calling***');
              if(ip.LifeCycleStatusOfTheInstalledProduct__c==system.label.CLAPR14SRV05 || ip.LifeCycleStatusOfTheInstalledProduct__c==system.label.CLAPR14SRV04 ||(ip.LifeCycleStatusOfTheInstalledProduct__c==system.label.CLAPR14SRV07 && ip.DecomissioningDate__c !=null) )
                {
                    ip.AssetCategory2__c =system.label.CLAPR14SRV00;
                    system.debug('11:'+ip.AssetCategory2__c);
                }
                else if(ip.UnderContract__c==true || ipidSCPActiveMap.containskey(ip.id))
                {
                    ip.AssetCategory2__c =system.label.CLAPR14SRV03;
                    system.debug('22:'+ip.AssetCategory2__c);
                }
                
                else if(ip.LifeCycleStatusOfTheInstalledProduct__c==system.label.CLAPR14SRV06) 
                {
                    ip.AssetCategory2__c =system.label.CLAPR14SRV02;
                    system.debug('33:'+ip.AssetCategory2__c);
                }
                else if(acclist.size()>0 )
                {
                            ip.AssetCategory2__c =system.label.CLAPR14SRV02;
                            system.debug('33:'+ip.AssetCategory2__c);
                }
                else if(ipidwoListMap.containsKey(ip.id))
                {
                    if(ipidwoListMap.get(ip.id).size()>0){
                         ip.AssetCategory2__c =system.label.CLAPR14SRV02;
                         system.debug('44:'+ip.AssetCategory2__c);
                    }
                }
                
                else
                {
                    ip.AssetCategory2__c =system.label.CLAPR14SRV01;
                    system.debug('4455:'+ip.AssetCategory2__c);
                
                }
                system.debug('******************'+ip.AssetCategory2__c);
                system.debug('******************'+oldmap.get(ip.id) );
                if(ip.AssetCategory2__c != oldmap.get(ip.id) ){
                    returnList.add(ip);
                }
            }
            
            
            
        }
        if(returnList != null && returnList.size()>0)
        return returnList;
        else {return null;}

    }
/*   
/*
Created by-Deepak
April 14 Release
BR-4237-Throw a validation error when if IB is already there with same specification...
Date-26-02-2014
*/
    public Static void IBValidation(List<SVMXC__Installed_Product__c> iplist)
    { 
        //  EUS - 040105
        /*
        set<Id> bid = New set<Id>();
        set<Id> dtid = New set<Id>();
        set<Id> rid = New set<Id>();
        set<String> strings = New set<String>();
        Set<String> techField = new Set<String>();
        Map<String, SVMXC__Installed_Product__c> existingObjsMap = new Map<String, SVMXC__Installed_Product__c>();
        
        List<SVMXC__Installed_Product__c> ipl = New List<SVMXC__Installed_Product__c>();
        if(iplist.size()>0)
        {
            for(SVMXC__Installed_Product__c ip:iplist)
            {
                if(ip.Brand2__c!=null &&ip.DeviceType2__c !=null && 
                   ip.Category__c !=null && ip.SVMXC__Serial_Lot_Number__c!=null && ip.SVMXC__Serial_Lot_Number__c!='')// && ip.SVMXC__Product__c !=null)
                {  
                    bid.add(ip.Brand2__c);
                    dtid.add(ip.DeviceType2__c);
                    rid.add(ip.Category__c);
                    strings.add(ip.SVMXC__Serial_Lot_Number__c);
                    String st=''+ip.Category__c+ip.Brand2__c+ip.SVMXC__Serial_Lot_Number__c+ip.DeviceType2__c;
                    techField.add(st);
                }
            }
        }

        // EUS - 040105
        //ipl =[Select Id,Name,Tech_DuplicateRecordID__c From SVMXC__Installed_Product__c Where DeviceType2__c =:dtid AND 
              // Brand2__c =:bid AND Category__c =:rid AND SVMXC__Serial_Lot_Number__c =:strings AND Tech_DuplicateRecordID__c=''];

        if(techField != null && techField.size()>0)

        {      
            ipl =[Select Id,Name,Tech_DuplicateRecordID__c,Brand2__c ,DeviceType2__c ,Category__c,SVMXC__Serial_Lot_Number__c,UniqueIBfieldID__c From SVMXC__Installed_Product__c Where UniqueIBfieldID__c in :techField];

            if(ipl != null && ipl.size()>0){
                for(SVMXC__Installed_Product__c exobj:ipl)
                {
                    
                    existingObjsMap.put(exobj.UniqueIBfieldID__c, exobj);
                }
                for(SVMXC__Installed_Product__c ip:iplist)
                {
                    if(ip.SVMXC__Product__c ==null && (ip.Tech_DuplicateRecordID__c==null || ip.Tech_DuplicateRecordID__c ==''))
                    {
                        //if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLSEP12PRM01))
                        //{
                            if(existingObjsMap.containskey(''+ip.Category__c+ip.Brand2__c+ip.SVMXC__Serial_Lot_Number__c+ip.DeviceType2__c))
                            {
                                ip.Tech_DuplicateRecordID__c = existingObjsMap.get(''+ip.Category__c+ip.Brand2__c+ip.SVMXC__Serial_Lot_Number__c+ip.DeviceType2__c).id;
                            }
                       // }
                    }
                    else if(ip.SVMXC__Product__c ==null && (ip.Tech_DuplicateRecordID__c!=null || ip.Tech_DuplicateRecordID__c !=''))
                    {
                       if(existingObjsMap.containskey(''+ip.Category__c+ip.Brand2__c+ip.SVMXC__Serial_Lot_Number__c+ip.DeviceType2__c))
                        {
                                ip.Tech_DuplicateRecordID__c = existingObjsMap.get(''+ip.Category__c+ip.Brand2__c+ip.SVMXC__Serial_Lot_Number__c+ip.DeviceType2__c).id;
                        }                
                    }
                    
                    
                }
                
                
            }
            else
            {
                for(SVMXC__Installed_Product__c ip:iplist)

                {
                    ip.Tech_DuplicateRecordID__c ='';

                }
            }
            
        }
        
        */
    }
    
     // Added By anand for Q3 Release for BR-8232
    public Static void acceptedinstalledproduct(set<id> ipset,map<string,SVMXC__Installed_Product__c> groupidmap)
    {
        List<SVMXC__Installed_Product__c> updatediplist= new List<SVMXC__Installed_Product__c>();
        List<SVMXC__Installed_Product__c> iplist= new List<SVMXC__Installed_Product__c>();
        iplist=[select id,name,ProductGroupID__c from SVMXC__Installed_Product__c where ProductGroupID__c in:groupidmap.keySet() AND id not in:ipset];
        if(iplist.size()>0) {
        for(SVMXC__Installed_Product__c ip:iplist){
            ip.SVMXC__Date_Installed__c=groupidmap.get(ip.ProductGroupID__c).SVMXC__Date_Installed__c;
            updatediplist.add(ip);      
            }
        }
        if(updatediplist.size()>0)
            update updatediplist;
    }
    
// Added By anand for Q3 Release for BR-9243
  public Static void ProductWarrantyDeletion(List<SVMXC__Installed_Product__c> iplist)
    {
    set<string> StringSet = New Set<string>();
    set<Id> ipidset = New Set<Id>();
    list<SVMXC__Installed_Product__c> ipplist= new  list<SVMXC__Installed_Product__c>();
    List<SVMXC__Service_Template_Products__c> ProductAPList =new List<SVMXC__Service_Template_Products__c> ();
    string DBRstring ;
     if(iplist!=null && iplist.size()>0){
            for(SVMXC__Installed_Product__c ip :iplist){
              if(ip.Brand2__c != null && ip.Category__c != null &&  ip.DeviceType2__c != null ){
                    DBRstring =String.valueOf(ip.Brand2__c).SubString(0,15)+'-'+String.valueOf(ip.Category__c).SubString(0,15)+'-'+String.valueOf(ip.DeviceType2__c).SubString(0,15);
                    StringSet.add(DBRstring);
                }
            }
        }
        if(StringSet.size()>0){
            ProductAPList = [Select Id,Tech_BDRIdSet__c,SVMXC__Service_Template__c,Product__c,Product__r.TECH_IsEnriched__c,UniqueAPId__c From SVMXC__Service_Template_Products__c Where Tech_BDRIdSet__c in :StringSet ];
            }
            
        if(ProductAPList.size()==0) 
        {
        if(iplist!=null && iplist.size()>0){
        for(SVMXC__Installed_Product__c ip :iplist)
            {

                ipidset.add(ip.id);
            }
        }
        if(ipidset.size()>0){
            list<SVMXC__Warranty__c> warrentylist=[select id,name,SVMXC__Installed_Product__c from SVMXC__Warranty__c where SVMXC__Installed_Product__c in:ipidset];
            if (warrentylist.size()>0)
                Delete warrentylist;
        }
        
        }
    }
 
 
    public Static void ProductWarrantyCreation(List<SVMXC__Installed_Product__c> iplist)
    {
         
    
        string DBRstring ;
        Boolean isAPFound = false;
        set<string> StringSet = New Set<string>();
        set<Id> ipid = New Set<Id>();
        set<String> WTIdset = New Set<String>();
        //set<Id> WTIdset1 = New Set<Id>();
        List<SVMXC__Service_Template__c> wtlist = New List<SVMXC__Service_Template__c>();
        List<SVMXC__Warranty__c> listpw = New List<SVMXC__Warranty__c>();
        List<SVMXC__Warranty__c> listpwtoupdate = New List<SVMXC__Warranty__c>();
        List<SVMXC__Warranty__c> PWlist = New List<SVMXC__Warranty__c>();
        List<SVMXC__Installed_Product__c> IPlisttoUpdate= New List<SVMXC__Installed_Product__c>();
        List<SVMXC__Warranty__c> newWarrantyList = new List<SVMXC__Warranty__c>();
        List<SVMXC__Warranty__c> updateWarrantyList = new List<SVMXC__Warranty__c>();

        Map<id,SVMXC__Service_Template__c> wtRecMap = new Map<id,SVMXC__Service_Template__c>();
        Map<id,SVMXC__Service_Template__c> apWTRecMap = new Map<id,SVMXC__Service_Template__c>();
        
        
        Map<String,id> stwarridmap = new Map<String,id>();
        //Map<String,id> stwarridmap1 = new Map<String,id>();
        Map<String,SVMXC__Warranty__c> wtexWarrlistMap = new Map<String,SVMXC__Warranty__c>();
        Set<String> ipPorductIdSet = new set<String>();
        Map<String , id> stidmap = new Map<String,id>();
        Set<id> idSet = new Set<id>();
        Set<String> ipstSet = new Set<String>();
        
        
        Map<String,List<SVMXC__Service_Template_Products__c>> bcdServceTempMap = new Map<String,List<SVMXC__Service_Template_Products__c>>();
        
        Map<id,SVMXC__Installed_Product__c> ipMap = new Map<id,SVMXC__Installed_Product__c>();
        Map<id,List<SVMXC__Warranty__c>> ipwMap = new Map<id,List<SVMXC__Warranty__c>>();
        List<SVMXC__Warranty__c> todelete = new List<SVMXC__Warranty__c>();
        
        if(iplist!=null && iplist.size()>0){
            for(SVMXC__Installed_Product__c ip :iplist){
              if(ip.Brand2__c != null && ip.Category__c != null &&  ip.DeviceType2__c != null ){
                    DBRstring =String.valueOf(ip.Brand2__c).SubString(0,15)+'-'+String.valueOf(ip.Category__c).SubString(0,15)+'-'+String.valueOf(ip.DeviceType2__c).SubString(0,15);
                    StringSet.add(DBRstring);
                    system.debug('string set values'+StringSet);
                        if(ip.SVMXC__Product__c !=null)
                        {
                            stidmap.put(DBRstring, ip.SVMXC__Product__c);
                            ipPorductIdSet.add(ip.SVMXC__Product__c);
                            system.debug('please show product'+ip.SVMXC__Product__c);
                            ipstSet.add(ip.id);
                            system.debug('please show product id'+ip.id);
                        }
                }
            }
            ipMap.putAll(iplist);
            for(SVMXC__Warranty__c waobj: [select id,SVMXC__Installed_Product__c,SVMXC__End_Date__c, SVMXC__Service_Template__c from SVMXC__Warranty__c where SVMXC__Installed_Product__c in :ipMap.keySet() ]){
                if(ipwMap.containskey(waobj.SVMXC__Installed_Product__c)){
                    ipwMap.get(waobj.SVMXC__Installed_Product__c).add(waobj);
                }
                else{
                    ipwMap.put(waobj.SVMXC__Installed_Product__c, new List<SVMXC__Warranty__c> {waobj});
                }
            }
        }
        
        if(StringSet.size()>0){
            //only one applicable product it will select
            
            
            List<SVMXC__Service_Template_Products__c> ProductAPList = [Select Id,Tech_BDRIdSet__c,SVMXC__Service_Template__c,Product__c,Product__r.TECH_IsEnriched__c,UniqueAPId__c From SVMXC__Service_Template_Products__c Where Tech_BDRIdSet__c in :StringSet ];
            system.debug('Applicable Product'+ProductAPList.size());
            for(SVMXC__Service_Template_Products__c obj:ProductAPList){
                 system.debug('ipPorductIdSet'+ipPorductIdSet); 
                 system.debug('Applicable Product'+obj.Product__c);
                 if(!bcdServceTempMap.containskey(obj.Tech_BDRIdSet__c))
                 {
                    List<SVMXC__Service_Template_Products__c> ApplicableProducts =new List<SVMXC__Service_Template_Products__c>();
                    ApplicableProducts.add(obj);
                    bcdServceTempMap.put(obj.Tech_BDRIdSet__c, ApplicableProducts);
                 }
                 else{
                    bcdServceTempMap.get(obj.Tech_BDRIdSet__c).add(obj);
                 }
                  WTIdset.add(obj.SVMXC__Service_Template__c); 
                  stwarridmap.put(obj.Tech_BDRIdSet__c,obj.SVMXC__Service_Template__c);
            }
            System.debug('\n log: '+bcdServceTempMap);
            system.debug('ip record status'+ bcdServceTempMap);
            /*for(List<SVMXC__Service_Template_Products__c> aps :bcdServceTempMap.values()){
                for(SVMXC__Service_Template_Products__c ap:aps)
                {
                     WTIdset.add(ap.SVMXC__Service_Template__c); 
                     system.debug('warrenty term name'+ap.SVMXC__Service_Template__c);
                     stwarridmap.put(ap.Tech_BDRIdSet__c,ap.SVMXC__Service_Template__c);
                }
            }*/
        } 
             
        if(WTIdset != null && WTIdset.size()>0)
        {
            System.debug('\n log: '+WTIdset);
            List<SVMXC__Service_Template__c> wtobjlist=[Select Id,SVMXC__Unit_of_Time_Material__c,SVMXC__Duration_of_Material_Coverage__c From SVMXC__Service_Template__c Where id in :WTIdset];
            System.debug('warrenty list size '+wtobjlist.size());
           if(wtobjlist != null && wtobjlist.size()>0)
            {                
                wtRecMap.putAll(wtobjlist);             
            }
                
           // List<SVMXC__Warranty__c>  wlist =[select id,SVMXC__Installed_Product__c,SVMXC__End_Date__c, SVMXC__Service_Template__c from SVMXC__Warranty__c where SVMXC__Service_Template__c in : WTIdset ];
           String st = SOQLListFormat( WTIdset);
           system.debug('installed product size'+ipstSet.size());
           string ipidst = SOQLListFormat( ipstSet);
           String stQuery = 'select id,SVMXC__Installed_Product__c,SVMXC__End_Date__c, SVMXC__Service_Template__c from SVMXC__Warranty__c where SVMXC__Service_Template__c in ('+ st +')  and  SVMXC__Installed_Product__c in ('+ ipidst +') ';
           
            system.debug('product warrenty size'+stQuery);
           List<SVMXC__Warranty__c>  wlist = Database.query(stQuery);
           system.debug('product warrenty  list size'+wlist.size());
            for(SVMXC__Warranty__c w: wlist){
                String concatstidipid = String.valueOf(w.SVMXC__Service_Template__c)+String.valueOf(w.SVMXC__Installed_Product__c);
                wtexWarrlistMap.put(concatstidipid, w);               
                /*if(wtexWarrlistMap.containskey(w.SVMXC__Service_Template__c+w.SVMXC__Installed_Product__c))
                {
                    wtexWarrlistMap.get(w.SVMXC__Service_Template__c).add(w);
                }
                else{                   
                    wtexWarrlistMap.put(w.SVMXC__Service_Template__c, new List<SVMXC__Warranty__c> {w});                
                }*/
            }
        }
        
        for(SVMXC__Installed_Product__c ip :iplist){
            
            if(ip.Brand2__c != null && ip.Category__c != null &&  ip.DeviceType2__c != null ){
            
                DBRstring =String.valueOf(ip.Brand2__c).SubString(0,15)+'-'+String.valueOf(ip.Category__c).SubString(0,15)+'-'+String.valueOf(ip.DeviceType2__c).SubString(0,15);
                
                SVMXC__Service_Template_Products__c ApplicableProduct = new SVMXC__Service_Template_Products__c();
                Boolean isQadrilet = false;
                Boolean productIsEnriched = false;
                //first check with BR-8022 For Qadrilet Condition then go  
                if(bcdServceTempMap.containskey(DBRstring))
                {
                    List<SVMXC__Service_Template_Products__c> ApplicableProducts = bcdServceTempMap.get(DBRstring);
                    for(SVMXC__Service_Template_Products__c app: ApplicableProducts){
                        System.debug('\n clog: '+ip);
                        System.debug('\n clog: '+app.Product__c);
                        System.debug('\n clog: '+ip.SVMXC__Product__c);
                        if(ip.SVMXC__Product__c != null && app.Product__c != null && ip.SVMXC__Product__c == app.Product__c)
                        {   
                            
                            ApplicableProduct = app;
                            isQadrilet = true;
                        }
                    }
                }
             
                 //ApplicableProduct.Product__r.TECH_IsEnriched__c = true
                 if(bcdServceTempMap.containskey(DBRstring)){                
                                        
                    List<SVMXC__Service_Template_Products__c> ApplicableProducts = bcdServceTempMap.get(DBRstring);
                    for(SVMXC__Service_Template_Products__c app: ApplicableProducts){
                        System.debug('\n clog: '+ip);
                        System.debug('\n clog: '+app.Product__c);
                        System.debug('\n clog: '+ip.SVMXC__Product__c);
                        if(app.Product__c != null && app.Product__r.TECH_IsEnriched__c)
                        {                           
                            ApplicableProduct = app;
                            productIsEnriched = true;
                        }
                    }
                 
                 }
                if(isQadrilet || productIsEnriched){
                
                    id wtid =ApplicableProduct.SVMXC__Service_Template__c ;
                    String wtidipid = String.valueOf(wtid)+String.valueOf(ip.id);
                    if(wtexWarrlistMap.containskey(wtidipid)){
                        //isFound = true;
                        SVMXC__Warranty__c wobj = wtexWarrlistMap.get(wtidipid);
                        if( !idSet.contains(wobj.id)){
                                //isFound = true;                       
                                SVMXC__Service_Template__c wtermobj = wtRecMap.get(wtid);
                                wobj.SVMXC__Start_Date__c =ip.WarrantyTriggerDate__c;
                                Integer freq = wtermobj.SVMXC__Duration_of_Material_Coverage__c.intValue();
                            
                                date dt = ip.WarrantyTriggerDate__c;
                                if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Days'){
                                    dt=dt.addDays(freq);
                                    wobj.SVMXC__End_Date__c = dt-1;   
                                }
                                else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Months'){                            
                                    dt=dt.addMonths(freq);
                                    wobj.SVMXC__End_Date__c = dt-1;
                                }
                               else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Years'){                          
                                    dt=dt.addYears(freq);
                                    wobj.SVMXC__End_Date__c = dt-1;
                                }
                                else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Weeks'){                         
                                    dt=dt.addDays(freq*7);
                                    wobj.SVMXC__End_Date__c = dt-1;                   
                                }
                                
                            updateWarrantyList.add(wobj);
                            idSet.add(wobj.id);
                        }
                        
                    }
                     else{
                    
                            if(ipwMap.containskey(ip.id))
                            {
                                SVMXC__Warranty__c wobj =   ipwMap.get(ip.id)[0];
                                SVMXC__Service_Template__c wtermobj = wtRecMap.get(wtid);
                                wobj.SVMXC__Service_Template__c = wtermobj.id;                              
                                wobj.SVMXC__Start_Date__c =ip.WarrantyTriggerDate__c;
                                Integer freq = wtermobj.SVMXC__Duration_of_Material_Coverage__c.intValue();
                            
                                date dt = ip.WarrantyTriggerDate__c;
                                if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Days'){
                                    dt=dt.addDays(freq);
                                    wobj.SVMXC__End_Date__c = dt-1;   
                                }
                                else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Months'){                            
                                    dt=dt.addMonths(freq);
                                    wobj.SVMXC__End_Date__c = dt-1;
                                }
                               else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Years'){                          
                                    dt=dt.addYears(freq);
                                    wobj.SVMXC__End_Date__c = dt-1;
                                }
                                else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Weeks'){                         
                                    dt=dt.addDays(freq*7);
                                    wobj.SVMXC__End_Date__c = dt-1;                   
                                }
                                
                                updateWarrantyList.add(wobj);
                                idSet.add(wobj.id);
                            
                            }
                            else{
                                
                                SVMXC__Service_Template__c wtermobj = wtRecMap.get(wtid);
                                SVMXC__Warranty__c w = new SVMXC__Warranty__c();
                                w.SVMXC__Installed_Product__c = ip.id;
                                w.SVMXC__Service_Template__c = wtid;
                                w.SVMXC__Start_Date__c =ip.WarrantyTriggerDate__c;
                                Integer freq = wtermobj.SVMXC__Duration_of_Material_Coverage__c.intValue();
                                
                                date dt = ip.WarrantyTriggerDate__c;
                                if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Days'){
                                    dt=dt.addDays(freq);
                                    w.SVMXC__End_Date__c = dt-1;  
                                }
                                else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Months'){                            
                                    dt=dt.addMonths(freq);
                                    w.SVMXC__End_Date__c = dt-1;
                                }
                               else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Years'){                          
                                    dt=dt.addYears(freq);
                                    w.SVMXC__End_Date__c = dt-1;
                                }
                                else if(wtermobj.SVMXC__Unit_of_Time_Material__c=='Weeks'){                         
                                    dt=dt.addDays(freq*7);
                                    w.SVMXC__End_Date__c = dt-1;                  
                                }
                                
                                newWarrantyList.add(w);
                            
                            }
                    
                    }
                    
                }
            }
        }
        if(newWarrantyList != null && newWarrantyList.size()>0)
        {
            system.debug('warrentylist size'+newWarrantyList.size());
            insert newWarrantyList;
            //DataBase.insert(newWarrantyList,false);
        
        }
        if(updateWarrantyList != null && updateWarrantyList.size()>0)
        {
        system.debug('updateWarrantyList size'+newWarrantyList.size());
           update updateWarrantyList;
           //  DataBase.Update(updateWarrantyList,false);
        
        }
        if(todelete != null && todelete.size()>0){
            //delete todelete;

        }
        
        
    }
    public Static void ScToUpdate(set<Id> ipid)
    {
        set<Id> scid= new set<Id>();
        Map<Id,List<SVMXC__Service_Contract_Products__c>> ipidCPlistMap = new Map<Id,List<SVMXC__Service_Contract_Products__c>>();
        List<SVMXC__Service_Contract_Products__c> cplist=new List<SVMXC__Service_Contract_Products__c>();
        cplist=[Select Id,SVMXC__Service_Contract__r.ParentContract__c,SVMXC__Installed_Product__c,SVMXC__Service_Contract__c From SVMXC__Service_Contract_Products__c Where SVMXC__Installed_Product__c in :ipid];
        for(SVMXC__Service_Contract_Products__c cp :cplist){
            if(ipidCPlistMap.containskey(cp.SVMXC__Installed_Product__c)){
                ipidCPlistMap.get(cp.SVMXC__Installed_Product__c).add(cp);
            }
            else{
                ipidCPlistMap.put(cp.SVMXC__Installed_Product__c, new List<SVMXC__Service_Contract_Products__c>{cp});
            
            }
        
        }
        for(Id ipi :ipid){
            if(ipidCPlistMap.containsKey(ipi)){
                for(SVMXC__Service_Contract_Products__c cp :ipidCPlistMap.get(ipi)){
                     //if(cp.SVMXC__Service_Contract__r.ParentContract__c !=null){
                        //scid.add(cp.SVMXC__Service_Contract__r.ParentContract__c);//Commented for Defect#7368
                      if(cp.SVMXC__Service_Contract__c!=null){
                        scid.add(cp.SVMXC__Service_Contract__c);  
                    }
                }
            }
        }
        if(scid !=null){
            AP_ServiceContract.SCIBSyncUpdate(scid);
        }
    }
    
    public Static void IBchangeMaster(set<id>set1){
    list<SVMXC__Installed_Product__c> iplist= new list<SVMXC__Installed_Product__c>();
    list<SVMXC__Installed_Product__c> iplist1= new list<SVMXC__Installed_Product__c>();
    map<id,SVMXC__Installed_Product__c> ipmap= new map<id,SVMXC__Installed_Product__c>();
    //if(set1!= null)
    if( !(set1.isempty()) && (set1.size() > 0)) //Added by VISHNU C for July Release 2016 Resolving Too Many SOQL Error WO Interface
    {
        iplist=[select id,name,ManageIBinFO__c from SVMXC__Installed_Product__c where id in: set1];
        ipmap.putall(iplist);
    }
    if(!ipmap.isEmpty())
    for(SVMXC__Installed_Product__c ipp:ipmap.values())
            {
            if(ipp.ManageIBinFO__c!=true){
            ipp.ManageIBinFO__c=true;
            ipp.RecordTypeId=System.Label.CLAPR14SRV09;
            iplist1.add(ipp);
                }
            }
            if(iplist1!=null)
            update iplist1;
    }
    //For Creation of Location
    public Static void CreateLocation(List<SVMXC__Installed_Product__c> iplist,set<id> accidset){
        set<Id> locid = new set<Id>();
        map<Id,Id> accLocMap = new map<Id,Id> ();
         Map<id,Id> accidsiteidMap = new Map<id,Id>();
          List<SVMXC__Site__c> PrimaryLocationNeedtoCreate = new List<SVMXC__Site__c>();
          Set<id> accneedprimarylocIdSet = new Set<id>();
          
        if(iplist.size()>0 && accidset !=null){
            
            for(SVMXC__Site__c obj:[select id,SVMXC__Account__c,PrimaryLocation__c from SVMXC__Site__c where  SVMXC__Account__c in :accidset and PrimaryLocation__c = true])
            {
                if(obj.PrimaryLocation__c)
                    accidsiteidMap.put(obj.SVMXC__Account__c,obj.id);
            }
            for(ID accid :accidset){
                if(!accidsiteidMap.containskey(accid))
                accneedprimarylocIdSet.add(accid);
            }
            
            if(accneedprimarylocIdSet != null && accneedprimarylocIdSet.size()>0)
            {
                List<SVMXC__Site__c> siteList = Ap_Location.createLocation(accneedprimarylocIdSet);
                if(siteList != null && siteList.size()>0){
                    Database.SaveResult[] srList = Database.insert(siteList, false);
                    for(Integer k=0;k<srList.size();k++ )
                    {
                        Database.SaveResult sr =srList[k];
                        if(sr.isSuccess())
                        {
                            accidsiteidMap.put(siteList[k].SVMXC__Account__c,sr.getId());
                        }
                    }                       
                }
                
            }
            for(SVMXC__Installed_Product__c ip :iplist){
                if(accidsiteidMap.containsKey(ip.SVMXC__Company__c)){
                    ip.SVMXC__Site__c=accidsiteidMap.get(ip.SVMXC__Company__c);
                }
            }
            
        }
    }
    //WD error message: Secondary Ip is decommissioned.
    public Static void  IPWorkDetailUpdate(set<Id> ipid) 
    {
        List<SVMXC__Service_Order_Line__c> wdlist = new List<SVMXC__Service_Order_Line__c>();
        set<Id> woid = new set<Id>();
        string ProductServiced = Label.CLAPR15SRV60;
        if ( !ipid.isempty() && ipid.size() > 0 ) //Added by VISHNU C for Too Many SOQL Error B2F SC Interface
        {
            wdlist =[Select Id,Name,SVMXC__Serial_Number__c,SVMXC__Service_Order__c From SVMXC__Service_Order_Line__c Where RecordTypeId=:ProductServiced and (SVMXC__Service_Order__r.SVMXC__Order_Status__c !='New' or SVMXC__Service_Order__r.SVMXC__Order_Status__c !='Unscheduled') and SVMXC__Serial_Number__c in :ipid];
            
            for(SVMXC__Service_Order_Line__c wd :wdlist){
                if(wd.SVMXC__Service_Order__c !=null){
                    woid.add(wd.SVMXC__Service_Order__c);
                }
            }
            
            List<SVMXC__Service_Order__c> wolist = new List<SVMXC__Service_Order__c>();
            
            List<SVMXC__Service_Order__c> wolisttoupdate = new List<SVMXC__Service_Order__c>();
            //if(woid !=null)
            if (!woid.isempty() && woid.size() > 0 ) //Added by VISHNU C for Too Many SOQL Error B2F SC Interface. Commented above if loop
            {
                wolist=[Select Id,Tech_SwappedSecondryIP__c from SVMXC__Service_Order__c where id in :woid  and (SVMXC__Order_Status__c !='Unscheduled' OR SVMXC__Order_Status__c !='New')];
            }
            if(wolist.size()>0){
                for(SVMXC__Service_Order__c wo :wolist){
                    if(wo.Tech_SwappedSecondryIP__c ==false){
                        wo.Tech_SwappedSecondryIP__c =true;
                    }
                    wolisttoupdate.add(wo);
                }
                
            }
            if(wolisttoupdate.size()>0){
                Database.SaveResult[] srList = Database.update(wolisttoupdate, false);

            }
        }
         
    }
    
    public static string SOQLListFormat(set<string> input){   
        String SOQL_ListFormat = '';
        for (string Value : input) {
                String value_in_quotes = '\''+String.escapeSingleQuotes(Value)+'\'';
        if (SOQL_ListFormat!='') { SOQL_ListFormat+=','; }  //  add a comma if this isn't the first one
                SOQL_ListFormat += value_in_quotes;
        }   
        return SOQL_ListFormat;
    }
    
 }