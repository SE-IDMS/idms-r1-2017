global with sharing class VFC_AddOfferComponent{ 
        
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Offer_Lifecycle__c offerRecord{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public String compString{get;set;}{compString='';}
    public String businessString{get;set;}{businessString='';}
    public String productLineString{get;set;}{productLineString='';}
    public String productFamilyString{get;set;}{productFamilyString='';}
    public String familyString{get;set;}{familyString='';}
    Public List<OfferComponents__c> lstOCs {get; set;}
    Public List<Offer_Lifecycle__c > offerList {get; set;}
    public String textarea {get;set;}
    public boolean IsError{get;set;}
    public VFC_AddOfferComponent(ApexPages.StandardController controller){        
        offerRecord = (Offer_Lifecycle__c)controller.getRecord();
        IsError=false;
        set<Id> userAccSet = new set<Id>();         
        id profileId = [Select Id,Name from Profile where name = 'System Administrator'].id;
          Offer_Lifecycle__c offerRecord1 = [Select id,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c, name from Offer_Lifecycle__c where id =:offerRecord.id];
        userAccSet = new set<Id>{offerRecord1.ownerid,offerRecord1.Segment_Marketing_VP__c,offerRecord1.Launch_Area_Manager__c,offerRecord1.Marketing_Director__c,offerRecord1.Withdrawal_Owner__c};
        system.debug('** Offer Record'+ offerRecord);
        
         if(!userAccSet.contains(Userinfo.getuserid()) && userinfo.getProfileid()!=profileId) {                
             IsError=true;                
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));           
         }   
    }
    
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
     @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        system.debug('**exact search'+ exactSearch);
        system.debug('**searchincommercialrefs'+ searchincommercialrefs);
        system.debug('**excludeGDP'+ excludeGDP);
        system.debug('**excludeDocs'+ excludeDocs);
        system.debug('**excludeSpares'+ excludeSpares);
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
 
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        system.debug('&&& business'+ business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    
    Public pagereference performSelect(){ 
    
                  
        if(compString=='')    
            compString=pageParameters.get('compString');    
            
       system.debug('***compString'+compString);
       
       if(businessString=='')    
            businessString=pageParameters.get('businessString'); 
      
       if(productLineString=='')    
            productLineString=pageParameters.get('productLineString'); 
      
      if(productFamilyString=='')    
            productFamilyString=pageParameters.get('productFamilyString'); 
       
      if(familyString=='')    
            familyString=pageParameters.get('familyString');  
       
       system.debug('** businessString'+ businessString);
       system.debug('** productLineString'+ productLineString);
       system.debug('** productFamilyString'+ productFamilyString);
       system.debug('** familyString'+ familyString);
           
            OfferComponents__c oc = new OfferComponents__c();
            oc.OfferLifeCycle__c = offerRecord.id;
            if( businessString!='--None--')
            oc.Business__c = businessString;
             if( productLineString!='--None--')
            oc.ProductLine__c = productLineString;
            if( productFamilyString!='--None--')
            oc.ProductFamily__c = productFamilyString;
            if( familyString!='--None--')
            oc.Family__c = familyString;
            oc.Component__c = textarea;
            system.debug('** oc'+ oc);
            
        try{
            insert oc;
            
            PageReference offerPage;            
            offerPage=new PageReference('/'+ offerRecord.id);                        
            return offerPage;   
                   
        }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    }      
    
 public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(offerRecord.id!=null){
            pageResult = new PageReference('/'+ offerRecord.id);
             
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
}