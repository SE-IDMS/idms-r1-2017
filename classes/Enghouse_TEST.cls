/**
    Author          : Nitin Khunal
    Date Created    : 19/09/2016
    Description     : Test Class for Enghouse apex class
*/
@isTest
public class Enghouse_TEST {
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        testAccount.City__c = 'Bangalore';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        testcontact.Phone = '999999999';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        Case objCase = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id,'testing');
        insert objCase;
        Task caseTask = Utils_TestMethods.createTask(objCase.Id, testcontact.Id, 'In Progress');
        insert caseTask;
    }
    
    @isTest static void testMethod1() {
        List<Task> t = [select id from task limit 1];
        List<Contact> c = [select id,phone from contact limit 1];
        Enghouse.getUser();
        Enghouse.getActivityFields(t[0].Id, false);
        Enghouse.SearchClid(t[0].Id, '999999999');
        Enghouse.SearchClid(t[0].Id, ':@');
        Enghouse.SearchQuery(t[0].Id, 'Test');
        Enghouse.SearchQuery('00000000000000000000000000', 'Test');
        Enghouse.getHistory(t[0].Id, '');
        Enghouse.getHistory(t[0].Id, '5');
        Enghouse.getPicklistValues(t[0].Id, 'task', 'status');
        Enghouse.getPicklistValues(t[0].Id, 't', 'status');
        Enghouse.getObjectFields(t[0].Id,'task');
        Enghouse.getObjectFields(t[0].Id,'t');
        String query = 'select id from task limit 1';
        Enghouse.getQuery(t[0].Id, query);
        Enghouse.getQuery(t[0].Id, 'adsdfsf');
    }
}