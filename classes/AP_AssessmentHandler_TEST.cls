@isTest
private class AP_AssessmentHandler_TEST{
    public static testMethod void myUnitTest(){
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            usr.BypassWF__c = false;
            usr.ByPassVR__c = true;
            usr.ProgramAdministrator__c = true;
                        
            System.runas(usr){
                
                Country__c country = Utils_TestMethods.createCountry();
                country.countrycode__c ='ASM';
                insert country;
                
                //creates a global partner program
                PartnerPRogram__c gp1 = new PartnerPRogram__c();
                gp1 = Utils_TestMethods.createPartnerProgram();
                gp1.TECH_CountriesId__c = country.id;
                gp1.recordtypeid = Label.CLMAY13PRM15;
                gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                insert gp1;
                
                //creates a program level for the global program
                ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                insert prg1;
                prg1.levelstatus__c = 'active';
                update prg1;
                
                //creates a country partner program
                PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
                insert cp1;
                cp1.ProgramStatus__c = 'active';
                update cp1;

                //creates a program level for the country partner program
                ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
                insert prg2;
                prg2.levelstatus__c = 'active';
                update prg2;

                Recordtype prgORFAssessmentRecordType = [Select id from RecordType where developername =  'PRMProgramApplication' limit 1];

                Assessment__c asm = Utils_TestMethods.createAssessment();
                asm.recordTypeId = prgORFAssessmentRecordType.Id ;
                asm.AutomaticAssignment__c = true;
                asm.Active__c = true;
                asm.Name = 'Sample Assessment';
                asm.PartnerProgram__c = cp1.id;
                asm.ProgramLevel__c = prg2.id;
                
                insert asm;
                
                
                AssessementProgram__c asmPgm = new AssessementProgram__c();
                //asmPgm = Utils_TestMethods.createAssessment();
                //asmPgm.AutomaticAssignment__c = true;
                //asmPgm.recordTypeId = prgORFAssessmentRecordType.id ;
                asmPgm.Assessment__c = asm.Id;
                asmPgm.Active__c = true;
                asmPgm.PartnerProgram__c = cp1.id;
                asmPgm.ProgramLevel__c = prg2.id;
                try {
                insert asmPgm;
                }
                catch(Exception e) {
                    System.debug('Error');
                }
                OpportunityAssessmentQuestion__c oppAssmQues = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'PickList', 1);
                oppAssmQues.Required__c = true;
                insert oppAssmQues;
                AP_AssessmentHandler asmHandler = new AP_AssessmentHandler();
                List<OpportunityAssessmentQuestion__c> oppList = new List<OpportunityAssessmentQuestion__c>();
                oppList.add(oppAssmQues);
                //AP_AssessmentHandler.UpdateGlobalProgramApprovalFlag(oppList);
            }
        }
    }
}