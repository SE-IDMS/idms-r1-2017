public class VFC36_CaseOrderController 
{
    public Case CurrentCase = new Case();
    public CaseOrder CaseImplementation{get;set;}

    public VFC36_CaseOrderController(ApexPages.StandardController controller) 
    {
        CurrentCase = (Case)controller.getRecord();
        CaseImplementation = new CaseOrder();
    }
   
    public class CaseOrder implements Utils_OrderMethods
    {    
        public void Init(VCC07_OrderConnectorController PageController)
        {
            List<Case> CseList = [SELECT ID, CaseNumber, AccountID FROM Case WHERE ID=:PageController.StandardObjectID LIMIT 1];
            
            System.debug('#### StandardObjectID : ' + PageController.StandardObjectID );
            if(CseList.size()==1)
                PageController.RelatedRecord = CseList[0];
                
            Case Cse = (Case)PageController.RelatedRecord;  
            PageController.ActionLabel = 'Select Order';   
            PageController.DateCapturer.Account__c = cse.AccountID;
          
            PageController.BackOfficeRefresher();  // Populate the back office list         

            // Key is made from the Order Number + The Back Office Name
            PageController.KeyForExistingRecords.add('SEOrderReference__c');
            PageController.KeyForExistingRecords.add('BackOffice__c'); 
             
            // Creation of the set of already existing external references 
            PageController.existingOrders = new Set<Object>();                 
            For(CSE_ExternalReferences__c ExtReference:[SELECT Id, Name, TECH_BackOffice__c, SystemReference__c, ClientReference__c, Case__c FROM CSE_ExternalReferences__c WHERE Case__c=:PageController.StandardObjectID]) 
            {
                String NewKey = ExtReference.SystemReference__c + ExtReference.TECH_BackOffice__c;
                PageController.existingOrders.add((String)NewKey);                              
            }
        }
        
        public pagereference Cancel(VCC07_OrderConnectorController PageController)
        {
            Case Cse = (Case)PageController.RelatedRecord;
            PageReference CasePage =  new ApexPages.StandardController(Cse).cancel();
            return CasePage;
        }
        
        public pagereference PerformAction(VCC07_OrderConnectorController PageController)
        {
            Case Cse = (Case)PageController.RelatedRecord;   
            CSE_ExternalReferences__c ExternalReference = new CSE_ExternalReferences__c();
            ExternalReference.Case__c = Cse.ID;  
            ExternalReference.ClientReference__c  = String.valueof(PageController.SelectedOrder.PONumber__c);
            ExternalReference.Type__c = Label.CL00496;
            ExternalReference.SystemReference__c = PageController.SelectedOrder.SEOrderReference__c;
            ExternalReference.ClientReference__c = String.valueof(PageController.SelectedOrder.PONumber__c);
            ExternalReference.TECH_BackOffice__c = PageController.SelectedOrder.BackOffice__c; 
            ExternalReference.TECH_Account__c = PageController.DateCapturer.Account__c;
            DataBase.SaveResult InsertedExternalReference = DataBase.insert(ExternalReference); 
                       
            CSE_ExternalReferences__c ExtReference = [SELECT Id, Name FROM CSE_ExternalReferences__c WHERE ID=:ExternalReference.Id];           
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00497 + ' ' + ExtReference.Name)); 
              
            String NewKey = ExternalReference.SystemReference__c + ExternalReference.TECH_BackOffice__c;
            PageController.existingOrders.add((String)NewKey); 
            
            PageController.DisableSelectAction = true;
            //Added as part of May 2013 Release- BR-2047
            PageController.tabnumber = 1;
            PageController.TabInFocus = 'OrderDetails';

            PageReference CasePage = new ApexPages.StandardController(Cse).view(); 
            return null;      
        }
    }
}