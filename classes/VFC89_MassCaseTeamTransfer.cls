/*
    Author          : Nicolas PALITZYNE (nicolas.palitzyne@accenture.com)
    Date Created    : 23/01/2012 
    Description     : Controller for VFP89 Visual Force page managing mass team changing
*/

public class VFC89_MassCaseTeamTransfer
{
    public List<String> caseListID{get;set;}

    public VFC89_MassCaseTeamTransfer(ApexPages.StandardSetController controller) 
    {
        System.debug('VFC89.Constructor.INFO - Constructor is called.');
    
        caseListID = new List<String>(); 
        List<Case> relatedCases = (List<Case>)controller.getSelected();
        
        for(Case caseItem:relatedCases ) {  
            caseListID.add(caseItem.ID);
        }
        
        System.debug('VFC89.Constructor.INFO - List of related case IDs  = ' + caseListID);
        System.debug('VFC89.Constructor.INFO - End of Constructor.');
    }    
}