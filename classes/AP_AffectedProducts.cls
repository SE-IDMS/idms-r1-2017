public class AP_AffectedProducts{
    public static void updateProductLine(list<CommercialReference__c> crlist){
        list<Problem__c> updatePrlmLst = new list<Problem__c>();
        set<id> prblmIds = new set<id>();
        for(CommercialReference__c cr : crlist){
            prblmIds.add(cr.Problem__c);
        }
        list<Problem__c> prblmLst = [Select id,Name,AffectedProductLines__c from Problem__c where id in: prblmIds];
        list<CommercialReference__c> crlst = [Select id, ProductLine__c, Problem__c from CommercialReference__c where Problem__c in: prblmIds ];
        for(Problem__c prblm : prblmLst){
            prblm.AffectedProductLines__c = '';
            for(CommercialReference__c cr : crlst){
                system.debug('product line of the Affected product is--->'+cr.ProductLine__c);
                if(cr.ProductLine__c != null && cr.ProductLine__c != ''){
                system.debug('affected Product lines are--->'+prblm.AffectedProductLines__c);
                    if(String.isNotBlank(prblm.AffectedProductLines__c) ){ // && prblm.AffectedProductLines__c != null ){
                        system.debug('enetered as there is the value in the affected product line');
                        if(!(prblm.AffectedProductLines__c.contains(cr.ProductLine__c.substring(0,5)))){
                            prblm.AffectedProductLines__c = prblm.AffectedProductLines__c+','+cr.ProductLine__c.substring(0,5);
                        }
                    }else if(prblm.AffectedProductLines__c == '' || prblm.AffectedProductLines__c == null){
                        system.debug('enetered as there is no  value in the affected product line');
                        prblm.AffectedProductLines__c = cr.ProductLine__c.substring(0,5);
                    }
                    system.debug('Affected Product line is--->'+prblm.AffectedProductLines__c+'--Problem is-->'+prblm.Name+'--Product line is -->'+cr.ProductLine__c);
                }
            }         
                updatePrlmLst.add(prblm);
        }
        if(updatePrlmLst.size()>0){
            update updatePrlmLst;
        }
    }
}