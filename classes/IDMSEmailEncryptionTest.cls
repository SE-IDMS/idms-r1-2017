/**
* This is the test class for IDMSEmailEncryption.
**/
@isTest
class IDMSEmailEncryptionTest{ 
    //This method is to test email encryption. 
    static testmethod void testIDMSEmailEncryption(){
        
        User userObj = new User(alias = 'user', email='testmyuser12934' + '@cognizant.com', 
                                emailencodingkey='UTF-8', lastname='Testing1', firstname='test1', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                timezonesidkey='Europe/London', username='testuser1234134' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                FederationIdentifier ='Testmyuser12345'+ '@bridge-fo.com',IDMSToken__c='test2367',IDMSSignatureGenerationDate__c=system.now().date());
        
        insert userObj;
        
        String signature='test2367';  
        Datetime dt=system.today().Adddays(8);
        userObj.IDMSToken__c='test2367';
        userObj.IDMSSignatureGenerationDate__c=system.now().date();      
        update userObj;
        
        IDMSEmailEncryption EmailEncryption=new IDMSEmailEncryption ();
        IDMSEmailEncryption.EmailDecryption('test2367',userObj.Id);
        IDMSEmailEncryption.EmailEncryption(userObj);
        IDMSEmailEncryption.generateRandomString(20);
    }
}