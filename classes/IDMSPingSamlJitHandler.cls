global class IDMSPingSamlJitHandler implements Auth.SamlJitHandler {
    
    public Final String division = 'division';
    public Final String country = 'c';
    public Final String phone = 'telephoneNumber';
    public Final String email = 'mail';
    public Final String FirstName = 'givenName';
    public Final String LastName = 'sn';
    public Final String City = 'l';
    public Final String FederatedId = 'employeeID';
    
    public final String profileIdInternalUser = system.Label.CLNOV16IDMS080;
    
    
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,String federationIdentifier, Map<String, String> attributes, String assertion) {
        /*system.debug('jit handler create user');
        displayMap(attributes);
        
            User jitUser = new User();
        
            //Fill user fields based on attributes of the SAML response
            if(String.isNotBlank(federationIdentifier)){
                jitUser.federationIdentifier = federationIdentifier.toUpperCase();
                mapPingIdmsUser(jitUser, attributes, true);
            }        
            //Create the corresponding person Account
            IDMS_UIMS_Mapping mapping = new IDMS_UIMS_Mapping();
            Account personAccount = mapping.createPersonAccount(jitUser);
            insert personAccount;
            
            //Get the contact Id value
            Account actRetrieve= [Select Id, PersonContactId From Account Where Id = :personAccount.Id Limit 1][0];
            String ContactId =  actRetrieve.PersonContactId;
            jitUser.ContactId = ContactId;
            
            system.debug('Complete jitUser for Ping: '+jitUser);
            return jitUser;*/
            return null;
    }
    
    public void updateUser(Id userId, String federationIdentifier, Map<String, String> attributes){
        //get the jitUser sObject
        if(String.IsNotBlank(userId)){
            List<User> jitUsers = [Select Id, isActive from User where Id = :userId and isActive = FALSE];
            if(jitUsers.size() > 0){
                User jitUser = jitUsers[0];
                jitUser.isActive = TRUE;
                Update jitUser;
                //mapPingIdmsUser(jitUser, attributes, false);
                //IDMSPingSamlJitHandler.updateAsync(jitUserJson);       
            
            } else {
                List<UserLogin > jitUsersLogin =  [Select Id, IsFrozen, IsPasswordLocked
                                            from UserLogin where userId = :userId and isFrozen = TRUE];
                if(jitUsersLogin.size() > 0){
                    UserLogin jitUserLogin = jitUsersLogin[0];
                    jitUserLogin.IsFrozen = FALSE;
                    jitUserLogin.IsPasswordLocked = FALSE;
                    update jitUserLogin;
                   //mapPingIdmsUser(jitUser, attributes, false);
                   //IDMSPingSamlJitHandler.updateAsync(jitUserJson); 
                }
            }
        } else{
            system.debug('UserId is blank'); 
        }
    }
    
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('jit handler update user');
        displayMap(attributes);
        updateUser(userId, federationIdentifier, attributes);
    }
    
    @future
    public Static void updateAsync(String jitUserJson){
        User jitUser = (User)JSON.deserialize(jitUserJson,User.class);
        system.debug('jitUser to update: '+ jitUser);
        update jitUser;
    }
    
    public void displayMap(Map<String,String> attributes){
        system.debug('MAP Attributes values: ');
        system.debug('country is ' + attributes.get(country));
        system.debug('division is ' + attributes.get(division));
        system.debug('phone is ' + attributes.get(phone));
        system.debug('email is ' + attributes.get(email));
        system.debug('FirstName is ' + attributes.get(FirstName));
        system.debug('LastName is ' + attributes.get(LastName));
        system.debug('City is ' + attributes.get(City));
        system.debug('FederatedId is ' + attributes.get(FederatedId));
    }
    
    public String mapPingIdmsUser(User jitUser, Map<String,String> attributes, boolean isCreation){
        jitUser.IDMSDivision__c     = attributes.get(division);
        jitUser.country             = attributes.get(country);
        jitUser.phone               = attributes.get(phone);
        jitUser.email               = attributes.get(email);
        jitUser.FirstName           = attributes.get(FirstName);
        jitUser.LastName            = attributes.get(LastName);
        jitUser.City                = attributes.get(City);
        if(isCreation){
            jitUser.username            = jitUser.email + Label.CLJUN16IDMS71;
            jitUser.profileId           = profileIdInternalUser;
            jitUser.Alias               = 'Alias';
            jitUser.IDMSIdentityType__c = 'Email';
            jitUser.IsIDMSUser__c       = true;
            jitUser.LocaleSidKey        = 'en_US';
            jitUser.EmailEncodingKey    ='UTF-8' ;
            jitUser.LanguagePreferred__c= 'English';
            jitUser.TimeZoneSidKey      = 'America/Los_Angeles';
            jitUser.LanguageLocaleKey   = 'en_US';   
        }
        return Json.serialize(jitUser);        
    }
    
}