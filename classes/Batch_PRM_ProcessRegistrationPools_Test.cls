// Test Class for Batch_PRM_ProcessRegistrationPools
@isTest
public class Batch_PRM_ProcessRegistrationPools_Test{
    @testSetup static void testDataSetupInserts(){
        List<CS_PRM_ApexJobSettings__c> CSJobTestList = new List<CS_PRM_ApexJobSettings__c>();
        CSJobTestList.add(new CS_PRM_ApexJobSettings__c(Name='REG_REPLENISH_CHNL_POOLS', InitialSync__c=true, NumberValue__c=10));
        CSJobTestList.add(new CS_PRM_ApexJobSettings__c(Name='REG_REPLENISH_CNTRY_POOLS', InitialSync__c=false, NumberValue__c=10));
        CSJobTestList.add(new CS_PRM_ApexJobSettings__c(Name='REG_REPLENISH_NOCNTRY_NOCHNL_POOLS', InitialSync__c=false, NumberValue__c=10));
        insert CSJobTestList;
        
        Country__c TestCountry = new Country__c();
            TestCountry.Name   = 'India';
            TestCountry.CountryCode__c = 'IN';
            TestCountry.InternationalPhoneCode__c = '91';
            TestCountry.Region__c = 'APAC';
            insert TestCountry;
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            insert testStateProvince;
        List<PRMCountry__c> lstPRMCntry = new List<PRMCountry__c>();
        
        PRMCountry__c newPRMCntry = new PRMCountry__c(Name='IND',Country__c = testCountry.Id, PLDatapool__c = 'test111',CountryPortalEnabled__c=true, SupportedLanguage1__c='EN');
        lstPRMCntry.add(newPRMCntry);
        
        Insert lstPRMCntry;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild; 
        
        CountryChannels__c cChannels = new CountryChannels__c();
        cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
        cChannels.Country__c = testCountry.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true; cChannels.PRMCountry__c = lstPRMCntry[0].Id;
        insert cChannels;
            
        FieloEE__Program__c prgm = new FieloEE__Program__c();
            prgm.Name = 'PRM Is On';
            prgm.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
            prgm.FieloEE__SiteURL__c = System.Label.CLAPR15PRM308;
            prgm.FieloEE__RecentRewardsDays__c = 10.0;
            insert prgm;
        
        CS_WebServiceEndPoint__c webserEndpointTest = new CS_WebServiceEndPoint__c(Name='PRM_FIELO_PRG_ID', Description__c='Id of the fielo program (PRM Is On)', ServiceEndpointURI__c=prgm.Id);
        insert webserEndpointTest;
    }
    
    @isTest
    static void ProcessRegistrationPoolsTest1(){
        Test.startTest();
            Batch_PRM_ProcessRegistrationPools bProcessPools = new Batch_PRM_ProcessRegistrationPools ();
            Database.executebatch(bProcessPools, 1);
            AP_PRMRegistrationPoolingService aPRMReg = new AP_PRMRegistrationPoolingService();
            aPRMReg.ReprocessUsersAndMembers();
            
            AP_PRMRegistrationPool ap_PRMReg = new AP_PRMRegistrationPool();
            FieloEE__Member__c member = new FieloEE__Member__c();
            ap_PRMReg.MemberInfo = member;
        Test.stopTest();
    }
    @isTest
    static void ProcessRegistrationPoolsTest2(){
        Test.startTest();
            CS_PRM_ApexJobSettings__c processCtryPools = CS_PRM_ApexJobSettings__c.getInstance('REG_REPLENISH_CNTRY_POOLS');
            processCtryPools.InitialSync__c = true;
            update processCtryPools;
            CS_PRM_ApexJobSettings__c processCtryPools1 = CS_PRM_ApexJobSettings__c.getInstance('REG_REPLENISH_CHNL_POOLS');
            processCtryPools1.InitialSync__c = false;
            update processCtryPools1;
            Batch_PRM_ProcessRegistrationPools bProcessPools = new Batch_PRM_ProcessRegistrationPools ();
            Database.executebatch(bProcessPools, 1);
            AP_PRMRegistrationPoolingService aPRMReg = new AP_PRMRegistrationPoolingService();
            aPRMReg.ReprocessUsersAndMembers();
            //AP_PRMRegistrationPoolingService.calculateActiveCountryAndChannelsPools(true,true,true);
        Test.stopTest();
    }
    @isTest
    static void ProcessRegistrationPoolsSchTest1(){
        Test.startTest();
            CS_PRM_ApexJobSettings__c cs = new CS_PRM_ApexJobSettings__c(Name='REG_POOLING_SCHEDULE', NumberValue__c=6);
            insert cs;
            Batch_PRM_ProcessRegistrationPools bProcessPools = new Batch_PRM_ProcessRegistrationPools ();
            DateTime currentDate = System.Now().addHours(5)  ;
            String CRON_EXPRESSION = '' + 59 + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' 
                              + currentDate.day() + ' ' + currentDate.month() + ' ? ' + currentDate.year();
    
            String jobId = System.schedule('Create Pooled Records', CRON_EXPRESSION, bProcessPools);
        Test.stopTest();
    }
    @isTest
    static void ProcessRegistrationPoolsSchTest2(){
        Test.startTest();
            
            Batch_PRM_ProcessRegistrationPools bProcessPools = new Batch_PRM_ProcessRegistrationPools ();
            DateTime currentDate = System.Now().addHours(5)  ;
            String CRON_EXPRESSION = '' + 59 + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' 
                              + currentDate.day() + ' ' + currentDate.month() + ' ? ' + currentDate.year();
    
            String jobId = System.schedule('Create Pooled Records', CRON_EXPRESSION, bProcessPools);
        Test.stopTest();
    }
    @isTest
    static void ProcessRegistrationPoolsTest222(){
        Test.startTest();
            AP_PRMAppConstants.ProcessRegistrationPool psPool = AP_PRMAppConstants.ProcessRegistrationPool.ASSIGN_POOLPERMSET;
            Batch_PRM_ProcessRegistrationPools bProcessPools = new Batch_PRM_ProcessRegistrationPools (psPool);
            Database.executebatch(bProcessPools, 1);
        Test.stopTest();
    }
}