/*
*   Created By: 
*   Created Date: 01/06/2016
*   Modified by:  01/12/2016 
*   Below class and methods are generated from WSDL
* */
public class WS_IdmsUimsUserManagerV2Impl {
    public class UserManager_UIMSV2_ImplPort {
        public String endpoint_x = System.Label.CLJUN16IDMS149;
        
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://uimsv2.impl.service.ims.schneider.com/', 'WS_IdmsUimsUserManagerV2Impl', 'http://uimsv2.service.ims.schneider.com/', 'WS_IdmsUimsUserManagerV2Service'};
            // Below Method to get access control for user in UIMS
            public WS_IdmsUimsUserManagerV2Service.accessTree getAccessControl(String callerFid,String samlAssertion) {
                WS_IdmsUimsUserManagerV2Service.getAccessControl request_x = new WS_IdmsUimsUserManagerV2Service.getAccessControl();
                request_x.callerFid = callerFid;
                request_x.samlAssertion = samlAssertion;
                WS_IdmsUimsUserManagerV2Service.getAccessControlResponse response_x;
                Map<String, WS_IdmsUimsUserManagerV2Service.getAccessControlResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.getAccessControlResponse>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        '',
                        'http://uimsv2.service.ims.schneider.com/',
                        'getAccessControl',
                        'http://uimsv2.service.ims.schneider.com/',
                        'getAccessControlResponse',
                        'WS_IdmsUimsUserManagerV2Service.getAccessControlResponse'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.return_x;
            }
        // Below Method to activate Identity for user in UIMS
        public Boolean activateIdentity(String callerFid,String password,String authentificationToken) {
            WS_IdmsUimsUserManagerV2Service.activateIdentity request_x = new WS_IdmsUimsUserManagerV2Service.activateIdentity();
            request_x.callerFid = callerFid;
            request_x.password = password;
            request_x.authentificationToken = authentificationToken;
            WS_IdmsUimsUserManagerV2Service.activateIdentityResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.activateIdentityResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.activateIdentityResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'activateIdentity',
                    'http://uimsv2.service.ims.schneider.com/',
                    'activateIdentityResponse',
                    'WS_IdmsUimsUserManagerV2Service.activateIdentityResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to request Email change for User Manager in UIMS
        public Boolean requestEmailChange(String callerFid,String samlAssertion,WS_IdmsUimsUserManagerV2Service.accessElement application,String newEmail) {
            WS_IdmsUimsUserManagerV2Service.requestEmailChange request_x = new WS_IdmsUimsUserManagerV2Service.requestEmailChange();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.application = application;
            request_x.newEmail = newEmail;
            WS_IdmsUimsUserManagerV2Service.requestEmailChangeResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.requestEmailChangeResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.requestEmailChangeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'requestEmailChange',
                    'http://uimsv2.service.ims.schneider.com/',
                    'requestEmailChangeResponse',
                    'WS_IdmsUimsUserManagerV2Service.requestEmailChangeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to get user details from UIMS
        public WS_IdmsUimsUserManagerV2Service.userV5 getUser(String callerFid,String samlAssertionOrToken) {
            WS_IdmsUimsUserManagerV2Service.getUser request_x = new WS_IdmsUimsUserManagerV2Service.getUser();
            request_x.callerFid = callerFid;
            request_x.samlAssertionOrToken = samlAssertionOrToken;
            WS_IdmsUimsUserManagerV2Service.getUserResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.getUserResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.getUserResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'getUser',
                    'http://uimsv2.service.ims.schneider.com/',
                    'getUserResponse',
                    'WS_IdmsUimsUserManagerV2Service.getUserResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to set user password in UIMS
        public Boolean setPassword(String callerFid,String authentificationToken,String password) {
            WS_IdmsUimsUserManagerV2Service.setPassword request_x = new WS_IdmsUimsUserManagerV2Service.setPassword();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            request_x.password = password;
            WS_IdmsUimsUserManagerV2Service.setPasswordResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.setPasswordResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.setPasswordResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'setPassword',
                    'http://uimsv2.service.ims.schneider.com/',
                    'setPasswordResponse',
                    'WS_IdmsUimsUserManagerV2Service.setPasswordResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to accept user merge in UIMS
        public Boolean acceptUserMerge(String callerFid,String authentificationToken) {
            WS_IdmsUimsUserManagerV2Service.acceptUserMerge request_x = new WS_IdmsUimsUserManagerV2Service.acceptUserMerge();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            WS_IdmsUimsUserManagerV2Service.acceptUserMergeResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.acceptUserMergeResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.acceptUserMergeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'acceptUserMerge',
                    'http://uimsv2.service.ims.schneider.com/',
                    'acceptUserMergeResponse',
                    'WS_IdmsUimsUserManagerV2Service.acceptUserMergeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to reject User Merge in UIMS
        public Boolean rejectUserMerge(String callerFid,String authentificationToken) {
            WS_IdmsUimsUserManagerV2Service.rejectUserMerge request_x = new WS_IdmsUimsUserManagerV2Service.rejectUserMerge();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            WS_IdmsUimsUserManagerV2Service.rejectUserMergeResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.rejectUserMergeResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.rejectUserMergeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'rejectUserMerge',
                    'http://uimsv2.service.ims.schneider.com/',
                    'rejectUserMergeResponse',
                    'WS_IdmsUimsUserManagerV2Service.rejectUserMergeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to update user in UIMS
        public Boolean updateUser(String callerFid,String samlAssertion,WS_IdmsUimsUserManagerV2Service.userV5 user_x) {
            //public Boolean updateUser(String FedId,WS_IdmsUimsUserManagerV2Service.userV5 user_x,String callerFid){
            WS_IdmsUimsUserManagerV2Service.updateUser request_x = new WS_IdmsUimsUserManagerV2Service.updateUser();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            //request_x.federatedId='fa8c8129-eef1-4059-9df4-fddfe86a9271';
            request_x.user_x = user_x;
            WS_IdmsUimsUserManagerV2Service.updateUserResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.updateUserResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.updateUserResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'updateUser',
                    'http://uimsv2.service.ims.schneider.com/',
                    'updateUserResponse',
                    'WS_IdmsUimsUserManagerV2Service.updateUserResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to Update user email in UIMS
        public Boolean updateEmail(String callerFid,String authentificationToken) {
            WS_IdmsUimsUserManagerV2Service.updateEmail request_x = new WS_IdmsUimsUserManagerV2Service.updateEmail();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            WS_IdmsUimsUserManagerV2Service.updateEmailResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.updateEmailResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.updateEmailResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'updateEmail',
                    'http://uimsv2.service.ims.schneider.com/',
                    'updateEmailResponse',
                    'WS_IdmsUimsUserManagerV2Service.updateEmailResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to update user password in UIMS
        public Boolean updatePassword(String callerFid,String samlAssertion,String oldPassword,String newPassword) {
            WS_IdmsUimsUserManagerV2Service.updatePassword request_x = new WS_IdmsUimsUserManagerV2Service.updatePassword();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.oldPassword = oldPassword;
            request_x.newPassword = newPassword;
            WS_IdmsUimsUserManagerV2Service.updatePasswordResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.updatePasswordResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.updatePasswordResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'updatePassword',
                    'http://uimsv2.service.ims.schneider.com/',
                    'updatePasswordResponse',
                    'WS_IdmsUimsUserManagerV2Service.updatePasswordResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        // Below Method to get programs of user from UIMS
        public WS_IdmsUimsUserManagerV2Service.program[] getPrograms(String callerFid,String samlAssertion) {
            WS_IdmsUimsUserManagerV2Service.getPrograms request_x = new WS_IdmsUimsUserManagerV2Service.getPrograms();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            WS_IdmsUimsUserManagerV2Service.getProgramsResponse response_x;
            Map<String, WS_IdmsUimsUserManagerV2Service.getProgramsResponse> response_map_x = new Map<String, WS_IdmsUimsUserManagerV2Service.getProgramsResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'getPrograms',
                    'http://uimsv2.service.ims.schneider.com/',
                    'getProgramsResponse',
                    'WS_IdmsUimsUserManagerV2Service.getProgramsResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}