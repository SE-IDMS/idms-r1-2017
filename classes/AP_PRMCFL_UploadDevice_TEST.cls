//Very quick & dirty way for unit testing - no time left to builf proper unit test cases
@isTest
private class AP_PRMCFL_UploadDevice_TEST {


	@isTest static void test_GetterSetter() {
		AP_PRMCFL_UploadDeviceController upload = new AP_PRMCFL_UploadDeviceController();
		
		//upload.inputValue = 'a';
		//System.assertEquals('a',AP_PRMCFL_UploadDeviceController.inputValue);
		
		upload.title = 'a';
		System.assertEquals('a',upload.title);
		
		upload.description = 'a';
		System.assertEquals('a',upload.description);

		upload.characteristic = 'a';
		System.assertEquals('a',upload.characteristic);

		upload.productPartNumber = 'a';
		System.assertEquals('a',upload.productPartNumber);


		upload.otherBrand = 'a';
		System.assertEquals('a',upload.otherBrand);

		upload.otherProduct = 'a';
		System.assertEquals('a',upload.otherProduct);

		upload.productBrandSelected = 'a';
		System.assertEquals('a',upload.productBrandSelected);
	
		upload.deviceTypeSelected = 'a';
		System.assertEquals('a',upload.deviceTypeSelected);

		upload.documentCategorySelected = 'a';
		System.assertEquals('a',upload.documentCategorySelected);

		upload.sharingSelected = 'a';
		System.assertEquals('a',upload.sharingSelected);

		upload.countrySelected = 'a';
		System.assertEquals('a',upload.countrySelected);

		upload.document = new Document ();
		System.assertNotEquals(null,upload.document);

		upload.fileContent = null;
		System.assertEquals(null,upload.fileContent);


	}

	@isTest static void test_getContentVersions() {
		AP_PRMCFL_UploadDeviceController upload = new AP_PRMCFL_UploadDeviceController();
		List<ContentVersion> cv = upload.getContentVersions();
		//System.assertEquals(cv.size()>0, '');

	}
	
	@isTest static void test_getBrandByCountry() {
		// Implement test code
		AP_PRMCFL_UploadDeviceController upload = new AP_PRMCFL_UploadDeviceController();
		 List<SelectOption> brands = upload.getBrandByCountry();
		System.assert(brands.size()>0, 'No brands defined in picklist');	
		Integer origSize = 	brands.size();
		brands.add(new SelectOption('Other','a'));
		System.assert(brands.size()==origSize+1, 'No brands defined in picklist');	
		
	}

	@isTest static void test_productBrandSelectList() {
		// Implement test code
		AP_PRMCFL_UploadDeviceController upload = new AP_PRMCFL_UploadDeviceController();
		 List<SelectOption> products = upload.productBrandSelectList();
		System.assert(products.size()>0, 'Cannot add value in picklist');			 
	}

   @isTest static void testRedirect() {
	test.startTest();    
   	AP_PRMCFL_UploadDeviceController c = new AP_PRMCFL_UploadDeviceController();
    PageReference pr = c.redirectPageLogin();
    System.assertEquals(null,pr);
    //System.assertEquals(new pageReference('/collaboration'),new pageReference('/collaboration'));
    
		 pr = new pageReference('/collaboration');
    	System.assertEquals(pr.getURL(),new pageReference('/collaboration').getURL());
    

    test.stopTest();      
   }


   @isTest static void testTPicklistEntry() {
	test.startTest();    
   	AP_PRMCFL_UploadDeviceController.TPicklistEntry tple = new AP_PRMCFL_UploadDeviceController.TPicklistEntry();
   	tple.active = 'a';
    System.assertEquals('a',tple.active);
    tple.defaultValue = 'a';
    System.assertEquals('a',tple.defaultValue);

    test.stopTest();      
   }


   @isTest static void test_uploadContents() {
	test.startTest();    
   	AP_PRMCFL_UploadDeviceController upload = new AP_PRMCFL_UploadDeviceController();
   	PageReference ref = upload.uploadContents() ;
    System.assertEquals(null,ref);
	System.assertEquals(true, ApexPages.hasMessages(ApexPages.severity.FATAL), 'Should have at some FATAL messages');
	//AP_PRMCFL_UploadDeviceController.title='test';
	ref = upload.uploadContents() ;

    test.stopTest();      
   }
	
}