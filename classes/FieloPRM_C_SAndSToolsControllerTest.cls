@Istest
public with sharing class FieloPRM_C_SAndSToolsControllerTest{

    static testMethod void testPLComponentController(){
         User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;

        System.runAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
            insert c;
            
            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLDataPool__c ='en_US2'
            );
            insert acc;
                    
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test',
                F_Country__c = c.Id
            );
            insert mem;
        
            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
            insert memb;
            
            FieloEE.MemberUtil.setmemberId(mem.Id);
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'test'
            );
            insert menu;

            Fielo_CRUDObjectSettings__c testCrud = new Fielo_CRUDObjectSettings__c(
                F_ObjectAPIName__c = 'Account'
            );
            insert testCrud;
        
            FieloEE__Component__c component = new FieloEE__Component__c(
                FieloEE__Menu__c = Menu.Id,
                F_CRUDObjectSettings__c = testCrud.Id,
                F_PRM_DocType__c='notNull'
            );
            insert component;
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.Name='myTagTest';
            insert tag;
            
            FieloEE__Category__c cat = new FieloEE__Category__c();
            cat.name = 'test';
            cat.F_PRM_CategoryFilter__c = 'cat' + datetime.now();
            insert cat;
                             
            RecordType rt = [select id,Name from RecordType where SobjectType='FieloEE__News__c' and DeveloperName='FieloPRM_Featured_Document' Limit 1];
            
            FieloEE__News__c   n01 = new FieloEE__News__c(recordTypeId=rt.Id);  
            //n01.setName('myNewsTest');
            //n01.RecordType.Id=rt.Id;
            //n01.FieloEE__Body__c = 'body test';
            //n01.FieloEE__ExtractText__c = 'test extract text';
            //n01.FieloEE__Order__c=100;
            //n01.FieloEE__IsActive__c = true;
            //n01.FieloEE__HasSegments__c = false;
            //n01.IsLocked = false;
            //n01.FieloEE__Component__c = component.id;
            //n01.FieloEE__CategoryItem__c = cat.Id;
            insert n01;
            
            FieloEE__TagItem__c n01_ti = new FieloEE__TagItem__c();
            n01_ti.FieloEE__News__c=n01.Id;
            n01_ti.FieloEE__Tag__c=tag.Id;    
            insert n01_ti;
            
            
            
            PRMCountry__c country = new PRMCountry__c();
            country.PLDatapool__c = 'asdd';
            insert country;
            PRM_SST_Conf__c conf = new PRM_SST_Conf__c();
            conf.PRMCountry__c = country.Id;
            conf.brand__c = 'Schneider Electric';
            conf.channel__c ='Contractors';
            conf.country__c = 'US';
            conf.DocTypeGroupBslOids__c = '3541958';
            conf.FieloCategory__c=cat.Id;
            conf.Use_Mockup__c = true;
            insert conf;
            
             Test.startTest();


            FieloPRM_VFC_SAndSToolsController myController = new FieloPRM_VFC_SAndSToolsController();
            myController.testMode=false;
            myController.myComponentId = component.Id;
            myController.myTagId = tag.Id;
            myController.myCategoryId = cat.Id;
            System.currentPageReference().getParameters().put('idCategory', cat.Id);
            System.currentPageReference().getParameters().put('docRef', 'dummy');
            System.currentPageReference().getParameters().put('docTypeOid', '000001');
            System.currentPageReference().getParameters().put('docTypeGroupOid', '000001');
            System.currentPageReference().getParameters().put('type', 'document');
            System.currentPageReference().getParameters().put('firstResult', '0');
            System.currentPageReference().getParameters().put('maxResult', '10');
            
            Test.setMock(HttpCalloutMock.class, new FieloPRM_C_SST_MockHttpResponseGenerator());

            
            myController.getScope();
            FieloPRM_VFC_SAndSToolsController.getLocale();
            
            FieloPRM_VFC_SAndSToolsController.getMemberInfo();
            //myController.getComponentDetails();
            FieloPRM_VFC_SAndSToolsController.getFederationIdentifier();
            
            FieloPRM_VFC_SAndSToolsController.getListUrl(); //M:type
            myController.getDocDetailUrl();
            myController.getShowRecentlyAdded();
            myController.getFeaturedToolList();
            myController.getComponentDetails();
            /*FieloPRM_VFC_SAndSToolsController.getDownloadUrl();
            FieloPRM_VFC_SAndSToolsController.getDownloadUrlByDocOid();
            FieloPRM_VFC_SAndSToolsController.getDownloadUrlByDocRef();
            FieloPRM_VFC_SAndSToolsController.getDownloadThumbnailUrlByDocRef();*/

            FieloPRM_VFC_SAndSToolsController.getPage();
            FieloPRM_VFC_SAndSToolsController.getInnerPage();
            FieloPRM_VFC_SAndSToolsController.getPageNumber(); //M:firstResult M:maxResult
            
            FieloPRM_VFC_SAndSToolsController.getFirstResultPrev(); //M:firstResult M:maxResult
            FieloPRM_VFC_SAndSToolsController.getFirstResultNext(); //M:firstResult M:maxResult
            
            FieloPRM_VFC_SAndSToolsController.getMaxResult();//;M:maxResult

            FieloPRM_VFC_SAndSToolsController.getDownloadUrlByDocRef();
            FieloPRM_VFC_SAndSToolsController.getDownloadThumbnailUrlByDocRef();


            
            System.debug(Logginglevel.INFO,'getRecentlyAddedDocList:' + myController.getRecentlyAddedDocList().error); 
            
            myController.getAdditionalInfo();
            myController.getFeaturedDocList();
            myController.getDocTypeGroupCount();
            myController.getDocTypeCount();
            myController.getRangeCount();
            FieloPRM_VFC_SAndSToolsController.fromObjectToStringResizedForDisplay('ddb.description',10,'...');
            FieloPRM_VFC_SAndSToolsController.fromObjectToStringResizedForDisplay('ddb.description',200,'...');
            BslUtils.DocumentDetailsBean details = myController.getDocDetails();
            System.debug(Logginglevel.INFO,'getDocDetails:' + details.error +
                         details.creationDate+' '+
                         details.description+' '+
                         details.detailsUrl+' '+
                         details.docId+' '+
                         details.documentDate+' '+
                         details.docTypeName+' '+
                         details.downloadUrl+' '+
                         details.fileName+' '+
                         details.fileSize+' '+
                         details.fileExtension+' '+
                         details.keywords+' '+
                         details.lastModificationDate+' '+
                         details.reference+' '+
                         details.title+' '+
                         details.iconFileName
                         
                        );
            
            
            
            myController.getDocList();
            
            
            BslUtils.getDownloadUrl('');
            BslUtils.getDownloadUrlByDocOid('');
            BslUtils.getDownloadUrlByDocRef('');
            BslUtils.getDownloadUrlByDocRefForMultipleFiles('','');
            //BslUtils.getDownloadThumbnailUrlByDocRef();
            BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentList');
            BslUtils.callBsl(true,'dummy','BslApi_DocumentServiceV002_getDocumentList',new Map<String,String>());
            BslUtils.mockCall('dummy','BslApi_DocumentServiceV002_getDocumentList',new Map<String,String>());
            BslUtils.mockCall('dummy','BslApi_DocumentServiceV002_getDocDetails',new Map<String,String>());
            BslUtils.mockCall('dummy','BslApi_DocumentServiceV002_getDocumentCount',new Map<String,String>());

            BslUtils.parseSoapResponse(BslUtils.mockCall('dummy','BslApi_DocumentServiceV002_getDocumentList',new Map<String,String>()),new List<String>{'title','reference','docId','detailsUrl','documentDate','documentType@translation'});
            BslUtils.getImageFileName('txt');
            BslUtils.findBslScope(cat.Id);
            BslUtils.getDocTypeGroupBslOids(cat.Id);
            BslUtils.getRecentlyAddedDocTypeGroupBslOids(cat.Id);
            BslUtils.toDocTypeGroupBslOidXmlListString('1234,2354,1234');
            
            BslUtils.getDownloadThumbnailUrlByDocRef(System.Label.BslApi_PublicDownloadAudience);
            BslUtils.getImageFileName('bin');
            BslUtils.getImageFileName('doc');
            BslUtils.getImageFileName('dwf');
            BslUtils.getImageFileName('dwg');
            BslUtils.getImageFileName('dxf');
            BslUtils.getImageFileName('exe');
            BslUtils.getImageFileName('gif');
            BslUtils.getImageFileName('htm');
            BslUtils.getImageFileName('html');
            BslUtils.getImageFileName('img');
            BslUtils.getImageFileName('ipg');
            BslUtils.getImageFileName('pdf');
            BslUtils.getImageFileName('png');
            BslUtils.getImageFileName('ppt');
            BslUtils.getImageFileName('rar');
            BslUtils.getImageFileName('stp');
            BslUtils.getImageFileName('pdf');
            BslUtils.getImageFileName('stp');
            BslUtils.getImageFileName('xls');
            BslUtils.getImageFileName('xml');
            BslUtils.getImageFileName('zip');
            Test.stopTest();
       }
        
     }

}