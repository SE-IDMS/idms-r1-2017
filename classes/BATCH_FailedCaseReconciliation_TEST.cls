@isTest
private class BATCH_FailedCaseReconciliation_TEST {
    static TestMethod void test_BATCH_FailedCaseReconciliation(){
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name = 'TestCCCCountry'; 
        objCountry.CountryCode__c='CCC';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        FailedCase__c objFailedCase = Utils_TestMethods.createFailedCase(objContact.id);
        insert objFailedCase;
        
        FailedEmailMessage__c objFailedEmailMessage = Utils_TestMethods.createFailedEmailMessage(objFailedCase.id);
        insert objFailedEmailMessage;

        
        Attachment objAttachment = Utils_TestMethods.createAttachment(objFailedCase.id);
        insert objAttachment;

        BATCH_FailedCaseReconciliation objFailedCaseReconciliation = new BATCH_FailedCaseReconciliation(1000);
        database.executeBatch(objFailedCaseReconciliation);
    }
}