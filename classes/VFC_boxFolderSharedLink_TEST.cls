/**
TEST CLASS Q1 release
**/


@istest
public class VFC_boxFolderSharedLink_TEST{
    static testmethod void boxFolderLinkOffer(){
        Test.startTest();
        ApexPages.StandardController controller1;
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();
        User newUser = Utils_TestMethods.createStandardUser('hdkadkny');         
         Database.SaveResult UserInsertResult = Database.insert(newUser, false);   
        
       // system.runas(userList[0]){
            Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
            insert offerRec;
            PageReference pg = Page.VFP_BoxSharedLink;
            Test.setCurrentPage(pg);
            pg.getParameters().put('Id', offerRec.Id);
            VFC_boxFolderSharedLink controller = new VFC_boxFolderSharedLink(controller1);
            controller.offerBoxSharelink();
            //controller.projectBoxSharelink();
       // }
        Test.stopTest();
    }
    
    static testmethod void boxFolderLinkProject(){
        Test.startTest();
        ApexPages.StandardController controller1;
       // list<User> userList = AP_EllaProjectForecast_TEST.createUsers();
       User newUser = Utils_TestMethods.createStandardUser('ltrasjkla');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);  
        //system.runas(userList[0]){
            Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
            insert offerRec;
            country__c c = new country__c(name = 'India',countrycode__c = 'IN');
            insert c;
            Milestone1_Project__c projRec = AP_EllaProjectForecast_TEST.createProject(offerRec.Id, c.Id, newUser.Id);
            insert projRec;
            
            PageReference pg = Page.VFP_BoxSharedLink;
            Test.setCurrentPage(pg);
            pg.getParameters().put('Id', projRec.Id);
            VFC_boxFolderSharedLink controller = new VFC_boxFolderSharedLink(controller1);
            //controller.offerBoxSharelink();
            controller.projectBoxSharelink();
       // }
        Test.stopTest();
    }
}