/*
    Author          : Accenture Team
    Date Created    : 14/05/2012
    Description     : Test class for AP50_RevenueLineIntegrationMethods
*/
@isTest

private class AP50_RevenueLineIntegrationMethods_TEST 
{
    static testMethod void testAP50_RevenueLineIntegrationMethods() 
    {
    Account Acc = Utils_TestMethods.createAccount();
       
    insert Acc;

    DataTemplate__c GMRProduct = new DataTemplate__c();
    GMRProduct.Field1__c='Test1';
    GMRProduct.Field2__c='Test2';
    GMRProduct.Field3__c='Test3';
    GMRProduct.Field4__c='Test4';
    GMRProduct.Field5__c='Test5';
    GMRProduct.Field6__c='Test6';
    GMRProduct.Field7__c='Test7';
    GMRProduct.Field8__c='Test8';
    GMRProduct.Field9__c='Test9';
        
    List<DataTemplate__c> GMRProducts = new List<DataTemplate__c>();
        
    Opportunity Opp = Utils_TestMethods.createOpenOpportunity(Acc.Id); 
    Opp.CurrencyIsoCode = 'EUR';
    insert Opp;
    Revenue_Line__c RevenueLine = Utils_TestMethods.createRevenueLine(Opp.Id); 
    insert RevenueLine;
    
    Map<Revenue_Line__c,DataTemplate__c> ProductMap = new Map<Revenue_Line__c,DataTemplate__c>();
              
    ProductMap.put(RevenueLine,GMRProduct);
       
    AP50_RevenueLineIntegrationMethods.UpdateRL(ProductMap);
       
    Map<Opportunity,DataTemplate__c> ProductMap2 = new Map<Opportunity,DataTemplate__c>();
    ProductMap2.put(Opp,GMRProduct );
       
    AP50_RevenueLineIntegrationMethods.InsertRevenueLine(ProductMap2);
    }
}