/*
Created By: Renjith Jose
Release: BR-6098, Automatic assignment of program type = Channel Feature

Batch_AssignAccountChannelFeature batch_ppsa = new Batch_AssignAccountChannelFeature(); 
batch_ppsa.query = 'Select id,ClassLevel1__c,ClassLevel2__c,Country__c, LastModifiedDate from Account Where PRMAccount__c = true and LastModifiedDate =Last_N_Days:1';
Database.executebatch(batch_ppsa);

*/
global class Batch_AssignAccountChannelFeature implements Database.Batchable<sObject>,Schedulable  {
    
    public String query;
    public static String CRON_EXP = '0 0 22 * * ? *'; // CRON JOB expression to run the batch job at 10PM everyday
    
    global Batch_AssignAccountChannelFeature() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        Map<String, CS_PRM_ApexJobSettings__c> csApexJobSeting = CS_PRM_ApexJobSettings__c.getAll(); 
        String strLastRunDate='';
        
        if(csApexJobSeting.containsKey('AssignChannelFeatureByAccount'))
        {
            if(!csApexJobSeting.get('AssignChannelFeatureByAccount').InitialSync__c) //if the initial sync is not selected
            {
                if(String.valueOf(csApexJobSeting.get('AssignChannelFeatureByAccount').LastRun__c) != null)
                {
                    DateTime dt = csApexJobSeting.get('AssignChannelFeatureByAccount').LastRun__c;
                    strLastRunDate = ' AND LastModifiedDate > ' +dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                }
            }                    
        }
    
        if(query== null)
            query = 'Select id,ClassLevel1__c,ClassLevel2__c,Country__c, LastModifiedDate from Account Where IsPartner = true'+strLastRunDate ; 
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        
        if(scope != null && scope.size() >0)    
            AP_AAP_AccountProgramUpdate.CreateAccountAssignedProgram(scope,null);
       
    } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
        
   /* global static String ScheduleIt (String batchCronExpression) {
        Batch_AssignAccountChannelFeature bpcf = new Batch_AssignAccountChannelFeature();
        return System.schedule('Create Account Channel Feature Program', batchCronExpression, bpcf);
    } */
    

    //Schedule method
    global void execute(SchedulableContext sc) 
    {
        Batch_AssignAccountChannelFeature batch_ppsa = new Batch_AssignAccountChannelFeature(); 
        Database.executebatch(batch_ppsa);
    }


}