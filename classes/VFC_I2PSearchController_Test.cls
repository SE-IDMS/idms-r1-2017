/*
Author          : Global Delivery Team - Bangalore
Date Created    : 19/11/2014
Description     : Test class for VFC_I2PSearchController class.
*/
@isTest (SeeAllData=true)
private class VFC_I2PSearchController_Test {
    //public static User u;
    static testMethod void searchproblems() { 
    Id idSupplierAcctRecType1=   [SELECT Id FROM RecordType WHERE 
                                 SObjectType = 'Account' AND Name = 'Supplier'].id;
       
            Account supplier1 = new Account();
            supplier1.RecordTypeId = idSupplierAcctRecType1;
            supplier1.Name = 'Test Supplier12'; 
            supplier1.ProblemtoPreventionDeployed__c=false;
            insert supplier1;
            
    Problem__c prob1 = new Problem__c();
                prob1.Title__c = 'TestTitle12';   
                prob1.Sensitivity__c = 'Confidential';
                prob1.WhatIsTheProblem__c='Test Problem1';
                prob1.WhenWasTheProblemDetected__c='Test Problem Detected';
                prob1.WhereWasTheProblemDetected__c ='Test';
                prob1.WhoDetectedTheProblem__c ='Test';
                prob1.WhyIsItaProblem__c = 'Test';    
                prob1.HowWasTheProblemDetected__c ='Test Scenario';
                prob1.Severity__c ='Minor';
                prob1.StatusOfRootCauseAnalysis__c='Not Started';
                prob1.StatusOfCorrection__c='Not Started';     
                prob1.PartnerAccount__c = supplier1.id;
                prob1.OwnerId=userInfo.getUserId();
                prob1.ProblemLeaderEmail__c = 'test.schneider@schneider.com';
                prob1.RecordTypeId = System.Label.CLI2PAPR120014;
                prob1.ProblemLeaderEmail__c ='test1.schneider@schneider.com';
                prob1.ProblemLeaderSupplier__c = 'test';
                prob1.Status__c ='In Progress';
                prob1.TECH_Partner_Account__c = supplier1.id;
                insert prob1; 
                test.startTest();
                VFC_I2PSearchController vfcsearch = new VFC_I2PSearchController();
                vfcsearch.searchText = 'Test';
                vfcsearch.searchproblems();
                vfcsearch.searchText = '';
                vfcsearch.searchproblems();
                vfcsearch.username ='test@test1.com';
                vfcsearch.password = '#shjjks';
                System.assertEquals(null, vfcsearch.doLogin());
                vfcsearch.forgotPasswordI2PCommunity();
                test.stopTest();    
    }
}