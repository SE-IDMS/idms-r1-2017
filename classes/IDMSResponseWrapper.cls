/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This is global wrapper class which holds response message and status code for Resend PIN Rest API.
**/ 
global with sharing class IDMSResponseWrapper{
    public String Message;  
    public String Status; 
    public User IDMSUserRecord;        
    
    public IDMSResponseWrapper(){}
    
    public IDMSResponseWrapper(User IDMSUserRecord,string Status,string Message){
        this.IDMSUserRecord     = IDMSUserRecord;
        this.Status             = Status;
        this.Message            = Message;
    }    
}