global class ServiceMaxBatchLocationValidation implements Database.Batchable<sObject>,Database.AllowsCallouts {
  
    public static String GOOGLE_API_URL = Label.CLAPR14SRV18;
    public static String GOOGLE_API_KEY = Label.CLAPR14SRV17;
    public String query = 'SELECT Id, Name, SVMXC__City__c, SVMXC__Street__c, SVMXC__State__c, SVMXC__Country__c, LocationCountry__c, LocationCountry__r.Name, SVMXC__Zip__c, SVMXC__Longitude__c, SVMXC__Latitude__c FROM SVMXC__Site__c WHERE Valid__c = false limit '+Label.CLAPR14SRV19;
    
    global Database.Querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        for ( SVMXC__Site__c site: (List<SVMXC__Site__c>) scope) { 
            System.debug('al:'+site.id);
            site.SVMXC__Latitude__c = null;
            site.SVMXC__Longitude__c = null;
            List<String> addr = new List<String>();
            if(site.SVMXC__Street__c != null && site.SVMXC__Street__c != '')
            {
                addr.add(site.SVMXC__Street__c);
            }
            
            if(site.SVMXC__City__c != null && site.SVMXC__City__c != '')
            {
                addr.add(site.SVMXC__City__c);
            }
            
            if(site.SVMXC__State__c != null && site.SVMXC__State__c != '')
            {
                addr.add(site.SVMXC__State__c);
            }
            
            if (site.LocationCountry__c != null)
            {
              addr.add(site.LocationCountry__r.Name);
            }
            else if(site.SVMXC__Country__c != null && site.SVMXC__Country__c != '')
            {
                addr.add(site.SVMXC__Country__c);
            }
             
            if ( site.SVMXC__Zip__c != null && site.SVMXC__Zip__c != '')
            {
                    addr.add(site.SVMXC__Zip__c);  
            }
            
            if (addr.size() > 0)
            {
                processGeocodeDom ( String.join(addr, ','), site);
            }
            else
            {
              site.ValidationError__c = 'No Address Info';
              site.Valid__c = false;
            }
        }
                   
        update scope;
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
            
    }
    
    public void processGeocodeDom( string addr, SVMXC__Site__c site) { 
         
    
        HttpRequest req = new HttpRequest();           
        string url = GOOGLE_API_URL+'?sensor=true&address=' + EncodingUtil.urlEncode(addr,'UTF-8')+'&key='+Label.CLAPR14SRV17;
        req.setEndpoint( url );
        req.setMethod('GET');
        system.debug(addr);
        
        
        try {
            Http http = new Http();
            HttpResponse response = null;
            
            if (Test.isRunningTest())
            {
                response = new HttpResponse();
                response.setStatus('200');
                response.setBody('{"results":[{"address_components":[{"long_name":"1600","short_name":"1600","types":["street_number"]},{"long_name":"AmphitheatrePkwy","short_name":"AmphitheatrePkwy","types":["route"]},{"long_name":"MountainView","short_name":"MountainView","types":["locality","political"]},{"long_name":"SantaClara","short_name":"SantaClara","types":["administrative_area_level_2","political"]},{"long_name":"California","short_name":"CA","types":["administrative_area_level_1","political"]},{"long_name":"UnitedStates","short_name":"US","types":["country","political"]},{"long_name":"94043","short_name":"94043","types":["postal_code"]}],"formatted_address":"1600AmphitheatrePkwy,MountainView,CA94043,USA","geometry":{"location":{"lat":37.42291810,"lng":-122.08542120},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":37.42426708029149,"lng":-122.0840722197085},"southwest":{"lat":37.42156911970850,"lng":-122.0867701802915}}},"types":["street_address"]}],"status":"OK"}');  
                response.setStatusCode(200);
            }
            else
            {
                response = http.send(req);
            }
            
            if (response.getStatusCode() != 200 ) { 
                site.ValidationError__c = dumpResponse ( response);
            } else {
                String code = '';
                JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                /*if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'code')) 
                {
                    parser.nextToken();
                    code = parser.getText();
                }*/
                /*if (code == '200' && (parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'coordinates'))
                {
                    parser.nextToken();
                    parser.nextToken();
                    site.SVMXC__Longitude__c = parser.getDecimalValue();
                    parser.nextToken();
                    site.SVMXC__Latitude__c = parser.getDecimalValue();
                }*/
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'lat')) 
                {
                    parser.nextToken();
                    site.SVMXC__Latitude__c = parser.getDecimalValue();
                }
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'lng')) 
                {
                    parser.nextToken();
                    site.SVMXC__Longitude__c = parser.getDecimalValue();
                }
            }
            } 
        } catch( System.Exception e) {
            site.ValidationError__c = 'ERROR: ' + e;
        } 
        
        if (site.SVMXC__Longitude__c != null && site.SVMXC__Latitude__c != null)
        {
          site.Valid__c = true;
          site.ValidationError__c = null;
        } 
        
        system.debug('YTI - '+site.ValidationError__c);
    }
    
    private static String dumpResponse(HttpResponse response) { 
      List<String> error = new List<String>();
        error.add('GEOCODE ERROR: Could not parse or locate address'); 
        error.add('STATUS:'+response.getStatus());
        error.add('STATUS_CODE:'+response.getStatusCode());
        error.add('BODY: '+response.getBody());
        
        return String.join(error, '\n');
    }
    
    @isTest
    public static void test()
    { 
      Account acc = Utils_TestMethods.createAccount();
      insert acc;
      Country__c c = Utils_TestMethods.createCountry(); 
      insert c;
      
      SVMXC__Site__c site = new SVMXC__Site__c(
         Name = 'TestBatchName',
         SVMXC__City__c = 'Junction City',
         SVMXC__State__c = 'Kansas',
         SVMXC__Street__c = '704 S. Adams',
         SVMXC__Zip__c = '66441',
         LocationCountry__c = c.Id,
         SVMXC__Location_Type__c='Building',
         SVMXC__Account__c=acc.Id,
         TimeZone__c='India');
         
      
      insert site;
      
      Test.startTest();
      ServiceMaxBatchLocationValidation batch = new ServiceMaxBatchLocationValidation();
      batch.query = 'SELECT Id, Name, SVMXC__City__c, SVMXC__Street__c, SVMXC__State__c, SVMXC__Country__c, LocationCountry__c, LocationCountry__r.Name, SVMXC__Zip__c, SVMXC__Longitude__c, SVMXC__Latitude__c FROM SVMXC__Site__c WHERE ID = \'' + site.Id + '\'';
      Database.executeBatch(batch);
      Test.stopTest();
      
      site = [SELECT Valid__c, SVMXC__Longitude__c, SVMXC__Latitude__c FROM SVMXC__Site__c WHERE Id = :site.Id LIMIT 1];
      system.assert(site.Valid__c);
      system.assert(site.SVMXC__Longitude__c != null);
        system.assert(site.SVMXC__Latitude__c != null);
    }
      
}