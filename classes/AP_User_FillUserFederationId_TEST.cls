/**
 * This class contains unit tests for validating the behavior of Apex trigger 'UserBeforeInsertBeforeUpdate '
 *
 * ~ Created by Alexandre Gonzalo
 * ~ SCHNEIDER JUNE'12 RELEASE - June 18th 2012
 * ~ Modified by Alexandre Gonzalo
 * ~ SCHNEIDER OCTOBER'12 RELEASE - June 18th 2012
 * 
**/


@isTest
private class AP_User_FillUserFederationId_TEST
{

    static testMethod void testFillUserFederationId()
    {
        Test.startTest();
    
        User u = [select Id, Username, FederationIdentifier from User where Id = : UserInfo.getUserId()];
        u.FederationIdentifier = 'SESAXXX';
        update u;
        
        u = [select Id, Username, FederationIdentifier from User where Id = : UserInfo.getUserId()];
        System.assertEquals('SESAXXX', u.FederationIdentifier);
        
        u.Username = 'sesa1234XX@bridge-fo.com.devSSO';
        u.FederationIdentifier = null;
        update u;
        u = [select Id, Username, FederationIdentifier from User where Id = : UserInfo.getUserId()];
        System.assertEquals('SESA1234XX', u.FederationIdentifier);
        
        u.FederationIdentifier = 'sesaXXX';
        update u;
        u = [select Id, Username, FederationIdentifier from User where Id = : UserInfo.getUserId()];
        System.assertEquals('SESAXXX', u.FederationIdentifier);
        
        Test.stopTest();
    }
    
}