/***
Test clss for VFC_COINbFO_BCABYCountryChart;
***/

    @isTest
    public class VFC_COINbFO_BCABYCountryChart_TEST {
    
        public static testMethod void myUnitTest() {
            Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
            inOffLif.Name='Test Schneider';
           // inOffLif.Leading_Business_BU__c ='IT';
           // inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Kilo Euro';
           // inOffLif.Launch_Master_Assets_Status__c='Red';
            //inOffLif.Stage__c ='0 - Define';
            
            //Insert inOffLif; 
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            mileProject.RecordTypeId=MPRecordTypeId;
            mileProject.Name ='By Country Project ';
           // mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.Country__c=cotryObj.id;
            mileProject.Country_Announcement_Date__c=Date.today();
            
            insert mileProject ;
            
            List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_YEAR.size(); i++ ) {
                offForecast = new OfferLifeCycleForecast__c();
                offForecast.project__c=mileProject.id;
                offForecast.month__c=monthName(i);
                offForecast.ForcastYear__c='2014';
                offForecast.Actual__c =1000;
                offForecast.BU__c =2000;
                offForecast.Country2__c='France';
                offForecast.Country__c=4000;
                if(i <=3) {
                offForecast.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast.add(offForecast);
            
            
            }
            insert  ListoffForecast;
            ApexPages.currentPage().getParameters().put('Id', inOffLif.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(inOffLif);       
            VFC_COINbFO_BCABYCountryChart  MPForecast = new VFC_COINbFO_BCABYCountryChart(controller);
            
        
        
        }
    private static final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December','Total by Year'};


    private static String monthName(Integer monthNum) {

        return(MONTHS_OF_YEAR[monthNum - 1]);

    }
    
    
    
    }