/*
------------------------------
   Author : A.D.N.V.SATYANARAYANA
   Description : Test class for VFC_PRMProgAssignment
   Created Date : 20-06-2016
------------------------------   */
@isTest
public class VFC_PRMProgAssignment_Test{
@testSetup static void inserts(){
        CS_PRM_ApexJobSettings__c jobSet= new CS_PRM_ApexJobSettings__c(); 
        jobSet.name='PAGESIZE_INTERNALUSER';
        jobSet.QueryFields__c='5';
        insert jobSet;
       
       CS_PRM_ApexJobSettings__c jobSet1= new CS_PRM_ApexJobSettings__c(); 
       jobSet1.name='PRMAccountFieldList1';
       jobSet1.InitialSync__c=True;
       jobSet1.QueryFields__c='Id,PRMCompanyName__c,PRMAccount__c,PRMCompanyPhone__c,PRMCorporateHeadquarters__c,PRMStreet__c,PRMIncludeCompanyInfoInSearchResults__c,PRMAdditionalAddress__c,PRMBusinessType__c,PRMCity__c,PRMAreaOfFocus__c,PRMCountry__c,PRMMarketServed__c,';
       insert jobSet1;
       CS_PRM_ApexJobSettings__c jobSet2= new CS_PRM_ApexJobSettings__c(); 
       jobSet2.name ='PRMAccountFieldList2';
       jobSet2.InitialSync__c=True;
       //jobSet2.QueryFields__c='PRMStateProvince__c,PRMTaxId__c,PRMZipCode__c,PRMEmployeeSize__c,PRMSEAccountNumber__c,PRMCurrencyIsoCode__c,PRMPreferredDistributor1__c,PRMAccountRegistrationStatus__c,PRMPreferredDistributor2__c,PRMReasonForDecline__c,PRMPreferredDistributor3__c,';
       jobSet2.QueryFields__c='PRMStateProvince__c,PRMTaxId__c,PRMZipCode__c,PRMEmployeeSize__c,PRMSEAccountNumber2__c,PRMCurrencyIsoCode__c,PRMPreferredDistributor1__c,PRMAccountRegistrationStatus__c,PRMPreferredDistributor2__c,PRMReasonForDecline__c,PRMPreferredDistributor3__c,';
       insert jobSet2;
       CS_PRM_ApexJobSettings__c jobSet3= new CS_PRM_ApexJobSettings__c(); 
       jobSet3.name='PRMAccountFieldList3';
       jobSet3.InitialSync__c=True;
       jobSet3.QueryFields__c='PRMAdditionalComments__c,PRMPreferredDistributor4__c,PRMRegistrationActivationDate__c,PRMUIMSID__c,PRMOrigin__c,PRMUIMSSEAccountId__c,PRMTermsAndConditionsAccepted__c,PRMIsActiveAccount__c,PRMIsEnrolledAccount__c,';
       insert jobSet3;
       CS_PRM_ApexJobSettings__c jobSet4= new CS_PRM_ApexJobSettings__c(); 
       jobSet4.name='PRMAccountFieldList4';
       jobSet4.InitialSync__c=True;
       jobSet4.QueryFields__c='PRMExcludeFromReports__c,PRMAnnualSales__c,PRMRegistrationReviewedRejectedDate__c,PLCompanyName__c,PLMainContactFirstName__c,PLMainContactLastName__c,PLMainContactPhone__c,PLMainContactEmail__c,PLWebsite__c,PLCompanyDescription__c,PLStreet__c,';
       insert jobSet4;
       
       CS_PRM_ApexJobSettings__c jobSet5= new CS_PRM_ApexJobSettings__c(); 
       jobSet5.name='PRMAccountFieldList5';
       jobSet5.InitialSync__c=True;
       jobSet5.QueryFields__c='PLAdditionalAddress__c,PLStateProvince__c,PLCity__c,PLCountry__c,PLZipCode__c,PLShowInPartnerLocator__c,PLBusinessType__c,PLAreaOfFocus__c,PLMarketServed__c,PLPartnerLocatorFeatureGranted__c,PLShowInPartnerLocatorForced__c,PRMDomainsOfExpertise__c,';
       insert jobSet5;
       
       CS_PRM_ApexJobSettings__c jobSet6= new CS_PRM_ApexJobSettings__c(); 
       jobSet6.name='PRMAccountFieldList6';
       jobSet6.InitialSync__c=True;
       jobSet6.QueryFields__c='IsPersonAccount,PRMOriginSource__c,PRMOriginSourceInfo__c,PRMAccountCompletedData__c,PLDomainsOfExpertise__c';
       insert jobSet6;
       
       CS_PRM_ApexJobSettings__c jobSet7= new CS_PRM_ApexJobSettings__c(); 
       jobSet7.name='PRMAccountFieldList7';
       jobSet7.InitialSync__c=True;
       jobSet7.QueryFields__c=',AdditionalAddress__c,AnnualSales__c,ClassLevel2__c,ClassLevel1__c,City__c,Name,Phone,CorporateHeadquarters__c,Country__c,CurrencyIsoCode,EmployeeSize__c,MarketServed__c,SEAccountID__c,StateProvince__c,Street__c,ZipCode__c,PRMAccountCompletedProfile__c';
       insert jobSet7;
       
       CS_PRM_ApexJobSettings__c jobSet8= new CS_PRM_ApexJobSettings__c(); 
       jobSet8.name='PRMContactFieldList1';
       jobSet8.InitialSync__c=True;
       jobSet8.QueryFields__c='Id,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMJobTitle__c,PRMJobFunc__c,PRMWorkPhone__c,PRMMobilePhone__c,PRMCountry__c,PRMCustomerClassificationLevel1__c,PRMCustomerClassificationLevel2__c,PRMOrigin__c,PRMLastLoginDate__c,PRMTaxId__c,PRMContact__c,';
       insert jobSet8;
       
       CS_PRM_ApexJobSettings__c jobSet9= new CS_PRM_ApexJobSettings__c(); 
       jobSet9.name='PRMContactFieldList2';
       jobSet9.InitialSync__c=True;
       jobSet9.QueryFields__c='PRMPrimaryContact__c,PRMUser__c,IsMasterSalesContact__c,PRMContactRegistrationStatus__c,PRMReasonForDecline__c,PRMRegistrationActivationDate__c,PRMSecondary1ClassificationLevel1__c,PRMSecondary2ClassificationLevel1__c,';
       insert jobSet9;
       
        CS_PRM_ApexJobSettings__c jobSet10= new CS_PRM_ApexJobSettings__c(); 
       jobSet10.name='PRMContactFieldList3';
       jobSet10.InitialSync__c=True;
       jobSet10.QueryFields__c='PRMSecondary3ClassificationLevel1__c,PRMSecondary1ClassificationLevel2__c,PRMSecondary2ClassificationLevel2__c,PRMSecondary3ClassificationLevel2__c,PRMExcludeFromReports__c,PRMIsActiveContact__c,PRMUIMSID__c,AccountId,';
       insert jobSet10;
       
       CS_PRM_ApexJobSettings__c jobSet11= new CS_PRM_ApexJobSettings__c(); 
       jobSet11.name='PRMContactFieldList4';
       jobSet11.InitialSync__c=True;
       jobSet11.QueryFields__c='PRMRegistrationReviewedRejectedDate__c,PRMApplicationRequested__c,PRMIsEnrolledContact__c,PRMOriginSource__c,PRMOriginSourceInfo__c,FieloEE__Member__c,PRMUIMSSEContactId__c,PRMDoNotHaveCompany__c,PRMOnly__c,FieloEE__Member__r.Name,FirstName,LastName,Email';
       insert jobSet11;
       
       Country__c TestCountry = new Country__c();
            TestCountry.Name   = 'India';
            TestCountry.CountryCode__c = 'IN';
            TestCountry.InternationalPhoneCode__c = '91';
            TestCountry.Region__c = 'APAC';
            INSERT TestCountry;
     
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            INSERT testStateProvince;
            
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', 
                    Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMCountry__c=TestCountry.id,PRMBusinessType__c='FI',
                         PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, StateProvince__c = testStateProvince.id,City__c='Bangalore');
            insert PartnerAcc;
            
        Contact PartnerCon = new Contact(
        FirstName='Test',
        LastName='lastname',
        AccountId=PartnerAcc.Id,
        JobTitle__c='Z3',
        CorrespLang__c='EN',
        WorkPhone__c='999121212',
        Country__c=TestCountry.Id,
        PRMFirstName__c = 'Test',
        PRMLastName__c = 'lastname',
        PRMEmail__c = 'test.lastName@yopmail.com',
        PRMWorkPhone__c = '999121212',
        PRMPrimaryContact__c = true);
        insert PartnerCon;
        
}

 static testMethod void testMe(){
 Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) {
       
            List<String> names =new List<String>{'PAGESIZE_INTERNALUSER','PRMAccountFieldList1','PRMAccountFieldList2','PRMAccountFieldList3','PRMAccountFieldList4','PRMAccountFieldList5','PRMAccountFieldList6','PRMAccountFieldList7','PRMContactFieldList1','PRMContactFieldList2','PRMContactFieldList3','PRMContactFieldList4'};
            List<CS_PRM_ApexJobSettings__c> pajs =new List<CS_PRM_ApexJobSettings__c>([select name,QueryFields__c,InitialSync__c from CS_PRM_ApexJobSettings__c where name in : names]);            
            Country__c country = [select Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c Where Name='India'];
            StateProvince__c stateProv = [select id,name,Country__c,CountryCode__c,StateProvinceCode__c from StateProvince__c  where name='Karnataka'];    
            Account acc =[select id,name,Street__c,POBox__c,ZipCode__c,Country__c,PRMCompanyName__c,PRMCountry__c,PRMBusinessType__c,PRMAreaOfFocus__c,PLShowInPartnerLocatorForced__c,StateProvince__c,City__c from Account where City__c='Bangalore'];
            Contact con = [Select FirstName,LastName,JobTitle__c,CorrespLang__c,AccountId,WorkPhone__c,Country__c,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMWorkPhone__c,PRMPrimaryContact__c from Contact where WorkPhone__c='999121212'];
           
            PartnerProgram__c ppm =new PartnerProgram__c();
            ppm.name='123TestProgram456';
            ppm.ProgramStatus__c='Active';
            ppm.country__c= country.Id;
            ppm.recordTypeId=System.Label.CLMAY13PRM15;
            insert ppm;
            ProgramLevel__c plv =new ProgramLevel__c();
            plv.name ='Testing';
            plv.PartnerProgram__c=ppm.id;
            plv.Description__c='This is a Test Description ';   
            plv.ModifiablebyCountry__c='Yes';
            insert plv;
            test.startTest();
            System.debug('PartnerAcc 245 :'+acc.Id);
            Test.SetCurrentPageReference(New PageReference('Page.VFC_PRMProgAssignment'));
            String RecordId =System.CurrentPageReference().getParameters().put('id',acc.Id);   
            VFC_PRMProgAssignment ppa = new VFC_PRMProgAssignment();
            ppa.searchString ='TestProgram';    
            ppa.Search();   
            ppa.SaveProgram();    
            Test.stopTest();
        }

    }  
 
    static testMethod void testME1(){
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) {
            List<String> names =new List<String>{'PAGESIZE_INTERNALUSER','PRMAccountFieldList1','PRMAccountFieldList2','PRMAccountFieldList3','PRMAccountFieldList4','PRMAccountFieldList5','PRMAccountFieldList6','PRMAccountFieldList7','PRMContactFieldList1','PRMContactFieldList2','PRMContactFieldList3','PRMContactFieldList4'};
            List<CS_PRM_ApexJobSettings__c> pajs =new List<CS_PRM_ApexJobSettings__c>([select name,QueryFields__c,InitialSync__c from CS_PRM_ApexJobSettings__c where name in : names]);            
            Country__c country = [select Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c Where Name='India'];
            StateProvince__c stateProv = [select id,name,Country__c,CountryCode__c,StateProvinceCode__c from StateProvince__c  where name='Karnataka'];    
            Account acc =[select id,name,Street__c,POBox__c,ZipCode__c,Country__c,PRMCompanyName__c,PRMCountry__c,PRMBusinessType__c,PRMAreaOfFocus__c,PLShowInPartnerLocatorForced__c,StateProvince__c,City__c from Account where City__c='Bangalore'];
            Contact con = [Select FirstName,LastName,JobTitle__c,CorrespLang__c,AccountId,WorkPhone__c,Country__c,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMWorkPhone__c,PRMPrimaryContact__c from Contact where WorkPhone__c='999121212'];
                 
            PartnerProgram__c ppm =new PartnerProgram__c();
            ppm.name='123TestProgram456';
            ppm.ProgramStatus__c='Active';
            ppm.country__c= country.Id;
            ppm.recordTypeId=System.Label.CLMAY13PRM15;
            insert ppm;
            ProgramLevel__c plv =new ProgramLevel__c();
            plv.name ='Testing';
            plv.PartnerProgram__c=ppm.id;
            plv.Description__c='This is a Test Description ';   
            plv.ModifiablebyCountry__c='Yes';
            insert plv;
            
            Test.startTest();
            Test.SetCurrentPageReference(New PageReference('Page.VFC_PRMProgAssignment'));    
            String recId =System.CurrentPageReference().getParameters().put('id',con.Id);
            VFC_PRMProgAssignment ppa = new VFC_PRMProgAssignment();
            ppa.searchString ='TestProgram';   
            ppa.Search(); 
            Test.stopTest();    
        } 
    } 

}