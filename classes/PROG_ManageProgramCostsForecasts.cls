public with sharing class PROG_ManageProgramCostsForecasts {
        
    public ApexPages.StandardController controller;
    public ProgramCostsForecasts__c pCF {get;set;}
    public List<Integer> yearsInt {get;set;}
    
    public Map<Integer,string> yearsMap {get;set;}
    
    public Map<string,string> alphaTypes {get;set;}
    
    public Map<string,string> colourMap {get;set;}
    
    public Map<Integer,List<string>> keysforyear {get;set;}
    
    public String mapkey{get;set;}{mapkey='';}
    
    public List<string> lstTypeOrder {get;set;}
    
    public Set<string> setTypeOrder {get;set;}
    
    Map<string,DMTProgramCostForecastCostItemsFields__c> mapCS {get;set;}
     
    public List<string> lstType {get;set;}
public String fieldnameMapkeyset{get;set;}{fieldnameMapkeyset='';}
    Boolean boolActive = false;
    public Map<string,string> keyMap {get;set;}
    {
        keyMap = new Map<string,string>();
    }
    public Map<string,string> fieldnameMap {get;set;}
    {
        fieldnameMap = new Map<string,string>();
        system.debug(DMTProgramCostForecastCostItemsFields__c.getall().values());
        Map<String,DMTProgramCostForecastCostItemsFields__c> csMap = DMTProgramCostForecastCostItemsFields__c.getAll();
        System.Debug('csMap size: ' + csMap.keySet().size());
        for(String s:csMap.keySet())
        {
            string strValue = csMap.get(s).CostItemFieldAPIName__c;
            string stValue = csMap.get(s).CostItemFieldLabel__c;
            System.Debug('strValue: ' + strValue);
            fieldnameMap.put(stValue ,strValue);
            fieldnameMapkeyset+='$'+stValue+'$';
            mapkey += stValue + ',';
            keyMap.put(strValue,stValue );
            
        }
        System.Debug('Mapkey: ' + mapkey);
    }
    
    public string startYear {get;set;}
        public PROG_ManageProgramCostsForecasts(ApexPages.StandardController controller)
    {
        this.controller = controller;
        colourMap = new Map<String,String>();
        for(DMTPastBudgetFields__c cs1:DMTPastBudgetFields__c.getAll().values())
        {
            if(!colourMap.containsKey(cs1.Name))
                colourMap.put(cs1.Name,cs1.BackgroundColour__c);
        }
 
        if(!Test.isRunningTest())
        {
            controller.addFields(fieldnameMap.values());
        }                
        pCF = (ProgramCostsForecasts__c) controller.getRecord();
        mainInit();
         
    }
    
    private void mainInit()
    {
        yearsInt = new List<Integer>();
        yearsMap = new Map<Integer,string>();
        alphaTypes = new Map<string,string>();
        keysforyear = new Map<Integer,List<string>>();
        lstTypeOrder = new List<string>();
        setTypeOrder = new Set<string>();
        mapCS = DMTProgramCostForecastCostItemsFields__c.getAll();
        lstType = new List<string>();
        
        for(DMTProgramCostForecastCostItemsFields__c cs1:mapCS.values())
        {
            if(!alphaTypes.containsKey(cs1.Type__c))
                alphaTypes.put(cs1.Type__c,cs1.Type__c);        
            if(!setTypeOrder.contains(cs1.TypeOrder__c))
                setTypeOrder.add(cs1.TypeOrder__c);
        }
        List<Integer> iList=new List<Integer>();
        String sType='';
        for(String s:setTypeOrder){
   
            iList.add(integer.valueOf(s.substring(5,s.length())));
            sType=s.substring(0,5);
    
        }
        iList.sort();
        List<String> stringTypeOrder=new List<String>();
        for(Integer i:iList){
            stringTypeOrder.add(sType+i);
        }
        /*
        for(string to:setTypeOrder)
        {
            lstTypeOrder.add(to);
        }
        */
        //lstTypeOrder.sort();
        
        //lstTypeOrder=new String[]{'Type 1','Type 2','Type 3','Type 4','Type 5','Type 6','Type 7','Type 8','Type 9','Type 10','Type 11','Type 12','Type 13','Type 14','Type 15'};
        lstTypeOrder =  stringTypeOrder;
        for(integer i=0;i<lstTypeOrder.size();i++)
        {
            for(DMTProgramCostForecastCostItemsFields__c cs1:mapCS.values())
            {
                if(cs1.TypeOrder__c == lstTypeOrder.get(i))
                {
                    lstType.add(cs1.Type__c);
                    break;
                }
            }
        }               
                       
        pCF = [SELECT Active__c,CreatedDate,CurrencyIsoCode,FY0AppServicesCustExp__c,FY0BusinessCashoutCAPEX__c,FY0BusinessCashoutOPEXOTC__c,FY0BusinessInternal__c,FY0BusinessRunningCostsImpact__c,FY0BusInternalChargedbacktoIPO__c,FY0CashoutCAPEX__c,FY0CashoutOPEX__c,FY0GDCosts__c,FY0NonIPOITCostsInternal__c,FY0PLImpact__c,FY0Regions__c,FY0RunningITCostsImpact__c,FY0SmallEnhancements__c,FY0TechnologyServices__c,FY1AppServicesCustExp__c,FY1BusinessCashoutCAPEX__c,FY1BusinessCashoutOPEXOTC__c,FY1BusinessInternal__c,FY1BusinessRunningCostsImpact__c,FY1BusInternalChargedbacktoIPO__c,FY1CashoutCAPEX__c,FY1CashoutOPEX__c,FY1GDCosts__c,FY1NonIPOITCostsInternal__c,FY1PLImpact__c,FY1Regions__c,FY1RunningITCostsImpact__c,FY1SmallEnhancements__c,FY1TechnologyServices__c,FY2AppServicesCustExp__c,FY2BusinessCashoutCAPEX__c,FY2BusinessCashoutOPEXOTC__c,FY2BusinessInternal__c,FY2BusinessRunningCostsImpact__c,FY2BusInternalChargedbacktoIPO__c,FY2CashoutCAPEX__c,FY2CashoutOPEX__c,FY2GDCosts__c,FY2NonIPOITCostsInternal__c,FY2PLImpact__c,FY2Regions__c,FY2RunningITCostsImpact__c,FY2SmallEnhancements__c,FY2TechnologyServices__c,FY3AppServicesCustExp__c,FY3BusinessCashoutCAPEX__c,FY3BusinessCashoutOPEXOTC__c,FY3BusinessInternal__c,FY3BusinessRunningCostsImpact__c,FY3BusInternalChargedbacktoIPO__c,FY3CashoutCAPEX__c,FY3CashoutOPEX__c,FY3GDCosts__c,FY3PLImpact__c,FY3Regions__c,FY3RunningITCostsImpact__c,FY3SmallEnhancements__c,FY3TechnologyServices__c,FY3NonIPOITCostsInternal__c,Id,LastActiveofBudgetYear__c,Name,NewFY0CashoutCAPEX__c,NewFY1CashoutCAPEX__c,NewFY2CashoutCAPEX__c,NewFY3CashoutCAPEX__c,TechBCD__c FROM ProgramCostsForecasts__c where Id = :pCF.Id];
        
        startYear = string.valueOf(pCF.CreatedDate.year());
        system.debug('***************'+ startYear);
        generateKeysBasedOnStartYear();
    }
    private void generateKeysBasedOnStartYear()
    {
        List<String> concat = new List<String>();
        Integer iYear = Integer.valueOf(startYear.trim());
        Integer calcYear = 0;
        
        
        for (integer i = 0;i<4;i++) 
        {
            concat = new List<String>();
            calcYear = iYear + i;         
            yearsInt.add(calcYear);
            yearsMap.put(calcYear,'Y' + i);
            for (String subkey : alphaTypes.values())
            {
                string strVal = yearsMap.get(calcYear);
                System.debug('***asdf'+subkey);
                System.debug('***asdf'+strVal);
                concat.add(subkey+strVal);                              
            }
            keysforyear.put(calcYear, concat);
        }               
    }
    public pagereference updateFundingEntity()
    {       
        try
        { 
                update pCF;
        }        
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));
                      
        }              
        
        PageReference pageRef = new PageReference('/apex/PROG_ProgramCostForeCast?id='+ pCF.Id);
        pageRef.setRedirect(true);
        return pageRef;  
        
    }
     public Pagereference backToFundingEntity()
    {
        PageReference pageRef = new PageReference('/'+ pCF.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
     public class noMessageException extends Exception{}
}