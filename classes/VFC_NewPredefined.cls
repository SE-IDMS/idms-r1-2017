/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:    Prepopulate certain case fields. Show error message if contact is marked for deletion, thus preventing case creation
    Test Class:     VFC_NewPredefined_TEST
    Modified:    
            Q3 2016 Release     Pooja Suresh    BR-9679:removed cancel() method from class, cancel redirection managed at page for console compatibilty
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public Class VFC_NewPredefined{
    public String ContactId{get;set;}  
    public String AccountId{get;set;}
    public String CtiSkillLocalStorage{get;set;}
    public String SpocPhoneNoLocalStorage{get;set;}
    public String SubscriptionIDLocalStorage{get;set;}
    public String CountryCodeLocalStorage{get;set;}
    public boolean TobeDeletedVar{get;set;}
    public List   <Contact> lstContact{get;set;}
    public boolean NewPredefined{get;set;}
    
    public String CountryName{get;set;}
    public List<Country__c> lstCountries;
    
    public VFC_NewPredefined(){
        if(apexpages.currentpage().getparameters().ContainsKey('def_contact_id')){
            ContactId=apexpages.currentpage().getparameters().get('def_contact_id');
        }
        if(apexpages.currentpage().getparameters().ContainsKey('def_account_id')){
            AccountId=apexpages.currentpage().getparameters().get('def_account_id');
        }
        if(ContactId!=null && ContactId!=''){
            List<Contact> lstContact = [SELECT id,AccountId, ToBeDeleted__c,ReasonForDeletion__c,Inactive__c from Contact Where id=:ContactId];
            if(lstContact[0].ToBeDeleted__c==TRUE && lstContact[0].ReasonForDeletion__c!=Label.CL00521)
            {    
                TobeDeletedVar=True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC01);
                ApexPages.addMessage(myMsg);
            }
            else if(lstContact[0].Inactive__c==TRUE)
            {   
                TobeDeletedVar=True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC03);
                ApexPages.addMessage(myMsg);
            }
        }
        if(AccountId!=null && AccountId!=''){
            List<Account> lstAccount = [SELECT id,ToBeDeleted__c,ReasonForDeletion__c,Inactive__c from Account Where id=:AccountId];
            System.debug('lstAccount is : '+lstAccount);
            if(lstAccount[0].ToBeDeleted__c==TRUE && lstAccount[0].ReasonForDeletion__c!=Label.CL00521)
            {
                TobeDeletedVar=True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC02);
                ApexPages.addMessage(myMsg);
            }
            else if(lstAccount[0].Inactive__c==TRUE)
            {   
                TobeDeletedVar=True;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC04);
                ApexPages.addMessage(myMsg);
            }
        }
    }
    public PageReference pageaction(){
        PageReference objPageReference;
         lstContact=[select id,lastname,AccountId from contact where id=:ContactId]; 
        if(CtiSkillLocalStorage=='' || CtiSkillLocalStorage==null){
            objPageReference= new PageReference('/apex/VFP81_NewCase?def_contact_id='+ContactId+'&def_account_id='+lstContact[0].AccountId);
            return objPageReference;
        }
        else if(CtiSkillLocalStorage!='' && CtiSkillLocalStorage!= null){
            String strURLParams;
            NewPredefined=false;
            //String CaseCategory = 'TroubleShooting*';
            list<CustomerCareTeam__c> lstCCTeam = [Select id,LevelofSupport__c,CCCountry__r.name from CustomerCareTeam__c where CTI_Skill__c=:CtiSkillLocalStorage];
            
            if(!lstCCTeam.Isempty() && lstCCTeam!=null){
                list<TeamCaseJunction__c> SupportedCaseClasification = [select id,Default__c from TeamCaseJunction__c where CCTeam__c=:lstCCTeam[0].Id AND Default__c=true];
                    if(!SupportedCaseClasification.Isempty()&& SupportedCaseClasification !=null){
                        TeamCaseJunction__c Classification = [Select id,CCTeam__r.Name,CCTeam__r.LevelofSupport__c,CCTeam__r.CCCountry__r.name,CaseClassification__r.Category__c,CaseClassification__r.Reason__c,CaseClassification__r.SubReason__c from TeamCaseJunction__c where Id=:SupportedCaseClasification[0].Id];
                        
                        List<PointOfContact__c> lstPointOfContact = new List<PointOfContact__c>(); 
                        strURLParams = '?';
                        boolean blnParamAdded=false;
                        
                        if(SpocPhoneNoLocalStorage!=null && SpocPhoneNoLocalStorage!=''){
                            lstPointOfContact  = [select id,IsActive__c,SupportedCountry__r.name,Organization__r.name  from PointOfContact__c where Name=:SpocPhoneNoLocalStorage and IsActive__c=True];
                            System.debug('lstPointOfContact'+lstPointOfContact);
                            
                            if(lstPointOfContact!=null && !lstPointOfContact.Isempty()){
                                strURLParams = strURLParams +'PocCountry='+lstPointOfContact[0].SupportedCountry__r.Name +'&RepAccOrg='+lstPointOfContact[0].Organization__r.Name+'&SpocPhONE='+SpocPhoneNoLocalStorage ;  
                                blnParamAdded=true;
                                System.debug('strURLParams:'+strURLParams );       
                            }
                        }
                        if(SubscriptionIDLocalStorage!=null && SubscriptionIDLocalStorage!=''){
                            if(blnParamAdded){
                                strURLParams=strURLParams +'&def_Subscription_Id='+SubscriptionIDLocalStorage;
                            }
                            else{
                                strURLParams=strURLParams +'def_Subscription_Id='+SubscriptionIDLocalStorage;
                            }
                            System.debug('strURLParams:'+strURLParams );
                        }
                        
                        if(CountryCodeLocalStorage!=null && CountryCodeLocalStorage!='' ){
                            lstCountries= [Select Id,Name,CountryCode__c from Country__c where CountryCode__c=:CountryCodeLocalStorage];
                            if(lstCountries !=null && !lstCountries.Isempty() ){
                                CountryName=lstCountries[0].Name;
                                system.debug('country name'+CountryName);
                            }   
                        }
                        else{
                            if(lstPointOfContact !=null && !lstPointOfContact.Isempty() ){
                                CountryName= lstPointOfContact[0].SupportedCountry__r.Name;
                                system.debug('country name from poc'+CountryName);
                            }
                        }
                        if(CountryName!=null && CountryName!=''){
                            if(blnParamAdded){
                                strURLParams=strURLParams +'&CountryCode='+CountryName;
                            }
                            else{
                                strURLParams=strURLParams +'CountryCode='+CountryName;
                            }
                        }
                        system.debug('ContactId'+ContactId);
                        system.debug('lstContact[0].AccountId'+lstContact[0].AccountId);
                        system.debug('Classification.CaseClassification__r.Category__c'+Classification.CaseClassification__r.Category__c);
                        system.debug('CtiSkillLocalStorage'+CtiSkillLocalStorage);
                        system.debug('Classification.CaseClassification__r.Reason__c'+Classification.CaseClassification__r.Reason__c);
                        system.debug('lstCCTeam[0].LevelofSupport__c'+lstCCTeam[0].LevelofSupport__c);
                        system.debug('Classification.CCTeam__r.Name'+Classification.CCTeam__r.Name);
                        system.debug('Classification.CaseClassification__r.SubReason__c'+Classification.CaseClassification__r.SubReason__c);
                        
                        
                            
                        strURLParams=strURLParams +'&def_contact_id='+ContactId+'&def_account_id='+lstContact[0].AccountId + '&CaseCategory='+Classification.CaseClassification__r.Category__c+'&CTISkill='+CtiSkillLocalStorage+'&CaseReason='+Classification.CaseClassification__r.Reason__c+'&LOE='+lstCCTeam[0].LevelofSupport__c+'&CaseTeam='+Classification.CCTeam__r.Name+'&SubReason='+Classification.CaseClassification__r.SubReason__c;
                        system.debug('strURLParams'+strURLParams);
                    
                        objPageReference = new pagereference('/apex/VFP_NewCaseRedirectionPage'+strURLParams);
                        return objPageReference; 
                    } 
                    else{
                        objPageReference = new PageReference('/apex/VFP81_NewCase?def_contact_id='+ContactId+'&def_account_id='+lstContact[0].AccountId);
                        return objPageReference;
                    }
            }
            else {
                objPageReference = new PageReference('/apex/VFP81_NewCase?def_contact_id='+ContactId+'&def_account_id='+lstContact[0].AccountId);
                return objPageReference;
            }
        }
        else{
            objPageReference= new PageReference('/apex/VFP81_NewCase?def_contact_id='+ContactId+'&def_account_id='+lstContact[0].AccountId);
            return objPageReference;
        }
        return null;
    }
    
}