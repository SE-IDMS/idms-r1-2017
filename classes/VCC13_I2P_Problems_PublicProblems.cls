/*
    Author          : Srinivas Nallapati
    Date Created    : 05/Mar/2012
    Description     : For generating a list of Public Problems
                        
*/
public with sharing class VCC13_I2P_Problems_PublicProblems {

    public List<Problem__c> lstPublicProblems { get; set; }
    
    public VCC13_I2P_Problems_PublicProblems(){
        String NoPastDays = System.label.CLI2PMAR120003;
        Integer NoPastDaysInt = Integer.valueOf(NoPastDays);
        
        String NoLimit = System.label.CLI2PMAR120004;
        Integer NoProblemsLimit = Integer.valueOf(NoLimit);
        
        lstPublicProblems  = new List<Problem__c>();
        lstPublicProblems  = [SELECT AccountableOrganization__c,AffectedOffer__c,Name,ProblemLeader__c,ProblemLeader__r.name, Severity__c,Status__c,TECH_ProblemMadePublicOn__c,Title__c,AccountableOrganization__r.Location__c, AccountableOrganization__r.Location_Type__c, AccountableOrganization__r.SubEntity__c, AccountableOrganization__r.Entity__c FROM Problem__c where Sensitivity__c= 'Public' and TECH_ProblemMadePublicOn__c!= null and TECH_ProblemMadePublicOn__c > :system.today()-NoPastDaysInt limit :NoProblemsLimit  ];
        
    }
    
    public string SfUrl {get;set;} { SfUrl = URL.getSalesforceBaseUrl().toExternalForm();}
}