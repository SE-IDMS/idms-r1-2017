/*
    Author          : Deepak
    Date Created    : 20/12/2013
    Description     : Test class for VFC_ServiceContract_Prepopulate class 
*/
@isTest(SeeAllData=true)
private class VFC_ServiceContract_Prepopulate_TEST 
{
    static testMethod void testServiceContractPrepopulate2()
    {
        Test.startTest();
        SVMXC__Service_Contract__c sc1 = new SVMXC__Service_Contract__c();
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
        insert testContact;
        PageReference pageRef = Page.VFP_ServiceContract_Prepopulate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(Label.CLOCT13SRV09,testAccount.Name);
        ApexPages.currentPage().getParameters().put(Label.CLOCT13SRV10,testAccount.id);
        ApexPages.StandardController sc =new ApexPages.StandardController(sc1);
        VFC_ServiceContract_Prepopulate cont = new VFC_ServiceContract_Prepopulate(sc);
        cont.gotoeditpage();
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put(Label.CLOCT13SRV17,testAccount.Name);
        ApexPages.currentPage().getParameters().put(Label.CLOCT13SRV18,testAccount.id);
        VFC_ServiceContract_Prepopulate cont2 = new VFC_ServiceContract_Prepopulate(sc);
        cont2.gotoeditpage();
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put(Label.CLOCT13SRV07,testContact.Name);
        ApexPages.currentPage().getParameters().put(Label.CLOCT13SRV08,testContact.id);
        VFC_ServiceContract_Prepopulate cont3 = new VFC_ServiceContract_Prepopulate(sc);
        cont3.gotoeditpage();
        
        
        
    }
}