//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This is class implements regisration handler class.
//                   This class creates LinkedIn Social provider depending on Context i.e. Home and Work of application.
//
//*******************************************************************************************

global class IdmsLinkedInRegHandler implements Auth.RegistrationHandler{
    global boolean canCreateUser(Auth.UserData data) {
        
        if(data.email!=null) {
            system.debug('data 1st'+data);
            return true;
        }else{
            return false;
        }
    }
    
    //This is global method to create Home or Work Users as per the context of app.
    global User createUser(Id portalId, Auth.UserData data){
        String appid;
        String context;
        String registrationSource;
        //Custom setting to retrieve an application Id.
        IdmsSocialUser__c idmsSocialUser = IdmsSocialUser__c.getInstance('IdmsKey');
        appid                            = idmsSocialUser.AppValue__c;
        
        //code for getting context and app details        
        IDMSApplicationMapping__c appMap;
        appMap = IDMSApplicationMapping__c.getInstance(appid);
        if(appMap != null){
            context            = (String)appMap.get('context__c');
            registrationSource = (String)appMap.get('AppName__c');
        }
        if(context == 'Home' || context == 'home'){
            User usr_home;
            system.debug('Entered to create Home user');
            Id ProfileId = getProfileId(context);
            //Create Consumer Account and return ID.    
            try{
                Id contactId = createPersonAccountContact(data);
                if(ProfileId != null){
                    usr_home = createIdmsUser(data, ProfileId);
                }
                usr_home.contactId            = contactId;
                usr_home.IDMS_User_Context__c = Label.CLQ316IDMS090;
                insert usr_home;
            }catch (Exception e){
                System.debug('Error:' + e.getMessage() + e.getStackTraceString());
            }
            return usr_home;
        }else {
            User usr_work;            
            Id ProfileId = getProfileId(context);
            try{
                if(ProfileId != null){
                    usr_work = createIdmsUser(data, ProfileId);
                }  
                //call bFO method to create at work users with Digital accounts.       
                bFoMatchingResult__c bfoMatchingRes = AP_MatchingModule.preMatch(usr_work);
                usr_work.ContactId                  = bfoMatchingRes.Contact__c;
                usr_work.IDMS_User_Context__c       = Label.CLQ316IDMS080;
                Insert usr_work;
            }catch (Exception e){
                System.debug('Error:' + e.getMessage() + e.getStackTraceString());
            }
            return usr_work;
        }
        
    }
    
    //Create at home or at work users
    private User createIdmsUser(Auth.UserData data, Id ProfileId) {
        User u                          = new User();
        u.username                      = Label.CLQ316IDMS020+data.email+Label.CLJUN16IDMS71;
        u.email                         = data.email;
        u.lastName                      = data.lastName;
        u.firstName                     = data.firstName;
        u.alias                         = 'Alias';
        u.languagelocalekey             = 'en_US';
        u.localesidkey                  = 'en_US';
        u.emailEncodingKey              = 'UTF-8';
        u.timeZoneSidKey                = 'America/Los_Angeles';
        u.IDMS_Registration_Source__c   = Label.CLQ316IDMS020;
        u.IsIDMSUser__c                 = True;        
        u.IDMSIdentityType__c           = Label.CLQ316IDMS091;
        u.profileid                     = ProfileId;
        return u;
    }
    
    //This method creates person account for home user.
    private Id createPersonAccountContact(Auth.UserData data) {
        Account person = new Account();
        try{
            
            person.LastName                         = data.lastName;
            person.FirstName                        = data.FirstName;
            person.personEmail                      = data.email;
            person.IDMS_Contact_UserExternalId__pc  = Label.CLQ316IDMS020+data.email+Label.CLJUN16IDMS71;
            person.RecordTypeId = [Select Id From RecordType 
                                   Where SobjectType='Account' 
                                   AND isPersonType=true LIMIT 1].id;
            
            insert person;
        }catch(Exception e) {
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
        }
        /**
* This next step is necessary to get a valid contact Id,
* it won't exist until the PersonAcct is saved
**/         
        Account a = [Select PersonContactId From Account Where Id = :person.Id];
        return a.PersonContactId; 
    }
    
    //method that provide profile Id of provided context.
    public Id getProfileId(String context){
        String profilename = IDMS_Profile_Mapping__c.getInstance(context).Profile_Name__c;
        List<Profile> ProfileIds = [SELECT Id FROM Profile WHERE Name=:profilename limit 1];
        if(ProfileIds.size() > 0){
            return ProfileIds[0].id;
        }
        return null;
    }
    
    //This method update user if social user did first login with his linkedin account.
    global void updateUser(Id userId, Id portalId, Auth.UserData data){
        
        User u = new User(Id=userId);
        try{
            u.IDMS_Registration_Source__c   = data.provider;
            u.username                      = Label.CLQ316IDMS020+data.email+Label.CLJUN16IDMS71;
            update u;
        }catch(Exception e){
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
        }
    }
}