public with sharing class VFC_AssociateOLSearch 
{    
    public string prodLine{get;set;}
    public string selProdBU{get;set;}
    public Integer numberOfRecord{get;set;} 
    public Boolean searchButtonClicked{get;set;}
    public Boolean warningMessage{get;set;}
    public String first_picklistOption = 'No Filter';
    public List<OpportunityLines> oppLinesList{get; set;}{oppLinesList = new List<OpportunityLines>();}
    public List<OPP_ProductLine__c> selectedOppLines = new List<OPP_ProductLine__c>();
    public string sourceType{get;set;}
    public string VCPAccount{get;set;}
    public string recordID;
    public String strSearch;
    OPP_ValueChainPlayers__c valueChain = new OPP_ValueChainPlayers__c();
    public OPP_OpportunityCompetitor__c compOpp {get;set;}{compOpp = new OPP_OpportunityCompetitor__c();}
    List<VCPOpportunityLineLink__c> selectedVCPOppLineLinks = new List<VCPOpportunityLineLink__c>();
    List<OPP_OpportunityLineCompetitor__c> selectedCompOppLineLinks = new List<OPP_OpportunityLineCompetitor__c>();
    List<VCPOpportunityLineLink__c> valueChainOLs = new List<VCPOpportunityLineLink__c>();
    Set<Id> linkedOLines= new Set<Id>();
    Set<Id> linkedCompOLines= new Set<Id>();
    public String lineStatusDel=System.Label.CLOCT14SLS69;
    
    List<OPP_ProductLine__c> queryList = new List<OPP_ProductLine__c>();
    
    public VFC_AssociateOLSearch() 
    {
        sourceType = ApexPages.currentPage().getParameters().get('SourceType');
        recordID = ApexPages.currentPage().getParameters().get('id');
        if(sourceType != null)
        {
            if (sourceType == 'COMPOL' ) //if component is called from OL Competitor Object
            {
                compOpp = [select Id,Name,OpportunityName__c,TECH_CompetitorName__r.Name from OPP_OpportunityCompetitor__c where Id =:recordID];
                System.debug('------>>>>'+ compOpp);  
            }
            else if (sourceType == 'VCPOL' )  //if component is called from VCP-OL Link Object
            {
                valueChain= [select Id,Name,OpportunityName__c,Account__r.Name from OPP_ValueChainPlayers__c where Id =:recordID];
                if(valueChain.Account__r.Name != null)
                    VCPAccount = valueChain.Account__r.Name;
                else
                    VCPAccount = '';
                System.debug('------>>>> valueChain '+ VCPAccount );
                //System.debug('------>>>> valueChain '+ valueChain);
                //System.debug('------>>>> valueChain.OpportunityName__c '+ valueChain.OpportunityName__c );
            }               
        }
        numberOfRecord=0;
        searchButtonClicked=False;
        warningMessage=False;
        selProdBU='No Filter';
        searchRecords();
    }
    
    public class OpportunityLines 
    {
        public OPP_ProductLine__c oppLine {get; set;}
        public Boolean selected {get; set;}
        public Integer quantityOl {get; set;}
        public OpportunityLines(OPP_ProductLine__c ol) 
        {
            oppLine = ol;
            selected = false;
            quantityOl = NULL;
        }
    }
    
    public List<SelectOption> getprodBUS() 
    {
        List<CS_ProductLineBU__C> productLines = CS_ProductLineBU__c.getall().values();
        Set<String> prodLines = new Set<String>();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(first_picklistOption,first_picklistOption));
        for(OPP_Product__c pb: [SELECT Id, BusinessUnit__c FROM OPP_Product__c WHERE ProductLine__c = NULL AND BusinessUnit__c != NULL ORDER BY BusinessUnit__c])
        {
            if(!(prodLines.contains((pb.BusinessUnit__c))))
            {
                options.add(new SelectOption(pb.BusinessUnit__c,pb.BusinessUnit__c));
                prodLines.add(pb.BusinessUnit__c);
            }
            System.debug(pb);
        }
        return options;
    }
    
     public void searchRecords()
    {
        searchButtonClicked=True;
        if (sourceType == 'COMPOL' )
        {
            List <OPP_OpportunityLineCompetitor__c> existingCompOls = [select OpportunityLine__c from OPP_OpportunityLineCompetitor__c where OpportunityCompetitor__c=:recordID];
            for(OPP_OpportunityLineCompetitor__c compOL: existingCompOls )
            {
                System.debug('------>>>> compOL.OpportunityLine__c'+ compOL.OpportunityLine__c);
                linkedCompOLines.add(compOL.OpportunityLine__c);
                System.debug('------>>>> end of loop linkedCompOLines '+ linkedCompOLines);
            }
            
            System.debug('------>>>> compOpp.OpportunityName__c '+ compOpp.OpportunityName__c );
            Id oppid = compOpp.OpportunityName__c;
            strSearch = 'Select Id, Name,Opportunity__c,ProductBU__c,ProductLine__c, Quantity__c, Amount__c, LineStatus__c, LineAmount__c, TECH_CommercialReference__c from OPP_ProductLine__c where Opportunity__c =:oppid and LineStatus__c!=:lineStatusDel';
            
            String whereClause='';
            if(sourceType == 'COMPOL' && linkedCompOLines!=null)
                whereClause += ' AND Id NOT IN :linkedCompOLines';
            if(prodLine!=null && prodLine!='')
                whereClause += ' AND ProductLine__c LIKE \'%'+Utils_Methods.escapeForWhere(prodLine)+'%\'';
            if(selProdBU!='No Filter')
                whereClause += ' AND ProductBU__c =\''+Utils_Methods.escapeForWhere(selProdBU)+'\'';
                if(whereClause.length()>0){
                    strSearch +=' '+whereClause+' ';
                } 
            strSearch += ' ORDER BY Name';
            strSearch += ' LIMIT 200';
            System.debug('strSearch ---->>>>'+strSearch);
            
            List<OPP_ProductLine__c> queryList = Database.query(strSearch);
            System.debug('queryList ---->>>>'+queryList );
            
            try
            {
                oppLinesList.clear();
                numberOfRecord=0;
                warningMessage=False;
                System.debug('oppLinesList ---->>>>'+queryList );
                for(OPP_ProductLine__c ol :queryList)
                {
                   numberOfRecord++;
                   oppLinesList.add(new OpportunityLines(ol));
                }
                System.debug('oppLinesList ---->>>>'+oppLinesList);
                if(numberOfRecord==200)
                     warningMessage=True;
                if(Test.isRunningTest())
                      throw new TestException();
            }
            catch(Exception ex)
            {
                ApexPages.addMessages(ex);
            }
    
        }
        else if(sourceType == 'VCPOL' )
        {
            List <VCPOpportunityLineLink__c> existingOls = [select OpportunityLine__c from VCPOpportunityLineLink__c where valueChainPlayer__c=:recordID];
            for(VCPOpportunityLineLink__c vcOL: existingOls)
            {
                System.debug('------>>>> vcOL.OpportunityLine__c'+ vcOL.OpportunityLine__c);
                linkedOLines.add(vcOL.OpportunityLine__c);
                System.debug('------>>>> end of loop linkedOLines '+ linkedOLines);
            }
            
            System.debug('------>>>> valueChain.OpportunityName__c '+ valueChain.OpportunityName__c );
            Id oppid = valueChain.OpportunityName__c;
            strSearch = 'Select Id, Name,Opportunity__c,ProductBU__c,ProductLine__c, Quantity__c, Amount__c, LineStatus__c, LineAmount__c, TECH_AmountNotToBeUsed__c  from OPP_ProductLine__c where Opportunity__c =:oppid and LineStatus__c!=:lineStatusDel';
            
            String whereClause='';
            if(sourceType == 'VCPOL' && linkedOLines!=null)
                whereClause += ' AND Id NOT IN :linkedOLines';
            if(prodLine!=null && prodLine!='')
                whereClause += ' AND ProductLine__c LIKE \'%'+Utils_Methods.escapeForWhere(prodLine)+'%\'';
            if(selProdBU!='No Filter')
                whereClause += ' AND ProductBU__c =\''+Utils_Methods.escapeForWhere(selProdBU)+'\'';
                if(whereClause.length()>0){
                    strSearch +=' '+whereClause+' ';
                } 
            strSearch += ' ORDER BY Name';
            strSearch += ' LIMIT 200';
            System.debug('strSearch ---->>>>'+strSearch);
            
            List<OPP_ProductLine__c> queryList = Database.query(strSearch);
            System.debug('oppLinesList ---->>>>'+queryList );
            
            try
            {
                oppLinesList.clear();
                numberOfRecord=0;
                warningMessage=False;
                System.debug('oppLinesList ---->>>>'+queryList );
                for(OPP_ProductLine__c ol :queryList)
                {
                   numberOfRecord++;
                   OpportunityLines opptyLine = new OpportunityLines(ol);
                   opptyLine.oppLine.TECH_AmountNotToBeUsed__c = ol.Amount__c;
                   opptyLine.quantityOl = Integer.ValueOf(ol.Quantity__c);
                   oppLinesList.add(opptyLine);
                }
                System.debug('oppLinesList ---->>>>'+oppLinesList);
                if(numberOfRecord==200)
                     warningMessage=True;
                if(Test.isRunningTest())
                      throw new TestException();
            }
            catch(Exception ex)
            {
                ApexPages.addMessages(ex);
            }
        }
    }
    
    public pagereference clearSearch()
    {
        PageReference curPage = new PageReference('/apex/VFP_AssociateOLSearch');
        curPage.getParameters().put('SourceType',sourceType);
        curPage.getParameters().put('id',recordID);
        curPage.setRedirect(true);
        return curPage;
    }
    
    public class TestException extends Exception {}  
    
    public pagereference cancel()
    {
        return new Pagereference('/'+recordID);
    }

    
    public PageReference Save() 
    {
        Database.SaveResult[] associateOL;
        selectedVCPOppLineLinks.clear();
        try
        {
            if(sourceType == 'COMPOL')
            {
                for(OpportunityLines olwrap1 : oppLinesList)
                {
                    if(olwrap1.selected == true)
                    {
                        OPP_OpportunityLineCompetitor__c compOlLink = new OPP_OpportunityLineCompetitor__c();
                        compOlLink.OpportunityCompetitor__c = compOpp.Id;
                        compOlLink.OpportunityLine__c = olwrap1.oppLine.Id;
                        compOlLink.Opportunity__c = compOpp.OpportunityName__c;
                        selectedCompOppLineLinks.add(compOlLink);
                    }
                }
                if(selectedCompOppLineLinks.size()>0)
                    associateOL = Database.insert(selectedCompOppLineLinks); 
                else
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, label.CLOCT14SLS59));
                    return null;
                }
            }
            else if(sourceType == 'VCPOL')
            {

                for(OpportunityLines olwrap : oppLinesList)
                {
                    if(olwrap.selected == true)
                    {
                        VCPOpportunityLineLink__c vcpLink = new VCPOpportunityLineLink__c();
                        vcpLink.ValueChainPlayer__c = valueChain.Id;
                        vcpLink.OpportunityLine__c = olwrap.oppLine.Id;
                        vcpLink.Opportunity__c = valueChain.OpportunityName__c;
                        vcpLink.VCPUnitAmount__c = olwrap.oppLine.TECH_AmountNotToBeUsed__c;
                        vcpLink.VCPQuantity__c = olwrap.quantityOl;
                        selectedVCPOppLineLinks.add(vcpLink);
                    }
                }
                if(selectedVCPOppLineLinks.size()>0)
                    associateOL = Database.insert(selectedVCPOppLineLinks); 
                else
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, label.CLOCT14SLS59));
                    return null;
                }
            }
            for(Database.SaveResult sr: associateOL )
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0]; 
                    System.Debug('######## Error Inserting: '+err.getStatusCode()+' '+err.getMessage());                   
                }
            }
            oppLinesList.clear();
            selectedVCPOppLineLinks.clear();            
        }
        catch(Exception ex)
        {
            ApexPages.addMessages(ex);
            return null;
        }

        return new Pagereference('/'+recordID); 
    }   
}