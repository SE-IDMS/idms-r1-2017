/*Copyright (c) 2010, Sovane Bin, Professional Services, Salesforce.com Inc.
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
  1.    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
  2.    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
  3.    Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this
        software without specific prior written permission.*/
@isTest
private class VFC01_OpptyCockpit_TEST {
    static Account a1;
    static Opportunity o1,o2,o3;
    static List<FeedPost> fp_list = new List<FeedPost>();
    static OpportunityFeed oppf ;
    static OPP_Project__Feed pfeed;
    static FeedComment fcomment;
    static OPP_Project__c prj;
    static 
    {
        // create records for test
        a1 = Utils_TestMethods.createAccount();
        insert a1;
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        prj =Utils_TestMethods.createMasterProject(a1.Id,country.Id);
        insert prj;       
        o1 = Utils_TestMethods.createOpportunity(a1.Id);
        o1.CloseDate = Date.Today();
        o1.ReasonForDeletion__c = 'To Be Deleted';
        o1.ToBeDeleted__c = true; 
        o1.Project__c=prj.Id;       
        insert o1;
        o1.Amount=100;
        update o1;
        
        o2 = Utils_TestMethods.createOpportunity(a1.Id);
        o2.CloseDate=Date.Today();
        o2.ParentOpportunity__c = o1.Id;
        o2.IncludedInForecast__c = 'Yes';
        o2.ToBeDeleted__c = false;
        o2.amount=1;
        insert o2;

    }
    static testMethod void myUnitTest() 
    {                
            FeedPost fp = new FeedPost( ParentId = o1.Id, LinkUrl = 'http://www.google.com');
            fp_list.add(fp);
            FeedPost fp1 = new FeedPost( ParentId = o1.Id, Title='Test', Body ='Test');
            fp_list.add(fp1);
            FeedPost fp2 = new FeedPost( ParentId = prj.Id, Title='Test', Body ='Test');
            fp_list.add(fp2);
            insert fp_list;
            oppf = [select Id from OpportunityFeed where FeedpostId = :fp.Id];
            fcomment = new FeedComment();
            fcomment.FeedItemId = oppf.Id;
            fcomment.CommentBody = 'This is a comment';
            insert(fcomment);

            pfeed = [select Id from OPP_Project__Feed where FeedpostId = :fp2.Id];
            fcomment = new FeedComment();
            fcomment.FeedItemId = pfeed.Id;
            fcomment.CommentBody = 'This is a comment';
            insert(fcomment);

            PageReference pageRef=Page.VFP01_OpptyCockpit;
            Test.setCurrentPageReference(pageRef);
            VFC01_OpptyCockpit myController=new VFC01_OpptyCockpit(new ApexPages.StandardController(o1));
            
            myController.updopptyId = o1.Id;
            myController.updopptyaction = true;
            String str = myController.str_updopptyaction;
            myController.updopptyaction = false;
            str = myController.str_updopptyaction;
            myController.updateOpportunity();
            myController.updopptyId =null;

    }
}