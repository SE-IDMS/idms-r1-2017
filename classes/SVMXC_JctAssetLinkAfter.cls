public class SVMXC_JctAssetLinkAfter {
    
    public static void manageIPLink (List<JctAssetLink__c> triggerNew, Map<Id, JctAssetLink__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to update IP Parent reference on insert of JCtAssetLink - Written : June 6, 2015
            NOTE : This currently does not manage further updates to AssetLink records to further update IP records
        */
        
        Map<Id, Id> assetLinkMap = new Map<Id, Id>();
        
        System.debug('##### Entering SVMXC_JctAssetLinkAfter manageIPLink method');
        
        if (isInsert) {
            for (JCtAssetLink__c al : triggerNew) {
                
                // InstalledProduct__c - downstream , LinkToAsset__c - upstream
                System.debug('##### al.LinkToAsset__c ' + al.LinkToAsset__c);
                System.debug('##### al.InstalledProduct__c ' + al.InstalledProduct__c);
                System.debug('##### al.LinkType__c ' + al.LinkType__c);
                System.debug('##### al.UpdateRelatedIPonInsert__c ' + al.UpdateRelatedIPonInsert__c);
                
                // AS_LINK_1 equals the English translated value of "Child"
                if (al.LinkToAsset__c != null && al.InstalledProduct__c != null && al.LinkType__c == 'AS_LINK_1' && al.UpdateRelatedIPonInsert__c) {
                    if (!assetLinkMap.containsKey(al.InstalledProduct__c))
                        assetLinkMap.put(al.InstalledProduct__c, al.LinkToAsset__c);    
                }   
            }
            
            if (!assetLinkMap.isEmpty()) {
                
                // find IPs to be updated
                List<SVMXC__Installed_Product__c> installedProducts = [SELECT SVMXC__Parent__c, Id FROM SVMXC__Installed_Product__c
                                                                        WHERE Id IN: assetLinkMap.keySet()];
                                                                        
                if (!installedProducts.isEmpty()) {
                    
                    for (SVMXC__Installed_Product__c ip : installedProducts) {
                        ip.SVMXC__Parent__c = assetLinkMap.get(ip.Id);
                    }
                    
                    update installedProducts;
                }
            }
        }
    }
}