@isTest
private class VFC_ShowRelatedProgramsForFeature_Test {
   @isTest static void test_method_two() {
   
			Country__c country = Utils_TestMethods.createCountry();
			insert country;
			Lead led = new Lead(FirstName = 'test', LastName  ='testLead', Street__c='testStreet', Company = 'testCompany',  Country__c = country.id, Priority__c = 3, Email = 'testLead@accenture.com',  OpportunityType__c = 'Standard',OpportunityScope__c = 'BDPRD--Building Management Products',POBOx__c = 'xxx');
			insert led;
			
			Account acc = Utils_TestMethods.createAccount();
			acc.country__c = country.id;
			insert acc;
		  
			acc.PRMAccount__c = True;
			update acc;

			Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
			contact1.PRMContact__c = true;
			insert contact1;

			Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner' Limit 1];
			User user1 = new User(
				Username = 'test12345test@bridge-fo.com',
				ContactId = contact1.Id,
				ProfileId = portalProfile.Id,
				Alias = 'test123',
				Email = 'test12345@test.com',
				EmailEncodingKey = 'UTF-8',
				LastName = 'Kumar',
				CommunityNickname = 'test12345',
				TimeZoneSidKey = 'America/Los_Angeles',
				LocaleSidKey = 'en_US',
				LanguageLocaleKey = 'en_US'
			);
			Database.insert(user1); 

			led.ownerId = user1.id;
			update led;
			
			AP19_Lead.processPassedtoPartnerLeads(new Lead[]{led});
				
   }

  @isTest static void test_method_one() {
    // Implement test code

        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
       // u.ProgramAdministrator__c = true;
        insert u;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.TECH_CountriesId__c = country.id;
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram;
                
        ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
        VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
        
        Recordtype levelRecordType = [Select id from RecordType where developername =  'GlobalProgramLevel' limit 1];
        Recordtype featureRecordType = [Select id from RecordType where developername =  'GlobalFeature' limit 1];
        
        Recordtype countryLevelRecordType = [Select id from RecordType where developername =  'CountryProgramLevel' limit 1];
        Recordtype countryFeatureRecordType = [Select id from RecordType where developername =  'CountryFeature' limit 1];
        
        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype programActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ProgramRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
        
        insert newRecTypeMap;
        
        list<ProgramLevel__c> lstPRGLevel = new list<ProgramLevel__c>();
        list<FeatureCatalog__c> lstFeatureCat = new list<FeatureCatalog__c>();
        list<RequirementCatalog__c> lstReqCat = new list<RequirementCatalog__c>();
        list<FeatureRequirement__c> lstFeatureReq = new list<FeatureRequirement__c>();
        list<ProgramFeature__c> lstPRGFtr = new list<ProgramFeature__c>();
        list<ProgramRequirement__c> lstPRGReq = new list<ProgramRequirement__c>();
       
        
       // system.runAs(u)
        //{
            
            ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel.recordtypeid = levelRecordType.Id;
            lstPRGLevel.add(newLevel);
            
            ProgramLevel__c newLevel2 = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel2.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel2.recordtypeid = levelRecordType.Id;
            lstPRGLevel.add(newLevel2);
            
            insert lstPRGLevel;
            
            FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
            feature.Enabled__c = 'Contact';
            feature.Visibility__c = 'Contact';
            lstFeatureCat.add(feature);
            insert lstFeatureCat;
            
                                    
            
            ProgramFeature__c newFeature = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature.FeatureStatus__c = Label.CLMAY13PRM09;
            newFeature.recordtypeid = featureRecordType.Id ;
            lstPRGFtr.add(newFeature);
            
            
            
            ProgramFeature__c newFeature2 = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel2.Id, feature.Id);
            newFeature2.FeatureStatus__c = Label.CLMAY13PRM09;
            newFeature2.recordtypeid = featureRecordType.Id;
            lstPRGFtr.add(newFeature2);
            insert lstPRGFtr;
            
            RequirementCatalog__c reqCat = Utils_TestMethods.createRequirementCatalog();
            insert reqCat;
            
            ProgramRequirement__c newReq = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel2.Id, reqCat.Id);
            newReq.Active__c = True;
            insert newReq;
            
            
                       
            myPageCon1.goToCountryPRGEditPage();
            PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country.Id);
            system.debug('countryProgram:'+countryProgram);
            insert countryProgram;
            list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
            countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
            system.debug('countryPartnerProgram234 :'+countryPartnerProgram );
            if(!countryPartnerProgram.isEmpty())
            {
                countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
                update countryPartnerProgram;
            
                list<ProgramLevel__c> lstProgramLevel = [Select id,levelstatus__c from ProgramLevel__c where partnerprogram__c = :globalProgram.Id limit 10];
                list<ProgramLevel__c> lstCountryLevel = new list<ProgramLevel__c>();
                
                for(PRogramLevel__c globalLevel : lstProgramLevel )
                {
                    ProgramLevel__c countryLevel = new ProgramLevel__c();
                    countryLevel.levelstatus__C = Label.CLMAY13PRM19;
                    countryLevel.recordtypeid = countryLevelRecordType.Id;
                    countryLevel.partnerprogram__c = countryPartnerProgram[0].Id;
                    countryLevel.globalprogramlevel__C = globalLevel.Id;
                    lstCountryLevel.add(countryLevel);
                }
                insert lstCountryLevel;
                
                Lead led = new Lead(FirstName = 'test', LastName  ='testLead', Street__c='testStreet', Company = 'testCompany',  Country__c = country.id, Priority__c = 3, Email = 'testLead@accenture.com',  OpportunityType__c = 'Standard',OpportunityScope__c = 'BDPRD--Building Management Products',POBOx__c = 'xxx');
               insert led;
                         
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = countryPartnerProgram[0].country__c;
                insert acc;
                
                
                test.startTest();
                
                acc.PRMAccount__c = true;
                update acc;
				
				System.debug('**** Account is updated ->'+ acc.PRMAccount__c);

                Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
                contact1.PRMContact__c = true;
                insert contact1;
                List<User> lstU = [Select id from User where User.ContactId != null];
                if(lstU.size() > 0)
                  led.ownerId = lstU[0].id;


                FeatureCatalog__c newFeatureCatalog = Utils_TestMethods.createFeatureCatalog();
                insert newFeatureCatalog;

                FeatureRequirement__c newFeatureRequirement = Utils_TestMethods.createFeatureRequirement(newFeatureCatalog.Id , reqCat.Id);
                insert newFeatureRequirement;


                AccountAssignedFeature__c newAccountAssignedFeature = Utils_TestMethods.createAccountFeature(newFeatureCatalog.Id, acc.Id);
                newAccountAssignedFeature.ProgramFeature__c = newFeature2.Id;
                newAccountAssignedFeature.Active__c = True;
                insert newAccountAssignedFeature;

                AccountAssignedRequirement__c newAsgnReq = new AccountAssignedRequirement__c();
                newAsgnReq.ProgramRequirement__c = newReq.id;
                newAsgnReq.RequirementStatus__c = 'Pending';
                newAsgnReq.Account__c = acc.id;
                newAsgnReq.PartnerProgram__c = globalprogram.Id;
                newAsgnReq.FeatureRequirement__c = newFeatureRequirement.Id;
                newAsgnReq.AccountAssignedFeature__c = newAccountAssignedFeature.Id;
                insert newAsgnReq;
                
                newAsgnReq.RequirementStatus__c = 'Expired';
                update newAsgnReq;

                AccountAssignedRequirement__c newAsgnReq1 = new AccountAssignedRequirement__c();
                newAsgnReq1.ProgramRequirement__c = newReq.id;
                newAsgnReq1.RequirementStatus__c = 'Expired';
                newAsgnReq1.Account__c = acc.id;
                newAsgnReq1.PartnerProgram__c = globalprogram.Id;
                newAsgnReq1.FeatureRequirement__c = newFeatureRequirement.Id;
                newAsgnReq1.AccountAssignedFeature__c = newAccountAssignedFeature.Id;
                insert newAsgnReq1;
				
				System.debug('**** partner account details ->'+acc.IsPartner);
				//acc.PRMAccount__c = true;
				acc.IsPartner = true;
                update acc;
				System.debug('**** partner account details after->'+acc.IsPartner);
                
                ACC_PartnerProgram__c accPRogram = Utils_TestMethods.createAccountProgram(lstCountryLevel[0].Id , countryPartnerProgram[0].Id,acc.Id);
                accProgram.Active__c = True;
                accProgram.Eligiblefor__c = 'Promotion';
                try
                {
                    insert accPRogram;
                    accPRogram.Eligiblefor__c = 'Demotion';
                    update accPRogram;
                }
                catch(Exception e){}
                
                


                System.debug('Account Partner Program:'+accPRogram);

                //insert accPRogram;  
                ID levelID = accPRogram.programlevel__c;
                list<contactassignedprogram__c> lstcontact = [Select id, AccountAssignedProgram__c,Contact__c,PartnerProgram__c from ContactAssignedProgram__c where ProgramLevel__c =:levelID ];
      
                list<ProgramFeature__c> lstProgramFeature = [Select id, featurestatus__c,programlevel__c,featurecatalog__c from ProgramFeature__c where partnerprogram__c = :globalProgram.Id limit 10];
                list<ProgramFeature__c> lstCountryProgramFeature = new list<ProgramFeature__c>();
               // for(ProgramFeature__c globalFeature : lstProgramFeature )
               // {
                    ProgramFeature__c countryFeature = Utils_TestMethods.createProgramFeature(countryPartnerProgram[0].Id , lstCountryLevel[0].Id, lstProgramFeature[0].featurecatalog__c);
                    countryFeature.FeatureStatus__C = Label.CLMAY13PRM09;
                    countryFeature.globalprogramfeature__c = lstProgramFeature[0].Id;
                    countryFeature.recordtypeid = countryFeatureRecordType.Id;
                    lstCountryProgramFeature.add(countryFeature);
               // }
               insert lstCountryProgramFeature;                                 
              
               
                List<ContactAssignedFeature__c> lsCAF1 = [Select Id,Contact__C, FeatureCatalog__C, Active__c from ContactAssignedFeature__c where Active__c=true];
                System.debug('lsCAF1 size:'+lsCAF1.size());
                if(lsCAF1.size() > 0){
                    System.debug('Inside If');
                    ApexPages.StandardController stcon = new ApexPages.StandardController(lsCAF1[0]);
                    VFC_ShowRelatedProgramsForFeature vcCOn = new VFC_ShowRelatedProgramsForFeature(stcon);
                }    

                Batch_ProcessAccountFeatures batch_paf = new Batch_ProcessAccountFeatures(); 
        batch_paf.query = 'SELECT id,Account__c, PartnerProgram__c, ProgramLevel__c FROM ACC_PartnerProgram__c WHERE Active__c= true AND LastModifiedDate =Last_N_Days:15 limit 10';
           if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued')] < 2)
          Database.executebatch(batch_paf);

          Batch_ProcessContactFeatures batch_pcf = new Batch_ProcessContactFeatures(); 
        batch_pcf.query = 'SELECT id,Contact__c, PartnerProgram__c, ProgramLevel__c, AccountAssignedProgram__c FROM ContactAssignedProgram__c WHERE Active__c= true AND LastModifiedDate =Last_N_Days:15 limit 10';
           if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued')] < 2)
          Database.executebatch(batch_pcf);

          Batch_ProcessAccountFeatures batch_paf2 = new Batch_ProcessAccountFeatures();
           if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued')] < 2)
          Database.executebatch(batch_paf2);
          
          Batch_ProcessContactFeatures batch_pcf2 = new Batch_ProcessContactFeatures();
           if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued')] < 2) 
        Database.executebatch(batch_pcf2);


        Batch_processPartnerPermissionSets batch_ppsa = new Batch_processPartnerPermissionSets(); 
         // batch_ppsa.query = 'Select Active__c, Contact__c,FeatureCatalog__c, LastModifiedDate from ContactAssignedFeature__c Where LastModifiedDate =Last_N_Days:15';
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued')] < 2)
          Database.executebatch(batch_ppsa);

        Test.stopTest();


           }
       // }
  }
  
  
  
}