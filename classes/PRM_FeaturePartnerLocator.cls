public class PRM_FeaturePartnerLocator implements FieloPRM_FeatureCustomLogic{
     public static String PARTNER_LOCATOR_FEATURE_CODE ='PartnerLocator';

     public void promote(Set<String> PRMUIMSIdList){
        List<Account> accountList = new List<Account>();
        System.debug('PRM_FeaturePartnerLocator promote');
        for(Account account : [select Id,PLPartnerLocatorFeatureGranted__c from Account where Id in (select AccountId from Contact where PRMUIMSId__c in: PRMUIMSIdList and PRMUIMSId__c!=null) ]){
            account.PLPartnerLocatorFeatureGranted__c=true;
            accountList.add(account);
        }
        System.debug('#accountList promote'+accountList);
        update accountList;
     }
    
    public void demote(Set<String> PRMUIMSIdList){
        List<Account> accountList = [select Id,PLPartnerLocatorFeatureGranted__c from Account where Id in (select AccountId from Contact where PRMUIMSId__c in: PRMUIMSIdList and PRMUIMSId__c!=null)];
        Set<Id> accountToNotTouch = new Set<Id>();
        System.debug('#accountList demote accountMap: '+accountList);
        for(FieloPRM_MemberFeature__c feature :[select F_PRM_Member__r.F_Account__c,PRMUIMSId__c from FieloPRM_MemberFeature__c where F_PRM_Feature__r.F_PRM_FeatureCode__c =:PARTNER_LOCATOR_FEATURE_CODE and PRMUIMSId__c not in: PRMUIMSIdList and PRMUIMSId__c!=null and F_PRM_Member__r.F_Account__c in: accountList]){
            accountToNotTouch.add(feature.F_PRM_Member__r.F_Account__c);
        }
        System.debug('#accountList demote to not touch: '+accountToNotTouch);
        List<Account> accountToUpdate = new List<Account>();
        for(Account account : accountList){
            if(!accountToNotTouch.contains(account.Id)){
                account.PLPartnerLocatorFeatureGranted__c=false;
                accountToUpdate.add(account);
            }
        }
        System.debug('#accountList demote'+accountToUpdate);
        update accountToUpdate;
    }
}