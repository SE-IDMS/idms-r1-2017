global Class CaseArchiving_Batch implements Database.batchable<Sobject>{
  
    LIST<CS_ForFieldMapping__c> lstCseToArchive = CS_ForFieldMapping__c.getall().values();
    LIST<CS_NewFieldMapping__c> lstNewCseToArchive = CS_NewFieldMapping__c.getall().values();
    global Database.QueryLocator start (Database.BatchableContext bc){
        String sqlQuery;
        String sqlHeader ='';
        for(CS_ForFieldMapping__c obj :lstCseToArchive){
            if(obj.SourceField__c!=Null&& obj.Sourceobject__c=='Case' && !(obj.InActive__c==true)){   
                if(sqlHeader!='')
                    sqlHeader+=',';
                sqlHeader+=obj.SourceField__c;
            }           
        }
        String strYear=System.Label.CLJUL14CCC02;
        integer intClsDate = Integer.Valueof(strYear);
        DateTime clsDate =System.today().addYears(intClsDate);  
        System.debug(clsDate);
        String strYearNew=System.Label.CLJUL14CCC03;
        integer intClsDateNew = Integer.Valueof(strYearNew);
        DateTime clsDateNew =System.today().addYears(intClsDateNew);  
        System.debug(clsDateNew);
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC04);
        //String size=string.valueof(System.Label.CLJUL14CCC04);
               
        if(Test.isRunningTest()){
             clsDate =System.today();
             System.debug(clsDate);
             sqlQuery='Select CCCLocation__r.Name,AdvancedLastCaseTeam__r.Name,CreatedByTeam__r.Name,ExpertLastCaseTeam__r.Name,AdvancedLeadCaseTeam__r.Name,ExpertLeadCaseTeam__r.Name,Team__r.name,PrimaryLeadCaseTeam__r.name,PrimaryLastCaseTeam__r.name,toLabel(Case_BU__c),toLabel(ActualTechnicalLevelofCase__c),'+sqlHeader+' from Case Where ClosedDate>:clsDate ';
        }
        else{
            if(size==1)
                sqlQuery='Select CCCLocation__r.Name,AdvancedLastCaseTeam__r.Name,CreatedByTeam__r.Name,ExpertLastCaseTeam__r.Name,AdvancedLeadCaseTeam__r.Name,ExpertLeadCaseTeam__r.Name,Team__r.Name,PrimaryLeadCaseTeam__r.name,PrimaryLastCaseTeam__r.name,toLabel(Case_BU__c),toLabel(ActualTechnicalLevelofCase__c),'+sqlHeader+' from Case Where caseNumber ='+System.label.CLJUL14CCC07+ 'and Status='+System.label.CLJUL14CCC01 + ' and ArchivedStatus__c!='+System.label.CLJUL14CCC09+' and ArchivedStatus__c!='+System.label.CLJUL14CCC10+' AND archivedStatus__c!='+System.label.CLJUL14CCC05 +' AND ArchivedStatus__c!='+ System.label.CLJUN16CCC09 +' AND ArchivedStatus__c!='+ System.label.CLJUN16CCC07;
            else
                sqlQuery='Select CCCLocation__r.Name,AdvancedLastCaseTeam__r.Name,CreatedByTeam__r.Name,ExpertLastCaseTeam__r.Name,AdvancedLeadCaseTeam__r.Name,ExpertLeadCaseTeam__r.Name,Team__r.name,PrimaryLeadCaseTeam__r.name,PrimaryLastCaseTeam__r.name,toLabel(Case_BU__c),toLabel(ActualTechnicalLevelofCase__c),'+sqlHeader+' from Case Where Status='+System.label.CLJUL14CCC01+ ' and ArchivedStatus__c!='+System.label.CLJUL14CCC09+' and ArchivedStatus__c!='+System.label.CLJUL14CCC10+' AND ArchivedStatus__c!='+ System.label.CLJUL14CCC05 +' AND ArchivedStatus__c!='+ System.label.CLJUN16CCC07 +' AND ArchivedStatus__c!='+ System.label.CLJUN16CCC09 +' and ClosedDate<=:clsDate limit ' + size;
 //sqlQuery='Select AdvancedLastCaseTeam__r.Name,CreatedByTeam__r.Name,ExpertLastCaseTeam__r.Name,AdvancedLeadCaseTeam__r.Name,ExpertLeadCaseTeam__r.Name,Team__r.name,PrimaryLeadCaseTeam__r.name,PrimaryLastCaseTeam__r.name,toLabel(Case_BU__c),toLabel(ActualTechnicalLevelofCase__c),'+sqlHeader+' from Case Where Status='+System.label.CLJUL14CCC01+ ' and ArchivedStatus__c!='+System.label.CLJUL14CCC09+' and ArchivedStatus__c!='+System.label.CLJUL14CCC10+' AND ArchivedStatus__c!='+ System.label.CLJUL14CCC05 +' and ClosedDate<=:clsDate limit ' + size; 
         //    sqlQuery='Select '+sqlHeader+' from Case Where ClosedDate<=:clsDate and RelatedSupportcase__c=null and Status=:'+System.label.CLJUL14CCC01+ ' limit ' +  size;  
         //    sqlQuery='Select '+sqlHeader+' from Case Where caseNumber =\'00930975\' and Status='+System.label.CLJUL14CCC01;
        
           
        } 
     
        System.debug('\n query: '+sqlQuery); 
        return  Database.getQueryLocator(sqlQuery);
    }   
    global void execute(Database.BatchableContext Bc,List<Sobject> Scope){
       
        List<ArchiveCase__c> lstArchiveCases= new List<ArchiveCase__c>();
        List<ArchiveLog__c> lstArchiveLog = new List<ArchiveLog__c>();
        Set<id> cseIdSet = new Set<id>();
        Set<id> cseIdSetDup = new Set<id>();
        Set<id> cseIdErrorSet = new Set<id>();
        Set<id> cseOwnerIdSet = new Set<id>();
        set<ID> parentIdSet = new set<Id>();
        Set<ID> ChilIdSet = new Set<ID>();
        Map<String, Case> cseNumRecMap = new Map<String, Case>();           
        Map<String, ArchiveCase__c> aCseNameRecMap = new Map<String, ArchiveCase__c>();
        Map<id,ArchiveCase__C> aCseIdRecMap = new Map<id,ArchiveCase__C>();  
        Map<id, User> UserMap = new Map<id, User>();
        Map<Id,case> cseIdRecMap = new Map<Id,case>();
                      
        User archiveUser = [select id, name from user where username =: System.label.CLJUN16CCC05 limit 1];
        
        for(sObject sObj: Scope){
            Case caseObj= (Case)sObj;
            cseOwnerIdSet.add(caseObj.OwnerId);
        }
        if(cseOwnerIdSet != null && cseOwnerIdSet.size()>0){
            List<User> lstUsers= [Select id,name,IsActive from user where id = : cseOwnerIdSet];
            UserMap.putALL(lstUsers);
        }
       
       //Moving Case Records to ArchiveCase
        for(sObject genralisedSObj: Scope){
            sObject sObj = genralisedSObj;
            System.debug('genralisedSObj'+genralisedSObj);
            Case cseObj = (Case)genralisedSObj;
            cseNumRecMap.put(cseObj.CaseNumber,cseObj);
            cseIdRecMap.put(cseObj.id,cseObj);
            cseIdSet.add(cseObj.id);
            System.debug('cseIdSetsize'+cseIdSet.size());
            System.debug('cseIdSet'+cseIdSet);
            ArchiveCase__c archObj = new ArchiveCase__c();       
            sObject tObj = (sObject)archObj;
            for(CS_ForFieldMapping__c obj : lstCseToArchive ){
                if(obj.SourceField__c !=null && obj.TargetField__c!=null && !(obj.InActive__c==true) && obj.Sourceobject__c=='Case' && obj.TargetObject__c=='ArchiveCase__c'){
                    tObj.put(obj.TargetField__c , sObj.get(obj.SourceField__c));
                }
            } 
            tObj.put('PrimaryLeadCaseTeam__c' ,cseObj.PrimaryLeadCaseTeam__r.name);
            tObj.put('PrimaryLastCaseTeam__c',cseObj.PrimaryLastCaseTeam__r.name);
            tObj.put('AdvancedLastCaseTeam__c',cseObj.AdvancedLastCaseTeam__r.name);
            tObj.put('CreatedByTeam__c',cseObj.CreatedByTeam__r.name);
            tObj.put('Team__c',cseObj.Team__r.name);
            tObj.put('ExpertLastCaseTeam__c',cseObj.ExpertLastCaseTeam__r.name);
            tObj.put('AdvancedLeadCaseTeam__c',cseObj.AdvancedLeadCaseTeam__r.name);
            tObj.put('ExpertLeadCaseTeam__c',cseObj.ExpertLeadCaseTeam__r.name);
            tObj.put('CCCLocation__c',cseObj.CCCLocation__r.Name);
            tObj.put('Case_BU__c',cseObj.Case_BU__c);
            tObj.put('ActualTechnicalLevelofCase__c',cseObj.ActualTechnicalLevelofCase__c);
            
            
            if(UserMap.keySet().contains(cseObj.OwnerId)){
               // User userobj= [select id,name from user where id=:cseObj.OwnerId];
               // tObj.put('PreviousOwner__c',userobj.name);
               //tObj.put('OwnerId' , archiveUser.id);
                tObj.put('PreviousOwner__c',UserMap.get(cseObj.OwnerId).name);
                tObj.put('OwnerId' , archiveUser.id);
            }
            
                lstArchiveCases.add((ArchiveCase__c)tObj);
        }           
        cseIdSetDup.addAll(cseIdSet);
        if(lstArchiveCases!= null && lstArchiveCases.size()>0){
           Database.SaveResult[] sresults  =  Database.insert(lstArchiveCases, false);
            for(Integer k=0;k<sresults.size();k++ ){
                Database.SaveResult sr =sresults[k];
                if(sr.isSuccess()&&!Test.isrunningTest())
                    System.debug('Successfully inserted ID: ' + sr.getId());
                else{
                    String str = '';
                    ArchiveLog__c aLogObj= new ArchiveLog__c();
                    String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                    String idstr=cseNumRecMap.get(lstArchiveCases[k].Name).id;
                    String RecordURL =sfdcBaseURL + '/' +idstr;
                    for(Database.Error err : sr.getErrors()){
                        str =str+err.getMessage();
                    }
                    aLogObj.ErrorMessage__c=str;
                    aLogObj.Date__c=System.now();
                    aLogObj.RecordURL__c=RecordURL;
                    aLogObj.FailedRecordName__c=lstArchiveCases[k].Name;
                    lstArchiveLog.add(aLogObj);
                    cseIdErrorSet.add(idstr);
                }
            }
            cseIdSet.removeAll(cseIdErrorSet);
          if(cseIdErrorSet!=null && cseIdErrorSet.size()>0){
               List<Case> lstFailedCases= [select id,casenumber,ArchivedStatus__c from case where id in :cseIdErrorSet];
                for(Case cobj:lstFailedCases)
                   cobj.ArchivedStatus__c ='Failed';
        
                Database.SaveResult[] sresultsofcses =  Database.update(lstFailedCases, false); 
                for(Integer k=0;k<sresultsofcses.size();k++ ){
                    Database.SaveResult sr =sresultsofcses[k];
                    if(sr.isSuccess()&&!Test.isrunningTest())
                        System.debug('Successfully inserted ID: ' + sr.getId());
                    else{
                        String str = '';
                        ArchiveLog__c aLogObj= new ArchiveLog__c();
                        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                        String idstr=cseNumRecMap.get(lstArchiveCases[k].Name).id;
                        String RecordURL =sfdcBaseURL + '/' +idstr;
                        for(Database.Error err : sr.getErrors()){
                            str =str+err.getMessage();
                        }
                        aLogObj.ErrorMessage__c=str;
                        aLogObj.Date__c=System.now();
                        aLogObj.RecordURL__c=RecordURL;
                        aLogObj.FailedRecordName__c=lstArchiveCases[k].Name;
                        lstArchiveLog.add(aLogObj);
                    }
                }
            }
        }
         if(lstArchiveLog!=null && lstArchiveLog.size()>0)
                insert  lstArchiveLog;
        for(ArchiveCase__c ac:lstArchiveCases){
            aCseNameRecMap.put(ac.Name,ac);
            acseIdRecMap.put(ac.id,ac);
        }
        System.debug('cseIdSet'+cseIdSet );
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
                    
            System.debug('cseIdSet'+cseIdSet );
        
            
        // Processing Rest of Archive fields
        Set<id> failedCseidsOfArc= CaseArchiving_Handler.processArchiveCase(cseIdSet,aCseNameRecMap,lstNewCseToArchive);
            if(failedCseidsOfArc!= null && failedCseidsOfArc.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfArc];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] ArcSresults=  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfArc); 
            }  
        //inorder to increase the coverage added this blcok of code
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);    
            
        // Processing CCCActions
        Set<id> failedCseidsOfCCC= CaseArchiving_Handler.processCCCActions(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfCCC != null && failedCseidsOfCCC.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCCC];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] CCCSresults=  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfCCC); 
            }  
        //inorder to increase the coverage added this blcok of code
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
               
       //Processing InternalComments     
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> idsOfFailedcasesInL2L3 = CaseArchiving_Handler.processCSEInternalComments(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(idsOfFailedcasesInL2L3 != null && idsOfFailedcasesInL2L3.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :idsOfFailedcasesInL2L3];
                    for(Case cseobj:lstFailedCases)
                        cseobj.ArchivedStatus__c ='Failed';
            
                    Database.SaveResult[] internalCommentsSresults=Database.update(lstFailedCases, false);
                    cseIdSet.removeAll(idsOfFailedcasesInL2L3);
            }
        }  
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
            
        
        //Processing CustomerSafetyIssue
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfCSI= CaseArchiving_Handler.processSafetyIssueLists(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfCSI != null && failedCseidsOfCSI.size()>0 ){
            List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCSI];
            for(Case cobj:lstFailedCases)
                cobj.ArchivedStatus__c = 'Failed';
                                                       
                Database.SaveResult[] CSISresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfCSI);
            }
        }  
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
            
        //Processing ISR
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfISR = CaseArchiving_Handler.processISR(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfISR != null && failedCseidsOfISR.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfISR ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfISR );
            }
        }   

        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);    
            
          //Processing OLHMails
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsofMail= CaseArchiving_Handler.processOLHMails(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsofMail != null && failedCseidsofMail.size()>0 ){
            List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsofMail];
            for(Case cobj:lstFailedCases)
                cobj.ArchivedStatus__c = 'Failed';
                                                       
                Database.SaveResult[] CSISresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsofMail);
            }
        }     
        
        
        
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
                 
        //Processing ExpertInternalComment
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfEIC= CaseArchiving_Handler.processExpertInternalComments(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfEIC != null && failedCseidsOfEIC.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfEIC];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                    Database.SaveResult[] EICSresults  =  Database.update(lstFailedCases, false);
                    cseIdSet.removeAll(failedCseidsOfEIC);
            }
        }
              
           if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
               
       //Processing LiveChatTranscripts     
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> idsOfFailedTranscripts = CaseArchiving_Handler.processLiveChatTranscripts(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(idsOfFailedTranscripts != null && idsOfFailedTranscripts.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :idsOfFailedTranscripts];
                    for(Case cseobj:lstFailedCases)
                        cseobj.ArchivedStatus__c ='Failed';
            
                    Database.SaveResult[] OLHChatresults=Database.update(lstFailedCases, false);
                    cseIdSet.removeAll(idsOfFailedTranscripts);
            }
        }        
              
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        
        //Processing ServiceMaintainHistory
        if(cseIdSet != null && cseIdSet.size()>0){
        Set<id> failedCseidsOfSMH =CaseArchiving_Handler.processServiceMaintainenceHistory(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfSMH != null && failedCseidsOfSMH.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfSMH];
                    for(Case cobj:lstFailedCases)
                        cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresultsSMH =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfSMH);
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing PartsOrders
        if(cseIdSet != null && cseIdSet.size()>0){
        Set<id> failedCseidsOfParts =CaseArchiving_Handler.processPartsOrders(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfParts != null && failedCseidsOfParts.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfParts];
                    for(Case cobj:lstFailedCases)
                        cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] PartOrdersresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfParts);
            }
        }
               
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing WorkOrders
        if(cseIdSet != null && cseIdSet.size()>0){
        Set<id> failedCseidsOfWorks =CaseArchiving_Handler.processWorkOrders(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfWorks != null && failedCseidsOfWorks.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfWorks];
                    for(Case cobj:lstFailedCases)
                        cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfWorks);
            }
        }
                
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing ITBAsset
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfITB = CaseArchiving_Handler.processITBAssets(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfITB != null && failedCseidsOfITB.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfITB ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfITB );
            }
        }   

        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing LiteratureRequestList
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfLitReq = CaseArchiving_Handler.processLiteratureRequest(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfLitReq != null && failedCseidsOfLitReq.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfLitReq];
                for(Case cobj:lstFailedCases)
                cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                 cseIdSet.removeAll(failedCseidsOfLitReq);
            }
        }   
        
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing ComplaintRequestList
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfCompReq = CaseArchiving_Handler.processComplaintRequests(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfCompReq != null && failedCseidsOfCompReq.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCompReq];
                for(Case cobj:lstFailedCases)
                cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                 cseIdSet.removeAll(failedCseidsOfCompReq);
            }
        }   
        
           if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
               
       //Processing OLHChats     
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> idsOfFailedcasesOLH = CaseArchiving_Handler.processOLH_Chat(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(idsOfFailedcasesOLH != null && idsOfFailedcasesOLH.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :idsOfFailedcasesOLH];
                    for(Case cseobj:lstFailedCases)
                        cseobj.ArchivedStatus__c ='Failed';
            
                    Database.SaveResult[] OLHChatresults=Database.update(lstFailedCases, false);
                    cseIdSet.removeAll(idsOfFailedcasesOLH);
            }
        }  
        
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing Opportunity
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfOpp= CaseArchiving_Handler.processOpportunities(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfOpp != null && failedCseidsOfOpp.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfOpp];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                    Database.SaveResult[] sresults =  Database.update(lstFailedCases, false);
                    cseIdSet.removeAll(failedCseidsOfOpp);
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing opportunityNotifications
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfOppNot = CaseArchiving_Handler.processOpportunityNotificationList(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfOppNot != null && failedCseidsOfOppNot.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfOppNot];
                    for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults10  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfOppNot);
            }
        }   
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing TEX
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfTEX = CaseArchiving_Handler.processTEX(cseIdSet,aCseNameRecMap,cseIdRecMap );
            if(failedCseidsOfTEX != null && failedCseidsOfTEX.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfTEX];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfTEX);
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing WorkOrderNotifications
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfWON = CaseArchiving_Handler.processWorkOrderNotifications(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfWON != null && failedCseidsOfWON.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfWON];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfWON);
            }
        }
         
       if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing CaseStages
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfCseStge = CaseArchiving_Handler.processCaseStages(cseIdSet,lstCseToArchive,aCseNameRecMap,cseNumRecMap,aCseIdRecMap,cseIdRecMap);
            if(failedCseidsOfCseStge != null && failedCseidsOfCseStge.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCseStge];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                    
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfCseStge);
            }
        }
               
       if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing RMA
        if(cseIdSet != null && cseIdSet.size()>0){
            Set<id> failedCseidsOfRMA = CaseArchiving_Handler.processRMA(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfRMA != null && failedCseidsOfRMA.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfRMA];
                for(Case cobj:lstFailedCases)
                cobj.ArchivedStatus__c = 'Failed';
            
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfRMA);
            }
        } 
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing processBRECseLink
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfBRE = CaseArchiving_Handler.processBRECseLink(cseIdSet,aCseNameRecMap,cseNumRecMap,aCseIdRecMap,cseIdRecMap);
            if(failedCseidsOfBRE != null && failedCseidsOfBRE.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfBRE];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfBRE);
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing processProblemCaseLink
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfProblem = CaseArchiving_Handler.processProblemCaseLink(cseIdSet,aCseNameRecMap,cseNumRecMap,aCseIdRecMap,cseIdRecMap);
            if(failedCseidsOfProblem != null && failedCseidsOfProblem.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfProblem];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfProblem);
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
            /*
        //Processing CSE_Externalreference
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfExtRefer = CaseArchiving_Handler.processExternalreferences(cseIdSet,aCseNameRecMap,cseIdRecMap);
            if(failedCseidsOfExtRefer != null && failedCseidsOfExtRefer.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfExtRefer ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
            
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfExtRefer );
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
            */
            //Processing CSE_Externalreference
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfExtRefer = CaseArchiving_Handler.processExternalreferences(cseIdSet,lstCseToArchive ,aCseNameRecMap,cseIdRecMap,cseNumRecMap,aCseIdRecMap);
            if(failedCseidsOfExtRefer != null && failedCseidsOfExtRefer.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfExtRefer ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
            
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfExtRefer );
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing InquiraFAQ
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfInquira = CaseArchiving_Handler.processInquiraFaqs(cseIdSet,lstCseToArchive ,aCseNameRecMap,cseNumRecMap,aCseIdRecMap,cseIdRecMap );
            if(failedCseidsOfInquira != null && failedCseidsOfInquira.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfInquira ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
        
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfInquira );
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing CaseComments  
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfCseComments = CaseArchiving_Handler.processCseComments(cseIdSet,lstCseToArchive ,aCseNameRecMap,cseIdRecMap,cseNumRecMap,aCseIdRecMap);
        
            if(failedCseidsOfCseComments != null && failedCseidsOfCseComments .size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCseComments ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
            
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfCseComments );
            }
        }
        
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing CaseStakeHolders
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfCseStkHldr = CaseArchiving_Handler.processCaseStakeHolders(cseIdSet,aCseNameRecMap,cseIdRecMap);
        
            if(failedCseidsOfCseStkHldr != null && failedCseidsOfCseStkHldr .size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCseStkHldr ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
            
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfCseStkHldr );
            }
        }
        
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing processCaseRelProducts
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfCseRelProducts = CaseArchiving_Handler.processCaseRelProducts(cseIdSet,aCseNameRecMap,cseIdRecMap);
        
            if(failedCseidsOfCseRelProducts != null && failedCseidsOfCseRelProducts .size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfCseRelProducts ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
            
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfCseRelProducts );
            }
        }
        
        
        
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
         //Processing ArchivedInstalledProducts
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfIP = CaseArchiving_Handler.processInstalledProducts(cseIdSet,lstCseToArchive ,aCseNameRecMap,cseNumRecMap,aCseIdRecMap,cseIdRecMap);
            if(failedCseidsOfIP != null && failedCseidsOfIP .size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfIP ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfIP);
            }
        }
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
        //Processing Email Messages   
        if(cseIdSet!=null && cseIdSet.size()>0){
            Set<id> failedCseidsOfEmailMsg = CaseArchiving_Handler.processEmailMsgs(cseIdSet,lstCseToArchive ,aCseNameRecMap,cseIdRecMap,cseNumRecMap,aCseIdRecMap);
            if(failedCseidsOfEmailMsg!= null && failedCseidsOfEmailMsg.size()>0 ){
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :failedCseidsOfEmailMsg ];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
                
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
                cseIdSet.removeAll(failedCseidsOfEmailMsg );
            }
        }
     
        if(Test.isRunningTest())
            cseIdSet.addAll(cseIdSetDup);
    // Setting the Archivedstatus and ArchivedDate field values of the remaining cases in caseIdSet to Success
        
        if(cseIdSet!=null && cseIdSet.size()>0){
            List<Attachment> lstAttachments = [select id,ParentID from attachment where ParentID in :cseIdSet];
            List<Case>  lstChildCases=[Select id,RelatedSupportcase__c from Case Where Id in :cseIdSet and RelatedSupportcase__c!=Null];
            for(Attachment Attachobj : lstAttachments){
                parentIdSet.add(Attachobj.ParentID);
            }
            for(Case caseObj:lstChildCases){
                ChilIdSet.add(caseObj.id);
            }
            
        }   
        if(cseIdSet != null && cseIdSet.size()>0){
            List<case> caseList=[select id,CaseNumber,Archivedstatus__c,ArchivedDate__c from case where id  in : cseIdSet];
            for(case cObj:caseList){
                
                if(ChilIdSet.contains(cobj.id)){
                        cobj.ArchivedStatus__c='Has Parent Case';   
                }
                else if(parentIdSet.contains(cobj.id)){
                    cobj.ArchivedStatus__c='Has Attachment';   
                }
                else{
                    cObj.ArchivedStatus__c='success';
                    cobj.ArchivedDate__c=system.now();
                }
            }   
            if(caseList!=null && caseList.size()>0)
            Database.SaveResult[] sresults  =  Database.update(caseList, false);
        }
    }
    global void finish(Database.BatchableContext Bc){
        
    }
}