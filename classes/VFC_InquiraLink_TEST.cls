@isTest
private With sharing class VFC_InquiraLink_TEST{
    
    static testMethod void InquiraLinkTestMethod(){          
    
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account accounts = Utils_TestMethods.createAccount();
        accounts.country__c=country.id;
        insert accounts;
        
        contact contact =Utils_TestMethods.createContact(accounts.Id,'TestContact');
        contact.country__c=country.id;
        contact.CorrespLang__c='french';
        database.insert(contact);           
    
        Case cases =Utils_TestMethods.createCase(accounts.id,contact.id,'closed');    
        cases.answerToCustomer__c='please close the case'; 
        cases.Family__c='Test Family';
        cases.CommercialReference__c='TestCommercial Reference';    
        database.insert(cases);
        
        InquiraFAQ__c objInquira = new  InquiraFAQ__c();
        //objInquira.Case__c=cases.id;
        objInquira.URL__c = 'www.schneider-electric.com';
        objInquira.InfoCenterURL__c  = 'www.schneider-electric.com';
        //insert objInquira;
        
        VFC_InquiraLink.InquiraResultWarpper objInquiraResultWarpper = new VFC_InquiraLink.InquiraResultWarpper();
        
                
        objInquiraResultWarpper.IsSelected = true;
        objInquiraResultWarpper.objInquiraFAQ = objInquira;
       
             
        String strWrapper = JSON.serialize(objInquiraResultWarpper);
        List<String> wrapperList = new List<String>();
        wrapperList.add(strWrapper);
        
       
        
        ApexPages.StandardController sc1= new ApexPages.StandardController(cases);         
        VFC_InquiraLink InquiraObj =new VFC_InquiraLink(sc1);
        VFC_InquiraLink.getContactCorspLanguage();       
        VFC_InquiraLink.getInquiraCountry(cases.casenumber);
        VFC_InquiraLink.getBUEntities();
        VFC_InquiraLink.checkExistingFAQ(cases.id,'faq123');
        VFC_InquiraLink.attachFAQ(cases.id,wrapperList);
        VFC_InquiraLink.performInquiraSearch(cases.id,'test','US','en','testEntity');
    }
}