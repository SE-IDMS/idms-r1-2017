@isTest
private class VFC92_RLMassEditController_TEST{
     static TestMethod void RLMassEditControllerTEST(){    
         Account account1 = Utils_TestMethods.createAccount();
         insert account1;
         Opportunity TestOpp = Utils_TestMethods.createOpenOpportunity(account1.Id);
         insert TestOpp;
         Revenue_Line__c rl = new revenue_Line__c();
         rl.opportunity__c = TestOpp.id;
         insert rl;
                 
         List<Revenue_Line__c> TestRLList = new List<Revenue_Line__c>();
         TestRLList.add(rl);
         ApexPages.StandardSetController RLSetController = new ApexPages.StandardSetController(TestRLList);
         RLSetController.setSelected(TestRLList);
         VFC92_RLMassEditController VFController = new VFC92_RLMassEditController(RLSetController);
         VFController.CustomSave();
         
         // Profile profile = [select id from profile where name='SE - Salesperson'];        
         // User user = new User(alias = 'user', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
         // lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
         // BypassWF__c = false, timezonesidkey='Europe/London', username='user' + '@bridge-fo.com'); 
         // insert user;
         User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Salesperson');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }

        AccountShare aShare = new AccountShare();
        aShare.accountId= account1.Id;
        aShare.UserOrGroupId = user.Id;
        aShare.accountAccessLevel = 'edit';
        aShare.OpportunityAccessLevel = 'read';
        aShare.CaseAccessLevel = 'edit';
        insert aShare; 
        
         system.runAs(user){ 
             VFController.CustomSave();
         }
     }
}