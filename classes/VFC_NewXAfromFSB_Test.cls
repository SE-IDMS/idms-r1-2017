@isTest
private class VFC_NewXAfromFSB_Test {
    
    static testMethod void Method1() {
    
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
        
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                    accOrg1.Name='Test';
                    accOrg1.Entity__c='Test Entity-2'; 
                    insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';
            prob.RecordTypeid = Label.CLI2PAPR120014;
            insert prob;
            
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Status__c = '1. New';
            FSB.Description__c = 'Test Description';
            FSB.Problem__c = prob.Id;
            FSB.TypeofDocumenttoGenerate__c = 'FSB';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB; 
            
            List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
            
            ContainmentAction__c testConAction1 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
            testConAction1.FieldServiceBulletin__c = FSB.Id;
            testConAction1.Status__c = 'Executing';
            lstXA.add(testConAction1);
            insert lstXA;  
            
            ApexPages.StandardSetController setController = new ApexPages.StandardSetController(lstXA);
            PageReference pageRef = Page.VFP_NewXAfromFSB; 
            ApexPages.currentPage().getParameters().put('id', FSB.id);
            VFC_NewXAfromFSB newXA = new VFC_NewXAfromFSB(setController);
            
            newXA.goToEditPage();
        }    
    }
}