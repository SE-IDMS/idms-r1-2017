/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description:  This class implements queueable interface which calls UIMS unit APIs to create User and Company in UIMS. 
*                 Creates Users with both identity types Mobile and Email in UIMS.
*                 It captures the exceptions while creating User and Company and send it for reconciliation batch.
**/
public class IDMSCreateUIMSUser implements Queueable,Database.AllowsCallouts{
    
    User UserRecord;
    IDMSCaptureError Errorcls;
    
    public IDMSCreateUIMSUser(USer UserRecord) {
        this.UserRecord= UserRecord;
        system.debug('enteing create user uims-->');
        system.debug('---id--'+this.UserRecord);
    }
    //This method executes to create User with identity type Mobile and Email in UIMS and captures UIMS Unit API responses.  
    public void execute(QueueableContext context) {       
        system.debug('--u--'+UserRecord);  
        system.debug('Entering queable');
        try{
            //Stub Class object
            WS_IdmsUimsAuthUserManagerV2Impl.AuthenticatedUserManager_UIMSV2_ImplPort  cls  = new WS_IdmsUimsAuthUserManagerV2Impl.AuthenticatedUserManager_UIMSV2_ImplPort();       
            WS_IdmsUimsAuthUserManagerV2Service.accessElement application                   = new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            application.type_x                                                              = System.label.CLJUN16IDMS70;
            application.id                                                                  = UserRecord.IDMS_Registration_Source__c; // As per discussion on May17
            cls.clientCertName_x                                                            = System.Label.CLJUN16IDMS103;
            string callerFid                                                                = LAbel.CLJUN16IDMS104;
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort clsPhoneId=  new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            clsPhoneId.endpoint_x                                                           = label.CLQ316IDMS129;
            WS_uimsv22ServiceImsSchneider.accessElement applicationPhone                    = new WS_uimsv22ServiceImsSchneider.accessElement ();
            applicationPhone.type_x                                                         = System.label.CLJUN16IDMS70;
            applicationPhone.id                                                             = UserRecord.IDMS_Registration_Source__c;
            clsPhoneId.clientCertName_x                                                     = System.Label.CLJUN16IDMS103;
            WS_uimsv22ServiceImsSchneider.userV6 identityPhone                              = new WS_uimsv22ServiceImsSchneider.userV6();
            WS_IdmsUimsAuthUserManagerV2Service.userV5 identity                             = new WS_IdmsUimsAuthUserManagerV2Service.userV5();
            
            if(UserRecord.IDMSIdentityType__c.equals('Mobile')){
                identityPhone = createIdentityPhone(userRecord);
            }
            else{
                identity = createIdentity(UserRecord);
            }
            
            WS_IdmsUimsAuthUserManagerV2Service.createdIdentityReport resp ;
            WS_uimsv22ServiceImsSchneider.createdIdentityReport respPhone;
            if(UserRecord.IDMSIdentityType__c.equals('Mobile'))
            {
                system.debug('in createIdentityPhone');
                respPhone = clsPhoneId.createIdentityWithPhoneId(callerFid ,identityPhone , applicationPhone);
                system.debug(respPhone);
                
                //reconciliation code starts
                if(String.isEmpty(respPhone.federatedID)){
                    //call future method to insert failed user
                    IDMSUserReconHandlerClass.userIdentityPhoneToRecon(respPhone.errorMessage, UserRecord.id);
                }
                //reconciliation code ends
            }else{
                if(!Test.isrunningTest()){ 
                    resp = cls.createIdentity(callerFid ,identity, application);
                    system.debug('response:'+resp+'bfo User id:'+UserRecord.id);
                    
                    //reconciliation code starts
                    if(String.isEmpty(resp.federatedID)){
                        system.debug('Inside federation id check:');
                        //call future method to insert failed user
                        IDMSUserReconHandlerClass.userIdenEmailToRecon(resp.errorMessage, UserRecord.id);
                    }}
                //reconciliation code ends
            }
            
            system.debug('response uims-->' + resp + respPhone ) ; 
            system.debug('--res ---'+resp + respPhone  );
            
            string createComRes = '';
            
            if(userRecord.IDMS_User_Context__c.equalsignorecase('Work') || userRecord.IDMS_User_Context__c.equalsignorecase('@Work')){
                system.debug('entering companyname-->' ) ;
                if(resp != null && userRecord.CompanyName!=null && userRecord.CompanyName!=''){
                    System.debug('inside uims create company');
                    createComRes = createCompany(userrecord,resp.federatedID);
                }
                if(respPhone != null && userRecord.CompanyName!=null && userRecord.CompanyName!=''){
                    createComRes = createCompany(userrecord,respPhone.federatedID);
                }
                
                //if create is true
                if(string.isnotblank(createComRes)){         
                    userrecord.IDMS_VC_OLD__c = '1';
                }
                
                //batch reconciliation code starts
                if(String.isEmpty(createComRes)){
                    String responseValue = Label.CLNOV16IDMS061;
                    //call future method to insert failed user
                    IDMSCompanyReconHandlerClass.idmsCompanyToRecon(responseValue, UserRecord.id);
                }
            }
            system.debug('createComRes-->' + createComRes) ; 
            // Code to update FedId in sfdc - Start
            
            if(resp != null && String.isnotBlank(resp.federatedID)){
                system.debug('resp.federatedID; '+resp.federatedID);
                UserRecord.FederationIdentifier                     = resp.federatedID;
                userrecord.IDMS_VU_OLD__c                           ='1';
                if(createComRes != null && string.isnotblank(createComRes)){
                    UserRecord.IDMSCompanyFederationIdentifier__c   = createComRes;
                    userrecord.IDMS_VC_OLD__c                       = '1';
                }
                Database.update(UserRecord,false);
            }   
            else
            {
                system.debug('respPhone.federatedID: '+respPhone.federatedID);  
                UserRecord.FederationIdentifier                     = respPhone.federatedID; 
                userrecord.IDMS_VU_OLD__c                           = '1';
                if(createComRes != null && string.isnotblank(createComRes)){
                    UserRecord.IDMSCompanyFederationIdentifier__c   = createComRes;
                    userrecord.IDMS_VC_OLD__c                       = '1';
                }
                system.debug('user record: '+UserRecord);
                Database.update(UserRecord,false);
                system.debug('update ok');
            }
            
        }
        catch(Exception e){
            Errorcls= new IDMSCaptureError();
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            
            if(UserRecord.IDMSCompanyFederationIdentifier__c == null){
                //catch callout exception 
                IDMSCompanyReconHandlerClass.idmsCompanyToRecon(ExceptionMessage, UserRecord.id);
            }
            
            //catch callout exception and create log.
            if(UserRecord.IDMSIdentityType__c.equals('Mobile')){
                IDMSUserReconHandlerClass.userIdentityPhoneToRecon(ExceptionMessage, UserRecord.id);
            }else {
                IDMSUserReconHandlerClass.userIdenEmailToRecon(ExceptionMessage, UserRecord.id);
            }    
            
            if(UserRecord != null && UserRecord.IDMS_Registration_Source__c != null && UserRecord.IDMS_Registration_Source__c.equalsignorecase(Label.CLJUN16IDMS18)){
                Errorcls.IDMScreateLog(null,Label.CLJUN16IDMS73,Label.CLJUN16IDMS116,UserRecord.username ,ExceptionMessage ,UserRecord.IDMS_Registration_Source__c,Datetime.now(),UserRecord.IDMS_User_Context__c,null,false,null,false,Label.CLJUN16IDMS117);
            } 
            else{
                Errorcls.IDMScreateLog(Label.CLJUN16IDMS74,Label.CLJUN16IDMS73,Label.CLJUN16IDMS116,UserRecord.username ,ExceptionMessage ,UserRecord.IDMS_Registration_Source__c,Datetime.now(),UserRecord.IDMS_User_Context__c,null,false,null,false,Label.CLJUN16IDMS117);           
            }
            
        }
    }
    
    //This method calls UIMS Unit API to create Company in UIMS and returns response.
    public string createCompany(user userrecord,string fedId){
        
        system.debug('fed id'+fedId);
        WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort cls   = new WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort();
        cls.clientCertName_x                                                            = System.Label.CLJUN16IDMS103;
        cls.endpoint_x                                                                  = label.CLQ316IDMS128;
        WS_IDMSUIMSCompanyService.companyV3 companyObj                                  = new WS_IDMSUIMSCompanyService.companyV3();
        companyObj.OrganizationName                                                     = UserRecord.CompanyName;
        companyObj.GoldenId                                                             = UserRecord.IDMSCompanyGoldenId__c;
        companyObj.CountryCode                                                          = UserRecord.Company_Country__c;
        companyObj.CurrencyCode                                                         = UserRecord.DefaultCurrencyIsoCode;
        companyObj.CustomerClass                                                        = UserRecord.IDMSClassLevel2__c;
        companyObj.LocalityName                                                         = UserRecord.Company_City__c;
        companyObj.PostalCode                                                           = UserRecord.Company_Postal_Code__c;
        companyObj.PostOfficeBox                                                        = UserRecord.IDMSCompanyPoBox__c;
        companyObj.state                                                                = UserRecord.Company_State__c;
        companyObj.Street                                                               = UserRecord.Company_Address1__c;
        companyObj.AddInfoAddress                                                       = UserRecord.Company_Address2__c;
        if(UserRecord.IDMSCompanyHeadquarters__c){
            companyObj.HeadQuarter                                                      ='TRUE';
        }
        else
        {
            companyObj.HeadQuarter                                                      = 'FALSE';
        }
        
        companyObj.County                                                               = UserRecord.IDMSCompanyCounty__c;
        
        companyObj.WebSite                                                              = UserRecord.Company_Website__c;
        If(!String.isBlank(UserRecord.IDMSMarketSubSegment__c)){
            companyObj.MarketSegment                                                    = UserRecord.IDMSMarketSubSegment__c;
        }else{
            companyObj.MarketSegment                                                    = UserRecord.IDMSMarketSegment__c;
        }  
        companyObj.MarketServed                                                         = UserRecord.IDMSCompanyMarketServed__c;
        companyObj.EmployeeSize                                                         = UserRecord.IDMSCompanyNbrEmployees__c;
        companyObj.businessType                                                         = UserRecord.IDMSClassLevel1__c;
        companyObj.TaxIdentificationNumber                                              = UserRecord.IDMSTaxIdentificationNumber__c; 
        string callerFid                                                                = Label.CLJUN16IDMS104;
        
        String createCompanyRes = cls.createCompany(callerFid,fedId,companyObj);
        system.debug(createCompanyRes);
        return createCompanyRes;
    }    
    
    public WS_uimsv22ServiceImsSchneider.userV6 createIdentityPhone(User userRecord){
        WS_uimsv22ServiceImsSchneider.userV6 identityPhone = new WS_uimsv22ServiceImsSchneider.userV6();
        identityPhone.phoneId                   = UserRecord.MobilePhone;
        identityPhone.federatedID               = (UserRecord.FederationIdentifier !=null && !((string)UserRecord.FederationIdentifier).equalsignorecase('null'))?USerRecord.FederationIdentifier:null ;
        identityPhone.firstName                 = (UserRecord.firstname !=null && !((string)UserRecord.firstname ).equalsignorecase('null'))?UserRecord.firstname:null;
        identityPhone.lastName                  = (UserRecord.lastname !=null && !((string)UserRecord.lastname ).equalsignorecase('null'))?UserRecord.lastname:null;
        identityPhone.languageCode              = (UserRecord.IDMS_PreferredLanguage__c !=null && !((string)UserRecord.IDMS_PreferredLanguage__c ).equalsignorecase('null'))?UserRecord.IDMS_PreferredLanguage__c:null;
        identityPhone.countryCode               = (UserRecord.Country !=null && !((string)UserRecord.Country ).equalsignorecase('null'))?UserRecord.Country:null;
        identityPhone.fax                       = (UserRecord.fax !=null && !((string)UserRecord.fax ).equalsignorecase('null'))?UserRecord.fax:null;
        identityPhone.telephoneNumberPersonal   = (UserRecord.phone !=null && !((string)UserRecord.phone ).equalsignorecase('null'))?UserRecord.phone:null;                
        identityPhone.county                    = (UserRecord.IDMS_County__c  !=null && !((string)UserRecord.IDMS_County__c  ).equalsignorecase('null'))?UserRecord.IDMS_County__c:null;
        identityPhone.street                    = (UserRecord.street !=null && !((string)UserRecord.street ).equalsignorecase('null'))?UserRecord.street:null;
        identityPhone.postalCode                = (UserRecord.PostalCode !=null && !((string)UserRecord.PostalCode ).equalsignorecase('null'))?UserRecord.PostalCode:null;
        identityPhone.postOfficeBox             = (UserRecord.IDMS_POBox__c !=null && !((string)UserRecord.IDMS_POBox__c ).equalsignorecase('null'))?UserRecord.IDMS_POBox__c:null;
        identityPhone.cell                      = (UserRecord.mobilephone !=null && !((string)UserRecord.mobilephone ).equalsignorecase('null'))?UserRecord.mobilephone :null;  
        identityPhone.state                     = (UserRecord.state !=null && !((string)UserRecord.state ).equalsignorecase('null'))?UserRecord.state :null;                  
        identityPhone.LocalityName              = (UserRecord.city!=null && !((string)UserRecord.city).equalsignorecase('null'))?UserRecord.city:null;                  
        identityPhone.addInfoAddress            = (UserRecord.IDMS_AdditionalAddress__c !=null && !((string)UserRecord.IDMS_AdditionalAddress__c ).equalsignorecase('null'))?UserRecord.IDMS_AdditionalAddress__c :null;                      
        identityPhone.JobTitle                  = (UserRecord.Job_Title__c!=null && !((string)UserRecord.Job_Title__c).equalsignorecase('null'))?UserRecord.Job_Title__c:null;                      
        identityPhone.JobFunction               = (UserRecord.Job_Function__c !=null && !((string)UserRecord.Job_Function__c ).equalsignorecase('null'))?UserRecord.Job_Function__c :null;                      
        identityPhone.JobDescription            = (UserRecord.IDMSJobDescription__c!=null && !((string)UserRecord.IDMSJobDescription__c).equalsignorecase('null'))?UserRecord.IDMSJobDescription__c:null;
        identityPhone.MiddleName                = (UserRecord.IDMSMiddleName__c!=null && !((string)UserRecord.IDMSMiddleName__c).equalsignorecase('null'))?UserRecord.IDMSMiddleName__c:null;
        identityPhone.Salutation                = (UserRecord.IDMSSalutation__c!=null && !((string)UserRecord.IDMSSalutation__c).equalsignorecase('null'))?UserRecord.IDMSSalutation__c:null;
        return identityPhone;
    }
    
    public WS_IdmsUimsAuthUserManagerV2Service.userV5 createIdentity(User userRecord){
        WS_IdmsUimsAuthUserManagerV2Service.userV5 identity = new WS_IdmsUimsAuthUserManagerV2Service.userV5();
        identity.federatedID                    = (UserRecord.FederationIdentifier !=null && !((string)UserRecord.FederationIdentifier).equalsignorecase('null'))?USerRecord.FederationIdentifier:null ;
        identity.email                          = (UserRecord.email !=null && !((string)UserRecord.email).equalsignorecase('null'))?UserRecord.email:null ;
        identity.firstName                      =(UserRecord.firstname !=null && !((string)UserRecord.firstname ).equalsignorecase('null'))?UserRecord.firstname:null;
        identity.lastName                       = (UserRecord.lastname !=null && !((string)UserRecord.lastname ).equalsignorecase('null'))?UserRecord.lastname:null;
        identity.languageCode                   = (UserRecord.IDMS_PreferredLanguage__c !=null && !((string)UserRecord.IDMS_PreferredLanguage__c ).equalsignorecase('null'))?UserRecord.IDMS_PreferredLanguage__c:null;
        identity.countryCode                    = (UserRecord.Country !=null && !((string)UserRecord.Country ).equalsignorecase('null'))?UserRecord.Country:null;
        identity.fax                            = (UserRecord.fax !=null && !((string)UserRecord.fax ).equalsignorecase('null'))?UserRecord.fax:null;
        identity.telephoneNumberPersonal        = (UserRecord.phone !=null && !((string)UserRecord.phone ).equalsignorecase('null'))?UserRecord.phone:null;                
        identity.county                         = (UserRecord.IDMS_County__c  !=null && !((string)UserRecord.IDMS_County__c  ).equalsignorecase('null'))?UserRecord.IDMS_County__c:null;
        identity.street                         = (UserRecord.street !=null && !((string)UserRecord.street ).equalsignorecase('null'))?UserRecord.street:null;
        identity.postalCode                     = (UserRecord.PostalCode !=null && !((string)UserRecord.PostalCode ).equalsignorecase('null'))?UserRecord.PostalCode:null;
        identity.postOfficeBox                  = (UserRecord.IDMS_POBox__c !=null && !((string)UserRecord.IDMS_POBox__c ).equalsignorecase('null'))?UserRecord.IDMS_POBox__c:null;
        identity.cell                           = (UserRecord.mobilephone !=null && !((string)UserRecord.mobilephone ).equalsignorecase('null'))?UserRecord.mobilephone :null;  
        identity.state                          = (UserRecord.state !=null && !((string)UserRecord.state ).equalsignorecase('null'))?UserRecord.state :null;                  
        identity.LocalityName                   = (UserRecord.city!=null && !((string)UserRecord.city).equalsignorecase('null'))?UserRecord.city:null;                       
        identity.addInfoAddress                 = (UserRecord.IDMS_AdditionalAddress__c !=null && !((string)UserRecord.IDMS_AdditionalAddress__c ).equalsignorecase('null'))?UserRecord.IDMS_AdditionalAddress__c :null;                      
        identity.JobTitle                       = (UserRecord.Job_Title__c!=null && !((string)UserRecord.Job_Title__c).equalsignorecase('null'))?UserRecord.Job_Title__c:null;                      
        identity.JobFunction                    = (UserRecord.Job_Function__c !=null && !((string)UserRecord.Job_Function__c ).equalsignorecase('null'))?UserRecord.Job_Function__c :null;                      
        identity.JobDescription                 = (UserRecord.IDMSJobDescription__c!=null && !((string)UserRecord.IDMSJobDescription__c).equalsignorecase('null'))?UserRecord.IDMSJobDescription__c:null;
        identity.MiddleName                     = (UserRecord.IDMSMiddleName__c!=null && !((string)UserRecord.IDMSMiddleName__c).equalsignorecase('null'))?UserRecord.IDMSMiddleName__c:null;
        identity.Salutation                     = (UserRecord.IDMSSalutation__c!=null && !((string)UserRecord.IDMSSalutation__c).equalsignorecase('null'))?UserRecord.IDMSSalutation__c:null;
        system.debug('Identity to send to uims is: '+   identity);
        return identity;
    }
}