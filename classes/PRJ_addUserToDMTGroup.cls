public class PRJ_addUserToDMTGroup
{

  public static void AddToGroups(list<User> userIds)
  {
    Group g=[select Id from Group Where DeveloperName = :System.label.DMTLibraryName];
    List<GroupMember>listGroupMember =new List<GroupMember>();  
    // loop the users that have been created
    for (User user : userIds)
    { 
      system.debug('adding user');
      GroupMember gm= new GroupMember(); 
      gm.GroupId=g.id;
      gm.UserOrGroupId = user.id;
      listGroupMember.add(gm); 
    }  
     
     if(!listGroupMember.isEmpty())
     {
        Database.SaveResult[] SaveResult = database.insert(listGroupMember,false);
        for(Integer i=0;i<SaveResult.size();i++ )
          {
            Database.SaveResult sr =SaveResult[i];
            if(!sr.isSuccess())
            {
              Database.Error err = sr.getErrors()[0];
              system.debug('Exception while adding User to Group:'+err.getMessage());
            }
          }
    }  
  }
}