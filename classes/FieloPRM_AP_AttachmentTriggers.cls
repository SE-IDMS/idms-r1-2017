/******************************************
* Developer: Fielo Team                   *
*******************************************/
public without sharing class FieloPRM_AP_AttachmentTriggers{

    public static void copyAttachmentIdToInvoice(){

        List<Attachment> triggernew = trigger.new;

        map<Id,Attachment> mapParentIdAttach = new map<Id,Attachment>();
        
        for(Attachment attach: triggernew){
            mapParentIdAttach.put(attach.ParentId, attach);
        }
        
        List<FieloPRM_Invoice__c> invoiceToUpdate = new List<FieloPRM_Invoice__c>();
        
        for(FieloPRM_Invoice__c invoice: [SELECT id, F_PRM_AttachmentId__c FROM FieloPRM_Invoice__c WHERE Id IN: mapParentIdAttach.keySet()]){         
            invoice.F_PRM_AttachmentId__c = mapParentIdAttach.get(invoice.Id).Id;
            invoice.F_PRM_AttachmentId__c = invoice.F_PRM_AttachmentId__c.substring(0,invoice.F_PRM_AttachmentId__c.length()-3);
            invoiceToUpdate.add(invoice);
        }
        if(!invoiceToUpdate.isEmpty()){
            update invoiceToUpdate;
        }
        
    }
    
}