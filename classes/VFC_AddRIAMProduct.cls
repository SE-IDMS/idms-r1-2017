global with sharing class VFC_AddRIAMProduct {    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public ReturnItemsApprovalMatrix__c riamRecord{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public VFC_AddRIAMProduct(ApexPages.StandardController controller){
        riamRecord=(ReturnItemsApprovalMatrix__c)controller.getRecord();  
        
         if(riamRecord.id!=null){
            riamRecord=[select id, CommercialReference__c,BusinessUnit__c ,Family__c ,ProductDescription__c ,ProductFamily__c ,ProductGroup__c,ProductLine__c,ProductSuccession__c,SubFamily__c  from ReturnItemsApprovalMatrix__c where id=:riamRecord.id];
            if(riamRecord.BusinessUnit__c!=null)
                pageParameters.put('businessLine',riamRecord.BusinessUnit__c);            
            if(riamRecord.ProductLine__c!=null)
                pageParameters.put('productLine',riamRecord.ProductLine__c);            
            if(riamRecord.ProductFamily__c!=null)
                pageParameters.put('strategicProductFamily',riamRecord.ProductFamily__c);            
            if(riamRecord.Family__c!=null)
                pageParameters.put('family',riamRecord.Family__c);            
            if(riamRecord.CommercialReference__c!=null)
                pageParameters.put('commercialReference',riamRecord.CommercialReference__c);
            String URLCR='';
            if(pageParameters.containsKey('commercialReference'))
                URLCR =pageParameters.get('commercialReference');          
        }                     
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    global static String getSearchText() {
        String scriteria='Vimal';
        /*
        System.debug('--Vimal--');
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
        System.debug('--Vimal--: ' + scriteria);
        */
        return scriteria; 
    }
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        system.debug('&&& business'+ business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    public PageReference performSelect(){      
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        WS_GMRSearch.gmrDataBean DBL= (WS_GMRSearch.gmrDataBean) JSON.deserialize(jsonSelectString, WS_GMRSearch.gmrDataBean.class);   
                
        if(riamRecord.id != null){  
                    riamRecord.CommercialReference__c= DBL.commercialReference;
                    //riamRecord.Family__c = DBL.family.label;
                    riamRecord.BusinessUnit__c =DBL.businessLine.label;
                    riamRecord.Family__c =DBL.family.label;                   
                    riamRecord.ProductDescription__c =DBL.description;
                    riamRecord.ProductFamily__c =DBL.strategicProductFamily.label;
                    riamRecord.ProductGroup__c =DBL.productGroup.label;
                    riamRecord.ProductLine__c =DBL.productLine.label;
                    riamRecord.ProductSuccession__c =DBL.productSuccession.label;
                    riamRecord.SubFamily__c =DBL.subFamily.label;
                    
                    if(DBL.businessLine.value != null)
                        riamRecord.TECH_GMRCode__c = DBL.businessLine.value;
                    if(DBL.productLine.value != null)
                        riamRecord.TECH_GMRCode__c += DBL.productLine.value;
                    if(DBL.strategicProductFamily.label != null)
                        riamRecord.TECH_GMRCode__c += DBL.strategicProductFamily.value;
                    if(DBL.family.label != null)
                        riamRecord.TECH_GMRCode__c += DBL.family.value;
                    if(DBL.subFamily.label != null)
                        riamRecord.TECH_GMRCode__c += DBL.subFamily.value;
                    if(DBL.productSuccession.label != null)
                        riamRecord.TECH_GMRCode__c += DBL.productSuccession.value;
                    if(DBL.productGroup.label != null)
                        riamRecord.TECH_GMRCode__c += DBL.productGroup.value;
        }                  
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'RPDT Id cannot be blank'));
            return null;
        }       

        try{
            upsert riamRecord;
            PageReference casePage;            
            casePage=new PageReference('/'+riamRecord.id);                        
            return casePage;
        }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    
    }
    public PageReference performSelectFamily(){   
    
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        WS04_GMR.gmrDataBean DBL= (WS04_GMR.gmrDataBean) JSON.deserialize(jsonSelectString, WS04_GMR.gmrDataBean.class);   
    
        if(riamRecord.id!=null){         
            if(DBL.businessLine.label != null)
                riamRecord.BusinessUnit__c = DBL.businessLine.label;                     
            if(DBL.productLine.label != null)
                riamRecord.ProductLine__c = DBL.productLine.label;                        
            if(DBL.strategicProductFamily.label != null)
                riamRecord.ProductFamily__c = DBL.strategicProductFamily.label;                       
            if(DBL.family.label != null)
                riamRecord.Family__c = DBL.family.label;                 

            riamRecord.CommercialReference__c = null;                    
            riamRecord.ProductDescription__c = null;
            riamRecord.SubFamily__c = null;
            riamRecord.ProductSuccession__c = null;
            riamRecord.ProductGroup__c = null;       
            riamRecord.ProductDescription__c = null;      
            if(DBL.businessLine.value != null)
                riamRecord.TECH_GMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                riamRecord.TECH_GMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                riamRecord.TECH_GMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                riamRecord.TECH_GMRCode__c += DBL.family.value;


        }
        else{
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'CaseID cannot be blank'));
             return null;
        }       

        try{
            upsert riamRecord;
            PageReference casePage;            
            casePage=new PageReference('/'+riamRecord.id);                        
            return casePage;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
        }        

        
        return null;
    }
    
 public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(riamRecord.id!=null){
            pageResult = new PageReference('/'+ riamRecord.id);
             
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
   // return null;

}