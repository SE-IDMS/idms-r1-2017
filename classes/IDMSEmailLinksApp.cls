Public Class IDMSEmailLinksApp {  
    /*
        this method returns the first part of email links
    */
    public Static Final Integer registrationFlow = 0;
    public Static Final Integer updateEmailFlow = 1;
    public Static Final Integer resetPasswordFlow = 2;
    public Static Final Integer invitationFlow = 3;
    
    public static String getEmailLinkApp(String appName, Integer flow){
        String returnLink;
        //get all custom setting records
        List<IDMSApplicationMapping__c> apps = IDMSApplicationMapping__c.getall().values();
        //Loop on each one to check the corresponding one
        for (IDMSApplicationMapping__c app : apps){
            //If corressponding one then return email link
            if(app.Name==appName){
                if(flow == IDMSEmailLinksApp.registrationFlow){
                    returnLink = app.UrlAppSetPwPage__c;
                    
                }else if(flow == IDMSEmailLinksApp.updateEmailFlow){
                    returnLink = app.EmailLinkUpdateEmail__c;
                    
                }else if(flow == IDMSEmailLinksApp.resetPasswordFlow ){
                    returnLink = app.UrlAppResetPwPage__c;
                    
                }else if(flow == IDMSEmailLinksApp.invitationFlow){
                    returnLink = app.EmailLinkInvitation__c;
                }
                system.debug('email link: '+returnLink);
                return returnLink ;
            }
        }
        return null;            
    }




}