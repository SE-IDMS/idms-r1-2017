public class AP_PermissionSetGroupMethods {

    public static List<PermissionSetAssignmentRequests__c> generateRequest(List<PermissionSetGroup__c> psGroupList, List<User> userList, String mode) {
        
        List<PermissionSetAssignmentRequests__c> psAssignmentRequestList = new List<PermissionSetAssignmentRequests__c>();
        
        Set<Id> userIdSet = new Set<Id>();
        
        for(User user:userList) {
            userIdSet.add(user.Id);
        }
        
        Map<Id, PermissionSetGroup__c> psGroupMap = getExistingAssignmentsMap(userIdSet);
        Map<Id, Map<Id, PermissionSetAssignmentRequests__c>> psAssignMap = getPsRequestMap(userIdSet, psGroupMap.keyset());
        
        Map<Id, List<UserPackageLicense>> userLicenseMap = getUserLicences(userIdSet, psGroupList);
        
        List<PermissionSetAssignmentRequests__c> newRequests = new List<PermissionSetAssignmentRequests__c>();
        
        system.debug('User List = ' + userList);
        system.debug('psGroupList = ' + psGroupList);
        system.debug('psAssignMap = ' + psAssignMap);
        
        for(User user:userList) {
            
            for(PermissionSetGroup__c psGroup:psGroupList) {
                if(psGroup.Id != null) {
                    Boolean existingRequest = false;
                    
                    if(mode != 'IgnoreExisting' && psAssignMap.get(user.Id) != null 
                        && psAssignMap.get(user.Id).get(psGroup.Id) != null) {
                        
                        existingRequest = true;
                    }
                    
                    system.debug('existingRequest = ' + existingRequest);
                    
                    if(!existingRequest) {
                        PermissionSetAssignmentRequests__c newRequest = new PermissionSetAssignmentRequests__c(PermissionSetGroup__c = psGroup.Id);
                        setStatus(newRequest, psGroup, user, userLicenseMap);
                        psAssignmentRequestList.add(newRequest);
                    }                  
                }
            }
        }
        
        return psAssignmentRequestList;
    }
    
    public static void setStatus(PermissionSetAssignmentRequests__c anAssignRequest, PermissionSetGroup__c psGroup, 
                            User user, Map<Id, List<UserPackageLicense>> userLicenseMap) {

        if(!user.IsActive) {
            anAssignRequest.PendingAssigneeName__c = user.FirstName+' '+user.LastName;
            anAssignRequest.PendingAssigneeId__c   = user.Id;
            anAssignRequest.Status__c              = 'PendingActivation';
        }
        else {
            anAssignRequest.Assignee__c = user.Id;
        }

        if(anAssignRequest.Status__c == null) {
            anAssignRequest.Assignee__c = user.Id;
            
            if(psGroup.License__c != null && false) {
            
                Boolean isLicensedAssigned = false;
            
                for(UserPackageLicense userPackageLicense:userLicenseMap.get(user.Id)) {
                    if(userPackageLicense.PackageLicense.NameSpacePrefix == psGroup.License__c) {
                        isLicensedAssigned = true;
                    }
                }
                
                if(!isLicensedAssigned ) {
                    anAssignRequest.Status__c = 'PendingLicense'; 
                    anAssignRequest.addError('Assignment of License '+psGroup.License__c+' is required.');  
                }
            }
        }
        
        if(psGroup.name == 'FS Skill Management'){
        
        anAssignRequest.Status__c = 'Skill Management'; 
        }
        
        if(anAssignRequest.Status__c == null) {
             if(psGroup.RequiresValidation__c) {
                 anAssignRequest.Status__c = 'PendingCPValidation';   
             }   
             else {
                 anAssignRequest.Status__c = 'Assigned'; 
             }
        }      
    }
    
    public static Map<Id, Map<Id, PermissionSetAssignmentRequests__c>> getPsRequestMap(Set<Id> userIdSet, Set<Id> permissionGroupIdSet) {
        
        Map<Id, Map<Id, PermissionSetAssignmentRequests__c>> userRequestMap = new Map<Id, Map<Id, PermissionSetAssignmentRequests__c>>();
        
        for(PermissionSetAssignmentRequests__c psAssignmentRequest:[SELECT Id, Assignee__c, PendingAssigneeId__c, PermissionSetGroup__c 
                                                                    FROM PermissionSetAssignmentRequests__c WHERE (PendingAssigneeId__c IN :userIdSet
                                                                    OR Assignee__c IN :userIdSet)
                                                                    AND PermissionSetGroup__c IN :permissionGroupIdSet]) {
            
            String userId = psAssignmentRequest.Assignee__c != null ? psAssignmentRequest.Assignee__c : psAssignmentRequest.PendingAssigneeId__c;
            
            if(userRequestMap.get(userId ) == null) {
                userRequestMap.put(userId, new Map<Id, PermissionSetAssignmentRequests__c>());
            }
            
            userRequestMap.get(userId).put(psAssignmentRequest.PermissionSetGroup__c, psAssignmentRequest);
        }
        
        return userRequestMap;
    }
    
    public static Map<Id, List<UserPackageLicense>> getUserLicences(Set<Id> userIdSet, List<PermissionSetGroup__c> psGroupList) {
        
        Set<String> licenceNameSet = new Set<String>();
        
        for(PermissionSetGroup__c psGroup:psGroupList) {
            if(psGroup.License__c != null) {
                licenceNameSet.add(psGroup.License__c);
            }
        }
        
        Map<Id, List<UserPackageLicense>> userIdLicenseListMap = new Map<Id, List<UserPackageLicense>>();
        
        for(UserPackageLicense licenceAssignment:[SELECT UserId FROM UserPackageLicense 
                                                  WHERE PackageLicense.NamespacePrefix IN :licenceNameSet 
                                                  AND UserId IN :userIdSet]) {
                                                  
            if(userIdLicenseListMap.get(licenceAssignment.UserId) == null) {
                userIdLicenseListMap.put(licenceAssignment.UserId, new List<UserPackageLicense>());
            }
            
            userIdLicenseListMap.get(licenceAssignment.UserId).add(licenceAssignment);
        }
        
        return userIdLicenseListMap;

    }
    
    public static Map<Id, PermissionSetGroup__c> getExistingAssignmentsMap(Set<Id> userIdSet) {
    
        Map<Id, PermissionSetGroup__c> existingAssignmentsMap = new Map<Id, PermissionSetGroup__c>();
    
        for(PermissionSetGroup__c permissionSetGroup:[SELECT Id, Name, RequiresValidation__c, Description__c, TECH_IsAssigned__c, TECH_Status__c,
                                                      (SELECT Id, Name, PermissionSetId__c FROM PermissionSetGroupMembers__r), 
                                                      (SELECT PermissionSetGroup__c, Status__c, Assignee__c, PendingAssigneeId__c
                                                      FROM PSAssignmentRequests__r 
                                                      WHERE Assignee__c IN :userIdSet OR PendingAssigneeId__c IN :userIdSet),                              
                                                      MemberList__c, Type__c, Stream__c FROM PermissionSetGroup__c 
                                                      WHERE Id IN (SELECT PermissionSetGroup__c FROM PermissionSetAssignmentRequests__c 
                                                      WHERE (Assignee__c IN :userIdSet OR PendingAssigneeId__c IN :userIdSet))]) {

            
            existingAssignmentsMap.put(permissionSetGroup.Id, permissionSetGroup);
        }    
        
        return existingAssignmentsMap;
    }    
    
    @future
    public static void updateStatus(Set<Id> permissionRequestIdSet) {

        List<PermissionSetAssignmentRequests__c> psAssignmentRequestList = [SELECT Id, Status__c, PendingAssigneeId__c, PermissionSetGroup__c 
                                                                      FROM PermissionSetAssignmentRequests__c 
                                                                      WHERE Id IN :permissionRequestIdSet];
        
        for(PermissionSetAssignmentRequests__c psAssignmentRequest:psAssignmentRequestList) {
            psAssignmentRequest.Status__c = 'Assigned';
        }
        
        update psAssignmentRequestList;    
    }
    
    public static void assignPS(Set<Id> activatedUserSet) {
        
        if(!System.isFuture() && !System.isBatch()) {
            List<PermissionSetAssignmentRequests__c> psAssignmentRequestList = [SELECT Id, Status__c, PendingAssigneeId__c, PermissionSetGroup__c 
                                                                          FROM PermissionSetAssignmentRequests__c 
                                                                          WHERE PendingAssigneeId__c IN :activatedUserSet
                                                                          AND Status__c = 'PendingActivation'];
            
            AP_PsAssignmentMethods.assignPermissionSet(psAssignmentRequestList);
            
            Set<Id> permissionSetRequestIdSet = new Set<Id>();
            
            for(PermissionSetAssignmentRequests__c psAssignmentRequest:psAssignmentRequestList) {
                permissionSetRequestIdSet.add(psAssignmentRequest.Id);
            }
            
            updateStatus(permissionSetRequestIdSet);         
        }                   
    }
}