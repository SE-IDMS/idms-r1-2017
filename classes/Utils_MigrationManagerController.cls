public class Utils_MigrationManagerController {
/*
*********************************************
public Blob myBlob{get;set;}
public List<List<String>> blobContent{get;set;}{blobContent=new List<List<String>>();}
public boolean isUploaded{get;set;}{isUploaded=false;}
public Integer noOfCols{get;set;}{noOfCols=2;}

public Integer batchSize{get;set;}{batchSize=400;}

public String objectSelected {get;set;}
public String fieldSelected{get;set;}
public String fieldSelected1{get;set;}

public List<SelectOption> objectNames{get;set;}
public List<SelectOption> fieldNames{get;set;}
public List<SelectOption> fieldNames1{get;set;}

Map<String,Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();  
public String batchQuery{get;set;}{batchQuery='';}   
public Map<String,String> batchMap{get;set;}
public List<AsyncApexJob> asyncJobs{get;set;}{asyncJobs=new List<AsyncApexJob>();}
*********************************************
*/
public Utils_MigrationManagerController (){      
    string tempfortest= '';
/*
*********************************************
    objectNames= new List<SelectOption>();
    objectNames.add(new SelectOption('--None--',''));
    List<String> namesList=new List<String>();
    for(Schema.SObjectType f : globalDescribeMap.values())
    {
    namesList.add(f.getDescribe().getName());       
    }
    namesList.sort();    
    for(String sss:namesList)
    {
       objectNames.add(new SelectOption(sss,sss)); 
    }
*********************************************
*/
}
/*
*********************************************
public void populateFields()
{
System.debug('#objectSelected#'+objectSelected);
System.debug('#hasKey#'+Schema.getGlobalDescribe().containsKey(objectSelected));
fieldNames=new List<SelectOption>();
fieldNames.clear();
if(Schema.getGlobalDescribe().containsKey(objectSelected)){
 Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectSelected).getDescribe().fields.getMap();
 System.debug('#fieldMap#'+fieldMap);
 List<String> fieldsList=new List<String>();
 
  for (String fieldName: fieldMap.keySet()) {
     fieldsList.add(fieldName);        
  }
  fieldsList.sort();
  for(String sss:fieldsList)
  {
      fieldNames.add(new SelectOption(sss,sss));
  }
 if(blobContent[0].size()==4) //a special map
    {
    fieldNames1=new List<SelectOption>();
    List<String> fieldsList1=new List<String>();
       for (String fieldName: fieldMap.keySet()) {
       fieldsList1.add(fieldName);        
       }
       for(String ss:fieldsList1)
        {
        fieldNames1.add(new SelectOption(ss,ss));
        }
    }             
}
}


public void saveBlob()
{
    for(String levelOne:myBlob.toString().split('\n'))
    {
        if(levelOne.trim()!=''){
            blobContent.add(levelOne.split(','));
            noOfCols=levelOne.split(',').size();
        }
    } 
    isUploaded=true;
}

public void showQuery()
{
   batchQuery='select '+fieldSelected+' from '+objectSelected+' where '+fieldSelected+'!=\'\'';
   batchMap=new Map<String,String>();
   if(noOfCols==4)
   {
       batchQuery='select '+fieldSelected+','+fieldSelected1+' from '+objectSelected+' where '+fieldSelected+'!=\'\'';
   }
   //special cases
   if(objectSelected=='Lead')
   batchQuery+=' and ConvertedDate = null';
   if(objectSelected=='Event')
   batchSize=1;
   
   Integer index=0;
   for(List<String> eachBlob:blobContent)
   {
       index++;
     if(index==1)
     continue;     
     if(eachBlob.size()==2)
     batchMap.put(eachBlob[0],eachBlob[1]);       
     else if(eachBlob.size()==4)
     batchMap.put(eachBlob[0]+':'+eachBlob[1],eachBlob[2]+':'+eachBlob[3]);       
   }    
}

public void queryBatches()
{
    asyncJobs=[SELECT ApexClassId,CompletedDate,CreatedById,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status,TotalJobItems FROM AsyncApexJob where createdDate=Today order by createdDate DESC];
}

public void addTOBatchQueue(){
//query,field,object,map
   if(noOfCols==4)
   {
    Database.executeBatch(new Utils_MigrationManagerBatch(batchQuery,fieldSelected,objectSelected,batchMap,fieldSelected1),batchSize);
   }
   else
   Database.executeBatch(new Utils_MigrationManagerBatch(batchQuery,fieldSelected,objectSelected,batchMap,null),batchSize);
}
*********************************************
*/
}