@isTest
global class AP_BoxConnectImpl_TEST implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals(label.CLAPRIL15ELLAbFO05, req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
            res.setBody('{"type": "folder","id": "3333","sequence_id": "1","etag": "1","name": "Pictures","created_at": "2024-12-12T10:53:43-08:00","modified_at": "2012-12-12T11:15:04-08:00","description": "Some pictures I took","size": 629644,"path_collection": {"total_count": 1,"entries": [{"type": "folder","id": "0","sequence_id": null,"etag": null,"name": "All Files"}]},"created_by": {"type": "user","id": "26788","name": "sean rose","login": "sean@box.com"},"modified_by": {"type": "user","id": "17738362","name": "sean rose","login": "sean@box.com"},"owned_by": {"type": "user","id": "17738362","name": "sean rose","login": "sean@box.com"},"shared_link": {"url": "https://www.box.com/s/vspke7y05sb214wjokpk","download_url": "https://www.box.com/shared/static/vspke7y05sb214wjokpk","vanity_url": null,"is_password_enabled": false,"unshared_at": null,"download_count": 0,"preview_count": 0,"access": "open","permissions": {"can_download": true,"can_preview": true}},"folder_upload_email": {"access": "open","email": "upload.Picture.k13sdz1@u.box.com"},"parent": {"type": "folder","id": "0","sequence_id": null,"etag": null,"name": "All Files"},"item_status": "active","item_collection": {"total_count": 1,"entries": [{"type": "file","id": "555566","sequence_id": "3","etag": "3","sha1": "134b65991ed521fcfe4724b7d814ab8ded5185dc","name": "tigers.jpeg"}],"offset": 0,"limit": 100},"tags": ["approved","ready to publish"]}');
       
        //res.setBody('{"Id":"1234","folderShareLink":"www.google.com","url":"www.google.com","expires_in":30}');
        //res.setStatus(label.CLAPR15ELLAbFO32);
        res.setStatusCode(200);
        res.setStatus(label.CLAPR15ELLAbFO32);
        return res;
    }
}