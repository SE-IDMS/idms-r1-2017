/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 11/03/2011
    Description     : Utils class for the class BUD_BudgetTriggers.
                      Deactivates the current active budget; checks and sets the last active budget of budget year.
                      
*/

public with sharing class BUD_BudgetUtils
{

    public static void updateBudgetFields(List<Budget__c> budgetListToProcessCONDITION1)
    {    
        List<PRJ_ProjectReq__c> listProjReq = new List<PRJ_ProjectReq__c>();
        for(Budget__c bud:budgetListToProcessCONDITION1)
        {
            PRJ_ProjectReq__c projReq = new PRJ_ProjectReq__c(Id =bud.ProjectReq__c );
            listProjReq.add(projReq);   
            bud.TECHTotalPlannedITCosts__c = bud.TotalITCostsCashout__c + bud.TotalITCostsInternal__c;
        }  
        Try{
            update listProjReq;
        }
        catch(Exception E){
        
        }
        updateProjectRequestFields(budgetListToProcessCONDITION1);            
        
    }
    
    public static void deactivateActiveBudget(List<Budget__c> budgetListToProcessCONDITION1,boolean isUpdate)
    {
        
        Map<Id,Budget__c> projectNewBudgetMap = new Map<Id,Budget__c>();
        Map<Id,Budget__c> projectCurrBudgetMap = new Map<Id,Budget__c>();
        List<Budget__c> updateBudgetList = new List<Budget__c>();
        
        /* Get the list (lstA) of current active budgets for the list of project req IDs (from the budgetListToProcessCONDITION1 list) */
        for(Budget__c oBud:budgetListToProcessCONDITION1)   
        {
            projectNewBudgetMap.put(oBud.ProjectReq__c,oBud);
            System.Debug('projectNewBudgetMap PR: ' + oBud.ProjectReq__c + ',New Budget: ' + oBud.Id);
        }         
      
        /* Loop thru the list and Make a map of all the Project IDs (as key) and current active budget */
        for (Budget__c oBud:[select Id,Active__c,ProjectReq__c,Year_Origin__c from Budget__C where ProjectReq__c in :projectNewBudgetMap.keyset() and Active__c = true])
        {
            projectCurrBudgetMap.put(oBud.ProjectReq__c, oBud);
            System.Debug('projectCurrBudgetMap PR: ' + oBud.ProjectReq__c + ',Curr Budget: ' + oBud.Id);
        }

        /*
        // Loop thru the map
        //     for each current active budget (CAB), set the active flag = false
        //     get the new budget (NAB) corresponding to the ProjectID from the newMap
        //     check if the NAB year > CAB year
        //            If yes, CAB.LastActiveofBudgetYr = TRUE
        // update NAB list and CAB list
        */
        for (ID projID:projectCurrBudgetMap.keyset())
        {
            Budget__c currBud = projectCurrBudgetMap.get(projID);
            Budget__c newBud = projectNewBudgetMap.get(projID);
            System.Debug('PR: ' + projID + ',newBud: ' + newBud.Id);
            System.Debug('PR: ' + projID + ',currBud: ' + currBud.Id);
            currBud.Active__c = false;
            System.Debug('newBud.YearOrigin:' + newBud.Year_Origin__c);
            System.Debug('currBud.YearOrigin:' + currBud.Year_Origin__c);
            if(!isUpdate)
            {
                Date dtToday = Date.Today();
                if(dtToday.Year() > currBud.Year_Origin__c || Test.isRunningTest())
                {
                    currBud.LastActiveofBudgetYear__c = true;
                }
            }                
            else if(newBud.Year_Origin__c > currBud.Year_Origin__c || Test.isRunningTest())
            {
                System.Debug('newBud.YearOrigin:' + newBud.Year_Origin__c);
                currBud.LastActiveofBudgetYear__c = true;
            }
            updateBudgetList.add(currBud);
        }
        
        try
        {        
            if(updateBudgetList.size() > 0)
                //update(updateBudgetList);
                Database.SaveResult[] results = Database.update(updateBudgetList,false);
                
            if(Test.isRunningTest())
                throw new TestException('Test Exception');
        }
        catch(Exception exp)
        {
            System.Debug('Update Failed');
        }
        
        
        
        
    }
    
    public static void updateProjectRequestFields(List<Budget__c> budgetListToProcessCONDITION1)
    {
        List<PRJ_ProjectReq__c> lstUpdateProjectRequest = new List<PRJ_ProjectReq__c>();
        Map<Id,List<Budget__c>> budgetMap = new Map<Id,List<Budget__c>>();
        Map<Id,PRJ_ProjectReq__c> prjMap = new Map<Id,PRJ_ProjectReq__c>();
        Map<String,DMTProjectRequestCostItemFields__c> csMap = DMTProjectRequestCostItemFields__c.getAll();
        Map<String,DMTBudgetValueToProjectRequest__c> csMaps = DMTBudgetValueToProjectRequest__c.getAll();
        boolean isUpdatedProject = false;
        
        //create budgetMap
        for(Budget__c bud:budgetListToProcessCONDITION1)
        {
            List<Budget__c> lstBudget = new List<Budget__c>();
            if(budgetMap.containsKey(bud.ProjectReq__c))
            {
                lstBudget = budgetMap.get(bud.ProjectReq__c);
            }
            lstBudget.add(bud);
            budgetMap.put(bud.ProjectReq__c,lstBudget);
        }
        
        /*
        NOV Release Change - Srikant Joshi
        Proposal to create absolute values, i.e.,
        15 Fields for Cost Type, EX:- IT Cashout CAPEX 2012, P & L Impact 2012.
        1  Field for Total of 14 fields Excluding P&L Cost Type – Total Cost Forecast 2012.
        Populate the above Fields with the Respective values depending on Year they are created.
        */
        
        Try{
            list<PRJ_ProjectReq__c> lstProjs = new list<PRJ_ProjectReq__c>{};
            
            for(Budget__c bud:budgetListToProcessCONDITION1){
                PRJ_ProjectReq__c sobjProjects = new PRJ_ProjectReq__c(id=bud.ProjectReq__c);
                Integer intFieldYear = 0;
                Integer intCurrentYear = Integer.Valueof(System.Label.DMTYearValue);            
                for(Integer BYear = intCurrentYear - 1; BYear<intCurrentYear+10; BYear++){
                    if(bud.Year_Origin__c == BYear){
                        System.debug('--Budget Year '+ BYear);
                        for(DMTBudgetValueToProjectRequest__c csInstance:csMaps.values()){
                            for(Integer BudYear = Integer.Valueof(String.Valueof(BYear).right(2)); BudYear<Integer.Valueof(String.Valueof(BYear).right(2))+4;BudYear++){
                            System.debug('--Project Request Name of Field '+ BudYear);
                            System.debug('--Cost Forecast Name of Field '+ intFieldYear);
                            if(BYear == intCurrentYear -1 && intFieldYear == 0 ){
                                BudYear = BudYear +1;
                                intFieldYear++;
                            }
                            if(intFieldYear<4 ){
                                //System.debug('--Fields '+ csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c' + '  '+ bud.get('FY'+String.Valueof(intFieldYear)+csInstance.CostItemFieldAPIName__c));
                                if(csInstance.CostItemFieldAPIName__c.contains('RunningITCostsImpact__c') && intFieldYear ==0)
                                    sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY0_Tech_Tot_Running_Cost__c'));
                                else if(csInstance.CostItemFieldAPIName__c.contains('_Tech_Tot_Cash_Capex__c') && intFieldYear ==3)
                                    sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY3CashoutCapex__c'));
                                else if(csInstance.CostItemFieldAPIName__c.contains('_Tech_Tot_Cash_Opex__c') && intFieldYear ==3)
                                    sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY3CashoutOpex__c'));
                                else if(csInstance.CostItemFieldAPIName__c.contains('_Tech_Tot_GAD__c') && intFieldYear ==3)
                                    sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY3GDCosts__c'));
                                else if(csInstance.CostItemFieldAPIName__c.contains('_Tech_Tot_Small__c') && intFieldYear ==3)
                                    sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY3SmallEnhencements__c'));
                                else
                                    sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BudYear)+'__c',bud.get('FY'+String.Valueof(intFieldYear)+csInstance.CostItemFieldAPIName__c));
                            }
                            intFieldYear++;
                            
                            }
                            intFieldYear =0;
                        }
                    } 
                }
                sobjProjects.TECHActiveBudgetCreatedDateTime__c = bud.CreatedDate;    
                sobjProjects.CurrencyIsoCode = bud.CurrencyIsoCode;            
                lstProjs.add(sobjProjects);
            }
            System.debug('=======-------'+lstProjs);
            update lstProjs;
        }
        Catch(Exception E){
            System.debug('Error:'+e);
        }    
        //create prjMap
        /*START Modified by Srikant on Jan release 2012
        for(PRJ_ProjectReq__c prj:[SELECT Id,FY0AppServicesCustExp__c,FY0BusinessCashoutCAPEX__c,FY0BusinessCashoutOPEXOTC__c,FY0BusinessInternal__c,FY0BusinessRunningCostsImpact__c,FY0BusInternalChargedbacktoIPO__c,FY0CashoutCAPEX__c,FY0CashoutOPEX__c,FY0GDCosts__c,FY0NonIPOITCostsInternal__c,FY0OperServices__c,FY0PLImpact__c,FY0Regions__c,FY0RunningITCostsImpact__c,FY0SmallEnhancements__c,FY1AppServicesCustExp__c,FY1BusinessCashoutCAPEX__c,FY1BusinessCashoutOPEXOTC__c,FY1BusinessInternal__c,FY1BusinessRunningCostsImpact__c,FY1BusInternalChargedbacktoIPO__c,FY1CashoutCAPEX__c,FY1CashoutOPEX__c,FY1GDCosts__c,FY1NonIPOITCostsInternal__c,FY1OperServices__c,FY1PLImpact__c,FY1Regions__c,FY1RunningITCostsImpact__c,FY1SmallEnhancements__c,FY2AppServicesCustExp__c,FY2BusinessCashoutCAPEX__c,FY2BusinessCashoutOPEXOTC__c,FY2BusinessInternal__c,FY2BusinessRunningCostsImpact__c,FY2BusInternalChargedbacktoIPO__c,FY2CashoutCAPEX__c,FY2CashoutOPEX__c,FY2GDCosts__c,FY2NonIPOITCostsInternal__c,FY2OperServices__c,FY2PLImpact__c,FY2Regions__c,FY2RunningITCostsImpact__c,FY2SmallEnhancements__c,FY3AppServicesCustExp__c,FY3BusinessCashoutCAPEX__c,FY3BusinessCashoutOPEXOTC__c,FY3BusinessInternal__c,FY3BusinessRunningCostsImpact__c,FY3BusInternalChargedbacktoIPO__c,FY3CashoutCAPEX__c,FY3CashoutOPEX__c,FY3GDCosts__c,FY3NonIPOITCostsInternal__c,FY3OperServices__c,FY3PLImpact__c,FY3Regions__c,FY3RunningITCostsImpact__c,FY3SmallEnhancements__c,PastBudBusCashoutOPEXOTCCumulative__c,PastBudBusinessRunningCostCumulative__c,PastBudgetASCECumulative__c,PastBudgetBusCashoutCAPEXCumulative__c,PastBudgetBusinessInternalCumulative__c,PastBudgetCashoutCAPEXCumulative__c,PastBudgetCashoutOPEXCumulative__c,PastBudgetGDCostsCumulative__c,PastBudgetPLImpactCumulative__c,PastBudgetRegionsCumulative__c,PastBudgetSmallEnhancementsCumulative__c,PastBudIntChargedBacktoIPOCumulative__c,PastBudNonIPOITCostsIntCumulative__c,PastBudOperServicesCumulative__c,PastBudRunningITCostsImpCumulative__c,TECHActiveBudgetCreatedDateTime__c,TECHTotalPlannedITCosts__c,CurrencyIsoCode FROM PRJ_ProjectReq__c where Id in :budgetMap.keySet()])
        {
            if(!prjMap.containsKey(prj.Id))
                prjMap.put(prj.Id,prj);
        }
        
        for(Id projId:budgetMap.keySet())
        {           
            PRJ_ProjectReq__c prj = prjMap.get(projId);
            SObject sobjProject = (SObject)prj;
            isUpdatedProject = false;
            for(Budget__c bud:budgetMap.get(projId))
            {
                if(bud.Active__c)
                {            
                    SObject sobjBud = (SObject)bud;
                    for(DMTProjectRequestCostItemFields__c csInstance:csMap.values())
                    {
                        sobjProject.put(csInstance.ProjectRequestAPIName__c,sobjBud.get(csInstance.BudgetAPIName__c));
                    }
                    prj.TECHActiveBudgetCreatedDateTime__c = bud.CreatedDate;    
                    prj.CurrencyIsoCode = bud.CurrencyIsoCode;            
                    isUpdatedProject = true;
                }
            }
            if(isUpdatedProject)
                lstUpdateProjectRequest.add(prj);
            
        }
        
        if(lstUpdateProjectRequest.size()>0)
        {
            try
            {
                Database.SaveResult[] results = Database.update(lstUpdateProjectRequest,false);
                if (results != null)
                {
                    for (Database.SaveResult result : results) 
                    {
                        if (!result.isSuccess()) 
                        {
                            Database.Error[] errs = result.getErrors();
                            for(Database.Error err : errs)
                            System.debug(err.getStatusCode() + ' - ' + err.getMessage());
 
                        }
                    }
                }
            }
            catch(Exception exp)
            {
                System.debug(exp.getTypeName() + ' - ' + exp.getCause() + ': ' + exp.getMessage());
            } 
        }    
        END Modified by Srikant on Jan release 2012 */          
    }
    /*
        NOV Release Change - Srikant Joshi
        Proposal to create absolute values, i.e.,
        15 Fields for Cost Type, EX:- IT Cashout CAPEX 2012, P & L Impact 2012.
        1  Field for Total of 14 fields Excluding P&L Cost Type – Total Cost Forecast 2012.
        Populate the above Fields with Value 0 as the ONLY active Cost Forecast is being deleted.
    */
    public static void updateProjectRequestFieldsonDelete(List<Budget__c> budgetListToProcessCONDITION1)
    {
        list<PRJ_ProjectReq__c> lstProjs = new list<PRJ_ProjectReq__c>{};
        Integer intCurrentYear = Integer.Valueof(System.Label.DMTYearValue);
        System.debug('--DeleteProjectReq'+budgetListToProcessCONDITION1);
        
        Try{
            for(Budget__c bud:budgetListToProcessCONDITION1){
                PRJ_ProjectReq__c sobjProjects = new PRJ_ProjectReq__c(id=bud.ProjectReq__c);
                for(DMTBudgetValueToProjectRequest__c csInstance:DMTBudgetValueToProjectRequest__c.getAll().values()){
                    for(Integer BYear = intCurrentYear; BYear<intCurrentYear + 4;BYear++){
                        System.debug('--DeleteProject'+sobjProjects);
                            sobjProjects.put(csInstance.ProjectRequestFieldAPIName__c+String.Valueof(BYear).right(2)+'__c',0);
                    }
                }
                lstProjs.add(sobjProjects);
            }
            update lstProjs;
        }
        Catch(Exception e){
            System.debug('Error:'+e);
        }
    }
    
    public class TestException extends Exception{}
    
}