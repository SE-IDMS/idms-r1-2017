/*
    Pooja Suresh
    Release     : Jul16 Release (BR-10118) - 
    Test Class for adding external sales responsible to account team : AP_UpdateExternalSalesAgent
 */
@isTest
private class AP_UpdateExternalSalesAgent_TEST
{
    @testSetup static void createTestData()
    {
        Id agentProfileId = [SELECT id FROM Profile WHERE Name='SE - Agent (Global - Community)'].id;
        Id agentPermSetId = [SELECT Id FROM PermissionSet WHERE Name='SA_Sales_Agent_Global' limit 1].Id;
        
        Country__c country = Utils_TestMethods.createCountry();
        country.CountryCode__c='IT';
        insert country;
        
        StateProvince__c stateProv1 = new StateProvince__c(Name='Padova', Country__c=country.Id, CountryCode__c='IT', StateProvinceCode__c='PD');
        insert stateProv1;        
        StateProvince__c stateProv2 = new StateProvince__c(Name='Mantova', Country__c=country.Id, CountryCode__c='IT', StateProvinceCode__c='MN');
        insert stateProv2;
        
        Account communityAccount = Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
        communityAccount.City__c='test city';
        communityAccount.Street__c='test street';
        communityAccount.ZipCode__c='34534';
        insert communityAccount;
        
        Contact communityContact1 = new Contact(FirstName='Test',LastName='Agent',AccountId=communityAccount.Id);
        communityContact1.Country__c=country.id;
        communityContact1.Email='test@se.agent';
        insert communityContact1;

        Contact communityContact2 = new Contact(FirstName='Test',LastName='Agent',AccountId=communityAccount.Id);
        communityContact2.Country__c=country.id;
        communityContact2.Email='test@se.agent';
        insert communityContact2;
       
        AccountOwnershipRule__c rule1 = new AccountOwnershipRule__c(ClassLevel1__c='RT', Country__c=country.id, County__c='ES43', AccountOwner__c=UserInfo.getUserId(), Functionality__c='EAO');  // For StateCode=PD
        insert rule1;
        AccountOwnershipRule__c rule2 = new AccountOwnershipRule__c(ClassLevel1__c='RT', Country__c=country.id, County__c='ES48', AccountOwner__c=UserInfo.getUserId(), Functionality__c='EAO');  // For StateCode=MN
        insert rule2;
                       
        User agent1 = new User(alias = 'test123', email='test123@noemail.com', emailencodingkey='UTF-8', lastname='Agent1', languagelocalekey='en_US',
                localesidkey='en_US', profileid = agentProfileId, country='IT', IsActive=true, ContactId = communityContact1.Id,
                timezonesidkey='America/Los_Angeles', username='sesa123321@noemail.com');
        insert agent1;

        User agent2 = new User(alias = 'test456', email='test456@noemail.com', emailencodingkey='UTF-8', lastname='Agent2', languagelocalekey='en_US',
                localesidkey='en_US', profileid = agentProfileId, country='IT', IsActive=true, ContactId = communityContact2.Id,
                timezonesidkey='America/Los_Angeles', username='sesa456654@noemail.com');
        insert agent2;
        
        System.runAs(new User(Id = UserInfo.getUserId()))
        {
            PermissionSetAssignment agentAssignPermSet = new PermissionSetAssignment(PermissionSetId = agentPermSetId, AssigneeId = agent1.Id);
            insert agentAssignPermSet;
            
            List<CS_StateCountyMapping__c> custSett = new List<CS_StateCountyMapping__c>();
            custSett.add(new CS_StateCountyMapping__c(Name='PD',County__c='ES43'));
            custSett.add(new CS_StateCountyMapping__c(Name='MN',County__c='ES48'));
            insert custSett;
            System.debug('custSett---->>>'+custSett);
        }
                
        System.runAs(agent1)
        {
            Account agentAcc1 = Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
            agentAcc1.City__c='test city111';
            agentAcc1.Street__c='test street111';
            agentAcc1.ZipCode__c='34534';
            agentAcc1.StateProvince__c=stateProv1.id;
            agentAcc1.ClassLevel1__c='RT';
            insert agentAcc1;
        }
    }
    
    static testmethod void myUnitTest()
    {
        Account agentAcc1 = [SELECT Id, ExternalSalesResponsible__c FROM Account WHERE billingCountry = 'IT' and ExternalSalesResponsible__c != null Limit 1];
        agentAcc1.ExternalSalesResponsible__c = userInfo.getUserId();
        update agentAcc1;
    }

}