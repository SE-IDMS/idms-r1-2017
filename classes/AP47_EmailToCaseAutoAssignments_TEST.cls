/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 30/01/2012
    Description: Test class for AP47_EmailToCaseAutoAssignments
                 IF(ENABLE_ASSERT)System.assert can be disabled by editing the related custom setting record
*/   

@istest
private class AP47_EmailToCaseAutoAssignments_TEST
{
    private static final String SystemInterfaceUserName  = 'System Interface'; // 'System Interface';
    private static testMethod void testAutoAssignment() {    
        /*=======================================
            TEST DATA PREPARATION
        =======================================*/
        
        List<CS010_StatusSupportLevel__c> statusSupportLevelList = new List<CS010_StatusSupportLevel__c>();
        statusSupportLevelList.add(new CS010_StatusSupportLevel__c(Name='Open',LevelOfSupport__c=' Primary'));
        statusSupportLevelList.add(new CS010_StatusSupportLevel__c(Name='Open/ Level 1 Escalation', LevelOfSupport__c=' Primary'));
        statusSupportLevelList.add(new CS010_StatusSupportLevel__c(Name='Open/ Level 2 Escalation', LevelOfSupport__c=' Advanced'));
        statusSupportLevelList.add(new CS010_StatusSupportLevel__c(Name='Open/ Level 3 Escalation',LevelOfSupport__c=' Expert'));
        insert statusSupportLevelList;
        
        // Create all case categories as a Case classification 
        List<CaseClassification__c> caseCategories = new List<CaseClassification__c>();
        
        CaseClassification__c caseClassification1 = new CaseClassification__c(Category__c='1 - Pre-Sales Support');
        caseCategories.add(caseClassification1);

        CaseClassification__c caseClassification2 = new CaseClassification__c(Category__c='2 - Price and Availability');
        caseCategories.add(caseClassification2);
        
        CaseClassification__c caseClassification3 = new CaseClassification__c(Category__c='3 - Order Admin');
        caseCategories.add(caseClassification3);
        
        CaseClassification__c caseClassification4 = new CaseClassification__c(Category__c='4 - Post-Sales Tech Support');
        caseCategories.add(caseClassification4);
        
        CaseClassification__c caseClassification5 = new CaseClassification__c(Category__c='5 - Documentation Request');
        caseCategories.add(caseClassification5);
        
        CaseClassification__c caseClassification6 = new CaseClassification__c(Category__c='6 - General');
        caseCategories.add(caseClassification6);
        
        insert caseCategories;
               
        // Create 2 different CCC Countries
        
        List<Country__c> countryList = new List<Country__c>();
        
        Country__c testCountry_1 = Utils_TestMethods.createCountry();
        testCountry_1.Name='TEST COUNTRY_1';
        testCountry_1.CountryCode__c='IN';
        countryList.add(testCountry_1 );

        Country__c testCountry_2 = Utils_TestMethods.createCountry();
        testCountry_2.Name='TEST COUNTRY_2';
        testCountry_2.CountryCode__c='IT';
        countryList.add(testCountry_2);
        
        insert countryList;
        
        // Insert keywords for the first country
        List<EmailToCaseKeyword__c> keywordList_1 = new List<EmailToCaseKeyword__c>();

        List<String> keywordsInstructions = new List<String>();
        keywordsInstructions.add('2 Keywords with relevancy "very high"');
        keywordsInstructions.add('2 Keywords with relevancy "high"');
        keywordsInstructions.add('1 Keyword with relevancy "medium"');
        keywordsInstructions.add('1 Keyword with relevancy "low"');
        
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_1', CaseCategory__c=caseCategories[0].ID,  Priority__c=1, CCCountry__c = testCountry_1.ID, POCType__c = 'Email',TotalMatch__c = 0));
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_2', CaseCategory__c=caseCategories[1].ID,  Priority__c=2, CCCountry__c = testCountry_1.ID, POCType__c = 'Email',TotalMatch__c = 0));
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_3', CaseCategory__c=caseCategories[2].ID,  Priority__c=3, CCCountry__c = testCountry_1.ID, POCType__c = 'Email',TotalMatch__c = 0));
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_4', CaseCategory__c=caseCategories[3].ID,  Priority__c=4, CCCountry__c = testCountry_1.ID, POCType__c = 'Email',TotalMatch__c = 0));        
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_5', CaseCategory__c=caseCategories[4].ID,  Priority__c=5, CCCountry__c = testCountry_1.ID, POCType__c = 'Email',TotalMatch__c = 0)); 
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_6', CaseCategory__c=caseCategories[5].ID,  Priority__c=6, CCCountry__c = testCountry_1.ID, POCType__c = 'Email',TotalMatch__c = 0)); 

        List<DataBase.SaveResult> keyword_SR_1 = Database.insert(keywordList_1,false);        
        
        // Insert keywords for the second country        
        List<EmailToCaseKeyword__c> keywordList_2 = new List<EmailToCaseKeyword__c>();
        
        keywordList_2.add(new EmailToCaseKeyword__c(Name='TestWord2_1', CaseCategory__c=caseCategories[0].ID,  Priority__c=1, POCType__c = 'Email', CCCountry__c = testCountry_2.ID));
        keywordList_2.add(new EmailToCaseKeyword__c(Name='TestWord2_2', CaseCategory__c=caseCategories[1].ID,  Priority__c=2, POCType__c = 'Email', CCCountry__c = testCountry_2.ID));
        keywordList_2.add(new EmailToCaseKeyword__c(Name='TestWord2_3', CaseCategory__c=caseCategories[2].ID,  Priority__c=3, POCType__c = 'Email', CCCountry__c = testCountry_2.ID));
        keywordList_2.add(new EmailToCaseKeyword__c(Name='TestWord2_4', CaseCategory__c=caseCategories[3].ID,  Priority__c=4, POCType__c = 'Email', CCCountry__c = testCountry_2.ID));        
        keywordList_2.add(new EmailToCaseKeyword__c(Name='TestWord2_5', CaseCategory__c=caseCategories[4].ID,  Priority__c=5, POCType__c = 'Email', CCCountry__c = testCountry_2.ID)); 
        keywordList_2.add(new EmailToCaseKeyword__c(Name='TestWord2_6', CaseCategory__c=caseCategories[5].ID , Priority__c=6, POCType__c = 'Email', CCCountry__c = testCountry_2.ID)); 

        List<DataBase.SaveResult> keyword_SR_2 = Database.insert(keywordList_2,false);        
        
        /*=======================================
            TESTING AP47_EmailToCaseAutoAssignments.returnCaseCategory
        =======================================*/ 
        
        Test.startTest();
        
        // TEST 1. For TEST_COUNTRY_1 no keyword found neither in the subject nor the body
        Messaging.InboundEmail TestEmail1 = new Messaging.InboundEmail();
        TestEmail1.subject = 'TEST_SUBJECT';
        TestEmail1.PlainTextBody = 'TEST_BODY';
       // String resultCategory1 = AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail1, testCountry_1.ID);
        AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail1, testCountry_1.ID);
           
        // TEST 2. For TEST_COUNTRY_1 All keywords are in the subject, the one with the highest priority is returned
        Messaging.InboundEmail TestEmail2 = new Messaging.InboundEmail();
        TestEmail2.subject = 'TestWord1_1 TestWord1_2 TestWord1_3 TestWord1_4 TestWord1_5 TestWord1_6 TestWord1_7';
        TestEmail2.PlainTextBody = 'TEST_BODY';
       // String resultCategory2 = AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail2, testCountry_1.ID);
            AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail2, testCountry_1.ID);
                             
        // TEST 3. For TEST_COUNTRY_1 All keywords are in the subject, the one with the highest priority is returned
        Messaging.InboundEmail TestEmail3 = new Messaging.InboundEmail();
        TestEmail3.subject = 'TEST_SUBJECT';
        TestEmail3.PlainTextBody = 'TestWord2_1 TestWord2_2 TestWord2_3 TestWord2_4 TestWord2_5 TestWord2_6 TestWord2_7 \n TestWord1_1 TestWord1_2 TestWord1_3 TestWord1_4 TestWord1_5 TestWord1_6 TestWord1_7';
        //String resultCategory3 = AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail3, testCountry_1.ID);
        AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail3, testCountry_1.ID);

        // TEST 4. Only the last keyword for Country 2 is in the body
        Messaging.InboundEmail TestEmail4 = new Messaging.InboundEmail();
        TestEmail4.subject = 'TEST_SUBJECT';
        TestEmail4.PlainTextBody = 'TestWord2_6';
        //String resultCategory4 = AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail4, testCountry_2.ID);
        AP47_EmailToCaseAutoAssignments.returnCaseCategory(TestEmail4, testCountry_2.ID);

       /*=======================================
            TESTING AP47_EmailToCaseAutoAssignments.assignTeam
        =======================================*/ 
        User caseCreationUser = Utils_TestMethods.createStandardUser('TestUser');
        caseCreationUser.BypassVR__c =true;
        Insert caseCreationUser;        
        
        // Create account with customer classification level 1 = TT
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.ClassLevel1__c = 'TT';
        insert testAccount;
        System.debug('INSERTED_ACCOUNT ' + testAccount);
        // Create a contact with support level = VIC
        Contact testContact = Utils_TestMethods.createContact(testAccount.ID,'testContact');
        testContact.ServiceContractType__c = 'VIC';
        testContact.Pagent__c = caseCreationUser.id;
        testContact.PBAAgent__c = caseCreationUser.id;
        insert testContact;
         
        
        
        System.debug('INSERTED_CONTACT ' + testContact);
             
        List<CustomerCareTeam__c> CCTeamList = new List<CustomerCareTeam__c>();
        
        CustomerCareTeam__c TC2_Team_1 = new CustomerCareTeam__c(name='TC2_Team_1', CCCountry__c = testCountry_2.ID, LevelOfSupport__c = 'Primary', CaseOrigin__c = 'Email;Web' );
        CCTeamList.add(TC2_Team_1);
        CustomerCareTeam__c TC1_Team_1 = new CustomerCareTeam__c(name='TC1_Team_1', CCCountry__c = testCountry_1.ID, LevelOfSupport__c = 'Primary', CaseOrigin__c = 'Email;Web' );
        CCTeamList.add(TC1_Team_1);
        CustomerCareTeam__c TC1_Team_2 = new CustomerCareTeam__c(name='TC1_Team_2', CCCountry__c = testCountry_1.ID, LevelOfSupport__c = 'Primary', CaseOrigin__c = 'Email;Web' );
        CCTeamList.add(TC1_Team_2);
        CustomerCareTeam__c TC1_Team_3 = new CustomerCareTeam__c(name='TC1_Team_3', CCCountry__c = testCountry_1.ID, LevelOfSupport__c = 'Primary', CaseOrigin__c = 'Email;Web' );
        CCTeamList.add(TC1_Team_3);
        CustomerCareTeam__c TC1_Team_4 = new CustomerCareTeam__c(name='TC1_Team_4', CCCountry__c = testCountry_1.ID, LevelOfSupport__c = 'Primary', CustomerClassifications__c = 'TT', SupportClassification__c='VIC', CaseOrigin__c = 'Email;Web');
        CCTeamList.add(TC1_Team_4);
        CustomerCareTeam__c TC1_Team_5 = new CustomerCareTeam__c(name='TC1_Team_5', CCCountry__c = testCountry_1.ID, LevelOfSupport__c = 'Primary', CustomerClassifications__c = 'TT', CaseOrigin__c = 'Email;Web');
        CCTeamList.add(TC1_Team_5);
        CustomerCareTeam__c TC1_Team_6 = new CustomerCareTeam__c(name='TC1_Team_6', CCCountry__c = testCountry_1.ID, LevelOfSupport__c = 'Primary', SupportClassification__c='VIC', CaseOrigin__c = 'Email;Web');
        CCTeamList.add(TC1_Team_6);
        
        insert CCTeamList;
        
        List<TeamCaseJunction__c> CCTeamCategory = new List<TeamCaseJunction__c>();
        
        // Add Cagories to CC teams 
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_1.ID, CaseClassification__c = caseCategories[0].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC2_Team_1.ID, CaseClassification__c = caseCategories[1].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_1.ID, CaseClassification__c = caseCategories[1].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_3.ID, CaseClassification__c = caseCategories[2].ID)); 
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_4.ID, CaseClassification__c = caseCategories[2].ID)); 
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_5.ID, CaseClassification__c = caseCategories[3].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_1.ID, CaseClassification__c = caseCategories[3].ID)); 
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC2_Team_1.ID, CaseClassification__c = caseCategories[4].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = TC1_Team_6.ID, CaseClassification__c = caseCategories[4].ID));
         
        insert CCTeamCategory; 
        
        Map<String,Case> testCaseMap = new Map<String,Case>();
        testCaseMap.put('Category not covered.', new Case(CCCountry__c=testCountry_1.ID, SupportCategory__c=caseCategories[5].category__c));
        testCaseMap.put('No Support category.', new Case(CCCountry__c=testCountry_1.ID));
        testCaseMap.put('No Country.', new Case(SupportCategory__c=caseCategories[1].category__c));
        testCaseMap.put('Category only country 1.', new Case(SupportCategory__c=caseCategories[1].category__c, CCCountry__c=testCountry_1.ID));
        testCaseMap.put('Category only country 2.', new Case(SupportCategory__c=caseCategories[1].category__c, CCCountry__c=testCountry_2.ID));
        testCaseMap.put('Contact and account.', new Case(AccountID=testAccount.ID, ContactID=testContact.ID, SupportCategory__c=caseCategories[2].category__c, CCCountry__c=testCountry_1.ID));
        testCaseMap.put('Account only.', new Case(AccountID=testAccount.ID, ContactID=testContact.ID, SupportCategory__c=caseCategories[3].category__c, CCCountry__c=testCountry_1.ID));
        testCaseMap.put('Contact only.', new Case(AccountID=testAccount.ID, ContactID=testContact.ID, SupportCategory__c=caseCategories[4].category__c, CCCountry__c=testCountry_1.ID));
        
        AP47_EmailToCaseAutoAssignments.assignTeam(testCaseMap.values(),false);
        
        Test.stopTest();

    }
    private static testMethod void test_KeywordMatch() {    
        String customerCareUSAPOC1EMailAddress   = 'poc.usacustomercare1234@bridge-fo.com';
        String fromAddressKnownContact1USA   =   'contact-usa@hotmail.com';
        
        List<CaseClassification__c> caseCategories = new List<CaseClassification__c>();
        CaseClassification__c caseClassification1 = new CaseClassification__c(Category__c='Product Selection & Services');
        caseCategories.add(caseClassification1);

        CaseClassification__c caseClassification2 = new CaseClassification__c(Category__c='Pricing and Availability');
        caseCategories.add(caseClassification2);
        
        insert caseCategories;
               
        List<Country__c> countryList = new List<Country__c>();
        
        Country__c objPOCCountry1 = Utils_TestMethods.createCountry();
        objPOCCountry1.Name='TestCountry2'; 
        objPOCCountry1.CountryCode__c='IT';
        countryList.add(objPOCCountry1);
            
        insert countryList;
        
            
        BusinessRiskEscalationEntity__c objPOC1Organization = Utils_TestMethods.createBRE('USA1POCEntity','USA1POCSubEntity','USA1POCLocation','Customer Care Center');
        Database.insert(objPOC1Organization);
        
        
        PointOfContact__c objUSAPOC1 = Utils_TestMethods.createPointOfContact(objPOC1Organization.Id, objPOCCountry1.Id, customerCareUSAPOC1EMailAddress,'Email');
        objUSAPOC1.Priority1Routing__c = 'Account Preferred Team';
        Database.insert(objUSAPOC1);
        System.debug('POC ID:' + objUSAPOC1.Id);
        
        
        List<EmailToCaseKeyword__c> keywordList_1 = new List<EmailToCaseKeyword__c>();

        
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_1', CaseCategory__c=caseCategories[0].ID,  PointOfContact__c=objUSAPOC1.Id,Priority__c=1, CCCountry__c = objPOCCountry1.ID, POCType__c = 'Email', TotalMatch__c = 0));
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWord1_2', CaseCategory__c=caseCategories[1].ID,  PointOfContact__c=objUSAPOC1.Id,Priority__c=2, CCCountry__c = objPOCCountry1.ID, POCType__c = 'Email', TotalMatch__c = 0));
        
        List<DataBase.SaveResult> keyword_SR_1 = Database.insert(keywordList_1,false);        
        
        Test.startTest();
        
        Messaging.InboundEmail          objInboundEmailForKeyWord           =   new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope       objInboundEnvelopeForKeyWord        =   new Messaging.InboundEnvelope();
        
        objInboundEmailForKeyWord.Subject = 'My TestWord1_1 Testing Email To Case Keyword Match';
        objInboundEmailForKeyWord.fromname = 'Contact USA';
        objInboundEmailForKeyWord.fromAddress = fromAddressKnownContact1USA;
        objInboundEmailForKeyWord.ToAddresses = new List<String>();
        objInboundEmailForKeyWord.CCAddresses = new List<String>();
        objInboundEmailForKeyWord.plaintextbody =  Utils_TestMethods.generateRandomString(100);

        AP47_EmailToCaseAutoAssignments.returnCaseCategory(objInboundEmailForKeyWord,objUSAPOC1.Id);        
        
        Test.stopTest();
    }
    private static testMethod void test_WebPOCPriority2Routing() {    
        
        List<CaseClassification__c> caseCategories = new List<CaseClassification__c>();
        
        CaseClassification__c caseClassification1 = new CaseClassification__c(Category__c='Product Selection & Services');
        caseCategories.add(caseClassification1);

        CaseClassification__c caseClassification2 = new CaseClassification__c(Category__c='Pricing and Availability');
        caseCategories.add(caseClassification2);
        
        insert caseCategories;
               
        List<Country__c> countryList = new List<Country__c>();
        
        Country__c objWebPOCCountry = Utils_TestMethods.createCountry();
        objWebPOCCountry.Name='TestCountry2'; 
        objWebPOCCountry.CountryCode__c='GB';
        countryList.add(objWebPOCCountry);
            
        insert countryList;
        
            
        BusinessRiskEscalationEntity__c objWebPOCOrganization = Utils_TestMethods.createBRE('USA1POCEntity','USA1POCSubEntity','USA1POCLocation','Customer Care Center');
        Database.insert(objWebPOCOrganization);
        
        
        PointOfContact__c objWebPOC = Utils_TestMethods.createPointOfContact(objWebPOCOrganization.Id, objWebPOCCountry.Id, 'WebPortalTeam','Web');
        objWebPOC.Priority1Routing__c = 'Contact/Account Preferred Team';
        objWebPOC.Priority2Routing__c = 'Case classification match';
        
        Database.insert(objWebPOC);
        
        List<CustomerCareTeam__c> CCTeamList = new List<CustomerCareTeam__c>();
        
        CustomerCareTeam__c WebPOCTeam1 = new CustomerCareTeam__c(name='WebPOCTeam_1', DefaultSupportedOrganization__c = objWebPOCOrganization.Id, CCCountry__c = objWebPOCCountry.ID, LevelOfSupport__c = 'Primary', CaseOrigin__c = 'Email;Web',CustomerClassifications__c='RT');
        CustomerCareTeam__c WebPOCTeam2 = new CustomerCareTeam__c(name='WebPOCTeam_1', DefaultSupportedOrganization__c = objWebPOCOrganization.Id, CCCountry__c = objWebPOCCountry.ID, LevelOfSupport__c = 'Primary', CaseOrigin__c = 'Email;Web',SupportClassification__c='VIC');
        CCTeamList.add(WebPOCTeam1);
        CCTeamList.add(WebPOCTeam2); 
        insert CCTeamList;
        
        List<TeamCaseJunction__c> CCTeamCategory = new List<TeamCaseJunction__c>();
        
        // Add Cagories to CC teams 
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = WebPOCTeam1.ID, CaseClassification__c = caseCategories[0].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = WebPOCTeam1.ID, CaseClassification__c = caseCategories[1].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = WebPOCTeam2.ID, CaseClassification__c = caseCategories[0].ID));
        CCTeamCategory.add(new TeamCaseJunction__c(CCTeam__c = WebPOCTeam2.ID, CaseClassification__c = caseCategories[1].ID));
         
        insert CCTeamCategory; 

        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objWebPOCCountry.Id;
        objAccount.ClassLevel1__c='RT';
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        objContact.ServiceContractType__c = 'VIC';
        Database.insert(objContact);

        Test.startTest();
        

        Case objCase = new Case(Subject='This is a test for Web POC', Origin='Web', SupportCategory__c = 'Pricing and Availability', PortalId__c = 'WebPortalTeam',SuppliedCountry__c='TestCountry2');
        insert objCase;
        
        Test.stopTest();
    }
    
    private static testMethod void test_KeywordMatchForWebCase() {    
        //String customerCareUSAPOC1EMailAddress   = 'poc.usacustomercare1234@bridge-fo.com';
        //String fromAddressKnownContact1USA   =   'contact-usa@hotmail.com';
        
        List<CaseClassification__c> caseCategories = new List<CaseClassification__c>();
        CaseClassification__c caseClassification1 = new CaseClassification__c(Category__c='Product Selection & Services');
        caseCategories.add(caseClassification1);

        CaseClassification__c caseClassification2 = new CaseClassification__c(Category__c='Pricing and Availability');
        caseCategories.add(caseClassification2);
        
        insert caseCategories;
               
        List<Country__c> countryList = new List<Country__c>();
        
        Country__c objWebPOCCountry = Utils_TestMethods.createCountry();
        objWebPOCCountry.Name='TestWebCountry'; 
        objWebPOCCountry.CountryCode__c='IT';
        countryList.add(objWebPOCCountry);
            
        insert countryList;
        
            
        BusinessRiskEscalationEntity__c objWebPOCOrganization = Utils_TestMethods.createBRE('WebPOCEntity','WebPOCSubEntity','WebPOCLocation','Customer Care Center');
        Database.insert(objWebPOCOrganization);
        
        
        PointOfContact__c objWebPOC = Utils_TestMethods.createPointOfContact(objWebPOCOrganization.Id, objWebPOCCountry.Id, 'TestPortalIDforKeyWord','Web');
        objWebPOC.Priority2Routing__c = 'Keyword match';
        Database.insert(objWebPOC);
        
        
        //TestPortalIDforKeyWord
        List<EmailToCaseKeyword__c> keywordList_1 = new List<EmailToCaseKeyword__c>();

        
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWebKeyWord_1', CaseCategory__c=caseCategories[0].ID,  PointOfContact__c=objWebPOC.Id,Priority__c=1, CCCountry__c = objWebPOCCountry.ID, POCType__c = 'Web', TotalMatch__c = 0));
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWebKeyWord_2', CaseCategory__c=caseCategories[1].ID,  PointOfContact__c=objWebPOC.Id,Priority__c=2, CCCountry__c = objWebPOCCountry.ID, POCType__c = 'Web', TotalMatch__c = 0));
        
        List<DataBase.SaveResult> keyword_SR_1 = Database.insert(keywordList_1,false);        
        
        Test.startTest();
        
        AP47_EmailToCaseAutoAssignments.returnWebFormKeywordMatch('TestWebKeyWord_1 My Test', objWebPOC.Id,caseCategories[0].Category__c);
        
        Test.stopTest();
    }

    private static testMethod void test_PoCMatchForWebCase() {    
        
        List<CaseClassification__c> caseCategories = new List<CaseClassification__c>();
        CaseClassification__c caseClassification1 = new CaseClassification__c(Category__c='Product Selection & Services');
        caseCategories.add(caseClassification1);

        CaseClassification__c caseClassification2 = new CaseClassification__c(Category__c='Pricing and Availability');
        caseCategories.add(caseClassification2);
        
        insert caseCategories;
               
        List<Country__c> countryList = new List<Country__c>();
        
        Country__c objWebPOCCountry = Utils_TestMethods.createCountry();
        objWebPOCCountry.Name='TestWebCountry'; 
        objWebPOCCountry.CountryCode__c='IT';
        countryList.add(objWebPOCCountry);
            
        insert countryList;
        
            
        BusinessRiskEscalationEntity__c objWebPOCOrganization = Utils_TestMethods.createBRE('WebPOCEntity','WebPOCSubEntity','WebPOCLocation','Customer Care Center');
        Database.insert(objWebPOCOrganization);
        
        
        PointOfContact__c objWebPOC = Utils_TestMethods.createPointOfContact(objWebPOCOrganization.Id, objWebPOCCountry.Id, 'TestPortalIDforKeyWord','Web');
        objWebPOC.Priority2Routing__c = 'Keyword match';
        Database.insert(objWebPOC);
        
        
        //TestPortalIDforKeyWord
        List<EmailToCaseKeyword__c> keywordList_1 = new List<EmailToCaseKeyword__c>();

        CustomerCareTeam__c webCCTeam = new CustomerCareTeam__c(name='webCCTeam_1', CCCountry__c = objWebPOCCountry.ID, LevelOfSupport__c = 'Advanced', CaseOrigin__c = 'Email;Web' );
        insert webCCTeam;
        

        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWebKeyWord_1', CaseCategory__c=caseCategories[0].ID,  PointOfContact__c=objWebPOC.Id,Priority__c=1, CCCountry__c = objWebPOCCountry.ID, POCType__c = 'Web', TotalMatch__c = 0, CaseTeam__c = webCCTeam.Id));
        keywordList_1.add(new EmailToCaseKeyword__c(Name='TestWebKeyWord_2', CaseCategory__c=caseCategories[1].ID,  PointOfContact__c=objWebPOC.Id,Priority__c=2, CCCountry__c = objWebPOCCountry.ID, POCType__c = 'Web', TotalMatch__c = 0, CaseTeam__c = webCCTeam.Id));
        
        List<DataBase.SaveResult> keyword_SR_1 = Database.insert(keywordList_1,false);        
        User SystemInterfaceUser = [select Id from User where Name = :SystemInterfaceUserName][0];
        Test.startTest();
        System.runAs(SystemInterfaceUser){
        	Case objWebCase = new Case();
            objWebCase.OwnerId = SystemInterfaceUser.Id;
            objWebCase.Description = 'TestWebKeyWord_1 My Test and this is again a test';
            objWebCase.SupportCategory__c = caseCategories[0].Category__c;
            objWebCase.SuppliedCountry__c = 'TestWebCountry';
            objWebCase.Origin = 'Web';
            objWebCase.PortalId__c = 'TestPortalIDforKeyWord';
            insert objWebCase;    
        }
                
        Test.stopTest();
    }
}