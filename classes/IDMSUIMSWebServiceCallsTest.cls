/**
Description: This covers IDMSUIMSWebServiceCalls class
Release: Sept 2016
**/ 

@isTest
class IDMSUIMSWebServiceCallsTest{
    //Test 1: This method is used to test activate identity
    static testmethod void testActivateIdentity(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.ActivateIdentity('Welcome1','1234');
            iDMSUIMSWebServiceCallsClass.ActivateIdentity('','1234');
            iDMSUIMSWebServiceCallsClass.ActivateIdentity('Welcome1','');
            iDMSUIMSWebServiceCallsClass.ActivateIdentity('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    //Test 2: This method is used to test activate identity in UIMS
    static testmethod void testActivateIdentityUIMS(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.ActivateIdentityUIMS('Welcome1','1234');
            IDMSUIMSWebServiceCalls.ActivateIdentityUIMS('','1234');
            IDMSUIMSWebServiceCalls.ActivateIdentityUIMS('Welcome1','');
            IDMSUIMSWebServiceCalls.ActivateIdentityUIMS('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //This method is used to test create company in UIMS
    static testmethod void testCreateCompanyUIMS(){
        try{
            test.Starttest();
            User userObj1 = new User(alias = 'user', email='test2_6Sep16' + '@cognizant.com', 
                                     emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                     timezonesidkey='Europe/London', username='test2_6Sep16' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='test26Sep16',IDMS_Registration_Source__c = 'Test',
                                     IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                     IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500');
            
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.CreateCompanyUIMS(userObj1);
            iDMSUIMSWebServiceCallsClass.CreateCompanyUIMS(new User());
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 4: This method is used to test create uims user 
    static testmethod void testCreateUserUIMS(){
        try{
            test.Starttest();
            User userObj1 = new User(alias = 'user', email='test2_6Sep16' + '@cognizant.com', 
                                     emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                     timezonesidkey='Europe/London', username='test2_6Sep16' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='test26Sep16',IDMS_Registration_Source__c = 'Test',
                                     IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                     IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500');
            
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.CreateUserUIMS(userObj1);
            iDMSUIMSWebServiceCallsClass.CreateUserUIMS(new User());
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 5: This method is used to test search user uims
    static testmethod void testSearchUserUIMS(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.SearchUserUIMS('testUIMSSearch@accenture.com');
            iDMSUIMSWebServiceCallsClass.SearchUserUIMS('test2_6Sep16@cognizant.com');
            iDMSUIMSWebServiceCallsClass.SearchUserUIMS('test99_6Sep16@cognizant.com');
            iDMSUIMSWebServiceCallsClass.SearchUserUIMS('test2_6Sep169cognizant.com');
            iDMSUIMSWebServiceCallsClass.SearchUserUIMS('');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 6: This method is used to test webservice calls
    static testmethod void testIDMSUIMSWebServiceCalls1(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.clone();
            iDMSUIMSWebServiceCallsClass.SearchUserUIMSReset('test2_6Sep16@cognizant.com');
            iDMSUIMSWebServiceCallsClass.SearchUserUIMSReset('test2_6Sep168cognizant.com');
            iDMSUIMSWebServiceCallsClass.SearchUserUIMSReset('');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 7: This method is used test request email change
    static testmethod void testrequestEmailChangeUIMS(){
        try{
            test.Starttest();
            UserRole userRole=[SELECT Id,Name FROM UserRole WHERE Name = 'CEO'];
            User userObj2 = new User(alias = 'user', email='test3_6Sep16' + '@cognizant.com', 
                                     emailencodingkey='UTF-8', lastname='test3_6Sep16', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                     timezonesidkey='Europe/London', username='test3_6Sep16' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='test36Sep16',IDMS_Registration_Source__c = 'Test',
                                     IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                     IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500',UserRoleId=userRole.Id);
            insert userObj2;
            System.assert(userObj2!=null);       
            IDMSUIMSWebServiceCalls.requestEmailChangeUIMS('test98_6Sep16@cognizant.com',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestEmailChangeUIMS('test98_6Sep16cognizant.com',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestEmailChangeUIMS('',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestEmailChangeUIMS('test98_6Sep16@cognizant.com',userObj2.Id,'');
            IDMSUIMSWebServiceCalls.requestEmailChangeUIMS('test98_6Sep16@cognizant.com','','IDMS');
            IDMSUIMSWebServiceCalls.requestEmailChangeUIMS('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 8:This method is used to test request email change in uims
    static testmethod void testrequestToEmailChangeUIMS(){
        try{
            test.Starttest();
            UserRole userRole=[SELECT Id,Name FROM UserRole WHERE Name = 'CEO'];
            User userObj2 = new User(alias = 'user', email='test3_6Sep16' + '@cognizant.com', 
                                     emailencodingkey='UTF-8', lastname='test3_6Sep16', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                     timezonesidkey='Europe/London', username='test3_6Sep16' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='test36Sep16',IDMS_Registration_Source__c = 'Test',
                                     IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                     IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500',UserRoleId=userRole.Id);
            insert userObj2;
            System.assert(userObj2!=null);       
            
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('test98_6Sep16@cognizant.com',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('test98_6Sep167cognizant.com',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('test3_6Sep16@cognizant.com',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('',userObj2.Id,'IDMS');
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('test98_6Sep16@cognizant.com',userObj2.Id,'');
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('test98_6Sep16@cognizant.com','','IDMS');
            IDMSUIMSWebServiceCalls.requestToEmailChangeUIMS('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 9: this method is used to test reset password
    static testmethod void testresetPassword(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            UserRole userRole=[SELECT Id,Name FROM UserRole WHERE Name = 'CEO'];
            User userObj2 = new User(alias = 'user', email='test3_6Sep16' + '@cognizant.com', 
                                     emailencodingkey='UTF-8', lastname='test3_6Sep16', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                     timezonesidkey='Europe/London', username='test3_6Sep16' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='test36Sep16',IDMS_Registration_Source__c = 'Test',
                                     IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                     IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500',UserRoleId=userRole.Id);
            insert userObj2;
            System.assert(userObj2!=null);       
            iDMSUIMSWebServiceCallsClass.resetPassword(userObj2.FederationIdentifier,'IDMS');
            iDMSUIMSWebServiceCallsClass.resetPassword(userObj2.FederationIdentifier,'');
            iDMSUIMSWebServiceCallsClass.resetPassword('','IDMS');
            iDMSUIMSWebServiceCallsClass.resetPassword('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    
    
    //Test 10: This method is used to test uims set password
    static testmethod void testuimsSetPassword(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.uimsSetPassword('Welcome1','1234');
            iDMSUIMSWebServiceCallsClass.uimsSetPassword('','1234');
            iDMSUIMSWebServiceCallsClass.uimsSetPassword('Welcome1','');
            iDMSUIMSWebServiceCallsClass.uimsSetPassword('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 11: This method is used to test uims set password
    static testmethod void testuimsSetPasswordUser(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.uimsSetPasswordUser('Welcome1','1234');
            IDMSUIMSWebServiceCalls.uimsSetPasswordUser('Welcome1','');
            IDMSUIMSWebServiceCalls.uimsSetPasswordUser('','1234');
            IDMSUIMSWebServiceCalls.uimsSetPasswordUser('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 12: This method is used to test uims update email
    static testmethod void testuimsUpdateEmail(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.uimsUpdateEmail('Welcome1','1234');
            iDMSUIMSWebServiceCallsClass.uimsUpdateEmail('Welcome1','');
            iDMSUIMSWebServiceCallsClass.uimsUpdateEmail('','1234');
            iDMSUIMSWebServiceCallsClass.uimsUpdateEmail('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    //Test 12: This method is used to test  update user ail
    static testmethod void testuimsUpdateUserAILGrant(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant('Welcome1','1234','test1-test2-test3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant('','1234','test1-test2-test3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant('Welcome1','','test1-test2-test3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant('Welcome1','1234','');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant('Welcome1','1234','t3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    //Test 13: This method is used to test  revoke user ail
    static testmethod void testuimsUpdateUserAILRevoke(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke('Welcome1','1234','test1-test2-test3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke('','1234','test1-test2-test3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke('Welcome1','','test1-test2-test3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke('Welcome1','1234','');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke('Welcome1','1234','st3');
            IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    //Test 14: This method is used to test  create with password
    static testmethod void testuimscreateIdentityWithPassword(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16@yopmail.com","MobilePhone": "9999999999","Phone":"123-456-7890","FirstName": "test","LastName":"user120","IDMS_Email_opt_in__c":"Y","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","CompanyName":"Cognizant" }','Welcome1','IDMS');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16@yopmail.com","MobilePhone": "9999999999","Phone":"123-456-7890","FirstName": "test","LastName":"user120","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","IDMSCompanyHeadquarters__c":true }','Welcome1','IDMS');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16yopmail.com","MobilePhone": "9999999999","Phone":"@#$%^&123-456-7890","FirstName": "test","LastName":"","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","IDMSCompanyHeadquarters__c":"NYC" }','Welcome1','IDMS');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16yopmail.com","MobilePhone": "9999999999","Phone":"@#$%^&123-456-7890","FirstName": "test","LastName":"","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","IDMSCompanyHeadquarters__c":"NYC" }','Welcome1','IDMS');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16yopmail.com","MobilePhone": "9999999999","Phone":"@#$%^&123-456-7890","FirstName": "test","LastName":"","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","IDMSCompanyHeadquarters__c":"NYC" }','','IDMS');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16yopmail.com","MobilePhone": "9999999999","Phone":"@#$%^&123-456-7890","FirstName": "test","LastName":"","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","IDMSCompanyHeadquarters__c":"NYC" }','Welcome1','');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('{"Email": "test4_9Sep16yopmail.com","MobilePhone": "9999999999","Phone":"@#$%^&123-456-7890","FirstName": "test","LastName":"","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku","IDMS_User_Context__c":"work","IDMSCompanyHeadquarters__c":"NYC" }','','');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('email1','','');
            IDMSUIMSWebServiceCalls.uimscreateIdentityWithPassword('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    //Test 15: This method is used to test  update with password
    static testmethod void testupdatePassword(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls iDMSUIMSWebServiceCallsClass=new IDMSUIMSWebServiceCalls();
            iDMSUIMSWebServiceCallsClass.updatePassword('Welcome1','Welcome2','1234');
            iDMSUIMSWebServiceCallsClass.updatePassword('','Welcome2','1234');
            iDMSUIMSWebServiceCallsClass.updatePassword('Welcome1','','1234');
            iDMSUIMSWebServiceCallsClass.updatePassword('Welcome1','Welcome2','');
            iDMSUIMSWebServiceCallsClass.updatePassword('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    //Test 16: This method is used to test  update with password UIMS
    static testmethod void testupdatePasswordUIMS(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.updatePasswordUIMS('Welcome1','Welcome2','1234');
            IDMSUIMSWebServiceCalls.updatePasswordUIMS('','Welcome2','1234');
            IDMSUIMSWebServiceCalls.updatePasswordUIMS('Welcome1','','1234');
            IDMSUIMSWebServiceCalls.updatePasswordUIMS('Welcome1','Welcome2','');
            IDMSUIMSWebServiceCalls.updatePasswordUIMS('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 17: This method is used to test  update with UIMS
    static testmethod void testupdateUIMSUser(){
        try{
            test.Starttest();
            UserRole userRole=[SELECT Id,Name FROM UserRole WHERE Name = 'CEO'];
            User userObj2 = new User(alias = 'user', email='test3_6Sep16' + '@cognizant.com', 
                                     emailencodingkey='UTF-8', lastname='test3_6Sep16', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = '00eA0000000uVHP', BypassWF__c = true,BypassVR__c = true,
                                     timezonesidkey='Europe/London', username='test3_6Sep16' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='test36Sep16',IDMS_Registration_Source__c = 'Test',
                                     IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                     IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='9538333500',UserRoleId=userRole.Id);
            insert userObj2;
            System.assert(userObj2!=null);       
            IDMSUIMSWebServiceCalls.updateUIMSUser('{"Id":"'+userObj2.Id+'","Email": "test4_6Sep16@yopmail.com","MobilePhone": "9999999999","Phone":"123-456-7890","FirstName": "test","LastName":"user120","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku" }','1234');
            IDMSUIMSWebServiceCalls.updateUIMSUser('{"Id":"008123456789876","Email": "test4_6Sep16@yopmail.com","MobilePhone": "9999999999","Phone":"123-456-7890","FirstName": "test","LastName":"user120","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku" }','1234');
            IDMSUIMSWebServiceCalls.updateUIMSUser('{"Id":"008123456789876","Email": "test4_6Sep16@yopmail.com","MobilePhone": "9999999999","Phone":"123-456-7890","FirstName": "test","LastName":"user120","IDMS_Email_opt_in__c":"Y","IDMS_User_Context__c":"@Home","Country":"IN","IDMS_PreferredLanguage__c":"en","DefaultCurrencyIsoCode": "USD","Street": "Bangalore","City":"Bangalore","PostalCode":"566066","IDMS_State__c":"Karnataka","IDMS_County__c":"IN","IDMS_POBox__c":"560076","FederationIdentifier":"test4_6Sep161","IDMS_AdditionalAddress__c":"Building A","IDMS_Registration_Source__c":"Heroku" }','');
            IDMSUIMSWebServiceCalls.updateUIMSUser('id1','');
            IDMSUIMSWebServiceCalls.updateUIMSUser('','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test 18: This method is used to test  password with SMS
    static testmethod void testsetPasswordWithSMS(){
        try{
            test.Starttest();
            IDMSUIMSWebServiceCalls.setPasswordWithSMS('709798','Welcome1','700u9u08y98hg98y98');
            IDMSUIMSWebServiceCalls.setPasswordWithSMS('709798','','700u9u08y98hg98y98');
            IDMSUIMSWebServiceCalls.setPasswordWithSMS('709798','Welcome1','');
            IDMSUIMSWebServiceCalls.setPasswordWithSMS('','Welcome1','700u9u08y98hg98y98');
            IDMSUIMSWebServiceCalls.setPasswordWithSMS('','','');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    
    
    
}