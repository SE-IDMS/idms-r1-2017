/********************************************************************************************************
    Author: Fielo Team
    Date: 05/03/2015
    Description: REST API that receives 2 parameters: fromTime and toTime in juliane format yyyyMMddHHmm
                 and returns a list of salesforce contact Ids of members that had feature update during 
                 that timeframe.
                 If no Contact Ids were found for that time frame an empty list is returned.
    Related Components: 
*********************************************************************************************************/
@RestResource(urlMapping='/RestUpdatedFeatureOnContacts/*') 
global class FieloPRM_REST_UpdatedFeatOnContacts{   

    /**
    * [getUpdatedFeatureOnContacts returns Contacts that were updated in a particular time frame]
    * @method   getUpdatedFeatureOnContacts
    * @Pre-conditions  
    * @Post-conditions 
    * @param    String         fromTime  [juliane format yyyyMMddHHmm]
    * @param    String         toTime    [juliane format yyyyMMddHHmm]
    * @return   List<String>             [list of salesforce Contact Ids]
    */ 
    @HttpGet
    global static List<String> getUpdatedFeatureOnContacts(){  
    
        String fromTime = RestContext.request.params.get('fromTime');
        String toTime = RestContext.request.params.get('toTime');
        
        return  FieloPRM_UTILS_Features.getUpdatedFeatureOnContacts(fromTime,toTime );  
    
    }  

}