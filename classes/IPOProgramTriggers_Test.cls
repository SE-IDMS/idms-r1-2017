@isTest
private class IPOProgramTriggers_Test {
    static testMethod void testIPOProgramTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPO1');
        System.runAs(runAsUser){
            IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC', Year__c=Label.ConnectYear);
            insert IPOp;
            IPO_Strategic_Milestone__c IPOmile=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA');
            insert IPOmile;
            IPO_Strategic_Deployment_Network__c C=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM');
            insert C;
            IPO_Strategic_Deployment_Network__c C1=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Business', Entity__c='ITB');
            insert C1;
            IPO_Strategic_Deployment_Network__c C2=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Operation Regions', Entity__c='NA', Champion__c = runAsUser.id);
            insert C2;
            
            Program_Progress_Snapshot__c PP1 = new Program_Progress_Snapshot__c(IPO_Program__c = IPOp.id,IPO_Program_Health_Q1__c = 'In Progress');
            insert PP1;
            
            Test.startTest();
            IPOp.Global_Functions__c='GM;S&I;HR;IPO';
            IPOp.Global_Business__c='ITB;power;industry';
            IPOp.Operation__c='NA;APAC;EMEAS';
                     
            update IPOp;
            IPOp.Global_Functions__c='S&I';
            IPOp.Global_Business__c='Industry';
            IPOp.Operation__c='EMEAS';
                      
            IPOmile.Global_Functions__c='Finance';
            IPOmile.Global_Business__c='Infrastructure';
            IPOmile.Operation_Regions__c='test';
            
            try{
            update IPOp;
            update IPOmile;
            
            }catch(DmlException d)
            {
            d.getMessage();
            }
            IPOp.Global_Functions__c='';
            IPOp.Global_Business__c='';
            IPOp.Operation__c='';
            try{
            update IPOp;
            }catch(DmlException e)
            {
            e.getMessage();
            }
            
            Test.stopTest();
            }
            }
            }