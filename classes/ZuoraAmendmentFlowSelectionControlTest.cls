@isTest (SeeAllData=true)
public with sharing class ZuoraAmendmentFlowSelectionControlTest {

    private static testMethod void ZuoraAmendmentFlowSelectionControlTestMethod() {

        ///*
        List<Country__c> countryList = [SELECT Id from Country__c WHERE Name = 'France'];

        Id countryId = countryList.get(0).Id;

        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=countryId, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');

        List<Account> accList = new List<Account>();
        accList.add(acc);
        insert accList;

        Zuora__CustomerAccount__c zBillingAccount1 = testObjectCreator.CreateBillingAccount('TestBA1', 'ContactName', acc.Id);

        zqu__Quote__c quote = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = '1234');
        List<zqu__Quote__c> quoteList = new List<zqu__Quote__c>();
        quoteList.add(quote);
        insert quoteList;

        zqu__ZProduct__c pr1 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='1234', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Primary', AddonSKUs__c='A.B.C.D.E.F.G.H.I.J.K');
        zqu__ZProduct__c pr2 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='5678', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Standalone');

        List<zqu__ZProduct__c> prList = new List<zqu__ZProduct__c>{pr1, pr2};
        insert prList;

        zqu__ProductRatePlan__c prp1 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='1234', zqu__ZProduct__c=pr1.Id, zqu__Description__c='description');
        zqu__ProductRatePlan__c prp2 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='5678', zqu__ZProduct__c=pr2.Id);

        List<zqu__ProductRatePlan__c> prpList = new List<zqu__ProductRatePlan__c>{prp1, prp2};
        insert prpList;

        ZuoraAmendmentFlowSelectionController zpsrc = new ZuoraAmendmentFlowSelectionController(new ApexPages.StandardController(quote));
        Test.startTest();
        zpsrc.onload();
        zpsrc.getItems();
        zpsrc.navigateBack();
        zpsrc.SelectedItem = 'ChangeProduct';
        zpsrc.next();
        zpsrc.SelectedItem = 'AddRemoveOption';
        //zpsrc.next();
        ZuoraAmendmentFlowSelectionController.appendMessage(ApexPages.Severity.WARNING, 'Quote not found.');
        ZuoraUtilities.updateVatIdInZuora(accList);

        Test.stopTest();
    }
}