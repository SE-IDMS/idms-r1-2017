//*********************************************************************************
// Class Name       :  VFC_GetCustomerInformation_TEST
// Purpose          : Test Class for  VFC_GetCustomerInformation
// Created by       : Vimal Karunakaran
// Date created     : 04th June 2013
// Modified by      :
// Date Modified    :
// Remarks          : 

@isTest
private with sharing class  VFC_GetCustomerInformation_TEST {

    static testMethod void verifyverifyCustomerCodeTest() {
        String SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c st = Utils_TestMethods.createStateProvince(country.id);
        insert st;
        
        list<Account> newAccounts = new list<Account>();
        
        
        Account account = Utils_TestMethods.createAccount();
        account.Phone = '986435';
        account.Country__c = country.id;
        account.StateProvince__c = st.id;
        account.LeadingBusiness__c = 'Buildings';
        account.ClassLevel1__c = 'Retailer';
        account.Type = 'Prospect';
        //account.CurrencyIsoCode ='IND';
        account.City__c = 'bangalore';
        account.ZipCode__c= '452234';
        account.Street__c= 'aSDASDASD';
        account.POBox__c = '122344';
        newAccounts.add(account);
        system.debug('#######'+account);
        
        Account account2 = Utils_TestMethods.createAccount();
        account2.Phone = '426272';
        account2.Country__c = country.id;
        account2.StateProvince__c = st.id;
        account2.LeadingBusiness__c = 'Buildings';
        account2.ClassLevel1__c = 'Retailer';
        account2.Type = 'Prospect';
        //account.CurrencyIsoCode ='IND';
        account2.City__c = 'bangalore';
        account2.ZipCode__c= '452234';
        account2.Street__c= 'aSDASDASD';
        account2.POBox__c = '122344';
        newAccounts.add(account2); 
        system.debug('#######' + account2);
        
        
        
        insert newAccounts;
        
        LegacyAccount__c objLegacyAccount = Utils_TestMethods.createLegacyAccount(account.Id);
        objLegacyAccount.LegacyNumber__c = '6454637';
        objLegacyAccount.LegacyName__c  = 'CN_SAP'; 
        
        insert objLegacyAccount;
                
        PageReference pageRef = Page.VFP_GetCustomerInformation;
        Test.setCurrentPage(pageRef);
        
         VFC_GetCustomerInformation controller = new  VFC_GetCustomerInformation();
        //No customerCode 
        controller.verifyCustomerCode();
        //No results
        ApexPages.currentPage().getParameters().put('customerCode', '6454637');
        system.debug('#################' + ApexPages.currentPage().getParameters().get('customerCode'));
        controller.verifyCustomerCode();
        
        
         ApexPages.currentPage().getParameters().put('customerCode', '64546343');
        system.debug('#################' + ApexPages.currentPage().getParameters().get('customerCode'));
        controller.verifyCustomerCode();
        
        LegacyAccount__c objLegacyAccount1 = Utils_TestMethods.createLegacyAccount(account2.Id);
        objLegacyAccount1.LegacyNumber__c = '6454637';
        objLegacyAccount1.LegacyName__c  = 'CN_SDC'; 
        
        insert objLegacyAccount1;
        
        controller.verifyCustomerCode();
        
        controller.btestflag = true;
        controller.verifyCustomerCode();
        controller.btestflag = false;      
    }
}