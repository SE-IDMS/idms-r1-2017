/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

private class AP_WorkOrderTechnicianNotification_TEST {

   // public boolean executeFromtest = false;
    
    static testMethod void myUnitTest() {
        
        User cuser = CreateUser();
        insert cuser;
        system.runAs(cuser){
    
        // TO DO: implement unit test
       boolean executeFromtest = true;
        //profile used
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];        
        
        //User creation
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        insert user;
        
        User user1 = new User(alias = 'user1', email='user1' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user1' + '@bridge-fo.com');
        insert user1;
        
        User user2 = new User(alias = 'user2', email='user2' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing12', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user2' + '@bridge-fo.com');
        insert user2;
        
         //Create Country
         Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Type ='SA';
        insert acc;
        //SVMXC__Site__c site = new SVMXC__Site__c(name= 'site1');
        //insert site;
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'site1';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        site.RecordTypeId = Label.CLNOV13SRV02;
        insert site;
        
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(name='testsc');
        insert sc;

        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
         SVMXC__Service_Group_Members__c sgm2;
        SVMXC__Service_Group_Members__c sgm3;
                
        RecordType rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c' and DeveloperName ='Technician'];
        RecordType rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c' and DeveloperName = 'Technican'];  
 
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rts.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
                insert st;
         
                sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
                insert sgm1;
                
                sgm2 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user1.Id,
                                                            Send_Notification__c = 'SMS',
                                                            SVMXC__Email__c = 'sgm2.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
                insert sgm2;
                
                sgm3 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user2.Id,
                                                            Send_Notification__c = 'SMS and Email',
                                                            SVMXC__Email__c = 'sgm3.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
                insert sgm3;
            //}
        //}
   
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder.Work_Order_Category__c='On-site';   
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.WorkOrderSubType__c='3-Day Workshop'; 
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'Energy';
         workOrder.CustomerTimeZone__c='Asia/Baghdad';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
        // workOrder.Customer_Location__c = Cust_Location.Id;
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         workOrder.SVMXC__Site__c = site.id;
         workOrder.SVMXC__Company__c= acc.id;
         workOrder.SVMXC__Service_Contract__c = sc.id;
         insert workOrder;
         
         SVMXC__Service_Order__c workOrder1 =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder1.Work_Order_Category__c='On-site';                 
         workOrder1.SVMXC__Order_Type__c='Maintenance';
          workOrder1.CustomerTimeZone__c='Asia/Baghdad';
         workOrder1.WorkOrderSubType__c='3-Day Workshop'; 
         workOrder1.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder1.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder1.CustomerRequestedDate__c = Date.today();
         workOrder1.Service_Business_Unit__c = 'Energy';
         workOrder1.SVMXC__Priority__c = 'Normal-Medium';
        // workOrder1.Customer_Location__c = Cust_Location.Id;
         workOrder1.SendAlertNotification__c = true;
         workOrder1.TECH_IsAlertFSESent__c = true;
         workOrder1.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         database.insert (workOrder1,false);

         
        
            List<AssignedToolsTechnicians__c> assignedFSRs = new List<AssignedToolsTechnicians__c>();
            AssignedToolsTechnicians__c technician1 = Utils_TestMethods.CreateAssignedTools_Technician(WorkOrder.id, sgm1.id);
            technician1.Status__c ='Assigned';
            try{
            
            Database.insert(technician1);
            assignedFSRs.add(technician1);
            }
            catch(Exception ex){

            System.debug('Exception is' +ex.getmessage());
            }
            AssignedToolsTechnicians__c technician2 = Utils_TestMethods.CreateAssignedTools_Technician(workOrder.id, sgm2.id);
            technician2.Status__c ='Assigned';

            try{
            Database.insert(technician2);
            assignedFSRs.add(technician2);
            }
            catch(Exception ex){
            System.debug('Exception is' +ex.getmessage());
            }
            AssignedToolsTechnicians__c technician3 = Utils_TestMethods.CreateAssignedTools_Technician(WorkOrder.id, sgm3.id);
            technician3.Status__c ='Assigned';
            try{
            Database.insert(technician3);
            assignedFSRs.add(technician3);
            }
            catch(Exception ex){
            System.debug('Exception is' +ex.getmessage());
            }
        
        workOrder.BackOfficeReference__c = 'XXX';
        workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
       //AP_WorkOrderTechnicianNotification.IsTesting(true);
        // commented on 21 due to test failure
       // update WorkOrder;  
        string FSENames;
        
        AP_WorkOrderTechnicianNotification.getPickList();  
        AP_WorkOrderTechnicianNotification.NotifyTechnicians(workOrder);
        AP_WorkOrderTechnicianNotification.NotifyTechnicians2(workOrder);

        AP_WorkOrderTechnicianNotification.SMS_Notification(workOrder,sgm1);
        AP_WorkOrderTechnicianNotification.SMS_RSNotification(workOrder,workOrder1,sgm2,acc,1);
          AP_WorkOrderTechnicianNotification.SMS_RSNotification(workOrder,workOrder1,sgm2,acc,2);
        AP_WorkOrderTechnicianNotification.Email_RSNotification(workOrder,workOrder1,sgm2,acc,1);
          AP_WorkOrderTechnicianNotification.Email_RSNotification(workOrder,workOrder1,sgm2,acc,2);
        List<String> assignedFSRsIds = new List<String>();
        String assignedFSRsId = technician1.id+','+technician2.id+','+technician3.id+',';
        //String assignedFSRsId = technician1.id+','+technician2.id+',';
        assignedFSRsIds.add(assignedFSRsId);
        system.debug('The assignedFSRsIds is' +assignedFSRsIds);
        AP_WorkOrderTechnicianNotification.Flow_SMS_Notification(assignedFSRsIds);
          
        String replyToAddress = 'test@test.com';
        AP_WorkOrderTechnicianNotification.Email_Notification(workOrder,sgm2, FSENames,technician2, replyToAddress);
        try{
        test.starttest();
        AP_WorkOrderTechnicianNotification.QuickRescheduleNotification(workOrder,workOrder,1);
        AP_WorkOrderTechnicianNotification.UncheckAlertFSE(workOrder1);
        }
        catch(Exception ex){
        }
         try{
        AP_WorkOrderTechnicianNotification.QuickRescheduleNotification(workOrder,workOrder,2);
        Test.stoptest();
        }
        catch(Exception ex){
        }
        }
    }
    
    public Static User CreateUser(){
        String ByPassTriggers='AP_WorkOrderTechnicianNotification;SVMX07;SVMX05;SVMX06;AP54;AP_Contact_PartnerUserUpdate;SVMX_InstalledProduct2;SVMX23;SVMX07;SRV06';
        //CLDEC14SRV01 ->> SE - Interface – WorkOrderConnector profile id
        User user = new User(alias = 'user', email='user1' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLMAR16SC01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = ByPassTriggers,
                             timezonesidkey='Europe/London', username='user111' + '@bridge-fo.com',FederationIdentifier = '12345');                             
        return user;
    
    }
}