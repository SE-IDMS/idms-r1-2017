@isTest
public class VFC_NewCAPPlatformingScoring_TEST{
    public static testMethod void Testrun1() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    a.ClassLevel1__c='FI';
    a.MarketSegment__c='BD1';
    a.country__c=c.id;
    insert a;
    system.debug('**Account id **'+a.id);
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    cap.CriterWeightQu1__c=2;
    cap.CriterWeightQu2__c=2;
    cap.CriterWeightQu3__c=2;
    cap.CriterWeightQu4__c=2;
    cap.CriterWeightQu5__c=2;
    cap.CriterWeightQu6__c=2;
    cap.CriterWeightQu7__c=2;
    cap.CriterWeightQu8__c=2;
    insert cap;
    system.debug('**cap **'+cap);
    List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3,MarketSegment__c='BD1');
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c=a.ClassLevel1__c);                                                      
            capgmd.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
       for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
        SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Cancel();
    }
    
    public static testMethod void Testrun6() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    insert cap;
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    //capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Cancel();
    }
    public static testMethod void Testrun7() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    a.Inactive__c=true;  
    a.ToBeDeleted__c=true;
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    insert cap;
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Cancel();
    }
    public static testMethod void Testrun2() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    a.ClassLevel1__c='FI';
    a.MarketSegment__c='BD4';
    a.country__c=c.id;
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    insert cap;
    system.debug('**cap **'+cap);
    AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
    SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
    List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='SC',Country__c=c.id,CountryWeight__c=3,MarketSegment__c='BD1');
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c=a.ClassLevel1__c);                                                      
            capgmd.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
       for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Save();
    capctrl.Cancel();
    }

public static testMethod void Testrun3() {
    Id userId=userInfo.getUserId();
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    a.ClassLevel1__c='Default';
    a.country__c=c.id;
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    insert cap;
    system.debug('**cap **'+cap);
    AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
    SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
    List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='SC',Country__c=c.id,CountryWeight__c=3);
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='SC');   //add nonFI                                                 
            capgmd.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
       for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Save();
    capctrl.Cancel();
    }

public static testMethod void Testrun4() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
        insert c;
    Account a = Utils_TestMethods.createAccount();
    a.ClassLevel1__c='FI';
    a.MarketSegment__c='BD4';
    a.country__c=c.id;
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    insert cap;
    system.debug('**cap **'+cap);
    AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
    insert amp;
    SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
    List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3);
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='SC');                                                  
            capgmd.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
       for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Save();
    capctrl.Cancel();
    }

public static testMethod void Testrun5() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    a.ClassLevel1__c='FI';
    a.MarketSegment__c='BD4';
    a.country__c=c.id;
    insert a;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(userId);
    insert cap;
    system.debug('**cap **'+cap);
    AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
    insert amp;
    SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
    List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3,MarketSegment__c='BD4');
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='SC');   //add nonFI                                                 
            capgmd.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
       for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        
    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    capctrl.continuePS();
    capctrl.Save();
    capctrl.Cancel();
    }
    
    public static testMethod void Testrun8() {
    Id userId=userInfo.getUserId(); 
    Country__c c = Utils_TestMethods.createCountry(); 
    insert c;
    Account a = Utils_TestMethods.createAccount();
    a.ClassLevel1__c='FI';
    a.MarketSegment__c='BD4';
    a.country__c=c.id;
    a.OwnerId=userId;
    a.ExternalSalesResponsible__c = userId; 
    insert a;
    Account a2 = Utils_TestMethods.createAccount();
    a2.ClassLevel1__c='FI';
    a2.MarketSegment__c='BD4';
    a2.country__c=c.id;
    a2.OwnerId=userId;
    a2.ExternalSalesResponsible__c = userId; 
    insert a2;
    User newUser=Utils_TestMethods.createStandardUser('testuser');
    insert newUser;
    AccountTeamMember teammember=Utils_TestMethods.createAccTeamMember(a.id,newUser.id);
    insert teammember;
    SFE_IndivCAP__c cap=Utils_TestMethods.createIndividualActionPlan(newUser.id);
    insert cap;
    system.debug('**cap **'+cap);
    AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
    insert amp;
    AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp.id,PAM__c=10,DirectSales__c=2,IndirectSales__c=4);
    insert aPAM;
    SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
    Datetime LastYearsDate= Datetime.now().addDays(-365);
    System.debug('***LastYearsDate:***'+LastYearsDate);
    pltfromscoring=Utils_TestMethods.createPlatformScoring(cap.id,a2.id,10,'2');
    insert pltfromscoring;
            
    Test.setCreatedDate(pltfromscoring.Id, LastYearsDate);
    update pltfromscoring;
    System.debug('***pltfromscoring after update:***'+pltfromscoring);
    List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3,MarketSegment__c='BD4');
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',Weight__c=2,AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;

    Pagereference pg = new pagereference('/apex/VFP_NewCAPPlatformingScoring?Capid='+cap.id); 
    Test.setCurrentPageReference(pg);
    VFC_NewCAPPlatformingScoring  capctrl = new VFC_NewCAPPlatformingScoring(new ApexPages.StandardController(cap));  
    VFC_NewCAPPlatformingScoring.wrapscoring wrpScoring= new VFC_NewCAPPlatformingScoring.wrapscoring();
    capctrl.ps.PlatformedAccount__c=a.id;
    Test.starttest();
    capctrl.continuePS();
    capctrl.Save();
    capctrl.Cancel();
    Test.stopTest();
    }
    }