//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This is Global class exposing Confirm PIN custom REST API.  
//                  Apex REST End point with POST method which accepts six parameters Id, federation id, 
//                  profile update source, Pin code, Oepration and password.It returns http response with status code.
//*******************************************************************************************


@RestResource(urlMapping='/ConfirmPIN')
global with sharing class IdmsConfirmpinRest{
    //Http post method called by API to send PIN confirmation and to confirm the PIN received as SMS by the user on Phone.
    @HttpPost
    global static IDMSConfirmPinResponse doPost(string id,String FederationIdentifier,String IDMS_Profile_update_source,String PinCode,String Operation,String Password){
        IDMSConfirmPin obj                  = new IDMSConfirmPin(); 
        IDMSConfirmPinResponse response     = obj.Confirmpin(id,FederationIdentifier,IDMS_Profile_update_source,PinCode,Operation,password);
        RestResponse res                    = RestContext.response;
        
        if(Response.Status.equalsignorecase('Error')){
            res.statuscode = Integer.valueOf(Label.CLDEC16IDMS001);
            if(Response.Message.startsWith('Error in')){
                res.statuscode = Integer.valueOf(Label.CLDEC16IDMS001);
            }
            if(Response.Message.startsWith('User not')){
                res.statuscode = Integer.valueOf(Label.CLDEC16IDMS002);
            }
        }
        return Response;
    }
}