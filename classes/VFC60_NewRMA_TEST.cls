@isTest
private class VFC60_NewRMA_TEST
{
    static testMethod void testVFC60() 
    {


         // Country__c accountCountry= Utils_TestMethods.createCountry();
       Country__c accountCountry=[SELECT id, Name, CountryCode__c FROM Country__c Where CountryCode__c='IN'];

        //accountCountry.CountryCode__c='IN';
      //  Insert accountCountry;
        
        StateProvince__c accountStateProvince = new StateProvince__c();
        accountStateProvince.Country__c = accountCountry.ID;
        accountStateProvince.CountryCode__c = 'in';
        accountStateProvince.StateProvinceCode__c = 'WW';
        insert accountStateProvince;
        
        Account account1 = Utils_TestMethods.createAccount();
    // account1.StateProvince__c = accountStateProvince.ID;
        account1.Country__c = accountCountry.ID;
        account1.AdditionalAddress__c = 'AA';
        account1.City__c = 'CITY';
        account1.ZipCode__c = '12345';
     //   account1.County__c = 'DD';
        account1.POBoxZip__c = '678';
        account1.StreetLocalLang__c = 'LOCAL STREET';
        account1.LocalAdditionalAddress__c = 'RR';
        account1.LocalCity__c = 'LOCAL CITY';
      //  account1.LocalCounty__c = 'LOCAL COUNTY';*/

        insert account1;



        
        Contact contact1=Utils_TestMethods.createContact(account1.Id,'x923tgh');
             contact1.FirstName='cder';
             contact1.WorkPhone__c='9611320932';
        insert contact1;
              
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');


        insert Case1;
        
        
        RMA__c newRMA = new RMA__c();
        newRMA.case__c = case1.ID;
        
        PageReference ref = new PageReference('/apex/VFC60_NewRMA?returl=I'+case1.ID);
        Test.setCurrentPage(ref); 
        
        ApexPages.StandardController RMAStandardController = new ApexPages.StandardController(newRMA);
        VFC60_NewRMA RMAController = new VFC60_NewRMA(RMAStandardController );

        RMAController.goToEditPage();
        
        Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case2.Commercialreference__c='DIA2ED1070704TW';
        case2.Family__c='MARKETING COMM.POWER';
        insert Case2;

BusinessRiskEscalationEntity__c Org01 = new BusinessRiskEscalationEntity__c(SubEntity__c = '', Location__c = '', Entity__c = '@#$123TestEntity*&^',Location_Type__c = 'Country Front Office', ContactEmail__c = 'testContactEmail01@testSE.com');
        insert Org01;
        BusinessRiskEscalationEntity__c Org02 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '', Entity__c = '@#$123TestEntity*&^', Location_Type__c = 'Adaptation Center', ContactEmail__c = 'testContactEmail02@testSE.com');
        insert Org02;
        BusinessRiskEscalationEntity__c Org03 = new BusinessRiskEscalationEntity__c(SubEntity__c = '123TestSubEntity', Location__c = '123TestSubEntityLocation', Entity__c = '@#$123TestEntity*&^' , Location_Type__c = 'Adaptation Center', ContactEmail__c = 'testContactEmail03@testSE.com');
        insert Org03;
         TEX__c tex1 = new TEX__c( OpenDate__c = System.today(),Status__c = 'Expert Assessment to be started', PromiseDate__c = System.today(), case__C = Case2.id, OfferExpert__c = UserInfo.getUserId(), ExpertCenter__c = Org01.ID, FrontOfficeTEXManager__c = UserInfo.getUserId() , FrontOfficeOrganization__c = Org02.ID, LineofBusiness__c = Org03.ID,AssessmentType__c='Product Return to Expert Center');
        insert tex1;
        system.debug('@@@tex1@@@'+tex1);


        
        RMA__c newRMA1 = new RMA__c();
        newRMA1.case__c = case2.ID;
        //newRMA1.TEX__c =tex1.id;
         TEX__c relatedTex=new TEX__c();
        relatedTex=tex1;
        PageReference ref1 = new PageReference('/apex/VFC60_NewRMA?returl=I'+case2.ID);
        Test.setCurrentPage(ref1); 
        
        ApexPages.StandardController RMAStandardController1 = new ApexPages.StandardController(newRMA1);
        VFC60_NewRMA RMAController1 = new VFC60_NewRMA(RMAStandardController1 );
 //RelObjId =tex1.id;
        RMAController1.goToEditPage();
        RMAController1.doCancel();
    }
}