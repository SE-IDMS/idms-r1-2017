@isTest
private class VFC_NewCaseRedirectionPage_TEST
{
    static testMethod void testVFC_NewCaseRedirectionPage_TestMethod() 
    {
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        Account account1 = Utils_TestMethods.createAccount();
        account1.RecordTypeId = System.Label.CLJUL15CCC01;
        account1.Country__c =country.id;
        account1.City__c='Bangalore';
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.PRMContact__c=true;
        insert contact1;
    
        Case objCase = new Case();
        system.currentpagereference().getParameters().put('def_contact_id',contact1.Id);
        system.currentpagereference().getParameters().put('def_account_id',account1.Id);
        system.currentpagereference().getParameters().put('def_Subscription_Id','123456');
        system.currentpagereference().getParameters().put('CTISkill','test');
        system.currentpagereference().getParameters().put('CaseReason','TroubleShooting*');
        system.currentpagereference().getParameters().put('SubReason','TroubleShoot');
        system.currentpagereference().getParameters().put('LOE','Primary');  
        system.currentpagereference().getParameters().put('CaseCategory','testCategory');
         system.currentpagereference().getParameters().put('CountryCode','fr'); 
          system.currentpagereference().getParameters().put('SpocPhONE','3456789'); 
           system.currentpagereference().getParameters().put('Subject','testcase');
        
       // system.currentpagereference().getParameters().put('def_Subscription_Id',RepAccOrg);
        PageReference VFC_PageRef = new PageReference('/apex/VFP_NewCaseRedirectionPage?returl=/'+contact1.ID+'&def_contact_id=' +contact1.ID+'&def_account_id='+account1.Id );
        Test.setCurrentPage(VFC_PageRef); 
        PageReference pageRef = ApexPages.currentPage();
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC_NewCaseRedirectionPage NewCaseController = new VFC_NewCaseRedirectionPage(CaseStandardController);
        NewCaseController.redirectToCaseEditPage();
        
        account1.RecordTypeId =System.Label.CLOCT13ACC08;
        update account1;
        
        PageReference VFC_PageRef1 = new PageReference('/apex/VFP_NewCaseRedirectionPage?returl=/'+account1.Id+'&def_account_id='+account1.Id+'&def_contact_id=' +contact1.ID+'&CaseReason='+'tESTREASON'+'&CountryCode='+'FR'+'&CaseCategory='+'TESTCATEGORY'+'&SubReason='+'subreason'+'&Subject='+'testsubject');
        Test.setCurrentPage(VFC_PageRef1); 
        PageReference pageRef1 = ApexPages.currentPage();
        NewCaseController.redirectToCaseEditPage();
        
        delete contact1;
        PageReference VFC_PageRef2 = new PageReference('/apex/VFP_NewCaseRedirectionPage?def_contact_id=' +contact1.ID );
        Test.setCurrentPage(VFC_PageRef2); 
        PageReference pageRef2 = ApexPages.currentPage();
        NewCaseController.redirectToCaseEditPage();
        
        PageReference VFC_PageRef3 = new PageReference('/apex/VFP_NewCaseRedirectionPage?def_contact_id=' +contact1.ID+'&CF00N1200000B1IfS='+'12346');
        Test.setCurrentPage(VFC_PageRef3); 
        PageReference pageRef3 = ApexPages.currentPage();
        NewCaseController.redirectToCaseEditPage();
        NewCaseController.CTISkill = 'TEST';
        NewCaseController.CaseReason= 'TEST';
        NewCaseController.LOE= 'Primary';
        NewCaseController.SubscripId = '937973';
        pagereference caseEditPage=ApexPages.currentPage();
        caseEditPage.getParameters().put('cas14', 'def_contact_id');
        

    }

    static testMethod void testVFC_NewCaseRedirectionPage_TestMethod2(){
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        Account account1 = Utils_TestMethods.createAccount();
        account1.Country__c = country.id;
        account1.City__c='Bangalore';
        insert account1;
        
        account1.ToBeDeleted__c=TRUE;
        account1.ReasonForDeletion__c='Company closed';
        update account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
        
        contact1.ToBeDeleted__c=TRUE;
        contact1.ReasonForDeletion__c='Company closed';
        update Contact1;
        System.debug('account1:'+account1);
        System.debug('contact1:'+contact1);
        Case objCase = new Case();
        system.currentpagereference().getParameters().put('def_contact_id',contact1.Id);
        system.currentpagereference().getParameters().put('def_account_id',account1.Id);
        
        PageReference VFC_PageRef = new PageReference('/apex/VFP_NewCaseRedirectionPage?returl=/'+contact1.ID+'&def_contact_id=' +contact1.ID+'&def_account_id='+account1.Id );
        Test.setCurrentPage(VFC_PageRef); 
        PageReference pageRef = ApexPages.currentPage();
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC_NewCaseRedirectionPage NewCaseController = new VFC_NewCaseRedirectionPage(CaseStandardController);
        NewCaseController.redirectToCaseEditPage();
        
    }
    
    static testMethod void testVFC_NewCaseRedirectionPage_TestMethod3(){
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        Account account1 = Utils_TestMethods.createAccount();
        account1.Country__c = country.id;
        account1.City__c='Bangalore';
        insert account1;
        
        account1.inactive__c=TRUE;
        update account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
        
        contact1.Inactive__c=TRUE;
        update Contact1;
        
        Case objCase = new Case();
        system.currentpagereference().getParameters().put('def_contact_id',contact1.Id);
        system.currentpagereference().getParameters().put('def_account_id',account1.Id);
        
        PageReference VFC_PageRef = new PageReference('/apex/VFP_NewCaseRedirectionPage?returl=/'+contact1.ID+'&def_contact_id=' +contact1.ID+'&def_account_id='+account1.Id );
        Test.setCurrentPage(VFC_PageRef); 
        PageReference pageRef = ApexPages.currentPage();
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC_NewCaseRedirectionPage NewCaseController = new VFC_NewCaseRedirectionPage(CaseStandardController);
        NewCaseController.redirectToCaseEditPage();
        
    }
    
    
}