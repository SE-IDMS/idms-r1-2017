@isTest
public class IDMSUserReconcScheduleBatchTest {
    static testmethod void testExecute(){
        Test.starttest();
        IDMSUserReconcScheduleBatch batchScheduler=new IDMSUserReconcScheduleBatch();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test IDMSUserReconc', sch, batchScheduler); 
        Test.stoptest();
    }
}