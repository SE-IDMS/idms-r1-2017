public class VFC_CassiniCaseDetailPage {
    public String strCaseId {get;set;}
    public string comment{get;set;}
    public VFC_CassiniCaseDetailPage(){
        strCaseId=ApexPages.currentPage().getParameters().get('id');
    }
    
    @RemoteAction  
    public static List<Case> getCaseDetails(String caseId){
        List<Case> lstCases;
        lstCases=[SELECT AnswerToCustomer__c,CaseNumber,CreatedDate,DebitPoints__c,Description,LeadAdvancedAgent__r.name,LastLevel3User__c,LastModifiedDate,toLabel(PointDebitReason__c),ProductBU__c,Status,CustomerRequest__c,CommercialReference_lk__r.Name,Points__c,ActionNeededFrom__c,Family_lk__r.Name,TeamLeader__r.Name,LastLevel2User__r.Name FROM Case where Id= :caseId];
        return lstCases;
    } 
    
    @RemoteAction  
    public static List<CaseComment> getCaseComments(String caseId){
        List<CaseComment> lstCaseComments;
        lstCaseComments=[SELECT CommentBody,CreatedBy.Name,CreatedDate FROM CaseComment where ParentId= :caseId ORDER BY CreatedDate DESC];
        return lstCaseComments;
    } 
    
    @RemoteAction  
    public static List<Attachment> getCaseAttachments(String caseId){
        List<Attachment> lstCaseAttachments;
        lstCaseAttachments=[SELECT BodyLength,CreatedBy.Name,Id,LastModifiedDate,Name,ParentId FROM Attachment where ParentId= :caseId ORDER BY CreatedDate DESC];
        return lstCaseAttachments;
    } 
    @RemoteAction
    public static List<CaseComment> saveCaseComment(String comment, String strCaseId) {
        
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = true;
        CaseComment caseComment=new CaseComment();
        caseComment.CommentBody=comment;
        caseComment.ParentId=strCaseId;
        caseComment.IsPublished=true;
        database.insert(caseComment, dlo);
        List<CaseComment> lstCaseComments=[SELECT CommentBody,CreatedBy.Name,CreatedDate FROM CaseComment where ParentId= :strCaseId ORDER BY CreatedDate DESC];
        return lstCaseComments;
    }
    @RemoteAction
    public static List<CaseComment> submitForCaseClosure(String strCaseId) {
        CaseComment caseComment=new CaseComment();
        caseComment.CommentBody=System.Label.CLJUN16CSN085;
        caseComment.ParentId=strCaseId;
        caseComment.IsPublished=true;
        insert caseComment;
        List<CaseComment> lstCaseComments=[SELECT CommentBody,CreatedBy.Name,CreatedDate FROM CaseComment where ParentId= :strCaseId ORDER BY CreatedDate DESC];
        return lstCaseComments;
    }
    @RemoteAction  
    public static List<PackageTemplate__c> getPackageTemplatesForCaseDetail(){
    
        List<PackageTemplate__c> lstPackageTemplate;
        lstPackageTemplate=[SELECT Id,Name,OfferedPrice__c,Points__c FROM PackageTemplate__c WHERE Points__c !=null ORDER BY Points__c ASC NULLS LAST];
        return lstPackageTemplate;
    }   
    @RemoteAction  
    public static Boolean putPointsRequestForCaseDetail(String Points,String Price,String Reference,String Description,String ShortDescription){
        
        List<User> lstContactId=[SELECT Id,ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        System.debug(lstContactId);
        List <CTR_ValueChainPlayers__c> lstCVCPs=new List <CTR_ValueChainPlayers__c>();
        if(lstContactId[0].ContactId!=null){
            lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,Contract__r.Owner.Email,Contract__r.ContractNumber,Contract__r.Name,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            System.debug(lstCVCPs);
        }
        List<Contract> lstContract=new List<Contract>();
        if(!lstCVCPs.isEmpty()) {
            lstContract=[SELECT OwnerID FROM Contract WHERE Id =:lstCVCPs[0].Contract__c ORDER BY CreatedDate DESC Limit 1];
            System.debug(lstContract);
        }
        Boolean Flag=false;
        String strDescription='';
        String strShortDescription='';
        
        if(Description!='undefined' && Description!='' && Description!=null) 
            strDescription=Description;
        if(ShortDescription!='undefined' && ShortDescription!='' && ShortDescription!=null) 
            strShortDescription=ShortDescription;
        
        String subject;
        Messaging.reserveSingleEmailCapacity(2);
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        mail1.setTargetObjectId(lstContactId[0].Id);
        mail1.setSaveAsActivity(false);
        mail1.setReplyTo(Userinfo.GetUserEmail());
        mail1.setSenderDisplayName(Userinfo.GetName());
        
        if(!Test.IsRunningTest()){
              subject=Label.CLJUN16CSN119 +' - '+Userinfo.getName() +' - '+ Reference+ ' - '+Label.CLJUN16CSN120 +lstCVCPs[0].Contract__r.ContractNumber;  
              mail1.setSubject(subject);
        }   
        else
              mail1.setSubject('Test Subject1234512');
        
        mail1.setBccSender(false);
        mail1.setUseSignature(false);
        String strDesc1;
        if(!Test.IsRunningTest()){
            strDesc1=Label.CLJUN16CSN121+' <br><br> <b>'+ Label.CLJUN16CSN122 + '</b> ' + Userinfo.getName() +' <br> <b>'+Label.CLJUN16CSN123+'</b> '+ Reference +'<br><b> '+ Label.CLJUN16CSN124+' </b> ' +Points+ '<br><b> ' +Label.CLJUN16CSN125 +' </b>'+ Price+ '<br> <b> '+Label.CLJUN16CSN126 +' </b> '+strShortDescription+'<br><b> '+Label.CLJUN16CSN127+'</b> '+lstCVCPs[0].Contract__r.ContractNumber+'<br><b> '+Label.CLJUN16CSN128 +' </b>' +strDescription+'<br><br><br>'+ Label.CLJUN16CSN176 ;
            mail1.setHtmlBody(strDesc1);
        }
        else{
            mail1.setHtmlBody('Test HTML Body');
        }
        System.debug('mail1:'+mail1);
        Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
        if(!Test.IsRunningTest()){
            mail2.setTargetObjectId(lstContract[0].OwnerID);
        }
        else{
            mail2.setTargetObjectId(lstContactId[0].Id);   
        }
        mail2.setSaveAsActivity(false);
        mail2.setReplyTo(Userinfo.GetUserEmail());
        mail2.setSenderDisplayName(Userinfo.GetName());
        if(!Test.IsRunningTest())
            mail2.setSubject(subject);
        else
            mail2.setSubject('Test Subject1234512');
        
        mail2.setBccSender(false);
        mail2.setUseSignature(false);
        //String strDesc2=Label.CLJUN16CSN121+' </br></br> <b>'+ Label.CLJUN16CSN122 + '</b>' + Userinfo.getName() +' </br> <b>'+Label.CLJUN16CSN123+'</b> '+ Reference +'</br><b> '+ Label.CLJUN16CSN124+' </b> ' +Points+ '</br><b> ' +Label.CLJUN16CSN125 +' </b>'+ Price+ '</br> <b> '+Label.CLJUN16CSN126 +' </b> '+strShortDescription+'</br><b> '+Label.CLJUN16CSN127+'</b> '+lstCVCPs[0].Contract__r.ContractNumber+'</br><b> '+Label.CLJUN16CSN128 + strDescription+'</br></br> '+Label.CLJUN16CSN176;
        //strDesc2=Label.CLJUN16CSN121+' <br><br> <b>'+ Label.CLJUN16CSN122 + '</b>' + Userinfo.getName() +' <br> <b>'+Label.CLJUN16CSN123+'</b> '+ Reference +'<br><b> '+ Label.CLJUN16CSN124+' </b> ' +Points+ '<br><b> ' +Label.CLJUN16CSN125 +' </b>'+ Price+ '<br> <b> '+Label.CLJUN16CSN126 +' </b> '+strShortDescription+'<br><b> '+Label.CLJUN16CSN127+'</b> '+lstCVCPs[0].Contract__r.ContractNumber+'<br><b> '+Label.CLJUN16CSN128 + strDescription+'<br><br><br>'+ Label.CLJUN16CSN176 ;
        if(!Test.IsRunningTest()){
            mail2.setHtmlBody(strDesc1);
        }
        else{
            mail2.setHtmlBody('test Email HTML BODy');
        }
        System.debug('mail2:'+mail2);
        emails.add(mail1);
        emails.add(mail2);
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails);  
        Flag=true;
        return Flag;
    }    
}