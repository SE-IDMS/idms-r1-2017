public class VFC_PRMLogout{

    public String countryCode {get;set;}
    
    public VFC_PRMLogout(){
        FieloEE__Member__c member = FieloEE.MemberUtil.getAllFieldsLoggedMember();
        if(member!=null){
              countryCode = member.F_PRM_CountryCode__c;
              system.debug('Country1:' + countryCode);
         }
         else if(ApexPages.currentPage().getCookies().containsKey('country')){
               countryCode = String.valueOf(ApexPages.currentPage().getCookies().get('country').getValue()); 
                system.debug('Country4:' + countryCode);          
         }
        else{
            countryCode = UserInfo.getLocale();
            countryCode  = countryCode.contains('_') ?countryCode.substring(3,5) :countryCode;
             system.debug('Country2:' + countryCode);
       }
              system.debug('Country3:' + countryCode);
    }
    
    public String getRedirectURL(){
        PRMCountry__c countrycluster;
        String IDToken = ApexPages.currentPage().getParameters().get('newEmail');
        System.debug('>>>>'+IDToken); 
        
        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('type')) && ApexPages.currentPage().getParameters().get('type') == 'pr'){
          getFlushCookies();
          return '/partners/Menu/Login';
        }

        else if(IDToken != null){
            return '/partners/menu/login?IDtoken1='+IDtoken;
        }
        
        else if(countryCode!=null){
            String countryCodeAux = '%' + countryCode  + '%';
            system.debug('***Countrycode***' + countryCode);
            system.debug('***countryCodeAux***' + countryCodeAux);

            PRMCountry__c[] countryclusterList = [SELECT id,DefaultLandingPage__c FROM PRMCountry__c WHERE (CountryCode__c = : countryCode OR  TECH_Countries__c like : countryCodeAux) and CountryPortalEnabled__c=true LIMIT  1];
             system.debug('***countryclusterList-- ***' + countryclusterList );

            if(countryclusterList.size()>0){
                countrycluster = countryclusterList[0];
                  system.debug('***countrycluster0***' + countryclusterList[0]);
            }else{
                countrycluster = [SELECT id,DefaultLandingPage__c FROM PRMCountry__c WHERE CountryCode__c = 'WW'];
            }
             system.debug('***countrycluster.DefaultLandingPage__c1***' + countrycluster.DefaultLandingPage__c);
            return countrycluster.DefaultLandingPage__c;
        }else{
            countrycluster = [SELECT id,DefaultLandingPage__c FROM PRMCountry__c WHERE CountryCode__c = 'WW'];
            return countrycluster.DefaultLandingPage__c;
       }
    }


    public String getFlushCookies(){
        //String cookiesCommaSeparated = System.label.CLPRMMAR16008;
        Map<String,System.Cookie> pageCookies = ApexPages.currentPage().getCookies();
        //for(String cookieName: cookiesCommaSeparated.split(',')){
          for(String cookieName: pageCookies.keyset()){
            if(ApexPages.currentPage().getCookies().containsKey(cookieName) && cookieName != 'country' && cookieName != 'language'){
                Cookie cookieToDelete = new Cookie(cookieName, null, null, 0, false);
                ApexPages.currentPage().setCookies(new Cookie[]{cookieToDelete});
            }
        }
        return '...';
    }
}