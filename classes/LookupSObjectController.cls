/**
 * (c) Tony Scott. This code is provided as is and without warranty of any kind.
 *
 * This work by Tony Scott is licensed under a Creative Commons Attribution 3.0 Unported License.
 * http://creativecommons.org/licenses/by/3.0/deed.en_US
 */

 /**
 * Apex Controller for looking up an SObject via SOSL
 */
public with sharing class LookupSObjectController 
{
    /**
     * Aura enabled method to search a specified SObject for a specific string
     */
    @AuraEnabled
    public static Result[] lookup(String searchString, String sObjectAPIName)
    {
        // Sanitze the input
        String sanitizedSearchString = String.escapeSingleQuotes(searchString);
        String sanitizedSObjectAPIName = String.escapeSingleQuotes(sObjectAPIName);

		List<Result> results = new List<Result>();

        // Build our SOSL query
        String searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName + '(id,name) Limit 50'; 

        // Execute the Query
        List<List<SObject>> searchList = search.query(searchQuery);

        // Create a list of matches to return
		for (SObject so : searchList[0])
        {
            results.add(new Result((String)so.get('Name'), so.Id));
        }
        
        return results;
    }
    
    /**
     * Inner class to wrap up an SObject Label and its Id
     */
	public class Result
    {
        @AuraEnabled public String SObjectLabel {get; set;}
        @AuraEnabled public Id SObjectId {get; set;}
        
        public Result(String sObjectLabel, Id sObjectId)
        {
            this.SObjectLabel = sObjectLabel;
            this.SObjectId = sObjectId;
        }
    }
    
    public class AdvancedLookupResult
    {
        @AuraEnabled public String SObjectLabel {get; set;}
        @AuraEnabled public Id SObjectId {get; set;}
        @AuraEnabled public SObject sobjectitem {get;set;}
        public AdvancedLookupResult(String sObjectLabel, Id sObjectId,SObject sobjectitem)
        {
            this.SObjectLabel = sObjectLabel;
            this.SObjectId = sObjectId;
            this.sobjectitem = sobjectitem;
        }
    }
    
    @AuraEnabled
    public static AdvancedLookupResult[] advlookup(String searchString, String sObjectAPIName,String fields)
    {
        // Sanitze the input
        String sanitizedSearchString = String.escapeSingleQuotes(searchString);
        String sanitizedSObjectAPIName = String.escapeSingleQuotes(sObjectAPIName);
        
		List<AdvancedLookupResult> results = new List<AdvancedLookupResult>();

        // Build our SOSL query
        String searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName + '(Id,Name,'+fields+') Limit 50'; 
		System.debug(searchQuery);
        // Execute the Query
        List<List<SObject>> searchList = search.query(searchQuery);

        // Create a list of matches to return
		for (SObject so : searchList[0])
        {
            results.add(new AdvancedLookupResult((String)so.get('Name'), so.Id,so));
        }
        
        return results;
    }
    
    @AuraEnabled
    public static AdvancedLookupResult[] advlookup(String searchString, String sObjectAPIName,String fields,String searchtype)
    {
        // Sanitze the input
        String sanitizedSearchString = String.escapeSingleQuotes(searchString);
        String sanitizedSObjectAPIName = String.escapeSingleQuotes(sObjectAPIName);
        
		List<AdvancedLookupResult> results = new List<AdvancedLookupResult>();
		String searchQuery = '';
        // Build our SOSL query
        if(searchtype == 'ALL')
        searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName + '(Id,Name,'+fields+') Limit 50'; 
        else
        searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN NAME FIELDS RETURNING ' + sanitizedSObjectAPIName + '(Id,Name,'+fields+') Limit 50'; 
		System.debug(searchQuery);
        // Execute the Query
        List<List<SObject>> searchList = search.query(searchQuery);

        // Create a list of matches to return
		for (SObject so : searchList[0])
        {
            results.add(new AdvancedLookupResult((String)so.get('Name'), so.Id,so));
        }
        
        return results;
    }
    
    @AuraEnabled
    public static List<Contact> getContactsForAccount(String accountId,String fields)
    {
       return Database.query('Select Id,Name,'+fields+' from Contact where AccountId = \''+accountId+'\'');
    }
    
    @AuraEnabled
    public static List<Contact> getContactsForAccounts(String accountIdString,String fields)
    {
       return Database.query('Select Id,Name,'+fields+' from Contact where AccountId in ('+accountIdString+')');
    }
    
}