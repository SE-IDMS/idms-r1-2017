global with sharing class VFC_AddRPDTProduct {    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public RMAShippingToAdmin__c rpdtRecord{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public VFC_AddRPDTProduct(ApexPages.StandardController controller){
        rpdtRecord=(RMAShippingToAdmin__c)controller.getRecord();  
        
         if(rpdtRecord.id!=null){
            rpdtRecord=[select id, CommercialReference__c,BusinessUnit__c ,Family__c ,ProductDescription__c ,TECH_ProductFamily__c ,ProductGroup__c,ProductLine__c,ProductSuccession__c,SubFamily__c  from RMAShippingToAdmin__c where id=:rpdtRecord.id];
            if(rpdtRecord.BusinessUnit__c!=null)
                pageParameters.put('businessLine',rpdtRecord.BusinessUnit__c);            
            if(rpdtRecord.ProductLine__c!=null)
                pageParameters.put('productLine',rpdtRecord.ProductLine__c);            
            if(rpdtRecord.TECH_ProductFamily__c!=null)
                pageParameters.put('strategicProductFamily',rpdtRecord.TECH_ProductFamily__c);            
            if(rpdtRecord.Family__c!=null)
                pageParameters.put('family',rpdtRecord.Family__c);            
            if(rpdtRecord.CommercialReference__c!=null)
                pageParameters.put('commercialReference',rpdtRecord.CommercialReference__c);
            String URLCR='';
            if(pageParameters.containsKey('commercialReference'))
                URLCR =pageParameters.get('commercialReference');          
        }                     
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    global static String getSearchText() {
        String scriteria='Vimal';
        /*
        System.debug('--Vimal--');
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
        System.debug('--Vimal--: ' + scriteria);
        */
        return scriteria; 
    }
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        system.debug('&&& business'+ business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    public PageReference performSelect(){      
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        WS_GMRSearch.gmrDataBean DBL= (WS_GMRSearch.gmrDataBean) JSON.deserialize(jsonSelectString, WS_GMRSearch.gmrDataBean.class);   
                
        if(rpdtRecord.id != null){  
                    rpdtRecord.CommercialReference__c= DBL.commercialReference;
                    //rpdtRecord.Family__c = DBL.family.label;
                    rpdtRecord.BusinessUnit__c =DBL.businessLine.label.unescapeHtml3();
                    rpdtRecord.Family__c =DBL.family.label.unescapeHtml3();                   
                    rpdtRecord.ProductDescription__c =DBL.description.unescapeHtml3();
                    rpdtRecord.TECH_ProductFamily__c =DBL.strategicProductFamily.label.unescapeHtml3();
                    rpdtRecord.ProductGroup__c =DBL.productGroup.label.unescapeHtml3();
                    rpdtRecord.ProductLine__c =DBL.productLine.label.unescapeXML();//.unescapeHtml3();
                    rpdtRecord.ProductSuccession__c =DBL.productSuccession.label.unescapeHtml3();
                    rpdtRecord.SubFamily__c =DBL.subFamily.label.unescapeHtml3();
                    
                    if(DBL.businessLine.value != null)
                        rpdtRecord.TECH_GMRCode__c = DBL.businessLine.value;
                    if(DBL.productLine.value != null)
                        rpdtRecord.TECH_GMRCode__c += DBL.productLine.value;
                    if(DBL.strategicProductFamily.label != null)
                        rpdtRecord.TECH_GMRCode__c += DBL.strategicProductFamily.value;
                    if(DBL.family.label != null)
                        rpdtRecord.TECH_GMRCode__c += DBL.family.value;
                    if(DBL.subFamily.label != null)
                        rpdtRecord.TECH_GMRCode__c += DBL.subFamily.value;
                    if(DBL.productSuccession.label != null)
                        rpdtRecord.TECH_GMRCode__c += DBL.productSuccession.value;
                    if(DBL.productGroup.label != null)
                        rpdtRecord.TECH_GMRCode__c += DBL.productGroup.value;
        }                  
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'RPDT Id cannot be blank'));
            return null;
        }       

        try{
            upsert rpdtRecord;
            PageReference casePage;            
            casePage=new PageReference('/'+rpdtRecord.id);                        
            return casePage;
        }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    
    }
    public PageReference performSelectFamily(){   
    
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        WS04_GMR.gmrDataBean DBL= (WS04_GMR.gmrDataBean) JSON.deserialize(jsonSelectString, WS04_GMR.gmrDataBean.class);   
    
        if(rpdtRecord.id!=null){         
            if(DBL.businessLine.label != null)
                rpdtRecord.BusinessUnit__c = DBL.businessLine.label;                     
            if(DBL.productLine.label != null)
                rpdtRecord.ProductLine__c = DBL.productLine.label;                        
            if(DBL.strategicProductFamily.label != null)
                rpdtRecord.TECH_ProductFamily__c = DBL.strategicProductFamily.label;                       
            if(DBL.family.label != null)
                rpdtRecord.Family__c = DBL.family.label;                 

            rpdtRecord.CommercialReference__c = null;                    
            rpdtRecord.ProductDescription__c = null;
            rpdtRecord.SubFamily__c = null;
            rpdtRecord.ProductSuccession__c = null;
            rpdtRecord.ProductGroup__c = null;       
            rpdtRecord.ProductDescription__c = null;      
            if(DBL.businessLine.value != null)
                rpdtRecord.TECH_GMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                rpdtRecord.TECH_GMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                rpdtRecord.TECH_GMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                rpdtRecord.TECH_GMRCode__c += DBL.family.value;


        }
        else{
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'CaseID cannot be blank'));
             return null;
        }       

        try{
            upsert rpdtRecord;
            PageReference casePage;            
            casePage=new PageReference('/'+rpdtRecord.id);                        
            return casePage;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
        }        

        
        return null;
    }
    
 public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(rpdtRecord.id!=null){
            pageResult = new PageReference('/'+ rpdtRecord.id);
             
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
   // return null;

}