@isTest
public class AP_CountryChannelUserRegistration_Test{
     static TestMethod void CountryChannelUserRegistration(){
        Country__c country= Utils_TestMethods.createCountry();
        insert country;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLC1 = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Financier1', Active__c = true, ParentClassificationLevel__c = cLC.Id);
        insert cLC1;
        CountryChannels__c cCh = new CountryChannels__c(Active__c = True,Country__c = country.id, Channel__c = cLC.id, SubChannel__c = cLC1.Id);
        insert cCh;
        List<CountryChannelUserRegistration__c> cCUReg = new  List<CountryChannelUserRegistration__c>();
        cCUReg.add(new CountryChannelUserRegistration__c(CountryChannels__c = cCh.Id, UserRegFieldName__c = 'LastName'));
        cCUReg.add(new CountryChannelUserRegistration__c(CountryChannels__c = cCh.Id, UserRegFieldName__c = 'LastName'));
        test.startTest();
        try{
            insert cCUReg;
        }
        Catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains(System.Label.CLAPR15PRM015) ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        test.stopTest();
     }
}