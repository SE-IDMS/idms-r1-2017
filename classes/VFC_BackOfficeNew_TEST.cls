@isTest(seeAllData=true)
public class VFC_BackOfficeNew_TEST
{
    static testMethod void myTest()
    {
         BusinessRiskEscalationEntity__c bree=new BusinessRiskEscalationEntity__c();
         bree.Name='TestOrg';
         bree.Entity__c='Sample';
         insert bree;
         
         BusinessRiskEscalationEntity__c bree1=new BusinessRiskEscalationEntity__c();
         bree1.Name='TestOrg1';
         bree1.Entity__c='Sample1';
         insert bree1;
         
         BackOfficesystem__c bos=new BackOfficesystem__c();
         bos.BackOfficeSystem__c='SAPECC1';
         bos.FrontOfficeOrganization__c=bree1.Id;
         insert bos;
    
         Country__c con=new Country__c();
         con.Name='India';
         con.CountryCode__c='91';
         //con.Currency__c='SGD';
         insert con;
         
         Country__c con1=new Country__c();
         con1.Name='US';
         con1.CountryCode__c='11';
         //con.Currency__c='SGD';
         insert con1;
         
        system.currentpagereference().getParameters().put('id1',bree1.Id);
        system.currentpagereference().getParameters().put('coid',con.Id);
        
        ApexPages.StandardController SC=new ApexPages.StandardController(bos); 
        VFC_BackOfficeNew bon= new VFC_BackOfficeNew(SC);
              
        bon.initializeComplaintRequest(bos);
        bon.goToEditPage();
       
        system.currentpagereference().getParameters().put('id1','');
        //system.currentpagereference().getParameters().put('coid',con.Id);
        
            
        bon.frontorg =null;
        bon.initializeComplaintRequest(bos);
        bon.goToEditPage();       
        
        
    }

}