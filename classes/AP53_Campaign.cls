/*
26-Oct-2012    Srinivas Nallapati    Initial Creation : Dec 12 release Campaign methods 

*/
public class AP53_Campaign
{   
    public static List<Country__c> lstCountry  = [select name from Country__C];
    public static String CountryName;
    
    public string getAllCountryNames(){
        //String CountryName;
        //List<Country__c> lstCo = [select name from Country__C];
        if(AP53_Campaign.CountryName ==null)
        {   
            CountryName  = ''; 
            for(Country__c co : AP53_Campaign.lstCountry)
             CountryName = co.name + ';'+CountryName;
        }     
        
        return  CountryName;
    }
            
 
    public void createUpdateCampaignCountry(set<id> camIdsToRelateAllCountries, set<id> camIdsToUnrelateAllCountries)
    {
        if(camIdsToUnrelateAllCountries.size() > 0)
        {
            List<CampaignCountry__c> lstCamCountryToDel = new List<CampaignCountry__c>();
            lstCamCountryToDel = [select id from CampaignCountry__c where Campaign__c in :camIdsToUnrelateAllCountries];
            if(lstCamCountryToDel.size() > 0)
            {   
                for(CampaignCountry__c cc : lstCamCountryToDel )
                {    
                    cc.Tech_DeletedFromParentTrigger__c = true;
                }
                update lstCamCountryToDel;
                delete  lstCamCountryToDel;
            }
        }
        
        if(camIdsToRelateAllCountries.size() > 0)
        {
            List<CampaignCountry__c> lstCamCountryToInsert = new List<CampaignCountry__c>();
            
            
            if(lstCountry.size() > 0)
            {
                for(id camid : camIdsToRelateAllCountries)
                {
                    for(Country__c coun : lstCountry)
                    {
                        CampaignCountry__c CamCoun = new CampaignCountry__c();
                        CamCoun.Campaign__c = camid;
                        CamCoun.Country__c = coun.id;
                        CamCoun.TECH_CountryNameUpdated__c = true;   // to restrict the Workflow rule from firing to update Campaign
                        lstCamCountryToInsert.add(CamCoun);
                    }
                }
            }
            
            if(lstCamCountryToInsert.size() > 0)
               Database.insert(lstCamCountryToInsert, false);
        }
    }//End of method createUpdateCampaignCountry

}//End of class