/*
Test Class for the Class Milestone1_Expense_Trigger_Utility
April 2015 Release
Divya M 
*/
@isTest
public class Milestone1_Expense_Trigger_Utility_Test
{
  static testMethod void testTriggerUtility(){
        List<Milestone1_Expense__c> recs = new List<Milestone1_Expense__c>{new Milestone1_Expense__c()};
        Milestone1_Expense_Trigger_Utility.handleExpenseBeforeTrigger(recs);
        for(Milestone1_Expense__c rec : recs)
        {
            System.assertEquals(UserInfo.getUserId(),rec.Incurred_By__c);
            System.assertEquals(Date.today(),rec.Date_Incurred__c);
        }
    }
    
}