/**
* @author Lucas Caceres (Fielo)
* @date 07/06/2016
* @description 
*/
public with sharing class FieloPRM_VFC_PublicBanner {
    
    public String sectionPlacement {get;set;}
    public String componentId {get;set;}
    public Integer quantity{get{if(quantity == null) quantity = 1; return quantity;}set;}

    public FieloEE__Component__c comp {get{
            system.debug('comp get');
            system.debug('componentId: ' + componentId);
            if(comp == null){            
                if(componentId != null){
                    /*if(PageController.componentsMap != null){                
                        comp = PageController.componentsMap.get(componentId);
                    }else{*/
                        system.debug('comp before query: ' + comp);
                        comp = [SELECT FieloEE__CSSClasses__c, FieloEE__BannerPlacement__c, FieloEE__Category__c, FieloEE__Tag__c, FieloEE__FieldSet__c, FieloEE__Modal__c FROM FieloEE__Component__c WHERE Id =: componentId];
                        system.debug('comp after query: ' + comp);
                    //}
                }
            }
            return comp;
        } set;}

        public static String docURL{get{
            if(docURL == null){
                String instance = FieloEE__PublicSettings__c.getOrgDefaults().FieloEE__Instance__c;
                if(String.isNotBlank(instance)){
                    docURL = '//c.' + instance + '.content.force.com';
                }else{
                    docURL = '';
                }
            }   
            return docURL;
        } set;}


    public static FieldsML fieldsML{get{if(fieldsML == null) fieldsML = getFieldsML(); return fieldsML;} set;}

    private static FieldsML getFieldsML(){
        system.debug('getFieldsML');
        FieldsML result = new FieldsML();
        String language = FieloEE.OrganizationUtil.getLanguage();
        Set<String> bannerFields = Schema.SObjectType.FieloEE__Banner__c.fields.getMap().keySet();
        
        String fieldNameAttachment = 'AttachmentId_' + language + '__c';
        Boolean attachmentMLExist = bannerFields.contains(fieldNameAttachment.toLowerCase());
        if(attachmentMLExist)
            result.fieldAttachment = fieldNameAttachment;
        else
            result.fieldAttachment = FieloEE.OrganizationUtil.getPrefix() + 'AttachmentId__c';

        String fieldNameLink = 'Link_' + language + '__c';
        Boolean linkMLExist = bannerFields.contains(fieldNameLink.toLowerCase());
        if(linkMLExist)
            result.fieldLink = fieldNameLink;
        else
            result.fieldLink = FieloEE.OrganizationUtil.getPrefix() + 'Link__c';

        String fieldNameDescription = 'Description_' + language + '__c';
        Boolean descriptionMLExist = bannerFields.contains(fieldNameDescription.toLowerCase());
        if(descriptionMLExist)
            result.fieldDescription = fieldNameDescription;
        else
            result.fieldDescription = FieloEE.OrganizationUtil.getPrefix() + 'Description__c';

        String fieldNameOverlayHTML = 'OverlayHtml_' + language + '__c';
        Boolean overlayHTMLMLExist = bannerFields.contains(fieldNameOverlayHTML.toLowerCase());
        if(overlayHTMLMLExist)
            result.fieldOverlayHTML = fieldNameOverlayHTML;
        else
            result.fieldOverlayHTML = FieloEE.OrganizationUtil.getPrefix() + 'OverlayHtml__c';

        String fieldNameOverlayText = 'OverlayText_' + language + '__c';
        Boolean overlayTextMLExist = bannerFields.contains(fieldNameOverlayText.toLowerCase());
        if(overlayTextMLExist)
            result.fieldOverlayText = fieldNameOverlayText;
        else
            result.fieldOverlayText = FieloEE.OrganizationUtil.getPrefix() + 'OverlayText__c';
    
        return result;
    }

    public FieloPRM_VFC_PublicBanner(){}
   
    private List<FieloEE__Banner__c> bannerItems;
    public List<FieloEE__Banner__c> getBannerItems(){

        String whereCondition = '';
        String publicFilter =Apexpages.currentPage().getParameters().get('pf');
        if(!String.isEmpty(publicFilter)){
            whereCondition = ' AND F_PRM_PublicFilter__c = \'' + publicFilter + '\' ';
        
            system.debug('getBannerItems');
            if(bannerItems == null){


                Set<String> fieldset = new Set<String>{fieldsML.fieldAttachment, fieldsML.fieldDescription, fieldsML.fieldOverlayHTML, fieldsML.fieldOverlayText, fieldsML.fieldLink};
                bannerItems = getBanners(fieldset, FieloEE.ProgramUtil.getProgramByDomain().Id, FieloEE.MemberUtil.getMemberId(),  componentId, quantity, null, null, whereCondition);          
            }
            return bannerItems;
        }else{
            return null;
        }
        
    }

    public class fieldsML{
        public String fieldAttachment{get; set;}
        public String fieldLink{get; set;}
        public String fieldDescription{get; set;}
        public String fieldOverlayHTML{get; set;}
        public String fieldOverlayText{get; set;}
    }


    public static List<FieloEE__Banner__c> getBanners(Set<String> fields, Id programId, Id memberId, Id componentId,  Integer quantity, Integer offset, String orderBy, String whereCondition){                
        
        system.debug('fields: ' + fields);
        system.debug('programId: ' + programId);
        system.debug('memberId: ' + memberId);
        system.debug('componentId: ' + componentId);
        system.debug('whereCondition: ' + whereCondition);
        String query = 'SELECT ' + String.join(new List<String>(fields), ',') + 
            ' FROM FieloEE__Banner__c WHERE FieloEE__Program__c =: programId AND FieloEE__IsActive__c = true AND ' + 
            ' ( ( FieloEE__From__c = null OR FieloEE__From__c <= Today ) AND ( FieloEE__To__c = null OR FieloEE__To__c >= Today ) )  ';

        if(componentId != null){
            //COMPONENT
            query += '  AND FieloEE__Component__c =: componentId ';
        }
        
        Set<Id> bannerIds;
        
       

        if(!String.isEmpty(whereCondition) ){
            query += whereCondition;
        }
        
        

        if(String.isNotBlank(orderBy)){
            query += ' ORDER BY ' + orderBy;
        }else{
            query += ' ORDER BY FieloEE__Order__c ';
        }

        //LIMIT
        if(quantity != null){
            query += ' LIMIT ' + quantity;
        }

        //OFFSET
        if(offset != null){
            query += ' OFFSET ' + offset;
        }

        system.debug('query: ' + query);

        return Database.query(query);        
    }
    
}