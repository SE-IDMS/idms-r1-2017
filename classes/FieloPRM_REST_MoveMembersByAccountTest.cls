@isTest

private class FieloPRM_REST_MoveMembersByAccountTest{

    static testMethod void unitTest(){
    
         
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'lastAccName'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'firstAccName';
            member.FieloEE__Street__c = 'testStreeet';
            member.F_Account__c = acc.id;
                 
            insert member;
            
            FieloEE__Member__c membertO = new FieloEE__Member__c();
            membertO .FieloEE__LastName__c= 'testt';
            membertO .FieloEE__FirstName__c = 'tesstt';
            membertO .FieloEE__Street__c = 'tets';
            membertO .F_Account__c = accTo.id;
                 
            insert membertO ;     
            
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;
            
        
            FieloPRM_REST_MoveMembersByAccount rest = new FieloPRM_REST_MoveMembersByAccount();
            FieloPRM_REST_MoveMembersByAccount.postMoveMembersByAccount(acc.id,accTo.id);
        }

    }
    
}