public class AP_CAR_UpdateRequirementFeature {

    public static void updateContactFeature(map<Id,ContactAssignedRequirement__c> mapNewContactRequirement , map<Id, ContactAssignedRequirement__c> mapOldContactRequirement)
    {
        list<ContactAssignedFeature__c> lstContactFeature = new list<ContactAssignedFeature__c>();
        map<Id,ContactAssignedRequirement__c> mapConFeatureReq = new  map<Id,ContactAssignedRequirement__c>();
        
        for(Id ReqID : mapNewContactRequirement.keySet())
        {
            if(mapNewContactRequirement.get(reqID).RequirementStatus__c != mapOldContactRequirement.get(reqID).RequirementStatus__c && mapNewContactRequirement.get(reqID).FeatureRequirement__c != null)
                mapConFeatureReq.put(mapNewContactRequirement.get(reqID).ContactAssignedFeature__c,mapNewContactRequirement.get(reqID));
        }
        
        lstContactFeature = [Select id,Active__c from ContactAssignedFeature__c where id in :mapConFeatureReq.keySet() limit 10000];
        if(!lstContactFeature.isEmpty())
        {
            for(ContactAssignedFeature__c conFTR : lstContactFeature)
            {
                if(mapConFeatureReq.get(conFTR.ID).RequirementStatus__c == Label.CLMAY13PRM16 || mapConFeatureReq.get(conFTR.ID).RequirementStatus__c == Label.CLMAY13PRM17)
                    conFTR.Active__C = true;
                else
                    conFTR.Active__C = false;
                    
            }
            try
            {
            	update lstContactFeature;
            }
            catch(Exception e)
            {
            	for (Integer i = 0; i < e.getNumDml(); i++) 
            	{ 
                	mapNewContactRequirement.values().get(0).addError( e.getDmlMessage(i)); 
            	} 
            }
        }
        
        
    }
}