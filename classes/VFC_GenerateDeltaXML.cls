public class VFC_GenerateDeltaXML {   
    
    public List<String> CompListID{get;set;}
    public List<Component__c> NewCompList = new List<Component__c>();
    public String compXMLResult {get;set;}   
    Map<string,string> CompMap = new Map<String,String>();
    Map<String, List<String>> packageComponentMap = new Map<String, List<String>>();    
    
     public VFC_GenerateDeltaXML(ApexPages.StandardSetController controller) {
        /*------------------MAP OF COMPONENTS----------------------------*/
        CompMap.put('Account Owner Sharing Rule','AccountOwnerSharingRule');
        CompMap.put('Account Criteria Based Sharing Rule','AccountCriteriaBasedSharingRule');     
        CompMap.put('Analytic Snapshot','AnalyticSnapshot');
        CompMap.put('Account Owner Sharing Rule','AccountOwnerSharingRule');
        CompMap.put('Apex Class','ApexClass');     
        CompMap.put('Apex Sharing Reason','SharingReason');
        CompMap.put('Apex Trigger','ApexTrigger');   
        CompMap.put('App','CustomApplication');
        CompMap.put('Button or Link','WebLink');
        CompMap.put('Campaign Criteria Based Sharing Rule','CampaignCriteriaSharingRule');
        CompMap.put('Campaign Owner Sharing Rule','CampaignOwnerSharingRule');
        CompMap.put('Case Criteria Based Sharing Rule','CaseCriteriaSharingRule');
        CompMap.put('Case Owner Sharing Rule','CaseOwnerSharingRule');
        CompMap.put('Contact Criteria Based Sharing Rule','ContactCriteriaSharingRule');
        CompMap.put('Contact Owner Sharing Rule','ContactOwnerSharingRule');
        CompMap.put('Custom Data Type','CustomDataType');
        CompMap.put('Profile','Profile');
        CompMap.put('Custom Field','CustomField');  
        CompMap.put('Custom Label','CustomLabel');    
        CompMap.put('Custom Object','CustomObject');
        CompMap.put('Custom Object Criteria Based Sharing Rule','CustomObjectCriteriaSharingRule');
        CompMap.put('Custom Object Owner Sharing Rule','CustomObjectOwnerSharingRule'); 
        CompMap.put('CustomPageWebLink','CustomPageWebLink'); // added for DEC 2014 HotFIX  
        CompMap.put('Custom Report Type','ReportType');
        CompMap.put('Custom Setting','CustomObject');
        CompMap.put('Dashboard','Dashboard');
        CompMap.put('Document','Document');
        CompMap.put('Email Template','EmailTemplate');
        CompMap.put('Field Set','FieldSet');
        //CompMap.put('Flow','InteractionDefinition'); For Oct 13 Release By Vimal
        CompMap.put('Flow','Flow'); //For Oct 13 Release By Vimal
        CompMap.put('Approval Process','ApprovalProcess');  //For Oct 13 Release By Vimal
        CompMap.put('Folder','Folder');
        CompMap.put('Group','Group');    
        CompMap.put('Home Page Component','HomePageComponent');     
        CompMap.put('Home Page Layout','HomePageLayout');
        //CompMap.put('Language Translation','Translation');
        CompMap.put('Language Translation','CustomObjectTranslation');
        CompMap.put('Lead Criteria Based Sharing Rule','LeadCriteriaSharingRule');
        CompMap.put('Lead Owner Sharing Rule','LeadOwnerSharingRule');
        CompMap.put('Letterhead','Letterhead');  // corrected for DEC 2014 HotFIX  
        CompMap.put('List View','ListView');
        CompMap.put('Opportunity Criteria Based Sharing Rule','OpportunityCriteriaSharingRule');
        CompMap.put('Opportunity Owner Sharing Rule','OpportunityOwnerSharingRule');
        CompMap.put('Page Layout','Layout');
        CompMap.put('Permission Set','PermissionSet');
        CompMap.put('Queue','Queues');
        CompMap.put('Record Type','RecordType');
        CompMap.put('Remote Site','RemoteProxy');
        CompMap.put('Report','Report');
        CompMap.put('Role','UserRole');
        CompMap.put('S-Control','Scontrol');
        CompMap.put('Static Resource','StaticResource');     
        CompMap.put('Tab','CustomTab');
        CompMap.put('Validation Rule','ValidationRule');
        CompMap.put('Visualforce Component','ApexComponent');
        CompMap.put('Visualforce Page','ApexPage');   
        CompMap.put('Workflow Email Alert','WorkflowAlert');   
        CompMap.put('Workflow Field Update','WorkflowFieldUpdate');     
        CompMap.put('Workflow Outbound Message','WorkflowOutboundMessage');
        CompMap.put('Workflow Rule','WorkflowRule'); 
        CompMap.put('Workflow','Workflow');    
        CompMap.put('Workflow Task','WorkflowTask');
        //Srinivas  MAY13
        CompMap.put('Lookup filter','NamedFilter');
        /*----------END OF MAP---------------------*/
          
        CompListID = new List<String>(); 
        List<Component__c> components = (List<Component__c>)controller.getSelected();
        
        for(Component__C compItem:components) {  
            CompListID.add(compItem.ID);
        }
        
       If(CompListID != null && CompListID.size()>0){
           NewCompList = [Select FieldName__c,ComponentType__c from Component__c Where Id IN:CompListID AND Ismigrate__c = TRUE];
       }
       
       If(NewCompList != null && NewCompList.size()>0){
       
        for(Component__c objComponent: NewCompList){    
            if(packageComponentMap.containsKey(objComponent.ComponentType__c)){
                packageComponentMap.get(objComponent.ComponentType__c).add(objComponent.FieldName__c);
            }
            else{
                List<String> lstFieldName = new List<String>();
                lstFieldName.add(objComponent.FieldName__c);
                packageComponentMap.put(objComponent.ComponentType__c,lstFieldName);
            }
        }        
        
           if(packageComponentMap.size()>0){            
              compXMLResult = GenarateXMLCls(packageComponentMap);    
              System.debug(compXMLResult); 
           }            
       }
       else
       {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Records found!, Check Migrate checkbox for the components..');
        ApexPages.addmessage(myMsg);
       }
    }
    
  
    
    public string GenarateXMLCls(Map<String, List<String>> pckComponentMap)
    {
          XmlStreamWriter w = new XmlStreamWriter();  
     
           
          w.writeStartDocument('UTF-8','1.0');                  
          w.writeStartElement(null, 'Package',null);        
          w.writeNamespace('xmlns', 'http://soap.sforce.com/2006/04/metadata');
           for(String objComponent: pckComponentMap.Keyset())
           {              
               w.writeStartElement(null, 'types', null);                
               for(String valuefromlst :pckComponentMap.get(objComponent))
               {
                  if(valuefromlst!='' && valuefromlst!=null){
                      w.writeStartElement(null, 'members', null);
                      w.writeCharacters(valuefromlst);
                      w.writeEndElement(); // End Members
                  }            
               }     
              w.writeStartElement(null, 'name', null);
              w.writeCharacters(CompMap.get(objComponent));
              w.writeEndElement(); // End name             
              w.writeEndElement(); //End Types
             
          }
          w.writeStartElement(null, 'version', null);
          //w.writeCharacters('25.0'); 
          //w.writeCharacters('28.0'); //Updated to Version 28 By Vimal For Oct Release
          w.writeCharacters('31.0'); //update for DEC 2014 Hotfix
          w.writeEndElement(); // End version         
          w.writeEndElement(); //End Package                
          String xmlOutput = w.getXmlString();         
          w.close();        
          return xmlOutput;
          }  
}