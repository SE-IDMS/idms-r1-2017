/*
    Author          : Hari Krishna
    Date Created    : 30/10/2013 November Release
    Description     : Batch Apex class that creates the products 
              
*/
global class Batch_ProductIsEnriched implements Database.Batchable<sObject>{
    
   global final String Query;
   global String ErrorMessages ='';
   /*
    global void execute(SchedulableContext sc) {
      
      String querySt ='Select id,ExtProductId__c , Name ,CategoryId__r.Name,SKU__c,Brand2__c , Brand2__r.SDHBRANDID__c,DeviceType2__c,DeviceType2__r.SDHDEVICETYPEID__c, CategoryId__c,CategoryId__r.SDHCategoryID__c, TECH_IsEnriched__c from product2 where  Brand2__c <> NULL AND DeviceType2__c <> NULL AND CategoryId__c <> null AND TECH_IsEnriched__c = false ';
      Batch_ProductIsEnriched batch = new Batch_ProductIsEnriched();
      ID batchprocessid = Database.executeBatch(batch,Integer.valueOf(system.label.CLOCT13SRV66));
   }
  */
  

   global Database.QueryLocator start(Database.BatchableContext BC){
        String querySt ='Select id,ExtProductId__c , Name ,CategoryId__r.Name,SKU__c,Brand2__c , Brand2__r.SDHBRANDID__c,DeviceType2__c,DeviceType2__r.SDHDEVICETYPEID__c, CategoryId__c,CategoryId__r.SDHCategoryID__c, TECH_IsEnriched__c from product2 where  Brand2__c <> NULL AND DeviceType2__c <> NULL AND CategoryId__c <> null AND TECH_IsEnriched__c = false ';
      //Batch_ProductIsEnriched batch = new Batch_ProductIsEnriched();
      return Database.getQueryLocator(querySt );
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<product2> products = (List<Product2>)scope;
        Set<String> Brandset = new Set<String>();
        Set<String> DeviceTypeset = new Set<String>();
        Set<String> Categoryset = new Set<String>(); 
        /*
        Map<String,Brand__c>  brandMap = new Map<String,Brand__c>();    
        Map<String,DeviceType__c>  DeviceTypeMap = new Map<String,DeviceType__c>(); 
        Map<String,Category__c>  CategoryMap = new Map<String,Category__c>(); 
        */
        Set<String> tempExtidSet = new set<String>(); 
        Set<String> existingExtidset = new set<String>();
        Set<String> uniqueExtidset = new set<String>();
         
        List<product2> prodtocreate = new  List<product2>();
        System.debug('\n CLog: '+prodtocreate);
        /*
         for(product2 prod : products){
                if(prod.Brand__c != null )
                Brandset.add(prod.Brand2__c);
                if(prod.DeviceType__c != null )
                DeviceTypeset.add(prod.DeviceType2__c);
               //RHA correction : CategoryId__c instead of SDHCategoryId__c
                if(prod.CategoryId__c != null )
                 //RHA correction : CategoryId__c instead of SDHCategoryId__c
                Categoryset.add(prod.CategoryId__c);             
                
         }
         if(Brandset != null && Brandset.size()>0)
         {
        
            for(Brand__c  obj :[select id, SDHBRANDID__c  from Brand__c where SDHBRANDID__c in : Brandset])
            {
                brandMap.put(obj.SDHBRANDID__c , obj);
            }
            
         }
         if(DeviceTypeset != null && DeviceTypeset.size()>0)
         {
            for(DeviceType__c  obj :[select id, SDHDEVICETYPEID__c  from DeviceType__c where SDHDEVICETYPEID__c in : DeviceTypeset])
            {
                DeviceTypeMap.put(obj.SDHDEVICETYPEID__c , obj);
            }
            
         }
         if(Categoryset != null && Categoryset.size()>0)
         {
            for(Category__c  obj :[select id, SDHCategoryID__c  from Category__c where id in : Categoryset])
            {
                CategoryMap.put(obj.SDHCategoryID__c , obj);
            }
            
         }
         */
         
         
          for(product2 prod : products){
            
            String ExtProduct ='';
            
            ExtProduct +=prod.CategoryId__r.SDHCategoryID__c;
            ExtProduct +='_';               
            ExtProduct +=prod.Brand2__r.SDHBRANDID__c;
            ExtProduct +='_';    
            ExtProduct +=prod.DeviceType2__r.SDHDEVICETYPEID__c;
            tempExtidSet.add(ExtProduct);
            /*
                Product2 newprod = new Product2();
                //RHA product Name = Category Name
                newprod.name = CategoryMap.get(prod.CategoryID__c).Name;
                if(prod.Brand2__c != null && brandMap.containskey(prod.Brand2__c))
                    newprod.Brand2__c =  brandMap.get(prod.Brand2__c).id;
                    
                if(prod.DeviceType2__c != null && DeviceTypeMap.containskey(prod.DeviceType2__c))
                    newprod.DeviceType2__c =  DeviceTypeMap.get(prod.DeviceType2__c).id;
                    
                    // RHA CategoryId__c instead of SDHCategoryId__c
                if(prod.CategoryId__c != null && CategoryMap.containskey(prod.CategoryId__c))
                   // RHA correction CategoryId__c instead of DeviceType__c
                    newprod.CategoryId__c =  CategoryMap.get(prod.CategoryId__c).id;
                
                newprod.IsActive = true;
                // RHA field added
                newprod.TECH_IsEnriched__c = true;
                
                prodtocreate.add(newprod);  
                */        
                
         }
         if(tempExtidSet != null && tempExtidSet.size()>0){
            
            for(product2 prod :[Select id,ExtProductId__c , Name from  product2 where   TECH_IsEnriched__c = true and ExtProductId__c in :tempExtidSet])
            existingExtidset.add(prod.ExtProductId__c);
            System.debug('\n CLog: '+existingExtidset);
            System.debug('\n CLog: '+existingExtidset.size());
            
         }
         for(product2 prod : products){
            String ExtProduct ='';
            ExtProduct +=prod.CategoryId__r.SDHCategoryID__c;
            ExtProduct +='_';               
            ExtProduct +=prod.Brand2__r.SDHBRANDID__c;
            ExtProduct +='_';    
            ExtProduct +=prod.DeviceType2__r.SDHDEVICETYPEID__c;
            System.debug('\n CLog: '+ExtProduct);
            System.debug('\n CLog: '+uniqueExtidset);
            System.debug('\n CLog: '+existingExtidset);
            
            if(!uniqueExtidset.contains(ExtProduct) && !existingExtidset.contains(ExtProduct) ){
                
                Product2 newprod = new Product2();
                newprod.name = prod.CategoryId__r.Name;
                newprod.Brand2__c = prod.Brand2__c;
                newprod.DeviceType2__c =  prod.DeviceType2__c;
                newprod.CategoryId__c =   prod.CategoryId__c;
                newprod.IsActive = true;
                newprod.TECH_IsEnriched__c = true;
                newprod.ExtProductId__c = ExtProduct;
                prodtocreate.add(newprod);  
                uniqueExtidset.add(ExtProduct);
            }
            
            System.debug('\n CLog: '+prodtocreate);
            /*
                Product2 newprod = new Product2();
                //RHA product Name = Category Name
                newprod.name = CategoryMap.get(prod.CategoryID__c).Name;
                if(prod.Brand2__c != null && brandMap.containskey(prod.Brand2__c))
                    newprod.Brand2__c =  brandMap.get(prod.Brand2__c).id;
                    
                if(prod.DeviceType2__c != null && DeviceTypeMap.containskey(prod.DeviceType2__c))
                    newprod.DeviceType2__c =  DeviceTypeMap.get(prod.DeviceType2__c).id;
                    
                    // RHA CategoryId__c instead of SDHCategoryId__c
                if(prod.CategoryId__c != null && CategoryMap.containskey(prod.CategoryId__c))
                   // RHA correction CategoryId__c instead of DeviceType__c
                    newprod.CategoryId__c =  CategoryMap.get(prod.CategoryId__c).id;
                
                newprod.IsActive = true;
                // RHA field added
                newprod.TECH_IsEnriched__c = true;
                
                prodtocreate.add(newprod);  
                */        
                
         }
         if(prodtocreate != null && prodtocreate.size()>0){
            
            System.debug('\n CLog: '+prodtocreate);
            System.debug('\n CLog: '+prodtocreate.size());
           Database.SaveResult[] sresults = Database.insert(prodtocreate, false);
            
            for(Integer k=0;k<sresults.size();k++ )
            {
                Database.SaveResult sr =sresults[k];
                if(!sr.isSuccess())
                {                        
                    String Message ='';
                    for(Database.Error err : sr.getErrors()) {
                        Message= ' '+err.getFields()+' Error : '+err.getMessage();
                        
                    }                        
                    ErrorMessages +=Message;    
                } 
            }                 
            
         }
     
     
     
     
    }

   global void finish(Database.BatchableContext BC){
        
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'harikrishna.singara@schneider-electric.com'};
        // String[] toAddresses = new String[] {'ramzi.haddad@accenture.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch_ProductIsEnriched Execution Report');
        String message = ErrorMessages;
        mail.setPlainTextBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
   }

}