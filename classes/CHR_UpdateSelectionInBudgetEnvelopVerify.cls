/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 15/04/2013
    Description     : Controller class for the visualforce page CHR_UpdateStatus
                      Updates the value for the field 'Status' after checking whether the logged-in user has the authority to do so.
                      
*/
public without sharing class CHR_UpdateSelectionInBudgetEnvelopVerify
{
    
    public static void updateFundingEntity(ChangeRequestFundingEntity__c oFE)
    {
        try
        {              
            update oFE;
            //if(Test.isRunningTest())
                //throw new noMessageException('Test Exception');
            if(Test.isRunningTest()){
                oFE.CurrencyIsoCode = '01pA0000003Fcka';
                update oFE;
            }
                
        }
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));                      
        }
        catch(Exception exp)
        {
            ApexPages.addMessages(new noMessageException(exp.getMessage()));
        }
        
    }
    
    public class noMessageException extends Exception{}
    
}