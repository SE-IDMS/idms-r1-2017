@isTest
private class VFC73_AddRMAProduct_TEST
{
    static testMethod void testVFC73() 
    {
    
        String testcr='TEST_XBTG';
      Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        objOpenCase.CommercialReference__c= 'Test Commercial Reference';
        objOpenCase.ProductBU__c = 'Business';
        objOpenCase.ProductLine__c = 'ProductLine';
        objOpenCase.ProductFamily__c ='ProductFamily';
        objOpenCase.Family__c ='Family';
        Database.insert(objOpenCase);
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
/*      Country__c CountryObj = new Country__c(
        region__c='APAC', 
        countrycode__c='IN' 
        //frontofficeorganization__c=BusinessRiskEscalationEntityObj.id  
        );
        insert CountryObj;
*/
        Country__c CountryObj = country;

        StateProvince__c StateProvinceObj = new StateProvince__c(
        /*Required Field Start*/
        stateprovincecode__c='01', /*Required Field End*/
        /*Required Field Start*/
        countrycode__c='IN', /*Required Field End*/
        stateprovinceexternalid__c='IN01', country__c=CountryObj.id  /* For Country__c id=a0H17000001H401EAC  */
        );
        insert StateProvinceObj;
            
        /* BusinessRiskEscalationEntity__c BREEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        insert BREEntity;
        BusinessRiskEscalationEntity__c BRESubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        insert BRESubEntity;
        BusinessRiskEscalationEntity__c BusinessRiskEscalationEntityObj = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        insert BusinessRiskEscalationEntityObj;
        system.debug('****Orgid and Name***'+BusinessRiskEscalationEntityObj.id +' * '+BusinessRiskEscalationEntityObj.Name);
        system.debug('****Orgid and Name1***'+BRESubEntity.id+ ' * '+BRESubEntity.Name);*/
        BusinessRiskEscalationEntity__c oResolutionBREE1 = new BusinessRiskEscalationEntity__c(Entity__c  =  'TESTBREE',Location_Type__c = 'Country Front Office');
             insert oResolutionBREE1;
              User newUser = Utils_TestMethods.createStandardUser('TestUser1'); 
            BusinessRiskEscalationEntity__c oResolutionBREE = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE', SubEntity__c =  'BRESubEnty', Location_Type__c = 'Country Front Office', Street__c = 'Street__c');
            insert oResolutionBREE;
            oResolutionBREE.Street__c = '365437sdStreet__c232';
            update oResolutionBREE;
            BusinessRiskEscalationEntity__c oOriginatingBREE1 = new BusinessRiskEscalationEntity__c(SubEntity__c = 'BRESubEnty', Location__c = 'BRELocation123', Entity__c = 'TESTBREE', Location_Type__c = 'Country Front Office');
             insert oOriginatingBREE1; 
        TEX__c TEXObj = new TEX__c(
        assessmenttype__c='Product Return to Expert Center', tech_contactemail__c='sukumar.salla@schneider-electric.com', receive_automatic_email__c=false, receiveautomaticemailtocustomer__c=false, frontofficeorganization__c=oOriginatingBREE1.id,   /* For BusinessRiskEscalationEntity__c id=a1H1700000000NUEAY  */forcemanualselection__c=false, 
        expertcenter__c=oOriginatingBREE1.id,   /* For BusinessRiskEscalationEntity__c id=a1H1700000000NUEAY  */sendautomaticemailtocustomerineng__c=false, contactemail__c='sukumar.salla@schneider-electric.com', contactname__c='I2P Test Contact', status__c='Expert Assessment to be started', opendate__c=System.today(), 
        manualexpertcenteroverride__c=false, prioritylevel__c='High', affectedoffer__c='INTRUSION', tech_csiflag__c=false, confirm__c=false,PromiseDate__c=System.today(),LineofBusiness__c=oOriginatingBREE1.id,FrontOfficeTEXManager__c=newUser.id
        );
        system.debug('****TEXObj***'+TEXObj );
        insert TEXObj;      
        
        RMA__c RMA1 = new RMA__c();
        RMA1.case__c = objOpenCase.Id;
        RMA1.AccountName__c = objAccount.Id;

        //Account Address
        RMA1.AccountStreet__c  = 'Account Street';
        RMA1.AccountCity__c    = 'Account City';
        RMA1.AccountZipCode__c = '12345';
        RMA1.AccountCountry__c = country.Id;
        RMA1.AccountPOBox__c   = 'Account PO Box';

        //Shipping Address
        RMA1.ShippingStreet__c  = 'Shipping Street';
        RMA1.ShippingCity__c    = 'Shipping City';
        RMA1.ShippingZipCode__c = '67890';
        RMA1.ShippingCountry__c = country.Id;
        RMA1.ShippingPOBox__c   = 'Shipping PO Box';
        //RMA1.Status__c=Label.CLMAY13RR12;
        insert RMA1;
        
        RMA_Product__c ReturnedProducts = new RMA_Product__c();
        ReturnedProducts.RMA__c = RMA1.ID;
        insert ReturnedProducts;

        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(ReturnedProducts);   
        VFC73_AddRMAProduct GMRSearchPage = new VFC73_AddRMAProduct(CaseStandardController );
        
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        GMRSearchPage.jsonSelectString=jsonSelectString;
        Test.startTest();
        PageReference pageRef = Page.VFP73_AddRMAProduct;
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);  
        pageRef.getParameters().put('commercialReference',testcr);  
        pageRef.getParameters().put('retURL','AnyRMAId');  
        
        Test.setCurrentPage(pageRef);

        GMRSearchPage.performSelect();
        GMRSearchPage.performSelectFamily();
        GMRSearchPage.pageCancelFunction();
        GMRSearchPage.goToProductEdit();
      
      List<Object> test1;
      VFC73_AddRMAProduct.remoteSearch('Test Commercial Reference','12',true,false,false,false,true);
      VFC73_AddRMAProduct.remoteSearch('Test Commercial Reference','1234',true,false,false,false,true);
      VFC73_AddRMAProduct.remoteSearch('Test Commercial Reference','123456',true,false,false,false,true);
      VFC73_AddRMAProduct.remoteSearch('Test Commercial Reference','12345678',true,false,false,false,true);
      
      VFC73_AddRMAProduct.getOthers('ANYBUSINESS');
      
      pageRef.getParameters().put('retURL',RMA1.id);  
      pageRef.getParameters().put('mode','ChP');
      pageRef.getParameters().put('jsonSelectString','');  
      Test.setCurrentPage(pageRef);
      
      RMA1.Status__c=Label.CLMAY13RR12;
      RMA1.TEX__c=TEXObj.id;
      update RMA1;
      GMRSearchPage = new VFC73_AddRMAProduct(CaseStandardController );
      GMRSearchPage.jsonSelectString='';
      try{
        GMRSearchPage.performSelect();
        } catch (Exception e){}
        
        try{
        GMRSearchPage.performSelectFamily();
        } catch(Exception e){}
      
      GMRSearchPage.Validate();
      GMRSearchPage.doGoBack();
      
        Test.stopTest();
    }
}