//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : class used as controller for VF page update password
//
//*******************************************************************************************

public with sharing class VFC_IdmsUserPasswordUpdateController{
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}  
    public String myemail{get; set;}  
    public Boolean setpwdFlag{get; set;}  
    public Boolean resetpwdFlag{get; set;}    
    public Boolean pwdresetSuccess{get; set;}
    public boolean redirectToApp {get; set;}
    public boolean testclassvar{get; set;}
    public String testStringvar{get; set;}  
    public String appUrl{get; set;} 
    public string authToken = ApexPages.currentPage().getParameters().get('token');
    public string registrationSource = ApexPages.currentPage().getParameters().get('App');
    
    //set password to activate identity
    public PageReference setPassword(){
        if(!Pattern.matches(Label.CLJUN16IDMS48, newPassword ))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS52)); 
            return null;
        }
        if(verifyNewPassword!=newPassword )
        {        
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS53));    
            return null;
        }
        else{
            try{
                IDMSUserService userService =new IDMSUserService();
                //needs to uncommented when we get correct values
                Boolean res	= userService.activateIdentity(newPassword,authToken);
                if(res){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Your password has been set successfully.'));
                    redirectToApp = true;
                    return null;
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Error in setting password')); 
                    return null;
                }
            }
            
            catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Error in setting password')); 
                return null;
            }
        }
        setpwdFlag = true;
        return null;
    }
    
    //set password     
    public PageReference usersetPassword(){
        if(!Pattern.matches(Label.CLJUN16IDMS48, newPassword ))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS52)); 
            return null;
        }
        if(verifyNewPassword != newPassword )
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS53)); 
            return null;
        }
        else{
            try{
                IDMSUserService userService =new IDMSUserService();
                //needs to uncommented when we get correct values
                Boolean res	= userService.uimsSetPassword(newPassword,authToken);
                //this condition will run in case of test class only
                if(Test.isRunningTest()){
                    res		= testclassvar;
                    testStringvar.length();
                }
                //end test condition  
                if(res){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Your password has been set successfully.'));
                    redirectToApp=true;
                    return null;
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Error in setting password')); 
                    return null;
                }
            }
            
            catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Error in setting password')); 
                return null;
                
            }
        }
        setpwdFlag	= true;
        return null;
    }
    
    //search user in UIMS
    public void searchusermethod(string email){
        IDMSUserService userService = new IDMSUserService();
        try{
            WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType resp1 = userService.SearchUserUIMSReset(email); 
            system.debug('--search user resp111111--'+resp1 );
            if(resp1 != null){
                String fedId	= resp1.federatedId; 
                resetPasswordMethod(fedId);
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLJUN16IDMS54));
            }
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLJUN16IDMS54)); 
        }
    }
    //reset password in UIMS
    public PageReference resetPasswordMethod(String federatedId){
        system.debug('---fed id--'+federatedId);
        try{
            IDMSUserService userService = new IDMSUserService();
            string emailValue			= userService.resetPassword(federatedId,registrationSource);
            if(emailValue == myemail){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, Label.CLJUN16IDMS65));
                return null;
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLJUN16IDMS55)); 
                return null;
            }
        }
        catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLJUN16IDMS55)); 
            return null;
        }
    }
    
    //reset password method
    public PageReference resetPassword(){
        if(!String.isBlank(myemail)){
            searchusermethod(myemail);
            return null;
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLJUN16IDMS08)); 
            return null;
        }
        return null;
    }
    
    // constructor used for fetching data from custom settings and redirection
    public VFC_IdmsUserPasswordUpdateController() {
        //get ap url on the basis of app name
        IdmsAppMapping__c appmap;
        appmap=IdmsAppMapping__c.getInstance(registrationSource);
        if(appmap != null){
            appUrl		= (String)appmap.get('AppURL__c');
            if(appUrl == null){
                appUrl	= Label.CLJUN16IDMS26;
            }
        }
        else{
            appUrl		= Label.CLJUN16IDMS26;
        }
    }
    
}