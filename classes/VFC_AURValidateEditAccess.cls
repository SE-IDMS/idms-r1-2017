public with sharing class VFC_AURValidateEditAccess {
    
    
    public AccountUpdateRequest__c aur {get;set;} {aur = new AccountUpdateRequest__c();}
   
    public boolean UserHasEditAccess = false;
    
    public VFC_AURValidateEditAccess(ApexPages.StandardController controller) {
        aur = (AccountUpdateRequest__c)controller.getRecord();
        aur = [select Id, Account__c, ApprovalStatus__c from AccountUpdateRequest__c where id = :aur.id ];
    }
    
    
    public Pagereference redirect()
    {
        
        UserRecordAccess ura = [SELECT RecordId, HasEditAccess,HasReadAccess FROM UserRecordAccess WHERE RecordId = :aur.Account__c AND UserId = :UserInfo.getUserid() ];
        System.Debug(ura+ '  '+aur);
        
        if(ura.HasEditAccess == false)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLAPR14ACC02)); 
            return null;
        }
        else
        {
            return new ApexPages.StandardController(aur).edit();
        }
        
    }

}