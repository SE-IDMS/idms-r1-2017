@isTest
private class PRJ_QuotedGateReportLink_TEST
{
    static testMethod void test_PRJ_QuotedGateReportLink()
    {
        User u = [SELECT Id FROM User WHERE Profile.Name ='SE - DMT Standard User' and IsActive =true  limit 1 ];
        DMTAuthorizationMasterData__c d = new DMTAuthorizationMasterData__c();
        d.BusinessTechnicalDomain__c = 'Servers';
        d.AuthorizedUser6__c = u.Id;
        d.NextStep__c = 'Quoted';
        insert d;
        DMTAuthorizationMasterData__c d1 = new DMTAuthorizationMasterData__c();
        d1.BusinessTechnicalDomain__c = 'CS&Q';
        d1.AuthorizedUser6__c = u.Id;
        d1.NextStep__c = 'Quoted';
        insert d1;
        System.runAs(u)
        {
            System.debug('!!!!!!!!!! Auth user');
            PRJ_QuotedGateReportLink vfCtrl = new PRJ_QuotedGateReportLink();
            vfCtrl.assignValuesToReport();
        }
        u.Id = System.label.DMTCIO;
        System.runAs(u)
        {
            System.debug('!!!!!! CIO');
            PRJ_QuotedGateReportLink vfCtrl = new PRJ_QuotedGateReportLink();
            vfCtrl.assignValuesToReport();
        }
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tst1');
        insert runAsUser;
        System.runAs(runAsUser)
        {
            System.debug('!!!!!! user');
            PRJ_QuotedGateReportLink vfCtrl1 = new PRJ_QuotedGateReportLink();
            vfCtrl1.assignValuesToReport();
        }
    }
}