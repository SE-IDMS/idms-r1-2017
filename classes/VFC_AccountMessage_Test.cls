@isTest
private class VFC_AccountMessage_Test {
	
	@isTest static void testAccountMessagePositive() {
		// Implement test code
		Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234',ToBeDeleted__c=true,ReasonForDeletion__c='Other');      
        insert acc1;
        ApexPages.StandardController controller=new ApexPages.StandardController(acc1);
        new VFC_AccountMessage(controller);
	}
	@isTest static void testAccountMessageNegative() {
		// Implement test code
		Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234',ToBeDeleted__c=true,ReasonForDeletion__c='Other');      
        insert acc1;
        //Utils_TestMethods.createLeadWithAcc(Id accId, Id countryId, Decimal priority)
		Lead myLead=Utils_TestMethods.createLeadWithAcc(acc1.id,ct.id,1);
		insert myLead;
		acc1=[Select id,ToBeDeleted__c, (select id from Leads__r),(select id from Opportunities), (select id from Cases) From Account Where id=:acc1.id];
        ApexPages.StandardController controller=new ApexPages.StandardController(acc1);
        new VFC_AccountMessage(controller);
	}
}