@isTest

private class VFC_CRReassignFourceManualSel_Test {
    
    public static testmethod void Test_ComplaintRqReassign1 () {
         Test.startTest();    
                CS_Stakeholders__c csStakeholder1=new CS_Stakeholders__c(Name='ComplaintRequestTest1', ObjectName__c='ComplaintRequest__c',
                                     Role__c='CS&Q Manager', EmailTemplate__c='ManagerTemplate', EscalationDays__c=1);

       CS_Stakeholders__c csStakeholder2=new CS_Stakeholders__c(Name='ComplaintRequestTest2', ObjectName__c='ComplaintRequest__c',
                                     Role__c='Complaint/Request Owner', EmailTemplate__c='ManagerTemplate', EscalationDays__c=1);

       CS_Stakeholders__c csStakeholder3=new CS_Stakeholders__c(Name='ComplaintRequestTest3', ObjectName__c='ComplaintRequest__c',
                                     Role__c='CS&Q Manager', EmailTemplate__c='ManagerTemplate', EscalationDays__c=1);
       List<CS_Stakeholders__c> shList=new List<CS_Stakeholders__c>{ csStakeholder1,
                                                                     csStakeholder2,
                                                                     csStakeholder3 };
       insert shList;
  
       Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;
        
        List<BusinessRiskEscalationEntity__c> bree=new List<BusinessRiskEscalationEntity__c>();
        
        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST - LCR Sub-Entity - LCR Location',
                                            Entity__c='LCR TEST',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c =''
                                            );
         
        //bree.add(lCROrganzObj);
        
        insert lCROrganzObj;
      
        BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST - LCR Sub-Entity - LCR Location1',
                                            Entity__c='LCR TEST',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location1',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c =''
                                            );
         
        //bree.add(lCROrganzObj);
        
        insert lCROrganzObj1;
        BusinessRiskEscalationEntity__c lCROrganzObj2 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST - LCR Sub-Entity - LCR Location2',
                                            Entity__c='LCR TEST',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location2',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='ram.chilukuri@apcc.com'
                                            );
         
        //bree.add(lCROrganzObj);
        
        insert lCROrganzObj2;
        OrganizationStakeholdersForCR__c OSHCR = new OrganizationStakeholdersForCR__c();
        OSHCR.AdditionalInformation__c='Test';
        OSHCR.ContactEmail__c='';
        OSHCR.Department__c='aaa';
        OSHCR.Owner__c=UserInfo.getUserId();
        OSHCR.Role__c='Complaint/Request Owner';
        OSHCR.TECH_UniqueRole__c='test';
        OSHCR.Organization__c=lCROrganzObj.Id;
        
        insert OSHCR;
          
          system.debug('@@@OSHCR'+OSHCR);

   
        List<EntityStakeholder__c> eshList=new List<EntityStakeholder__c>();
         EntityStakeholder__c EntityStack = new EntityStakeholder__c();
                //EntityStack.Role__c = 'Complaint/Request Owner';
                EntityStack.Role__c = Label.CLDEC13LCR05;
                EntityStack.User__c =UserInfo.getUserId();
                EntityStack.BusinessRiskEscalationEntity__c =lCROrganzObj1.id;
                
                
                eshList.add(EntityStack);
       
                insert eshList;
                
        ComplaintRequest__c  CRequestReassign = new ComplaintRequest__c(
                Status__c='Open',
                //Case__c  =CaseObj.id,
               //Category__c ='1 - Pre-Sales Support',
                reason__c='Marketing Activity Response',
                Category__c ='10 - Troubleshooting',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj.id
                );
                
        Insert CRequestReassign;        
      
     
                PageReference pageRef = Page.VFP_CRReassignFourceManualSel;
                pageRef.getParameters().put('CRId',CRequestReassign.Id);
                pageRef.getParameters().put('TargetOrgId',lCROrganzObj.Id);
                pageRef.getParameters().put('SelectKeepInvd','true');
               
                Test.setCurrentPage(pageRef); 
                
                system.debug('HANAN-------------%%%%'+bree);
                ApexPages.StandardController ForceManualSel = new ApexPages.StandardController(CRequestReassign);   
                VFC_CRReassignFourceManualSel FMSpage = new VFC_CRReassignFourceManualSel(ForceManualSel);
                              FMSpage.accountOrgId=lCROrganzObj.name;
                FMSpage.searchAcctOrganization();
                
                FMSpage.save(); 
                FMSpage.setFlag(); 
                FMSpage.accountOrgId=lCROrganzObj1.name;
                FMSpage.searchAcctOrganization();
                FMSpage.forecManualActOrg[0].selected=true;
                FMSpage.save(); 
                FMSpage.setFlag(); 
                FMSpage.accountOrgId=lCROrganzObj2.name;
                FMSpage.searchAcctOrganization();
                
                FMSpage.save(); 
                FMSpage.setFlag(); 
            pageRef.getParameters().put('TargetOrgId',null);
                pageRef.getParameters().put('SelectKeepInvd','true');
               
                Test.setCurrentPage(pageRef); 
                
                system.debug('HANAN-------------%%%%'+bree);
                ApexPages.StandardController ForceManualSel1 = new ApexPages.StandardController(CRequestReassign);   
                VFC_CRReassignFourceManualSel FMSpage1 = new VFC_CRReassignFourceManualSel(ForceManualSel1);
                              FMSpage1.isBusinessRiskPrasent=True;
                            
                FMSpage1.searchAcctOrganization();
                FMSpage.forecManualActOrg[0].selected=true;
                FMSpage.selectedValue=lCROrganzObj.id;
                FMSpage1.save(); 
                FMSpage1.setFlag(); 
                FMSpage1.isBusinessRiskPrasent=false;
                FMSpage1.isTarOrgPresent=true;
                FMSpage1.validateErr=true;
                FMSpage1.searchAcctOrganization();
                
       Test.stopTest();                   
        
     }
}