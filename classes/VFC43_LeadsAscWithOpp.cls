/*
    Author          : Accenture Team
    Date Created    : 18/07/2011 
    Description     : Controller extensions for VFP43_LeadsAscWithOpp.This class Access the
                      corresponding Opportunity record via the standard Opportunity controller method
    18/Apr/2012    Srinivas Nallapati    updated the SOQL   added "order by Status asc"
    06/Aug/2012    Srinivas Nallapati    Updated for modifying the columns DEF-0661     
    30/Oct/2012    Srinivas Nallapati    Updated too show Tech_Campaignname, This filed shows Campaign name based on the Lead type Marcomm / Non-Marcomm             
*/

public with sharing class VFC43_LeadsAscWithOpp extends VFC_ControllerBase
{
    //Iniatialize variables
    public Opportunity opp{get;private set;}
    private List<Lead> leads{get;set;}
    public List<DataTemplate__c> dataTemplates {get;set;}
    public List<SelectOption> columns{get;set;}
    //Constructor
    public VFC43_LeadsAscWithOpp(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC43_LeadsAscWithOpp******');  
        opp= (Opportunity)controller.getRecord();
        leads = new List<Lead>();
        dataTemplates = new List<DataTemplate__c>();
        columns = new List<SelectOption>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC43_LeadsAscWithOpp******'); 
    }
    
    /* This method is called on the load of VFP43_LeadsAscWithOpp page. This method 
       queries the Converted & unconverted leads related to the corresponding opportunity 
       and displays the leads lists or a message if no Leads associated.
    */  
    
    public pagereference displayLeads()
    {
        System.Debug('****** Displaying of Leads Begins ******');  
        if(opp!=null)
        {
            leads = [select TECH_CampaignName__c, Timeframe__c,SubStatus__c, Status, SolutionInterest__c, PromoTitle__c, Priority__c, Name, CreatedDate, ClosedDate__c, Category__c,Owner.FirstName,Owner.LastName, OwnerID, ResponseDate__c, CampaignName__c,keycode__c, ConvertedDate, MarcomType__c, Media__c, Tech_date__c  from Lead where Opportunity__c =:opp.Id AND RecordTypeId !=: System.Label.CLJUL14MKT01 order by Status asc];      
        }
        
        columns.add(new SelectOption('Field1__c',Schema.sObjectType.Lead.Fields.Tech_Date__c.label)); // First Column
        columns.add(new SelectOption('Field2__c','Lead Name')); 
        columns.add(new SelectOption('Field3__c',Schema.sObjectType.Lead.Fields.SubStatus__c.label));
        //Modifiled for BR-1911
        columns.add(new SelectOption('Field4__c',Schema.sObjectType.Lead.Fields.TECH_CampaignName__c.label));
        
        columns.add(new SelectOption('Field5__c',Schema.sObjectType.Lead.Fields.MarcomType__c.label));
        columns.add(new SelectOption('Field6__c',Schema.sObjectType.Lead.Fields.Media__c.label));
        columns.add(new SelectOption('Field7__c',Schema.sObjectType.Lead.Fields.PromoTitle__c.label));
        columns.add(new SelectOption('Field8__c',Schema.sObjectType.Lead.Fields.keycode__c.label)); // Last Column
        
        for(Lead lead: leads)
        {
            DataTemplate__c dt = new DataTemplate__c();
            
            dt.field1__c = String.valueOf(lead.Tech_Date__c.format());
            
            if(lead.ConvertedDate!=null)
                dt.field2__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/apex/VFP44_LeadDetail?id='+lead.Id+'" target="_blank">'+lead.Name+'</a>';
            else
                dt.field2__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+lead.Id+'" target="_blank">'+lead.Name+'</a>';
            
            dt.field3__c = lead.SubStatus__c;
            dt.field4__c = lead.TECH_CampaignName__c; //Modified for BR-1911
            dt.field5__c = lead.MarcomType__c;
            
            dt.field6__c = lead.Media__c;
            dt.field7__c = lead.PromoTitle__c;
            dt.field8__c = lead.keycode__c;
            dataTemplates.add(dt);
        }
        System.Debug('****** Displaying of Leads Begins ******'); 
        return null;
    }
}