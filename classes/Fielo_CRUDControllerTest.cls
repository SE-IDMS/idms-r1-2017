@istest
public with sharing class Fielo_CRUDControllerTest {
    public static testMethod void unitTest() {
      
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345'
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'test'
        );
        insert menu;

        Fielo_CRUDObjectSettings__c testCrud = new Fielo_CRUDObjectSettings__c(
            F_ObjectAPIName__c = 'Account'
        );
        insert testCrud;
    
        FieloEE__Component__c component = new FieloEE__Component__c(
            FieloEE__Menu__c = Menu.Id,
            F_CRUDObjectSettings__c = testCrud.Id
        );
        insert component;
        
        Fielo_PageLayout__c page = new Fielo_PageLayout__c(
            F_ButtonAccess__c = 'Edit; Delete; Back',
            F_DeveloperName__c = 'Test',
            F_CRUD_ObjectSettings__c = testCrud.id
        );
        insert page;
        
        Fielo_PageSection__c sect = new Fielo_PageSection__c(
            Name = 'Test section',
            F_PageLayout__c = page.Id 
        );
        insert sect;
        
        Fielo_PageComponent__c comp = new Fielo_PageComponent__c(
            F_PageSection__c = sect.Id,
            RecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Fielo_Field'],
            F_Name__c = 'Name'
        );    
        insert comp;
        
        PageReference myPage = new Pagereference('www.test.com');
        myPage.getParameters().put('Mode','e');
        myPage.getParameters().put('MenuId', menu.Id);
        myPage.getParameters().put('fComponentId',component.Id);
        myPage.getParameters().put('RecordId', acc.Id);
        myPage.getParameters().put('SObjectType','Account');
        myPage.getParameters().put('fieloRetURL','algunURL');
        Test.setCurrentPage(myPage);  
        Fielo_CRUDController crudController = new Fielo_CRUDController();
        
        myPage.getParameters().put('Mode','d');
        Test.setCurrentPage(myPage); 
        crudController = new Fielo_CRUDController();
        
        myPage.getParameters().put('Mode','r');
        Test.setCurrentPage(myPage);
        crudController = new Fielo_CRUDController();
        crudController.doEdit();
        crudController.doSave();
        crudController.doBack();
        crudController.doDelete(); 
        
        myPage.getParameters().put('Mode','c');
        Test.setCurrentPage(myPage);
        crudController = new Fielo_CRUDController();
        crudController.doSave();
    }
}