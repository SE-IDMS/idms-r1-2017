global class AggregateResultMergeAccIterator implements Iterator<AggregateResult> {
    AggregateResult [] results {get;set;}
    // tracks which result item is returned
    Integer index {get; set;}
          
    global AggregateResultMergeAccIterator() {
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        index = 0;
        List<String> listPartnerAccountId = new List<String>();
        for(AggregateResult ar : [SELECT PRMUIMSSEAccountId__c, COUNT(Name) FROM Account
            WHERE RecordtypeId =: partnerAccRTId AND PRMUIMSID__c <> null AND isPartner = true
            GROUP BY PRMUIMSSEAccountId__c HAVING COUNT(Name) = 1 LIMIT 200]){
                listPartnerAccountId.add(String.valueOf(ar.get('PRMUIMSSEAccountId__c')));
        }
        results = [SELECT SEAccountID__c, COUNT(Name) FROM Account WHERE RecordtypeId =: businessAccRTId AND PRMUIMSID__c =: null
                   AND SEAccountID__c IN: listPartnerAccountId 
                   GROUP BY SEAccountID__c HAVING COUNT(Name) >= 1 LIMIT 200];
    }
    
    global boolean hasNext(){
        return results != null && !results.isEmpty() && index < results.size();
    }   
    
    global AggregateResult next(){       
        return results[index++];           
    }      
}