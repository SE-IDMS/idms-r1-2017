/***************************************
* Developer: Fielo Team                *
****************************************/
public with sharing class FieloPRM_VFC_InvoiceStatusUpdateCont{
    
    private FieloPRM_Invoice__c inv {get;set;}
    public String confirmation {get;set;}
    public String result {get;set;}
    
    public FieloPRM_VFC_InvoiceStatusUpdateCont(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()){
            controller.addFields(new List<String>{'F_PRM_Status__c','F_PRM_RetailerAccount__c'});
        }
        inv = (FieloPRM_Invoice__c)controller.getRecord();
    }
    
    public void doUpdateStatus(){
        confirmation = null;
        result = null;
        if(inv.F_PRM_Status__c == 'Pending'){
            List<FieloPRM_InvoiceDetail__c> listInvDetails = [SELECT Id FROM FieloPRM_InvoiceDetail__c WHERE F_PRM_Invoice__c =: inv.Id];
            if(inv.F_PRM_RetailerAccount__c == null){
                result = 'Please complete a Retailer Account before approving the invoice.';
                confirmation = 'INFO';                
            }else if(!listInvDetails.isEmpty()){
                inv.F_PRM_Status__c = 'Approved';
                try{
                    update inv;
                    confirmation = 'OK';
                    result = 'Invoice was processed succesfully.';
                }catch (DMLException e){
                    result = 'An error ocurred while processing the invoice. Error details: ' + e.getMessage();
                    confirmation = 'ERROR';
                }
            }else{
                result = 'Please fill in the invoice details before proceeding to approve.';
                confirmation = 'INFO';              
            }
        }else if(inv.F_PRM_Status__c == 'Approved'){
                result = 'Invoice was already processed.';
                confirmation = 'INFO';            
        }else if(inv.F_PRM_Status__c == 'Rejected'){
                result = 'Invoice cannot be processed because it was rejected.';
                confirmation = 'INFO';          
        }
             
    }

    public PageReference doGoToInv(){
        return new PageReference('/'+inv.Id);
    }

}