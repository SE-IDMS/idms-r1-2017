public class VFC_NewCountryChannel {
    public CountryChannels__c cChannelRecord {get; set;}
    public List<SelectOption> channelValues {get; set;}
    public List<SelectOption> subChannelValues {get; set;}
    public List<SelectOption> country {get; set;}
    public Map<Id,List<SelectOption>> chanSubChannelValues ;
    public String ErrMsg {get;set;}
    public List<SelectOption> CurrencyValues {get; set;}
    public PRMAccountOwnerAssignment__c PRMAccountOwAssRule{get; set;}
    public Map<Id,String> Channelcode;
    public Boolean isEditMode {get;set;}
    public string countrySelected {get;set;}
    public String clusterCountry {get;set;}
    public String CountryCode{get;set;}
    public String RegFormMode{get;set;}
    
    public VFC_NewCountryChannel(ApexPages.StandardController stdcontroller){
        ErrMsg = '';
        isEditMode = false;
        cChannelRecord = New CountryChannels__c();
        chanSubChannelValues = New Map<Id,List<SelectOption>>();
        channelValues = New List<SelectOption>();
        country =  New List<SelectOption>();
        subChannelValues =  New List<SelectOption>();
        CurrencyValues = New List<SelectOption>();
        Channelcode = New Map<Id,String>();
        PRMAccountOwAssRule = new PRMAccountOwnerAssignment__c();
        
        String countryChannelId = ApexPages.currentPage().getParameters().get('id');
        System.debug('*****'+countryChannelId);
        countrySelected = ApexPages.currentPage().getParameters().get('country');
        System.debug('*****'+countrySelected);
        clusterCountry = ApexPages.currentPage().getParameters().get('prmCountry');
        System.debug('*****'+clusterCountry);
        String DefaultLandingPage = ApexPages.currentPage().getParameters().get('LandingPage');
        System.debug('*****'+DefaultLandingPage);
        CountryCode = ApexPages.currentPage().getParameters().get('CountryCode');
        System.debug('*****'+RegFormMode);
        RegFormMode = ApexPages.currentPage().getParameters().get('RegMode');
        
        // This logic is, When the user clicks on edit button
        
        if(String.isNotBlank(countryChannelId) && !test.isRunningTest()){
            String[] countryChannelFields = new String[] {'RegistrationFormMode__c','Active__c','F_PRM_Menu__c','AllowPrimaryToManage__c','Channel__c','SubChannel__c','Country__c',
                                                          'PRMCountry__c','PRMCountry__r.CountryCode__c','CompleteUserRegistrationonFirstLogin__c','PredictiveCompanyMatchEnabled__c',
                                                          'F_PRM_ReviewLocatorListing__c','AccountOwner__c','ChannelCode__c','SubChannelCode__c','DefaultLandingPage__c'};
            List<String> conChanFields = new List<String>(countryChannelFields);
            stdController.addFields(conChanFields);
            cChannelRecord = (CountryChannels__c)stdController.getRecord();
            if(String.IsBlank(cChannelRecord.RegistrationFormMode__c))
               cChannelRecord.RegistrationFormMode__c = CS_PRM_ApexJobSettings__c.getvalues('Registration Form Mode').QueryFields__c; // Simplified
            //System.debug('*****'+PRMAccountOwAssRule);
            List<PRMAccountOwnerAssignment__c> PRMAccountRule = [SELECT Id, Classification__c, SubClassification__c, Country__c, AccountOwner__c FROM PRMAccountOwnerAssignment__c 
                                      WHERE CountryChannel__c =:cChannelRecord.Id];
            if(CountryCode != 'WW'){
                if(PRMAccountRule.size() > 0 ) PRMAccountOwAssRule = PRMAccountRule[0];
                else PRMAccountOwAssRule.AccountOwner__c = Userinfo.getUserId();
            }
            //System.debug('*****'+PRMAccountOwAssRule);
        }
        // This logic is, When the user tries to create a Country Channel from Country/Cluster
        else if(String.isNotBlank(clusterCountry) && (String.isNotBlank(countrySelected) || (string.isBlank(countrySelected) && CountryCode == 'WW'))){
            System.debug('>>>>><<<<'+isEditMode);
            cChannelRecord.PRMCountry__c = clusterCountry;
            cChannelRecord.DefaultLandingPage__c = DefaultLandingPage;
            cChannelRecord.RegistrationFormMode__c = RegFormMode;
            
            System.debug('****Inside If');
            System.debug('>>>>>'+isEditMode);
            isEditMode = true; 
            System.debug('>>>>>'+isEditMode);
            System.debug('>>>>>'+countrySelected);
            if(CountryCode != 'WW')
                PRMAccountOwAssRule.AccountOwner__c = Userinfo.getUserId();
        }
        
        
        List<ClassificationLevelCatalog__c> cLCatalogList =  [SELECT Id, Name, ClassificationLevelName__c, ParentClassificationLevel__c, 
                                                             (SELECT Id, Name, ClassificationLevelName__c,ParentClassificationLevel__c FROM 
                                                              Classification_Levels__r Where Active__C = True ORDER BY ClassificationLevelName__c) 
                                                              FROM ClassificationLevelCatalog__c WHERE Active__c = true AND ParentClassificationLevel__c = '' 
                                                              ORDER BY ClassificationLevelName__c];
        List<Country__c> lCountries = [SELECT Id, Name, CountryCode__c FROM Country__c ORDER BY Name];
        channelValues.add(new SelectOption('','--None--'));
        subChannelValues.add(new SelectOption('','--None--'));
         for(ClassificationLevelCatalog__c cLCatalog: cLCatalogList ){
            channelValues.add(new SelectOption(cLCatalog.Id, cLCatalog.ClassificationLevelName__c));
            Channelcode.put(cLCatalog.Id, cLCatalog.Name);
            for(ClassificationLevelCatalog__c cChildCalog: cLCatalog.Classification_Levels__r){
                Channelcode.put(cChildCalog.Id, cChildCalog.Name);
                if(chanSubChannelValues.containsKey(cLCatalog.id))
                chanSubChannelValues.get(cLCatalog.id).add(new SelectOption(cChildCalog.Id, cChildCalog.ClassificationLevelName__c));
                else{
                    List<SelectOption> subChannels = New List<SelectOption>();
                    subChannels.add(new SelectOption(cChildCalog.Id, cChildCalog.ClassificationLevelName__c));
                    chanSubChannelValues.put(cLCatalog.id, subChannels);
                }
            }
            System.debug('We are getting values' +chanSubChannelValues);
        }
        if(!isEditMode) getsubChannels();
        country.add(new SelectOption('','--None--')); 
        for(Country__c con: lCountries){
            country.add(new SelectOption(con.Id,con.Name));
        }
        CurrencyValues.add(new SelectOption('','--None--'));
        Schema.DescribeFieldResult dfr = CountryChannels__c.CurrencyIsoCode.getDescribe();
        for(Schema.PicklistEntry pLEntry: dfr.getPicklistValues()){
            if(pLEntry.isActive())
            CurrencyValues.add(new SelectOption(pLEntry.getValue(), pLEntry.getLabel()));
        }
        
    }
    
    public Void getsubChannels(){
        subChannelValues.clear();
        subChannelValues.add(new SelectOption('','--None--'));
        if(chanSubChannelValues.containsKey(cChannelRecord.Channel__c)){
            subChannelValues.addAll(chanSubChannelValues.get(cChannelRecord.Channel__c));
        }
        
    }
    public pagereference saveChannel(){
        pagereference pg = null;
        ApexPages.getMessages().clear();
        if(isEditMode){
            List<Country__c> cntry = [SELECT Id, Name FROM Country__c WHERE Name = :countrySelected ];
            if(!cntry.isEmpty()){
                PRMAccountOwAssRule.Country__c = cntry[0].Id;
                cChannelRecord.Country__c = cntry[0].Id;
            }
        }
        
        PRMAccountOwAssRule.Name = Channelcode.containsKey(cChannelRecord.Channel__c) ? Channelcode.get(cChannelRecord.Channel__c) + ' - Account Owners' :null;
        PRMAccountOwAssRule.Classification__c = Channelcode.containsKey(cChannelRecord.Channel__c) ? Channelcode.get(cChannelRecord.Channel__c) :null;
        PRMAccountOwAssRule.SubClassification__c = Channelcode.containsKey(cChannelRecord.SubChannel__c) ? Channelcode.get(cChannelRecord.SubChannel__c) :null;
        
        Savepoint sp = Database.setSavepoint();
        try{
            System.debug('>>>>'+CountryCode+'>>>>'+cChannelRecord.Country__c);
            if(cChannelRecord.Channel__c != Null && cChannelRecord.subChannel__c != Null && (((CountryCode == 'WW' || cChannelRecord.PRMCountry__r.CountryCode__c == 'WW') && string.isBlank(cChannelRecord.Country__c))|| cChannelRecord.Country__c != Null) && cChannelRecord.CurrencyIsoCode != Null){
                //cChannelRecord.CompleteUserRegistrationonFirstLogin__c = True;
                System.debug('***Is it entering this' +cChannelRecord.Id);
                Database.UpsertResult sr = Database.Upsert(cChannelRecord, false);
                List<CountryChannelUserRegistration__c> lstUserRegFields = new List<CountryChannelUserRegistration__c>([SELECT Id, UserRegFieldName__c, DefaultValue__c, CountryChannels__c FROM CountryChannelUserRegistration__c WHERE CountryChannels__c = :cChannelRecord.Id AND UserRegFieldName__c = 'companyCurrency']);
                if(!lstUserRegFields.isEmpty() && lstUserRegFields.size() > 0){
                    for(CountryChannelUserRegistration__c cCReg : lstUserRegFields){
                        cCreg.DefaultValue__c = cChannelRecord.CurrencyISOCode;
                    }
                    update lstUserRegFields;
                }
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, err.getMessage()));    
                    }
                }
                else if(PRMAccountOwAssRule.AccountOwner__c != Null){
                    PRMAccountOwAssRule.CountryChannel__c = sr.getId();
                    Database.UpsertResult sRes = Database.Upsert(PRMAccountOwAssRule, false);
                    if(!sRes.isSuccess()){
                        for(Database.Error err : sRes.getErrors()){
                            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, err.getMessage()));    
                        }
                    }
                }
            }
            else{
                ErrMsg = System.label.CLAPR15PRM183;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, ErrMsg));
            }
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage())); 
            System.debug(e.getMessage());
            Database.rollback(sp);     
        }
        if(isEditMode && String.isNotBlank(cChannelRecord.Id)){
            CountryChannels__c cChn = [Select Id,Name FROM CountryChannels__c WHERE ID = :cChannelRecord.Id];
            pg = new pagereference('/apex/VFP_CountryChannelUserRegistration?id='+cChannelRecord.Id+'&Name='+cChn.Name);
        }
        else if(!isEditMode && ApexPages.getMessages().size() == 0){
            pg = new pagereference('/'+cChannelRecord.Id);
        }
        return pg;
    }
}