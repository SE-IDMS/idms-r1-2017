@isTest(seeAllData=true)
 
public class VFC_RMAConnectorCheckWarranty_TEST{
      boolean boffice=true;
      boolean flag=true;
  static testMethod void myUnitTest() 
   {

    system.debug('Method1');
        
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    User TestUser1 = Utils_TestMethods.createStandardUser('RRuser1');
    Insert TestUser1 ; 
      
    BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg;
        
    // Country__c CountryObj = Utils_TestMethods.createCountry();
    Country__c countryObj=new Country__c();
    CountryObj.Name='India';      
    countryObj.CountryCode__c='91';
    //countryObj.FrontOfficeOrganization__c=RCorg.Id;
    Insert CountryObj;
      
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    accountObj.SEAccountID__c='TEST';
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
    
ResolutionOptionAndCapabilities__c rs=new ResolutionOptionAndCapabilities__c();
rs.Type__c='Repair';
rs.ProductCondition__c='Defective';
rs.CustomerResolution__c='REP';
rs.ProductDestinationCapability__c='Repair defective product';
insert rs;

    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';   
    Insert CaseObj;
    
       
    RMA__c RR = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                AccountName__c=accountObj.id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP',
                AccountStreet__c = 'Test street',
                OriginalSalesOrder__c='TestSalesOrder',
                CountryofBackOfficeSystem__c='India',
                BackOfficeSystem__c='SAP ECC',
                TECH_RequestType__c='Repair'
                //SynchronizationMessage__c=null
                );
    Insert RR;
    
     system.currentpagereference().getParameters().put('CW','no');
     
     ApexPages.Standardcontroller SC = New ApexPages.StandardController(RR);
     VFC_RMAConnectorCheckWarranty RRcheckWarrnty = new VFC_RMAConnectorCheckWarranty(SC);
       
     CountryObj.FrontOfficeOrganization__c=RCorg.id;
     update CountryObj;
     
     RRcheckWarrnty.redirectToextRefereceExternalSystem();
        
     BackOfficesystem__c backoffs=new BackOfficesystem__c();
     backoffs.BackOfficesystem__c='SAPECC1';
     backoffs.FrontOfficeOrganization__c=RCorg.id; 
     //backoffs.Country__c=CountryObj.Id;
     backoffs.Deployed__c=true;
     insert backoffs; 
           
           BackOfficesystem__c backoffs2=new BackOfficesystem__c();
     backoffs2.BackOfficesystem__c='SAPECC3';
     backoffs2.FrontOfficeOrganization__c=RCorg.id; 
     //backoffs.Country__c=CountryObj.Id;
     backoffs2.Deployed__c=true;
     insert backoffs2; 
           
     boolean isSelect=true;
     RRcheckWarrnty.redirectToextRefereceExternalSystem();
     RRcheckWarrnty.doProcess();
     RRcheckWarrnty.doCancel(); 
     
     
   RMA_Product__c RI = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='xbtg5230',
                        ProductFamily__c='HMI SCHNEIDER BRAND',
                        RMA__c=RR.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        WarrantyStatus__c = '',
                        TECH_RequestType__c ='Repair',
                        //BillingMethod__c='Contract',
                        ApproverLevel1__c= TestUser1.id,
                        TECH_GMRCode__c='02BF6D'
                        );
    Insert RI;
    
    RRcheckWarrnty.redirectToextRefereceExternalSystem();    
  
    //--------
    
CountryObj.Name='China';
CountryObj.FrontOfficeOrganization__c=null;
update CountryObj;

 backoffs.FrontOfficeOrganization__c=null; 
 backoffs.Country__c=CountryObj.Id;
 update backoffs; 
 backoffs2.FrontOfficeOrganization__c=null; 
 backoffs2.Country__c=CountryObj.Id;
 update backoffs2; 
 ApexPages.Standardcontroller SC1 = New ApexPages.StandardController(RR);
     VFC_RMAConnectorCheckWarranty RRcheckWarrnty1 = new VFC_RMAConnectorCheckWarranty(SC1);
     RRcheckWarrnty1.redirectToextRefereceExternalSystem();  
       
   }

 }