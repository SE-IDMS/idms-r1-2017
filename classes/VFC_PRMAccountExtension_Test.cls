@isTest
public class VFC_PRMAccountExtension_Test {

    @testSetup static void testSetupMethodPRMAccountExtension() {
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince; 
        CS_PRM_NoCompanyHoldingAccount__c csDummyAcc = new CS_PRM_NoCompanyHoldingAccount__c();
            csDummyAcc.Name = 'DummyAccountInfo';
            csDummyAcc.AccountId__c = '00117000008vEH0AAM';
            csDummyAcc.ContactCount__c = 48;
        INSERT csDummyAcc;
        CS_ContactLanguageToUserLanguage__c csContactLang = new CS_ContactLanguageToUserLanguage__c(); 
            csContactLang.Name = 'EN';
            csContactLang.UserLanguageKey__c = 'en_US';
        INSERT csContactLang;
        
        PRMCountry__c countryCluster = new PRMCountry__c();
            countryCluster.Country__c = testCountry.Id;
            countryCluster.CountryPortalEnabled__c = True; 
            countryCluster.PLDatapool__c = 'IN_en2';
            countryCluster.SupportedLanguage1__c = 'en_US';
            countryCluster.SupportedLanguage2__c = 'hi';
            countryCluster.SupportedLanguage3__c = 'en_IN';
        INSERT countryCluster;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
            insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI3', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
            insert cLChild;
        CountryChannels__c cChannels = new CountryChannels__c();
            cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
            cChannels.Country__c = testCountry.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true; 
            cChannels.PRMCountry__c = countryCluster.Id;
        insert cChannels;
        
    
    }


    public static testMethod void AccountExtensionFirst(){
        
         Country__c testCountry = [SELECT Id, CountryCode__c FROM Country__c WHERE CountryCode__c =:'IN'];
        StateProvince__c testStateProvince = [SELECT Id, StateProvinceCode__c, CountryCode__c, Country__c FROM StateProvince__c WHERE StateProvinceCode__c=:'10'];
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
        //INSERT acc;
        
        Contact con = new Contact();
            
            con.PRMFirstName__c = 'FirstName TestClass';
            con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.PRMEmail__c          = 'testclass1@test.com';    
            //con.PRMJobFunc__c
            //con.PRMJobTitle__c
            //con.AccountId            = ;
        //INSERT con;
        
        ApexPages.StandardController stdAccCont = new ApexPages.standardController(acc);
        VFC_PRMAccountExtension prmAccountExtn = new VFC_PRMAccountExtension(stdAccCont);
        
        prmAccountExtn.cont = con;
        prmAccountExtn.phoneType = 'Work';
        prmAccountExtn.phoneNumber = '9876789988';
        prmAccountExtn.doNotHaveCompany = True;
        prmAccountExtn.SelectedApplication = 'POMP';
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            prmAccountExtn.cChannels();
            AP_PRMUtils.UserEmailSearchStatus tResult  = VFC_PRMUserRegistrationController.verifyEmail(con.PRMEmail__c);
            pageReference pgSaveRecord = prmAccountExtn.saveRecord();
            /*if(pgSaveRecord != Null){
                pageReference pgSaveRecord2 = prmAccountExtn.methodUims();
                System.debug('**methodUims**'+pgSaveRecord2);
            }*/  
        Test.stopTest();    
    }
    
    public static testMethod void AccountExtensionSecond(){
        
         Country__c testCountry = [SELECT Id, CountryCode__c FROM Country__c WHERE CountryCode__c =:'IN'];
        StateProvince__c testStateProvince = [SELECT Id, StateProvinceCode__c, CountryCode__c, Country__c FROM StateProvince__c WHERE StateProvinceCode__c=:'10'];
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
        //INSERT acc;
        
        Contact con = new Contact();
            
            con.PRMFirstName__c = 'FirstName TestClass';
            con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.PRMEmail__c          = 'testclass1@test.com'; 
            con.PRMLanguage__c = 'EN';      
            //con.PRMJobFunc__c
            //con.PRMJobTitle__c
            //con.AccountId            = ;

        ApexPages.StandardController stdAccCont = new ApexPages.standardController(acc);
        VFC_PRMAccountExtension prmAccountExtn = new VFC_PRMAccountExtension(stdAccCont);
        
        
        prmAccountExtn.cont = con;
        prmAccountExtn.phoneType = 'Work';
        prmAccountExtn.phoneNumber = '9876789988';
        prmAccountExtn.contactName = 'ContactName';
        prmAccountExtn.saveRecord = new PageReference('/003');
        UserRegResult res = new UserRegResult();
        res.Success = true;
        res.UserFederationId = '798976978775446jg77';
        prmAccountExtn.result = res;
        prmAccountExtn.contLanguage = 'en_US';
        prmAccountExtn.SelectedApplication = 'PRTNRPRTL';
        prmAccountExtn.channelSelected = 'FI';
        Test.startTest();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            prmAccountExtn.getContactLanguages();
            prmAccountExtn.doNotHaveCompany = true;
            prmAccountExtn.cancelSkipCompany();
            prmAccountExtn.saveRecord();    
            prmAccountExtn.saveSkipCompany();
            prmAccountExtn.cancel();
            prmAccountExtn.getresult();
            prmAccountExtn.setresult(res);
            prmAccountExtn.showMessageToUser2();
            prmAccountExtn.cChannels();
            prmAccountExtn.cSubChannels();
            prmAccountExtn.checkSelectedApp();
            /*if(pgSaveRecord4 != Null){
                pageReference pgSaveRecord5 = prmAccountExtn.methodUims();
                System.debug('**methodUims**'+pgSaveRecord3);
            }*/  
        Test.stopTest();    
    }
    
    public static testMethod void AccountExtensionThird(){
        
         Country__c testCountry = [SELECT Id, CountryCode__c FROM Country__c WHERE CountryCode__c =:'IN'];
        StateProvince__c testStateProvince = [SELECT Id, StateProvinceCode__c, CountryCode__c, Country__c FROM StateProvince__c WHERE StateProvinceCode__c=:'10'];
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
            acc.PRMBusinessType__c = 'FI';
            acc.ClassLevel1__c     = 'FI';
            acc.PRMAreaOfFocus__c  = 'FI3';
            acc.ClassLevel2__c     = 'FI3';
        INSERT acc;
        
        Contact con = new Contact();
            
            con.FirstName = con.PRMFirstName__c = 'FirstName TestClass';
            con.LastName = con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.Email = con.PRMEmail__c          = 'testclass1@test.com'; 
            con.PRMLanguage__c = 'EN'; 
            con.AccountId = acc.Id;     
        INSERT con;
        Contact conTwo = new Contact();
            
            conTwo.FirstName = con.PRMFirstName__c = 'FirstName TestClass2';
            conTwo.LastName = con.PRMLastName__c  = 'LastName TestClass2';
            conTwo.PRMCustomerClassificationLevel1__c  = 'FI';
            conTwo.PRMCustomerClassificationLevel2__c  = 'FI3';
            conTwo.Email = con.PRMEmail__c          = 'testclass2@test.com'; 
            conTwo.PRMLanguage__c = 'EN'; 
            conTwo.AccountId = acc.Id;     
            conTwo.TECH_ExistingContactLookup__c = con.Id;
        INSERT conTwo;
        
        
        ApexPages.StandardController stdAccCont = new ApexPages.standardController(acc);
        VFC_PRMAccountExtension prmAccountExtn = new VFC_PRMAccountExtension(stdAccCont);
        
        prmAccountExtn.acct = acc;
        prmAccountExtn.cont = conTwo;
        prmAccountExtn.phoneType = 'Work';
        prmAccountExtn.phoneNumber = '9876789988';
        UserRegResult res = new UserRegResult();
        res.Success = true;
        res.UserFederationId = '798976978775446jg77';
        prmAccountExtn.result = res;
        prmAccountExtn.SelectedApplication = 'POMP';
        Test.startTest();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            prmAccountExtn.cont = conTwo;
            prmAccountExtn.runQuery();
            prmAccountExtn.userConfirmed();
            prmAccountExtn.checkSelectedApp();
        Test.stopTest();    
    }
    
    public static testMethod void AccountExtensionFourth(){
        
         Country__c testCountry = [SELECT Id, CountryCode__c FROM Country__c WHERE CountryCode__c =:'IN'];
        StateProvince__c testStateProvince = [SELECT Id, StateProvinceCode__c, CountryCode__c, Country__c FROM StateProvince__c WHERE StateProvinceCode__c=:'10'];
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            //acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
            acc.PRMBusinessType__c = 'FI';
            acc.PRMAreaOfFocus__c = 'FI3';
        INSERT acc;
        
        Contact con = new Contact();
            
            con.FirstName = con.PRMFirstName__c = 'FirstName TestClass';
            con.LastName = con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.Email = con.PRMEmail__c          = 'testclass1@test.com'; 
            con.PRMLanguage__c = 'EN'; 
            con.AccountId = acc.Id;     
        INSERT con;
        
        ApexPages.StandardController stdAccCont = new ApexPages.standardController(acc);
        VFC_PRMAccountExtension prmAccountExtn = new VFC_PRMAccountExtension(stdAccCont);
        
        prmAccountExtn.acct = acc;
        prmAccountExtn.cont = con;
        prmAccountExtn.phoneType = 'Work';
        prmAccountExtn.phoneNumber = '9876789988';
        UserRegResult res = new UserRegResult();
        res.Success = true;
        res.UserFederationId = '798976978775446jg77';
        prmAccountExtn.result = res;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            prmAccountExtn.cChannels();
        Test.stopTest();    
    }
    
    public static testMethod void AccountExtensionRunQuery(){
        
         Country__c testCountry = [SELECT Id, CountryCode__c FROM Country__c WHERE CountryCode__c =:'IN'];
        StateProvince__c testStateProvince = [SELECT Id, StateProvinceCode__c, CountryCode__c, Country__c FROM StateProvince__c WHERE StateProvinceCode__c=:'10'];
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
            acc.PRMBusinessType__c = 'FI';
            acc.ClassLevel1__c     = 'FI';
            acc.PRMAreaOfFocus__c  = 'FI3';
            acc.ClassLevel2__c     = 'FI3';
        INSERT acc;
        
        Contact con = new Contact();
            
            con.FirstName = con.PRMFirstName__c = 'FirstName TestClass';
            con.LastName = con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.Email = con.PRMEmail__c          = 'testclass1@test.com'; 
            con.PRMLanguage__c = 'EN'; 
            con.AccountId = acc.Id;   
            con.PRMWorkPhone__c  = '123456';
            con.PRMMobilePhone__c = '987654321'; 
            con.PRMLanguage__c = 'EN';
        INSERT con;
        Contact conTwo = new Contact();
            
            conTwo.FirstName = con.PRMFirstName__c = 'FirstName TestClass2';
            conTwo.LastName = con.PRMLastName__c  = 'LastName TestClass2';
            conTwo.PRMCustomerClassificationLevel1__c  = 'FI';
            conTwo.PRMCustomerClassificationLevel2__c  = 'FI3';
            conTwo.Email = con.PRMEmail__c          = 'testclass2@test.com'; 
            conTwo.PRMLanguage__c = 'EN'; 
            conTwo.AccountId = acc.Id;     
            conTwo.TECH_ExistingContactLookup__c = con.Id;
            conTwo.PRMWorkPhone__c  = '123456';
            conTwo.PRMMobilePhone__c = '987654321';
            conTwo.PRMLanguage__c = 'EN';
        INSERT conTwo;
        
        UIMSCompany__c uim = new UIMSCompany__c();
        uim.UserfederatedId__c = '798976978775446jg77';
        uim.AccountCreationStatus__c = False;
        uim.ContactCreationStatus__c = False; 
        INSERT uim;
        
        
        ApexPages.StandardController stdAccCont = new ApexPages.standardController(acc);
        VFC_PRMAccountExtension prmAccountExtn = new VFC_PRMAccountExtension(stdAccCont);
        
        prmAccountExtn.acct = acc;
        prmAccountExtn.cont = conTwo;
        prmAccountExtn.phoneType = 'Work';
        prmAccountExtn.phoneNumber = '9876789988';
        UserRegResult res = new UserRegResult();
        res.Success = true;
        res.UserFederationId = '798976978775446jg77';
        prmAccountExtn.result = res;
        prmAccountExtn.SelectedApplication = 'PRTNRPRTL';
        Test.startTest();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            prmAccountExtn.cont = conTwo;
            prmAccountExtn.runQuery();
            prmAccountExtn.userConfirmed();
        Test.stopTest();    
    }
    
}