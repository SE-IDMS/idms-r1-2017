/*
* Test class of Idms Social user registration controller (@home context) 
* */
@IsTest
private class IdmsSocialUserRegistrationControllerTest{
    
    //Data setup for test class
    @testSetup
        static void updateuser()
    {
        IdmsSocialUserRegistrationController controller = new IdmsSocialUserRegistrationController();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        
        User u = new User(Alias = 'standt', 
                          Email='testidmsuser@accenture.com', 
                          EmailEncodingKey='UTF-8',
                          FirstName ='Test First',      
                          LastName='Testing',
                          CommunityNickname='CommunityName123',      
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='testidmsuser@accenture.com' + Label.CLJUN16IDMS71,
                          UserRoleId = r.Id,
                          IDMS_Registration_Source__c = 'Facebook',
                          isActive = True,   
                          BypassVR__c = True);
        Insert u;  
        
        
        
        
        String RegSourceFb = [select Id, IDMS_Registration_Source__c from User where Id=:u.Id limit 1].IDMS_Registration_Source__c;
        System.assertEquals(Label.CLJUN16IDMS154,RegSourceFb );
        String languageUser=Label.CLJUN16IDMS09;
        
        controller.firstName = 'Test First';
        controller.lastname = 'Testing';
        controller.phomenum = '';
        controller.notvalid = false;
        controller.checked = true;
        controller.checkedEmailOpt = true;
        controller.popUPrender = true;
    }
    
    //test method 1 of user detail verification
    public static testMethod void verifyuserdetails1() {
        ApexPages.currentPage().getParameters().put('firstName', '');
        ApexPages.currentPage().getParameters().put('lastname', '');
        ApexPages.currentPage().getParameters().put('phomenum', '');
        ApexPages.currentPage().getParameters().put('ucountry', '');
        ApexPages.currentPage().getParameters().put('regCheckEmailOpt', 'false');
        IdmsSocialUserRegistrationController controller = new IdmsSocialUserRegistrationController();
    }
    
    //test method 2 of user detail verification
    static Testmethod void verifyuserdetails()
    {
        IdmsSocialUserRegistrationController controller = new IdmsSocialUserRegistrationController();
        controller.verify();
        controller.reset();
        controller.firstName = '';
        controller.lastname = '';
        controller.phomenum = '';
        controller.notvalid = true;
        controller.checked = false;
        controller.checkedEmailOpt = false;
        controller.popUPrender = false;
        
    }
    
    //test method of captcha verification by invoking google API
    static Testmethod void verifycaptcha()
    {
        IdmsSocialUserRegistrationController controller = new IdmsSocialUserRegistrationController();
        controller.verify();
        
    }
    
    //test method of auto validating country
    static Testmethod void autovalidatecountry()
    {
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        
        testUser.autoRun();
        testUser.ucountry='IN';
        testUser.displayPopup=true;
        testUser.email=null;
        testUser.save();
        
    }
    
    //test method 1 of user registration
    public static testMethod void idmsRegisterUserTest1(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        
        testUser.ucountry='IN';
        testUser.email='asdasd';
        testUser.save();
    }
    //test method 2 of user registration
    public static testMethod void idmsRegisterUserTest2(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        
        testUser.ucountry='IN';
        testUser.email='asdasd';
        testUser.save();
    }
    //test method 3 of user registration
    public static testMethod void idmsRegisterUserTest5(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.email='asdasd@accenture.com';
        testUser.phomenum='1234567890';
        testUser.checked=false;
        testUser.save();
    }
    //test method 4 of user registration
    public static testMethod void idmsRegisterUserTest6(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.email='asdasd@accenture.com';
        testUser.phomenum='1234567890';
        testUser.checked=false;
        testUser.save();
    }
    //test method 5 of user registration
    public static testMethod void idmsRegisterUserTest7(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.email='tester321@accenture.com';
        testUser.checked=true;
        testUser.enterRecaptchcorrect=false;
        testUser.phomenum='1234567890';
        
        testUser.save();
    }
    //test method 6 of user registration
    public static testMethod void idmsRegisterUserTest8(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap; 
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage); 
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationController testUser=new IdmsSocialUserRegistrationController();
        
        //make custom setting blank
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c();
        cust_obj.name ='IdmsKey';
        cust_obj.AppValue__c = 'test';
        Insert cust_obj; 
        
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.checked=true;
        testUser.email='asdas11d@accenture.com';
        testUser.enterRecaptchcorrect=true;
        testUser.phomenum='1234567890';
        testUser.save();
    }
    
}