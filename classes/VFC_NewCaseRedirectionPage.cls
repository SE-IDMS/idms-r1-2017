public class VFC_NewCaseRedirectionPage {
    public Id accId{get; set;}
    public Id conId{get; set;}
    
    Public String SubscripId{get;set;}
    Public String caseEditPageURL{get;set;}
    public Boolean blnPageError{get; set;}{blnPageError=false;}
    public Boolean flagPRM{get; set;}
    public Boolean TobeDeletedVar{get; set;}{TobeDeletedVar=false;}
    public Boolean inactiveVar{get; set;}{inactiveVar=false;}
    Public String CTISkill{get;set;}
    Public String CaseReason{get;set;}
    
    Public String LOE{get;set;}
    public Account acc{get; set;} {acc=new Account();}
    public Contact con{get; set;} {con=new Contact();}

    public VFC_NewCaseRedirectionPage(ApexPages.StandardController controller) {
        accId = ApexPages.currentPage().getParameters().get('def_account_id');
        conId = ApexPages.currentPage().getParameters().get('def_contact_id');
        System.debug('AccId is : '+accid);
        System.debug('ConId is : '+conid);
        
        // Check if contact is marked as inactive or marked for deletion and display respective error messages
        if(conId!=null){
        
            List<Contact> lstContact = [SELECT Id, ToBeDeleted__c, ReasonForDeletion__c, Inactive__c, Account.Name 
                                        FROM Contact WHERE id=:conId LIMIT 1];
                                        
            System.debug('lstContact is : '+lstContact );
            if(lstContact[0].ToBeDeleted__c==TRUE && lstContact[0].ReasonForDeletion__c!=Label.CL00521)
            {
                TobeDeletedVar=True;
                blnPageError=true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC01);
                ApexPages.addMessage(myMsg);
            }
            else if(lstContact[0].Inactive__c==TRUE)
            {
                inactiveVar=True;
                blnPageError=true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC03);
                ApexPages.addMessage(myMsg);
            }
            
         }  
         
        // Check if account is marked as inactive or marked for deletion and display respective error messages         
         if(accId!=null){
            List<Account> lstAccount = [SELECT id,ToBeDeleted__c,ReasonForDeletion__c,Inactive__c from Account Where id=:accId];
            System.debug('lstAccount is : '+lstAccount);
            if(lstAccount[0].ToBeDeleted__c==TRUE && lstAccount[0].ReasonForDeletion__c!=Label.CL00521)
            {
                TobeDeletedVar=True;
                blnPageError=true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC02);
                ApexPages.addMessage(myMsg);
            }
            else if(lstAccount[0].Inactive__c==TRUE)
            {
                inactiveVar=True;
                blnPageError=true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.CLMAR16CCC04);
                ApexPages.addMessage(myMsg);
            }
         }
         
        System.debug('AccId is : '+accid);
        System.debug('ConId is : '+conid);
    }
    
    public PageReference buildCaseCreationURL() {

        PageReference caseEditPage = new PageReference('/'+SObjectType.Case.getKeyPrefix()+'/e' );
        
        String retURL = system.currentpagereference().getParameters().get('retURL'); // Get return URL paramater
        String def_contact_id = system.currentpagereference().getParameters().get('def_contact_id'); // Get the contact ID
        String def_account_id = system.currentpagereference().getParameters().get('def_account_id'); // Get the account ID
        String def_Subscription_Id= system.currentpagereference().getParameters().get('def_Subscription_Id'); // Get the SubscriptionId
        String SubscriptionId= system.currentpagereference().getParameters().get('SubscriptionId'); // Get the CountryCODE
        string RepAccOrg=  system.currentpagereference().getParameters().get('RepAccOrg'); // Get the SupportedCountry
        string CaseCategory=system.currentpagereference().getParameters().get('CaseCategory'); // Get the SupportedCountry
        string CTISkill=system.currentpagereference().getParameters().get('CTISkill');
        string CaseReason=system.currentpagereference().getParameters().get('CaseReason');
        string SubReason=system.currentpagereference().getParameters().get('SubReason');
        string LOE=system.currentpagereference().getParameters().get('LOE');
        string CountryCode=system.currentpagereference().getParameters().get('CountryCode');
        string PocCountry=system.currentpagereference().getParameters().get('PocCountry');
        string SpocPhONE=system.currentpagereference().getParameters().get('SpocPhONE');
        string CaseTeam=system.currentpagereference().getParameters().get('CaseTeam');
        string  Subject = system.currentpagereference().getParameters().get('Subject');
       
        System.debug('### Contact Id ###: ' +  def_contact_id);
        System.debug('### Account Id ###:' + def_account_id);
        System.debug('###Subscription Id ###:' + def_Subscription_Id);
        System.debug('###Subscription Id ###:' + SubscriptionId);
        System.debug('###RepAccOrgId ###:' + RepAccOrg);
        System.debug('###CaseCategory###:' + CaseCategory);


        System.debug(caseEditPage);
        // Set the return URL to the edit page with the value from the current visual force page
        if(retURL  != null) {
            caseEditPage.getParameters().put(Label.CL00330, retURL);  
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.SUCCESS - Return URL (retURL) =' +retURL);
        }
        else {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.WARNING - Return URL is null.');
        }
        
         // Set the the contact ID for the next page with the ID get through the URL for the current page
        if(def_contact_id != null){
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.SUCCESS - Contact ID (def_contact_id) ='+def_contact_id);
            caseEditPage.getParameters().put('def_contact_id', def_contact_id);
        }
        else {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.WARNING - Contact ID is null.');
        }
        
        // Set the the contact ID for the next page with the ID get through the URL for the current page
        if(def_account_id != null) {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.SUCESS - Account ID (def_account_id) ='+def_account_id);
            caseEditPage.getParameters().put('def_account_id', def_account_id);
        }
        else {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.WARNING - Account ID is null.');
        }
        if(SpocPhONE!= null) {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.SUCESS - Account ID (def_Subscription_Id) ='+def_Subscription_Id);
            //caseEditPage.getParameters().put('CF00N1200000B1IfN',RepAccOrg);
            CaseEditPage.getParameters().put('CF00NA0000009eEhK',PocCountry);
            
        }
        If(RepAccOrg!=null){
        caseEditPage.getParameters().put('CF00N1200000B1IfN',RepAccOrg);
        }
        else {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.WARNING - Subscription ID is null.');
        }
        // Set the the Subscription value when new case is populating
        if(def_Subscription_Id!= null) {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.SUCESS - Account ID (def_Subscription_Id) ='+def_Subscription_Id);
            caseEditPage.getParameters().put('CF00N1200000B1IfS', def_Subscription_Id);
        }
        else {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.WARNING - Subscription ID is null.');
        }
        // Set the the Subscription value when new case is populating
        
         if(CTISkill!= null) {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.SUCESS - Account ID (def_Subscription_Id) ='+def_Subscription_Id);
            if(CountryCode!=null){
              caseEditPage.getParameters().put('CF00NA0000009eEhK',CountryCode);
            }
            if(CaseCategory!=null){
            caseEditPage.getParameters().put('00NA00000081Ap2',CaseCategory);
            //caseEditPage.getParameters().put('00NA00000081Ap2',CaseReason);
            caseEditPage.getParameters().put('00NA0000009yWJ3',LOE);
            caseEditPage.getParameters().put('CF00NA0000009hS6K',CaseTeam);
            }
        }
        if(CaseReason!=null&&CaseReason!='null'&&CaseReason!='null')
        {
          caseEditPage.getParameters().put('00NA00000081Ap3',CaseReason);
        }
        if(SubReason!=null&&SubReason!='null')
        {
          caseEditPage.getParameters().put('00NA00000081Aog',SubReason);
        }
        if(Subject!=null)
        { 
          caseEditPage.getParameters().put('cas14',Subject);
        }
        else {
            System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.WARNING - Subscription ID is null.');
        }
        if(CaseCategory!=null){
            caseEditPage.getParameters().put('00NA00000081Ap2',CaseCategory);
        }
        // Disable override
        caseEditPage.getParameters().put(Label.CL00690, Label.CL00691); 
        System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.INFO - Override disabled.');
        return caseEditPage; 
    }    
    
    public pagereference redirectToCaseEditPage(){
        PageReference caseEditPage;
        System.debug('AccId in redirect method is : '+accid);
        System.debug('ConId in redirect method is : '+conid);
        
        if(TobeDeletedVar==false && inactiveVar==false) {
    
            try {
        
                // Logic to validate if the account/contact is a PRMAccount or PRMContact   
                if(accId!=null) {
                    acc = [SELECT Id, RecordTypeId FROM Account WHERE id=:accId LIMIT 1];
                }
                
                if(conId!=null) {
                    con = [SELECT Id, FirstName, LastName, Email, PRMContact__c, Account.Name, Account.RecordTypeId 
                           FROM Contact WHERE Id=:conId LIMIT 1];
                }
                
                System.debug('Account is : '+acc);
                System.debug('Contact is : '+con);
                System.debug('flagPRM Boolean in beginning is : '+flagPRM);
                
                if(acc.RecordTypeId==System.Label.CLJUL15CCC01 || 
                    con.Account.RecordTypeId == System.Label.CLJUL15CCC01) {        
                    
                    if(con.Account.Name.contains(Label.CLJUL15PRM027)) {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, Label.DigitalAcc_CannotConvertPrmDefaultErrorMessage));
                        blnPageError = true;
                    }
                    else {
                    
                        caseEditPage = new PageReference(Label.DigitalAcc_ConvertPrmCaseCreationURL + con.Id);
                        PageReference caseCreationURL = buildCaseCreationURL();                                
                        caseEditPage.getParameters().put('saveURL', caseCreationURL.getURL());
                        String thisRetURL = system.currentpagereference().getParameters().get('retURL');
                        
                        if(thisRetURL != null) {
                            caseEditPage.getParameters().put('retURL', thisRetURL);                    
                        }
                        
                        flagPRM=true;   
                    }
                    
                    
                } else if(Userinfo.getProfileId()=='00eq0000000DmvTAAS') {
                   
                        System.debug(Userinfo.getProfileId());
                        caseEditPage =  new PageReference('/VFP_AddSiteandProductForCase' );

                } else {
                    caseEditPage = caseEditPage = buildCaseCreationURL(); 
                }
                
            }
            catch(Exception e){
            
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                System.debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.'+e.getTypeName()+' - '+ e.getMessage());
                System.debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.'+e.getTypeName()+' - Line = '+ e.getLineNumber());
                System.debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage.'+e.getTypeName()+' - Stack trace = '+ e.getStackTraceString());
            }
        }  
             
        System.Debug('VFC_NewCaseRedirectionPage.redirectToCaseEditPage - End of Method.');
        System.Debug(caseEditPage);
        if(caseEditPage!=null)
            caseEditPageURL = caseEditPage.getUrl();
        return null;        
    }                
}