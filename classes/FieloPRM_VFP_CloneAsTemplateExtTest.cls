/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description:
********************************************************************/
@isTest
private class FieloPRM_VFP_CloneAsTemplateExtTest  {
    
    @testSetup static void testSetupMethodPRM() {
        Country__c testCountry = new Country__c();
        testCountry.Name   = 'Global';
        testCountry.CountryCode__c = 'WW';
        insert testCountry;
       
        PRMCountry__c testPRmcountry = new PRMCountry__c();
        testPRmcountry.Name   = 'Global';
        testPRmcountry.Country__c = testCountry.id;
        testPRmcountry.PLDatapool__c = '123123test';
        insert testPRmcountry;
    }
    
    @isTest static void test_one() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            CountryChannels__c channels = new CountryChannels__c();
            insert channels;
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            parentMenu.FieloEE__ExternalName__c = 'parentMenuTest';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.id);
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.name = 'test';
            insert tag;
            
            FieloEE__Component__c component = new FieloEE__Component__c();
            component.FieloEE__Menu__c = thisMenu.id;
            component.FieloEE__Tag__c = tag.id;
            insert component;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c();
            contentFeed.FieloEE__IsActive__c = true;
            contentFeed.FieloEE__Component__c = component.id;
            contentFeed.FieloEE__CategoryItem__c = category.id;
            insert contentFeed;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c();
            tagItem.FieloEE__News__c = contentFeed.id;
            tagItem.FieloEE__Tag__c = tag.id;
            insert tagItem;
            
            FieloEE__Component__c compo = new FieloEE__Component__c(); 
            compo.FieloEE__Menu__c = thisMenu.id;
            insert compo;
            
            FieloPRM_MenuOptions__c options = new FieloPRM_MenuOptions__c();
            options.Name = 'Option1';
            options.F_PRM_Help__c = 'Big';
            options.F_PRM_Label__c = 'Lalala';
            insert options;
            
            List<FieloEE__Menu__c> menus = new List<FieloEE__Menu__c>();
            menus.add(thisMenu);

            FieloEE__Menu__c thisMenu2 = new FieloEE__Menu__c();
            thisMenu2.FieloEE__Title__c = 'Test';
            thisMenu2.F_PRM_Type__c = 'Template';
            thisMenu2.FieloEE__ExternalName__c = 'thisMenu2Test';
            insert thisMenu2;
                       
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_ChallengeFilter__c = 'Ch' + datetime.now();
            insert challenge;
            
            FieloEE__SegmentDomain__c segDomain = new FieloEE__SegmentDomain__c();
            segDomain.FieloCH__Challenge__c = challenge.id; 
            segDomain.FieloEE__Menu__c  = thisMenu.id ;
            segDomain.FieloEE__Segment__c = segment.id;
            insert segDomain;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(thisMenu);
            
            test.startTest();
            FieloPRM_VFP_CloneAsTemplateExtension thisExt = new FieloPRM_VFP_CloneAsTemplateExtension(sc);
            thisExt.redirectCloneAsTemplate();    
            test.stopTest();
        }
    }
    
}