@isTest
private class VFC_ReturnItemsMassCreate_TEST{

static testMethod void myUnitTest() {

    // Create Country
    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    // Create User
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    // Create Account
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id; 
    insert accountObj;
    
    // Create Contact to the Account
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
      
    // Create Case
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
     CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.Quantity__c = 4;
    CaseObj.Family__c = 'ADVANCED PANEL';
    CaseObj.CommercialReference__c='xbtg5230';
    CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
    CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
    Insert CaseObj;

    // Create ROC
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             AssessmentType__c = 'Product Return to Expert Center',
                                             ProductDestinationCapability__c = 'Technical Expert Assessment - In-House'
                                             );
    Insert ROC;
    
    ResolutionOptionAndCapabilities__c ROC1 = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC1;   


    // Create Organization (Expert Center)
     BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg; 

     // Create Organization (Expert Center)
     BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST1',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg1 ; 


     RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Technical Expert Assessment - In-House',
                                CommercialReference__c='rxbtg5230',
                                 //CommercialReference__c='',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT;

   
      RMA__c RR = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
      Insert RR;
      
       RMA_Product__c RI = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='xbtg5230',
                        ProductFamily__c='HMI SCHNEIDER BRAND',
                        RMA__c=RR.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1'
                        );
                        RI.SerialNumber__c='12';
    Insert RI;
    
    RMA__c RR_1 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street',
                status__c =Label.CLMAY13RR12
                
                );
      Insert RR_1;

     List<CS_ReturnItemsCSVMap__c> csri=new List<CS_ReturnItemsCSVMap__c>();
     csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=0,Name='Commercial Reference'));
     csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=1,Name='Family'));
     csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=2,Name='Quantity'));
     csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=3,Name='Customer Resolution'));
     csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=4,Name='Serial Numbers'));
     csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=5,Name='Data Codes'));
    csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=6,Name='Warranty Status'));
    csri.add(new CS_ReturnItemsCSVMap__c(ColumnNumber__c=7,Name='Billing Method'));
     insert csri;
     
     //HBP
    

      ApexPages.StandardSetController Controller = new ApexPages.StandardSetController(new List<RMA__c>{RR});
      VFC_ReturnItemsMassCreate controllerinstance=new VFC_ReturnItemsMassCreate(controller);

      ApexPages.CurrentPage().getParameters().put('id', RR.id);
      

      controllerinstance= new VFC_ReturnItemsMassCreate(controller);
      List<List<String>> CsvList = new List<List<String>>();
      CsvList.add(new List<String>{'Commercial Reference','Product Family','Quantity','Customer Resolution','Serial Numbers','Data Codes','Warranty Status','Billing Method'});
      CsvList.add(new List<String>{'XBTG5230','XYZ','100','REP','111','222','test','TEST Billing'});
      
     
      Blob bodyBlob=Blob.valueOf('Commercial Reference,Family,Quantity,Customer Resolution,Serial Numbers,Data Codes,Warranty Status,Billing Method\rxbtg5230, ,100,rep,111111,1/28/2013\rxbtg5230, ,100,rep,111111,1/28/2013,Out of warranty,After Inspection\rxbtg5230, ,-100,rep,211111,1/28/2013,Out of warranty,After Inspection\rxbtg5230, ,100,reSS,121111,1/28/2013\r, ,100,reSS,121111,1/28/2013\rxbtg5230, ,100,,121111,1/28/2013,Out of warranty,After Inspection');
      controllerinstance.ContentFile= bodyBlob;
     
      //Test.setMock(WebServiceMock.class, new VFC_ReturnItemsMassCreate_WebTEST());
      controllerinstance.displayCSVEntries();
      controllerinstance.displayReturnItemsAfterValidation();
      controllerinstance.insertReturnItems();
      
      controllerinstance.doCancel();
      // controllerinstance.ContentFile=null;
     
      //Test.setMock(WebServiceMock.class, new VFC_ReturnItemsMassCreate_WebTEST());
      
      controllerinstance.doCancel();
      Blob bodyBlob1=Blob.valueOf('Commercial Reference,Product Family,Quantity,Customer Resolution,Serial Numbers,Data Codes\"rxbtg5230", ,"100","rep","111111","1/28/2013\rxbtg5230", ,"100",rep,111111,1/28/2013\rxbtg5230, ,-100,rep,211111,1/28/2013\rxbtg5230, ,100,reSS,121111,1/28/2013\r, ,100,reSS,121111,1/28/2013\rxbtg5230, ,100,,121111,1/28/2013"');
      controllerinstance.ContentFile= bodyBlob1;
      //controllerinstance.displayCSVEntries();
      //controllerinstance.displayReturnItemsAfterValidation();
      //HBP
      ApexPages.StandardSetController Controller_1= new ApexPages.StandardSetController(new List<RMA__c>{RR_1});
      VFC_ReturnItemsMassCreate controllerinstance_1=new VFC_ReturnItemsMassCreate(controller_1);

      ApexPages.CurrentPage().getParameters().put('id', RR_1.id);
      
      Blob bodyBlob2=Blob.valueOf('Commercial Reference,Product Family,Quantity,Customer Resolution,Serial Numbers,Data Codes\"rxbtg5230", ,"-100",,,"1/28/2013');
      controllerinstance_1.ContentFile= null;
      //controllerinstance.displayCSVEntries();
      controllerinstance.displayReturnItemsAfterValidation(); 
     
    }
    
    static testMethod void myUnitTest_2() {
     
       // Create Country
        Country__c CountryObj = Utils_TestMethods.createCountry();
        Insert CountryObj;
        
        // Create User
        User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
        Insert RRTestUser ;
        
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObj.Id; 
        insert accountObj;
        
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
        insert ContactObj;
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
        CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
        Insert CaseObj;
      
      
        RMA__c RR_1 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street',
                status__c =Label.CLMAY13RR12
                
                );
      Insert RR_1;
      ApexPages.StandardSetController Controller_1= new ApexPages.StandardSetController(new List<RMA__c>{RR_1});
      VFC_ReturnItemsMassCreate controllerinstance_1=new VFC_ReturnItemsMassCreate(controller_1);

      ApexPages.CurrentPage().getParameters().put('id', RR_1.id);
      
      Blob bodyBlob2=Blob.valueOf('Commercial Reference,Product Family,Quantity,Customer Resolution,Serial Numbers,Data Codes\"rxbtg5230", ,"-100",,,"1/28/2013,,');
      controllerinstance_1.ContentFile= null;
      controllerinstance_1.displayCSVEntries();
      controllerinstance_1.displayReturnItemsAfterValidation(); 
      controllerinstance_1.insertReturnItems();
      
    
     }
    


}