public class SVMXC_InstalledProductViewTechAttExt {

	public String searchString {get; set;}

	public class technicalAttribute {
    	public String name {get; set;}
    	public String apiname {get; set;}
    	public String value {get; set;}
	}
	
	//List<technicalAttribute> technicalAttributes = new List<technicalAttribute>();
	public List<technicalAttribute> technicalAttributes {get; set;}
	
	public List<technicalAttribute> parseJSON(String techAttributes){

		List<technicalAttribute> returnTechnicalAttributes = new List<technicalAttribute>();
		// Create a JSON Parser from String.
        JSONParser parser = JSON.createParser(techAttributes);
        while (parser.nextToken() != null) {
            // Check for JSON array starting.
            // START_ARRAY This token is returned when '[' is encountered.
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    // Check for JSON object starting
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        // Read entire invoice object, including its array of line items.
                        technicalAttribute ta = (technicalAttribute)parser.readValueAs(technicalAttribute.class);
                        system.debug('Technical Attributte' + ta);             
    
                        returnTechnicalAttributes.add(ta);   
                    }    
				}        
			}
		}
        return returnTechnicalAttributes;  
    }
	
	private final SVMXC__Installed_Product__c ip;
	
	public SVMXC__Installed_Product__c currentIP {get; set;}

    public ApexPages.standardController controller {get; set;}
            
    public SVMXC_InstalledProductViewTechAttExt(ApexPages.StandardController stdcontroller) {
        controller = stdcontroller;
        this.ip = (SVMXC__Installed_Product__c)controller.getRecord();
    }
    
    public PageReference init() {
      		
		technicalAttributes = new List<technicalAttribute>();
		currentIP = [SELECT TechnicalAttributePart1__c FROM SVMXC__Installed_Product__c WHERE Id =: ip.Id LIMIT 1];
		
		if (currentIP.TechnicalAttributePart1__c != null)
			technicalAttributes = parseJSON(currentIP.TechnicalAttributePart1__c);
    
    	return null;	
    }
    
	public PageReference returnToIP() {
        
        PageReference scPage = new PageReference('/' + currentIP.Id);
        scPage.setRedirect(true);
        return scPage; 
    }
    
    public PageReference search() {
    
    	return null;

    }
    
    public PageReference save() {
    
    	return null;

    }
}