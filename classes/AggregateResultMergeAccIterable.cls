global class AggregateResultMergeAccIterable implements Iterable<AggregateResult> {
   global Iterator<AggregateResult> Iterator(){
      return new AggregateResultMergeAccIterator();
   }
}