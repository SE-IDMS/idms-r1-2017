/*
Created By: Deepak Kumar
Created Date: 16-03-2014
Description: User After Update for FSR
*/
public class AP_User_TechnicianUpdate
{   
   
    public Static void UpdateTechnician(List<User> Ulist,set<id> uid)
    {   
        List<SVMXC__Service_Group_Members__c> techlist = New List<SVMXC__Service_Group_Members__c>(); 
        List<SVMXC__Service_Group_Members__c> techlistToUpdate = New List<SVMXC__Service_Group_Members__c>(); 
        Map<id,List<SVMXC__Service_Group_Members__c>> uidGMListMap = new Map<id, List<SVMXC__Service_Group_Members__c>>();
        
        Map<id, User>  uidRecMap = new Map<id, User>();
        
        if(Ulist != null && Ulist.size()>0){
            uidRecMap.putAll(Ulist);
        }
        
        
        
        techlist= [Select Business_Unit__c,Name,DepotCity__c,DepotState__c,SESAID__c,SVMXC__Role__c,SVMXC__Zip__c,SVMXC__Phone__c,
                          SVMXC__Country__c,SVMXC__Salesforce_User__c   From SVMXC__Service_Group_Members__c Where SVMXC__Salesforce_User__c=:uid];
                          
        for(SVMXC__Service_Group_Members__c gmObj:techlist){
        
            if(uidGMListMap.containsKey(gmObj.SVMXC__Salesforce_User__c)){
            
                uidGMListMap.get(gmObj.SVMXC__Salesforce_User__c).add(gmObj);
            
            }
            else{
                uidGMListMap.put(gmObj.SVMXC__Salesforce_User__c , new List<SVMXC__Service_Group_Members__c>());
                uidGMListMap.get(gmObj.SVMXC__Salesforce_User__c).add(gmObj);
            }
        
        }
        
        for(id mid: uid){
        
            if(uidGMListMap.containskey(mid))
            {
                
                    List<SVMXC__Service_Group_Members__c> gmList = uidGMListMap.get(mid);
                    
                    user u = uidRecMap.get(mid);
                    
                    List<Country__c> cn = New List<Country__c>();
                    cn = [SELECT CountryCode__c,Id,Name FROM Country__c];
                    string s = u.Country;
                                        
                    for(SVMXC__Service_Group_Members__c tech: gmList)
                    {
                        
                        tech.SVMXC__Street__c=u.Street;
                        tech.SVMXC__Zip__c=u.PostalCode;
                        tech.SVMXC__City__c=u.City;
                        tech.SVMXC__State__c=u.State;
                        tech.Name=u.FirstName+' '+ u.LastName;
                        tech.Business_Unit__c=u.UserBusinessUnit__c;
                        tech.SESAID__c=u.FederationIdentifier;  
                        tech.SVMXC__Phone__c = u.MobilePhone;
                        tech.SVMXC__Email__c = u.Email;
                        tech.Manager__c=u.ManagerId;
                        // tech.SVMXC__Role__c=u.UserRole;
                        
                        for(Country__c c :cn)
                        {
                            if(c.CountryCode__c ==s)
                            { 
                                tech.SVMXC__Country__c = c.Name;
                            }
                        }
                        techlistToUpdate.add(tech);                        
                    
                    }
            }
        
        }
        
        if(techlistToUpdate.size()>0) 
        {
           //update techlistToUpdate;
           List<Database.SaveResult> SR =   Database.update(techlistToUpdate,false);
           
        }

    } 
}