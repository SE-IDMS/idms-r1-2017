/*
    Author          : Ramesh Rajasekaran (SFDC) 
    Date Created    : 08/10/2012
    Description     : Test class for VFC_QuestionController
*/
@isTest
private class VFC_QuestionController_TEST {

    static testMethod void testcontrollermethod() {
        
     Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        QOTW__c qc = new QOTW__c(StartDate__c = system.date.today(),Enddate__c = system.date.today(),IsActive__c = true);
        insert qc;
        QuestionTemplate__c q = new QuestionTemplate__c(QuestionoftheWeek__c = qc.id,
        IncludeOther__c = true,
        Answers__c = 'test\ntest\n');
        insert q;
        CustomerQuickQuestion__c  cv = new CustomerQuickQuestion__c(QuestionTemplate__c = q.id,
        Account__c = objAccount.Id,
        Contact__c = objContact.Id);
        insert cv;
        
        VFC_QuestionController qtp = new VFC_QuestionController(new ApexPages.StandardController(objContact));
        qtp.saveresponse();
        qtp.getAnswerChoices();
        qtp.getChoices();   
      
    }
}