/*Created by: Jyotiranjan Singhlal
Date:11/11/2012
For the purpose of Pre Populating Field Values

Date Modified       Modified By         Comments
----------------------------------------------------------------------------------------------------------------
25/02/2013      Jyotiranjan Singhlal    Pre Populating the Customer location Time Zone for December 2012 release
21/03/2013      Vimal                   Prepopulation on Service Business Unit,if logged in user's User Business is ITB, May 2013 Release
11/04/2013      Jyotiranjan Singhlal    Pre Populating the Service Request Reason for May 2013 release
19/04/2013      Jyotiranjan Singhlal    Commenetd the Pre Populating the Service Request Reason code
15/07/2013      Jyotiranjan Singhlal    Added logic for Pre Populating the Case Account Location values.
17/07/2013      Deepak Kumar            Added the logic To pre Populate the Case Contact name value.
*/
public class VFC_WorkOrderNotification_Prepopulate
{
    PageReference NewEditPage = new PageReference('/'+SObjectType.WorkOrderNotification__c.getKeyPrefix()+'/e' );
    String CustomerLocationid;
    String CustomerLocationname;
    String Locationid;
    String Locationname;

    //Constructor

    public VFC_WorkOrderNotification_Prepopulate(ApexPages.StandardController controller)
    {
      
    }
    /* This method will take the control to editpage i.e and will pre populate the customer
     location, customer time zone*/
     
    public Pagereference gotoeditpage()
    {
        //system.debug('custl:'+System.currentPageReference().getParameters().get('retURL'));
        //CustomerLocationName = System.currentpagereference().getParameters().get(Label.CLDEC12SRV06); 
        //CustomerLocationid = System.currentpagereference().getParameters().get(Label.CLDEC12SRV07);
        LocationName = System.currentpagereference().getParameters().get(Label.CLMAY13SRV11); 
        Locationid = System.currentpagereference().getParameters().get(Label.CLMAY13SRV12);
        String caseNumber = System.currentpagereference().getParameters().get(Label.CLDEC12SRV44);
        String caseId = System.currentpagereference().getParameters().get(Label.CLDEC12SRV45);
        
        String OnSiteContactName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV05);
        String OnSiteContactNameId = System.currentpagereference().getParameters().get(Label.CLOCT13SRV06);
        
        String SoltToAccName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV01);
        String SoldToAccountId = System.currentpagereference().getParameters().get(Label.CLOCT13SRV02);
        
        User u=[select id, name,UserBusinessUnit__c,Country__c from user where id=:UserInfo.getUserId()];

        //system.debug('CustomerLocationid:'+CustomerLocationid);
    /*
        if(CustomerLocationid !=null && CustomerLocationid !='' )
        {
             CustomerLocation__c custl = new CustomerLocation__c();
             custl = [Select RelatedAccount__c, RelatedAccount__r.Name,Time_Zone__c from CustomerLocation__c where id =: CustomerLocationid];
             
             if(custl.RelatedAccount__c != null)
             {
                NewEditPage.getParameters().put(Label.CLDEC12SRV06, CustomerLocationName );
                NewEditPage.getParameters().put(Label.CLDEC12SRV07, CustomerLocationid );
                NewEditPage.getParameters().put(Label.CLDEC12SRV08, custl.Time_Zone__c );
             }
             else 
             {
                System.Debug('VPF_WorkOrderNotification_PrePopulate.gotoeditpage.WARNING - CustomerLocationId is null.');
             }*/
        if(Locationid !=null && Locationid !='' )
        {
            SVMXC__Site__c loc = new SVMXC__Site__c();
            loc = [Select SVMXC__Account__c,SVMXC__Account__r.name,TimeZone__c from SVMXC__Site__c where id =: Locationid];
            
            if(loc.SVMXC__Account__c != null)
            {
                NewEditPage.getParameters().put(Label.CLMAY13SRV11, LocationName );
                NewEditPage.getParameters().put(Label.CLMAY13SRV12, Locationid );
                NewEditPage.getParameters().put(Label.CLDEC12SRV08, loc.TimeZone__c );
            }
            else 
            {
                System.Debug('VPF_WorkOrderNotification_PrePopulate.gotoeditpage.WARNING - LocationId is null.');
            }
            NewEditPage.getParameters().put(Label.CL00330, '/'+ Locationid);
             
        }
        else if(caseID != null && caseId.trim()!= '')
        {
            Case case1 = new Case();
            case1 = [Select SerialNumber__c,AccountId,Account.Name,AssetOwner__c,AssetOwner__r.Name,
            ContactId,Contact.Name,SVMXC__Site__c,SVMXC__Site__r.Name,SVMXC__Site__r.TimeZone__c,
            SVMXC__Component__c,SVMXC__Component__r.Name,CustomerRequest__c,SVMXC__Service_Contract__c,
            SVMXC__Service_Contract__r.Name,SVMXC__SLA_Terms__c,SVMXC__SLA_Terms__r.Name,Priority from Case where id =: caseID];
            
            NewEditPage.getParameters().put(Label.CLDEC12SRV44, caseNumber);
            NewEditPage.getParameters().put(Label.CLDEC12SRV45, caseId);
            NewEditPage.getParameters().put(Label.CLDEC12SRV50, case1.SerialNumber__C);
            //NewEditPage.getParameters().put(Label.CLOCT13SRV01, case1.Account.Name);
            //NewEditPage.getParameters().put(Label.CLOCT13SRV02, case1.AccountId);
            //NewEditPage.getParameters().put(Label.CLOCT13SRV05, case1.Contact.Name);
            //NewEditPage.getParameters().put(Label.CLOCT13SRV06, case1.ContactId);
            NewEditPage.getParameters().put(Label.CLOCT13SRV26, case1.SVMXC__Component__r.Name);
            NewEditPage.getParameters().put(Label.CLOCT13SRV27, case1.SVMXC__Component__c);


            
            if(case1.AssetOwner__c != null)
            {
                NewEditPage.getParameters().put(Label.CLOCT13SRV24, case1.AssetOwner__r.Name);
                NewEditPage.getParameters().put(Label.CLOCT13SRV25, case1.AssetOwner__c);   
            }
            else if(case1.AccountId != null)
            {
                NewEditPage.getParameters().put(Label.CLOCT13SRV24, case1.Account.Name);
                NewEditPage.getParameters().put(Label.CLOCT13SRV25, case1.AccountId);  
            }
            
                        
            if(case1.SVMXC__Site__c !=null)
            {
                system.debug('*****');
                NewEditPage.getParameters().put(Label.CLMAY13SRV11, case1.SVMXC__Site__r.Name);
                NewEditPage.getParameters().put(Label.CLMAY13SRV12, case1.SVMXC__Site__c);
                NewEditPage.getParameters().put(Label.CLDEC12SRV08, case1.SVMXC__Site__r.TimeZone__c);
            }
            else
            {   
                if(case1.AssetOwner__c != null)
                {
                    List<SVMXC__Site__c> loc1 = new List<SVMXC__Site__c>();
                    loc1 = [Select Id,Name,TimeZone__c from SVMXC__Site__c where SVMXC__Account__c =: case1.AssetOwner__c AND PrimaryLocation__c =: True limit 1];
                    system.debug('loc1:'+loc1);
                    List<SVMXC__Site__c> loc2 = new List<SVMXC__Site__c>();
                    loc2 = [Select Id,Name,TimeZone__c from SVMXC__Site__c where SVMXC__Account__c =: case1.AssetOwner__c  AND PrimaryLocation__c =: False limit 1];
                    system.debug('loc2:'+loc2);
                    if(loc1 != null & loc1.size()>0)
                    {
                        NewEditPage.getParameters().put(Label.CLMAY13SRV11, loc1[0].Name);
                        NewEditPage.getParameters().put(Label.CLMAY13SRV12, loc1[0].Id);
                        NewEditPage.getParameters().put(Label.CLDEC12SRV08, loc1[0].TimeZone__c );
                    }
                    else if(loc2 != null& loc2.size()>0)
                    {
                        NewEditPage.getParameters().put(Label.CLMAY13SRV11, loc2[0].Name);
                        NewEditPage.getParameters().put(Label.CLMAY13SRV12, loc2[0].Id);
                        NewEditPage.getParameters().put(Label.CLDEC12SRV08, loc2[0].TimeZone__c );
                    }
                }
                else if(case1.AccountId != null)
                {
                    List<SVMXC__Site__c> loc3 = new List<SVMXC__Site__c>();
                    loc3 = [Select Id,Name,TimeZone__c from SVMXC__Site__c where SVMXC__Account__c =: case1.AccountId AND PrimaryLocation__c =: True limit 1];
                    List<SVMXC__Site__c> loc4 = new List<SVMXC__Site__c>();
                    loc4 = [Select Id,Name,TimeZone__c from SVMXC__Site__c where SVMXC__Account__c =: case1.AccountId  AND PrimaryLocation__c =: False limit 1];
                    system.debug('loc2:'+loc4);
                    if(loc3 != null& loc3.size()>0)
                    {
                        NewEditPage.getParameters().put(Label.CLMAY13SRV11, loc3[0].Name);
                        NewEditPage.getParameters().put(Label.CLMAY13SRV12, loc3[0].Id);
                        NewEditPage.getParameters().put(Label.CLDEC12SRV08, loc3[0].TimeZone__c );
                    }
                    else if(loc4 != null& loc4.size()>0)
                    {
                        NewEditPage.getParameters().put(Label.CLMAY13SRV11, loc4[0].Name);
                        NewEditPage.getParameters().put(Label.CLMAY13SRV12, loc4[0].Id);
                        NewEditPage.getParameters().put(Label.CLDEC12SRV08, loc4[0].TimeZone__c );
                    }
                }
            }         
            //system.debug('SoltToAccName'+SoltToAccName);
            NewEditPage.getParameters().put(Label.CLMAY13SRV41, case1.CustomerRequest__c);
            NewEditPage.getParameters().put(Label.CLOCT13SRV42, case1.CustomerRequest__c);
            NewEditPage.getParameters().put(Label.CLOCT13SRV37, case1.SVMXC__Service_Contract__c);
            NewEditPage.getParameters().put(Label.CLOCT13SRV38, case1.SVMXC__Service_Contract__r.Name);
            NewEditPage.getParameters().put(Label.CLOCT13SRV39, case1.SVMXC__SLA_Terms__c);
            NewEditPage.getParameters().put(Label.CLOCT13SRV40, case1.SVMXC__SLA_Terms__r.Name);
            NewEditPage.getParameters().put(Label.CLOCT13SRV41, case1.Priority);
            NewEditPage.getParameters().put(Label.CL00330, '/'+ caseID);  
        }
         //Start: ITB WON Requirment
        if(u.UserBusinessUnit__c!= null /*&& u.UserBusinessUnit__c=='IT'*/ ){   
                NewEditPage.getParameters().put(System.Label.CLMAY13CCC23,u.UserBusinessUnit__c);
        }
        NewEditPage.getParameters().put('nooverride', '1'); 
        system.debug('NewEditPage:'+NewEditPage);
        return NewEditPage;
    }
}