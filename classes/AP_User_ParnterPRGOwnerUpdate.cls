/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 21 March 2013
    Description : For PRM May 2013 Release:
                  1.    To add the Users having ‘Program Owner’ checkbox checked to ‘Partner Program Owners’ public group.
                  2.    To remove the Users whose ‘Program Owner’ checkbox is unchecked from ‘Partner Program Owners’ public group.
                 
    
********************************************************************************************************************/
public class AP_User_ParnterPRGOwnerUpdate
{
    public static void addUserToGroup(list<User> lstUser)
    {
        list<GroupMember> lstGroupMember = new list<GroupMember>();
        for(User u : lstUser)
        {
            if(u.programOwner__c)
            {
                GroupMember member = new GroupMember();
                member.UserOrGroupId = u.Id;
                member.GroupId = Label.CLMAY13PRM25;
                lstGroupMember.add(member);
            }
            if(u.programAdministrator__c || u.programApprover__c)
            {
                GroupMember member = new GroupMember();
                member.UserOrGroupId = u.Id;
                member.GroupId = Label.CLMAY13PRM39;
                lstGroupMember.add(member);
            }
        }
        if(!lstGroupMember.isEmpty())
        {
            Database.SaveResult[] SaveResult = database.insert(lstGroupMember,false);
               
            for(Integer i=0;i<SaveResult.size();i++ )
            {
               Database.SaveResult sr =SaveResult[i];
               if(!sr.isSuccess())
               {
                  Database.Error err = sr.getErrors()[0];
                  system.debug('Exception while adding User to Group:'+err.getMessage());
                  lstUser[0].addError(Label.CLMAY13PRM26);
               }
               
            }
        }
    }
    
    public static void removeUserFromGroup(list<User> lstUser)
    {
        list<GroupMember> lstGroupMem = new list<GroupMember>();
        list<GroupMember> lstGroupMemToDelete = new list<GroupMember>();
        map<ID,set<Id>> mapUserGroup = new map<ID, set<ID>>();
system.debug('lstUser:'+lstUser);
        for(User u : lstUser)
        {
            if(u.programowner__c == false)
            {
                if(!mapUserGroup.containsKey(u.Id))
                    mapUserGroup.put(u.ID, new set<ID> {(Label.CLMAY13PRM25)});
                else
                    mapUserGroup.get(u.ID).add(Label.CLMAY13PRM25);
            }
            if(u.programapprover__c == false || u.programadministrator__c == false)
            {
                if(!mapUserGroup.containsKey(u.Id))
                    mapUserGroup.put(u.ID, new set<ID> {(Label.CLMAY13PRM39)});
                else
                    mapUserGroup.get(u.ID).add(Label.CLMAY13PRM39);
            }
        }
system.debug('mapUserGroup:'+mapUserGroup);
        lstGroupMem = [Select id,groupid,userorgroupid from GroupMember where (groupid = :Label.CLMAY13PRM25 or groupid =: Label.CLMAY13PRM39) and UserOrGroupId in :lstUser limit 10000];
system.debug('lstGroupMem:'+lstGroupMem);
        if(!lstGroupMem.isEmpty())
        {
            for(GroupMember member : lstGroupMem)
            {
                if(mapUserGroup.get(member.userorgroupid).contains(member.groupid))
                    lstGroupMemToDelete.add(member);
            }
            if(!lstGroupMemToDelete.isEmpty())
            {
                Database.DeleteResult[] SaveResult = database.delete(lstGroupMemToDelete,false);
                   
                for(Integer i=0;i<SaveResult.size();i++ )
                {
                   Database.DeleteResult sr =SaveResult[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      system.debug('Exception while adding User to Group:'+err.getMessage());
                      lstUser[0].addError(Label.CLMAY13PRM26);
                   }
                   
                }
            }
        }
    }
}