/**
 * Author: EdifiXio (Akram GARGOURI)
 * Date: 12/05/2016
 * Description: 
 * Related Components: 
 */

public with sharing class PRM_VFC_PropertiesAndActivitiesCatalog {

	public ExternalPropertiesCatalog__c mysObject {get;set;} 
    public ExternalPropertiesCatalogType__c currentExternalPropertiesCatalogType {get;set;}
    public FieloEE__Badge__c outputBadge {get;set;}
    public FieloCH__MissionCriteria__c currentMissionCriteria  {get;set;}
    public ProgramLevel__c currentProgramLevel {get;set;}
    public PRMTransactionCatalog__c outputTransactionCatalog {get;set;}
    public List<FieloPRM_EventCatalog__c> outputEventCatalogList {get;set;}
    public Boolean allowRetroCalculation  {get;set;}
    public List<PRMExternalPropertyEvent__c>  outputPropertyEventCatalogList {get;set;}
    private Set<PRMExternalPropertyEvent__c> outputPropertyEventCatalogToDeleteList = new Set<PRMExternalPropertyEvent__c>();
    public FieloPRM_EventCatalog__c event {get;set;}
    public Map<String,Set<String>> typeBySource;
    private Map<string,Schema.SObjectField> eventSchemaMaps {get;set;} 
    public static String AUTOMATIC_EVENT_TYPE='Account Assigned Program';
    //public String selectedCountry{get;set;}
    //Check get parameters
    public Id recordTypeId = !String.isEmpty(Apexpages.currentpage().getParameters().get('recordTypeId'))?(Id)Apexpages.currentpage().getParameters().get('recordTypeId'):null;
    public String source = !String.isEmpty(Apexpages.currentpage().getParameters().get('Source'))?(String)Apexpages.currentpage().getParameters().get('Source'):null;
    public String systemType = !String.isEmpty(Apexpages.currentpage().getParameters().get('SystemType'))?(String)Apexpages.currentpage().getParameters().get('SystemType'):null;
    public String programLevelName = !String.isEmpty(Apexpages.currentpage().getParameters().get('ProgramLevelName'))?(String)Apexpages.currentpage().getParameters().get('ProgramLevelName'):null;

    public Id missionId = !String.isEmpty(Apexpages.currentpage().getParameters().get('missionId'))?(Id)Apexpages.currentpage().getParameters().get('missionId'):null;
    public Id programLevelId { get{ 
        return !String.isEmpty(Apexpages.currentpage().getParameters().get('ProgramLevelId'))?(Id)Apexpages.currentpage().getParameters().get('ProgramLevelId'):null;}
    }
    //Properties
    public String externalSystemType {get{
            return internalRecordType?'':externalActivitiesRecordType?'External Activities':'External Properties';
        }
    }
    public String createEventCatalogLink{ get{
            Schema.DescribeSObjectResult r = FieloPRM_EventCatalog__c.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            return '/'+keyPrefix+'/e?RecordType='+Label.CLJUN16PRM102+'&Name='+(mysObject.SubType__c!=null?mysObject.SubType__c:(currentExternalPropertiesCatalogType.Type__c!=null?currentExternalPropertiesCatalogType.Type__c:''))+'&00N1200000BL3Vi=1&00N1200000BL3Vf='+(event.F_PRM_Type__c!=null?event.F_PRM_Type__c:'')+'&CF00N1200000BL3Vd=Global&00N1200000BL3Vg='+(event.F_PRM_ValueText__c!=null?event.F_PRM_ValueText__c:'')+'&00N1200000BL3Ve=Automatic%20Event';
        }
    }
    public Boolean internalRecordType{ get{
            return mysObject.RecordTypeId!=null?(Id)mysObject.RecordTypeId == (Id)System.label.CLJUN16PRM057:false;
        }
    }
    public Boolean externalActivitiesRecordType{ get{
            return mysObject.RecordTypeId!=null?(Id)mysObject.RecordTypeId == (Id)System.label.CLJUN16PRM065:false;
        }
    }
    public List<SelectOption> sourceList { get{
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', ''));
            for(String opt:  typeBySource.keySet()){
                options.add(new SelectOption(opt, opt));
            }
            return options;
        }
    }

    public List<SelectOption> typeList { get{
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', ''));
            System.debug('currentExternalPropertiesCatalogType.Source__c'+ currentExternalPropertiesCatalogType.Source__c);
            System.debug('typeBySource'+ typeBySource);
            if(!String.isEmpty(currentExternalPropertiesCatalogType.Source__c)){
                for(String opt:  typeBySource.get(currentExternalPropertiesCatalogType.Source__c)){
                    options.add(new SelectOption(opt, opt));
                }
            }
            return options;
        }
    }
    /*
    public List<SelectOption> getCountriesOptions() {
        List<SelectOption> countryOptions = new List<SelectOption>();
        countryOptions.add(new SelectOption('All','All'));
        countryOptions.add(new SelectOption('Global','Global'));
        for(PRMCountry__c country =[select id,Name,Country__c,MemberCountry1__c,MemberCountry2__c,MemberCountry3__c,MemberCountry4__c,MemberCountry5__c, from PRMCountry__c]){
            countryOptions.add(new SelectOption(country.Country__c,country.Country__c));
            if(MemberCountry1__c!null){
            }countryOptions.add(new SelectOption(country.Country__c,country.Country__c));
        }
        return countryOptions;
    }*/

    public PRM_VFC_PropertiesAndActivitiesCatalog(ApexPages.StandardController stdController) {
        try
        {
            this.mysObject = (ExternalPropertiesCatalog__c)stdController.getRecord();
            event = new FieloPRM_EventCatalog__c();
            eventSchemaMaps = Schema.SObjectType.FieloPRM_EventCatalog__c.fields.getMap();
            outputEventCatalogList = new List<FieloPRM_EventCatalog__c>();
            outputPropertyEventCatalogList = new List<PRMExternalPropertyEvent__c>();
            currentExternalPropertiesCatalogType = new ExternalPropertiesCatalogType__c();
            currentMissionCriteria = new FieloCH__MissionCriteria__c(FieloCH__Mission__c=missionId);
            retrieveEvent();
            //selectedCountry = 'All';
            //bind parameters with variables
            mysObject.RecordTypeId = recordTypeId!=null?recordTypeId:mysObject.RecordTypeId;
            currentExternalPropertiesCatalogType.Source__c=source!=null?source:currentExternalPropertiesCatalogType.Source__c;
            currentExternalPropertiesCatalogType.Type__c=systemType!=null?systemType:currentExternalPropertiesCatalogType.Type__c;
            
            if(mysObject.Id==null && !internalRecordType){
                if(programLevelId!=null && programLevelName!=null){
                    //We have to create the associatedExtPropCatalog
                    mysObject.SubType__c=programLevelId;
                    mysObject.PropertyName__c=programLevelName;
                    currentProgramLevel= new ProgramLevel__c(Id=programLevelId);
                    mysObject.ExternalPropertiesCatalogType__r = new ExternalPropertiesCatalogType__c(FunctionalKey__c=currentExternalPropertiesCatalogType.Source__c+currentExternalPropertiesCatalogType.Type__c);
                }
                else{
                    typeBySource = new Map<String,Set<String>>();
                    String soqlTypes = 'select id,Source__c,Type__c from ExternalPropertiesCatalogType__c where Source__c!=\'BFO\' and PropertiesAndCatalogRecordType__c=';
                    soqlTypes+= '\''+externalSystemType+'\'';
                    System.debug('soqlTypes'+ soqlTypes);
                    for(ExternalPropertiesCatalogType__c type: Database.query(soqlTypes)) {
                        if(!typeBySource.containsKey(type.Source__c)){
                            Set<String> myTypeSet = new Set<String>{type.Type__c};
                            typeBySource.put(type.Source__c,myTypeSet);
                        }else{
                            Set<String> myTypeSet = typeBySource.get(type.Source__c);
                            myTypeSet.add(type.Type__c);
                        }
                    }
                    System.debug('typeBySource'+ typeBySource);
                }
            }else{
                if(mysObject.Id!=null){
                    this.mysObject = [select Id,ExternalPropertiesCatalogType__r.Source__c,ExternalPropertiesCatalogType__r.Type__c,SubType__c,Badge__c,TransactionCatalog__c from ExternalPropertiesCatalog__c where Id=:mysObject.Id limit 1];
                    outputPropertyEventCatalogList = [select Id,ExternalPropertiesCatalog__c,EventCatalog__c,EventCatalog__r.Name from PRMExternalPropertyEvent__c where ExternalPropertiesCatalog__c=:mysObject.Id];
                }
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'An unexpected error has occurred: ' + ex.getMessage()));
        }
        
    }

    
    Boolean step1IsValid(){
        if(!internalRecordType && (String.isEmpty(currentExternalPropertiesCatalogType.Source__c) || String.isEmpty(currentExternalPropertiesCatalogType.Type__c))){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Fields cannot be empty'));
            return false;
        }else{
            if(internalRecordType && !PRM_ExternalPropertiesCatalog.checkBFOPropertiesFieldName(new List<ExternalPropertiesCatalog__c>{mysObject})){
                return false;
            }
        }
        return true;
    }

    public PageReference step1() {
       return Page.VFP_PropertiesAndActivitiesCatalogStep1;
    }

    public PageReference step2() {
        if(!step1IsValid()){
            return null;
        }
        // Create a savepoint while AccountNumber is null
        Savepoint sp = Database.setSavepoint();

         //Insert
        try{
            if(!internalRecordType){
                mysObject.ExternalPropertiesCatalogType__r = new ExternalPropertiesCatalogType__c(  FunctionalKey__c=currentExternalPropertiesCatalogType.Source__c+currentExternalPropertiesCatalogType.Type__c);
            }

            System.debug('mysObject: '+mysObject);
            insert mysObject;
            if(programLevelId!=null){
                ProgramLevel__c progLevel = [select id,PropertiesAndActivitiesCatalog__c from ProgramLevel__c where id=:programLevelId];
                if(progLevel.PropertiesAndActivitiesCatalog__c==null){
                    progLevel.PropertiesAndActivitiesCatalog__c = mysObject.Id;
                    update progLevel;
                }
                FieloPRM_EventCatalog__c automaticEvent = new FieloPRM_EventCatalog__c(F_PRM_isActive__c=true,Name=programLevelName,F_PRM_Type__c=AUTOMATIC_EVENT_TYPE,F_PRM_ValueText__c=programLevelName,RecordTypeId=Label.CLJUN16PRM102);
                insert automaticEvent;
                System.debug('automaticEvent: '+automaticEvent);
                outputPropertyEventCatalogList.add(new PRMExternalPropertyEvent__c(ExternalPropertiesCatalog__c=mysObject.Id,EventCatalog__c=automaticEvent.Id));
                insert outputPropertyEventCatalogList;  
                System.debug('outputPropertyEventCatalogList: '+outputPropertyEventCatalogList);          
            }
        }catch(Exception e) {
            System.debug('ExceptionCatch: '+e.getMessage()); 
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'An unexpected error has occurred: ' + e.getMessage()));
            return null;
        }
        Pagereference pageRef = new PageReference('/apex/VFP_PropertiesAndActivitiesCatalogStep2');
        pageref.setRedirect(true);
        pageref.getParameters().put('Id',mysObject.Id);
        pageref.getParameters().put('missionId',missionId);
        pageref.getParameters().put('ProgramLevelId',programLevelId);
        pageref.getParameters().put('ProgramLevelName',programLevelName);
        return pageRef;
    }

    public PageReference step3() {
       return Page.VFP_PropertiesAndActivitiesCatalogStep3;
    }
    
    public void addEventCatalog(){
        system.debug('eventId: '+Apexpages.currentpage().getParameters().get('eventId'));
        Id eventId = Apexpages.currentpage().getParameters().get('eventId');
        for(PRMExternalPropertyEvent__c e:outputPropertyEventCatalogList){
            if(e.EventCatalog__r.Id==eventId)
                return;
        }
        FieloPRM_EventCatalog__c myEvent = new FieloPRM_EventCatalog__c(Id=Apexpages.currentpage().getParameters().get('eventId'),Name=Apexpages.currentpage().getParameters().get('eventName'));
        outputPropertyEventCatalogList.add(new PRMExternalPropertyEvent__c(ExternalPropertiesCatalog__c=mysObject.Id,EventCatalog__c=myEvent.Id,EventCatalog__r=myEvent));
        system.debug('outputPropertyEventCatalogList ' + outputPropertyEventCatalogList);

    }

    public PageReference saveWithEvents(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(programLevelId!=null && allowRetroCalculation){
                Map<Id ,ContactAssignedProgram__c> concernedContact = new Map<Id ,ContactAssignedProgram__c>([select Id,ProgramLevel__c,Contact__c from ContactAssignedProgram__c where ProgramLevel__c=:programLevelId]);
                AP_PartnerContactAssignementMigration.createAndDeleteMemberProperties(null,concernedContact);
            }
            System.debug('outputPropertyEventCatalogList: '+ outputPropertyEventCatalogList);
            upsert outputPropertyEventCatalogList;
            delete new List<PRMExternalPropertyEvent__c>(outputPropertyEventCatalogToDeleteList);
            System.debug('mysObject.Badge__c:'+mysObject.Badge__c);
            System.debug('mysObject.TransactionCatalog__c:'+mysObject.TransactionCatalog__c);
            System.debug('mysObject:'+mysObject);
            update mysObject;
            Pagereference pageRef = new PageReference('/'+mysObject.Id);
            pageref.setRedirect(true);
            return pageRef;
        } catch(Exception e) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'An unexpected error has occurred: ' + e.getMessage()));
        }
        return Apexpages.currentpage();
    }


    public void removeEventCatalog(){
        Id eventId = Apexpages.currentpage().getParameters().get('eventId');
        Integer i=0;
        for(PRMExternalPropertyEvent__c e:outputPropertyEventCatalogList){
            if(e.EventCatalog__r.Id==eventId){
                if(e.Id!=null){
                    outputPropertyEventCatalogToDeleteList.add(e);
                }
                break;
            }
            i++;
        }
        if(eventId!=null && outputPropertyEventCatalogList.size()>i){
            outputPropertyEventCatalogList.remove(i);
        }
    }

    public void refreshtable(){
    
        list<String> listAux = new list<String>();
        listAux.addAll(eventSchemaMaps.keySet());
        String queryCatalog = ' Select ' +  String.Join(listAux, ',') + ' FROM FieloPRM_EventCatalog__c WHERE F_PRM_isActive__c  = true AND Recordtype.developerName = \'Automatic\' ';
        
        
        if(!string.isblank(event.Name)){
            queryCatalog += ' AND name like \'%' + event.Name + '%\' ';
        }
        if(!string.isblank(event.F_PRM_Type__c)){
            queryCatalog += ' AND F_PRM_Type__c like \'%' + event.F_PRM_Type__c + '%\' ';
        }
        if(!string.isblank(event.F_PRM_ValueText__c)){
            queryCatalog += ' AND F_PRM_ValueText__c like \'%' + event.F_PRM_ValueText__c + '%\' ';
        }

        /*if(!string.isblank(selectedCountry)){
                if(selectedCountry == 'All' || selectedCountry == 'Global'){
                   queryCatalog += ' AND  F_PRM_Country__r.Name = \'Global\'';
                }else{
                    queryCatalog += ' AND F_PRM_Country__r.CountryCode__c = \'' + selectedCountry + '\'  ' ;
                }
            }*/
        queryCatalog += 'LIMIT 10';
        
        system.debug('queryCatalog ' + queryCatalog );
        outputEventCatalogList = Database.query(queryCatalog);
        
       
        system.debug('outputEventCatalogList ' + outputEventCatalogList);
    
        
    }

    public void retrieveEvent(){
        if(currentMissionCriteria!=null && !String.isEmpty(currentMissionCriteria.FieloCH__Mission__c)){
            System.debug('currentMissionCriteria'+currentMissionCriteria);
            for(FieloCH__MissionCriteria__c missCrit:[select id,FieloCH__FieldName__c, FieloCH__Values__c from FieloCH__MissionCriteria__c where FieloCH__Mission__c =:currentMissionCriteria.FieloCH__Mission__c]){
                if(missCrit.FieloCH__FieldName__c=='FieloEE__Type__c'){
                     event.F_PRM_Type__c=missCrit.FieloCH__Values__c;
                }else if(missCrit.FieloCH__FieldName__c=='F_PRM_ValueText__c'){
                    event.F_PRM_ValueText__c=missCrit.FieloCH__Values__c;
                }
            } 
            refreshtable();
        }else{
            event.F_PRM_Type__c='';
            event.F_PRM_ValueText__c='';
        }
    }
}