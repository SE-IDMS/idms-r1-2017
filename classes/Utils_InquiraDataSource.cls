/*
    Author          : Nicolas Palitzyne ~ nicolas.palitzyne@accenture.com 
    Date Created    : 28/04/2011
    Description     : Utility abstract class for Data Source Access
*/

public class Utils_InquiraDataSource extends Utils_DataSource
{
    final Integer TIMEOUT = 40000;
    final String INQUIRA_ENDPOINT = Label.CL00372;
    public list<WS05_Inquira.faqBean> Results;
     // Define Columns Label and displayed field  
     public override List<SelectOption> getColumns()
     {
        List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Name',Label.CL00367)); // First Column
        Columns.add(new SelectOption('Title__c',Label.CL00368)); 
        Columns.add(new SelectOption('Excerpt__c',Label.CL00369)); 
        Columns.add(new SelectOption('URL__c',Label.CL00370));
        Columns.add(new SelectOption('Date__c',Label.CL00371)); // Last Column
        return Columns;
     } 
   
     // Main Search Method
     public override Result Search(List<String> KeyWords, List<String> Criteria)
     {
        WS05_Inquira.localeBean Local = new WS05_Inquira.localeBean();
        
        if(Criteria.size()>0)
            Local.isoCountry = Criteria[0];
        if(Criteria.size()>1)    
            Local.isoLanguage = Criteria[1].ToLowerCase();
                 
        WS05_Inquira.FaqServiceImplPort InquiraConnection = new WS05_Inquira.FaqServiceImplPort();
        InquiraConnection.endpoint_x = INQUIRA_ENDPOINT;
        InquiraConnection.timeout_x = TIMEOUT;
        
        String SearchText;          
        if(Keywords.size()>0)
            SearchText = KeyWords[0];
        else 
            SearchText = '';
        
        System.debug('#### Inquira Parameters #####');
        System.debug('#### Search Text: ' + SearchText);
        System.debug('#### Local.isoCountry: ' + Local.isoCountry);
        System.debug('#### Local: ' + Local);
        
        if(!Test.isRunningTest())                  
            Results = InquiraConnection.searchFaq(SearchText, Local.isoCountry, Local);
        
        System.debug('#### results: ' +  Results );
        
        List<InquiraFAQ__c> InquiraFAQs = new List<InquiraFAQ__c>();
       
        Utils_DataSource.Result ReturnedResult = new Utils_DataSource.Result();   
       
        if(Results == null)
        {
            ReturnedResult.RecordList = InquiraFAQs;
            return ReturnedResult;
        }    
               
        For(WS05_Inquira.faqBean FAQ:Results)
        {
             InquiraFAQ__c InquiraFAQ = new InquiraFAQ__c();
             InquiraFAQ.Name = FAQ.id;
             InquiraFAQ.Date__c = FAQ.date_x.date();
             InquiraFAQ.Excerpt__c = FAQ.Excerpt;
             InquiraFAQ.title__c = FAQ.Title;
             InquiraFAQ.URL__c = FAQ.url;
             InquiraFAQs.add(InquiraFAQ);   
        }
        
        ReturnedResult.RecordList = InquiraFAQs;
        
        return ReturnedResult;
     }
}