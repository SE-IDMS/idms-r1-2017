@isTest
global class WS_MOCK_SPA_TEST implements WebServiceMock 
{
   public string respType;
   public WS_MOCK_SPA_TEST(String responseType)
   {
       this.respType = responseType;
       System.debug('>>>>>>respType >>>>>'+respType );
   }
   
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
           {              
               System.debug('>>>>>>>>>>>>respType '+respType );
               if(respType == 'PosCommReference')
               {
                    System.debug('>>>>>PosCommReference>>>>');    
                   Ws_ProductandPricing.productAndPricingInformationResult Results = new Ws_ProductandPricing.productAndPricingInformationResult();                   
                   WS_ProductandPricing.productAndPricingInformationDataBean prodInfoBean = new WS_ProductandPricing.productAndPricingInformationDataBean();
                   prodInfoBean.catalogNumber = 'test';
                   prodInfoBean.localPricingGroup = 'test';
                   prodInfoBean.standardPrice = 12;
                   prodInfoBean.standardDiscount = 12;
                   prodInfoBean.standardMulitplier = 1;
                   prodInfoBean.listPrice = 12;
                   prodInfoBean.currencyCode = 'INR';
                   prodInfoBean.compensation = 13;
                   prodInfoBean.description = 'test';                   
                   WS_ProductandPricing.returnDataBean returnInfo = new WS_ProductandPricing.returnDataBean();
                   returnInfo.returnCode = 'TEST';
                   returnInfo.returnMessage= 'test';                   
                   results.productAndPricingInfo = prodInfoBean;
                   results.returnInfo = returnInfo;        
                   WS_ProductandPricing.getProductAndPricingInformationResponse resp = new WS_ProductandPricing.getProductAndPricingInformationResponse();        
                   resp.return_x = results;        
                   Map<String, WS_ProductandPricing.getProductAndPricingInformationResponse> response_map_x3 = new Map<String, WS_ProductandPricing.getProductAndPricingInformationResponse>();
                   response_map_x3.put('response_x', resp);                   
                   response.put('response_x', resp);         
               }
               else if(respType == 'NegCommReference')
               {   
                   System.debug('>>>>>NegCommReference>>>>');                
                   Ws_ProductandPricing.productAndPricingInformationResult Results = new Ws_ProductandPricing.productAndPricingInformationResult();                   
                   WS_ProductandPricing.productAndPricingInformationDataBean prodInfoBean = new WS_ProductandPricing.productAndPricingInformationDataBean();
                   prodInfoBean.catalogNumber = 'test';
                   prodInfoBean.localPricingGroup = 'test';
                   prodInfoBean.standardPrice = 12;
                   prodInfoBean.standardDiscount = 12;
                   prodInfoBean.standardMulitplier = 1;
                   prodInfoBean.listPrice = 12;
                   prodInfoBean.currencyCode = 'INR';
                   prodInfoBean.compensation = 13;
                   prodInfoBean.description = 'test';                   
                   WS_ProductandPricing.returnDataBean returnInfo = new WS_ProductandPricing.returnDataBean();
                   returnInfo.returnCode = 'E';
                   returnInfo.returnMessage= 'test';                   
                   results.productAndPricingInfo = prodInfoBean;
                   results.returnInfo = returnInfo;        
                   WS_ProductandPricing.getProductAndPricingInformationResponse resp = new WS_ProductandPricing.getProductAndPricingInformationResponse();        
                   resp.return_x = results;        
                   Map<String, WS_ProductandPricing.getProductAndPricingInformationResponse> response_map_x3 = new Map<String, WS_ProductandPricing.getProductAndPricingInformationResponse>();
                   response_map_x3.put('response_x', resp);                   
                   response.put('response_x', resp);             
               }
               else if(respType == 'PosProductPricing')
               {
                  System.debug('>>>>>PosProductPricing>>>>');
                   WS_ProductandPricing.returnDataBean returnInfo = new WS_ProductandPricing.returnDataBean();
                   returnInfo.returnCode = 'TE';
                   returnInfo.returnMessage= 'test';                   
                   WS_ProductandPricing.localPricingGroupInformationDataBean localPricingGroupInfo1 = new WS_ProductandPricing.localPricingGroupInformationDataBean();
                   localPricingGroupInfo1.compensation = 89;
                   localPricingGroupInfo1.standardDiscount = 37.0;
                   localPricingGroupInfo1.description= 'testDesc';
                   localPricingGroupInfo1.standardMultiplier=1;        
                   WS_ProductandPricing.localPricingGroupInformationResult sample = new WS_ProductandPricing.localPricingGroupInformationResult();
                   sample.localPricingGroupInfo = localPricingGroupInfo1;
                   sample.returnInfo = returnInfo;                   
                   WS_ProductandPricing.getLocalPricingGroupInformationResponse respElement = 
                   new WS_ProductandPricing.getLocalPricingGroupInformationResponse();
                   Map<String, WS_ProductandPricing.getLocalPricingGroupInformationResponse> response_map_x = new Map<String, WS_ProductandPricing.getLocalPricingGroupInformationResponse>();                   
                   respElement.return_x = sample;           
                   response_map_x.put('response_x', respElement);        
                   response.put('response_x', respElement);                    
               }
               else if(respType == 'NegProductPricing')
               {
                   System.debug('>>>>>NegProductPricing>>>>');
                   WS_ProductandPricing.returnDataBean returnInfo = new WS_ProductandPricing.returnDataBean();
                   returnInfo.returnCode = 'E';
                   returnInfo.returnMessage= 'test';                   
                   WS_ProductandPricing.localPricingGroupInformationDataBean localPricingGroupInfo1 = new WS_ProductandPricing.localPricingGroupInformationDataBean();
                   localPricingGroupInfo1.compensation = 89;
                   localPricingGroupInfo1.standardDiscount = 37.0;
                   localPricingGroupInfo1.description= 'testDesc';
                   localPricingGroupInfo1.standardMultiplier=1;        
                   WS_ProductandPricing.localPricingGroupInformationResult sample = new WS_ProductandPricing.localPricingGroupInformationResult();
                   sample.localPricingGroupInfo = localPricingGroupInfo1;
                   sample.returnInfo = returnInfo;                   
                   WS_ProductandPricing.getLocalPricingGroupInformationResponse respElement = 
                   new WS_ProductandPricing.getLocalPricingGroupInformationResponse();
                   Map<String, WS_ProductandPricing.getLocalPricingGroupInformationResponse> response_map_x = new Map<String, WS_ProductandPricing.getLocalPricingGroupInformationResponse>();                   
                   respElement.return_x = sample;           
                   response_map_x.put('response_x', respElement);        
                   response.put('response_x', respElement);                   
               }
               else if(respType == 'PosMultipleProducts')
               {
                   Ws_ProductandPricing.multipleProductAndPricingInformationResult Results = new Ws_ProductandPricing.multipleProductAndPricingInformationResult();                  
                   WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean prodInfoBean = new WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean();
                   List<WS_ProductandPricing.productAndPricingInformationResult> productAndPricingInfoList1 = new List<WS_ProductandPricing.productAndPricingInformationResult>();
                   WS_ProductandPricing.productAndPricingInformationResult prodPricInfo = new WS_ProductandPricing.productAndPricingInformationResult();
                   WS_ProductandPricing.productAndPricingInformationDataBean productAndPricingInfo1 = new WS_ProductandPricing.productAndPricingInformationDataBean();
                   List<WS_ProductandPricing.productAndPricingInformationResult> resultList = new List<WS_ProductandPricing.productAndPricingInformationResult>();
                   productAndPricingInfo1.catalogNumber = 'test';
                   productAndPricingInfo1.localPricingGroup = 'test';
                   productAndPricingInfo1.standardPrice = 12;
                   productAndPricingInfo1.standardDiscount = 12;
                   productAndPricingInfo1.standardMulitplier = 1;
                   productAndPricingInfo1.listPrice = 12;
                   productAndPricingInfo1.currencyCode = 'INR';
                   productAndPricingInfo1.compensation = 13;
                   productAndPricingInfo1.description = 'test';                   
                   WS_ProductandPricing.returnDataBean returnInfo = new WS_ProductandPricing.returnDataBean();
                   returnInfo.returnCode = 'S';
                   returnInfo.returnMessage= 'test';                   
                   prodPricInfo.productAndPricingInfo = productAndPricingInfo1;
                   prodPricInfo.returnInfo= returnInfo;
                   productAndPricingInfoList1.add(prodPricInfo);                   
                   results.productAndPricingInfoList = productAndPricingInfoList1;
                   results.returnInfo = returnInfo;                   
                   WS_ProductandPricing.getMultipleProductAndPricingInformationResponse resp = new WS_ProductandPricing.getMultipleProductAndPricingInformationResponse();
                   resp.return_x = results;
                   Map<String, WS_ProductandPricing.getMultipleProductAndPricingInformationResponse> response_map_x3 = new Map<String, WS_ProductandPricing.getMultipleProductAndPricingInformationResponse>();                   
                   response_map_x3.put('response_x', resp);                   
                   response.put('response_x', resp);       
               }
               else if(respType == 'NegMultipleProducts')
               {
                   Ws_ProductandPricing.multipleProductAndPricingInformationResult Results = new Ws_ProductandPricing.multipleProductAndPricingInformationResult();                  
                   WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean prodInfoBean = new WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean();
                   List<WS_ProductandPricing.productAndPricingInformationResult> productAndPricingInfoList1 = new List<WS_ProductandPricing.productAndPricingInformationResult>();
                   WS_ProductandPricing.productAndPricingInformationResult prodPricInfo = new WS_ProductandPricing.productAndPricingInformationResult();
                   WS_ProductandPricing.productAndPricingInformationDataBean productAndPricingInfo1 = new WS_ProductandPricing.productAndPricingInformationDataBean();
                   List<WS_ProductandPricing.productAndPricingInformationResult> resultList = new List<WS_ProductandPricing.productAndPricingInformationResult>();
                   productAndPricingInfo1.catalogNumber = 'test';
                   productAndPricingInfo1.localPricingGroup = 'test';
                   productAndPricingInfo1.standardPrice = 12;
                   productAndPricingInfo1.standardDiscount = 12;
                   productAndPricingInfo1.standardMulitplier = 1;
                   productAndPricingInfo1.listPrice = 12;
                   productAndPricingInfo1.currencyCode = 'INR';
                   productAndPricingInfo1.compensation = 13;
                   productAndPricingInfo1.description = 'test';                   
                   WS_ProductandPricing.returnDataBean returnInfo = new WS_ProductandPricing.returnDataBean();
                   returnInfo.returnCode = 'E';
                   returnInfo.returnMessage= 'test';                   
                   prodPricInfo.productAndPricingInfo = productAndPricingInfo1;
                   prodPricInfo.returnInfo= returnInfo;
                   productAndPricingInfoList1.add(prodPricInfo);                   
                   results.productAndPricingInfoList = productAndPricingInfoList1;
                   results.returnInfo = returnInfo;                   
                   WS_ProductandPricing.getMultipleProductAndPricingInformationResponse resp = new WS_ProductandPricing.getMultipleProductAndPricingInformationResponse();
                   resp.return_x = results;
                   Map<String, WS_ProductandPricing.getMultipleProductAndPricingInformationResponse> response_map_x3 = new Map<String, WS_ProductandPricing.getMultipleProductAndPricingInformationResponse>();                   
                   response_map_x3.put('response_x', resp);                   
                   response.put('response_x', resp);       
               }
      }
}