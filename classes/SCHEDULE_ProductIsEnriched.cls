global class SCHEDULE_ProductIsEnriched implements Schedulable{
	 global void execute(SchedulableContext ctx) {
        Batch_ProductIsEnriched batch = new Batch_ProductIsEnriched();
      	ID batchprocessid = Database.executeBatch(batch);
    }
}