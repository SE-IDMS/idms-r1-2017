/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Created Date: 26/09/2014                                  *
************************************************************/
public with sharing class Fielo_ConvertToMemberController{

    public String message {get;set;}
    public String success {get;set;}
    public FieloEE__Member__c member {get;set;}

    private Contact con;
    
            
    public Fielo_ConvertToMemberController(ApexPages.StandardController controller){

        List<String> fields = new List<String>{'FirstName','AccountId','Email','JobTitle__c','JobFunc__c','Country__c','FieloEE__Member__c'};
        controller.addFields(fields);

        con = (Contact)controller.getRecord();
        
    }
    
    public void doConvert(){

        message = '';
        success = '';

        if(con.FieloEE__Member__c != null){
            message = 'Member already exists for the Contact. ';
            member = new FieloEE__Member__c(Id = con.FieloEE__Member__c);
            return;
        }

        Account acc;
                       
        if(con.AccountId != null){
            acc = [SELECT Id, Name, Phone, Street__c, AdditionalAddress__c, City__c, POBox__c, 
                           CorporateHeadquarters__c, ClassLevel1__c, ClassLevel2__c, ZipCode__c
                           FROM Account WHERE Id =: con.AccountId];
            if(con.FirstName == null){
                message += '\n First Name for Contact is requested. ';
            }
            if(con.LastName == null){
                message += '\n Last Name for Contact is requested. ';
            }
            if(con.Email == null){
                message += '\n Email for Contact is requested. ';
            }
            if(acc.ClassLevel1__c != 'RT' && acc.ClassLevel1__c != 'SC'){
                message += '\n Business Type Installer or Retailer is requested for Account. ';
            }
        }else{
            message = '\n Contact must have a parent Account. ';
        }

        if(message == ''){
            member = new FieloEE__Member__c(            
                F_Title__c                 = con.Salutation,
                FieloEE__FirstName__c      = con.FirstName,
                FieloEE__LastName__c       = con.LastName,
                FieloEE__Email__c          = con.Email,
                F_JobTitle__c              = con.JobTitle__c,
                F_JobFunction__c           = con.JobFunc__c,
                F_Country__c               = con.Country__c,
                F_CompanyName__c           = acc.Name,
                F_CompanyPhoneNumber__c    = acc.Phone,
                F_Address1__c              = acc.Street__c,
                F_NearLandmark__c          = acc.AdditionalAddress__c,
                F_City__c                  = acc.City__c,
                F_POBox__c                 = acc.POBox__c,
                F_CorporateHeadquarters__c = acc.CorporateHeadquarters__c,
                F_BusinessType__c          = acc.ClassLevel1__c,
                F_AreaOfFocus__c           = acc.ClassLevel2__c,
                FieloEE__Approved__c       = true
            );
            try{
                Fielo_ContactTriggers.bypassAccountCreation = true;
                insert member;
                List<Contact> listContacts = [SELECT Id FROM Contact WHERE FieloEE__Member__c =: member.Id LIMIT 1];
                if(!listContacts.isEmpty()){
                    delete listContacts[0];
                }
                con.FieloEE__Member__c = member.Id;
                update con;
                message = '';
                success = 'Member created succesfully. ';
            }catch(DMLException e){
                message = 'Error: ' + e.getMessage();
            }
        }else{
            message = 'Member was not created due to the following:' + message;
        }
                            
    }
    
    public PageReference doGoToContact(){
        return new PageReference('/' + con.Id);
    }   

    public PageReference doGoToMember(){
        if(member.Id != null){
            return new PageReference('/' + member.Id);
        }else{
            return null;
        }
    }   
        
}