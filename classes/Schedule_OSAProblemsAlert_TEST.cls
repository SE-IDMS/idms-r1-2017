@isTest
public class Schedule_OSAProblemsAlert_TEST{

    public static testMethod void CreateData(){
    
        set<Problem__c> setprob1 = new  set<Problem__c>();
        set<ID> setProbId = new set<ID>();
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');  
           newUser2.BypassVR__c = TRUE;
           newUser2.BypassTriggers__c = 'AP1000';
           newUser2.UserRoleID = rol_ID ;
        Database.insert(newUser2);
       Test.startTest();            
       System.runas(newUser2) {
           BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
           insert brEntity;
           BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
           insert brSubEntity;
           BusinessRiskEscalationEntity__c AccOrg = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
           insert AccOrg;
             
           List<Problem__c> lsprob = new List<Problem__c>();
             
           Problem__c prob = Utils_TestMethods.createProblem(AccOrg.Id,12);
           prob.ProblemLeader__c = newUser2.id;
           prob.ProductQualityProblem__c = false;
           prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
           prob.RecordTypeid = Label.CLI2PAPR120014;
           prob.Sensitivity__c = 'Public';
           prob.Severity__c = 'Safety related';
           prob.ActualUserLastmodifiedDate__c = system.now() - 30; 
           prob.TECH_ReminderNoUpdateDate__c = null;
           lsprob.add(prob);
           setProbId.add(prob.Id);
           
           Problem__c prob1 = Utils_TestMethods.createProblem(AccOrg.Id,12);
           prob1.ProblemLeader__c = newUser2.id;
           prob1.ProductQualityProblem__c = false;
           prob1.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
           prob1.RecordTypeid = Label.CLI2PAPR120014;
           prob1.Sensitivity__c = 'Public';
           prob1.Severity__c = 'Safety related';
           prob1.ActualUserLastmodifiedDate__c = system.now() - 30; 
           prob1.TECH_ReminderNoUpdateDate__c = System.now() - 7;
           lsprob.add(prob1);
           setProbId.add(prob1.Id);
           
           Database.insert(lsprob);

           System.debug('--------------->'+lsprob);

    }    
        
        Schedule_OSAProblemsAlert batch_Prob = new Schedule_OSAProblemsAlert(); 
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_Prob,1000);
      Test.stopTest();
    }
    
    static testMethod void myUnitTest_schedule() {
        Test.startTest();
            String CRON_EXP = '0 0 0 1 1 ? 2025';  
            String jobId = System.schedule('Schduel Schedule_OSAProblemsAlert', CRON_EXP, new Schedule_OSAProblemsAlert() );
        Test.stopTest();
    }
}