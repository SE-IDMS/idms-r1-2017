@isTest(seealldata=true)
public class AP_PartsOrderLine_Test {

	static List<Country__c> countryList = new List<Country__c>();
	static Country__c country1;
	static List<StateProvince__c> stateProvinceList = new List<StateProvince__c>();
	static StateProvince__c stateCountry1_01;
	static StateProvince__c stateCountry1_02;
	
	static User planner01;
	
	static Account acc1;
	static Contact contact1;
	static SVMXC__Site__c site;
	
	static SVMXC__Service_Order__c workOrder;

	static SVMXC__Installed_Product__c ip;
	
	static {	
        country1 = Utils_TestMethods.createCountry();
        country1.name='France';
        country1.CountryCode__c = 'FR';
        countryList.add(country1);
        
        upsert countryList CountryCode__c;
        
        // Create States/Provinces
        
        stateCountry1_01 = new StateProvince__c(Name='FR - Paris', Country__c=country1.Id, CountryCode__c=country1.CountryCode__c, StateProvinceCode__c='75');
        stateProvinceList.add(stateCountry1_01);
        stateCountry1_02 = new StateProvince__c(Name='FR - Hauts-de-Seine', Country__c=country1.Id, CountryCode__c=country1.CountryCode__c, StateProvinceCode__c='92');
        stateProvinceList.add(stateCountry1_02);
        
        System.debug('#### Inserting stateProvinceList: '+stateProvinceList);
        insert stateProvinceList;
        System.debug('#### Inserted stateProvinceList: '+stateProvinceList);
        
        planner01 = Utils_TestMethods.createStandardUser('polusr');
        insert planner01;
        
        
        acc1 = Utils_TestMethods.createAccount();
        acc1.country__c = country1.Id;
        acc1.PostalStateProvince__c = stateCountry1_01.Id;
        acc1.Name = 'Account FR 01';
        acc1.City__c = 'Paris';
        acc1.PostalStateProvince__c = stateCountry1_01.Id;
        acc1.StateProvince__c = stateCountry1_01.Id;
        acc1.BillingCity = acc1.City__c;
        insert acc1;
        
        contact1 = Utils_TestMethods.CreateContact(acc1.Id,'TContact');
        insert contact1;

        site = new SVMXC__Site__c();
        site.Name = 'Test Location12';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc1.id;
        site.TimeZone__c = 'Test';
        site.PrimaryLocation__c = true;
        insert site;

        //Installed prdoduct
        
        List <SVMXC__Installed_Product__c> ipList = new List<SVMXC__Installed_Product__c>();

        ip = new SVMXC__Installed_Product__c();
        ip.SVMXC__Company__c = acc1.id;
        ip.Name = 'Test Intalled Product IP0';
        ip.SVMXC__Status__c= 'new';
        ip.GoldenAssetId__c = 'GoldenAssertId0';
        ip.BrandToCreate__c ='Test Brand';
        ip.DeviceTypeToCreate__c ='Test Device Type';
        ip.SVMXC__Site__c = site.id;
        ip.SVMXC__Serial_Lot_Number__c = 'VIS09032016';
        ipList.add(ip);
        
        insert ipList;
        
        workOrder =  Utils_TestMethods.createWorkOrder(acc1.id);
        workOrder.Work_Order_Category__c='On-site';                 
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.BackOfficeReference__c = '111111';
        workOrder.IsBillable__c = 'No';
        workOrder.Comments_to_Planner__c='testing';
        workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
        workOrder.SVMXC__Site__c = site.Id;
        workOrder.EarliestAvailabilityDate__c = Date.today()+7;
        workOrder.PlannerContact__c = planner01.Id;
        insert workOrder;
        
	}
	
	
	
    static testMethod void myUnitTest2() {
        
        /*
        SVMXC__Service_Order__c workOrder =  new SVMXC__Service_Order__c();
        workOrder.Work_Order_Category__c='On-site';                 
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c ='Scheduled';
        workOrder.CustomerTimeZone__c ='Asia/Jakarta';
        insert workOrder;
        Country__c countrySobj =Utils_TestMethods.createCountry();
        countrySobj.CountryCode__c ='00'; 
        //insert countrySobj ;
        Account accountSobj = Utils_TestMethods.createAccount();
        accountSobj.SEAccountID__c ='SEAccountID';
        accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
        accountSobj.Country__c= countrySobj.id;
        // insert accountSobj;
        ip = new SVMXC__Installed_Product__c();
        //ip.SVMXC__Company__c = accountSobj.id;
        ip.Name = 'Test Installed Product1';
        ip.SVMXC__Status__c= 'new';
        ip.GoldenAssetId__c = 'TEST1';
        ip.BrandToCreate__c ='Test Brand';
        ip.CustomerSerialNumber__c = 'SerialNumber';
        //ip.SVMXC__Product__c = prod.id;
        ip.SVMXC__Serial_Lot_Number__c = 'VIS09032016';
        insert ip;
        */

        Test.startTest();

        //SVMXC__RMA_Shipment_Order__c po = new SVMXC__RMA_Shipment_Order__c(recordtypeid='012A0000000nYLQ',SVMXC__Service_Order__c=workOrder.id);
        SVMXC__RMA_Shipment_Order__c po = createPartsOrder(workOrder, null, acc1, acc1, null, '012A0000000nYLQ');
        insert po;
        SVMXC__RMA_Shipment_Line__c poline= new SVMXC__RMA_Shipment_Line__c(Generic_reference__c='Generic Ref', SVMXC__Serial_Number__c=ip.id,CommercialReference__c='test',recordtypeid='012A0000000nphZ',SVMXC__RMA_Shipment_Order__c=po.id,Shipped_Qty__c=11);
        insert poline;
        poline.Shipped_Qty__c=12;
        poline.Create_RMA_Line__c=true;
        poline.Lead_Date__c = Date.today() + 10;
        update poline;
        List<SVMXC__RMA_Shipment_Line__c> polList = new List<SVMXC__RMA_Shipment_Line__c>();
        polList.add(poline);
        AP_PartsOrderLine.SendNotificationToPlanner(pollist);
        Test.stopTest();
    }
    
    /*
    static testMethod void myUnitTest() {
        
        List<SVMXC__RMA_Shipment_Order__c> polist= new  list<SVMXC__RMA_Shipment_Order__c>();
        List<SVMXC__RMA_Shipment_Line__c> Polinelist = new list<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__RMA_Shipment_Line__c> Polinelist1 = new list<SVMXC__RMA_Shipment_Line__c>();
        //try{
        //SVMXC__RMA_Shipment_Order__c po = new SVMXC__RMA_Shipment_Order__c(recordtypeid='012A0000000nYLQ',Parts_Order_RMA__c=null);
        SVMXC__RMA_Shipment_Order__c po = createPartsOrder(null, null, acc1, acc1, null, '012A0000000nYLQ');
        insert po;
        polist.add(po);
        Test.startTest();
        SVMXC__RMA_Shipment_Line__c poLine= new SVMXC__RMA_Shipment_Line__c(CommercialReference__c='test',recordtypeid='012A0000000nphZ',Create_RMA_Line__c =true,SVMXC__RMA_Shipment_Order__c=po.id);
        //insert poline;
        poLineList.add(poLine);
        SVMXC__RMA_Shipment_Line__c poLine1 = new SVMXC__RMA_Shipment_Line__c(CommercialReference__c='test',recordtypeid='012A0000000nphZ',Create_RMA_Line__c = false,SVMXC__RMA_Shipment_Order__c=po.id);
        //insert poline1;
        poLine1.Create_RMA_Line__c= true;
        poLine1.CommercialReference__c='test123';
        //update poline1;
        poLineList.add(poLine1);
        AP_Partsorderline.POlineCreation(polist,Polinelist);
        Test.stopTest();
        //  }
        //catch (exception e){}
        
    }
    */
    
    //added by suhail for june 2016 release br-9102 start
    public static testmethod void ProductQualityAlert()
    {
        //creating data 
        //country
        
        //Country__c country = [select name,ID,CountryCode__c from country__c where countrycode__c ='FR'];
        
        // Product Quality Alert
        boolean inserted = false;
        Product_Quality_Alert__c pqa1 = new Product_Quality_Alert__c();
        pqa1.Alert_Type__c = 'Request For Analysis'; 
        pqa1.ReturnReason__c = 'Request For Analysis Direct Shipment';
        pqa1.Start_Date__c = System.TODAY()-10 ;
        pqa1.End_Date__c = System.TODAY()+10;
        insert pqa1; 
        //product
        product2 proda = new product2();
        proda.Name = 'testProductA';
        insert proda;
        //Target Product
        Target_Products__c tp1 = new Target_Products__c();
        tp1.Product__c = proda.id;
        tp1.Product_Quality_Alert__c = pqa1.id;
        insert tp1;
        //organization
        BusinessRiskEscalationEntity__c org = new BusinessRiskEscalationEntity__c();
        org.Name = 'TEST';
        org.Entity__c = 'Testing';
        org.RCCountry__c = country1.id;
        insert org;
        
        
        
        
        
        //plant
        Plant__c plant = new plant__C();
        plant.Name = 'TEST PLANT';
        Plant.Country__c = country1.id;
        insert plant;
        // covered org           
        CoveredOrganization__c covorg = new CoveredOrganization__c();
        covorg.Product_Quality_Alert__c = pqa1.id;
        covorg.Plant__c = Plant.id;
        covorg.Organization__c = org.id ;
        insert covorg;

		/*
        //account
        Account acc = new account();
        acc = Utils_TestMethods.createAccount();
        acc.Country__c = country1.id;
        //insert acc;
        
        
        
        //contact
        // Create Contact to the Account
        Contact Cont=Utils_TestMethods.CreateContact(acc.Id,'TContact');
        try{
            insert Cont;
        }catch(exception e){}
        //work order cc
        SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c();
        workOrder.SVMXC__Company__c = acc.id ;
        insert workOrder;
        */
        
        Test.startTest();
        //parts order 
        SVMXC__RMA_Shipment_Order__c porder = new SVMXC__RMA_Shipment_Order__c();
        porder.SVMXC__Order_Status__c = 'Open';
        porder.SVMXC__Service_Order__c = workOrder.id;//cc
        porder.SVMXC__Company__c = acc1.id;
        porder.SVMXC__Contact__c = contact1.id;
        porder.Ship_to__c = acc1.Id;
        //parts order 2
        SVMXC__RMA_Shipment_Order__c porder2 = new SVMXC__RMA_Shipment_Order__c();
        porder2.SVMXC__Order_Status__c = 'Open';
        porder2.SVMXC__Service_Order__c = workOrder.id;//cc
        porder2.SVMXC__Company__c = acc1.id;
        porder2.SVMXC__Contact__c = contact1.id;
        porder2.Ship_to__c = acc1.Id;
        porder2.acknowledege__c = true;
        //partsorder line 
        SVMXC__RMA_Shipment_Line__c porderline = new SVMXC__RMA_Shipment_Line__c();
        porderline.PQAnumber__c = pqa1.id;
        porderline.SVMXC__Product__c = proda.id;
        
        //partsorder line 2
        SVMXC__RMA_Shipment_Line__c porderline2 = new SVMXC__RMA_Shipment_Line__c();
        porderline2.PQAnumber__c = pqa1.id;
        porderline2.SVMXC__Product__c = proda.id;
        
        //inserting 
        insert porder;
        insert porder2;
        porderline.SVMXC__RMA_Shipment_Order__c = porder.id;
        porderline2.SVMXC__RMA_Shipment_Order__c = porder2.id;
        System.debug(porderline2.acknowledgeOnPartOrder__c);
        insert porderline2;
        try{
            
            insert porderline;
            system.debug(porderline);
        }
        
        catch(DmlException e){
            System.debug('#### The following exception has occurred: ' + e.getMessage());
            porder.Acknowledege__c = true;
            update porder;
            system.debug(porderline);
            system.debug(porder);  
            insert porderline;
        }
        Test.stopTest();
    }
    
    static private SVMXC__RMA_Shipment_Order__c createPartsOrder(SVMXC__Service_Order__c aWorkOrder, String aStatus, Account anAccount, Account aShipTo, Contact aContact, String aRecordTypeID) {
    	SVMXC__RMA_Shipment_Order__c result = new SVMXC__RMA_Shipment_Order__c();
		if (aWorkOrder != null) {
			result.SVMXC__Service_Order__c = aWorkOrder.id;
		}
    	if (aStatus != null) {
        	result.SVMXC__Order_Status__c = aStatus;
    	}
    	if (anAccount != null) {
        	result.SVMXC__Company__c = anAccount.id;
    	}
    	if (aContact != null) {
        	result.SVMXC__Contact__c = aContact.id;
    	}
    	if (aShipTo != null) {
        	result.Ship_to__c = aShipTo.Id;
    	}
    	else {
    		result.From_Name__c = 'From someone';
    		result.SVMXC__Source_Street__c = 'Main Street';
    		result.SVMXC__Source_City__c = 'Metropolis';
    		result.From_State__c  = stateCountry1_01.Id;
    		result.From_Country__c  = country1.Id;
    	}
    	if (aRecordTypeID != null) {
    		result.recordtypeid = aRecordTypeID;
    	}
    	return result;
    }
    
}