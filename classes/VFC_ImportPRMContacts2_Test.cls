@isTest
public class VFC_ImportPRMContacts2_Test{
    

    static testMethod void testMethodOne(){
        CS_PRM_ApexJobSettings__c jobSet =new CS_PRM_ApexJobSettings__c(Name='IMPORT_PRM_CNT_TMPL_ID',InitialSync__c=True,NumberValue__c=2);
        Insert jobSet;
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
            INSERT testCountry;
            
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince;
    
        AP_UserRegModel userRegModel = new AP_UserRegModel();
        userRegModel.companyStateProvince = testStateProvince.Id;
        userRegModel.companyCountry = testCountry.Id;
        userRegModel.phoneType = 'Mobile';
        
        List<AP_UserRegModel> regLst = new List<AP_UserRegModel>();
        regLst.add(userRegModel);
    
        VFC_ImportPRMContacts2.ImportPRMContacts(regLst);
        VFC_ImportPRMContacts2.GetNewBatchImportId();
        VFC_ImportPRMContacts2.GetActiveChannels(new List<String>{'test'});
        VFC_ImportPRMContacts2 icnt2= new VFC_ImportPRMContacts2();
        icnt2.getOriginSourceOptions();
    
    }
    
    static testMethod void testMethodTwo(){
        CS_PRM_ApexJobSettings__c jobSet =new CS_PRM_ApexJobSettings__c(Name='IMPORT_PRM_CNT_TMPL_ID',InitialSync__c=True,NumberValue__c=2);
        Insert jobSet;
        Country__c TestCountry = new Country__c (Name='USA', CountryCode__c='US'); 
            INSERT TestCountry;
            StateProvince__c testSt = new StateProvince__c(Name = 'New York', Country__c = TestCountry.Id, StateProvinceCode__c = 'NY', CountryCode__c='US', StateProvinceExternalId__c = 'NYC');
            INSERT testSt;
            
            
        
            Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', City__c = 'New York',
                                    Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMCountry__c=TestCountry.id,PRMBusinessType__c='FI',
                                    PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, StateProvince__c = testSt.Id);
            INSERT PartnerAcc;
          
            Contact PartnerCon = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  Country__c=TestCountry.Id,
                  PRMFirstName__c = 'Test',
                  PRMLastName__c = 'lastname',
                  PRMEmail__c = 'test.lastName@yopmail.com',
                  PRMWorkPhone__c = '999121212',
                  PRMPrimaryContact__c = true
                  );
                  
            INSERT PartnerCon;
    
        AP_UserRegModel userRegModel = new AP_UserRegModel();
        userRegModel.companyStateProvince = testSt.Id;
        userRegModel.companyCountry = testCountry.Id;
        userRegModel.phoneType = 'Mobile';
        userRegModel.accountId = PartnerAcc.Id;
        userRegModel.contactId = PartnerCon.Id;
        
        List<AP_UserRegModel> regLst = new List<AP_UserRegModel>();
        regLst.add(userRegModel);
    
        VFC_ImportPRMContacts2.ImportPRMContacts(regLst);
        VFC_ImportPRMContacts2.GetNewBatchImportId();
        VFC_ImportPRMContacts2.GetActiveChannels(new List<String>{'test'});
        VFC_ImportPRMContacts2 icnt2= new VFC_ImportPRMContacts2();
        icnt2.getOriginSourceOptions();
    
    }
}