/**
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 13/12/2010 
    Description     : Utility class used by Test Methods related to Quote Approval.
*/
public with sharing class RIT_Utils_TestMethods
{ 
   //Creates Account
   public static Account createAccount()
    {
        Account acc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX');
        return acc;
    }
    //Creates Opportunity
    public static Opportunity createOpportunity(Id accountId)
    {
        Opportunity opp = new Opportunity(Name='Test Opp',  AccountId=accountId, StageName='0 - Closed',Reason__c='testReason', CloseDate=Date.today(), LeadingBusiness__c='BU');
        return opp;
    }
    //Creates QuoteLink
    public static OPP_QuoteLink__c createQuoteLink(Id oppId)
    {
        OPP_QuoteLink__c quoteLink = new OPP_QuoteLink__c(OpportunityName__c = oppId);
        return quoteLink;
    }
    //Creates Financial Environment
    public static RIT_FinancialEnv__c createFinEnv(Id quoteLinkId)
    {
        RIT_FinancialEnv__c finEnv = new RIT_FinancialEnv__c(QuoteLink__c = quoteLinkId);
        return finEnv;
    }
    //Creates Project Information with SE Member of Consortium
    public static RIT_ProjectInformation__c createProjInfowithConsort(Id quoteLinkId)
    {
        RIT_ProjectInformation__c projInfo = new RIT_ProjectInformation__c(QuoteLink__c = quoteLinkId, SEMemberOfAConsortium__c = 'Yes');
        return projInfo;
    }    

    //Creates Project Information
    public static RIT_ProjectInformation__c createProjInfo(Id quoteLinkId)
    {
        RIT_ProjectInformation__c projInfo = new RIT_ProjectInformation__c(QuoteLink__c = quoteLinkId);
        return projInfo;
    }    

    //Creates Contractual Environment
    public static RIT_ContractualEnv__c createConEnv(Id quoteLinkId)
    {
        RIT_ContractualEnv__c conEnv  = new RIT_ContractualEnv__c(QuoteLink__c = quoteLinkId);
        return conEnv ;
    }
    //Creates Techical Manufacturing Scope
    public static RIT_TechManScope__c createTechManf(Id quoteLinkId)
    {
        RIT_TechManScope__c techManf  = new RIT_TechManScope__c(QuoteLink__c = quoteLinkId);
        return techManf ;
    }
    //Creates Commercial & Competitive Environment
    public static RIT_CommCompEnv__c createCommComp(Id quoteLinkId)
    {
        RIT_CommCompEnv__c commComp= new RIT_CommCompEnv__c(QuoteLink__c = quoteLinkId);
        return commComp;
    }
    //Creates Liquidated Damages
    public static RIT_LiquidatedDamages__c createLiqDam(Id contrEnvId)
    {
        RIT_LiquidatedDamages__c liqDam= new RIT_LiquidatedDamages__c(ContractualEnvironment__c = contrEnvId);
        return liqDam;
    }
    //Creates Bank Guarantees
    public static RIT_BankGuarantee__c createBankGuar(Id finEnvId)
    {
        RIT_BankGuarantee__c bankGuar = new RIT_BankGuarantee__c(FinancialEnvironment__c = finEnvId);
        return bankGuar;        
    }
    //Creates Project Management
    public static RIT_ProjectManagement__c createProjMgnt(Id quoteLinkId)
    {
        RIT_ProjectManagement__c projMgnt = new RIT_ProjectManagement__c(QuoteLink__c = quoteLinkId);
        return projMgnt;        
    }
    //Creates Budget
    public static RIT_Budget__c createBudget(Id quoteLinkId)
    {
        RIT_Budget__c budget = new RIT_Budget__c(QuoteLink__c = quoteLinkId);
        return budget;        
    } 
    //create BudgetLine
    public static RIT_BudgetLine__c createBudgetLine(Id budgetId,String businessUnit)
    {
        RIT_BudgetLine__c budgetLine=new RIT_BudgetLine__c(Budget__c=budgetId,BusinessUnit__c=businessUnit);
        return budgetLine;
    }


    //Creates Sub Contracters
    public static RIT_SubContractOGSupp__c createSubContr(Id quoteLinkId)
    {
        RIT_SubContractOGSupp__c subContract= new RIT_SubContractOGSupp__c(QuoteLink__c = quoteLinkId);
        return subContract;        
    }    
    
    static Id projMgntRecTypeId = [select Id from RecordType where Name like '%Project Management%'].Id;
    static Id finEnvRecTypeId = [select Id from RecordType where Name like '%Financial Information%'].Id;
    static Id techManfRecTypeId = [select Id from RecordType where Name like '%Technical Manufacturing Scope%'].Id;
    static Id subContRecTypeId = [select Id from RecordType where Name like '%Sub Contractors – OG Suppliers%'].Id;
    static Id commCompRecTypeId = [select Id from RecordType where Name like '%Commercial and Competitive Env%'].Id;
    static Id contEnvRecTypeId = [select Id from RecordType where Name like '%Contractual Environment%'].Id;
    
        
    //Creates Project Management Risks
    public static RIT_Risks__c createProjMgntRisks(Id quoteLinkId,Id projMgntRiskId)
    {
        RIT_Risks__c risk = new RIT_Risks__c(QuoteLink__c = quoteLinkId,ProjectManagementRisks__c = projMgntRiskId , RecordTypeId = projMgntRecTypeId);
        return risk;        
    } 
    //Creates Comm & comp Risks
    public static RIT_Risks__c createCommCompRisks(Id quoteLinkId,Id commCompRiskId)
    {
        RIT_Risks__c risk = new RIT_Risks__c(QuoteLink__c = quoteLinkId,CommercialAndCompetitiveEnvRisks__c = commCompRiskId, RecordTypeId = commCompRecTypeId);
        return risk;        
    } 
    //Creates Technical & Manufacturing Risks
    public static RIT_Risks__c createTechManfRisks(Id quoteLinkId,Id techManfRiskId)
    {
        RIT_Risks__c risk = new RIT_Risks__c(QuoteLink__c = quoteLinkId,TechnicalManufacturingRisks__c = techManfRiskId, RecordTypeId = techManfRecTypeId);
        return risk;        
    } 

    //Creates Sub Contracters Risks
    public static RIT_Risks__c createSubContractersRisks(Id quoteLinkId,Id subContRiskId)
    {
        RIT_Risks__c risk = new RIT_Risks__c(QuoteLink__c = quoteLinkId,SubContractorsAndOGSuppRisks__c = subContRiskId, RecordTypeId = subContRecTypeId);
        return risk;        
    } 
    
    //Creates Financial Env Risks
    public static RIT_Risks__c createFinEnvRisks(Id quoteLinkId,Id finEnvRiskId)
    {
        RIT_Risks__c risk = new RIT_Risks__c(QuoteLink__c = quoteLinkId,FinancialRisks__c = finEnvRiskId, RecordTypeId = finEnvRecTypeId);
        return risk;        
    } 

    //Creates Contractual Env Risks
    public static RIT_Risks__c createConEnvRisks(Id quoteLinkId,Id conEnvRiskId)
    {
        RIT_Risks__c risk = new RIT_Risks__c(QuoteLink__c = quoteLinkId,ContractualEnvironmentalRisks__c = conEnvRiskId, RecordTypeId = contEnvRecTypeId);
        return risk;        
    } 

    //Creates Terms and Means of Payment
    public static RIT_TermsAndMeansOfPayment__c createTermsAndMeans(Id finEnvId)
    {
        RIT_TermsAndMeansOfPayment__c termsAndMeans = new RIT_TermsAndMeansOfPayment__c(FinancialEnvironment__c = finEnvId, MileStone__c='Down Payment');
        return termsAndMeans;        
    }    
    //Creates Notes
    public static Note createNotes(Id parentId)
    {
        Note note = new Note(Title='test', Body='test', parentId =parentId);
        return note;
    }
    
    //Creates Budget History
    public static RIT_Budget__History createBudgetHistory(Id parentId, String field)
    {
        RIT_Budget__History budHist = new RIT_Budget__History(ParentId = parentId, Field=field);
        return budHist ;
    }    
}