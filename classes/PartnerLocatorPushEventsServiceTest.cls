@IsTest
public class PartnerLocatorPushEventsServiceTest {
    //ShowPL Picklist Values
    private static String PICKLIST_AGREE = 'Agree';
    private static String PICKLIST_REFUSE = 'Refuse';
    //Event picklist values
    private static String PICKLIST_ADD_OR_UPDATE = 'AU';
    private static String PICKLIST_DELETE_IF_EXIST = 'DE';
    //Type picklist values
    private static String PICKLIST_PL = 'PartnerLocator';
    

    
    public static Account getBusinessAccount(String name,Id CountryId){

        Account acc = new Account(name=name,Street__c='Street',ZipCode__c='92300',Country__c=CountryId,PLDatapool__c='US_en2');
        return acc;
    }
    
    public static Account getSupplierAccount(String name,Id CountryId){
        Account acc = new Account(name=name,Street__c='Street',ZipCode__c='92300',Country__c=CountryId);
        return acc;
    }
    
    public static Account getExistingAccount(ID id){
        return [select Id,PLShowInPartnerLocator__c,RecordTypeId,PLShowInPartnerLocatorForced__c From Account where Id=:id limit 1].get(0);
    }
    
    public static void verifyEvent(Id accountId,Integer numberOfEvents,String[] eventTypes){
        List<IntegrationSimpleEvent__c> insertedEvent = [select id,name,ActionCode__c from IntegrationSimpleEvent__c where RelatedBFOId__c=:accountId order by CreatedDate ];
        ////System.assertEquals(numberOfEvents,insertedEvent.size());
        for(Integer i=0;i<eventTypes.size();i++){
            ////System.assertEquals(eventTypes[i],insertedEvent[i].ActionCode__c);
        }
    }

    public static void verifyEventByType(Id accountId,Integer numberOfEvents,String eventType){
        List<IntegrationSimpleEvent__c> insertedEvent = [select id,name,ActionCode__c from IntegrationSimpleEvent__c where RelatedBFOId__c=:accountId and ActionCode__c=:eventType order by CreatedDate ];
        ////System.assertEquals(numberOfEvents,insertedEvent.size());
    }
        
    
    public static void cleanEvents(String accountId){
        List<IntegrationSimpleEvent__c> insertedEvent = [select id,name,ActionCode__c from IntegrationSimpleEvent__c where RelatedBFOId__c=:accountId ];
        delete insertedEvent;
    }
    
    public static testMethod void testEventMethods()
    {
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        Account acc = getSupplierAccount('NULL',france.Id);
        insert acc;
        List<IntegrationSimpleEvent__c> integListToInsert = new List<IntegrationSimpleEvent__c>();
        PartnerLocatorPushEventsService.generateEvent(PICKLIST_ADD_OR_UPDATE,acc.Id,null,'Undelete record',PICKLIST_PL,integListToInsert,Datetime.now());
        ////System.assertEquals(1,integListToInsert.size());
        
        Boolean checkNew = PartnerLocatorPushEventsService.checkNewAndGenerateEvent('A','B',PICKLIST_ADD_OR_UPDATE,acc.Id,'Test',PICKLIST_PL,integListToInsert,Datetime.now());
        //should not create event because A != B
        ////System.assertEquals(false,checkNew);
        ////System.assertEquals(1,integListToInsert.size());
        checkNew = PartnerLocatorPushEventsService.checkNewAndGenerateEvent('A','A',PICKLIST_ADD_OR_UPDATE,acc.Id,'Test',PICKLIST_PL,integListToInsert,Datetime.now());
        //should create event because A == A
        //System.assertEquals(true,checkNew);
        //System.assertEquals(2,integListToInsert.size());
        //Change test
        checkNew = PartnerLocatorPushEventsService.checkChangeAndGenerateEvent('A','A',PICKLIST_ADD_OR_UPDATE,acc.Id,'Test',PICKLIST_PL,integListToInsert,Datetime.now());
        //should not create event because A == A (no changes)
        //System.assertEquals(false,checkNew);
        //System.assertEquals(2,integListToInsert.size());
        checkNew = PartnerLocatorPushEventsService.checkChangeAndGenerateEvent('A','B',PICKLIST_ADD_OR_UPDATE,acc.Id,'Test',PICKLIST_PL,integListToInsert,Datetime.now());
        //should create event because A != B (changes)
        //System.assertEquals(true,checkNew);
        //System.assertEquals(3,integListToInsert.size());
    }

    public static testMethod void testInsertAndUpdateShowInPLField()
    {
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'AP30;PRM_FieldChangeListener;SyncObjects;FieloPRM_MemberTrigger;FieloPRM_AP_AccountTriggers';
        insert us;
        System.runAs(us){
            test.startTest();
            Account acc0 = getBusinessAccount('NULL',france.Id);
            insert acc0;
            List<Account> accList = new List<Account>();
            accList.add(acc0);
            Map<String,Boolean> eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accList,null);
            System.assertNotEquals(null,acc0.PLDataPool__c);
            //System.assertEquals(true,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
            verifyEvent(acc0.Id,2,new String[]{'AU','AU'});
            
            Account acc1 = getBusinessAccount('Agree',france.Id);
            acc1.PLShowInPartnerLocator__c = 'Agree';
            insert acc1;
            verifyEvent(acc1.Id,3,new String[]{'AU','AU','AU'});
            
            Account acc2 = getBusinessAccount('Refuse',france.Id);
            acc2.PLShowInPartnerLocator__c = 'Refuse';
            insert acc2;
            verifyEventByType(acc2.Id,2,'AU');
            verifyEventByType(acc2.Id,1,'DE');
            
            
            //Updates
            Account acc = getBusinessAccount('Business Account',france.Id);
            insert acc;
            cleanEvents(acc.Id);
            
            acc = getExistingAccount(acc.Id);
            acc.name = 'Stay NULL';
            update acc;
            verifyEvent(acc.Id,0,new List<String>());
            
            acc.PLShowInPartnerLocator__c = 'Agree';
            update acc;
            verifyEvent(acc.Id,1,new String[]{'AU'});
            
            acc.name = 'Stay Agree';
            update acc;
            verifyEvent(acc.Id,1,new String[]{'AU'});
            
            acc.PLShowInPartnerLocator__c = null;
            update acc;
            verifyEvent(acc.Id,2,new String[]{'AU','DE'});
            
            acc.PLShowInPartnerLocator__c = 'Refuse';
            update acc;
            verifyEvent(acc.Id,3,new String[]{'AU','DE','DE'});
            
            acc.name = 'Stay Refuse';
            update acc;
            verifyEvent(acc.Id,3,new String[]{'AU','DE','DE'});
            
            acc.PLShowInPartnerLocator__c = 'Agree';
            update acc;
            verifyEvent(acc.Id,4,new String[]{'AU','DE','DE','AU'});
            
            acc.PLShowInPartnerLocator__c = 'Refuse';
            update acc;
            verifyEvent(acc.Id,5,new String[]{'AU','DE','DE','AU','DE'});
            
            acc.PLShowInPartnerLocator__c = null;
            update acc;
            verifyEvent(acc.Id,6,new String[]{'AU','DE','DE','AU','DE','AU'});
            test.stopTest();
        }
    }
    
        
    public static testMethod void testInsertAndUpdateShowInPLFieldForced()
    {
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;

        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'AP30;PRM_FieldChangeListener;SyncObjects;FieloPRM_MemberTrigger;FieloPRM_AP_AccountTriggers';
        insert us;
        System.runAs(us){
            test.startTest();
            Account acc0 = getBusinessAccount('NULL',france.Id);
            insert acc0;
            verifyEvent(acc0.Id,2,new String[]{'AU','AU'});
            
            Account acc1 = getBusinessAccount('true',france.Id);
            acc1.PLShowInPartnerLocatorForced__c = true;
            insert acc1;
            verifyEvent(acc1.Id,3,new String[]{'AU','AU','AU'});
            
            Account acc2 = getBusinessAccount('false',france.Id);
            acc2.PLShowInPartnerLocatorForced__c = false;
            insert acc2;
            verifyEventByType(acc2.Id,2,'AU');
            verifyEventByType(acc2.Id,0,'DE');
            
            
            //Updates
            Account acc = getBusinessAccount('Business Account',france.Id);
            insert acc;
            cleanEvents(acc.Id);
            
            acc = getExistingAccount(acc.Id);
            acc.name = 'Stay NULL';
            update acc;
            verifyEvent(acc.Id,0,new List<String>());
            
            acc.PLShowInPartnerLocatorForced__c = true;
            update acc;
            verifyEvent(acc.Id,1,new String[]{'AU'});
            
            acc.name = 'Stay True';
            update acc;
            verifyEvent(acc.Id,1,new String[]{'AU'});
            
            acc.PLShowInPartnerLocatorForced__c = false;
            update acc;
            verifyEventByType(acc.Id,1,'AU');
            verifyEventByType(acc.Id,1,'DE');
            
            acc.name = 'Stay false';
            update acc;
            verifyEventByType(acc.Id,1,'AU');
            verifyEventByType(acc.Id,1,'DE');
            
            acc.PLShowInPartnerLocatorForced__c = true;
            update acc;
            verifyEventByType(acc.Id,2,'AU');
            verifyEventByType(acc.Id,1,'DE');
            
            test.stopTest();
        }
    }
    
    
    public static testMethod void testInsertAndUpdatePLFields()
    {
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c = true;
        us.BypassTriggers__c = 'AP30;PRM_FieldChangeListener;SyncObjects;FieloPRM_MemberTrigger;FieloPRM_AP_AccountTriggers';
        insert us;
        System.runAs(us){
           test.startTest();
            Account acc0 = getSupplierAccount('NULL',france.Id);
            insert acc0;
            verifyEvent(acc0.Id,0,new String[]{});

            Account acc00 = getBusinessAccount('Company',france.Id);
            acc00.PLDataPool__c = null;
            insert acc00;
            verifyEvent(acc00.Id,0,new String[]{});
            
            Account acc1 = getBusinessAccount('Company',france.Id);
            acc1.PLShowInPartnerLocatorForced__c = true;
            insert acc1;
            verifyEvent(acc1.Id,3,new String[]{'AU','AU','AU'});
            
            Account acc2 = getBusinessAccount('null',france.Id);
            acc2.PLShowInPartnerLocatorForced__c = true;
            acc2.PLPartnerLocatorFeatureGranted__c = true;
            acc2.PLShowInPartnerLocator__c = 'Agree';
            insert acc2;
            verifyEvent(acc2.Id,5,new String[]{'AU','AU','AU','AU','AU'});
            
            
            //Updates
            Account acc = getBusinessAccount('Business Account',france.Id);
            insert acc;
            cleanEvents(acc.Id);
            
            acc = getExistingAccount(acc.Id);
            acc.name = 'Stay NULL';
            update acc;
            verifyEvent(acc.Id,0,new List<String>());
            
            acc.PLShowInPartnerLocatorForced__c = true;
            update acc;
            verifyEvent(acc.Id,1,new String[]{'AU'});
            
            acc.name = 'Stay True';
            update acc;
            verifyEvent(acc.Id,1,new String[]{'AU'});
            
            acc.PLShowInPartnerLocatorForced__c = false;
            update acc;
            verifyEvent(acc.Id,2,new String[]{'AU','DE'});
            
            acc.name = 'Stay null';
            update acc;
            verifyEvent(acc.Id,2,new String[]{'AU','DE'});
            
            acc.PLShowInPartnerLocatorForced__c = true;
            update acc;
            verifyEvent(acc.Id,3,new String[]{'AU','DE','AU'});
            test.stopTest();
        }
    }


    public static testMethod void testEligibility(){
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        List<Account> accListNew = new List<Account>();
        List<Account> accListOld = new List<Account>();
        String zipCode='125';
        String name='125dafae';
        accListNew.add(new Account(name='name',Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='9200',Country__c=france.Id,PRMUIMSID__c='ABCD'));
        accListOld.add(new Account(name='name',Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='356',Country__c=france.Id,PRMUIMSID__c='ABCD'));

        Map<String,Boolean> eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,accListOld);
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(true,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,null);
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(true,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        accListNew = new List<Account>();
        accListOld = new List<Account>();
        accListNew.add(new Account(name='name',Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='9200',Country__c=france.Id));
        accListOld.add(new Account(name='name',Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='356',Country__c=france.Id));

        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,accListOld);
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,null);
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        accListNew = new List<Account>();
        accListOld = new List<Account>();
        accListNew.add(new Account(name=name,Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='9200',Country__c=france.Id));
        accListOld.add(new Account(name=name,Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='356',Country__c=france.Id,PLDatapool__c='aa'));

        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,accListOld);
        //System.assertEquals(true,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,null);
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        accListNew = new List<Account>();
        accListOld = new List<Account>();
        accListNew.add(new Account(name=name,Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='9200',Country__c=france.Id));
        accListOld.add(new Account(name=name,Street__c='Street',ZipCode__c=''+zipCode,PRMZipCode__c='356',Country__c=france.Id,PRMUIMSID__c='ABCD',PLDatapool__c='aa'));

        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,accListOld);
        //System.assertEquals(true,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(true,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));


        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents(accListNew,null);
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE));
        //System.assertEquals(false,eligibileByfeature.get(PartnerLocatorPushEventsService.BFOPROPERTIES_FEATURE));
    }
    
    public static testMethod void testAfterDelete(){
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        Account acc = getBusinessAccount('To DEL',france.Id);
        insert acc;
        cleanEvents(acc.Id);
        //Delete
        List<IntegrationSimpleEvent__c> insertedEvent = [select id,name,ActionCode__c from IntegrationSimpleEvent__c where RelatedBFOId__c=:acc.Id order by CreatedDate ];
        System.debug('## '+insertedEvent);
        Id deleted = acc.Id;
        delete acc;
        Test.startTest();
        verifyEvent(deleted,1,new String[]{'DE'});
        cleanEvents(deleted);
        undelete acc;
        Account[] undeleted = [select id,IsDeleted From Account where id=:deleted];
        //System.assertEquals(1,undeleted.size());
        //System.assertEquals(false,undeleted[0].IsDeleted);
        insertedEvent = [select id,name,ActionCode__c from IntegrationSimpleEvent__c where BFOId__c=:undeleted[0].Id order by CreatedDate ];
        //System.assertEquals(1,insertedEvent.size());
        Test.stopTest();
        
    }


}