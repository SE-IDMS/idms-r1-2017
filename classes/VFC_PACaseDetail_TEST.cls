/**
Author:Beerappa K
Class Name:VFC_PACaseDetail
Purpose: This class will display case detail
Date :21/07/2016
*/
@isTest
public class VFC_PACaseDetail_TEST{
        
    @isTest static void testCaseDetail(){  
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        testAccount.City__c = 'Bangalore';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        Case objCase = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id,'testing');
        insert objCase;
        VFC_PACaseDetail caseDetail = new VFC_PACaseDetail();
        caseDetail.comment ='Testing';
       
        Test.startTest();
        VFC_PACaseDetail.getCaseAttachments(objCase.Id);
        VFC_PACaseDetail.getCaseDetails(objCase.Id);
        VFC_PACaseDetail.getCaseComments(objCase.Id);
        VFC_PACaseDetail.submitForCaseClosure(objCase.Id);
        VFC_PACaseDetail.saveCaseComment('Test', objCase.Id);
        VFC_PACaseDetail.subscribeUnsubscribeCase(objCase.Id);   
        VFC_PACaseDetail.userSubscribeCaseStatus(objCase.Id);  
        VFC_PACaseDetail.subscribeUnsubscribeCase(objCase.Id);  
        Test.stopTest();
        
    }
}