@isTest
public class VFC_PRMEmailsConfigration_Test {
     static TestMethod void VFC_PRMEmailsConfigrationTest() {
        List<CSPRMEmailNotification__c> cs_RecList = new List<CSPRMEmailNotification__c>();
        Country__c country= Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c state = Utils_TestMethods.createStateProvince(country.Id);
        insert state;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild;
        CSPRMEmailNotification__c cs_Rec = new CSPRMEmailNotification__c(AppliedTo__c = 1, Configurable__c = true, Default__c = true, Description__c = 'This is a test Custom Setting', EmailID__c = 'TestEmail1', Name = 'EM3', EmailTemplate__c ='this is test');
        cs_RecList.add(cs_Rec);
        CSPRMEmailNotification__c cs_Rec2 = new CSPRMEmailNotification__c(AppliedTo__c = 1, Configurable__c = false, Default__c = false, Description__c = 'This is a test Custom Setting ', EmailID__c = 'TestEmail2', Name = 'EM4',EmailTemplate__c ='this is test');
        cs_RecList.add(cs_Rec2);
        insert cs_RecList;
        PRMCountry__c prmCountry = new PRMCountry__c(CountryPortalEnabled__c = true,
                                          CountrySupportEmail__c = 'abc@xyz.com',
                                          PLDatapool__c = 'rn_US',
                                          Country__c = country.id
                                          );
        insert prmCountry;
        
        CountryChannels__c cChannels = new CountryChannels__c(Active__c = true,
                                                              AllowPrimaryToManage__c = True,
                                                              Channel__c = cLC.Id,
                                                              SubChannel__c = cLChild.id,
                                                              Country__c = country.id,
                                                              CompleteUserRegistrationonFirstLogin__c = true,
                                                              PRMcountry__c = prmCountry.Id);
        
        insert cChannels;
        PageReference pageRef = Page.VFP_PRMEmailsConfigration;
        Test.setCurrentPage(pageRef);
        Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(cChannels);
        ApexPages.currentPage().getParameters().put('Id',cChannels.id);
        VFC_PRMEmailsConfigration thisPRMEmailsConfigration = new VFC_PRMEmailsConfigration(thisController);
        thisPRMEmailsConfigration.doSave();
    }
}