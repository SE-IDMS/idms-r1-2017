/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 04/20/2012
    Description     : Controller class for the visualforce page 
                      Forces an update of the record and rolls back the change to list the validation rule errors                      
*/
public with sharing class PRJ_DraftValidationErrorsController
{
    public ApexPages.StandardController controller;
    
    public PRJ_ProjectReq__c oPR{get;set;}
    
    public string strValidationError {get;set;}
           
    public PRJ_DraftValidationErrorsController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        oPR = (PRJ_ProjectReq__c) controller.getRecord();
        strValidationError = '';        
    }    
    
    private void mainInit()
    {
        oPR = [Select Id,NextStep__c,RecordType.DeveloperName from PRJ_ProjectReq__c where Id = :oPR.Id];                   
    }    
    
    public PageReference updateProject()
    { 
        mainInit();       
        if(oPR.RecordType.DeveloperName == System.Label.DMT_RecordTypeDocumentProjectRequest)
            oPR.NextStep__c = System.Label.DMT_StatusCreated;
        else if(oPR.RecordType.DeveloperName == System.Label.DMT_RecordTypeValidateObjectivesStrategy)
            oPR.NextStep__c = System.Label.DMT_StatusValid;
        else if(oPR.RecordType.DeveloperName == System.Label.DMT_RecordTypeDevelopITProjectProposal)
            oPR.NextStep__c = System.Label.DMT_StatusQuoted;
        else if(oPR.RecordType.DeveloperName == System.Label.DMT_RecordTypeInitiateITProject)
            oPR.NextStep__c = System.Label.DMT_StatusProjectOpen;
        else if(oPR.RecordType.DeveloperName == System.Label.DMT_RecordTypeProjectFinished)
            oPR.NextStep__c = System.Label.DMT_StatusFinished;                        
    
        try
        {     
            Savepoint sp = Database.setSavepoint();  
            System.Debug('Status: ' + oPR.NextStep__c);                
            update oPR;
            Database.rollback(sp);
            if(Test.isRunningTest())
                throw new noMessageException('Test Exception');            
        }        
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
            {
                System.Debug('DMT Outside: ' + dmlexp.getMessage());
                if(!dmlexp.getMessage().toUpperCase().contains('INSUFFICIENT'))
                {
                    strValidationError += dmlexp.getDmlMessage(i);                                
                    System.Debug('DMT Inside: ' + dmlexp.getMessage());                    
                }
            }
            return null;                     
        }         
        catch(Exception exp)
        {
            System.Debug('Outside: ' + exp.getMessage());
            if(!exp.getMessage().toUpperCase().contains('INSUFFICIENT'))            
            {
                strValidationError += exp.getMessage();
                System.Debug('Inside: ' + exp.getMessage());
            }
            return null;    
        } 
        
        return null;   
    
    }
    
    public class noMessageException extends Exception{}
    
}