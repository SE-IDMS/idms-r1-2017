Public class VFC_COINbFO_BCAByCountryChart {

       
    public string theMonthYearJson{get;set;}
    public string  forCstBUJson{get;set;}
    public Integer forCstCountryJson{get;set;}
    public Integer forCstActualJson{get;set;}
    public string monthlyRevenueJson{get;set;}
    public boolean bldisplay{get;set;}
    
    public string dr{get;set;}
    
    public List<Integer> listFcastBU{get;set;}
    public List<Integer> listFcastCountry {get;set;}
    public List<Integer> listFcastActual {get;set;}

    private string offercycle;
    public Offer_Lifecycle__c offerlife{get;set;}
    public Set<string> setMProject {get;set;} 
    public List<string> liststr {get;set;} 
    public List<Milestone1_Project__c> listmProject{get;set;}
    public VFC_COINbFO_BCABYCountryChart(ApexPages.StandardController controller) {
        List<string> listMAndy  =new List<string>();
        listFcastBU  =new List<Integer>();
        listFcastCountry =new List<Integer>();
        listFcastActual  =new List<Integer>();
        List<string> listmonthlyRevenue  =new List<string>();

        setMProject = new Set<string>();
        offercycle=System.currentPageReference().getParameters().get('id');
        for (Milestone1_Project__c mpObj:[select id,Offer_Launch__c,RecordType.Name from Milestone1_Project__c where Offer_Launch__c=:offercycle AND RecordType.Name='Offer Introduction Project' ]) {

            setMProject.add(mpObj.id);

        }
        system.debug('setMProject--------9999999'+setMProject);
        
        List<offerCycleForecastData> listWarpperData=getForecastData();
        for(offerCycleForecastData  wrpObj:listWarpperData) {
            listMAndy.add(wrpObj.theMonth);
            listFcastBU.add(integer.valueof(wrpObj.forCstBU));
            listFcastCountry.add(Integer.valueof(wrpObj.forCstCountry));
            listFcastActual.add(Integer.valueof(wrpObj.forCstActual));
            listmonthlyRevenue.add(string.valueof(wrpObj.monthlyRevenue));
        }
        
        theMonthYearJson = JSON.serialize(listMAndy);
       // forCstBUJson =JSON.serialize(listFcastBU).replace('"','');
        system.debug('Test--------9999999'+forCstBUJson);
        //forCstCountryJson =JSON.serialize(listFcastCountry);
        //forCstActualJson =Json.serialize(listFcastActual);
        monthlyRevenueJson =Json.Serialize(listmonthlyRevenue);
    }



 

    public  List<AggregateResult>  listOfferCycleResults() {
    
    
    //List<AggregateResult> aggresults = [SELECT SUM(Actual__c) act,SUM(BU__c)bu,SUM(Country__c)cout,Month__c,ForcastYear__c,Projects__r.Country__c FROM OfferLifeCycleForecast__c where project__c IN:setMProject GROUP BY  ForcastYear__c,Month__c,Projects__r.Country__c];
    
    List<AggregateResult> aggresults = [SELECT SUM(Actual__c) act,SUM(BU__c)bu,SUM(Country__c)cout,Country2__c  FROM OfferLifeCycleForecast__c where project__c IN:setMProject AND Month__c!='Total by Year'  GROUP BY  Country2__c ];
    
    
    system.debug('Test------->'+aggresults);
    
    return aggresults;
    }
    
    
    public List<offerCycleForecastData> getForecastData() {
        Integer offcount = 0;
        Integer strStartYear =Date.today().year();
        Set<String> setListYears = new Set<String> (); 
        Integer forCastYears = 0;
        string stryear;
        List<AggregateResult> listCycleResults = new List<AggregateResult>();
        
        for(integer i=0; i< 4 ;i++) {
           
            forCastYears = strStartYear  + i;
            stryear=(forCastYears).format().replace(',','');
            if (stryear.length()==5) {
                stryear = stryear.substring(0,1)+ stryear.substring(2);
            }
            
            setListYears.add(stryear);
        }   
        
        listCycleResults =listOfferCycleResults();
        
        List<offerCycleForecastData> theResults = new List<offerCycleForecastData>();
        //for(string stryr:setListYears) {
        //     for (Integer month = 1 ; month <= MONTHS_OF_YEAR.size() ; month++) {
                  
                       for(AggregateResult ar:listCycleResults) {
                        offerCycleForecastData  offerCyclewarp=null;
                     // if(offcount <= listCycleResults.size()) {
                     
                     
                      //if(stryr==listCycleResults[offcount].get('ForcastYear__c')) {
                        //string prastMonth = string.valueof(listCycleResults[offcount].get('Month__c'));
                        //System.debug('TestBp--------->'+prastMonth);
                            //if(monthName(month)==prastMonth)  {
                                offerCyclewarp = new offerCycleForecastData(string.valueof(ar.get('Country2__c')));
                                
                                offerCyclewarp.forCstBU=Integer.valueOf(ar.get('bu'));
                                offerCyclewarp.forCstCountry =Integer.valueOf(ar.get('cout'));
                                offerCyclewarp.forCstActual =Integer.valueOf(ar.get('act'));
                                System.debug('Test-offerCyclewarp--------->'+offerCyclewarp);
                               if(offerCyclewarp.forCstBU!=0  ) {
                                offerCyclewarp.monthlyRevenue =(100 * (offerCyclewarp.forCstCountry-offerCyclewarp.forCstBU)) / offerCyclewarp.forCstBU;
                                }
                                
                                offerCyclewarp.RevenuePercent=string.valueof(offerCyclewarp.monthlyRevenue)+'%';
                                 System.debug('Test-offerCyclewarp--------->'+offerCyclewarp.monthlyRevenue);
                             offcount++;
                            //}
                       // }   
                            if(offerCyclewarp != null) {

                            theResults.add(offerCyclewarp);

                            }
                   
                      //}
                     }
                      
                     
              
               
          //  }
      // }
    
       
     System.debug('Test---------->'+theResults);
     if(theResults.size() > 0) {
         bldisplay=true;
     }
 

        return theResults;

    }

   // To Get the English month string

    private final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December'};


    private String monthName(Integer monthNum) {

        return(MONTHS_OF_YEAR[monthNum - 1]);

    }

    public class offerCycleForecastData {

        public String theMonth { get; set; }
        public Integer forCstBU { get; set; }
        public Integer forCstCountry { get; set; }
        public Integer forCstActual{ get; set; }
        public Double monthlyRevenue { get; set; }
        public string RevenuePercent{ get; set; }


        public offerCycleForecastData (String mon) {

            this(mon, 0, 0, 0,0,'0');

        }

        public offerCycleForecastData(String mon, Integer BU, Integer Ctry, integer CstActual,Double monthRev,string monthVal ) {

        this.theMonth = mon;
        this.forCstBU = Bu;
        this.forCstCountry = Ctry;
        this.forCstActual =CstActual;
        this.monthlyRevenue = monthRev;
        this.RevenuePercent=monthVal ;

        }
    }   
}