public class VFC_CCCFollowupResponse{
    public Map<String,String> pageParameters = System.currentPageReference().getParameters();
    public String strCaseId=null;
    public String strResponse=null;
    public boolean blnDMLRequired =false;
    Map<Integer,String> MapMonthList = new Map<Integer,String>();
    public PageReference updateCase() {
        MapMonthList.put(1,'January');
        MapMonthList.put(2,'February');
        MapMonthList.put(3,'March');
        MapMonthList.put(4,'April');
        MapMonthList.put(5,'May');
        MapMonthList.put(6,'June');
        MapMonthList.put(7,'July');
        MapMonthList.put(8,'August');
        MapMonthList.put(9,'September');
        MapMonthList.put(10,'October');
        MapMonthList.put(11,'November');
        MapMonthList.put(12,'December');
        if(pageParameters.containsKey('caseId'))
            strCaseId = pageParameters.get('caseId');
        if(pageParameters.containsKey('response'))
            strResponse = pageParameters.get('response');
        
        try{
            if(strResponse!= null && strCaseId != null){
                List<Case> lstCase = new List<Case>([Select id, CaseNumber, Status, ActionNeededFrom__c,AnswerToCustomer__c,TECH_CustomerResponseReceived__c, SupportCategory__c, Symptom__c, OtherSymptom__c, CommercialReference_lk__c, Family_lk__c, Team__c, ContactId, Contact.CorrespLang__c, Subject,CustomerRequest__c, CCCountry__c, Origin, LevelOfExpertise__c,TECH_SPOC__c,OwnerId,Severity__c,AutoFollowupwithCustomer__c,AutoFollowUpStage__c, AutoFollowUpCount__c,REP_AutoFollowUpFirstResponse__c from Case where Id=:strCaseId ]);
                
                String strCustomerMessageThankYou           = '';
                String strCustomerMessageSorry              = ''; 
                String strCustomerMessageAlreadyResponded   = '';
                Map<String,Id> mapExternalString = new Map<String,Id>();
                Map<Id,ExternalStringLocalization> mapExternalStringLocalization = new Map<Id,ExternalStringLocalization>();
                Set<Id> setExternalStringId = new Set<Id>();
                if(lstCase.size()>0 && lstCase[0].ContactId != null && lstCase[0].Contact.CorrespLang__c !='EN'){
                    List<String> lstCustomLabelNames = new List<String>();
                    
                    String CLOCT14CCC53 = 'CLOCT14CCC53';
                    String CLOCT14CCC54 = 'CLOCT14CCC54';
                    String CLOCT14CCC55 = 'CLOCT14CCC55';
                    String CLOCT14CCC50 = 'CLOCT14CCC50';
                    
                    lstCustomLabelNames.add(CLOCT14CCC53);
                    lstCustomLabelNames.add(CLOCT14CCC54);
                    lstCustomLabelNames.add(CLOCT14CCC55);
                    lstCustomLabelNames.add(CLOCT14CCC50);
                    List<ExternalString> lstCustomLabels = new List<ExternalString>([Select Id, Name from ExternalString where Name in: lstCustomLabelNames]);
                    for(ExternalString objExternalString: lstCustomLabels ){
                        mapExternalString.put(objExternalString.Name,objExternalString.Id);
                        setExternalStringId.add(objExternalString.Id);
                    }
                    if(setExternalStringId.size()>0){
                        List<ExternalStringLocalization> lstCustomLabelsLocal = new List<ExternalStringLocalization>([Select Id, ExternalStringId, Value from ExternalStringLocalization where ExternalStringId  in: setExternalStringId AND Language =: lstCase[0].Contact.CorrespLang__c]);
                        for(ExternalStringLocalization  objExternalStringLocalization : lstCustomLabelsLocal){
                            mapExternalStringLocalization.put(objExternalStringLocalization.ExternalStringId,objExternalStringLocalization);
                        }
                        if(mapExternalString.containsKey(CLOCT14CCC53) && mapExternalString.containsKey(CLOCT14CCC54)){
                            if(mapExternalStringLocalization.containsKey(mapExternalString.get(CLOCT14CCC53)) && mapExternalStringLocalization.containsKey(mapExternalString.get(CLOCT14CCC54))){
                                strCustomerMessageThankYou           = mapExternalStringLocalization.get(mapExternalString.get(CLOCT14CCC53)).Value + ' ' + lstCase[0].CaseNumber  + ' ' + mapExternalStringLocalization.get(mapExternalString.get(CLOCT14CCC54)).Value;
                            }   
                        }
                        
                        if(mapExternalString.containsKey(CLOCT14CCC55)){
                            if(mapExternalStringLocalization.containsKey(mapExternalString.get(CLOCT14CCC55))){
                                strCustomerMessageSorry           = mapExternalStringLocalization.get(mapExternalString.get(CLOCT14CCC55)).Value;
                            }   
                        }
                        
                        if(mapExternalString.containsKey(CLOCT14CCC50)){
                            if(mapExternalStringLocalization.containsKey(mapExternalString.get(CLOCT14CCC50))){
                                strCustomerMessageAlreadyResponded           = mapExternalStringLocalization.get(mapExternalString.get(CLOCT14CCC50)).Value;
                            }   
                        }
                    }
                }
                if(strCustomerMessageThankYou=='')
                    strCustomerMessageThankYou           = System.Label.CLOCT14CCC53 + ' ' + lstCase[0].CaseNumber  + ' ' + System.Label.CLOCT14CCC54;
                    
                if(strCustomerMessageSorry=='') 
                    strCustomerMessageSorry              = System.Label.CLOCT14CCC55;
                    
                if(strCustomerMessageAlreadyResponded=='')  
                    strCustomerMessageAlreadyResponded   = System.Label.CLOCT14CCC50 + ' ' + lstCase[0].CaseNumber + '.';
                
                String strPageMessage                       = '';
                String strCustomerConfirmedNo               = '';
                String strCustomerConfirmedYes              = '';
                
                if(strResponse.equalsIgnoreCase('yes')){
                    if(lstCase.size()>0 && lstCase[0].Status != 'Closed'){
                        lstCase[0].Status='Closed';
                        lstCase[0].TECH_CustomerResponseReceived__c=true;
                        lstCase[0].ActionNeededFrom__c = lstCase[0].LevelOfExpertise__c;
                        lstCase[0].AutoFollowUpStage__c = System.Label.CLOCT15CCC09;
                        if(String.isEmpty(lstCase[0].REP_AutoFollowUpFirstResponse__c)) 
                            lstCase[0].REP_AutoFollowUpFirstResponse__c = 'Yes';
                        String yearYes = System.Today().Year().format(); //BR-7596 begin
                        String s1=yearYes.substringBefore(',');
                        String s2=yearYes.substringAfter(',');
                        String NewYearYes= s1+s2;
                        strCustomerConfirmedYes = System.Today().Day().format() + 'th ' + MapMonthList.get(System.Today().Month()) + ' ' + NewYearYes + System.Label.CLOCT14CCC51;  //BR-7596 end
                        lstCase[0].AnswerToCustomer__c=(lstCase[0].AnswerToCustomer__c!=null) ? lstCase[0].AnswerToCustomer__c + '<br/>' + strCustomerConfirmedYes: strCustomerConfirmedYes;
                        strPageMessage = strCustomerMessageThankYou;
                        blnDMLRequired=true;
                    }
                    else if(lstCase.size()>0 && lstCase[0].Status == 'Closed'){
                        if(lstCase.size()>0 && lstCase[0].TECH_CustomerResponseReceived__c == false){
                            strPageMessage = strCustomerMessageThankYou;
                        }
                        else if(lstCase.size()>0 && lstCase[0].TECH_CustomerResponseReceived__c == true){
                            strPageMessage = strCustomerMessageAlreadyResponded;
                        }
                        if(String.isEmpty(strPageMessage))
                            strPageMessage = strCustomerMessageThankYou;
                    }
                }
                else if(strResponse.equalsIgnoreCase('no')){
                    if(lstCase.size()>0 && lstCase[0].Status == 'Closed'){
                        Case objCase = new Case();
                        objCase.RelatedSupportCase__c           = lstCase[0].Id;
                        objCase.SupportCategory__c              = lstCase[0].SupportCategory__c;
                        objCase.Symptom__c                      = lstCase[0].Symptom__c;
                        objCase.OtherSymptom__c                 = lstCase[0].OtherSymptom__c;
                        objCase.CommercialReference_lk__c       = lstCase[0].CommercialReference_lk__c;
                        objCase.Family_lk__c                    = lstCase[0].Family_lk__c;
                        objCase.Team__c                         = lstCase[0].Team__c;
                        objCase.Subject                         = lstCase[0].Subject;
                        objCase.CustomerRequest__c              = System.Label.CLOCT14CCC57;
                        objCase.CCCountry__c                    = lstCase[0].CCCountry__c;
                        objCase.Origin                          = 'Email';
                        objCase.LevelOfExpertise__c             = lstCase[0].LevelOfExpertise__c;
                        objCase.TECH_SPOC__c                    = lstCase[0].TECH_SPOC__c;
                      //objCase.OwnerId                         = Label.CLOCT13CCC15;
                        objCase.Severity__c                     = lstCase[0].Severity__c;
                        objCase.ContactId                       = lstCase[0].ContactId;
                        objCase.AutoFollowUpStage__c            = System.Label.CLOCT15CCC10;
                        Database.SaveResult CaseInsertResult = Database.insert(objCase, false);
                        strPageMessage = strCustomerMessageSorry;
                    }
                    else if(lstCase.size()>0 && lstCase[0].Status != 'Closed'){
                        if(lstCase.size()>0 && lstCase[0].TECH_CustomerResponseReceived__c == false){
                            lstCase[0].Status='In Progress';
                            lstCase[0].TECH_CustomerResponseReceived__c =true;
                            lstCase[0].AutoFollowupwithCustomer__c =false;
                            lstCase[0].ActionNeededFrom__c = lstCase[0].LevelOfExpertise__c;
                            lstCase[0].AutoFollowUpStage__c = System.Label.CLOCT15CCC10;
                            lstCase[0].AutoFollowUpCount__c          =(lstCase[0].AutoFollowUpCount__c!=null) ? lstCase[0].AutoFollowUpCount__c + 1: 1;
                            if(String.isEmpty(lstCase[0].REP_AutoFollowUpFirstResponse__c))  
                                lstCase[0].REP_AutoFollowUpFirstResponse__c = 'No';
                            String yearNo = System.Today().Year().format();  //BR-7596 begin
                            String s1=yearNo.substringBefore(',');
                            String s2=yearNo.substringAfter(',');
                            String NewYearNo= s1+s2;                            
                            strCustomerConfirmedNo = System.Today().Day().format() + 'th ' + MapMonthList.get(System.Today().Month()) + ' ' + NewYearNo+ System.Label.CLOCT14CCC52;  //BR-7596 end
                            lstCase[0].AnswerToCustomer__c=(lstCase[0].AnswerToCustomer__c!=null) ? lstCase[0].AnswerToCustomer__c + '<br/>' + strCustomerConfirmedNo : strCustomerConfirmedNo;
                            strPageMessage = strCustomerMessageSorry;
                            blnDMLRequired=true;
                        }
                        else if(lstCase.size()>0 && lstCase[0].TECH_CustomerResponseReceived__c == true){
                            strPageMessage = strCustomerMessageAlreadyResponded;
                        }
                        if(String.IsEmpty(strPageMessage))
                            strPageMessage = strCustomerMessageSorry;
                    }
                }
                
                if(blnDMLRequired){
                    Database.SaveResult[] CaseUpdateResult = Database.update(lstCase, false);
                    for(Database.SaveResult objSaveResult: CaseUpdateResult){
                        if(!objSaveResult.isSuccess()){
                            Database.Error err                          = objSaveResult.getErrors()[0];
                            String strErrorId                           = objSaveResult.getId() + ':' + '<br/>';
                            String  strErrorMessage                     = err.getStatusCode()+' '+err.getMessage()+ '<br/>';
                            System.debug(strErrorId + ' ' + strErrorMessage);
                            lstCase[0].Status                           = 'In Progress';
                            lstCase[0].TECH_CustomerResponseReceived__c =true;
                            lstCase[0].ActionNeededFrom__c              = lstCase[0].LevelOfExpertise__c;
                            if(strResponse.equalsIgnoreCase('yes')){
                                lstCase[0].AnswerToCustomer__c          =(lstCase[0].AnswerToCustomer__c!=null) ? lstCase[0].AnswerToCustomer__c + '<br/>' + strCustomerConfirmedYes: strCustomerConfirmedYes;
                            }
                            else if(strResponse.equalsIgnoreCase('no')){
                                lstCase[0].AnswerToCustomer__c          =(lstCase[0].AnswerToCustomer__c!=null) ? lstCase[0].AnswerToCustomer__c + '<br/>' + strCustomerConfirmedNo : strCustomerConfirmedNo;
                            }
                            CaseUpdateResult = Database.update(lstCase, false);
                        }
                    }
                }
                if(!(String.IsEmpty(strPageMessage))){
                    AP_PageMessages.addMessage(new AP_PageMessages.PageMessage(AP_PageMessages.severity.Info, strPageMessage));   
                }
            }
        }
        catch(Exception ex){
            AP_PageMessages.addMessage(new AP_PageMessages.PageMessage(AP_PageMessages.severity.Error,'Contact Customer Care'));
        }
        return null;
    }
}