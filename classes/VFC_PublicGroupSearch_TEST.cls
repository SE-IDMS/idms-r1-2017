@isTest
public class VFC_PublicGroupSearch_TEST {

    static testMethod void searchForAGroup() {
        
        Group regularGroup = Utils_TestMethods.createRegularGroup('Test Group');
        insert regularGroup; 
        
        PageReference publicGroupSearch = Page.VFP_PublicGroupSearch;
        VFC_PublicGroupSearch publicGroupController = new VFC_PublicGroupSearch();
        
        publicGroupController.searchString = 'Test Group';
        publicGroupController.find();
        
        system.assertEquals(1, publicGroupController.searchResult.size(), 'No result is returned by the search');
        
        publicGroupController.PerformAction(new DataTemplate__c(Name='Test Group', 
                                                                Field1__c = 'Test', 
                                                                Field2__c = 'Test'), 
                                                                publicGroupController);
        
        VCC06_DisplaySearchResults testVcc06 = publicGroupController.resultsController;
    }
}