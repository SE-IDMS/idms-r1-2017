@isTest
private class VFC_MassUpdateContainmentAction_Test {
    static testMethod void xacreate(){
         Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
         User newUser = Utils_TestMethods.createStandardUser('TestUser');    
         newUser.UserRoleID = rol_ID ;
        
         Set<Id> XAids = new Set<Id>();
        
         BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
         brEntity.Entity__c = 'Entity 12';
         Database.insert(brEntity);
         BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
         brSubEntity.Entity__c = 'Entity 12';
         brSubEntity.SubEntity__c = 'SubEntity 12';
         Database.insert(brSubEntity);
         BusinessRiskEscalationEntity__c AccOrg = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();  
         AccOrg.Entity__c = 'Entity 12';
         AccOrg.SubEntity__c = 'SubEntity 12';
         Database.insert(AccOrg,false);
         
         Problem__c prob1 = Utils_TestMethods.createProblem(AccOrg.Id,12);
         prob1.ProblemLeader__c = newUser.Id;
         prob1.ProductQualityProblem__c = false;
         prob1.RecordTypeid = Label.CLI2PAPR120014;
         prob1.Sensitivity__c = 'Public';
         prob1.Severity__c = 'Safety related';
         Database.insert(prob1);
         
         List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
         ContainmentAction__c XA1 = Utils_TestMethods.createContainmentAction(prob1.Id,AccOrg.Id);
         lstXA.add(XA1);
         ContainmentAction__c XA2 = Utils_TestMethods.createContainmentAction(prob1.Id,AccOrg.Id);
         lstXA.add(XA2);
         Database.insert(lstXA); 
        
    }

    static testMethod void massUpdate() {
       xacreate();
       ContainmentAction__c xa = new ContainmentAction__c();
       List<ContainmentAction__c> xaList = [SELECT description__c FROM ContainmentAction__c LIMIT 20];
       
       ApexPages.StandardSetController setCtr = new ApexPages.StandardSetController(xaList);
       setCtr.setSelected(new ContainmentAction__c []{xaList[0]});
       VFC_MassUpdateContainmentAction controller = new VFC_MassUpdateContainmentAction(setCtr);
       System.assertEquals(1, controller.getRecordSize());
        
       String value = '123test';
       controller.valueToUpdate=value; 
       controller.updateContainmentAction(); 
       controller.cancelContainmentAction();
    }
    
    static testMethod void massUpdate2() {
        xacreate();
        ContainmentAction__c xa = new ContainmentAction__c();
        List<ContainmentAction__c> xaList = [SELECT description__c FROM ContainmentAction__c LIMIT 10];
       
        ApexPages.StandardSetController setCtr = new ApexPages.StandardSetController(xaList);
        setCtr.setSelected(new ContainmentAction__c []{});
        VFC_MassUpdateContainmentAction controller = new VFC_MassUpdateContainmentAction(setCtr);
        System.assertEquals(0, controller.getRecordSize());
        
        String value = '123test';
        controller.valueToUpdate=value; 
        controller.updateContainmentAction(); 
        controller.cancelContainmentAction();
    }
    
    static testMethod void massUpdate3() {
        xacreate();
        ContainmentAction__c xa1 = new ContainmentAction__c();
        List<ContainmentAction__c> xaList1 = [SELECT description__c FROM ContainmentAction__c LIMIT 20 ];
        xaList1[0].description__c = '';
        update xaList1;
       
        ApexPages.StandardSetController setCtr1 = new ApexPages.StandardSetController(xaList1);
        setCtr1.setSelected(new ContainmentAction__c []{xaList1[0]});
        VFC_MassUpdateContainmentAction controller1 = new VFC_MassUpdateContainmentAction(setCtr1);
        System.assertEquals(1, controller1.getRecordSize());
            
        String value1 = '';
       
        controller1.valueToUpdate=value1; 
        controller1.updateContainmentAction();  
        controller1.cancelContainmentAction();
    }
    
    static testMethod void massUpdate4() {
        xacreate();
        ContainmentAction__c xa1 = new ContainmentAction__c();
        List<ContainmentAction__c> xaList1 = [SELECT description__c FROM ContainmentAction__c LIMIT 20 ];
        xaList1[0].description__c = '';
        update xaList1;
       
        ApexPages.StandardSetController setCtr1 = new ApexPages.StandardSetController(xaList1);
        setCtr1.setSelected(new ContainmentAction__c []{xaList1[0]});
        VFC_MassUpdateContainmentAction controller1 = new VFC_MassUpdateContainmentAction(setCtr1);
        System.assertEquals(1, controller1.getRecordSize());
            
        String value1 = 'test';
       
        controller1.valueToUpdate=value1; 
        controller1.updateContainmentAction();  
        controller1.cancelContainmentAction();
        controller1.VideoLink();
        
        
        
        
        
        BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                insert  accOrg1;
        BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                accOrg2.Name='Test';
                accOrg2.Entity__c='Test Entity-2'; 
                accOrg2.SubEntity__c='Test Sub-Entity 2';
                insert  accOrg2;
        BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                accOrg.Name='Test';
                accOrg.Entity__c='Test Entity-2';  
                accOrg.SubEntity__c='Test Sub-Entity 2';
                accOrg.Location__c='Test Location 2';
                accOrg.Location_Type__c='Design Center';
                insert  accOrg;
        
        Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
        prob.Severity__c = 'Safety Related';
        prob.RecordTypeId = Label.CLI2PAPR120014;
        insert prob;
        
        List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
        ContainmentAction__c con1 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        insert con1;
        lstXA.add(con1);
        
        ApexPages.StandardSetController setCtr2 = new ApexPages.StandardSetController(lstXA);
        setCtr2.setSelected(new ContainmentAction__c []{lstXA[0]});
        VFC_MassUpdateContainmentAction controller2 = new VFC_MassUpdateContainmentAction(setCtr2);
        System.assertEquals(1, controller2.getRecordSize());
        
        
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG';
        CommercialReference__c crs = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                CommercialReference__c = commRefTOTest,Family__c = 'Family', 
                                                                GMRCode__c = gmrCodeToTest,ProductLine__c = 'ProductLine', 
                                                                ProductFamily__c ='ProductFamily');
        insert crs;       
        
        
        List<VFC_MassUpdateContainmentAction.WrapperClass> lstWrapperClass = new List<VFC_MassUpdateContainmentAction.WrapperClass>();
       
        controller2.getItems();
        
        lstWrapperClass = controller2.getDisplayAffectedProducts(); 
        
        VFC_MassUpdateContainmentAction.WrapperClass mywclistwc = new VFC_MassUpdateContainmentAction.WrapperClass(crs);
        lstWrapperClass.add(mywclistwc);
        lstWrapperClass[0].checkbox = true;       
        
        controller2.functions = 'None';
        controller2.updateContainmentAction();
        
        
        ContainmentAction__c con2 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        insert con2;
        lstXA.add(con2);
        
        XAAffectedProduct__c xaap1 = new XAAffectedProduct__c();
        xaap1.ContainmentAction__c = con2.Id;
        xaap1.AffectedProduct__c = crs.Id;
        insert xaap1;
        
        ApexPages.StandardSetController setCtr3 = new ApexPages.StandardSetController(lstXA);
        setCtr3.setSelected(new ContainmentAction__c []{lstXA[1]});
        VFC_MassUpdateContainmentAction controller3 = new VFC_MassUpdateContainmentAction(setCtr3);
        System.assertEquals(1, controller3.getRecordSize());
        
        List<VFC_MassUpdateContainmentAction.WrapperClass> lstWrapperClass1 = new List<VFC_MassUpdateContainmentAction.WrapperClass>();
        lstWrapperClass1 = controller3.getDisplayAffectedProducts(); 
       
        VFC_MassUpdateContainmentAction.WrapperClass mywclistwc1 = new VFC_MassUpdateContainmentAction.WrapperClass(crs);
        lstWrapperClass1.add(mywclistwc1);
        lstWrapperClass1[0].checkbox = true;
        
        controller3.functions = 'Add To XA(s)';
        controller3.updateContainmentAction();
        
        ContainmentAction__c con3 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        insert con3;
        lstXA.add(con3);
        
        XAAffectedProduct__c xaap = new XAAffectedProduct__c();
        xaap.ContainmentAction__c = con3.Id;
        xaap.AffectedProduct__c = crs.Id;
        insert xaap;
        
        ApexPages.StandardSetController setCtr4 = new ApexPages.StandardSetController(lstXA);
        setCtr4.setSelected(new ContainmentAction__c []{lstXA[2]});
        VFC_MassUpdateContainmentAction controller4 = new VFC_MassUpdateContainmentAction(setCtr4);
        System.assertEquals(1, controller4.getRecordSize());
        
        List<VFC_MassUpdateContainmentAction.WrapperClass> lstWrapperClass2 = new List<VFC_MassUpdateContainmentAction.WrapperClass>();
        lstWrapperClass2 = controller4.getDisplayAffectedProducts(); 
       
        VFC_MassUpdateContainmentAction.WrapperClass mywclistwc2 = new VFC_MassUpdateContainmentAction.WrapperClass(crs);
        lstWrapperClass2.add(mywclistwc2);
        lstWrapperClass2[0].checkbox = true;
        
        controller4.functions = 'Delete From XA(s)';
        controller4.updateContainmentAction();
    }

}