/*
    Author: Pooja Bhute
    Date: 09/02/2011
    Description:This class tests the Opportunity Trigger Class.

*/

@isTest
private class AP05_OpportunityTrigger_Test{
  static List<OppAgreement__c> lstOpAgreementForTest;
  static testMethod void testOpportunityTriggers(){
         
        System.Debug('****** Test class AP05_OpportunityTrigger_Test Started ****');
         
        Test.startTest();
        VFC61_OpptyClone.isOpptyClone=false;
    
        //Create Frame Agreement
        User newUser = Utils_TestMethods.createStandardUser('TestUser');
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
           
        if(!UserInsertResult.isSuccess())
            Database.Error err = UserInsertResult.getErrors()[0];
        
        Country__c newCountry = Utils_TestMethods.createCountry();
        newCountry.countrycode__c ='IN';
        Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
           
        if(!CountryInsertResult.isSuccess())
            Database.Error err = CountryInsertResult.getErrors()[0];
             
        OppAgreement__c objAgree = Utils_TestMethods.createFrameAgreement(newUser.Id);
        Database.SaveResult AgreementInsertResult = Database.insert(objAgree, false);
            
        if(!AgreementInsertResult.isSuccess())
            Database.Error err = AgreementInsertResult.getErrors()[0];
                 
        List<OppAgreement__c> lstAgreement = new List<OppAgreement__c>();
        lstAgreement.add(objAgree);        
        
        //Create Frame Opportunity
        Account acc = Utils_TestMethods.createAccount();
        Database.SaveResult AccountInsertResult = Database.insert(acc, false);
           
        if(!AccountInsertResult.isSuccess())
            Database.Error err = AccountInsertResult.getErrors()[0];
      
        Opportunity  objOpp = Utils_TestMethods.createFrameOpporutnity(acc.Id, objAgree.Id, newCountry.Id);
        Database.SaveResult OpportunityInsertResult = Database.insert(objOpp, false);
            
        if(!OpportunityInsertResult.isSuccess())
            Database.Error err = OpportunityInsertResult.getErrors()[0];
      
        List<Opportunity> lstOpp= new  List<Opportunity>();
        lstOpp.add(objOpp);
       
        ProductLineForAgreement__c objPLAGR = Utils_TestMethods.createProductLineAgr(objAgree.ID, 100);
        Database.SaveResult ProductLineInsertResult = Database.insert(objPLAGR, false);
             
        if(!OpportunityInsertResult.isSuccess())
            Database.Error err = OpportunityInsertResult.getErrors()[0];
         
        //AP05_OpportunityTrigger.addAgreementProdLineToOppts(lstOpp);
        

        //Create Sales Contributor
        OPP_SalesContributor__c objSales =  Utils_TestMethods.createSalesContr(objOpp.Id, newUser.Id);
        Database.SaveResult SalesContributorInsertResult = Database.insert(objSales, false);
             
        if(!SalesContributorInsertResult.isSuccess())
            Database.Error err = SalesContributorInsertResult.getErrors()[0];
        
        AP05_OpportunityTrigger.addContributorToSalesContributor(lstOpp);
        
        
        //Catch Block Test
        /*
         ProductLineForAgreement__c objPLAGR1 = Utils_TestMethods.createProductLineAgr(objAgree.ID, 100);
         objPLAGR1.Amount__c = null ; 
         Database.SaveResult ProductLineInsertResult1 = Database.insert(objPLAGR1, false);
         */
                                         
     Test.stopTest();
      
       System.Debug('****** Test class AP05_OpportunityTrigger_Test Ended ****');
  }
    //Check if TECH_CrossProcessConversionAmount__c field is filled, creates an Opportunity Line
    static testMethod void testOpportunityTriggersmethod2()
    {  
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='IN';
        insert country;
         Country__c country2 = Utils_TestMethods.createCountry();
        country2.countrycode__c ='IT';
        insert country2;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        Insert acc;
        acc.isPartner = true;
        acc.PRMAccount__c = True;
        update acc;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
         globalProgram.TECH_CountriesId__c = country.id;
         globalPRogram.recordtypeid = Label.CLMAY13PRM15;
         globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
         insert globalProgram;
         PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country.Id);
         countryProgram.ProgramStatus__c='Active';
         insert countryProgram;
         PartnerProgram__c countryProgram2 = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country2.Id);
         countryProgram2.ProgramStatus__c='Active';
         insert countryProgram2;
        ProgramLevel__c prg = Utils_TestMethods.createCountryProgramLevel(countryProgram.id);
        prg.levelstatus__c = 'active';
        insert prg;
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(countryProgram2.id);
        prg2.levelstatus__c = 'active';
        insert prg2;

        ACC_PartnerProgram__c accPgm = Utils_TestMethods.createAccountProgram(prg.id, countryProgram.id, acc.id);
        accPgm.active__c = true; 
        ACC_PartnerProgram__c accPgm2 = Utils_TestMethods.createAccountProgram(prg2.id, countryProgram2.id, acc.id);
        accPgm2.active__c = true; 
        List<ACC_PartnerProgram__c> accpgmlst=new List<ACC_PartnerProgram__c >();
        accpgmlst.add(accPgm);
        accpgmlst.add(accPgm2);
        insert accpgmlst;

        //Insert Custom Setting for the corresponding Opp Scope
        CS_ProductLineBusinessUnit__c prodLneBU = new CS_ProductLineBusinessUnit__c();
        prodLneBU.Name = 'TESTGMR';
        prodLneBU.ProductBU__c = 'PARTNER';
        prodLneBU.ProductLine__c = 'ENSL2--Energy Systems - Level 2';
        insert prodLneBU;
        
        Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
        contact1.PRMContact__c = true;
        insert contact1;
         
        Test.startTest();
        //Insert Master project 
       OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
       OPP_Project__c prj2 =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        List<OPP_Project__c> prjlist=new List<OPP_Project__c>();
        prjlist.add(prj);
        prjlist.add(prj2);
        insert prjlist;

        
        ContactAssignedProgram__c contactAssignedProgram = new ContactAssignedProgram__c();
        contactAssignedProgram.AccountAssignedProgram__c = accPgm.Id;
        contactAssignedProgram.Active__c = True;
        contactAssignedProgram.Contact__c = contact1.Id;
        contactAssignedProgram.PartnerProgram__c = countryProgram.id;
        contactAssignedProgram.ProgramLevel__c = prg.id;
        insert contactAssignedProgram;
        //Inserts Opportunity
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.TECH_CrossProcessConversionAmount__c = 23;
        opp2.Project__c=prj.Id;
        opp2.TECH_Partner_Program__c=countryProgram.Id;
        insert opp2;   
        //opportunityTeamMember opportunityTeam1 = new opportunityTeamMember(OpportunityID=opp2.id,userId=UserInfo.getUserId(),TeamMemberRole=System.label.CLOCT13PRM13);
        //insert opportunityTeam1;
        opp2.PartnerInvolvement__c = 'Partner Called-In';
        opp2.Amount=1234;
        opp2.Project__c=prj2.Id;
        opp2.Status__c = Label.CLOCT13SLS11;
        opp2.StageName = Label.CL00220;
        update opp2;
        
        try{
             opportunityTeamMember opportunityTeam1 = new opportunityTeamMember(OpportunityID=opp2.id,userId=UserInfo.getUserId(),TeamMemberRole=System.label.CLOCT13PRM13);
             insert opportunityTeam1;
            opp2.PartnerInvolvement__c = 'Partner Called-In';
            update opp2;
        }
        
        catch(Exception e){
            system.debug('Exception: ' + e);
        }
        delete opp2;
        Test.stopTest();
    }
    
    
    
    
    static testMethod void testOpportunityTriggersmethod3() {
      
     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
    
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='IN';
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        Insert acc;
        acc.isPartner = true;
        acc.PRMAccount__c = True;
        update acc;
    
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;
        Test.startTest();   
        Task createTask = AP_OpportunityTeamMember.createTask(opp2,u.Id);
        
        opportunityTeamMember opportunityTeam1 = new opportunityTeamMember(OpportunityID=opp2.id,userId=UserInfo.getUserId(),TeamMemberRole=System.label.CLOCT13PRM13);
        insert opportunityTeam1;
        
        List<opportunityTeamMember> lstOpptyTeam = new List<opportunityTeamMember>();
        lstOpptyTeam.add(opportunityTeam1);
        
        AP_OpportunityTeamMember.checkOpportunityPartnerInvolvement(lstOpptyTeam);
    Test.stopTest();
    
    }  
    static testMethod void testOpportunityTriggersmethod4() {
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='IN';
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        Insert acc;
        
        Opportunity opp = Utils_TestMethods.createOpportunity(acc.Id);
         opp.StageName='4 - Influence & Develop';
        insert opp;
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp.CloseDate=Date.today()+10;
        insert opp2;

        opp.StageName = Label.CL00272;
        opp.Status__c = Label.CLSEP12SLS14;
        update opp;
        opp.StageName='4 - Influence & Develop';
        update opp;
        Test.startTest();
  
        opp.StageName = Label.CL00272;
        opp.Status__c = Label.CLOCT13SLS08;
        update opp;
           
        opp.StageName='4 - Influence & Develop';
        update opp;
        opp.StageName =Label.CL00220;
        opp.Status__c = Label.CLOCT13SLS10 ;
        opp.CloseDate=Date.today()+1;
        opp.ParentOpportunity__c=opp2.Id;
        update opp;
        
        Test.stopTest();
    }
}