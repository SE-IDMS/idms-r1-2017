/*
    Author: Vamsi 
    Date: 09/02/2011
    Description:This class tests the AP03_ObjectiveSettingsTrigger class.
*/

@isTest
private class AP03_ObjectiveSettingsTrigger_Test { 
    static TestMethod void TestObjectiveSettingsTrigger() 
    {
        
        System.Debug('****** Test class AP03_ObjectiveSettingsTrigger_Test Started ****'); 
        AP03_ObjectiveSettingsTrigger.AP03TestClassFlag = False;
            
        User v = Utils_TestMethods.createStandardUser('AP03User');
        insert v;
        
        User u = Utils_TestMethods.createStandardUserWithManager('AP03Usr2', v);
        insert u;
        
        System.RunAs(u){
            Country__c newCountry = Utils_TestMethods.createCountry();
            Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
               
            if(!CountryInsertResult.isSuccess())
                Database.Error err = CountryInsertResult.getErrors()[0];      
               
            ObjectivesSettings__c newObjSettings = Utils_TestMethods.createObjectiveSettings();
            insert newObjSettings;
            
            ObjectivesDetail__c newObjDetail = Utils_TestMethods.createObjectiveDetails(newObjSettings.Id, newCountry.Id);
            insert newObjDetail; 
                
            // Create an approval request for the ObjectivesSettings
            /*Approval.ProcessSubmitRequest SubmitReq = new Approval.ProcessSubmitRequest();
            SubmitReq.setComments('Objective_Setting_Approval');
            SubmitReq.setObjectId(newObjSettings.id);
            
            // Submit the approval request for the ObjectivesSettings
            List<String> approverIds = new List<String>();
            approverIds.add(u.ManagerID);
            SubmitReq.setNextApproverIds(approverIds);
            
            Approval.ProcessResult result = Approval.process(SubmitReq);
               
            Approval.ProcessWorkitemRequest WorkItemReq = new Approval.ProcessWorkitemRequest();
            WorkItemReq.setComments('Approving request.');
            WorkItemReq.setAction('Approve');
            */               
            newObjSettings.PeriodType__c = 'Quarter';
            update newObjSettings;                                             
            
            newObjDetail.JanuaryAmount__c = 1000;
            update newObjDetail; 
            
            SubObjective__c newSubObj = Utils_TestMethods.createSubObjective(newObjDetail.Id);             
            insert newSubObj;  
            
            newObjSettings.Tech_ReportIsApproved__c = 'TRUE';
            newObjSettings.ObjectivesModifiedAfterApproval__c = False;
            update newObjSettings;
            
            newSubObj.Amount__c = 1000;
            update newSubObj;
            // Testing the Apporval Process         
           
            System.Debug('****** Test class AP03_ObjectiveSettingsTrigger_Test Ended****');  
        }                                                                  
    }}