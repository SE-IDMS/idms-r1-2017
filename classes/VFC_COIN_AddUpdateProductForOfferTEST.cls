/*****
Last Modified By:Hanamanth asper Q1 release 
******/

@isTest
private class VFC_COIN_AddUpdateProductForOfferTEST{

    static testMethod void AddUpdateProductforOfferTestMethod() {
    //Start of Test Data preparation
    
        Offer_Lifecycle__c objOffer = new Offer_Lifecycle__c();
        Database.insert(objOffer);
        
        objOffer.Product_BU__c = 'Business';
        objOffer.Product_Line__c = 'ProductLine';
        objOffer.Product_Family__c ='ProductFamily';
        objOffer.Family__c ='Family';
        //Database.insert(objOpenCase);
        Database.update(objOffer);
          
        ApexPages.StandardController OfferStandardController = new ApexPages.StandardController(objOffer);   
        VFC_COIN_AddUpdateProductForOffer GMRSearchPage = new VFC_COIN_AddUpdateProductForOffer(OfferStandardController );
        List<Opp_Product__c> product0 = new List<Opp_Product__c>();
        
        Opp_Product__c OPBU = new Opp_Product__c(Name = 'BU',TECH_PM0CodeInGMR__c='gmr', IsActive__c = true);
        product0.add(OPBU);
        
        Opp_Product__c OPPL = new Opp_Product__c(Name = 'PL', IsActive__c = true);
        
        Opp_Product__c OPPF = new Opp_Product__c(Name = 'PF', IsActive__c = true);
        Opp_Product__c OPFA = new Opp_Product__c(Name = 'FA', IsActive__c = true);
         insert product0; 
        
        
        Test.startTest();
        PageReference pageRef = Page.VFP_COIN_AddUpdateProductForOffer;  
        Test.setCurrentPage(pageRef);
       
         pageRef.getParameters().put('Business','gmr');
         pageRef.getParameters().put('ProductLine','gmr');
         pageRef.getParameters().put('ProductFamily','gmr');
         pageRef.getParameters().put('Family','gmr');
        GMRSearchPage.getOfferRecord();
        //GMRSearchPage.
        VFC_COIN_AddUpdateProductForOffer.getOthers('test');

        GMRSearchPage.PageSelectedFunction();
        
        GMRSearchPage.getBusinessO();
        GMRSearchPage.getProdLineO();
        GMRSearchPage.getProdFamilyO();
        GMRSearchPage.getFamilyO();


        GMRSearchPage.pageClearFunction();        
        GMRSearchPage.pageCancelFunction();
        
        GMRSearchPage.setOffer(objOffer);
        GMRSearchPage.getOffer();

        //ApexPages.currentPage().getParameters().get('myParamName')
        pageRef.getParameters().get('offer.Product_BU___c');
        pageRef.getParameters().get('offer.Product_Line___c');
        pageRef.getParameters().get('offer.Product_Family___c');
        pageRef.getParameters().get('offer.Family___c');
        
        GMRSearchPage.setBusinessO(OPBU);
        GMRSearchPage.setProdLineO(OPPL);
        GMRSearchPage.setProdFamilyO(OPPF);
        GMRSearchPage.setFamilyO(OPFA);
        
        GMRSearchPage.setOfferRecord(objOffer );
        
        //pageRef.getParameters().get('FamilyO');




        Test.stopTest();
    }
}