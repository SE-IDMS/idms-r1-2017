@isTest
private class AP_ResponseHandler_Test{
    static testMethod void myTest(){
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;

        system.runas(user1){
           Country__c country = Utils_TestMethods.createCountry();
            country.countrycode__c ='ASM';
            insert country;
            Country__c country1 = Utils_TestMethods.createCountry();
            country1.countrycode__c ='IN';
            insert country1;

            PartnerPRogram__c gp1 = new PartnerPRogram__c();
            Assessment__c asm = new Assessment__c();
            user1.country__c = country.countrycode__c;

            gp1 = Utils_TestMethods.createPartnerProgram();
            gp1.TECH_CountriesId__c = country.id;
            gp1.Country__c = country.id;
            gp1.recordtypeid = Label.CLMAY13PRM15;
            gp1.ProgramStatus__c = Label.CLMAY13PRM47;
            gp1.Name = Label.CLAPR14PRM25;
            insert gp1;

            ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
            prg1.Name = Label.CLAPR14PRM25;
            insert prg1;
            prg1.levelstatus__c = 'active';
            update prg1;

            PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
            cp1.Name = Label.CLAPR14PRM25;
            insert cp1;
            cp1.ProgramStatus__c = 'active';
            update cp1;

            ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
            prg2.Name = Label.CLAPR14PRM25;
            insert prg2;
            prg2.levelstatus__c = 'active';
            update prg2;

            ProgramLevel__c prgLevel = new ProgramLevel__c();
            prgLevel.PartnerProgram__c = gp1.Id;
            prgLevel.GlobalProgramLevel__c = prg1.id;
            Insert prgLevel;
            
            Recordtype prgORFAssessmentRecordType = [Select id from RecordType where developername =  'PRMProgramApplication' limit 1];
            
            Assessment__c assessment = new Assessment__c();
            assessment = Utils_TestMethods.createAssessment();
            assessment.recordTypeId = prgORFAssessmentRecordType.Id ;
            assessment.AutomaticAssignment__c = true;
            assessment.Active__c = true;
            assessment.Name = 'Sample Assessment';
            assessment.PartnerProgram__c = gp1.id;
            assessment.ProgramLevel__c = prg1.id;
            insert assessment;
            
            OpportunityAssessmentQuestion__c oppQues = new OpportunityAssessmentQuestion__c();
            oppQues.AnswerType__c = 'Text';
            oppQues.Assessment__c = assessment.Id;
            oppQues.Required__c = True;
            insert oppQues;

            //Account acc = [SELECT Id FROM Account WHERE PRMAccount__c = true AND SEAccountID__c != null LIMIT 1];
            Account acc = new Account();
            acc.Name = 'Test Account';
            acc.Country__c = country.id;
            System.debug('Acccountry.id'+country.id);
            acc.SEAccountId__c = '12345';
            insert acc;

            AccountAssessment__c accAssessment = new AccountAssessment__c();
            accAssessment.Account__c = acc.Id;//'00117000002PfhaAAC';
            accAssessment.PartnerProgram__c = gp1.Id;
            accAssessment.ProgramLevel__c = prg1.Id;
            insert accAssessment;

            PartnerAssessment__c pAssessment = new PartnerAssessment__c();
            pAssessment.Account__c = acc.Id;//'00117000002PfhaAAC';
            pAssessment.Assessment__c = assessment.Id;
            try {
            insert pAssessment;
            }
            catch(Exception e) {
                System.debug('Error');
            }
            OpportunityAssessmentResponse__c oppres = new OpportunityAssessmentResponse__c();
            oppres.Question__c = oppQues.Id;
            oppres.PartnerAssessment__c = pAssessment.Id;
            insert oppres;
            
            test.starttest();
            acc.isPartner = true;
            acc.PRMAccount__c = True;
            update acc;
            
            test.stopTest();
            
            ACC_PartnerProgram__c accProgram = new ACC_PartnerProgram__c();
            accProgram.Account__c = acc.Id;//'00117000002MViIAAW';//'00117000002PfhaAAC';
            accProgram.PartnerProgram__c = cp1.Id;
            accProgram.ProgramLevel__c = prg2.Id;
            insert accProgram;
        }
    }
}