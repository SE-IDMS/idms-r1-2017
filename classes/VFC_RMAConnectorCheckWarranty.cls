/*
    Author          : Ram Chilukuri
    Date Created    : 25/Feb/2014
    Description     : Class utilised by Synchronize Button on Return Request .
*/
public class VFC_RMAConnectorCheckWarranty 
 {
    public String RetunRequestId;
    public string CWflag;
    public String userID;
    public RMA__c returnRequest;    
    public Boolean flag{get;set;}
    public List<BackOfficesystem__c> backOffList{get;set;}    
    public set<string> setofbackoffices{get;set;}
    public String RRURL;    
    public List<SelectOption> options{get;set;} 
    public String selectedoption{get;set;} 
    //public List<RMA_Product__c> returnItemsList =new  List<RMA_Product__c> ();
    public List<String> ritems =new List<String>();
        
        public VFC_RMAConnectorCheckWarranty (ApexPages.StandardController controller)
            {
                RetunRequestId=Controller.getID(); 
                options = new List<SelectOption>();
                CWflag= system.currentpagereference().getParameters().get('CW'); 
                setofbackoffices = new set<string>();
            }            
        public pageReference redirectToextRefereceExternalSystem()
            {           
               flag=false;        
                System.Debug('***** CWflag***'+CWflag);   
            if(RetunRequestId!=null)
                {
                    returnRequest = [select AccountName__r.Country__r.Name,BackOfficeId__c, AccountName__r.SEAccountID__c, AccountCountry__r.CountryCode__c , AccountName__r.Country__r.FrontOfficeOrganization__r.Name,CountryofBackOfficeSystem__c,BackOfficeSystem__c, TECH_RequestType__c from RMA__c where ID=:RetunRequestId];                    
                     System.debug(' returnRequest '+ returnRequest );
                     System.debug(' returnRequestCountry '+ returnRequest.AccountName__r.Country__r.Name );
                     map<string,List<BackOfficesystem__c>> mapFrOffOrgBO=new map<string,List<BackOfficesystem__c>>();
                     map<string,List<BackOfficesystem__c>> mapCountryBO=new map<string,List<BackOfficesystem__c>>();
                    if(CWflag=='no')                   
                        {
                            if (returnRequest.BackOfficeId__c==null ||returnRequest.BackOfficeId__c=='')                     
                                {
                                    List<RMA_Product__c> returnItemsListOld = new  List<RMA_Product__c> ();                                          
                                         returnItemsListOld =[select name , WarrantyStatus__c, BillingMethod__c from RMA_Product__c where RMA__c =:returnRequest.id ];
                                         System.debug('%%%%% returnItemsListOld '+ returnItemsListOld );
                                         for(RMA_Product__c tritem: returnItemsListOld)
                                            {
                                                if (tritem.WarrantyStatus__c==null)
                                                    {
                                                        ritems.add(tritem.name);
                                                    }
                                                   
                                            }
                                            if (ritems.size()>0)
                                                    {                                                                                      
                                                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLAPR14RRC05+ritems));  
                                                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLAPR14RRC06));        
                                                        return null; 
                                                    }
                                }
                            else{
                                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLAPR14RRC04));          
                                    return null; 
                                }
                    
                        }
                    List<BackOfficesystem__c> bolist= [select id,BackOfficeSystem__c, FrontOfficeOrganization__r.Name,Country__r.Name from BackOfficesystem__c where (Country__c !=:null or FrontOfficeOrganization__c!=:null) and Deployed__c=true];
                     System.debug('***********bolist.size()'+bolist.size());
                    
                        for(BackOfficesystem__c bo:bolist)
                            {
                                if(mapFrOffOrgBO.containsKey(bo.FrontOfficeOrganization__r.Name)){
                                        mapFrOffOrgBO.get(bo.FrontOfficeOrganization__r.Name).add(bo);
                                    }
                                else
                                    {
                                        mapFrOffOrgBO.put(bo.FrontOfficeOrganization__r.Name, new List<BackOfficesystem__c>());
                                        mapFrOffOrgBO.get(bo.FrontOfficeOrganization__r.Name).add(bo);
                                    }   
                                if(mapCountryBO.containsKey(bo.Country__r.Name)){
                                        mapCountryBO.get(bo.Country__r.Name).add(bo);
                                    }
                                else
                                    {
                                        mapCountryBO.put(bo.Country__r.Name, new List<BackOfficesystem__c>());
                                        mapCountryBO.get(bo.Country__r.Name).add(bo);
                                    }      
                                    System.debug('***********mapCountryBO'+mapCountryBO);
                                    System.debug('***********mapFrOffOrgBO'+mapFrOffOrgBO);
                            }                 
                                 
                                if(mapFrOffOrgBO.containskey(returnRequest.AccountName__r.Country__r.FrontOfficeOrganization__r.Name))
                                {
                                    backOffList=mapFrOffOrgBO.get(returnRequest.AccountName__r.Country__r.FrontOfficeOrganization__r.Name);
                                    System.debug('***********backOffListFO'+backOffList);
                                    System.debug('***********backOffListFObackOffList.size()'+backOffList.size());
                                    if(backOffList.size()==1)
                                    {                                    
                                        for(BackOfficesystem__c tempBO:backOffList)
                                            {
                                                returnRequest.BackOfficesystem__c=tempBO.BackOfficesystem__c;
                                        
                                            }
                                            update returnRequest;
                                            return connectURL();
                                    }
                                    else if(backOffList.size()>1)
                                        {
                                            flag=true;
                                            for(BackOfficesystem__c tempBO:backOffList)
                                                {
                                               system.debug('&&&&&&&back'+ tempBO.BackOfficeSystem__c);
                                                    setofbackoffices.add(tempBO.BackOfficeSystem__c);
                            
                                                }                                               
                                        }
                                }
                                else if(mapCountryBO.containskey(returnRequest.AccountName__r.Country__r.Name))
                                    {
                                        backOffList=mapCountryBO.get(returnRequest.AccountName__r.Country__r.Name);
                                        System.debug('***********backOffListcO'+backOffList);
                                    System.debug('***********backOffListcObackOffList.size()'+backOffList.size());
                                        if(backOffList.size()==1)
                                        {                                         
                                            for(BackOfficesystem__c tempBO:backOffList)
                                            {
                                                returnRequest.BackOfficesystem__c=tempBO.BackOfficesystem__c;
                                        
                                            }
                                           update returnRequest;
                                            return connectURL();
                                        }
                                        else if(backOffList.size()>1)
                                        {
                                        flag=true;
                                            for(BackOfficesystem__c tempBO:backOffList)
                                                {
                                                    setofbackoffices.add(tempBO.BackOfficesystem__c);
                            
                                                }                                                
                                        }
                                    }
                                else
                                    {
                                        flag=false;  
                                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLAPR14RRC03));  
                                    }  
                                                   
                }   
                     
                System.debug('\n*********************'+setofbackoffices);
                if(setofbackoffices != null && setofbackoffices.size()>0)
                {
                   for(String tempBackoff:setofbackoffices){
                     options.add(new SelectOption(tempBackoff,tempBackoff));
                }
                
                }
               // flag=true;
                return null;
             }
        
public pageReference  doProcess(){
              
            returnRequest.BackOfficesystem__c=selectedoption;        
            update  returnRequest;
            return connectURL();   
}
Public Pagereference  doCancel()
   {
   PageReference pr = new PageReference('/'+returnRequest.id);
   pr.setRedirect(true);
     return pr;
   }
public pageReference connectURL(){
        string rwsyid;
        if(CWflag=='yes'){
                rwsyid= Label.CLAPR14RRC02; //rwsyid= 'RW.UPD'
            } 
        else{
                rwsyid= Label.CLAPR14RRC05; //rwsyid= 'RR.SYNC';
            }
        RRURL = (System.label.CLAPR14RRC01+
        rwsyid+ Label.CLAPR14RRC08+returnRequest.AccountName__r.SEAccountID__c+        
                Label.CLAPR14RRC09+ Label.CLAPR14RRC07+        
                Label.CLAPR14RRC10+returnRequest.id +        
                Label.CLAPR14RRC11+returnRequest.AccountCountry__r.CountryCode__c +        
                Label.CLAPR14RRC12+returnRequest.BackOfficeSystem__c +
                Label.CLJAN16RRC01+UserInfo.getSessionId());
       // '&soref='+returnRequest.OriginalSalesOrder__c) ;                                                  
        System.debug(' RRURL '+ RRURL + UserInfo.getName());
        return new pageReference(RRURL+'&view=Yes');
    }  
}