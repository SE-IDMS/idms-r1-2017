global class Batch_LiveAgent_UpdateTranscripts implements Database.batchable<Sobject> {
    
    global Map<String, String> CountryNamesMapping;
    
    global Batch_LiveAgent_UpdateTranscripts() {
        CountryNamesMapping = new Map<String, String>();
        String[] mappingList = System.Label.CLSEP16CCCLA18.split(';');
        for (String mapping : mappingList) {
            if (String.isNotBlank(mapping)) {
                String[] mappingCouple = mapping.split(',');
                if (mappingCouple.size() > 1) {
                    if (String.isNotBlank(mappingCouple[0]) && String.isNotBlank(mappingCouple[1])) {
                        CountryNamesMapping.put(mappingCouple[0].trim(), mappingCouple[1].trim());
                    }
                }
            }
        }
    }
    
    global Database.QueryLocator start (Database.BatchableContext bc) {
        String query = ' SELECT Id, LiveChatDeploymentId, DeploymentName__c, Application__c, ';
        query += ' LiveChatButtonId, AssignedToChatButtonName__c, ';
        query += ' ReportingChatButtonId__c, ReportingChatButtonName__c, ChatType__c, ';
        query += ' SourceChatButtonId__c, SourceChatButtonName__c, ';
        query += ' Location, RegionCode__c, CountryCode__c, Country__c, ';
        query += ' ChatKey, ChatKey__c, OwnerId, CreatedDate, RecalculateTranscriptFields__c ';
        query += ' FROM LiveChatTranscript ';
        query += ' WHERE CreatedDate = LAST_N_DAYS:10 ';
        query += ' OR (ReportingChatButtonId__c != \'\' AND ReportingChatButtonName__c = \'\') ';
        query += ' OR (SourceChatButtonId__c != \'\' AND SourceChatButtonName__c = \'\') ';
        query += ' OR ReportingChatButtonId__c = \'\' ';
        query += ' OR SourceChatButtonId__c = \'\' ';
        query += ' OR DeploymentName__c = \'\' ';
        query += ' OR Application__c = \'\' ';
        query += ' OR RegionCode__c = \'\' ';
        query += ' OR CountryCode__c = \'\' ';
        query += ' OR Country__c = NULL ';
        query += ' OR (ChatKey != \'\' AND ChatKey__c = \'\') ';
        query += ' OR OwnerId = \'' + System.Label.CLNOV16CCCLA01 + '\'';
        query += ' OR RecalculateTranscriptFields__c = true ';
        
        LiveChatButton[] updatedChatButtons = [select Id from LiveChatButton where LastModifiedDate = LAST_N_DAYS:10];
        if (Test.isRunningTest()) updatedChatButtons = [select Id from LiveChatButton];
        if (!updatedChatButtons.isEmpty()) {
        	String buttonList = '(';
            for (LiveChatButton b : updatedChatButtons) {
                buttonList += '\'' + b.Id + '\',';
            }
            buttonList = buttonList.left(buttonList.length()-1);
        	buttonList += ')';
            query += ' OR LiveChatButtonId IN ' + buttonList;
            query += ' OR ReportingChatButtonId__c IN ' + buttonList;
            query += ' OR SourceChatButtonId__c IN ' + buttonList;
        }
        
        LiveChatDeployment[] updatedChatDeployments = [select Id from LiveChatDeployment where LastModifiedDate = LAST_N_DAYS:10];
        if (Test.isRunningTest()) updatedChatDeployments = [select Id from LiveChatDeployment];
        if (!updatedChatDeployments.isEmpty()) {
        	String deploymentList = '(';
            for (LiveChatDeployment d : updatedChatDeployments) {
                deploymentList += '\'' + d.Id + '\',';
            }
            deploymentList = deploymentList.left(deploymentList.length()-1);
        	deploymentList += ')';
            query += ' OR LiveChatDeploymentId IN ' + deploymentList;
        }
        
        system.debug('***** Query: ' + query); 
        system.debug('***** QueryResult: ' + Database.getQueryLocator(query));    
        return  Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, LiveChatTranscript[] scope) {
        
        System.debug('***** Number of transcripts: ' + scope.size());
        
        String scopeIds = 'Number of transcripts: ' + scope.size() + '\n';
        
        Set<Id> allChatButtonIds = new Set<Id>();
        Set<Id> deploymentIds = new Set<Id>();
        Set<String> regionCodes = new Set<String>();
        Set<String> countryCodes = new Set<String>();
        Set<String> countryNames = new Set<String>();
        
        Id buttonId18;
        for (LiveChatTranscript lct : scope) {
            scopeIds += lct.Id + ', ';
            if (lct.LiveChatButtonId != null) {
            	allChatButtonIds.add(lct.LiveChatButtonId);
            }
            if (lct.ReportingChatButtonId__c != null) {
            	allChatButtonIds.add(lct.ReportingChatButtonId__c);
            }
            if (lct.SourceChatButtonId__c != null) {
            	allChatButtonIds.add(lct.SourceChatButtonId__c);
            }
            if (lct.LiveChatDeploymentId != null) {
                deploymentIds.add(lct.LiveChatDeploymentId);
            }
            if (lct.CountryCode__c != null) {
                countryCodes.add(lct.CountryCode__c);
            }
            if (lct.Location != null) {
                String countryFromLocation = getCountryFromLocation(lct);
                if (countryFromLocation != null) {
                    countryNames.add(countryFromLocation);
                }
            }
        }
        
        // Log Ids in scope
     	if (System.Label.CLDEC16CCCLA02 == '1' || Test.IsRunningTest()) {
            createLog(scopeIds);
        }
        
        Map<ID,LiveChatButton> chatButtons = new Map<ID,LiveChatButton>([select Id, MasterLabel, Type from LiveChatButton where Id in : allChatButtonIds]);
        System.debug('***** Number of LiveChatButton: ' + chatButtons.size());
        
        Map<ID,LiveChatDeployment> chatDeployments = new Map<ID,LiveChatDeployment>([select Id, MasterLabel from LiveChatDeployment where Id in : deploymentIds]);
        System.debug('***** Number of LiveChatDeployment: ' + chatDeployments.size());
        
        String countryRegionCode;
        for (LiveChatDeployment dpl : chatDeployments.values()) {
            countryRegionCode = getCountryOrRegionCode(dpl);
            if (countryRegionCode.length() == 2) {
                countryCodes.add(countryRegionCode);
            }
        }
        
        Map<String, Country__c> countryByCode = new Map<String, Country__c>();
        Map<String, Country__c> countryByName = new Map<String, Country__c>();
        for (Country__c country : [select Id, Name, CountryCode__c from Country__c where Name in : countryNames or CountryCode__c in : countryCodes]) {
            countryByCode.put(country.CountryCode__c, country);
            countryByName.put(country.Name, country);
        }
        System.debug('***** Number of Country__c: ' + countryByCode.size());
        
        LiveChatTranscript[] transcriptsToUpdate = new LiveChatTranscript[0];
        for (LiveChatTranscript lct : scope) {
            system.debug('***** lct: ' + lct);
            Boolean updated = false;
            
            if (lct.RecalculateTranscriptFields__c) {
                lct.RecalculateTranscriptFields__c = false;
                updated =  true;
            }
            
            // Chat Key is an invisible field
            // Copied in the ChatKey__c field to be used
            if (String.isBlank(lct.ChatKey__c) && String.isNotBlank(lct.ChatKey)) {
                lct.ChatKey__c = lct.ChatKey;
                updated =  true;
            }
            
            // Copy Live Chat Button field into Assigned To Chat Button Name
            String assignedToButtonName = chatButtons.get(lct.LiveChatButtonId).MasterLabel;
            if (lct.AssignedToChatButtonName__c != assignedToButtonName) {
                lct.AssignedToChatButtonName__c = assignedToButtonName;
                updated = true;
                system.debug('***** NEW lct.AssignedToChatButtonName__c: ' + lct.AssignedToChatButtonName__c);
            }
            
            // Populate the Reporting Chat Button Name
            if (String.isBlank(lct.ReportingChatButtonId__c)) {
                lct.ReportingChatButtonId__c = lct.LiveChatButtonId;
                updated = true;
            }
            // Update button ids to 18-char
            buttonId18 = Id.valueOf(lct.ReportingChatButtonId__c);
            if (lct.ReportingChatButtonId__c != String.valueOf(buttonId18)) {
                system.debug('***** update ReportingChatButtonId__c: ' + buttonId18);
                lct.ReportingChatButtonId__c = buttonId18;
                updated =  true;
            }
            String reportingButtonName = chatButtons.get(lct.ReportingChatButtonId__c).MasterLabel;
            if (lct.ReportingChatButtonName__c != reportingButtonName) {
                lct.ReportingChatButtonName__c = reportingButtonName;
                updated = true;
                system.debug('***** NEW lct.ReportingChatButtonName__c: ' + lct.ReportingChatButtonName__c);
            }
        	
            // Populate the Managing Chat Button Name (API Name: SourceChatButtonId__c)
            if (String.isBlank(lct.SourceChatButtonId__c)) {
                lct.SourceChatButtonId__c = lct.LiveChatButtonId;
                updated = true;
            }
            // Update button ids to 18-char
            buttonId18 = Id.valueOf(lct.SourceChatButtonId__c);
            if (lct.SourceChatButtonId__c != String.valueOf(buttonId18)) {
                system.debug('***** update SourceChatButtonId__c: ' + buttonId18);
                lct.SourceChatButtonId__c = buttonId18;
                updated =  true;
            }
            String sourceButtonName = chatButtons.get(lct.SourceChatButtonId__c).MasterLabel;
            if (lct.SourceChatButtonName__c != sourceButtonName) {
                lct.SourceChatButtonName__c = sourceButtonName;
                updated = true;
                system.debug('***** NEW lct.SourceChatButtonName__c: ' + lct.SourceChatButtonName__c);
            }
            
            // Populate the chat type (Chat Button or Chat Invite) based on the source chat button
            String chatType = chatButtons.get(lct.ReportingChatButtonId__c).Type;
            String chatTypeLabel = chatType == System.Label.CLSEP16CCCLA07 ? System.Label.CLSEP16CCCLA05 : System.Label.CLSEP16CCCLA06;
            if (lct.ChatType__c != chatTypeLabel) {
                lct.ChatType__c = chatTypeLabel;
                updated = true;
                system.debug('***** NEW lct.ChatType__c: ' + lct.ChatType__c);
            }
            
            // Populate the Chat Deployment Name and the Application Name
            if (lct.LiveChatDeploymentId != null) {
                String deploymentName = chatDeployments.get(lct.LiveChatDeploymentId).MasterLabel;
                if (lct.DeploymentName__c != deploymentName) {
                    lct.DeploymentName__c = deploymentName;
                    updated = true;
                    system.debug('***** NEW lct.DeploymentName__c: ' + lct.DeploymentName__c);
                }
                
                String[] deploymentNameSplit = deploymentName.split('-');
                if (String.isNotBlank(deploymentNameSplit[1])) {
                    String appName = deploymentNameSplit[1].trim();
                    if (lct.Application__c == null || lct.Application__c != appName) {
                        lct.Application__c = appName;
                        updated = true;
                    }
                }
            }
            
            // Populate Country Code
            Country__c country;
            countryRegionCode = getCountryOrRegionCode(chatDeployments.get(lct.LiveChatDeploymentId));
            System.debug('***** countryRegionCode: ' + countryRegionCode);
            // WW is not a country code so it is set to blank
            if (String.isNotBlank(lct.CountryCode__c) && lct.CountryCode__c.toUpperCase() == 'WW') {
                lct.CountryCode__c = '';
                updated = true;
            }
            if (String.isBlank(lct.CountryCode__c)) {
                if (countryRegionCode.length() == 2) {
                    lct.CountryCode__c = countryRegionCode;
                    updated = true;
                } else if (String.isNotBlank(lct.Location)) {
                    String countryName = getCountryFromLocation(lct);
                    System.debug('***** countryName: ' + countryName);
                    if (String.isNotBlank(countryName)) {
                        if (countryName.toLowerCase() == 'unknown' && countryRegionCode == 'NAM') {
                            countryName = 'USA';
                        }
                        country = CountryByName.get(countryName);
                        System.debug('***** country: ' + country);
                        if (country != null) {
                            lct.CountryCode__c = country.CountryCode__c;
                            updated = true;
                        }
                    }
                }
            }
            if (String.isNotBlank(lct.CountryCode__c)) {
                if (!lct.CountryCode__c.isAllUpperCase()) {
                    lct.CountryCode__c = lct.CountryCode__c.toUpperCase();
                    updated = true;
                }
                // Populate Country lookup field
                country = CountryByCode.get(lct.CountryCode__c);
                if (country != null) {
                    if (lct.Country__c == null || lct.Country__c != country.Id) {
                        lct.Country__c = country.Id;
                        updated = true;
                    }
                }
            }
            // Populate Region Code
            if (String.isBlank(lct.RegionCode__c)) {
                if (countryRegionCode != null && countryRegionCode.length() < 6) {
                    lct.RegionCode__c = countryRegionCode;
                    updated = true;
                } else if (String.isNotBlank(lct.CountryCode__c)) {
                    lct.RegionCode__c = lct.CountryCode__c;
                    updated = true;
                }
            }
            
            // Reassigned missed chats assigned to Automated Process User
            if (lct.OwnerId == System.Label.CLNOV16CCCLA01) {
                if (lct.CountryCode__c == 'DE') {
                    lct.OwnerId = Test.isRunningTest() ? System.Label.CLNOV16CCCLA01 : System.Label.CLNOV16CCCLA03;
                    updated = true;
                } else {
                    lct.OwnerId = Test.isRunningTest() ? UserInfo.getUserId() : System.Label.CLNOV16CCCLA02;
                    updated = true;
                }
            }
            
            if (updated) {
                transcriptsToUpdate.add(lct);
            }
        }
        if (!transcriptsToUpdate.isEmpty()) {
            Database.SaveResult[] srList = Database.update(transcriptsToUpdate, false);
            String orgId = UserInfo.getOrganizationId();
            if (System.Label.CLDEC16CCCLA01 == '1' || Test.IsRunningTest()) {
                Integer nbErrors = 0;
                String errors = '';
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess() || Test.IsRunningTest()) {
                        nbErrors += 1;
                        errors += 'Transcripts Batch Update error for ID:' + sr.getId() + '\n';
                        // Operation failed, so get all errors       
                        for(Database.Error err : sr.getErrors()) {                   
                            errors += 'Error: ' + err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            errors += 'Transcript fields that affected this error: ' + err.getFields() + '\n';
                        }
                    }
                }
                if (nbErrors > 0 || Test.IsRunningTest()) {
                    String description = 'Found ' + nbErrors + ' errors \n' + errors;
                    createLog(description);
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
    
    private String getCountryFromLocation(LiveChatTranscript transcript) {
        String[] locationSplit = transcript.Location.split(',');
        System.debug('***** locationSplit: ' + locationSplit);
        if (locationSplit.size() > 2 && String.isNotBlank(locationSplit[2])) {
            String locationSplitCountry = locationSplit[2].trim();
            if (CountryNamesMapping.containsKey(locationSplitCountry)) {
                return CountryNamesMapping.get(locationSplitCountry);
            } else {
                return locationSplitCountry;
            }
        } else {
            return null;
        }
    }
    
    private String getCountryOrRegionCode(LiveChatDeployment chatDeployment) {
        return chatDeployment.MasterLabel.split('-')[0].trim().toUpperCase();
    }
    
    private void createLog(String description) {
        Database.DMLOptions DMLoption = new Database.DMLOptions(); 
        DMLoption.optAllOrNone = false;
        DMLoption.AllowFieldTruncation = true;
        DebugLog__c log = new DebugLog__c();
        //log.StackTrace__c = ex.getStackTraceString();
        //log.ExceptionType__c = ex.getTypeName();
        log.Description__c = description;
        log.OwnerId = System.Label.CLOCT14CCC24; // System Interface User
        DataBase.SaveResult logInsertSaveResult = database.insert(log, DMLoption);
    }
}