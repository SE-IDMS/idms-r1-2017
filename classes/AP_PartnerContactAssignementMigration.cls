public with sharing class AP_PartnerContactAssignementMigration {


	public static void createAndDeleteMemberProperties(Map<Id ,ContactAssignedProgram__c> triggerOldMap,Map<Id ,ContactAssignedProgram__c> triggerNewMap){
		//Akram for Q2 release
        Map<Id ,Set<String>> badgesByContactToAssign = new Map<Id ,Set<String>>();
        Map<Id ,Set<String>> badgesByContactToDelete = new Map<Id ,Set<String>>();

        if(triggerOldMap!=null){
        	for(Id ContactPrgID : triggerNewMap.keySet()){
        		if(triggerNewMap.get(ContactPrgID).Active__c != triggerOldMap.get(ContactPrgID).Active__c){
	    			Map<Id ,Set<String>> badgesByContact = triggerNewMap.get(ContactPrgID).Active__c?badgesByContactToAssign:badgesByContactToDelete;
	                if(badgesByContact.containsKey(triggerNewMap.get(ContactPrgID).Contact__c)){
	                    badgesByContact.get(triggerNewMap.get(ContactPrgID).Contact__c).add(triggerNewMap.get(ContactPrgID).ProgramLevel__c);
	                }else{
	                    badgesByContact.put(triggerNewMap.get(ContactPrgID).Contact__c, new Set<String>{triggerNewMap.get(ContactPrgID).ProgramLevel__c});
	                }
        		}

        		if(triggerNewMap.get(ContactPrgID).ProgramLevel__c != triggerOldMap.get(ContactPrgID).ProgramLevel__c)
	            {
	                if(badgesByContactToAssign.containsKey(triggerNewMap.get(ContactPrgID).Contact__c)){
	                    badgesByContactToAssign.get(triggerNewMap.get(ContactPrgID).Contact__c).add(triggerNewMap.get(ContactPrgID).ProgramLevel__c);
	                }else{
	                    badgesByContactToAssign.put(triggerNewMap.get(ContactPrgID).Contact__c, new Set<String>{triggerNewMap.get(ContactPrgID).ProgramLevel__c});
	                }
	                //Akram for Q2 release: delete memberProperties
	                if(badgesByContactToDelete.containsKey(triggerOldMap.get(ContactPrgID).Contact__c)){
	                    badgesByContactToDelete.get(triggerOldMap.get(ContactPrgID).Contact__c).add(triggerNewMap.get(ContactPrgID).ProgramLevel__c);
	                }else{
	                    badgesByContactToDelete.put(triggerOldMap.get(ContactPrgID).Contact__c, new Set<String>{triggerNewMap.get(ContactPrgID).ProgramLevel__c});
	                }
	            }
        	}
        }else{
        	for(Id ContactPrgID : triggerNewMap.keySet()){
        		if(badgesByContactToAssign.containsKey(triggerNewMap.get(ContactPrgID).Contact__c)){
                    badgesByContactToAssign.get(triggerNewMap.get(ContactPrgID).Contact__c).add(triggerNewMap.get(ContactPrgID).ProgramLevel__c);
                }else{
                	system.debug('triggerNewMap.get(ContactPrgID).Contact__c: '+triggerNewMap.get(ContactPrgID).Contact__c);
                	system.debug('triggerNewMap.get(ContactPrgID).ProgramLevel__c: '+triggerNewMap.get(ContactPrgID).ProgramLevel__c);
                    badgesByContactToAssign.put(triggerNewMap.get(ContactPrgID).Contact__c, new Set<String>{triggerNewMap.get(ContactPrgID).ProgramLevel__c});
                }
	        }
        }
        system.debug('badgesByContactToAssign: '+badgesByContactToAssign);
        system.debug('badgesByContactToDelete: '+badgesByContactToDelete);
        if(!badgesByContactToAssign.isEmpty())
        	PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactToAssign);
        if(!badgesByContactToDelete.isEmpty())
        	PRM_MemberExternalPropertiesUtils.removeMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactToDelete);
	}



    public static void deleteMemberProperties(Map<Id ,ContactAssignedProgram__c> triggerOldMap){
        Map<Id ,Set<String>> badgesByContactToDelete = new Map<Id ,Set<String>>();

        for(Id ContactPrgID : triggerOldMap.keySet()){
            if(badgesByContactToDelete.containsKey(triggerOldMap.get(ContactPrgID).Contact__c)){
                badgesByContactToDelete.get(triggerOldMap.get(ContactPrgID).Contact__c).add(triggerOldMap.get(ContactPrgID).ProgramLevel__c);
            }else{
                system.debug('triggerOldMap.get(ContactPrgID).Contact__c: '+triggerOldMap.get(ContactPrgID).Contact__c);
                system.debug('triggerOldMap.get(ContactPrgID).ProgramLevel__c: '+triggerOldMap.get(ContactPrgID).ProgramLevel__c);
                badgesByContactToDelete.put(triggerOldMap.get(ContactPrgID).Contact__c, new Set<String>{triggerOldMap.get(ContactPrgID).ProgramLevel__c});
            }
        }
        if(!badgesByContactToDelete.isEmpty())
            PRM_MemberExternalPropertiesUtils.removeMemberExternalPropertyByContactId('BFO','MigrationToProgramV2', badgesByContactToDelete);
    }
}