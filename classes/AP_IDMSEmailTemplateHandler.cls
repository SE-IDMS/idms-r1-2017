Public Class AP_IDMSEmailTemplateHandler{
    
    Public Static IDMSEmailTable__c GetEmailTemplateCS(String AppName, String lang){
        IDMSEmailTable__c result;
        String appEmailTemplate;
        
        if(string.isNotBlank(AppName) != null && string.isNotBlank(lang)){
            appEmailTemplate = AppName + '_' + lang;
            System.debug('Email template name constructed dynamically:'+appEmailTemplate);
            
            //Get app email template from custom setting. 
            IDMSEmailTable__c idmsEmailFinder;

            idmsEmailFinder = IDMSEmailTable__c.getInstance(appEmailTemplate);
            if(idmsEmailFinder != null){
                result = idmsEmailFinder;
            }else{
                appEmailTemplate = AppName + '_'+ 'default';
                idmsEmailFinder = IDMSEmailTable__c.getInstance(appEmailTemplate);
                if(idmsEmailFinder != null){
                    result = idmsEmailFinder; 
                }else{
                    result = IDMSEmailTable__c.getInstance('Idms_default'); // To be stored in a CL
                }
            }
        } else {
            result = IDMSEmailTable__c.getInstance('Idms_default'); // To be stored in a CL
        }
        
        if(result != null){
        	system.debug('email Template CS record to be returned as result is: '+result.Name);
        }
        return result;
    }
    
}