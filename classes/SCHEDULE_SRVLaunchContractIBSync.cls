global class SCHEDULE_SRVLaunchContractIBSync implements Schedulable{
   global void execute(SchedulableContext ctx) {
        BatchSRVLaunchContractIBSync  batch = new BatchSRVLaunchContractIBSync ();
        ID batchprocessid = Database.executeBatch(batch);
    }
}