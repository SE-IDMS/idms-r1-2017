/*
Author: Siddharth N(GD Solutions)
Purpose:Extension for VFP_SPALineItemMassCreate page and it has methods to prepare a sobjectwrapper for line items
        and validate csv headers.
Methods:VFC_SPALineItemMassCreate
        populate data into Sparequestrecord to be used by the section header on VFP_SPALineItemMassCreate
*/
 public with sharing class VFC_SPALineItemMassCreate {

    public StringListWrapper csvList{get;set;}{csvList=new StringListWrapper();} //the wrapper which is exchanged between componentcontroller and current extension
    public List<LineItemWrapper> lineItemWrapperRecords{get;set;}{lineItemWrapperRecords=new List<LineItemWrapper>();}
    public SPARequest__c sparequestrecord{get;set;}{sparequestrecord=new SPARequest__c();}
    public Map<String,CS_SPALineItemCSVMap__c> csvNameAPINameMap = new Map<String,CS_SPALineItemCSVMap__c>();
    public boolean isTableReady{get;set;}{isTableReady=false;}      
    public Map<String,boolean> isRequiredMap{get;set;}{isRequiredMap=new Map<String,boolean>();}
    public Map<String,String> headerAPItoCSVMap{get;set;}{headerAPItoCSVMap=new Map<String,String>();}
    public boolean hasErrors{get;set;}{hasErrors=false;}
    public boolean displayReadOnlyFields{get;set;}{displayReadOnlyFields=false;}
    public boolean hasNoLineItemErrors{get;set;}{hasNoLineItemErrors=true;}
    public Set<String> justRequiredMap=new Set<String>();
    public boolean disableEverything{get;set;}{disableEverything=false;}
    public String type{get;set;}{type=null;}
    public boolean flag{get;set;}

    //Option to choose either LPG or CR 
      public void typeSelect()
     {
          System.debug('>>>>>>Type is>>>>'+type);
          this.type = type;
          if(type==null || type=='')
              sparequestrecord.addError(System.Label.CLOCT14SLS34);
              
     }
    
    public VFC_SPALineItemMassCreate(ApexPages.StandardSetController controller) {
        System.debug('>>>>> Constructor csv size******'+csvList.stringList.size());
        System.debug('>>>>> Constructor type******'+type);
        /*flag is used to control the UI as middleware and bFO deployment is not on the same day 
        before middleware deployment CLOCT14SLS53 is set to SPA OCT14 Not Deployed, post their deployment we will
        be fliping value of the label CLOCT14SLS53, So that user will get ability to mass create LPG also.
        */
        if(System.label.CLOCT14SLS53=='SPA OCT14 Not Deployed'){
            flag=False;
            type=System.Label.CLOCT14SLS31;  
        }
        else
            flag=True;
        if(ApexPages.currentPage().getParameters().containsKey('id'))        
            for(SPARequest__c requestrecord:[select id,Name,Requestedtype__c,startDate__c,EndDate__c,BackOfficeSystemID__c,ChannelLegacyNumber__c,BackOfficeCustomerNumber__c,ApprovalStatus__c from SPARequest__c where id =:ApexPages.currentPage().getParameters().get('id')])
                sparequestrecord=requestrecord;
            if(sparequestrecord.ApprovalStatus__c=='Approved' || sparequestrecord.ApprovalStatus__c=='Approval in Progress') 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot edit a locked record'));
            
        }

        public void preapareLineItemList()
        {
            String recordtype;
            if(type==System.Label.CLOCT14SLS31)
                recordtype=System.Label.CLOCT14SLS52;
            else
                recordtype=System.Label.CLOCT14SLS61;
            try{
                System.debug('>>>>> Method type'+type);
                System.debug('>>>>> Method csv size>>>'+csvList.stringList.size());
                if (type==System.Label.CLOCT14SLS31){
                   for(CS_SPALineItemCSVMap__c  cs: [select Name,ApiName__c,FieldType__c,isRequired__c,MassCreationType__c from CS_SPALineItemCSVMap__c where MassCreationType__c =:System.Label.CLOCT14SLS31 OR MassCreationType__c=:System.Label.CLOCT14SLS62])
                        csvNameAPINameMap.put(cs.Name,cs); 
                      System.debug('>>>>>'+csvNameAPINameMap);
                  }
                  
                 else if (type==System.Label.CLOCT14SLS32){
                   for(CS_SPALineItemCSVMap__c  cs: [select Name,ApiName__c,FieldType__c,isRequired__c,MassCreationType__c  from CS_SPALineItemCSVMap__c where MassCreationType__c =:System.Label.CLOCT14SLS32 OR MassCreationType__c=:System.Label.CLOCT14SLS62])
                        csvNameAPINameMap.put(cs.Name,cs); 
                        System.debug('>>>>>'+csvNameAPINameMap);
                  }
                
                if(csvList.stringList.size()>0)
                {
                    List<String> headerRow=new List<String>();                      
                    headerRow=csvList.stringList[0];            
                    populateRequiredMap();
                    if(validateHeaders(headerRow)){                        
                        for(Integer rowcount=1;rowcount<csvList.stringList.size();rowcount++)
                        {
                            List<String> cells=csvList.stringList[rowcount];                    
                            SPARequestLineItem__c tempLineItem=new SPARequestLineItem__c(SPARequest__c=sparequestrecord.id,RecordTypeId=recordtype);
                            for(Integer cellcount=0;cellcount<cells.size();cellcount++){
                            //the name is retreived from headerRow[cellcount] and the corresponding value is retreived from cells[cellcount]
                                String fieldType=csvNameAPINameMap.get(headerRow[cellcount]).FieldType__c;                              
                                if(cells[cellcount].trim()!=null && cells[cellcount].trim()!=''){
                                    if(fieldType == 'currency' || fieldType == 'double' || fieldType == 'percent' || fieldType == 'decimal' )
                                    {                                
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c, decimal.valueOf(cells[cellcount].trim())); 
                                    }
                                    else if(fieldType == 'boolean')
                                    {
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,Boolean.valueOf(cells[cellcount])); 
                                    }                   
                                    else if(fieldType == 'date')
                                    {
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,date.valueOf(cells[cellcount])); 
                                    }                
                                    else
                                    {
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,cells[cellcount]);
                                    }                                                                                   
                                    if(headerAPItoCSVMap.keySet().size()!=headerRow.size())
                                        headerAPItoCSVMap.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,headerRow[cellcount]);
                                }    
                            }                   
                            lineItemWrapperRecords.add(new LineItemWrapper(tempLineItem,'Yet to Insert'));
                        }                                                           
                    }
                    else
                    {                                            
                        String errorMessage='Please ensure that your csv has all the required and valid information. Please check the template or sample.';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage));
                        hasErrors=true;
                    }

                }              
            }
            catch(Exception ex)     
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getmessage()));
                hasErrors=true;
            }
            finally{
                isTableReady=true;                
            }
        }
        private boolean validateHeaders(List<String> headerRow)
        {   
            Set<String> headerRowset= new Set<String>(headerRow);
            System.debug('csvNameAPINameMap.keySet()'+new Set<String>(csvNameAPINameMap.keySet()));
            System.debug('headerRow'+headerRow);
            System.debug('!csvNameAPINameMap.keySet().containsAll(headerRow)'+!csvNameAPINameMap.keySet().containsAll(headerRow));         
            System.debug('csvNameAPINameMap.keySet().retainAll(headerRow)'+csvNameAPINameMap.keySet().retainAll(headerRow));
            
            if(!csvNameAPINameMap.keySet().containsAll(headerRowset) || !headerRowset.containsAll(justRequiredMap) )            
                return false;            
            else
                return true;
        }
        
        private void populateRequiredMap(){
            for(CS_SPALineItemCSVMap__c csvmaprecord :csvNameAPINameMap.values()){                
                isRequiredMap.put(csvmaprecord.ApiName__c,csvmaprecord.isRequired__c);  
                if(csvmaprecord.isRequired__c)              
                justRequiredMap.add(csvmaprecord.Name);
            }
        }

        public PageReference create()
        {
            displayReadOnlyFields=false;
            hasNoLineItemErrors=true;
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Integration piece is missing,this feature will be available later'));
           // hasErrors=true;
            WS_ProductandPricing.ProductAndPricingInformationPort  spaconnection=new WS_ProductandPricing.ProductAndPricingInformationPort();
            spaconnection.ClientCertName_x=System.Label.CL00616;
            spaconnection.timeout_x = 90000;
            WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean searchcriteria=new WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean();        
            WS_ProductandPricing.multipleProductAndPricingInformationResult Results;//results
            searchcriteria.endDate=sparequestrecord.EndDate__c;//Added for the BR-4973 OCT 2014 release
            searchcriteria.legacyAccountId=sparequestrecord.BackOfficeCustomerNumber__c;
            searchcriteria.sdhLegacyPSName=sparequestrecord.BackOfficeSystemID__c;
            searchcriteria.legacyChannelId=sparequestrecord.ChannelLegacyNumber__c;
            searchcriteria.startDate=sparequestrecord.StartDate__c;    
            searchcriteria.productAndPricingLineList=new  List<WS_ProductandPricing.productAndPricingLineDataBean>();
            
            for(LineItemWrapper lineItemWrapperRecord :lineItemWrapperRecords)
            {
                WS_ProductandPricing.productAndPricingLineDataBean tempRecord=new WS_ProductandPricing.productAndPricingLineDataBean();
                tempRecord.catalogNumber=lineItemWrapperRecord.requestlineitem.CommercialReference__c;
                tempRecord.localPricingGroup=lineItemWrapperRecord.requestlineitem.LocalPricingGroup__c;// Added for BR-4973 OCT 2014 release         
                tempRecord.requestedType=sparequestrecord.requestedType__c;// Requested Type taken from the header(SPA Request) as per BR-4964 
                tempRecord.requestedValue=lineItemWrapperRecord.requestlineitem.requestedValue__c;
                tempRecord.quantity=Integer.valueOf(lineItemWrapperRecord.requestlineitem.TargetQuantity__c);
                searchcriteria.productAndPricingLineList.add(tempRecord);
            }  
            System.debug('searchcriteria>>>>>>>'+searchcriteria);
            //if(!Test.isRunningTest())
            Results = spaconnection.getMultipleProductAndPricingInformation(searchcriteria);
            System.debug('Results.returnInfo>>>>>>>'+Results.returnInfo);
            if(Results.returnInfo.returnCode=='S')
            {
            //success
                List<WS_ProductandPricing.productAndPricingInformationResult> resultList=Results.productAndPricingInfoList;
                for(Integer i=0;i<resultList.size();i++)
                {
                    System.debug('resultList['+i+'].returnInfo.returnCode'+resultList[i].returnInfo.returnCode);
                    if(resultList[i].returnInfo.returnCode=='E'){
                      //  hasErrors=true;                        
                        lineItemWrapperRecords[i].message='Error';
                        hasNoLineItemErrors=false;
                    }
                    else
                    {
                        // Requested Type taken from the header(SPA Request) as per BR-4964 
                        lineItemWrapperRecords[i].requestlineitem.requestedtype__c=sparequestrecord.requestedtype__c;
                        System.debug('resultList['+i+'].productAndPricingInfo'+resultList[i].productAndPricingInfo);
                        System.debug('lineItemWrapperRecords['+i+'].requestlineitem'+lineItemWrapperRecords[i].requestlineitem);
                        if(resultList[i].productAndPricingInfo.localPricingGroup!=null && (resultList[i].productAndPricingInfo.localPricingGroup+'')!='null')
                            lineItemWrapperRecords[i].requestlineitem.put('LocalPricingGroup__c',String.valueOf(resultList[i].productAndPricingInfo.localPricingGroup+''));
                        if(resultList[i].productAndPricingInfo.standardPrice!=null && (resultList[i].productAndPricingInfo.standardPrice+'')!='null')
                            lineItemWrapperRecords[i].requestlineitem.StandardPrice__c=Double.valueOf(resultList[i].productAndPricingInfo.standardPrice+'');
                        if(resultList[i].productAndPricingInfo.standardDiscount!=null && (resultList[i].productAndPricingInfo.standardDiscount+'')!='null')
                            lineItemWrapperRecords[i].requestlineitem.StandardDiscount__c=Double.valueOf(resultList[i].productAndPricingInfo.standardDiscount+'');            
                        if(resultList[i].productAndPricingInfo.standardMulitplier!=null && (resultList[i].productAndPricingInfo.standardMulitplier+'')!='null')
                            lineItemWrapperRecords[i].requestlineitem.StandardMultiplier__c=Double.valueOf(resultList[i].productAndPricingInfo.standardMulitplier+'');
                        if((resultList[i].productAndPricingInfo.catalogNumber+'')!='null' && resultList[i].productAndPricingInfo.listPrice!=null)
                            lineItemWrapperRecords[i].requestlineitem.listPrice__c=Double.valueOf(resultList[i].productAndPricingInfo.listPrice);
                       // else
                       //    lineItemWrapperRecords[i].requestlineitem.listPrice__c=(lineItemWrapperRecords[i].requestlineitem.StandardPrice__c*100/(100-lineItemWrapperRecords[i].requestlineitem.StandardDiscount__c));
                        if((resultList[i].productAndPricingInfo.catalogNumber+'')!='null' && (resultList[i].productAndPricingInfo.currencyCode+'')!='null' && resultList[i].productAndPricingInfo.currencyCode!=null)
                            lineItemWrapperRecords[i].requestlineitem.currencyIsoCode=resultList[i].productAndPricingInfo.currencyCode;            

                        if((resultList[i].productAndPricingInfo.compensation+'')!='null' && resultList[i].productAndPricingInfo.compensation!=null)
                            lineItemWrapperRecords[i].requestlineitem.Compensation__c=resultList[i].productAndPricingInfo.compensation;    

                        if((resultList[i].productAndPricingInfo.description+'')!='null' && resultList[i].productAndPricingInfo.description!=null)
                            lineItemWrapperRecords[i].requestlineitem.MaterialDescription__c=resultList[i].productAndPricingInfo.description; 
                           /* OCT 2014 release, For advised discount Compensation cannot be blank. Preference is given first to the Backoffice value then to manually entered value,
                             if they are blank then updated to zero since compensation is used in threshold comparision.  */ 
                            if(lineItemWrapperRecords[i].requestlineitem.requestedType__c==System.Label.CLOCT14SLS13 && lineItemWrapperRecords[i].requestlineitem.Compensation__c==null)
                                lineItemWrapperRecords[i].requestlineitem.Compensation__c=0; 
                            //OCT 2014 release, for Free of Charge Requested value is 100
                            if(lineItemWrapperRecords[i].requestlineitem.RequestedType__c==System.Label.CLOCT14SLS11)
                                lineItemWrapperRecords[i].requestlineitem.requestedValue__c=100;
                            //As per  DEF-5097 
                            if(lineItemWrapperRecords[i].requestlineitem.requestedType__c!=System.Label.CLOCT14SLS13)
                                lineItemWrapperRecords[i].requestlineitem.Compensation__c=null;      
                     
                    }
                }
                displayReadOnlyFields=true;
                
            }        
            else
            {
            //failure
               // if(!Test.isRunningTest())
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Results.returnInfo.returnMessage));                
            }
          return null;
        }
        
        public PageReference proceedWithInsert(){
        List<SPARequestLineItem__c> tobeInsertedLineItems=new List<SPARequestLineItem__c>();
        for(LineItemWrapper liwrecord:lineItemWrapperRecords)
        {
            if(liwrecord.message!='Error')
                tobeInsertedLineItems.add(liwrecord.requestlineitem);
        }

        if(tobeInsertedLineItems.size()>0)
        {
            List<Database.SaveResult> lstSaveresults=Database.insert(tobeInsertedLineItems,false);
            for(Integer i=0;i<lstSaveresults.size();i++)
            {
                if(lstSaveresults[i].isSuccess())
                {
                  lineItemWrapperRecords[i].status='OK';
                  lineItemWrapperRecords[i].message='<a class="link" href="/'+lstSaveresults[i].getId()+'">Record Created</a>';
                }
                else
                {
                    lineItemWrapperRecords[i].status='KO';
                    lineItemWrapperRecords[i].message=lstSaveresults[i].getErrors()[0].getMessage();
                }
            }           
        }
        disableEverything=true;        
        return null;
        }
        
        public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(System.Label.CLOCT14SLS31,System.Label.CLOCT14SLS31)); 
        options.add(new SelectOption(System.Label.CLOCT14SLS32,System.Label.CLOCT14SLS32)); 
         return options; 
        }
        
        class LineItemWrapper
        {
            public SPARequestLineItem__c requestlineitem{get;set;}
            public String status{get;set;}          
            public String message{get;set;}
            LineItemWrapper(SPARequestLineItem__c requestlineitem,String status){
                this.requestlineitem=requestlineitem;
                this.status=status;
            }           
        }
    }