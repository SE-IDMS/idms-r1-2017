/*
    Author              : Sreedevi Surendran (Schneider Electric)
    Date Created        : 22-December-2012
    Modification Log    : 
    Description         : Connect Target Tool Report (by Country)
                          Based on ConnectTargetDashboardController created by Gayathri Shivakumar

*/

public with sharing class ConnectTargetDashboardCountryController
{

  public ConnectTargetDashboardCountryController(){
  
      Country = null;
      Quarter = 'Q1';
      Years = Label.ConnectYear;
      }
      public string Years {get;set;}
      
    //Attributes
   // List<Global_KPI_Targets__c> GlobalKPI = [Select KPI_Name__c,Country_KPI_Target_Q4__c,Transformation__c, Unit_of_Measure__c, OverallKPIOrder__c  from Global_KPI_Targets__c where KPI_Type__c = 'Country KPI'];
    
   // Map<Id,String> mapCountries = new Map<Id,String>();
      
    
    //Properties, Getters and Setters
    public string renPDF {get; set;}
    public string Country {get;set;} 
      
    public string Quarter{get;set;}
    
    public List<SelectOption> getFilterQuarter() 
    {
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('Q1','Q1')); 
     options.add(new SelectOption('Q2','Q2')); 
     options.add(new SelectOption('Q3','Q3')); 
     options.add(new SelectOption('Q4','Q4/Yearly'));
     
     return options;
     }
     
     public List<SelectOption> getFilterYear() 
    {
     List<SelectOption> Yearoptions = new List<SelectOption>();
     Yearoptions.add(new SelectOption('2013','2013')); 
     Yearoptions.add(new SelectOption('2014','2014')); 
          
     return Yearoptions;
     }
     List<Cascading_KPI_Target__c> lstEntityKPI = new List<Cascading_KPI_Target__c>();
    
    public List<SelectOption> getFilterList() 
    {
    Set<String> SetCountries = new Set<String>();
     lstEntityKPI = [SELECT id, KPI_Name__c, KPI_Acronym__c, Q1_Actual__c,Q2_Actual__c,Q3_Actual__c,Q4_Actual__c, Country_KPI_Target_Q1__c,Country_KPI_Target_Q2__c,Country_KPI_Target_Q3__c, Country_KPI_Target_Q4__c, Transformation__c, Unit_of_Measure__c,KPI_Type__c,CountryPickList__c,Digital_Readiness__c,Digital_Readiness_Target_Q2__c,Digital_Readiness_Target_Q3__c,Digital_Readiness_Target_Q4_Yearly__c,Digital_Rediness_Percentage__c,Rediness_Percentage_Q2__c,Rediness_Percentage_Q3__c,Rediness_Percentage_Q4__c, EntityOverallKPIOrder__c FROM Cascading_KPI_Target__c where KPI_Type__c = 'Country KPI' and KPI_Acronym__c != :Label.ConnectGrowthPriorityCountry and Year__c = :Years order by EntityOverallKPIOrder__c];      
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','<Select a Country>'));     
     
      for(Cascading_KPI_Target__c eKPI:lstEntityKPI)
       {
       
       
           //if(!mapCountries.containsKey(eKPI.Country__c))
            //  mapCountries.put(eKPI.Country__c,'');
          if(eKPI.CountryPickList__c != null)
            SetCountries.add(eKPI.CountryPickList__c);
        }
          
       // List<Country__c> lstCountries = [SELECT Id,Name from Country__c where Id in :mapCountries.keySet()];
      
     //   options.add(new SelectOption(eKPI.CountryPickList__c,eKPI.CountryPickList__c));
        for(String ct:SetCountries)
        {
            options.add(new SelectOption(ct,ct)); 
           // mapCountries.put(ct.Id,ct.Name);   
       }    
        options.sort();          
        return options;
    }
    
    public List<KPIList> getConnectToCustomers()
    {
        List<KPIList> lstConnectToCustomers = new List<KPIList>();
        
        if(Country != null)               
            retrieveEntityKPIs(lstConnectToCustomers,Label.ConnectTransCustomer);
       
        return(lstConnectToCustomers);
    }

    public List<KPIList> getConnectEverywhere()
    {
        List<KPIList> lstConnectEverywhere = new List<KPIList>();
        
        if(Country != null) 
            retrieveEntityKPIs(lstConnectEverywhere,Label.ConnectTransEverywhere);
       
        return(lstConnectEverywhere);
    }

    public List<KPIList> getConnectPeople()
    {
        List<KPIList> lstConnectPeople = new List<KPIList>();       
         
        if(Country != null) 
            retrieveEntityKPIs(lstConnectPeople,Label.ConnectTransPeople);
       
        return(lstConnectPeople);
    }

    public List<KPIList> getConnectForEfficiency()
    {
        List<KPIList> lstConnectForEfficiency = new List<KPIList>();
        
         if(Country != null) 
            retrieveEntityKPIs(lstConnectForEfficiency,Label.ConnectTransEfficiency);
       
                return(lstConnectForEfficiency);
    }    
    
    public List<KPIList> getGPE()
    {
        List<KPIList> lstGPE = new List<KPIList>();
        
         if(Country != null) 
            retrieveGPE(lstGPE);
       
                return(lstGPE);
    }    
    
    
    public List<KPIList> getConnect()
    {
        List<KPIList> lstConnect = new List<KPIList>();
        
        if(Country != null) 
            retrieveEntityKPIs(lstConnect,'Connect');
      
        return(lstConnect);
    }    
    //Constructors
    public ConnectTargetDashboardCountryController(ApexPages.StandardController controller)
    {
        renPDF = '';
       Country = null;
       Quarter = 'Q1';
       Years = Label.ConnectYear;
    }
    
    //Methods
    public pagereference RenderPDF()
    {
        renPDF = 'PDF';
        return null ;
    }  
    
   
    public pagereference FilterChange()
    {
        return null;  
    }
    
    public pagereference YearChange()
    {
        if(Years == '2014'){
        Country = null;
     PageReference pageRef = new PageReference('/apex/CountryTargetDashboard');
     return (pageRef);
     }
     else{
      return null;
       }    
    }
    private void retrieveGPE(List<KPIList> lstTransformation)
    { 
      
    string Target;
    string Actual;
     
    List<Growth_Priority_Execution_Target__c> GPEKPI = [Select  Entity_Target__r.KPI_Acronym__c, Country_KPI_Actual_Q1__c,Entity_Target__r.KPI_Name__c,Entity_Target__r.Unit_of_Measure__c,Country_KPI_Actual_Q2__c,Country_KPI_Actual_Q3__c,Country_KPI_Actual_Q4_Yearly__c,Country__c,Growth_Priority__c,Country_KPI_Target_Q1__c,Country_KPI_Target_Q2__c,Country_KPI_Target_Q3__c,Country_KPI_Target_Q4_Yearly_Target__c from Growth_Priority_Execution_Target__c where Country__c = :Country ];
    
    for(Growth_Priority_Execution_Target__c GPE: GPEKPI){
     KPIList lst = new KPIList();
     lst.kpiName = Label.ConnectGrowth + '-' + GPE.Growth_Priority__c ;
     lst.kpiid = GPE.id;
    if (Quarter == 'Q1'){
        Target = GPE.Country_KPI_Target_Q4_Yearly_Target__c;
        Actual = GPE.Country_KPI_Actual_Q1__c;
        }    
    else 
        if (Quarter == 'Q2'){
        Target = GPE.Country_KPI_Target_Q4_Yearly_Target__c;
        Actual = GPE.Country_KPI_Actual_Q2__c;
        }    
    else 
        if (Quarter == 'Q3'){
        Target = GPE.Country_KPI_Target_Q4_Yearly_Target__c;
        Actual = GPE.Country_KPI_Actual_Q3__c;
        }   
    else 
        if (Quarter == 'Q4'){
        Target = GPE.Country_KPI_Target_Q4_Yearly_Target__c;
        Actual = GPE.Country_KPI_Actual_Q4_Yearly__c;
        }           
      
      if(Target == null)
                Target = '';
                if (Actual == null)
                       Actual = '';
                       
                    if(GPE.Entity_Target__r.Unit_of_Measure__c != null)
                    {
                        lst.target = 'Target: ' + Target +' '+' Actual:' + Actual;
                        lst.um = GPE.Entity_Target__r.Unit_of_Measure__c;
                    }
                    else
                    {
                        lst.Target = 'Target: ' + Target + ' ' +'Actual: ' + Actual;
                        lst.um = GPE.Entity_Target__r.Unit_of_Measure__c;
                    }
                    lstTransformation.add(lst); 
                   }   
     }
  
    private void retrieveEntityKPIs(List<KPIList> lstTransformation, string lblTransformation)
    {   
       
        string Target;
        string Actual;
        
       if(Country != null){
        for(Cascading_KPI_Target__c EKPI: lstEntityKPI)
        {
            if(Country == EKPI.CountryPickList__c)
            {
                KPIList lst = new KPIList();
            
             if(EKPI.Transformation__c == lblTransformation)
                {
            if(EKPI.KPI_Acronym__c != Label.ConnectGrowth){
                 if(EKPI.KPI_Acronym__c != 'DMLC' ){
                    lst.kpiName = EKPI.KPI_Name__c;
                    lst.kpiid = EKPI.id;
               if (Quarter == 'Q1'){
                  Target = EKPI.Country_KPI_Target_Q4__c;
                  Actual = EKPI.Q1_Actual__c;
                  }
               else if(Quarter == 'Q2'){
                  Target = EKPI.Country_KPI_Target_Q4__c;
                  Actual = EKPI.Q2_Actual__c;
                  }
               else if(Quarter == 'Q3'){
                  Target = EKPI.Country_KPI_Target_Q4__c;
                  Actual = EKPI.Q3_Actual__c;
                  }
               else if(Quarter == 'Q4'){
                 Target = EKPI.Country_KPI_Target_Q4__c;
                 Actual = EKPI.Q4_Actual__c;
                 }
                }
              else if(EKPI.KPI_Acronym__c == 'DMLC'){
                
                 lst.kpiid = EKPI.id;              
                 lst.kpiName = EKPI.KPI_Name__c;
               if (Quarter == 'Q1'){
                  Target = String.ValueOf(EKPI.Digital_Readiness__c);
                  Actual = String.ValueOf(EKPI.Digital_Rediness_Percentage__c);
                  }
               else if(Quarter == 'Q2'){
                  Target = String.ValueOf(EKPI.Digital_Readiness_Target_Q2__c);
                  Actual = String.ValueOf(EKPI.Rediness_Percentage_Q2__c);
                  }
               else if(Quarter == 'Q3'){
                  Target = String.ValueOf(EKPI.Digital_Readiness_Target_Q3__c);
                  Actual = String.ValueOf(EKPI.Rediness_Percentage_Q3__c);
                  }
               else if(Quarter == 'Q4'){
                  Target = String.ValueOf(EKPI.Digital_Readiness_Target_Q4_Yearly__c);
                  Actual = String.ValueOf(EKPI.Rediness_Percentage_Q4__c);
                 }
                }
                             
                if(Target == null)
                        Target = '';
                if (Actual == null)
                       Actual = '';
                       
                    if(EKPI.Unit_of_Measure__c != null)
                    {
                        lst.target = 'Target: ' + Target +' '+' Actual:' + Actual;
                        lst.um = EKPI.Unit_of_Measure__c;
                    }
                    else
                    {
                        lst.Target = 'Target: ' + Target + ' ' +'Actual: ' + Actual;
                        lst.um = EKPI.Unit_of_Measure__c;
                    }
                    lstTransformation.add(lst);     
                }
            }
          }
       } 
     }
   }
    //Classes
    public class KPIList
    {
        public string kpiName {get;set;}
        public string kpiid{get;set;}
        public string target {get;set;}
        public string um {get;set;}
    }

}