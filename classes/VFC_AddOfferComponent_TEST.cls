/*****
Last Modified By:2016Q1 release 
******/

@isTest
private class VFC_AddOfferComponent_TEST
{
    static testMethod void testVFC_AddOfferComponent() 
    {        
        Offer_Lifecycle__c offcycle = new Offer_Lifecycle__c (Name='test',Solution_Name__c='Test5555#',Description__c='test',Segment_Marketing_VP__c=UserInfo.getUserId(),Leading_Business_BU__c='Energy',Launch_Owner__c=UserInfo.getUserId(),Process_Type__c='PMP');
        insert offcycle;
          
        ApexPages.StandardController controller = new ApexPages.StandardController(offcycle);   
        VFC_AddOfferComponent GMRSearchPage = new VFC_AddOfferComponent(controller);
        PageReference pageRef = Page.VFP_AddOfferComponent;
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);
        pageRef.getParameters().put('Id',offcycle.Id);
        System.debug('$$$$$'+pageRef);
        Test.setCurrentPage(pageRef);
        GMRSearchPage.jsonSelectString=jsonSelectString;
        VFC_AddOfferComponent.getOthers('02CABFCW');
        VFC_AddOfferComponent.remoteSearch('test','02CABFCW',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddOfferComponent.remoteSearch('test','02CABF',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddOfferComponent.remoteSearch('test','02CA',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddOfferComponent.remoteSearch('test','02',FALSE,FALSE,FALSE,FALSE,FALSE);
        Test.startTest(); 
        GMRSearchPage.performSelect();
        GMRSearchPage.pageCancelFunction();
        Test.stopTest(); 
   }
}