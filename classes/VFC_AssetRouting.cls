public with sharing class VFC_AssetRouting {
  public VFC_AssetRouting() {
    
  }


  public PageReference init(){

     id userid = userinfo.getUserid();
      PageReference  pref;

     User  userobj =[select id,UserBusinessUnit__c,ServiceMaxUser__c, name from User where id =:userid];
     //if(userobj.UserBusinessUnit__c == 'IT'){
     if(userobj.ServiceMaxUser__c == false){ //Added for BR-8150
         pref= new PageReference('/apex/VFP_InTouchAssetSearch?caseId='+ApexPages.currentPage().getParameters().get('id')); 
           pref.setRedirect(true);
           

     }
     else if(userobj.ServiceMaxUser__c == true){ //Added for BR-8150

         pref= new PageReference('/apex/VFP_SearchInstalledProduct?objid='+ApexPages.currentPage().getParameters().get('id')+'&sobj=CASE'); 
           pref.setRedirect(true);
           
     }
     return pref;
  }

}