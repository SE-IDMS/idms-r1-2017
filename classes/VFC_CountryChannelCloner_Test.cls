@isTest
public class VFC_CountryChannelCloner_Test{

    static TestMethod void countryChannelCloner() {
        List<CS_PRM_UserRegistration__c> cs_RecList = new List<CS_PRM_UserRegistration__c>();
        List<CountryChannelUserRegistration__c> userRegFieldsList = new List<CountryChannelUserRegistration__c>();
        Country__c country= Utils_TestMethods.createCountry();
        insert country;
        
        PRMCountry__c prmCountry = new PRMCountry__c();
        prmCountry.Name = 'Test Class PRMCountry';
        prmCountry.Country__c = country.Id;
        prmCountry.PLDatapool__c = 'DataPool';
        prmCountry.CountryPortalEnabled__c = true;
        insert prmCountry;
        
        StateProvince__c state = Utils_TestMethods.createStateProvince(country.Id);
        insert state;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild;
        
        CountryChannels__c cChannels = new CountryChannels__c();
        cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
        cChannels.Country__c = country.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true;
        insert cChannels;
        
        PRMAccountOwnerAssignment__c prmAccOwner = new PRMAccountOwnerAssignment__c();
        prmAccOwner.Classification__c = 'FI';
        prmAccOwner.SubClassification__c = cLChild.id;
        prmAccOwner.Country__c = country.Id;
        prmAccOwner.CountryChannel__c = cChannels.Id;
        insert prmAccOwner;
        
        PageReference pageRef = Page.VFP_CountryChannelCloner;
        Test.setCurrentPage(pageRef);
        Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(cChannels);
        ApexPages.currentPage().getParameters().put('Id',cChannels.id);
        VFC_CountryChannelCloner thisCountryChannelCloner = new VFC_CountryChannelCloner(thisController);
        thisCountryChannelCloner.selectedCountry = 'TCY';
        thisCountryChannelCloner.cloneAll();
        thisCountryChannelCloner.getactiveCountryClusters();
    }
}