/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  12-Dec-2012
    Description         : Class for IPO Deployment Network Triggers, Assigns Champion Names for Team Members. The Validate ScopeEntity class validates if the selected
    // Scope and Entity are part of the Parent Program. Also Update Program Network on Record Deletion
*/

public with sharing class IPODeploymentNetworkTriggers
{
    //Public Static void IPODeploymentNetworkBeforeInsert(List<Team_Members__c> Team)
    //  {
      
          //System.Debug('****** IPODeploymentNetworkBeforeInsert Start ****'); 
          
          // Update Champions
          
         // list<Champions__c> Champion = new List<Champions__c>();
         // Champion = [select Scope__c, Entity__c, Champion_Name__r.FirstName, Champion_Name__r.LastName from Champions__c];
        //  for(Team_Members__c T:Team){
        //     for(Champions__c C: Champion){
        //       If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c))) {
        //           T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                   
        //           }
        //          } // For loop 1 Ends
        //         } // For loop 2 Ends
         //        
              //   System.Debug('****** TeamMembersBeforeInsert End ****'); 
                 
              
                 
           //  } // Static Class Ends
                
      Public Static void IPODeploymentNetworkBeforeUpdate(List<IPO_Strategic_Deployment_Network__c> Team)
         {
      
         // System.Debug('****** TeamMembersBeforeUpdate Start ****'); 
          
         // list<Champions__c> Champion = new List<Champions__c>();
         // Champion = [select Scope__c, Entity__c, Champion_Name__r.FirstName, Champion_Name__r.LastName from Champions__c];
         // for(Team_Members__c T:Team){
          //   for(Champions__c C: Champion){
          //     If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c))) {
          //         T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
          //         
          //         }
          //        } // For loop 1 Ends
          //       } // For loop 2 Ends
                 
                   // Update Program Network
                
            List<IPO_Strategic_Deployment_Network__c> T = new List<IPO_Strategic_Deployment_Network__c>();         
            List<IPO_Strategic_Deployment_Network__c> lstUpdate = new List<IPO_Strategic_Deployment_Network__c>();
            
            T = [Select Name, Program_Network__c, IPO_Strategic_Program_Name__c from IPO_Strategic_Deployment_Network__c where IPO_Strategic_Program_Name__c = :Team[0].IPO_Strategic_Program_Name__c and Name != :Team[0].Name];
                 
            For(IPO_Strategic_Deployment_Network__c Tm : T) {
               Tm.Program_Network__c = False;
               lstUpdate.add(Tm);
               System.Debug('Team name: ' + Tm.Name);
               }
               Update lstUpdate;  
                 System.Debug('****** IPODeploymentNetworkBeforeUpdate End ****'); 
                 
                } // Static Class Ends
                
             // Class to Validate Scope and Entity with the Parent Program             
            
             
             Public Static void IPODeploymentNetworkBeforeInsert_ValidateScopeEntity(List<IPO_Strategic_Deployment_Network__c> Team)
              {
                  List<IPO_Strategic_Program__c> Program = new List<IPO_Strategic_Program__c>();
                  Program = [Select Name, Global_Functions__c, Operation__c, Global_Business__c from IPO_Strategic_Program__c];
                  integer GF; integer GB; integer PR; integer GSC; integer PAR;
                  for(IPO_Strategic_Deployment_Network__c T:Team){
                  GF = 0;
                  GB = 0;
                  PR = 0;
                  GSC = 0;
                  PAR = 0;
                     for(IPO_Strategic_Program__c P: Program){
                         if(T.IPO_Strategic_Program_Name__c == P.Name){
                           if(((P.Global_Functions__c == null)&& (T.Scope__c == Label.Connect_Global_Functions)) ||((P.Global_Business__c == null)&& (T.Scope__c == Label.Connect_Global_Business)) ||((P.Operation__c == null)&& (T.Scope__c == Label.Connect_Power_Region)))
                              T.ValidateScopeonInsertUpdate__c = True;
                              
                             if((T.Scope__c == Label.Connect_Global_Functions) && (P.Global_Functions__c != null) && !(P.Global_Functions__c.Contains(T.Entity__c)))
                               GF = GF + 1;
                             
                             if((T.Scope__c == Label.Connect_Global_Business) && (P.Global_Business__c != null) && !(P.Global_Business__c.Contains(T.Entity__c)))
                               GB = GB + 1;
                               
                               if((T.Scope__c == Label.Connect_Power_Region) && (P.Operation__c != null) && !(P.Operation__c.Contains(T.Entity__c)))
                               PR = PR + 1;
                               
                               
                               
                               } // if Ends
                               
                               if((GF > 0) || (GB > 0) || (PR > 0))
                                   T.ValidateScopeonInsertUpdate__c = True;
                              } // For Ends
                             } //For Ends
                             
   
                  
              }
              
   // Change Deployment Network Status Before Deleting a Deployment Network Record    
   
   Public Static void IPODeploymentNetworkBeforeDelete(List<IPO_Strategic_Deployment_Network__c> T)
      {
      
          System.Debug('****** IPODeploymentNetworkBeforeDelete Start ****'); 
          List<IPO_Strategic_Deployment_Network__c> Team = new List<IPO_Strategic_Deployment_Network__c>();
          //System.Debug('Team Delete ' + T[0].Program_Name__c);
          List<IPO_Strategic_Deployment_Network__c> lstUpdate = new List<IPO_Strategic_Deployment_Network__c>();
         Team = [Select Name, Program_Network__c, IPO_Strategic_Program_Name__c from IPO_Strategic_Deployment_Network__c where IPO_Strategic_Program_Name__c = :T[0].IPO_Strategic_Program_Name__c];
          System.Debug('Team size: ' + Team.size());         
            For(IPO_Strategic_Deployment_Network__c Tm : Team) {
               Tm.Program_Network__c = False;
               lstUpdate.add(Tm);
               System.Debug('Team name: ' + Tm.Name);
               }
               Update lstUpdate;
            System.Debug('****** IPODeploymentNetworkBeforeDelete Stop ****'); 
    }     
                             
                 
              }