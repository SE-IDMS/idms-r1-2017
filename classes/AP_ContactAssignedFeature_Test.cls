@isTest
private class AP_ContactAssignedFeature_Test {
    
    @isTest static void test_method_one() {
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            usr.BypassWF__c = false;
            usr.ProgramAdministrator__c = true;
            usr.UserRoleID = '00EA0000000MiDK';
            System.runas(usr){
                permissionSet pset1 = [select id from permissionSet where name like '%PRMProjectRegistration%' limit 1];

                Country__c country = Utils_TestMethods.createCountry();
                insert country;
                CS_PRM_FeaturePermissionSetMap__c cs = new CS_PRM_FeaturePermissionSetMap__c();
                cs.name = 'Simple ORF';
                cs.PermissionSetIds__c = pset1.id;
                insert cs; 
        
                permissionSet pset2 = [select id from permissionSet where name like '%PRMPassedToPartnerOpportunities%' limit 1];
    
                CS_PRM_FeaturePermissionSetMap__c cs2 = new CS_PRM_FeaturePermissionSetMap__c();
                cs2.name = 'Passed to Partner Opportunities';
                cs2.PermissionSetIds__c = pset2.id;
                insert cs2;
        
                list<FeatureCatalog__c> lstFeatureCat = new list<FeatureCatalog__c>();

                FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
                feature.Enabled__c = 'Contact';
                feature.Visibility__c = 'Contact';
                feature.Category__c='Passed to Partner Opportunities';
                feature.name = 'Passed to Partner';
                lstFeatureCat.add(feature);
                FeatureCatalog__c feature2 = Utils_TestMethods.createFeatureCatalog();
                feature2.Enabled__c = 'Contact';
                feature2.Visibility__c = 'Contact';
                feature2.Category__c='Simple ORF';
                feature2.name = 'Simple ORF';
                lstFeatureCat.add(feature2);
                insert lstFeatureCat;

                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = country.id;
                insert acc;
                    
                test.startTest();
        
                acc.IsPartner  = true;
                update acc;

                Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
                contact1.PRMContact__c = true;
                contact1.PRMUser__c  = true;
            
                insert contact1;
                contact1.PRMUser__c  = true;
                update contact1;
                
                CS_PRM_Contact_Assign_Permission__c cap1 = new CS_PRM_Contact_Assign_Permission__c();
                cap1.Name = 'Cap';
                cap1.ContactId__c = contact1.Id;
                cap1.TargetApplicationRequested__c = 'POMP';
                INSERT cap1;
                
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' Limit 1];
            User user1 = new User(
                Username = 'test12345test@bridge-fo.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Kumar',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
           // Database.insert(user1); 

            list<ContactAssignedFeature__c> lscaf = new list<ContactAssignedFeature__c>();
            ContactAssignedFeature__c cf1 = new ContactAssignedFeature__c();
            cf1.Contact__c = contact1.id;
            cf1.FeatureCatalog__c =feature.id;
            cf1.Active__c = true;
            lscaf.add(cf1);
            ContactAssignedFeature__c cf2 = new ContactAssignedFeature__c();
            cf2.Contact__c = contact1.id;
            cf2.FeatureCatalog__c =feature2.id;
            cf2.Active__c = true;
            lscaf.add(cf2);

            insert lscaf;
            
            

        //User u = [Select id from User where Profile.name='System Administrator'  and isActive=true limit 1];

        //system.runAs(u)
        //{
            AP_ContactAssignedFeature.createPermissionSetAssignments2(lscaf);

            for(ContactAssignedFeature__c c : lscaf)
                c.Active__c = false;
            update  lscaf;
            AP_ContactAssignedFeature.deletePermissionSetAssignments2(lscaf);

            Batch_processPartnerPermissionSets batch_ppsa = new Batch_processPartnerPermissionSets(); 
            //batch_ppsa.query = 'Select Active__c, Contact__c,FeatureCatalog__c, LastModifiedDate from ContactAssignedFeature__c Where LastModifiedDate =Last_N_Days:15';
            //Database.executebatch(batch_ppsa);
            batch_ppsa.execute(Null);
        //}  
            }
        }
    }
}