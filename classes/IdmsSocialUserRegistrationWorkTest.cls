/*
* Test for IdmsSocialUserRegistrationWorkController
* */
@IsTest
private class IdmsSocialUserRegistrationWorkTest{
    //Test 1: creating and updating a user for social registration
    @testSetup
    static void updateuser()
    {
        IdmsSocialUserRegistrationWorkController controller = new IdmsSocialUserRegistrationWorkController();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        
        User u = new User(Alias = 'standt', 
                          Email='testidmsuser@accenture.com', 
                          EmailEncodingKey='UTF-8',
                          FirstName ='Test First',      
                          LastName='Testing',
                          CommunityNickname='CommunityName123',      
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='testidmsuser@accenture.com' + Label.CLJUN16IDMS71,
                          UserRoleId = r.Id,
                          IDMS_Registration_Source__c = 'Facebook',
                          isActive = True,   
                          BypassVR__c = True);
        Insert u;  
        
        
        
        
        String RegSourceFb = [select Id, IDMS_Registration_Source__c from User where Id=:u.Id limit 1].IDMS_Registration_Source__c;
        System.assertEquals(Label.CLJUN16IDMS154,RegSourceFb );
        String languageUser=Label.CLJUN16IDMS09;
        controller.firstName = 'Test First';
        controller.lastname = 'Testing';
        controller.phomenum = '';
        controller.notvalid = false;
        controller.checked = true;
        controller.checkedEmailOpt = true;
        controller.popUPrender = true;
        
    }
    
    //Test 2: varifying the user details for social registration
    public static testMethod void verifyuserdetails1() {
        ApexPages.currentPage().getParameters().put('firstName', '');
        ApexPages.currentPage().getParameters().put('lastname', '');
        ApexPages.currentPage().getParameters().put('phomenum', '');
        ApexPages.currentPage().getParameters().put('ucountry', '');
        ApexPages.currentPage().getParameters().put('regCheckEmailOpt', 'false');
        IdmsSocialUserRegistrationWorkController controller = new IdmsSocialUserRegistrationWorkController();
        
    }
    //Test 3: varifying the user details for social registration
    static Testmethod void verifyuserdetails()
    {
        IdmsSocialUserRegistrationWorkController controller = new IdmsSocialUserRegistrationWorkController();
        controller.verify();
        controller.reset();
        controller.firstName = '';
        controller.lastname = '';
        controller.phomenum = '';
        controller.notvalid = true;
        controller.checked = false;
        controller.checkedEmailOpt = false;
        controller.popUPrender = false;
        
    }
    //Test 4: varifying the captcha for social registration
    static Testmethod void verifycaptcha()
    {
        IdmsSocialUserRegistrationWorkController controller = new IdmsSocialUserRegistrationWorkController();
        controller.verify();
        
    }
    //Test 5: varifying country
    static Testmethod void autovalidatecountry()
    {
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        
        testUser.autoRun();
        testUser.ucountry='IN';
        testUser.displayPopup=true;
        testUser.email=null;
        testUser.save();
        
    }
    //Test 6: varifying registration
    public static testMethod void idmsRegisterUserTest1(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        
        testUser.ucountry='IN';
        testUser.email='asdasd';
        testUser.save();
    }
    //Test 7: varifying registration
    public static testMethod void idmsRegisterUserTest2(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        
        testUser.ucountry='IN';
        testUser.email='asdasd';
        testUser.save();
    }
    
    //Test 8: varifying registration for remaining values
    public static testMethod void idmsRegisterUserTest5(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.email='asdasd@accenture.com';
        testUser.phomenum='1234567890';
        testUser.checked=false;
        testUser.save();
    }
    //Test 9: varifying registration for remaining values
    public static testMethod void idmsRegisterUserTest6(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.email='asdasd@accenture.com';
        testUser.phomenum='1234567890';
        testUser.checked=false;
        testUser.save();
    }
    
    //Test 10: varifying registration for remaining values
    public static testMethod void idmsRegisterUserTest7(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap;
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage);
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.email='tester321@accenture.com';
        testUser.checked=true;
        testUser.enterRecaptchcorrect=false;
        testUser.phomenum='1234567890';
        
        testUser.save();
    }
    //Test 11: varifying registration for remaining values
    public static testMethod void idmsRegisterUserTest8(){
        IdmsTermsAndConditionMapping__c termMap = new IdmsTermsAndConditionMapping__c();
        termMap.name = 'test';
        termMap.url__c= 'test';
        insert termMap;
        
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap; 
        
        IdmsCountryMapping__c CountryMap = new IdmsCountryMapping__c();
        CountryMap.name = 'IN';
        CountryMap.DefaultCurrencyIsoCode__c = 'INR';
        insert CountryMap; 
        
        PageReference mypage= Page.SocialUserRegistration;
        Test.setCurrentPage(mypage); 
        
        ApexPages.currentpage().getparameters().put('recaptcha_challenge_field' ,'abc');
        ApexPages.currentpage().getparameters().put('recaptcha_response_field' ,'abc');
        ApexPages.currentpage().getparameters().put('app' ,'test');
        ApexPages.currentpage().getparameters().put('context' ,'test');
        IdmsSocialUserRegistrationWorkController testUser=new IdmsSocialUserRegistrationWorkController();
        
        //make custom setting blank
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c();
        cust_obj.name ='IdmsKey';
        cust_obj.AppValue__c = 'test';
        Insert cust_obj;
        
        testUser.ucountry='IN';
        testUser.firstName='abc';
        testUser.lastName='abc';
        testUser.checked=true;
        testUser.email='asdas11d@accenture.com';
        testUser.enterRecaptchcorrect=true;
        testUser.phomenum='1234567890';
        testUser.save();
    }
    
}