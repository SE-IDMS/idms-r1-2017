Public Class AP_SpecializationRequirement
{
    public static void createChildSpecializationRequirements(List<SpecializationRequirement__c> lstGloSpeReq, set<id> stRequirementIds)
    {
        set<id> stGlobSpecIds = new set<id>();
        map<id, list<SpecializationRequirement__c>> mpSpecToReqs = new map<id, list<SpecializationRequirement__c>>();
        for(SpecializationRequirement__c spr : lstGloSpeReq)
        {
            stGlobSpecIds.add(spr.Specialization__c);
            if(!mpSpecToReqs.containsKey(spr.Specialization__c))
                mpSpecToReqs.put(spr.Specialization__c, new List<SpecializationRequirement__c>{(spr)});
            else
                mpSpecToReqs.get(spr.Specialization__c).add(spr);   
        }
        
        //Dont not create child Specialization Requirements id already exist
        if(stRequirementIds != null)
        {
            List<SpecializationRequirement__c> lstExitingReqs = [SELECT Specialization__c, Specialization__r.GlobalSpecialization__c, GlobalSpecializationRequirement__c  
                                    FROM SpecializationRequirement__c  WHERE GlobalSpecializationRequirement__c  in:stRequirementIds];
            
            if(!lstExitingReqs.isEmpty())
            {
                for(SpecializationRequirement__c exSpr : lstExitingReqs )
                   stGlobSpecIds.remove(exSpr.Specialization__r.GlobalSpecialization__c);
            }
        }
        
        //Getting the list of Child Specilizations
        List<Specialization__c> lstChildSpecialization = [SELECT name,Status__c,GLobalSpecialization__C  from Specialization__c WHERE GLobalSpecialization__C in:stGlobSpecIds ];
        
        //Iterate over the Child Specializations and Create the Child Specialization Requirements
        List<SpecializationRequirement__c> lstSpeReqToCreate = new List<SpecializationRequirement__c>();
        for(Specialization__c childSP : lstChildSpecialization)
        {
            for(SpecializationRequirement__c  GloSpr : mpSpecToReqs.get(childSP.GLobalSpecialization__C ))
            {
                SpecializationRequirement__c newspr = new SpecializationRequirement__c();
                
                newspr.GlobalSpecializationRequirement__c = GloSpr.id;
                newspr.Specialization__c = childSP.id;
                newspr.RequirementCatalog__c = GloSpr.RequirementCatalog__c;
                newspr.recordtypeid = GloSpr.recordtypeid;
                newspr.target__C = GloSpr.target__c;
                
                lstSpeReqToCreate.add(newspr);
            }
            
        }
        
        insert lstSpeReqToCreate;
        
    }//end of method
    
    //Update the child Specialization Requirements when a Global Specialization is updated
    Public static void updateChildSpecializationRequirements(List<SpecializationRequirement__c> lstGloSpeReq)
    {
            //TODO
    }


    //Create new Account Specialization Requirements, when ane new Specialization Requirement is added
    public static void createNewAccountSpecializationRequirements(List<SpecializationRequirement__c> lstSpeReq, String newStatus)
    {
        map<id,List<SpecializationRequirement__c>> mpSpeIdtoSpeReqs = new map<id,List<SpecializationRequirement__c>>();
        for(SpecializationRequirement__c sr : lstSpeReq)
        {
            if(!mpSpeIdtoSpeReqs.containsKey(sr.Specialization__c))
                mpSpeIdtoSpeReqs.put(sr.Specialization__c, new List<SpecializationRequirement__c>{sr});
            else
                mpSpeIdtoSpeReqs.get(sr.Specialization__c).add(sr);
        }

        List<AccountSpecialization__c> lstAccSpe = [SELECT id, name, Status__c,Specialization__c, Account__c  FROM AccountSpecialization__c WHERE Specialization__c in:mpSpeIdtoSpeReqs.keyset()];
        
        //Exisinting Account Specilization Requirements
        List<AccountSpecializationRequirement__c> lstExistAccSpeReqs = [SELECT AccountSpecialization__c,SpecializationRequirement__c from AccountSpecializationRequirement__c WHERE SpecializationRequirement__c in:lstSpeReq AND AccountSpecialization__c in:lstAccSpe];
        set<String> setExistingAccSpeReq = new set<String>();
        for(AccountSpecializationRequirement__c tmpASR : lstExistAccSpeReqs)
        {
            setExistingAccSpeReq.add(tmpASR.AccountSpecialization__c + '-'+tmpASR.SpecializationRequirement__c);
        }    

        /////------
        //list Account Specialization Requirements to insert
        List<AccountSpecializationRequirement__c> lstAccSpeReqToInsert = new List<AccountSpecializationRequirement__c>();
        for(AccountSpecialization__c accspe : lstAccSpe)
        {
            for(SpecializationRequirement__c sr : mpSpeIdtoSpeReqs.get(accspe.Specialization__c))
            {
                String tmpStr = accspe.id+'-'+sr.id;
                if(!setExistingAccSpeReq.contains(tmpStr))
                {
                    AccountSpecializationRequirement__c asr = new AccountSpecializationRequirement__c();
                    asr.SpecializationRequirement__c = sr.id;
                    asr.AccountSpecialization__c = accspe.id;
                    asr.status__c = newStatus;
                    //TODO
                    asr.recordtypeid = CS_RequirementRecordType__c.getValues(sr.RequirementCatalog__r.RecordTypeId).ASPRRecordTypeID__c;
                                

                    lstAccSpeReqToInsert.add(asr);  
                }
                
            }
        }

        insert lstAccSpeReqToInsert;    
    }//End of method


    //inactivate Account Specialization Requirements, when the a sepcialization Requirement is meade inactive
    public static void deActivateChildAccountSpecializationRequirements(List<SpecializationRequirement__c> lstSpeReq)
    {
        List<AccountSpecializationRequirement__c> lstAccSpeReq = [SELECT name, Status__c from AccountSpecializationRequirement__c where SpecializationRequirement__c = :lstSpeReq];
        for(AccountSpecializationRequirement__c asreq: lstAccSpeReq)
        {
            asreq.status__c = 'Inactive';

        }   

        update  lstAccSpeReq;
    }



}//End of class