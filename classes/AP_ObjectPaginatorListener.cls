global interface AP_ObjectPaginatorListener {
    void handlePageChange(List<Object> newPage);
}