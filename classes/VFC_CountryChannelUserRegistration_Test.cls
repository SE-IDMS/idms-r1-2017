@isTest
public class VFC_CountryChannelUserRegistration_Test {
    
    static TestMethod void CountryChannelUserRegistrationTest() {
        List<CS_PRM_UserRegistration__c> cs_RecList = new List<CS_PRM_UserRegistration__c>();
        List<CountryChannelUserRegistration__c> userRegFieldsList = new List<CountryChannelUserRegistration__c>();
        Country__c country= Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c state = Utils_TestMethods.createStateProvince(country.Id);
        insert state;
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild; 
        
        CountryChannels__c cChannels = new CountryChannels__c();
        cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
        cChannels.Country__c = country.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true;
        insert cChannels;
        
        User u = Utils_TestMethods.createStandardUser('Ssahu');
        
        PRMAccountOwnerAssignment__c prmAccountOwnerAssgn = new PRMAccountOwnerAssignment__c();
        prmAccountOwnerAssgn.Name               = cChannels.ChannelCode__c;
        prmAccountOwnerAssgn.AccountOwner__c    = u.Id;
        prmAccountOwnerAssgn.Classification__c  = cChannels.ChannelCode__c;
        prmAccountOwnerAssgn.Country__c         = cChannels.Country__c;
        prmAccountOwnerAssgn.StateProvince__c   = state.Id;
        prmAccountOwnerAssgn.SubClassification__c = cChannels.SubChannelCode__c;
        insert prmAccountOwnerAssgn;
        
        
        CS_PRM_UserRegistration__c cs_Rec = new CS_PRM_UserRegistration__c(AppliedTo__c = 1, FieldLabel__c = 'Contact: Email', FieldSortOrder__c = 1, MappedField__c = 'PRMEmail__c', MappedObject__c = 'Contact', Name = 'email');
        cs_RecList .add(cs_Rec);
        CS_PRM_UserRegistration__c cs_Rec2 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1, FieldLabel__c = 'Contact: First Name', FieldSortOrder__c = 2, MappedField__c = 'PRMFirstName__c ', MappedObject__c = 'Contact', Name = 'firstName');
        cs_RecList .add(cs_Rec2);
        CS_PRM_UserRegistration__c cs_Rec3 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Contact: Email', FieldSortOrder__c = 3, MappedField__c = 'PRMLastName__c', MappedObject__c = 'Contact', Name = 'lastName');
        cs_RecList.add(cs_Rec3);
        CS_PRM_UserRegistration__c cs_Rec4 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Contact: Business Type', FieldSortOrder__c = 4, MappedField__c = 'PRMBusinessType__c', MappedObject__c = 'Account', Name = 'businessType');
        cs_RecList.add(cs_Rec4);
        CS_PRM_UserRegistration__c cs_Rec5 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Contact: Area of Focus', FieldSortOrder__c = 5, MappedField__c = 'PRMAreaOfFocus__c', MappedObject__c = 'Account', Name = 'areaOfFocus');
        cs_RecList.add(cs_Rec5);
        CS_PRM_UserRegistration__c cs_Rec6 = new CS_PRM_UserRegistration__c(AppliedTo__c = 2,FieldLabel__c = 'Your Account: Company Country', FieldSortOrder__c = 13, MappedField__c = 'PRMCountry__c', MappedObject__c = 'Account', Name = 'companyCountry');
        cs_RecList.add(cs_Rec6);
        CS_PRM_UserRegistration__c cs_Rec7 = new CS_PRM_UserRegistration__c(AppliedTo__c = 2,FieldLabel__c = 'Your Account: State / Province', FieldSortOrder__c = 17, MappedField__c = 'PRMStateProvince__c', MappedObject__c = 'Account', Name = 'companyStateProvince');
        cs_RecList.add(cs_Rec7);
        insert cs_RecList;
        
        CountryChannelUserRegistration__c counChaUserReg7 = new CountryChannelUserRegistration__c (CountryChannels__c = cChannels.id, UserRegFieldName__c = 'companyStateProvince');
        userRegFieldsList.add(counChaUserReg7);
        insert userRegFieldsList;
        
        
        PageReference pageRef = Page.VFP_CountryChannelUserRegistration;
        Test.setCurrentPage(pageRef);
        Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(cChannels);
        ApexPages.currentPage().getParameters().put('Id',cChannels.id);
        VFC_CountryChannelUserRegistration thisCountryChannelRegistration = new VFC_CountryChannelUserRegistration(thisController);
        thisCountryChannelRegistration.companyStateProvince(country.Id);
        thisCountryChannelRegistration.doSave();
       
    }
    
}