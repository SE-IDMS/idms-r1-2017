@isTest
private class VFC_AddRIAMProduct_TEST
{
    static testMethod void testVFC_AddRIAMProduct() 
    {
       Country__c country = Utils_TestMethods.createCountry();
        insert country;

       
        
        
        ReturnItemsApprovalMatrix__c RIAM = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = Country.Id,
                                 CommercialReference__c='xbtg5230',
                                 ProductFamily__c='HMI SCHNEIDER BRAND',
                                 CustomerResolution__c = 'REP',                                                             
                                 Category__c = '4 - Post-Sales Tech Support',  
                                 Reason__c = 'Installation/ Setup',
                                 SubReason__c ='Hardware',
                                 BusinessUnit__c = 'BusinessUnit__c',
                                 ProductLine__c='ProductLine__c',
                                 Family__c='Family__c'
                                 );
    Insert RIAM;
        //String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        OPP_Product__c oppPdct = new OPP_Product__c(
                                Name = 'Name',
                                TECH_PM0CodeInGMR__c = 'GMR business__c',
                                IsActive__c = true
                                );
        insert oppPdct;
        
        //global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        List<Object> rs8 = VFC_AddRIAMProduct.remoteSearch('searchString','gmrcode1',true,true,true,true,true);
        List<Object> rs6 = VFC_AddRIAMProduct.remoteSearch('searchString','gmrcod',true,true,true,true,true);
        List<Object> rs4 = VFC_AddRIAMProduct.remoteSearch('searchString','gmrc',true,true,true,true,true);
        List<Object> rs2 = VFC_AddRIAMProduct.remoteSearch('searchString','gm',true,true,true,true,true);
        
        //global static String getSearchText()
        String str = VFC_AddRIAMProduct.getSearchText();
        
        //global static List<OPP_Product__c> getOthers(String business)
        List<OPP_Product__c> opp = VFC_AddRIAMProduct.getOthers('business');
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(RIAM);   
        VFC_AddRIAMProduct GMRSearchPage = new VFC_AddRIAMProduct(CaseStandardController );
        
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        GMRSearchPage.jsonSelectString=jsonSelectString;
       // GMRSearchPage.Mode='ordered';
        Test.startTest();
        PageReference pageRef = Page.VFP_AddRIAMProduct;
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);
         pageRef.getParameters().put('Mode','ordered');
        Test.setCurrentPage(pageRef);
      //  GMRSearchPage.ChangeProduct = true;
        GMRSearchPage.performSelect();
        GMRSearchPage.performSelectFamily();
        GMRSearchPage.pageCancelFunction();
       
              Test.stopTest(); 
    }
}