@isTest
Public class VFC_OPPintermediate_Test {
    static testMethod void myUnitTest() {
    PageReference pageRef = Page.VFP_OPPintermediate;
        Test.setCurrentPage(pageRef);
        opportunity opp= new opportunity(name='testopp',StageName='2 - Define Opportunity Portfolio',CloseDate=system.today());
        insert opp;
         VFC_OPPintermediate VFController = new VFC_OPPintermediate(new ApexPages.StandardController(opp));
         VFController.RedirecPMOpage();
         VFController.selectedvalue='By Installed Product';
         VFController.RedirecSearchpage();
    }
     static testMethod void myUnitTest1() {
    PageReference pageRef = Page.VFP_OPPintermediate;
        Test.setCurrentPage(pageRef);
        opportunity opp= new opportunity(name='testopp',StageName='2 - Define Opportunity Portfolio',CloseDate=system.today());
        insert opp;
         VFC_OPPintermediate VFController = new VFC_OPPintermediate(new ApexPages.StandardController(opp));
         VFController.selectedvalue='By PMO Selection';
         VFController.RedirecPMOpage();
         VFController.RedirecSearchpage();
    }
     static testMethod void myUnitTest3() {
     
        User user = new User(alias = 'user', email='user' + '@accenture.com', ServiceMaxUser__c=true,
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000');
       
        insert user;
        system.runas(user){
    PageReference pageRef = Page.VFP_OPPintermediate;
        Test.setCurrentPage(pageRef);
        opportunity opp= new opportunity(name='testopp',StageName='2 - Define Opportunity Portfolio',CloseDate=system.today());
        insert opp;
         VFC_OPPintermediate VFController = new VFC_OPPintermediate(new ApexPages.StandardController(opp));
         VFController.RedirecPMOpage();
         VFController.selectedvalue='none';
         VFController.RedirecSearchpage();
         }
    }
}