/*
    Author          : Accenture IDC Team 
    Date Created    : 18/07/2011
    Description     : Class utilised by triggers QuoteLinkAfterUpdate, QuoteLinkAfterInsert, QuoteLinkBeforeDelete to
                      to update the Quote ISsuance Date to Customer field of related Opportunity.
*/
public class AP21_UpdateOpportunity
{
    /**Method automatically updates the Quote Issuance Date to customer field 
     from the active quotes attached to the Opportunity
    */
    public static void updtQteIsuDateToCust(List<OPP_Quotelink__c> newQuote, Boolean isDelete)
    {
        System.Debug('############ START of updtQteIsuDateToCust method ############');
        // Initializing the Variables
        List<Opportunity> updOpportunity = new List<Opportunity>();
        Set<Id> oppId = new Set<Id>();
        Set<Id> delQuoteId = new Set<ID>();
        Map<Id,DateTime> opportunity = new Map<Id,DateTime>();
        Map<Id, Opportunity> oppToUpdate = New Map<Id, Opportunity>();
        Boolean isAdd = False; // Variable to determine whether Opportunity Needs to be updated

        for(OPP_Quotelink__c quotes:newQuote)    
        {
            oppId.add(quotes.OpportunityName__c); // List of Related Opportunity Ids
            // Checking whether Code has beem called from Before Delete Trigger
            if(isDelete)
                delQuoteId.add(quotes.Id); // List of QuotelInk Ids to be deleted    
        }
        
        // Searching All the Active QuoteLink associated with the Opportunity Id list created in previous Step and Iterating the Result Set.
        for(OPP_Quotelink__c quote : [select OpportunityName__c,createdDate,IssuedQuoteDateToCustomer__c from OPP_Quotelink__c  where OpportunityName__c IN:oppId AND ActiveQuote__c=true order By createdDate desc])
        {
            // Checking whether Map contains the current Opportunity ID or Not.
            // If Doesn't contain add the Opportunity Id as the key and current quotelink's created date as value to the Map.
            // If contains then check the stored created date is greater than the Current quotelink's cretaed Date.
            if(opportunity.isEmpty() || !opportunity.containsKey(quote.OpportunityName__c))
            {                
                if(quote.IssuedQuoteDateToCustomer__c != NULL) // Check that IssuedQuoteDate field has some valid value.
                {                    
                    if(isDelete) // Codeblock to be executed when Quote is deleted. 
                    {
                        // If the Current Quote Id is not in Delted Quote ID list then Add the related Opportunity as Key and Quote Created Date to the Map and
                        // add the Opporunity value to a List to Update. 
                        if(!delQuoteId.Contains(quote.Id)) // If the Current Quote is not the Deleted Quote.
                        {
                            opportunity.put(quote.OpportunityName__c,quote.createdDate); // Adding the Related Opportunity ID and Quote Created Date to Map
                            isAdd = True;
                        }
                    }
                    else // Codeblock to be executed when Quote is Inserted or Updated.
                    {
                        opportunity.put(quote.OpportunityName__c,quote.createdDate);
                        isAdd = True;
                    }
                }
            }
            else
            {
                if(opportunity.get(quote.OpportunityName__c) < quote.createdDate)// Compare whether current Quote's created date is greater than other Quotes associated with Same Opportunity. Then only update the value.
                {
                    isAdd = True;     
                }
            }
            
            if(isAdd)
            {
                oppToUpdate.put(quote.OpportunityName__c,new Opportunity(Id = quote.OpportunityName__c, QuoteIssuanceDateToCustomer__c = quote.IssuedQuoteDateToCustomer__c)); // Creating the Opportunity Data. Adding the Opportunity to Update.
                oppId.remove(quote.OpportunityName__c); // Removing the Opportunity Id from the List to filter out the Opportunities for which Issued Quote Date to Customer should be NULL.
                isAdd = False;
            }   
        }
        
        System.debug('AP21.updtQteIsuDateToCust.INFO - Opportunity to update list =' +oppToUpdate );
        
        if(!oppToUpdate.isEmpty())// Check whether Opportunity to Update Set is Empty or not.
        { 
           updOpportunity.addAll(oppToUpdate.values()); // Adding the Opportunity Set to a list.
        }
        else
        {
            // If Opportunity to Update Set is Empty and the Initial Opportunity Id has the value
            // then set the Issued Quote Date to Customer for those Opportunitiesa to NULL.
            for(Id ids : oppId)
            {
                Opportunity opps = new Opportunity(Id = ids, QuoteIssuanceDateToCustomer__c = NULL);
                updOpportunity.add(opps);
            }    
        }
        
        System.debug('AP21.updtQteIsuDateToCust.INFO - Updated opportunity =' + updOpportunity);
        
        if(!updOpportunity.isEmpty())
        {
            if(updOpportunity.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                System.Debug('######## AP21 Error Update: '+ Label.CL00264);
            else
            {
                Database.saveResult []svr = Database.update(updOpportunity); // Updating the Opportunity.
                for(Database.SaveResult svrt : svr)
                {
                    if(!svrt.isSuccess())
                        System.Debug('######## AP21 Error Update: '+ svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());              
                }
            }
        }
        System.Debug('############ END of updtQteIsuDateToCust method ############');
    } 

    public static void updateSolutionCenterInOpportunity(List<OPP_SupportRequest__c> newSupportReq)
    {
        //*********************************************************************************
        // Method Name      : updateSolutionCenterInOpportunity
        // Purpose          : On Creation on Supports Request it will update Opportunity Field SolutionCenter when it was empty.
        // Created by       : Hari Krishna - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For April Sales - 12 Release
        ///********************************************************************************/
        
         System.Debug('############ START of updateSolutionCenterInOpportunity method ############');
         
         Set<id> oppidSet=new Set<id>();
         List<Opportunity> opplist=new List<Opportunity>();
         List<Opportunity> opplisttoupdate=new List<Opportunity>();
         Map<id,Opportunity> oppidRecMap = new Map<id,Opportunity>();
         for(OPP_SupportRequest__c suppRec:newSupportReq){
            if(suppRec.Opportunity__c != null){
                oppidSet.add(suppRec.Opportunity__c);               
            }
         }
         if(oppidSet != null && oppidSet.size()>0){
            opplist =[Select id,name,SolutionCtr__c from Opportunity where id in :oppidSet and SolutionCtr__c = null];
            if(opplist != null && opplist.size()>0){
                oppidRecMap.putAll(opplist);
            }
         }
         for(OPP_SupportRequest__c suppRec:newSupportReq){
                if(suppRec.SolutionCenter__c != null ){
                    
                    if(suppRec.Opportunity__c != null){
                        
                        if(oppidRecMap.containsKey(suppRec.Opportunity__c)){
                            
                            Opportunity opp=oppidRecMap.get(suppRec.Opportunity__c);
                            if(opp.SolutionCtr__c == null ){
                                opp.SolutionCtr__c = suppRec.SolutionCenter__c;
                                opplisttoupdate.add(opp);
                            }
                            
                        }
                        
                    }
                }
            
         }
         if(opplisttoupdate != null && opplisttoupdate.size()>0){
            update opplisttoupdate;
         }
         
         System.Debug('############ END of updateSolutionCenterInOpportunity method ############');  
    }
    
    
    public static void updateTECH_HasActiveQLKwthMargin(List<OPP_Quotelink__c> QLKs, String triggerType)
    //public static void updateTECH_HasActiveQLKwthMargin(List<OPP_Quotelink__c> QLKs)
    {
        //*********************************************************************************
        // Method Name      : updateTECH_HasActiveQLKwthMargin
        // Purpose          : On Creation/Update of QLK update the TECH field Has an Active QLK with Margin filled
        // Created by       : Jules Faligot  - Accenture
        // Date created     : 20/02/2012
        // Modified by      : 
        // Date Modified    :
        // Remarks          : For April Sales - 12 Release
        ///********************************************************************************/
        try{
            if(QLKs!=null && !QLKs.isEmpty()){
                Set<ID> OppIDs = New Set<ID>();                     //Set of Opportunity ID to update
                List<Opportunity> Opps = New List<Opportunity>();   //List of Opportunity to update
                
                for(OPP_Quotelink__c qlk : QLKs){
                    OppIDs.add(qlk.OpportunityName__c);    //add the qlk's opportunity id in the set
                }
                system.debug('#### quotes from trigger'+QLKs);
                
                //set the Opp tech field "Has an active qlk with a margin captured" to false on all Opp
                Opps = [SELECT ID, TECH_HasActiveQLKwthMargin__c FROM Opportunity WHERE ID IN: OppIDs];
                if(!Opps.isEmpty()){
                    for(Opportunity o: Opps){
                        o.TECH_HasActiveQLKwthMargin__c = false;
                    }
                }
                system.debug('#### opp to update' + Opps);
                system.debug('#### opp ids ' + OppIDs);
                //Loop over all qlks and set the Opp tech field "Has an active qlk with a margin captured" to true when the quote has a margin captured
                List<OPP_Quotelink__c> quoteList = [SELECT ID, Name, OpportunityName__c, OpportunityName__r.TECH_HasActiveQLKwthMargin__c, Margin__c, ActiveQuote__c FROM OPP_Quotelink__c  WHERE OpportunityName__c IN: OppIDs];
                system.debug('#### list of quotes related to the opps to update' + quoteList);
                
                if(triggerType == Label.CL00749){ //coming from before insert trigger
                    for(OPP_QuoteLink__c qlkfromtrigger : QLKs){
                        quoteList.add(qlkfromtrigger);
                    }
                }
                system.debug('#### list of quotes (db+trigger)related to the opps to update' + quoteList);
                if(!quoteList.isEmpty()){
                    for(OPP_QuoteLink__c qlk : quoteList){
                        Boolean qlkComesFromTrigger = false;
                        for(OPP_QuoteLink__c qlkfromtrigger : QLKs){     // if the quote is from the trigger
                            if(qlk.id == qlkfromtrigger.id){              
                                qlk = qlkfromtrigger;                    //use the quote from the trigger
                                qlkComesFromTrigger = true;
                                break;       
                            }
                        }
                        system.debug('#### qlkComesFromTrigger 1' + qlkComesFromTrigger );
                        if(qlk.margin__c!=null && !Opps.isEmpty() && qlk.ActiveQuote__c){
                            for(Opportunity o : Opps){
                                if(qlk.OpportunityName__c == o.id){ //for each active qlk with a margin, find the related opp and update it
                                    if(triggerType == Label.CL00749 || triggerType == Label.CL00751){  // if not coming from before delete trigger
                                        o.TECH_HasActiveQLKwthMargin__c = true;
                                    }
                                    else if(!qlkComesFromTrigger){    //if coming from before delete trigger && quote is not coming from trigger's list  
                                        o.TECH_HasActiveQLKwthMargin__c = true ;
                                    }
                                }
                            }
                        }
                    }    
                }
                system.debug('#### opp after update before dml' + opps);
                if(!Opps.isEmpty()){
                    if(Opps.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                        System.Debug('######## AP21 Error Update: '+ Label.CL00264);
                    else{
                        Database.saveResult []svr = Database.update(Opps, false); // Updating the Opportunity.
                        for(Database.SaveResult svrt : svr)
                        {
                            if(!svrt.isSuccess()){
                                System.Debug('######## AP21 Error Update: '+ svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());
                                for(OPP_QuoteLink__c qlk : QLKs){
                                    qlk.addError(svrt.getErrors()[0].getMessage());
                                }
                            }           
                        }
                    }
                } 
            }         
        }
        catch(Exception e){
            system.debug('#### Exception: ' + e);
        }
    }
     public static void updateOpportunityTECHActiveWthApproved(List<OPP_Quotelink__c> QLKs)
     {
        //*********************************************************************************
        // Method Name      : updateOpportunityTECHActiveWthApproved
        // Purpose          : 
        // Created by       : Hari Krishna - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For December Sales - 12 Release
        ///********************************************************************************/
        if(QLKs!= null && QLKs.size()>0){
        
            Set<id> OppIDs = new Set<id>();
            List<Opportunity> Opps = New List<Opportunity>();
            
            for(OPP_Quotelink__c ql:QLKs){
                
                if(ql.QuoteStatus__c == 'Approved') {
                    OppIDs.add(ql.OpportunityName__c);
                }
                
            }
            
            if(OppIDs != null && OppIDs.size()>0){
                
                Opps = [SELECT ID, TECH_HasActiveQLKwthApproved__c FROM Opportunity WHERE ID IN: OppIDs];
                if(!Opps.isEmpty()){
                    for(Opportunity o: Opps){
                        if(!o.TECH_HasActiveQLKwthApproved__c)
                        o.TECH_HasActiveQLKwthApproved__c= true;
                    }
                    if(Opps.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                        System.Debug('######## AP21 Error Update: '+ Label.CL00264);
                    else{
                        Database.saveResult []svr = Database.update(Opps, false); // Updating the Opportunity.
                        for(Database.SaveResult svrt : svr)
                        {
                            if(!svrt.isSuccess()){
                                System.Debug('######## AP21 Error Update: '+ svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());
                                for(OPP_QuoteLink__c qlk : QLKs){
                                    qlk.addError(svrt.getErrors()[0].getMessage());
                                }    
                            }                           
                          
                        }
                    }
                }
                
            }
        
        }
        
         
     }
    
}