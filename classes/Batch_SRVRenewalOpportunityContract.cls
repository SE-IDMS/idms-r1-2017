global class Batch_SRVRenewalOpportunityContract implements Database.Batchable<sObject>, Schedulable {
   
    
    String ServiceContractRT = System.Label.CLAPR15SRV01;
     public Integer rescheduleInterval = Integer.ValueOf(Label.CLOCT15SRV07);
     public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
     public String scheduledJobName = 'Batch_SRVRenewalOpportunityContract';
     public Integer scopelimit = Integer.ValueOf(Label.CLQ316SRV04);
     String ServiceContractConnectedRT = System.Label.CLAPR15SRV04;
     Date stDate =System.today()-Integer.ValueOf(Label.CLJULY16SRV05);
     Date eDate =System.today()-Integer.ValueOf(Label.CLJULY16SRV06);
     String WhereClause =Label.CLJULY16SRV07;
    //Public String query ='SELECT ID, Tech_RenewalOpportunityCreat__c FROM SVMXC__Service_Contract__c WHERE (EarliestEndDate__c != TODAY AND SVMXC__Active__c = TRUE AND LeadingBusinessBU__c !=null AND TECH_RenewalOpptyDate__c != NULL AND (RecordTYPeid = :ServiceContractRT OR  RecordTYPeid = :ServiceContractConnectedRT )AND ((IsEvergreen__c= TRUE AND SendCustomerRenewalNotification__c = TRUE) OR (IsEvergreen__c= FALSE AND SendCustomerRenewalNotification__c = FALSE)))'; 
    String query ='SELECT name,ID, Tech_RenewalOpportunityCreat__c FROM SVMXC__Service_Contract__c WHERE ((TECH_RenewalOpptyDate__c  >= : stDate and TECH_RenewalOpptyDate__c <= : eDate) and Tech_RenewalOpportunityCreat__c = false AND LatestRenewalOpportunity__c =Null AND LeadingBusinessBU__c !=null  AND (RecordTYPeid = :ServiceContractRT OR RecordTYPeid = :ServiceContractConnectedRT )AND ((IsEvergreen__c= TRUE AND SendCustomerRenewalNotification__c = TRUE) OR (IsEvergreen__c= FALSE AND SendCustomerRenewalNotification__c = FALSE)))'+' And '+WhereClause;
    
    
    global Database.Querylocator start(Database.BatchableContext BC){
       
        return Database.getQueryLocator(query);    
       }
    
    global void execute(Database.BatchableContext BC, List <sObject> scope){
       List<SVMXC__Service_Contract__c> servContrList =  new List<SVMXC__Service_Contract__c>();
       List<SVMXC__Service_Contract__c> scList = (List<SVMXC__Service_Contract__c>) scope;
       //List<SVMXC__Service_Contract__c> recList =new List<SVMXC__Service_Contract__c>();
       //recList.addAll(scope);
        System.debug('Scope SIze :'+scList.size());
        for(SVMXC__Service_Contract__c rec: scList){
           rec.Tech_RenewalOpportunityCreat__c = true;
           servContrList.add(rec);
        } 
        System.debug('List Size :'+servContrList.size());
        
        try{
             Database.Update(servContrList ,false) ;
        }
        catch(Exception ex){
            System.debug(' Exception ex:'+ex.getMessage());
        }
        
    }
    
global void finish(Database.BatchableContext BC){
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);

        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new Batch_SRVRenewalOpportunityContract(), scheduledJobName, rescheduleInterval);
        }
    }
    global void execute(SchedulableContext sc) {
        Integer ScopeValue = Integer.ValueOf(Label.CLJULY16SRV08);
        Batch_SRVRenewalOpportunityContract OppnotifCreation = new Batch_SRVRenewalOpportunityContract();
        Database.executeBatch(OppnotifCreation,scopelimit);
    }   

}