/**
Pooja Suresh  
Q2 2016 Release
Test Class for auto-merge functionality of Consumer Account : Batch_ConsAcctsAutomatebFOMerge
 */

@isTest
private class Batch_ConsAcctsAutomatebFOMerge_TEST
{

    @testSetup static void createTestData()
    {
        User deactiveUser = Utils_TestMethods.createStandardUser('deacUser');
        insert deactiveUser;
    
        User admin = Utils_TestMethods.createStandardUser('TestUser');
        admin.BypassVR__c=true;
        admin.BypassWF__c=true;
        admin.BypassTriggers__c = 'AP30;AP_ACC_PartnerAccountUpdate;PartnerLocatorPushEventsService;SyncObjects;AP_ACCCreateWithSEAccountId;AP_AccountBeforeInsertHandler;AP_AURRecordUpdate;AP_CIS_ISRHandler;AP_Opp_UpdateParentCloseDate;AP_SendOpportunityNotification;AP_SPARequestCalculations_opp;AP01;AP05;AP08;AP44;AP49;CaseAfterUpdate;CaseBeforeUpdate;PRM_FieldChangeListener;SRV01;SRV03;SVMX05;SVMX07;SVMX08;SVMX09;SVMX10;SVMX14;SVMX16;SVMX22;SVMX_InstalledProduct2;SVMX22';
        insert admin;

        System.RunAs(admin)
        {
            Country__c country = new Country__c(Name='TestCountry1', CountryCode__c='IT');
            insert country;

            Account masterConsAcct  = Utils_TestMethods.createConsumerAccount(country.Id);
            masterConsAcct.IsMasterSalesContact__pc = true;
            masterConsAcct.SEAccountID__c = 'TEST0001234';
            insert masterConsAcct;

            Account slaveConsAcct  = Utils_TestMethods.createConsumerAccount(country.Id);
            insert slaveConsAcct;
            
            slaveConsAcct.SEAccountID__c = 'TEST0001234';
            update slaveConsAcct;
            
            Account slaveConsAcc = [SELECT PersonContactId from Account where Id=:slaveConsAcct.Id];

            Opportunity slaveOpp = new Opportunity(Name='Test Opp Parent',RecordTypeId='012A0000000nYNa', AccountId=slaveConsAcct.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=country.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R', OpptyType__c='Standard', closedate=Date.today());
            insert slaveOpp;

            Case slaveCase = new Case(AccountId=slaveConsAcct.Id,Status='New',SupportCategory__c='6-General',Priority='Medium',Subject='Test Case for Merge',origin='Phone');
            insert slaveCase;

            Case slaveCaseFail = new Case(AccountId=slaveConsAcct.Id,Status='New',SupportCategory__c='6-General',Priority='Medium',Subject='Test Case for Merge',origin='Phone',OwnerId=deactiveUser.Id);
            insert slaveCaseFail;
            
            CIS_ISR__c slaveISR1 = new CIS_ISR__c(Name='Test ISR 1',CIS_EndUserContact__c=slaveConsAcc.PersonContactId );
            insert slaveISR1;

            CIS_ISR__c slaveISR2 = new CIS_ISR__c(Name='Test ISR 2',CIS_OEMContact__c=slaveConsAcc.PersonContactId );
            insert slaveISR2;

            AccountUpdateRequest__c slaveAUR = new AccountUpdateRequest__c(Account__c = slaveConsAcct.id, RecordTypeId=System.Label.CLMAR16ACC01, AccountName__c='Test AUR for Merge');
            insert slaveAUR;

            LiveChatVisitor slaveVisitor = new LiveChatVisitor();
            insert slaveVisitor;

            LiveChatTranscript slaveChat = new LiveChatTranscript(AccountId=slaveConsAcct.Id, CaseId=slaveCase.Id, LiveChatVisitorId=slaveVisitor.Id);
            insert slaveChat;

            CustomerLocation__c slaveCustLocation  = Utils_TestMethods.createCustomerLocation(slaveConsAcct.Id,country.Id);
            insert slaveCustLocation;

            LegacyAccount__c slaveLegAcct  = Utils_TestMethods.createLegacyAccount(slaveConsAcct.Id);
            insert slaveLegAcct;

            ANI__c slaveANI = new ANI__c(ANINumber__c= '+9000000001', Account__c = slaveConsAcct.Id);
            insert slaveANI;

            CustomerSatisfactionSurvey__c slaveCustSatSur = new CustomerSatisfactionSurvey__c(GoldenCustomerSatisfactionSurveyId__c= 'CustomerSatisfactionSurveyId',Account__c = slaveConsAcct.id);
            insert slaveCustSatSur;

            Role__c slaveServiceRole1 = new Role__c(Account__c = slaveConsAcct.Id, RecordTypeId = System.Label.CLNOV13SRV11 , Role__c = System.Label.CLOCT13SRV51 );
            insert slaveServiceRole1;

            Role__c slaveServiceRole2 = new Role__c(RecipientAccount__c = slaveConsAcct.Id, RecordTypeId = System.Label.CLNOV13SRV11 , Role__c = System.Label.CLOCT13SRV51 );
            insert slaveServiceRole2;

            SVMXC__Service_Contract__c slaveServContract = new SVMXC__Service_Contract__c(Name = 'Test',SoldtoAccount__c = slaveConsAcct.Id,SVMXC__End_Date__c=date.today(),SVMXC__Start_Date__c=date.Today());
            insert slaveServContract;

            SVMXC__Service_Contract_Services__c slaveServices = new SVMXC__Service_Contract_Services__c(ShipTo__c = slaveConsAcct.Id,SVMXC__Service_Contract__c =slaveServContract.Id);
            insert slaveServices;

            SVMXC__Service_Order__c slaveWorkOrder = new SVMXC__Service_Order__c(BillToAccount__c=slaveConsAcct.Id, SVMXC__Order_Status__c = 'Customer Confirmed');
            insert slaveWorkOrder;

            WorkOrderNotification__c slaveWorkOrderNotif = new WorkOrderNotification__c(SoldToAccount__c=slaveConsAcct.Id, WorkOrderReason__c='Open');
            insert slaveWorkOrderNotif;
            
            SVMXC__Site__c slaveSite = new SVMXC__Site__c(Name = 'Test Location',SVMXC__Street__c  = 'Test Street',SVMXC__Account__c = slaveConsAcct.Id,PrimaryLocation__c = true);            
            insert slaveSite;
            
            SVMXC__Installed_Product__c slaveInstalledProd = new SVMXC__Installed_Product__c(SVMXC__Company__c=slaveConsAcct.Id, Name='Test Installed Product', SVMXC__Status__c= 'New', GoldenAssetId__c = 'GoledenAssertId1', BrandToCreate__c ='Test Brand', DeviceTypeToCreate__c = 'device1');
            insert slaveInstalledProd;

            GCSSite__c slaveGCS = new GCSSite__c(Account__c=slaveConsAcct.Id,LegacyIBSiteID__c='TestIBSiteID1');
            insert slaveGCS;

            CustomerQuickQuestion__c slaveQuickQues1 = new CustomerQuickQuestion__c();
            slaveQuickQues1.Contact__c=slaveConsAcc.PersonContactId;
            slaveQuickQues1.Account__c=slaveConsAcct.Id;
            insert slaveQuickQues1;
            
            LocalAttribute__c slaveLocalAttr = new LocalAttribute__c(Name='Test LocalAttribute',  Category__c = 'Quality and Control');
            insert slaveLocalAttr;
            
            ACC_LocalAttribute__c slaveAcctLocalAttr = New ACC_LocalAttribute__c(Account__c=slaveConsAcct.Id, Comments__c='TestComment', LocalAttribute__c=slaveLocalAttr.Id);
            insert slaveAcctLocalAttr;

            Note slaveNote = new Note(Title='TestNote', Body='test', ParentId =slaveConsAcct.Id);
            insert slaveNote;

            Attachment slaveAttachment = new Attachment(Name='TestAttachment', Body=Blob.valueOf('Test Data'), ParentId =slaveConsAcct.Id); 
            insert slaveAttachment;
            
            deactiveUser.IsActive=false;
            update deactiveUser;
        }
        
    }

    static testmethod void myUnitTest()
    {
        User admin = Utils_TestMethods.createStandardUser('TestUser');
        admin.BypassVR__c=true;
        admin.BypassWF__c=true;
        admin.BypassTriggers__c = 'AP30;AP_ACC_PartnerAccountUpdate;PartnerLocatorPushEventsService;SyncObjects;AP_ACCCreateWithSEAccountId;AP_AccountBeforeInsertHandler;AP_AURRecordUpdate;AP_CIS_ISRHandler;AP_Opp_UpdateParentCloseDate;AP_SendOpportunityNotification;AP_SPARequestCalculations_opp;AP01;AP05;AP08;AP44;AP49;CaseAfterUpdate;CaseBeforeUpdate;PRM_FieldChangeListener;SRV01;SRV03;SVMX05;SVMX07;SVMX08;SVMX09;SVMX10;SVMX14;SVMX16;SVMX22;SVMX_InstalledProduct2;SVMX22';
        insert admin;

        System.RunAs(admin)
        {
            Batch_ConsAcctsAutomatebFOMerge batchCont = new Batch_ConsAcctsAutomatebFOMerge();
            database.executeBatch(batchCont);
        }
    }

}