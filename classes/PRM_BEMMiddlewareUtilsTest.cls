@IsTest
public with sharing class PRM_BEMMiddlewareUtilsTest {
  static testMethod void testBemCallout(){
    
    Test.setMock(HttpCalloutMock.class, new FieloPRM_VFC_SearchGeneralSearchTest.MockSearchHttpResponseGenerator(200,
                          'Complete',
                                                    '{"name": "myProject","elements":    [{"name": "newLeads",         "content": "1"      },            {         "name": "actionRequest",         "content": "3"      },            {         "name": "ongoing",         "content": "10"      }   ]}',
                                                    null));
        Test.startTest();
        PRMCockpitComponentConfiguration__c myCmp = new PRMCockpitComponentConfiguration__c(WSURL__c='http://www.google.com');
        PRM_BEMMiddlewareUtils.WidgetBean widgetBean = PRM_BEMMiddlewareUtils.getWidget(myCmp,new PRM_BEMMiddlewareUtils.ParameterBean('myProject','TOKEN','FEDID','test@accenture.com','IN'),null);
        String testName = widgetBean.name;
        Long testCalc = widgetBean.calculationTimeInMillisecondes;
        //System.assertEquals('myProject',widgetBean.name);
        PRM_BEMMiddlewareUtils bemmid = new PRM_BEMMiddlewareUtils();
        PRM_BEMMiddlewareUtils.ElementBean elementBean = new PRM_BEMMiddlewareUtils.ElementBean();      
        String testElementBean = elementBean.name;
        String testElementBeanContent = elementBean.content;
        Test.stopTest();
  }


  static testMethod void testBemCallout2(){
    

    User us = Utils_TestMethods.createStandardUser('123!');
    us.BypassVR__c=True;
    us.PRMTemporarySamlToken__c='testToken';
    insert us;

    System.runAs(us){  
        Test.setMock(HttpCalloutMock.class, new FieloPRM_VFC_SearchGeneralSearchTest.MockSearchHttpResponseGenerator(200, 'Complete','{  "returnCode": "S",  "returnMessage": null,  "calculationTimeInMillisecondes": 2149,  "name": "myProject",  "elements":    [           {        "name": "actionRequest",        "content": "18"     },           {        "name": "ongoing",        "content": "19"     }  ]},            {         "name": "actionRequest",         "content": "3"      },            {         "name": "ongoing",         "content": "10"      }   ]}',
                                                    null));
        Test.startTest();
        PRMCockpitComponentConfiguration__c myCmp = new PRMCockpitComponentConfiguration__c(WSURL__c='http://www.google.com');
        PRM_BEMMiddlewareUtils.WidgetBean widgetBean = PRM_BEMMiddlewareUtils.getWidget(myCmp,new PRM_BEMMiddlewareUtils.ParameterBean('myProject','TOKEN','FEDID','test@accenture.com','IN'),null);
        String testName = widgetBean.name;
        Long testCalc = widgetBean.calculationTimeInMillisecondes;
        //System.assertEquals('myProject',widgetBean.name);
        
        PRM_BEMMiddlewareUtils bemmid = new PRM_BEMMiddlewareUtils();
        
        PRM_BEMMiddlewareUtils.ElementBean elementBean = new PRM_BEMMiddlewareUtils.ElementBean();      
        String testElementBean = elementBean.name;
        String testElementBeanContent = elementBean.content;
        
        Test.stopTest();
    }
  }
}