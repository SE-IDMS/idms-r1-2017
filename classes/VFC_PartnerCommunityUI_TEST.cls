/**
 * 17-Sep-2014
 * Swapnil Saurav
 * Description: Test class for VFC_PartnerCommunityUI 
*/
@isTest
public class VFC_PartnerCommunityUI_TEST {
    
    @testSetup static void PartnerCommunityUItestsetup(){
        List<BypassTriggersOnLeadConversion__c> BypassTriggersOnLeadConversion = new List<BypassTriggersOnLeadConversion__c>{
            new BypassTriggersOnLeadConversion__c(Name='AP44'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ACC_PartnerAccountUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_Contact_PartnerUserUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='FieloPRM_AP_AccountTriggers'),
            new BypassTriggersOnLeadConversion__c(Name='Fielo_ContactTriggers'),
            new BypassTriggersOnLeadConversion__c(Name='PartnerLocatorPushEventsService'),
            new BypassTriggersOnLeadConversion__c(Name='PRM_FieldChangeListener'),
            new BypassTriggersOnLeadConversion__c(Name='SyncObjects'),
            new BypassTriggersOnLeadConversion__c(Name='AP25'),
            new BypassTriggersOnLeadConversion__c(Name='AccountProgramBeforeInsert')
            };
        insert BypassTriggersOnLeadConversion;
        
        CS_PRM_ApexJobSettings__c ApexJob = new CS_PRM_ApexJobSettings__c(Name = 'ParseListViews', ObjectToParse__c = 'Account,Opportunity,OpportunityRegistrationForm__c', RelatedObjects__c = 'OPP_ProductLine__c,Event,Task,Contact,OpportunityRegistrationProducts__c,ActivityHistory,OpenActivity,Note,OPP_ValueChainPlayers__c,PartnerAssessment__c,OpportunityAssessmentResponse__c,Assessment__c,OpportunityAssessmentQuestion__c', InitialSync__c = true);
        insert ApexJob;
        
        Task opTsk1 = new Task(Status = 'In Progress', ActivityDate = System.today(), Subject = 'test1', Priority = 'Normal', CurrencyIsoCode='EUR');
        Task opTsk2 = new Task(Status = 'In Progress', ActivityDate = System.today(), Subject = 'test2', Priority = 'Normal', CurrencyIsoCode='EUR');
        List<Task> tsklist=new List<Task>();
        tsklist.add(opTsk1);
        tsklist.add(opTsk2);
        insert tsklist;
        
    }
    
    static testMethod void getTabsTest() {    
    Profile profile = [select id from Profile where name = 'System Administrator'];
        
        User usr = Utils_TestMethods.createStandardUser(profile.Id,'tPrtnr11');
        usr.BypassWF__c = true;
        usr.BypassVR__c = true;
        usr.ProgramApprover__c = true;
        usr.ProgramAdministrator__c = true;
        usr.ProgramOwner__c = true;
        usr.userRoleId = '00EA0000000MiDK';
        //usr.BypassTriggers__c = 'AccountProgramBeforeInsert';
        Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
        Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();      
        
        System.RunAs(usr) {
            Utils_SDF_Methodology.BypassOnLeadConvertion = true;
            
            //Id p = [select Id from profile where name = 'System Administrator'].id;
          Country__c country= Utils_TestMethods.createCountry();
            insert country; 

            // Create global program (PartnerProgram__c)
            
            // Create program level (ProgramLevel__c)
            // Create country program (PartnerProgram__c)
            // Create Counry Program Level (ProgramLevel__c)
            
            // Create Account      
            Account ac= Utils_TestMethods.createAccount();
            ac.Country__c=country.id;
            insert ac;
                    
            ac.isPartner = true;
            ac.PRMAccount__c = true;
            update ac;
            
            // Create Contact
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=ac.id, email = 'TestPartCrebfo@test.com', Country__c = country.id);
            insert con1;
            
            //con1.PRMUser__c = true;
            con1.PRMContact__c = true;
            con1.PRMPrimaryContact__c = true;
            update con1;
            
            con1.Country__c = country.id;
            con1.Email = 'test@org.com';
            con1.MobilePhone = '9733423423';
            update con1;

            // Create Account assigned program and assign Country program created above to the account
            // AccountAssigndProgram__c - Active = true
            // 
            Profile portalProfile = [select id from Profile where Name = 'SE - Channel Partner (Community)'];
           
            User usr2 = new User(alias = 'TestPart', email = 'TestPartCrebfo@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = portalProfile.id, BypassWF__c = True, ContactId = con1.id,
            timezonesidkey='Europe/London', username= 'TestPartCrebfo@test.com.bfo-portal.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='France',BypassVR__c = true,UserPermissionsSFContentUser=true);
            insert usr2;
            
            // Creating and assigning permission sets
            List<PermissionSet> pset1 = [select name,id from PermissionSet where name = 'PRMRegularORFCommunity' OR name = 'PRMProjectRegistrationCommunity' OR name = 'PRMPassedToPartnerOpportunitiesCommunity'];
            List<PermissionSetAssignment> psetAssignList = new List<PermissionSetAssignment>();
            for(PermissionSet pest:pset1){
                PermissionSetAssignment psetAssign = new PermissionSetAssignment();
                psetAssign.PermissionSetId = pest.id;
                psetAssign.AssigneeId = usr2.Id;
                psetAssignList.add(psetAssign);
            }
            insert psetAssignList;
            
            Account acc = Utils_TestMethods.createAccount(usr.id);
            insert acc;
            acc.isPartner = true;
            acc.PRMAccount__c = true;
            update acc;
            
            Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
            opp.OwnerId = usr2.id;
            insert opp;
            
            Note nte = new Note(ParentId = opp.Id, Title = 'Test', Body = 'This is a test to test task',OwnerId = usr2.Id);
            insert nte;
            
            //OPP_ProductLine__c oProd = Utils_TestMethods.createProductLine(opp.id);
            List<OPP_ProductLine__c> oProdList = new List<OPP_ProductLine__c>();
            List<OPP_ProductLine__c> oProdUpdate = new List<OPP_ProductLine__c>();
            List<OPP_ProductLine__c> oProdDelete = new List<OPP_ProductLine__c>();
            OPP_ProductLine__c oProd1 = new OPP_ProductLine__c(Opportunity__c = opp.Id, Quantity__c = 3, Amount__c = 100, LineStatus__c = 'Pending', PartnerProductLine__c = 'PTCTR');
            oProdList.add(oProd1);
            OPP_ProductLine__c oProd2 = new OPP_ProductLine__c(Opportunity__c = opp.Id, Quantity__c = 2, Amount__c = 10, LineStatus__c = 'Pending', PartnerProductLine__c = 'PTCTR');
            oProdList.add(oProd2);
            insert oProdList;
            
            oProdUpdate.add(oProdList[0]);
            oProdDelete.add(oProdList[1]);
            
            OPP_ValueChainPlayers__c oppValChnPlr = new OPP_ValueChainPlayers__c(Account__c = acc.Id, OpportunityName__c = opp.Id, AccountRole__c = 'Architect');
            Insert oppValChnPlr;

            //Create Lead
            Lead led = Utils_TestMethods.createLead(opp.Id, country.Id, 1);
            led.OwnerId = usr2.Id;
            insert led;
            
            //Create Lead history
            LeadHistory lhist = Utils_TestMethods.createLeadHistory(led.Id, 'Opportunity');
            
      //Creating open activity
            List<Task> taskList = [SELECT OwnerId, WhatId, Status, ActivityDate, Subject, Priority, TECH_OpportunityId__c FROM Task WHERE Subject = 'test1' OR Subject = 'test2'];
            List<Task> updateTask = new List<Task>(); 
            Task tsk123;
            for(Task tsk:taskList){
                tsk.OwnerId = usr2.Id;
                tsk.WhatId = opp.Id;
                if(tsk.Subject == 'test2')
                    tsk.Status = 'Completed';
                else
                    tsk123 = tsk;
                tsk.TECH_OpportunityId__c = opp.id;
                updateTask.add(tsk); 
            }
            update updateTask;
            /*Task opTsk1 = new Task(OwnerId = usr.Id, WhatId = opp.Id, Status = 'In Progress', ActivityDate = System.today(), Subject = 'test', Priority = 'Normal', TECH_OpportunityId__c = opp.id, CurrencyIsoCode='EUR');
            Task opTsk2 = new Task(OwnerId = usr.Id, WhatId = opp.Id, Status = 'Completed', ActivityDate = System.today(), Subject = 'test', Priority = 'Normal', TECH_OpportunityId__c = opp.id, CurrencyIsoCode='EUR');
            List<Task> tsklist=new List<Task>();
            tsklist.add(opTsk1);
            tsklist.add(opTsk2);
            insert tsklist;
            System.debug('whole object'+tsklist);
            Opportunity o = [SELECT
                            (SELECT Id, Name, PartnerProductLine__c, Amount__c, Quantity__c, LineAmount__c, LineStatus__c
                                FROM Product_Line_2__r),
                            (SELECT ActivityDate, ActivityType, Subject, IsTask, Status, Priority, Owner.Name FROM OpenActivities),
                            (SELECT ActivityDate, ActivityType, Subject, IsTask, Owner.Name, LastModifiedDate FROM ActivityHistories)
                        FROM Opportunity WHERE Id = :opp.Id];
            System.debug('whole object'+o.OpenActivities);
            System.debug('whole history'+o.ActivityHistories);
            System.debug('whole history'+o.Product_Line_2__r);
            System.Debug('whole Opportunity '+O); */
    
            List<String> Types = new List<String>();
            // Creating OpportunityRegistrationForm
            
            //Creating Global Partner Program 
              PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
              globalProgram.TECH_CountriesId__c = country.id;
              globalPRogram.recordtypeid = Label.CLMAY13PRM15;
              globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
              insert globalProgram;
            
              // Creating Global program level
              ProgramLevel__c pLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
                insert pLevel;
               
            // creating country partner program
              PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country.Id);
              insert countryProgram; 
            
              //Creating country program Level
              ProgramLevel__c countryLevel = Utils_TestMethods.createCountryProgramLevel(countryProgram.Id);
              insert countryLevel;
                
              list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
              countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
             
              if(!countryPartnerProgram.isEmpty())
              {
                countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
                update countryPartnerProgram;
              }
             OpportunityRegistrationForm__c ORF1 = new OpportunityRegistrationForm__c( Status__c='Pending', 
                                                AccountName__c= 'Test',
                                                AccountCountry__c = country.Id,
                                                Amount__c = 100000,
                                                ContactEmail__c='Test@test.com',
                                                OpportunityType__c='Standard',
                                                OpportunityScope__c='ENMVP - SECONDARY MV PRODUCTS',
                                                ExpectedOrderDate__c=System.Today(),
                                                ContactFirstName__c='Test',
                                                ContactLastName__c='User',
                                                ProjectName__c='Test Project',
                                                ProductType__c='BDIBS',
                                                PartnerProgram__c=countryPartnerProgram[0].id,
                                                ProgramLevel__c = countryLevel.Id,
                                                ownerid=usr2.id,
                                                Tech_CheckORFStatusOnSave__c='True');
            Insert ORF1;
            
            //ACC_PartnerProgram__c acc_Part = new ACC_PartnerProgram__c(Account__c = acc.Id, Active__c = true, PartnerProgram__c = countryPartnerProgram[0].id, Eligiblefor__c = 'Promotion', ProgramLevel__c = countryLevel.Id);
            //insert acc_Part;
            
      //Creating opportunity registration Products
      List<OpportunityRegistrationProducts__c> oppProdList = new List<OpportunityRegistrationProducts__c>();
      OpportunityRegistrationProducts__c oppProd1 = new OpportunityRegistrationProducts__c(ProductName__c = 'IDMOT', Quantity__c = 3, Price__c = 100, ORFId__c = ORF1.Id);
            oppProdList.add(oppProd1);
            OpportunityRegistrationProducts__c oppProd2 = new OpportunityRegistrationProducts__c(ProductName__c = 'IDMOT', Quantity__c = 3, Price__c = 100, ORFId__c = ORF1.Id);
             oppProdList.add(oppProd2);
            insert oppProdList;
            
            List<CustomRecentlyViewed__c> cusRecViewList = new List<CustomRecentlyViewed__c>();
            cusRecViewList.add(new CustomRecentlyViewed__c(LastViewedDate__c = System.now(), RecordId__c = ORF1.Id, RecordName__c = ORF1.Id, Type__c='OpportunityRegistrationForm__c', User__c = usr2.Id, OwnerID = usr2.Id));
            cusRecViewList.add(new CustomRecentlyViewed__c(LastViewedDate__c = System.now(), RecordId__c = opp.Id, RecordName__c = opp.Id, Type__c='Opportunity', User__c = usr2.Id, OwnerID = usr2.Id));
            cusRecViewList.add(new CustomRecentlyViewed__c(LastViewedDate__c = System.now(), RecordId__c = acc.Id, RecordName__c = acc.Id, Type__c='Account', User__c = usr2.Id, OwnerID = usr2.Id));
            Insert cusRecViewList;
            
            CS_PRM_ApexJobSettings__c ApexJob = new CS_PRM_ApexJobSettings__c(Name = 'ParseListViews', ObjectToParse__c = 'Account,Opportunity,OpportunityRegistrationForm__c', RelatedObjects__c = 'OPP_ProductLine__c,Event,Task,Contact,OpportunityRegistrationProducts__c,ActivityHistory,OpenActivity,Note,OPP_ValueChainPlayers__c,PartnerAssessment__c,OpportunityAssessmentResponse__c,Assessment__c,OpportunityAssessmentQuestion__c', InitialSync__c = true);
            insert ApexJob;
            
            System.runAs(usr2) {
            Test.startTest();
                //Commented method calls are the ones which don't work
              PageReference pageRef = Page.VFP_PartnerCommunityUI;
                Test.setCurrentPage(pageRef);
                ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(usr2);
                VFC_PartnerCommunityUI partner = new VFC_PartnerCommunityUI();
                ApexPages.currentPage().getParameters().put('id',usr2.id);
                //VFC_PartnerCommunityUI.getAppTabs();
              //VFC_PartnerCommunityUI.getAppConfiguration();
              
              VFC_PartnerCommunityUI.getListViewOptions('Account'); 
              VFC_PartnerCommunityUI.getDashboard();
                VFC_PartnerCommunityUI.getReferenceData();
                VFC_PartnerCommunityUI.getTemplateUri();
                VFC_PartnerCommunityUI.getUserPhotolink(usr.Id);
                List<VFC_PartnerCommunityUI.ListView> lview = VFC_PartnerCommunityUI.getListViewOptions('Account');
          
                String filterId = lview[0].id;
                VFC_PartnerCommunityUI.getOpportunityRelatedLists(opp.Id);   
                VFC_PartnerCommunityUI.getRecord('Opportunity', opp.id); 
                VFC_PartnerCommunityUI.getRecentlyViewed(types,true);
                VFC_PartnerCommunityUI.getLeadRelatedList(led.id);
                VFC_PartnerCommunityUI.getORFRelatedLists(ORF1.Id,countryPartnerProgram[0].Id,countryLevel.Id);
                VFC_PartnerCommunityUI.doSaveORF(ORF1, oppProdList,null); // doesn't go in the try block of the method.
                VFC_PartnerCommunityUI.getExtensionQuestionnaire(ORF1.Id);
                VFC_PartnerCommunityUI.getRelatedActivity(null, 'note', 'Opportunity', opp.Id);
                VFC_PartnerCommunityUI.doSaveRelatednote(nte);
                VFC_PartnerCommunityUI.doSaveRelatedtask(tsk123);
                VFC_PartnerCommunityUI.SaveResult svRes = VFC_PartnerCommunityUI.acceptRejectOpportunity('reject',tsk123);
                System.debug('The svRes '+svRes);
                VFC_PartnerCommunityUI.getProgramProducts('8987865645ty5');
                Event evnt = new Event();
                VFC_PartnerCommunityUI.doSaveRelatedevent(evnt);
                VFC_PartnerCommunityUI.doGetAllRecords(false, new List<String>());
                VFC_PartnerCommunityUI.doGetProgramLevelFeatures('yur745yhey43');
                VFC_PartnerCommunityUI.doGetSelectedTaskView('test');
                VFC_PartnerCommunityUI.getAppTabs();
                VFC_PartnerCommunityUI.getReferenceData();
                VFC_PartnerCommunityUI.getUserRegAvailablePicklistValues('en_US');
                //VFC_PartnerCommunityUI.getListData('78yuht67576', 'Account', 5);
                VFC_PartnerCommunityUI.getStaticLabelTranslations();
                VFC_PartnerCommunityUI.doSaveOpp(opp, oProdUpdate, oProdDelete);
                VFC_PartnerCommunityUI.doDeleteActivity(tsk123);
                VFC_PartnerCommunityUI Vfc_Part = new VFC_PartnerCommunityUI();
                Test.stopTest();
            }
        }  
    }
    
    static testMethod void PartnerCommunityUI_Test(){
        Profile profile = [select id from Profile where name = 'System Administrator'];
        User usr = Utils_TestMethods.createStandardUser(profile.Id,'tPrtnr11');
        usr.BypassWF__c = true;
        usr.BypassVR__c = true;
        usr.ProgramApprover__c = true;
        usr.ProgramAdministrator__c = true;
        usr.ProgramOwner__c = true;
        usr.userRoleId = '00EA0000000MiDK';      
        
        System.RunAs(usr) {
            Utils_SDF_Methodology.BypassOnLeadConvertion = true;
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc= Utils_TestMethods.createAccount();
            Acc.Country__c=country.id;
            insert Acc;
                    
            Acc.isPartner = true;
            Acc.PRMAccount__c = true;
            update Acc;
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, 
                                       PRMContact__c = true, PRMPrimaryContact__c = true, Email = 'test@org.com', MobilePhone = '9733423423', Country__c = country.id );
            insert con1;
            
            Profile portalProfile = [select id from Profile where Name = 'SE - Channel Partner (Community)'];
            
            User usr2 = new User(alias = 'TestPart', email = 'TestPartCrebfo@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = portalProfile.id, BypassWF__c = True, ContactId = con1.id,
            timezonesidkey='Europe/London', username= 'TestPartCrebfo@test.com.bfo-portal.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='France',BypassVR__c = true,UserPermissionsSFContentUser=true);
            insert usr2;
            
            // Creating and assigning permission sets
            List<PermissionSet> pset1 = [select name,id from PermissionSet where name = 'PRMRegularORFCommunity' OR name = 'PRMProjectRegistrationCommunity' OR name = 'PRMPassedToPartnerOpportunitiesCommunity'];
            List<PermissionSetAssignment> psetAssignList = new List<PermissionSetAssignment>();
            for(PermissionSet pest:pset1){
                PermissionSetAssignment psetAssign = new PermissionSetAssignment();
                psetAssign.PermissionSetId = pest.id;
                psetAssign.AssigneeId = usr2.Id;
                psetAssignList.add(psetAssign);
            }
            insert psetAssignList;
            
            Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
            opp.OwnerId = usr2.Id;
            insert opp;
            
            Note nte = new Note(ParentId = opp.Id, Title = 'Test', Body = 'This is a test to test task',OwnerId = usr2.Id);
            insert nte;
            
            //OPP_ProductLine__c oProd = Utils_TestMethods.createProductLine(opp.id);
            List<OPP_ProductLine__c> oProdList = new List<OPP_ProductLine__c>();
            List<OPP_ProductLine__c> oProdUpdate = new List<OPP_ProductLine__c>();
            List<OPP_ProductLine__c> oProdDelete = new List<OPP_ProductLine__c>();
            OPP_ProductLine__c oProd1 = new OPP_ProductLine__c(Opportunity__c = opp.Id, Quantity__c = 3, Amount__c = 100, LineStatus__c = 'Pending', PartnerProductLine__c = 'PTCTR');
            oProdList.add(oProd1);
            OPP_ProductLine__c oProd2 = new OPP_ProductLine__c(Opportunity__c = opp.Id, Quantity__c = 2, Amount__c = 10, LineStatus__c = 'Pending', PartnerProductLine__c = 'PTCTR');
            oProdList.add(oProd2);
            insert oProdList;
            
            oProdUpdate.add(oProdList[0]);
            oProdDelete.add(oProdList[1]);
            
            OPP_ValueChainPlayers__c oppValChnPlr = new OPP_ValueChainPlayers__c(Account__c = acc.Id, OpportunityName__c = opp.Id, AccountRole__c = 'Architect');
            Insert oppValChnPlr;

            //Create Lead
            /*Lead led = Utils_TestMethods.createLead(opp.Id, country.Id, 1);
            led.OwnerId = usr2.Id;
            insert led;
            
            List<Task> taskList = [SELECT OwnerId, WhatId, Status, ActivityDate, Subject, Priority, TECH_OpportunityId__c FROM Task WHERE Subject = 'test1' OR Subject = 'test2'];
            List<Task> updateTask = new List<Task>(); 
            Task tsk123;
            for(Task tsk:taskList){
                tsk.OwnerId = usr2.Id;
                tsk.WhatId = opp.Id;
                if(tsk.Subject == 'test2')
                    tsk.Status = 'Completed';
                else
                    tsk123 = tsk;
                tsk.TECH_OpportunityId__c = opp.id;
                updateTask.add(tsk); 
            }
            update updateTask; */
            
            // Creating OpportunityRegistrationForm
                
            //Creating Global Partner Program 
              PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
              globalProgram.TECH_CountriesId__c = country.id;
              globalPRogram.recordtypeid = Label.CLMAY13PRM15;
              globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
              insert globalProgram;
            
              // Creating Global program level
              ProgramLevel__c pLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
                insert pLevel;
               
            // creating country partner program
              PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country.Id);
              insert countryProgram; 
            
              //Creating country program Level
              ProgramLevel__c countryLevel = Utils_TestMethods.createCountryProgramLevel(countryProgram.Id);
              countryLevel.LevelStatus__c = 'Active';
              insert countryLevel;
                
              list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
              countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
             
              if(!countryPartnerProgram.isEmpty())
              {
                countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
                update countryPartnerProgram;
              }
             OpportunityRegistrationForm__c ORF1 = new OpportunityRegistrationForm__c( Status__c='Pending', 
                                                AccountName__c= 'Test',
                                                AccountCountry__c = country.Id,
                                                Amount__c = 100000,
                                                ContactEmail__c='Test@test.com',
                                                OpportunityType__c='Standard',
                                                OpportunityScope__c='ENMVP - SECONDARY MV PRODUCTS',
                                                ExpectedOrderDate__c=System.Today(),
                                                ContactFirstName__c='Test',
                                                ContactLastName__c='User',
                                                ProjectName__c='Test Project',
                                                ProductType__c='BDIBS',
                                                PartnerProgram__c=countryPartnerProgram[0].id,
                                                ProgramLevel__c = countryLevel.Id,
                                                ownerid=usr2.Id,
                                                Tech_CheckORFStatusOnSave__c='True');
            Insert ORF1;
            OpportunityRegistrationForm__c ORF2 = new OpportunityRegistrationForm__c( Status__c='Pending', 
                                                AccountName__c= 'Test',
                                                AccountCountry__c = country.Id,
                                                Amount__c = 100000,
                                                ContactEmail__c='Test123@test.com',
                                                OpportunityType__c='Standard',
                                                OpportunityScope__c='ENMVP - SECONDARY MV PRODUCTS',
                                                ExpectedOrderDate__c=System.Today(),
                                                ContactFirstName__c='Test',
                                                ContactLastName__c='User',
                                                ProjectName__c='Test Project',
                                                ProductType__c='BDIBS',
                                                PartnerProgram__c=countryPartnerProgram[0].id,
                                                ProgramLevel__c = countryLevel.Id,
                                                ownerid=usr2.Id,
                                                Tech_CheckORFStatusOnSave__c='True');
            Insert ORF2;
            List<OpportunityRegistrationProducts__c> oppProdList = new List<OpportunityRegistrationProducts__c>();
            OpportunityRegistrationProducts__c oppProd1 = new OpportunityRegistrationProducts__c(ProductName__c = 'IDMOT', Quantity__c = 3, Price__c = 100, ORFId__c = ORF1.Id);
            oppProdList.add(oppProd1);
            OpportunityRegistrationProducts__c oppProd2 = new OpportunityRegistrationProducts__c(ProductName__c = 'IDMOT', Quantity__c = 3, Price__c = 100, ORFId__c = ORF1.Id);
             oppProdList.add(oppProd2);
            insert oppProdList;
            
             Assessment__c ass = new Assessment__c(
                            Active__c = true, AutomaticAssignment__c = false, 
                            Description__c = 'This is Test', LeadingBusiness__c = 'ID', MaximumPossibleQualification__c = 4, 
                            MaximumScore__c = 100, RecordTypeId = '01212000000oF4Z', ProgramLevel__c = countryLevel.Id, PartnerProgram__c = countryPartnerProgram[0].id);//, PartnerProgram__c = , ProgramLevel__c = , );
            insert ass;
            PartnerAssessment__c partnerassessment = new PartnerAssessment__c();
               partnerassessment.Account__c = acc.Id;
               //partnerassessment.AccountAssignedProgram__c = accprgm.Id;
               partnerassessment.Assessment__c = ass.Id;
               partnerassessment.OpportunityRegistrationForm__c =  ORF1.Id;
               partnerassessment.Tech_Status__c = 'Draft';
               Insert partnerassessment;
            
            ACC_PartnerProgram__c acc_Part = new ACC_PartnerProgram__c(Account__c = acc.Id, Active__c = true, PartnerProgram__c = countryPartnerProgram[0].id, Eligiblefor__c = 'Promotion', ProgramLevel__c = countryLevel.Id);
            insert acc_Part;
            
            System.runAs(usr2) {
                Test.startTest();
                UserAccessibleListViews__c UsrAccListView = new UserAccessibleListViews__c(Columns__c = '   Name,CountryOfDestination__c,PartnerInvolvement__c,Amount,SEReference__c,CloseDate,ChannelPartnerStage__c,Status__c', UniqueName__c = 'Opportunity.All_Opportunities_Partner_View1', ViewId__c = '00BA0000007qarpMAA');
                insert UsrAccListView;
                Opportunity oppTest = new Opportunity();
                ApexPages.StandardController sc = new ApexPages.StandardController(oppTest);
                PageReference pageRef = Page.VFP_PartnerCommunityUI;
                Test.setCurrentPage(pageRef);
                //ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(usr2);
                VFC_PartnerCommunityUI partner = new VFC_PartnerCommunityUI();
                ApexPages.currentPage().getParameters().put('id',usr2.Id);
                List<VFC_PartnerCommunityUI.Question> quesList = new List<VFC_PartnerCommunityUI.Question>();
                VFC_PartnerCommunityUI.Question ques1 = new VFC_PartnerCommunityUI.Question(null, 'This is test description', 1, false, 'Text', 'This is answertext');
                quesList.add(ques1);
                VFC_PartnerCommunityUI.ORFExtension ORFExt = new VFC_PartnerCommunityUI.ORFExtension(ass.id, 'Test', 'This is test Desc', acc_Part.Id, countryLevel.Id, ORF1.Id);
                ORFExt.Questions = quesList;
                VFC_PartnerCommunityUI.SaveResult svRes= VFC_PartnerCommunityUI.doSaveORFExtension(ORFExt, 'save');
                    System.debug('The svRes '+svRes);
                VFC_PartnerCommunityUI.getAppConfiguration();
                VFC_PartnerCommunityUI.getListData(UsrAccListView.UniqueName__c,'Opportunity',1);
                String imageIdTest = partner.imageId;
                String ORFStatusOptionsTest = partner.ORFStatusOptions;
                VFC_PartnerCommunityUI.getRecord('Opportunity', null);
                VFC_PartnerCommunityUI.getORFRelatedLists(null,countryPartnerProgram[0].Id,countryLevel.Id);
                VFC_PartnerCommunityUI.getExtensionQuestionnaire(ORF1.Id);
                VFC_PartnerCommunityUI.getRelatedActivity(nte.Id, 'Note', 'Opportunity', opp.Id);
                VFC_PartnerCommunityUI.doSaveRelatednote(new Note(ParentId = opp.Id, Title = null, Body = null,OwnerId = usr2.Id));
                VFC_PartnerCommunityUI.doSaveRelatedtask(new Task(Status = null, ActivityDate = null, Subject = 'test1', Priority = 'Normal'));
                VFC_PartnerCommunityUI.deleteORFProductLines(oppProd2.Id, ORF1.Id);
                VFC_PartnerCommunityUI.deleteORF(ORF2.Id);
                VFC_PartnerCommunityUI.deleteOppLines(oProd1.Id, opp.Id);
                VFC_PartnerCommunityUI.recentViewedItemsList ViewTest = new VFC_PartnerCommunityUI.recentViewedItemsList();
                VFC_PartnerCommunityUI.ORFExtension ORFExtensionTest = new VFC_PartnerCommunityUI.ORFExtension();
                VFC_PartnerCommunityUI.Question QuestionTest = new VFC_PartnerCommunityUI.Question();
                VFC_PartnerCommunityUI.Answer AnswerTest = new VFC_PartnerCommunityUI.Answer();
                VFC_PartnerCommunityUI.Answer AnswerTest1 = new VFC_PartnerCommunityUI.Answer('89iu786', '89iu786y', 3, 'Text');
                VFC_PartnerCommunityUI.SaveResult SaveResult = new VFC_PartnerCommunityUI.SaveResult();
                VFC_PartnerCommunityUI.ResultMessage ResultMessageTest = new VFC_PartnerCommunityUI.ResultMessage();
                VFC_PartnerCommunityUI.RefData RefDataTest = new VFC_PartnerCommunityUI.RefData();
                
            }
        }
    }
}