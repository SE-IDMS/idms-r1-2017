/*    
      Author          : Srikant (Schneider Electric)    
      Date Created    : 04/21/2013   
      Description     : Controller UTIL class for the Change request.
*/

public with sharing class CHR_ProjectUtils {
        
        public static void verifyHFMCodeEnteredandSelectedInEnvelop(List<Change_Request__c> projList)
        {
            Map<Id,List<ChangeRequestFundingEntity__c>> fundentMap = new Map<Id,List<ChangeRequestFundingEntity__c>>();
            
            //loop through the active funding entity records associated with every project request and ensure that the HFM Code has been keyed in.
            
            for(ChangeRequestFundingEntity__c oFundEnt:[select Id,ActiveForecast__c,ChangeRequest__c,HFMCode__c,SelectedinEnvelop__c from ChangeRequestFundingEntity__c where ChangeRequest__c in :projList and ActiveForecast__c = true])
            {           
                List<ChangeRequestFundingEntity__c> lstFundEnt = new List<ChangeRequestFundingEntity__c>();         
                if (fundentMap.containsKey(oFundEnt.ChangeRequest__c)) 
                {
                    lstFundEnt = fundentMap.get(oFundEnt.ChangeRequest__c);
                }           
                lstFundEnt.add(oFundEnt);
                fundentMap.put(oFundEnt.ChangeRequest__c,lstFundEnt);
            }
            
            
            for(Change_Request__c prj:projList)
            {
                boolean isEmpty = false;            
                if (fundentMap.containsKey(prj.Id))
                {
                    for(ChangeRequestFundingEntity__c oFundEnt:fundentMap.get(prj.Id))
                    {   
                        if(oFundEnt.HFMCode__c == null || oFundEnt.HFMCode__c == '')
                        {
                            isEmpty = true;
                            break;
                        }
                    }
                }
                if(isEmpty)
                {
                    prj.addError(System.Label.DMT_MessageEnterHFMCode);
                }
            }
            
            for(Change_Request__c prj:projList)
            {
                boolean isNotSelected = false;            
                if (fundentMap.containsKey(prj.Id))
                {
                    for(ChangeRequestFundingEntity__c oFundEnt:fundentMap.get(prj.Id))
                    {   
                        if(!oFundEnt.SelectedinEnvelop__c)
                        {
                            isNotSelected = true;
                            break;
                        }
                    }
                }
                if(isNotSelected)
                {
                    prj.addError(System.Label.DMT_MessageProjectRequestNotSelectedinEnvelop);
                }
            }
            
        
        
        }
        
        /* public static void sendCreatedNotification(List<Change_Request__c> lstProj, string strCause)
        {
            List<DMTDomainOwnerEmail__c> lstDOwners = new List<DMTDomainOwnerEmail__c>();
            Map<id,string> prjDomainMap = new Map<id,string>(); 
            for(Change_Request__c pr: lstProj)
            {
                if(!prjDomainMap.containsKey(pr.Id))
                {
                    prjDomainMap.put(pr.Id,pr.BusinessTechnicalDomain__c);
                }
            }
            System.Debug('prjDomainMap Size: ' + prjDomainMap.values().size());
                   
            List<DMTAuthorizationMasterData__c> lstAuth = [Select BusinessTechnicalDomain__c, Parent__c, ParentFamily__c, BusGeographicZoneIPO__c, NextStep__c, AuthorizedUser1__c, AuthorizedUser2__c, AuthorizedUser3__c, AuthorizedUser4__c, AuthorizedUser5__c, AuthorizedUser6__c, AuthorizedUser7__c, AuthorizedUser8__c, AuthorizedUser9__c, AuthorizedUser10__c From DMTAuthorizationMasterData__c d where NextStep__c =:System.Label.DMT_StatusValid AND BusinessTechnicalDomain__c in :prjDomainMap.values()];
            System.Debug('lstAuth size: ' + lstAuth.size());        
            //for(DMTAuthorizationMasterData__c dmt:[Select BusinessTechnicalDomain__c, Parent__c, ParentFamily__c, BusGeographicZoneIPO__c, NextStep__c, AuthorizedUser1__c, AuthorizedUser2__c, AuthorizedUser3__c, AuthorizedUser4__c, AuthorizedUser5__c, AuthorizedUser6__c, AuthorizedUser7__c, AuthorizedUser8__c, AuthorizedUser9__c, AuthorizedUser10__c From DMTAuthorizationMasterData__c d where NextStep__c =:System.Label.DMT_StatusValid AND BusinessTechnicalDomain__c in :prjDomainMap.values()])
            for(DMTAuthorizationMasterData__c dmt:lstAuth)        
            {
                System.Debug('Outer Loop');
                for(Change_Request__c pr:lstProj)
                {
                    System.Debug('Inner Loop');
                    if(dmt.BusinessTechnicalDomain__c == pr.BusinessTechnicalDomain__c)
                    {
                        SObject sobjAuth =(SObject)dmt;
                        DMTDomainOwnerEmail__c downer = new DMTDomainOwnerEmail__c();
                        SObject sobjDOwner = (SObject)downer;
                        for(Integer i=1;i<11;i++)
                        {
                            if(sobjAuth.get('AuthorizedUser'+i+'__c') !=null)
                                sobjDOwner.put('AuthorizedUser'+i+'__c',sobjAuth.get('AuthorizedUser'+i+'__c')+'');
                        }
                        downer.ProjectRequest__c = pr.Id;
                        downer.Cause__c = strCause;
                        lstDOwners.add(downer);
                    }
                }
                
                                    
            }        
            
            
            
            System.Debug('lstDOwners size: ' + lstDOwners.size());
            if(lstDOwners.size()>0)
            {
                try
                {
                    Database.SaveResult[] results = Database.insert(lstDOwners,false);
                    if (results != null)
                    {
                        for (Database.SaveResult result : results) 
                        {
                            if (!result.isSuccess()) 
                            {
                                Database.Error[] errs = result.getErrors();
                                for(Database.Error err : errs)
                                System.debug(err.getStatusCode() + ' - ' + err.getMessage());
     
                            }
                        }
                    }
                }
                catch(Exception exp)
                {
                    System.debug(exp.getTypeName() + ' - ' + exp.getCause() + ': ' + exp.getMessage());
                }
            }
                
                
            
        }
        *///end   
        
        public static void updateProjectRequestFields(List<Change_Request__c> projList)
        {
          Try{
           /*Added as a part of September Release - Srikant Joshi
           To reflect changes in STATUS(diagram) on Project Request
           STATUS(diagram) in Project Request will show SELECT=Yes, if all the funding Entity records are selected and active.
           STATUS(diagram) in Project Request will show Budget Year = 2012 if funding Entity records selected and active are with Budget Year = '2012' and 
                                                        Budget Year = 2013 if funding Entity records selected and active are with Budget Year = '2013'.
           STATUS(diagram) in Project Request will show SELECT=NO, if any Funding Entity is not Selected or not Active or Both. 
           */
           System.debug('---------'+ 'Code here..');
            map<String,integer> mapCountFE= new map<String,integer>{};
            map<String,String> mapBudgetYear= new map<String,String>{};
            map<String,integer> mapCountFEactiveSelected= new map<String,integer>{};
            
            For(AggregateResult groupedResults : [SELECT COUNT(id)aver,max(BudgetYear__c) BudgetYear,ChangeRequest__c FROM ChangeRequestFundingEntity__c where ChangeRequest__c in :projList and ActiveForecast__c = True GROUP BY ChangeRequest__c]){
                mapCountFE.put(String.Valueof(groupedResults.get('ChangeRequest__c')),Integer.valueof(groupedResults.get('aver')));
                mapBudgetYear.put(String.Valueof(groupedResults.get('ChangeRequest__c')),String.valueof(groupedResults.get('BudgetYear')));
            }

            For(AggregateResult groupedResults : [SELECT COUNT(id)aver,ChangeRequest__c FROM ChangeRequestFundingEntity__c where ChangeRequest__c in :projList and ActiveForecast__c = True and SelectedinEnvelop__c = True GROUP BY ChangeRequest__c]){
                mapCountFEactiveSelected.put(String.Valueof(groupedResults.get('ChangeRequest__c')),Integer.valueof(groupedResults.get('aver')));
            }
            
            For(Change_Request__c projReq :projList ){
                System.debug('---------'+ 'Code here..');
                if(mapBudgetYear.get(projReq.id)!=null)
                    projReq.TECHBudgetYear__c = Integer.Valueof(mapBudgetYear.get(projReq.id));
            }
        }
      catch(Exception e){ 
      }  
    }

      
      //START Srikant Joshi Added Code for Febraury Release 2013
 
      public static void projectRequestDmtFEAutoCreation(List<Change_Request__c> PrjReqListToProcessCONDITION1){
        list<ChangeRequestFundingEntity__c> lstDMTFundingEntity = new list<ChangeRequestFundingEntity__c>();
        for(Change_Request__c prjReq : PrjReqListToProcessCONDITION1){
            ChangeRequestFundingEntity__c dmtFE = new ChangeRequestFundingEntity__c(ChangeRequest__c = prjReq.id);
            dmtFE.BudgetYear__c = String.Valueof(System.today().Year());
            lstDMTFundingEntity.add(dmtFE);
        }
        try{
            insert lstDMTFundingEntity;
        }
        catch(Exception e){
        }
      }
      // End

 }