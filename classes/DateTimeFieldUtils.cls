public with sharing class DateTimeFieldUtils {
    public string ObjId= System.currentPageReference().getParameters().get('ObjId');
    public string ObjAPIName= System.currentPageReference().getParameters().get('ObjAPIName');
    public string FieldAPIName= System.currentPageReference().getParameters().get('FieldAPIName');
    public string returnUrl='';
    public List<Sobject> sobjLst;
    
    public DateTimeFieldUtils (){
        displayImage = true;
    }
    public void updateObject()
    {
        try{
            if(FieldAPIName != null && FieldAPIName != '' && ObjAPIName != null && ObjAPIName != '' && ObjId != null && ObjId != '')
            {
                String SOQL  = 'SELECT Id , '+FieldAPIName+' FROM '+ObjAPIName +' where id  =:ObjId';
                sobjLst=Database.query(SOQL);
            }
        }
        catch(Exception e){}
        if( sobjLst!=null && sobjLst.size() > 0 )
        {
            datetime dt = datetime.now();
            sobjLst[0].put(FieldAPIName,datetime.valueOf(dt));
            update sobjLst;
        }
    }
    
    public String getRecid()
    {
      return ObjId;
    }
    public Boolean displayImage { get; set; }
}