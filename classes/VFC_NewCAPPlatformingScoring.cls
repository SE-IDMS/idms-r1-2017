public class VFC_NewCAPPlatformingScoring{
    public SFE_IndivCAP__c cap{get;set;}{cap=new SFE_IndivCAP__c();}
    public SFE_PlatformingScoring__c ps{get;set;}{ps=new SFE_PlatformingScoring__c();}
    public Boolean flag{get;set;}
    public List<AccountMasterProfile__c> ampList = new List<AccountMasterProfile__c>();
    public List<SFE_PlatformingScoring__c> lastYearPS = new List<SFE_PlatformingScoring__c>();
    public List<SFE_PlatformingScoring__c> lastYearBUPS = new List<SFE_PlatformingScoring__c>();
    public List<wrapscoring> wrapscoringList{get;set;} {wrapscoringList=new List<wrapscoring>();}
    public Account acc=new Account();
   

    public VFC_NewCAPPlatformingScoring(ApexPages.StandardController controller) {
        if(System.currentPageReference().getParameters().get( 'Capid' )!=null)
            cap=[select Id,Name,AssignedTo__c,CriterWeightQu1__c,CriterWeightQu2__c,CriterWeightQu3__c,CriterWeightQu4__c,CriterWeightQu5__c,CriterWeightQu6__c,CriterWeightQu7__c,CriterWeightQu8__c from SFE_IndivCAP__c where id=:(ID)System.currentPageReference().getParameters().get( 'Capid' )];
        else
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CAP details not found'));
    }
    
    public void continuePS(){
        Map<String,Decimal> currencyMap = new Map<String,Decimal>();
        //Fetch currecy conversion rate
        for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
            currencyMap.put(ct.IsoCode,ct.ConversionRate);  
        User curuser=[select CurrencyIsoCode,Name,DefaultCurrencyIsoCode from user where Id=:userinfo.getUserId()]; //fetch current user currency
        User u=[select UserBusinessUnit__c,CurrencyIsoCode from user where Id=:cap.AssignedTo__c]; //fetch Assigned To user BU
        
        flag=true;
        
        if(ps.PlatformedAccount__c==null){
            flag=false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select Account'));
        }
        else if(ps.PlatformedAccount__c!=null){
            acc=[select Id,Name,Inactive__c,ToBeDeleted__c,MarketSegment__c,ClassLevel1__c,OwnerID,ExternalSalesResponsible__c,country__c from Account where ID=:ps.PlatformedAccount__c];
            
            System.debug('>>>>>>>acc>>>'+acc.id);
            if(acc.Inactive__c){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Account selected is marked inactive. Please choose another account'));
                 flag=false;
            }if(acc.ToBeDeleted__c ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Account selected is marked TobeDeleted. Please choose another account'));
                 flag=false;
           }
           Integer cnt=[select count() from SFE_PlatformingScoring__c where PlatformedAccount__c=:ps.PlatformedAccount__c and IndivCAP__c=:cap.Id];
           if(cnt!=0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This Account already exists in the CAP'));
                 flag=false;
           }
           
           if(flag){
              ps.IndivCAP__c=cap.Id;
              List<CAPGlobalMasterData__c> capQuesAnsList=new List<CAPGlobalMasterData__c>();
              List<CAPGlobalMasterData__c> capQuesAnsGlobalList=new List<CAPGlobalMasterData__c>();
              List<CAPCountryWeight__c> capCountryWeightList=new List<CAPCountryWeight__c>();
              List<CAPCountryWeight__c> capCountryWeightListCL=new List<CAPCountryWeight__c>();
              List<CAPCountryWeight__c> capCountryWeightListCLMKT=new List<CAPCountryWeight__c>();

              for(Integer i=0;i<8;i++){
                wrapscoring ws=new wrapscoring();
                ws.qseq='Q'+(i+1);
                wrapscoringList.add(ws);
                }

                //Weight  
                if(cap.CriterWeightQu1__c!=null && cap.CriterWeightQu1__c !=0)        
                    wrapscoringList[0].wgt=cap.CriterWeightQu1__c ;
                if(cap.CriterWeightQu2__c!=null && cap.CriterWeightQu2__c !=0)    
                    wrapscoringList[1].wgt=cap.CriterWeightQu2__c ;
                if(cap.CriterWeightQu3__c!=null && cap.CriterWeightQu3__c !=0)    
                    wrapscoringList[2].wgt=cap.CriterWeightQu3__c ;
                if(cap.CriterWeightQu4__c!=null && cap.CriterWeightQu4__c !=0)    
                    wrapscoringList[3].wgt=cap.CriterWeightQu4__c ;
                if(cap.CriterWeightQu5__c!=null && cap.CriterWeightQu5__c !=0)    
                    wrapscoringList[4].wgt=cap.CriterWeightQu5__c ;
                if(cap.CriterWeightQu6__c!=null && cap.CriterWeightQu6__c !=0)    
                    wrapscoringList[5].wgt=cap.CriterWeightQu6__c ;
                if(cap.CriterWeightQu7__c!=null && cap.CriterWeightQu7__c !=0)    
                    wrapscoringList[6].wgt=cap.CriterWeightQu7__c ;
                if(cap.CriterWeightQu8__c!=null && cap.CriterWeightQu8__c !=0)    
                    wrapscoringList[7].wgt=cap.CriterWeightQu8__c ; 
                
                List<CAPGlobalMasterData__c> cwgList=new List<CAPGlobalMasterData__c>();
                cwgList=[SELECT QuestionInterpretation__c,AnswerInterpretation1__c,AnswerInterpretation2__c,AnswerInterpretation3__c,AnswerInterpretation4__c,ClassificationLevel1__c,Weight__c,QuestionSequence__c from CAPGlobalMasterData__c where ClassificationLevel1__c=:acc.ClassLevel1__c OR ClassificationLevel1__c='Default' ORDER BY QuestionSequence__c ASC];
                for(CAPGlobalMasterData__c cwg:cwgList){
                    if(cwg.ClassificationLevel1__c=='Default')
                        capQuesAnsGlobalList.add(cwg);
                     else
                        capQuesAnsList.add(cwg);
                 }
                
              //Question Interpretation and Answer Interpretation as per custom classification L1 from CAPGlobalMasterData
              if(capQuesAnsList.size()==8){
                for(Integer i=0;i<8;i++)
                  wrapscoringList[i].ques=capQuesAnsList[i].QuestionInterpretation__c;
               for(Integer i=0;i<8;i++){
                  wrapscoringList[i].ans1=capQuesAnsList[i].AnswerInterpretation1__c;
                  wrapscoringList[i].ans2=capQuesAnsList[i].AnswerInterpretation2__c;
                  wrapscoringList[i].ans3=capQuesAnsList[i].AnswerInterpretation3__c;
                  wrapscoringList[i].ans4=capQuesAnsList[i].AnswerInterpretation4__c;
               }
           }
      
          //Fetch the Default Question and Answer Interpretation from CAPGlobalMasterData if its not available for the respective classification
          else if(capQuesAnsGlobalList.size()==8){
           for(Integer i=0;i<8;i++)
              wrapscoringList[i].ques=capQuesAnsGlobalList[i].QuestionInterpretation__c;

            for(Integer i=0;i<8;i++){
              wrapscoringList[i].ans1=capQuesAnsGlobalList[i].AnswerInterpretation1__c;
              wrapscoringList[i].ans2=capQuesAnsGlobalList[i].AnswerInterpretation2__c;
              wrapscoringList[i].ans3=capQuesAnsGlobalList[i].AnswerInterpretation3__c;
              wrapscoringList[i].ans4=capQuesAnsGlobalList[i].AnswerInterpretation4__c;
               }
           }
           System.debug('>>>'+cap);
           if(cap.CriterWeightQu1__c==null || cap.CriterWeightQu1__c==0 || cap.CriterWeightQu2__c==null || cap.CriterWeightQu2__c==0 || cap.CriterWeightQu3__c==null || cap.CriterWeightQu3__c==0 || cap.CriterWeightQu4__c==null || cap.CriterWeightQu4__c==0 || cap.CriterWeightQu5__c==null || cap.CriterWeightQu5__c==0 || cap.CriterWeightQu6__c==null || cap.CriterWeightQu6__c==0 || cap.CriterWeightQu7__c==null || cap.CriterWeightQu7__c==0 || cap.CriterWeightQu8__c==null || cap.CriterWeightQu8__c==0){
                 capCountryWeightList=[Select ClassificationLevel1__c,Country__c,CountryWeight__c,MarketSegment__c,QuestionSequence__c from CAPCountryWeight__c where ClassificationLevel1__c=:acc.ClassLevel1__c ORDER BY QuestionSequence__c ASC];
                 for(CAPCountryWeight__c cw:capCountryWeightList){
                     if(cw.MarketSegment__c!=null)
                         capCountryWeightListCLMKT.add(cw);
                      else
                         capCountryWeightListCL.add(cw);
                 }
             }
             
             if(capCountryWeightListCLMKT.size()==8){
                 for(CAPCountryWeight__c cw:capCountryWeightListCLMKT){
                     if(wrapscoringList[0].wgt==null && cw.QuestionSequence__c=='Q1')
                         wrapscoringList[0].wgt=cw.CountryWeight__c;
                     else if(wrapscoringList[1].wgt==null && cw.QuestionSequence__c=='Q2')
                         wrapscoringList[1].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[2].wgt==null && cw.QuestionSequence__c=='Q3')
                         wrapscoringList[2].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[3].wgt==null && cw.QuestionSequence__c=='Q4')
                         wrapscoringList[3].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[4].wgt==null && cw.QuestionSequence__c=='Q5')
                         wrapscoringList[4].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[5].wgt==null && cw.QuestionSequence__c=='Q6')
                         wrapscoringList[5].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[6].wgt==null && cw.QuestionSequence__c=='Q7')
                         wrapscoringList[6].wgt=cw.CountryWeight__c;
                     else if(wrapscoringList[7].wgt==null && cw.QuestionSequence__c=='Q8')
                         wrapscoringList[7].wgt=cw.CountryWeight__c;
                 }
             }
             else if(capCountryWeightListCL.size()==8){
                for(CAPCountryWeight__c cw:capCountryWeightListCL){
                     if(wrapscoringList[0].wgt==null && cw.QuestionSequence__c=='Q1')
                         wrapscoringList[0].wgt=cw.CountryWeight__c;
                     else if(wrapscoringList[1].wgt==null && cw.QuestionSequence__c=='Q2')
                         wrapscoringList[1].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[2].wgt==null && cw.QuestionSequence__c=='Q3')
                         wrapscoringList[2].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[3].wgt==null && cw.QuestionSequence__c=='Q4')
                         wrapscoringList[3].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[4].wgt==null && cw.QuestionSequence__c=='Q5')
                         wrapscoringList[4].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[5].wgt==null && cw.QuestionSequence__c=='Q6')
                         wrapscoringList[5].wgt=cw.CountryWeight__c;
                      else if(wrapscoringList[6].wgt==null && cw.QuestionSequence__c=='Q7')
                         wrapscoringList[6].wgt=cw.CountryWeight__c;
                     else if(wrapscoringList[7].wgt==null && cw.QuestionSequence__c=='Q8')
                         wrapscoringList[7].wgt=cw.CountryWeight__c;
                 } 
             }
             else if(capQuesAnsList.size()==8){
                for(CAPGlobalMasterData__c cw:capQuesAnsList){
                     if(wrapscoringList[0].wgt==null && cw.QuestionSequence__c=='Q1')
                         wrapscoringList[0].wgt=cw.Weight__c;
                     else if(wrapscoringList[1].wgt==null && cw.QuestionSequence__c=='Q2')
                         wrapscoringList[1].wgt=cw.Weight__c;
                      else if(wrapscoringList[2].wgt==null && cw.QuestionSequence__c=='Q3')
                         wrapscoringList[2].wgt=cw.Weight__c;
                      else if(wrapscoringList[3].wgt==null && cw.QuestionSequence__c=='Q4')
                         wrapscoringList[3].wgt=cw.Weight__c;
                      else if(wrapscoringList[4].wgt==null && cw.QuestionSequence__c=='Q5')
                         wrapscoringList[4].wgt=cw.Weight__c;
                      else if(wrapscoringList[5].wgt==null && cw.QuestionSequence__c=='Q6')
                         wrapscoringList[5].wgt=cw.Weight__c;
                      else if(wrapscoringList[6].wgt==null && cw.QuestionSequence__c=='Q7')
                         wrapscoringList[6].wgt=cw.Weight__c;
                     else if(wrapscoringList[7].wgt==null && cw.QuestionSequence__c=='Q8')
                         wrapscoringList[7].wgt=cw.Weight__c;
                 } 
             }
             else if(capQuesAnsGlobalList.size()==8){
                for(CAPGlobalMasterData__c cw:capQuesAnsGlobalList){
                     if(wrapscoringList[0].wgt==null && cw.QuestionSequence__c=='Q1')
                         wrapscoringList[0].wgt=cw.Weight__c;
                     else if(wrapscoringList[1].wgt!=null && cw.QuestionSequence__c=='Q2')
                         wrapscoringList[1].wgt=cw.Weight__c;
                      else if(wrapscoringList[2].wgt==null && cw.QuestionSequence__c=='Q3')
                         wrapscoringList[2].wgt=cw.Weight__c;
                      else if(wrapscoringList[3].wgt==null && cw.QuestionSequence__c=='Q4')
                         wrapscoringList[3].wgt=cw.Weight__c;
                      else if(wrapscoringList[4].wgt==null && cw.QuestionSequence__c=='Q5')
                         wrapscoringList[4].wgt=cw.Weight__c;
                      else if(wrapscoringList[5].wgt==null && cw.QuestionSequence__c=='Q6')
                         wrapscoringList[5].wgt=cw.Weight__c;
                      else if(wrapscoringList[6].wgt==null && cw.QuestionSequence__c=='Q7')
                         wrapscoringList[6].wgt=cw.Weight__c;
                     else if(wrapscoringList[7].wgt==null && cw.QuestionSequence__c=='Q8')
                         wrapscoringList[7].wgt=cw.Weight__c;
                 } 
             }
           
             if((acc.ExternalSalesResponsible__c==null && cap.AssignedTo__c==acc.OwnerID)|| (acc.ExternalSalesResponsible__c!=null && cap.AssignedTo__c==acc.ExternalSalesResponsible__c)){
                  ampList =[select CurrencyIsoCode,TotalPAM__c,Directsales__c,Indirectsales__c, Q1Rating__c, Q2Rating__c, Q3Rating__c, Q4Rating__c, Q5Rating__c, Q6Rating__c, Q7Rating__c, Q8Rating__c, Account__c from AccountMasterProfile__c where Account__c = :acc.Id];
                  if(ampList.size()>0){
                        if(ampList[0].TotalPAM__c!=null)
                                ps.PAMPlaforming__c= Math.round(ampList[0].TotalPAM__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(ampList[0].CurrencyIsoCode));
                            else
                               ps.PAMPlaforming__c=null;
                            if(ampList[0].DirectSales__c!=null) 
                                ps.DirectSalesPlatforming__c = Math.round(ampList[0].DirectSales__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(ampList[0].CurrencyIsoCode));
                            else
                                ps.DirectSalesPlatforming__c =null;
                            if(ampList[0].IndirectSales__c!=null)
                                ps.IndirectSalesPlatforming__c= Math.round(ampList[0].IndirectSales__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(ampList[0].CurrencyIsoCode));
                            else
                               ps.IndirectSalesPlatforming__c=null; 
                           wrapscoringList[0].rating=ampList[0].Q1Rating__c;
                           wrapscoringList[1].rating=ampList[0].Q2Rating__c;
                           wrapscoringList[2].rating=ampList[0].Q3Rating__c;
                           wrapscoringList[3].rating=ampList[0].Q4Rating__c;
                           wrapscoringList[4].rating=ampList[0].Q5Rating__c;
                           wrapscoringList[5].rating=ampList[0].Q6Rating__c;
                           wrapscoringList[6].rating=ampList[0].Q7Rating__c;
                           wrapscoringList[7].rating=ampList[0].Q8Rating__c;
                           ps.Tech_ReferenceRecord__c='OwnerMapping_'+ampList[0].Id;
                     }
                }
               else{
                 Integer teamMemberCheck=[select count() from AccountTeamMember where UserId = :cap.AssignedTo__c and Account.OwnerId != :cap.AssignedTo__c and Account.ExternalSalesResponsible__c!= :cap.AssignedTo__c and AccountId=:acc.Id];
                 if(teamMemberCheck>0){
                     ps.Tech_ReferenceRecord__c='TeamMemberMapping';
                     String GMRBU;
                     CS_UserBUGMRBusinessUnitMapping__c uBUGMRBUMap= CS_UserBUGMRBusinessUnitMapping__c.getValues(u.UserBusinessUnit__c);
                     if(uBUGMRBUMap!=null)
                         GMRBU= uBUGMRBUMap.GMRBusinessUnit__c;
                     if(GMRBU!=null){
                        List<AccountMasterProfilePAM__c> amppamList=[select AccountMasterProfile__c,AccountMasterProfile__r.Account__c,DirectSales__c, IndirectSales__c, PAM__c, GMRBusinessUnit__c from AccountMasterProfilePAM__c where AccountMasterProfile__r.Account__c = :acc.Id and GMRBusinessUnit__c=:GMRBU and Active__c=TRUE];
                        if(amppamList.size()>0){
                            if(amppamList[0].PAM__c!=null)
                                ps.PAMPlaforming__c= Math.round(amppamList[0].PAM__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(amppamList[0].CurrencyIsoCode));
                            else
                               ps.PAMPlaforming__c=null;
                            if(amppamList[0].DirectSales__c!=null) 
                                ps.DirectSalesPlatforming__c = Math.round(amppamList[0].DirectSales__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(amppamList[0].CurrencyIsoCode));
                            else
                                ps.DirectSalesPlatforming__c =null;
                            if(amppamList[0].IndirectSales__c!=null)
                                ps.IndirectSalesPlatforming__c= Math.round(amppamList[0].IndirectSales__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(amppamList[0].CurrencyIsoCode));
                            else
                               ps.IndirectSalesPlatforming__c=null; 
                          ps.Tech_ReferenceRecord__c=ps.Tech_ReferenceRecord__c+'_'+GMRBU+'_PAMrec='+amppamList[0].Id;
                        }
                    }
                    
                    else if(GMRBU==null){
                        ampList =[select CurrencyIsoCode,TotalPAM__c,Directsales__c,Indirectsales__c, Q1Rating__c, Q2Rating__c, Q3Rating__c, Q4Rating__c, Q5Rating__c, Q6Rating__c, Q7Rating__c, Q8Rating__c, Account__c from AccountMasterProfile__c where Account__c = :acc.Id];
                        if(ampList.size()>0){
                           if(ampList[0].TotalPAM__c!=null)
                                ps.PAMPlaforming__c= Math.round(ampList[0].TotalPAM__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(ampList[0].CurrencyIsoCode));
                            else
                               ps.PAMPlaforming__c=null;
                            if(ampList[0].DirectSales__c!=null) 
                                ps.DirectSalesPlatforming__c = Math.round(ampList[0].DirectSales__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(ampList[0].CurrencyIsoCode));
                            else
                                ps.DirectSalesPlatforming__c =null;
                            if(ampList[0].IndirectSales__c!=null)
                                ps.IndirectSalesPlatforming__c= Math.round(ampList[0].IndirectSales__c * currencyMap.get(curuser.DefaultCurrencyIsoCode)/currencyMap.get(ampList[0].CurrencyIsoCode));
                            else
                               ps.IndirectSalesPlatforming__c=null; 
                           ps.Tech_ReferenceRecord__c=ps.Tech_ReferenceRecord__c+'_PAMrec='+ampList[0].Id;

                        }
                    }
                     lastYearPS=[select CurrencyIsoCode,createddate,PlatformedAccount__c,AssignedTo__c,IndivCAP__r.AssignedTo__c,Tech_AccAssignedToBUMap__c, PAMPlaforming__c, DirectSalesPlatforming__c, IndirectSalesPlatforming__c,Q1Rating__c, Q2Rating__c, Q3Rating__c,Q4Rating__c,Q5Rating__c,Q6Rating__c,Q7Rating__c,Q8Rating__c from SFE_PlatformingScoring__c where IndivCAP__r.StartDate__c=LAST_N_DAYS:548 and IndivCAP__r.Status__c=:Label.CL00444 and PlatformedAccount__c=:acc.id and IndivCAP__r.AssignedTo__c=:cap.AssignedTo__c Order by IndivCAP__r.StartDate__c desc limit 1];
                     if(lastYearPS.size()>0){
                         wrapscoringList[0].rating=lastYearPS[0].Q1Rating__c;
                         wrapscoringList[1].rating=lastYearPS[0].Q2Rating__c;
                         wrapscoringList[2].rating=lastYearPS[0].Q3Rating__c;
                         wrapscoringList[3].rating=lastYearPS[0].Q4Rating__c;
                         wrapscoringList[4].rating=lastYearPS[0].Q5Rating__c;
                         wrapscoringList[5].rating=lastYearPS[0].Q6Rating__c;
                         wrapscoringList[6].rating=lastYearPS[0].Q7Rating__c;
                         wrapscoringList[7].rating=lastYearPS[0].Q8Rating__c;
                         ps.Tech_ReferenceRecord__c=ps.Tech_ReferenceRecord__c+'_Ratingrec='+lastYearPS[0].Id;
                     }
                     else{
                        lastYearBUPS=[select CurrencyIsoCode,createddate,PlatformedAccount__c,AssignedTo__c,IndivCAP__r.AssignedTo__c,Tech_AccAssignedToBUMap__c, PAMPlaforming__c, DirectSalesPlatforming__c, IndirectSalesPlatforming__c,Q1Rating__c, Q2Rating__c, Q3Rating__c,Q4Rating__c,Q5Rating__c,Q6Rating__c,Q7Rating__c,Q8Rating__c from SFE_PlatformingScoring__c where IndivCAP__r.StartDate__c=LAST_N_DAYS:548 and  IndivCAP__r.Status__c=:Label.CL00444 and PlatformedAccount__c=:acc.id and Tech_AccAssignedToBUMap__c=:acc.id+'_'+u.UserBusinessUnit__c Order by IndivCAP__r.StartDate__c desc limit 1];
                        if(lastYearBUPS.size()>0){
                         wrapscoringList[0].rating=lastYearBUPS[0].Q1Rating__c;
                         wrapscoringList[1].rating=lastYearBUPS[0].Q2Rating__c;
                         wrapscoringList[2].rating=lastYearBUPS[0].Q3Rating__c;
                         wrapscoringList[3].rating=lastYearBUPS[0].Q4Rating__c;
                         wrapscoringList[4].rating=lastYearBUPS[0].Q5Rating__c;
                         wrapscoringList[5].rating=lastYearBUPS[0].Q6Rating__c;
                         wrapscoringList[6].rating=lastYearBUPS[0].Q7Rating__c;
                         wrapscoringList[7].rating=lastYearBUPS[0].Q8Rating__c;
                         ps.Tech_ReferenceRecord__c=ps.Tech_ReferenceRecord__c+'_Ratingrec='+lastYearBUPS[0].Id;
                     }
                   }
                 }
               }
           }
        }
        System.debug('>>>>>>wrapscoringList'+ wrapscoringList);
    }
    
     public PageReference Save(){
            ps.Q1Rating__c=wrapscoringList[0].rating;
            ps.Q2Rating__c=wrapscoringList[1].rating;
            ps.Q3Rating__c=wrapscoringList[2].rating;
            ps.Q4Rating__c=wrapscoringList[3].rating;
            ps.Q5Rating__c=wrapscoringList[4].rating;
            ps.Q6Rating__c=wrapscoringList[5].rating;
            ps.Q7Rating__c=wrapscoringList[6].rating;
            ps.Q8Rating__c=wrapscoringList[7].rating;
            try{
                insert ps;
                PageReference curPage = new PageReference(Site.getPathPrefix()+'/apex/VFP_ViewCAPPlatformingScoring');
                curPage.getParameters().put('id',ps.ID);
                curPage.setRedirect(true);
                return curPage;
           }
           Catch(Exception ex){
           ApexPages.addMessages(ex);        
          } 
          return null;
    }
   public PageReference Cancel(){
      PageReference curPage = new PageReference(Site.getPathPrefix()+'/'+cap.Id);
      curPage.setRedirect(true);
      return curPage;
    }
      public class wrapscoring{
        public String qseq{get; set;}
        public String ques{get; set;}
        public String rating{get; set;}
        public String ans1{get; set;}
        public String ans2{get; set;}
        public String ans3{get; set;}
        public String ans4{get; set;}
        public Decimal wgt{get; set;}
        public wrapscoring(){}
     }
}