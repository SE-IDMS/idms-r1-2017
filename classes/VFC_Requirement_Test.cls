@isTest

private class VFC_Requirement_Test
{
    static testMethod void testVFC_RequirementCertifications() 
    {
        RequirementCatalog__c rc = new RequirementCatalog__c();
        rc.name = 'test Requirement 1';
        insert rc;
        
        CertificationCatalog__c cc1 = new CertificationCatalog__c();
        cc1.CertificationName__c= 'cert 1';
        insert cc1;
        
        RequirementCertification__c rcert = new RequirementCertification__c();
        rcert.CertificationCatalog__c = cc1.id;
        rcert.RequirementCatalog__c  = rc.id;
        insert rcert;
        
        Apexpages.Standardcontroller cont1 = new Apexpages.Standardcontroller(rc);
        Pagereference pg1 = Page.VFP_RequirementCertifications; 
        Test.setCurrentPage(pg1);        
        VFC_RequirementCertifications rcp = new VFC_RequirementCertifications(cont1);
        
        
        Apexpages.Standardcontroller cont2 = new Apexpages.Standardcontroller(rc);
        Pagereference pg2 = Page.VFP_RequirementSpecializations; 
        Test.setCurrentPage(pg2);
        VFC_RequirementSpecializations rcp2 = new VFC_RequirementSpecializations(cont2);
        
    }

}