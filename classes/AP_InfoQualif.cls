public class AP_InfoQualif
{
    public static void setCompletion(List<InfoQualif__c> infoQualifs)
    {
        List<Id> accountsId = new List<Id>();

        for (InfoQualif__c infoQualif : infoQualifs) {
            accountsId.add(infoQualif.account__c);
        }

        Map<Id, Account> accounts = new Map<Id, Account>([
            SELECT Id, Owner.UserBusinessUnit__c, ClassLevel1__c, Name
            FROM Account
            WHERE Id IN :accountsId
        ]);

        for (InfoQualif__c infoQualif : infoQualifs) {
            Integer completionCount = 0;

            // Default fields
            Set<String> validationDateFields = new Set<String> {
                'NameValidationDate__c',
                'AddressValidationDate__c',
                'PhoneValidationDate__c',
                'SiretValidationDate__c',
                'NafValidationDate__c',
                'ClassLevel1ValidationDate__c',
                'EmployeeSizeValidationDate__c',
                'AnnualRevenueValidationDate__c',
                'AnnualEquipmentAmountValidationDate__c',
                'AnnualSchneiderAmountValidationDate__c',
                'OpinionLeaderValidationDate__c',
                'ContenderPurchaseValidationDate__c',
                'RelationShipValidationDate__c',
                'PriceSensitivityValidationDate__c',
                'SchneiderPromoterValidationDate__c'
            };

            Account currentAccount = accounts.get(infoQualif.account__c);

            if (currentAccount != null) {
                // Classification Level 2
                if (
                    !(
                        (
                            currentAccount.Owner.UserBusinessUnit__c == 'PR' ||
                            currentAccount.Owner.UserBusinessUnit__c == 'PW'
                        ) &&
                        (
                            currentAccount.ClassLevel1__c == 'SP' ||
                            currentAccount.ClassLevel1__c == 'LC' ||
                            currentAccount.ClassLevel1__c == 'SC'
                        ) &&
                        (
                            infoQualif.classLevel1Approb__c != null &&
                            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
                            infoQualif.classLevel1Approb__c == null &&
                            infoQualif.ClassLevel1ValidationDate__c != null
                        )
                    )
                ) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'ClassLevel2ValidationDate__c'
                        }
                    );
                }

                // Classification détaillée
                if (
                    (
                        currentAccount.Owner.UserBusinessUnit__c == 'PR' ||
                        currentAccount.Owner.UserBusinessUnit__c == 'PW'
                    ) &&
                    (
                        currentAccount.ClassLevel1__c == 'SP' ||
                        currentAccount.ClassLevel1__c == 'LC' ||
                        currentAccount.ClassLevel1__c == 'SC'
                    ) &&
                    (
                        infoQualif.classLevel1Approb__c != null &&
                        infoQualif.classLevel1Approb__c != 'En attente approbation' ||
                        infoQualif.classLevel1Approb__c == null &&
                        infoQualif.ClassLevel1ValidationDate__c != null
                    )
                ) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'ClassDetailValidationDate__c'
                        }
                    );
                }

                // Electriciens
                if (isElectriciens(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'CountCrewsValidationDate__c',
                            'LogementsValidationDate__c',
                            'CanalAchatValidationDate__c',
                            'PartElecValidationDate__c',
                            'TypeChantierValidationDate__c',
                            'PartRenovationValidationDate__c',
                            'EcoEnergieValidationDate__c',
                            'DecoValidationDate__c',
                            'CommandeValidationDate__c',
                            'RetraitValidationDate__c'
                        }
                    );
                }

                // Prescripteurs
                if (isPrescripteurs(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'DomaineValidationDate__c',
                            'LogementsValidationDate__c',
                            'TertiaireValidationDate__c',
                            'HotelValidationDate__c',
                            'SanteValidationDate__c',
                            'GammeValidationDate__c',
                            'AppareillageValidationDate__c',
                            'ConnectesValidationDate__c',
                            'TableauxComValidationDate__c',
                            'ComptageValidationDate__c'
                        }
                    );

                    validationDateFields.remove('AnnualEquipmentAmountValidationDate__c');
                    validationDateFields.remove('AnnualSchneiderAmountValidationDate__c');
                    validationDateFields.remove('ContenderPurchaseValidationDate__c');
                    validationDateFields.remove('PriceSensitivityValidationDate__c');
                }

                // ITB
                if (isItb(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'NombrePersonnesAvantVenteValidationDate__c',
                            'ProgrammePartenaireITBValidationDate__c',
                            'VenteAnnuelleServeursValidationDate__c',
                            'OnduleursValidationDate__c',
                            'RacksValidationDate__c',
                            'PDUsValidationDate__c',
                            'CommerciauxProduitsValidationDate__c',
                            'AutonomieVendreSchneiderValidationDate__c',
                            'PotentielCroissanceValidationDate__c',
                            'RisquePerteCAValidationDate__c',
                            'CompatibiliteOffreValidationDate__c'
                        }
                    );
                }

                // PPEB Tableautiers
                if (isPpebTableautiers(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'UltiParentAccValidationDate__c',
                            'PersonnelFixeValidationDate__c',
                            'NbChefsValidationDate__c',
                            'NbCableursValidationDate__c',
                            'UniteDeporteeValidationDate__c',
                            'JDBValidationDate__c',
                            'MoyensTechniquesValidationDate__c',
                            'QualiteValidationDate__c',
                            'SegmentsValidationDate__c',
                            'TableauValidationDate__c',
                            'IndiceValidationDate__c',
                            'EnveloppesUniversellesValidationDate__c',
                            'CoffretsValidationDate__c',
                            'TGBT3200AValidationDate__c',
                            'PDMDistriValidationDate__c',
                            'PRMControleValidationDate__c',
                            'DOValidationDate__c'
                        }
                    );
                }

                // IND System Integrators
                if (isIndSystemIntegrators(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'MarketSegmentValidationDate__c',
                            'MarketSubSegmentValidationDate__c',
                            'SIAllianceValidationDate__c',
                            'PotentielCroissanceValidationDate__c',
                            'PAMOneValidationDate__c',
                            'CiblageValidationDate__c',
                            'AdequationOffreValidationDate__c',
                            'ChargesAffairesValidationDate__c'
                        }
                    );
                    validationDateFields.remove('AnnualEquipmentAmountValidationDate__c');
                    validationDateFields.remove('AnnualSchneiderAmountValidationDate__c');
                }

                // IND Tableautiers
                if (isIndTableautiers(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'MarketSegmentValidationDate__c',
                            'MarketSubSegmentValidationDate__c',
                            'SIAllianceValidationDate__c',
                            'NbCableursValidationDate__c',
                            'PotentielCroissanceValidationDate__c',
                            'PAMOneValidationDate__c',
                            'CiblageValidationDate__c',
                            'AdequationOffreValidationDate__c'
                        }
                    );
                    validationDateFields.remove('AnnualEquipmentAmountValidationDate__c');
                    validationDateFields.remove('AnnualSchneiderAmountValidationDate__c');
                }

                // IND OEMs
                if (isIndOems(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'MarketSegmentValidationDate__c',
                            'MarketSubSegmentValidationDate__c',
                            'SIAllianceValidationDate__c',
                            'PotentielCroissanceValidationDate__c',
                            'PAMOneValidationDate__c',
                            'CiblageValidationDate__c',
                            'AdequationOffreValidationDate__c',
                            'MachinesValidationDate__c'
                        }
                    );
                    validationDateFields.remove('AnnualEquipmentAmountValidationDate__c');
                    validationDateFields.remove('AnnualSchneiderAmountValidationDate__c');
                }

                // DIS
                if (isDis(currentAccount, infoQualif)) {
                    validationDateFields.addAll(
                        new Set<String> {
                            'DisAgencementValidationDate__c',
                            'DisDiffusionTvValidationDate__c',
                            'DisTypesClientsValidationDate__c'
                        }
                    );

                    // REXEL account specificity
                    if (
                        currentAccount.Name != null &&
                        currentAccount.Name.toUpperCase().contains('REXEL')
                    ) {
                        validationDateFields.addAll(
                            new Set<String> {
                                'DisRexelTopValidationDate__c'
                            }
                        );
                    }

                    validationDateFields.remove('ContenderPurchaseValidationDate__c');
                    validationDateFields.remove('RelationShipValidationDate__c');
                    validationDateFields.remove('PriceSensitivityValidationDate__c');
                    validationDateFields.remove('AnnualRevenueValidationDate__c');
                    validationDateFields.remove('OpinionLeaderValidationDate__c');
                    validationDateFields.remove('SchneiderPromoterValidationDate__c');
                }

                Datetime maxValidationDate = null;
                for (String field : validationDateFields) {
                    if (infoQualif.get(field) != null) {
                        if (
                            maxValidationDate == null ||
                            (Datetime) infoQualif.get(field) > maxValidationDate
                        ) {
                            maxValidationDate = (Datetime) infoQualif.get(field);
                        }

                        completionCount++;
                    }
                }

                for (String field : validationDateFields) {
                    System.debug(field);
                }

                infoQualif.CompletionCount2__c = completionCount;
                infoQualif.Completion2__c = (completionCount * 1.0 / validationDateFields.size()) * 100;
                infoQualif.LastValidationDate__c = maxValidationDate;
            }
        }
    }

    public static Boolean isElectriciens(Account account, InfoQualif__c infoQualif)
    {
        return (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        ) &&
        (
            account.Owner.UserBusinessUnit__c == 'PR' ||
            account.Owner.UserBusinessUnit__c == 'PW'
        ) &&
        (
            account.ClassLevel1__c == 'SC' ||
            account.ClassLevel1__c == 'LC' &&
            infoQualif.RetailClassDetail__c == 'Installateur major'
        );
    }

    public static Boolean isPrescripteurs(Account account, InfoQualif__c infoQualif)
    {
        return (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        ) &&
        (
            account.Owner.UserBusinessUnit__c == 'PR' ||
            account.Owner.UserBusinessUnit__c == 'PW'
        ) &&
        (
            account.ClassLevel1__c == 'SP' ||
            account.ClassLevel1__c == 'LC' &&
            (
                infoQualif.RetailClassDetail__c == 'CMIste (installateur)' ||
                infoQualif.RetailClassDetail__c == 'Entreprise générale du bâtiment (EGB)'
            )
        );
    }

    public static Boolean isItb(Account account, InfoQualif__c infoQualif)
    {
        return account.Owner.UserBusinessUnit__c == 'IT';
    }

    public static Boolean isPpebTableautiers(Account account, InfoQualif__c infoQualif)
    {
        return (
            account.Owner.UserBusinessUnit__c == 'PP' ||
            account.Owner.UserBusinessUnit__c == 'BD'
        ) &&
        (
            account.ClassLevel1__c == 'PB'
        ) &&
        (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        );
    }

    public static Boolean isIndSystemIntegrators(Account account, InfoQualif__c infoQualif)
    {
        return (
            account.Owner.UserBusinessUnit__c == 'IA' ||
            account.Owner.UserBusinessUnit__c == 'IB' ||
            account.Owner.UserBusinessUnit__c == 'ID'
        ) &&
        (
            account.ClassLevel1__c == 'SI'
        ) &&
        (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        );
    }

    public static Boolean isIndTableautiers(Account account, InfoQualif__c infoQualif)
    {
        return (
            account.Owner.UserBusinessUnit__c == 'IA' ||
            account.Owner.UserBusinessUnit__c == 'IB' ||
            account.Owner.UserBusinessUnit__c == 'ID'
        ) &&
        (
            account.ClassLevel1__c == 'PB'
        ) &&
        (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        );
    }

    public static Boolean isIndOems(Account account, InfoQualif__c infoQualif)
    {
        return (
            account.Owner.UserBusinessUnit__c == 'IA' ||
            account.Owner.UserBusinessUnit__c == 'IB' ||
            account.Owner.UserBusinessUnit__c == 'ID'
        ) &&
        (
            account.ClassLevel1__c == 'OM'
        ) &&
        (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        );
    }

    public static Boolean isDis(Account account, InfoQualif__c infoQualif)
    {
        return (
            infoQualif.classLevel1Approb__c != null &&
            infoQualif.classLevel1Approb__c != 'En attente approbation' ||
            infoQualif.classLevel1Approb__c == null &&
            infoQualif.ClassLevel1ValidationDate__c != null
        ) &&
        (
            account.Owner.UserBusinessUnit__c == 'PR' ||
            account.Owner.UserBusinessUnit__c == 'PW'
        ) &&
        (
            account.ClassLevel1__c == 'WD'
        );
    }
}