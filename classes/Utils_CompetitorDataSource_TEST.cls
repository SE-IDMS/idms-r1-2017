/*
    Author          : Accenture IDC Team 
    Date Created    : 31/07/2011
    Description     : Test Class for Competitor Data Source Utility class
*/
@Istest

class Utils_CompetitorDataSource_TEST
{
    private static Country__c country;
    private static Competitor__c competitor;
    private static CompetitorCountry__c competitorCountry;
  
    static
    {
    // create test data
      country = Utils_TestMethods.createCountry();
      insert country;
      competitor = Utils_TestMethods.createCompetitor();
      insert competitor;
      competitorCountry = Utils_TestMethods.createCompetitorCountry(competitor.Id,country.Id);
      insert competitorCountry;
    }
    
    
    static testMethod void competitorData_TestMethod()
    {
        Utils_DataSource CompetitorDataSource = Utils_Factory.getDataSource('COMP');
        List<SelectOption> Columns = CompetitorDataSource.getColumns();
        List<String> dummyList1 = new List<String>();
        List<String> dummyList2 = new List<String>();
        Integer counter = 0;
        
        for(Integer index=1;index<9;index++)
        {
            dummyList1.clear();
            dummyList2.clear();                
            counter = index;
            if(counter == 1)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 2)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 3)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 4)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 5)
            {
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 6)
            {
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 7)
            {
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            if(counter == 8)
            {
                dummyList1.add('UNKNOWN');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = CompetitorDataSource.search(dummyList1, dummyList2);
            }
            
        }
        
        
        Utils_DataSource.Result results = CompetitorDataSource.search(dummyList1, dummyList2);
    }
}