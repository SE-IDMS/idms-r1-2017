/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For May 13 PRM Release:
                 1. To update the Feature related to the requirement as ‘Active when all the Requirement related to feature has ‘Met’ or ‘Waived’ status.
    
    Updated By : Shruti Karn
    Description : For October 13 PRM Release:
                 1. Calls AP_AAR_UpdateRequirmentFeature.updateAccountProgramOnExpiration to update the Account Program 'Eligible for' related 
                     to the requirement as ‘Demotion ' when the Requirement status is updated as 'Inactive'.
********************************************************************************************************************/
public class AP_AAR_UpdateRequirmentFeature
{
    public static void updateAccountFeature(map<Id,AccountAssignedRequirement__c> mapNewAccountRequirement , map<Id, AccountAssignedRequirement__c> mapOldAccountRequirement)
    {
        set<Id> setAccFeatureId = new set<Id>();
        list<AccountAssignedFeature__c> lstAccountFeature = new list<AccountAssignedFeature__c>();
        map<Id,AccountAssignedRequirement__c> mapAccFeatureReq = new  map<Id,AccountAssignedRequirement__c>();
        map<Id,list<AccountAssignedRequirement__c>> mapAccFeatureReqlist = new map<Id,list<AccountAssignedRequirement__c>>();
        set<ID> setAccountFeature = new set<ID>();
        for(Id ReqID : mapNewAccountRequirement.keySet())
        {
            if(mapNewAccountRequirement.get(reqID).RequirementStatus__c != mapOldAccountRequirement.get(reqID).RequirementStatus__c && mapNewAccountRequirement.get(reqID).FeatureRequirement__c != null)
                mapAccFeatureReq.put(mapNewAccountRequirement.get(reqID).AccountAssignedFeature__c,mapNewAccountRequirement.get(reqID));
        }
        
        list<AccountAssignedRequirement__c> lstAccountReq = [Select id, AccountAssignedFeature__c,RequirementStatus__c from AccountAssignedRequirement__c where AccountAssignedFeature__c in :mapAccFeatureReq.keySet() and (requirementstatus__c !=:Label.CLMAY13PRM16 or requirementstatus__c !=: Label.CLMAY13PRM17) limit 10000];
        for(AccountAssignedRequirement__c req : lstAccountReq)
        {
                if(!mapAccFeatureReqlist.containsKey(req.AccountAssignedFeature__c))
                    mapAccFeatureReqlist.put(req.AccountAssignedFeature__c , new list<AccountAssignedRequirement__c> {(req)});
                else
                    mapAccFeatureReqlist.get(req.AccountAssignedFeature__c).add(req);
                if(!(req.RequirementStatus__c == Label.CLMAY13PRM16 || req.RequirementStatus__c == Label.CLMAY13PRM17))
                {
                    setAccountFeature.add(req.AccountAssignedFeature__c);
                }
                    
        }
        
        for(Id accountFeatureID : setAccountFeature)
            mapAccFeatureReqlist.remove(accountFeatureID);
        lstAccountFeature = [Select id,Active__c from AccountAssignedFeature__c where id in :mapAccFeatureReqlist.keySet() or id in :setAccountFeature limit 10000];
        if(!lstAccountFeature.isEmpty())
        {
            for(AccountAssignedFeature__c accFTR : lstAccountFeature)
            {
                if(mapAccFeatureReqlist.containsKey(accFTR.Id))
                    accFTR.Active__C = true;
                if(setAccountFeature.contains(accFTR.ID))
                    accFTR.Active__C = false;
            }
            try
            {
                update lstAccountFeature;
            }
            catch(Exception e)
            {
                for (Integer i = 0; i < e.getNumDml(); i++) 
                { 
                    mapNewAccountRequirement.values().get(0).addError( e.getDmlMessage(i)); 
                } 
            }
        } 
    
    }
    
    
    public static void updateAccountProgramOnExpiration(set<ID> setAccountProgramID)
    {
        list<ACC_PartnerProgram__c> lstAccountProgram = new list<ACC_PartnerProgram__c>();
        try
        {
            lstAccountProgram = [Select id,Eligiblefor__c from ACC_PartnerProgram__c where id in:setAccountProgramID limit 10000];
            for(ACC_PartnerProgram__c acctProgram : lstAccountProgram)
                acctProgram.Eligiblefor__c = Label.CLOCT13PRM25;
            update lstAccountProgram;
        }
        catch(Exception e)
        {
            system.debug('Exception while updating Account Assigned Program to Demotion:'+e.getMessage());
        }
    }    
}