/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 30-sep-2013
    Description     : Test Class for IPO Acid Test Controller
*/

@isTest
private class IPOAcidTestController_Test {
    static testMethod void testIPOAcidTestController() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOAt');
                System.runAs(runAsUser){
                
                 //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.IPOAcidTestQuestions;
       
       //In this case, the Visualforce page named 'IPO Acid Test' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
        
       //Insert Data
     
      IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
       
      IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
       
       IPOActidTestQuestions__c ActQ1 = new IPOActidTestQuestions__c(Qno__c=1,Question__c='Test Question 1');
       insert ActQ1;
       IPOAcidTestAnswers__c ActA1 = new IPOAcidTestAnswers__c(IPOAcidTestQuestions__c = ActQ1.id,  Answer__c='Yes',Remarks__c = 'test');
       insert ActA1;
     
           
         ApexPages.StandardController stanPage= new ApexPages.StandardController(IPOp);
        IPOAcidTestController controller = new IPOAcidTestController (stanPage);
            
  
        
         controller.getItems();
         controller.getQuestionAnswer();
         controller.change();
         controller.cancel();
         controller.save();
         }
        }
       }