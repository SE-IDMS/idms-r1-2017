/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is used for calling UIMS webservices
**/
public without sharing class IDMSUIMSWebServiceCalls{
    
    static IDMSCaptureError errorLog = new IDMSCaptureError();
    IDMSEncryption idmsEncryption    = new IDMSEncryption();
    WS_IdmsUimsAuthUserManagerV2Impl.AuthenticatedUserManager_UIMSV2_ImplPort  unitApiAuthService;
    WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiService;
    
    
    public IDMSUIMSWebServiceCalls(){
        unitApiAuthService                  = new WS_IdmsUimsAuthUserManagerV2Impl.AuthenticatedUserManager_UIMSV2_ImplPort();
        unitApiAuthService.clientCertName_x = System.Label.CLJUN16IDMS103;
        unitApiService                      = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();    
        unitApiService.clientCertName_x     = System.Label.CLJUN16IDMS103;      
    }
    
    //this future method is used to update uims user
    @future (callout = true)
    public static void updateUIMSUser(String UserRecord1,string callerFid){
        user UserRecord = (user)JSON.deserialize(UserRecord1,user.class);
        try{
            system.debug('user in uims'+UserRecord);
            user u = [SELECT IDMS_VC_NEW__c,IDMS_VC_OLD__c,IDMS_VU_NEW__c,IDMS_VU_OLD__c,IsIDMSUser__c,FederationIdentifier  
                      FROM User 
                      WHERE id =:UserRecord.id];
            
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiService = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            unitApiService.clientCertName_x                                         = System.Label.CLJUN16IDMS103;           
            userrecord.FederationIdentifier                                         = u.FederationIdentifier;
            IDMSEncryption idmsEncryption                                           = new IDMSEncryption();
            String samlAssrestion                                                   = idmsEncryption.ecryptedToken(UserRecord.FederationIdentifier,u.IDMS_VU_NEW__c);
            
            WS_IdmsUimsUserManagerV2Service.userV5 identity = new WS_IdmsUimsUserManagerV2Service.userV5();
            identity.federatedID                            = (UserRecord.FederationIdentifier != null)?USerRecord.FederationIdentifier:null;
            identity.middleName                             = (UserRecord.IDMSMiddleName__c != null)?USerRecord.IDMSMiddleName__c:null;
            identity.salutation                             = (UserRecord.IDMSSalutation__c != null)?USerRecord.IDMSSalutation__c:null;            
            identity.email                                  = (UserRecord.email != null)?UserRecord.email:null;
            identity.firstName                              = (UserRecord.firstname != null)?UserRecord.firstname:null;
            identity.lastName                               = (UserRecord.lastname != null)?UserRecord.lastname:null;
            identity.languageCode                           = (UserRecord.IDMS_PreferredLanguage__c != null)?UserRecord.IDMS_PreferredLanguage__c.toLowerCase():null;
            identity.countryCode                            = (UserRecord.Country != null)?UserRecord.Country:null;
            identity.fax                                    = (UserRecord.fax != null)?UserRecord.fax:null;
            identity.phone                                  = (UserRecord.phone != null)?UserRecord.phone:null;
            identity.state                                  = (UserRecord.State != null)?UserRecord.State :null;
            identity.county                                 = (UserRecord.IDMS_County__c  != null)?UserRecord.IDMS_County__c:null;
            identity.street                                 = (UserRecord.street != null)?UserRecord.street:null;
            identity.postalCode                             = (UserRecord.PostalCode != null)?UserRecord.PostalCode:null;
            identity.postOfficeBox                          = (UserRecord.IDMS_POBox__c !=null)?UserRecord.IDMS_POBox__c:null;
            identity.cell                                   = (UserRecord.mobilephone != null)?UserRecord.mobilephone :null;
            identity.addInfoAddress                         = (UserRecord.IDMS_AdditionalAddress__c != null)?UserRecord.IDMS_AdditionalAddress__c:null; 
            identity.localityName                           = (UserRecord.city != null)?UserRecord.city:null;                 
            identity.JobTitle                               = (UserRecord.Job_Title__c != null && !((string)UserRecord.Job_Title__c).equalsignorecase('null'))?UserRecord.Job_Title__c:null;                      
            identity.JobFunction                            = (UserRecord.Job_Function__c != null && !((string)UserRecord.Job_Function__c ).equalsignorecase('null'))?UserRecord.Job_Function__c :null;                      
            identity.JobDescription                         = (UserRecord.IDMSJobDescription__c != null && !((string)UserRecord.IDMSJobDescription__c).equalsignorecase('null'))?UserRecord.IDMSJobDescription__c:null;
            identity.MiddleName                             = (UserRecord.IDMSMiddleName__c != null && !((string)UserRecord.IDMSMiddleName__c).equalsignorecase('null'))?UserRecord.IDMSMiddleName__c:null;
            identity.Salutation                             = (UserRecord.IDMSSalutation__c != null && !((string)UserRecord.IDMSSalutation__c).equalsignorecase('null'))?UserRecord.IDMSSalutation__c:null;
            
            callerFid = Label.CLJUN16IDMS104;
            boolean res;
            if(!Test.isrunningTest()){
                res = unitApiService.updateUser(callerFid,samlAssrestion,identity);
            }      
            system.debug(res);
            if(res){
                UserRecord.idms_vu_old__c = u.IDMS_VU_NEW__c;
                system.debug(UserRecord.idms_vu_old__c+'     '+u.IDMS_VU_NEW__c);
                update userrecord;
            }
        }
        catch(exception e)
        {
            system.debug('---error Update---'+e); 
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            if(String.isNotEmpty(e.getMessage())){
                //add this user for reconciliation
                system.debug('Inside federation id check for user:'+UserRecord.id);
                Integer attemptsMaxLmt                                         = integer.valueof(label.CLNOV16IDMS022);
                List<IDMSUserReconciliationBatchHandler__c> userToReconciliate = [select Id,AtCreate__c, AtUpdate__c, IdmsUser__c,HttpMessage__c, NbrAttempts__c 
                                                                                  from IDMSUserReconciliationBatchHandler__c 
                                                                                  where IdmsUser__c =: UserRecord.id];
                if(userToReconciliate.size()>0){
                    if(userToReconciliate[0].AtCreate__c == true){
                        userToReconciliate[0].AtUpdate__c = false;
                    }else{
                        userToReconciliate[0].AtUpdate__c = true;
                    }
                    userToReconciliate[0].HttpMessage__c  = ExceptionMessage;
                    update userToReconciliate;
                }else {
                    system.debug('userToReconciliate id:'+userToReconciliate );
                    IDMSUserReconciliationBatchHandler__c userToReconciliateObj = new IDMSUserReconciliationBatchHandler__c ();
                    userToReconciliateObj.AtCreate__c                           = false;
                    userToReconciliateObj.AtUpdate__c                           = true;
                    userToReconciliateObj.HttpMessage__c                        = ExceptionMessage;
                    userToReconciliateObj.IdmsUser__c                           = UserRecord.id;
                    userToReconciliateObj.NbrAttempts__c                        = 1;
                    insert userToReconciliateObj;
                }
            }
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','updateUser UIMS web Service call',UserRecord.username,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'',UserRecord.FederationIdentifier,false,'',false,null);
        }
    }
    
    //This future method is used to make request for email change
    @future (callout = true)
    public static void requestEmailChangeUIMS(String newEmail,string userId,string appName){
        try{
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiAuthService = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();                     
            unitApiAuthService.clientCertName_x                       = System.Label.CLJUN16IDMS103;
            IDMSEncryption idmsEncryption                             = new IDMSEncryption();
            WS_IdmsUimsUserManagerV2Service.accessElement application = new WS_IdmsUimsUserManagerV2Service.accessElement();
            application.type_x                                        = System.label.CLJUN16IDMS70;
            application.id                                            = appName;
            user u                                                    = [select federationidentifier,IDMS_VU_NEW__c from user where id=:userId];
            String samlAssrestion                                     = idmsEncryption.ecryptedToken(u.FederationIdentifier,u.IDMS_VU_NEW__c);
            
            //NEED TO PASS USER EMAIL AND SAML ASSERTION
            boolean isEmailChanged;
            if(!Test.isrunningTest()){
                isEmailChanged = unitApiAuthService.requestEmailChange(Label.CLJUN16IDMS104,samlAssrestion,application,newEmail);
            }
            if(isEmailChanged){
                boolean updateRes = unitApiAuthService.updateEmail(Label.CLJUN16IDMS104,samlAssrestion);
                if(updateRes){
                    u.IDMS_VU_OLD__c = u.IDMS_VU_NEW__c;
                    update u;
                }
            }
        }               
        catch(exception e){ 
            system.debug('---error Update---'+e);  
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','requestEmailChange UIMS web Service call',userId,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);         
        }
    }
    
    //this method is used to update password at uims
    public boolean updatePassword(string oldPwd,String newPwd,string samlassertion){
        try{
            WS_IdmsUimsUserManagerV2Service.userV5 userUpdate = new WS_IdmsUimsUserManagerV2Service.userV5();
            string callerFid                                  = Label.CLJUN16IDMS104;
            boolean result;
            if(!Test.isrunningTest()){
                result = unitApiService.updatePassword(callerFid,samlassertion,oldPwd,newPwd);
            }
            return result;
        }
        catch(exception e){
            system.debug('---error Update---'+e); 
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','updatePassword UIMS Web Service Call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
            return null;
        }
    }
    
    //This method is used to rest passwrod in UIMS
    public string resetPassword(String fedId,string appid){
        try{            
            WS_IdmsUimsAuthUserManagerV2Service.accessElement application = new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            application.type_x                                            = System.label.CLJUN16IDMS70;
            application.id                                                = appid;
            string callerFid                                              = Label.CLJUN16IDMS104;       
            String resultReset;
            if(!Test.isrunningTest()){
                resultReset = unitApiAuthService.resetPassword(callerFid,fedId,application);        
            }
            return resultReset;
        }
        catch(exception e){
            system.debug('---error Update---'+e); 
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','resetPassword UIMS web Service call','',Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'',fedId,false,'',false,null);
            return null;
        }
    }  
    
    //request email change from pages
    public static boolean requestToEmailChangeUIMS(String newEmail,string userId,string appName){
        try{
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiService = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();                     
            unitApiService.clientCertName_x                           = System.Label.CLJUN16IDMS103;
            WS_IdmsUimsUserManagerV2Service.accessElement application = new WS_IdmsUimsUserManagerV2Service.accessElement();
            application.type_x                                        = System.label.CLJUN16IDMS70;
            application.id                                            = appName;
            String samlAssrestion                                     = [select  Assertion__c from USerSAMLAssertion__c where UserId__c=:userId].Assertion__c;
            //NEED TO PASS USER EMAIL AND SAML ASSERTION
            boolean isEmailChanged;
            if(!Test.isrunningTest()){
                isEmailChanged = unitApiService.requestEmailChange(Label.CLJUN16IDMS104,samlAssrestion,application,newEmail);
            }
            return isEmailChanged;
        }               
        catch(exception e){ 
            system.debug('---error Update---'+e);  
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','requestEmailChange UIMS web Service call',userId,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);         
            return false;
        }
    }
    
    //This method is used to set uims password
    public boolean uimsSetPassword(String newPassword,string authToken){
        try{
            boolean setPasswordRes;
            if(!Test.isrunningTest()){
                setPasswordRes = unitApiService.setPassword(Label.CLJUN16IDMS104,authToken,newPassword);
            }
            return setPasswordRes;
        }
        catch(Exception e){
            system.debug('---error Update---'+e);
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','uimsSetPassword UIMS web Service call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
            return null;         
        }
    }
    
    //This method is used to search user in UIMS
    public WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType SearchUserUIMSReset(string email){
        try{
            WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType resp ;
            if(!Test.isrunningTest()){
                resp = unitApiAuthService.searchUser(Label.CLJUN16IDMS104,email); 
            }
            system.debug('--search user resp--'+resp );
            if(resp != null && resp.federatedId != null)
                return resp;
        }
        catch(Exception e){
            system.debug('---error Update---'+e);
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','SearchUserUIMSReset UIMS web Service call','',Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);         
            return null;
        }    
        return null;
    }
    
    //UIMS Call for updateEmail
    public boolean uimsUpdateEmail(String callerFid,string authentificationToken){
        try{
            boolean updateEmailResp;
            if(!Test.isrunningTest()){
                updateEmailResp = unitApiService.updateEmail(callerFid,authentificationToken);
            } 
            return updateEmailResp;
        }catch(Exception e){
            system.debug('---error Update---'+e);
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','uimsUpdateEmail UIMS web Service call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);          
            return false;      
        }
    }
    
    //Method for searching user in UIMS.  
    public boolean SearchUserUIMS(string email){
        system.debug('---in searchuser uims--');
        try{
            WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType resp;
            if(!Test.isrunningTest()){ 
                resp = unitApiAuthService.searchUser(Label.CLJUN16IDMS104,email); 
            }
            else{
                if(Test.isrunningTest() && email=='testUIMSSearch@accenture.com'){
                    return true;
                }
            } 
            system.debug('--search user resp--'+resp );
            if(resp != null && resp.federatedId != null)
                return true;
        }
        catch(Exception e){
            system.debug('--in search user uims--catch--');
            return false;
        }    
        return false;
    }
    
    //method to create user in UIMS. User Object would passed as input parameter
    //Modified by Otmane Haloui to be asynchrounous
    public void CreateUserUIMS(User UserRecord){
        system.debug('entering create uims--> ') ; 
        if(!Test.isrunningTest()){
            String UserRecordJson = JSON.serialize(UserRecord);
            IDMSUIMSWebServiceCalls.enqueueJob(UserRecordJson);
        }  
    }  
    
    //Method added by Otmane
    @future
    Public static void enqueueJob(String UserRecordJson){
        User UserRecord = (User)JSON.deserialize(UserRecordJson,User.class);
        ID jobID        = System.enqueueJob(new IDMSCreateUIMSUser(UserRecord));
        system.debug('--- jobid---'+jobID );    
    }
    
    //createcompany
    public void CreateCompanyUIMS(User UserRecord){
        if(!Test.isrunningTest()){
            IDMSCreateUIMSCompany UIMSObj  = new IDMSCreateUIMSCompany(UserRecord);
            ID jobID                       = System.enqueueJob(UIMSObj );
            system.debug('--- jobid---'+jobID );
        } 
    }   
    
    //This method is used to Activate user in UIMS
    public Boolean ActivateIdentity(String password, string authToken){
        system.debug('auth token '+authToken);
        String endpoint_x = System.Label.CLJUN16IDMS149;        
        Boolean response  = unitApiService.activateIdentity(Label.CLJUN16IDMS104,password,authToken);
        return response;     
    }
    
    //This method is used to updatePasswordUIMS
    @future (callout=true)
    public static void updatePasswordUIMS(string oldPwd,String newPwd,string samlassertion){
        try{
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiService = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();    
            unitApiService.clientCertName_x                = System.Label.CLJUN16IDMS103;  
            WS_IdmsUimsUserManagerV2Service.userV5 userUpd = new WS_IdmsUimsUserManagerV2Service.userV5();
            string callerFid                               = Label.CLJUN16IDMS104;
            boolean result;
            if(!Test.isrunningTest()){
                result = unitApiService.updatePassword(callerFid,samlassertion,oldPwd,newPwd);
            }
        }
        catch(exception e){
            system.debug('---error Update---'+e); 
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','updatePassword UIMS Web Service Call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
        }
    }
    
    //This method is used to Activate identity in UIMS
    @future (callout = true)
    public static void ActivateIdentityUIMS(String password, string authToken){
        WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiService = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();    
        unitApiService.clientCertName_x = System.Label.CLJUN16IDMS103;
        String endpoint_x     = System.Label.CLJUN16IDMS149;        
        Boolean response      = unitApiService.activateIdentity(Label.CLJUN16IDMS104,password,authToken);
    }
    
    //This method is used to set password in UIMS
    @future (callout=true)
    public static void uimsSetPasswordUser(String newPassword,string authToken){
        try{
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort unitApiService = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();    
            unitApiService.clientCertName_x = System.Label.CLJUN16IDMS103;
            boolean setPasswordRes;
            if(!Test.isrunningTest()){
                setPasswordRes    = unitApiService.setPassword(Label.CLJUN16IDMS104,authToken,newPassword);
            }
        }
        catch(Exception e){
            system.debug('---error Update---'+e);
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','uimsSetPassword UIMS web Service call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
        }
    }
    
    //This method is used to create identity in uims with password 
    @future(callout=true) 
    public static void uimscreateIdentityWithPassword(String UserRecordJson,String password,String appid){
        //Deserialize UserRecordJson
        User UserRecord = (User)JSON.deserialize(UserRecordJson,User.class);
        try{
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort identity = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            WS_uimsv22ServiceImsSchneider.createdIdentityReport report                           = new WS_uimsv22ServiceImsSchneider.createdIdentityReport(); 
            WS_uimsv22ServiceImsSchneider.userV6 userV6                                          = new WS_uimsv22ServiceImsSchneider.userV6(); 
            WS_uimsv22ServiceImsSchneider.accessElement application                              = new WS_uimsv22ServiceImsSchneider.accessElement() ; 
            identity.clientCertName_x                                                            = System.Label.CLJUN16IDMS103;
            identity.endpoint_x                                                                  = label.CLQ316IDMS129;
            string callerFid                                                                     = Label.CLJUN16IDMS104;
            
            userV6.phoneId            = UserRecord.MobilePhone;
            userV6.email              = (UserRecord.email !=null && !((string)UserRecord.email).equalsignorecase('null'))?UserRecord.email:null ;
            userV6.federatedID        = (UserRecord.FederationIdentifier !=null && !((string)UserRecord.FederationIdentifier).equalsignorecase('null'))?USerRecord.FederationIdentifier:null ;
            userV6.firstName          = (UserRecord.firstname !=null && !((string)UserRecord.firstname ).equalsignorecase('null'))?UserRecord.firstname:null;
            userV6.lastName           = (UserRecord.lastname !=null && !((string)UserRecord.lastname ).equalsignorecase('null'))?UserRecord.lastname:null;
            userV6.languageCode       = (UserRecord.IDMS_PreferredLanguage__c !=null && !((string)UserRecord.IDMS_PreferredLanguage__c ).equalsignorecase('null'))?UserRecord.IDMS_PreferredLanguage__c:null;
            userV6.countryCode        = (UserRecord.Country !=null && !((string)UserRecord.Country ).equalsignorecase('null'))?UserRecord.Country:null;
            userV6.fax                = (UserRecord.fax !=null && !((string)UserRecord.fax ).equalsignorecase('null'))?UserRecord.fax:null;
            userV6.phone              = (UserRecord.phone !=null && !((string)UserRecord.phone ).equalsignorecase('null'))?UserRecord.phone:null;                
            userV6.county             = (UserRecord.IDMS_County__c  !=null && !((string)UserRecord.IDMS_County__c  ).equalsignorecase('null'))?UserRecord.IDMS_County__c:null;
            userV6.street             = (UserRecord.street !=null && !((string)UserRecord.street ).equalsignorecase('null'))?UserRecord.street:null;
            userV6.postalCode         = (UserRecord.PostalCode !=null && !((string)UserRecord.PostalCode ).equalsignorecase('null'))?UserRecord.PostalCode:null;
            userV6.postOfficeBoxCode  = (UserRecord.IDMS_POBox__c !=null && !((string)UserRecord.IDMS_POBox__c ).equalsignorecase('null'))?UserRecord.IDMS_POBox__c:null;
            userV6.cell               = (UserRecord.mobilephone !=null && !((string)UserRecord.mobilephone ).equalsignorecase('null'))?UserRecord.mobilephone :null;  
            userV6.state              = (UserRecord.state !=null && !((string)UserRecord.state ).equalsignorecase('null'))?UserRecord.state :null;                  
            userV6.addInfoAddress     = (UserRecord.IDMS_AdditionalAddress__c !=null && !((string)UserRecord.IDMS_AdditionalAddress__c ).equalsignorecase('null'))?UserRecord.IDMS_AdditionalAddress__c :null;                      
            userV6.isActive           = true;   
            application.type_x        = System.label.CLJUN16IDMS70;
            application.id            = appid; // Registration Source 
            
            if(!Test.isrunningTest()) {
                report = identity.createIdentityWithPassword(callerFid,userv6,application, password) ;
                system.debug('report: '+ report);
            }  
            
            //Send record for reconciliation
            if(String.isEmpty(report.federatedId)){
                //call future method to insert failed user
                IDMSUserReconHandlerClass.userIdentityPhoneToRecon(report.errorMessage, UserRecord.id);
            }
            
            String createCompanyRes='';
            //for company
            if(UserRecord.IDMS_User_Context__c.equalsignorecase('Work') && userRecord.CompanyName != null && userRecord.CompanyName != ''){
                string fedId                                                                                    = report.federatedId;
                WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort unitApiCompanyService = new WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort();
                unitApiCompanyService.clientCertName_x                                                          = System.Label.CLJUN16IDMS103;
                unitApiCompanyService.endpoint_x                                                                = label.CLQ316IDMS128;
                WS_IDMSUIMSCompanyService.companyV3 companyObj                                                  = new WS_IDMSUIMSCompanyService.companyV3();
                
                companyObj.OrganizationName    = UserRecord.CompanyName;
                companyObj.GoldenId            = UserRecord.IDMSCompanyGoldenId__c;
                companyObj.CountryCode         = UserRecord.Company_Country__c;
                companyObj.CurrencyCode        = UserRecord.DefaultCurrencyIsoCode;
                companyObj.CustomerClass       = UserRecord.IDMSClassLevel2__c;
                companyObj.LocalityName        = UserRecord.Company_City__c;
                companyObj.PostalCode          = UserRecord.Company_Postal_Code__c;
                companyObj.PostOfficeBox       = UserRecord.IDMSCompanyPoBox__c;
                companyObj.state               = UserRecord.Company_State__c;
                companyObj.Street              = UserRecord.Company_Address1__c;
                if(UserRecord.IDMSCompanyHeadquarters__c){
                    companyObj.HeadQuarter     = 'true';
                }else{
                    companyObj.HeadQuarter     = 'false';
                }
                companyObj.County              = UserRecord.IDMSCompanyCounty__c;
                companyObj.WebSite             = UserRecord.Company_Website__c;
                companyObj.MarketServed        = UserRecord.IDMSCompanyMarketServed__c;
                // number in idms and string in UIMS
                companyObj.businessType        = UserRecord.IDMSClassLevel1__c;    
                companyObj.EmployeeSize        = UserRecord.IDMSCompanyNbrEmployees__c;
                companyObj.AddInfoAddress      = UserRecord.Company_Address2__c;
                createCompanyRes               = unitApiCompanyService.createCompany(callerFid,fedId,companyObj);
                system.debug(createCompanyRes);
            }
            if(report != null && string.isnotblank(report.federatedID)){
                UserRecord.FederationIdentifier = report.federatedID; 
                userrecord.IDMS_VU_OLD__c       = '1';
                if(createCompanyRes != null && string.isnotblank(createCompanyRes)){
                    UserRecord.IDMSCompanyFederationIdentifier__c = createCompanyRes;
                    userrecord.IDMS_VC_OLD__c                     = '1';
                }
                system.debug('user record: '+UserRecord);
                Database.update(UserRecord,false);
            }
            //Send record for reconciliation
            if(String.isEmpty(createCompanyRes)){
                system.debug('***** Company create inside reconc response*****'+createCompanyRes);
                String responseValue = Label.CLNOV16IDMS061;
                //call future method to insert failed user
                IDMSCompanyReconHandlerClass.idmsCompanyToRecon(responseValue, UserRecord.id);
            }
        }catch(exception e ) {
            system.debug('---error create identity with password---'+e);
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','uimscreateIdentityWithPassword',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
            IDMSUserReconHandlerClass.userIdentityPhoneToRecon(ExceptionMessage, UserRecord.id);
            if(UserRecord.IDMSCompanyFederationIdentifier__c == null){
                //catch callout exception 
                System.debug('*****Inside user company check with password****'+UserRecord.IDMSCompanyFederationIdentifier__c);
                IDMSCompanyReconHandlerClass.idmsCompanyToRecon(ExceptionMessage, UserRecord.id);
            }
        }
    }
    //This method is used to set password with sms
    @future(callout = true)
    public static void setPasswordWithSMS(String PhoneId,String Password,String token){        
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort uimsService = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            uimsService.clientCertName_x                                               = System.Label.CLJUN16IDMS103;
            uimsService.endpoint_x                                                     = label.CLQ316IDMS134;
            string TokenType                                                           = 'contact activation';
            string callerFid                                                           = Label.CLJUN16IDMS104;
            boolean res                                                                = uimsService.setPasswordWithSms(callerFid,PhoneId,token,TokenType,Password);   
            system.debug(res);
        }
        catch(exception e ) {
            system.debug('---error setPasswordWithSMS---'+e);
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUIMSWebserviceCalls','setPasswordWithSMS',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
        }
    }
    
    //Update User AIL UIMS call.
    @future(callout = true)
    public static void uimsUpdateUserAILGrant(String callerFid,String federatedId, String access){
        try{
            system.debug('Update User AIL has been received by UIMS'+federatedId);
            callerFid                                                                        = Label.CLJUN16IDMS104;
            IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort uimsAILService = new IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort();
            uimsAILService.clientCertName_x                                                  = System.Label.CLJUN16IDMS103;
            IdmsUserAILuimsv2ServiceIms.accessElement accessname                             = new IdmsUserAILuimsv2ServiceIms.accessElement();
            Integer accessLength                                                             = access.length(); 
            Integer i                                                                        = access.indexOf('-');
            String accessId                                                                  = access.substring(i+1,accessLength);
            String accessType                                                                = access.substring(0,i);
            String toUpparId                                                                 = accessId.toUpperCase();
            String toUpparType                                                               = accessType.toUpperCase();
            accessname.id                                                                    = toUpparId.trim();
            accessname.type_x                                                                = toUpparType.trim();  
            boolean accessResponse;
            if(!Test.isrunningTest()) {
                accessResponse = uimsAILService.grantAccessControlToUser(Label.CLJUN16IDMS104, federatedId, accessname);
                system.debug('access response:'+accessResponse );
            }
        }catch(exception e){
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','uims Update Users AIL UIMS web Service call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
        }
    }
    
    //Update User AIL UIMS call.
    @future(callout = true)
    public static void uimsUpdateUserAILRevoke(String callerFid,String federatedId, String access){
        try{
            callerFid                                                                        = Label.CLJUN16IDMS104;
            IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort uimsAILService = new IdmsUserAILuimsv2ImplServiceIms.UserAccessManager_UIMSV2_ImplPort();
            uimsAILService.clientCertName_x                                                  = System.Label.CLJUN16IDMS103;
            IdmsUserAILuimsv2ServiceIms.accessElement accessname                             = new IdmsUserAILuimsv2ServiceIms.accessElement();
            Integer accessLength                                                             = access.length(); 
            Integer i                                                                        = access.indexOf('-');
            String accessId                                                                  = access.substring(i+1,accessLength );
            String accessType                                                                = access.substring(0,i );
            String toUpparId                                                                 = accessId.toUpperCase();
            String toUpparType                                                               = accessType.toUpperCase();
            accessname.id                                                                    = toUpparId.trim(); 
            accessname.type_x                                                                = toUpparType.trim();  
            boolean accessResponse;
            if(!Test.isrunningTest()) {
                accessResponse = uimsAILService.revokeAccessControlToUser(Label.CLJUN16IDMS104, federatedId, accessname);
                system.debug('access response:'+accessResponse );
            }
        }catch(exception e){
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','uims Update Users AIL UIMS web Service call',null,Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'','',false,'',false,null);
        }
    }
}