/*Created by: Deepak Kumar
Date:18/07/2013
For the purpose of Pre Populating Field Values of service COntract.
OCT13 Release
*/
public class VFC_ServiceContract_Prepopulate
{
PageReference NewEditPage = new PageReference('/'+SObjectType.SVMXC__Service_Contract__c.getKeyPrefix()+'/e' );
String Contact1Id;
String ContactName;
String Account1Id;
String AccountName;
String BillToAccountId;
String BillToAccountName;
String DefaultInsAtAccountId;
String DefaultInsAtAccountName;
String DefaultShipToAccountId;
String DefaultShipToAccountName;
String SoldToAccountId;
String SoldToAccountName;




 /*=======================================
      CONSTRUCTOR
    =======================================*/  


    public VFC_ServiceContract_Prepopulate(ApexPages.StandardController controller)
    {
    }

/* This method will take the controle to editpage i.e and will pre populate the Account and Contact*/

    public Pagereference gotoeditpage()
    {
        ContactName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV07); 
        Contact1Id = System.currentpagereference().getParameters().get(Label.CLOCT13SRV08);
        AccountName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV09);
        Account1Id  =System.currentpagereference().getParameters().get(Label.CLOCT13SRV10);
        
        BillToAccountName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV11);
        BillToAccountId  =System.currentpagereference().getParameters().get(Label.CLOCT13SRV12);
        
        DefaultInsAtAccountName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV15);
        DefaultInsAtAccountId  =System.currentpagereference().getParameters().get(Label.CLOCT13SRV16);
        
        DefaultShipToAccountName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV13);
        DefaultShipToAccountId  =System.currentpagereference().getParameters().get(Label.CLOCT13SRV14);
        
        SoldToAccountName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV17);
        SoldToAccountId  =System.currentpagereference().getParameters().get(Label.CLOCT13SRV18);
        

         if(Contact1Id !=Null && Contact1Id !='')
         {
          Contact con = New Contact();
          con = [SELECT Id,Contact.Name FROM Contact where Id =:Contact1Id Limit 1];
         
             if(con.Id != Null)
             {
              NewEditPage.getParameters().put(Label.CLOCT13SRV07, con.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV08,con.Id );
             }
             
            else 
            {
             System.Debug('VFC_ServiceContract_Prepopulate.gotoeditpage.WARNING - ContactId is null.');
            }
         }

         else if(Account1Id !=Null && Account1Id !='' )//|| (SoldToAccountId !=Null && SoldToAccountId !='') )
         {       
             Account acc = New Account();
             acc = [SELECT Id,Account.Name FROM Account WHERE Id =:Account1Id limit 1];
            
            if(acc.Id != Null)
             {
              NewEditPage.getParameters().put(Label.CLOCT13SRV09, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV10,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV11, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV12,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV13, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV14,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV15, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV16,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV17, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV18,acc.Id );
               
             }
                 
         }
         
         else if(SoldToAccountId !=Null && SoldToAccountId !='') 
         {       
             Account acc = New Account();
              acc = [SELECT Id,Account.Name FROM Account WHERE Id =:SoldToAccountId limit 1];
            
            if(acc.Id != Null)
             {
              NewEditPage.getParameters().put(Label.CLOCT13SRV09, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV10,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV11, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV12,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV13, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV14,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV15, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV16,acc.Id );
              
              NewEditPage.getParameters().put(Label.CLOCT13SRV17, acc.Name );
              NewEditPage.getParameters().put(Label.CLOCT13SRV18,acc.Id );
              }
        }     
              
     NewEditPage.getParameters().put('nooverride', '1'); 
     return NewEditPage;
    }
}