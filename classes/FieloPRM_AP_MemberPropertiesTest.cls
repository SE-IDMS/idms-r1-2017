@isTest
private class FieloPRM_AP_MemberPropertiesTest {
    
    
    @isTest static void test_method_one() {
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
                 
            insert member;

            Contact con = [Select id FROM contact WHERE FieloEE__Member__c = : member.id ];

            con.PRMUIMSId__c = '123';

            update con;
            

               
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'test';
            //badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_TypeSelection__c = 'bFO Properties';
            badge.F_PRM_BadgeAPIName__c = 'test';
            insert badge;
            
            PRMTransactionCatalog__c transCatalog = new PRMTransactionCatalog__c();
            transCatalog.Approved__c = true;
            transCatalog.ExternalValue__c = 100;
            transCatalog.Type__c = 'Tweet';
            insert transCatalog;
            
            Id ExternalPropCatalogRTId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName = 'BFOProperties'].Id;
            ExternalPropertiesCatalog__c propCatalog = new ExternalPropertiesCatalog__c();
            propCatalog.Active__c = true;
            propCatalog.PRMCountryFieldName__c  = 'PRMCountry__c';
            propCatalog.PropertyName__c = 'PropertyName';
            propCatalog.TableName__c = 'Contact';
            propCatalog.FieldName__c = 'PRMEmail__c';
            propCatalog.RecordTypeId = ExternalPropCatalogRTId;
            propCatalog.SubType__c = 'SubType';
            propCatalog.Badge__c = badge.id;
            propCatalog.TransactionCatalog__c = transCatalog.Id;
            insert propCatalog;

            List<FieloPRM_EventCatalog__c> listEventCat = new List<FieloPRM_EventCatalog__c>();
            Id automaticEventCatalogRid = Schema.SObjectType.FieloPRM_EventCatalog__c.getRecordTypeInfosByName().get('Automatic').getRecordTypeId();
            for (Integer i = 0; i < 3; i++) {
                FieloPRM_EventCatalog__c eventCat = new FieloPRM_EventCatalog__c();
                eventCat.F_PRM_isActive__c = true;
                eventCat.RecordTypeId = automaticEventCatalogRid;
                eventCat.F_PRM_Type__c = 'type' + i;
                eventCat.F_PRM_Value__c = i;
                eventCat.F_PRM_ValueText__c = 'value' + i;
                eventCat.F_PRM_Points__c = 100;
                listEventCat.add(eventCat);
            }
            insert listEventCat;

            List<PRMExternalPropertyEvent__c> listJunction = new List<PRMExternalPropertyEvent__c>();
            for (Integer i = 0; i < 3; i++) {
                PRMExternalPropertyEvent__c junc = new PRMExternalPropertyEvent__c();
                junc.EventCatalog__c = listEventCat[i].Id;
                junc.ExternalPropertiesCatalog__c = propCatalog.Id;
                listJunction.add(junc);
            }
            insert listJunction;

            FieloPRM_ValidBadgeTypes__c validBadgeAux = new FieloPRM_ValidBadgeTypes__c(FieloPRM_ApiExtId__c = 'test', FieloPRM_Type__c = 'BFOProperties', name = 'test' , FieloPRM_CreateEvent__c = true ,FieloPRM_CreateBadgeMember__c = true);
            insert validBadgeAux;


            test.startTest(); 




            list<MemberExternalProperties__c> listmemberProperties = new list<MemberExternalProperties__c>();
            list<MemberExternalProperties__c> listmemberPropertiesDelete = new list<MemberExternalProperties__c>();
            

            MemberExternalProperties__c memberProperties = new MemberExternalProperties__c();
            memberProperties.ExternalPropertiesCatalog__c =  propCatalog.id;
            memberProperties.Type__c =  'BFOProperties';
            memberProperties.SubType__c =  'test';
            memberProperties.PRMUIMSId__c =  '123';
            memberProperties.FunctionalKey__c = memberProperties.Type__c + memberProperties.SubType__c + memberProperties.PRMUIMSId__c;
            



            listmemberProperties.add(memberProperties);
            listmemberPropertiesDelete.add(memberProperties);
            

            MemberExternalProperties__c memberProperties1 = new MemberExternalProperties__c();
            //memberProperties1.Member__c =  member.id;
            memberProperties1.ExternalPropertiesCatalog__c =  propCatalog.id;
            memberProperties1.Type__c =  'BFOProperties';
            memberProperties1.SubType__c =  'test1';
            memberProperties1.PRMUIMSId__c =  '123';
            memberProperties1.FunctionalKey__c = memberProperties1.Type__c + memberProperties1.SubType__c + memberProperties1.PRMUIMSId__c;

            listmemberProperties.add(memberProperties1);
            

            insert listmemberProperties;    
            

            delete listmemberPropertiesDelete; 

            test.stopTest();
        }
    }
    
    
    
}