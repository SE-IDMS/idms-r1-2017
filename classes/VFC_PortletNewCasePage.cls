Public Class VFC_PortletNewCasePage{
  
     Public Case CaseObj{get;set;}
     Public Case NewCaseObj{get;set;}
     
     Public string contactName{get;set;}
     Public String accountName{get;set;}
     Public String OwnerId{get;set;}
     Public String CommercialReference{get;set;}
     Public String ProductName{get;set;}
     Public String sites{get;set;}
     public string sitesname{get;set;}
     Public String Status{get;set;} 
     Public VFC_PortletNewCasePage(ApexPages.StandardController Ctrl){
      
        ProductName=apexpages.currentpage().getparameters().get('ProductName');      
        CommercialReference =apexpages.currentpage().getparameters().get('CommercialReference');
        Sites=apexpages.currentpage().getparameters().get('Site');
        if(Sites!=null){
            sitesname = [select id,name from GCSSite__c where id=:Sites].name;
        }
        ID accid = apexpages.currentpage().getparameters().get('account'); 
        Id conid =  apexpages.currentpage().getparameters().get('Contact');
        CaseObj = new Case();
        CaseObj.contactId=conid;
        CaseObj.AccountId=accid;  
        if(!Test.IsRunningTest()){
            contactName = [select name from contact where id=:conid].Name;    
            accountName=  [select name from account where id=:accid].Name; 
        }  
        else{
          contactName = [select name from contact limit 1].Name;    
            accountName=  [select name from account limit 1].Name; 
        } 
        }
      Public PageReference SaveRec(){
      System.debug('team:'+system.label.CLOCT15CCC39);
       List<CustomerCareTeam__c>  lstCCCTeam = [select id,name from CustomerCareTeam__c where name=:system.label.CLOCT15CCC39 and Inactive__c=false limit 1];//Foxboro -L1
       CaseObj.Status ='New';
       CaseObj.origin='Portal';
       CaseObj.site__c = Sites;
       CaseObj.CommercialReference_lk__c = ProductName;
       CaseObj.LevelOfExpertise__c=system.label.CLOCT15CCC29;//'Primary';
       CaseObj.SupportCategory__c=system.label.CLOCT15CCC30;//'Troubleshooting*';
          
       if(lstCCCTeam!=null && !lstCCCTeam.isEmpty()){
         CaseObj.team__c=lstCCCTeam[0].Id;
         }
      
       CaseObj.CustomerRequest__c= CaseObj.Description;
       insert CaseObj;
       System.debug('CaseObject'+CaseObj);
       Pagereference PageRef =New PageReference('/'+CaseObj.Id);
       return PageRef;
      }
      Public Pagereference CancelRec(){
            PageReference pageResult;
            pageResult = new PageReference('/apex/VFP_PortletCasesView');
            return pageResult;
      } 
      
       Public Pagereference goBack(){
            PageReference pageResult;
            pageResult = new PageReference('/apex/VFP_AddSiteandProductForCase');
            return pageResult;
      } 
}