/************************************************************
* Developer: Waldemar Mayo                                  *
* Type: VF Component Controller                             *
* Component: Fielo_LookupToPicklist                         *
* Created Date: 11.07.2014                                  *
************************************************************/
public without sharing class Fielo_LookupToPicklistController {
    
    public String myFieldToshowParam {get;set;}
    public String myLookUpFieldParam {get;set;}
    public sObject mySObjectParam {get;set;}
    public String myWhereParam {get;set;}
    
    private Id memberId;    
    
    public List<SelectOption> getListValues(){
        String myReferenceTo;
        List<SelectOption> listValues = new List<SelectOption>();
        
        //Get the reference to Object
        for(Schema.SObjectField field: Schema.getGlobalDescribe().get(String.ValueOf(mySObjectParam.getSObjectType())).getDescribe().fields.getMap().values()){
            if(field.getDescribe().getName() == myLookUpFieldParam){
                myReferenceTo = String.valueof(field.getDescribe().getReferenceTo()[0]);
            }
        }
        
        if(myReferenceTo != null){
            //Creates the Query
            String query = 'SELECT Id, ' + myFieldToshowParam +  ' FROM ' + myReferenceTo;
            if(myWhereParam != null){
                memberId = FieloEE.memberUtil.getMemberId();
                query += ' '+myWhereParam;
            }
            List<SObject> queryResult = Database.Query(query);
            
            //Adds the Fields to the Picklist
            listValues.add(new selectOption('', '- None -')); //add the first option of '- None -' in case the user doesn't want to select a value or in case no values are returned from query below
            if(!queryResult.isEmpty()){
                for(Sobject obj : queryResult){
                    listValues.add(new selectOption(String.valueOf(obj.get('Id')), String.valueOf(obj.get(myFieldToshowParam)))); //for all records found - add them to the picklist options
                }
            }
        }
        return listValues;
    }
    
    //Gets Label of the Field
    public String getMyLabelParam(){
        return String.valueOf(Schema.getGlobalDescribe().get(String.ValueOf(mySObjectParam.getSObjectType())).getDescribe().fields.getMap().get(myLookUpFieldParam).getDescribe().getLabel());
    }
    
    public Boolean getHasMsgs(){
        return ApexPages.hasMessages();
    }
}