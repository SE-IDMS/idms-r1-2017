global class Batch_ProcessPRMRequestQueueItems implements Database.Batchable<sObject>,Database.Stateful {
     List<sObject> sObjLst = new List<sObject>();
     public String query; 
     List<PRMRequestQueue__c> lstRequestQueue = new List<PRMRequestQueue__c>();
     Set<Id> reqQueueToProcess = new Set<Id>();
     
     global Batch_ProcessPRMRequestQueueItems(Set<Id> reqBatchId){
         reqQueueToProcess = reqBatchId;
         query = 'SELECT Id, AccountId__c, ContactId__c, ProcessingStatus__c, RequestType__c FROM PRMRequestQueue__c WHERE Id IN :reqQueueToProcess';
         System.debug('>>>>:query'+query);
         System.debug('>>>>:reqQueueToProcess'+reqQueueToProcess);
     }
     
     global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext BC, List<sObject> scope) {        
        System.debug('**Scope**'+scope);
        sObjLst.addall(scope);
        List<PRMRequestQueue__c> prmReqQueueLstToUpdate = new List<PRMRequestQueue__c>();
        for(PRMRequestQueue__c prmReqQueue : (List<PRMRequestQueue__c>)sObjLst) {
            prmReqQueue.ProcessingStatus__c = 'Processed';
            prmReqQueueLstToUpdate.add(prmReqQueue);
        }
        System.debug('** PRMRequestQueueUpdate **'+prmReqQueueLstToUpdate);
        Update prmReqQueueLstToUpdate;
     }
     global void finish(Database.BatchableContext BC){
     }
}