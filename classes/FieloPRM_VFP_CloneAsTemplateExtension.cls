/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description: Page extension class for page FieloPRM_VFP_CloneAsTemplate
********************************************************************/
global with sharing class FieloPRM_VFP_CloneAsTemplateExtension{

    public String menuRecordTypeId {get;set;}
    public FieloEE__Menu__c menuStandarController {get;set;}
    public String categoryRecordTypeId {get;set;}

    public FieloPRM_VFP_CloneastemplateExtension(ApexPages.StandardController stdController){
        this.menuStandarController = (FieloEE__Menu__c)stdController.getRecord();

        String soqlMenu = 'SELECT ';
        for(String fieldName : Schema.getGlobalDescribe().get('FieloEE__Menu__c').getDescribe().fields.getMap().keySet()) {
           soqlMenu += fieldName + ', ';
        }
        soqlMenu = soqlMenu.left(soqlMenu.lastIndexOf(','));
        soqlMenu += ' FROM ' + 'FieloEE__Menu__c' + ' WHERE Id = \'' + menuStandarController.id + '\'' + ' LIMIT 1';
        List<FieloEE__Menu__c> listMenu= (List<FieloEE__Menu__c>) Database.query(soqlMenu);

        menuStandarController = listMenu[0];
        //FieloEE__Placement__c ,FieloEE__Program__c,RecordTypeId ,F_DeveloperName__c ,F_PRM_Type__c ,FieloEE__Menu__c,Name,FieloEE__visibility__c FROM FieloEE__Menu__c WHERE id =: menuStandarController.id];
        menuRecordTypeId = [SELECT id,DeveloperName FROM RecordType WHERE DeveloperName  = 'SubMenu'].id;
        categoryRecordTypeId = [SELECT id,DeveloperName FROM RecordType WHERE DeveloperName  = 'CategoryItem'].id;
    }

    public PageReference redirectCloneAsTemplate(){
              
        string idNewMenuTemplate = FieloPRM_UTILS_MenuCloneMethods.cloneAsTemplateContent(menuStandarController.id, menuStandarController.name + ' Template', menuStandarController.name,null);
        //update countrychannel;
        if(idNewMenuTemplate.length() > 18 ){
            return null;
        }

        List<FieloEE__Menu__c> listMenus = [SELECT Id, F_PRM_Type__c, FieloEE__Placement__c, FieloEE__Menu__c, FieloEE__RedemptionRule__c FROM FieloEE__Menu__c WHERE Id =: idNewMenuTemplate LIMIT 1];

        if(!listMenus.isEmpty()){
            listMenus[0].FieloEE__RedemptionRule__c = null;
            listMenus[0].F_PRM_Type__c = 'Template';
            listMenus[0].FieloEE__Placement__c = 'Template'; 
            listMenus[0].FieloEE__Menu__c = null; 
            update listMenus;
        }
        
        return new PageReference('/'+ idNewMenuTemplate );
        
    }
    
}