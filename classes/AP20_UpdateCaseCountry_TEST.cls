@isTest
private class AP20_UpdateCaseCountry_TEST{
    static testMethod void caseCountryInsertForPhoneOrigin_TestMethod(){
        
        List<User> lstUser = new List<User>();
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('TestUser');
        lstUser.add(user1);
       
        
        //Database.insert(lstUser);
        
        
        List<Country__c> lstCountry = new List<Country__c>();
        Country__c country = Utils_TestMethods.createCountry();
        lstCountry.add(country);
        //database.insert(country);
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.Name='TestCountry2'; 
        country1.CountryCode__c='IN';
        lstCountry.add(country1);
        //database.insert(country1);
        
        Country__c country2 = Utils_TestMethods.createCountry();
        country2.Name='TestCountry3'; 
        country2.CountryCode__c='ES';
        lstCountry.add(country2);
        //database.insert(country2);
        
        Country__c country3 = Utils_TestMethods.createCountry();
        country3.Name='TestCountry4'; 
        country3.CountryCode__c='GB';
        lstCountry.add(country3);
        Database.insert(lstCountry);
        
        Account accounts = Utils_TestMethods.createAccount();
        accounts.Country__c = country2.Id;
        Database.insert(accounts);
        
        List<Contact> lstContact = new List<Contact>();
        Contact contacts1=Utils_TestMethods.createContact(accounts.Id,'TestContact');
        lstContact.add(contacts1);
        //database.insert(contacts1);
        
        Contact contacts2=Utils_TestMethods.createContact(accounts.Id,'TestContact');
        contacts2.Country__c = country3.Id;
        lstContact.add(contacts2);
        
        Database.insert(lstContact);
         
        /*----------Test Starts---------------------*/
        Test.startTest();
        system.runAs(user1)
        {                     
            List<Case> lstCases = new List<Case>();
            
            Case objCase1 = Utils_TestMethods.createCase(accounts.id,contacts1.id,'In Progress');
            objCase1.Origin = 'Phone';
            objCase1.CCCountry__c=null; 
            objCase1.Description = 'objCase1 Description';
            Case objCase2 = Utils_TestMethods.createCase(accounts.id,contacts2.id,'In Progress');
            objCase2.Origin = 'Phone';
            objCase2.CCCountry__c=null; 
            objCase2.Description = 'objCase2 Description';
            
            lstCases.add(objCase1);
            lstCases.add(objCase2);
            Database.insert(lstCases);
        }
        Test.stopTest();
    }
    static testMethod void caseUpdateDueDate_TestMethod(){
        
        Id DefaultBusinessHours  = [select Id from BusinessHours where IsDefault = true][0].Id;

        Country__c country1 = Utils_TestMethods.createCountry();
        country1.Name='Default Case Business Hour'; //For BusinessHour Data 
        country1.CountryCode__c='IN';
        country1.BusinessHours__c=DefaultBusinessHours;
        database.insert(country1);

        
        User user1 = Utils_TestMethods.createStandardUser('TstCUr1');
        user1.Country__c='Default Case Business Hour';
        //database.insert(user1);
        

        
        Account accounts = Utils_TestMethods.createAccount();
        accounts.Country__c = country1.Id;
        Database.insert(accounts);
        
        Contact contacts1=Utils_TestMethods.createContact(accounts.Id,'TestContact');
        database.insert(contacts1);
        
        ServiceLevelAgreement__c sla = Utils_TestMethods.createServiceLevelAgreement(country1.Id);
        sla.PrimaryFirstAssignmentSLA__c = 2;
        sla.AdvancedFirstAssignmentSLA__c = 5;
        sla.ExpertFirstAssignmentSLA__c = 7;
        database.insert(sla);
         
        /*----------Test Starts---------------------*/
        Test.startTest();
         
        system.runAs(user1){                     
            Case createCaseRec = new case();
            list<case> newcases = new list<case>();
            
            Case createInternalCaseRec = Utils_TestMethods.createCase(accounts.id,null,'New');
            createInternalCaseRec.CCCLocation__c = country1.Id;
            newcases.add(createInternalCaseRec);
            insert createInternalCaseRec;
            createInternalCaseRec.Origin = 'Email';
            AP_Case_CaseHandler.blnUpdateTestRequired =true;
            update createInternalCaseRec;
            AP_Case_CaseHandler.blnUpdateTestRequired =false;
            AP20_UpdateCaseCountry.updateBusinessHours(newcases);
        }
        Test.stopTest();
    }
    static testMethod void webCaseCountryUpdate_TestMethod(){
        Country__c objCountryIN = Utils_TestMethods.createCountry();
        objCountryIN.Name='objCountryIN'; 
        objCountryIN.CountryCode__c='IN';
        Database.insert(objCountryIN);
        

        Case objWebCase = Utils_TestMethods.createCase(null,null,'New');
        objWebCase.Origin = Label.CL00445;
        objWebCase.SuppliedCountry__c = 'IN';
        insert objWebCase;

    }
    static testMethod void updateInternalCases_TestMethod(){
        Country__c objCountryIN = Utils_TestMethods.createCountry();
        objCountryIN.Name='objCountryIN'; 
        objCountryIN.CountryCode__c='IN';
        Database.insert(objCountryIN);
        

        Account objAccountForCase = Utils_TestMethods.createAccount();
        objAccountForCase.Country__c = objCountryIN.Id;
        objAccountForCase.ClassLevel1__c = 'IG';
        Database.insert(objAccountForCase);
        
        Contact objContactForCase=Utils_TestMethods.createContact(objAccountForCase.Id,'TestContact');
        database.insert(objContactForCase);
        
        List<Case> lstCase = new List<Case>();
        
        Case objInternalCase = Utils_TestMethods.createCase(null,null,'New');
        lstCase.add(objInternalCase);
        AP20_UpdateCaseCountry.updateInternalCases(lstCase);
    }
}