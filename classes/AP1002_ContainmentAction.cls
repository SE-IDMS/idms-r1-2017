public class AP1002_ContainmentAction{
    public static void populateContainmentActionOwner(List <ContainmentAction__c> containActionList){
    //*********************************************************************************
        // Method Name      : populateContainmentActionOwner
        // Purpose          : To populate Containment Action Owner from Accountable Organization 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 25th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1002_ContainmentAction  populateContainmentActionOwner  Started****');
        
        //Variable Declaration Section
        Map<ID,EntityStakeholder__c[]>  stakeholderMap = new Map<Id,EntityStakeholder__c[]>();
        Map<ID,String>  orgRoleMap = new Map<Id,String>();
        List<EntityStakeholder__c> esList = null;
        Id csqManager = null;
        
        for(ContainmentAction__c cntnActn : containActionList){
            if(cntnActn.AccountableOrganization__c != null)
                /** Look for ALL in map **/
                orgRoleMap.put(cntnActn.AccountableOrganization__c, Label.CL10074); 
        }
        stakeholderMap = Utils_P2PMethods.retrieveOrganizationUserDetails(orgRoleMap);
        
        for(ContainmentAction__c cntnActn : containActionList)
        {
            if(cntnActn.AccountableOrganization__c != null)
            {
                if(stakeholderMap.containsKey(cntnActn.AccountableOrganization__c))
                {
                  cntnActn.Owner__c = null;
                  esList = stakeholderMap.get(cntnActn.AccountableOrganization__c);
                  
                  /* First priority is populating the Default Action Owner 
                     Looping till a Default Action Owner is found */
                  for (EntityStakeholder__c esHolder : esList)
                  {
                    /** If Default Action Owner */
                    if (esHolder.Role__c == Label.CL10075)
                    {
                      cntnActn.Owner__c = esHolder.User__c;
                      break;
                    }
                    /** If CS & Q Manager */
                    if (esHolder.Role__c == Label.CL10056)
                    {
                      /** Storing the first CS&Q Manager **/
                      csqManager = esHolder.User__c;
                    }
                  }
                  /* If Owner__c is populated, there is a Default Action Owner 
                     Otherwise, looking for CS&Q Manager */
                  if (cntnActn.Owner__c == null)
                  {
                    /* Next priority for Owner - CS&Q Manager */
                    if (csqManager != null)
                    {
                      cntnActn.Owner__c = csqManager;
                        
                    }
                  }
                }
              }
              /** REMOVING - Last option is to populate creator of the record as Owner */              
        }
        
        System.Debug('****AP1002_ContainmentAction  populateContainmentActionOwner  Finished****');
    }
    
    public static void addContainmentOwnerToProblemShare(List <ContainmentAction__c> containActionList){
        //*********************************************************************************
        // Method Name      : addContainmentOwnerToProblemShare
        // Purpose          : To add ContainmentOwner to Problem Share Object 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 25th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        System.Debug('****AP1002_ContainmentAction addContainmentOwnerToProblemShare  Started****');
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        
        for(ContainmentAction__c cntnActn: containActionList){
            prblmShareList.add(Utils_P2PMethods.createProblemShare(cntnActn.RelatedProblem__c,cntnActn.Owner__c,Label.CL10060));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareList);
        System.Debug('****AP1002_ContainmentAction addContainmentOwnerToProblemShare  Finished****');
    }
    
    public static void deleteContainmentOwnerFromProblemShare(Map<Id,ContainmentAction__c> containActionOldMap){
        //*********************************************************************************
        // Method Name      : deleteContainmentOwnerFromProblemShare
        // Purpose          : Delete Sharing for ContainmentAction when Problem Containment Action Owner is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 26th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1002_ContainmentAction deleteContainmentOwnerFromProblemShare Started****');
        Set<Id> prblmId = new Set<id>();
        Set<Id> cntnActionOwnerId = new Set<id>();
        List<ContainmentAction__c> oldContainActionList = containActionOldMap.values();
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        Map <String, Problem__Share> prblmShareMap = new Map<String, Problem__Share>();
        
        System.Debug('##OldContainmentList:##'+ oldContainActionList );
        
        List<Problem__Share> prblmShareListForDelete = new List<Problem__Share>();
        for(ContainmentAction__c cntnActn: oldContainActionList){
            prblmId.add(cntnActn.RelatedProblem__c);
            cntnActionOwnerId.add(cntnActn.Owner__c);
        }

        prblmShareList = [Select Id, ParentId, UserOrGroupId, AccessLevel,RowCause from  Problem__Share where ParentId IN:prblmId 
                                                                                                            AND UserOrGroupId IN: cntnActionOwnerId AND RowCause=:Label.CL10060 ];
        for (Problem__Share prblmShare:prblmShareList ){
            prblmShareMap.put(String.valueOf(prblmShare.ParentId) + String.valueOf(prblmShare.UserOrGroupId) ,prblmShare);
        }
        //Delete the record from Problem Share only, if the Owner is not present in any other Containment Action
        //The Map "otherContainActionMap" will get all the other Containment Action associated with the Problem for this validation. 
        Map<String,ContainmentAction__c> otherContainActionMap= new Map<String,ContainmentAction__c>();
        for (ContainmentAction__c  otherContainAction : [Select Id, RelatedProblem__c, Owner__c from  ContainmentAction__c where RelatedProblem__c IN:prblmId 
                                                                                                        AND Id NOT IN: containActionOldMap.KeySet()]) {
            otherContainActionMap.put(String.valueOf(otherContainAction.RelatedProblem__c) + String.valueOf(otherContainAction.Owner__c),otherContainAction);
        }
        
        for(ContainmentAction__c cntnActn : oldContainActionList){
            if(prblmShareMap.containsKey(String.valueOf(cntnActn.RelatedProblem__c) + String.valueOf(cntnActn.Owner__c))){
                if(!(otherContainActionMap.containsKey(String.valueOf(cntnActn.RelatedProblem__c) + String.valueOf(cntnActn.Owner__c)))){
                    prblmShareListForDelete.add(prblmShareMap.get(String.valueOf(cntnActn.RelatedProblem__c) + String.valueOf(cntnActn.Owner__c)));
                }
            }
        }
        System.debug('##prblmShareListForDelete##:' + prblmShareListForDelete);
        if(prblmShareListForDelete.size()>0){
            Utils_P2PMethods.deletePrblmShare(prblmShareListForDelete);
        }
        System.Debug('****AP1002_ContainmentAction deleteContainmentOwnerFromProblemShare Finished****');
    }
    public static void updateContainmentOwnerToProblemShare(Map<Id,ContainmentAction__c> containActionOldMap,Map<Id,ContainmentAction__c> containActionNewMap){
        //*********************************************************************************
        // Method Name      : updateContainmentOwnerToProblemShare
        // Purpose          : To Update Sharing for Containment Action Owner when Containment Action Record is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 19th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1002_ContainmentAction updateContainmentOwnerToProblemShare Started****');
        
        deleteContainmentOwnerFromProblemShare(containActionOldMap);
         
        List<Problem__Share> prblmShareListForInsert = new List<Problem__Share>();
        List<ContainmentAction__c> ContainActionList =containActionNewMap.values();
        for(ContainmentAction__c cntnActn: ContainActionList){
            prblmShareListForInsert.add(Utils_P2PMethods.createProblemShare(cntnActn.RelatedProblem__c,cntnActn.Owner__c,Label.CL10060));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareListForInsert);
        
        System.Debug('****AP1002_ContainmentAction updateContainmentOwnerToProblemShare Finished****');
    }
    //==============================Start by Mohit on 22 June 2012 for September Release 2012
public static void validateXA( List <ContainmentAction__c> lstContainmentaction,string strTriggerEvent)
{
if(strTriggerEvent=='beforeinsert')
{
//validateCustomerContainment(  lstContainmentaction);
}
set<Id> stRelatedOrgazationId=new set<Id>();
Map<id,id> mapAOCon=new map<id,id>();
map<id,string> mapStatus=new map<id,string>();
list<EntityStakeholder__c> templistStakeholder=new list<EntityStakeholder__c> ();
Map<id,list<EntityStakeholder__c>> mapROStk=new Map<id,list<EntityStakeholder__c>>();
list<EntityStakeholder__c> lstEntityStake;

for(ContainmentAction__c varXa: lstContainmentaction)
{    
    lstEntityStake =new list<EntityStakeholder__c>();
     
    if( varXa.AccountableOrganization__c!=null)
    {
        stRelatedOrgazationId.add(varXa.AccountableOrganization__c);
        mapAOCon.put(varXa.id, varXa.AccountableOrganization__c);
        mapStatus.put(varXa.id,varXa.Status__c);
    }
    
    
}

lstEntityStake = [select id,Role__c,User__c,BusinessRiskEscalationEntity__c from EntityStakeholder__c where Role__c='Country President'  and BusinessRiskEscalationEntity__c IN:stRelatedOrgazationId and User__c!=null];
for(string str: stRelatedOrgazationId)
{templistStakeholder=new list<EntityStakeholder__c> ();
for(EntityStakeholder__c varstk: lstEntityStake)
{
if(str==varstk.BusinessRiskEscalationEntity__c)
{
 templistStakeholder.add(varstk);
}
}
mapROStk.put(str,templistStakeholder);
}
system.debug('lstContainmentaction contains-->>'+lstContainmentaction);
for(ContainmentAction__c varCA : lstContainmentaction)
{
//Start---Added by Nikhil for May Release 2013
 //If(varCA.RecordTypeId !=Label.CLMAY13I2P05)
      {
//End-----Added by Nikhil for May Release 2013
if(mapROStk.get(varCA.AccountableOrganization__c)!=null && mapROStk.get(varCA.AccountableOrganization__c).size() > 0 && varCA.AccountableOrganization__c!=null && varCA.status__c==Label.CLSEP12I2P29)
{


varCA.CountryPresident__c=mapROStk.get(varCA.AccountableOrganization__c) [0].User__c;

}
}


}


}
/*public static void validateCustomerContainment( List <ContainmentAction__c> lstContainmentaction)
{
set<Id> stProblemId=new set<Id>();
Map<id,id> mapPrbCon=new map<id,id>();
map<id,string> mapSeverity=new map<id,string>();
map<id,string> mapDecision=new map<id,string>();
for(ContainmentAction__c varXa: lstContainmentaction)
{
    if( varXa.RelatedProblem__c!=null)
    {
        stProblemId.add(varXa.RelatedProblem__c);
        mapPrbCon.put(varXa.id, varXa.RelatedProblem__c);
    }
}

for(problem__c varPrb: [select id ,LatestOSACDecision__c,Severity__c from problem__c where Id In:stProblemId])
{
if(varPrb.Severity__c !=null )
 mapSeverity.put(varPrb.id,varPrb.Severity__c );
if(varPrb.LatestOSACDecision__c  !=null )
      mapDecision.put(varPrb.id,varPrb.LatestOSACDecision__c);
}
for(ContainmentAction__c varXa: lstContainmentaction)
{
    if(varXa.recordtypeId== Label.CLSEP12I2P09 )
    {
       System.debug('@@@@1'+mapSeverity.get(varXa.RelatedProblem__c));
       System.debug('@@@@1'+label.CLSEP12I2P10);
       System.debug('@@@@2'+mapDecision.get(varXa.RelatedProblem__c) );
       System.debug('@@@@2'+label.CLSEP12I2P11);
       System.debug('@@@@2'+label.CLSEP12I2P12);
       System.debug('@@@@3'+label.CLSEP12I2P13);
        if( varXa.RelatedProblem__c!=null &&(( mapSeverity.get(varXa.RelatedProblem__c)!=label.CLSEP12I2P10 ) &&( mapDecision.get(varXa.RelatedProblem__c) ==label.CLSEP12I2P11 )))
        {
       
        }else
        { 
        varXa.addError(label.CLSEP12I2P13);
        }
        
    }
}

}*/
//==============================End by Mohit on 22 June 2012 for September Release 2012
//==============================End by Mohit on 22 June 2012 for September Release 2012

//====================================================== SUKUMAR CODE - START ==============================================================

    public static void updateAllStakeholders( map<id, ContainmentAction__c> CNANewMap){
    
        //*********************************************************************************
        // Method Name      : updateAllStakeholders
        // Purpose          : 
        // Created by       : Sukumar Salla - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For May - 2013 Release
        ///********************************************************************************/
        System.Debug('****AP1002_ContainmentAction updateAllStakeholders  Started****');
        

        Map<Id, Id> mapCNAOrg= new Map<Id,Id>(); 
        Set<Id> setAccOrgs = new Set<Id>();
        for( Id CNAId: CNANewMap.keyset())
        {   

             // We need to get the value of Accountable Org and see if it have the Related Organizations
             // To get the related Org we need to query Organization table where RelatedOrganizationtoNotifyofXA__c = Accountable Org
             // Map<AccOrg, ContainmentAction>
             // MAP <AccOrg, List<RelatedOrg>>
            ContainmentAction__c CNA = CNANewMap.get(CNAId);
            mapCNAOrg.put(CNA.Id, CNA.AccountableOrganization__c);
            setAccOrgs.add(CNA.AccountableOrganization__c);
        }
        
         System.debug('## mapCNAOrg ##'+ mapCNAOrg);
         system.debug('## setAccOrgs ##'+ setAccOrgs);
         
        List<BusinessRiskEscalationEntity__c> RelatedOrganizations = [Select id, RelatedOrganizationtoNotifyofXA__c from BusinessRiskEscalationEntity__c where RelatedOrganizationtoNotifyofXA__c in: setAccOrgs];

        Map<Id, List<BusinessRiskEscalationEntity__c>> mapAccOrgRelOrg = new map<id, List<BusinessRiskEscalationEntity__c>>();
        Set<Id> setRelOrgIds =  new Set<Id> ();
        for(BusinessRiskEscalationEntity__c RelOrg:RelatedOrganizations)
        {
            // make map of AccountableOrg and List of RelatedOrganizations
            if(mapAccOrgRelOrg.containskey(RelOrg.RelatedOrganizationtoNotifyofXA__c))
            {
                mapAccOrgRelOrg.get(RelOrg.RelatedOrganizationtoNotifyofXA__c).add(RelOrg);

            }
            else
            {
                mapAccOrgRelOrg.put(RelOrg.RelatedOrganizationtoNotifyofXA__c, new List<BusinessRiskEscalationEntity__c>());
                mapAccOrgRelOrg.get(RelOrg.RelatedOrganizationtoNotifyofXA__c).add(RelOrg);
            }

            // make a set of RelatedOrganization Ids
            setRelOrgIds.add(RelOrg.Id);
        }
        System.debug('## mapAccOrgRelOrg ##'+mapAccOrgRelOrg);
        System.debug('## mapAccOrgRelOrg.SIZE ##'+mapAccOrgRelOrg.size());

        System.debug('## setRelOrgIds ##'+ setRelOrgIds);
        
        //==============
        if(mapAccOrgRelOrg.size()>0)
        {
        map<id, List<BusinessRiskEscalationEntity__c>> mapCNARelOrgs = new map<id, List<BusinessRiskEscalationEntity__c>>();
        for(id CNAId:mapCNAOrg.keyset())
        {
            if(mapCNAOrg.containskey(CNAId))
            {
                Id AccOrgId = mapCNAOrg.get(CNAId);
                if(mapAccOrgRelOrg.containskey(AccOrgId))
                {
                    List<BusinessRiskEscalationEntity__c> lstRelOrgs = mapAccOrgRelOrg.get(AccOrgId);
                    if (lstRelOrgs.size()!=0)
                    {
                        mapCNARelOrgs.put(CNAId, lstRelOrgs);
                    }
                }
            }

        }
        
        system.debug('## mapCNAOrg ## '+ mapCNAOrg);
        system.debug('## mapCNARelOrgs ##'+ mapCNARelOrgs);
        
        //========================
        //map<ContainmentAction__c, List<EntityStakeholder__c>> mapCNAStkholders = getFilteredStkholders(mapCNAOrg, mapAccOrgRelOrg, setRelOrgIds)
        map<Id, List<EntityStakeholder__c>> mapCNAStkholders = getFilteredStkholders(mapCNARelOrgs, setRelOrgIds);
        InsertStakeholder(mapCNAStkholders);
        }
        else
        {
          List<RelatedOrganizationtoNotifyofXA__c> lstCNAStkhldrsToDelete = [select id, ContainmentAction__c from RelatedOrganizationtoNotifyofXA__c where ContainmentAction__c in: mapCNAOrg.Keyset()];
          system.debug('##lstCNAStkhldrsToDelete##'+ lstCNAStkhldrsToDelete);
          Delete lstCNAStkhldrsToDelete;   
        }
        System.Debug('****AP1002_ContainmentAction updateAllStakeholders  Finished****');
    }
    
     Public Static Map<Id, List<EntityStakeholder__c>> getFilteredStkholders(Map<Id, List<BusinessRiskEscalationEntity__c>> mapCNARelOrgs, Set<Id> setRelOrgIds){
        //*********************************************************************************
        // Method Name      : getFilteredStkholders
        // Purpose          : 
        // Created by       : Sukumar Salla - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For May - 2013 Release
        ///********************************************************************************/    
        Map<id, List<EntityStakeholder__c>> mapRelOrgIdStkhldrs = new Map<id, List<EntityStakeholder__c>>();
        Map<id, List<EntityStakeholder__c>> mapRelOrgIdFltrdStkhldrs = new Map<id, List<EntityStakeholder__c>>();
        // we need stakeholders of these relatedOrganizations who has the roles "Default Conatinmentaction owner" and "CSQ&Manager"
        // create Labels CL1 and CL2
        List <EntityStakeholder__c> lstRelOrgStkhldrs = [Select Id, BusinessRiskEscalationEntity__c, User__r.name, Role__c, User__c from EntityStakeholder__c where (BusinessRiskEscalationEntity__c in: setRelOrgIds) ];
        //AND ((Role__C=Label.CLMAY13I2P06) OR (Role__c=Label.CLMAY13I2P07))
        for(EntityStakeholder__c RelOrgstkhldr: lstRelOrgStkhldrs)
        {  
                if(mapRelOrgIdStkhldrs.containsKey(RelOrgstkhldr.BusinessRiskEscalationEntity__c))
                {
                    mapRelOrgIdStkhldrs.get(RelOrgstkhldr.BusinessRiskEscalationEntity__c).add(RelOrgstkhldr);
                }
                else
                {
                    mapRelOrgIdStkhldrs.put(RelOrgstkhldr.BusinessRiskEscalationEntity__c, new List<EntityStakeholder__c>());
                    mapRelOrgIdStkhldrs.get(RelOrgstkhldr.BusinessRiskEscalationEntity__c).add(RelOrgstkhldr);
                }

        }
        
        system.debug('##mapRelOrgIdStkhldrs##'+mapRelOrgIdStkhldrs);
        // from the map we have to check  from the list of stkhldrs for a particular relorg whether any stkhld is having the role "Default Containment Action Owner" -CLMAY13I2P07
        // if no, the check any of the stkhldr have the role assigned "CSQ&Manager" - CLMAY13I2P07
        
        Boolean roleFlag = False;
        for(Id RelOrgId: mapRelOrgIdStkhldrs.keyset())
        {

            if(mapRelOrgIdStkhldrs.containskey(RelOrgId))
            {
                List<EntityStakeholder__c> RelOrgStkhldrs = mapRelOrgIdStkhldrs.get(RelOrgId);
                system.debug('## RelOrgStkhldrs ##'+RelOrgStkhldrs );
                for(EntityStakeholder__c tempRelOrgStkhldr: RelOrgStkhldrs)
                {
                    if(tempRelOrgStkhldr.role__c==Label.CLMAY13I2P06)
                        
                    {   roleFlag=True;
                        if(mapRelOrgIdFltrdStkhldrs.containsKey(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c))
                        {
                             mapRelOrgIdFltrdStkhldrs.get(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c).add(tempRelOrgStkhldr);
                        }   
                         else
                         {
                            mapRelOrgIdFltrdStkhldrs.put(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c, new List<EntityStakeholder__c>());
                            mapRelOrgIdFltrdStkhldrs.get(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c).add(tempRelOrgStkhldr);
                        }
                    }
                    
                   

                }
                system.debug('## mapRelOrgIdFltrdStkhldrs ##'+ mapRelOrgIdFltrdStkhldrs);
                
                if(roleFlag==False)
                {
                    for(EntityStakeholder__c tempRelOrgStkhldr: RelOrgStkhldrs)
                    {
                        system.debug('### tempRelOrgStkhldr ###'+ tempRelOrgStkhldr);
                                       
                        if(tempRelOrgStkhldr.role__c==Label.CLMAY13I2P07)
                        {
                            if(mapRelOrgIdFltrdStkhldrs.containsKey(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c))
                            {
                             mapRelOrgIdFltrdStkhldrs.get(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c).add(tempRelOrgStkhldr);
                            }   
                            else
                             {
                            mapRelOrgIdFltrdStkhldrs.put(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c, new List<EntityStakeholder__c>());
                            mapRelOrgIdFltrdStkhldrs.get(tempRelOrgStkhldr.BusinessRiskEscalationEntity__c).add(tempRelOrgStkhldr);
                            }
                        }
                    }
                } 
                system.debug('##### mapRelOrgIdFltrdStkhldrs #####'+ mapRelOrgIdFltrdStkhldrs);
                if( roleFlag=True)
                {
                  roleFlag=False;
                }
            }
        }

        // Preapare a map between ContainmentAction and Stkhldrs
        map<Id, List<EntityStakeholder__c>> mapCNAStkhldrs = new map<Id, List<EntityStakeholder__c>>();
        for(Id CNAId: mapCNARelOrgs.keyset())
        {
            if(mapCNARelOrgs.containskey(CNAId))
            {
                list<BusinessRiskEscalationEntity__c> RelOrgs = mapCNARelOrgs.get(CNAId);
                for(BusinessRiskEscalationEntity__c tempRelOrg: RelOrgs)
                {
                    if(mapRelOrgIdFltrdStkhldrs.containskey(tempRelOrg.id))
                    {
                        List<EntityStakeholder__c> RelOrgstkhldrs = mapRelOrgIdFltrdStkhldrs.get(tempRelOrg.id);
                        for(EntityStakeholder__c stkhldr: RelOrgstkhldrs)
                        {
                        if(mapCNAStkhldrs.containskey(CNAId))
                        {
                            mapCNAStkhldrs.get(CNAId).add(stkhldr);
                        }
                        else
                        {
                            mapCNAStkhldrs.put(CNAId, new List<EntityStakeholder__c>());
                            mapCNAStkhldrs.get(CNAId).add(stkhldr);
                        }
                        }
                    }
                }
            }
        }
        system.debug('## mapCNAStkhldrs ##'+ mapCNAStkhldrs);
        return mapCNAStkhldrs;
 
    }
    
    Public static void InsertStakeholder(map<Id, List<EntityStakeholder__c>> mapCNAStkholders){

        //*********************************************************************************
        // Method Name      : InsertStakeholder
        // Purpose          : 
        // Created by       : Sukumar Salla - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For May - 2013 Release
        ///********************************************************************************/

        // Insert the stakeholders names into the containment action related List RelatedOrganizationtoNotifyofXA__c

        List<RelatedOrganizationtoNotifyofXA__c> lstRelOrgToNotifyOfXA = new List<RelatedOrganizationtoNotifyofXA__c>();
        
        
        List<RelatedOrganizationtoNotifyofXA__c> lstCNAStkhldrsToDelete = [select id, ContainmentAction__c from RelatedOrganizationtoNotifyofXA__c where ContainmentAction__c in: mapCNAStkholders.keyset()];
        system.debug('##lstCNAStkhldrsToDelete##'+ lstCNAStkhldrsToDelete);
        Delete lstCNAStkhldrsToDelete;       

        for(Id CNAId: mapCNAStkholders.keyset())
        {
            List<EntityStakeholder__c> stkhldrs = mapCNAStkholders.get(CNAId);
            
            system.debug('stkhldrs'+ stkhldrs);

            for(EntityStakeholder__c stkhldr: stkhldrs)
            {
                RelatedOrganizationtoNotifyofXA__c objRelOrgToNotifyOfXA = new RelatedOrganizationtoNotifyofXA__c();
                 system.debug(' ## stkhldr ##'+ stkhldr);
                 system.debug(' ## stkhldr.user ##'+ stkhldr.user__c);
                objRelOrgToNotifyOfXA .ContainmentAction__c = CNAId;
                objRelOrgToNotifyOfXA .User__c = stkhldr.user__c;
                lstRelOrgToNotifyOfXA.add(objRelOrgToNotifyOfXA );
                system.debug('##### lstRelOrgToNotifyOfXA #### '+ lstRelOrgToNotifyOfXA);
            }
            //lstRelOrgToNotifyOfXA.add(tmplstRelOrgToNotifyOfXA);
        }
        
        system.debug('## lstRelOrgToNotifyOfXA ## '+ lstRelOrgToNotifyOfXA);
        Insert lstRelOrgToNotifyOfXA; 
    }
    
    // ============= Method added by Uttara for OCT 2015 Release - Open XA - Alert ===========================================
    
    public static void createAlertForXA_Stackholders(set<ID> setXAIdsToProcess) {
        
        List<ContainmentAction__c> listXA = new List<ContainmentAction__c>();
        
        // To send Alert after 30 days of XA being Open and every week there after (7 days)
        Map<Id,Id> MapOrgXA = new Map<Id,Id>(); 
        Map<Id,Id> MapUsersXA = new Map<Id,Id>(); 
        
        for(ContainmentAction__c varXA : [SELECT Id, Owner__c, AccountableOrganization__c, TECH_ReminderNoUpdateDate__c FROM ContainmentAction__c WHERE Id IN: setXAIdsToProcess]) {
            
                varXA.TECH_ReminderNoUpdateDate__c = System.now();
                listXA.add(varXA);
                MapOrgXA.put(varXA.AccountableOrganization__c, varXA.Id); 
                MapUsersXA.put(varXA.Owner__c, varXA.Id);
        }
        
        // To send Alert after 30 days of XA being Open and every week there after (7 days) 
        DATABASE.update(listXA,false); // TECH_ReminderNoUpdateDate field value updated to Today
        
        for(EntityStakeholder__c varES : [SELECT BusinessRiskEscalationEntity__c, Role__c, User__c FROM EntityStakeholder__c WHERE BusinessRiskEscalationEntity__c IN: MapOrgXA.keyset() AND Role__c =: Label.CL10056]) {  //CL10056 = 'CS&Q Manager'
        
            MapUsersXA.put(varES.User__c, MapOrgXA.get(varES.BusinessRiskEscalationEntity__c));
        }
        
        List<RelatedOrganizationtoNotifyofXA__c> listROXA = new List<RelatedOrganizationtoNotifyofXA__c>();
        RelatedOrganizationtoNotifyofXA__c ROXA;
        
        for(Id varId : MapUsersXA.keySet()) {
            ROXA = new RelatedOrganizationtoNotifyofXA__c();
            ROXA.User__c = varId;
            ROXA.ContainmentAction__c = MapUsersXA.get(varId);
            ROXA.Check__c = 'batch';
            listROXA.add(ROXA);
        }
        
        Database.INSERT(listROXA,false);
    }    
       
}