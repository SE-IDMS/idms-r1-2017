/*
    Author          : Hari Krishna Singara 
    Date Created    : 22/11/2012
    Description     : Test class for the apex class AP_SharingAccessToCBATeam.
    
*/

@isTest

private class AP_SharingAccessToCBATeam_TEST {
    static testMethod void GrantAcessToQteLink_TestMethod()
    {
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        
        Opportunity opportunity= Utils_TestMethods.createOpenOpportunity(accounts.id);
        Database.insert(opportunity);                      
              
        OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        Database.insert(newQuoteLink);
        
        user newUser=Utils_TestMethods.createStandardUser('UTtest');
        Database.insert(newUser);
        
        list<CBATeam__c > newCba= new list<CBATeam__c >();
        for(integer i=0;i<200;i++)
        {
        CBATeam__c  sme = Utils_TestMethods.CreateCBA(newQuoteLink.id);        
         newCba.add(sme);
        }
        Database.insert(newCba);           
                         
        list<CBATeam__c> cbaList= new list<CBATeam__c>();
        user newUser2=Utils_TestMethods.createStandardUser('UT2test');
        Database.insert(newUser2);
        for(CBATeam__c cbaUpdt:newCba)
        {
         cbaUpdt.CBAMember__c=newUser2.id;
         cbaList.add(cbaUpdt);
        }
        Database.Update(cbaList);   
             
        list<CBATeam__c > deletecba= new list<CBATeam__c >();
        for(integer i=0;i<200;i++)
        {
            CBATeam__c cbaNew = Utils_TestMethods.createCBA(newQuoteLink.id); 
            deletecba.add(cbaNew );
        }
        database.insert(deletecba); 
        delete   deletecba;
        
        
    }

}