global class VFC_PRMProgressiveProfile {

    global class ProgressiveProfile{
        global AP_ProgressiveProfileCfgInfo ProfileForm;
        global Map<String,List<AP_PRMUtils.PicklistEntry>> SelectOptionsMap;
        global Map<String,String> ConfigList;
        global Map<String, Object> Contdependentdata;
        global String selectLabel {set; get{return System.Label.CLAPR15PRM178;}}
        global ProgressiveProfile(){
            ProfileForm = new AP_ProgressiveProfileCfgInfo();
            SelectOptionsMap = new Map<String,List<AP_PRMUtils.PicklistEntry>> ();
            ConfigList = new Map<String,String> ();
            Contdependentdata = new Map<String, Object>();
        }
    }  
    global transient string UserLanguage {get;set;}
    public transient String ProfileFormWrapper { get; private set; }
    public String sectionPlacement { get; set; }
    public String componentId {
        get;
        set { 
            componentId = value;
            this.getData();
        }
    }
    public Integer quantity{get{if(quantity == null) quantity = 1; return quantity;}set;}
    public transient List<PRMProfileConfigInfo__c> ProfileFormInfo { get; private set; }
    public transient Map<String, CS_PRM_ProgressiveProfileConfig__c> tpeMap { get; set;}

    public Boolean ServeMinified { 
        get {
            CS_PRM_ApexJobSettings__c tSetting = CS_PRM_ApexJobSettings__c.getInstance ('SERVE_MINIFIED');
            if (tSetting == null) return false;
            else return tSetting.InitialSync__c;
        }
        private set;
    }

    public VFC_PRMProgressiveProfile() {
        this.tpeMap = new Map<String, CS_PRM_ProgressiveProfileConfig__c>();
        this.ProfileFormWrapper = '';
    }
    public String getData() {
        ProgressiveProfile prof = new ProgressiveProfile();
        Map<String, Object> mapDependentData = new Map<String, Object>();
        try {
            Map<String, Object> srlzMap = new Map<String, Object>();
            Map<String,String> configStr = new Map<String,String> ();
            Map<String,List<AP_PRMUtils.PicklistEntry>> mapSelectOptions = new Map<String, List<AP_PRMUtils.PicklistEntry>>();
            system.debug('**** componentId'+componentId);
            ComponentProfileMap__c comProfMap = [SELECT Id,PRMProfileConfig__c FROM ComponentProfileMap__c WHERE Active__c = TRUE AND Component__c = :this.componentId];
            List<PRMProfileConfigInfo__c> ppci = new List<PRMProfileConfigInfo__c>([SELECT Id, FieldAPIName__c, ObjectAPIName__c, Mandatory__c FROM PRMProfileConfigInfo__c WHERE PRMProfileConfig__c = :comProfMap.PRMProfileConfig__c]);
            this.ProfileFormInfo = ppci;
            System.debug('ProfileFormInfo:' + this.ProfileFormInfo);
            String qryFields = '';
            for (PRMProfileConfigInfo__c pci : ppci) {
                if (String.isNotBlank(pci.FieldAPIName__c)) {

                    CS_PRM_ProgressiveProfileConfig__c cp = CS_PRM_ProgressiveProfileConfig__c.getInstance (pci.FieldAPIName__c);
                    configStr.put(pci.FieldAPIName__c,cp.FieldDataType__c);
                    if (cp != null && 'Account'.equalsIgnoreCase(cp.MappedObject__c)) {
                        tpeMap.put(pci.FieldAPIName__c,cp);
                        qryFields += 'Contact.' + cp.MappedObject__c + '.' + cp.MappedField__c+',';
                    }
                    else if (cp != null && 'Contact'.equalsIgnoreCase(cp.MappedObject__c)) {
                        tpeMap.put(pci.FieldAPIName__c,cp);
                        qryFields += cp.MappedObject__c + '.' + cp.MappedField__c+',';
                        system.debug('qryFields:' + qryFields);
                    }
                }
            }
            System.debug('tpeMap:' + tpeMap);
            String qryStr = 'SELECT Id,FederationIdentifier,Contact.Account.PRMCountry__r.Name,ContactId,Contact.AccountId,Contact.Account.PRMUIMSId__c,LanguageLocaleKey,Contact.FieloEE__Member__c, Contact.FieloEE__Member__r.F_PRM_PrimaryChannel__c, Contact.FieloEE__Member__r.F_PRM_Country__c ';
            if (String.isNotBlank(qryFields)){
                if(qryFields.indexOf('Contact.Account.PRMCountry__c') == -1)
                    qryFields += 'Contact.Account.PRMCountry__c,';
                qryStr = qryStr + ',' + qryFields.removeEnd(',');
            }

            qryStr = qryStr + ' FROM User WHERE Id = \'' + UserInfo.getUserId() + '\'';
            system.debug('qryStr:' + qryStr);
            User sObj = (User) Database.query(qryStr);
            System.debug('usr:'+sObj);
            for (PRMProfileConfigInfo__c pci : ppci) {
                if (sObj != null){
                    UserLanguage = sObj.LanguageLocaleKey;
                    if(String.isBlank(UserLanguage))
                        UserLanguage = 'en_US';
                    if('Account'.equalsIgnoreCase(tpeMap.get(pci.FieldAPIName__c).MappedObject__c))
                        srlzMap.put(pci.FieldAPIName__c,sObj.getSobject('Contact').getSobject('Account').get(tpeMap.get(pci.FieldAPIName__c).MappedField__c));
                    else if('Contact'.equalsIgnoreCase(tpeMap.get(pci.FieldAPIName__c).MappedObject__c))
                        srlzMap.put(pci.FieldAPIName__c,sObj.getSObject('Contact').get(tpeMap.get(pci.FieldAPIName__c).MappedField__c));

                    if(tpeMap.get(pci.FieldAPIName__c).FieldDataType__c.containsIgnoreCase('Picklist')){
                        String type = tpeMap.get(pci.FieldAPIName__c).MappedObject__c == 'Account'?'PRMAccount':'Customer';
                        List<AP_PRMUtils.PicklistEntry> optionList = AP_PRMUtils.getSelectOptions(tpeMap.get(pci.FieldAPIName__c).MappedObject__c, type, tpeMap.get(pci.FieldAPIName__c).MappedField__c, UserLanguage);
                        mapSelectOptions.put(pci.FieldAPIName__c,optionList);
                    }
                    srlzMap.put('accountId',sObj.Contact.AccountId);
                    srlzMap.put('contactId', sObj.ContactId);
                    srlzMap.put('companyFederatedId',sObj.Contact.Account.PRMUIMSId__c);
                    srlzMap.put('federationId',sObj.FederationIdentifier);
                    srlzMap.put('companyCountry',sObj.Contact.Account.PRMCountry__r.Name);
                    srlzMap.put('companyCountryId',sObj.Contact.Account.PRMCountry__c);
                    srlzMap.put('memberId',sObj.Contact.FieloEE__Member__c);
                }
                srlzMap.put(pci.FieldAPIName__c + 'Required', pci.Mandatory__c);

            }
            if(String.isNotBlank(sObj.Contact.Account.PRMCountry__c))
                mapDependentData = getDependentPickList(UserLanguage,sObj.Contact.Account.PRMCountry__c);
            
            String srlzString = JSON.serialize(srlzMap);
            System.debug('srlzString:' + srlzString);
            AP_ProgressiveProfileCfgInfo ProfileForm = (AP_ProgressiveProfileCfgInfo)JSON.deserialize(srlzString, AP_ProgressiveProfileCfgInfo.Class);
            prof.ProfileForm = ProfileForm;
            prof.SelectOptionsMap =mapSelectOptions;
            prof.ConfigList = configStr;
            prof.Contdependentdata = mapDependentData;
        }
        catch (Exception e) {
            System.debug(' ******* An exception occurred in the process of retriving the Fields and processing them ********' + e.getMessage() + e.getStackTraceString());
        }
        this.ProfileFormWrapper = JSON.serialize(prof);
        System.debug('ProfileFormWrapper:' + ProfileFormWrapper);
        return ProfileFormWrapper;
    }
    @RemoteAction
    global static UserRegResult updateProgProfile(AP_ProgressiveProfileCfgInfo prgProfile,Map<String,String> configList,Id memberId){
        UserRegResult result = new UserRegResult();
        AP_UserRegModel userRegModel = new AP_UserRegModel();
        String userInfo = JSON.serialize(userRegModel);
        String profInfo = JSON.serialize(prgProfile);
        Integer currCount = Limits.getQueueableJobs();
        Integer maxCount = Limits.getLimitQueueableJobs();
        Map<Id,Contact> ContactList = new Map<Id,Contact> ();
        Map<String,Object> profMap = (Map<String,Object>)JSON.deserializeUntyped(profInfo);
        Map<String,Object> userRegMap = (Map<String,Object>)JSON.deserializeUntyped(userInfo);
        System.debug('ProfMap:' + profMap);
        
        Account acc = new Account(Id = prgProfile.accountId);
        Contact con = new Contact(Id = prgProfile.contactId);
        userRegMap.put('companyFederatedId',prgProfile.companyFederatedId);
        userRegMap.put('federationId',prgProfile.federationId);
        userRegMap.put('accountId',prgProfile.accountId);
        for(String s :configList.keySet()){
            System.debug('Field:' + s);
            CS_PRM_ProgressiveProfileConfig__c cfg = CS_PRM_ProgressiveProfileConfig__c.getInstance(s);
            System.debug('value:' + profMap.get(cfg.Name));
            if(cfg.MappedObject__c == 'Account'){
                acc.put(cfg.MappedField__c,profMap.get(cfg.Name));
            }
            else if(cfg.MappedObject__c == 'Contact'){
                con.put(cfg.MappedField__c,profMap.get(cfg.Name));
            }
            userRegMap.put(cfg.Name,profMap.get(cfg.Name));
        }
        System.debug('Account:' + acc);
        System.debug('Contact:' + con);
        System.debug('userRegMap:' + userRegMap);
        ContactList.put(prgProfile.contactId,con);
        try{
            userInfo = JSON.serialize(userRegMap);
            userRegModel = (AP_UserRegModel)JSON.deserialize(userInfo,AP_UserRegModel.class);
            result.Success = AP_PRMIMSBOService.UpdateAccountInUIMS_AdminSync(userRegModel,true);
            System.debug('UpdateAccountInUIMS_AdminSync:'+result.Success);
            result.Success = AP_PRMIMSBOService.UpdateContactInUIMS_AdminSync(userRegModel,true);
            System.debug('UpdateContactInUIMS_AdminSync:'+result.Success);
            PRM_FieldChangeListener.enableForAccount = false;
            PRM_FieldChangeListener.enableForContact = false;
            update acc;
            update con;
            List<String> rules = FieloEE.RedemptionUtil.lookForMatchingRedemptionRules(new FieloEE__Member__c(Id = memberId));
            result.AccountId = prgProfile.accountId;
            result.ContactId = prgProfile.contactId;
            result.StatusCode = String.join(rules, '%2C');
            result.Success = true;
        }
        catch(exception e){
            result.ErrorMsg = e.getMessage() + e.getStackTraceString();
            System.debug('Update Failed:' + e.getMessage() + e.getStackTraceString());
        }
        finally{
            if(currCount < maxCount && !Test.isRunningTest()){
                ID jobID = System.enqueueJob (new AP_ProcessPRMRegistration(ContactList, AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES));
                System.debug('* job ID***' + jobID);    
            }
        }
        return result;
    }
    global static Map<String, Object> getDependentPickList(String language,Id Country) {
        Map<String, Object> mRefData = new Map<String, Object>();
        List<Country__c> lCountries = [SELECT Id, Name, CountryCode__c,StateProvinceRequired__c,ZipcodeOptional__c, (SELECT Id,Name,StateProvinceCode__c FROM StatesProvinces__r) FROM Country__c WHERE Id = :Country];
        
        mRefData.put('CountryList', lCountries);
        List<CountryStateLanguageTranslation__c> lCountryStateLanguage = [SELECT Id, Country__c, LanguageCode__c, LanguageText__c, StateProvinces__c FROM CountryStateLanguageTranslation__c WHERE LanguageCode__c=:language ORDER BY LanguageText__c];
        mRefData.put('CountryStateLanguageList',lCountryStateLanguage);
        return mRefData;
    }
}