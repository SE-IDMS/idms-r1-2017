/* 
Batch Job to caluculate and update the SumOfAllOpportunities__c and TotalProjectAmountForecastedWon__c 
for the Master Projects whose Tech_AmoutUpdateByBatch__c=TRUE.
We have to take entire hierarchy opportunities either old model data 3 levels(Master-Object-Simple) or
new model data 2 levels (Object - simple) under master project.
Monalisa - OCT 2015 */

/*
Added Customer Coverage batch in the finish method. As we have reached the Scheduling limit,
hence including Q3 2016 job in the existing daily jobs
*/

global class Batch_UpdateMasterProjectAmount implements Database.Batchable<sObject>,Schedulable
{
    //Start method
    global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id,TotalProjectAmountForecastedWon__c,SumOfAllOpportunities__c,currencyIsocode,Tech_AmoutUpdateByBatch__c,Tech_SumOfOpenOpportunities__c,Tech_SumOfCancelledOpportunities__c,Tech_SumOfLostOpportunities__c,Tech_SumOfWonOpportunities__c From OPP_Project__c where Tech_AmoutUpdateByBatch__c=TRUE]);
    }
    //Execute Method
    global void execute(Database.BatchableContext BC, List<OPP_Project__c> masterProjects)
    {
        Map<String,Decimal> currencyMap = new Map<String,Decimal>();
        Double sumOfAllOpportunities=0;
        Double totalProjectAmountForecastedOpenWon=0;
        Double sumOfOpenOpportunities=0;
        Double sumOfCancelledOpportunities=0;
        Double sumOfLostOpportunities=0;
        Double sumOfWonOpportunities=0;
        Map<Id,List<Opportunity>> prjOpplstMap = new Map<Id,List<Opportunity>>();
        Map<Id,OPP_Project__c> projectMap= new Map<Id,OPP_Project__c>();
        List<Opportunity> opplstlevel1= new List<Opportunity>();
        List<Opportunity> opplstlevel2= new List<Opportunity>();
        List<Opportunity> opplstlevel3= new List<Opportunity>();
        List<Opportunity> opportunityList=new List<Opportunity>();
        List<OPP_Project__c> updateprojectList= new List<OPP_Project__c>();
        
        //Fetch currecy conversion rate
        for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
            currencyMap.put(ct.IsoCode,ct.ConversionRate);      
       
       //Map of Project Id and Project Details
        for(Opp_Project__c prj:masterProjects)
              projectMap.put(prj.Id,prj);
              
         //First Level Opportunities
        opplstlevel1=[select Amount,currencyIsocode,ForecastCategoryName,IncludedInForecast__c,ToBeDeleted__c,StageName,Project__c,Status__c  from Opportunity where Project__c=:masterProjects];      

        //Second level Opportunities      
        opplstlevel2=[select Amount,currencyIsocode,ForecastCategoryName,IncludedInForecast__c,ToBeDeleted__c,StageName,ParentOpportunity__r.Project__c,Status__c  from Opportunity where ParentOpportunity__c in :opplstlevel1 ];

        //Third level Opportunities      
        opplstlevel3=[select Amount,currencyIsocode,ForecastCategoryName,IncludedInForecast__c,ToBeDeleted__c,StageName,ParentOpportunity__r.ParentOpportunity__r.Project__c,Status__c  from Opportunity where ParentOpportunity__c in :opplstlevel2 and IncludedInForecast__c=:System.Label.CLOCT15SLS07 and ToBeDeleted__c=False];
         
        if(opplstlevel1.size()>0){
            for(Opportunity opp:opplstlevel1){
                if(opp.IncludedInForecast__c==System.Label.CLOCT15SLS07 && opp.ToBeDeleted__c==False){
                    if(prjOpplstMap.containsKey(opp.Project__c))
                        prjOpplstMap.get(opp.Project__c).add(opp);
                    else
                        prjOpplstMap.put(opp.Project__c,new List<Opportunity>{opp}); 
                }
            }
        }
       
        if(opplstlevel2.size()>0){
            for(Opportunity opp:opplstlevel2){
                if(opp.IncludedInForecast__c==System.Label.CLOCT15SLS07 && opp.ToBeDeleted__c==False){ 
                    if(prjOpplstMap.containsKey(opp.ParentOpportunity__r.Project__c))
                        prjOpplstMap.get(opp.ParentOpportunity__r.Project__c).add(opp);
                    else
                        prjOpplstMap.put(opp.ParentOpportunity__r.Project__c,new List<Opportunity>{opp}); 
               }
            }
        } 
              
        if(opplstlevel3.size()>0){
            for(Opportunity opp:opplstlevel3){
                if(prjOpplstMap.containsKey(opp.ParentOpportunity__r.ParentOpportunity__r.Project__c))
                    prjOpplstMap.get(opp.ParentOpportunity__r.ParentOpportunity__r.Project__c).add(opp);
                else
                    prjOpplstMap.put(opp.ParentOpportunity__r.ParentOpportunity__r.Project__c,new List<Opportunity>{opp}); 
            }
        }
       
       for(OPP_Project__c prj:masterProjects){
           sumOfAllOpportunities=0;
           totalProjectAmountForecastedOpenWon=0;
           sumOfOpenOpportunities=0;
           sumOfCancelledOpportunities=0;
           sumOfLostOpportunities=0;
           sumOfWonOpportunities=0;
           opportunityList=prjOpplstMap.get(prj.Id);
           
           if(opportunityList!=null && opportunityList.size()>0){
              for(Opportunity opp:opportunityList){
                  if(opp.Amount == null)
                      opp.Amount =0;
                  sumOfAllOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
                  if(opp.StageName!=System.Label.CLOCT15SLS77)
                      totalProjectAmountForecastedOpenWon+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
                  if(opp.StageName==System.Label.CLOCT15SLS77 && (opp.Status__c==System.Label.CLOCT13SLS08 || opp.Status__c==System.Label.CLOCT13SLS09))
                      sumOfCancelledOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
                  if(opp.StageName==System.Label.CLOCT15SLS77 && (opp.Status__c==System.Label.CLSEP12SLS13 || opp.Status__c==System.Label.CLSEP12SLS14))
                      sumOfLostOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
                  if(opp.StageName==System.Label.CL00220)
                      sumOfWonOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
                  if(opp.StageName!=System.Label.CL00220 && opp.StageName!=System.Label.CLOCT15SLS77)
                      sumOfOpenOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
                  }
            }
            OPP_Project__c project=new OPP_Project__c(Id=prj.Id);
            project.SumOfAllOpportunities__c=sumOfAllOpportunities*currencyMap.get(projectMap.get(prj.Id).CurrencyIsoCode);
            project.TotalProjectAmountForecastedWon__c=totalProjectAmountForecastedOpenWon*currencyMap.get(projectMap.get(prj.Id).CurrencyIsoCode);
            project.Tech_SumOfOpenOpportunities__c=sumOfOpenOpportunities*currencyMap.get(projectMap.get(prj.Id).CurrencyIsoCode);
            project.Tech_SumOfCancelledOpportunities__c=sumOfCancelledOpportunities*currencyMap.get(projectMap.get(prj.Id).CurrencyIsoCode);
            project.Tech_SumOfLostOpportunities__c=sumOfLostOpportunities*currencyMap.get(projectMap.get(prj.Id).CurrencyIsoCode);
            project.Tech_SumOfWonOpportunities__c =sumOfWonOpportunities*currencyMap.get(projectMap.get(prj.Id).CurrencyIsoCode);
            project.Tech_AmoutUpdateByBatch__c=False;
            updateprojectList.add(project);
       }

       if(updateprojectList.size()>0)
           Database.update(updateprojectList,false);
       
    }
    
     global void execute(SchedulableContext sc)
    {
        Batch_UpdateMasterProjectAmount prjUpdates = new Batch_UpdateMasterProjectAmount(); 
        database.executebatch(prjUpdates,Integer.ValueOf(Label.CLOCT15SLS78));
    }
    //Finish Method
      global void finish(Database.BatchableContext BC)
    {
        System.debug('Schedule daily job for Customer Coverage to ensure Account information is in sync with AMP');
        Batch_UpdateAccountFromAMP ampBatch = new Batch_UpdateAccountFromAMP();
        Database.executeBatch(ampBatch,Integer.ValueOf(Label.CLQ316SLS009));
    }

}