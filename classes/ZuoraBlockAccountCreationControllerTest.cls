@isTest (SeeAllData=true)
public with sharing class ZuoraBlockAccountCreationControllerTest {
  private static testMethod void ZuoraBlockAccountCreationControllerTestMethod() {

    Country__c country = null;
    List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
    if(countries.size()==1) {
       country = countries[0];
    }
    else {
       country = new Country__c(Name='France', CountryCode__c='FR');
       insert country;
    }
	    
	Account acc = Utils_TestMethods.createAccount();
	acc.Country__c = country.Id;
	insert acc;
	       
	Contact con = Utils_TestMethods.createContact(acc.Id, 'Contact');
	con.Country__c = country.Id;
	con.PRMUIMSID__c = 'Federated Id';
	insert con;

	Zuora__CustomerAccount__c ba = testObjectCreator.CreateBillingAccount('BillAcc_1', 'Con_1', acc.Id);
	ba.Zuora__Zuora_Id__c = 'BA Zuora Id';
	update ba;

	ApexPages.currentPage().getParameters().put('billingAccountId','BA Zuora Id');
	ApexPages.currentPage().getParameters().put('crmAccountId', acc.Id);

    ZuoraBlockAccountCreationController zbacc = new ZuoraBlockAccountCreationController(new ApexPages.StandardController(new zqu__Quote__c()));
        zbacc.onload();
        zbacc.navigateBack();
        ZuoraBlockAccountCreationController.appendMessage(ApexPages.Severity.ERROR, 'You must create the billing account first in the account screen or select existing ones.');
  }
}