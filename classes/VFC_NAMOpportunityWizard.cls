/*
Controller for the VFP_NAMOpportunityWizard 
*/
global class VFC_NAMOpportunityWizard {
    public Opportunity opportunityRecord{get;set;}{opportunityRecord=new Opportunity();}
    public List<Opportunity> results{get;set;}{results=new List<Opportunity>();}
    public boolean disableCreateOpportunitybutton{get;set;}{disableCreateOpportunitybutton=true;}
    public String opptyName{get;set;}
    public boolean searchStep{get;set;}{searchStep=true;} 
    public Id countryRecord{get;set;}
    public String OpptyType{get;set;}
    public List<OPP_ValueChainPlayers__c> vcpLst{get;set;}{vcpLst=new List<OPP_ValueChainPlayers__c>();}
    public OPP_ValueChainPlayers__c vcp1{get;set;}{vcp1=new OPP_ValueChainPlayers__c();}
    public OPP_ValueChainPlayers__c vcp2{get;set;}{vcp2=new OPP_ValueChainPlayers__c();}
    public OPP_ValueChainPlayers__c vcp3{get;set;}{vcp3=new OPP_ValueChainPlayers__c();}
    public OPP_ValueChainPlayers__c vcp4{get;set;}{vcp4=new OPP_ValueChainPlayers__c();}
    public OPP_ValueChainPlayers__c vcp5{get;set;}{vcp5=new OPP_ValueChainPlayers__c();}
    public Opportunity opp{get;set;}{opp=new Opportunity();}
    public OpportunityTeamMember member{get;set;}{member=new OpportunityTeamMember();}
    public boolean creatStep{get;set;}{creatStep=false;} 
    public User currentUser;
    public boolean errormsg{get;set;}{errormsg=false;}
    public static list<ResultSet> searchedRecord {get;set;}
    public String sortDirection{get;set;} {sortDirection = 'ASC';}
    public String sortExp = 'name';
    public String sortExpression
    {get{return sortExp;}
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
       sortExp = value;
     }
    }
    
    //Constructor
    public VFC_NAMOpportunityWizard(ApexPages.StandardController controller){
        //Default country to USA record.
        countryRecord=[select id from Country__c where CountryCode__c=:System.Label.CLQ416SLS012 limit 1].id;
        opportunityRecord.CountryOfDestination__c=countryRecord;
        //populate value incase you have started from Account field
        if(System.currentPageReference().getParameters().get('accid')!=null)
            opportunityRecord.AccountId=System.currentPageReference().getParameters().get('accid');
        opportunityRecord.QuoteSubmissionDate__c=System.Today();
        disableCreateOpportunitybutton=true;
    }
    
    public void initialCheck(){
        //User can access wizard from Account field or from tab instead of from VFC_OpportunitySearch in that case we need to ensure they have access
        List<AggregateResult> cntList=[SELECT count(Id) cnt FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name =:System.Label.CLQ416SLS013 ];
        currentUser=[select country,UserBusinessUnit__c  from User where Id=:UserInfo.getUserId()];
        System.debug('>>>onload>>>>'+cntList+'>>>>>'+currentUser);
        if((integer)cntList[0].get('cnt') == 0 ||  (currentUser.country !=System.Label.CLQ416SLS012 && currentUser.country !=System.Label.CLQ416SLS013)){
           Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,System.label.CLQ416SLS015));
           errormsg=true;
        }
    }
    
    public PageReference createOpportunity()
    {
        //Default values for the create page
        if(opportunityRecord.QuoteSubmissionDate__c!=null)
            opportunityrecord.CloseDate=opportunityRecord.QuoteSubmissionDate__c.addMonths(6);
        else
            opportunityrecord.CloseDate=System.Today().addMonths(6);
        if(opportunityRecord.CountryOfDestination__c==null)
            opportunityRecord.CountryOfDestination__c=countryRecord;
        searchStep=false;
        creatStep=True;
       
        //Default the Leading BU value similar like in Opportunity search
        if(currentUser.UserBusinessUnit__c !='' && currentUser.UserBusinessUnit__c!=null){
            if(CS_OpportunityLeadingBUMapping__c.getValues(currentUser.UserBusinessUnit__c)!=null)
                opportunityRecord.LeadingBusiness__c=CS_OpportunityLeadingBUMapping__c.getValues(currentUser.UserBusinessUnit__c).OpportunityLeadingBU__c;
       }
        return null;
    }
    
    //Same rules as Opportunity Search Page
    public void searchOpportunities(){
        results.clear();
        Integer counter=0; // to ensure atleast two filters other than country are filled
        Integer counterNameCheck=0; //For better performance - along with name filter we should have different lookup filters
        
        String searchString = 'Select Id,Name,Location__c,StateProvince__c,StateProvince__r.Name,StageName,CloseDate,QuoteSubmissionDate__c,Amount,OwnerId, Owner.Name, AccountId,Account.Name From Opportunity';
        List<String> whereConditions = new List<String>();
        String whereClause='';
        
        if(opptyName!=null && opptyName!=''){
           whereConditions.add('Name LIKE \'%'+Utils_Methods.escapeForWhere(opptyName)+'%\'');
           opportunityRecord.Name=opptyName;
           counterNameCheck++; 
        }
        
        if(opportunityRecord.CountryOfDestination__c!=null)     
            whereConditions.add('CountryOfDestination__c=\''+opportunityRecord.CountryOfDestination__c+'\'');
        
        if(opp.AccountId!=null){     
            whereConditions.add('AccountId=\''+opp.AccountId+'\'');
            counter++; 
            counterNameCheck++; 
        }
      
        if(opportunityRecord.StateProvince__c!=null){     
            whereConditions.add('StateProvince__c=\''+opportunityRecord.StateProvince__c+'\'');
            counter++; 
            counterNameCheck++; 
        }
         if(opportunityRecord.QuoteSubmissionDate__c!=null) {
                counter++;               
                whereConditions.add('QuoteSubmissionDate__c<='+String.valueof(opportunityRecord.QuoteSubmissionDate__c.addDays(30)));
                whereConditions.add('QuoteSubmissionDate__c>='+String.valueof(opportunityRecord.QuoteSubmissionDate__c.addDays(-30)));
        }
         if(opportunityRecord.OpportunityTerritory__c!=null) {
                counter++;
                counterNameCheck++;                
                whereConditions.add('OpportunityTerritory__c=\''+opportunityRecord.OpportunityTerritory__c+'\'');
        }

        whereConditions.add('StageName!=\''+Label.CL00272+'\'');
        whereConditions.add('StageName!=\''+Label.CL00220+'\'');

        System.debug('>>>>>>>Counter<<<<<'+counter);
        //Minimum two filters needs to be filled other than country
        if( (counter < 2 && counterNameCheck<2) || whereConditions.size() == 0){
            Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,System.Label.CLQ416SLS028));
        }
        else if((counter >=2 || counterNameCheck>=2)&& whereConditions.size() > 0){
            searchString += ' WHERE ';
            for(String condition : whereConditions) 
                searchString += condition + ' AND '; 
            String sortFullExp = sortExpression + ' ' + sortDirection;
            string nullvaluesort;
            if(sortDirection == 'ASC')            
                nullvaluesort= ' NULLS LAST';
            if(sortDirection == 'DESC')            
                nullvaluesort = ' NULLS FIRST';    
            searchString = searchString.substring(0, searchString.lastIndexOf(' AND '));  
            searchString+=' ORDER BY ' + sortFullExp + nullvaluesort + ' Limit 101 ';

            system.debug('searchString>>>>>'+ searchString );
            try{
                results = Database.query(searchString);
            }
            catch(Exception ex){    
                Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,ex.getmessage()));
            } 
            // disable create button incase count is greater than 30 or 100 but display different messages        
            if(results.size()>30){
                disableCreateOpportunitybutton=true;
                Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.Warning,Label.CL00015));
            }
            else
                disableCreateOpportunitybutton=false;   
            if(results.size()>100)        
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349)); 
        }
    }
        
    // typeahead functinality similar like we have for competitors functionality
    @RemoteAction
    global static ResultSet[] getRecords(String searchText,Id cnty) {
        System.debug('>>>>>cnty'+cnty);
        searchedRecord = new list<ResultSet>();
        //SOSL Text should be more then one charecter
        if(searchText.length() >0){
           String qry='SELECT Name,Owner.Name,Country__r.Name,StateProvince__r.Name,Street__c,City__c FROM Account WHERE Name like \'' + searchtext + '%\' and country__c=\'' +cnty+'\' and recordtypeID= \'' + system.Label.CLOCT13ACC08 +'\' order by Name Limit 100' ;
            System.debug('>>>>>>>query'+qry);
            List<Account> searchList = Database.query(qry); 
            for(Account o:searchList){
                    searchedRecord.add(new VFC_NAMOpportunityWizard.ResultSet(o)); 
            }
        }
        return searchedRecord;
    }
     /*Record Wrapper*/
    global class ResultSet{
        public String Id {get;set;} 
        public String Name{get;set;}
        public string Owner{get;set;}
        public string Street{get;set;}
        public string City{get;set;}
        public string State{get;set;}
        public string Country{get;set;}
        
        
        public ResultSet(sObject s){
            this.Id = s.Id;
            this.Name = s.get('Name')+''; 
            this.Owner=(String) s.getSobject('Owner').get('Name')+'';
            if(s.get('Street__c')!=null)
            this.Street= s.get('Street__c')+''; 
            else
               this.Street= '';  
            if(s.get('City__c')!=null)
                this.City= s.get('City__c')+''; 
             else
                 this.City='';
             if(s.getSobject('StateProvince__r')!=null)
                this.State=(String) s.getSobject('StateProvince__r').get('Name')+'';
             else
                 this.state='';
            if(s.getSobject('Country__r')!=null)
                this.Country=(String) s.getSobject('Country__r').get('Name')+'';
            else
                this.Country='';
        } 
    }
    
    //Opportunity creation
    public void opptyCreation(){
        opp.AccountId=null; // dont default the value of search screen
        if(OpptyType!=null && OpptyType!='')
            opportunityRecord.OpptyType__c=OpptyType;
        if(opportunityRecord.OpptyType__c==null || opportunityRecord.OpptyType__c==''){
            errormsg=TRUE; 
            Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,System.Label.CLQ416SLS016));
        }
        else if (opportunityRecord.OpptyType__c==System.Label.CLQ416SLS001 && (opportunityRecord.MarketSegment__c==null || opportunityRecord.MarketSegment__c=='')){
            errormsg=TRUE; 
            Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,System.Label.CLQ416SLS017));
        }
        else if(opportunityRecord.OpptyType__c==System.Label.CLQ416SLS001 &&  opportunityRecord.StageName!=null && !(opportunityRecord.StageName.containsAny(System.Label.CLQ416SLS038))){
            errormsg=TRUE; 
            Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,System.Label.CLQ416SLS039));
        }
        else{
            try{
                System.debug('******'+OpptyType);
                String opptyName;
                opptyName=opportunityRecord.Name;
                if(opportunityRecord.Location__c!=null)
                opptyName=opptyName+' - '+opportunityRecord.Location__c;
                //Opportunity Name format
                if(opportunityRecord.AccountId!=null){
                    Account acc=[select Id,name from Account where Id=:opportunityRecord.AccountId];
                    opptyName=opptyName+' - '+acc.Name;
                }
                opp.Name=opptyName;
                opp.BusinessMix__c=System.Label.CLQ416SLS018;
                opp.AccountId=opportunityRecord.AccountId;
                opp.CloseDate=opportunityRecord.CloseDate;
                opp.CountryOfDestination__c=opportunityRecord.CountryOfDestination__c;
                opp.CurrencyIsoCode=System.Label.CLQ416SLS019;
                opp.IncludedInForecast__c=System.Label.CLQ416SLS020;
                opp.LeadingBusiness__c=opportunityRecord.LeadingBusiness__c;
                opp.OpportunityCategory__c=System.Label.CLQ416SLS021;
                opp.OpportunityScope__c=System.Label.CLQ416SLS022;
                opp.OpportunitySource__c=System.Label.CLQ416SLS023;
                opp.OpportunityTerritory__c=opportunityRecord.OpportunityTerritory__c;
                opp.Location__c=opportunityRecord.Location__c;
                opp.StateProvince__c=opportunityRecord.StateProvince__c;
                opp.OpptyType__c=  opportunityRecord.OpptyType__c; 
                opp.MarketSegment__c=opportunityRecord.MarketSegment__c;
                opp.QuoteSubmissionDate__c=opportunityRecord.QuoteSubmissionDate__c;
                opp.SourceReference__c=System.Label.CLQ416SLS006;
                opp.TECH_CrossProcessConversionAmount__c=opportunityRecord.Amount;
                opp.StageName=opportunityRecord.StageName;
                System.debug('>>>>'+opportunityRecord.StageName);
                insert opp;
                creatStep=false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,System.Label.CLQ416SLS024+' '+'<b><a href='+Site.getPathPrefix()+'/'+opp.Id+'>'+opp.Name +'</a></b>')); 
           }
           catch(Exception ex){ 
               errormsg=TRUE;    
                Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,ex.getmessage()));
            }
        }  
    }
    Public Pagereference skip(){
        createRelatedRecords();

        PageReference pageref= new PageReference(Site.getPathPrefix()+'/'+opp.id);    
        pageref.setRedirect(true);
        return pageref;
    }
    
     Public Pagereference saveVCP(){
        vcpLst=new List<OPP_ValueChainPlayers__c>();
        vcpLst.clear(); 
        
        if((vcp1.Account__c !=null && vcp1.AccountRole__c==null) || (vcp2.Account__c !=null && vcp2.AccountRole__c==null) || (vcp3.Account__c !=null && vcp3.AccountRole__c==null) || (vcp4.Account__c !=null && vcp4.AccountRole__c==null) || (vcp5.Account__c !=null && vcp5.AccountRole__c==null)){
             Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,System.Label.CLQ416SLS025));
            return null;
        }
        List<OPP_ValueChainPlayers__c> vcpInitialList=new List<OPP_ValueChainPlayers__c>();
        vcpInitialList.add(vcp1);
        vcpInitialList.add(vcp2);
        vcpInitialList.add(vcp3);
        vcpInitialList.add(vcp4);
        vcpInitialList.add(vcp5);
        
        for(Integer i=0;i<5;i++){
            if(vcpInitialList[i].Account__c!=null){
                OPP_ValueChainPlayers__c vcp = new OPP_ValueChainPlayers__c ();
                vcp.Account__c = vcpInitialList[i].Account__c;
                vcp.OpportunityName__c=opp.Id;
                vcp.AccountRole__c=vcpInitialList[i].AccountRole__c;
                vcpLst.add(vcp);
            }
        }
        
        createRelatedRecords();

        PageReference pageref= new PageReference(Site.getPathPrefix()+'/'+opp.id);    
        pageref.setRedirect(true);
        return pageref;

    }

    public void createRelatedRecords(){
    
        System.debug('Entered createRelatedRecords block>>>>');
        System.debug('>>>>>>><<<<<'+vcpLst);
        List<sObject> objects = new List<sObject>();
        for(Integer i=0;i<vcpLst.size();i++)
           objects.add(vcpLst[i]);

        if(member.UserID!=null && member.UserID!=Userinfo.getUserID()){
            member.OpportunityId=opp.Id;
            member.TeamMemberRole=System.Label.CLQ416SLS033;
            objects.add(member);
            OpportunityShare shr=new OpportunityShare();
            shr.OpportunityAccessLevel='Edit';
            shr.OpportunityId=opp.Id;
            shr.UserOrGroupId=member.UserID;
            objects.add(shr);
            
        }
        try{
            if(objects.size()>0)
                insert objects;
        }
      catch(Exception ex){
        errormsg=TRUE;  
        Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,ex.getmessage())); 
      }
    }  
    
    Public Pagereference cancel(){
        if(System.currentPageReference().getParameters().get('accid')!=null){
            PageReference pageref= new PageReference(Site.getPathPrefix()+'/'+System.currentPageReference().getParameters().get('accid'));    
            pageref.setRedirect(true);
            return pageref;
        }
        else{
            PageReference pageref= new PageReference(Site.getPathPrefix()+System.Label.CLQ416SLS031);    
            pageref.setRedirect(true);
            return pageref;
        }
    
    }
    public List<SelectOption> getstageName()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =Opportunity.stagename.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('', System.Label.CLQ416SLS036));
        for( Schema.PicklistEntry f : ple){
            if(!(f.getValue().containsAny(System.Label.CLQ416SLS037))){
                if(OpptyType==System.Label.CLQ416SLS001 && f.getValue().containsAny(System.Label.CLQ416SLS038))//for solution Opportunity only stage 2 and 3 
                    options.add(new SelectOption(f.getValue(),f.getLabel()));
                else if(OpptyType!=System.Label.CLQ416SLS001)// for standard and fast track stages 2,3,4,5 allowed
                    options.add(new SelectOption(f.getValue(),f.getLabel()));
            }  
       }       
       return options;
    }
    public void typeStageMapping(){
        System.debug('>>>>>OpptyType'+OpptyType);
        getstageName();
        if(OpptyType==System.Label.CLQ416SLS001)
            opportunityRecord.StageName=System.Label.CLQ416SLS026; //default stage 3 for solutions
        else
            opportunityRecord.StageName=System.Label.CLQ416SLS027; // default stage 5 for standard and fast track
    }  
}