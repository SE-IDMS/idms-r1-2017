/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: Revert Fielo Transaction page controller
********************************************************************/

public with sharing class FieloPRM_VFE_TransactionRevertExtension {
    
    public String messageRet {get;set;}
    
    private Id trxId;
    
    public FieloPRM_VFE_TransactionRevertExtension(ApexPages.StandardController controller) {
        trxId = ((FieloEE__Transaction__c)controller.getRecord()).id;
    }
    
    public void doRevert(){
        if(trxId != null){
            try{
                messageRet = FieloEE.TransactionUtil.revertTransaction(trxId);
                ApexPages.addMessage(new ApexPages.Message((messageRet.containsIgnoreCase('error') ? ApexPages.severity.error : ApexPages.severity.confirm), messageRet));
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getMessage()));
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, 'Please select a transaction'));
        }
    }

}