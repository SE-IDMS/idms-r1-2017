/*
    Author          : Deepak
    Date Created    : 27/01/2015 April Release
    Description     : Batch Apex class Opportunity Creation.
              
*/
    global class Batch_EndWarrOpportunityCreation implements Database.Batchable<sObject>, Schedulable{

        
        public String scheduledJobName = Label.CLOCT15SRV08;
        public Integer rescheduleInterval = Integer.ValueOf(Label.CLOCT15SRV07);
        public Integer querylimit = Integer.ValueOf(Label.CLOCT15SRV09);
        public Integer scopelimit = Integer.ValueOf(Label.CLQ316SRV03);
        public String OrderBy = Label.CLOCT16SRV01; //Added by VISHNU C for Q3 2016 Release BR-9886
        public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
        global boolean debug = false;
        
        
        global Database.QueryLocator start(Database.BatchableContext BC){ 
       
        Integer EndofWarrMax = Integer.ValueOf(Label.CLAPR15SRV75);
        Integer EndofWarrMin = Integer.ValueOf(Label.CLAPR15SRV76);
        Set<String> buset = new Set<String>();
        List<CS_BUCountry__c>  listBuCountry = CS_BUCountry__c.getall().values();
        
        for(CS_BUCountry__c obj:listBuCountry ){
            if(obj.BatchType__c =='EOW'){
              buset.add(obj.BusinessUnit__c+obj.CountryCode__c);
            }
        }
         
                System.debug('#######OHA############ BUSET: '+buset);
        Date myDateMin = system.today() + EndofWarrMin;
        Date myDateMax = system.today() + EndofWarrMax;
        String CataVar = 'Category 3';
                
        try{
        
            String Query ='select id,EndOfWarrantyOpportunityGenerated__c,ObsolescenceOpportunityGenerated__c , SVMXC__Product__r.RenewableService__c ,EndOfWarrantyOpportunityTriggered__c,ObsolescenceOpportunityTriggered__c,SVMXC__Product__r.Serviceability__c,';
                    Query +='Tech_CheckForEndOfWarranty__c,Tech_IpAssociatedToSC__c,AssetCategory2__c,UnderContract__c,SVMXC__Company__c,SVMXC__Company__r.Name,';
                    Query +='SVMXC__Product__r.BusinessUnit__c, SVMXC__Company__r.MarketSegment__c,SVMXC__Warranty_End_Date__c,';
                    Query +='SVMXC__Company__r.LeadingBusiness__c,   SVMXC__Company__r.AccType__c,SVMXC__Product__r.ProductGDP__c,'; 
                    Query +='SVMXC__Company__r.Country__c,SKUToCreate__c,ProductBuCountry__c,CommissioningDateInstallDate__c,';
                    Query +='Tech_IPGrouping__c  from SVMXC__Installed_Product__c Where  (SVMXC__Warranty_End_Date__c  <= :myDateMax AND SVMXC__Warranty_End_Date__c > :myDateMin) And EndOfWarrantyOpportunityGenerated__c =false and UnderContract__c =false And AssetCategory2__c <> : CataVar and SVMXC__Product__r.RenewableService__c =true ';
            if(OrderBy == 'True' )
            {               
                Query += ' ORDER BY SVMXC__Company__c ';                
            }           
            Query +=' limit :querylimit ';  
            
            return Database.getQueryLocator(Query);
           
            //Changes by VISHNU C Ends here
        }
            catch(Exception e) {
                return null;
            }
       
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){ 
        system.debug('scope11'+scope.size());
        AP_OpportunityCreationFromIB.ProcessInstalledProduct(scope);
        
        //Database.update(scope,false);
    }


    global void finish(Database.BatchableContext BC){
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);

        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new Batch_EndWarrOpportunityCreation(), scheduledJobName, rescheduleInterval);
        }
    }
    global void execute(SchedulableContext sc) {
        Batch_EndWarrOpportunityCreation OptyCreation = new Batch_EndWarrOpportunityCreation();
        Database.executeBatch(OptyCreation,scopelimit);
    }   

}