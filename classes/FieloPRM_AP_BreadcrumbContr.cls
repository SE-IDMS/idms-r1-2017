public class FieloPRM_AP_BreadcrumbContr{
    
    public FieloEE__Menu__c homeMenu {get; set;}
    public FieloEE__Menu__c parentMenu {get; set;}
    public FieloEE__Menu__c actualMenu {get; set;}
    public String fieldMenuName {get; set;}
    
    public FieloPRM_AP_BreadcrumbContr(){
        if(ApexPages.currentPage().getCookies().containsKey('myHomeMenu')){
        
            fieldMenuName = 'Name_' + FieloEE.OrganizationUtil.getLanguage() + '__c';
            if(!Schema.SObjectType.FieloEE__Menu__c.fields.getMap().containskey(fieldMenuName)){
                fieldMenuName = 'Name';
            }
            
            String homeExternalName = ApexPages.currentPage().getCookies().get('myHomeMenu').getValue();
            Id programId = FieloEE.ProgramUtil.getProgramByDomain().Id;
            
            List<FieloEE__Menu__c> homeMenus = Database.query('SELECT Id, ' + fieldMenuName + ', F_PRM_MultiLanguageCustomLabel__c, FieloEE__CustomURL__c, (SELECT Id FROM FieloEE__Components__r LIMIT 1), (SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__r ORDER BY FieloEE__Order__c) FROM FieloEE__Menu__c WHERE FieloEE__ExternalName__c =: homeExternalName AND FieloEE__Program__c =: programId LIMIT 1');
            if(!homeMenus.isEmpty()){
                homeMenu = homeMenus[0];
            }
            
            String idMenu = ApexPages.currentPage().getParameters().get('idMenu');
            List<FieloEE__Menu__c> actualMenu = Database.query('SELECT Id, ' + fieldMenuName + ', FieloEE__visibility__c, F_PRM_MultiLanguageCustomLabel__c, FieloEE__Menu__c, FieloEE__CustomURL__c, (SELECT Id FROM FieloEE__Components__r LIMIT 1), (SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__r ORDER BY FieloEE__Order__c) FROM FieloEE__Menu__c WHERE Id =: idMenu LIMIT 1');
            if(!actualMenu.isEmpty()){
                this.actualMenu = actualMenu[0];
                if(actualMenu[0].FieloEE__Menu__c != null){
                    parentMenu = Database.query('SELECT Id, ' + fieldMenuName + ', F_PRM_MultiLanguageCustomLabel__c, FieloEE__CustomURL__c, (SELECT Id FROM FieloEE__Components__r LIMIT 1), (SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__r where ID = \'' +   this.actualMenu.ID + '\'  ORDER BY FieloEE__Order__c) FROM FieloEE__Menu__c WHERE Id = \'' + actualMenu[0].FieloEE__Menu__c + '\' LIMIT 1');
                }
            }
        }
        
    }
    
}