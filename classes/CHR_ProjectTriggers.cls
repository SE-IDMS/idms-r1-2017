public with sharing class CHR_ProjectTriggers {

    public static final Map<Id,RecordType> recordTypeMap=new Map<Id,RecordType>([select id, Name, DeveloperName from RecordType where sObjectType='Change_Request__c' and IsActive = true]);
    
    public static void CHRBeforeUpdate(Map<Id,Change_Request__c> newMap,Map<Id,Change_Request__c> oldMap){
        list<Change_Request__c> projectListToProcessCONDITION1 = new list<Change_Request__c>();
        list<Change_Request__c> projectListToProcessCONDITION2 = new list<Change_Request__c>();
        list<Change_Request__c> projectListToProcessCONDITION3 = new list<Change_Request__c>();
        list<Change_Request__c> projectListToProcessCONDITION4 = new list<Change_Request__c>();
     
        for (ID projId:newMap.keyset()){
            Change_Request__c newProj = newMap.get(projId);
            Change_Request__c oldProj = oldMap.get(projId);
           
              if (recordTypeMap.containskey(oldProj.RecordTypeId))
              {
                  if (newProj.RecordTypeId <> oldProj.RecordTypeId)
                  {
                      newProj.PreviousStep__c = recordTypeMap.get(oldProj.RecordTypeId).Name;
                  }
                  
                  if(newProj.NextStep__c == System.Label.DMT_StatusProjectOpen) 
                  {
                      newProj.TECHActualStep__c = System.Label.DMT_StatusInProgress;
                  }
                  else if(newProj.NextStep__c == System.Label.DMT_StatusCancelled || newProj.NextStep__c == System.Label.DMT_StatusPostponed)
                  {
                      if(recordTypeMap.get(newProj.RecordTypeId).DeveloperName == System.Label.DMT_RecordTypeProjectFinished)
                      {
                          newProj.TECHActualStep__c = System.Label.DMT_StatusInProgress;
                      }    
                      else
                      {
                          newProj.TECHActualStep__c = recordTypeMap.get(newProj.RecordTypeId).Name;
                      }
                  }
                  else
                  {
                      newProj.TECHActualStep__c = recordTypeMap.get(newProj.RecordTypeId).Name;
                  }
              }
            
            if(newProj.NextStep__c == System.Label.DMT_StatusProjectOpen && newProj.NextStep__c <> oldProj.NextStep__c)
            {
                projectListToProcessCONDITION3.add(newProj);
            }
                
        }
          
        if (projectListToProcessCONDITION3.size()>0) 
        {
            CHR_ProjectUtils.verifyHFMCodeEnteredandSelectedInEnvelop(projectListToProcessCONDITION3);   
        }
        
        if(newMap.values().size()>0)
            CHR_ProjectUtils.updateProjectRequestFields(newMap.values());
       
     }
     
    
     public static void CHRAfterInsert(List<Change_Request__c> PrjReqListToProcessCONDITION1){
         CHR_ProjectUtils.projectRequestDmtFEAutoCreation(PrjReqListToProcessCONDITION1);
     }
      
}