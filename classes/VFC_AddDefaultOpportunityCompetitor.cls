/*
    June 2016 release 
    BR-9556 - Add default Competitors of owner to the Opportunity Competitor 
    Controller for VFP_AddDefaultOpportunityCompetitor
*/
public with sharing class VFC_AddDefaultOpportunityCompetitor {
    Id oppID;
    public List<wrapOpportunityCompetitor> defaultOpptyCompetitorList{get;set;}{defaultOpptyCompetitorList = new List<wrapOpportunityCompetitor>();}
    public Boolean allChecked { get; set; }{allChecked=False; }
    public string sourcePage;
    public String opptyStatus;
    public String opptyReason;
    public String opptyAmount;

    public VFC_AddDefaultOpportunityCompetitor(ApexPages.StandardController con) {
        oppID=(ID)System.currentPageReference().getParameters().get( 'oppID' );
        sourcePage = System.currentPageReference().getParameters().get( 'sourcePage' );
        opptyStatus = System.currentPageReference().getParameters().get( 'status' );
        opptyReason = System.currentPageReference().getParameters().get( 'reason' );
        opptyAmount = System.currentPageReference().getParameters().get( 'amount' );
    }

    public PageReference doConfirm(){
        List<UserParameter__c> defaultCompList=new List<UserParameter__c>();
        Map<Id,Competitor__c> activecompetitorMap = new Map<Id,Competitor__c>([Select Id,Name from Competitor__c where Active__c=True]);
        
        //fetch the defaulted competitors of current user
        defaultCompList=[select Id,User__c,competitor__c,competitor2__c,competitor3__c,competitor4__c,competitor5__c,competitor6__c,competitor7__c,competitor8__c,competitor9__c,competitor10__c from UserParameter__c where User__c=:userinfo.getUserId()];
       
        //check if the Competitors in the default list are the active competitors 
        if(defaultCompList.size()>0 && defaultCompList!=null){
            if(defaultCompList[0].competitor__c!=null || defaultCompList[0].competitor2__c!=null || defaultCompList[0].competitor3__c!=null || defaultCompList[0].competitor4__c!=null || defaultCompList[0].competitor5__c!=null || defaultCompList[0].competitor6__c!=null || defaultCompList[0].competitor7__c!=null || defaultCompList[0].competitor8__c!=null || defaultCompList[0].competitor9__c!=null || defaultCompList[0].competitor10__c!=null){
                if(defaultCompList[0].competitor__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor2__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor2__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor2__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor3__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor3__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor3__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor4__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor4__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor4__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor5__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor5__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor5__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor6__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor6__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor6__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor7__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor7__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor7__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor8__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor8__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor8__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor9__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor9__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor9__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
                if(defaultCompList[0].competitor10__c!=null && activecompetitorMap.containsKey(defaultCompList[0].competitor10__c)){
                    OPP_OpportunityCompetitor__c opc=new OPP_OpportunityCompetitor__c();
                    opc.TECH_CompetitorName__c=defaultCompList[0].competitor10__c;
                    opc.OpportunityName__c=oppID;
                    defaultOpptyCompetitorList.add(new wrapOpportunityCompetitor(opc));
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLJUN16SLS11));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLJUN16SLS11));
        }
        return null;
    }

    public PageReference addDefaultCompetitor(){
        List<OPP_OpportunityCompetitor__c> selecteddefaultOpptyCompetitorList= new List<OPP_OpportunityCompetitor__c>();

        //Insert the default Opportunity Competitiors which are selected  
        for(wrapOpportunityCompetitor oppcomp : defaultOpptyCompetitorList){
        if(oppcomp.selected == true)
            selecteddefaultOpptyCompetitorList.add(oppcomp.rec);
        }
        if(selecteddefaultOpptyCompetitorList.size()>0 && selecteddefaultOpptyCompetitorList!=null){
            try{
                    insert selecteddefaultOpptyCompetitorList;
                    System.debug('>>>>sourcePage'+sourcePage);
                    if(sourcePage == 'OpptyLost')  // functinality for Win(Stage 7)/Close button on Opportunity
                    {
                        String url=Site.getPathPrefix()+'/apex/VFP_OpportunityLost?';
                        Pagereference pref = new Pagereference(url);
                        If( oppID!= null)
                            pref.getParameters().put( 'id', oppID);
                        if (opptyStatus!=null)
                            pref.getParameters().put('status', opptyStatus);
                        if (opptyReason!=null)
                            pref.getParameters().put('reason', opptyReason); 
                        if (opptyAmount!=null)
                            pref.getParameters().put('amount', opptyAmount);    
                        pref.setredirect(true);
                        System.debug('---->>>> Save Method: From VFP_OpportunityLost :: '+pref.getUrl() );   
                        return pref;                     
                    }
                    PageReference pr = new PageReference(Site.getPathPrefix()+'/'+oppID);
                    pr.setredirect(true); 
                    return pr;
            }
            Catch(Exception ex){
                System.debug('>>>>>>'+ex);
                List<Id> extComp=new List<Id>();
                for (Integer i = 0; i < ex.getNumDml(); i++) {
                    if(ex.getDmlStatusCode(i)==Label.CLJUN16SLS16)
                        extComp.add((Id)ex.getDmlMessage(i).Right(15));
                     else
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getDmlMessage(i)));
               }
               if(extComp.size()>0)
               {
                   List<OPP_OpportunityCompetitor__c> compList=new List<OPP_OpportunityCompetitor__c>();
                   compList=[Select TECH_CompetitorName__c,TECH_CompetitorName__r.Name from OPP_OpportunityCompetitor__c where Id in :extComp];
                   for(OPP_OpportunityCompetitor__c opp:compList)
                      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLJUN16SLS15+opp.TECH_CompetitorName__r.Name+Label.CLJUN16SLS17));
               }         
            }   
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLJUN16SLS12));
        }
        return null;
    }

      //Cancel method
    public PageReference Cancel(){
      PageReference pr = new PageReference(Site.getPathPrefix()+'/'+oppID);
      pr.setredirect(true); 
      return pr;
    }
    
    public class wrapOpportunityCompetitor {
        public OPP_OpportunityCompetitor__c rec {get; set;}
        public Boolean selected {get; set;}
        public wrapOpportunityCompetitor(OPP_OpportunityCompetitor__c opc) {
            rec = opc;
            selected = false;
        }
    }
}