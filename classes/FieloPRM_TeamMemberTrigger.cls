/********************************************************************
* Company: Fielo
* Developer: Julio Enrique  
* Created Date: 13/03/2015 
* Description:
********************************************************************/
public with sharing class FieloPRM_TeamMemberTrigger {

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description add a team to a challenge. Challenge must be of type Teams
    * @return Void
    */
    public static void validateTeamMember(){
        List<FieloCH__TeamMember__c> teamMembers = Trigger.New;
        Set<Id> membersAdded = new Set<Id>();
        for(FieloCH__TeamMember__c tMember : teamMembers){
            if(!membersAdded.contains(tMember.FieloCH__Member2__c)){
                membersAdded.add(tMember.FieloCH__Member2__c);
            }else{
                tMember.addError('the member can be in only one team or cannot be repeated on the same one');
            }
        }
        for(FieloCH__TeamMember__c tMember : [SELECT FieloCH__Team2__c, FieloCH__Member2__c FROM FieloCH__TeamMember__c WHERE FieloCH__Member2__c IN:membersAdded]){
            for(FieloCH__TeamMember__c newRec : (List<FieloCH__TeamMember__c>)Trigger.New){
                if(newRec.FieloCH__Member2__c == tMember.FieloCH__Member2__c){
                    newRec.addError('the member can be in only one team or cannot be repeated on the same one');
                }
            }
        }
    }

}