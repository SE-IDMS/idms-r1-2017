/*
Class for the Project Cockpit which will display 
 Project and its children Opportunity with Lines
*/
public with sharing class VFC_ProjectCockpit {

    public VFC_ProjectCockpit(ApexPages.StandardController controller) {
        
    }
    
    //Wrapper class to fetch the lines and Child Opportunities
    public class opportunitywrapper {
        List<OPP_ProductLine__c> oppLineList{get;set;}
        List<Opportunity> opplst{get;set;}    
    }
   
   /* wrapper class to fetch Project and child Opportunities along with project owner pic.
    wrapper class was created since we are not able to access Owner.smallPhotoUrl in the query of
    Project (Custom Object) directly, Hence we are wrapping the Profile pic of the
    Owner with the Project */
    
   public class projectWrapper {
        OPP_Project__c prodrecord{get;set;}
        User ownerDetails{get;set;}   
        User currentUserDetails{get;set;}  
    }

    //This method is called onLoad of Page
     @RemoteAction  
    public static projectWrapper getParentdata(Id prjID){
        System.debug('>>>>>Id'+prjID);      
        // Fetch the project request currency fields  with the convertCurrency function
        List<OPP_Project__c> proj =  [Select Name,OwnerId,Owner.Name,ProjectEndUserInvestor__c,ProjectEndUserInvestor__r.Name,convertCurrency(SumOfAllOpportunities__c),EstimateProjectCloseDate__c,ProjectStatus__c,convertCurrency(TotalProjectAmountForecastedWon__c),VIPIcon__c,(Select Id,Name,convertCurrency(Amount),Owner.SmallPhotoUrl,Owner.Name,AccountId,Account.Name,toLabel(Account.ClassLevel1__c),OpptyType__c,StageName,QuoteSubmissionDate__c,CloseDate,ForecastCategoryName,Opportunity_VIPIcon__c,IncludedInForecast__c,Probability,Status__c from Opportunities__r Order By StageName,CloseDate) from OPP_Project__c where Id=:prjID];
        System.debug('>>>>>>>prj[0]>>>'+proj[0]);
        projectWrapper pwrap= new projectWrapper();
        pwrap.prodrecord=proj[0];
        
        //for custom object currently salesforce not letting Owner.smallPhotoUrl value hence we are querying seperately
        User u=[select Name,smallPhotoUrl,CurrencyIsoCode,DefaultCurrencyIsoCode from user where Id=:proj[0].OwnerId];
        
        // fetch the currency details of the current user who is viewing
        User currentUser=[select CurrencyIsoCode,DefaultCurrencyIsoCode from user where Id=:UserInfo.getUserId()];
        pwrap.ownerDetails=u;
        pwrap.currentUserDetails=currentUser;
        return pwrap;
     }
     
    //This method will fetch Opportunity Lines and Child Opportunities On click  - Lazy Loading 
    @RemoteAction  
    public static List<opportunitywrapper> getChildren(Id opp){
    System.debug('>>>> child Opportunity got called>>>>>>>>>>>>>>>>>');
        List<opportunitywrapper > lstwrap= new List<opportunitywrapper >();
        List <Opportunity> lstopp = [SELECT Id,CurrencyIsocode,Name,convertCurrency(Amount),Owner.SmallPhotoUrl,Owner.Name,AccountId,Account.Name,toLabel(Account.ClassLevel1__c),OpptyType__c,StageName,QuoteSubmissionDate__c,CloseDate,ForecastCategoryName,IncludedInForecast__c,Opportunity_VIPIcon__c,Probability,Status__c
                                   ,(select Id,Name,LowestLevelInPyramid__c,Lowestlevelvalue__c,LineStatus__c,Quantity__c,convertCurrency(Amount__c),convertCurrency(LineAmount__c),LineClosedate__c,ConcatenatedID__c,IncludedinForecast__c,Specified__c from Product_Line_2__r ORDER BY LineStatus__c),(Select Id,CurrencyIsocode,Name,convertCurrency(Amount),Owner.SmallPhotoUrl,Owner.Name,AccountId,Account.Name,toLabel(Account.ClassLevel1__c),OpptyType__c,StageName,QuoteSubmissionDate__c,CloseDate,ForecastCategoryName,IncludedInForecast__c,Opportunity_VIPIcon__c,Probability,Status__c from Opportunities__r  Order By StageName,CloseDate) FROM Opportunity where Id=:opp Order By StageName,CloseDate];
       
        for (Opportunity a: lstopp) {
            opportunitywrapper awrap = new opportunitywrapper ();
           awrap.oppLineList=a.Product_Line_2__r;
           awrap.opplst=a.Opportunities__r;
            lstwrap.add(awrap);
        }
        System.debug('>>>>>>>>>getChildOpportunity'+lstwrap);
        return lstwrap;
     }
}