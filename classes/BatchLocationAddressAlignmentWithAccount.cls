global class BatchLocationAddressAlignmentWithAccount implements Database.Batchable<sObject>, Schedulable {
  
    public Integer rescheduleInterval = Integer.ValueOf(Label.CLAPR14SRV13);
    
    public String query = 'SELECT Id, Name, SVMXC__Street__c, SVMXC__City__c, StateProvince__c, SVMXC__Zip__c, LocationCountry__c, SVMXC__Latitude__c, SVMXC__Longitude__c, SVMXC__Account__c, SVMXC__Account__r.Name, SVMXC__Account__r.Street__c, SVMXC__Account__r.City__c, SVMXC__Account__r.StateProvince__c, SVMXC__Account__r.ZipCode__c, SVMXC__Account__r.Country__c, SVMXC__Account__r.AutomaticGeoLocation__Latitude__s, SVMXC__Account__r.AutomaticGeoLocation__Longitude__s FROM SVMXC__Site__c WHERE TECH_isAddressSynchedWithAccount__c = False and SVMXC__Account__r.Id!=null limit '+Label.CLAPR14SRV14;
    
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
    
    global boolean debug = false;
    
    public String scheduledJobName = 'LocationAddressAlignmentWithAccount';
    
    global Database.Querylocator start(Database.BatchableContext BC){
        System.debug('### Starting BatchLocationAddressAlignmentWithAccount Batch');
        System.debug('### rescheduleInterval: '+rescheduleInterval);
        return Database.getQueryLocator(query);    
    }
    
/*    global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        System.debug('### Executing BatchLocationAddressAlignmentWithAccount Batch');
        System.debug('### rescheduleInterval: '+rescheduleInterval);
        if (scope != null) {
            if (debug) {
                System.debug('### Nb of locations: '+scope.size());
            }
            
            for ( SVMXC__Site__c site: (List<SVMXC__Site__c>) scope) { 
                if (debug) {
                    System.debug('### Current location:'+site);
                }
                site.SVMXC__Street__c = site.SVMXC__Account__r.Street__c;
                site.SVMXC__City__c = site.SVMXC__Account__r.City__c;
                site.StateProvince__c = site.SVMXC__Account__r.StateProvince__c;
                site.SVMXC__Zip__c = site.SVMXC__Account__r.ZipCode__c;
                site.LocationCountry__c = site.SVMXC__Account__r.Country__c;            
                if (debug) {
                    System.debug('### Updated location:'+site);
                }
            }
             
            try {
                Database.SaveResult[] results = Database.Update(scope, false);
                
                for(Database.SaveResult result: results) {
                    if (result.isSuccess()) {
                        System.debug('### Successfully updated location: '+result.getId());
                    }
                    else {
                        for (Database.Error err : result.getErrors()) {
                            System.debug('### Error updating location: '+ result.getId()+'\n\tCode: '+err.getStatusCode()+'\n\t'+'Msg: '+err.getMessage()+'\n\tFields: '+err.getFields());
                        }
                    }
                }
            }
            catch (DmlException dmle) {
                System.debug('### Exception / Error: '+dmle.getMessage());
            }
        }
        else {
            if (debug) {
                System.debug('### scope is null');
            }
        }
    }
*/    

    global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        System.debug('### Executing BatchLocationAddressAlignmentWithAccount Batch');
        System.debug('### Scope size: '+scope.size());
        System.debug('### Scope: '+scope);
        LocationHandler.alignAddressesWithAccounts((List<SVMXC__Site__c>) scope);
    }
    
    global void finish(Database.BatchableContext BC) {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);
                     
        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            // There is at least one scheduled job already for this name
            // Looking if one of scheduled job with the correct name is in an 'Active' state
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new BatchLocationAddressAlignmentWithAccount(), scheduledJobName, rescheduleInterval);
        }
    }
    
    global void execute(SchedulableContext sc) {
        BatchLocationAddressAlignmentWithAccount updateLocationAddresses = new BatchLocationAddressAlignmentWithAccount();
        Database.executeBatch(updateLocationAddresses);
    }    
}