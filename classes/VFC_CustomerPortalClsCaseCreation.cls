public without sharing class VFC_CustomerPortalClsCaseCreation{


Map<String, String> productFamily = new Map<String, String>{
'SFW'=> 'POWER SYST SOFTWARE',
'HDW'=> 'OPTIMUM ADVANCED RANGE'};


//===========GLOBAL VARIABLE FOR CREATION FORM=============
Id usid;
Id contid;
Id accid; 

Contact userContact = new Contact();    
Account userAcc = new Account();
User userr = null;

public Boolean checkOperatingSystem;   
public Boolean checkSoftwareVersion{get;set;}

public Boolean getCheckOperatingSystem(){return this.checkOperatingSystem;}
public void setCheckOperatingSystem(Boolean cos){this.checkOperatingSystem = cos;}

public Boolean getCheckSoftwareVersion(){return this.checkSoftwareVersion;}
public void setCheckSoftwareVersion(Boolean csv){this.checkSoftwareVersion = csv;}    

String selectedProductType {get; set;}
public String selectedProduct {get; set;}

public String softwareVersion;
public String operatingSystem {get; set;}

//String caseCategory = '4 - Post-Sales Tech Support';
//String caseReason   = 'Troubleshooting*';

String caseCategory = 'Troubleshooting*';
String caseReason   = '';

String caseSubReason = 'Software';  
String caseOrigin = 'Portal';
String caseStatus = 'New';

String caseSubject = '';

String caseDescription = null;

String ContactPhone = null;
String accountName = null;

//=========For adding an Attachment on case creation===============
public boolean isSfw {get; set;}
public boolean isHdw {get; set;}

public Attachment attach {get;set;}
public Attachment getAttach(){return attach;}
public void setAttach(Attachment a){this.attach = a;}

//===========GLOBAL GETTER & SETTER =============
    
public Case c;

public boolean getIsAuthenticated ()
{
  return (IsAuthenticated());
}

public boolean IsAuthenticated ()
{
  return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
}

public String acctNum { get; set; }
    
public Contact getUserContact() {return userContact;}

public User getUserr() {return userr;}

public Account getUserAcc() {return UserAcc;}

public String getselectedProductType() {return selectedProductType;}
public void setselectedProductType(String pt) {this.selectedproductType = pt;}

public String getOperatingSystem() {return OperatingSystem;}
public void setOperatingSystem(String os) {this.OperatingSystem = os;}

public String getSoftwareVersion () {return softwareVersion ;}
public void setSoftwareVersion (String sv) {this.softwareVersion  = sv;}

public String getCaseSubject () {return caseSubject;}
public void setCaseSubject (String cs) {this.CaseSubject = cs;}

public String getCaseDescription () {return CaseDescription ;}
public void setCaseDescription (String cd) {this.CaseDescription = cd;}

public String getContactPhone () {return ContactPhone ;}
public void setContactPhone (String Phone) {this.ContactPhone = ContactPhone ;}

public String getAccountName () {return accountName;}
public void setAccountName (String Phone) {this.accountName = accountName  ;}


public List<SelectOption> getProductTypes()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--- None ---'));        
        options.add(new SelectOption('SFW','Software'));
        options.add(new SelectOption('HDW','Hardware'));
        return options;
    } 
    


//===========THE CONSTRUCTOR =============
public VFC_CustomerPortalClsCaseCreation() {
           
            usid = UserInfo.getUserId();
             if ( usid != null)
                {
                  contid = [Select ContactId from User where Id =: usid LIMIT 1][0].ContactId;
                  userr = [Select FirstName, LastName, Name, Email from User where Id =: usid LIMIT 1][0];
                  checkOperatingSystem = true;
                  checkSoftwareVersion = true;
                  //checkProduct = true;
                    
                }
                  
             if ( contid != null)
                {
                  accid = [Select AccountId from Contact where Id =: contid].AccountId;
                  userContact = [Select WorkPhone__c from Contact where Id =: contId][0];
                  userAcc = [Select Name from Account where Id =: Accid][0];
                  ContactPhone = userContact.WorkPhone__c;
                  accountName = userAcc.Name;                  

                }

                    c = new Case();
                    attach = new Attachment();
        
                        /* MAPPING OF THE FIELDS 
                            mandatory field for case creation
                        - subject (*) ==> subject
                        - Category (*) ==> SupportCategory__c
                        - Reason (dependent picklist from Category) (*) ==> Symptom__c
                        - Account Name ==> Account(lookup)
                        - Contact Name ==> Contact(lookup)
                        - Case Description ==> CustomerRequest__c
                        - Status ==> Status
                        - Case Origin ==> Origin                              
                        
                        field in bFO for case creation  
                        - product_Family 
                        */
    }
    

//================PROCESSING FUNCTIONS ===================

//Methode to check the two fields (OS & SV) change to readonly to editable
    public void readToWriteField()
    {        
        if (selectedProductType == 'None'){
            setCheckSoftwareVersion(true);
            setCheckOperatingSystem(true);
            setSoftwareVersion('');
            setOperatingSystem('');
            //checkProduct = true;

        }
        
        if (selectedProductType == 'SFW'){
            setCheckSoftwareVersion(false);
            setCheckOperatingSystem(true);
            setOperatingSystem('');
            
        }
        if (selectedProductType == 'HDW'){
            setCheckOperatingSystem(false);
            setCheckSoftwareVersion(true);
            setSoftwareVersion('');
            }
    }                  
    

//===============Case submission=============================

    
public PageReference submitCase() 
{
  // Is the User authenticated?
  
  if (IsAuthenticated())
  {       
        c.SubSymptom__c = 'Software*';
        
        boolean bool = false; 
    
        if (caseSubject.equalsIgnoreCase(''))
        {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a subject for your request');
                ApexPages.addMessage(msg); 
        }
        
       
        else if (caseDescription.equalsIgnoreCase('')){
                
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a Description for your request');
                ApexPages.addMessage(msg);
        }

        else if (selectedProductType == 'None'){
                
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Product Type');
                ApexPages.addMessage(msg);                
         }
                
         else if (selectedProductType == 'SFW')
         {
           if((SoftwareVersion.length() == 0) && (SoftwareVersion.equalsIgnoreCase('')))
           {
             ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a Software Version for your request');
             ApexPages.addMessage(msg);
           }
           else if((OperatingSystem.length() == 0) && (OperatingSystem.equalsIgnoreCase('')))
           {
             ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter an Operating System for your request');
             ApexPages.addMessage(msg);
           }
           else 
           {
             c.SubSymptom__c   = 'Software*'; 
             c.Product_Type__c = 'Software';
             
             bool = true;
           } 
         }
         
         else if (selectedProductType == 'HDW')
         {
           bool = true; 
           
           softwareVersion = ' ';
           operatingSystem = ' ';
           
           c.SubSymptom__c   = 'Hardware Product*'; 
           c.Product_Type__c = 'Hardware'; 
         }
               
         if (bool)
         {
           try
           {
             c.Subject            = caseSubject;
             c.CustomerRequest__c = caseDescription != null ? caseDescription.substring(0,Math.min(caseDescription.length(),30000)) : '';
             c.family__c          = productFamily.get(selectedProductType);
                
             caseSubReason = selectedProductType;
                
             c.Operating_System__c = operatingSystem;
             c.SoftwareVersion__c  = softwareVersion;
             c.AccountId           = accid;
             c.ContactId           = contid;
             c.supportCategory__c  = caseCategory;
             c.symptom__c          = caseReason;            
             c.status              = caseStatus;
             c.origin              = caseOrigin;
             c.LevelOfExpertise__c = 'Advanced';
             c.PortalID__c         = 'PWRMGR'; //to be set to PWRMGR whenever a Case is created in Compass. This will allow Power Management Agents to filter their Case List Views on Cases created through the Portal.
                                     
             // Specify DML options to ensure the assignment rules are executed                
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.useDefaultRule = false;
                c.setOptions(dmlOpts); 
                              
                    
         
            // Insert the case                            
            insert c;
            
            //--------------- Here to insert an attachment on a case ----------------/
            if((attach.name != null) && (c.Id != null)){

                attach.ParentId = c.Id;        
                insert attach;}

            
            
            //------------------- Here to insert an attachment on a case -------------------
            
            
               
                return new PageReference('/apex/VFP_CustomerPortalCls');
                            
            }
         catch (Exception e) {
             ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to save the Case:' + e.getMessage());
             ApexPages.addMessage(msg);
            }}          
  }
         
return null;
}



}