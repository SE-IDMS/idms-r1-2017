/*
    Author          : Abhishek (ACN) 
    Date Created    : 26/05/2011
    Description     : Test class for the apex class VFC30_SynchronizeQuoteLink .
*/
@isTest
private class VFC30_SynchronizeQuoteLink_TEST {
    
/* @Method <This method SynchronizeQuoteLinkTestMethod is used test the VFC30_SynchronizeQuoteLink class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Exception>
*/
    static testMethod void SynchronizeQuoteLinkTestMethod()
    {
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        
        Opportunity opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        Database.insert(opportunity);         
        
        OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        newQuoteLink.QuoteRefNumberinLegacy__c='QLK-1234';
        Database.insert(newQuoteLink);
        
        Test.startTest();
        ApexPages.StandardController sc1= new ApexPages.StandardController(newQuoteLink);
        Pagereference p = page.VFP30_SynchronizeQuoteLink;
        Test.setCurrentPageReference(p); 
        VFC30_SynchronizeQuoteLink SynchronizeQuoteLink = new VFC30_SynchronizeQuoteLink(sc1);
        SynchronizeQuoteLink.redirectToQuoteLinkExternalSystem();
        newQuoteLink.TECH_IsSyncNeededForOpp__c=true;
        newQuoteLink.countryOfQuoting__c='France';
        newQuoteLink.quotingSystem__c=' DevisSOL';
        newQuoteLink.QuoteRefNumberinLegacy__c='123456';
        Database.Update(newQuoteLink);
        ApexPages.StandardController sc2= new ApexPages.StandardController(newQuoteLink);
        VFC30_SynchronizeQuoteLink viewQuoteLink = new VFC30_SynchronizeQuoteLink(sc2);
        viewQuoteLink.redirectToQuoteLinkExternalSystem();
        newQuoteLink.countryOfQuoting__c='Global';
        newQuoteLink.quotingSystem__c=Label.CLAPR15SLS63;
        Database.Update(newQuoteLink);  
        viewQuoteLink.redirectToQuoteLinkExternalSystem();
        newQuoteLink.quotingSystem__c=Label.CLAPR15SLS82;
        Database.Update(newQuoteLink);     
        viewQuoteLink.redirectToQuoteLinkExternalSystem();
        newQuoteLink.quoteStatus__c='denied';
        Database.Update(newQuoteLink);     
        viewQuoteLink.redirectToQuoteLinkExternalSystem();        
        newQuoteLink.TECH_IsBoundToBackOffice__c=true;
        
        
        sc1= new ApexPages.StandardController(newQuoteLink);
        p = page.VFP30_SynchronizeQuoteLink;
        Test.setCurrentPageReference(p); 
        newQuoteLink.TECH_IsSyncNeededForOpp__c=true;
        newQuoteLink.countryOfQuoting__c=System.Label.CLJUN13SLS04;
        newQuoteLink.quotingSystem__c=System.Label.CLJUN13SLS04;
        newQuoteLink.QuoteRefNumberinLegacy__c='123456789';
        Database.Update(newQuoteLink);
        SynchronizeQuoteLink = new VFC30_SynchronizeQuoteLink(sc1);
        SynchronizeQuoteLink.redirectToQuoteLinkExternalSystem();
        newQuoteLink.QuoteRefNumberinLegacy__c=null;
        Database.Update(newQuoteLink);
        SynchronizeQuoteLink.redirectToQuoteLinkExternalSystem();
        Test.stopTest();   
        
        ApexPages.StandardController sc3= new ApexPages.StandardController(newQuoteLink);
        VFC30_SynchronizeQuoteLink cpqQuoteLink = new VFC30_SynchronizeQuoteLink(sc3);
        newQuoteLink.countryOfQuoting__c='Global';
        newQuoteLink.quotingSystem__c=Label.CLAPR15SLS86;
        Database.Update(newQuoteLink);           
        cpqQuoteLink.redirectToQuoteLinkExternalSystem();        
    }
}