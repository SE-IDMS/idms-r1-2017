/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : March 2013
    Description : For PRM May 2013 Release:
                 1. To create Program Requiremtnfor Country PArtner Program when a new Global REq is added.
                 2. To update country program req when a global req is updated
    Modified Date: PRM Oct 2013 Release:
                1. When a new requirement is added to Program, pass the program to Accounts & Contacts as Pending
********************************************************************************************************************/
public class AP_PGR_CountryRequirementUpdate {
    
    public static void creatCountryRequirement(map<ID,list<ProgramRequirement__c>> mapLevelRequirement , list<ID> lstreqProgramID , set<Id> setRequirementID) {
        list<ID> lstreqLevelID = new list<ID>();
        list<ProgramRequirement__c> lstCountryPRGRequirement = new list<ProgramRequirement__c>();
        list<ProgramRequirement__c> lstExistingRequirement = new list<ProgramRequirement__c>();
       
        
        map<Id,ProgramLevel__c> mapPRGLevel = new map<Id,ProgramLevel__c>([Select p.PartnerProgram__r.GlobalPartnerProgram__c, p.PartnerProgram__c, p.GlobalProgramLevel__c From ProgramLevel__c p 
                                                WHERE p.GlobalProgramLevel__c in :mapLevelRequirement.keySet() and p.PartnerProgram__r.GlobalPartnerProgram__c in :lstreqProgramID limit 50000]);
/*
        // START OCT 2013: Get the current list of program & levels assigned to Accounts
        List<ProgramLevel__c> lstAccPartnerProg = new List<ProgramLevel__c>([SELECT Id,
                                                            (SELECT Id, Account__c, PartnerProgram__c, ProgramLevel__c FROM AccountAssignedPrograms__r),
                                                            (SELECT Id, AccountAssignedProgram__c, Contact__c, PartnerProgram__c, ProgramLevel__c FROM ContactAssignedPrograms__r WHERE Contact__r.PRMContact__c = true)
                                                            FROM ProgramLevel__c 
                                                            WHERE id IN :mapPRGLevel.keySet() ]);
        System.Debug('Existing Account Program Assignment: ' + lstAccPartnerProg);
        
        Map<Id, List<ACC_PartnerProgram__c>> mapAccountAssignedProg = new Map<Id, List<ACC_PartnerProgram__c>> ();
        Map<Id, List<ContactAssignedProgram__c>> mapContactAssignedProg = new Map<Id, List<ContactAssignedProgram__c>> ();
        
        for(ProgramLevel__c accapp : lstAccPartnerProg) {
            if (mapAccountAssignedProg.containsKey(accapp.id)) {
                mapAccountAssignedProg.get(accapp.id).addAll(accapp.AccountAssignedPrograms__r);
            } else
                mapAccountAssignedProg.put(accapp.id, accapp.AccountAssignedPrograms__r);

            if (mapContactAssignedProg.containsKey(accapp.id)) {
                mapContactAssignedProg.get(accapp.id).addAll(accapp.ContactAssignedPrograms__r);
            } else
                mapContactAssignedProg.put(accapp.id, accapp.ContactAssignedPrograms__r);
        }
        System.Debug('Account Assigned Program Map:' + mapAccountAssignedProg);
        System.Debug('Contact Assigned Program Map:' + mapContactAssignedProg);
        // END OCT 2013
*/
        if(setRequirementID!= null)
        {
            lstExistingRequirement = [Select id,partnerprogram__c,globalprogramrequirement__c,ProgramLevel__c from ProgramRequirement__c where globalprogramrequirement__c in :setRequirementID limit 50000];
            if(!lstExistingRequirement.isEmpty())
            {
               for(ProgramRequirement__c req : lstExistingRequirement)
                    mapPRGLevel.remove(req.ProgramLevel__c);
            }

        }
        List<AccountAssignedRequirement__c> lstAccAssignedReq = new List<AccountAssignedRequirement__c>();
        List<ContactAssignedRequirement__c> lstConAssignedReq = new List<ContactAssignedRequirement__c>();

        for(ProgramLevel__c prgLevel : mapPRGLevel.values()) {
            for(ProgramRequirement__c PRGRequirement: mapLevelRequirement.get(prgLevel.GlobalProgramLevel__c)) {
                ProgramRequirement__c newPRGRequirement = new ProgramRequirement__c();
                newPRGRequirement.Active__c = false;
                //newPRGRequirement.recordtypeid = Label.CLMAY13PRM14;
                newPRGRequirement.recordtypeid = PRGRequirement.recordtypeid;
                newPRGRequirement.RequirementCatalog__c = PRGRequirement.RequirementCatalog__c;
                newPRGRequirement.PartnerProgram__c = prgLevel.PartnerProgram__c;
                newPRGRequirement.ProgramLevel__c = prgLevel.Id;
                newPRGRequirement.GlobalProgramRequirement__c  = PRGRequirement.Id;
                lstCountryPRGRequirement.add(newPRGRequirement);                
            }
        }
        
        try {
            insert lstCountryPRGRequirement;
            Map<Id, ProgramRequirement__c> newCountryProgReq = new Map<Id, ProgramRequirement__c> (lstCountryPRGRequirement);
            
            lstCountryPRGRequirement = new List<ProgramRequirement__c>( [SELECT Id, Active__c, Recordtypeid, RequirementCatalog__c, PartnerProgram__c, ProgramLevel__c, GlobalProgramRequirement__c, Applicableto__c FROM ProgramRequirement__c WHERE Id IN :newCountryProgReq.keySet()]);
        /*
        // START OCT 2013: Prepare the list of AccountAssignedRequirements
            // For every account where a level is assigned, for every program requirement created
            // create an Account Assiged Requirement
            System.Debug('New Country Program Requirement: ' + lstCountryPRGRequirement);
            for(ProgramRequirement__c prc : lstCountryPRGRequirement) {
                List<ACC_PartnerProgram__c> accProg = mapAccountAssignedProg.get(prc.ProgramLevel__c);
                for (ACC_PartnerProgram__c acprg : accProg) {
                    AccountAssignedRequirement__c accreq = new AccountAssignedRequirement__c(
                            Account__c = acprg.Account__c,
                            AccountAssignedProgram__c = acprg.Id,
                            PartnerProgram__c = acprg.PartnerProgram__c,
                            ProgramLevel__c = acprg.ProgramLevel__c,
                            ProgramRequirement__c = prc.id,
                            RequirementStatus__c = System.Label.CLOCT13PRM20);

                    lstAccAssignedReq.add (accreq);
                }
                
                // TODO: for requirements where ApplicableTo Contact is set
                System.Debug('Contact Condition: ' + prc.Applicableto__c);
                if (prc.Applicableto__c == 'Contact') {
                    System.Debug('Contact Program Level: ' + prc.ProgramLevel__c);
                    System.Debug('Contact Assigned Prog: ' + mapContactAssignedProg.get(prc.ProgramLevel__c));
                    List<ContactAssignedProgram__c> conProg =  mapContactAssignedProg.get(prc.ProgramLevel__c);
                    for (ContactAssignedProgram__c cProg : conProg) {
                        if (prc.ProgramLevel__c == cProg.ProgramLevel__c) {
                            ContactAssignedRequirement__c conreq = new ContactAssignedRequirement__c(
                                Contact__c = cProg.Contact__c,
                                ContactAssignedProgram__c = cProg.id,
                                PartnerProgram__c = cProg.PartnerProgram__c, 
                                ProgramRequirement__c = prc.id,
                                RequirementStatus__c = System.Label.CLOCT13PRM20 );

                            lstConAssignedReq.add ( conreq );
                        }
                        
                    }
                }
            }
            System.Debug('New Account Assigned Requirement: ' + lstAccAssignedReq);
            insert lstAccAssignedReq;
            System.Debug('New Contact Assigned Requirement: ' + lstAccAssignedReq);
            insert lstConAssignedReq;
            // END OCT 2013
     */
        }
        catch(Exception e) {
          mapLevelRequirement.values().get(0).get(0).addError(Label.CLMAY13PRM44);
        }
    }
    
    //public static void updateCountryRequirement(list<ProgramRequirement__c> lstNewPRGRequirement)
    public static void updateCountryRequirement(map<Id,ProgramRequirement__c> mapNewGlobalPRGRequirement , map<Id,ProgramRequirement__c> mapOldGlobalPRGRequirement)
    {
      list<ProgramRequirement__c> lstChildRequirement = new list<ProgramRequirement__c>();
      String strFields = '';
      map<Id,ProgramRequirement__c> mapGlobalChildRequirement = new map<Id,ProgramRequirement__c>();
      try
      {        
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.ProgramRequirement__c.fields.getMap();
                   
        if (mapFields != null)
        {
            for (Schema.SObjectField ft : mapFields.values())// loop through all field tokens (ft)
            { 
                if (ft.getDescribe().isUpdateable())// field is updateable
                { 
                    strFields += ft.getDescribe().getName()+',';
                }
            }
        }
        
        if (strFields.endsWith(','))
        {
            strFields = strFields.substring(0,strFields.lastIndexOf(','));
        }
     
        set<Id> setGlobalRequirementID = new set<ID>();
        list<PRogramRequirement__c> lstReqToUpdate = new list<PRogramRequirement__c>();
        set<PRogramRequirement__c> setReqToUpdate = new set<PRogramRequirement__c>();
        setGlobalRequirementID.addAll(mapNewGlobalPRGRequirement.keySet());
        String childFetaureQuery = 'Select '+ strFields + ' , Mandatory__c,PArtnerProgram__r.PRogramStatus__c, ProgramLevel__r.LevelStatus__c FROM ProgramRequirement__c WHERE GlobalProgramRequirement__c in :setGlobalRequirementID limit 10000';
        lstChildRequirement = database.query(childFetaureQuery);
        /*for(ProgramFeatue__C prgreq : lstChildRequirement)
          mapGlobalChildRequirement.put(prgreq.GlobalProgramRequirement__c , prgreq);*/
      
      
      if(!lstChildRequirement.isEmpty())
          {
            for(PRogramRequirement__c childRequirement : lstChildRequirement)
            {   
                for (String str : mapFields.keyset()) 
                {
                    if(mapFields.get(str).getDescribe().isUpdateable() && mapNewGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str) != mapOldGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str))
                    {
                       if((str == Label.CLMAY13PRM24 && mapNewGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str) == false))//childRequirement.Mandatory__c == 'No')// && !((str == Label.CLMAY13PRM24 && mapNewGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str) == true) && (childRequirement.PArtnerProgram__r.PRogramStatus__c != Label.CLMAY13PRM21 || childRequirement.ProgramLevel__r.LevelStatus__c != Label.CLMAY13PRM19)))// Should not update Draft Country Program's or Draft LEvel's status to Active
                       {
                           //For July13 PRM Release. Global Status should not be passed to Country Requirements.
                           //childRequirement.put(str, mapNewGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str));
                           childRequirement.put('TECH_InactiveGlobalRequirement__c', true);
                           
                           setReqToUpdate.add(childRequirement);
                       }
                       else if(CS_GlobalReqtoCountryReqFields__c.getValues(str.toLowerCase()) == null)//childRequirement.Mandatory__c == 'No')// && !((str == Label.CLMAY13PRM24 && mapNewGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str) == true) && (childRequirement.PArtnerProgram__r.PRogramStatus__c != Label.CLMAY13PRM21 || childRequirement.ProgramLevel__r.LevelStatus__c != Label.CLMAY13PRM19)))// Should not update Draft Country Program's or Draft LEvel's status to Active
                       {
                           childRequirement.put(str, mapNewGlobalPRGRequirement.get(childRequirement.globalProgramRequirement__C).get(str));
                           setReqToUpdate.add(childRequirement);
                       }
                       /*else if(str == Label.CLMAY13PRM24)
                       {
                            childRequirement.put('TECH_InactiveGlobalReq__c', true);
                            lstReqToUpdate.add(childRequirement);
                       }*/
                       
                    }
                }
            }
          }  
          
          if(!setReqToUpdate.isEmpty())
          {
              lstReqToUpdate.addAll(setReqToUpdate);
              update lstReqToUpdate;
          }
      }
      catch(Exception e)
      {
      /*for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                mapNewGlobalPRGRequirement.values().get(0).addError(e.getDmlMessage(i)); 
                
                System.debug(e.getDmlMessage(i)); 
            } */
            mapNewGlobalPRGRequirement.values().get(0).addError('An error occured while updating child records. Please contact your system administrator');
            System.debug(e.getMessage());
      } 
    }
    
    /*
    Srinivas July13 BR-3082
    */
    public static void  updateAccountContactRequirements(List<ProgramRequirement__c> lstProgramReqs)
    {
      map<id,ProgramRequirement__c> mpPrgReqs = new map<id,ProgramRequirement__c>(lstProgramReqs);
      list<AccountAssignedRequirement__c> lstAAR = [SELECT RequirementStatus__c,Account__c,PartnerProgram__c,ProgramLevel__c,ProgramRequirement__c FROM AccountAssignedRequirement__c WHERE ProgramRequirement__c IN :lstProgramReqs];
      list<ContactAssignedRequirement__c> lstCAR = [SELECT ContactAssignedFeature__c,ContactAssignedProgram__c,PartnerProgram__c,ProgramLevel__c,ProgramRequirement__c,RequirementStatus__c FROM ContactAssignedRequirement__c WHERE ProgramRequirement__c IN :lstProgramReqs];
      
      for(AccountAssignedRequirement__c aar : lstAAR)
      {
        aar.RequirementStatus__c = Label.CLJUL13PRM21;
      }
      update lstAAR;

      for(ContactAssignedRequirement__c car : lstCAR)
      {
        car.RequirementStatus__c = Label.CLJUL13PRM21;
      }  
      update lstCAR;

    }

    //Oct13
    public static void newAcccountAssigendRequirements(List<ProgramRequirement__c> lstProReqs)
    {
        set<id> setProgLevelId = new set<id>();
        for(ProgramRequirement__c proreq : lstProReqs)
        {
          setProgLevelId.add(proreq.ProgramLevel__c);
        }

        List<AccountAssignedRequirement__c> lstExistinAccReqs = [select AccountAssignedProgram__c,ProgramRequirement__c,ProgramLevel__c from AccountAssignedRequirement__c WHERE ProgramRequirement__c =:lstProReqs];
        map<id,set<id>> mpPrIdtoExistingAAP = new map<id,set<id>>();
        for(AccountAssignedRequirement__c aareq : lstExistinAccReqs)
        {
          if(!mpPrIdtoExistingAAP.containsKey(aareq.ProgramRequirement__c))
              mpPrIdtoExistingAAP.put(aareq.ProgramRequirement__c, new set<id>{(aareq.AccountAssignedProgram__c)});
          else
              mpPrIdtoExistingAAP.get(aareq.ProgramRequirement__c).add(aareq.AccountAssignedProgram__c);  
        }  

        List<ContactAssignedRequirement__c> lstExistinContReqs = [select ContactAssignedProgram__c,ProgramRequirement__c,Contact__C from ContactAssignedRequirement__c WHERE ProgramRequirement__c =:lstProReqs];
        map<id,set<id>> mpPrIdtoExistingCAP = new map<id,set<id>>();
        for(ContactAssignedRequirement__c careq : lstExistinContReqs)
        {
          if(!mpPrIdtoExistingCAP.containsKey(careq.ProgramRequirement__c))
              mpPrIdtoExistingCAP.put(careq.ProgramRequirement__c, new set<id>{(careq.ContactAssignedProgram__c)});
          else
              mpPrIdtoExistingCAP.get(careq.ProgramRequirement__c).add(careq.ContactAssignedProgram__c);  
        } 

        // START OCT 2013: Get the current list of program & levels assigned to Accounts
        List<ProgramLevel__c> lstAccPartnerProg = new List<ProgramLevel__c>([SELECT Id,
                                  (SELECT Id, Account__c, PartnerProgram__c, ProgramLevel__c FROM AccountAssignedPrograms__r),
                                  (SELECT Id, AccountAssignedProgram__c, Contact__c, PartnerProgram__c, ProgramLevel__c FROM ContactAssignedPrograms__r WHERE Contact__r.PRMContact__c = true)
                                  FROM ProgramLevel__c 
                                  WHERE id IN :setProgLevelId ]);
        System.Debug('Existing Account Program Assignment: ' + lstAccPartnerProg);
        
        Map<Id, List<ACC_PartnerProgram__c>> mapAccountAssignedProg = new Map<Id, List<ACC_PartnerProgram__c>> ();
        Map<Id, List<ContactAssignedProgram__c>> mapContactAssignedProg = new Map<Id, List<ContactAssignedProgram__c>> ();
        
        for(ProgramLevel__c accapp : lstAccPartnerProg) {
          if (mapAccountAssignedProg.containsKey(accapp.id)) {
            mapAccountAssignedProg.get(accapp.id).addAll(accapp.AccountAssignedPrograms__r);
          } else
            mapAccountAssignedProg.put(accapp.id, accapp.AccountAssignedPrograms__r);

          if (mapContactAssignedProg.containsKey(accapp.id)) {
            mapContactAssignedProg.get(accapp.id).addAll(accapp.ContactAssignedPrograms__r);
          } else
            mapContactAssignedProg.put(accapp.id, accapp.ContactAssignedPrograms__r);
        }
        System.Debug('Account Assigned Program Map:' + mapAccountAssignedProg);
        System.Debug('Contact Assigned Program Map:' + mapContactAssignedProg);
        // END OCT 2013

        // START OCT 2013: Prepare the list of AccountAssignedRequirements
        // For every account where a level is assigned, for every program requirement created
        // create an Account Assiged Requirement
        List<AccountAssignedRequirement__c> lstAccAssignedReq = new List<AccountAssignedRequirement__c>();
        List<ContactAssignedRequirement__c> lstConAssignedReq = new List<ContactAssignedRequirement__c>();

        System.Debug('New Country Program Requirement: ' + lstProReqs);
        for(ProgramRequirement__c prc : lstProReqs) 
        {
            List<ACC_PartnerProgram__c> accProg = mapAccountAssignedProg.get(prc.ProgramLevel__c);
            for (ACC_PartnerProgram__c acprg : accProg) 
            {
              if(mpPrIdtoExistingAAP.get(prc.id) == null || !mpPrIdtoExistingAAP.get(prc.id).contains(acprg.id))// AAR does not exists alrady
              {  AccountAssignedRequirement__c accreq = new AccountAssignedRequirement__c(
                    Account__c = acprg.Account__c,
                    AccountAssignedProgram__c = acprg.Id,
                    PartnerProgram__c = acprg.PartnerProgram__c,
                    ProgramLevel__c = acprg.ProgramLevel__c,
                    ProgramRequirement__c = prc.id,
                    RequirementCatalog__c = prc.RequirementCatalog__c,
                    RequirementStatus__c = System.Label.CLOCT13PRM20);

                lstAccAssignedReq.add (accreq);
              }  
            }
            
            // TODO: for requirements where ApplicableTo Contact is set
            System.Debug('Contact Condition: ' + prc.Applicableto__c);
            if (prc.Applicableto__c == 'Contact') 
            {
                System.Debug('Contact Program Level: ' + prc.ProgramLevel__c);
                System.Debug('Contact Assigned Prog: ' + mapContactAssignedProg.get(prc.ProgramLevel__c));

                List<ContactAssignedProgram__c> conProg =  mapContactAssignedProg.get(prc.ProgramLevel__c);
                for (ContactAssignedProgram__c cProg : conProg) 
                {
                  if(mpPrIdtoExistingCAP.get(prc.id) == null || !mpPrIdtoExistingCAP.get(prc.id).contains(cprog.id))// if no Contact Assigned Requirement exists for the PRog Reqs
                    if (prc.ProgramLevel__c == cProg.ProgramLevel__c ) {
                      ContactAssignedRequirement__c conreq = new ContactAssignedRequirement__c(
                        Contact__c = cProg.Contact__c,
                        ContactAssignedProgram__c = cProg.id,
                        PartnerProgram__c = cProg.PartnerProgram__c, 
                        ProgramRequirement__c = prc.id,
                         RequirementCatalog__c = prc.RequirementCatalog__c,
                        RequirementStatus__c = System.Label.CLOCT13PRM20 );
                       
                      lstConAssignedReq.add ( conreq );
                    }
                    
                }
            }
        }
        System.Debug('New Account Assigned Requirement: ' + lstAccAssignedReq);
        insert lstAccAssignedReq;
        System.Debug('New Contact Assigned Requirement: ' + lstAccAssignedReq);
        insert lstConAssignedReq;
        // END OCT 2013

    }//End of method
//-------------------------------------------------------------------------------------------------------------
// October Release 2014
//-------------------------------------------------------------------------------------------------------------
    public static void  validateDuplicateProgramRequirement(List<ProgramRequirement__c> lstProgramReqmnt)
    {
        map<String,ProgramRequirement__c> mapPrgRequirement = new map<String,ProgramRequirement__c>();
        Set<id> setLevel = new Set<id>();
        Set<id> setReqCatalog = new Set<id>();
        
        for(ProgramRequirement__c aProgramReq :lstProgramReqmnt)
        {
            system.debug('***** prog req ***'+aProgramReq);
            if(mapPrgRequirement.containsKey(aProgramReq.ProgramLevel__c+'-'+aProgramReq.RequirementCatalog__c))
            {
                system.debug('***** prog req key***'+aProgramReq.ProgramLevel__c+'-'+aProgramReq.RequirementCatalog__c+'-'+aProgramReq.id);
                aProgramReq.addError(System.label.CLOCT14PRM23);
                
            }       
            else
                mapPrgRequirement.put(aProgramReq.ProgramLevel__c+'-'+aProgramReq.RequirementCatalog__c,aProgramReq);   


            setLevel.add(aProgramReq.ProgramLevel__c);
            setReqCatalog.add(aProgramReq.RequirementCatalog__c);
        
        }
        
        list<ProgramRequirement__c> listOldReq = [Select id,ProgramLevel__c,RequirementCatalog__c from ProgramRequirement__c
                                                    where ProgramLevel__c in :setLevel and RequirementCatalog__c in :setReqCatalog ];
                                                    
        for(ProgramRequirement__c aOldPrgReq :listOldReq)
        {
        
            if(mapPrgRequirement.containsKey(aOldPrgReq.ProgramLevel__c+'-'+aOldPrgReq.RequirementCatalog__c))
            {
                system.debug('***** prog req key***'+aOldPrgReq.ProgramLevel__c+'-'+aOldPrgReq.RequirementCatalog__c+'-'+aOldPrgReq.id);
                if(aOldPrgReq.id != mapPrgRequirement.get(aOldPrgReq.ProgramLevel__c+'-'+aOldPrgReq.RequirementCatalog__c).id)
                    mapPrgRequirement.get(aOldPrgReq.ProgramLevel__c+'-'+aOldPrgReq.RequirementCatalog__c).addError(System.label.CLOCT14PRM23); 
            }       
            else
                mapPrgRequirement.put(aOldPrgReq.ProgramLevel__c+'-'+aOldPrgReq.RequirementCatalog__c,aOldPrgReq);  
        
        
        }
        
        
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------

}