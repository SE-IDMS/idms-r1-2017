public class AP31_CoachingVisitFormSharing {
    
    public static void shareCVF(List<SFE_CoachingVisitForm__c> CVFstoUpdate){
    //*********************************************************************************
            // Method Name      : removeShare 
            // Purpose          :This is the public static method that is called from the trigger (CoachingVisitFormAfterInsertUpdate)
                                // This method in turn calls 2 methods 
                                // addShare  - for adding sharing with the Salesperson, and 
                                // removeShare –for removing an existing Salesperson (if already in the sharing)

            // Created by       : Ramesh Rajasekaran - Global Delivery Team
            // Date created     : 9th November 2011
            // Modified by      :
            // Date Modified    :
            // Remarks          : For Dec - 11 Release
            ///********************************************************************************/
            
            System.Debug('****AP31_CoachingVisitFormSharing shareCVF   Started****');
            
            if(CVFstoUpdate !=null && CVFstoUpdate.size()>0)
            removeShare(CVFstoUpdate);
            
            System.Debug('****AP31_CoachingVisitFormSharing shareCVF   Finished****');
    }   
     
    public static void removeShare(List<SFE_CoachingVisitForm__c> CVFstoUpdate){
        //*********************************************************************************
        // Method Name      : removeShare 
        // Purpose          : To remove Sharing from the CoachingVisit Form 
        // Created by       : Ramesh Rajasekaran - Global Delivery Team
        // Date created     : 9th November 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP31_CoachingVisitFormSharing removeShare   Started****');
                
        if(CVFstoUpdate != null && CVFstoUpdate.Size() > 0){
            Map<string,SFE_CoachingVisitForm__c> mapCVFsToUpdate = new Map<string,SFE_CoachingVisitForm__c>();
            List<SFE_CoachingVisitForm__Share> lstCVFsCurrentShare = new List<SFE_CoachingVisitForm__Share>();
            List<SFE_CoachingVisitForm__Share> lstSharingToRemove = new List<SFE_CoachingVisitForm__Share>();
            List<Id> CoachingvisitformList = new List<Id>();
            
            for(SFE_CoachingVisitForm__c CoachingVisitformObj : CVFstoUpdate){
            string objKey = CoachingVisitformObj.id+''+CoachingVisitformObj.SalesRep__c;  
            mapCVFsToUpdate.put(objKey,CoachingVisitformObj);
            CoachingvisitformList.add(CoachingVisitformObj.Id);
            }
            
            if(CoachingvisitformList != null && CoachingvisitformList.size()>0)
            lstCVFsCurrentShare = [select parentID,UserOrGroupId from SFE_CoachingVisitForm__Share where parentID IN : CoachingvisitformList and AccessLevel = 'Read'];
            
            
            if(lstCVFsCurrentShare != null && lstCVFsCurrentShare.size() >0){
                for(SFE_CoachingVisitForm__Share shareObj:lstCVFsCurrentShare){
                    string matchingKey = shareObj.ParentId+''+shareObj.UserOrGroupId;
                    if(mapCVFsToUpdate.containsKey(matchingKey))
                    mapCVFsToUpdate.remove(matchingKey);                    
                    else
                    lstSharingToRemove.add(shareObj);           
                }
            }
                    
            if(lstSharingToRemove != null && lstSharingToRemove.size()>0)
            if(lstSharingToRemove.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                 System.Debug('######## AP31_CoachingVisitFormSharing-Share Error Deleting');
            }
            else{
                Database.DeleteResult[] CoachingVisitShareDeleteResult = Database.delete(lstSharingToRemove);
                for(Database.DeleteResult dr: CoachingVisitShareDeleteResult){
                    if(!dr.isSuccess()){
                        Database.Error err = dr.getErrors()[0];
                        System.Debug('######## AP31_CoachingVisitFormSharing Error Deleting : '+err.getStatusCode()+' '+err.getMessage());
                    }
                }
            }
            
            // Calling AddShare Method
            
            if(mapCVFsToUpdate!= null && mapCVFsToUpdate.size()>0){
                addShare(mapCVFsToUpdate); 
            }           
        }   
        System.Debug('****AP31_CoachingVisitFormSharing removeShare   Finished****');
    }
    
    private static void addShare(Map<string,SFE_CoachingVisitForm__c> CVFstoShare){
   // private static void addShare(List<SFE_CoachingVisitForm__c> CVFstoShare){
    //*********************************************************************************
            // Method Name      : addShare 
            // Purpose          : To add Sharing to the CoachingVisit Form Share object
            // Created by       : Ramesh Rajasekaran - Global Delivery Team
            // Date created     : 9th November 2011
            // Modified by      :
            // Date Modified    :
            // Remarks          : For Dec - 11 Release
            ///********************************************************************************/
                                    
            System.Debug('****AP31_CoachingVisitFormSharing addShare   Started****');
            if(CVFstoShare != null && CVFstoShare.Size() > 0){              
            List<SFE_CoachingVisitForm__Share>  lstCVFShare = new List<SFE_CoachingVisitForm__Share>();
            
            for(SFE_CoachingVisitForm__c CoachingVisitformObj:CVFstoShare.values()){
                SFE_CoachingVisitForm__Share CoachingVisitFormShare = new SFE_CoachingVisitForm__Share();
                CoachingVisitFormShare.ParentId = CoachingVisitformObj.Id;
                CoachingVisitFormShare.UserOrGroupId = CoachingVisitformObj.SalesRep__c;
                CoachingVisitFormShare.AccessLevel = 'Read';            
                CoachingVisitFormShare.RowCause = Schema.SFE_CoachingVisitForm__Share.rowCause.SalespersonApexShare__c;//'Salesperson_Apex_Share';
                lstCVFShare.add(CoachingVisitFormShare);
            }
            
            if(lstCVFShare.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                 System.Debug('######## AP31_CoachingVisitFormSharing-Share Error Inserting : '+ Label.CL00264);
            }
            else{
                Database.SaveResult[] CoachingVisitShareInsertResult = Database.insert(lstCVFShare, false);
                for(Database.SaveResult sr: CoachingVisitShareInsertResult){
                    if(!sr.isSuccess()){
                        Database.Error err = sr.getErrors()[0];
                        System.Debug('######## AP31_CoachingVisitFormSharing-Share Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                    }
                }
            }
        }   
        System.Debug('****AP31_CoachingVisitFormSharing addShare   Finished****');
            
    }       
}