global class ListViewDefinition {
        public List<Condition> conditions;
        public List<String> columns;
        public String overallcondition;  
	    public String statuscondition;
    
global class Condition{
        public String appdatafieldmap;
        public String displayname;
        public String fieldtype;
        public String value;
        public Integer index; 
        public String type;	    
    }
    
    global void initialize(){
        ListViewDefinition mydefinition = new ListViewDefinition();
        List<ListViewDefinition.Condition> conditions = new List<ListViewDefinition.Condition>();
        ListViewDefinition.Condition condition1 = new ListViewDefinition.Condition();        
        ListViewDefinition.Condition condition2 = new ListViewDefinition.Condition();
        
        condition1.appdatafieldmap='textfield1__c';
        condition1.displayname='Text';
        condition1.fieldtype='Text';
        condition1.value='Text';
        condition1.index=1; 
        
        condition2.appdatafieldmap='textfield2__c';
        condition2.displayname='Text';
        condition2.fieldtype='Text';
        condition2.value='Text';
        condition2.index=1; 
        conditions.add(condition1);
        conditions.add(condition2);
        
        mydefinition.conditions = conditions;        
        mydefinition.columns=new List<String>{'textfield1__c','textfield2__c'};
        mydefinition.overallcondition='1 AND 2';
        mydefinition.statuscondition='';
    }
}