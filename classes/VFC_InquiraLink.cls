global with sharing class VFC_InquiraLink{ 
    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Case objTempCase{get; set;}
    
    global class InquiraOuterWarpper{
        global String message;
        global List<InquiraResultWarpper> lstInquiraResultWarpper;
    }
    
    global class InquiraResultWarpper{
        global boolean isSelected;
        global InquiraFAQ__c objInquiraFAQ;
        global String strInquiraDate;
    }
    global static Set<String> setFAQId= new Set<String>();
    //global static Id caseId;
    global static boolean isExistinFAQPopulated=false;
    
    public VFC_InquiraLink(ApexPages.StandardController controller){

        Case caseRecord=(Case)controller.getRecord();                        
        if(caseRecord.id!=null){
            caseRecord=[Select Id, CaseNumber, Subject,CommercialReference__c,Family__c,Contact.CorrespLang__c from Case where Id=:caseRecord.Id];
            String searchKeyword = caseRecord.Subject.trim();          
            if (caseRecord.CommercialReference__c!=null)
                searchKeyword = searchKeyword +' ' + caseRecord.CommercialReference__c.trim();   
               
            if (caseRecord.Family__c != null)
                searchKeyword = searchKeyword + ' ' + caseRecord.Family__c;
                
            if(searchKeyword!=null && searchKeyword!='')
                pageParameters.put('searchKeyword',searchKeyword);            
                
            if (caseRecord.Contact.CorrespLang__c != null)
                pageParameters.put('correspLanguage',caseRecord.Contact.CorrespLang__c);
            
            if (caseRecord.CaseNumber != null)
                pageParameters.put('CaseNumber',caseRecord.CaseNumber);

            if (caseRecord.Id != null){
                pageParameters.put('ID',caseRecord.Id);
                //caseId = caseRecord.Id;
            }
            isExistinFAQPopulated=false;                    
        }
    }
    
    global static boolean checkExistingFAQ(Id caseId, String strFAQId) {
        boolean blnReturn=false;
        //System.debug('Inside checkExistingFAQ() caseId:  ' + caseId);
        System.debug('Inside checkExistingFAQ() isExistinFAQPopulated: ' + isExistinFAQPopulated);
        if(!isExistinFAQPopulated){
            List<InquiraFAQ__c> lstInquiraFAQ = new List<InquiraFAQ__c>([Select Id, Name from InquiraFAQ__c where Case__c=:caseId]); 
            if(lstInquiraFAQ.size()>0){
                for(InquiraFAQ__c objInquira:lstInquiraFAQ){
                    setFAQId.add(objInquira.Name);
                }
            }
            isExistinFAQPopulated=true;
        }
        if(setFAQId.contains(strFAQId))
            blnReturn=true;
        return blnReturn;       
    }
    
    @RemoteAction
    global static List<String> getContactCorspLanguage() {
        Schema.DescribeFieldResult contactCorrespondingLanguageField= Contact.CorrespLang__c.getDescribe();
        List<Schema.PicklistEntry> lstLanguages= contactCorrespondingLanguageField.getPicklistValues();
        //display in alphabetical order
        List<String> lstSortResults=new List<String>();       
        for(Schema.PicklistEntry strLanguage: lstLanguages){
            lstSortResults.add(strLanguage.getLabel()+':'+strLanguage.getValue());
        }
        lstSortResults.sort();
        return lstSortResults;       
    }
    
    @RemoteAction
    global static Country__c getInquiraCountry(String strCaseNumber) {
        Country__c returnCountry=new Country__c();
        List<Case> lstCase = new List<Case>([Select Id, Contact.Country__c, CCCountry__c, Account.Country__c, Contact.Country__r.Name, Account.Country__r.Name, CCCountry__r.Name from Case where CaseNumber =: strCaseNumber LIMIT 1]);
        if(lstCase.size()>0){
            Case objCase = lstCase[0];
            if(objCase.Contact !=null && objCase.Contact.Country__c !=null){
                returnCountry =new Country__c(Id=objCase.Contact.Country__c,name=objCase.Contact.Country__r.Name);
            }
            else if(objCase.Account !=null && objCase.Account.Country__c !=null){
                returnCountry =new Country__c(Id=objCase.Account.Country__c,name=objCase.Account.Country__r.Name);
            }
            else{
                returnCountry =new Country__c(Id=objCase.CCCountry__c,name=objCase.CCCountry__r.Name);
            }
        }
        return returnCountry;       
    }
    @RemoteAction
    global static List<CS_InquiraBUEntities__c> getBUEntities(){
        List<CS_InquiraBUEntities__c> lstInquiraBUEntities = new List<CS_InquiraBUEntities__c>();
        
        // Find all the BU Entitie in the custom setting
        Map<String, CS_InquiraBUEntities__c> mapBUEntities = CS_InquiraBUEntities__c.getAll();
        lstInquiraBUEntities = mapBUEntities.values();
        lstInquiraBUEntities.sort();
        return lstInquiraBUEntities;
    }
    
    @RemoteAction
    global static Database.SaveResult[] attachFAQ(Id caseId, List<String> selectedFAQ){
        //Added as part of <BR-7552> Begin
        List<TeamAgentMapping__c> tmlist = [Select Id,CCTeam__r.Name,CCAgent__c From  TeamAgentMapping__c Where DefaultTeam__c = true and CCAgent__c = :UserInfo.getUserId()];
        //Added as part of <BR-7552> End
        List<InquiraResultWarpper> lstSelectedInquiraResultWarpper = new List<InquiraResultWarpper>();
        List<InquiraFAQ__c> lstInquiraFAQ = new List<InquiraFAQ__c>();
        List<Database.SaveResult> lstSaveResult= new List<Database.SaveResult>();
        System.debug('selectedFAQ: ' + selectedFAQ);
        for(String strFAQ   :selectedFAQ){
            InquiraResultWarpper objInquiraResultWarpper = (InquiraResultWarpper) JSON.deserialize(strFAQ, InquiraResultWarpper.class);
            if(!checkExistingFAQ(caseId, objInquiraResultWarpper.objInquiraFAQ.Name)){
                objInquiraResultWarpper.objInquiraFAQ.URL__c = objInquiraResultWarpper.objInquiraFAQ.URL__c.unEscapeXML();
                objInquiraResultWarpper.objInquiraFAQ.InfoCenterURL__c = objInquiraResultWarpper.objInquiraFAQ.InfoCenterURL__c.unEscapeXML();
                objInquiraResultWarpper.objInquiraFAQ.Case__c= caseId;
                if(tmlist.size()>0){
                    objInquiraResultWarpper.objInquiraFAQ.Agent_Team__c=tmlist[0].CCTeam__r.Name ;//added as part of <BR-7552>
                }
                lstInquiraFAQ.add(objInquiraResultWarpper.objInquiraFAQ);
            }
        }
        if(lstInquiraFAQ.size()>0){
            lstSaveResult = Database.insert(lstInquiraFAQ,false);
        }
        for(DataBase.SaveResult SaveResult:lstSaveResult){
            System.Debug('#### DML Errors : ' + SaveResult.getErrors() ); 
        }
        //Case objCase = new Case(Id=caseId);
        Case objCase = [Select Id, AnswerToCustomer__c from Case where Id=:caseId];
        AP12_CaseIntegrationMethods.UpdateCaseWithInquiraFaQ(objCase);
        return lstSaveResult;
    }
    
    @RemoteAction
    global static InquiraOuterWarpper performInquiraSearch(Id caseId, String strKeyword,String strCountryISO,String strLanguage,String strBUEntity){
        InquiraOuterWarpper objInquiraOuterWarpper = new InquiraOuterWarpper();
        List<InquiraResultWarpper> lstInquiraResultWarpper = new List<InquiraResultWarpper>();
        InquiraFAQ__c objInquira;
        InquiraResultWarpper objInquiraResultWarpper;
        String strCountryCodeISO='';
        final Integer TIMEOUT = Integer.valueOf(System.Label.CLMAY13CCC39);
        final String INQUIRA_ENDPOINT     = Label.CLMAY14CCC25;
        final String INQUIRA_CERTIFICATE  = Label.CLMAY14CCC23;
        WS_Inquira.faqCKMSearchResponse objSearchFaqResponse = new WS_Inquira.faqCKMSearchResponse();
        
        if(strCountryISO!='' && strCountryISO!=null){
            List<Country__c> lstCountry = new List<Country__c>([select Id, CountryCode__c from Country__c where Name =: strCountryISO LIMIT 1]);
            if(lstCountry.size()>0)
               strCountryCodeISO = lstCountry[0].CountryCode__c;
        }
        
        WS_Inquira.localeBean locale = new WS_Inquira.localeBean();
        locale.isoCountry   = strCountryCodeISO.toUpperCase();
        locale.isoLanguage  = strLanguage.toLowerCase();
                             
        WS_Inquira.FaqCKMServiceImplPort InquiraConnection = new WS_Inquira.FaqCKMServiceImplPort();
        InquiraConnection.endpoint_x         = INQUIRA_ENDPOINT;
        InquiraConnection.ClientCertName_x   =INQUIRA_CERTIFICATE;
        //InquiraConnection.endpoint_x         ='http://oreo-sqe.dev.schneider-electric.com/faq-service-logic/FaqCKMServiceImplService';
        InquiraConnection.timeout_x = TIMEOUT;
        
        String userID = UserInfo.getUserName();
        userID = (userID != null && userID.contains('@')) ? userID.Left(userID.indexOf('@')).toUpperCase() : userID;                    
        try{
            System.debug(InquiraConnection);
            if(!Test.isRunningTest())                  
                objSearchFaqResponse = InquiraConnection.searchFaq(strKeyword,strBUEntity,locale,userID);
                //objSearchFaqResponse = Utils_DummyDataForTest.searchFaq(false,false);
            else
                objSearchFaqResponse = Utils_DummyDataForTest.searchFaq(false,false);
            
            if(objSearchFaqResponse!=null){
                objInquiraOuterWarpper.message = objSearchFaqResponse.message;
                if(objSearchFaqResponse!=null && objSearchFaqResponse.results!=null && objSearchFaqResponse.results.size()>0){
                    for(WS_Inquira.faqCKMBean objFAQBean:objSearchFaqResponse.results){
                        objInquira = new InquiraFAQ__c();
    
                        objInquira.Name             = objFAQBean.id;
                        String strTitle             = objFAQBean.title;
                        objInquira.Title__c         = (strTitle.length()>255) ? strTitle.substring(0,252)+ '...': strTitle;
                        objInquira.Excerpt__c       = objFAQBean.excerpt + System.Label.CLMAY14CCC22;
                        objInquira.InfoCenterURL__c = objFAQBean.url;
                        objInquira.Date__c          = objFAQBean.date_x.date();
                        objInquira.Visibility__c    = objFAQBean.securityLevel;
                        objInquira.URL__c           = objFAQBean.public_Url;
                        
                        objInquiraResultWarpper = new InquiraResultWarpper();
                        objInquiraResultWarpper.objInquiraFAQ = objInquira;
                        objInquiraResultWarpper.isSelected=checkExistingFAQ(caseId,objInquira.Name);
                        objInquiraResultWarpper.strInquiraDate = objInquira.Date__c.format();
                        lstInquiraResultWarpper.add(objInquiraResultWarpper);    
                    }
                }
                else if(objSearchFaqResponse!=null && objSearchFaqResponse.results!=null && objSearchFaqResponse.results.size()==0){
                    objInquiraOuterWarpper.message =System.Label.CL00013;
                }
                objInquiraOuterWarpper.lstInquiraResultWarpper = lstInquiraResultWarpper;
            }
        }
        catch(CalloutException callOutEx){
            String strException=callOutEx.getMessage();
            String strExceptionMessage ='';
            if(strException.containsIgnoreCase(System.Label.CLMAY13CCC49)){
                strExceptionMessage = System.Label.CLMAY14CCC19;
            }
            else if(strException.containsIgnoreCase(System.Label.CLMAY13CCC50)){
                strExceptionMessage = System.Label.CLMAY14CCC20;
            }
            else if(strException.containsIgnoreCase(System.Label.CLMAY13CCC51)){
                strExceptionMessage = System.Label.CLMAY14CCC21;
            }
            else{
                strExceptionMessage =callOutEx.getMessage();
            }
            if(objInquiraOuterWarpper==null)
                objInquiraOuterWarpper = new InquiraOuterWarpper();
            objInquiraOuterWarpper.message =strExceptionMessage;
        }
        catch(Exception ex){
            if(objInquiraOuterWarpper==null)
                objInquiraOuterWarpper = new InquiraOuterWarpper();
            objInquiraOuterWarpper.message =ex.getMessage();
        }
        return objInquiraOuterWarpper;
    }
}