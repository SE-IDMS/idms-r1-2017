/*
Author:Siddharth
Purpose:Test Methods for the class VFC_SPARecordTypeSelection
Methods:    Constructor,                       

*/
@isTest
private class VFC_SPARecordTypeSelection_TEST
{

    static testMethod void runPositiveTestCases() {
        SPARequestLineItem__c lineitem=new SPARequestLineItem__c();
        ApexPages.StandardController controller=new ApexPages.StandardController(lineitem);
        VFC_SPARecordTypeSelection rec=new VFC_SPARecordTypeSelection(controller);
        ApexPages.CurrentPage().getParameters().put('param1','value1');
        ApexPages.CurrentPage().getParameters().put('param2','value2');
        rec.choose();
        rec.generateRecordTypeOptions();
        rec.selectedRecordType='some recordtype id';
        rec.choose();
    }
}