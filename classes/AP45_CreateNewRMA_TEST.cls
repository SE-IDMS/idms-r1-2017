/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | AP45_CreateNewRMA_TEST                                                             |
|                       |                                                                                    |
|     - Object(s)       | RMA__c                                                                             |
|     - Description     |   - Test method for the Apex class AP45_CreateNewRMA                               |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
private class AP45_CreateNewRMA_TEST
{
    static testMethod void testAP45_CreateNewRMA()
    {
       System.debug('#### START test method for testAP45_CreateNewRMA ####');

       ///CCC Country
       Country__c country = Utils_TestMethods.createCountry();
       insert country;
       System.debug('>>>>> country = ' + country);

       //Account
       Account acc = Utils_TestMethods.createAccount();
       insert acc;

       //Contact
       Contact con = Utils_TestMethods.createContact(acc.Id,'TestContact');
       insert con;

       //Case
       Case cse = Utils_TestMethods.createCase(acc.Id, con.Id, 'Open');
       cse.CCCountry__c = country.Id;
       insert cse;
       System.debug('>>>>> cse = ' + cse);

       //Insert a list of RMAs
       List<RMA__c> RMAList = new List<RMA__c>();
       for(integer i=0; i<200; i++)
       {
          RMA__c aRMA = new RMA__c(Case__c            = cse.Id,
                                   Approver__c        = UserInfo.getUserId(),
                                   AccountName__c     = acc.Id,
                                   ContactName__c     = con.Id,
                                   ReturnType__c      = 'Commercial',
                                   AccountStreet__c   = 'the Account Address - Street',
                                   AccountCity__c     = 'the Account Address - City',
                                   AccountZipCode__c  = '12345',
                                   AccountCountry__c  = country.Id);
          RMAList.add(aRMA);
       }

       Database.Saveresult[] resRMAList = Database.insert(RMAList);
       System.debug('>>>>> resRMAList = ' + resRMAList);

       //5 - Check Results
       Set<Id> RMAIDSet = new Set<Id>();

       for(Database.Saveresult aResult : resRMAList)
       {
          RMAIDSet.add(aResult.getId());
       }
       System.debug('>>>>> RMAIDSet = ' + RMAIDSet);

       List<RMA__c> RMAList2 = [SELECT
                                  Id,
                                  RecordTypeId
                                FROM RMA__c
                                WHERE Id IN :RMAIDSet];
       system.debug('>>>>> RMAList2 = ' + RMAList2);

       for(RMA__c resultRMA : RMAList2)
       {
          System.assertEquals(resultRMA.RecordTypeId, Label.CL10203);
       }

       System.debug('#### END   test method for testAP45_CreateNewRMA_KEY ####');
    }
}