/*    
        This web service need to be able to search for an existing account in bFO based on bFOAccountId, SEAccountId, combination of LegacyName and LegacyNumber.
        if a unique account is not found, then create a new account with duplicate check of bFO and return a bFO ID (if account created).
*/

global class WS_Account{

    // web service Account request input fields
    global class bFOAccount{
        webservice String bFOAccountID;
        webservice String publisherID;
        webservice String legacyAccountID;
        webservice String SEAccountID;
        webservice String AccountName;
        webservice String AccountLocalName;
        webservice String PhysicalStreet;
        webservice String AdditionalAddress;
        webservice String PhysicalZipCode;
        webservice String PhysicalCity;
        webservice String PhysicalStateCode;
        webservice String PhysicalCountryCode;
        webservice String POBox;
        webservice String StreetLocalLang;
        webservice String LocalAdditionalAddress;
        webservice String LocalCity;
        webservice String CurrencyIsoCode;
        webservice String AccType;
        webservice String ClassLevel1;
        webservice String MarketSegment;
        webservice String MarketSubSegment;
        webservice String LeadingBusiness;
        webservice String OwnerSESAID;
        webservice String RecordType;
    }

    // webservice Account response output fields
    global class AccountResult{
        webservice Boolean success;
        webservice String type;
        webservice String errorMessage;
        webservice Id bFOAccountID;
        webservice String legacyAccountID;
        webservice String publisherID;
        webservice String SEAccountID;
    }
    
   //description : main method to search for an existing account and create a new account (if necessary) in frontoffice and return a bFO ID to backoffice.
   //input : list of bFOAccount
   //output: list of AccountResult
    webService static List<AccountResult> bulkCreateAccounts(List<bFOAccount> AccountList){

        List<AccountResult> resultList = new List<AccountResult>();

        List<bFOAccount> accBOLst = new List<bFOAccount>();
        List<bFOAccount> accDeDupToBeProcessed = new List<bFOAccount>();
        List<Account> existingSEIdList = new List<Account>();
        List<Account> forceInsertAcctLst = new List<Account>();
        List<Account> accLstToInsert = new List<Account>();
        List<LegacyAccount__c> legAcctsToInsert = new List<LegacyAccount__c>();
        Set<String> accDeDupOwnerIds = new Set<String>();
        Set<String> accDeDupStateCodes = new Set<String>();
        Set<String> accDeDupCountryCodes = new Set<String>();
        Map<String,bFOAccount> accbFOIdMap = new Map<String,bFOAccount>();
        Map<String,bFOAccount> accSEIdMap = new Map<String,bFOAccount>();
        Map<String,bFOAccount> accLegacyIdMap = new Map<String,bFOAccount>();
        Map<String,bFOAccount> accPubIdMap = new Map<String,bFOAccount>();
        Map<Id,Account> existingbFOIdMap = new Map<Id,Account>();
        Map<String,Account> existingSEIdMap = new Map<String,Account>();
        Map<Id,LegacyAccount__c> existingLegacyPubIdMap = new Map<Id,LegacyAccount__c>();
        Map<String,LegacyAccount__c> existingLegAccMap = new Map<String,LegacyAccount__c>();

        // Check if Account exists in bFO with same 1.bFOAccountId 2.SEAccountID 3.LegacyName and LegacyNumber comnination
        // If not then create account in bFO with Duplicate rule check

        if(!AccountList.isEmpty())
        {
            for(bFOAccount accnt : AccountList)
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account passed :- '+accnt);
                //Check if values exist in the mandatory fields of the bFOAccount record 
                MandatoryFields mf = new MandatoryFields();
                mf = CreateMandataryCheck(accnt);
                if( mf.isMFPass)                
                {
                    System.debug('WS_Account.bulkCreateAccounts :- Passed Mandatory fields check ');
                    //check if bFOAccountID value sent?
                    if(String.isNotBlank(accnt.bFOAccountID))
                    {
                        System.debug('WS_Account.bulkCreateAccounts :- Added to bFOID exist check - ');
                        accbFOIdMap.put(accnt.bFOAccountID,accnt);
                    }
                    //check if SEAccountID value sent?
                    else if(String.isNotBlank(accnt.SEAccountID))
                    {
                        System.debug('WS_Account.bulkCreateAccounts :- Added to SDHID exist check - ');
                        accSEIdMap.put(accnt.SEAccountID,accnt);
                    }
                    //create list of bFOAccounts with backOffice data
                    else
                    {
                        System.debug('WS_Account.bulkCreateAccounts :- Added to LegacyID+PublisherID exist check - ');
                        accBOLst.add(accnt);
                        if(accnt.publisherID !=null && accnt.publisherID !='' &&accnt.legacyAccountID != null && accnt.legacyAccountID !='' )
                        {
                            accPubIdMap.put(accnt.publisherID,accnt);
                            accLegacyIdMap.put(accnt.legacyAccountID,accnt);
                        }
                    }
                }
                //send msg if mandatory fields of bFOAccount record are not filled
                else
                {
                    System.debug('WS_Account.bulkCreateAccounts :- All Mandatory fields do not have values- ');
                    AccountResult result6 = new AccountResult();
                    result6.success=False;
                    result6.type='CREATION';
                    result6.bFOAccountID=accnt.bFOAccountID;
                    result6.legacyAccountID=accnt.legacyAccountID;
                    result6.publisherID=accnt.publisherID;
                    result6.errorMessage =' Mandatory Fields are missing '+ mf.fields;
                    resultList.add(result6);
                }
            }
        }
        //send msg if no bFOAccount record sent in AccountList
        else
        {
            System.debug('WS_Account.bulkCreateAccounts :- AccountList Empty - ');
            AccountResult result7 = new AccountResult();
            result7.success = false;
            result7.type = 'CREATION';
            result7.errorMessage = ' Please send request ';
            resultList.add(result7);
        }

        //Check Accounts based on bFOAccountID
        System.debug('WS_Account.bulkCreateAccounts :- Check Account Exists in bFO as per bFOAccountID check - ');
        //Check for accounts that exist in bFO System
        if(!accbFOIdMap.keySet().isEmpty())
            existingbFOIdMap = new Map<Id,Account>([SELECT SEAccountID__c FROM Account WHERE Id in :accbFOIdMap.keySet()]);


        for(bFOAccount accnt :accbFOIdMap.values())
        {
            if(existingbFOIdMap.containsKey(accnt.bFOAccountID))
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account with bFOID exist in bFO - ');
                AccountResult result1 = new AccountResult();
                result1.success=True;
                result1.type='EXIST';
                result1.bFOAccountID=accnt.bFOAccountID;
                result1.legacyAccountID=accnt.legacyAccountID;
                result1.publisherID=accnt.publisherID;
                result1.SEAccountID=existingbFOIdMap.get(accnt.bFOAccountID).SEAccountID__c;
                resultList.add(result1);
            }
            else
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account with bFOID does not exist in bFO - ');
                AccountResult result2 = new AccountResult();
                result2.success=False;
                result2.type='EXIST';
                result2.bFOAccountID=accnt.bFOAccountID;
                result2.legacyAccountID=accnt.legacyAccountID;
                result2.publisherID=accnt.publisherID;
                result2.errorMessage = ' Requested account with bFOAccountId ' + accnt.bFOAccountID + ' does not exist in bFO';
                resultList.add(result2);
            }
        }


        //Check Accounts based on SEAccountID
        System.debug('WS_Account.bulkCreateAccounts :- Check Account Exists in bFO as per SEAccountID check - ');
        //Check for accounts that exist in bFO System
        if(!accSEIdMap.keySet().isEmpty())
            existingSEIdList = [SELECT Id, SEAccountID__c FROM Account WHERE SEAccountID__c in :accSEIdMap.keySet()];
            System.debug('WS_Account.bulkCreateAccounts :- Query successful - ');
            
        for(Account acc: existingSEIdList)
        {
            System.debug('WS_Account.bulkCreateAccounts :- Existing account with SE Account ID in bFO - ');
            existingSEIdMap.put(acc.SEAccountID__c, acc);
        }
        
        

        for(bFOAccount accnt :accSEIdMap.values())
        {
            System.debug('WS_Account.bulkCreateAccounts :- SDHId entered for loop - ');
            if(existingSEIdMap.containsKey(accnt.SEAccountID))
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account with SEID exist in bFO - ');
                AccountResult result3 = new AccountResult();
                result3.success=True;
                result3.type='EXIST';
                result3.bFOAccountID=existingSEIdMap.get(accnt.SEAccountID).Id;
                result3.legacyAccountID=accnt.legacyAccountID;
                result3.publisherID=accnt.publisherID;
                result3.SEAccountID=accnt.SEAccountID;
                resultList.add(result3);
            }
            else
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account with SEID does not exist in bFO - ');
                AccountResult result4 = new AccountResult();
                result4.success=False;
                result4.type='EXIST';
                //result4.bFOAccountID=accnt.bFOAccountID;
                result4.legacyAccountID=accnt.legacyAccountID;
                result4.publisherID=accnt.publisherID;
                result4.errorMessage = ' Requested account with SEAccountId ' + accnt.SEAccountID + ' does not exist in bFO';
                resultList.add(result4);
            }
        }

        //Check Accounts based on legacyAccountID&&publisherID
        System.debug('WS_Account.bulkCreateAccounts :- Check Account Exists in bFO as per legacyAccountID+publisherID check - ');
        //Check for Legacy accounts that exist in bFO System
        
        // commented for time out issue 
       /* if(!accLegacyIdMap.keySet().isEmpty())
            existingLegacyPubIdMap = new Map<Id,LegacyAccount__c>([SELECT Account__c, Account__r.SEAccountID__c, LegacyName__c, LegacyNumber__c FROM LegacyAccount__c WHERE LegacyName__c IN :accPubIdMap.keySet() AND  LegacyNumber__c IN :accLegacyIdMap.keySet()]);

        for(LegacyAccount__c legRec : existingLegacyPubIdMap.values())
        {
            System.debug('WS_Account.bulkCreateAccounts :- Legacy Accounts in bFO - ');
            existingLegAccMap.put(legRec.LegacyName__c+legRec.LegacyNumber__c, legRec);
        }*/
        if(!accLegacyIdMap.keySet().isEmpty() && !accPubIdMap.keySet().isEmpty())
        {           
            String Query = 'SELECT Account__c, Account__r.SEAccountID__c, LegacyName__c, LegacyNumber__c FROM LegacyAccount__c';            
            String WhereClause = '';
            if(accLegacyIdMap != null && accLegacyIdMap.size()>0)
            {
                String accLegStr = SOQLListFormat(accLegacyIdMap.keySet());
                WhereClause = ' LegacyNumber__c in ('+accLegStr+')';
                
            }
            if(accPubIdMap != null && accPubIdMap.size()>0)
            {
                String accPubidStr = SOQLListFormat(accPubIdMap.keySet());
                if(whereClause.length()>0)
                WhereClause = WhereClause+' AND LegacyName__c in ('+accPubidStr+')';
                else{           
                    WhereClause = ' LegacyName__c in ('+accPubidStr+')';
                }
                
            }
            if(whereClause.length()>0){
                String FinalQuery =Query+' where '+WhereClause;
                System.debug('**********************'+FinalQuery);
                List<LegacyAccount__c> laccList = new List<LegacyAccount__c>();
                laccList = Database.query(FinalQuery);
                
                existingLegacyPubIdMap = new Map<Id,LegacyAccount__c>() ;  
                existingLegacyPubIdMap.putAll(laccList);

                for(LegacyAccount__c legRec : existingLegacyPubIdMap.values())
                {
                    System.debug('WS_Account.bulkCreateAccounts :- Legacy Accounts in bFO - ');
                    existingLegAccMap.put(legRec.LegacyName__c+legRec.LegacyNumber__c, legRec);
                }
            }
        }
        
        for(bFOAccount accnt :accBOLst)
        {
            String keyComb = accnt.publisherID+accnt.legacyAccountID;
            if(existingLegAccMap.containskey(keyComb))
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account with legacy Account exists in bFO - ');            
                AccountResult result5 = new AccountResult();
                result5.success=True;
                result5.type='EXIST';
                result5.bFOAccountID=existingLegAccMap.get(keyComb).Account__c;
                result5.legacyAccountID=accnt.legacyAccountID;
                result5.publisherID=accnt.publisherID;
                result5.SEAccountID=existingLegAccMap.get(keyComb).Account__r.SEAccountID__c;
                resultList.add(result5);
            }
            else
            {
                System.debug('WS_Account.bulkCreateAccounts :- Account with legacy Account info to be processed - '); 
                accDeDupOwnerIds.add(accnt.OwnerSESAID);
                accDeDupStateCodes.add(accnt.PhysicalStateCode);
                accDeDupCountryCodes.add(accnt.PhysicalCountryCode);
                accDeDupToBeProcessed.add(accnt);
            }
        }
        
        if(!accDeDupToBeProcessed.isEmpty())
            accLstToInsert = createAccountsToBeInserted(accDeDupToBeProcessed ,accDeDupOwnerIds, accDeDupStateCodes,accDeDupCountryCodes);       
        
        // Assumption : Only one Account is sent to webservice at a time
        if(!accLstToInsert.isEmpty() && !accDeDupToBeProcessed.isEmpty())
        {            
            bFOAccount accnt = accDeDupToBeProcessed[0];
            System.debug('WS_Account.bulkCreateAccounts :- Account to be dedeup checked - '+ accnt );
            
            Database.SaveResult sr =  Database.insert(accLstToInsert[0], false);
            if(sr.isSuccess())
            {   
                AccountResult result8 = new AccountResult();
                result8.success=True;
                result8.type='CREATION';
                result8.bFOAccountID=sr.getId();
                result8.legacyAccountID=accnt.legacyAccountID;
                result8.publisherID=accnt.publisherID;
                resultList.add(result8);
                System.debug('WS_Account.bulkCreateAccounts :- Account inserted with no duplicates found - '+result8);
                
                LegacyAccount__c legAccnt = new LegacyAccount__c(LegacyName__c=accnt.publisherID,LegacyNumber__c=accnt.legacyAccountID,LegacyAccountType__c=accnt.AccType,Account__c=sr.getId());
                legAcctsToInsert.add(legAccnt);
            }
            else
            {
                Boolean failed = false;
                Boolean dedupError = false;
                String errorMsg = 'Creation of Account encountered error : ';
                
                for(Database.Error err : sr.getErrors()) 
                {
                    if(err instanceof Database.DuplicateError)
                    {
                        Database.DuplicateError duplicateError = (Database.DuplicateError)err;
                        Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                        Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();
                        Datacloud.MatchResult matchResult = matchResults[0];
                        Integer numOfDuplicates = matchResult.getSize();
    
                        if(numOfDuplicates == 1)
                        {
                            Datacloud.MatchRecord matchRecord = matchResult.getMatchRecords().get(0);
    
                            AccountResult result9 = new AccountResult();
                            result9.success=True;
                            result9.type='EXIST';
                            result9.bFOAccountID=(Id)matchRecord.getRecord().get('Id');
                            result9.legacyAccountID=accnt.legacyAccountID;
                            result9.publisherID=accnt.publisherID;
                            resultList.add(result9);
    
                            LegacyAccount__c legAccntForDup = new LegacyAccount__c(LegacyName__c=accnt.publisherID,LegacyNumber__c=accnt.legacyAccountID,LegacyAccountType__c=accnt.AccType,Account__c=(Id)matchRecord.getRecord().get('Id'));
                            legAcctsToInsert.add(legAccntForDup);
    
                        }
                        else if(numOfDuplicates > 1)
                        {   
                            dedupError = true;
                            forceInsertAcctLst.add(accLstToInsert[0]);
                        }
                    }
                    else
                    {
                        failed = true;
                        errorMsg = errorMsg + err.getMessage() + ' at field : ' +err.getFields() +', ';                        
                    }
                }

                // If there are errors other than DuplicateError, then send one response by concatenating all the errors
                if(failed && !dedupError)
                {
                    AccountResult result10 = new AccountResult();
                    result10.success=False;
                    result10.type='CREATION';
                    //result10.bFOAccountID=accnt.bFOAccountID;
                    result10.legacyAccountID=accnt.legacyAccountID;
                    result10.publisherID=accnt.publisherID;
                    result10.errorMessage = errorMsg;
                    resultList.add(result10);
                }
            }
        }
    
        //Force insert Accounts with more than 1 Duplicates
        if(!forceInsertAcctLst.isEmpty() && !accDeDupToBeProcessed.isEmpty())
        {
            bFOAccount accnt = accDeDupToBeProcessed[0];
            Database.DMLOptions objDMLOptions = new Database.DMLOptions();
            objDMLOptions.DuplicateRuleHeader.AllowSave = true;
            Database.SaveResult accForRes = Database.insert(forceInsertAcctLst[0],objDMLOptions);
            if(accForRes.isSuccess())
            {   
                AccountResult result11 = new AccountResult();
                result11.success=True;
                result11.type='CREATION';
                result11.bFOAccountID=accForRes.getId();
                result11.legacyAccountID=accnt.legacyAccountID;
                result11.publisherID=accnt.publisherID;
                resultList.add(result11);
    
                LegacyAccount__c legAccnt = new LegacyAccount__c(LegacyName__c=accnt.publisherID,LegacyNumber__c=accnt.legacyAccountID,LegacyAccountType__c=accnt.AccType,Account__c=accForRes.getId());
                legAcctsToInsert.add(legAccnt);
    
            }
            else
            {
                Database.Error err = accForRes.getErrors()[0];
                System.debug('WS_Account.bulkCreateAccounts :- Error message - '+ err.getMessage() + ' at field : ' +err.getFields());
                AccountResult result8 = new AccountResult();
                result8.success=False;
                result8.type='CREATION';
                //result8.bFOAccountID=accnt.bFOAccountID;
                result8.legacyAccountID=accnt.legacyAccountID;
                result8.publisherID=accnt.publisherID;
                result8.errorMessage = 'Creation of Account encountered error : ' + err.getMessage() + ' at field : ' +err.getFields();
                resultList.add(result8);
            }
    
        }    
    
        //Insert all Legacy Accounts
        Database.SaveResult[] legAcctsSaveResults  =  Database.insert(legAcctsToInsert, false);
        
        
        
        return resultList;
    }// END OF WEBSERVICE MAIN METHOD

    public class MandatoryFields{
        public Boolean isMFPass{get;set;}
        public String fields{get;set;}
    }

    public static MandatoryFields CreateMandataryCheck(bFOAccount request){
        MandatoryFields mf = new MandatoryFields();
        String mandatoryFields ='';                
        Boolean mandatory = true; 

        //Check mandatory fields have values sent in
        if(request.publisherID != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' publisherID';
        }

        if(request.legacyAccountID != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' legacyAccountID';
        }

        if(request.AccountName != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' AccountName';
        }

        if(request.PhysicalStreet != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' PhysicalStreet';
        }

        if(request.PhysicalCity != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' PhysicalCity';
        }

        if(request.PhysicalCountryCode != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' PhysicalCountryCode';
        }

        if(request.CurrencyIsoCode != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' CurrencyIsoCode';
        }

        if(request.AccType != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' AccType';
        }

        if(request.ClassLevel1 != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' ClassLevel1';
        }

        if(request.OwnerSESAID != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' OwnerSESAID';
        }

        if(request.RecordType != null ){
        }
        else{
            mandatory = false;
            mandatoryFields += mandatoryFields + ' RecordType';
        }

        //return mandatory check results
        mf.isMFPass = mandatory;
        mf.fields = mandatoryFields;
        return mf;
    }

    public Static List<Account> createAccountsToBeInserted(List<bFOAccount> accDeDupToBeProcessed ,Set<String> accDeDupOwnerIds, Set<String> accDeDupStateCodes, Set<String> accDeDupCountryCodes)
    {
        String countryCode;
        List<Account> accLstToInsert = new List<Account>();
        List<StateProvince__c> existingStateLst = new List<StateProvince__c>();
        List<Country__c> existingCountryLst = new List<Country__c>();
        Map<Id,User> existingUserMap = new Map<Id,User>();
        Map<String,User> accOwnerIdMap = new Map<String,User>();
        Map<String,Country__c> cntryCodeIdMap = new Map<String,Country__c>();
        Map<String,RecordType> accRecTypeMap = new Map<String,RecordType>();
        Map<String,bFOAccount> accKeyInsertMap = new Map<String,bFOAccount>();
    
        //Get all userIds of the account owners
        if(!accDeDupOwnerIds.isEmpty())
            existingUserMap = new Map<Id,User>([SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier IN :accDeDupOwnerIds]);
    
        for(User u :existingUserMap.values())
        {
            accOwnerIdMap.put(u.FederationIdentifier, u);
        }
        
        //Get all State Ids for respective state codes
        if(!accDeDupStateCodes.isEmpty())
            existingStateLst = [SELECT Id, Name, StateProvinceCode__c, CountryCode__c  FROM StateProvince__c WHERE StateProvinceCode__c IN :accDeDupStateCodes];
    
        System.debug('WS_Account.createAccountsToBeInserted :- Existing State Records '+ existingStateLst );

        //Get all Country Ids for respective country codes
        if(!accDeDupCountryCodes.isEmpty())
            existingCountryLst = [SELECT Id, Name, CountryCode__c FROM Country__c WHERE CountryCode__c IN :accDeDupCountryCodes];
    
        for(Country__c ct :existingCountryLst)
        {
            cntryCodeIdMap.put(ct.CountryCode__c, ct);
        }

        //Get recordtype id of the account to be created
        for(RecordType rt: [SELECT Id,Name,SobjectType,DeveloperName FROM RecordType where SobjectType ='Account']){
            accRecTypeMap.put(rt.DeveloperName , rt);
        }
    
        //Create Account records for each of the bFOAccount record in accDeDupToBeProcessed
        //Perform insert of the corresponding list of account records, to run the duplicate rules
        if(!accDeDupToBeProcessed.isEmpty())
        {
            for(bFOAccount accnt :accDeDupToBeProcessed)
            {
                //String accKey ='';
                Account a = new Account();
                
                // fields with mandatory input values
                if(String.isNotBlank(accnt.AccountName))
                {
                    //accKey= accKey + String.valueOf(accnt.AccountName);
                    a.Name=accnt.AccountName;
                }
                if(String.isNotBlank(accnt.PhysicalStreet))
                {
                    //accKey= accKey + String.valueOf(accnt.PhysicalStreet);
                    //a.BillingStreet=accnt.PhysicalStreet;
                    a.Street__c=accnt.PhysicalStreet;
                }
                if(String.isNotBlank(accnt.PhysicalCity))
                {
                    //accKey= accKey + String.valueOf(accnt.PhysicalCity);
                    //a.BillingCity=accnt.PhysicalCity;
                    a.City__c=accnt.PhysicalCity;
                }
                if(String.isNotBlank(accnt.PhysicalCountryCode) && cntryCodeIdMap.containsKey(accnt.PhysicalCountryCode))
                {
                    //accKey= accKey + String.valueOf(accnt.PhysicalCountryCode);
                    //a.BillingCountryCode=accnt.PhysicalCountryCode;
                    countryCode = cntryCodeIdMap.get(accnt.PhysicalCountryCode).CountryCode__c;
                    a.Country__c=cntryCodeIdMap.get(accnt.PhysicalCountryCode).Id;
                    System.debug('WS_Account.createAccountsToBeInserted :- Country Code '+ countryCode );
                }
                if(String.isNotBlank(accnt.CurrencyIsoCode))
                {
                    //accKey= accKey + String.valueOf(accnt.CurrencyIsoCode);
                    a.CurrencyIsoCode=accnt.CurrencyIsoCode;
                }
                if(String.isNotBlank(accnt.AccType))
                {
                    //accKey= accKey + String.valueOf(accnt.AccType);
                    a.AccType__c=accnt.AccType;
                }
                if(String.isNotBlank(accnt.ClassLevel1))
                {
                    //accKey= accKey + String.valueOf(accnt.ClassLevel1);
                    a.ClassLevel1__c=accnt.ClassLevel1;
                }
                if(String.isNotBlank(accnt.OwnerSESAID) && accOwnerIdMap.containsKey(accnt.OwnerSESAID))
                {
                    //accKey= accKey + String.valueOf(accnt.OwnerSESAID);
                    a.OwnerId=accOwnerIdMap.get(accnt.OwnerSESAID).Id;
                }
               if(String.isNotBlank(accnt.RecordType))
               {
                    a.RecordTypeId=accRecTypeMap.get(accnt.RecordType).Id;
               }
                
                //fields with conditional mandate on input values
                if(String.isNotBlank(accnt.AccountLocalName))
                    a.AccountLocalName__c=accnt.AccountLocalName;                        
                if(String.isNotBlank(accnt.PhysicalZipCode))
                {                    
                    //a.BillingPostalCode=accnt.PhysicalZipCode;
                    a.ZipCode__c = accnt.PhysicalZipCode;
                }
                if(String.isNotBlank(accnt.PhysicalStateCode))
                {
                    //a.BillingStateCode=accnt.PhysicalStateCode;
                    System.debug('WS_Account.createAccountsToBeInserted :- State Province Values '+ existingStateLst);
                    for(StateProvince__c st :existingStateLst)
                    {
                        System.debug('WS_Account.createAccountsToBeInserted :- State Province '+ st);
                        
                        if(st.StateProvinceCode__c == accnt.PhysicalStateCode && st.CountryCode__c == accnt.PhysicalCountryCode)                        
                        {                                                         
                                a.StateProvince__c=st.Id;                            
                        }
                    }
                    System.debug('WS_Account.createAccountsToBeInserted :- State Province Account value '+ a.StateProvince__c);
                }
                if(String.isNotBlank(accnt.LocalCity))
                    a.LocalCity__c=accnt.LocalCity;
                if(String.isNotBlank(accnt.MarketSegment))
                    a.MarketSegment__c=accnt.MarketSegment;
                    
                // fields with no mandate on input values
                if(String.isNotBlank(accnt.AdditionalAddress))
                    a.AdditionalAddress__c=accnt.AdditionalAddress;
                if(String.isNotBlank(accnt.POBox))
                    a.POBox__c=accnt.POBox;
                if(String.isNotBlank(accnt.StreetLocalLang))
                    a.StreetLocalLang__c=accnt.StreetLocalLang;
                if(String.isNotBlank(accnt.LocalAdditionalAddress))
                    a.LocalAdditionalAddress__c=accnt.LocalAdditionalAddress;                                           
                if(String.isNotBlank(accnt.MarketSubSegment))
                    a.MarketSubSegment__c=accnt.MarketSubSegment;
                if(String.isNotBlank(accnt.LeadingBusiness))
                    a.LeadingBusiness__c=accnt.LeadingBusiness;
                
                accLstToInsert.add(a);
                
            }
            System.debug('WS_Account.bulkCreateAccounts :- Account List to be inserted - '+accLstToInsert); 
            
        }
        
        return accLstToInsert;
    }
            
    public static string SOQLListFormat(set<string> input){   
        String SOQL_ListFormat = '';
        for (string Value : input) {
            String value_in_quotes = '\''+String.escapeSingleQuotes(Value)+'\'';
            if (SOQL_ListFormat!='') { SOQL_ListFormat+=','; }  //  add a comma if this isn't the first one
            SOQL_ListFormat += value_in_quotes;
        }   
        return SOQL_ListFormat;
    }
    
}