@isTest
public class AP_LocationManager_Test{

    static testMethod void testLocationManager() 
    {
        Country__c country= Utils_TestMethods.createCountry(); 
        country.CountryCode__c= 'hkh';   
        insert country; 
        StateProvince__c sp = new StateProvince__c();
        sp.Country__c =  country.id;
        sp.Name ='Test State Provinces';
        sp.CountryCode__c ='hk';
        sp.StateProvinceCode__c ='spc';
        sp.StateProvinceExternalId__c ='Tesid';
        insert sp;
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeid = Label.CLOCT13ACC08;
        objAccount.StateProvince__c =sp.id;
        objAccount.City__c ='Bang';
        objAccount.Country__c = country.id;
        insert objAccount;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = objAccount .id;
        site1.PrimaryLocation__c = false;
        site1.RecordTypeId = Label.CLNOV13SRV02;
        insert site1;
        
        Account objAccount2 = Utils_TestMethods.createAccount();
        objAccount2.RecordTypeid = Label.CLOCT13ACC08;
        objAccount2.StateProvince__c =sp.id;
        objAccount2.City__c ='Bang';
        objAccount2.Country__c = country.id;
        insert objAccount2;
        
        SVMXC__Site__c site2 = new SVMXC__Site__c();
        site2.Name = 'Test Location 2';
        site2.SVMXC__Street__c  = 'Test Street';
        site2.SVMXC__Account__c = objAccount2.id;
        site2.PrimaryLocation__c = true;
        site2.RecordTypeId = Label.CLNOV13SRV02;
        insert site2;
        
        SVMXC__Site__c site3 = new SVMXC__Site__c();
        site3.Name = 'Test Location 2';
        site3.SVMXC__Street__c  = 'Test Street';
        site3.SVMXC__Account__c = objAccount2.id;
        site3.SVMXC__Parent__c = site2.id;
       
        site3.RecordTypeId = Label.CLNOV13SRV02;
        insert site3;
        List<SVMXC__Site__c> sitelist = new List<SVMXC__Site__c>();
        sitelist.add(site1);
        sitelist.add(site2);
        sitelist.add(site3);
        
        AP_LocationManager.ProcessLocations((List<Sobject>)sitelist);
    
    }
}