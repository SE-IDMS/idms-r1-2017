public class PRJ_planviewReportController
  {
    public ApexPages.StandardController controller;
    public string fromDate{get;set;}
    public string toDate{get;set;}
    string fromDate1;
    string toDate1;
    string timeToAppend;
    string formattedFromDate;
    string formattedToDate;
     DateTime fromDate2;
    DateTime toDate2;
    list<PRJ_ProjectReq__c> getTheModifiedProjects = new list<PRJ_ProjectReq__c> ();
    Integer year;
    Integer nextYear;
    public list<WrapperClass> wrapperClassList{get; set;}
    list<String> statusString;
    String program;
    
    public list<PRJ_ProjectReq__c> projectsModified{get; set;}

    public PRJ_planviewReportController(ApexPages.StandardController controller)
    {  
      this.controller = controller;
      timeToAppend = '00:00:00';
      fromDate1='';
      toDate1='';
      formattedFromDate='';
      formattedToDate='';
      fromDate2 = DateTime.newInstance(1997, 1, 31, 7, 8, 16);
      toDate2 = DateTime.newInstance(1997, 1, 31, 7, 8, 16);
      year = System.Today().year();
      nextYear = System.Today().year()+ 1;
      wrapperClassList = new list<WrapperClass> ();
      statusString = new list<String> {'Draft','Archive'};
      program = 'GLOBAL MARKETING PROGRAM';
    } 

    public pageReference exportToExcel()
    { 
      if(fromDate != null)
      {
        system.debug('fromDate:'+ fromDate);

        String[] splitString = fromDate.split('/');
        system.debug('>>>' + splitString);
        splitString[0] = splitString[0].length() == 1? 0+splitString[0]:splitString[0];
        system.debug('>>>' + splitString[0]);

        splitString[1] = splitString[1].length() == 1? 0+splitString[1]:splitString[1];
        system.debug('>>>' + splitString[1]);
        fromDate = splitString[0]+'/'+splitString[1]+'/'+splitString[2];

        system.debug('fromDate:' + fromDate);
        formattedFromDate = fromDate+' '+timeToAppend;
        system.debug('formattedFromDate:' + formattedFromDate);
        fromDate1 = formattedFromDate.substring(6,10) + '-' + 
                    formattedFromDate.substring(3,5) + '-' +
                    formattedFromDate.substring(0,2) +' '+
                    formattedFromDate.substring(11,19);
        system.debug('fromDate1:'+ fromDate1);
        fromDate2 = datetime.valueOfGmt(fromDate1);
        system.debug('fromDate2:'+ fromDate2);
     }

      if(toDate != null)
      {
        String[] splitString1 = toDate.split('/');
        system.debug('>>>' + splitString1);
        splitString1[0] = splitString1[0].length() == 1? 0+splitString1[0]:splitString1[0];
        system.debug('>>>' + splitString1[0]);

        splitString1[1] = splitString1[1].length() == 1? 0+splitString1[1]:splitString1[1];
        system.debug('>>>' + splitString1[1]);
        toDate = splitString1[0]+'/'+splitString1[1]+'/'+splitString1[2];

        system.debug('toDate:' + toDate); 

        formattedToDate = toDate+' '+timeToAppend;
        toDate1 = formattedToDate.substring(6,10) + '-' + 
                  formattedToDate.substring(3,5) + '-' + 
                   formattedToDate.substring(0,2) + ' '+
                   formattedToDate.substring(11,19);
        toDate2 = datetime.valueOfGmt(toDate1);
        system.debug('toDate2:'+ toDate2);
      }

      list<String> fields = new list<String> {'ITCashoutCAPEX','ITCashoutOPEXOTC','ITCashoutSmallEnhancementsOTC','ITCashoutRunningCostsImpact','IPOInternalRegions','IPOInternalRegionsAPAC','IPOInternalRegionsCHINA','IPOInternalRegionsEUROPE','IPOInternalRegionsISMA','IPOInternalRegionsNAM','IPOInternalAppServicesCustExp','IPOInternalTechnologyServices','IPOInternalGDCosts','ITCostsInternalNonIPO'};

      String  concatFields ='';
      for(integer i =0;i<fields.size();i++)
      {
       if(i == (fields.size()-1))
       {
          concatFields += fields[i] + year + '__c,';
          concatFields += fields[i] + nextYear + '__c';
       }
       else
       {
          concatFields += fields[i] + year + '__c,';
          concatFields += fields[i] + nextYear +'__c,';
       }
      }
          
      String prjReqQuery ='SELECT id,(SELECT HFMCode__c, SelectedinEnvelop__c, CostCenter__c FROM DMT_Funding_Entities__r), Name,ParentFamily__c,Parent__c,Tiering__c,ProjectLegacyCode__c,WorkType__c,Master_Project__c,ProjectRequestDate__c,ExpectedStartDate__c,GoLiveTargetDate__c, ProjectClosureDate__c,ProgrammedGoLiveDate__c,EffectiveGoLiveDate__c,Owner.Name,Owner.UserName,BusGeographicZoneIPO__c,BusGeographicalIPOOrganization__c,GeographicZoneSE__c,Country__r.Name,NextStep__c,ActualStep__c,Description__c,GlobalProcess__c,SubProcess__c,BusinessTechnicalDomain__c,NatureofProject__c,TypeofTechnology__c,DMTProgram__c,ProjectClassification__c,ObjectivesSummary__c,ScoreResultCalculated__c,BCINumber__c,AuthorizeFunding__c,Platform__c, Number_of_users_deployed_per_platform__c,ProjectRequestID__c,Tech_NumberofDMTFundingEntities__c,TECH_FundingEntitiesFY0__c,TECH_FundingEntitiesFY0ActiveSelected__c,'+ concatFields + ' FROM PRJ_ProjectReq__c WHERE LastModifiedDate >= :fromDate2 AND LastModifiedDate <= :toDate2 AND NextStep__c NOT IN :statusString AND DMTProgram__c != :Program';

      system.debug('>>>>' + prjReqQuery );

      getTheModifiedProjects = database.query(prjReqQuery);
      system.debug('>>>' + getTheModifiedProjects );
      system.debug('>>>' + getTheModifiedProjects.size());

      if(getTheModifiedProjects.size()>0)
      {  
        for(PRJ_ProjectReq__c prj:getTheModifiedProjects)  
        {  
          WrapperClass wrapperClassVariable = new WrapperClass();       
          if(prj.Tech_NumberofDMTFundingEntities__c <= 1)
          {
            system.debug('no of entities:' + prj.Tech_NumberofDMTFundingEntities__c);
            wrapperClassVariable.oPR = prj;
            for(DMTFundingEntity__c d:prj.DMT_Funding_Entities__r)
              wrapperClassVariable.oFE = d;
            wrapperClassVariable.count = (prj.TECH_FundingEntitiesFY0__c ==0 ?0:(prj.TECH_FundingEntitiesFY0__c-prj.TECH_FundingEntitiesFY0ActiveSelected__c == 0 ? 2:0));
            system.debug('project:' + wrapperClassVariable.oPR);
            system.debug('Fund:' + wrapperClassVariable.oFE);
            system.debug('count:' + wrapperClassVariable.count);
            wrapperClassList.add(wrapperClassVariable);
          }
          else if(prj.Tech_NumberofDMTFundingEntities__c > 1)
          {
            system.debug('no of entities:' + prj.Tech_NumberofDMTFundingEntities__c);
            wrapperClassVariable.oPR = prj;
            wrapperClassVariable.oFE.HFMCode__c = ' ';
            wrapperClassVariable.oFE.CostCenter__c = ' ';
            wrapperClassVariable.count = (prj.TECH_FundingEntitiesFY0__c == prj.TECH_FundingEntitiesFY0ActiveSelected__c ? 2:(prj.TECH_FundingEntitiesFY0ActiveSelected__c >= 2? 1:0));
            system.debug('project:' + wrapperClassVariable.oPR);
            system.debug('Fund:' + wrapperClassVariable.oFE);
            system.debug('HFM Code:' + wrapperClassVariable.oFE.HFMCode__c);
            system.debug('cost Center:' + wrapperClassVariable.oFE.CostCenter__c);
            system.debug('count:' + wrapperClassVariable.count);
            wrapperClassList.add(wrapperClassVariable);
          } 
        }
        system.debug('wrapper class list:'+wrapperClassList);
        system.debug('wrapper class list size:'+ wrapperClassList.size());
        PageReference pageRef = new pageReference('/apex/PRJ_excelReportPage');
        return pageRef;
      } 
      return null;
    }

    public class WrapperClass
    {
      public PRJ_ProjectReq__c oPR {get; set;}
      public DMTFundingEntity__c oFE {get; set;}
      public Integer count {get; set;}
    }  
  }