/**
* This is test class for IdmsPasswordPolicy class.
**/
@isTest
private class IdmsPasswordPolicyTest{
    
    //This method tests check password policy
    static testmethod void testIdmsPasswordPolicy(){
    
     User objUser = Utils_TestMethods.createStandardUser('test');
     //IDMSTestClassUtility.createUser('Home', 'test@test.com', 'TestFirst','TestLast', 'English', 'Facebook');
       objUser.firstName ='First';
       insert objUser;
       string strPassword ='India@123';
       string strfedid='1234567';
       string id='1234';
      IdmsPasswordPolicy ipp;
       ipp.checkPolicyMetOrNot(strPassword, objUser.id,'');
       ipp.checkPolicyMetOrNotWithFNLN(strPassword,objUser.firstname,objUser.lastname);
       strPassword ='India321';
           
    }
}