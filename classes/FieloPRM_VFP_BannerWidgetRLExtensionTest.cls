/**************************************************************************************
    Author: Fielo Team
    Date: 22/06/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/
@isTest

public class FieloPRM_VFP_BannerWidgetRLExtensionTest{

    public static testMethod void test1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);
           
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;
            

            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.id);
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.name = 'test';
            insert tag;

            FieloEE__Tag__c tag2 = new FieloEE__Tag__c();
            tag2.name = 'test';
            insert tag2;
            
            FieloEE__Component__c component = new FieloEE__Component__c();
            component.FieloEE__Menu__c = thisMenu.id;
            component.FieloEE__Tag__c = tag.id;
            component.FieloEE__Category__c = category.Id;
            insert component;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c();
            banner.Name = 'Test';
            banner.FieloEE__Component__c = component.id;
            banner.FieloEE__Category__c = category.id;
            insert banner;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c();
            tagItem.FieloEE__Banner__c = banner.id;
            tagItem.FieloEE__Tag__c = tag.id;
            insert tagItem;
            
            FieloEE__Component__c compo = new FieloEE__Component__c(); 
            compo.FieloEE__Menu__c = thisMenu.id;
            insert compo;
            

                      
            test.startTest();

            ApexPages.StandardController stdController = new ApexPages.StandardController(component);
            FieloPRM_VFP_BannerWidgetRLExtension ext = new FieloPRM_VFP_BannerWidgetRLExtension(stdController);

            ext.doNewBanner();
            ApexPages.currentPage().getParameters().put('bannerId',banner.id);
            ext.doDelete();   
           
            test.stopTest();
        }
    }
 
}