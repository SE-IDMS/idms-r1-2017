/*
    Author          : Shruti Karn    
    Date Created    : 11/10/2012
    Description     : Test class for WS_Z2Services and Z2OutputClass
*/
@isTest
private class WS_Z2Services_TEST {
     static testMethod void testmethod1() {
         
         //test.startTest(); 
         
         Country__c testCountry = Utils_TestMethods.createCountry();
         insert testCountry;
         
         Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
         testAccount.Name = 'Test';
         insert testAccount;
         
         Account testAccount2 = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
         testAccount2.Name = 'Test2';
         insert testAccount2;
                  
         Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
         testContact.Country__c = testCountry.Id;
         insert testContact;
         
         Contact testContact2 = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
         testContact2.Country__c = testCountry.Id;
         insert testContact2;
                        
         Contract testContract = Utils_TestMethods.createContract(testAccount.Id , testContact.Id);
         insert testContract ;
         
         testContract.webaccess__c = 'Eclipse';
         testContract.Status = 'Activated';    
         testContract.startdate = system.today();
         testContract.ContractTerm = 3;
         update testContract;    
         
         PackageTemplate__c testPkgTemplate = new PackageTemplate__c();
         testPkgTemplate.Name = 'TEST TEMPLATE2';
         insert testPkgTemplate;
                 
         Package__c testPackage = new Package__c();
         testPackage.Contract__c = testContract.Id;
         testPackage.startdate__C = system.today();
         testPackage.enddate__c = system.today() +3 ;
         testPackage.status__c = 'Active';
         testPackage.PackageTemplate__c = testPkgTemplate.Id;
         insert testPackage;
         
         CTR_ValueChainPlayers__c testcvcp1 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
         testcvcp1.contact__c = testContact.id;
         testcvcp1.contactrole__C = 'Beneficiary';
         insert testcvcp1;
         
         CTR_ValueChainPlayers__c testcvcp = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
         testcvcp.contact__c = testcontact.id;
         testcvcp.contactrole__C = 'Admin';
         testcvcp.account_role__c = 'Agent';
         insert testcvcp;
         
         CTR_ValueChainPlayers__c testcvcp2 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
         testcvcp2.contact__c = testcontact.id;
         testcvcp2.contactrole__C = 'Admin';
         testcvcp2.account_role__c = 'Distributors / Resellers';
         insert testcvcp2;
         
         CTR_ValueChainPlayers__c testcvcp3 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
         testcvcp3.contact__c = testContact2.id;
         testcvcp3.contactrole__C = 'Beneficiary';
         insert testcvcp3;
         
         OPP_Product__c testProduct = Utils_TestMethods.createProduct('Test','Test','Test','Test');
         testProduct.TECH_PM0CodeInGMR__c ='123456789';
         testProduct.CCCRelevant__c = true;
         insert testProduct;
         
        list<CS_EclipseRoleUtils__c> RoleCustomSetting = new list<CS_EclipseRoleUtils__c>();
        
        CS_EclipseRoleUtils__c csr1 =new CS_EclipseRoleUtils__c();
        csr1.name = 'Beneficiary';
        csr1.ContactRole__c = 'Beneficiary';
        csr1.AccountRole__c = '';
        RoleCustomSetting.add(csr1);
            
        CS_EclipseRoleUtils__c csr2 =new CS_EclipseRoleUtils__c();
        csr2.name = 'Distributor Contact';
        csr2.ContactRole__c = 'Admin';
        csr2.AccountRole__c = 'Distributors / Resellers';
        RoleCustomSetting.add(csr2);
        
        CS_EclipseRoleUtils__c csr3 =new CS_EclipseRoleUtils__c();
        csr3.name = 'Distributor Inside Sales';
        csr3.ContactRole__c = 'Primary Partner Contacten';
        csr3.AccountRole__c = 'Distributors / Resellers';
        RoleCustomSetting.add(csr3);
        
        CS_EclipseRoleUtils__c csr4 =new CS_EclipseRoleUtils__c();
        csr4.name = 'Registered User';
        csr4.ContactRole__c = 'Beneficiary';
        csr4.AccountRole__c = 'Delegated as Primary';
        RoleCustomSetting.add(csr4);
        
        CS_EclipseRoleUtils__c csr5 =new CS_EclipseRoleUtils__c();
        csr5.name = 'Schneider Service Sales';
        csr5.ContactRole__c = 'Admin';
        csr5.AccountRole__c = 'Agent';
        RoleCustomSetting.add(csr5);
        
        CS_EclipseRoleUtils__c csr6 =new CS_EclipseRoleUtils__c();
        csr6.name = 'User';
        csr6.ContactRole__c = 'Beneficiary*';
        csr6.AccountRole__c = '';
        RoleCustomSetting.add(csr6);
        
        insert RoleCustomSetting;
         
         /**************getProductCVCPInfo***********/
         
         WS_Z2Services.CVCPObj contract = new WS_Z2Services.CVCPObj();
         contract.contactID = testContact.Id;
         
         //Positive Scenario
         Z2OutputClass.ProductResult Product = new Z2OutputClass.ProductResult();
         Product = WS_Z2Services.getProductCVCPInfo(contract);
         
         //Negative Scenario
         contract.contactID = null;
         Product = WS_Z2Services.getProductCVCPInfo(contract);
         /**************getProductCVCPInfo End***********/
         
         /**************createCase***********/
         
         WS_Z2Services.CaseCreationObj caseObj = new WS_Z2Services.CaseCreationObj();
         caseObj.ContactID = testContact.Id;
         caseObj.Priority = 'Normal';
         caseObj.Subject = 'Test Case';
         caseObj.CustomerRequest = 'Test Description';
         caseObj.Category = '1';
         caseObj.Reason = 'Software Dissatisfaction';
         caseObj.Subreason = 'Other';
         caseObj.CVCPID = '';
         caseObj.Family = 'Test';
         caseObj.GMRCode = '123456789';
         
         //Without Custom Setting
         Z2OutputClass.CaseCreationResult CaseResult = new Z2OutputClass.CaseCreationResult();
         CaseResult = WS_Z2Services.createCase(caseObj);
         
         // Create Custom Setting Data
        
        list<CS_EclipseUtils__c> customSetting = new list<CS_EclipseUtils__c>();
        
        CS_EclipseUtils__c cs1 =new CS_EclipseUtils__c();
        cs1.name = 'Origin';
        cs1.Value__c = 'Web';
        customSetting.add(cs1);
            
        CS_EclipseUtils__c cs2 =new CS_EclipseUtils__c();
        cs2.name = 'Priority';
        cs2.Value__c = 'Normal';
        customSetting.add(cs2);
        
        CS_EclipseUtils__c cs3 =new CS_EclipseUtils__c();
        cs3.name = 'Status';
        cs3.Value__c = 'Open';
        customSetting.add(cs3);
        
        CS_EclipseUtils__c cs4 =new CS_EclipseUtils__c();
        cs4.name = 'SubSymptom__c';
        cs4.Value__c = 'Product Dissatisfaction*';
        customSetting.add(cs4);
        
        CS_EclipseUtils__c cs5 =new CS_EclipseUtils__c();
        cs5.name = 'SupportCategory__c';
        cs5.Value__c = '4 - Post-Sales Tech Support*';
        customSetting.add(cs5);
        
        CS_EclipseUtils__c cs6 =new CS_EclipseUtils__c();
        cs6.name = 'Symptom__c';
        cs6.Value__c = 'Troubleshooting*';
        customSetting.add(cs6);
        
        CS_EclipseUtils__c cs7 =new CS_EclipseUtils__c();
        cs7.name = 'WorkFax';
        cs7.FieldAPI__c = 'Fax';
        customSetting.add(cs7);
        
        CS_EclipseUtils__c cs8 =new CS_EclipseUtils__c();
        cs8.name = 'ContactEmail';
        cs8.FieldAPI__c = 'Email';
        customSetting.add(cs8);
        
        CS_EclipseUtils__c cs9 =new CS_EclipseUtils__c();
        cs9.name = 'ContactPhone';
        cs9.FieldAPI__c = 'WorkPhone__c';
        customSetting.add(cs9);
        
        insert customSetting;
               
        CaseResult = WS_Z2Services.createCase(caseObj);
        caseObj.ContactID = null;
        CaseResult = WS_Z2Services.createCase(caseObj);
        caseObj.ContactID = testContact.Id;
        caseObj.CVCPID = testcvcp.Id;
        caseObj.Priority = '';
        caseObj.Category = '';
        caseObj.Reason = '';
        caseObj.Subreason = '';
        CaseResult = WS_Z2Services.createCase(caseObj);
        
        /**************createCase End***********/
        
        /**************getCaseList ***********/
        
        WS_Z2Services.CaseSearchObj caseSearch = new WS_Z2Services.CaseSearchObj();
        caseSearch.ContactID = '';
        caseSearch.CaseNumber = '';
        caseSearch.SType = '';
        caseSearch.Category = '';
        caseSearch.AccountName = '';
        caseSearch.ContactFirstName = '';
        caseSearch.ContactLastName = '';
        caseSearch.Family = '';
        caseSearch.Status = '';
             
        Z2OutputClass.CaseSearchResultList caseList = WS_Z2Services.getCaseList(caseSearch);
        caseSearch.ContactID = testContact.Id;
        caseSearch.SType = 'F';
        caseSearch.Category = 'Test';
        caseSearch.AccountName = 'Test';
        caseSearch.ContactFirstName = 'Test';
        caseSearch.ContactLastName = 'Test';
        caseSearch.Family = 'Test';
        caseSearch.Status = 'Open';
        caseSearch.CreationDateStart = System.now();
        caseSearch.CreationDateEnd = System.now()-2;
        caseSearch.ClosedDateStart = System.now()-5;
        caseSearch.ClosedDateEnd = System.now()-3;
        caseList = WS_Z2Services.getCaseList(caseSearch);
        /*
        caseSearch.SType = 'C';        
        caseList = WS_Z2Services.getCaseList(caseSearch);
        caseSearch.SType = 'S';
        caseList = WS_Z2Services.getCaseList(caseSearch);
        caseSearch.SType = 'D';
        caseList = WS_Z2Services.getCaseList(caseSearch);
        String casenumber = [Select casenumber from case where id = :CaseResult.CASEID limit 1].casenumber;
        caseSearch.CaseNumber = casenumber ;
        caseSearch.ContactID = testContact.Id;
        caseSearch.SType = 'F';
        caseList = WS_Z2Services.getCaseList(caseSearch); 
        caseSearch.SType = null;
        caseList = WS_Z2Services.getCaseList(caseSearch); 
        caseSearch.ContactID = testContact.Id;
        caseSearch.SType = 'M';
        caseSearch.Status = System.Label.CLOCT12CCC32;
        caseList = WS_Z2Services.getCaseList(caseSearch);
        caseSearch.SType = 'I';
        caseSearch.Status = 'Open/Escalation to Advanced';
        caseList = WS_Z2Services.getCaseList(caseSearch);
        /*
        /**************getCaseList End***********/
        
        /**************getCase ***********/
        WS_Z2Services.CaseObj caseInput = new WS_Z2Services.CaseObj();
        caseInput.ContactID = '';
        caseInput.CaseID = '';
        Z2OutputClass.CaseResult CaseDetail = WS_Z2Services.getCase(caseInput);
        caseInput.ContactID = testContact.Id;
        CaseDetail = WS_Z2Services.getCase(caseInput);
        caseInput.CaseID = CaseResult.caseid;
        // commented by Hari on 31-12-2013
        // CaseDetail = WS_Z2Services.getCase(caseInput);
        //test.stopTest();
        caseInput.ContactID = testContact2.Id;
        // commented by Hari on 31-12-2013
        //CaseDetail = WS_Z2Services.getCase(caseInput);
        /**************getCase End***********/
       } 
        
        static testMethod void testmethod2() {
         Country__c testCountry = Utils_TestMethods.createCountry();
         insert testCountry;
         
         Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
         testAccount.Name = 'Test';
         insert testAccount;
         
         Account testAccount2 = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
         testAccount2.Name = 'Test2';
         insert testAccount2;
                  
         Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
         testContact.Country__c = testCountry.Id;
         insert testContact;
         
         Contact testContact2 = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
         testContact2.Country__c = testCountry.Id;
         insert testContact2;
         
         Contract testContract = Utils_TestMethods.createContract(testAccount.Id , testContact.Id);
         insert testContract ;
         
         WS_Z2Services.CaseCreationObj caseObj = new WS_Z2Services.CaseCreationObj();
         caseObj.ContactID = testContact.Id;
         caseObj.Priority = 'Normal';
         caseObj.Subject = 'Test Case';
         caseObj.CustomerRequest = 'Test Description';
         caseObj.Category = '1';
         caseObj.Reason = 'Software Dissatisfaction';
         caseObj.Subreason = 'Other';
         caseObj.CVCPID = '';
         caseObj.Family = 'Test';
         caseObj.GMRCode = '123456789';
         
         //Without Custom Setting
         Z2OutputClass.CaseCreationResult CaseResult = new Z2OutputClass.CaseCreationResult();
         CaseResult = WS_Z2Services.createCase(caseObj);
         
         
        /**************getContractList***********/
        WS_Z2Services.ContractSearchObj contractobj = new WS_Z2Services.ContractSearchObj();
        contractobj.ContactID ='';
        contractobj.SType ='';
        contractobj.PackageTemplateID ='';
        contractobj.ExpiredType ='';
        contractobj.NumberOfDays =0;
        contractobj.SalesRep ='';
        contractobj.distributorContact ='';
        
        Z2OutputClass.ContractSearchResultList ContractList = new Z2OutputClass.ContractSearchResultList();
        ContractList = WS_Z2Services.getContractList(contractobj);
        
        contractobj.ContactID =testContact.Id;
        contractobj.SType ='F';
        contractobj.PackageTemplateID ='123456789012345';
        contractobj.ExpiredType ='B';
        contractobj.NumberOfDays =0;
        contractobj.SalesRep ='Test';
        contractobj.distributorContact ='Test';
        contractobj.Company = 'Test';
        ContractList = WS_Z2Services.getContractList(contractobj);
        
        contractobj.SType ='S';
        contractobj.ExpiredType ='N';
        ContractList = WS_Z2Services.getContractList(contractobj);
        
        contractobj.SType ='D';
        ContractList = WS_Z2Services.getContractList(contractobj);
        
        contractobj.SType ='M';
        ContractList = WS_Z2Services.getContractList(contractobj);
        
        contractobj.SType ='I';
        ContractList = WS_Z2Services.getContractList(contractobj);
        
        contractobj.SType ='C';
        ContractList = WS_Z2Services.getContractList(contractobj);
        contractobj.SType ='F';
        contractobj.ContactID =testContact.Id;
        contractobj.PackageTemplateID =null;
        contractobj.ExpiredType ='B';
        contractobj.NumberOfDays = null;
        contractobj.SalesRep =null;
        contractobj.distributorContact = null;
        contractobj.Company = null;
        ContractList = WS_Z2Services.getContractList(contractobj); 
        /**************getContractList End***********/
        
        /**************getContract***********/
        
         WS_Z2Services.ContractObj contractDet = new WS_Z2Services.ContractObj();
         contractDet.ContactID = testContact.Id;
         contractDet.ContractID = testContract.Id;
         Z2OutputClass.ContractResult ContractDetail = WS_Z2Services.getContract(contractDet);
         contractDet.ContactID = null;
         contractDet.ContractID = null;
         ContractDetail = WS_Z2Services.getContract(contractDet);
                  
        
        /**************getContract End***********/
        
        /**************addCaseNote***********/
         
          WS_Z2Services.CaseNoteObj note = new WS_Z2Services.CaseNoteObj();
         note.contactID = null;
         note.CaseID = null;
         note.Comments = null;
         Z2OutputClass.CaseNoteResult CaseCommentResult = WS_Z2Services.addCaseNote(note);
         note.contactID = testcontact.id;
         note.CaseID = CaseResult.CaseID;
         note.Comments = 'Test Comments';
         CaseCommentResult = WS_Z2Services.addCaseNote(note);
         note.contactID = testcontact.id;
         CaseCommentResult = WS_Z2Services.addCaseNote(note);
         note.CaseID = '123456789123456';
         CaseCommentResult = WS_Z2Services.addCaseNote(note);
         note.contactID = null;
         note.CaseID = CaseResult.CaseID;
         CaseCommentResult = WS_Z2Services.addCaseNote(note);  
         
        /**************addCaseNote End***********/ 
        
        /**************changeContractAccount***********/
        
         Z2OutputClass.cloneContractResult cloneContractResult = new Z2OutputClass.cloneContractResult();
         
         cloneContractResult = WS_Z2Services.changeContractAccount(null, null);
         cloneContractResult = WS_Z2Services.changeContractAccount(null, testAccount.Id);
         cloneContractResult = WS_Z2Services.changeContractAccount(testContract.Id, null);
         cloneContractResult = WS_Z2Services.changeContractAccount(testContract.Id, testAccount.Id);
         cloneContractResult = WS_Z2Services.changeContractAccount(testContract.Id, testAccount2.Id);
                 
        
        /**************changeContractAccount End***********/
        
        /**************getMyContactInfo***********/
        
         WS_Z2Services.ContactObj contact = new WS_Z2Services.ContactObj();
         contact.contactId = null;
         Z2OutputClass.ContactResult ContactDetail = WS_Z2Services.getMyContactInfo(contact);
         contact.contactId = testcontact.id;
         ContactDetail = WS_Z2Services.getMyContactInfo(contact);
         contact.contactId ='12345677898765';
         ContactDetail = WS_Z2Services.getMyContactInfo(contact);
        
        /**************getMyContactInfo End***********/
        
        /**************updateMyContactInfo ***********/
         test.starttest();        
         WS_Z2Services.ContactUpdateObj contactupdate = new WS_Z2Services.ContactUpdateObj();
         contactupdate.ContactID = null;
         contactupdate.ContactEmail = null;
         contactupdate.ContactPhone = null;
         contactupdate.Workfax = null;
         contactupdate.DeleteFields= null;
         Z2OutputClass.ContactUpdateResult ContactResult = WS_Z2Services.updateMyContactInfo(contactupdate);
         contactupdate.ContactID = testcontact.id;
         contactupdate.ContactEmail = 'test@test.com';
         contactupdate.ContactPhone = '1234567890';
         contactupdate.Workfax = '1234567890'; 
         ContactResult = WS_Z2Services.updateMyContactInfo(contactupdate);
         list<String> lstString = new list<String>();
         lstString.add('ContactEmail');
         lstString.add('ContactPhone');
         lstString.add('Workfax');
         contactupdate.DeleteFields = lstString;
         ContactResult = WS_Z2Services.updateMyContactInfo(contactupdate);    
        
        /**************updateMyContactInfo End***********/
        
        /**************getPackageTemplateList***********/
         
         WS_Z2Services.PackageTemplateObj pkgTemplate = new WS_Z2Services.PackageTemplateObj();
         pkgTemplate.ContactID =null;
         Z2OutputClass.PackageTemplateListResult PKGList = WS_Z2Services.getPackageTemplateList(pkgTemplate);
         pkgTemplate.ContactID = testContact.Id;
         pkgTemplate.Stype = 'F';
         PKGList = WS_Z2Services.getPackageTemplateList(pkgTemplate);
         pkgTemplate.Stype = 'M';
         PKGList = WS_Z2Services.getPackageTemplateList(pkgTemplate);
         pkgTemplate.Stype = 'D';
         PKGList = WS_Z2Services.getPackageTemplateList(pkgTemplate);
         pkgTemplate.Stype = 'I';
         PKGList = WS_Z2Services.getPackageTemplateList(pkgTemplate);
         pkgTemplate.ContactID = '123456789098765';
         PKGList = WS_Z2Services.getPackageTemplateList(pkgTemplate);
             
        
        /**************getPackageTemplateList End***********/
        
        /**************getUserRole***********/
        
         WS_Z2Services.ContactRoleObj role = new WS_Z2Services.ContactRoleObj();
         role.ContactID =null;
         Z2OutputClass.ValidContactResult UserRole =  WS_Z2Services.getUserRole(role);
         role.ContactID =testcontact2.ID;
         UserRole =  WS_Z2Services.getUserRole(role);
         
        /**************getUserRole End***********/
        test.stoptest();  
        
     }
     
     
}