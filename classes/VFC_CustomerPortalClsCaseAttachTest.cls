@isTest
public class VFC_CustomerPortalClsCaseAttachTest {

public static testMethod void VFC_CustomerPortalClsCaseDetailsAttach() {
 
    //Set up all conditions for testing.
    PageReference pageRef = Page.VFP_CustomerPortalClsCaseDetailsAttach;
    Test.setCurrentPage(pageRef);
        
    VFC_CustomerPortalClsCaseDetailsAttach controller = new VFC_CustomerPortalClsCaseDetailsAttach(); 
    VFC_CustomerPortalClsCaseDetailsAttach controller2 = new VFC_CustomerPortalClsCaseDetailsAttach(); 
    VFC_CustomerPortalClsCaseDetailsAttach controller3 = new VFC_CustomerPortalClsCaseDetailsAttach(); 
    VFC_CustomerPortalClsCaseDetailsAttach controller4 = new VFC_CustomerPortalClsCaseDetailsAttach();
        
    PageReference nextPage = controller.getCaseDetailView();
    
    // Verify that page fails without parameters
       System.assertEquals(null, nextPage);         
        
    account a = new account(name = 'name');
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345');
    
    
    case case1 = new case(ContactId=c.id, subject='case1', status='New');    
    insert(case1);
    case case2 = new case(ContactId=c.id, subject='case2', status='New');    
    insert(case2);
    Case case3 = new Case(ContactId=c.id, subject='case3', status='New');
    insert(case3);
    
    
    CaseComment comt1 = new CaseComment(ParentId=case1.id, CommentBody='My Comment body1', IsPublished=true);
    CaseComment comt2 = new CaseComment(ParentId=case1.id, CommentBody='My Comment body2', IsPublished=false);
    insert(comt1); insert(comt2);
            
                
    case caseDetails1 = [SELECT CaseNumber, Subject, family__c, Operating_System__c, SoftwareVersion__c, Status, SupportCategory__c, CustomerRequest__c, AnswerToCustomer__c, IsClosed FROM Case WHERE  Id =: Case1.Id]; 

    List<caseComment> caseComments1 = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC];
    List<Attachment> attachments1 = [SELECT Id, CreatedById, Name, CreatedDate, BodyLength, ContentType, Description, IsPrivate, LastModifiedDate FROM Attachment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC];

    casecomment cc = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: Case1.Id ORDER BY LastModifiedDate DESC][0]; 
    List<caseComment> caseCommentsOK = new List<caseComment>();
    caseCommentsOK.add(cc);
    
   
    Attachment att = new Attachment();
    Attachment att2 = new Attachment();
    Attachment att3 = new Attachment();
    
    att.Description='My description';
    att.name='test.txt';
    att.body=Blob.valueOf('Unit Test Attachment Body');
    controller.attach = att;
    
    att2.Description='';
    att2.name='test.txt';
    att2.body=Blob.valueOf('');
    controller2.attach = att2;
    att2.IsPrivate = true;

    att3.Description='';
    att3.name='test.txt';
    //att3.body=null;//Blob.valueOf('');
    controller3.attach = att3;
    
    // Testing Begin Here    
    Test.starttest(); 

//Nominal condition for saveAttachment function
    controller.setCaseId(case1.id);
    List<Attachment> lat = controller.getAttachments();
    controller.saveAttachment();
    Case c1 = controller.getCaseDetail();
    controller.getCaseComments();
    controller.getAttachments();
    controller.getCaseId();
    controller.setAttach(att);
    controller.getAttach();
    //System.assertEquals(c1.Status, c1.status);
    //System.assertEquals(controller.getCaseId(), case1.id);
    
//Testing condition 1 for saveAttachment function
    controller2.setCaseId(case2.Id);
    List<Attachment> lat2 = controller2.getAttachments();
    controller2.saveAttachment();
    controller2.getCaseDetail();
    controller2.getCaseComments();   
    controller.setCaseDetail(c1);
    
    
//Testing condition 2 for saveAttachment function
    controller3.setCaseId(case3.id);
    List<Attachment> lat3 = controller3.getAttachments();
    controller3.saveAttachment();
    Case c3 = controller3.getCaseDetail();
    controller3.getCaseComments();
    controller3.getAttachments();
    
    
//Evaluate when CaseId is Null and CaseComment is Null
    controller4.setCaseId(null);
    Case c4 = controller4.getCaseDetail();
//  System.assertEquals(controller2.getCaseId(), null);
   
    Test.stoptest(); 
    
}


}