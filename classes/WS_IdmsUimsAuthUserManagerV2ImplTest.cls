/*
* Test for WS_IdmsUimsAuthUserManagerV2Impl
* */
@IsTest 
public class WS_IdmsUimsAuthUserManagerV2ImplTest{
    //Test1 : Calling all method avaiable in WS_IdmsUimsAuthUserManagerV2Impl
    static testmethod void testMethod1(){
        WS_IdmsUimsAuthUserManagerV2Impl.AuthenticatedUserManager_UIMSV2_ImplPort  testuser=new WS_IdmsUimsAuthUserManagerV2Impl.AuthenticatedUserManager_UIMSV2_ImplPort();
        
        try{  
            
            WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType userFederatedIdAndType= new WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType();
            WS_IdmsUimsAuthUserManagerV2Service.userV5 id1=new WS_IdmsUimsAuthUserManagerV2Service.userV5();
            WS_IdmsUimsAuthUserManagerV2Service.accessElement app1=new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            WS_IdmsUimsAuthUserManagerV2Service.childs_element childs=new WS_IdmsUimsAuthUserManagerV2Service.childs_element();
            WS_IdmsUimsAuthUserManagerV2Service.createdIdentityReport return_x=new WS_IdmsUimsAuthUserManagerV2Service.createdIdentityReport();
            WS_IdmsUimsAuthUserManagerV2Service.accessElement app2=new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType return_x1=new WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType();
            WS_IdmsUimsAuthUserManagerV2Service.resetPassword rp= new WS_IdmsUimsAuthUserManagerV2Service.resetPassword();
            WS_IdmsUimsAuthUserManagerV2Service.RequestedEntryNotExistsException  rp1= new WS_IdmsUimsAuthUserManagerV2Service.RequestedEntryNotExistsException();
            
            WS_IdmsUimsAuthUserManagerV2Service.LdapTemplateNotReadyException rp2= new WS_IdmsUimsAuthUserManagerV2Service.LdapTemplateNotReadyException();
            WS_IdmsUimsAuthUserManagerV2Service.identity rp3= new WS_IdmsUimsAuthUserManagerV2Service.identity();
            WS_IdmsUimsAuthUserManagerV2Service.RequestedInternalUserException rp4= new WS_IdmsUimsAuthUserManagerV2Service.RequestedInternalUserException();
            WS_IdmsUimsAuthUserManagerV2Service.createIdentity rp5= new WS_IdmsUimsAuthUserManagerV2Service.createIdentity();
            WS_IdmsUimsAuthUserManagerV2Service.InactiveUserImsException rp6= new WS_IdmsUimsAuthUserManagerV2Service.InactiveUserImsException();
            WS_IdmsUimsAuthUserManagerV2Service.searchUser rp7= new WS_IdmsUimsAuthUserManagerV2Service.searchUser();
            WS_IdmsUimsAuthUserManagerV2Service.resetPasswordResponse rp8= new WS_IdmsUimsAuthUserManagerV2Service.resetPasswordResponse();
            WS_IdmsUimsAuthUserManagerV2Service.childs_element rp9= new WS_IdmsUimsAuthUserManagerV2Service.childs_element();
            WS_IdmsUimsAuthUserManagerV2Service.userV5 rp10= new WS_IdmsUimsAuthUserManagerV2Service.userV5();
            WS_IdmsUimsAuthUserManagerV2Service.createIdentityResponse rp11= new WS_IdmsUimsAuthUserManagerV2Service.createIdentityResponse();
            WS_IdmsUimsAuthUserManagerV2Service.reactivate rp12= new WS_IdmsUimsAuthUserManagerV2Service.reactivate();
            
        }
        catch(exception e){}
        try{
            testuser.searchUser('IDMSAdmin','test@test.com');
        }
        catch(exception e){}
        try{
            WS_IdmsUimsAuthUserManagerV2Service.accessElement application=new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            testuser.resetPassword('IDMSAdmin','testfedid',application);
        }
        catch(exception e){}
        try{
            WS_IdmsUimsAuthUserManagerV2Service.userV5 identity=new WS_IdmsUimsAuthUserManagerV2Service.userV5();
            WS_IdmsUimsAuthUserManagerV2Service.accessElement application1=new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            
            testuser.createIdentity('IDMSAdmin',identity,application1);
        }
        catch(exception e){}
        try{
            WS_IdmsUimsAuthUserManagerV2Service.accessElement application=new WS_IdmsUimsAuthUserManagerV2Service.accessElement();
            testuser.reactivate('IDMSAdmin','testfederatedId',application);}
        catch(exception e){}
    }
}