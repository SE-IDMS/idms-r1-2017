/*
    Author          : Hari Krishna Singara 
    Date Created    : 20/11/2012
    Description     : Class utilised by trigger CBATeam.
*/
public class AP_SharingAccessToCBATeam
{
    public static void insertCBATeamMember(Map<Id, CBATeam__c> newCBATeamMap)
    {
    
        List<OPP_QuoteLink__Share> quoteLinkShare= new List<OPP_QuoteLink__Share>();
        Set<ID> quoteID =new Set<ID>();
        quoteID = newCBATeamMap.keySet();
        List<OPP_QuoteLink__Share> oppQuoteLinkShare = [SELECT parentid,userOrGroupid  FROM OPP_QuoteLink__Share WHERE parentId IN : quoteID  AND  RowCause='GrantAccessToCBA__c'];
        Map<String,CBATeam__c> cbaTeam = new Map<String,CBATeam__c>();
        for(CBATeam__c  CBATeamMember  : newCBATeamMap.values())        
        {
            cbaTeam.put(String.valueOf(CBATeamMember.QuoteLink__c)+String.valueOf(CBATeamMember.id),CBATeamMember); 
                    
        }
        for(OPP_QuoteLink__Share  oppQuoteShare : oppQuoteLinkShare)
        {
                cbaTeam.keyset().remove(string.valueOf(oppQuoteShare.parentID)+string.valueOf(oppQuoteShare.userOrGroupid));         
        }
        set<String> cbaUsers= new set<String>();
        for(CBATeam__c  CBATeamObj  :  cbaTeam.values())
        {
            cbaUsers.add(newCBATeamMap.get(CBATeamObj.Id).CBAMember__c);
        }
        Set<String> cbaSalesUsers= new Set<String>();
        list<user> users=[select id, ProfileId, Permission_sets_assigned__c from user where id in : cbaUsers and (ProfileId=:Label.CLJUL13SLS01 or profileID =:label.CL00419)];// and profileID =:label.CL00419];
        users=Utils_Methods.filterUsers(users,new Set<String>{System.Label.CLJUL13SLS02},Label.CLJUL13SLS01,Label.CL00419);
        for(user user:users)
        {
            cbaSalesUsers.add(user.id);       
        }
        for(CBATeam__c  CBATeamObj  : cbaTeam.values())
        {
            OPP_QuoteLink__Share  oppQLKSCBATeam  = new OPP_QuoteLink__Share();
            oppQLKSCBATeam.UserOrGroupId  =  newCBATeamMap.get(CBATeamObj.Id).CBAMember__c;
           oppQLKSCBATeam.ParentID  =  newCBATeamMap.get(CBATeamObj.ID).Quotelink__c;
            if(cbaSalesUsers.contains(newCBATeamMap.get(CBATeamObj.Id).CBAMember__c))
            {
                oppQLKSCBATeam.AccessLevel=label.CL00418;
            }
            else
            {
                oppQLKSCBATeam.AccessLevel=label.CL00203;
            }
            oppQLKSCBATeam.RowCause=Schema.OPP_QuoteLink__Share.RowCause.GrantAccessToCBA__c;
            quoteLinkShare.add(oppQLKSCBATeam);
            system.debug('QuoteLinkShare'+quoteLinkShare);
        }
        if(quoteLinkShare.Size() > 0)
        {
            if(quoteLinkShare.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP16 Error updating : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] lsr = database.insert(quoteLinkShare,false);
                for( Database.SaveResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];               
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                        err.getMessage().contains('AccessLevel')){ 
                        system.debug('Success *Trivial Access*');
                    } 
                    else
                    {
                        system.debug('failed * Event share*'); 
                    } 
                    } 
                }
            }
        } 
        
        
        
        
        
    }
    public static void deleteExistingCBATeamMember(Map<Id,CBATeam__c> deleteCBATeamMap)
    {        
      
        System.Debug('******deleteExistingCBATeamMember method in SharingAccessToCBATeam Class Start****');
        Map<String,id> cbaMap = new  Map<String,id>();
        Set<String> existingCbaSet = new  Set<String>();
        set<Id> userId = new set<id>();
        set<Id> parentId = new set<Id>();
        for(CBATeam__c  cbaTeamMember :  deleteCBATeamMap.values())
        {        
            cbaMap.put(String.valueOf(cbaTeamMember.QuoteLink__c)+String.valueOf(cbaTeamMember.CBAMember__c),cbaTeamMember.QuoteLink__c); 
            userId.add(cbaTeamMember.CBAMember__c);
        } 
        List<CBATeam__c> existingCBAList= new List<CBATeam__c>([Select Id,name,QuoteLink__c,CBAMember__c from CBATeam__c where QuoteLink__c IN:cbaMap.values() AND CBAMember__c IN: userId ]);     
        

            for(CBATeam__c existingCBA : existingCBAList){
                 existingCbaSet.add(String.valueOf(existingCBA.QuoteLink__c)+String.valueOf(existingCBA.CBAMember__c)); 
            }
        
           for(String duplicate: existingCbaSet){
              cbaMap.remove(duplicate);
           }
        List<OPP_QuoteLink__Share> newQLSCBADelete = new List<OPP_QuoteLink__Share>();
        
        List<OPP_QuoteLink__Share> deleteQLSCBAList = [SELECT UserOrGroupId,ParentId,RowCause FROM OPP_QuoteLink__Share
                                                    WHERE RowCause = 'GrantAccessToCBA__c' and ParentId IN: cbaMap.values()];

        for(OPP_QuoteLink__Share  oppQLKShareObj: deleteQLSCBAList)
        {
            if(cbaMap.keyset().contains(string.valueOf(oppQLKShareObj.parentID)+string.valueOf(oppQLKShareObj.userOrGroupid)))
            {
                newQLSCBADelete.add(oppQLKShareObj);
            }
        }           
        system.debug('****'+ newQLSCBADelete);
        if(newQLSCBADelete.Size() > 0)
        {
            if(newQLSCBADelete.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP16 Error updating : '+ Label.CL00264);
            }
            else
            {
                Database.DeleteResult[] lsr = database.delete(newQLSCBADelete,false);
                for( Database.DeleteResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];               
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                        err.getMessage().contains('AccessLevel')){ 
                        system.debug('Success *Trivial Access*');
                    } 
                    else
                    {
                        system.debug('failed * Event share*'); 
                    } 
                    } 
                }
            }
        }         
        System.Debug('***deleteExistingCBATeamMember method in AP17_SharingAccessToCBATeam Class End***'); 
         
    }
    public static void updateCBATeamMember(Map<Id,CBATeam__c> newCBATeam,Map<Id,CBATeam__c> oldCBATeam)    
    {
        
        System.Debug('***updateCBATeamMember method in AP_SharingAccessToCBATeam  Class Start***');  
        deleteExistingCBATeamMember(oldCBATeam);
        insertCBATeamMember(newCBATeam);       
        System.Debug('***updateCBATeamMember method in AP_SharingAccessToCBATeam Class End***');  
        
    }
    
    
    
}