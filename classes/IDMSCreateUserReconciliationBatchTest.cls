@isTest
public class IDMSCreateUserReconciliationBatchTest {

    static testmethod void testmapIdmsCreateUserUims(){
        try{
        
        IDMSCreateUserReconciliationBatch idmsCreateUserReconcBatchcls=new IDMSCreateUserReconciliationBatch();
        IDMSUserReconciliationBatchHandler__c handler=new IDMSUserReconciliationBatchHandler__c (AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test134',NbrAttempts__c=1);
        insert handler;  
        IDMSUserReconciliationBatchHandler__c handler1=new IDMSUserReconciliationBatchHandler__c (AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test133',NbrAttempts__c=1);
        insert handler1;
        Test.starttest();                                                           
        Database.executeBatch(idmsCreateUserReconcBatchcls,20);
        Test.stoptest();
        }
        catch(Exception e){ }
    }
}