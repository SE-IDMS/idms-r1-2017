@IsTest
class AP28_WorkOrderLocalizedNotification_TEST
{
    static testMethod void testAP28_WorkOrderLocalizedNotificationMethods() 
    {
        /*
        //Profile profile = [select id from profile where name='SE - Customer Care Agent Level 1']; 
        // Modified this below line by GD Team ,CLCCCMAR120001 holds "SE - Customer Care Agent - Primary"  as value.    
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        */
        User user;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.isActive=true;
        System.runAs(sysAdmin){ 
        UserandPermissionSets userandpermissionsetrecord=new UserandPermissionSets('user','SE - Customer Care Agent - Primary');
        user = userandpermissionsetrecord.userrecord;
        insert user;
    
        List<PermissionSetAssignment> permissionsetassignmentlst=new List<PermissionSetAssignment>();    
        for(Id permissionsetId:userandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
            permissionsetassignmentlst.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
        insert permissionsetassignmentlst; 
        }
       // Avoid having a MIXED_DML_OPERATION error 
       System.runAs(user){
         Folder folder = [Select Name,Id From Folder where Name='ServiceMax Email Templates'];
         EmailTemplate template1 = new EmailTemplate(isActive = true, developerName = 'TPL_1',folderid = folder.id, TemplateType= 'Text', Name = 'TPL_1');
         insert template1;
         EmailTemplate template2 = new EmailTemplate(isActive = true, developerName = 'TPL_2',folderid = folder.id, TemplateType= 'Text', Name = 'TPL_2');
         insert template2;
       }
       
       Account Acc = Utils_TestMethods.createAccount();
       insert Acc;
       Contact contact1 = Utils_TestMethods.createContact(Acc.id, 'Doe');
       contact1.email = 'john.doe@acme.com';
       contact1.CorrespLang__c = 'XXX';
       Insert contact1;
       SVMXC__Service_Order__c workOrder1 = Utils_TestMethods.createWorkOrder(Acc.Id);
       workOrder1.SVMXC__Contact__c = contact1.id;
       insert WorkOrder1;
       Contact contact2 = Utils_TestMethods.createContact(Acc.id, 'Doe');
       contact2.email = 'john.doe@acme.com';
       contact2.CorrespLang__c = 'YYY';
       Insert contact2;
       SVMXC__Service_Order__c workOrder2 = Utils_TestMethods.createWorkOrder(Acc.Id);
       workOrder2.SVMXC__Contact__c = contact2.id;
       insert WorkOrder2;
       ServiceNotificationsSetting__c setting1 = new ServiceNotificationsSetting__c(Name = 'SETTING 1', EmailTemplate__c = 'TPL_1', LanguageCode__c = 'XXX', Type__c = 2);
       insert setting1;
       ServiceNotificationsSetting__c setting2 = new ServiceNotificationsSetting__c(Name = 'DEFAULT SETTING', EmailTemplate__c = 'TPL_2', LanguageCode__c = 'EN', Type__c = 2);
       insert setting2;
       
       List<ID> workOrderIds = new List<ID>();
       workOrderIds.add(WorkOrder1.id);
       workOrderIds.add(WorkOrder2.id);
 
       AP28_WorkOrderLocalizedNotification.sendEmailNotification(workOrderIds, 2);          
    }
}