/*
------------------------------
   Author : A.D.N.V.SATYANARAYANA
   Description : Test class for VFC_PRMProgAssignment
   Created Date : 20-06-2016
------------------------------   */

@isTest
public class VFC_ProgramLevelBrand_Test{

    static testMethod void testMe(){
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
        Brand__c brnd = new Brand__c();
        brnd.Name='TestBrand'; 
        insert brnd;
        PartnerProgram__c ppm=new PartnerProgram__c();
ppm.name='APC Value Added Tester';
ppm.ProgramType__c='Business';
insert ppm;
        ProgramLevel__c pml = new ProgramLevel__c();
        pml.Description__c='This is a Test Description';
        pml.ModifiablebyCountry__c='Yes';
       //pml.Hierarchy__c='1';
        pml.Name='TestRegistration';
        pml.PartnerProgram__c=ppm.Id;
        insert pml;

        ProgramLevelBrand__c plb = new ProgramLevelBrand__c();
        plb.Brand__c= brnd.Id;
        plb.ProgramLevel__c=pml.Id;
        insert plb;
        Test.SetCurrentPageReference(New PageReference('Page.VFC_ProgramLevelBrand'));
        ApexPages.Standardcontroller sc2 = New ApexPages.StandardController(plb);
        System.CurrentPageReference().getParameters().put('id',plb.Id);
        VFC_ProgramLevelBrand plb2 = new VFC_ProgramLevelBrand(sc2); 

    }
    }

}