/*
    Author: Bhuvana S(GD Solutions)
    Purpose: Merged different functionalities of Product Line After insert to a single class
*/
public with sharing class AP_ProductLineBeforeUpdateHandler 
{    
    List<OPP_ProductLine__c> prodLines= new List<OPP_ProductLine__c>();
    
    public void OnBeforeUpdate(List<OPP_ProductLine__c> newProductLines,Map<Id,OPP_ProductLine__c> newProductLinesMap,List<OPP_ProductLine__c> oldProductLines,Map<Id,OPP_ProductLine__c> oldProductLinesMap)
    {
        // EXECUTE Before Update LOGIC
        for(OPP_ProductLine__c prodLne: newProductLines)
        {
            if(prodLne.LineAmount__c!=null && prodLne.LineAmount__c!= oldProductLinesMap.get(prodLne.Id).LineAmount__c)
                prodLines.add(prodLne);
             /*For PRM oct 13 Release:
                Update Product line picklist if value is entered for Product Line text
                Update Prodct Line text if product line picklist is selected.*/
            if(prodLne.PartnerProductLine__c != oldProductLinesMap.get(prodLne.Id).PartnerProductLine__c)
            {    
                Map<String, CS_ProductLineBusinessUnit__c> mapProductLineBU = CS_ProductLineBusinessUnit__c.getAll();
                if(mapProductLineBU.containsKey(prodLne.PartnerProductLine__c))
                {
                    prodLne.ProductLine__c = mapProductLineBU.get(prodLne.PartnerProductLine__c).ProductLine__c;
                    prodLne.ProductBU__c = mapProductLineBU.get(prodLne.PartnerProductLine__c).ProductBU__c;
                }
            }
            if(prodLne.ProductLine__c != oldProductLinesMap.get(prodLne.Id).ProductLine__c && prodLne.ProductLine__c.length() >= 5)
                prodLne.PartnerProductLine__c = prodLne.ProductLine__c.substring(0,5);
        }
        
    }
}