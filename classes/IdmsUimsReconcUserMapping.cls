public class IdmsUimsReconcUserMapping {
     //Mapping of IDMS - UIMS User. 
    public static IdmsUsersResyncflowServiceIms.contactBean mapIdmsUserUims(User usr){
        IdmsUsersResyncflowServiceIms.contactBean uimsContactBean = new IdmsUsersResyncflowServiceIms.contactBean();
        uimsContactBean.Email = (usr.email !=null && !((string)usr.email).equalsignorecase('null'))?usr.email:null;
        uimsContactBean.FIRST_NAME = (usr.firstname !=null && !((string)usr.firstname ).equalsignorecase('null'))?usr.firstname:null;
        uimsContactBean.LAST_NAME = (usr.LastName !=null && !((string)usr.LastName).equalsignorecase('null'))?usr.LastName:null;
        uimsContactBean.LANGUAGE_CODE = (usr.IDMS_PreferredLanguage__c !=null && !((string)usr.IDMS_PreferredLanguage__c ).equalsignorecase('null'))?usr.IDMS_PreferredLanguage__c:null;
        uimsContactBean.COUNTRY_CODE = (usr.Country !=null && !((string)usr.Country ).equalsignorecase('null'))?usr.Country:null;
        uimsContactBean.FAX = (usr.fax !=null && !((string)usr.fax ).equalsignorecase('null'))?usr.fax:null;
        uimsContactBean.PHONE = (usr.phone !=null && !((string)usr.phone ).equalsignorecase('null'))?usr.phone:null;
        uimsContactBean.COUNTY = (usr.IDMS_County__c  !=null && !((string)usr.IDMS_County__c).equalsignorecase('null'))?usr.IDMS_County__c:null; 
        uimsContactBean.STREET =  (usr.street !=null && !((string)usr.street ).equalsignorecase('null'))?usr.street:null;  
        uimsContactBean.ZIP_CODE = (usr.PostalCode !=null && !((string)usr.PostalCode ).equalsignorecase('null'))?usr.PostalCode:null;
        uimsContactBean.POBOX = (usr.IDMS_POBox__c !=null && !((string)usr.IDMS_POBox__c ).equalsignorecase('null'))?usr.IDMS_POBox__c:null; 
        uimsContactBean.CELL =  (usr.mobilephone !=null && !((string)usr.mobilephone ).equalsignorecase('null'))?usr.mobilephone :null; 
        uimsContactBean.STATE_PROVINCE_CODE= (usr.state !=null && !((string)usr.state ).equalsignorecase('null'))?usr.state :null; 
        uimsContactBean.CITY = (usr.city!=null && !((string)usr.city).equalsignorecase('null'))?usr.city:null;  
        uimsContactBean.ADD_INFO_ADDRESS = (usr.IDMS_AdditionalAddress__c !=null && !((string)usr.IDMS_AdditionalAddress__c ).equalsignorecase('null'))?usr.IDMS_AdditionalAddress__c :null;   
        uimsContactBean.JOB_TITLE_CODE = (usr.Job_Title__c!=null && !((string)usr.Job_Title__c).equalsignorecase('null'))?usr.Job_Title__c:null;
        uimsContactBean.JOB_FUNCTION_CODE =  (usr.Job_Function__c !=null && !((string)usr.Job_Function__c ).equalsignorecase('null'))?usr.Job_Function__c :null;
        uimsContactBean.JOB_DESCRIPTION = (usr.IDMSJobDescription__c!=null && !((string)usr.IDMSJobDescription__c).equalsignorecase('null'))?usr.IDMSJobDescription__c:null;
        uimsContactBean.MIDDLE_NAME =  (usr.IDMSMiddleName__c!=null && !((string)usr.IDMSMiddleName__c).equalsignorecase('null'))?usr.IDMSMiddleName__c:null; 
        uimsContactBean.SALUTATION_CODE = (usr.IDMSSalutation__c!=null && !((string)usr.IDMSSalutation__c).equalsignorecase('null'))?usr.IDMSSalutation__c:null;
        system.debug('*****UIMS User to create from mapping method*****'+uimsContactBean);
        return uimsContactBean;
    }
}