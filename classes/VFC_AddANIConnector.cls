/*
    Author          : Rakhi Kumar 
    Date Created    : 25/02/2013
    Description     : Controller class utilised by VFP_AddANIConnector.
                      CTI Integration module for  May 2013 Release
    -----------------------------------------------------------------
                              MODIFICATION HISTORY
    -----------------------------------------------------------------
    Modified By     : Anil Budha
    Modified Date   : 17/03/2014
    Description     : Added code for handling Ani Creation means if the ANI is not existed then redirect the page to VFP_QuickANIUpdate page
    -----------------------------------------------------------------
*/
public class VFC_AddANIConnector {
    public String ANINum {get;set;}
    public String ANINumEncoded {get;set;}
    public String conID{get;set;}
    public String accID{get;set;}
    {ANINum = '';}
    {conID = '';}
    {accID = '';}
    
    //Method to create ANI object in bFO
    public PageReference AddANINumber()
    {   
        if(ApexPages.currentPage().getParameters().containsKey('ANI')){
            ANINum = ApexPages.currentPage().getParameters().get('ANI');
        }
        ANINumEncoded =EncodingUtil.urlEncode(AniNum,'UTF-8');
        System.Debug('######ANINumEncoded :' + ANINumEncoded );
       
        if(ANINum.length() > 0){
            System.Debug('######' + ANINum );
        }
            if(ApexPages.currentPage().getParameters().containsKey('contactID'))
                conID = ApexPages.currentPage().getParameters().get('contactID');
            if(ApexPages.currentPage().getParameters().containsKey('accountID'))
                accID = ApexPages.currentPage().getParameters().get('accountID');

            String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
            PageReference referenceURL = null;
            List<ANI__c> matchedANIs = null;
            if(conID.length() > 0) {
                //Verify if the ANI record already exists for the given contact
                System.debug('**conid'+conID);
                System.debug('**Aninumber'+ANINum);
                System.Debug('######ANINumEncoded :' + ANINumEncoded );
               if(ANINumEncoded.startsWith('+')){
                 matchedANIs = [SELECT Id,ANINumber__c  FROM ANI__c WHERE ANINumber__c = :ANINumEncoded  AND Contact__c = :conID];
                 }
                 else{
                 matchedANIs = [SELECT Id,ANINumber__c  FROM ANI__c WHERE ANINumber__c = :ANINum  AND Contact__c = :conID];
                 }
                
                System.debug('**'+matchedANIs); 
                if(matchedANIs.size() > 0){
                    referenceURL = new PageReference(baseURL + '/' + matchedANIs[0].id);//Redirect user to the first existing ANI
                }
                else{
                    System.debug('*AniNum before Encode:' + AniNum);  
                    AniNum =EncodingUtil.urlEncode(AniNum,'UTF-8');
                    if(ANINum.trim().startsWith('+')) // To handle the URL with ANI Number with + signs.
                    AniNum =EncodingUtil.urlEncode(AniNum,'UTF-8');
                    System.debug('*AniNum after Encode:' + AniNum);  
                  referenceURL = new PageReference(Label.CLAPR14CCC17+ANINum+Label.CLAPR14CCC18+conID);
                }
            }
            else{
                //Verify if the ANI record already exists for the given contact
                System.Debug('######ANINumEncoded :' + ANINumEncoded );
                 if(ANINumEncoded.startsWith('+')){
                matchedANIs = [SELECT Id FROM ANI__c WHERE ANINumber__c = :ANINumEncoded AND Account__c = :accID]; 
                }
             else{
                 matchedANIs = [SELECT Id,ANINumber__c  FROM ANI__c WHERE ANINumber__c = :ANINum  AND Account__c = :accID];
             } 
                if(matchedANIs.size() > 0){
                    referenceURL = new PageReference(baseURL + '/' + matchedANIs[0].id);//Redirect user to the first existing ANI
                }
                else{
                    AniNum =EncodingUtil.urlEncode(AniNum,'UTF-8');
                    if(ANINum.trim().startsWith('+')) // To handle the URL with ANI Number with + signs.
                    AniNum =EncodingUtil.urlEncode(AniNum,'UTF-8');     
                    referenceURL = new PageReference(Label.CLAPR14CCC17+ANINum+Label.CLAPR14CCC19+AccId);
                }
            }
            //Redirect to ANI Details page
            referenceURL .setRedirect(true);
            return referenceURL ; 
                
    }//End of AddANINumber()

}