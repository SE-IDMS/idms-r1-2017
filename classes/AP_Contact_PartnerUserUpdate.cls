/*
1-Mar-13    Shruti Karn            PRM Mar13 Release    BR-2782    Initial Creation
5-Mar-13    Srinivas Nallapati     PRM Mar13 Release    BR-2782    Updated Email alert  

*/
public class AP_Contact_PartnerUserUpdate{
    
    @future
    public static void insertPartnerUser (set<id> setContactId)
    {
		Id loggedInUserProfileId = userInfo.getProfileId();
        System.debug('**Loggedin Profile Id**'+loggedInUserProfileId);
		// Bypassing Partner User Insert for Merge User.CLAPR15PRM275
        if(!String.Valueof(loggedInUserProfileId).Contains(System.Label.CLAPR15PRM275)){
        list<contact> lstContactwithError = new list<Contact>();
        list<contact> lstContact = [select name, firstname, lastname, email,correspLang__c, phone, timeZone__c,PRMUIMSID__c,Account.recordTypeId from Contact WHERE id IN :setContactId];
        Map<String, CS_ContactLanguageToUserLanguage__c> mapLang= CS_ContactLanguageToUserLanguage__C.getAll();
        List<User> lstPartnerUser = [select id, ContactId, isActive from User where ContactId in :lstContact and isActive=true];
        Set<id> existingPartnerContactIds = new set<id>();
        Boolean isUserError = false;
        for(User us : lstPartnerUser )
        {
            existingPartnerContactIds.add(us.ContactId);
        }
        
        system.debug('AP_Contact_PartnerUserUpdate:insertPartnerUser () start');
        list<User> lstUser = new list<User>();
        //Database.DMLOptions dlo = new Database.DMLOptions();
        //dlo.EmailHeader.triggerUserEmail= true;
        
        for(Contact c: lstContact)
        {
           if(!existingPartnerContactIds.contains(c.id)) // Contact does not have existing active user
           {
                String alias;
                if(c.firstname!= null && c.firstname.length() >= 1)
                    alias = c.firstname.substring(0,1);
                else
                    alias = c.firstname;
                if(c.lastname!= null && c.lastname.length() >= 4)
                    alias = alias + c.lastname.substring(0,4);
                else
                    alias = alias+c.lastname;
                User u = new User(
                        alias = alias , 
                        email=c.email,
                        emailencodingkey=Label.CLMAR13PRM02,
                        firstname=c.firstname,
                        lastname=c.lastname,
                        //languagelocalekey=c.CorrespLang__c,
                        //localesidkey=c.CorrespLang__c, 
                        languagelocalekey='en_US',
                        localesidkey='en_US', 
                        //profileid = Label.CLMAR13PRM03,
                        contactid = c.id,
                        timezonesidkey=Label.CLMAR13PRM04
                        //username =c.firstname.replaceAll(' ','' )+c.lastname.replaceall(' ','')+Label.CLMAR13PRM05
                        
                        //username =c.email+Label.CLMAR13PRM05 //APR15
                    );
                
                //APR15
                
                      u.username =c.email+Label.CLMAR13PRM05; 
                      u.profileid = Label.CLAPR15PRM026; 
                
                //END APR15
                                  
                if(mapLang.ContainsKey(c.correspLang__c) &&  mapLang.get(c.correspLang__c)!= null )
                {
                    u.languagelocalekey = mapLang.get(c.correspLang__c).UserLanguageKey__c;
                    u.localesidkey = mapLang.get(c.correspLang__c).UserLanguageKey__c;
                }

                if(c.timeZone__c != null)
                    u.timezonesidkey = c.timeZone__c;
                    
                    
                //MAY15
                if(!string.isBlank(c.PRMUIMSID__c))
                    u.FederationIdentifier = c.PRMUIMSID__c;
                
                 lstUser.add(u);
            }  
        }

        if(!lstUser.isEmpty())
        {
            Map<id,Contact> mapContacts = new Map<id,Contact>(lstContact);
            //START:21-May-2014 Disabling welcome mail on creation of partner user
            Database.DMLOptions dlo = new Database.DMLOptions();
            dlo.EmailHeader.triggerOtherEmail = false;
            dlo.EmailHeader.triggerUserEmail = false;
            dlo.optAllOrNone = false;
            //END:21-May-2014
            Boolean NoError = true; 
            String ErrorDetails=' ';
            Database.SaveResult[] SaveResult = database.insert(lstUser,dlo);
            //SendPRMUserFaileMail(ErrorDetails);

            for(Integer k=0;k<SaveResult.size();k++ )
            {
                Database.SaveResult sr =SaveResult[k];
                if(!sr.isSuccess())
                {
                    isUserError = true;
                    NoError = false;
                    Database.Error err = sr.getErrors()[0];
                    //lstContact[0].addError(Label.CLMAR13PRM01+' ' +sr.getErrors());
                    Contact tmpCon = mapContacts.get(lstUser[k].ContactID); 
                    tmpCon.PRMUser__c = false;
                    lstContactwithError.add(tmpCon);
                    ErrorDetails = ErrorDetails+ '<br/><br/>Contact Name : '+ tmpCon.name+'<br/>Contact ID : '+tmpCon.id + '<br/>Erros : '+sr.getErrors();

                }
            }
            Integer subBatchSize;
            Integer subBatchCount;
            if(SaveResult.size() > 10)
            {
                subBatchSize = 10;
                Decimal decBatchCount = (Decimal)SaveResult.size() / subBatchSize;
                subBatchCount = (Integer) (decBatchCount.round(system.roundingMode.CEILING));
                
            }
            else
            {
                subBatchCount = SaveResult.size();
                subBatchSize =  1;
            }
            for (Integer iterCounter = 0; iterCounter < subBatchCount; iterCounter++) {
                list<GroupMember> lstGroupMem = new list<GroupMember>();
                for (Integer i = 0; i < subBatchSize; i++) {
                    Integer m = (iterCounter * subBatchSize) + i;
                    if(m<=SaveResult.size()-1)
                    if (SaveResult[m].isSuccess()) {
                        GroupMember newMember = new GroupMember();
                        newMember.GroupId = Label.CLMAR13PRM026;
                        newMember.UserOrGroupId = SaveResult[m].Id;
                        lstGroupMem.add(newMember);
                    }
                }
                insert lstGroupMem;
            } 
               if(!lstContactwithError.isEmpty())
               {
                   Database.SaveResult[] ContactSaveResult = database.update(lstContactwithError,false);
                                
                   
                   for(Integer i=0;i<ContactSaveResult.size();i++ )
                   {
                       Database.SaveResult sr =ContactSaveResult[i];
                       if(!sr.isSuccess())
                       {
                          //isUserError =  false;
                          //NoError = false;
                          Database.Error err = sr.getErrors()[0];
                          //lstContact[0].addError(Label.CLMAR13PRM01+'  ' +sr.getErrors());
                          Contact tmpCon = mapContacts.get(lstUser[i].ContactID);  
                          //tmpCon.PRMUser__c = false;
                          //lstContactwithError.add(tmpCon);        
                          ErrorDetails = ErrorDetails+ '<br/><br/>Contact Name :  '+ tmpCon.name+'<br/>Contact ID :  '+tmpCon.id + '<br/>Erros :  '+sr.getErrors();
                          system.debug('Error in Updating the Contact');
                       }
                     
                   } 
               }
               
               if(NoError == false)
               {
                   SendPRMUserFaileMail(ErrorDetails,isUserError);
               }     
               
             //insert lstUser;

            
         }  
        
        system.debug('AP_Contact_PartnerUserUpdate:insertPartnerUser () ends'); 
    }//End of method
    }
    
 
    public static void SendPRMUserFaileMail(String ErrorDetails, Boolean isUserError)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                   
        String[] toAddresses = new String[]{};
        toAddresses.addAll(System.Label.CLMAR13PRM021.split(',',0));
        //String[] ccAddresses = new String[] {'venkatasrinivasu.nallapati@schneider-electric.com'};
        
        mail.setToAddresses(toAddresses);
        //mail.setCcAddresses(ccAddresses);
        
        //mail.setReplyTo('bfo-noreply@schneider-electric.com');
        
        mail.setSenderDisplayName(System.Label.CLMAR13PRM022);
        if(isUserError)
        {
            mail.setSubject(System.Label.CLMAR13PRM023);
            ErrorDetails =   System.Label.CLMAR13PRM024 +'<br/>'+ ErrorDetails;
        }
        else
        {
            mail.setSubject(System.Label.CLMAR13PRM027);
            ErrorDetails =   System.Label.CLMAR13PRM028 +'<br/>'+ ErrorDetails;
        }
        mail.setBccSender(false);
        mail.setUseSignature(false);
       
        // mail.setPlainTextBody(ErrorDetails);
        
        mail.setHtmlBody(ErrorDetails);
        
        Messaging.sendEmail(new Messaging.email[] { mail });
     
    }//End of method
    
    
    
    
    @future
    public static void disablePartnerContact(set<Id> setContactId)
    {
        system.debug('AP_Contact_PartnerUserUpdate:enable_DisablePartnerUser() start');
        list<User> lstUser = new list<User>();
        map<Id,User> mapUser = new map<Id,User>();
        lstUser = [Select contactid,id,IsPortalEnabled ,username from user where contactid in :  setContactId limit 10000];
        if(!lstUser.isEmpty())
        {
            for(User u: lstUser)
                mapUser.put(u.contactid,u);
            
            for(Id contId : setContactId)
            {
                mapUser.get(contID).IsPortalEnabled = false;
                mapUser.get(contID).IsActive = false;
                mapUser.get(contID).username = '_'+mapUser.get(contID).username+System.now();
            }
            if(!mapUser.values().isEmpty())
            {
                try
                {
                    update mapUser.values();    
                }
                catch(Exception e)
                {
                    system.debug('Error while disabling Partner User');
                }
            }
        }
        system.debug('AP_Contact_PartnerUserUpdate:enable_DisablePartnerUser() ends');          
    }
    
    // Freeze the users based on the "PRMContactRegistrationStatus__c" values
    
    @future
    public Static Void toggleUser (set<Id> contactIds)
    {
        list<User> toinactiveuser = new List<User>();
        
        
        list<User> freezeUsers = [select id,isActive from user where contactid IN :contactIds ];
        //selects s = 'select id,IsFrozen,UserId from' +sObjectname;
        
        for(User u:freezeUsers) {
            u.isActive = !u.isActive;
            toinactiveuser.add(u); 
        } 
        try{
            
            if(!toinactiveuser.isEmpty())
               {
                   Database.SaveResult[] UserSaveResults = database.update(toinactiveuser,false);
                                
                   
                   for(Integer i=0;i<UserSaveResults.size();i++ )
                   {
                       Database.SaveResult sr =UserSaveResults[i];
                       if(!sr.isSuccess())
                       {
                          Database.Error err = sr.getErrors()[0];
                          string id = sr.getid();
                          system.debug('Error in Updating the Contact'+err+' The id of the errored records is ' +id);
                       }
                     
                   } 
               }
        }
        catch(exception e) {
            system.debug('the exception'+e);
        }
    }
    
    public static void updatePRMAdminEmail(List<Contact> Contacts){
        
        Set<Id> PRMCountryIds = new Set<Id>();
        for(Contact con:Contacts){
            if(con.PRMContact__c && String.isnotBlank(con.PRMCountryClusterSetup__c))
            PRMCountryIds.add(con.PRMCountryClusterSetup__c);
        }
        Map<Id,PRMCountry__c> PRMCountryMap = new Map<Id,PRMCountry__c>([SELECT Id,CountrySupportEmail__c FROM PRMCountry__c WHERE ID IN :PRMCountryIds AND CountryPortalEnabled__c = true]);
        
        for(Contact con:Contacts){
            if(con.PRMContact__c && String.isnotBlank(con.PRMCountryClusterSetup__c))
                con.PRMAdminEmail__c = PRMCountryMap.ContainsKey(con.PRMCountryClusterSetup__c) ? PRMCountryMap.get(con.PRMCountryClusterSetup__c).CountrySupportEmail__c : null;
        }
    }
    /*@future
    public static void notifyPrimaryContact(set<Id> accountIds){
        try{
            map<Id,List<String>> mapSEPerson = new map<Id,List<String>>();
            List<contact> primaryContacts = [Select Id, AccountId,PRMPrimaryContact__c,PRMEmail__c from Contact where AccountId IN :accountIds AND PRMPrimaryContact__c = True];
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            for(contact c:primaryContacts){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                System.debug('****** Mail Send ***');
                mail.settargetObjectId(c.ID);
                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                mail.settemplateId('00X17000000QPyT'); 
                //mail.setwhatId(c.ID);
                if(c.PRMEmail__c != null)
                mapSEPerson.put(c.Id,new List<String>{(c.PRMEmail__c)});
                mail.setSaveAsActivity(false);
                lstMail.add(mail);
            }
            if(!lstMail.isEmpty()){
                //Savepoint sp = Database.setSavepoint();
                Messaging.sendEmail(lstMail);
                /*Database.rollback(sp);
                List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
                for (Messaging.SingleEmailMessage email : lstMail) {
                    Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                    if(!mapSEPerson.containsKey(email.whatId)) continue;
                    emailToSend.setToAddresses(mapSEPerson.get(email.whatId));
                    System.debug('****** To Address **** :'+mapSEPerson.get(email.whatId));
                    emailToSend.setPlainTextBody(email.getPlainTextBody());
                    emailToSend.setHTMLBody(email.getHTMLBody());
                    emailToSend.setSubject(email.getSubject());
                    lstMsgsToSend.add(emailToSend);
                }

                Messaging.sendEmail(lstMsgsToSend);
            }
        }
        catch(Exception e){
            system.debug('Exception notifyPrimaryContact:Error in sending email to Primary Contact:'+e.getMessage());
        }
    }*/
}