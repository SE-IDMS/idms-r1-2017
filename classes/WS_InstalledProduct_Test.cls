@isTest
private class WS_InstalledProduct_Test {

    static testMethod void WS_InstalledProductExecution() {
        // TO DO: implement unit test
        //Test Data 
        //Account 
        
        List<CS_AccountToLocation__C> atlList = new List<CS_AccountToLocation__C>();
        CS_AccountToLocation__C cs1 = new CS_AccountToLocation__C();        
        cs1.Name = 'city';
        cs1.SourceObjField__c = 'City__c';
        cs1.TargetObjField__c ='SVMXC__City__c';
        atlList.add(cs1);
        CS_AccountToLocation__C cs2 = new CS_AccountToLocation__C();   
        cs2.Name = 'Street';     
        cs2.SourceObjField__c = 'Street__c';
        cs2.TargetObjField__c ='SVMXC__Street__c';
        atlList.add(cs2);
       
        CS_AccountToLocation__C cs3 = new CS_AccountToLocation__C();  
        cs3.Name = 'ZipCode';       
        cs3.SourceObjField__c = 'ZipCode__c';
        cs3.TargetObjField__c ='SVMXC__Zip__c';
        atlList.add(cs3);
        CS_AccountToLocation__C cs4 = new CS_AccountToLocation__C();   
        cs4.Name = 'Country';       
        cs4.SourceObjField__c = 'Country__c';
        cs4.TargetObjField__c ='SVMXC__Country__c';
        atlList.add(cs4);
        CS_AccountToLocation__C cs5 = new CS_AccountToLocation__C();
        cs5.Name = 'StateProvince';         
        cs5.SourceObjField__c = 'StateProvince__c';
        cs5.TargetObjField__c ='StateProvince__c';
        atlList.add(cs5);
        
        insert atlList;
        
        
        
        Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj;  
        Account accountSobj2 = Utils_TestMethods.createAccount();
        insert accountSobj2;  
        Account accountSobj3 = Utils_TestMethods.createAccount();
        insert accountSobj3;
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = accountSobj2.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        DeviceType__c dt = new DeviceType__c();
        dt.Name = 'TestDeviceType';
        dt.SDHDEVICETYPEID__c ='DEVICETYPE';
        insert dt;
        
        Brand__c drand = new Brand__c();
        drand.Name = 'TestBRAND';
        drand.SDHBRANDID__c ='BRAND';
        insert drand;
        
        
        
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
        ip.SVMXC__Company__c = accountSobj3.id;
        ip.Name = 'Test Intalled Product ';
        ip.SVMXC__Status__c= 'new';
        ip.GoldenAssetId__c = 'GoledenAssertId5';
        ip.BrandToCreate__c ='Test Brand';
        insert ip;
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.SVMXC__Company__c = accountSobj3.id;
        ip2.Name = 'Test Intalled Product ';
        ip2.SVMXC__Status__c= 'new';
        ip2.GoldenAssetId__c = 'GoledenAssertId8';
        ip2.BrandToCreate__c ='Test Brand';
        insert ip2;
        
              
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name ='Test Site';
        site.PrimaryLocation__c = true;
        insert site;
        Country__c countrySobj = Utils_TestMethods.createCountry();
        insert countrySobj;
        Product2 product = new Product2();
        product.Name = 'Test Product';
        product.ExtProductId__c = 'TestExtProductId';
        Product2 product2 = new Product2();
        product2.Name = 'Test Product';
        product2.ExtProductId__c = 'SDHCATEGORYID_DEVICETYPE_BRAND';
        insert product;
        insert product2;        
        List<WS_InstalledProduct.WS_UnitInstalledProduct> listUnitIP = new List<WS_InstalledProduct.WS_UnitInstalledProduct>();
        
        WS_InstalledProduct.WS_UnitInstalledProduct uip = getWSInstalledProdcut();
        uip.GoldenAssetId = 'GoldenAssetId';
        uip.Company = accountSobj.id;
        uip.Site = site.id;
        uip.MadeInOfTheAsset = countrySobj.id ;
        
        WS_InstalledProduct.WS_UnitInstalledProduct uip2 = getWSInstalledProdcut(); 
         
        WS_InstalledProduct.WS_UnitInstalledProduct uip3 = getWSInstalledProdcut();
        uip3.GoldenAssetId = 'GoldenAssetId3';
        uip3.Company = accountSobj2.id;
        uip3.IsInstalledProductReadOnly = false;
        //uip3.Site = site.id;
        uip3.MadeInOfTheAsset = countrySobj.id ;          
        uip3.SchneiderCommercialReference = null;
        uip3.SDHBrand = 'BRAND';
        uip3.SDHCategoryId = 'SDHCATEGORYID';       
        uip3.SDHDeviceType = 'DEVICETYPE';  
        WS_InstalledProduct.WS_UnitInstalledProduct uip4 = getWSInstalledProdcut();
        //uip2.Site = site.id;
        WS_InstalledProduct.WS_UnitInstalledProduct uip5 = getWSInstalledProdcut();
        uip5.InstalledProductbFOId = ip.id;
        
        // mandatoryFields Fields missing
        
       WS_InstalledProduct.WS_UnitInstalledProduct uip6 = getWSInstalledProdcut();
       uip6.Company =  null;
       uip6.GoldenAssetId = 'GoldenAssetId6';
       uip6.InstalledProductName =  null;
       
       // InstalledProductReadOnly Testing
        
       WS_InstalledProduct.WS_UnitInstalledProduct uip7 = getWSInstalledProdcut();
       uip7.Company =  null;
       uip7.GoldenAssetId = 'GoldenAssetId7';
       uip7.IsInstalledProductReadOnly =  true;
       
        WS_InstalledProduct.WS_UnitInstalledProduct uip8 = getWSInstalledProdcut();
        uip8.GoldenAssetId = 'GoledenAssertId8';
        uip8.Company = accountSobj.id;
        uip8.Site = site.id;
        uip8.MadeInOfTheAsset = countrySobj.id ;
        
        listUnitIP.add(uip);        
        listUnitIP.add(uip2);
        listUnitIP.add(uip3);
        listUnitIP.add(uip4);
        listUnitIP.add(uip5);
        listUnitIP.add(uip6);
        listUnitIP.add(uip7);
        listUnitIP.add(uip8);
        WS_InstalledProduct.WS_bulkInstalledProduct bip = new WS_InstalledProduct.WS_bulkInstalledProduct();
        bip.InstalledProduct =listUnitIP;
        WS_InstalledProduct.WS_InstalledProductDispatch(bip);
        
        
        
        
        
        
    }
    static WS_InstalledProduct.WS_UnitInstalledProduct getWSInstalledProdcut(){
         WS_InstalledProduct.WS_UnitInstalledProduct uip = new WS_InstalledProduct.WS_UnitInstalledProduct();
        //uip.Company = accountSobj.id;
        //uip.Site = site.id;
       // uip.MadeInOfTheAsset = countrySobj.id ;
        uip.InstalledProductName = 'InstalledProductName';
        uip.BreakingTechnology = 'BreakingTechnology';
        uip.CapacitorPowerUOM = 'CapacitorPowerUOM';
        uip.CustomerCommercialReference = 'CustomerCommercialReference';
        uip.CustomerSerialNumber = 'CustomerSerialNumber';
        //uip.GoldenAssetId = 'GoldenAssetId';
        uip.InstallationMode = 'InstallationMode';
        uip.InstalledProductRevisionNumber = 'InstalledProductRevisionNumber';
        uip.InstalledProductVersion = 'InstalledProductVersion';
        uip.AssetIPAddress = 'AssetIPAddress';
        uip.AssetMACAdress = 'AssetMACAdress';
        uip.ManufacturingLotCode = 'ManufacturingLotCode';
        uip.PowerUOM = 'PowerUOM';
        uip.PrimaryVoltageUOM = 'PrimaryVoltageUOM';
        uip.RatedOperatingVoltageUOM = 'RatedOperatingVoltageUOM';
        uip.RemoteDialingNumber = 'RemoteDialingNumber';
        uip.RemoteMonitoringSystemLink       = 'RemoteMonitoringSystemLink      ';
        uip.TransformerTechnology = 'TransformerTechnology';
        uip.SDHProductName  = 'SDHProductName ';
        uip.ThreePHParameterTable = 'ThreePHParameterTable';
        uip.SDHPublisherMaster = 'SDHPublisherMaster';
        uip.CustomerCriticity = 'CustomerCriticity';
        uip.RatedCurrentUOM       = 'RatedCurrentUOM      ';
        uip.SDHCategoryId = 'SDHCategoryId';
        uip.SDHBrand = 'SDHBrand';        
        uip.DeviceTypeToCreate = 'DeviceTypeToCreate';
        uip.TransformerTechnology = 'TransformerTechnology';
        uip.UniqueSEIdentification = 'UniqueSEIdentification';
        uip.Warranty = 'Warranty';
        uip.LifeCycleStatusOfTheInstalledProduct = 'LifeCycleStatusOfTheInstalledProduct';
        uip.ThreePHParameterTable = 'ThreePHParameterTable';
        uip.DisplayFirmware = 'DisplayFirmware';
        uip.SDHGoldenVersion = 'SDHGoldenVersion';
        uip.SDHPublisherMaster = 'SDHPublisherMaster';
        uip.CustomsCodeOfTheInstalledProduct = 'CustomsCodeOfTheInstalledProduct';
        uip.ManufacturingRank = 'ManufacturingRank';
        uip.CustomerCriticity = 'CustomerCriticity';
        uip.RatedCurrentUOM       = 'RatedCurrentUOM      ';
        uip.SDHCategoryId = 'SDHCategoryId';
        uip.SKU = 'SKU';
        uip.SDHDeviceType = 'SDHDeviceType';
        uip.SDHBrand = 'SDHBrand';      
        uip.DeviceTypeToCreate = 'DeviceTypeToCreate';
        uip.SchneiderCommercialReference = 'TestExtProductId';
        uip.DeviceTypeToCreate = 'DeviceTypeToCreate';
        
        
        return uip;
        
    }
}