public with sharing class VFC90_SearchSolutionCenter  extends VFC_ControllerBase{
    
//*********************************************************************************
// Controller Name  : VFC90_SearchSolutionCenter
// Purpose          : Controller class for Search Solution Center
// Created by       : Hari Krishna  Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For Sales April 2012 Release
///********************************************************************************/
    
/*============================================================================
                V.A.R.I.A.B.L.E.S
=============================================================================*/ 
    
    public Utils_DataSource dataSource;    
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public String interfaceType{get;set;}
    public List<String> keywords;
    public List<String> filters;
    public Boolean DisplayResults{get;set;}
    public Utils_DataSource.Result searchResult;
    Public Utils_PicklistManager plManager;
    public String level1{set; get;}
    public String level2{get; set;}
    
    public String level3{get; set;}
    
    Public List<String> Labels{get;set;}
    public String level1Label{set; get{return Labels[0];}}
    public String level2Label{set; get{return Labels[1];}}
    
    public String level3Label{set; get{return Labels[2];}}
    
    public List<SelectOption> items1{set; get;}
    public List<SelectOption> items2{set; get;}
    
    public List<SelectOption> items3{set; get;}
    
    public List<SelectOption> columns{get;set;}
    public string searchText{get;set;}  
    public string countrysearchText{get;set;}      
    List<DataTemplate__c> oppCompetitors = New List<DataTemplate__c>();
    public Boolean flag;
    public Boolean displayError{get;set;}
    String recordTypeId='';
    String oppName='';
    String oppid='';
    String rUrl='';
    
     // Search Solution Center Results Component
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
   
/*============================================================================
                             C.O.N.S.T.R.U.C.T.O.R
=============================================================================*/    
    public VFC90_SearchSolutionCenter(ApexPages.StandardController controller) {
       
        recordTypeId=ApexPages.currentPage().getParameters().get('RecordType');
        oppName=ApexPages.currentPage().getParameters().get(Label.CL10217);
        oppid=ApexPages.currentPage().getParameters().get(Label.CL10219);
        rUrl = ApexPages.currentPage().getParameters().get('retUrl');
        System.debug('%%%%%%%%%%%%%%%%%%%%%%%%000....rUrl '+rUrl);
       // System.debug('%%%%%%%%%%%%%%%%%%%%%%%%1111.... '+ApexPages.currentPage().getURL());       
        System.debug('%%%%%%%%%%%%%%%%%%%%%%%%'+ApexPages.currentPage().getParameters().get('scName'));
         searchText = ApexPages.currentPage().getParameters().get('scName');
         System.debug('%%%%%%%%%%%%%%%%%%%%%%%%'+searchText);
        
        keywords = new List<String>();
        filters = new List<String>();
        columns = new List<SelectOption>();
        Labels = new List<String>();
        interfaceType = 'SOCC';
        flag = false;
        displayError = false;
        dataSource = Utils_Factory.getDataSource(interfaceType);
        columns = new List<SelectOption>();
        columns = dataSource.getColumns();
        plManager = Utils_Factory.getPickListManager(interfaceType);
        
        if( plManager != null)
        {
            Labels = plManager.getPickListLabels();
            items1 = plManager.getPicklistValues(1, null);
            items2 = plManager.getPicklistValues(2, null);
            
            items3 = plManager.getPicklistValues(3, null);
            
        }
        
        fetchedRecords = new List<DataTemplate__c>();

    }
    
    //*********************************************************************************
    // Method Name      : clear
    // Purpose          : To clear the Search input and Filters values 
    // Created by       : Hari Krishna  Global Delivery Team
    // Date created     : 
    // Modified by      :
    // Date Modified    :
    // Remarks          : For Sales April 2012 Release
    ///********************************************************************************/   
     
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC90_SearchSolutionCenter******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchText = null;
        keywords = new List<String>();
        filters = new List<String>();        
        DisplayResults = false;
        level1 = null;
        level2 = null;
        
        level3 = null;
        
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC90_SearchSolutionCenter******');     
        return null;
        
    }
    
    //*********************************************************************************
    // Method Name      : search
    // Purpose          : To Search the Solution Center Country 
    // Created by       : Hari Krishna  Global Delivery Team
    // Date created     : 
    // Modified by      :
    // Date Modified    :
    // Remarks          : For Sales April 2012 Release
    ///********************************************************************************/ 
    public PageReference  search() 
    {  
    
        System.debug('Search Started ******');
         Utils_SDF_Methodology.startTimer();
         
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();
        
        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchText!=null)
            keywords.add(searchText.trim());
        if(level1!=null)
            filters.add(level1);
        if(level2!=null)
            filters.add(level2); 
            
        if(level3!=null)
            filters.add(level3); 
            
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords ); 
        
        if(keywords.size()!=0 || filters.size()!=0)
        {
            if(!filters[0].equalsIgnoreCase(Label.CL00355) || !filters[1].equalsIgnoreCase(Label.CL00355) || !filters[2].equalsIgnoreCase(Label.CL00355) ||keywords[0].length()>0)
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the Datasource call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                    System.debug(' Result'+searchResult);
                    DisplayResults = true;
                    
                    if(searchResult.NumberOfRecord > 100)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling Competitor Search '+ exc.getMessage() );
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,exc.getMessage()));
                }                
            }
            else if(filters[0].equalsIgnoreCase(Label.CL00355) && filters[1].equalsIgnoreCase(Label.CL00355) && filters[2].equalsIgnoreCase(Label.CL00355) && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL10218));

            }             
        } 
        
           
         return null;
    } 
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly 
     * 
     */
    //*********************************************************************************
    // Method Name      : PerformAction
    // Purpose          : on click of "Select"  this will navigate to VFPXX_NewAndEditSolutionCente
    // Created by       : Hari Krishna  Global Delivery Team
    // Date created     : 
    // Modified by      :
    // Date Modified    :
    // Remarks          : For Sales April 2012 Release
    ///********************************************************************************/ 
   
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** PerformAction Line Begins******');
        
        VFC90_SearchSolutionCenter thisController = (VFC90_SearchSolutionCenter)controllerBase;
       DataTemplate__c dt= (DataTemplate__c)obj;
       System.debug('*************'+dt.field4__c);
       System.debug('*************'+ApexPages.currentPage().getParameters().get('id') );
       
       if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id')!=''){
            OPP_SupportRequest__c sr=[select id, SolutionCenter__c from OPP_SupportRequest__c where id=:ApexPages.currentPage().getParameters().get('id')];
            sr.SolutionCenter__c = dt.field4__c;
            update sr;
            PageReference pr = new PageReference('/'+sr.id);
             return pr;
       }
        else{
        String url='/apex/'+Label.CL10207;
        
        
        PageReference newSolutionCenterPage = new PageReference(url);
        newSolutionCenterPage.getParameters().put('oppid',oppid);
        newSolutionCenterPage.getParameters().put( 'RecordType',recordTypeId);        
        newSolutionCenterPage.getParameters().put('scid', dt.field4__c);
        newSolutionCenterPage.getParameters().put('scname', dt.field1__c);
        newSolutionCenterPage.getParameters().put('sfdc.override', '1');
        newSolutionCenterPage.getParameters().put('ent',ApexPages.currentPage().getParameters().get('ent'));
        newSolutionCenterPage.getParameters().put('save_new_url',ApexPages.currentPage().getParameters().get('save_new_url'));
        newSolutionCenterPage.getParameters().put('save_new','1'); 
        return newSolutionCenterPage;
        }
    } 
    //*********************************************************************************
    // Method Name      : cancel
    // Purpose          : This method returns to the Opportunity Page.
    // Created by       : Hari Krishna  Global Delivery Team
    // Date created     : 
    // Modified by      :
    // Date Modified    :
    // Remarks          : For Sales April 2012 Release
    ///********************************************************************************/  
    public pagereference cancel()
    {
      
        PageReference pgr;
        if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id').length()>0)
        pgr = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));  
        else{pgr = new PageReference('/'+rUrl);} 
       return pgr;
    }        

}