@isTest(SeeAllData=true)
public with sharing class Ap_Location_TEST {
  
   static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Profile pr = [select id from profile where id = '00eA0000000aA5yIAE' ];     
        
       Ap_Location loc = new Ap_Location();
       
      
     
        string SDH_Profile =Label.CLNOV13SRV03;
        string STANDARD_LOCATION = Label.CLNOV13SRV01;
        Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj; 
        set<Id> accid = New set<Id>();
        accid.add(accountSobj.Id);
        Account accountSobj2 = Utils_TestMethods.createAccount();
        insert accountSobj2;  
        
        User user = new User(alias = 'alias', email='alias' + '@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pr.id, BypassWF__c = true,
            timezonesidkey='Europe/London', username='xyz-123' +'r-test'+ '@bridge-fo.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='FR',
            BypassVR__c=true
            );
        insert user;
        

        System.runAs(user){
        
        list<SVMXC__Site__c> loclist = new list<SVMXC__Site__c>();
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = accountSobj2.id;
        site1.PrimaryLocation__c = true;
        site1.GoldenLocationId__c = 'TestGoldenLocationId';
        site1.RecordTypeId = STANDARD_LOCATION;
        site1.TECH_CreateFromWS_IP__c = false;
        site1.SVMXC__Parent__c=null;
        insert site1;
       site1.TECH_CreateFromWS_IP__c=false;
       site1.SVMXC__Street__c  = 'Test Street123';
       update site1;
       
        loclist.add(site1);
        
        
       /* try{
          SVMXC__Site__c site2 = new SVMXC__Site__c();
          site2.Name = 'Test Location';
          site2.SVMXC__Street__c  = 'Test Street';
          site2.SVMXC__Account__c = accountSobj.id;
          site2.PrimaryLocation__c = true;
          site2.GoldenLocationId__c = 'TestGoldenLocationId';
          insert site2;
            
        }
        catch(Exception e){
          
        }*/
    
        
        Ap_Location.createLocation(accid);
           
        //loc.primaryLocationCreationForWS(loclist);
       }
    }

}