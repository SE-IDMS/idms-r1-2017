@isTest
public class AP_RR_FieldUpdate_Test{

  static TestMethod void testupdateRRStatus(){
        Test.startTest(); 

        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
           Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        User newUser = Utils_TestMethods.createStandardUser('TestUser856975');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        User newUser2 = Utils_TestMethods.createStandardUser('TestUser352634');         
        Database.SaveResult UserInsertResult2 = Database.insert(newUser2, false);        
         
        List<TEX__C> Listtech=New List<TEX__C>();
        TEX__c temptech=AP_TechnicalExpertAssessment_Test.createTEX(objCaseForAction.id,newUser.id);
        temptech.Material_Status__c='Delivered to Return Center';
        temptech.ShippedFromCustomer__c = null;
        temptech.ExpertAssessmentStarted__c = system.today();
        Listtech.add(temptech);  
         TEX__c temptech1=AP_TechnicalExpertAssessment_Test.createTEX(objCaseForAction.id,newUser2.id);
        temptech1.ShippedFromCustomer__c = null;
        temptech1.ExpertAssessmentStarted__c = system.today();
        Listtech.add(temptech1);      
        Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');       
        insert Listtech; 
        Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        
        RMA__c temRMA=AP_TechnicalExpertAssessment_Test.createReturnRequest(objCaseForAction.Id,objAccount.id, country.Id,temptech.id);
        temRMA.ShippedFromCustomer__c = system.today();
        temRMA.Status__c='Rejected';
         Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        insert temRMA;
         Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        RMA__c temRMA2=AP_TechnicalExpertAssessment_Test.createReturnRequest(objCaseForAction.Id,objAccount.id, country.Id,temptech.id);
        temRMA2.ShippedFromCustomer__c = system.today();
         Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
         insert temRMA2;
          Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        temRMA.ShippedFromCustomer__c = temRMA.ShippedFromCustomer__c.addDays(4);
        Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        update temRMA;
        Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        
         list<RMA_Product__c> listRtm=new list< RMA_Product__c>();
         OPP_Product__c prod = new OPP_Product__c(Name='Test Product Name', BusinessUnit__c = 'Business Unit',ProductLine__c='Product Line',ProductFamily__c='Product Family',Family__c='Family', TECH_PM0CodeInGMR__c='200201140', HierarchyType__c='Hierarchy type');
         insert prod;
        RMA_Product__c tempRTM=AP_TechnicalExpertAssessment_Test.createReturnItem(temRMA.id); 
        RMA_Product__c tempRTM1=AP_TechnicalExpertAssessment_Test.createReturnItem(temRMA.id);
        tempRTM1.ApprovalStatus__c='Approved';
        tempRTM1.ShippedFromCustomer__c = system.today().addDays(6);
        RMA_Product__c tempRTM2=AP_TechnicalExpertAssessment_Test.createReturnItem(temRMA.id);
        tempRTM2.ApprovalStatus__c='Rejected';  
        tempRTM2.ShippedFromCustomer__c = system.today().addDays(5);        
        listRtm.add(tempRTM);
        listRtm.add(tempRTM1);
        listRtm.add(tempRTM2);
         Utils_SDF_Methodology.removeFromRunOnce('AP_RI_RMAMailMerge');
         Utils_SDF_Methodology.removeFromRunOnce('AP_RR_FieldUpdate');
         Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
          Utils_SDF_Methodology.removeFromRunOnce('AP35');
         
        insert listRtm;
Utils_SDF_Methodology.removeFromRunOnce('AP_RI_RMAMailMerge');
Utils_SDF_Methodology.removeFromRunOnce('AP_RR_FieldUpdate');
 Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
Utils_SDF_Methodology.removeFromRunOnce('AP35');

        listRtm[0].ApprovalStatus__c='Approved1';
        listRtm[0].SentBacktoCustomer__c=system.today();
        listRtm[0].ShippedFromCustomer__c=system.today();
         Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
        update listRtm[0];
      Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');

       test.stoptest();
       Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
       Delete temRMA2;
        Utils_SDF_Methodology.removeFromRunOnce('AP_RI_FieldUpdate');
       delete tempRTM2;
      set<id> setRma=new set<id>();
    
      setRma.add(temRMA.id);
      AP_RR_FieldUpdate.updateRRStatus(setRma);
      List<RMA__c> ListRma=new List<RMA__c>();
      ListRma.add(temRMA);
     map<string,RMA_Product__c> mapGmrcodeNRi = new map<string,RMA_Product__c>();
     mapGmrcodeNRi.put(prod.TECH_PM0CodeInGMR__c,tempRTM);
      
      AP_RR_FieldUpdate.updatingMaterialStatusOnTEX(ListRma);
      AP_RR_FieldUpdate.updateRequestTypeOnRRDelete(listRtm);
      AP_RR_FieldUpdate.updateShippedFromCustomer(listRtm);
      AP_RR_FieldUpdate.updateRequestTypeOnRR(listRtm);
      AP_RR_FieldUpdate.updateOppProductFielsOnRI(mapGmrcodeNRi);
      
    } 
}