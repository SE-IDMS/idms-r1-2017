/*Copyright (c) 2010, Sovane Bin, Professional Services, Salesforce.com Inc.
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1.    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
2.    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3.    Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.*/
public with sharing class VFC01_OpptyCockpit{
    private ID myOpptyId;
    public String mData{get;set;}
    public Boolean fCompliancy{get;set;}
    public List<dobbleWrapper>wOutput{get;set;}{wOutput=new List<dobbleWrapper>();}
    public VFC01_OpptyCockpit(ApexPages.StandardController controller){
        myOpptyId=controller.getId();
        cockpit_hierarchy();
        cockpit_news();
    }
    public Map<Id,String> parentOpptyIdSet {get;set;}
    public Map<Id,String> projectIdSet {get;set;}
    public Id projectId{get;set;}{ProjectId=null;}
    public string updopptyId {get;set;}
    public boolean updopptyaction {get;set;}
    public string str_updopptyaction {get{return updopptyaction?'Yes':'No';}}
    public boolean SB_isok {get;set;}{SB_isok=true;}
    public void updateOpportunity(){
        if(updopptyId!=null)try{SB_isok=true;update new Opportunity(id=updopptyId,IncludedInForecast__c=str_updopptyaction);}catch(Exception e){SB_isok=false;ApexPages.addMessages(e);}
        cockpit_hierarchy();
    }
    private void cockpit_hierarchy(){
        Boolean fTopIsOn=false;
        Integer fCount=0;
        parentOpptyIdSet=new Map<Id,String>();
        projectIdSet=new Map<Id,String>();
        mData='data.addRows([';
        parentOpptyIdSet.put(myOpptyId,null);
        List<Integer>IntegerListToNullify=new List<Integer>();
        List<String>ListStringTemp1=new List<String>();
        List<String>ListStringTemp2=new List<String>();
        Integer secureBarrier=0;
        Integer i,iTemp;
        Integer IntegerRowLine=0;
        String lastSentenceJS='';
        List<Opportunity> myOpptyList;
        try{
            myOpptyList=[SELECT Id,ParentOpportunity__c,ParentOpportunity__r.ParentOpportunity__c,Project__c,ParentOpportunity__r.Project__c,(SELECT Id,Project__c FROM Opportunities__r) FROM Opportunity WHERE Id=:myOpptyId];
            if(myOpptyList.size()==1){
                if(myOpptyList[0].Id==myOpptyList[0].ParentOpportunity__c||myOpptyList[0].Id==myOpptyList[0].ParentOpportunity__r.ParentOpportunity__c)throw new BException(System.Label.CL00120);
                Opportunity myOpportunity=myOpptyList[0];
                parentOpptyIdSet.put(myOpportunity.id,null);
                if(myOpportunity.ParentOpportunity__c!=null){
                    parentOpptyIdSet.put(myOpportunity.ParentOpportunity__c,null);
                    if(myOpportunity.ParentOpportunity__r.ParentOpportunity__c!=null)parentOpptyIdSet.put(myOpportunity.ParentOpportunity__r.ParentOpportunity__c,null);
                }
            }
          //Added project fields for the monalisa project oct 2015
          for(Opportunity o:[SELECT Id,Project__c FROM Opportunity WHERE Id IN :parentOpptyIdSet.keySet() ]){
                if(o.Project__c!=null)
                projectIdSet.put(o.Project__c,null);
            }
            for(Opportunity o:[SELECT Id FROM Opportunity WHERE Project__c IN :projectIdSet.keyset() ])parentOpptyIdSet.put(o.Id,null);
            for(Opportunity o:[SELECT Id FROM Opportunity WHERE ParentOpportunity__c IN :parentOpptyIdSet.keySet() ])parentOpptyIdSet.put(o.Id,null);
            for(Opportunity o:[SELECT Id FROM Opportunity WHERE ParentOpportunity__r.ParentOpportunity__c  IN :parentOpptyIdSet.keySet() ])parentOpptyIdSet.put(o.Id,null);
            if(myOpptyList[0].ParentOpportunity__r!=null)for(Opportunity o:[SELECT Id FROM Opportunity WHERE ParentOpportunity__c IN :parentOpptyIdSet.keySet() ])parentOpptyIdSet.put(o.Id,null);
            List<Opportunity>OpportunityListForLoop=[SELECT CustomerSelected__c,Account.Name,AccountId,TobeDeleted__c,Owner.Name,owner.SmallPhotoUrl,convertCurrency(Amount),toLabel(LeadingBusiness__c),StageName,IncludedInForecast__c,closeDate, OpportunityCategory__c,OwnerId,Name,ParentOpportunity__c,ParentOpportunity__r.IncludedInForecast__c,Project__c,Project__r.Name,Project__r.ProjectEndUserInvestor__c,Project__r.ProjectEndUserInvestor__r.Name,Project__r.ProjectStatus__c,Project__r.EstimateProjectCloseDate__c,Project__r.OwnerId,Project__r.Owner.Name,convertCurrency(Project__r.TotalProjectAmountForecastedWon__c),ParentOpportunity__r.ParentOpportunity__c,ParentOpportunity__r.ParentOpportunity__r.IncludedInForecast__c FROM Opportunity WHERE Id IN :parentOpptyIdSet.keyset() ];
            ListStringTemp1.add(null);
            secureBarrier=0;
            fCompliancy=true;
            for(Opportunity o:OpportunityListForLoop){
                //if(o.ParentOpportunity__c==null&&o.IncludedInForecast__c=='Yes')fTopIsOn=true;
                //if(o.IncludedInForecast__c=='Yes')fCount++;
                //if((o.IncludedInForecast__c=='Yes'&&o.ParentOpportunity__c!=null&&o.ParentOpportunity__r.IncludedInForecast__c=='Yes')||(fcount>1&&fTopIsOn)){fCompliancy=false;break;}
                if((o.IncludedInForecast__c=='Yes'&&o.ParentOpportunity__c!=null&&o.ParentOpportunity__r.IncludedInForecast__c=='Yes')|| (o.IncludedInForecast__c=='Yes'&&o.ParentOpportunity__r.ParentOpportunity__c!=null&&o.ParentOpportunity__r.ParentOpportunity__r.IncludedInForecast__c=='Yes') ){fCompliancy=false;break;}
                if(o.Project__c!=null)projectId=o.Project__c;
            }
            while(secureBarrier<5000){
                i=0;
                for(Opportunity o:OpportunityListForLoop){
                    for(String s:ListStringTemp1){
                    //Added Project for the Monalisa Project oct 2015
                        if(o.ParentOpportunity__c==s && s==null && o.Project__c!=null){
                            User u=[Select smallphotoURL from user where Id=:o.Project__r.OwnerId];                         
                            mData+='[{v:\''+o.Project__c+'\',f:\'<div class="HOppty"><a title="Master Project" href='+Site.getPathPrefix()+'/'+o.Project__c+'>'+esc(o.Project__r.Name)+'</a></div><div class="HAccount"><a title="'+System.Label.CL00122+'" href='+Site.getPathPrefix()+'/'+o.Project__r.ProjectEndUserInvestor__c+'>'+esc(o.Project__r.ProjectEndUserInvestor__r.Name)+'</a></div><div class="HStageName">'+esc(o.Project__r.ProjectStatus__c)+'</div><div class="HAmount">'+(o.Project__r.TotalProjectAmountForecastedWon__c!=null?((o.Project__r.TotalProjectAmountForecastedWon__c/1000).intValue() +'k '+UserInfo.getDefaultCurrency()):' ')+'</div><div class="HCloseDate">'+o.Project__r.EstimateProjectCloseDate__c.format()+'</div><div class="Himgs"><a href='+Site.getPathPrefix()+'/'+o.Project__r.OwnerId+'" title="'+esc(o.Project__r.Owner.Name)+'><img src="'+u.SmallPhotoUrl+'" class="HPic"/></a><div>'+esc(o.Project__r.Owner.Name)+'</div><div class="CockpitFollow" id="follow1'+o.Project__c+'"></div>\'},\''+o.Project__c+'\',\''+esc(o.Project__r.Name)+'\'],';
                            lastSentenceJS+='data.setRowProperty('+IntegerRowLine+',\'style\',\'border:5px solid blue;\');';
                            IntegerRowLine++;     
                        }
                    
                        if(o.ParentOpportunity__c==s){
                        Boolean bIncludedInForecast = o.IncludedInForecast__c == 'Yes'? true : false;
                        mData+='[{v:\''+o.Id+'\',f:\'<div class="HOppty"><a title="/'+System.Label.CL00121+'" href='+Site.getPathPrefix()+'/'+o.Id+'>'+esc(o.Name)+'</a></div><div class="HAccount"><a title="/'+System.Label.CL00122+'" href='+Site.getPathPrefix()+'/'+o.AccountId+'>'+esc(o.Account.Name)+'</a></div><div class="HStageName">'+esc(o.StageName)+'</div><div class="HAmount">'+(o.Amount!=null?((o.Amount/1000).intValue() +'k '+UserInfo.getDefaultCurrency()):' ')+'</div><div class="HCloseDate">'+o.closeDate.format()+'</div><div id="HLeadingBusiness">'+esc(o.LeadingBusiness__c)+'</div><div class="Himgs"><a href='+Site.getPathPrefix()+'/'+o.ownerid+'" title="'+esc(o.owner.name)+'><img src="'+o.owner.SmallPhotoUrl+'" class="HPic"/></a><a class="HActions" title="'+(bIncludedInForecast?esc(System.Label.CL00123):esc(System.Label.CL00124))+'"><img id="pic'+o.id+'" onclick="updateOpportunityjs('+!bIncludedInForecast+');" src="'+esc(System.Label.CL00125)+'" class="'+(bIncludedInForecast?'Hexclude':'Hinclude')+'"/></a></div></div><div>'+esc(o.owner.name)+'</div><div class="CockpitFollow" id="follow1'+o.id+'"></div>\'},\''+(o.ParentOpportunity__c!=null?o.ParentOpportunity__c:(o.Project__c!=null?o.Project__c:o.Id))+'\',\''+esc(o.Name)+'\'],';
                        lastSentenceJS+=bIncludedInForecast?'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:5px solid green;\');':'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:3px solid #888888;\');';
                        if(o.TobeDeleted__c)lastSentenceJS+='data.setRowProperty('+IntegerRowLine+',\'style\',\'border: 3px solid red;\');';
                        ListStringTemp2.add(o.Id);
                        IntegerListToNullify.add(i);
                        IntegerRowLine++;
                    }
                  }
                    i++;
                }
                for(Integer j=IntegerListToNullify.size()-1;j>=0;j--)OpportunityListForLoop.remove(IntegerListToNullify.get(j));
                if(OpportunityListForLoop.isEmpty())break;
                ListStringTemp1=ListStringTemp2.clone();
                ListStringTemp2.clear();
                IntegerListToNullify.clear();
                secureBarrier++;
            }
            mData=mData.substring(0,mData.length()-1)+']);'+lastSentenceJS;
        }
        catch(Exception exc){ApexPages.addMessages(exc);}
    }
    private void cockpit_news(){
        Map<String,String>SBfield2Label=new Map<String,String>();
        Map<String, Schema.SObjectField>SBFields=Schema.SObjectType.Opportunity.fields.getMap();
        Map<String, Schema.SObjectField>SBFields1=Schema.SObjectType.OPP_Project__c.fields.getMap();
        Integer SOQLLimit=50;
        List<wrapperFeed> tempList;
        List<wrapperFeed>wNewsFields=new List<wrapperFeed>();
        List<wrapperFeed> wNewsChats=new List<wrapperFeed>();
        String temp;
        String xChanged=System.Label.CL00126;
        String xFrom=System.Label.CL00127;
        String xInto=System.Label.CL00128;
        String xPostFile=System.Label.CL00129;
        Map<Id,String>myUsers=new Map<Id,String>();
        //Added Master Project Feed in the cockpit News for the Monalisa Project - Oct 2015
        if(projectId!=null){
            for(OPP_Project__Feed pfeed:[SELECT ParentId,parent.name,Id,CreatedById,createdby.name,CreatedDate,(SELECT FieldName,OldValue,NewValue FROM FeedTrackedChanges),(SELECT CommentBody,CreatedBy.Name,CreatedById,CreatedDate FROM FeedComments) FROM OPP_Project__Feed WHERE ParentId=:projectId AND Type='TrackedChange' ORDER BY createddate DESC LIMIT:SOQLLimit]){
            myUsers.put(pfeed.CreatedById,null);
            wrapperFeed wf=new wrapperFeed();
            temp=pfeed.FeedTrackedChanges[0].fieldName.replace('OPP_Project__c.','').replace('Owner','OwnerId');
            if(SBfield2Label.get(pfeed.FeedTrackedChanges[0].fieldName)==null&&SBFields1.get(temp)!=null)SBfield2Label.put(pfeed.FeedTrackedChanges[0].fieldName,SBFields1.get(temp).getDescribe().getLabel());
            if(pfeed.FeedTrackedChanges[0].fieldName!='Created')wf.myText=SBfield2Label.get(pfeed.FeedTrackedChanges[0].fieldName)+' '+xChanged+' '+xFrom+' '+(pfeed.FeedTrackedChanges[0].OldValue==null?System.Label.CL00130:pfeed.FeedTrackedChanges[0].OldValue+'')+' '+xInto+' '+(pfeed.FeedTrackedChanges[0].NewValue==null?System.Label.CL00130:pfeed.FeedTrackedChanges[0].NewValue+'');
            else wf.myText='Created this Master Project';
            wf.myWhere_Name=pfeed.parent.name;
            wf.myWhere_Id=pfeed.ParentId;
            wf.myWho_Id=pfeed.CreatedById;
            wf.myWho_Name=pfeed.createdby.name;
            wf.myWhen=pfeed.CreatedDate.format();
            wf.myComments=new List<wrapperFeed>();
            for(FeedComment fc:pfeed.FeedComments){
                myUsers.put(fc.CreatedById,null);
                wrapperFeed wfc=new wrapperFeed();
                wfc.myText=fc.CommentBody;
                wfc.myWho_Id=fc.createdbyid;
                wfc.myWho_Name=fc.CreatedBy.Name;
                wfc.myWhen=fc.CreatedDate.format();
                wf.myComments.add(wfc);
            }
            wNewsFields.add(wf);
          }
          for(OPP_Project__Feed prfeed:[SELECT ParentId,Parent.Name,CreatedById,CreatedBy.name,CreatedDate,Type,(SELECT CommentBody,CreatedById,CreatedBy.name,CreatedDate FROM FeedComments),FeedPost.Body,FeedPost.LinkUrl,FeedPost.ContentFileName FROM OPP_Project__Feed WHERE ParentId=:projectId AND Type!='TrackedChange' ORDER BY createddate DESC LIMIT:SOQLLimit]){
            myUsers.put(prfeed.CreatedById,null);
            wrapperFeed wf=new wrapperFeed();
            wf.myText=prfeed.type=='LinkPost'?prfeed.FeedPost.LinkUrl:prfeed.type=='TextPost'?prfeed.FeedPost.Body:xPostFile+' '+prfeed.FeedPost.ContentFileName;
            wf.myWhere_Name=prfeed.parent.name;
            wf.myWhere_Id=prfeed.ParentId;
            wf.myWho_Id=prfeed.CreatedById;
            wf.myWho_Name=prfeed.createdby.name;
            wf.myWhen=prfeed.CreatedDate.format();
            wf.myComments=new List<wrapperFeed>();
            for(FeedComment fc:prfeed.FeedComments){
                myUsers.put(fc.CreatedById,'');
                wrapperFeed wfc=new wrapperFeed();
                wfc.myText=fc.CommentBody;
                wfc.myWhen=fc.CreatedDate.format();
                wfc.myWho_Id=fc.createdbyid;
                wfc.myWho_Name=fc.CreatedBy.Name;
                wf.myComments.add(wfc);
            }
            wNewsChats.add(wf);
           }
        }
        for(OpportunityFeed ofeed:[SELECT ParentId,parent.name,Id,CreatedById,createdby.name,CreatedDate,(SELECT FieldName,OldValue,NewValue FROM FeedTrackedChanges),(SELECT CommentBody,CreatedBy.Name,CreatedById,CreatedDate FROM FeedComments) FROM OpportunityFeed WHERE ParentId IN:parentOpptyIdSet.keySet() AND Type='TrackedChange' ORDER BY createddate DESC LIMIT:SOQLLimit]){
            myUsers.put(ofeed.CreatedById,null);
            wrapperFeed wf=new wrapperFeed();
            temp=ofeed.FeedTrackedChanges[0].fieldName.replace('Opportunity.','').replace('Owner','OwnerId');
            if(SBfield2Label.get(ofeed.FeedTrackedChanges[0].fieldName)==null&&SBFields.get(temp)!=null)SBfield2Label.put(ofeed.FeedTrackedChanges[0].fieldName,SBFields.get(temp).getDescribe().getLabel());
            if(ofeed.FeedTrackedChanges[0].fieldName!='Created')wf.myText=SBfield2Label.get(ofeed.FeedTrackedChanges[0].fieldName)+' '+xChanged+' '+xFrom+' '+(ofeed.FeedTrackedChanges[0].OldValue==null?System.Label.CL00130:ofeed.FeedTrackedChanges[0].OldValue+'')+' '+xInto+' '+(ofeed.FeedTrackedChanges[0].NewValue==null?System.Label.CL00130:ofeed.FeedTrackedChanges[0].NewValue+'');
            else wf.myText='Created this opportunity';
            wf.myWhere_Name=ofeed.parent.name;
            wf.myWhere_Id=ofeed.ParentId;
            wf.myWho_Id=ofeed.CreatedById;
            wf.myWho_Name=ofeed.createdby.name;
            wf.myWhen=ofeed.CreatedDate.format();
            wf.myComments=new List<wrapperFeed>();
            for(FeedComment fc:ofeed.FeedComments){
                myUsers.put(fc.CreatedById,null);
                wrapperFeed wfc=new wrapperFeed();
                wfc.myText=fc.CommentBody;
                wfc.myWho_Id=fc.createdbyid;
                wfc.myWho_Name=fc.CreatedBy.Name;
                wfc.myWhen=fc.CreatedDate.format();
                wf.myComments.add(wfc);
            }
            wNewsFields.add(wf);
        }
        tempList=new List<wrapperFeed>();
        for(OpportunityFeed ofeed:[SELECT ParentId,Parent.Name,CreatedById,CreatedBy.name,CreatedDate,Type,(SELECT CommentBody,CreatedById,CreatedBy.name,CreatedDate FROM FeedComments),FeedPost.Body,FeedPost.LinkUrl,FeedPost.ContentFileName FROM OpportunityFeed WHERE ParentId IN:parentOpptyIdSet.keySet() AND Type!='TrackedChange' ORDER BY createddate DESC LIMIT:SOQLLimit]){
            myUsers.put(ofeed.CreatedById,null);
            wrapperFeed wf=new wrapperFeed();
            wf.myText=ofeed.type=='LinkPost'?ofeed.FeedPost.LinkUrl:ofeed.type=='TextPost'?ofeed.FeedPost.Body:xPostFile+' '+ofeed.FeedPost.ContentFileName;
            wf.myWhere_Name=ofeed.parent.name;
            wf.myWhere_Id=ofeed.ParentId;
            wf.myWho_Id=ofeed.CreatedById;
            wf.myWho_Name=ofeed.createdby.name;
            wf.myWhen=ofeed.CreatedDate.format();
            wf.myComments=new List<wrapperFeed>();
            for(FeedComment fc:ofeed.FeedComments){
                myUsers.put(fc.CreatedById,'');
                wrapperFeed wfc=new wrapperFeed();
                wfc.myText=fc.CommentBody;
                wfc.myWhen=fc.CreatedDate.format();
                wfc.myWho_Id=fc.createdbyid;
                wfc.myWho_Name=fc.CreatedBy.Name;
                wf.myComments.add(wfc);
            }
            wNewsChats.add(wf);
        }
        for(User u:[SELECT SmallPhotoUrl FROM User WHERE Id IN:myUsers.keySet()])myUsers.put(u.id,u.SmallPhotoUrl);
        for(wrapperFeed wf:wNewsFields){
            wf.myPic=myUsers.get(wf.myWho_Id);
            for(wrapperFeed wfc:wf.myComments)wfc.myPic=myUsers.get(wfc.myWho_Id);
        }
        for(wrapperFeed wf:wNewsChats){
            wf.myPic=myUsers.get(wf.myWho_Id);
            for(wrapperFeed wfc:wf.myComments)wfc.myPic=myUsers.get(wfc.myWho_Id);
        }
        wOutput.add(new dobbleWrapper(wNewsFields,'CockpitNews_Fields'));
        wOutput.add(new dobbleWrapper(wNewsChats,'CockpitNews_Chats'));
    }
    private string esc(string s){return s!=null?String.escapeSingleQuotes(s):null;}
    public class wrapperFeed{
        public String myPic{get;set;}
        public String myText{get; set;}
        public String myWhere_Name{get; set;}
        public String myWhere_Id{get; set;}
        public String myWhen{get;set;}
        public String myWho_Id{get;set;}
        public String myWho_Name{get;set;}
        public List<wrapperFeed>myComments {get;set;}
    }
    public class dobbleWrapper{
        public dobbleWrapper(List<wrapperFeed> w,String c){myWrapper=w;myClass=c;}
        public List<wrapperFeed> myWrapper{get; set;}
        public String myClass{get; set;}
    }
    public class BException extends Exception {}    
}