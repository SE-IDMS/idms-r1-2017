/*******************
Offer Page Layout Chart
Hanamanth
********************/

Public class VFC_COLA_bFO_OfferlifeCycleReportChart {

Public   Offer_Lifecycle__c Offer;
public List<reportFilter> columnFilters = new List<reportFilter>();
public VFC_COLA_bFO_OfferlifeCycleReportChart (ApexPages.StandardController controller)
    {
      //offercycleid = ApexPages.currentPage().getParameters().get('CF00NZ0000001Nwhm_lkid');
       this.Offer = (Offer_Lifecycle__c)controller.getRecord();
       getAddedFilters();                 
    }
    
    
    public String getChartReportFilter() {
        return JSON.serialize(getAddedFilters());
    }

    private List<reportFilter> getAddedFilters() {
        columnFilters = new List<reportFilter>();
        columnFilters.add(new reportFilter('Offer_Lifecycle__c.Id','equals', String.valueof(Offer.Id)));
      
        return columnFilters;
    }

    
    public class reportFilter {
     
        public String column { get; set; }
        public String operator { get; set; }
        public String value { get; set; }
        
        public reportFilter(String cl, String optr, String vls) {
            column = cl;
            operator = optr;
            value = vls;
        }


    }

}