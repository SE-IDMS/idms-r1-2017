global class Batch_ContactMerge implements Database.Batchable<AggregateResult>, Database.stateful, Schedulable{

    global DateTime startTime;
    global DateTime endTime;
    global List<PRM_Technical_Merge_Record__c> listRecords = new List<PRM_Technical_Merge_Record__c>();
    
    global Iterable<AggregateResult> start(Database.BatchableContext BC) {
        return new AggregateResultIterable();
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        startTime = datetime.now();
        Map<string,integer> elligibleContMap = new Map<string,integer>();
        //Process all AggregateResult and store them in a Map with the count of each group
        for (Sobject so : scope)  {
            AggregateResult ar = (AggregateResult)so;
            elligibleContMap.put(string.valueof(ar.get('SEContactID__c')),Integer.valueOf(ar.get('expr0')));
        }
        System.debug('##########################elligibleContMap = ' + elligibleContMap);
        
        //Get all contacts from the elligibleContMap and store SurvivorContact and ToBeDeletedContact in their respective Maps
        Map<String,Contact> SurvivorContactMap = new Map<String,Contact>();
        Map<String,Contact> ToBeDeletedContactMap = new Map<String,Contact>();
        
        //Create a map to get the login history of ToBeDeletedContact associated user
        Map<Id, String> UserContactMap = new Map<Id, String>();
        Map<String, List<LoginHistory>> UserLoginMap = new Map<String, List<LoginHistory>>();
        Map<String, Id> SurvivorUserContactMap = new Map<String, Id>();
        
        //Survivor Contact
        for(Contact c : [SELECT Id, Email, PRMUIMSSEContactId__c, PRMCustomerClassificationLevel1__c, PRMCompanyInfoSkippedAtReg__c,
        PRMContactRegistrationStatus__c, PRMCountry__c, PRMDoNotHaveCompany__c, PRMEmail__c, PRMExcludeFromReports__c,
        PRMFirstName__c, PRMJobFunc__c, PRMLastLoginDate__c, PRMLastName__c,
        PRMMobilePhone__c, PRMOrigin__c, PRMJobTitle__c, PRMLanguage__c, PRMUser__c, PRMContact__c, PRMPrimaryContact__c,
        PRMRegistrationActivationDate__c, PRMReasonForDecline__c, PRMSecondary1ClassificationLevel1__c,
        PRMSecondary2ClassificationLevel1__c, PRMSecondary3ClassificationLevel1__c, PRMSecondary1ClassificationLevel2__c,
        PRMSecondary2ClassificationLevel2__c, PRMSecondary3ClassificationLevel2__c, PRMCustomerClassificationLevel2__c,
        PRMTnC__c, PRMWorkPhone__c, SEContactID__c, PRMUIMSID__c, PRMOnly__c
        FROM Contact WHERE SEContactID__c IN: elligibleContMap.keySet() AND PRMUIMSID__c =: null AND PRMOnly__c = false]){
            //PRMUIMSID__c = null AND PRMOnly__c = false => Survivor Contact
            SurvivorContactMap.put(c.SEContactID__c, c);
        }
        
        //ToBeDeleted Contact
        for(Contact c : [SELECT Id, Email, PRMUIMSSEContactId__c, PRMCustomerClassificationLevel1__c, PRMCompanyInfoSkippedAtReg__c,
        PRMContactRegistrationStatus__c, PRMCountry__c, PRMDoNotHaveCompany__c, PRMEmail__c, PRMExcludeFromReports__c,
        PRMFirstName__c, PRMJobFunc__c, PRMLastLoginDate__c, PRMLastName__c,
        PRMMobilePhone__c, PRMOrigin__c, PRMJobTitle__c, PRMLanguage__c, PRMUser__c, PRMContact__c, PRMPrimaryContact__c,
        PRMRegistrationActivationDate__c, PRMReasonForDecline__c, PRMSecondary1ClassificationLevel1__c,
        PRMSecondary2ClassificationLevel1__c, PRMSecondary3ClassificationLevel1__c, PRMSecondary1ClassificationLevel2__c,
        PRMSecondary2ClassificationLevel2__c, PRMSecondary3ClassificationLevel2__c, PRMCustomerClassificationLevel2__c,
        PRMTnC__c, PRMWorkPhone__c, SEContactID__c, PRMUIMSID__c, PRMOnly__c
        FROM Contact WHERE PRMUIMSSEContactId__c IN: elligibleContMap.keySet() AND PRMUIMSID__c <> null AND PRMOnly__c = true]){
            //PRMUIMSID__c != null AND PRMOnly__c = true => ToBeDeleted Contact
            ToBeDeletedContactMap.put(c.PRMUIMSSEContactId__c, c);
        }
        
        //Login History of ToBeDeleted User => Need Manage Users Permission!
        
        //ToBeDeleted User
        for(User u : [SELECT Id, ContactId, Contact.PRMUIMSSEContactId__c FROM User WHERE ContactId <> null AND Contact.PRMUIMSSEContactId__c IN: ToBeDeletedContactMap.keySet()]){
            UserContactMap.put(u.Id, u.Contact.PRMUIMSSEContactId__c);
        }
        
        //Survivor User
        for(User u : [SELECT Id, ContactId, Contact.SEContactId__c FROM User WHERE ContactId <> null AND Contact.SEContactId__c IN: SurvivorContactMap.keySet()]){
            SurvivorUserContactMap.put(u.Contact.SEContactId__c, u.Id);
        }
        
        for(LoginHistory lh : [SELECT Id, LoginTime, LoginType, SourceIp, LoginUrl, Application, Status, UserId, NetworkId
        FROM LoginHistory WHERE UserId IN: UserContactMap.keySet()]){
            if(!UserLoginMap.containsKey(UserContactMap.get(lh.UserId)))
                UserLoginMap.put(UserContactMap.get(lh.UserId), new List<LoginHistory>{lh});
            else
                UserLoginMap.get(UserContactMap.get(lh.UserId)).add(lh);
        }
        
        System.debug('##########################SurvivorContactMap = ' + SurvivorContactMap);
        System.debug('##########################ToBeDeletedContactMap = ' + ToBeDeletedContactMap);
        System.debug('##########################UserLoginMap = ' + UserLoginMap);
        
        //Copy of the PRM fields values in the Survivor Contact
        List<Contact> listContToBeUpdated = new List<Contact>();
        
        //Copy of login history
        List<PRM_Technical_Login_History__c> listLoginHist = new List<PRM_Technical_Login_History__c>();
        
        //Merge the contact & child records
        for(String s : SurvivorContactMap.keySet()){
            if(ToBeDeletedContactMap.containsKey(s)){
                PRM_Technical_Merge_Record__c p = new PRM_Technical_Merge_Record__c();
                if(SurvivorContactMap.get(s).email == ToBeDeletedContactMap.get(s).email){
                    try{
                        //API FIELO Merge Members
                        //FieloPRM_REST_MoveMemberByContact.postMoveMemberByContact(ToBeDeletedContactMap.get(s).Id, SurvivorContactMap.get(s).Id);
                        Contact c = new Contact();
                        c = copyPRMField(SurvivorContactMap.get(s), ToBeDeletedContactMap.get(s));
                        listContToBeUpdated.add(c);
                        Merge SurvivorContactMap.get(s) ToBeDeletedContactMap.get(s);
                        p.Name = SurvivorContactMap.get(s).SEContactID__c;
                        p.PRMUIMS__c = SurvivorContactMap.get(s).PRMUIMSID__c;
                        p.Deleted_Record_Id__c = ToBeDeletedContactMap.get(s).Id;
                        p.Survivor_Id__c = SurvivorContactMap.get(s).Id;
                        p.Object__c = 'Contact';
                        p.Success__c = true;
                        listRecords.add(p);
                        //Retrieve the login history and copy it to technical object
                        if(UserLoginMap.containsKey(s)){
                            for(LoginHistory lh : UserLoginMap.get(s)){
                                PRM_Technical_Login_History__c l = new PRM_Technical_Login_History__c();
                                l.User__c = SurvivorUserContactMap.get(s);
                                l.Application__c = lh.Application;
                                l.Login_Time__c = lh.LoginTime;
                                l.Login_Type__c = lh.LoginType;
                                l.Login_URL__c = lh.LoginUrl;
                                l.Source_IP__c = lh.SourceIp;
                                l.Status__c = lh.Status;
                                listLoginHist.add(l);
                            }
                        }
                    }
                    catch (Exception e){
                        p.Name = SurvivorContactMap.get(s).SEContactID__c;
                        p.PRMUIMS__c = SurvivorContactMap.get(s).PRMUIMSID__c;
                        p.Deleted_Record_Id__c = ToBeDeletedContactMap.get(s).Id;
                        p.Survivor_Id__c = SurvivorContactMap.get(s).Id;
                        p.Object__c = 'Contact';
                        p.Error__c = true;
                        p.Error_Message__c = e.getMessage();
                        listRecords.add(p);
                    }
                }
                else{
                    p.Name = SurvivorContactMap.get(s).SEContactID__c;
                    p.PRMUIMS__c = SurvivorContactMap.get(s).PRMUIMSID__c;
                    p.Deleted_Record_Id__c = ToBeDeletedContactMap.get(s).Id;
                    p.Survivor_Id__c = SurvivorContactMap.get(s).Id;
                    p.Object__c = 'Contact';
                    p.Error__c = true;
                    p.Error_Message__c = 'Different emails. Merge is not possible.';
                    listRecords.add(p);
                }
            }
        }
        //Update Survivor => Needed to bypass AP_Contact_PartnerUserUpdate
        update listContToBeUpdated;
        
        //Insert login history
        insert listLoginHist;
        
        endTime = datetime.now();
    }
    
    global void finish(Database.BatchableContext BC) {
        PRMTechnicalMergeHistory__c contactMergeHistory = new PRMTechnicalMergeHistory__c(Name = BC.getJobId(),
        Batch_Start_Time__c = startTime, Batch_End_Time__c = endTime);
        insert contactMergeHistory;
        
        for(PRM_Technical_Merge_Record__c r: listRecords){
            r.Batch_Id__c = contactMergeHistory.Id;
        }
        insert listRecords;
    }
    
    //Schedule method
    global void execute(SchedulableContext sc) {
        Batch_ContactMerge batchCont = new Batch_ContactMerge();
        database.executeBatch(batchCont);
    }
    
    global Contact copyPRMField(Contact Survivor, Contact toBeDeleted){
        Survivor.PRMUIMSSEContactId__c = toBeDeleted.PRMUIMSSEContactId__c;
        Survivor.PRMCustomerClassificationLevel1__c = toBeDeleted.PRMCustomerClassificationLevel1__c ;
        Survivor.PRMCompanyInfoSkippedAtReg__c = toBeDeleted.PRMCompanyInfoSkippedAtReg__c ;
        Survivor.PRMContactRegistrationStatus__c = toBeDeleted.PRMContactRegistrationStatus__c ;
        Survivor.PRMCountry__c = toBeDeleted.PRMCountry__c ;
        Survivor.PRMDoNotHaveCompany__c = toBeDeleted.PRMDoNotHaveCompany__c ;
        Survivor.PRMEmail__c = toBeDeleted.PRMEmail__c ;
        Survivor.PRMExcludeFromReports__c =toBeDeleted.PRMExcludeFromReports__c ;
        Survivor.PRMFirstName__c = toBeDeleted.PRMFirstName__c ;
        Survivor.PRMJobFunc__c = toBeDeleted.PRMJobFunc__c ;
        Survivor.PRMLastName__c = toBeDeleted.PRMLastName__c ;
        Survivor.PRMMobilePhone__c = toBeDeleted.PRMMobilePhone__c ;
        Survivor.PRMOrigin__c = toBeDeleted.PRMOrigin__c ;
        Survivor.PRMJobTitle__c = toBeDeleted.PRMJobTitle__c ;
        Survivor.PRMLanguage__c = toBeDeleted.PRMLanguage__c ;
        Survivor.PRMUser__c = toBeDeleted.PRMUser__c ;
        Survivor.PRMContact__c = toBeDeleted.PRMContact__c ;
        Survivor.PRMPrimaryContact__c = toBeDeleted.PRMPrimaryContact__c ;
        Survivor.PRMRegistrationActivationDate__c = toBeDeleted.PRMRegistrationActivationDate__c ;
        Survivor.PRMReasonForDecline__c = toBeDeleted.PRMReasonForDecline__c ;
        Survivor.PRMSecondary1ClassificationLevel1__c = toBeDeleted.PRMSecondary1ClassificationLevel1__c ;
        Survivor.PRMSecondary2ClassificationLevel1__c = toBeDeleted.PRMSecondary2ClassificationLevel1__c ;
        Survivor.PRMSecondary3ClassificationLevel1__c = toBeDeleted.PRMSecondary3ClassificationLevel1__c ;
        Survivor.PRMSecondary1ClassificationLevel2__c = toBeDeleted.PRMSecondary1ClassificationLevel2__c ;
        Survivor.PRMSecondary2ClassificationLevel2__c = toBeDeleted.PRMSecondary2ClassificationLevel2__c ;
        Survivor.PRMSecondary3ClassificationLevel2__c = toBeDeleted.PRMSecondary3ClassificationLevel2__c ;
        Survivor.PRMCustomerClassificationLevel2__c = toBeDeleted.PRMCustomerClassificationLevel2__c ;
        Survivor.PRMTnC__c = toBeDeleted.PRMTnC__c;
        Survivor.PRMWorkPhone__c = toBeDeleted.PRMWorkPhone__c ;
        Survivor.PRMUIMSID__c = toBeDeleted.PRMUIMSID__c ;
        
        return Survivor;
    }
}