public class VFC_CreateOpptyFromContract {
    private final SVMXC__Service_Contract__c cs;
    private  String OpportunitySource;
    
    public VFC_CreateOpptyFromContract(ApexPages.StandardController stdController) {
        this.cs = (SVMXC__Service_Contract__c)stdController.getRecord();
            }
    
     public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','--None--'));
            options.add(new SelectOption('Renewal','Service Contract - Renewal'));
            options.add(new SelectOption('Contract','Service Contract'));
            return options;
        }
            
        public String getOpportunitySource() {
            return OpportunitySource;
        }
            
       public void setOpportunitySource(String opp) {
            this.OpportunitySource = opp;
        }
    
    // Code we will invoke on page load.
       
    
    public PageReference CreateOppty() {
        String contractId = ApexPages.currentPage().getParameters().get('id');
      
        PageReference pageRef = new PageReference('/' + contractId);
        //pageRef.setRedirect(true);
        
        if(OpportunitySource == 'Renewal'){
           cs.OpportunitySource__c = 'Service Contract - Renewal';
               }
       else if(OpportunitySource == 'Contract')
       {
       
       cs.OpportunitySource__c= 'Service Contract';
       
       }
       else
       {
       
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select opportunity source value'));
        pageRef.setRedirect(false);
        return null;
       
       }
        
        update cs;
        return pageRef;
    }
}