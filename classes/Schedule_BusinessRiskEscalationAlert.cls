//*********************************************************************************
//Class Name :  Schedule_BusinessRiskEscalationAlert 
// Purpose : It's a batch class Witch is used to send alert to BRE stake holder if BRE with status open has not been update in last 7 days
// Created by : Shivdeep Gupta - Global Delivery Team
// Date created : Feb 08, 2015
// Modified by :
// Date Modified :
// Remarks : For April 15 Release  
///********************************************************************************/ 

global class Schedule_BusinessRiskEscalationAlert implements Database.Batchable<sObject>,Schedulable{
    
    set<ID> setBREIdsToProcess = new set<ID>();
    set<ID> setBREIdsEligibleToProcess = new set<ID>();
    
     global Schedule_BusinessRiskEscalationAlert() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(!Test.isrunningtest()){
            // CLAPR15I2P12 = open
            // CLAPR15I2P14 = 7
            return Database.getQueryLocator([Select id from BusinessRiskEscalations__c where Status__c =: System.Label.CLAPR15I2P12 AND Actual_User_LastmodifiedDate__c < : system.now()- integer.valueof(System.Label.CLAPR15I2P14)]);
        }
         if(Test.isrunningtest()){
            return Database.getQueryLocator([Select id from BusinessRiskEscalations__c limit 1]);
        }
        return NULL;
    }
    
    global void execute(Database.BatchableContext BC, List<BusinessRiskEscalations__c> scope) {
    
        for(BusinessRiskEscalations__c oBRE : scope){
            setBREIdsToProcess.add(oBRE.ID);
        }
        
        AP_ProvideChatterDetails PCD = new AP_ProvideChatterDetails();
    //    setBREIdsEligibleToProcess = PCD.returnObjectSet(setBREIdsToProcess);
    //    PCD.createAlertForBRE_Stackholders(setBREIdsEligibleToProcess);

        PCD.createAlertForBRE_Stackholders(setBREIdsToProcess);
       
    } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    /*
     public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 
    
    public static String sched = '0 05 * * * ?';  //Every Day at Midnight 

    global static String scheduleMe() {
        Batch_BusinessRiskEscalationAlert B_BRE_Alert = new Batch_BusinessRiskEscalationAlert(); 
        return System.schedule('Business Risk Escalation Alert batch Job', sched, B_BRE_Alert);
    }
    */
    global void execute(SchedulableContext sc) {
        Schedule_BusinessRiskEscalationAlert b1 = new Schedule_BusinessRiskEscalationAlert();
        
        // label : CLAPR15I2P13 = 200
        ID batchprocessid = Database.executeBatch(b1,Integer.valueOf(System.Label.CLAPR15I2P13));           
    }
}