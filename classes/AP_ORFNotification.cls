/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 29 May 2013
    Description  : For BR # 3063. PRM June 2013 Release
                        1. notifyPartner_OppTeam
                           (a) When the status of an ORF is updated to approved or ineligible send email to partner and 
                            include the Opportunity Team on the cc: line of the email.
                            
    Modified By   : Renjith Jose
    Modified Date : 22-Jan-2015
    Description   : #BR-6967, Trigger Email for ORF Status: "Additional Information Required"   
                    
***************************************************************************************************************************/
public class AP_ORFNotification
{
/***************************************************************************
When the status of an ORF is updated to approved or ineligible send email to partner and 
include the Opportunity Team on the cc: line of the email.

Conditions to include internal user on CC: line:
1. Partner User does not equal the opportunity leader
2. Only include members of Opportunity Team whose role is set up as "User" <do not include Partner User>
3. Only include on email when status of ORF is updated to Approved or Ineligible

FEB2014 Release: Anil Sistla, ignore sending notifications when ORF RecordType is ProjectRegistration / SimpleORF
**************************************************************************************************************************/
    public static void notifyPartner_OppTeamonApproval(list<OpportunityRegistrationForm__c> lstORF , set<Id> setOppId)
    {
        try
        {
            list<OpportunityTeamMember> lstOppTeam = new list<OpportunityTeamMember>();
            map<Id, list<opportunityteamMember>> mapOppTeam = new map<Id, list<opportunityteamMember>>();
            map<Id,Id> mapOppOwner = new map<Id,Id>();
            
            lstOppTeam = [SELECT Id,OpportunityId,UserId,Opportunity.OwnerId,User.Email FROM OpportunityTeamMember WHERE User.ContactId = null and OpportunityId in : setOppId limit 1000];
            System.debug('***** lstOppTeam ->'+lstOppTeam.size());
            for(opportunityteamMember oppTeam : lstOppTeam)
            {
                if(!mapOppOwner.containsKey(oppTeam.OpportunityId))
                    mapOppOwner.put(oppTeam.OpportunityId,oppTeam.Opportunity.OwnerId);
                if(!mapOppTeam.containsKey(oppTeam.opportunityId))
                    mapOppTeam.put(oppTeam.opportunityId , new list<opportunityteamMember> {(oppTeam)});
                else
                    mapOppTeam.get(oppTeam.opportunityId).add(oppTeam);
            }
            System.debug('****** Mail Send -> notifyPartner_OppTeamonApproval***');   
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            for(OpportunityRegistrationForm__c ORF : lstORF) {
                // Send notification if ORF recordtype is not ProjectRegistration and status != approved
                if (ORF.RecordTypeId == System.Label.CLFEB14PRM04) continue;                
               
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if(!mapOppTeam.isEmpty())
                {
                    if(mapOppTeam.containsKey(ORF.Tech_LinkedOpportunity__c))
                    { 
                        if(mapOppOwner.containsKey(ORF.Tech_LinkedOpportunity__c) && ORF.OwnerID != mapOppOwner.get(ORF.Tech_LinkedOpportunity__c))
                        {
                            list<String> ccAddresses = new list<String>(); 
                            for(opportunityteamMember teamMem : mapOppTeam.get(ORF.Tech_LinkedOpportunity__c))
                            {
                                ccAddresses.add(teamMem.User.Email);
                                mail.setCCAddresses(ccAddresses);
                                System.debug('**** ccAddresses ->'+ccAddresses);
                                
                            }
                        }
                    }
                }
                
                   // System.debug('**** ccAddresses ->'+ccAddresses);
                System.debug('****** Mail Send ***');   
                mail.settargetObjectId(ORF.ownerID);
                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                //mail.settemplateId(Label.CLJUN13PRM06); commented for #BR-6967
                //modified for #BR-6967
                if(ORF.status__c == Label.CLFEB15PRM01){
                    mail.settemplateId(Label.CLFEB15PRM02);
                    List<String> ccAdd = new List<String>();
                    ccAdd.add(ORF.TECH_ORFConverterEmailid__c);
                    mail.setCCAddresses(ccAdd);                 
                }   
                else
                    mail.settemplateId(Label.CLJUN13PRM06);
                    
                mail.setwhatId(ORF.Id);
                mail.setSaveAsActivity(false);
                lstMail.add(mail);
            }
            
            if(!lstMail.isEmpty())
                Messaging.sendEmail(lstMail);
        }
        catch(Exception e)
        {
            system.debug('Exception AP_ORFNotification:Error in sending email to Opportunity Team:'+e.getMessage());
        }
               
    }
    public static void notifyPartner_OppTeamonRejection(map<Id,OpportunityRegistrationForm__c> mapORF)
    {
        try
        {
            list<ORFwithOpportunities__c> lstORFOpp = new list<ORFwithOpportunities__c>();
            list<OpportunityTeamMember> lstOppTeam = new list<OpportunityTeamMember>();
            set<Id> setOppId = new set<Id>();
            map<Id,list<OpportunityRegistrationForm__c>> mapOppORF = new map<Id,list<OpportunityRegistrationForm__c>>();
            map<Id,list<OpportunityTeamMember>> mapOppTeam = new map<Id,list<OpportunityTeamMember>>();
            map<Id,Id> mapOppOwner = new map<Id,Id>();
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            
            lstORFOpp = [ Select id,Opportunity__c,Opportunity_Registration_Form__c from ORFwithOpportunities__c where Opportunity_Registration_Form__c in :mapORF.keySet() limit 1000];
            if(!lstORFOpp.isEmpty())
            {
                for(ORFwithOpportunities__c oppORF : lstORFOpp)
                {
                    setOppId.add(oppORF.Opportunity__c);
                    
                    if(!mapOppORF.containsKey(oppORF.Opportunity__c))
                        mapOppORF.put(oppORF.Opportunity__c , new list<OpportunityRegistrationForm__c> {(mapORF.get(oppORF.Opportunity_Registration_Form__c ))});
                    else
                        mapOppORF.get(oppORF.Opportunity__c).add(mapORF.get(oppORF.Opportunity_Registration_Form__c ));
                }
                lstOppTeam = [SELECT Id,OpportunityId,UserId,Opportunity.OwnerId,User.Email FROM OpportunityTeamMember WHERE User.ContactId = null and OpportunityId in : mapOppORF.keySet() limit 1000];
                for(OpportunityTeamMember teamMem : lstOppTeam)
                {
                    if(!mapOppOwner.containsKey(teamMem.OpportunityId))
                        mapOppOwner.put(teamMem.OpportunityId,teamMem.Opportunity.OwnerId);
                    if(!mapOppTeam.containsKey(teamMem.OpportunityId))
                        mapOppTeam.put(teamMem.OpportunityId , new list<OpportunityTeamMember> {(teamMem)});
                    else
                        mapOppTeam.get(teamMem.OpportunityId).add(teamMem);
                }
              
                
                for(Id oppID : mapOppORF.keySet())
                {
                    for(OpportunityRegistrationForm__c ORF :  mapOppORF.get(oppID)) {
                        // Send notification if ORF recordtype is not ProjectRegistration
                        if (ORF.RecordTypeId == System.Label.CLFEB14PRM04) continue;
                    
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setSaveAsActivity(false);
                        mail.settargetObjectId(ORF.ownerID);
                        mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                        mail.settemplateId(Label.CLJUN13PRM11);
                        mail.setwhatId(ORF.Id);
                        if(mapOppTeam.containsKey(oppID))
                        {
                            if(mapOppOwner.containsKey(oppID) && ORF.OwnerID != mapOppOwner.get(oppID))
                            {
                                list<String> ccAddresses = new list<String>();
                                for(OpportunityTeamMember teamMem : mapOppTeam.get(oppID))
                                {
                                   ccAddresses.add(teamMem.User.Email);
                                   mail.setCCAddresses(ccAddresses);
                                }
                            }
                        }
                        lstMail.add(mail);
                    }
                }
            }
            else
            {
                for(ID ORFID :  mapORF.keySet()) {
                    OpportunityRegistrationForm__c ORF = new OpportunityRegistrationForm__c();
                    ORF = mapORF.get(ORFID);
                    // Send notification if ORF recordtype is not ProjectRegistration
                    if (ORF.RecordTypeId == System.Label.CLFEB14PRM04) continue;
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setSaveAsActivity(false);
                    mail.settargetObjectId(ORF.ownerID);
                    mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                    mail.settemplateId(Label.CLJUN13PRM11);
                    mail.setwhatId(ORF.Id);
                    lstMail.add(mail);
                }
            }
            if(!lstMail.isEmpty())
                Messaging.sendEmail(lstMail);
            
        }
        catch(Exception e)
        {
            system.debug('Exception in AP_ORFNotification: Error in sending email on Rejection:'+e.getMessage());
        }
    }
    
    //--------------------------------------------------------------------------------------------------------------------
    //START: FEB15 Release
    //--------------------------------------------------------------------------------------------------------------------
    public static void notifySEPerson_AddInfoProvided(list<OpportunityRegistrationForm__c> lstORF)
    {
        try
        {
           
            map<Id,List<String>> mapSEPerson = new map<Id,List<String>>();   
            System.debug('****** Mail Send -> notifyPartner_OppTeamonApproval***');   
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            for(OpportunityRegistrationForm__c ORF : lstORF) {
                // Send notification if ORF recordtype is not ProjectRegistration and status != approved
                //if (ORF.RecordTypeId == System.Label.CLFEB14PRM04) continue;
                //modified for #BR-6967
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                     
               
                System.debug('****** Mail Send ***');   
                mail.settargetObjectId(ORF.ownerID);
                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                mail.settemplateId(Label.CLFEB15PRM11); 
                mail.setwhatId(ORF.Id);
                if(ORF.TECH_ORFConverterEmailid__c != null)
                    mapSEPerson.put(ORF.Id,new List<String>{(ORF.TECH_ORFConverterEmailid__c)});
                mail.setSaveAsActivity(false);
                lstMail.add(mail);
            }
            
            if(!lstMail.isEmpty()){
                Savepoint sp = Database.setSavepoint();
                Messaging.sendEmail(lstMail);
                Database.rollback(sp);
                
                List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
                for (Messaging.SingleEmailMessage email : lstMail) {
                    Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                    if(!mapSEPerson.containsKey(email.whatId)) continue;
                    emailToSend.setToAddresses(mapSEPerson.get(email.whatId));
                    System.debug('****** To Address **** :'+mapSEPerson.get(email.whatId));
                    emailToSend.setPlainTextBody(email.getPlainTextBody());
                    emailToSend.setHTMLBody(email.getHTMLBody());
                    emailToSend.setSubject(email.getSubject());
                    lstMsgsToSend.add(emailToSend);
                }
                
                Messaging.sendEmail(lstMsgsToSend);
                
            }   
        }
        catch(Exception e)
        {
            system.debug('Exception AP_ORFNotification:Error in sending email to SE Person:'+e.getMessage());
        }
               
    }
    //END: FEB15 Release
    //--------------------------------------------------------------------------------------------------------------------
     //Insert record in ORFwithOpportunities__c table if a converted ORF status is updated to Ineligible. For EUS#032815
    public static void addORFWithOpportunity(map<Id,ORFwithOpportunities__c> mapORFOpp)
    {
        list<ORFwithOpportunities__c> lstORFOpp = [Select id,Opportunity_Registration_Form__c from ORFwithOpportunities__c where Opportunity_Registration_Form__c in :mapORFOpp.keySet() limit 10000];
        for(ORFwithOpportunities__c orfOpp : lstORFOpp)
        {
            if(mapORFOpp.containsKey(orfOpp.Opportunity_Registration_Form__c))
                mapORFOpp.remove(orfOpp.Opportunity_Registration_Form__c);
        }
        
        if(!mapORFOpp.isEmpty())
        {
            try
            {
                insert mapORFOpp.values();
            }
            catch(DMLException e)
            {
                system.debug('Error in inserting ORF with Opportunity:'+e.getmessage());
            }
        }
    }
    
    public static void addAssessment(map<String,set<ID>> mapORFProgram)
    {
        list<Id> lstProgramId = new list<Id>();
        list<Id> lstProgramLevelId = new list<Id>();
        list<Assessment__c> lstAssessment = new list<Assessment__c>();
        list<OpportunityAssessmentQuestion__c> lstQues = new list<OpportunityAssessmentQuestion__c>();
        list<OpportunityAssessmentResponse__c> lstResponse = new list<OpportunityAssessmentResponse__c>();
        map<Id,list<OpportunityAssessmentQuestion__c>> mapAssessmentQuestion = new map<Id,list<OpportunityAssessmentQuestion__c>>();
        list<PartnerAssessment__c> lstPartnerAssessment = new list<PartnerAssessment__c>();
        Savepoint sp;
        
        try
        {
            for(String str : mapORFProgram.keySet())
            {
                if(str.contains(':'))
                {
                    lstProgramId.add(str.split(':').get(0));
                    lstProgramLevelId.add(str.split(':').get(1));
                }
                
            }
            lstAssessment = [Select id,partnerprogram__c,programlevel__c from Assessment__c where partnerprogram__c in:lstProgramId and programlevel__c in:lstProgramLevelId and AutomaticAssignment__c = true limit 10000];
            lstQues = [Select id,Assessment__c from OpportunityAssessmentQuestion__c where assessment__c in:lstAssessment limit 10000];
            for(OpportunityAssessmentQuestion__c ques : lstQues)
            {
                if(!mapAssessmentQuestion.containsKey(ques.assessment__c))
                    mapAssessmentQuestion.put(ques.assessment__c , new list<OpportunityAssessmentQuestion__c> {(ques)});
                else
                    mapAssessmentQuestion.get(ques.assessment__c).add(ques);
            }
            
             for(Assessment__c assessment : lstAssessment)
                {
                    if(mapORFProgram.containsKey(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c))
                    {
                        for(Id orfID : mapORFProgram.get(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c))
                        {
                            PartnerAssessment__c newAssessment = new PartnerAssessment__c();
                            newAssessment.assessment__c = assessment.Id;
                            newAssessment.OpportunityRegistrationForm__c = orfID;
                            newAssessment.RecordTypeId = Label.CLOCT13PRM01;
                            lstPartnerAssessment.add(newAssessment);
                                               
                        }
                    }
                }
                sp = Database.setSavepoint();
                if(!lstPartnerAssessment.isempty())
                    insert lstPartnerAssessment;
                for(PartnerAssessment__c partnerAssessment : lstPartnerAssessment)
                {
                    if(mapAssessmentQuestion.containsKey(partnerAssessment.assessment__c))
                    {
                        for(OpportunityAssessmentQuestion__c ques : mapAssessmentQuestion.get(partnerAssessment.assessment__c))
                        {
                            OpportunityAssessmentResponse__c response = new OpportunityAssessmentResponse__c();
                            response.partnerassessment__C = partnerAssessment.Id;
                            response.question__c = ques.Id;
                            lstResponse.add(response);
                        }
                    }
                }
                if(!lstResponse.isEmpty())
                    insert lstResponse;
           }
           catch(Exception e)
           {
               system.debug('Exception in addProgramAssessment:'+e.getmessage());
               database.rollback(sp);
           }
    }
    
    
}