// Added by Uttara - Q2 2016 Release : Notes to bFO

public class AP_FieldServiceBulletin {

    public static void checkStatusBeforeCompleted(Set<String> XAFSBids, Set<String> LFSBids, List<FieldServiceBulletin__c> TriggerNew) {
        
        Set<String> errorXAFSB = new Set<String>();
        Set<String> errorLFSB = new Set<String>();
        
        if(!XAFSBids.isEmpty()) {
            for(ContainmentAction__c xa : [SELECT Status__c, FieldServiceBulletin__c FROM ContainmentAction__c WHERE FieldServiceBulletin__c IN: XAFSBids]) {
                if(xa.Status__c == 'Not Started' || xa.Status__c == 'Executing')
                    errorXAFSB.add(xa.FieldServiceBulletin__c);
            }
        }
        System.debug('!!!! errorXAFSB : ' + errorXAFSB);
        
        if(!LFSBids.isEmpty()) {
            for(LocalizedFSB__c lfsb : [SELECT Status__c, FieldServiceBulletin__c FROM LocalizedFSB__c WHERE FieldServiceBulletin__c IN: LFSBids]) {
                if(lfsb.Status__c == 'Not Started' || lfsb.Status__c == 'Executing')
                    errorLFSB.add(lfsb.FieldServiceBulletin__c);
            }
        }
        System.debug('!!!! errorLFSB : ' + errorLFSB);
        
        for(FieldServiceBulletin__c fsb : TriggerNew) {
            if(errorXAFSB.contains(fsb.Id)) {
                if(!Test.isRunningTest())
                    fsb.addError('All the Containment Actions should be Completed to set the Status of the FSB to Completed.');
            }   
            if(errorLFSB.contains(fsb.Id)) {
                if(!Test.isRunningTest())
                    fsb.addError('All the Localized FSB should be Completed to set the Status of the FSB to Completed.');
            }
        }       
    }
    
    public static void setFSBApprover(Map<String,String> mapProductFSB, List<FieldServiceBulletin__c> toUpdateFSB) {
        
        Map<String,String> mapPLFSB = new Map<String,String>();
        Map<String,String> mapFSBQC = new Map<String,String>();
        
        for(OPP_Product__c prod : [SELECT ProductLine__c FROM OPP_Product__c WHERE Id IN: mapProductFSB.keySet()]) {
            mapPLFSB.put(prod.ProductLine__c,mapProductFSB.get(prod.Id));
        }
        System.debug('!!!! mapPLFSB : ' + mapPLFSB);
        
        for(ProductLineQualityContactMapping__c plis : [SELECT QualityContact__c, ProductLine__c FROM ProductLineQualityContactMapping__c WHERE ProductLine__c IN: mapPLFSB.keySet()]) {            
            if(plis.QualityContact__c != null)
                mapFSBQC.put(mapPLFSB.get(plis.ProductLine__c),plis.QualityContact__c);
        }
        System.debug('!!!! mapFSBQC : ' + mapFSBQC);
        
        for(FieldServiceBulletin__c fsb : toUpdateFSB) {
            fsb.FSBApprover__c = mapFSBQC.get(fsb.Id);
        }
        System.debug('!!!! toUpdateFSB : ' + toUpdateFSB);
    }   
    
    public static void setFSBApproverOnInsert(Set<String> stProducts, List<FieldServiceBulletin__c> toupdateFSB) {
        
        Set<String> stProdlines = new Set<String>();
        Map<String,String> mapPLQC = new Map<String,String>();
        
        for(OPP_Product__c prod : [SELECT ProductLine__c FROM OPP_Product__c WHERE Id IN: stProducts]) {
            stProdlines.add(prod.ProductLine__c);
        }
        System.debug('!!!! stProdlines : ' + stProdlines);
        
        for(ProductLineQualityContactMapping__c plis : [SELECT QualityContact__c, ProductLine__c FROM ProductLineQualityContactMapping__c WHERE ProductLine__c IN: stProdlines]) {            
            if(plis.QualityContact__c != null)
                mapPLQC.put(plis.ProductLine__c,plis.QualityContact__c);
        }
        System.debug('!!!! mapPLQC : ' + mapPLQC);
        
        for(FieldServiceBulletin__c fsb : toupdateFSB) {
            fsb.FSBApprover__c = mapPLQC.get(fsb.ProductLine__c);
        }
        System.debug('!!!! toupdateFSB : ' + toupdateFSB);
    }
}