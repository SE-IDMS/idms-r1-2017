public class VFC_GroupOverview extends VFC_ControllerBase {
    
    private static final String HIERARCHY_SEPARATOR = ' I ';
    
    public Group relatedGroup {get;set;}
    public List<PublicGroupMember> memberList {get;set;}
    public List<DataTemplate__c> memberObjectList {get;set;}
    public List<SelectOption> columnHeaders {get;set;}
    public List<String> columnHeaderValues {get;set;}
    public List<Group> groupSearchResult {get;set;}
    private Map<String, Group> roleGroupMap = new Map<String, Group>();
    private Map<String, User> userIdMap = new Map<String, User>();    
    Map<Id, Id> groupHierarchy = new Map<Id, Id>();
    public Boolean showInactiveUsers {get;set;}
    public String userSesaList {get;set;}
    public Boolean confirm = false;

    Map<String, String> roleTypeMap = new Map<String, String>{'RoleAndSubordinates' => 'Role And Subordinates',
                                                              'RoleAndInternalSubordinates' => 'Role And Internal Subordinates',
                                                              'RoleAndSubordinatesInternal' => 'Role And Internal Subordinates'};
    
    private Boolean csvExtract = false;    
    private String newLineCharacter = csvExtract ? ' ' : '<br/>';
    
    Map<Id, String> standardGroupNameMap = new Map<Id, String>{'00GA0000000z5HnMAI' => 'All Internal Users',
                                                               '00GA00000031zFmMAI' => 'All Partner Users',
                                                               '00GA0000003j9IGMAY' => 'All Customer Portal Users'};
    
    public Boolean grantAccessHierarchy {get;set;}    
        
    Map<String, UserRole> roleIdMap = new Map<String, UserRole>();

    public class PublicGroupMember {
    
        public String id {get;set;}
        public Boolean isUser {get;set;}
        public String name {get;set;}
        public String isActive {get;set;}
        public String username {get;set;}
        public String country  {get;set;}
        public String type     {get;set;}
        public String reasonofMembership {get;set;}
        public String hierarchy {get;set;}
    
    }

    public VFC_GroupOverview(ApexPages.StandardController groupController) {
        
        relatedGroup = [SELECT Id, Name, DoesIncludeBosses FROM Group WHERE Id = :groupController.getId() LIMIT 1];
        
        generateColumnHeaders();
        
        if(ApexPages.currentPage().getParameters().get('exportCSV') == 'true') {        
            csvExtract = true;
        }
        
        showInactiveUsers = false;
 
    }
        
    public void generateColumnHeaders() {
    
        columnHeaders = new List<SelectOption>();
        columnHeaders.add(new SelectOption('Name','Name'));
        columnHeaders.add(new SelectOption('Field3__c','Active'));
        columnHeaders.add(new SelectOption('Field4__c','Country'));
        columnHeaders.add(new SelectOption('Field6__c','SESA'));
        columnHeaders.add(new SelectOption('Field5__c','User Type'));                
        columnHeaders.add(new SelectOption('Field1__c','Reason For Membership'));
        columnHeaders.add(new SelectOption('Field2__c','Hierarchy'));    
    }
    
    public PageReference export() {
        return new PageReference('/apex/VFP_GroupOverviewExport?Id='+relatedGroup.Id);
    }
    
    public void getGroupMembers() {

        if(confirm) {        
            ApexPages.Message assignedPsMessage = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Users have been successfully added to the Group.');
            ApexPages.addMessage(assignedPsMessage);
            confirm = false; 
        }
    
        memberObjectList = new List<DataTemplate__c>();
       
        memberList = new List<PublicGroupMember>();
        Map<String, Group> groupIdMap = new Map<String, Group>();
        Set<Id> groupToQueryIdSet = new Set<Id>();
        groupToQueryIdSet.add(relatedGroup.Id);
        groupIdMap.put(relatedGroup.Id, null);

        while(true) {
        
            List<GroupMember> groupMemberList = [SELECT Id, GroupId, UserOrGroupId
                                                 FROM GroupMember WHERE GroupId IN :groupToQueryIdSet];
            
            groupToQueryIdSet.clear();
            
            for(GroupMember member:groupMemberList) {
                
                PublicGroupMember publicGroupMember  = new PublicGroupMember();
                String userOrGroupId = (String)member.UserOrGroupId;
                
                if(userOrGroupId.substring(0,3) == '00G') {
                    groupHierarchy.put(member.UserOrGroupId, member.GroupId);
                    groupIdMap.put(member.UserOrGroupId, null);   
                    groupToQueryIdSet.add(member.UserOrGroupId);    
                    
                    if(standardGroupNameMap.keyset().contains(userOrGroupId)) {
                       
                        publicGroupMember.reasonofMembership = standardGroupNameMap.get(userOrGroupId);
                        publicGroupMember.isUser             = false;
                        publicGroupMember.isActive           = 'N/A';
                        publicGroupMember.id                 = member.UserOrGroupId;
                        publicGroupMember.name               = standardGroupNameMap.get(userOrGroupId);
                        publicGroupMember.hierarchy          = 'N/A';   
                        memberList.add(publicGroupMember);                 
                    }             
                }
                else {
                    publicGroupMember.reasonofMembership = 'Direct Membership';
                    publicGroupMember.isUser             = true;
                    publicGroupMember.id                 = member.UserOrGroupId;
                    publicGroupMember.name               = 'TBD';
                    publicGroupMember.hierarchy          = member.GroupId;

                    Id parentId = groupHierarchy.get(member.GroupId);
 
                    while(parentId != null) {
                        publicGroupMember.reasonofMembership = 'Subgroup';
                        if(parentId != relatedGroup.Id) {
                            publicGroupMember.hierarchy += HIERARCHY_SEPARATOR + parentId;
                        }
                        
                        parentId = groupHierarchy.get(parentId);
                    }
  
                    memberList.add(publicGroupMember);  
                    userIdMap.put(member.UserOrGroupId, null);
                }         
            } 
        
            if(groupToQueryIdSet.isEmpty()) {
                break;
            }
        }
    
        for(User user:[SELECT Id, IsActive, UserType, Username, SESAExternalId__c, Country__c, Name FROM User WHERE Id IN :userIdMap.keyset()]) {
            userIdMap.put(user.Id, user);
        }        

        for(Group publicGroup:[SELECT Id, Name, RelatedId, Type, Related.Name FROM Group WHERE Id IN :groupIdMap.keyset()]) {
            
            groupIdMap.put(publicGroup.Id, publicGroup);
            
            if(publicGroup.RelatedId != null) {
                
                roleIdMap.put(publicGroup.RelatedId, new UserRole(Name =publicGroup.Related.Name));
 
                roleGroupMap.put(publicGroup.RelatedId, publicGroup);   
            }                     
        }
        
        Set<String> userRoleToQuerySet = new Set<String>();
        userRoleToQuerySet.addAll(roleGroupMap.keySet());
        
        populateMemberThroughHierarchy(userRoleToQuerySet);

        for(PublicGroupMember publicGroupMember:memberList) {
            
            if(!standardGroupNameMap.keySet().contains(publicGroupMember.Id)) {

                system.debug('publicGroupMember.reasonofMembership = ' + publicGroupMember.reasonofMembership);
           
                system.debug('Split = ' + publicGroupMember.hierarchy.split(HIERARCHY_SEPARATOR)); 
                
                publicGroupMember.name = '<a href="/'+publicGroupMember.id+'">'+userIdMap.get(publicGroupMember.id).Name+'</a>';                        
                publicGroupMember.username = userIdMap.get(publicGroupMember.id).SESAExternalId__c;                        
                publicGroupMember.isActive = userIdMap.get(publicGroupMember.id).IsActive ? 'True' : 'False';
                publicGroupMember.country  = userIdMap.get(publicGroupMember.id).Country__c;
                publicGroupMember.type     = userIdMap.get(publicGroupMember.id).UserType;
                system.debug('csvExtract = ' + csvExtract);
                
                if(csvExtract) {
                    publicGroupMember.name = userIdMap.get(publicGroupMember.id).Name; 
                }
                  
                for(String roleId:publicGroupMember.hierarchy.split(HIERARCHY_SEPARATOR)) {
    
                    if(publicGroupMember.hierarchy == (String)relatedGroup.Id) {
                        system.debug('NA');
                        publicGroupMember.hierarchy = 'N/A';
                    }
                    else {
                    
                        system.debug('roleId = ' + roleId);
           
                        if(roleGroupMap.get(roleId) != null) {  
                            
                            String roleType = roleGroupMap.get(roleId).Type;
                            
                            if(roleTypeMap.get(roleType) != null) {
                                roleType = roleTypeMap.get(roleType);
                            }
                                                  
                            publicGroupMember.reasonOfMembership = roleType;                        
                        }                        
                               
                        if(roleIdMap.get(roleId) != null) {  
                            String roleLink = '<a href="/'+roleId+'" style="color:#015BA7">'+roleIdMap.get(roleId).Name+'</a>';                                                          
                            
                            if(csvExtract) {
                                roleLink = roleIdMap.get(roleId).Name;
                            }
                            
                            publicGroupMember.hierarchy = publicGroupMember.hierarchy.replace(roleId, roleLink);     
                        }
                        
                        if(roleId.length() <= 18 && groupIdMap.get(roleId) != null) {
                            String groupId = roleId;
                            String groupLink = '<a href="/setup/own/groupdetail.jsp?id='+groupId+'">'+groupIdMap.get(groupId).Name+'</a>';
                            
                            if(csvExtract) {
                                groupLink = groupIdMap.get(groupId).Name;
                            }
                            
                            publicGroupMember.hierarchy = publicGroupMember.hierarchy.replace(groupId, groupLink);
                            
                        }                     
                    }                                      
                }
            }
        }
        
        Map<Id, DataTemplate__c> groupMemberIdMap = new Map<Id, DataTemplate__c>();
        Map<Id, Integer> groupMemberIndexMap = new Map<Id, Integer>();
        
        for(PublicGroupMember member:memberList) {
            if(standardGroupNameMap.keySet().contains(member.Id)) {
                if(!groupMemberIdMap.keySet().contains(member.Id)) {
                    groupMemberIdMap.put(member.Id, new DataTemplate__c(Name = member.Name, 
                                                             Field6__c = member.username,
                                                             Field1__c = member.reasonOfMembership,
                                                             Field2__c = member.hierarchy));                
                }
            } 
        }
        
        for(PublicGroupMember member:memberList) {
        
            if(!standardGroupNameMap.keySet().contains(member.Id)) {

                String hierarchy; 
                String reasonForMembership;

                if(groupMemberIndexMap.get(member.Id) == null) {
                    groupMemberIndexMap.put(member.Id, 1);
                }
                else {
                    Integer i = groupMemberIndexMap.get(member.Id);
                    i++;
                    groupMemberIndexMap.put(member.Id, i); 
                }
                
                if(groupMemberIdMap.keySet().contains(member.Id)) {
                    
                    Integer index = groupMemberIndexMap.get(member.Id);
                    system.debug('index = ' + index);
                    
                    reasonForMembership = (index == 2 ? '1. ' : '') + groupMemberIdMap.get(member.Id).Field1__c + (csvExtract ? ' ' : '<br/>') 
                                          + index + '. ' + member.reasonOfMembership; 
                    hierarchy = (index == 2 ? '1. ' : '') + groupMemberIdMap.get(member.Id).Field2__c + (csvExtract ? ' ' : '<br/>') 
                                + index + '. ' + member.hierarchy;                                
                }
                else {
                    hierarchy = member.hierarchy;
                    reasonForMembership = member.reasonOfMembership;                    
                }
                
                if(showInactiveUsers || (!showInactiveUsers && (member.isActive == 'True' 
                                                || standardGroupNameMap.keyset().contains(member.id)))) {
                
                    groupMemberIdMap.put(member.Id, new DataTemplate__c(Name = member.Name, 
                                                                     Field1__c = reasonForMembership,
                                                                     Field2__c = hierarchy,
                                                                     Field3__c = member.isActive,
                                                                     Field4__c = member.country,
                                                                     Field5__c = member.type,
                                                                     Field6__c = member.username));
                }
            }
        }    
        
        memberObjectList = groupMemberIdMap.values();
    }
    
    public void populateMemberThroughHierarchy(Set<String> userRoleToQuerySet) {
        
        if(userRoleToQuerySet != null && !userRoleToQuerySet.isEmpty()) {
           Map<String, String> roleHierarchyMap = new Map<String, String>();
    
            while(true) {
            
                for(User user:[SELECT Id, Name, Username, SESAExternalId__c, IsActive, UserType, Country__c, UserRoleId, UserRole.Name FROM User 
                               WHERE UserRoleId IN :userRoleToQuerySet]) {
    
                    PublicGroupMember publicGroupMember  = new PublicGroupMember();
    
                    publicGroupMember.reasonofMembership = 'Role';
                    publicGroupMember.isUser             = true;
                    publicGroupMember.id                 = user.Id;
                    publicGroupMember.name               = '<a href="/'+user.Id+'">'+user.Name+'</a>';
                    publicGroupMember.username           = user.SESAExternalId__c;
                    publicGroupMember.isActive           = user.IsActive ? 'True' : 'False';
                    publicGroupMember.hierarchy          = user.UserRoleId;
                    
                    userIdMap.put(user.Id, user);
                    
                    Id parentRoleId = roleHierarchyMap.get(user.UserRoleId);
    
                    while(parentRoleId != null) {
                        publicGroupMember.reasonofMembership = 'Role and Internal Subordinates';
                        publicGroupMember.hierarchy += HIERARCHY_SEPARATOR + parentRoleId;
                        
                        if(roleHierarchyMap.get(parentRoleId) != null) {
                            parentRoleId = roleHierarchyMap.get(parentRoleId);
                        }                        
                        else {
                            break;
                        }
                    }
                    
                    parentRoleId = parentRoleId != null? parentRoleId : user.UserRoleId;
                    
                    system.debug('roleGroupMap.get(parentRoleId) = ' + roleGroupMap.get(parentRoleId));
                    
                    if(roleGroupMap.get(parentRoleId) != null) {
                        
                        Id parentGroupId = roleGroupMap.get(parentRoleId).Id;
                        parentGroupId = groupHierarchy.get(parentGroupId);
                        
                        while(parentGroupId != null && parentGroupId != relatedGroup.Id) {             
                            publicGroupMember.hierarchy += HIERARCHY_SEPARATOR + parentGroupId;
                            parentGroupId = groupHierarchy.get(parentGroupId);
                        }                   
                    } 
             
                    memberList.add(publicGroupMember);                    

                } 
                
                Set<Id> parentRoleIdSet = new Set<Id>();
                
                for(Id roleId:userRoleToQuerySet) {
                    
                    if(roleGroupMap.get(roleId) != null) {
                        if(roleGroupMap.get(roleId).Type == 'RoleAndSubordinates' 
                          || roleGroupMap.get(roleId).Type == 'RoleAndSubordinatesInternal') {
                            parentRoleIdSet.add(roleId);
                        }
                    }
                    else {
                        parentRoleIdSet.add(roleId);
                    }
                }

                userRoleToQuerySet.clear();
                
                for(UserRole userRole:[SELECT Id, Name, ParentRoleId FROM UserRole 
                                       WHERE ParentRoleId IN :parentRoleIdSet]) {
                    
                    userRoleToQuerySet.add(userRole.Id);
                    roleHierarchyMap.put(userRole.Id, userRole.ParentRoleId);
                    roleIdMap.put(userRole.Id, userRole);
                }
                
                if(userRoleToQuerySet.isEmpty()) {
                    break;
                }
            }            
        }
    }
    
    public PageReference exportData() {
        csvExtract = true;
        getGroupMembers();
        userSesaList = '';
        
        columnHeaderValues = new List<String>();
        
        for(SelectOption header:columnHeaders) {
            columnHeaderValues.add(header.getLabel());
        }
        
        PageReference exportPage = new PageReference('/apex/VFP_GroupOverViewExport?Id='+relatedGroup.Id+'&exportCSV=true');
        return exportPage;
    }

    public String getNewLine() {
        return '\n';
    }
    
    public String getQuote() {
        return '';
    }
    
    public PageReference importUsers() {
        
        PageReference result = new PageReference('/apex/VFP_GroupOverview');
 
        if(userSesaList != null && userSesaList != '') {
        
            Set<String> sesaIdSet = new Set<String>();
            List<String> sesaIdList = new List<String>();
            
            for(String sesaNumber:userSesaList.split('\n')) {
                
                if(sesaNumber != null) {
                
                    if(sesaNumber.contains('@')) {
                        sesaIdSet.add(sesaNumber.trim().toLowerCase());
                        sesaIdList.add(sesaNumber.trim().toLowerCase());                    
                    }
                    else {
                        sesaIdSet.add(sesaNumber.trim().toUpperCase());
                        sesaIdList.add(sesaNumber.trim().toUpperCase());                                    
                    }                
                    
                }                
            }
            
            Map<String, SelectOption>  userSesaMap = new Map<String, SelectOption>();
            
            system.debug('sesaIdList = ' +sesaIdList);
            
            for(User user:[SELECT Id, UserName, Name, FirstName, LastName, SESAExternalID__c
                           FROM User WHERE SESAExternalID__c IN :sesaIdList 
                           OR Username IN :sesaIdSet]) {
                
                userSesaMap.put(user.SESAExternalID__c, new SelectOption(user.Id, user.Name));
                userSesaMap.put(user.Username, new SelectOption(user.Id, user.Name));
            }
            
            for(Integer i = 0; i < sesaIdList.size(); i++) {
            
                if(userSesaMap.get(sesaIdList.get(i)) == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Line '+(i+1)+': No user found for SESA: '+ sesaIdList.get(i)));
                    result = null;
                }
            }
            
            if(result != null) {
            
                List<GroupMember> newGroupMemberList = new List<GroupMember>();
            
                for(Integer i = 0; i < sesaIdList.size(); i++) {
                
                    if(userSesaMap.get(sesaIdList.get(i)) != null) {
                        Id userId = userSesaMap.get(sesaIdList.get(i)).getValue();
                        newGroupMemberList.add(new GroupMember(GroupId = relatedGroup.Id, UserOrGroupId = userId));                             
                    }
                }  
                
                confirm = true;                
                List<Database.SaveResult> newGroupMemberResults = Database.insert(newGroupMemberList);           
            }            
        }
        else {
            ApexPages.Message assignedPsMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'You must enter at least one user\'s sesa.');
            ApexPages.addMessage(assignedPsMessage);  
            result = null;
        }
        
        return result;
    }    

    public PageReference backToOverview() {
        return new PageReference('/apex/VFP_GroupOverview?id='+relatedGroup.Id);
    }

    public PageReference addUsers() {
        return new PageReference('/apex/VFP_AddSeveralUsersToAGroup?id='+relatedGroup.Id);
    }
    
    public PageReference displayGroup() {
        return new PageReference('/setup/own/groupdetail.jsp?id='+relatedGroup.Id+'&setupid=PublicGroups');
    }

    public PageReference editGroup() {
        return new PageReference('/setup/own/groupedit.jsp?id='+relatedGroup.Id+'&setupid=PublicGroups&retURL='+ApexPages.currentPage().getURL());
    }    
    
    public PageReference previous() {

        String searchString = ApexPages.currentPage().getParameters().get('searchstring');
        return new PageReference('/apex/VFP_PublicGroupSearch?searchString='+ searchstring);
    }
}