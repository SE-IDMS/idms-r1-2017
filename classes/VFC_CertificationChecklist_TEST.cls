/*
    Author          : Bhuvana Subramaniyan
    Description     : Test class for VFC_CertificationChecklist.
*/


@isTest
private class VFC_CertificationChecklist_TEST 
{
    static testMethod void testCertChecklist()
    {
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        insert cfwr;
        
        CertificationChecklist__c cc = new CertificationChecklist__c();
        cc.CertificationRiskAssessment__c = cfwr.Id;
        insert cc;
        
        test.startTest();
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cc);
        VFC_CertificationChecklist certChecklist= new VFC_CertificationChecklist(sc1);
        certChecklist.urlValue = Label.CLOCT15CFW01;
        certChecklist.urlPageRedirect();       
        
        certChecklist.urlValue = 'VFP_CertificationChecklist';
        certChecklist.urlPageRedirect();  
        
        certChecklist.urlValue = 'VFP_CFWGuidelinesPolicies';
        certChecklist.urlPageRedirect();  
        
        certChecklist.urlValue = 'VFP_RiskAssessment';
        certChecklist.urlPageRedirect();  
        certChecklist.submitCertReqst() ;   
        certChecklist.filterKO();
        certChecklist.unfilterAll();
        certChecklist.edit();
        certChecklist.cancel();
        
        certChecklist.AccessRights_Comments = 'test';
        certChecklist.AlignmentwithIPO_Comments = 'test';
        certChecklist.Anticipationfut_Comments = 'test';
        certChecklist.ApplicationOwner_Comments = 'test';
        certChecklist.BatchPlanGlobal_Comments = 'test';
        certChecklist.BatchPlanSE_Comments = 'test';
        certChecklist.BusinessOwner_Comments = 'test';
        certChecklist.ConnTobFO_Comments = 'test';
        certChecklist.ConnTobFO1_Comments = 'test';
        certChecklist.ConnTobFO2_Comments = 'test';
        certChecklist.ConnectionToSE_Comments = 'test';
        certChecklist.CustomWrittenCe_Comments = 'test';
        certChecklist.DataBreach_Comments = 'test';
        certChecklist.DataCollectionFm_Comments = 'test';
        certChecklist.DataPrivQuestion_Comments = 'test';
        certChecklist.DataRetention_Comments = 'test';
        certChecklist.DisasterRecovery_Comments = 'test';
        certChecklist.DisasterRecovery1_Comments = 'test';
        certChecklist.Encryption_Comments = 'test';
        certChecklist.EntMobMgnt_Comments = 'test';
        certChecklist.ExtractionPerData_Comments = 'test';
        certChecklist.ImpactStudyGlobal_Comments = 'test';
        certChecklist.ImpactStudySE_Comments = 'test';
        certChecklist.KnowledgeTransfer_Comments = 'test';
        certChecklist.LackOfAlignment_Comments = 'test';
        certChecklist.LocalDeployment_Comments = 'test';
        certChecklist.LocalDeployment1_Comments = 'test';
        certChecklist.MobileCoding_Comments = 'test';
        certChecklist.MobileStorage_Comments = 'test';
        certChecklist.MonitoringGlobal_Comments = 'test';
        certChecklist.MonitoringSE_Comments = 'test';
        certChecklist.AdminNonDisc_Comments = 'test';
        certChecklist.PersDataQual_Comments = 'test';
        certChecklist.PrivacyNotice_Comments = 'test';
        certChecklist.ProjScope_Comments = 'test';
        certChecklist.RelavancePerDt_Comments = 'test';
        certChecklist.SaaSCloudSec_Comments = 'test';
        certChecklist.SafeHosting_Comments = 'test';
        certChecklist.SENetwork_Comments = 'test';
        certChecklist.ServiceLevel_Comments = 'test';
        certChecklist.SignOfContract_Comments = 'test';
        certChecklist.SizingGlobal_Comments = 'test';
        certChecklist.SizingSE_Comments = 'test';
        certChecklist.AppCriticality_Comments = 'test';
        certChecklist.SupportModel_Comments = 'test';
        certChecklist.SupportModelGlobal_Comments = 'test';
        certChecklist.SupportModelSE_Comments = 'test';
        certChecklist.SupportTeamIden_Comments = 'test';
        certChecklist.UnapprCldInfra_Comments = 'test';
        certChecklist.UserAuthentication_Comments = 'test';
        certChecklist.UsersChoice_Comments = 'test';
        certChecklist.save();
        
        certChecklist.AccessRights_Comments = 'test1';
        certChecklist.AlignmentwithIPO_Comments = 'test1';
        certChecklist.Anticipationfut_Comments = 'test1';
        certChecklist.ApplicationOwner_Comments = 'test1';
        certChecklist.BatchPlanGlobal_Comments = 'test1';
        certChecklist.BatchPlanSE_Comments = 'test1';
        certChecklist.BusinessOwner_Comments = 'test1';
        certChecklist.ConnTobFO_Comments = 'test1';
        certChecklist.ConnTobFO1_Comments = 'test1';
        certChecklist.ConnTobFO2_Comments = 'test1';
        certChecklist.ConnectionToSE_Comments = 'test1';
        certChecklist.CustomWrittenCe_Comments = 'test1';
        certChecklist.DataBreach_Comments = 'test1';
        certChecklist.DataCollectionFm_Comments = 'test1';
        certChecklist.DataPrivQuestion_Comments = 'test1';
        certChecklist.DataRetention_Comments = 'test1';
        certChecklist.DisasterRecovery_Comments = 'test1';
        certChecklist.DisasterRecovery1_Comments = 'test1';
        certChecklist.Encryption_Comments = 'test1';
        certChecklist.EntMobMgnt_Comments = 'test1';
        certChecklist.ExtractionPerData_Comments = 'test1';
        certChecklist.ImpactStudyGlobal_Comments = 'test1';
        certChecklist.ImpactStudySE_Comments = 'test1';
        certChecklist.KnowledgeTransfer_Comments = 'test1';
        certChecklist.LackOfAlignment_Comments = 'test1';
        certChecklist.LocalDeployment_Comments = 'test1';
        certChecklist.LocalDeployment1_Comments = 'test1';
        certChecklist.MobileCoding_Comments = 'test1';
        certChecklist.MobileStorage_Comments = 'test1';
        certChecklist.MonitoringGlobal_Comments = 'test1';
        certChecklist.MonitoringSE_Comments = 'test1';
        certChecklist.AdminNonDisc_Comments = 'test1';
        certChecklist.PersDataQual_Comments = 'test1';
        certChecklist.PrivacyNotice_Comments = 'test1';
        certChecklist.ProjScope_Comments = 'test1';
        certChecklist.RelavancePerDt_Comments = 'test1';
        certChecklist.SaaSCloudSec_Comments = 'test1';
        certChecklist.SafeHosting_Comments = 'test1';
        certChecklist.SENetwork_Comments = 'test1';
        certChecklist.ServiceLevel_Comments = 'test1';
        certChecklist.SignOfContract_Comments = 'test1';
        certChecklist.SizingGlobal_Comments = 'test1';
        certChecklist.SizingSE_Comments = 'test1';
        certChecklist.AppCriticality_Comments = 'test1';
        certChecklist.SupportModel_Comments = 'test1';
        certChecklist.SupportModelGlobal_Comments = 'test1';
        certChecklist.SupportModelSE_Comments = 'test1';
        certChecklist.SupportTeamIden_Comments = 'test1';
        certChecklist.UnapprCldInfra_Comments = 'test1';
        certChecklist.UserAuthentication_Comments = 'test1';
        certChecklist.UsersChoice_Comments = 'test1';
        certChecklist.save();
        
        CertificationGuidelines__c certGuideline= new CertificationGuidelines__c();
        certGuideline.CertificationRiskAssessment__c = cfwr.Id;
        insert certGuideline;
       
        certChecklist.urlValue = 'VFP_CFWGuidelinesPolicies';
        certChecklist.urlPageRedirect();  
        
        test.stopTest();
    }
}