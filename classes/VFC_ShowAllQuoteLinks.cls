public class VFC_ShowAllQuoteLinks {
    List<OPP_QuoteLink__c> qlinkList=new List<OPP_QuoteLink__c>();
    Opportunity opp;
    
    public VFC_ShowAllQuoteLinks(ApexPages.StandardController controller) {
            
            opp=[Select id, name from Opportunity where id =:ApexPages.currentPage().getParameters().get('oppid')];
            qlinkList=[select id, name ,QuoteRefNumberinLegacy__c,Description__c,QuoteStatus__c,Amount__c,QuoteTo__c,ValidityDate__c,ActiveQuote__c,OwnerId from OPP_QuoteLink__c where OpportunityName__c = :ApexPages.currentPage().getParameters().get('oppid') ];
            
    }
    public List<OPP_QuoteLink__c> getAllQuoteLinks(){
        return qlinkList;
    }
    public Opportunity  getOpportunity(){
        return opp;
    }
    public PageReference doCancel(){
        String url='/'+opp.id;
        PageReference pref =  new PageReference (url);
        pref.setRedirect(true);
        
        return pref ;
    }

}