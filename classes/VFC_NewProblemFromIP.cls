// Added by Uttara PR - Q3 2016 Release - Escape Notes (NotestoI2P)

Public class VFC_NewProblemFromIP {
    
    public String IPid {get; set;}
    public CSQ_ImprovementProject__c IP {get; set;}
    
    public VFC_NewProblemFromIP(ApexPages.StandardSetController controller) {
        IPid = ApexPages.currentPage().getParameters().get('id');
        System.debug('!!!! IPid : ' + IPid);
    }
    
    public PageReference NewEditPage() {
        
        System.debug('!!!! Entered NewEditPage method');
        
        String ProjectStartDate = '';
        
        PageReference ProbeditPage = new PageReference('/'+SObjectType.Problem__c.getKeyPrefix()+'/e?');
        ProbeditPage.getParameters().put('ent', Label.CLQ316I2P05); // setting the ent with Problem object Id
        ProbeditPage.getParameters().put(Label.CL00690, Label.CL00691); // setting nooverride = 1 
        
        if(IPid != null) {        
            System.debug('!!!! From Improvement Project');
            
            IP = [SELECT Name, CSQ_DetectionOrganization__c, CSQ_DetectionOrganization__r.Name, CSQ_AccountableOrganization__c, CSQ_AccountableOrganization__r.Name, CSQ_PointsOfDetection__c, CSQ_WhatIsTheProblem__c, CSQ_WhyIsItaProblem__c, CSQ_HowWasTheProblemDetected__c, CSQ_WhenWasTheProblemDetected__c, CSQ_WhoDetectedTheProblem__c, CSQ_WhereWasTheProblemDetected__c, CSQ_Severity__c, CSQ_ProjectStartDate__c FROM CSQ_ImprovementProject__c WHERE Id =: IPid];
            
            if(IP.CSQ_ProjectStartDate__c != null)
                ProjectStartDate = IP.CSQ_ProjectStartDate__c.day() + '/' + IP.CSQ_ProjectStartDate__c.month() + '/' + IP.CSQ_ProjectStartDate__c.year();

            ProbeditPage.getParameters().put(Label.CLQ316I2P06, IP.CSQ_DetectionOrganization__r.Name); // setting the Detection Organization on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P07, IP.CSQ_DetectionOrganization__c); // setting the Detection Organization on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P08, IP.CSQ_AccountableOrganization__r.Name);  // setting the Accountable Organization on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P09, IP.CSQ_AccountableOrganization__c);  // setting the Accountable Organization on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P10, IP.CSQ_PointsOfDetection__c);  // setting the Point of Detection on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P11, IP.CSQ_WhatIsTheProblem__c);  // setting the WhatIsTheProblem on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P12, IP.CSQ_WhyIsItaProblem__c);  // setting the WhyIsItaProblem on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P13, IP.CSQ_HowWasTheProblemDetected__c);  // setting the HowWasTheProblemDetected on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P14, IP.CSQ_WhenWasTheProblemDetected__c);  // setting the WhenWasTheProblemDetected on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P15, IP.CSQ_WhoDetectedTheProblem__c);  // setting the WhoDetectedTheProblem on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P16, IP.CSQ_WhereWasTheProblemDetected__c);  // setting the WhereWasTheProblemDetected on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P26, IP.Name); // setting the Improvement Project on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P27, IPid); // setting the Improvement Project on Problem
            ProbeditPage.getParameters().put(Label.CLQ316I2P18, ProjectStartDate);  // setting the Date of 8D Start on Problem
            ProbeditPage.getParameters().put(Label.CL00330, IPid); // setting the return URL 
        }
        
        return ProbeditPage;
    }
}