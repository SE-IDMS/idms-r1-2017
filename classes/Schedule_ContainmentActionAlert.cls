//*********************************************************************************
// Class Name : Schedule_ContainmentActionAlert 
// Purpose : A batch class to send alerts for Containment Actions with 
//           any other Status other than Closed or Cancelled and 
//           also that have not been updated in the last 30 days
// Created by : Uttara P R - Global Delivery Team
// Date created : June 17, 2015
// Remarks : For Oct 2015 Release  
//********************************************************************************/ 

global class Schedule_ContainmentActionAlert implements Database.Batchable<sObject>,Schedulable {

    set<ID> setXAIdsToProcess = new set<ID>();
    
    global Schedule_ContainmentActionAlert() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(!Test.isrunningtest()){
            return Database.getQueryLocator([SELECT Id, ActualUserLastmodifiedDate__c, Status__c, TECH_ReminderNoUpdateDate__c FROM ContainmentAction__c WHERE ActualUserLastmodifiedDate__c <= : system.now() - Integer.valueOf(Label.CLOCT15I2P39) AND  (Status__c =: Label.CLOCT15I2P29 OR Status__c =: Label.CLOCT15I2P30) AND (TECH_ReminderNoUpdateDate__c = null OR TECH_ReminderNoUpdateDate__c <= : system.now() - Integer.valueOf(Label.CLOCT15I2P40))]);
        // CLOCT15I2P29 = 'Not Started'
        // CLOCT15I2P30 = 'Executing'
        }
        if(Test.isrunningtest()){
            return Database.getQueryLocator([SELECT Id FROM ContainmentAction__c LIMIT 1]);
        }
        return NULL;
    }
    
    global void execute(Database.BatchableContext BC, List<ContainmentAction__c> scope) {
        
        for(ContainmentAction__c XA : scope) {
            setXAIdsToProcess.add(XA.Id);
        }
        
        AP1002_ContainmentAction.createAlertForXA_Stackholders(setXAIdsToProcess);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    global void execute(SchedulableContext SC) {
        Schedule_ContainmentActionAlert P1 = new Schedule_ContainmentActionAlert();
        
        // label : CLAPR15I2P13 = 200
        ID batchprocessid = Database.executeBatch(P1,Integer.valueOf(System.Label.CLAPR15I2P13));           
    }
}