@isTest
Private class AP_Organization_Test{

  static TestMethod void testingupdateOrgination(){
  
  
     BusinessRiskEscalationEntity__c oResolutionBREE1 = new BusinessRiskEscalationEntity__c(Entity__c  =  'TESTBREE',Location_Type__c = 'Country Front Office');
             insert oResolutionBREE1;
             
              Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
            BusinessRiskEscalationEntity__c oResolutionBREE = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE', SubEntity__c =  'BRESubEnty', Location_Type__c = 'Country Front Office', Street__c = 'Street__c');
            insert oResolutionBREE;
             
            Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
            oResolutionBREE.Street__c = '365437sdStreet__c232';
            Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfUpdate');
            update oResolutionBREE;
            BusinessRiskEscalationEntity__c oOriginatingBREE1 = new BusinessRiskEscalationEntity__c(SubEntity__c = 'BRESubEnty', Location__c = 'BRELocation123', Entity__c = 'TESTBREE', Location_Type__c = 'Country Front Office');
             insert oOriginatingBREE1; 
             Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
             oOriginatingBREE1.SubEntity__c = '';
                try{
                Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfUpdate');
                update oOriginatingBREE1;
                }catch (DmlException e) {}
                Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
            BusinessRiskEscalationEntity__c oOriginatingBREE = new BusinessRiskEscalationEntity__c(SubEntity__c = 'BRESubEnty', Location__c = 'BRELocation', Entity__c = 'TESTBREE', Location_Type__c = 'Country Front Office');
             insert oOriginatingBREE; 
             Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
              BusinessRiskEscalationEntity__c oOriginatingBREE33 = new BusinessRiskEscalationEntity__c(SubEntity__c = 'BRESubEntyE', Location__c = '', Entity__c = 'TESTBREEE', Location_Type__c = 'Country Front Office');
             insert oOriginatingBREE33; 
             try{
             Delete oOriginatingBREE; 
             }catch (DmlException e) {}
             BusinessRiskEscalationEntity__c oOriginatingBREE123 = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE123', SubEntity__c = 'BRESubEnty123', Location__c = 'BRELocation', Location_Type__c = 'Country Front Office');
             try{insert oOriginatingBREE123;}catch (DmlException e) {}
                BusinessRiskEscalationEntity__c oOriginatingBREEqwe = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE123', Location__c = 'BRESubEnty123', Location_Type__c = 'Country Front Office');
             try{insert oOriginatingBREEqwe;}catch (DmlException e) {}
             BusinessRiskEscalationEntity__c oOriginating = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE123123', Location_Type__c = 'Country Front Office');
             insert oOriginating;
             BusinessRiskEscalationEntity__c oOrigina = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE123123',Location__c = 'BRESubEnty123', Location_Type__c = 'Country Front Office');
             try{insert oOrigina;}catch (DmlException e) {}
             BusinessRiskEscalationEntity__c oOrig = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE123123', SubEntity__c = 'BRESubEnty123123', Location__c = '', Location_Type__c = 'Country Front Office');
             insert oOrig;
                 Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
             BusinessRiskEscalationEntity__c oResolutionBREE55 = new BusinessRiskEscalationEntity__c(Entity__c  =  'TESTBREE5',Location_Type__c = 'Country Front Office');
             insert oResolutionBREE55;
             
              Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
            BusinessRiskEscalationEntity__c oResolutionBREE555 = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE5', SubEntity__c =  'BRESubEnty5', Location_Type__c = 'Country Front Office', Street__c = 'Street__c');
            insert oResolutionBREE555;
            try{
             Delete oResolutionBREE55; 
             }catch (DmlException e) {}
   
}
static TestMethod void myUnitTest(){
            Test.startTest(); 

        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        Country__c country = Utils_TestMethods.createCountry();
       Database.insert(country );
        

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
       
        objContact.Country__c=country.id;
        
        insert objContact;
        
         
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        User newUser = Utils_TestMethods.createStandardUser('TestUser856975');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        User newUser2 = Utils_TestMethods.createStandardUser('TestUser352634');         
        Database.SaveResult UserInsertResult2 = Database.insert(newUser2, false);        
         
       TEX__c temptech=AP_TechnicalExpertAssessment_Test.createTEX(objCaseForAction.id,newUser.id);
        temptech.Material_Status__c='Delivered to Return Center';
        temptech.ShippedFromCustomer__c = null;
        temptech.ExpertAssessmentStarted__c = system.today();
        insert temptech;  
                
        RMA__c temRMA=AP_TechnicalExpertAssessment_Test.createReturnRequest(objCaseForAction.Id,objAccount.id, country.Id,temptech.id);
        temRMA.ShippedFromCustomer__c = system.today();
        temRMA.Status__c='Rejected';
        insert temRMA;
         BusinessRiskEscalationEntity__c oResolutionBREE1 = new BusinessRiskEscalationEntity__c(Entity__c = 'TESTBREE');
            insert oResolutionBREE1;
            
        BusinessRiskEscalationEntity__c oResolutionBREE = new BusinessRiskEscalationEntity__c();
             oResolutionBREE.Entity__c = 'TESTBREE';
             oResolutionBREE.SubEntity__c =  'BRESubEnty';              
             oResolutionBREE.Location_Type__c = 'Return Center';
             oResolutionBREE.Street__c = 'Street__c';
            insert oResolutionBREE;
            
            
        OPP_Product__c prod = new OPP_Product__c(Name='Test Product Name', BusinessUnit__c = 'Business Unit',ProductLine__c='Product Line',ProductFamily__c='Product Family',Family__c='Family', TECH_PM0CodeInGMR__c='200201140', HierarchyType__c='Hierarchy type');
         insert prod;
        RMA_Product__c tempRTM=AP_TechnicalExpertAssessment_Test.createReturnItem(temRMA.id); 
        tempRTM.ReturnCenter__c = oResolutionBREE.id;
        insert tempRTM;
        oResolutionBREE.Street__c = '365437sdStreet__c232';
            update oResolutionBREE;
       test.stoptest();         
  
    }
    
    static testMethod void OrgMethod1() {
        
         List<BusinessRiskEscalationEntity__c> brlist = new List<BusinessRiskEscalationEntity__c>();
         BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
         insert brEntity;
         BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
         insert brSubEntity;
         BusinessRiskEscalationEntity__c br = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocationReturnCenter();
         insert br;  
         try{
                Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfUpdate');
                update brSubEntity;
                }catch (DmlException e) {}
         brlist.add(brEntity); 
         AP_Organization.checkOrgEntySubentyLocation(brlist);
         brlist.add(brSubEntity); 
         AP_Organization.checkOrgEntySubentyLocation(brlist);
         brlist.add(br);
         AP_Organization.checkOrgEntySubentyLocation(brlist);        
         
         Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfDelete');
         Delete brlist;
    }
}