@isTest(SeeAllData=true)
private class SVMXC_AcceptOrRejectWOServiceTest {

    static testMethod void testAcceptOrRejectWOService() {
    
        //profile used
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];  
        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        insert user;

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.subject = 'Test without Id';
        email.plainTextBody = 'FromEmail';
        email.fromAddress = user.Email;
        env.fromAddress = user.Email;
    
        //Create Country
        Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Type ='SA';
        insert acc;
        
        SVMXC__Site__c site = new SVMXC__Site__c(name= 'site1');
        insert site;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(name='testsc');
        insert sc;

        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
                
        RecordType rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c' and DeveloperName ='Technician'];
        RecordType rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c' and DeveloperName = 'Technican'];  
 
        st = new SVMXC__Service_Group__c(RecordTypeId =rts.Id, SVMXC__Active__c = true,Name = 'Test Service Team');
        insert st;
         
        sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
        insert sgm1;
    
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        workOrder.Work_Order_Category__c='On-site';   
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.WorkOrderSubType__c='3-Day Workshop'; 
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.CustomerTimeZone__c='Asia/Baghdad';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.SVMXC__Site__c = site.id;
        workOrder.SVMXC__Company__c= acc.id;
        workOrder.SVMXC__Service_Contract__c = sc.id;
        insert workOrder;

        Test.startTest();

        SVMXC_AcceptOrRejectWOService woService = new SVMXC_AcceptOrRejectWOService();
        woService.handleInboundEmail(email, env);
        email.subject = 'Accept (a4EO00000000LGZMA2a4EO00000000LGZMA2)';  //invalid Id   
        woService.handleInboundEmail(email, env);

        workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
        update workOrder;
           
        AssignedToolsTechnicians__c technician1 = Utils_TestMethods.CreateAssignedTools_Technician(workOrder.id, sgm1.id);
        technician1.Status__c ='Assigned';
        
        insert technician1;

        email.subject = 'Accept (' + sgm1.Id + workOrder.Id + ')';  //valid Id
        woService.handleInboundEmail(email, env);
        
        email.subject = 'Reject (' + sgm1.Id + workOrder.Id + ')';  //valid Id
        woService.handleInboundEmail(email, env);
        
        workOrder.SVMXC__Order_Status__c = 'Service Completed';
        update workOrder;
        
        email.subject = 'Accept (' + sgm1.Id + workOrder.Id + ')';  //valid Id
        woService.handleInboundEmail(email, env);
        
        Test.stopTest();
        
    }
}