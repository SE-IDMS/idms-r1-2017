/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 12/05/2011
    Description     : Controller class for the page BUD_CloneBudget
                      Clones the selected budget and the budget lines associated with it.
                      
*/

public class BUD_CloneBudgetController
{
    Budget__c clonedBud {get;set;}
    Budget__c oldBud {get;set;}
    List<string> lstBudgetFields {get;set;}
    public ApexPages.StandardController controller;
    List<PRJ_BudgetLine__c> lstClonedBudgetLines {get;set;}
    List<PRJ_BudgetLine__c> lstAddClonedBudgetLines {get;set;}
    Boolean isBudgetCreated {get;set;}
    List<string> lstBudgetLineTypesAll {get;set;}
    List<string> lstBudgetLineTypesTemp1 {get;set;}    
    List<string> lstBudgetLineTypesTemp2 {get;set;}        
    Integer CurrentYear {get;set;}
    Integer intNumBudLines {get;set;}
    
    //constructor
    public BUD_CloneBudgetController(ApexPages.StandardController controller)
    {   
        //add all the fields to the controller 
        lstBudgetFields = new List<string>{'Payback__c','Type__c','CurrencyIsoCode','ProjectReq__c','Description__c','Year_Origin__c','PastBudgetAPACCumulative__c','PastBudgetCHINACumulative__c','PastBudBusCashoutOPEXOTCCumulative__c','PastBudgetNAMCumulative__c','PastBudgetISMACumulative__c','PastBudgetEUROPECumulative__c','PastBudBusinessRunningCostCumulative__c','PastBudgetASCECumulative__c','PastBudgetBusCashoutCAPEXCumulative__c','PastBudgetBusinessInternalCumulative__c','PastBudgetCashoutCAPEXCumulative__c','PastBudgetCashoutOPEXCumulative__c','PastBudgetGDCostsCumulative__c','PastBudgetPLImpactCumulative__c','PastBudgetRegionsCumulative__c','PastBudgetSmallEnhancementsCumulativ__c','PastBudIntChargedBacktoIPOCumulative__c','PastBudNonIPOITCostsIntCumulative__c','PastBudOperServicesCumulative__c','PastBudRunningITCostsImpCumulative__c'};
        /*
        lstBudgetFields.add('Payback__c');
        lstBudgetFields.add('Type__c');
        lstBudgetFields.add('CurrencyIsoCode');
        lstBudgetFields.add('ProjectReq__c');
        lstBudgetFields.add('Description__c');
        lstBudgetFields.add('PastBudgetCashoutCAPEXCumulative__c');
        lstBudgetFields.add('PastBudgetCashoutOPEXCumulative__c');
        lstBudgetFields.add('PastBudgetInternalCAPEXCumulative__c');
        lstBudgetFields.add('PastBudgetInternalOPEXCumulative__c');
        lstBudgetFields.add('PastBudgetSmallEnhancementsCumulativ__c');
        lstBudgetFields.add('PastBudgetGDCostsCumulative__c');                                        
        lstBudgetFields.add('Year_Origin__c');
        lstBudgetFields.add('PastBudgetCashoutCAPEX__c');
        lstBudgetFields.add('PastBudgetCashoutOPEX__c');
        lstBudgetFields.add('PastBudgetInternalCAPEX__c');
        lstBudgetFields.add('PastBudgetInternalOPEX__c');
        lstBudgetFields.add('PastBudgetSmallEnhancements__c');
        lstBudgetFields.add('PastBudgetGDCostsCumulative__c');                                                
        if(!Test.isRunningTest())            
            controller.addFields(lstBudgetFields);        
        
        lstBudgetFields.add('HistoryTotalCashoutCAPEX__c');
        lstBudgetFields.add('HistoryTotalCashoutOPEX__c');
        lstBudgetFields.add('HistoryTotalInternalCAPEX__c');
        lstBudgetFields.add('HistoryTotalInternalOPEX__c');
        lstBudgetFields.add('HistoryTotalSmallEnhancements__c');
        */
        
        this.controller = controller;
        
        clonedBud = new Budget__c();    
        oldBud = (Budget__c)controller.getRecord();         
        isBudgetCreated = false;
        lstBudgetLineTypesAll = new List<string>();
        lstBudgetLineTypesTemp1 = new List<string>();
        lstBudgetLineTypesTemp2 = new List<string>();        
        CurrentYear = Integer.ValueOf(DateTime.Now().Year());
        //CurrentYear = 2012;
        
        //copy the budget line values
        //create 21 budget new budget lines
        lstClonedBudgetLines = new List<PRJ_BudgetLine__c>();
        lstAddClonedBudgetLines = new List<PRJ_BudgetLine__c>();
        intNumBudLines = 0;
    }
    
    //to clone the budget and redirect to the cloned budget
    public PageReference CloneBudget()
    {
        system.debug('inside cloneBudget');
        Boolean isTypeFound = false;
        string strBudgetLineType = '';
        Map<String,DMTPastBudgetFields__c> csMap = DMTPastBudgetFields__c.getAll();
        SObject sobjclonedBud = (SObject)clonedBud;
        SObject sobjoldBud = (SObject)oldbUd;
        
        
        //copy the budget field values
        clonedBud.ProjectReq__c = oldBud.ProjectReq__c;
        clonedBud.Description__c = oldBud.Description__c;
        clonedBud.Payback__c = oldBud.Payback__c;
        clonedBud.Type__c = oldBud.Type__c;
        clonedBud.CurrencyIsoCode = oldBud.CurrencyIsoCode;
        
        try
        {
            insert clonedBud;
            isBudgetCreated = true;
            system.debug('isBudgetCreated:' + isBudgetCreated);
            if(Test.isRunningTest())
                throw new TestException('Test Exception');
        }
        catch(Exception exp)
        {
            System.Debug('Insert Cloned Budget Failed');
        }
        
        
        
        //integer oldBudFirstYear = Integer.ValueOf(oldBud.Year_Origin__c);
        //integer oldBudFirstYear = 2012;
        //System.Debug('oldBudFirstYear: ' + oldBudFirstYear);
        /*
        clonedBud.PastBudgetCashoutCAPEXCumulative__c = oldBud.PastBudgetCashoutCAPEXCumulative__c;      
        clonedBud.PastBudgetCashoutOPEXCumulative__c = oldBud.PastBudgetCashoutOPEXCumulative__c;
        clonedBud.PastBudgetInternalCAPEXCumulative__c = oldBud.PastBudgetInternalCAPEXCumulative__c;
        clonedBud.PastBudgetInternalOPEXCumulative__c = oldBud.PastBudgetInternalOPEXCumulative__c;
        clonedBud.PastBudgetSmallEnhancementsCumulativ__c = oldBud.PastBudgetSmallEnhancementsCumulativ__c;
        clonedBud.PastBudgetGDCostsCumulative__c = oldBud.PastBudgetGDCostsCumulative__c;
        */
        
        for(DMTPastBudgetFields__c csInstance:csMap.values())
        {
            system.debug('csInstance:' + csInstance);
            sobjclonedBud.put(csInstance.BudgetFieldAPIName__c,sobjoldBud.get(csInstance.BudgetFieldAPIName__c));
        }
        clonedBud.Cloned__c = true;

                        
        //loop through the budget lines of oldBud and copy budgetLines for FY0 onwards        
        for(PRJ_BudgetLine__c oldBudLine : [select Id,Amount__c,Budget__c,Description__c,TECH_FY_Index__c,Type__c,Year__c,CurrencyIsoCode from PRJ_BudgetLine__c where Budget__c =: oldBud.Id and Type__c <> 'Internal CAPEX' AND Type__c <> 'Internal OPEX'])
        {
            system.debug('oldBudLine.Year__c:' + oldBudLine.Year__c);
            system.debug('CurrentYear:' + CurrentYear);
            if(oldBudLine.Year__c <> null)
            {
                if(Integer.ValueOf(oldBudLine.Year__c) >= CurrentYear)
                {
                    system.debug('copy amount values');
                    //copy the amount values                    
                    PRJ_BudgetLine__c newBudLine;
                    if(oldBudLine.Amount__c <> null)
                    {
                        newBudLine = new PRJ_BudgetLine__c(Amount__c = oldBudLine.Amount__c,Budget__c = clonedBud.Id,Description__c = oldBudLine.Description__c,TECH_FY_Index__c = Integer.ValueOf(oldBudLine.Year__c) - CurrentYear,Type__c = oldBudLine.Type__c,Year__c = oldBudLine.Year__c,CurrencyIsoCode = oldBudLine.CurrencyIsoCode);                                        
                    }
                    else
                    {
                        newBudLine = new PRJ_BudgetLine__c(Amount__c = 0,Budget__c = clonedBud.Id,Description__c = oldBudLine.Description__c,TECH_FY_Index__c = Integer.ValueOf(oldBudLine.Year__c) - CurrentYear,Type__c = oldBudLine.Type__c,Year__c = oldBudLine.Year__c,CurrencyIsoCode = oldBudLine.CurrencyIsoCode);                                           
                    }                        
                    lstClonedBudgetLines.add(newBudLine);
                }
                else
                {
                    //calculate the past values
                    System.Debug('Type 1:' + oldBudLine.Type__c);            
                    System.Debug('Type 2:' + System.Label.DMT_CashoutCAPEX);
                    System.Debug('Year:' + oldBudLine.Year__c);                        
                    System.Debug('PB1:' + clonedBud.PastBudgetCashoutCAPEXCumulative__c);                        
                    System.Debug('PB2:' + oldBud.PastBudgetCashoutCAPEXCumulative__c);                        
                    //System.Debug('Amount:' + oldBudLine.Amount__c);   
                    
                    //if(Integer.ValueOf(oldBudLine.Year__c) == oldBudFirstYear)
                    //{
                    decimal amt;
                    if(oldBudLine.Amount__c == null)
                    {
                        amt = 0;
                    }
                    else
                    {
                        amt = oldBudLine.Amount__c;
                    }
                    system.debug('amt:' + amt);
                    //for(DMTPastBudgetFields__c csInstance:csMap.values())
                    for(String strKey:csMap.keySet())
                    {   
                        system.debug('strKey:' + strKey);                
                        if(oldBudLine.Type__c == strKey)
                        {
                            system.debug('type:' + oldBudLine.Type__c);
                            decimal amountPast = Decimal.valueOf(sobjclonedBud.get(csMap.get(strKey).BudgetFieldAPIName__c) + '') + amt;
                            system.debug('amountPast:' + amountPast );
                            sobjclonedBud.put(csMap.get(strKey).BudgetFieldAPIName__c,amountPast);
                        }
                    }
                    
                    /*
                        if(oldBudLine.Type__c == System.Label.DMT_CashoutCAPEX)
                        {
                            clonedBud.PastBudgetCashoutCAPEXCumulative__c += amt;    
                        }  
                        if(oldBudLine.Type__c == System.Label.DMT_CashoutOPEX)
                        {
                            clonedBud.PastBudgetCashoutOPEXCumulative__c += amt;    
                        }  
                        if(oldBudLine.Type__c == System.Label.DMT_InternalCAPEX)
                        {
                            clonedBud.PastBudgetInternalCAPEXCumulative__c += amt;    
                        }  
                        if(oldBudLine.Type__c == System.Label.DMT_InternalOPEX)
                        {
                            clonedBud.PastBudgetInternalOPEXCumulative__c += amt;    
                        }  
                        if(oldBudLine.Type__c == System.Label.DMT_SmallEnhancements)
                        {
                            clonedBud.PastBudgetSmallEnhancementsCumulativ__c += amt;    
                        }  
                        if(oldBudLine.Type__c == System.Label.DMT_GDCosts)
                        {
                            clonedBud.PastBudgetGDCostsCumulative__c += amt;    
                        }  
                    //}   
                    */                  
                }
            }        
            
        }
        
        
        try
        {
            update clonedBud;
            update oldBud;
            
            if(Test.isRunningTest())
                throw new TestException('Test Exception');
        }
        catch(Exception exp)
        {
            System.Debug('Update Cloned Budget Failed');
        }
        
        
        //bring the total number of budget lines to 45
        //the number of budget lines can be 0,15,30 or 45
        //if 0, 45 no change
        //if 15, add 30 budget lines -> Amount = 0
        //                             TECH_FY_Index__c = 15 1s, 15 2s
        //                             Year__c = 15 FY1s, 15 FY2s 
        //                             Budget__c = clonedBudget
        //                             CurrencyIsoCode = clonedBudget.CurrencyIsoCode
        //If 30, add 15 budget lines -> Amount = 0
        //                             TECH_FY_Index__c = 15 2s
        //                             Year__c = 15 FY2s                               
        //                             Budget__c = clonedBudget  
        //                             CurrencyIsoCode = clonedBudget.CurrencyIsoCode
        
        Schema.DescribeFieldResult fieldResult = PRJ_BudgetLine__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry ent : ple)
        {
            lstBudgetLineTypesAll.add(ent.getValue());
        }
        
        intNumBudLines = Integer.ValueOf(PRJ_Config__c.getInstance().NumberOfYearsForBudget__c * lstBudgetLineTypesAll.size());
        
        //rules out 0 and 45
        if(lstClonedBudgetLines.size() <> 0 && lstClonedBudgetLines.size() < intNumBudLines)
        {
            //outer for loop for number of sets of budget lines
            for(integer i = 1 ; i <= ((intNumBudLines - lstClonedBudgetLines.size()) / lstBudgetLineTypesAll.size()) ; i++)
            {                
                integer k = Integer.ValueOf(PRJ_Config__c.getInstance().NumberOfYearsForBudget__c) - i;
                
                //inner for loop to add additional budget lines
                for(integer j = 0; j < lstBudgetLineTypesAll.size(); j++ )
                {
                    PRJ_BudgetLine__c tmpBudgetLine = new PRJ_BudgetLine__c(Amount__c = 0,TECH_FY_Index__c = k,Year__c = String.ValueOf(CurrentYear + k),Budget__c = clonedBud.Id,CurrencyIsoCode = clonedBud.CurrencyIsoCode, Type__c = lstBudgetLineTypesAll[j]);    
                    lstAddClonedBudgetLines.add(tmpBudgetLine);                    
                }
             }
             
        }   
                
                
                    
        //create the budget lines for the cloned budget
        try
        {                    
            if(lstClonedBudgetLines.size() > 0)
                insert lstClonedBudgetLines;
                
            if(lstAddClonedBudgetLines.size() > 0)
                insert lstAddClonedBudgetLines;
                
                
            if(Test.isRunningTest())
                throw new TestException('Test Exception');
        }
        catch(Exception exp)
        {
            System.Debug('Insert Failed');
        }   
        
        if(isBudgetCreated)     
            return new PageReference('/' + clonedBud.Id);
        else    
            return null;
    }
    
    public class TestException extends Exception{}
}