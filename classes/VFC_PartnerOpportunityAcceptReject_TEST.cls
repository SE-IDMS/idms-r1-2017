@isTest
public class VFC_PartnerOpportunityAcceptReject_TEST {
    static testMethod void myTest(){
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp = Utils_TestMethods.createOpportunity(acc.id);
        insert opp;

        FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
        feature.Enabled__c = 'Contact';
        feature.Visibility__c = 'Contact';
        feature.Category__c=System.Label.CLAPR14PRM51;
        insert feature;

        Contact con = Utils_TestMethods.createContact(acc.id, 'Test Contact');
        insert con;

        ContactAssignedFeature__c caf = new ContactAssignedFeature__c(
                Active__C = true,
                Contact__c = con.Id,
                FeatureCatalog__c = feature.ID
            );
        insert caf;
        
        User usr = Utils_TestMethods.createStandardUser('00eA0000000aywg','tUsVFC92');
        usr.contactid = con.Id;
        insert usr;
        
        OpportunityTeamMember opptm = Utils_TestMethods.createOppTeamMember(opp.id, usr.id);
        opptm.UserId = usr.id;
        opptm.OpportunityId = opp.id;
        opptm.TeamMemberRole = System.Label.CLOCT13PRM13;
        insert opptm;
        
        List<OPP_ValueChainPlayers__c> lstVCP = new List<OPP_ValueChainPlayers__c>();
        OPP_ValueChainPlayers__c opval = Utils_TestMethods.createValueChainPlayer(acc.id, con.id, opp.id);
        opval.ContactRole__c = System.Label.CLSEP12PRM16;
        opval.Contact__c = con.id;
        opval.OpportunityName__c = opp.id;
        lstVCP.add(opval);

        OPP_ValueChainPlayers__c opval2 = Utils_TestMethods.createValueChainPlayer(acc.id, con.id, opp.id);
        opval2.ContactRole__c = System.Label.CLMAY13PRM40;
        opval2.Contact__c = con.id;
        opval2.OpportunityName__c = opp.id;
        lstVCP.add(opval2);
        insert lstVCP;

        opp.PartnerInvolvement__c = System.Label.CLMAY13PRM41;
        update opp;

        Recordtype partneropprecordType = [Select id from RecordType where developername =  'PartnerOpportunityTask' limit 1];
        Task task1 = Utils_TestMethods.createPartnerOpportunityTask(opp, usr.id);
        task1.recordtypeid = partneropprecordType.id;
        task1.OwnerId = usr.id;
        insert task1;
        
        PartnerOpportunityStatusHistory__c popp = new PartnerOpportunityStatusHistory__c();
        popp.Opportunity__c = task1.TECH_OpportunityId__c;
        popp.user__c = task1.OwnerId;
        popp.Status__c = 'Assigned';
        insert popp;
        
        ApexPages.currentPage().getParameters().put('TaskId',task1.Id);
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(task1);
        VFC_PartnerOpportunityAcceptReject vfcon = new VFC_PartnerOpportunityAcceptReject(sdtcon1);
        vfcon.updatetask();

        Task opptyTsk = AP_OpportunityTeamMember.createTask(opp, usr.Id);
    }
}