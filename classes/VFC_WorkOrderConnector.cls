public class VFC_WorkOrderConnector {
    
    private final ID workOrderId;
    private final String action;
    private boolean hasAccess=true;
    private SVMXC__Service_Order__c workOrder;
    private List<SVMXC__Service_Order_Line__c > workDetails;
    private List<SVMXC__RMA_Shipment_Order__c > PartOrders; //Added this line by VISHNU C for BR-9101 Q2 Release 2016 
    //Added for BFS Release:Srart
    public Final String CORDYSURL = System.Label.CLAPR15SRV38;
    public Final String WOBFOID = System.Label.CLAPR15SRV39;
    public Final String BACKOFFICEREFERENCE = System.Label.CLAPR15SRV40;
    public Final String BACKOFFICESYSTEM = System.Label.CLAPR15SRV41 ;
    public Final String COUNTRYOFBACKOFFICE = System.Label.CLAPR15SRV42;
    public Final String EVENT = System.Label.CLAPR15SRV43;
    public Final String SOLDTO = System.Label.CLAPR15SRV44;
    Public Final String INSTALLEDAT = System.Label.CLAPR15SRV45;
    Public Final String PARTORDERBFOID = System.Label.CLAPR15SRV46;
    Public Final String BILLTO = System.Label.CLAPR15SRV56;
    Public Final String BILLTOLEGACYID = System.Label.CL16Q1SRV03;
    Public Final String INSTALLEDATCOUNTRY = System.Label.CLMAY15SRV01;
    //Added for BFS release:End
    public Static String Url{get;set;}
    //CLAPR15SRV80,CLAPR15SRV81,CLAPR15SRV82


    public VFC_WorkOrderConnector(ApexPages.StandardController controller) {
      workOrderId = Controller.getID();
      action = apexPages.currentPage().getParameters().get('action'); 
      Url = '';
    }
    
    public PageReference redirectToMW()
    {
        string parameters;
        string FinalCordysURL;
        system.debug('------------------------------------>' + workOrderId + '------' +action );
        workOrder = [select OperationName__c, ServiceItem__c,SVMXC__Service_Contract__c,ServiceLine__c,BackOfficeSystem__c, SoldToAccount__c,SVMXC__Company__c,SVMXC__Company__r.SEAccountID__c, SoldToAccount__r.SEAccountID__c, BillToAccount__c, SVMXC__Order_Status__c, BackOfficeReference__c,CountryOfBackOffice__c,SVMXC__Primary_Territory__r.Plant__r.Name, CustomerTimeZone__c,SVMXC__Company__r.Country__r.CountryCode__c,IsBillable__c,Sales_Order_Number__c, NumberOfPartOrderLinesNOTSynchedWithBO__c, BillToAccountLegacyID__c from SVMXC__Service_Order__c where id=:workOrderId];
        workDetails = [select SVMXC__Line_Type__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c=:workOrderId and TECH_SentToBackOffice__c = false];
        PartOrders = [select id,RecordTypeId,recordtype.name,SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where SVMXC__Service_Order__c=:workOrderId and SVMXC__Order_Status__c=:'Open'];//Added this line by VISHNU C for BR-9101 Q2 Release 2016 
        //INSTALLEDATCOUNTRY = workOrder.SVMXC__Company__r.Country__r.CountryCode__c;
        List<UserRecordAccess> useraccess = [select RecordId, HasEditAccess from UserRecordAccess where RecordId=:workOrderId and UserId=:UserInfo.getUserId()];
        if(useraccess.size()== 0 || (useraccess.size()>0 && !useraccess[0].HasEditAccess))
        hasAccess=false;
        System.debug('\n cLog: '+hasAccess);
        
        // Check that the user has enough rights
        if(!useraccess[0].HasEditAccess){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.CLDEC12SRV22));          
            return null; 
        // Check that the BO systems is set on the WO      
        }else if(workOrder.BackOfficeSystem__c==null){          
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.CLDEC12SRV23));          
            return null; 
        // Check if TZ / Service Contract / Service Item is populated (only for in-flight countries not bFS E2E (SAP brige and Oracle)
        }else if(!(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54 
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV80
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV81
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV82)){
            if(workOrder.CustomerTimeZone__c==null){          
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter the customer timezone.'));          
                return null; 
            }else if(workOrder.ServiceLine__c != null && workOrder.ServiceItem__c == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Service item is mandatory to create the Service Order.'));          
            return null;
            }
        }else{
            if(workOrder.NumberOfPartOrderLinesNOTSynchedWithBO__c > 0 &&  action =='SO.REL'){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'ATP Check is required to Release in BO.'));
                return null; 
            }
        }
         
        /** Yassine.B 17/03/2015 - START **/
        //Check if Plant on the Service Center Territory is mandatory for this Country/BO system before trying to synchronize 
        CS_BOFederationSystems__c cs = CS_BOFederationSystems__c.getValues(workOrder.CountryOfBackOffice__c+'_'+workOrder.BackOfficeSystem__c);
        System.debug('country_bo'+ workOrder.CountryOfBackOffice__c+'_'+workOrder.BackOfficeSystem__c);
        System.debug('cs'+cs);
        if(cs != null && cs.isPlantMandatory__c){
            if(workOrder.SVMXC__Primary_Territory__r.Plant__r.Name == null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The Plant of the Service Center Territory on the WO is mandatory for the synchronization with this specific Country/BO system'));          
                return null;
            }
        }
        /** Yassine.B 17/03/2015 - END **/
        
            
                
        //bFS CREATION case:Start
        
        if (action == Label.CLDEC12SRV19)
        {
         //creation case
         if(workOrder.BackOfficeReference__c == null)
         {  
            if(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV52 
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV80
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV81
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV82)
            {
                if(workOrder.SVMXC__Order_Status__c == 'Service Validated' || workOrder.SVMXC__Order_Status__c == 'Closed' || workOrder.SVMXC__Order_Status__c == 'Cancelled'  || workOrder.SVMXC__Order_Status__c == 'Pending cancellation')
                {          
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Work Order Status must be between "New"  and "Service Completed" to create the Service Order in the Back Office.'));          
                    return null; 
                }
                /**if(workOrder.SoldToAccount__r == null)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Sold To Account is mandatory to create the Service Order.'));
                    return null;             
                }**/
                /*
                if(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV52)
                {
                    if(workOrder.SoldToAccount__r == null)
                    {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Sold To Account is mandatory to create the Service Order.'));
                        return null;             
                    }
                    if(workOrder.SoldToAccount__r == null)
                    {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Sold To Account is mandatory to create the Service Order.'));
                        return null;             
                    }
                    
                }
                if(workOrder.BillToAccount__r == null)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Bill to Account is mandatory to create the Service Order.'));
                    return null;             
                }*/
                parameters = formatURL3(workOrder,Label.CLAPR15SRV53);
            }
            else
            {
                if(workOrder.SVMXC__Order_Status__c != 'New' && workOrder.SVMXC__Order_Status__c != 'Unscheduled' && workOrder.SVMXC__Order_Status__c != 'Scheduled'  ){          
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Work Order Status must be "New" or "Unscheduled" or "Scheduled" to create the Service Order in the Back Office.'));          
                    return null; 
                }else if(workOrder.SoldToAccount__r == null){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Sold To Account is mandatory to create the Service Order.'));
                    return null;             
                }
                parameters = formatURL(workOrder,action);
            }                             
         }
         //update case
         else
         {
            if(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV52 
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV80
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV81
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV82)
            {
                parameters = formatURL3(workOrder,action);
            }
            //only for canada
            else if(workOrder.BackOfficeSystem__c == Label.CLDEC12SRV58)
            {
                //url formating
                //action = Label.CLDEC12SRV19;
                parameters = formatURL(workOrder,action);
            }
            else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLDEC13SRV02));          
                return null;
            }
         }
        }
        //bFS CREATION case:End     
        
        //Action = wo.wd
        
         if(action == Label.CLDEC12SRV20)
         {
            if(workDetails == null || workDetails.size() == 0)
            {          
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, Label.CLDEC12SRV24));          
                return null;      
                // If all OK, redirect to the middleware with all the required parameters   
            }
            else
            {
                //url formating
                parameters = formatURL(workOrder,action);
            }
        }
        
         //October Release 2013 Started
        //if action =  SO.rel        
         
        if (action ==  'SO.REL')
        {
            if(workOrder.BackOfficeReference__c == null)
            {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'BackOffice Reference is mandatory for Released Service Order '));
                    return null;
            }
            else
            {
                Map<String, BridgeSystems__c> bridgeSystemCS = BridgeSystems__c.getAll();
                if(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV52 
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV80
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV81
                    || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV82)
                {
                    parameters = formatURL3(workOrder,Label.CLAPR15SRV55);
                }
                else if(bridgeSystemCS.containsKey(workOrder.BackOfficeSystem__c))
                {
                    if(workOrder.OperationName__c == null)
                    {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Operation Name is mandatory for Released Service Order '));
                        return null;
                    }
                    else
                    {
                        parameters = formatURL2(workOrder,action);
                    }
                }
                else 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ' Service Order Released is not for BackOffice System '));
                    return null;
                }
            }           
            /*
            else
            {
                if(workOrder.BackOfficeReference__c == null)
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'BackOffice Reference is mandatory for Released Service Order '));
                if(workOrder.OperationName__c == null)
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Operation Name is mandatory for Released Service Order '));
                return null;
            }*/         
         }
         //October Release 2013 End
       
        //Added for Cordys Redirection mapping for both existing and E2E connector
        if(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV52 
        || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54
        || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV80
        || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV81
        || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV82)
        {
            FinalCordysURL = System.Label.CLAPR15SRV38;
        }
        else
        {
            FinalCordysURL = System.Label.CLDEC12SRV15;
        }
        //End:Added for Cordys Redirection mapping for both existing and E2E connector
        
        //PageReference pg=new PageReference(Label.CLDEC12SRV15+'?'+parameters);//Commented for Cordys Redirection mapping for both existing and E2E connector
        PageReference pg=new PageReference(FinalCordysURL+'?'+parameters);
        //Added below line BY VISHNU C for Q2 2016 Release BR-9101
        //if (action ==  'SO.REL')
        //{
        workOrder.RequestForRelease__c = System.now();
        workOrder.Tech_RequestForRelease__c = System.now();
        update workOrder;
        if(PartOrders.size() > 0 )
        {
            for(SVMXC__RMA_Shipment_Order__c po: PartOrders)
            {
                if (workOrder.SVMXC__Order_Status__c != 'Service Complete' && po.recordtype.name == 'Shipment' )
                {
                    po.RequestForRelease__c = System.now();
                    //po.SVMXC__Order_Status__c = 'Pending Release in BO';
                
                }
                if (workOrder.SVMXC__Order_Status__c == 'Service Complete' && po.recordtype.name == 'RMA' )
                {
                    po.RequestForRelease__c = System.now();
                    //po.SVMXC__Order_Status__c = 'Pending Release in BO';
                }
            }
        }
            
        //}
        
        //Changes by VISHNU C Ends here
        //System.debug('Full URL'+ Label.CLDEC12SRV15+'?'+parameters);//Commented for Cordys Redirection mapping for both existing and E2E connector
        System.debug('Full URL'+ FinalCordysURL+'?'+parameters);
        
        pg.setRedirect(true);
        return pg;
          
        
        //return null;
    }
    
    
    
    public static string formatURL(SVMXC__Service_Order__c workOrder,string action){
        String parameters = Label.CLDEC12SRV16+'='+UserInfo.getSessionId();
        
        //CLDEC12SRV20 : WO.WD
        //CLDEC12SRV19 : WO.SYNC
        //CLDEC12SRV62 : WO.UPD
        if(action != Label.CLDEC12SRV20){
            if(workOrder.BackOfficeReference__c == null){
                action = Label.CLDEC12SRV19;
            }else{
                action = Label.CLDEC12SRV62;
            }
        }
        
        // Action
        parameters += '&'+Label.CLDEC12SRV17+'='+action;
        // Work Order Id
        parameters += '&'+Label.CLDEC12SRV18+'='+workOrder.id;
        // SE Account Id
        parameters += '&'+Label.CLDEC12SRV25+'='+workOrder.SVMXC__Company__r.SEAccountID__c;
        // SE Sold To Account Id
        // pas pour le ca
        if(workOrder.BackOfficeSystem__c != Label.CLDEC12SRV58){
          parameters += '&'+Label.CLDEC12SRV52+'='+workOrder.SoldToAccount__r.SEAccountID__c;
        }
        // SFDC server URL
        parameters += '&'+Label.CLDEC12SRV35+'='+System.URL.getSalesforceBaseURL().getHost();
        // Publisher ID
        parameters += '&'+Label.CLDEC12SRV36+'='+workOrder.BackOfficeSystem__c;
        Url = parameters;
        System.debug('****'+Url );
        return parameters;
    }
    public static string formatURL2(SVMXC__Service_Order__c workOrder,string action){
        String parameters = Label.CLDEC12SRV16+'='+UserInfo.getSessionId();        
        System.debug('\n cLog : '+action);
        // Action
        parameters += '&'+Label.CLDEC12SRV17+'='+action;
        // Work Order Id
        parameters += '&'+Label.CLDEC12SRV18+'='+workOrder.id;
        // SE Account Id
        parameters += '&'+Label.CLDEC12SRV25+'='+workOrder.SVMXC__Company__r.SEAccountID__c;
        // SE Sold To Account Id
        // pas pour le ca
        if(workOrder.BackOfficeSystem__c != Label.CLDEC12SRV58){
          parameters += '&'+Label.CLDEC12SRV52+'='+workOrder.SoldToAccount__r.SEAccountID__c;
        }
        // SFDC server URL
        parameters += '&'+Label.CLDEC12SRV35+'='+System.URL.getSalesforceBaseURL().getHost();
        // Publisher ID
        parameters += '&'+Label.CLDEC12SRV36+'='+workOrder.BackOfficeSystem__c;
        return parameters;
    }
    
    public  string formatURL3(SVMXC__Service_Order__c workOrder,string action)
    {
        string parameters = ''; 
        //parameters +=  CORDYSURL +'?';
        //Session ID
        parameters += Label.CLDEC12SRV16+'='+UserInfo.getSessionId();  
        
        //BO ref
        parameters += '&'+BACKOFFICEREFERENCE +'='+workorder.BackOfficeReference__c;
        
        //BO system
        //parameters += '&'+BACKOFFICESYSTEM +'='+workorder.BackOfficeSystem__c; 
        parameters += '&'+ Label.CLDEC12SRV36 +'='+workorder.BackOfficeSystem__c; 
        
        //BO country 
        parameters += '&'+COUNTRYOFBACKOFFICE +'='+workorder.CountryOfBackOffice__c;   
        
        //Event {WO.CREATION or WO.SYNC or WO.RELEASE}
        //parameters += '&'+EVENT +'='+action;
        parameters += '&'+Label.CLDEC12SRV17 +'='+action;
        
        //WO bFO Id
        //parameters += '&'+WOBFOID +'='+workorder.id;    
        parameters += '&'+Label.CLDEC12SRV18 +'='+workorder.id;
                
        // SFDC server URL//BackOfficeReference__c
        parameters += '&'+Label.CLDEC12SRV35+'='+System.URL.getSalesforceBaseURL().getHost();
        
        if(action == Label.CLAPR15SRV53 || action == Label.CLDEC12SRV19)
        {
            if(workOrder.BackOfficeSystem__c == Label.CLAPR15SRV80
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV81
                || workOrder.BackOfficeSystem__c == Label.CLAPR15SRV82)
            {
                parameters += '&'+SOLDTO +'='+workorder.SoldToAccount__c;
                //parameters += '&'+Label.CLDEC12SRV52+'='+workorder.SoldToAccount__r.SEAccountID__c;
                parameters += '&'+INSTALLEDAT+ + '=' +workorder.SVMXC__Company__c;
                //parameters += '&'+Label.CLDEC12SRV25+'='+workorder.SVMXC__Company__r.SEAccountID__c;
            } 
           if(workOrder.Sales_Order_Number__c == null && workOrder.SVMXC__Service_Contract__c == null && workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54){
                parameters += '&'+INSTALLEDATCOUNTRY+'='+workOrder.SVMXC__Company__r.Country__r.CountryCode__c;
           }

           if(workorder.IsBillable__c == 'Yes' && workOrder.BackOfficeSystem__c == Label.CLAPR15SRV54){
              parameters += '&'+BILLTOLEGACYID + '='+workorder.BillToAccountLegacyID__c;
              parameters += '&'+BILLTO +'='+workorder.BillToAccount__c;
              
           }
        }
        System.debug('parameters: '+parameters );
    
        return parameters ;  
    }
    

}