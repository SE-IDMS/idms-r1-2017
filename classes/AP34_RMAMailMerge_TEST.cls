@isTest
private class AP34_RMAMailMerge_TEST
{
    static testMethod void testAP34() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;

        Country__c country = Utils_TestMethods.createCountry();
        insert country;
 
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
        
        RMA__c RMA1 = new RMA__c();
        RMA1.case__c = Case1.Id;
        RMA1.AccountName__c = account1.Id;

        //Account Address
        RMA1.AccountStreet__c  = 'Account Street';
        RMA1.AccountCity__c    = 'Account City';
        RMA1.AccountZipCode__c = '12345';
        RMA1.AccountCountry__c = country.Id;
        RMA1.AccountPOBox__c   = 'Account PO Box';

        //Shipping Address
        RMA1.ShippingStreet__c  = 'Shipping Street';
        RMA1.ShippingCity__c    = 'Shipping City';
        RMA1.ShippingZipCode__c = '67890';
        RMA1.ShippingCountry__c = country.Id;
        RMA1.ShippingPOBox__c   = 'Shipping PO Box';

        insert RMA1;
        
        List<RMA_Product__c> returnedProductsList = new List<RMA_Product__c>();
       
        RMA_Product__c returnedProducts1 = new RMA_Product__c();
        ReturnedProducts1.RMA__c = RMA1.ID;
        returnedProductsList.add(returnedProducts1);
        insert returnedProducts1;

        RMA_Product__c returnedProducts2 = new RMA_Product__c();
        ReturnedProducts2.RMA__c = RMA1.ID;
        returnedProductsList.add(returnedProducts2);
        insert returnedProducts2;
        
        Integer CRNumber = 1;
        
        returnedProducts1.Name = 'CR1';
        update returnedProducts1;
        
        returnedProducts2.Name = 'CR2';
        update returnedProducts2;

        delete returnedProducts1;
        delete returnedProducts2;
    }
}