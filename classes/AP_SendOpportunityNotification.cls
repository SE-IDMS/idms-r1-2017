public class AP_SendOpportunityNotification
{
/***************************************************************************
For October 2013 Release
When the status of an Opportunity is updated  send email to partner and 
include the Opportunity Team on the cc: line of the email.

**************************************************************************/
    public static void notifyOppLeader_OppTeam(list<Opportunity> lstOpportunity ,Id templateId)
    {
        try
        {
            list<OpportunityTeamMember> lstOppTeam = new list<OpportunityTeamMember>();
            map<Id, list<opportunityteamMember>> mapOppTeam = new map<Id, list<opportunityteamMember>>();
            map<Id,Id> mapOppOwner = new map<Id,Id>();
            lstOpportunity = [Select id, ownerid,owner.email,owner.usertype,Owner.ProfileId from Opportunity where id in:lstOpportunity limit 50000];
            lstOppTeam = [SELECT Id,OpportunityId,UserId,User.Email,User.UserType,User.ProfileId,Opportunity.OwnerId,
                            Opportunity.Owner.ProfileId,Opportunity.Owner.Email,Opportunity.Owner.UserType  FROM OpportunityTeamMember 
                            WHERE OpportunityId in : lstOpportunity limit 1000];
            system.debug('lstOppTeam :'+lstOppTeam[0].User.ProfileId);
            for(opportunityteamMember oppTeam : lstOppTeam)
            {
                if(!mapOppOwner.containsKey(oppTeam.OpportunityId))
                    mapOppOwner.put(oppTeam.OpportunityId,oppTeam.Opportunity.OwnerId);
                if(!mapOppTeam.containsKey(oppTeam.opportunityId))
                    mapOppTeam.put(oppTeam.opportunityId , new list<opportunityteamMember> {(oppTeam)});
                else
                    mapOppTeam.get(oppTeam.opportunityId).add(oppTeam);
            }
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            list<String> ccOwnerAddresses = new list<String>(); 
            ID partnerUserId;
            Boolean isOwnerPartner = false;
            for(Opportunity opp : lstOpportunity)
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if(!mapOppTeam.isEmpty())
                {
                    if(mapOppTeam.containsKey(opp.id))
                    { 
                        
                        list<String> ccAddresses = new list<String>();
                        //list<String> toAddresses = new list<String>(); 
                        for(opportunityteamMember teamMem : mapOppTeam.get(opp.id))
                        {
                            
                            if(teamMem.User.UserType == 'PowerPartner' && Label.CLOCT13PRM82.contains(teamMem.User.ProfileId))
                                partnerUserId = teamMem.UserId;
                            
                            else
                                ccAddresses.add(teamMem.User.Email);
                                                                
                        }
                        if(partnerUserId == null && opp.Owner.UserType == 'PowerPartner' && Label.CLOCT13PRM82.contains(opp.Owner.ProfileId))
                            partnerUserId= opp.ownerID;
                        else if(opp.Owner.UserType == 'PowerPartner' && (Label.CLOCT13PRM82.contains(opp.Owner.ProfileId)))
                            ccAddresses.add(opp.owner.Email);
                        else if(!(opp.Owner.UserType == 'PowerPartner'))
                            ccAddresses.add(opp.owner.Email);
                        //mail.setToAddresses(toAddresses);
                        system.debug('ccAddresses:'+ccAddresses);
                        mail.setCCAddresses(ccAddresses);
                        
                    }
                }
                if(partnerUserId == null && opp.Owner.UserType == 'PowerPartner' && Label.CLOCT13PRM82.contains(opp.Owner.ProfileId))
                    partnerUserId= opp.ownerID;
              
                system.debug('partnerUserId:'+partnerUserId);
                mail.settargetObjectId(partnerUserId); 
               
                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                mail.settemplateId(templateID);
                mail.setwhatId(opp.Id);
                mail.setSaveAsActivity(false);
                lstMail.add(mail);
            }
            
            if(!lstMail.isEmpty())
                Messaging.sendEmail(lstMail);
        }
        catch(Exception e)
        {
            system.debug('Exception AP_SendOpportunityNotification:Error in sending email to Opportunity Team:'+e.getMessage());
        }
    }
    
    public static void setEmailsonAmountChange(list<Opportunity> lstOpportunity)
    {
        notifyOppLeader_OppTeam(lstOpportunity , Label.CLOCT13PRM76);
    }
    
    public static void setEmailsonStatusChange(list<Opportunity> lstOpportunity)
    {
        notifyOppLeader_OppTeam(lstOpportunity , Label.CLOCT13PRM44);
    }
}