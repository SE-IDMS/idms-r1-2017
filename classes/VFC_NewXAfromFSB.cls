// Added by Uttara - Q2 2016 Release - Notes to bFO 

public class VFC_NewXAfromFSB {
    
    public String fsbId {get; set;}
    public FieldServiceBulletin__c fsb {get; set;}
    
    public VFC_NewXAfromFSB (ApexPages.StandardSetController controller) {
        
        fsbId = ApexPages.currentPage().getParameters().get('id');
        System.debug('!!!! fsbId : ' + fsbId );
    }
    
    public PageReference goToEditPage() {
    
        System.debug('!!!! Enter goToEditPage');
        
        fsb = [SELECT Problem__c, Name, Problem__r.Name FROM FieldServiceBulletin__c WHERE Id =: fsbId];
        
        PageReference XAeditPage = new PageReference('/'+SObjectType.ContainmentAction__c.getKeyPrefix()+'/e?');
        
        XAeditPage.getParameters().put('ent', Label.CLQ216I2P08); // setting the ent with XA object Id
        XAeditPage.getParameters().put(Label.CL00690, Label.CL00691); // setting nooverride = 1 
        
        XAeditPage.getParameters().put(Label.CLQ216I2P09, fsb.Name); // setting the FSB lookup field
        XAeditPage.getParameters().put(Label.CLQ216I2P10, fsb.Id); // setting the FSB lookup field
        
        XAeditPage.getParameters().put(Label.CLQ216I2P11, fsb.Problem__r.Name); // setting the Problem lookup field
        XAeditPage.getParameters().put(Label.CLQ216I2P12, fsb.Problem__c); // setting the Problem lookup field
        
        XAeditPage.getParameters().put(Label.CLQ216I2P13, Label.CLQ216I2P14); // setting the Containment Action Type field
        
        XAeditPage.getParameters().put('RecordType', Label.CLQ216I2P15); // setting Customer XA record type
               
        XAeditPage.getParameters().put(Label.CL00330, fsbId); // setting the retURL
        
        return XAeditPage;
    }
}