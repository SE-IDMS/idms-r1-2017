/*
------------------------------
   Author : A.D.N.V.SATYANARAYANA
   Description : Test class for Batch_PRM_UpdateAccountPLBrands_Test
   Created Date : 27-06-2016
------------------------------   */

@isTest
public class Batch_PRM_UpdateAccountPLBrands_Test{
    
  static testMethod void case1(){
  
      Country__c testCountry = new Country__c();
                testCountry.Name   = 'India';
                testCountry.CountryCode__c = 'IN';
                testCountry.InternationalPhoneCode__c = '91';
                testCountry.Region__c = 'APAC';
            INSERT testCountry;
            

      PRMCountry__c prmCountry = new PRMCountry__c();
                prmCountry.Name = 'Test pagecontrol';
                prmCountry.CountryPortalEnabled__c = true;
                prmCountry.Country__c = testCountry.Id;
                prmCountry.PLDatapool__c = 'pagecontrol';
            INSERT prmcountry;
            
      User us = new user(id = userinfo.getuserId());
                us.BypassVR__c = true;
            update us;
            
      System.RunAs(us){

           FieloEE.MockUpFactory.setCustomProperties(false);          
                
           Account acc = new Account();
               acc.Name = 'test acc';
               insert acc;
                
           Contact con = new Contact();
                con.AccountId = acc.Id;
                con.FirstName = 'test contact Fn';
                con.LastName  = 'test contact Ln';
                con.PRMCompanyInfoSkippedAtReg__c = false;
                con.PRMCountry__c = testCountry.Id;
                con.PRMFirstName__c = 'PRM test contact Fn'; 
                con.PRMLastName__c  = 'PRM test contact Fn';
                insert con;
                
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
                if(profiles.size()>0){
                    UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                    User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                    u.ContactId = con.Id;
                    u.PRMRegistrationCompleted__c = false;
                    u.BypassWF__c = false;
                    u.BypassVR__c = true;
                    u.FirstName ='Test User First';
                    insert u;
                }
            
                
             FieloEE__Member__c member = new FieloEE__Member__c(
                    FieloEE__LastName__c = 'Polo',
                    FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                    FieloEE__Street__c = 'Calle Falsa',
                    F_PRM_PrimaryChannel__c = 'TST',F_Country__c= testCountry.Id,
                    F_Account__c=acc.Id
                );
                    insert member;
                System.debug('Member123 :'+member.Id);
                
              FieloEE.MemberUtil.setMemberId(member.Id);
                
              PartnerProgram__c ppm=new PartnerProgram__c();
                    ppm.name='APC Value Added Tester';
                    ppm.ProgramType__c='Business';
                    insert ppm;

              ProgramLevel__c pml = new ProgramLevel__c();
                    pml.Description__c='This is a Test Description';
                    pml.ModifiablebyCountry__c='Yes';
                    pml.Name='TestRegistration';
                    pml.PartnerProgram__c=ppm.Id;
                    insert pml;
                    
              FieloEE__Badge__c feb = new FieloEE__Badge__c();
                    feb.Name='Tester';
                    feb.F_PRM_TypeSelection__c='ProgramLevel';
                    feb.F_PRM_ProgramLevel__c=pml.Id;
                    insert feb;

               FieloEE__BadgeMember__c febm =new FieloEE__BadgeMember__c();
                    febm.Name='TestIvan';
                    febm.FieloEE__Badge2__c= feb.Id;
                    febm.FieloEE__Member2__c= member.id;
                    insert febm;

                Brand__c brd = new Brand__c();
                    brd.Name='APC';
                    insert brd;

                ProgramLevelBrand__c  plb =new ProgramLevelBrand__c ();
                    plb.ProgramLevel__c = pml.Id;
                    plb.Brand__c = brd.Id;
                    insert plb;   
       
           Test.startTest();
                Integer numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
                if(numBatchJobs<5){
                    Batch_PRM_UpdateAccountPLBrands sh1 = new Batch_PRM_UpdateAccountPLBrands ();            
                    DateTime currentDate = System.Now().addHours(5)  ;
                    String CRON_EXPRESSION = '' + 59 + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' 
                                  + currentDate.day() + ' ' + currentDate.month() + ' ? ' + currentDate.year();        
                    String jobId = System.schedule('Product2 update', CRON_EXPRESSION, sh1);
                }
           Test.stopTest();        

                
                
            }
        }
 
}