public with sharing class ServiceMaxWOAutoAssignment {
    
    @future
    public static void autoAssignTechnician(Map<Id, Id> woAssignmentMap)
    {
        List<SVMXC__Service_Order__c> woList = [SELECT Id, SVMXC__Group_Member__c, SVMXC__Service_Group__c,SVMXC__Group_Email__c, OwnerId FROM SVMXC__Service_Order__c WHERE Id IN :woAssignmentMap.keySet() ];
        Map<Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c>([SELECT Id, SVMXC__Service_Group__c, SVMXC__Email__c, SVMXC__Salesforce_User__c FROM SVMXC__Service_Group_Members__c WHERE Id IN :woAssignmentMap.values()]);
        
        for (SVMXC__Service_Order__c wo : woList)
        {
            SVMXC__Service_Group_Members__c tech = techMap.get(woAssignmentMap.get(wo.Id));
            wo.SVMXC__Group_Member__c = tech.Id;
            wo.SVMXC__Service_Group__c = tech.SVMXC__Service_Group__c;
            wo.SVMXC__Group_Email__c = tech.SVMXC__Email__c;
            wo.OwnerId = tech.SVMXC__Salesforce_User__c;
            wo.SVMXC__Order_Status__c = 'Unscheduled';
        }
        
        update woList;
    }

}