@isTest
private class AP37_ChatterForCase_TEST
{
    static testMethod void TEST_AP37() 
    {
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('TestUser');
        database.insert(user1);
            
        Country__c accountCountry = new Country__c();
        accountCountry.CountryCode__c = 'ZZ';
        insert accountCountry;
        
        StateProvince__c accountStateProvince = new StateProvince__c();
        accountStateProvince.Country__c = accountCountry.ID;
        accountStateProvince.CountryCode__c = 'ZZ';
        accountStateProvince.StateProvinceCode__c = 'WW';
        insert accountStateProvince;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.StateProvince__c = accountStateProvince.ID;
        account1.Country__c = accountCountry.ID;
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
        
        Case1.OwnerID = user1.ID;
        update Case1;
    }
}