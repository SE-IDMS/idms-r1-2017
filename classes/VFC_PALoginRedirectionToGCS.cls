global class VFC_PALoginRedirectionToGCS{

    global PageReference forwardToCustomAuthPage() {
        PageReference GcsPage = new PageReference(Label.CLQ316PA044);
        GcsPage.setRedirect(true);
        return GcsPage;
    }
    
}