/*
    Author          : Abhishek Yadav (ACN) 
    Date Created    : 18/05/2011
    Description     : Class utilised by Synchronize/View Quote Link Button.
    Modification Hisory: modifed by Siddhart
*/

public class VFC30_SynchronizeQuoteLink {
    public String quoteLinkID;
    public String userID;
    public OPP_QuoteLink__c quoteLink;
    Public Boolean flag{get;set;}
    public List<String> newuser;
    public String QuoteURL;
    public boolean closeWindow{get;set;}{closeWindow=false;}
        
        public VFC30_SynchronizeQuoteLink(ApexPages.StandardController controller) {
            quoteLinkID=Controller.getID();         
            }
            
        public pageReference redirectToQuoteLinkExternalSystem()
        {
        System.Debug('******redirectToQuoteLinkExternalSystem method in VFC30_SynchronizeQuoteLink Class Start****');    
            if(quoteLinkID!=null)
            {
                quoteLink = [select name,quotestatus__c,TECH_AuthorizationURLDomain__c,SynchronizationMessage__c,TECH_ApiPartnerServerURL__c,CountryOfQuoting__c,TECH_IsSyncNeededForOpp__c,TECH_IsBoundToBackOffice__c,QuotingSystem__c,opportunityName__c,QuoteRefNumberinLegacy__c,opportunityName__r.Name from OPP_QuoteLink__c where ID=:quoteLinkID];                    
            }            
            if(quoteLink.QuotingSystem__c==null)
            {
                flag=true;       
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CL00400));          
                return null;    
            }
            else
            {   
                //Added for BlueZebra and BFS BR-7311 April 2015 release -START
                if(quotelink.QuotingSystem__c==System.Label.CLAPR15SLS82) //ITB_SB
                {
                    QuoteURL =System.Label.CLAPR15SLS90+Label.CLAPR15SLS76+UserInfo.getSessionId()+Label.CLAPR15SLS77+quoteLinkID+Label.CLAPR15SLS78+UserInfo.getUserId()+Label.CLAPR15SLS79+System.URL.getSalesforceBaseURL().getHost()+Label.CLAPR15SLS80+quotelink.QuotingSystem__c;
                    System.debug('***>>>>>>>QuoteURL >>>>>****'+QuoteURL );
                    return new PageReference(QuoteURL);
                } 
                else if(quotelink.QuotingSystem__c==System.Label.CLAPR15SLS63) //Eurotherm_BZ
                {
                    QuoteURL =System.Label.CLAPR15SLS60+Label.CLAPR15SLS76+UserInfo.getSessionId()+Label.CLAPR15SLS77+quoteLinkID+Label.CLAPR15SLS78+UserInfo.getUserId()+Label.CLAPR15SLS79+System.URL.getSalesforceBaseURL().getHost()+Label.CLAPR15SLS80+quotelink.QuotingSystem__c;
                    System.debug('***>>>>>>>QuoteURL >>>>>****'+QuoteURL );
                    return new PageReference(QuoteURL);
                } 
                //Added for BlueZebra and BFS BR-7311 April 2015 release -END 
                //Added for Oracle CPQ APril 2015 release - START
                 else if(quoteLink.QuotingSystem__c==System.Label.CLAPR15SLS86)
                {
                    flag=true;       
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLAPR15SLS87));          
                    return null;    
                }
                //Added for Oracle CPQ APril 2015 release - END      
                else if(quotelink.QuotingSystem__c==System.Label.CLJUN13SLS04)
                {
                    if(quoteLink.QuoteRefNumberinLegacy__c!=null){
                    
                        if(Label.CLDEC13SLS44 =='false')
                        {
//                          QuoteURL = System.Label.CLJUN13SLS05+UserInfo.getUserId()+'&apiPartnerURL='+quoteLink.TECH_ApiPartnerServerURL__c+'&apiSessionID='+UserInfo.getSessionId()+System.Label.CLJUN13SLS06+ quoteLink.OpportunityName__c+ '&OwnerId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(0,4))+ '&QuoteId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(4,8));                        
                            QuoteURL = System.Label.CLJUN13SLS05+UserInfo.getUserId()+'&apiPartnerURL='+quoteLink.TECH_ApiPartnerServerURL__c+'&apiSessionID='+UserInfo.getSessionId()+System.Label.CLJUN13SLS06+ quoteLink.OpportunityName__c+ '&OwnerId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(0,4))+ '&QuoteId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(4,8))+'&BFO_Quote_Link_Number='+quoteLink.Name;
                        }
                        else if(Label.CLDEC13SLS44 =='true')
                        {
//                          QuoteURL = System.Label.CLJUN13SLS05+UserInfo.getUserId()+'&apiPartnerURL='+quoteLink.TECH_ApiPartnerServerURL__c+'&apiSessionID='+UserInfo.getSessionId()+System.Label.CLJUN13SLS06+ quoteLink.OpportunityName__c+ '&OwnerId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(0,4))+ '&QuoteId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(4,8));                        
                            QuoteURL = System.Label.CLJUN13SLS05+UserInfo.getUserId()+'&apiPartnerURL='+quoteLink.TECH_ApiPartnerServerURL__c+'&apiSessionID='+UserInfo.getSessionId()+System.Label.CLJUN13SLS06+ quoteLink.OpportunityName__c+ '&OwnerId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(0,4))+ '&QuoteId=' + Integer.valueOf(quoteLink.QuoteRefNumberinLegacy__c.substring(4,8))+'&BFO_Quote_Link_Number='+quoteLink.Name+'&AuthorizationURLDomain='+quoteLink.TECH_AuthorizationURLDomain__c;
                        }
                        return new PageReference(QuoteURL);
                    }
                    else//is in create mode
                    {                             
                        if(Label.CLDEC13SLS44 =='false')
                        {
                            QuoteURL = System.Label.CLJUN13SLS05+UserInfo.getUserId()+'&apiPartnerURL='+quoteLink.TECH_ApiPartnerServerURL__c+'&apiSessionID='+UserInfo.getSessionId()+System.Label.CLNOV13SLS10+ quoteLink.OpportunityName__c+'&QuoteId='+quoteLink.Id+'&BFO_Quote_Link_Number='+quoteLink.Name;
                        }
                        else if(Label.CLDEC13SLS44 =='true')
                        {
                            QuoteURL = System.Label.CLJUN13SLS05+UserInfo.getUserId()+'&apiPartnerURL='+quoteLink.TECH_ApiPartnerServerURL__c+'&apiSessionID='+UserInfo.getSessionId()+System.Label.CLNOV13SLS10+ quoteLink.OpportunityName__c+'&QuoteId='+quoteLink.Id+'&BFO_Quote_Link_Number='+quoteLink.Name+'&AuthorizationURLDomain='+quoteLink.TECH_AuthorizationURLDomain__c;
                        }
                        System.debug('QuoteURL for new '+QuoteURL);
                        return new PageReference(QuoteURL);
                    }                     
                }
                else
                {
                    userID=[Select UserName from user where ID=:UserInfo.getUserID()].userName;                    
                    if(userID!=null)
                    {
                    newuser=UserID.split('@');           
                    }
                    QuoteURL = (System.label.CL00343+'?UserId='+newuser[0]+'&QuoteLinkID='+quoteLink.Id+'&OpportunityID='+quoteLink.OpportunityName__c+'&QuoteIDInTheBackOffice='+quoteLink.QuoteRefNumberinLegacy__c+'&countryOfQuoting='+quoteLink.CountryOfQuoting__c+'&quotingSystem='+quoteLink.QuotingSystem__c+'&bfoSessionId='+ UserInfo.getSessionId() + '&bfoServerUrl=' + Label.CL00597);
                    if(quotelink.QuoteStatus__c.equalsIgnoreCase(system.label.CL00397))
                    { 
                       if(quotelink.CountryOfQuoting__c!=null && quotelink.QuotingSystem__c!=null){
                            if(quotelink.TECH_IsSyncNeededForOpp__c)
                            {                        
                                quotelink.TECH_IsSyncNeededForOpp__c=false;
                            }                                                             
                            quotelink.SynchronizationMessage__c=Label.CL00401+' '+quoteLink.QuotingSystem__c+' '+Label.CL00402+' '+system.now()+' '+Label.CL00403;                    
                            //Modif JFA 02/05/2012: add a try catch
                            try{
                                Database.update(quotelink);
                            }
                            catch (Exception e){
                                system.debug('error when updating Quote: '+ e);
                                return null;
                                //error is catch and displayed on the VFPage
                            }
                        }
                    System.debug('###>>>>>>>QuoteURL >>>>>###'+QuoteURL );    
                    return new pageReference(QuoteURL+'&view=No');
                    }
                    else
                    {
                        if(!quoteLink.TECH_IsBoundToBackOffice__c)
                        {
                            flag=true;       
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CL00404));          
                            return null;
                        }
                        else
                        {                        
                            System.Debug('******redirectToQuoteLinkExternalSystem method in VFC30_SynchronizeQuoteLink Class End****');
                            return new pageReference(QuoteURL+'&view=Yes');
                        }
                    }
                }
                
            }     
       
        }   


        //private methods for webcpq implementation
        /*
        Author:Siddharth N(GD Solutions)
        Purpose:To call the CPQ webservice which would return a xml response which will have the Quotation Number.
        Release:June 2013 release(BR-3183)
        */
        /*
        private void handleCPQinCreateMode()
        {

            WS_WebCPQ.CpqApiSoap webCPQinstance=new WS_WebCPQ.CpqApiSoap();
            webCPQinstance.timeout_x = 90000; 
         // String resultXMLString=webCPQinstance.NewQuoteFromSF('schneiderelectric',UserInfo.getSessionId(),'<?$Api.Partner_Server_URL_160 ?>','<?xml version="1.0" encoding="utf-8"?><Cart PREVENT_EMPTY_QUOTE="0"><MarketCode>USD</MarketCode><ShippingMethod></ShippingMethod><ShippingPrice></ShippingPrice><TaxExempt></TaxExempt><CartComment></CartComment><PromoCode></PromoCode><Crm ApplyMappings="1"><OpportunityId>'+quotelink.opportunityName__c+'</OpportunityId><OpportunityName>'+quoteLink.opportunityName__r.Name+'</OpportunityName></Crm><Items></Items><Properties><Property><Name>BFO_Quote_Link_Number</Name><Value>'+quoteLink.Name+'</Value></Property></Properties><Customers></Customers></Cart>');                    
//           String resultXMLString=webCPQinstance.NewQuoteFromSF('schneiderelectric',UserInfo.getSessionId(),quoteLink.TECH_ApiPartnerServerURL__c,'<?xml version="1.0" encoding="utf-8"?><Cart PREVENT_EMPTY_QUOTE="0"><MarketCode>USD</MarketCode><ShippingMethod></ShippingMethod><ShippingPrice></ShippingPrice><TaxExempt></TaxExempt><CartComment></CartComment><PromoCode></PromoCode><Crm ApplyMappings="1"><OpportunityId>'+quotelink.opportunityName__c+'</OpportunityId></Crm><Items></Items><Properties><Property><Name>BFO_Quote_Link_Number</Name><Value>'+quoteLink.Name+'</Value></Property></Properties><Customers></Customers></Cart>');                    
            String resultXMLString=webCPQinstance.NewQuoteFromSF('schneiderelectric',UserInfo.getSessionId(),quoteLink.TECH_ApiPartnerServerURL__c,'<?xml version="1.0" encoding="utf-8"?><Cart PREVENT_EMPTY_QUOTE="0"><MarketCode>USD</MarketCode><ShippingMethod/><ShippingPrice/><TaxExempt/><CartComment/><PromoCode/><Crm ApplyMappings="1"><OpportunityId>'+quotelink.opportunityName__c+'</OpportunityId></Crm><Items/><Properties><Property><Name>BFO_Quote_Link_Number</Name><Value>'+quoteLink.Name+'</Value></Property></Properties><Customers/><SFUserId>'+UserInfo.getUserId()+'</SFUserId><SFUserEmail>'+UserInfo.getUserEmail()+'</SFUserEmail><SFUserName>'+UserInfo.getName()+'</SFUserName></Cart>');                    
            Map<String,String> xmlMap=generateXMLMap(resultXMLString);
            System.debug('xmlMap'+xmlMap);
            if(xmlMap.containsKey('Status') && xmlMap.get('Status').equalsIgnoreCase('OK')) {
                quoteLink.QuoteRefNumberinLegacy__c=xmlMap.get('QuotationNumber');   
                quoteLink.TECH_IsBoundToBackOffice__c=true;
                if(quotelink.QuoteStatus__c.equalsIgnoreCase(system.label.CL00397))                    
                       if(quotelink.CountryOfQuoting__c!=null && quotelink.QuotingSystem__c!=null)
                            if(quotelink.TECH_IsSyncNeededForOpp__c)                                                    
                                quotelink.TECH_IsSyncNeededForOpp__c=false;                              
                try{
                  Database.update(quotelink);
                  closeWindow=true;                                    
                }
                catch (Exception ex){
                    flag=true;       
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));                          
                    //error is catch and displayed on the VFPage
                }                
              //  quotelink.BOLink__c=xmlMap.get('
            }
            else
            { 
                flag=true;       
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,xmlMap.get('Message')+'-'+xmlMap.get('Description')));                          
            }            
        }
*/
       
        /*
        Author:Siddharth N(GD Solutions)
        Purpose:This method takes in the xml string and creates a map of the required details and their respective values.
        Release:June 2013 release(BR-3183)
        */
       /*Commented for bFS Interim Quote connector to fix code coverage since this method is not been used.
        private Map<String,String> generateXMLMap(String xmlString)
        {

            List<String> keys=new List<String>();
            List<String> values=new List<String>();
            Map<String,String> xmlMap=new Map<String,String>();

            XmlStreamReader reader= new XmlStreamReader(xmlString);
            while(reader.hasNext()) {   
                if(reader.hasName() && reader.getEventType()!=XmlTag.START_ELEMENT)
                    keys.add(reader.getLocalName());
                if(reader.hasText())
                    values.add(reader.getText());   
                reader.next();
            }

            for(Integer i=0;i<values.size();i++)
                xmlMap.put(keys[i],values[i]);
            return xmlMap;
        }   */                         
 
}