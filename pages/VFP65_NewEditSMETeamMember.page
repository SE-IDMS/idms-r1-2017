<!-- PAGE VFP65_NEWEDITSMETEAMMEMBER  ALLOWS SME TEAM MEMBERS TO CREATE/EDIT SME TEAM MEMBER 
     DURING QUOTE LINK APPROVAL PROCESS -->    
<!-- CREATED BY ACN -->
<!-- SCHNEIDER DECEMBER RELEASE - CREATED ON - 24/10/2011 -->
<!-- Modified for April 2015 Release for BR-4488 and BR-4489 --->

<apex:page standardcontroller="RIT_SMETeam__c" extensions="VFC65_NewEditSMETeamMember" action="{!checkQuoteLinkStatus}">
    <apex:styleSheet value="{!$Resource.STR015_StyleSheet}"/>    
    <apex:includeScript value="{!$Resource.STR019_JavaScripts}"/>
     <div id='loading' class='divstyleCreateSeries'>
        <apex:image value="{!$Resource.STR014_ProgressBar}" style="display:block;margin:0 auto;text-align:center;margin-top:300px;"/>
    </div>

    <apex:form >
        <!-- The following hidden variables are used to avoid the query -->
        <apex:inputhidden value="{!RIT_SMETeam__c.QuoteLink__r.QuoteStatus__c}"/>
        <apex:inputhidden value="{!RIT_SMETeam__c.QuoteLink__r.OwnerId}"/>        
        <apex:sectionHeader title="{!$ObjectType.RIT_SMETeam__c.label} {!$Label.CL00203}" subtitle="{!IF(RIT_SMETeam__c.Id=null,$Label.CL00554+" "+$ObjectType.RIT_SMETeam__c.label,RIT_SMETeam__c.Name)}" rendered="{!IF(OR(hasEditAccess,isAdmin,isSME),true,false)}"/>

        <apex:pageMessage summary="{!pgeMsg}" severity="{!IF(renderMembAct=true,"Info","Error")}" strength="3" rendered="{!IF(NOT(OR(isAdmin,isSME,hasEditAccess)),true,false)}"/>  
        <center>
            <apex:commandButton value="{!$Label.CL00017}" action="{!save}" rendered="{!IF(OR(isAdmin,isSME,hasEditAccess),true,false)}" onclick="lockScreen()"/>   
            <apex:commandButton value="{!$Label.CLOCT14SLS67}" action="{!saveAndNew}"  rendered="{!IF(OR(isAdmin,isSME,hasEditAccess),true,false)}" onclick="lockScreen()"/> 
            <apex:commandButton value="{!$Label.CL00303}" action="{!cancel}" immediate="true" onclick="lockScreen()" rendered="{!IF(OR(isAdmin,isSME,hasEditAccess),true,false)}"/>                
        </center>
        <br/>
        <!-- Display SME info fields editable for Owners if it is in QLK is in draft and for admins -->
        <apex:pageBlock title="{!$ObjectType.RIT_SMETeam__c.label}  {!$Label.CL00203}" mode="edit" >
            <apex:pageBlockSection columns="2" title="{!$Label.CL00672}"  rendered="{!IF(OR(isAdmin, AND(hasEditAccess, OR(isDraft,isNew))),true,false)}">
                 <apex:repeat value="{!$ObjectType.RIT_SMETeam__c.FieldSets.SMETeamMemberInfo}" var="smeTeamMemb">
                    <apex:inputfield value="{!RIT_SMETeam__c[smeTeamMemb]}" required="{!smeTeamMemb.required}"/>      
                </apex:repeat>
          </apex:pageBlockSection>
          <!-- Display SME info fields read only for Owners if it is in QLK is not in draft and for non admins -->
            <apex:pageBlockSection columns="2" title="{!$Label.CL00672}"  rendered="{!IF(NOT(OR(isAdmin, AND(hasEditAccess, OR(isDraft,isNew)))),true,false)}">
                 <apex:repeat value="{!$ObjectType.RIT_SMETeam__c.FieldSets.SMETeamMemberInfo}" var="smeTeamMem">
                    <apex:outputfield value="{!RIT_SMETeam__c[smeTeamMem]}" />      
                </apex:repeat>
            </apex:pageBlockSection>
            <!-- Display SME feedback and comment fields as editable for Admins and SME team member-->
            <apex:pageBlockSection columns="2"  rendered="{!IF(OR(isAdmin,isSME),true,false)}">
                  <apex:repeat value="{!$ObjectType.RIT_SMETeam__c.FieldSets.SMETeamMemberFeedback}" var="smeTeamfdk">
                    <apex:inputfield value="{!RIT_SMETeam__c[smeTeamfdk]}"  required="{!smeTeamfdk.required}"/>      
                </apex:repeat>
            </apex:pageBlockSection>
            <!-- Display SME feedback and comment fields as read only for non Admins and  non SME team member and if it is not new record-->
            <apex:pageBlockSection columns="2"  rendered="{!IF(AND(NOT(OR(isAdmin,isSME)),NOT(isNew)),true,false)}">
                <apex:repeat value="{!$ObjectType.RIT_SMETeam__c.FieldSets.SMETeamMemberFeedback}" var="smeTeamfdbk">
                    <apex:outputfield value="{!RIT_SMETeam__c[smeTeamfdbk]}"/>      
                </apex:repeat>
            </apex:pageBlockSection>
                     
            <apex:pageBlockSection columns="1">
                <!-- Custom Field Detailed Comments field will be made read only and the text entered in the text area field will be appended --->
                <apex:inputTextarea cols="100" value="{!detailedComm}" label="{!$ObjectType.RIT_SMETeam__c.Fields.DetailedComments__c.label}" rows="10" richText="true" rendered="{!IF(OR(isAdmin,isSMe),true,false)}" />
                <apex:outputField value="{!RIT_SMETeam__c.DetailedComments__c}" rendered="{!IF(AND(RIT_SMETeam__c.Id!=null,RIT_SMETeam__c.DetailedComments__c!=null,OR(isAdmin,isSMe)),true,false)}"  label=""/> 
                <apex:outputField value="{!RIT_SMETeam__c.DetailedComments__c}" label="{!$ObjectType.RIT_SMETeam__c.Fields.DetailedComments__c.label}" rendered="{!IF(AND(NOT(OR(isAdmin,isSME)),NOT(isNew)),true,false)}" />                             
            </apex:pageBlockSection> 
        </apex:pageBlock>
        <center>
            <apex:commandButton value="{!IF(OR(hasEditAccess,isAdmin,isSME),$Label.CL00017,$Label.CL00660)}" action="{!save}" rendered="{!IF(OR(hasEditAccess,isAdmin,isSME,renderMembAct),true,false)}" onclick="lockScreen()"/>    
            <apex:commandButton value="{!$Label.CLOCT14SLS67}" action="{!saveAndNew}"  rendered="{!IF(OR(hasEditAccess,isAdmin,isSME),true,false)}" onclick="lockScreen()"/> <!-- added Save and New for BR-4488 April 2015 release -->
            <apex:commandButton value="{!IF(OR(hasEditAccess,isAdmin,isSME,renderMembAct),$Label.CL00303,$Label.CL00267)}" action="{!cancel}" immediate="true" onclick="lockScreen()"/>                
        </center>
    </apex:form>
</apex:page>