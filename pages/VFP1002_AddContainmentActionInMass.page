<apex:page sidebar="false" showHeader="true" standardController="ContainmentAction__c" extensions="VFC1002_AddContainmentActionInMass"> 

<!-- PAGE VFP1002_AddContainmentActionInMass TO create multiple containment actions -->    
    <!-- ~ Created by: Shukti Das 24/11/2011 -->
<!-- ~ SCHNEIDER I2P For Feb - 12 Release -->

    <apex:styleSheet value="{!$Resource.STR015_StyleSheet}"/>
    <apex:includescript value="{!URLFor($Resource.jquery_bundle,'jquery.js')}" />
    
    <!-- PAGE RESOURCES -->

    <!-- PAGE HEADER (title) -->
    <apex:sectionHeader title="Containment Action" subtitle="New Containment Action(s)"/>

    <!-- GENERAL PAGE ERROR MESSAGES -->
    <apex:pageMessages id="pageMessages"/>
    
    <!-- SEARCH ORGANIZATION SECTION WIZARD -->
    <apex:form id="theForm">
    <!-- do search when user clicks ENTER -->
        <apex:actionFunction action="{!doSearchAccountableOrgs}" name="doSearch" rerender="searchResult, pageMessages, VFP02_PAD"  status="status"/>
        <apex:outputPanel id="pb123">
            <apex:pageBlock rendered="{!blnRecordtype}">
                <apex:pageBlockSection title="Containment Action RecordType" columns="1" id="caInfo1">
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="Type" /><apex:selectList value="{!conAction.Recordtypeid}" disabled="false" size="1" style="width:300px" >
                        <apex:selectOptions value="{!lstselectOption}" ></apex:selectOptions></apex:selectList>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
              
                <apex:pageBlockButtons location="bottom">
                    <apex:commandButton value="Continue" action="{!hidecolumn}" reRender="pnl1,pb123,pageMessages" />
                    <apex:commandButton id="btnCancel" value="{!$Label.CL00010}" action="{!docancel}" immediate="true"/>
                </apex:pageBlockButtons>
            </apex:pageBlock>
        </apex:outputPanel>
        
        <apex:outputPanel id="pnl1">        
            <apex:pageBlock rendered="{!blnstatus1}" >
                <apex:pageBlockSection title="Containment Action Edit" columns="1" id="caInfo">
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="Title" />
                        <apex:inputField label="Title"         value="{!conAction.Title__c}"       style="width:550px" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="Description" />
                        <apex:inputField label="Description"   value="{!conAction.Description__c}"   style="width:500px; " /> 
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection columns="2" id="caAdditionalInfo">
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="Status" />
                        <apex:inputField label="Status"        value="{!conAction.Status__c}"        StyleClass="textboxpickStyle" />                     
                   </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputText label="Related Problem"  value="Related Problem"/>   
                        <apex:inputField label="Related Problem"  value="{!conAction.RelatedProblem__c}"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputText label="Due Date"  value="Due Date"/>   
                        <apex:inputField label="Due Date"  value="{!conAction.DueDate__c}" required="true"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem rendered="{!blnstatus}">
                        <apex:outputText label="Field Service Bulletin"  value="Field Service Bulletin"/>   
                        <apex:inputField label="Field Service Bulletin"  value="{!conAction.FieldServiceBulletin__c}" required="false"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem rendered="{!blnstatus}">
                        <apex:outputText label="Containment Action Type"  value="Containment Action Type"/>   
                        <apex:inputField label="Containment Action Type"  value="{!conAction.ContainmentActionTypes__c}" required="true"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                
                <!--Ship hold section added as part of December2013 release and BR is BR-4164-->
                <apex:pageBlockSection title="Required Actions" columns="1" id="caInfo1" rendered="{!NOT(blnstatus)}"> <!-- Section Name Modified , as per BR-7364, OCT15REL  Divya M -->
                    <apex:pageBlockSection columns="2" id="caAdditionalInfo">
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Shipments" />
                            <apex:inputField label="Shipments" value="{!conAction.Shipments__c}"  StyleClass="textboxpickStyle" required="true" /> 
                        </apex:pageBlockSectionItem>     
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Order Intake" />
                            <apex:inputField label="Order Intake"  value="{!conAction.OrderIntake__c}"  StyleClass="textboxpickStyle" required="true"/> 
                        </apex:pageBlockSectionItem>     
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Production" />
                            <apex:inputField label="Production"  value="{!conAction.Production__c}"   StyleClass="textboxpickStyle" required="true"/> 
                        </apex:pageBlockSectionItem>     
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Other Temporary Actions" />
                            <apex:inputField label="Other Temporary Actions"  value="{!conAction.OtherTemporaryActions__c}"  StyleClass="textboxpickStyle" required="true" /> 
                        </apex:pageBlockSectionItem>     
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Stock Sort & Quarantine" />
                            <apex:inputField label="Stock Sort & Quarantine"   value="{!conAction.StockSortQuarantine__c}"  StyleClass="textboxpickStyle" required="true" /> 
                        </apex:pageBlockSectionItem>     
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Stock Rework" />
                            <apex:inputField label="Stock Rework"  value="{!conAction.StockRework__c}" required="true" /> 
                            </apex:pageBlockSectionItem> 
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Stock Scrap" />
                            <apex:inputField label="Stock Scrap"  value="{!conAction.StockScrap__c}" StyleClass="textboxpickStyle"  required="true"/> 
                        </apex:pageBlockSectionItem>            
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Stock Return" />
                            <apex:inputField label="Stock Return" value="{!conAction.StockReturn__c}" StyleClass="textboxpickStyle" required="true" /> 
                        </apex:pageBlockSectionItem>     
                        <apex:pageBlockSectionItem >
                            <apex:outputText value="Other Permanent Actions" />
                            <apex:inputField label="Other Permanent Actions" value="{!conAction.OtherPermanentActions__c}"  StyleClass="textboxpickStyle" required="true" /> 
                        </apex:pageBlockSectionItem>     
                        <!-- Ends -->
                        <apex:pageBlockSectionItem > 
                            <apex:outputText label="Product Return Instructions"  value="Product Return Instructions"/>   
                            <apex:inputField label="Product Return Instructions"  value="{!conAction.ProductReturnInstructions__c}"/>
                        </apex:pageBlockSectionItem>           
                    </apex:pageBlockSection>
                </apex:pageBlockSection>
                <!--BR-4164-->
                <apex:outputPanel id="outpnl" rendered="{!blnstatus}">    
                </apex:outputPanel>          
            </apex:pageBlock>
            
            <!-- Added by Uttara - Notes2bFO - Q2 2016 Release -->
            
            <script type="text/javascript">
                function selectAllCheckboxes(obj,receivedInputID){
                    var inputCheckBox = document.getElementsByTagName("input");                  
                    for(var i=0; i<inputCheckBox.length; i++){          
                        if(inputCheckBox[i].id.indexOf(receivedInputID)!=-1){                                     
                            inputCheckBox[i].checked = obj.checked;
                        }
                    }
                }
            </script>
            
            <apex:pageBlock title="Affected Products" rendered="{!(blnstatus1 && APsPresent)}">     
            <apex:inputCheckbox onclick="selectAllCheckboxes(this,'inputId')"/>Select All           
                <apex:pageBlockTable var="AP" value="{!DisplayAffectedProducts}">
                    <apex:column headerValue="Select">
                        <apex:inputCheckbox value="{!AP.checkbox}" id="inputId"/>
                    </apex:column>                   
                    <apex:column headerValue="Product Line">
                        <apex:outputField value="{!AP.selectedAffProd.ProductLine__c}"/>
                    </apex:column> 
                    <apex:column headerValue="Product Family">
                        <apex:outputField value="{!AP.selectedAffProd.ProductFamily__c}"/>
                    </apex:column>
                    <apex:column headerValue="Family">
                        <apex:outputField value="{!AP.selectedAffProd.Family__c}"/>
                    </apex:column>  
                    <apex:column headerValue="Commercial Reference">
                        <apex:outputField value="{!AP.selectedAffProd.CommercialReference__c}"/>
                    </apex:column>
                    <apex:column headerValue="Date Codes / Serial Number">
                        <apex:outputField value="{!AP.selectedAffProd.DateCodesSerialNumber__c}"/>
                    </apex:column>
                </apex:pageBlockTable>  
            </apex:pageBlock>
            <!-- Ends - Uttara -->
            
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <!-- INPUT SEARCH SECTION -->
                <td width="350" valign="top">
                    <apex:pageBlock mode="Edit" id="inputSearch" rendered="{!blnstatus1}" >
                        <apex:pageBlockButtons id="theBlockBtn" >
                            <apex:commandButton id="btnSOSL" value="{!$Label.CL00332}" action="{!doSearchAccountableOrgs}" rerender="searchResult,pageMessages,VFP02_PAD, lookupRequired"  status="status"/>                            
                            <apex:commandButton id="btnCancel" value="{!$Label.CL00010}" action="{!docancel}" immediate="true"/>
                        </apex:pageBlockButtons>
                        
                        <apex:pageBlockSection title="Search Organization" columns="1" id="proInfo">
                            <apex:selectList label="Location Type" id="LocationType" value="{!LocType}" size="1" required="true">
                                 <apex:selectOptions value="{!options}">
                                 </apex:selectOptions>   
                            </apex:selectList>                             
                            <apex:inputText label="Location" value="{!searchString}"/>                        
                        </apex:pageBlockSection>
                    </apex:pageBlock>
                </td>
                
                <!-- SEARCH RESULT SECTION -->
                <td width="100%" valign="top">                
                <!-- SEARCH RESULTS ITEMS -->
                    <apex:pageBlock mode="Edit" id="searchResult" title="Accountable Organizations [{!IF(accOrgs!=null && accOrgs.size> 0,accOrgs.size,0)}{!IF(hasNext, '+', '')}]" rendered="{!blnstatus1}" >
                        <!--  <apex:pageMessage></apex:pageMessage> -->
                        <apex:pageBlockButtons >    
                        <apex:commandButton value="Create Containment Action(s)" action="{!continueCreation}" disabled="{!hasNext}"/>
                        <apex:actionFunction action="{!continueCreation}" name="CreateXAs"/>
                        </apex:pageBlockButtons>
                        <!--  Create Containment Actions (in Mass)"/ -->
                        <br/>
                        <font color="red">
                        <apex:outputText style="font-weight:800" value=" Please note that the search will return the first 150 results."/>
                        </font>
                        <apex:pageBlockTable value="{!AccOrgData}" var="pro" >
                            <apex:column headerValue="Select" >
                            <apex:inputCheckbox value="{!pro.isSelected}"/> 
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.BusinessRiskEscalationEntity__c.fields.Entity__c.label}">
                            <apex:outputText value="{!pro.entity}"/>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.BusinessRiskEscalationEntity__c.fields.SubEntity__c.label}">
                            <apex:outputText value="{!pro.subEntity }"/>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.BusinessRiskEscalationEntity__c.fields.Location__c.label}">
                            <apex:outputText value="{!pro.location}"/>
                            </apex:column>
                            <apex:column headerValue="Default Owner" >
                            <apex:outputField value="{!pro.stakeholder.User__c}" />
                            </apex:column>
                            <apex:column headerValue="Owner" >
                            <apex:inputField value="{!pro.conAction.Owner__c}" />
                            </apex:column>
                            <apex:column headerValue="  Additional Contact (email)" >
                            <apex:inputField value="{!pro.conAction.AdditionalContact__c}" />
                            </apex:column>                      
                        </apex:pageBlockTable>
                    </apex:pageBlock>
                   </td>
            </tr>
            </table>
             
            <!-- Create XA when user clicks ENTER -->
            <script>
            	jQuery(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                        CreateXAs(); 
                    }
               	});
            </script>
    	</apex:outputPanel>
    </apex:form>
    <!-- DEBUG MODE -->
    <c:PAD id="VFP02_PAD"/>
</apex:page>