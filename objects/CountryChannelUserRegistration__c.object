<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Country Channel User Registration Mandatory Fields</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>CountryChannels__c</fullName>
        <description>Country Channel</description>
        <externalId>false</externalId>
        <label>Country Channel</label>
        <referenceTo>CountryChannels__c</referenceTo>
        <relationshipLabel>Registration Form Fields</relationshipLabel>
        <relationshipName>CountryChannelUserRegistrationFields</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DefaultValue__c</fullName>
        <externalId>false</externalId>
        <label>Default Value</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Expression__c</fullName>
        <externalId>false</externalId>
        <label>Expression</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HiddenOnProfile__c</fullName>
        <defaultValue>false</defaultValue>
        <description>PRM 2015 - Determines whether the form field should be hidden on My Profile section</description>
        <externalId>false</externalId>
        <label>Hidden On Profile</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hidden__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Hidden form the country &amp; channel</description>
        <externalId>false</externalId>
        <label>Hidden On User Registration</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>HideField__c</fullName>
        <externalId>false</externalId>
        <label>Hide</label>
        <picklist>
            <picklistValues>
                <fullName>URONLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BOTH</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>MandatoryOnProfile__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Mandatory On Profile</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Mandatory__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check if field is mandatory for that country &amp; channel</description>
        <externalId>false</externalId>
        <label>Mandatory</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PublishingDescription__c</fullName>
        <description>Small description of last publication</description>
        <externalId>false</externalId>
        <inlineHelpText>Small description of last publication</inlineHelpText>
        <label>PublishingDescription</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>PublishingGoldenId__c</fullName>
        <description>Track the source Id</description>
        <externalId>false</externalId>
        <inlineHelpText>Track the source Id</inlineHelpText>
        <label>PublishingGoldenId</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingIsUpdated__c</fullName>
        <description>Identify if the record has been updated</description>
        <externalId>false</externalId>
        <formula>IF( OR(LastModifiedDate  &gt; PublishingTimestamp__c,ISBLANK(PublishingTimestamp__c)),true,false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identify if the record has been updated</inlineHelpText>
        <label>PublishingIsUpdated</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PublishingOdasevaId__c</fullName>
        <externalId>true</externalId>
        <label>PublishingOdasevaId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingPublisher__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Track who was the last publisher</description>
        <externalId>false</externalId>
        <inlineHelpText>Track who was the last publisher</inlineHelpText>
        <label>PublishingPublisher</label>
        <referenceTo>User</referenceTo>
        <relationshipName>PublisherCountryChannelUserRegistrationc</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PublishingStatusImage__c</fullName>
        <description>Identify at first sight that the record is set for publication</description>
        <externalId>false</externalId>
        <formula>CASE(TEXT(PublishingStatus__c),&quot;Ready for Publication&quot;,IMAGE(&quot;/img/samples/flag_green.gif&quot;,&quot;Ready for Publication&quot;),&quot;Awaiting Publication&quot;,IMAGE(&quot;/img/samples/flag_yellow.gif&quot;,&quot;Awaiting Publication&quot;),&quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identify at first sight that the record is set for publication</inlineHelpText>
        <label>PublishingStatusImage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingStatus__c</fullName>
        <externalId>false</externalId>
        <label>PublishingStatus</label>
        <picklist>
            <picklistValues>
                <fullName>Ready for Publication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Awaiting Publication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Published</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>PublishingTimestamp__c</fullName>
        <description>Date and time of the last publication</description>
        <externalId>false</externalId>
        <inlineHelpText>Date and time of the last publication</inlineHelpText>
        <label>PublishingTimestamp</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>TECH_MigrationUniqueID__c</fullName>
        <description>Unique ID to be used by ODASEVA for synchronizing the records between environments</description>
        <externalId>true</externalId>
        <label>Migration Unique ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_UserRegFieldName__c</fullName>
        <description>This field is used to copy the custom setting(CS_PRM_UserRegistration) value of this field.</description>
        <externalId>false</externalId>
        <label>Field Name</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UserRegFieldName__c</fullName>
        <externalId>false</externalId>
        <label>Field Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ValidationType__c</fullName>
        <externalId>false</externalId>
        <label>Validation Type</label>
        <picklist>
            <picklistValues>
                <fullName>Custom</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Regex</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Country Channel User Registration</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CountryChannels__c</columns>
        <columns>TECH_UserRegFieldName__c</columns>
        <columns>Mandatory__c</columns>
        <columns>HideField__c</columns>
        <columns>CREATEDBY_USER.ALIAS</columns>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER.ALIAS</columns>
        <columns>LAST_UPDATE</columns>
        <columns>OBJECT_ID</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>SESA108112</fullName>
        <columns>NAME</columns>
        <columns>TECH_UserRegFieldName__c</columns>
        <columns>Mandatory__c</columns>
        <columns>HideField__c</columns>
        <columns>UPDATEDBY_USER.ALIAS</columns>
        <columns>LAST_UPDATE</columns>
        <columns>CREATEDBY_USER.ALIAS</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CountryChannels__c</field>
            <operation>contains</operation>
            <value>CCN-0001066</value>
        </filters>
        <label>PRM Spain - SC/SC7</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>CCR-{0000000000}</displayFormat>
        <label>Country Channel User Registration</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Country Channel User Registrations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
