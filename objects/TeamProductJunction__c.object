<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>2012R04 - Junction object between the OPP_Product__c object and customer care team enabling a many to many relationship between them.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Business__c</fullName>
        <description>2012R04 - Cross-object formula displaying the value of the related business.</description>
        <externalId>false</externalId>
        <formula>TEXT(ProductFamily__r.BusinessUnit__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Business</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CCTeam__c</fullName>
        <description>2012R04 - Customer care team supporting the assigned product families.</description>
        <externalId>false</externalId>
        <label>Customer Care Team</label>
        <referenceTo>CustomerCareTeam__c</referenceTo>
        <relationshipName>TeamProductFamilyJunction</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DefaultSupportedProductFamily__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Mark this Supported Product Family as the default team for the CCC Team.</description>
        <externalId>false</externalId>
        <inlineHelpText>Mark this Supported Product Family as the default team for the CCC Team.</inlineHelpText>
        <label>Default Supported Product Family</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PM0_ProductFamily__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(ProductFamily__r.ProductFamily__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Family</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductFamily__c</fullName>
        <description>2012R04 - Master relationship with the OPP_product__c custom object, a filter ensures that only product families are available</description>
        <externalId>false</externalId>
        <label>Family</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>(1 AND 2) OR 3</booleanFilter>
            <errorMessage>Product is not active or not at Family Level.</errorMessage>
            <filterItems>
                <field>OPP_Product__c.CCCRelevant__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <filterItems>
                <field>OPP_Product__c.HierarchyType__c</field>
                <operation>equals</operation>
                <value>PM0-FAMILY</value>
            </filterItems>
            <filterItems>
                <field>$User.BypassVR__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>OPP_Product__c</referenceTo>
        <relationshipLabel>Supported Product Families</relationshipLabel>
        <relationshipName>TeamProductJunction</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ProductLine__c</fullName>
        <description>2012R04 - Cross-object formula displaying the value of the related product line.</description>
        <externalId>false</externalId>
        <formula>TEXT(ProductFamily__r.ProductLine__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Line</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_PFTeamUniqueKey__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>2012R04 - Unique key for Team - Product Family Assignment</description>
        <externalId>false</externalId>
        <label>TECH_PFTeamUniqueKey</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Supported Product Family</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CCTeam__c</columns>
        <columns>Business__c</columns>
        <columns>ProductLine__c</columns>
        <columns>ProductFamily__c</columns>
        <columns>PM0_ProductFamily__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>SPF-{0000000}</displayFormat>
        <label>Supported Product Family</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Supported Product Families</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
