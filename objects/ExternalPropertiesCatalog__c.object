<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <comment>Wizard to configure a Properties and Activity Catalog</comment>
        <content>VFP_PropertiesAndActivitiesCatalogStep1</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Badge__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Badge</label>
        <referenceTo>FieloEE__Badge__c</referenceTo>
        <relationshipName>External_Properties_Catalog</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EventCatalogCount__c</fullName>
        <externalId>false</externalId>
        <label>EventCatalogCount</label>
        <summaryForeignKey>PRMExternalPropertyEvent__c.ExternalPropertiesCatalog__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>EventCatalog__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Event Catalog</label>
        <referenceTo>FieloPRM_EventCatalog__c</referenceTo>
        <relationshipName>External_Properties_Catalog</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExternalPropertiesCatalogType__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>External System Type</label>
        <referenceTo>ExternalPropertiesCatalogType__c</referenceTo>
        <relationshipLabel>Properties and Activities Catalog</relationshipLabel>
        <relationshipName>Properties_and_Activities_Catalog</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExternalSystem__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK( &apos;/&apos;+ExternalPropertiesCatalogType__c ,  ExternalPropertiesCatalogType__r.Source__c +&quot; / &quot;+ ExternalPropertiesCatalogType__r.Type__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>External System</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FieldName__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Must be filled with the technical name of a picklist or text field</inlineHelpText>
        <label>FieldName</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FunctionalKey__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Functional Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>MinimumLengthCheck__c</fullName>
        <defaultValue>5</defaultValue>
        <description>Technical field to optimize queries on repository.
To support CONTAINS and NOT EQUAL OPERATIONS in Internal Properties Repository --&gt;  this field must be equal to 0 and the number of records in Intrenal Properties Repository  should not be huge</description>
        <externalId>false</externalId>
        <inlineHelpText>Technical field to optimize queries on repository.
To support CONTAINS and NOT EQUAL OPERATIONS in Internal Properties Repository --&gt;  this field must be equal to 0 and the number of records in Intrenal Properties Repository  should not be huge</inlineHelpText>
        <label>Minimum Length Check</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PRMCountryFieldName__c</fullName>
        <defaultValue>&apos;PRMCountry__c&apos;</defaultValue>
        <externalId>false</externalId>
        <label>PRMCountryFieldName</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PropertyName__c</fullName>
        <externalId>false</externalId>
        <label>Property Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingDescription__c</fullName>
        <description>Small description of last publication</description>
        <externalId>false</externalId>
        <inlineHelpText>Small description of last publication</inlineHelpText>
        <label>PublishingDescription</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>PublishingGoldenId__c</fullName>
        <description>Track the source Id</description>
        <externalId>false</externalId>
        <inlineHelpText>Track the source Id</inlineHelpText>
        <label>PublishingGoldenId</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingIsUpdated__c</fullName>
        <description>Identify if the record has been updated</description>
        <externalId>false</externalId>
        <formula>IF( OR(LastModifiedDate  &gt; PublishingTimestamp__c,ISBLANK(PublishingTimestamp__c)),true,false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identify if the record has been updated</inlineHelpText>
        <label>PublishingIsUpdated</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PublishingOdasevaId__c</fullName>
        <externalId>true</externalId>
        <label>PublishingOdasevaId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingPublisher__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Track who was the last publisher</description>
        <externalId>false</externalId>
        <inlineHelpText>Track who was the last publisher</inlineHelpText>
        <label>PublishingPublisher</label>
        <referenceTo>User</referenceTo>
        <relationshipName>PublisherExternalPropertiesCatalogc</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PublishingStatusImage__c</fullName>
        <description>Identify at first sight that the record is set for publication</description>
        <externalId>false</externalId>
        <formula>CASE(TEXT(PublishingStatus__c),&quot;Ready for Publication&quot;,IMAGE(&quot;/img/samples/flag_green.gif&quot;,&quot;Ready for Publication&quot;),&quot;Awaiting Publication&quot;,IMAGE(&quot;/img/samples/flag_yellow.gif&quot;,&quot;Awaiting Publication&quot;),&quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identify at first sight that the record is set for publication</inlineHelpText>
        <label>PublishingStatusImage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingStatus__c</fullName>
        <externalId>false</externalId>
        <label>PublishingStatus</label>
        <picklist>
            <picklistValues>
                <fullName>Ready for Publication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Awaiting Publication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Published</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>PublishingTimestamp__c</fullName>
        <description>Date and time of the last publication</description>
        <externalId>false</externalId>
        <inlineHelpText>Date and time of the last publication</inlineHelpText>
        <label>PublishingTimestamp</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SubType__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>TechnicalId</inlineHelpText>
        <label>Technical Id</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TECH_MigrationUniqueID__c</fullName>
        <description>Unique ID to be used by ODASEVA for synchronizing the records between environments</description>
        <externalId>true</externalId>
        <label>Migration Unique ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TableName__c</fullName>
        <externalId>false</externalId>
        <label>bFO Object Name</label>
        <picklist>
            <picklistValues>
                <fullName>Account</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Opportunity</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>TransactionCatalog__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Transaction Catalog</label>
        <referenceTo>PRMTransactionCatalog__c</referenceTo>
        <relationshipLabel>Properties and Activities Catalog</relationshipLabel>
        <relationshipName>External_Properties_Catalog</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>Length should not bypass 55 characters because FunctionnalKey length = 255 and subTypeLength=200</description>
        <externalId>false</externalId>
        <label>Type (deprecated)</label>
        <picklist>
            <picklistValues>
                <fullName>ApplicationAccess</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BFOProperties</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Training</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Certification</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Curriculum</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Test</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Session</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Video</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Login</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Properties and Activities Catalog</label>
    <listViews>
        <fullName>All</fullName>
        <columns>PropertyName__c</columns>
        <columns>NAME</columns>
        <columns>ExternalSystem__c</columns>
        <columns>Active__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>SubType__c</columns>
        <columns>TableName__c</columns>
        <columns>FieldName__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>EcoXpert_Invitation</fullName>
        <columns>NAME</columns>
        <columns>Badge__c</columns>
        <columns>EventCatalog__c</columns>
        <columns>FieldName__c</columns>
        <columns>SubType__c</columns>
        <columns>Active__c</columns>
        <columns>TableName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>NAME</field>
            <operation>contains</operation>
            <value>ECOX</value>
        </filters>
        <label>EcoXpert Invitation</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>External_Properties_LMS</fullName>
        <columns>NAME</columns>
        <columns>PropertyName__c</columns>
        <columns>SubType__c</columns>
        <columns>Badge__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ExternalSystem__c</field>
            <operation>contains</operation>
            <value>LMS / Curriculum</value>
        </filters>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>ExternalPropertiesCatalog__c.ExternalProperties</value>
        </filters>
        <label>External Properties - LMS</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Internal_Properties</fullName>
        <columns>NAME</columns>
        <columns>Active__c</columns>
        <columns>PropertyName__c</columns>
        <columns>TableName__c</columns>
        <columns>FieldName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>ExternalPropertiesCatalog__c.BFOProperties</value>
        </filters>
        <label>Internal Properties</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>PAC-{0000}</displayFormat>
        <label>External Properties Catalog Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Properties and Activities Catalog</pluralLabel>
    <recordTypes>
        <fullName>BFOProperties</fullName>
        <active>true</active>
        <description>Properties to be assigned inside bFO</description>
        <label>Internal Properties</label>
        <picklistValues>
            <picklist>TableName__c</picklist>
            <values>
                <fullName>Account</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Opportunity</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>BFOProperties</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>ExternalActivities</fullName>
        <active>true</active>
        <description>Activities done in external System</description>
        <label>External Activities</label>
        <picklistValues>
            <picklist>TableName__c</picklist>
            <values>
                <fullName>Account</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Contact</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Login</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>ExternalProperties</fullName>
        <active>true</active>
        <description>Properties to be assigned from external System</description>
        <label>External Properties</label>
        <picklistValues>
            <picklist>TableName__c</picklist>
            <values>
                <fullName>Account</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Contact</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>ApplicationAccess</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Certification</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Curriculum</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Session</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Test</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Training</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Video</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>BFOProperties_Required</fullName>
        <active>true</active>
        <errorConditionFormula>NOT($User.BypassVR__c) &amp;&amp; RecordType.DeveloperName==&apos;BFOProperties&apos; &amp;&amp; ( ISPICKVAL(TableName__c, &apos;&apos;) || ISBLANK(FieldName__c) || ISBLANK(SubType__c))</errorConditionFormula>
        <errorDisplayField>Active__c</errorDisplayField>
        <errorMessage>Internal Properties must have FieldName,ObjectName and a Technical Id</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ExtActivityNoSubtype</fullName>
        <active>true</active>
        <errorConditionFormula>NOT($User.BypassVR__c) &amp;&amp; (RecordTypeId=$Label.CLJUN16PRM065 &amp;&amp; SubType__c!=null)</errorConditionFormula>
        <errorDisplayField>SubType__c</errorDisplayField>
        <errorMessage>External Activities cannot have a property name</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ExtPropTypeNotNull</fullName>
        <active>true</active>
        <errorConditionFormula>NOT($User.BypassVR__c) &amp;&amp; (($RecordType.DeveloperName=&apos;ExternalProperties&apos; || $RecordType.DeveloperName=&apos;ExternalActivities&apos;) &amp;&amp; ExternalPropertiesCatalogType__c=null)</errorConditionFormula>
        <errorDisplayField>ExternalPropertiesCatalogType__c</errorDisplayField>
        <errorMessage>External Systme Type (ExternalPropertiesCatalogType__c) cannot be null for external properties/Activities</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>PropertyNameNotNull</fullName>
        <active>false</active>
        <errorConditionFormula>NOT($User.BypassVR__c) &amp;&amp; ((RecordType.DeveloperName =&apos;ExternalProperties&apos; || RecordType.DeveloperName =&apos;BFOProperties&apos;) &amp;&amp; (SubType__c==null || PropertyName__c==null))</errorConditionFormula>
        <errorMessage>PropertyName and TechnicalId cannot be null</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Manage_Outputs</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Manage Output Items</masterLabel>
        <openType>sidebar</openType>
        <page>VFP_PropertiesAndActivitiesCatalogStep2</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
