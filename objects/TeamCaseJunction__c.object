<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>2012R04 - Junction object enabling a many to many relationship between the case classification records and customer care team records.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>CCTeam__c</fullName>
        <description>2012R04 - Customer care team supporting the assigned case category.</description>
        <externalId>false</externalId>
        <label>Customer Care Team</label>
        <referenceTo>CustomerCareTeam__c</referenceTo>
        <relationshipLabel>Supported Case Classifications</relationshipLabel>
        <relationshipName>TeamCaseJunction</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CaseCategory__c</fullName>
        <description>2012R04 - Cross object formula displaying the category of the related case classification.</description>
        <externalId>false</externalId>
        <formula>CaseClassification__r.Category__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Category</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CaseClassification__c</fullName>
        <description>2012R04 - Master-Detail relationship with case classifcation object,.</description>
        <externalId>false</externalId>
        <label>Case Classification</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1</booleanFilter>
            <filterItems>
                <field>CaseClassification__c.Category__c</field>
                <operation>notEqual</operation>
                <value>null</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>CaseClassification__c</referenceTo>
        <relationshipLabel>Supported Case Classifications</relationshipLabel>
        <relationshipName>CC_Team_Supported_Case_Categories</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CaseReason__c</fullName>
        <description>2012R04 - Cross object formula displaying the reason of the related case classification.</description>
        <externalId>false</externalId>
        <formula>CaseClassification__r.Reason__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CaseSubReason__c</fullName>
        <description>2012R04 - Cross object formula displaying the sub reason of the related case classification.</description>
        <externalId>false</externalId>
        <formula>CaseClassification__r.SubReason__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Case Sub Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Default__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Default</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TECH_ClassTeamUniqueKey__c</fullName>
        <description>2012R04 – Uniqueness key for Team - Case Classification assignment</description>
        <externalId>false</externalId>
        <label>TECH_ClassTeamUniqueKey</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Supported Case Classification</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CCTeam__c</columns>
        <columns>CaseClassification__c</columns>
        <columns>CaseCategory__c</columns>
        <columns>CaseReason__c</columns>
        <columns>CaseSubReason__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>TC-{00000}</displayFormat>
        <label>Supported Case Classification</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Supported Case Classifications</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
