<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores Partner program and program level against Assessements.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Describes whether a Program level is active or not</description>
        <externalId>false</externalId>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Assessment__c</fullName>
        <externalId>false</externalId>
        <label>Assessment</label>
        <referenceTo>Assessment__c</referenceTo>
        <relationshipName>Assessement_Programs</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>AutomaticAssignment__c</fullName>
        <externalId>false</externalId>
        <formula>Assessment__r.AutomaticAssignment__c</formula>
        <label>Automatic Assignment</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PartnerProgram__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Partner Program</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>PartnerProgram__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Country Partner Program, Cluster Partner Program</value>
            </filterItems>
            <filterItems>
                <field>PartnerProgram__c.ProgramStatus__c</field>
                <operation>equals</operation>
                <value>Active</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>PartnerProgram__c</referenceTo>
        <relationshipLabel>Program Level Extensions</relationshipLabel>
        <relationshipName>Assessement_Programs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProgramLevel__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Program Level</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ProgramLevel__c.PartnerProgram__c</field>
                <operation>equals</operation>
                <valueField>$Source.PartnerProgram__c</valueField>
            </filterItems>
            <filterItems>
                <field>ProgramLevel__c.LevelStatus__c</field>
                <operation>equals</operation>
                <value>Active</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ProgramLevel__c</referenceTo>
        <relationshipLabel>ORF Extensions</relationshipLabel>
        <relationshipName>Assessement_Programs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Assessment Program</label>
    <nameField>
        <displayFormat>AP-{00000}</displayFormat>
        <label>Assessement Program Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Assessment Programs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ASP_VR_OCT14PRM01_CheckPartnerPrgLevel</fullName>
        <active>true</active>
        <description>Check whether Partner program and program level are null or not.</description>
        <errorConditionFormula>OR(PartnerProgram__c  = null, ProgramLevel__c  = null)</errorConditionFormula>
        <errorMessage>Partner program and Program Level should not be null.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>NewORFExtension</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New ORF Extension</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/a4l/e?CF00NA000000ALIcr={!ProgramLevel__c.Name}&amp;CF00NA000000ALIcr_lkid={!PartnerProgram__c.Name} &amp;retURL={!ProgramLevel__c.Id}&amp;RecordType=012A0000000nwE4</url>
    </webLinks>
</CustomObject>
