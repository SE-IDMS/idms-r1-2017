<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>DMT - Schneider actors</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ActorFirstName__c</fullName>
        <externalId>false</externalId>
        <label>Actor First Name</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ActorLastName__c</fullName>
        <externalId>false</externalId>
        <label>Actor Last Name</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ActorName__c</fullName>
        <description>DMT - Formula field to concatenate the first and last name of a Project Stakeholder record</description>
        <externalId>false</externalId>
        <formula>ActorNew__r.StakeholderName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actor Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ActorNew__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Actor</label>
        <referenceTo>DMTStakeholder__c</referenceTo>
        <relationshipLabel>Project Stakeholders</relationshipLabel>
        <relationshipName>Project_Stakeholders</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DMTOwner__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL( Role__c , &apos;PMO/Delivery project owner&apos;) , ActorName__c , ProjectReq__r.Owner:User.FirstName +&apos; &apos;+ProjectReq__r.Owner:User.LastName  )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DMT Owner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Exclude__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Check this if you do not want this stakeholder to receive email notifications in regard to this project</inlineHelpText>
        <label>Exclude from notification</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ProjectReq__c</fullName>
        <externalId>false</externalId>
        <label>Project Request</label>
        <referenceTo>PRJ_ProjectReq__c</referenceTo>
        <relationshipLabel>Project Stakeholders</relationshipLabel>
        <relationshipName>Stakeholders</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <externalId>false</externalId>
        <label>Role</label>
        <picklist>
            <picklistValues>
                <fullName>Architecture Responsible</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Business/IPO Analyst</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Customer Experience</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Entity Financial Controller</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Entity Process Advocate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Global Applications Services</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Global Operations Services</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Process Owner</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IPO Financial Controller</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Project Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Project Portfolio Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Purchase Responsible</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Requester</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Security Responsible</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sponsor</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>X Relationship Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>X Relationship Manager Team</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>R Relationship Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PMO/Delivery Project Owner</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Project Stakeholder</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Role__c</columns>
        <columns>ActorName__c</columns>
        <columns>ActorLastName__c</columns>
        <columns>ActorNew__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>Exclude__c</columns>
        <columns>ProjectReq__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>BG_stakeholders</fullName>
        <booleanFilter>1 AND (2 or 3 or 4)</booleanFilter>
        <columns>Role__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>Exclude__c</columns>
        <columns>ProjectReq__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Role__c</field>
            <operation>equals</operation>
            <value>Process Owner</value>
        </filters>
        <filters>
            <field>ProjectReq__c</field>
            <operation>contains</operation>
            <value>buildings</value>
        </filters>
        <filters>
            <field>ProjectReq__c</field>
            <operation>contains</operation>
            <value>pelco</value>
        </filters>
        <filters>
            <field>ProjectReq__c</field>
            <operation>contains</operation>
            <value>tac</value>
        </filters>
        <label>buildings stakelholders</label>
    </listViews>
    <nameField>
        <displayFormat>ACT-{0000}</displayFormat>
        <label>Project Actor Code</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Project Stakeholders</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>Manage_stakeholders</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Manage stakeholders</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/STK_StakeholderMgmt?Id={!PRJ_ProjectReq__c.Id}</url>
    </webLinks>
</CustomObject>
