<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Related Account</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Account_Energy_Usages</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <description>Annual Volume is a number with two decimal points that is the absolute value of the amount of energy units consumed by an account in the year indicated.</description>
        <externalId>false</externalId>
        <inlineHelpText>The annual volume of usage for the commodity or resource</inlineHelpText>
        <label>Annual Volume</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cost__c</fullName>
        <description>Annual Spend is the total amount of money expended by the account for the amount of energy of the indicated type and in the indicated year.</description>
        <externalId>false</externalId>
        <inlineHelpText>The annual spend associated with the commodity or resource</inlineHelpText>
        <label>Annual Spend</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EnergySupplierContractEnd__c</fullName>
        <description>Energy Supplier Contract End</description>
        <externalId>false</externalId>
        <inlineHelpText>The date the contract will end with the current energy supplier named above</inlineHelpText>
        <label>Energy Supplier Contract End</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EnergySupplier__c</fullName>
        <description>Energy Supplier</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the current energy supplier.  If necessary, indicate for which region the supplier provides service.</inlineHelpText>
        <label>Energy Supplier</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>EnergySupplyAdvisorContractEnd__c</fullName>
        <description>Energy Supply Advisor Contract End</description>
        <externalId>false</externalId>
        <inlineHelpText>The date the contract will end with the current energy supply advisor named above</inlineHelpText>
        <label>Energy Supply Advisor Contract End</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EnergySupplyAdvisor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Energy Supply Advisor</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the current energy supply advisor who is a competitor of Schneider Electric.  If the competitor is not found in the database, select Other and type the name.</inlineHelpText>
        <label>Energy Supply Advisor</label>
        <referenceTo>Competitor__c</referenceTo>
        <relationshipName>Account_Energy_Usages</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EnergySupplySustainablity__c</fullName>
        <description>Holds the Energy Supply &amp; Sustainability</description>
        <externalId>false</externalId>
        <label>Energy Supply &amp; Sustainability</label>
        <referenceTo>EnergySupplySustainability__c</referenceTo>
        <relationshipLabel>Commodity Usage</relationshipLabel>
        <relationshipName>Account_Energy_Usages</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EnergyType__c</fullName>
        <description>Commodity defines which type of energy is used by that account or enterprise.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the commodity or resource for which you will describe the usage.</inlineHelpText>
        <label>Commodity</label>
        <picklist>
            <picklistValues>
                <fullName>Electricity</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Natural Gas</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Propane</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Water</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Waste</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Diesel/Fuel</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Carbon</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Environmental</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>HelpText__c</fullName>
        <externalId>false</externalId>
        <formula>&quot;Enter the annual energy usage at the account.  Please indicate the Usage Year, Scope of Usage, Type of Commodity and the corresponding Cost to the account. The expected units of measure for each type are as follows:  Electric: kWh (kilowatt-hour), Natural Gas: Dth (decatherm), Propane: Gal (gallon) and Diesel: Gal(gallon).&quot;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>&lt;Help Text&gt;</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OtherNotes__c</fullName>
        <description>Notes</description>
        <externalId>false</externalId>
        <inlineHelpText>Include any additional notes about the account&apos;s commodity or resource usage and spend here.</inlineHelpText>
        <label>Other Notes</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Sites__c</fullName>
        <description>Sites</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of sites the usage data describes</inlineHelpText>
        <label>Sites</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SustainabilityAdvisorContractEnd__c</fullName>
        <description>Sustainability Advisor Contract End</description>
        <externalId>false</externalId>
        <inlineHelpText>The date the contract will end with the current sustainability or environmental advisor named above</inlineHelpText>
        <label>Sustainability Advisor Contract End</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SustainabilityAdvisor__c</fullName>
        <description>Sustainability Advisor</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the current sustainability or environmental advisor who is a competitor of Schneider Electric.  If the competitor is not found in the database, select Other and type the name.</inlineHelpText>
        <label>Sustainability Advisor</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Units__c</fullName>
        <description>Units</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the units of measure related to the annual volume entered.</inlineHelpText>
        <label>Units</label>
        <picklist>
            <picklistValues>
                <fullName>kWh</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tons</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cubic Meters</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Gallons</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Liters</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>UsageScope__c</fullName>
        <description>Scope defines the perimeter of an account that the energy was used.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select whether the usage data relates to the single site or corporate-wide.</inlineHelpText>
        <label>Scope</label>
        <picklist>
            <picklistValues>
                <fullName>Corporate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Site</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Year__c</fullName>
        <description>It represents the calendar year in which the energy for that was used</description>
        <externalId>false</externalId>
        <inlineHelpText>The year for which the usage data describes</inlineHelpText>
        <label>Year</label>
        <picklist>
            <picklistValues>
                <fullName>2011</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2012</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2013</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2014</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2015</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2016</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2017</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2018</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2019</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2020</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Commodity Usage</label>
    <nameField>
        <displayFormat>AEU-{0000}</displayFormat>
        <label>Energy Commodity Usage Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Commodity Usage</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
