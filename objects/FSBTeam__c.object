<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Q2 2016 Release : Notes to bFO 
I2P cross process bFS
Field Service Bulletin Team members gather :
- Stakeholders / Contributors to FSB work
- Persons needing to be notified as proofreaders for this FSB
Those persons can be either bFO existing Users Or non bFO Users tracked with their email</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Email__c</fullName>
        <externalId>false</externalId>
        <label>Email when user do not exist</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FieldServiceBulletin__c</fullName>
        <description>Q2 2016 Release : Notes to bFO</description>
        <externalId>false</externalId>
        <label>Field Service Bulletin</label>
        <referenceTo>FieldServiceBulletin__c</referenceTo>
        <relationshipName>FSB_Team</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ProofreaderOnlyExtendedTeam__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Proofreader Only (Extended Team)</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <description>Q2 2016 Release : Notes to bFO</description>
        <externalId>false</externalId>
        <label>Role</label>
        <picklist>
            <picklistValues>
                <fullName>R&amp;D Platform Engineer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>L3 Tech Support</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LOB Quality</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quality Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Zone Services Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Service Supply Chain Contacts</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Technical Writer Leader</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>8</visibleLines>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Q2 2016 Release : Notes to bFO</description>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>FSB_Team</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>FSB Team Member</label>
    <nameField>
        <displayFormat>R-{00000}</displayFormat>
        <label>FSB Team Member Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>FSB Team Members</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>VR_Q216_UserNotPresent</fullName>
        <active>true</active>
        <description>Q2 2016 Release : If a ‘User’ is not defined on the FSB team member then the ‘Email when user do not exist’ field should be filled.</description>
        <errorConditionFormula>AND( NOT( $User.BypassVR__c ) ,  ISBLANK( Email__c ) ,  ISBLANK(  User__c ))</errorConditionFormula>
        <errorDisplayField>Email__c</errorDisplayField>
        <errorMessage>If a ‘User’ is not defined on the FSB team member then the ‘Email when user do not exist’ field should be filled.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>EmailextendedTeam</fullName>
        <availability>online</availability>
        <description>Q2 2016 Release : Notes to bFO</description>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Email extended Team</masterLabel>
        <openType>sidebar</openType>
        <page>VFP_FSBTeamExtendedEmail</page>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
    </webLinks>
</CustomObject>
