<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object to manage the BSL scopes to be called for SST (Sales &amp; Sizing tools) application on PRM portal</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>DocTypeGroupBslOids__c</fullName>
        <description>comma separated list of BSL DocTypeGroupBslOids</description>
        <externalId>false</externalId>
        <label>BSL Doc Type Group for Tools</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FieloCategory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Category</label>
        <referenceTo>FieloEE__Category__c</referenceTo>
        <relationshipName>PRM_SST_BSL_scopes</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PRMCountry__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>PRM Country / Cluster Setup</label>
        <referenceTo>PRMCountry__c</referenceTo>
        <relationshipLabel>Sales &amp; Sizing Tools Configurations</relationshipLabel>
        <relationshipName>Sales_Sizing_Tools_Configurations</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PublishingDescription__c</fullName>
        <description>Small description of last publication</description>
        <externalId>false</externalId>
        <inlineHelpText>Small description of last publication</inlineHelpText>
        <label>PublishingDescription</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>PublishingGoldenId__c</fullName>
        <description>Track the source Id</description>
        <externalId>false</externalId>
        <inlineHelpText>Track the source Id</inlineHelpText>
        <label>PublishingGoldenId</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingIsUpdated__c</fullName>
        <description>Identify if the record has been updated</description>
        <externalId>false</externalId>
        <formula>IF( OR(LastModifiedDate  &gt; PublishingTimestamp__c,ISBLANK(PublishingTimestamp__c)),true,false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identify if the record has been updated</inlineHelpText>
        <label>PublishingIsUpdated</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PublishingOdasevaId__c</fullName>
        <externalId>true</externalId>
        <label>PublishingOdasevaId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingPublisher__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Track who was the last publisher</description>
        <externalId>false</externalId>
        <inlineHelpText>Track who was the last publisher</inlineHelpText>
        <label>PublishingPublisher</label>
        <referenceTo>User</referenceTo>
        <relationshipName>PublisherPRMSSTConfc</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PublishingStatusImage__c</fullName>
        <description>Identify at first sight that the record is set for publication</description>
        <externalId>false</externalId>
        <formula>CASE(TEXT(PublishingStatus__c),&quot;Ready for Publication&quot;,IMAGE(&quot;/img/samples/flag_green.gif&quot;,&quot;Ready for Publication&quot;),&quot;Awaiting Publication&quot;,IMAGE(&quot;/img/samples/flag_yellow.gif&quot;,&quot;Awaiting Publication&quot;),&quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identify at first sight that the record is set for publication</inlineHelpText>
        <label>PublishingStatusImage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PublishingStatus__c</fullName>
        <externalId>false</externalId>
        <label>PublishingStatus</label>
        <picklist>
            <picklistValues>
                <fullName>Ready for Publication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Awaiting Publication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Published</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>PublishingTimestamp__c</fullName>
        <description>Date and time of the last publication</description>
        <externalId>false</externalId>
        <inlineHelpText>Date and time of the last publication</inlineHelpText>
        <label>PublishingTimestamp</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>RecentlyAddedDocTypeGroupBslOids__c</fullName>
        <description>Enter here a BSL Doc Type Group Id (BSL OID) to to Filter results of &quot;Recently Added&quot;</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter here a BSL Doc Type Group Id (BSL OID) to to Filter results of &quot;Recently Added&quot;</inlineHelpText>
        <label>Filter for &quot;Recently Added&quot;</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShowRecentlyAdded__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Show &quot;Recently Added&quot;</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TECH_MigrationUniqueID__c</fullName>
        <description>Unique ID to be used by ODASEVA for synchronizing the records between environments</description>
        <externalId>true</externalId>
        <label>Migration Unique ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Use_Mockup__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Use Mockup</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>brand__c</fullName>
        <externalId>false</externalId>
        <label>Brand</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>channel__c</fullName>
        <externalId>false</externalId>
        <label>Channel</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>country__c</fullName>
        <externalId>false</externalId>
        <label>country</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>function__c</fullName>
        <externalId>false</externalId>
        <label>Function</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>market__c</fullName>
        <externalId>false</externalId>
        <label>Market</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>privacy__c</fullName>
        <externalId>false</externalId>
        <label>Privacy</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>program__c</fullName>
        <externalId>false</externalId>
        <label>Program</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>project__c</fullName>
        <externalId>false</externalId>
        <label>Project</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Sales &amp; Sizing Tools Configuration</label>
    <nameField>
        <label>SST Configuration name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Sales &amp; Sizing Tools Configurations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
