<!-------------------------------------------------------------------
- Company: Fielo
- Created Date: 22/08/2016
- Description: 
-------------------------------------------------------------------->
<apex:component layout="none" id="newstest">    
    <apex:attribute name="layout" description="layout" type="String" default="" />
    <apex:attribute name="display" description="display" type="String" default="" />
    <apex:attribute name="delay" description="news Delay" type="Integer" default="3000" />
    <apex:attribute name="componentId" description="component id" type="String" default="" />
    <apex:attribute name="qcolumns" description="Number of columns" type="integer" default="4" />
    <apex:attribute name="pageName" description="name of the page to go the detail" type="String" default="" />
    <apex:attribute name="newsList" description="news records to display" type="FieloEE__News__c[]" />
    <apex:attribute name="fieldSet" description="fieldset" type="String[]" />
    <apex:attribute name="cssClasses" description="css classes" type="String" />
    <apex:attribute name="docUrl" description="docUrl" type="String" />
    
    <script type="text/javascript">
    (function(exports, d) {
        function domReady(fn, context) {

        function onReady(event) {
          d.removeEventListener("DOMContentLoaded", onReady);
          fn.call(context || exports, event);
        }

        function onReadyIe(event) {
          if (d.readyState === "complete") {
            d.detachEvent("onreadystatechange", onReadyIe);
            fn.call(context || exports, event);
          }
        }

        d.addEventListener && d.addEventListener("DOMContentLoaded", onReady) ||
        d.attachEvent      && d.attachEvent("onreadystatechange", onReadyIe);
        }

        exports.domReady = domReady;
        })(window, document);
    </script>

    <div class="contentfeed {!layout} {!cssClasses}">

    <!-- ========== EMPTY ========== -->
    <apex:panelGroup rendered="{!(newsList.size <= 0)}">
        <div class="alert alert-info pagination-centered">
            {!$Label.FieloEE__InfoEmptyTable}
        </div>
    </apex:panelGroup>
    <!-- ========== EMPTY ========== -->

    <apex:panelGroup rendered="{!(newsList.size > 0)}" >

<!--
                *       *   *****   *****
                *       *   *         *
                *       *   *****     *
                *       *       *     *
                *****   *   *****     *
-->
        <apex:panelGroup rendered="{!layout == 'List'}">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <apex:repeat value="{!fieldSet}" var="f">
                            <th>
                                <apex:outputText value="{!$ObjectType.FieloEE__News__c.Fields[f].Label}" rendered="{!LEFT(f, 2)!='c:'}"/>
                            </th>
                        </apex:repeat>
                    </tr>
                </thead>
                <tbody>
                    <apex:panelGroup rendered="{!display != 'Accordion'}">
                        <apex:repeat value="{!newsList}" var="n">
                            <tr>
                                <td class="image">
                                    <div>
                                        <apex:panelGroup layout="none" rendered="{!NOT(ISBLANK(n.FieloEE__AttachmentId__c))}">
                                            <img src="{!IF(LEFT(n.FieloEE__AttachmentId__c,3) == '00P', URLFOR($Action.Attachment.Download, n.FieloEE__AttachmentId__c), IF(LEFT(n.FieloEE__AttachmentId__c,3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+n.FieloEE__AttachmentId__c+'&oid='+$Organization.Id, ''))}" />
                                        </apex:panelGroup>
                                        <apex:panelGroup rendered="{!(ISBLANK(n.FieloEE__AttachmentId__c))}">
                                            <i class="icon-camera icon-3x"></i>
                                        </apex:panelGroup>
                                    </div>
                                </td>
                                <td class="title">
                                    <apex:outputText value="{!n.FieloEE__Title__c}" rendered="{!display == 'none'}"/>
                                    <apex:panelGroup rendered="{!display == 'Internal Page'}">
                                        <apex:outputLink value="{!URLFOR($Site.Prefix + '/'+pageName, null, [Id=n.Id])+'&retURL='+$CurrentPage.Url+'&compId='+componentId }">{!n.FieloEE__Title__c}</apex:outputLink>
                                    </apex:panelGroup>                                    
                                </td>
                                <apex:repeat value="{!fieldSet}" var="f">
                                    <td class="{!$ObjectType.FieloEE__News__c.Fields[f].Name}">
                                        <c:FieloPRM_C_ShowField docURL="{!IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+n[f]+'&oid='+$Organization.Id}" record="{!n}" name="{!f}" describe="{!$ObjectType.FieloEE__News__c.Fields[f]}" />
                                    </td>
                                </apex:repeat>
                            </tr>
                        </apex:repeat>
                    </apex:panelGroup>                    
                </tbody>
            </table>
        </apex:panelGroup>
        <!-- END LIST -->


<!--
                *****   *****   *   **
                *       *   *   *   *  *
                *  **   *****   *   *   *
                *   *   * *     *   *  *
                *****   *   *   *   **
-->
        <apex:panelGroup rendered="{!layout == 'Grid'}">
            <apex:variable value="1" var="col"/>
            <apex:variable value="1" var="fil"/>

            <apex:variable value="1" var="i"/>
            <apex:variable value="1" var="qant"/>

            <apex:variable value="{!qcolumns}" var="qcol"/>
            <apex:variable value="1" var="cont"/>

            <apex:variable value='<div class="row-fluid"><ul class="thumbnails">' var="init"/>
            <apex:variable value='</ul></div>' var="end"/>

            <apex:repeat value="{!newsList}" var="n">
                <apex:outputText value="{!init}" escape="false" rendered="{!VALUE(cont) == 1}"/>
                <li class="span{!FLOOR(12/qcol)} gridLiContainer">
                    <div class="item">
                        <apex:panelGroup rendered="{!display == 'none'}">
                            <div>
                                <apex:repeat value="{!fieldSet}" var="f">
                                    <div class="{!$ObjectType.FieloEE__News__c.Fields[f].Name}">
                                        <c:FieloPRM_C_ShowField docURL="{!IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+n[f]+'&oid='+$Organization.Id}" record="{!n}" name="{!f}" describe="{!$ObjectType.FieloEE__News__c.Fields[f]}" />
                                    </div>
                                </apex:repeat>
                            </div>
                        </apex:panelGroup>

                        <apex:panelGroup rendered="{!display == 'Internal Page'}">
                            <apex:outputLink value="{!URLFOR($Site.Prefix + '/'+pageName, null, [Id=n.Id])+'&retURL='+$CurrentPage.Url+'&compId='+componentId }">
                                <div>
                                    <apex:repeat value="{!fieldSet}" var="f">
                                        <div class="{!$ObjectType.FieloEE__News__c.Fields[f].Name}">
                                            <c:FieloPRM_C_ShowField docURL="{!IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+n[f]+'&oid='+$Organization.Id}" record="{!n}" name="{!f}" describe="{!$ObjectType.FieloEE__News__c.Fields[f]}" />
                                        </div>
                                    </apex:repeat>
                                </div>
                            </apex:outputLink>
                        </apex:panelGroup>
                    </div>
                </li>
                <apex:panelGroup rendered="{!VALUE(col)==qcol}">
                    <apex:variable value="0" var="col"/>
                    <apex:variable value="{!VALUE(fil)+1}" var="fil"/>
                </apex:panelGroup>

                <apex:variable value="{!VALUE(col)+1}" var="col"/>
                <apex:variable value="{!VALUE(i)+1}" var="i"/>

                <apex:outputText value="{!end}" escape="false" rendered="{!(VALUE(cont) == qcol)}"/>

                <apex:variable value="0" var="cont" rendered="{!VALUE(cont) == qcol}"/>
                <apex:variable value="{!VALUE(cont) + 1}" var="cont"/>
            </apex:repeat>
            <apex:outputText value="{!end}" escape="false" rendered="{!(MOD(newsList.size,qcol) != 0 )}"/>
        </apex:panelGroup>
        <!-- END GRID -->

<!--
                *****   *       *   **      *****   *****
                *       *       *   *  *    *       *   *
                *****   *       *   *   *   *****   *****
                    *   *       *   *  *    *       * *
                *****   *****   *   **      *****   *   *
-->
        <apex:panelGroup rendered="{!layout == 'Slider'}">
            <div id="myCarousel{!componentId}" class="carousel slide">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <apex:variable value="0" var="qitems"/>
                    <apex:repeat value="{!newsList}" var="n">
                        <li data-target="#myCarousel{!componentId}" data-slide-to="{!qitems}"></li>
                        <apex:variable value="{!VALUE(qitems)+1}" var="qitems"/>
                    </apex:repeat>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <apex:repeat value="{!newsList}" var="n">

                        <div class="item">
                        <apex:panelGroup rendered="{!display == 'none'}">
                            <div>
                                <apex:repeat value="{!fieldSet}" var="f">
                                    <div class="{!$ObjectType.FieloEE__News__c.Fields[f].Name}">
                                        <c:FieloPRM_C_ShowField docURL="{!IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+n[f]+'&oid='+$Organization.Id}" record="{!n}" name="{!f}" describe="{!$ObjectType.FieloEE__News__c.Fields[f]}" />
                                    </div>
                                </apex:repeat>
                            </div>
                        </apex:panelGroup>

                        <apex:panelGroup rendered="{!display == 'Internal Page'}">
                            <apex:outputLink value="{!URLFOR($Site.Prefix + '/'+pageName, null, [Id=n.Id])+'&retURL='+$CurrentPage.Url+'&compId='+componentId }">
                                <div>
                                    <apex:repeat value="{!fieldSet}" var="f">
                                        <div class="{!$ObjectType.FieloEE__News__c.Fields[f].Name}">
                                            <c:FieloPRM_C_ShowField docURL="{!IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+n[f]+'&oid='+$Organization.Id}" record="{!n}" name="{!f}" describe="{!$ObjectType.FieloEE__News__c.Fields[f]}" />
                                        </div>
                                    </apex:repeat>
                                </div>
                            </apex:outputLink>
                        </apex:panelGroup>
                        </div>
                    </apex:repeat>
                </div>
                <!-- Carousel nav Check the web site of bootstrap Javascript -->
                    <a class="carousel-control left hidden-phone sliderLeft" href="#myCarousel{!componentId}" data-slide="prev"><i class="icon-angle-left"></i></a>
                    <a class="carousel-control right hidden-phone sliderRight" href="#myCarousel{!componentId}" data-slide="next"><i class="icon-angle-right"></i></a>
                <!-- -->
            </div>

            <script type="text/javascript">
                domReady(function(event) {
                    $('#myCarousel{!componentId}').carousel({
                        interval: {!delay}
                    })
                    $('#myCarousel{!componentId}').carousel('next');
                });
            </script>
        </apex:panelGroup>

    </apex:panelGroup>
</div>


</apex:component>