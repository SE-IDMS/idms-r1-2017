<!--
  The MultiselectPicklist component implements a multiselect picklist similar
  to that seen when adding tabs to a Force.com application.
  
  HTML elements use the same classes as the native multiselect picklist, to
  keep visual consistency in the UI.
  
  In addition to the visible elements, the component contains two hidden input
  elements, the purpose of which is to hold a string representation of the
  contents of each listbox. As options are added, removed or moved within the
  listboxes, the content of the hidden elements is synchronized to the content
  of the listboxes. When the Visualforce page is submitted, the 
  MultiselectController updates its SelectOption[] variables from these hidden 
  elements.
 -->
<apex:component controller="AP_MultiselectController">
  <apex:attribute name="leftLabel" description="Label on left listbox."
    type="String" required="true" />
  <apex:attribute name="rightLabel" description="Label on right listbox."
    type="String" required="true" />
  <apex:attribute name="size" description="Size of listboxes."
    type="Integer" required="true" />
  <apex:attribute name="width" description="Width of listboxes."
    type="String" required="true" />

  <apex:attribute name="leftOptins" description="Options list for left listbox." type="SelectOption[]" required="true" assignTo="{!leftOptions}" />
  <apex:attribute name="rightOptins" description="Options list for right listbox." type="SelectOption[]" required="true" assignTo="{!rightOptions}" />
  <apex:attribute name="hasParentOption" description="Option indicates that it has a parent in the same list" type="Boolean" required="false" default="false" assignTo="{!hasParent}" />
  <apex:attribute name="showUpDownArrow" description="Option indicates whether to show up/down arrow" type="Boolean" required="false" default="false" assignTo="{!hasUpDownArrow}" />

  <apex:includeScript value="{!URLFOR($Resource.jQueryLatest, 'jquery1.9.1.js')}" />
  <apex:includeScript value="{!URLFOR($Resource.MultiSelectScriptHandler)}" />
  
  <apex:outputPanel id="multiselectPanel" layout="block" styleClass="duelingListBox">
    <table class="layout">
      <tbody>
        <tr>
          <td class="selectCell">
            <apex:outputPanel layout="block" styleClass="selectTitle">
              <!-- 
                Visualforce prepends the correct prefix to the outputLabel's 
                'for' attribute
              -->
              <apex:outputLabel value="{!leftLabel}" 
                for="multiselectPanel:leftList" />
            </apex:outputPanel>
            <select id="{!$Component.multiselectPanel}:leftList" 
              class="multilist" multiple="multiple" size="{!size}" 
              style="width: {!width};">
              <apex:repeat value="{!leftOptions}" var="option">
                <option value="{!option.value}">{!option.label}</option>
              </apex:repeat>
            </select>
          </td>
          <td class="buttonCell">
            <apex:outputPanel layout="block" styleClass="text">Add</apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="text">
              <apex:outputLink value="javascript:moveSelectedOptions('{!$Component.multiselectPanel}:leftList', 
                  '{!$Component.multiselectPanel}:rightList', '{!$Component.leftHidden}', 
                  '{!$Component.rightHidden}','{!$Component.hasParentOptionHidden}');"
                id="btnRight">
                <apex:image value="/s.gif" alt="Add" styleClass="rightArrowIcon"
                  title="Add" />
              </apex:outputLink>
            </apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="text">
              <apex:outputLink value="javascript:moveSelectedOptions('{!$Component.multiselectPanel}:rightList', 
                  '{!$Component.multiselectPanel}:leftList', '{!$Component.rightHidden}', 
                  '{!$Component.leftHidden}','{!$Component.hasParentOptionHidden}');"
                id="btnLeft">
                <apex:image value="/s.gif" alt="Remove"
                  styleClass="leftArrowIcon" title="Remove" />
              </apex:outputLink>
            </apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="duelingText">Remove</apex:outputPanel>
          </td>
          <td class="selectCell">
            <apex:outputPanel layout="block" styleClass="selectTitle">
              <apex:outputLabel value="{!rightLabel}" for="multiselectPanel:rightList" />
            </apex:outputPanel>
            <select id="{!$Component.multiselectPanel}:rightList" 
              class="multilist" multiple="multiple" size="{!size}" 
              style="width: {!width};">
              <apex:repeat value="{!rightOptions}" var="option">
                <option value="{!option.value}">{!option.label}</option>
              </apex:repeat>
            </select>
          </td>
          <td class="buttonCell"><apex:outputPanel layout="block"
              styleClass="text" rendered="{!hasUpDownArrow}">Up</apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="text" rendered="{!hasUpDownArrow}">
              <apex:outputLink value="javascript:slideSelectedOptionsUp('{!$Component.multiselectPanel}:rightList', 
                  '{!$Component.rightHidden}');"
                id="upBtn">
                <apex:image value="/s.gif" alt="Up" styleClass="upArrowIcon"
                  title="Up" />
              </apex:outputLink>
            </apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="text" rendered="{!hasUpDownArrow}">
              <apex:outputLink value="javascript:slideSelectedOptionsDown('{!$Component.multiselectPanel}:rightList', 
                  '{!$Component.rightHidden}');"
                id="downBtn">
                <apex:image value="/s.gif" alt="Down" styleClass="downArrowIcon"
                  title="Down" />
              </apex:outputLink>
            </apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="text" rendered="{!hasUpDownArrow}">Down</apex:outputPanel>
          </td>
        </tr>
      </tbody>
    </table>
    <apex:inputHidden value="{!leftOptionsHidden}" id="leftHidden" />
    <apex:inputHidden value="{!rightOptionsHidden}" id="rightHidden" />
    <apex:inputHidden value="{!hasParent}" id="hasParentOptionHidden" />
  </apex:outputPanel>
  <script type="text/javascript">

    // initialize the string representations
    buildOutputString(document.getElementById('{!$Component.multiselectPanel}:leftList'), 
        document.getElementById('{!$Component.leftHidden}'));
    buildOutputString(document.getElementById('{!$Component.multiselectPanel}:rightList'), 
        document.getElementById('{!$Component.rightHidden}'));

    setChildIndent ('{!$Component.multiselectPanel}:leftList', 
        '{!$Component.multiselectPanel}:rightList', {!hasParent} );
        
jQY(document).ready(function ($){
        
        var selClass = '{!$Component.multiselectPanel}:rightList';
        var tElemId = '#' + escapeStr(selClass);
        var sorceClass = '{!$Component.multiselectPanel}:leftList';
        var sElemId = '#' + escapeStr(sorceClass);
        
        var clonedPELems = jQY('option:not([value*="_"])', tElemId );       
      
        var tmpSelect = jQY('<select>');
        jQY(clonedPELems).appendTo(tmpSelect);
        jQY.each(clonedPELems, function(idx, val) {
            var pId = jQY(val).val();
            var clonedCELems = jQY('option[value$="_' + pId + '"]', tElemId );
            jQY(clonedCELems).insertAfter(val);
            if(jQY('option[value$="_' + pId + '"]', sElemId ).length <= 0)
                jQY('option[value$="'+ pId + '"]', sElemId ).remove();
            
        });
        
        jQY(tElemId + ' option').remove();      
        jQY('option', tmpSelect).appendTo(tElemId);        
        
        
    });        
        
  </script>
</apex:component>