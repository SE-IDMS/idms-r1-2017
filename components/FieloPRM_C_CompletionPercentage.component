<!-- -------------------------------------------------------------------------------------------------
    Author: Fielo Team 
    Date: 23/02/2015
    Description: Component that shows the completion percentage of a Program(challenge) to subscribe.
    Related Components: -
---------------------------------------------------------------------------------------------------->
<apex:component controller="FieloPRM_AP_CompletionPercentageCon"  allowDML="true">
    <apex:attribute name="Idchallenge" type="String" description="The Challenge Id" required="true" assignTo="{!challengeId}" />  

    <div class="row-fluid CompletionPercentage">
        <div class="span10">
            <ul class="missionList">
                <apex:repeat value="{!missions}" var="m">
                    <li>
                        <div class="clearfix containerInfo">
                            <apex:repeat value="{!fieldSet}" var="fsl">
                                <apex:panelGroup rendered="{!NOT(CONTAINS(fsl, 'progressbar'))}">
                                    <div class="{!$ObjectType.FieloCH__Mission__c.Fields[fsl].Name} {!SUBSTITUTE(SUBSTITUTE(fsl, 'c:', ''), $Setup.FieloEE__PublicSettings__c.FieloEE__ChallengesPrefix__c, '')} clearfix">                                          
                                        <span>{!m[fsl]}</span>
                                    </div>
                                    <div class="{!$ObjectType.FieloCH__Mission__c.Fields[fsl].Name} {!SUBSTITUTE(SUBSTITUTE(fsl, 'c:', ''), $Setup.FieloEE__PublicSettings__c.FieloEE__ChallengesPrefix__c, '')} clearfix">&nbsp;</div>
                                </apex:panelGroup>
                                <apex:panelGroup rendered="{!AND((m.RecordType.DeveloperName == 'WithObjective'), (NOT(ISNULL(missionStatusMap))),(CONTAINS(fsl, 'progressbar'))) }" >
                                    <apex:panelGroup rendered="{!AND((NOT(ISNULL(missionStatusMap[m.Id]))), missionStatusMap[m.Id] < 100)}">
                                        <div class="containerBar">
                                            <div class="progress internalProgress">
                                                <div class="bar" style="width: {!missionStatusMap[m.Id]}%;"></div>
                                                <div class="progress-number">
                                                    <apex:outputText value="{0,number,0} %">
                                                        <apex:param value="{!missionStatusMap[m.Id]}" />
                                                    </apex:outputText>
                                                </div>
                                            </div>
                                        </div>
                                    </apex:panelGroup>
                                    <apex:panelGroup rendered="{!AND((NOT(ISNULL(missionStatusMap[m.Id]))), missionStatusMap[m.Id] >= 100)}">
                                        <div class="internalProgress">
                                            <span class="icon-stack">
                                                <i class="icon-sign-blank icon-stack-base"></i>
                                                <i class="icon-ok icon-light"></i>
                                            </span>
                                        </div>
                                    </apex:panelGroup>
                                </apex:panelGroup>
                                <apex:panelGroup rendered="{!AND((m.RecordType.DeveloperName == 'WithoutObjective'), (NOT(ISNULL(missionStatusMap))), (NOT(ISNULL(missionStatusMap[m.Id]))), (CONTAINS(fsl, 'progressbar')))}">
                                    <div class="performanceStatus internalProgress">
                                        <span class="badge" >
                                            <apex:outputText value="{0,number,0} ">
                                                <apex:param value="{!missionStatusMap[m.Id]}" />
                                            </apex:outputText>
                                        </span>
                                    </div>
                                </apex:panelGroup>
                            </apex:repeat>
                             
                        </div>
                    </li>
                </apex:repeat>
            </ul>
        </div>
        <div class="span2 button">
            <apex:form >
                <apex:commandLink rendered="{!buttonJoin}" styleClass="btn btn-success" action="{!JoinAction}" value="{!$Label.Fielo_PRM_CL_007_JoinButton}" id="joinButton"/>
                <apex:commandLink rendered="{!NOT(buttonJoin)}" styleClass="btn btn-success disabled" value="{!$Label.Fielo_PRM_CL_007_JoinButton}" id="joinButtonDisable"/>
            </apex:form>
        </div>
    </div>

</apex:component>