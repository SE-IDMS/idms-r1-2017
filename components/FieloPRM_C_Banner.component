<!-------------------------------------------------------------------
- Company: Fielo
- Created Date: 22/08/2016
- Description: 
-------------------------------------------------------------------->
<apex:component layout="none" >
    <apex:attribute Name="type" description="Type of banner" required="false" type="String" default="1" />        
    <apex:attribute Name="delay" description="Banner Delay" required="false" type="Integer" default="3000" />
    <apex:attribute Name="componentId" description="component related to" required="false" type="String" />
    <apex:attribute Name="bannersList"  description="banner records" required="false" type="FieloEE__Banner__c[]" />
    <apex:attribute Name="fieldsML" description="banner records" required="false" type="FieloPRM_FieldsML" />
    <apex:attribute name="cssClasses" description="css classes" type="String" />
    <apex:attribute name="docUrl" description="docUrl" type="String" />

    <script type="text/javascript">
        (function(exports, d) {
            function domReady(fn, context) {

            function onReady(event) {
              d.removeEventListener("DOMContentLoaded", onReady);
              fn.call(context || exports, event);
            }

            function onReadyIe(event) {
              if (d.readyState === "complete") {
                d.detachEvent("onreadystatechange", onReadyIe);
                fn.call(context || exports, event);
              }
            }

            d.addEventListener && d.addEventListener("DOMContentLoaded", onReady) ||
            d.attachEvent      && d.attachEvent("onreadystatechange", onReadyIe);
            }

            exports.domReady = domReady;
            })(window, document);
    </script>

    <div class="banner{!type} banner {!cssClasses}" >        
        <!-- TYPE Slider -->
        <apex:panelGroup rendered="{!type == 'Slider'}">
            <div id="myCarousel{!componentId}" class="carousel slide">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <apex:variable value="0" var="qitems"/>
                    <apex:repeat value="{!bannersList}" var="b">
                        <li data-target="#myCarousel{!componentId}" data-slide-to="{!qitems}"></li>
                        <apex:variable value="{!VALUE(qitems)+1}" var="qitems"/>
                    </apex:repeat>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <apex:repeat value="{!bannersList}" var="b">
                        <div class="item">
                            <apex:panelGroup rendered="{!b[fieldsML.fieldAttachment] != '' && b[fieldsML.fieldAttachment] != null}" >
                                <apex:panelGroup rendered="{!b[fieldsML.fieldLink] == ''}">
                                    <img alt="{!b[fieldsML.fieldDescription]}" src="{!IF(LEFT(b[fieldsML.fieldAttachment],3) == '00P', URLFOR($Action.Attachment.Download, b[fieldsML.fieldAttachment]), IF(LEFT(b[fieldsML.fieldAttachment],3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+b[fieldsML.fieldAttachment]+'&oid='+$Organization.Id, URLFOR($Resource.FieloEE__images, b[fieldsML.fieldAttachment])))}"/>
                                </apex:panelGroup>
                                <apex:panelGroup rendered="{!b[fieldsML.fieldLink] != ''}">
                                    <a href="{!b[fieldsML.fieldLink]}"><img alt="{!b[fieldsML.fieldDescription]}" src="{!IF(LEFT(b[fieldsML.fieldAttachment],3) == '00P', URLFOR($Action.Attachment.Download, b[fieldsML.fieldAttachment]), IF(LEFT(b[fieldsML.fieldAttachment],3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+b[fieldsML.fieldAttachment]+'&oid='+$Organization.Id, URLFOR($Resource.FieloEE__images, b[fieldsML.fieldAttachment])))}"/></a>
                                </apex:panelGroup>
                            </apex:panelGroup>
                            <apex:panelGroup rendered="{!! (ISBLANK(b[fieldsML.fieldOverlayHtml]) || ISNULL(b[fieldsML.fieldOverlayHtml])) }" layout="none">
                                <div class="banner-text bannerText--is{!fieldsML.fieldOverlayHtml}">
                                    <apex:outputText value="{!b[fieldsML.fieldOverlayHtml]}" escape="false"/>
                                </div>
                            </apex:panelGroup>
                            <apex:panelGroup rendered="{!  (ISBLANK(b[fieldsML.fieldOverlayHtml]) || ISNULL(b[fieldsML.fieldOverlayHtml])) }" layout="none">
                                <div class="banner-text bannerText--is{!fieldsML.fieldOverlayText}">
                                    <apex:outputText value="{!b[fieldsML.fieldOverlayText]}" escape="false"/>
                                </div>
                            </apex:panelGroup>
                        </div>
                    </apex:repeat>
                </div>
                <!-- Carousel nav Check the web site of bootstrap Javascript-->
                <a class="carousel-control left hidden-phone sliderLeft" href="#myCarousel{!componentId}" data-slide="prev"><i class="icon-angle-left"></i></a>
                <a class="carousel-control right hidden-phone sliderRight" href="#myCarousel{!componentId}" data-slide="next"><i class="icon-angle-right"></i></a>
            </div>
        </apex:panelGroup>
        
        
        <!-- TYPE Row -->
        <apex:panelGroup rendered="{!type == 'Row'}">
            <div class="row-fluid">
                <apex:repeat value="{!bannersList}" var="b">
                    <div class="span{!FLOOR(12/bannersList.size)} item">
                        <apex:panelGroup rendered="{!b[fieldsML.fieldAttachment] != '' && b[fieldsML.fieldAttachment] != null}">
                            <apex:panelGroup rendered="{!b[fieldsML.fieldLink] == ''}">
                                <img alt="{!b[fieldsML.fieldDescription]}" src="{!IF(LEFT(b[fieldsML.fieldAttachment],3) == '00P', URLFOR($Action.Attachment.Download, b[fieldsML.fieldAttachment]), IF(LEFT(b[fieldsML.fieldAttachment],3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+b[fieldsML.fieldAttachment]+'&oid='+$Organization.Id, URLFOR($Resource.FieloEE__Images, b[fieldsML.fieldAttachment])))}" />
                            </apex:panelGroup>
                            <apex:panelGroup rendered="{!b[fieldsML.fieldLink] != ''}">
                                <a href="{!b[fieldsML.fieldLink]}"><img alt="{!b[fieldsML.fieldDescription]}" src="{!IF(LEFT(b[fieldsML.fieldAttachment],3) == '00P', URLFOR($Action.Attachment.Download, b[fieldsML.fieldAttachment]), IF(LEFT(b[fieldsML.fieldAttachment],3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+b[fieldsML.fieldAttachment]+'&oid='+$Organization.Id, URLFOR($Resource.FieloEE__images, b[fieldsML.fieldAttachment])))}"/></a>
                            </apex:panelGroup>
                        </apex:panelGroup>
                        <apex:panelGroup rendered="{!! (ISBLANK(b[fieldsML.fieldOverlayHtml]) || ISNULL(b[fieldsML.fieldOverlayHtml])) }" layout="none">
                            <div class="banner-text bannerText--is{!fieldsML.fieldOverlayHtml}">
                                <apex:outputText value="{!b[fieldsML.fieldOverlayHtml]}" escape="false"/>
                            </div>
                        </apex:panelGroup>
                        <apex:panelGroup rendered="{!  (ISBLANK(b[fieldsML.fieldOverlayHtml]) || ISNULL(b[fieldsML.fieldOverlayHtml])) }" layout="none">
                            <div class="banner-text bannerText--is{!fieldsML.fieldOverlayText}">
                                <apex:outputText value="{!b[fieldsML.fieldOverlayText]}" escape="false"/>
                            </div>
                        </apex:panelGroup>
                    </div>
                </apex:repeat>
            </div>
        </apex:panelGroup>
        
        
        <!-- TYPE Column -->
        <apex:panelGroup rendered="{!type == 'Column'}">
            <apex:repeat value="{!bannersList}" var="b">
                <div class="row-fluid">
                    <div class="span12 item">
                        <apex:panelGroup rendered="{!b[fieldsML.fieldAttachment] != '' && b[fieldsML.fieldAttachment] != null}">
                            <apex:panelGroup rendered="{!b[fieldsML.fieldLink] == ''}">
                                <img alt="{!b[fieldsML.fieldDescription]}" src="{!IF(LEFT(b[fieldsML.fieldAttachment],3) == '00P', URLFOR($Action.Attachment.Download, b[fieldsML.fieldAttachment]), IF(LEFT(b[fieldsML.fieldAttachment],3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+b[fieldsML.fieldAttachment]+'&oid='+$Organization.Id, URLFOR($Resource.FieloEE__images, b[fieldsML.fieldAttachment])))}"/>
                            </apex:panelGroup>
                            <apex:panelGroup rendered="{!b[fieldsML.fieldLink] != ''}">
                                <a href="{!b[fieldsML.fieldLink]}"><img alt="{!b[fieldsML.fieldDescription]}" src="{!IF(LEFT(b[fieldsML.fieldAttachment],3) == '00P', URLFOR($Action.Attachment.Download, b[fieldsML.fieldAttachment]), IF(LEFT(b[fieldsML.fieldAttachment],3) == '015', IF(ISBLANK($Setup.FieloEE__PublicSettings__c.FieloEE__Instance__c), $Site.Prefix, DocURL)+'/servlet/servlet.ImageServer?id='+b[fieldsML.fieldAttachment]+'&oid='+$Organization.Id, URLFOR($Resource.FieloEE__images, b[fieldsML.fieldAttachment])))}"/></a>
                            </apex:panelGroup>
                        </apex:panelGroup>
                        <apex:panelGroup rendered="{!!(ISBLANK(b[fieldsML.fieldOverlayHtml]) || ISNULL(b[fieldsML.fieldOverlayHtml]))}" layout="none">
                            <div class="banner-text bannerText--is{!fieldsML.fieldOverlayHtml}">
                                <apex:outputText value="{!b[fieldsML.fieldOverlayHtml]}" escape="false"/>
                            </div>
                        </apex:panelGroup>
                        <apex:panelGroup rendered="{!(ISBLANK(b[fieldsML.fieldOverlayHtml]) || ISNULL(b[fieldsML.fieldOverlayHtml]))}" layout="none">
                            <div class="banner-text bannerText--is{!fieldsML.fieldOverlayText}">
                                <apex:outputText value="{!b[fieldsML.fieldOverlayText]}" escape="false"/>
                            </div>
                        </apex:panelGroup>
                    </div>
                </div>
            </apex:repeat>
        </apex:panelGroup>
        
        <script type="text/javascript">
            domReady(function(event) {
                $('#myCarousel{!componentId}').carousel({
                    interval: {!delay}
                })
                $('#myCarousel{!componentId}').carousel('next');
            });
        </script>
    </div>
</apex:component>